# LambdaMod

Abbreviated to "LMOD".

![](res/lambdamod.png)

An experimental multiplayer sandbox game with optional VR support that mixes the best of pen-and-paper roleplaying and video games. Just a passion project and in an early state, so stuff might be broken or not done properly.

Join the [Matrix room](https://app.element.io/#/room/#lambdamod:matrix.org) or the [Discord server](https://discord.gg/5dXtjdGkzD)!

![](doc/demo.gif)

## What is this?

LambdaMod can be used in many different ways including some I probably can't even predict. You could play deathmatches against your friends in a bespoke ruleset you like, build a kart racer or use it as a VR social space. **That said, it is first and foremost:**

A game intended to exist in the gap between pen-and-paper roleplaying games and real-time multiplayer video games. From traditional games it takes the flexibility of a human game master and a minimalist presentation, relying on the players' imagination captured through text and simple line graphics which can be created quickly. From video games it takes the speed and impartiality of a computer rule system and a system of immersive natural gameplay, where the players can explore environments fluidly and personally go toe-to-toe with their enemies.

It is intended to be played much like a pen-and-paper roleplaying game, with one or more game masters creating a scenario at the same time as it is being played by one or more players. Code replaces dice, rulebooks and character sheets, while line graphics and synthesizer audio replace battle maps and figurines in guiding the players' imagination. The art assets have been made as minimal as possible so that game masters can keep up with the pace of play and create content as fast as it is consumed.

## Contributing

**Unfortunately, contributions to this repository are not presently accepted.**

This is because there are a wide variety of both storefronts (such as the App Store, the only practical means of distribution for iOS devices) and VR hardware SDKs which have GPL-incompatible licenses. This means in the future it may be necessary to re-license certain versions of the project under other licenses to ensure it can reach the widest possible audience. This re-licensing is only possible if there are no external contributions, or all external contributions are accompanied by a Contributor License Agreement. I do not feel comfortable asking contributors to sign a CLA, so unfortunately contributions must be rejected.

You can however still contribute by joining the Matrix/Discord above and describing functionality your usage of LambdaMod would benefit from. I can't predict every way in which it will be used and a lot of use cases will need builtin features to support them.

## Platform Support

Right now the project only supports Linux and requires Vulkan 1.2 support from the graphics drivers. If only Vulkan 1.1 or 1.0 is available LambdaMod will attempt to replace the required functionality with equivalent extensions, which may allow it to run on some platforms that don't meet the minimum requirement. Raytracing is not supported on version 1.0.

## Roadmap

See [here](doc/roadmap.md).

## Building

**Dependencies:**

- ASIO (optional, required due to an upstream issue with WebSocket++)
- Boost (only required for build, compiled into the resulting binary)
- Bullet Physics (double precision version)
- libcurl
- glslang (only required for build, to compile the shaders)
- lunasvg
- miniupnpc (optional, if available UPnP support will be enabled for the server)
- msgpack-cxx (only required for build, compiled into the resulting binary)
- OpenSSL
- OpenXR (optional, if available VR support will be enabled for the client)
- libopus
- libpng
- freetype2
- Vulkan Memory Allocator (only required for build, compiled into the resulting binary)
- WebSocket++ (optional, if available the server will be able to receive connections from web clients) (only required for build, compiled into the resulting binary)
- libyaml

To build the project you will need to install all the other dependencies, then get `vk_mem_alloc.h` from [GPUOpen's VulkanMemoryAllocator](https://github.com/GPUOpen-LibrariesAndSDKs/VulkanMemoryAllocator) and place it in `src/libraries/vk_mem_alloc/`. Finally, run the following commands:

```sh
mkdir build
cd build
cmake ..
make
```

**Note:** If you are compiling with GCC and you experience compilation errors originating from the websocket++ headers: this is because those headers contain syntax which was removed in C++20 and which GCC does not maintain support for through compiler extensions (Clang does). websocket++ needs to release a new version to fix this but this hasn't occurred so you will need to edit the headers yourself to remove the offending syntax or replace them with the fixed version from the `develop` branch [here](https://github.com/zaphoyd/websocketpp/tree/develop).

The binaries are generated in the `bin` directory.

The following CMake flags are available:

| Flag                  | Default Value | Use                                                                                                                                                                                                                                                                                         |
|-----------------------|---------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `LMOD_PERSONAL`       | On            | When enabled compiles with full optimizations for the local machine. Fully optimized binaries may not be portable between processors.                                                                                                                                                       |
| `LMOD_DEPLOYMENT`     | On            | When enabled compiles with full optimizations and when disabled compiles with debugging symbols.                                                                                                                                                                                            |
| `LMOD_VK_VALIDATION`  | Off           | Compiles with Vulkan validation layers. Useful for debugging graphics issues but may have negative effects on performance.                                                                                                                                                                  |
| `LMOD_XR_VALIDATION`  | Off           | Compiles with OpenXR validation layers. Useful for debugging VR issues but may have negative effects on performance.                                                                                                                                                                        |
| `LMOD_WAYLAND`        | Off           | Only meaningful on Linux. Uses Wayland for windowing and input rather than X11. Experimental. This may cause the build to fail with "undefined reference" errors when first enabled, in which case you need to re-run CMake to detect the new source files generated by enabling this flag. |
| `LMOD_PIPEWIRE`       | Off           | Only meaningful on Linux. Uses PipeWire for audio input and output rather than PulseAudio. Experimental.                                                                                                                                                                                    |
| `LMOD_FAKELAG`        | Off           | Compiles with fake network latency. Useful for testing the Internet gameplay experience but will add 100 ms of delay to all messages arriving on the client.                                                                                                                                |
| `LMOG_DEBUG_MESSAGES` | Off           | Compiles with verbose debug messages. Useful for audio debugging.                                                                                                                                                                                                                           |

## Usage

See [here](doc/manual/readme.md).

## License

Developed by Amini Allight. Licensed under the GPL 3.0 license.

This project contains data from the CIPIC HRTF database under its license. Copyright notice:

> Copyright (c) 2001 The Regents of the University of California. All Rights Reserved

This project contains files from the Liberation Sans and Open Sans fonts under their respective licenses.

This project uses FreeType2 under the FreeType License.
