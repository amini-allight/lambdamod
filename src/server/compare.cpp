/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "compare.hpp"

Compare::Compare(
    const StepFrame& lastSeen,
    StepFrame& now,
    UserID userID,
    const function<void()>& step,
    size_t stepCount
)
{
    for (size_t i = 0; i < stepCount; i++)
    {
        step();
    }

    tie(_deltaEvents, _sideEffects) = now.compare(lastSeen);

    // User specific events
    const User* self = now.state.getUser(userID);
    const User& previousSelf = *lastSeen.state.getUser(userID);

    const Entity* host = now.state.get(self->entityID());

    vector<NetworkEvent> stateSelfEvents = now.compareUser(lastSeen, self);
    _selfDeltaEvents = stateSelfEvents;

    vector<NetworkEvent> userSelfEvents = self->compareSelf(previousSelf, host);
    _selfDeltaEvents.insert(_selfDeltaEvents.end(), userSelfEvents.begin(), userSelfEvents.end());
}

const vector<NetworkEvent>& Compare::deltaEvents() const
{
    return _deltaEvents;
}

const vector<EntityUpdateSideEffect>& Compare::sideEffects() const
{
    return _sideEffects;
}

const vector<NetworkEvent>& Compare::selfDeltaEvents() const
{
    return _selfDeltaEvents;
}
