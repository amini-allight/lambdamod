/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_tracker.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"
#include "script_tools.hpp"

TextTracker::TextTracker(TextID id, UserID userID, const optional<Lambda>& lambda, chrono::milliseconds startTime)
    : _id(id)
    , _type(Text_Unary)
    , _data(TextUnaryRequest())
    , _userID(userID)
    , _lambda(lambda)
    , _startTime(startTime)
{

}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextTitlecardRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Titlecard;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextTimeoutRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Timeout;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextUnaryRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Unary;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextBinaryRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Binary;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextOptionsRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Options;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextChooseRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Choose;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextAssignValuesRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Assign_Values;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextSpendPointsRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Spend_Points;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextAssignPointsRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Assign_Points;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextVoteRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Vote;
    _data = data;
}

template<>
TextTracker::TextTracker(
    TextID id,
    const TextChooseMajorRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
)
    : TextTracker(id, userID, lambda, startTime)
{
    _type = Text_Choose_Major;
    _data = data;
}

bool TextTracker::operator==(const TextTracker& rhs) const
{
    if (_id != rhs._id ||
        _type != rhs._type ||
        _userID != rhs._userID ||
        _lambda != rhs._lambda ||
        _startTime != rhs._startTime
    )
    {
        return false;
    }

    switch (_type)
    {
    default :
        fatal("Encountered unknown text tracker type '" + to_string(_type) + "'.");
    case Text_Titlecard :
        return get<TextTitlecardRequest>(_data) == get<TextTitlecardRequest>(_data);
    case Text_Timeout :
        return get<TextTimeoutRequest>(_data) == get<TextTimeoutRequest>(_data);
    case Text_Unary :
        return get<TextUnaryRequest>(_data) == get<TextUnaryRequest>(_data);
    case Text_Binary :
        return get<TextBinaryRequest>(_data) == get<TextBinaryRequest>(_data);
    case Text_Options :
        return get<TextOptionsRequest>(_data) == get<TextOptionsRequest>(_data);
    case Text_Choose :
        return get<TextChooseRequest>(_data) == get<TextChooseRequest>(_data);
    case Text_Assign_Values :
        return get<TextAssignValuesRequest>(_data) == get<TextAssignValuesRequest>(_data);
    case Text_Spend_Points :
        return get<TextSpendPointsRequest>(_data) == get<TextSpendPointsRequest>(_data);
    case Text_Assign_Points :
        return get<TextAssignPointsRequest>(_data) == get<TextAssignPointsRequest>(_data);
    case Text_Vote :
        return get<TextVoteRequest>(_data) == get<TextVoteRequest>(_data);
    case Text_Choose_Major :
        return get<TextChooseMajorRequest>(_data) == get<TextChooseMajorRequest>(_data);
    }
}

bool TextTracker::operator!=(const TextTracker& rhs) const
{
    return !(*this == rhs);
}

TextID TextTracker::id() const
{
    return _id;
}

TextType TextTracker::type() const
{
    return _type;
}

UserID TextTracker::userID() const
{
    return _userID;
}

const optional<Lambda>& TextTracker::lambda() const
{
    return _lambda;
}

chrono::milliseconds TextTracker::startTime() const
{
    return _startTime;
}

void TextTracker::start()
{
    _startTime = currentTime();
}

bool TextTracker::matches(const TextResponse& response) const
{
    return response.id() == _id && response.type() == _type;
}

bool TextTracker::timedOut() const
{
    switch (_type)
    {
    default :
    case Text_Signal :
    case Text_Knowledge :
    case Text_Clear :
        fatal("Cannot query non-tracked text type '" + to_string(_type) + "' for timeout.");
    case Text_Titlecard :
        return _startTime != chrono::milliseconds(0)
            ? currentTime() - _startTime > textTitlecardDuration
            : false;
    case Text_Timeout :
        return _startTime != chrono::milliseconds(0)
            ? currentTime() - _startTime > chrono::milliseconds(static_cast<u64>(data<TextTimeoutRequest>().timeout * 1000))
            : false;
    case Text_Unary :
    case Text_Binary :
    case Text_Options :
    case Text_Choose :
    case Text_Assign_Values :
    case Text_Spend_Points :
    case Text_Assign_Points :
        return false;
    case Text_Vote :
        return _startTime != chrono::milliseconds(0)
            ? currentTime() - _startTime > voteDuration
            : false;
    case Text_Choose_Major :
        return false;
    }
}

TextRequestEvent TextTracker::event() const
{
    switch (_type)
    {
    default :
        fatal("Cannot query non-tracked text type '" + to_string(_type) + "' for event.");
    case Text_Titlecard :
        return TextRequestEvent(TextRequest(_id, data<TextTitlecardRequest>()));
    case Text_Timeout :
        return TextRequestEvent(TextRequest(_id, data<TextTimeoutRequest>()));
    case Text_Unary :
        return TextRequestEvent(TextRequest(_id, data<TextUnaryRequest>()));
    case Text_Binary :
        return TextRequestEvent(TextRequest(_id, data<TextBinaryRequest>()));
    case Text_Options :
        return TextRequestEvent(TextRequest(_id, data<TextOptionsRequest>()));
    case Text_Choose :
        return TextRequestEvent(TextRequest(_id, data<TextChooseRequest>()));
    case Text_Assign_Values :
        return TextRequestEvent(TextRequest(_id, data<TextAssignValuesRequest>()));
    case Text_Spend_Points :
        return TextRequestEvent(TextRequest(_id, data<TextSpendPointsRequest>()));
    case Text_Assign_Points :
        return TextRequestEvent(TextRequest(_id, data<TextAssignPointsRequest>()));
    case Text_Vote :
        return TextRequestEvent(TextRequest(_id, data<TextVoteRequest>()));
    case Text_Choose_Major :
        return TextRequestEvent(TextRequest(_id, data<TextChooseMajorRequest>()));
    }
}

template<>
TextTracker YAMLNode::convert() const
{
    ScriptContext context;

    TextID id = at("id").as<TextID>();
    UserID userID = at("userID").as<UserID>();
    TextType type = textTypeFromString(at("type").as<string>());
    optional<Lambda> lambda;

    if (at("lambda").as<optional<string>>())
    {
        lambda = runSafely(
            &context,
            *at("lambda").as<optional<string>>(),
            "loading text tracker lambda"
        )->front().lambda;
    }

    chrono::milliseconds startTime = currentTime() - chrono::milliseconds(at("age").as<u64>());

    switch (type)
    {
    default :
        fatal("Encountered unknown text type '" + to_string(type) + "' in serialized text tracker.");
    case Text_Titlecard :
        return TextTracker(
            id,
            at("data").as<TextTitlecardRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Timeout :
        return TextTracker(
            id,
            at("data").as<TextTimeoutRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Unary :
        return TextTracker(
            id,
            at("data").as<TextUnaryRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Binary :
        return TextTracker(
            id,
            at("data").as<TextBinaryRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Options :
        return TextTracker(
            id,
            at("data").as<TextOptionsRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Choose :
        return TextTracker(
            id,
            at("data").as<TextChooseRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Assign_Values :
        return TextTracker(
            id,
            at("data").as<TextAssignValuesRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Spend_Points :
        return TextTracker(
            id,
            at("data").as<TextSpendPointsRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Assign_Points :
        return TextTracker(
            id,
            at("data").as<TextAssignPointsRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Vote :
        return TextTracker(
            id,
            at("data").as<TextVoteRequest>(),
            userID,
            lambda,
            startTime
        );
    case Text_Choose_Major :
        return TextTracker(
            id,
            at("data").as<TextChooseMajorRequest>(),
            userID,
            lambda,
            startTime
        );
    }
}

template<>
void YAMLSerializer::emit(TextTracker v)
{
    startMapping();

    emitPair("id", v.id());
    emitPair("type", textTypeToString(v.type()));

    switch (v.type())
    {
    default :
        fatal("Cannot query non-tracked text type '" + to_string(v.type()) + "' for data.");
    case Text_Titlecard :
        emitPair("data", v.data<TextTitlecardRequest>());
        break;
    case Text_Timeout :
        emitPair("data", v.data<TextTimeoutRequest>());
        break;
    case Text_Unary :
        emitPair("data", v.data<TextUnaryRequest>());
        break;
    case Text_Binary :
        emitPair("data", v.data<TextBinaryRequest>());
        break;
    case Text_Options :
        emitPair("data", v.data<TextOptionsRequest>());
        break;
    case Text_Choose :
        emitPair("data", v.data<TextChooseRequest>());
        break;
    case Text_Assign_Values :
        emitPair("data", v.data<TextAssignValuesRequest>());
        break;
    case Text_Spend_Points :
        emitPair("data", v.data<TextSpendPointsRequest>());
        break;
    case Text_Assign_Points :
        emitPair("data", v.data<TextAssignPointsRequest>());
        break;
    case Text_Vote :
        emitPair("data", v.data<TextVoteRequest>());
        break;
    case Text_Choose_Major :
        emitPair("data", v.data<TextChooseMajorRequest>());
        break;
    }

    emitPair("userID", v.userID());
    emitPair("lambda", v.lambda() ? v.lambda()->toString() : optional<string>());
    emitPair("age", static_cast<i64>((currentTime() - v.startTime()).count()));

    endMapping();
}
