/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_UPNP
#include "upnp.hpp"
#include "log.hpp"
#include "constants.hpp"

#include <miniupnpc/miniupnpc.h>
#include <miniupnpc/upnpcommands.h>
#if defined(WIN32)
#include <Ws2tcpip.h>
#endif

static constexpr f64 upnpDiscoveryTimeout = 0.5; // seconds
static constexpr i32 upnpTTL = 2;

optional<Address> upnpForward(u16 port, bool ipv6)
{
    i32 result;

    UPNPDev* device = upnpDiscover(
        upnpDiscoveryTimeout * 1000,
        nullptr,
        nullptr,
        UPNP_LOCAL_PORT_ANY,
        ipv6,
        upnpTTL,
        &result
    );

    if (result != 0)
    {
        error("Failed to query UPnP device: " + to_string(result));
        return {};
    }

    UPNPUrls urls;
    IGDdatas datas;

    char igdInternalAddress[INET6_ADDRSTRLEN]{0};
    char igdExternalAddress[INET6_ADDRSTRLEN]{0};

    result = UPNP_GetValidIGD(
        device,
        &urls,
        &datas,
        igdInternalAddress,
        INET6_ADDRSTRLEN,
        igdExternalAddress,
        INET6_ADDRSTRLEN
    );

    if (result != 1)
    {
        error("Failed to query UPnP IGD: " + to_string(result));
        freeUPNPDevlist(device);
        return {};
    }

    string controlURL = urls.controlURL;
    string serviceType = datas.first.servicetype;

    FreeUPNPUrls(&urls);
    freeUPNPDevlist(device);

    result = UPNP_GetExternalIPAddress(
        controlURL.c_str(),
        serviceType.c_str(),
        igdExternalAddress
    );

    if (result != 0)
    {
        error("Failed to get UPnP IGD external IP address: " + to_string(result));
        return {};
    }

    result = UPNP_AddPortMapping(
        controlURL.c_str(),
        serviceType.c_str(),
        to_string(port).c_str(),
        to_string(port).c_str(),
        igdInternalAddress,
        gameName,
        "TCP",
        nullptr,
        "0"
    );

    if (result != 0)
    {
        error("Failed to add UPnP port mapping: " + to_string(result));
        return {};
    }

    log("UPnP port forwarding succeeded.");

    return Address(igdExternalAddress, port);
}
#endif
