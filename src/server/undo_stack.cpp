/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "undo_stack.hpp"
#include "global.hpp"

UndoStack::UndoStack(UserID userID)
    : userID(userID)
{

}

void UndoStack::undo(World* world)
{
    if (undoFrames.empty())
    {
        return;
    }

    UndoFrame frame = undoFrames.back();
    undoFrames.pop_back();

    frame.undo(world);

    redoFrames.push_back(frame);
}

void UndoStack::redo(World* world)
{
    if (redoFrames.empty())
    {
        return;
    }

    UndoFrame frame = redoFrames.back();
    redoFrames.pop_back();

    frame.redo(world);

    undoFrames.push_back(frame);
}

void UndoStack::add(const Entity* after)
{
    redoFrames.clear();

    undoFrames.push_back(UndoFrame::addFrame(after));

    if (undoFrames.size() > g_config.maxUndoHistory)
    {
        undoFrames.pop_front();
    }
}

void UndoStack::remove(const Entity* before)
{
    redoFrames.clear();

    undoFrames.push_back(UndoFrame::removeFrame(before));

    if (undoFrames.size() > g_config.maxUndoHistory)
    {
        undoFrames.pop_front();
    }
}

void UndoStack::modify(const Entity* before, const Entity* after, bool mergeable)
{
    redoFrames.clear();

    UndoFrame frame = UndoFrame::modifyFrame(before, after, mergeable);

    if (mergeable && !undoFrames.empty() && undoFrames.back().mergeable())
    {
        undoFrames.back().merge(frame);
    }
    else
    {
        undoFrames.push_back(frame);
    }

    if (undoFrames.size() > g_config.maxUndoHistory)
    {
        undoFrames.pop_front();
    }
}
