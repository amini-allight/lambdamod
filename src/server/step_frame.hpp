/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "game_state.hpp"
#include "network_event.hpp"

class StepFrame
{
public:
    StepFrame();

    void makeReplayOf(const StepFrame& previous);
    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> compare(const StepFrame& previous) const;
    vector<NetworkEvent> compareUser(const StepFrame& previous, const User* user) const;
    vector<NetworkEvent> initial() const;
    vector<NetworkEvent> initialUser(const User* user) const;

    void step();
    void applyHybridEffects();
    void push(const vector<function<bool()>>& hybridEffects);
    void push(UserID id, const NetworkEvent& event);
    void push(UserID id, const vector<NetworkEvent>& events);

    GameState state;

private:
    vector<function<bool()>> hybridEffects;
    vector<tuple<UserID, NetworkEvent>> events;
};
