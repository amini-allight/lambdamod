/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "address.hpp"
#include "disconnection_reason.hpp"
#include "network_event.hpp"
#include "websocket_server.hpp"

#include <openssl/bio.h>
#include <openssl/ssl.h>

class Client
{
public:
    Client(
        const Address& address,
        SSL* ssl,
        const function<void()>& connectCallback,
        const function<void(const NetworkEvent&)>& receiveCallback,
        const function<void()>& disconnectCallback
    );
    Client(const Client& rhs) = delete;
    Client(Client&& rhs) = delete;
    ~Client();

    Client& operator=(const Client& rhs) = delete;
    Client& operator=(Client&& rhs) = delete;

    bool step();

    void send(const NetworkEvent& event);

    void markExternallyDisconnected();

private:
    Address address;
    SSL* ssl;
    function<void()> connectCallback;
    function<void(const NetworkEvent&)> receiveCallback;
    function<void()> disconnectCallback;

    u64 nextMessageSize;
    string buffer;
    bool disconnect;
    bool externallyDisconnected;

    void onReceive(const string& data);
};

class Server
{
public:
    Server(
        const string& certFile,
        const string& keyFile,
        const function<void(NetID)>& connectCallback,
        const function<void(NetID, const NetworkEvent&)>& receiveCallback,
        const function<void(NetID)>& disconnectCallback
    );
    Server(const Server& rhs) = delete;
    Server(Server&& rhs) = delete;
    virtual ~Server();

    Server& operator=(const Server& rhs) = delete;
    Server& operator=(Server&& rhs) = delete;

    void step();

    void listen(const Address& address);
    void holePunch(const string& sessionID);
    void send(NetID id, const NetworkEvent& event);
    void disconnect(NetID id);

protected:
    function<void(NetID)> connectCallback;
    function<void(NetID, const NetworkEvent&)> receiveCallback;
    function<void(NetID)> disconnectCallback;

    SSL_CTX* ctx;

    map<Address, Client*> clients;
    NetID nextID;
    map<Address, NetID> ids;
    map<NetID, Address> addresses;

    mutex tasksLock;
    vector<function<void()>> tasks;

    optional<thread> listenThread;
    optional<thread> holePunchThread;
    bool holePunchRunning;

#if defined(LMOD_WEBSOCKET)
    WebSocketServer webSocketServer;
#endif

    virtual void acceptConnections(const Address& address) = 0;
    void createClient(const Address& address, SSL* ssl);

    virtual void holePunchConnections(const string& sessionID) = 0;
};
