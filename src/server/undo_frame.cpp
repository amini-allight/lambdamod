/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "undo_frame.hpp"
#include "world.hpp"
#include "entity.hpp"

UndoFrame::UndoFrame()
    : type(Undo_Frame_Add)
    , parentID(0)
    , id(0)
    , before(nullptr)
    , after(nullptr)
    , _mergeable(false)
{

}

UndoFrame::UndoFrame(const UndoFrame& rhs)
{
    copy(rhs);

    if (before)
    {
        before = new Entity(*rhs.before);
    }

    if (after)
    {
        after = new Entity(*rhs.after);
    }
}

UndoFrame::UndoFrame(UndoFrame&& rhs) noexcept
{
    copy(rhs);

    rhs.before = nullptr;
    rhs.after = nullptr;
}

UndoFrame::~UndoFrame()
{
    if (after)
    {
        delete after;
    }

    if (before)
    {
        delete before;
    }
}

UndoFrame UndoFrame::addFrame(const Entity* after)
{
    UndoFrame frame;

    frame.type = Undo_Frame_Add;
    frame.parentID = after->parent() ? after->parent()->id() : EntityID();
    frame.id = after->id();
    frame.after = new Entity(*after);

    return frame;
}

UndoFrame UndoFrame::removeFrame(const Entity* before)
{
    UndoFrame frame;

    frame.type = Undo_Frame_Remove;
    frame.parentID = before->parent() ? before->parent()->id() : EntityID();
    frame.id = before->id();
    frame.before = new Entity(*before);

    return frame;
}

UndoFrame UndoFrame::modifyFrame(const Entity* before, const Entity* after, bool mergeable)
{
    UndoFrame frame;

    frame.type = Undo_Frame_Modify;
    frame.parentID = before->parent() ? before->parent()->id() : EntityID();
    frame.id = before->id();
    frame.before = new Entity(*before);
    frame.after = new Entity(*after);
    frame._mergeable = mergeable;

    return frame;
}

UndoFrame& UndoFrame::operator=(const UndoFrame& rhs)
{
    if (&rhs != this)
    {
        if (before)
        {
            delete before;
        }

        if (after)
        {
            delete after;
        }

        copy(rhs);

        before = new Entity(*rhs.before);
        after = new Entity(*rhs.after);
    }

    return *this;
}

UndoFrame& UndoFrame::operator=(UndoFrame&& rhs) noexcept
{
    if (&rhs != this)
    {
        if (before)
        {
            delete before;
        }

        if (after)
        {
            delete after;
        }

        copy(rhs);

        rhs.before = nullptr;
        rhs.after = nullptr;
    }

    return *this;
}

bool UndoFrame::mergeable() const
{
    return _mergeable;
}

void UndoFrame::merge(const UndoFrame& newer)
{
    after = new Entity(*newer.after);
}

void UndoFrame::undo(World* world)
{
    switch (type)
    {
    case Undo_Frame_Add :
        world->remove(id);
        break;
    case Undo_Frame_Remove :
    {
        if (parentID)
        {
            Entity* parent = world->get(parentID);

            if (!parent)
            {
                return;
            }

            world->add(parent->id(), new Entity(parent, before->id(), *before));
        }
        else
        {
            world->add(EntityID(), new Entity(world, before->id(), *before));
        }
        break;
    }
    case Undo_Frame_Modify :
    {
        Entity* entity = world->get(id);

        if (!entity)
        {
            return;
        }

        if (entity->parent())
        {
            *entity = Entity(entity->parent(), before->id(), *before);
        }
        else
        {
            *entity = Entity(world, before->id(), *before);
        }
        break;
    }
    }
}

void UndoFrame::redo(World* world)
{
    switch (type)
    {
    case Undo_Frame_Add :
    {
        if (parentID)
        {
            Entity* parent = world->get(parentID);

            if (!parent)
            {
                return;
            }

            world->add(parent->id(), new Entity(parent, before->id(), *before));
        }
        else
        {
            world->add(EntityID(), new Entity(world, before->id(), *before));
        }
        break;
    }
    case Undo_Frame_Remove :
        world->remove(id);
        break;
    case Undo_Frame_Modify :
    {
        Entity* entity = world->get(id);

        if (!entity)
        {
            return;
        }

        if (entity->parent())
        {
            Entity* parent = world->get(parentID);

            if (!parent)
            {
                return;
            }

            *entity = Entity(entity->parent(), after->id(), *after);
        }
        else
        {
            *entity = Entity(world, after->id(), *after);
        }
        break;
    }
    }
}

void UndoFrame::copy(const UndoFrame& rhs)
{
    type = rhs.type;
    parentID = rhs.parentID;
    id = rhs.id;
    before = rhs.before;
    after = rhs.after;
    _mergeable = rhs._mergeable;
}
