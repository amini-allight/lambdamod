/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "controller.hpp"
#include "tools.hpp"
#include "cryptography.hpp"
#include "global.hpp"
#include "error_messages.hpp"
#include "yaml_tools.hpp"
#include "paths.hpp"

Controller::Controller(
    Server* server,
    const string& password,
    const string& adminPassword,
    const string& savePath,
    const optional<string>& joinURL
)
    : server(server)
    , password(password)
    , adminPassword(adminPassword)
    , savePath(savePath)
    , saveBackupPath(savePath + backupExt)
    , joinURL(joinURL)
    , stepIndex(0)
    , shouldSave(false)
    , saving(true)
    , lastSaveTime(0)
    , pingSentTime(0)
{
    bool loaded = load();

    if (!loaded)
    {
        now.state.addWorld("main");
    }

    g_controller = this;
}

Controller::~Controller()
{
    g_controller = nullptr;

    save();
}

void Controller::run()
{
    stepSystemInputs();

    stepSimulation();

    stepSystemOutputs();
}

void Controller::onReceive(NetID id, const NetworkEvent& event)
{
    auto it = connections.find(NetID(id));

    Connection* connection = nullptr;

    if (it != connections.end())
    {
        connection = &it->second;
    }
    else
    {
        connections.insert({ id, Connection() });
        connection = &connections.at(id);
    }

    if (userMapping.contains(id) && !event.isSystem())
    {
        connection->eventBatcher.push(event);
    }
    else
    {
        systemEvents.push_back({ id, event });
    }
}

void Controller::onLeave(NetID id)
{
    systemEvents.push_back({ id, NetworkEvent(LeaveEvent()) });
}

void Controller::addCheckpoint(const string& name)
{
    _checkpoints.insert_or_assign(name, Checkpoint(now));

    sendAll(CheckpointEvent(Checkpoint_Event_Add, name));
}

void Controller::removeCheckpoint(const string& name)
{
    _checkpoints.erase(name);

    sendAll(CheckpointEvent(Checkpoint_Event_Remove, name));
}

void Controller::revertCheckpoint(const string& name)
{
    Checkpoint checkpoint = _checkpoints.at(name);

    now = checkpoint.get();

    for (auto it = _checkpoints.begin(); it != _checkpoints.end();)
    {
        if (it->second.newer(checkpoint))
        {
            sendAll(CheckpointEvent(Checkpoint_Event_Remove, it->first));

            _checkpoints.erase(it++);
        }
        else
        {
            it++;
        }
    }

    replayableHybridEffects.push_back([this, checkpoint]() -> bool {
        now = checkpoint.get();

        // we're already in hybrid effect application when this occurs so if we don't do this the effects stored in the checkpoint won't be used
        now.applyHybridEffects();

        return true;
    });
}

const map<string, Checkpoint>& Controller::checkpoints() const
{
    return _checkpoints;
}

void Controller::setShouldSave()
{
    shouldSave = true;
}

void Controller::sendOne(UserID id, const NetworkEvent& event) const
{
    server->send(userReverseMapping.at(id), event);
}

void Controller::sendAll(const NetworkEvent& event) const
{
    for (const auto& [ id, connection ] : connections)
    {
        server->send(id, event);
    }
}

void Controller::disconnect(NetID id, DisconnectionReason reason) const
{
    // There is no means to prevent a client from instantaneously reconnecting, so we just have to trust the client to disconnect
    server->send(id, KickEvent(reason));
}

GameState& Controller::game()
{
    return now.state;
}

void Controller::handleNetworkEvents()
{
    for (const auto& [ id, event ] : systemEvents)
    {
        handleNetworkEvent(id, event);
    }

    systemEvents.clear();

    for (auto& [ id, connection ] : connections)
    {
        handleNetworkEventsForConnection(id, connection);
    }
}

void Controller::handleNetworkEventsForConnection(NetID id, Connection& connection)
{
    auto it = userMapping.find(id);

    if (it == userMapping.end())
    {
        return;
    }

    optional<NetworkEventBatch> batch = connection.eventBatcher.pull();

    if (!batch)
    {
        return;
    }

    handleNetworkEventBatch(it->second, connection, *batch);
}

void Controller::handleNetworkEventBatch(UserID id, Connection& connection, const NetworkEventBatch& batch)
{
    connection.lastRemoteIndex = batch.remoteIndex;

    optional<u64> localIndex = batch.localIndex;

    if (!localIndex)
    {
        return;
    }
    else if (!history.has(*localIndex))
    {
        disconnect(userReverseMapping.at(id), Disconnection_Reason_Maximum_Latency_Exceeded);
        return;
    }
    else if (batch.events.empty())
    {
        const StepFrame& last = userHistories.at(id).get(*localIndex);

        Compare compare(last, now, id, []() -> void {});

        userHistories.at(id).set(stepIndex, now);

        sendDelta(id, connection, compare);
        return;
    }

    u64 targetIndex = stepIndex;
    stepIndex = *localIndex;

    now = history.get(stepIndex);

    now.push(id, batch.events);

    const StepFrame& last = userHistories.at(id).get(stepIndex);

    Compare compare(last, now, id, bind(&Controller::stepSimulation, this), targetIndex - stepIndex);

    userHistories.at(id).set(stepIndex, now);

    sendDelta(id, connection, compare);
}

void Controller::stepSimulation()
{
    // Freeze the loop while no users are connected to save power
    if (userMapping.empty())
    {
        return;
    }

    StepFrame fullNow = now;
    fullNow.push(replayableHybridEffects);
    replayableHybridEffects.clear();

    history.set(stepIndex, now);

    if (now.state.timeMultiplier() < 0)
    {
        stepBackwards();
    }
    // We must step even when the game is paused otherwise inputs will pile up and never be handled, including any that attempt to unpause the game
    else
    {
        stepForwards();
    }
}

void Controller::stepForwards()
{
    now.step();
    stepIndex++;

    if (history.has(stepIndex))
    {
        now.makeReplayOf(history.get(stepIndex));
    }
}

void Controller::stepBackwards()
{
    if (history.has(stepIndex - 1))
    {
        stepIndex--;
        // Preserve time multiplier otherwise a negative time multiplier will immediately rewind to the moment before it existed, cancelling itself
        f64 timeMultiplier = now.state.timeMultiplier();
        now = history.get(stepIndex);
        now.state.setTimeMultiplier(timeMultiplier);
    }
    else
    {
        now.state.setTimeMultiplier(0);
    }
}

void Controller::stepSystemInputs()
{
    if (currentTime() - pingSentTime >= pingInterval)
    {
        pingSentTime = currentTime();

        sendAll(NetworkEvent(PingEvent(true)));
    }

    server->step();

    handleNetworkEvents();
}

void Controller::stepSystemOutputs()
{
    for (const auto& [ netID, userID ] : userMapping)
    {
        User* user = now.state.getUser(userID);

        if (user->shouldKick())
        {
            user->resetKick();
            disconnect(netID, Disconnection_Reason_Kicked);
        }
    }

    if (shouldSave ||
        currentTime() - lastSaveTime >= chrono::milliseconds(static_cast<u64>(g_config.saveInterval * 1000))
    )
    {
        sendAll(NetworkEvent(SaveEvent(false)));

        save();

        sendAll(NetworkEvent(SaveEvent(true)));

        shouldSave = false;
        lastSaveTime = currentTime();
    }
}

bool Controller::handleNetworkEvent(NetID id, const NetworkEvent& event)
{
    switch (event.type())
    {
    default :
        break;
    case Network_Event_Join :
        handleJoinEvent(id, event.payload<JoinEvent>());
        return true;
    case Network_Event_Leave :
        handleLeaveEvent(id, event.payload<LeaveEvent>());
        return true;
    }

    auto it = userMapping.find(id);

    if (it == userMapping.end())
    {
        return false;
    }

    User* user = now.state.getUser(it->second);

    if (!user)
    {
        return false;
    }

    switch (event.type())
    {
    default :
        return false;
    case Network_Event_Voice :
        handleVoiceEvent(user, event.payload<VoiceEvent>());
        return true;
    case Network_Event_Ping :
        handlePingEvent(user, event.payload<PingEvent>());
        return true;
    case Network_Event_Checkpoint :
        handleCheckpointEvent(user, event.payload<CheckpointEvent>());
        return true;
    case Network_Event_Save :
        handleSaveEvent(user, event.payload<SaveEvent>());
        return true;
    }
}

void Controller::handleJoinEvent(NetID id, const JoinEvent& event)
{
    if (event.version > version)
    {
        disconnect(id, Disconnection_Reason_Client_Too_New);
        return;
    }
    else if (event.version < version)
    {
        disconnect(id, Disconnection_Reason_Client_Too_Old);
        return;
    }

    if (event.serverPassword != password && event.serverPassword != adminPassword)
    {
        disconnect(id, Disconnection_Reason_Incorrect_Password);
        return;
    }

    User* user = now.state.getUserByName(event.name);

    if (user)
    {
        if (user->active())
        {
            disconnect(id, Disconnection_Reason_User_Already_Logged_In);
            return;
        }

        if (sha256(event.password + user->salt()) != user->password())
        {
            disconnect(id, Disconnection_Reason_Incorrect_Password);
            return;
        }

        u32 oldMagnitude = now.state.magnitude();

        user->activate(event.vr);

        if (event.serverPassword == adminPassword)
        {
            user->setAdmin(true);
        }

        replayableHybridEffects.push_back([this, event]() -> bool {
            User* user = now.state.getUserByName(event.name);

            user->activate(event.vr);

            if (event.serverPassword == adminPassword)
            {
                user->setAdmin(true);
            }

            return false;
        });

        u32 newMagnitude = now.state.magnitude();

        if (newMagnitude != oldMagnitude)
        {
            now.state.log("magnitude-control-entered", { to_string(newMagnitude) });

            replayableHybridEffects.push_back([this, newMagnitude]() -> bool {
                now.state.log("magnitude-control-entered", { to_string(newMagnitude) });

                return false;
            });
        }
    }
    else
    {
        if (!isValidUsername(event.name))
        {
            disconnect(id, Disconnection_Reason_Invalid_Username);
            return;
        }

        UserID userID = now.state.nextUserID();

        User newUser(
            userID,
            event.name,
            now.state.getWorld("main") ? "main" : now.state.worlds().begin()->first,
            event.password
        );
        newUser.activate(event.vr);

        if (event.serverPassword == adminPassword)
        {
            newUser.setAdmin(true);
        }

        now.state.addUser(newUser);

        replayableHybridEffects.push_back([this, newUser]() -> bool {
            now.state.addUser(newUser);

            return false;
        });

        user = now.state.getUser(userID);
    }

    userMapping.insert_or_assign(id, user->id());
    userReverseMapping.insert_or_assign(user->id(), id);
    userHistories.insert_or_assign(user->id(), StepFrameHistory());
    userHistories.at(user->id()).set(stepIndex, now);

    UserID targetID = user->id();

    for (const NetworkEvent& event : now.initial())
    {
        sendOne(targetID, event);
    }

    for (const NetworkEvent& event : now.initialUser(user))
    {
        sendOne(targetID, event);
    }

    for (const NetworkEvent& event : user->initialSelf())
    {
        sendOne(targetID, event);
    }

    WelcomeEvent welcome;
    welcome.id = targetID;
    welcome.splashMessage = now.state.splashMessage();
    welcome.remoteIndex = stepIndex;

    sendOne(targetID, NetworkEvent(welcome));

    if (joinURL)
    {
        URLEvent url(*joinURL);

        sendOne(targetID, NetworkEvent(url));
    }

    now.state.log("user-joined", { event.name });

    replayableHybridEffects.push_back([this, event]() -> bool {
        now.state.log("user-joined", { event.name });

        return false;
    });
}

void Controller::handleLeaveEvent(NetID id, const LeaveEvent& event)
{
    auto it = userMapping.find(id);

    if (it == userMapping.end())
    {
        // honor the leave request even if the user isn't properly connected, otherwise clients get stuck loading forever when their connection fails at the LMOD protocol level (e.g. because they're already logged in)
        server->disconnect(id);
        return;
    }

    UserID userID = it->second;

    User* user = now.state.getUser(userID);

    if (!user->active())
    {
        return;
    }

    u32 oldMagnitude = now.state.magnitude();

    user->deactivate();

    now.state.log("user-left", { user->name() });

    replayableHybridEffects.push_back([this, userID]() -> bool {
        User* user = now.state.getUser(userID);

        user->deactivate();

        now.state.log("user-left", { user->name() });

        return false;
    });

    u32 newMagnitude = now.state.magnitude();

    if (newMagnitude != oldMagnitude)
    {
        now.state.log("magnitude-control-entered", { to_string(newMagnitude) });

        replayableHybridEffects.push_back([this, newMagnitude]() -> bool {
            now.state.log("magnitude-control-entered", { to_string(newMagnitude) });

            return false;
        });
    }

    server->disconnect(userReverseMapping.at(user->id()));

    userHistories.erase(user->id());
    userReverseMapping.erase(user->id());
    userMapping.erase(id);
    connections.erase(id);

    // Clear selection state of entities
    now.state.clearSessionUserReferences(user);

    replayableHybridEffects.push_back([this, userID]() -> bool {
        User* user = now.state.getUser(userID);

        now.state.clearSessionUserReferences(user);

        return false;
    });
}

void Controller::handleVoiceEvent(User* user, const VoiceEvent& event)
{
    vec3 linV;

    if (user->entityID())
    {
        linV = now.state.get(user->entityID())->linearVelocity();
    }

    VoiceEvent outEvent(
        user->id(),
        user->world(),
        user->viewer().position(),
        user->viewer().rotation(),
        linV,
        event.data
    );

    if (user->voiceChannel().empty())
    {
        sendAll(NetworkEvent(outEvent));
    }
    else
    {
        Team* team = now.state.getTeam(user->voiceChannel());

        for (UserID userID : team->userIDs)
        {
            sendOne(userID, NetworkEvent(outEvent));
        }
    }
}

void Controller::handlePingEvent(User* user, const PingEvent& event)
{
    if (event.ping)
    {
        sendOne(user->id(), NetworkEvent(PingEvent(false)));
    }
    else
    {
        u32 ping = (currentTime() - pingSentTime).count();

        user->setPing(ping);
    }
}

void Controller::handleCheckpointEvent(User* user, const CheckpointEvent& event)
{
    switch (event.type)
    {
    case Checkpoint_Event_Add :
        handleCheckpointAdd(user, event);
        break;
    case Checkpoint_Event_Remove :
        handleCheckpointRemove(user, event);
        break;
    case Checkpoint_Event_Revert :
        handleCheckpointRevert(user, event);
        break;
    }
}

void Controller::handleSaveEvent(User* user, const SaveEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("request-save"));
        return;
    }

    shouldSave = true;
}

void Controller::handleCheckpointAdd(User* user, const CheckpointEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("add-checkpoint"));
        return;
    }

    if (!isValidName(event.name))
    {
        user->pushMessage(invalidNameError(event.name));
        return;
    }

    if (_checkpoints.contains(event.name))
    {
        user->pushMessage(checkpointFoundError(event.name));
        return;
    }

    addCheckpoint(event.name);
}

void Controller::handleCheckpointRemove(User* user, const CheckpointEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("remove-checkpoint"));
        return;
    }

    if (!_checkpoints.contains(event.name))
    {
        user->pushMessage(checkpointNotFoundError(event.name));
        return;
    }

    removeCheckpoint(event.name);
}

void Controller::handleCheckpointRevert(User* user, const CheckpointEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("revert-checkpoint"));
        return;
    }

    if (!_checkpoints.contains(event.name))
    {
        user->pushMessage(checkpointNotFoundError(event.name));
        return;
    }

    revertCheckpoint(event.name);
}

bool Controller::load()
{
    log("Loading save...");

    if (!fs::exists(savePath))
    {
        if (fs::exists(saveBackupPath) && !getFile(saveBackupPath).empty())
        {
            warning("Save not found but non-empty backup was, restoring from backup.");
            fs::copy(saveBackupPath, savePath);
        }
        else
        {
            log("Save not found.");
            return false;
        }
    }
    else if (getFile(savePath).empty())
    {
        if (fs::exists(saveBackupPath) && !getFile(saveBackupPath).empty())
        {
            warning("Empty save found alongside non-empty backup, restoring from backup.");
            fs::remove(savePath);
            fs::copy(saveBackupPath, savePath);
        }
        else
        {
            error("Empty save found.");
            return false;
        }
    }

    try
    {
        loadState(savePath);
    }
    catch (const exception& e)
    {
        error("Failed to load save: " + string(e.what()));
        disableSaving();
        return false;
    }

    log("Loaded.");

    return true;
}

void Controller::save() const
{
    if (!saving)
    {
        return;
    }

    if (fs::exists(savePath))
    {
        fs::rename(savePath, saveBackupPath);
    }

    saveState(savePath);
}

void Controller::loadState(const string& path)
{
    YAMLParser ctx(path);

    _checkpoints = ctx.root()["checkpoints"].as<map<string, Checkpoint>>();

    stepIndex = ctx.root()["stepIndex"].as<u64>();
    now = ctx.root()["now"].as<StepFrame>();

    if (ctx.root().has("history"))
    {
        history = ctx.root()["history"].as<StepFrameHistory>();
    }
}

void Controller::saveState(const string& path) const
{
    YAMLSerializer ctx(path);

    ctx.startMapping();

    ctx.emitPair("checkpoints", _checkpoints);

    ctx.emitPair("stepIndex", stepIndex);
    ctx.emitPair("now", now);

    if (g_config.saveHistory)
    {
        ctx.emitPair("history", history);
    }

    ctx.endMapping();
}

void Controller::disableSaving()
{
    warning("Saving is now disabled to prevent overwriting of the damaged files.");
    saving = false;
}

void Controller::sendDelta(UserID id, const Connection& connection, const Compare& compare) const
{
    for (const NetworkEvent& event : compare.deltaEvents())
    {
        sendOne(id, event);
    }

    for (const NetworkEvent& event : compare.selfDeltaEvents())
    {
        sendOne(id, event);
    }

    StepEvent event;
    event.localIndex = connection.lastRemoteIndex;
    event.remoteIndex = stepIndex;

    sendOne(id, event);
}
