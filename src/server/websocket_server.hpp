/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(LMOD_WEBSOCKET)
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "address.hpp"
#include "network_event.hpp"

#define ASIO_STANDALONE
#include <websocketpp/config/asio.hpp>
#include <websocketpp/server.hpp>

class WebSocketClient
{
public:
    WebSocketClient(
        const shared_ptr<websocketpp::connection<websocketpp::config::asio_tls>>& connection,
        const function<void()>& connectCallback,
        const function<void(const NetworkEvent&)>& receiveCallback,
        const function<void()>& disconnectCallback
    );
    WebSocketClient(const WebSocketClient& rhs) = delete;
    WebSocketClient(WebSocketClient&& rhs) = delete;
    ~WebSocketClient();

    WebSocketClient& operator=(const WebSocketClient& rhs) = delete;
    WebSocketClient& operator=(WebSocketClient&& rhs) = delete;

    void step();
    void send(const NetworkEvent& event);
    void markExternallyDisconnected();

private:
    shared_ptr<websocketpp::connection<websocketpp::config::asio_tls>> connection;
    function<void()> connectCallback;
    function<void(const NetworkEvent&)> receiveCallback;
    function<void()> disconnectCallback;

    u64 nextMessageSize;
    string buffer;
    bool externallyDisconnected;

    mutex tasksLock;
    vector<function<void()>> tasks;

    void onMessage(websocketpp::connection_hdl handle, websocketpp::config::asio::message_type::ptr message);

    void handleReceive(const string& data);
};

class WebSocketServer
{
public:
    WebSocketServer(
        const string& certFile,
        const string& keyFile,
        const function<void(NetID)>& connectCallback,
        const function<void(NetID, const NetworkEvent&)>& receiveCallback,
        const function<void(NetID)>& disconnectCallback
    );
    WebSocketServer(const WebSocketServer& rhs) = delete;
    WebSocketServer(WebSocketServer&& rhs) = delete;
    ~WebSocketServer();

    WebSocketServer& operator=(const WebSocketServer& rhs) = delete;
    WebSocketServer& operator=(WebSocketServer&& rhs) = delete;

    void step();
    void listen(const Address& address);
    void send(NetID id, const NetworkEvent& event);
    void disconnect(NetID id);

private:
    function<void(NetID)> connectCallback;
    function<void(NetID, const NetworkEvent&)> receiveCallback;
    function<void(NetID)> disconnectCallback;

    string certFile;
    string keyFile;

    optional<thread> runThread;
    websocketpp::server<websocketpp::config::asio_tls>* server;

    NetID nextID;
    map<void*, NetID> ids;
    map<NetID, void*> handles;
    map<void*, WebSocketClient*> clients;

    mutex tasksLock;
    vector<function<void()>> tasks;

    void run(const Address& bindAddress);

    websocketpp::lib::shared_ptr<websocketpp::lib::asio::ssl::context> onTLSInit(websocketpp::connection_hdl handle);
    void onOpen(websocketpp::connection_hdl handle);
    void onClose(websocketpp::connection_hdl handle);

    void createClient(websocketpp::connection_hdl handle);
    void destroyClient(void* handle);
};

#endif
