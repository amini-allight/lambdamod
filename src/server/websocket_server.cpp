/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(LMOD_WEBSOCKET)
#include "websocket_server.hpp"
#include "log.hpp"
#include "message_boundaries.hpp"

WebSocketClient::WebSocketClient(
    const shared_ptr<websocketpp::connection<websocketpp::config::asio_tls>>& connection,
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : connection(connection)
    , connectCallback(connectCallback)
    , receiveCallback(receiveCallback)
    , disconnectCallback(disconnectCallback)
    , nextMessageSize(0)
    , externallyDisconnected(false)
{
    log("WebSocket connection opened.");

    connectCallback();

    connection->set_message_handler(bind(
        &WebSocketClient::onMessage,
        this,
        placeholders::_1,
        placeholders::_2
    ));
}

WebSocketClient::~WebSocketClient()
{
    log("WebSocket connection closed.");

    if (!externallyDisconnected)
    {
        disconnectCallback();
    }
    else
    {
        connection->close(websocketpp::close::status::normal, "");
    }
}

void WebSocketClient::step()
{
    vector<function<void()>> tasks;

    {
        lock_guard<mutex> lock(tasksLock);

        tasks = this->tasks;
        this->tasks.clear();
    }

    for (const function<void()>& task : tasks)
    {
        task();
    }
}

void WebSocketClient::send(const NetworkEvent& event)
{
    string data = event.data();

    connection->send(data.data(), data.size());
}

void WebSocketClient::markExternallyDisconnected()
{
    externallyDisconnected = true;
}

void WebSocketClient::onMessage(
    websocketpp::connection_hdl handle,
    websocketpp::config::asio::message_type::ptr message
)
{
    lock_guard<mutex> lock(tasksLock);

    tasks.push_back(bind(&WebSocketClient::handleReceive, this, message->get_payload()));
}

void WebSocketClient::handleReceive(const string& data)
{
    for (const string& message : messageUnboundary(data, nextMessageSize, buffer))
    {
        NetworkEvent event(message);

        receiveCallback(event);
    }
}

WebSocketServer::WebSocketServer(
    const string& certFile,
    const string& keyFile,
    const function<void(NetID)>& connectCallback,
    const function<void(NetID, const NetworkEvent&)>& receiveCallback,
    const function<void(NetID)>& disconnectCallback
)
    : connectCallback(connectCallback)
    , receiveCallback(receiveCallback)
    , disconnectCallback(disconnectCallback)
    , certFile(certFile)
    , keyFile(keyFile)
    , server(nullptr)
    , nextID(0x80000000) // start halfway through the u32 range to avoid collision with non-websocket connections
{

}

WebSocketServer::~WebSocketServer()
{
    if (runThread)
    {
        server->stop();
        runThread->join();
    }

    for (const auto& [ handle, client ] : clients)
    {
        delete client;
    }
}

void WebSocketServer::step()
{
    vector<function<void()>> tasks;

    {
        lock_guard<mutex> lock(tasksLock);

        tasks = this->tasks;
        this->tasks.clear();
    }

    for (const function<void()>& task : tasks)
    {
        task();
    }

    for (const auto& [ handle, client ] : clients)
    {
        client->step();
    }
}

void WebSocketServer::listen(const Address& address)
{
    runThread = thread(bind(&WebSocketServer::run, this, address));
}

void WebSocketServer::send(NetID id, const NetworkEvent& event)
{
    auto it = handles.find(id);

    if (it == handles.end())
    {
        return;
    }

    void* handle = it->second;

    clients.at(handle)->send(event);
}

void WebSocketServer::disconnect(NetID id)
{
    auto it = handles.find(id);

    if (it == handles.end())
    {
        return;
    }

    void* handle = it->second;

    clients.at(handle)->markExternallyDisconnected();

    destroyClient(handle);
}

void WebSocketServer::run(const Address& bindAddress)
{
    server = new websocketpp::server<websocketpp::config::asio_tls>;
    server->set_error_channels(websocketpp::log::elevel::none);
    server->set_access_channels(websocketpp::log::alevel::none);
    server->init_asio();
    server->listen(bindAddress.host, to_string(bindAddress.port));
    server->set_tls_init_handler(bind(&WebSocketServer::onTLSInit, this, placeholders::_1));
    server->set_open_handler(bind(&WebSocketServer::onOpen, this, placeholders::_1));
    server->set_close_handler(bind(&WebSocketServer::onClose, this, placeholders::_1));
    server->start_accept();
    server->run();
    delete server;
}

websocketpp::lib::shared_ptr<websocketpp::lib::asio::ssl::context> WebSocketServer::onTLSInit(
    websocketpp::connection_hdl handle
)
{
    namespace asio = websocketpp::lib::asio;

    shared_ptr<asio::ssl::context> ctx = make_shared<asio::ssl::context>(asio::ssl::context::tlsv13);

    ctx->use_certificate_chain(asio::buffer(certFile));
    ctx->use_private_key(asio::buffer(keyFile), asio::ssl::context::pem);

    return ctx;
}

void WebSocketServer::onOpen(websocketpp::connection_hdl handle)
{
    lock_guard<mutex> lock(tasksLock);

    tasks.push_back(bind(&WebSocketServer::createClient, this, handle));
}

void WebSocketServer::onClose(websocketpp::connection_hdl handle)
{
    lock_guard<mutex> lock(tasksLock);

    tasks.push_back(bind(&WebSocketServer::destroyClient, this, server->get_con_from_hdl(handle).get()));
}

void WebSocketServer::createClient(websocketpp::connection_hdl handle)
{
    auto connection = server->get_con_from_hdl(handle);

    if (!connection)
    {
        return;
    }

    NetID id = nextID++;

    ids.insert_or_assign(connection.get(), id);
    handles.insert_or_assign(id, connection.get());

    clients.insert_or_assign(connection.get(), new WebSocketClient(
        connection,
        bind(connectCallback, id),
        bind(receiveCallback, id, placeholders::_1),
        bind(disconnectCallback, id)
    ));
}

void WebSocketServer::destroyClient(void* handle)
{
    NetID id = ids.at(handle);

    ids.erase(handle);
    handles.erase(id);

    delete clients.at(handle);
    clients.erase(handle);
}
#endif
