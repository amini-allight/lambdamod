/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "server.hpp"
#include "log.hpp"
#include "global.hpp"
#include "ssl_tools.hpp"
#include "message_boundaries.hpp"

static constexpr size_t bufferSize = 1024;

Client::Client(
    const Address& address,
    SSL* ssl,
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : address(address)
    , ssl(ssl)
    , connectCallback(connectCallback)
    , receiveCallback(receiveCallback)
    , disconnectCallback(disconnectCallback)
    , nextMessageSize(0)
    , disconnect(false)
    , externallyDisconnected(false)
{
    log("Connection to " + address.toString() + " opened.");

    connectCallback();
}

Client::~Client()
{
    log("Connection to " + address.toString() + " closed.");

    SSL_shutdown(ssl);
    SSL_free(ssl);

    if (!externallyDisconnected)
    {
        disconnectCallback();
    }
}

bool Client::step()
{
    u8 buffer[bufferSize];

    while (true)
    {
        int result = SSL_read(ssl, buffer, bufferSize);

        if (result <= 0)
        {
            if (BIO_should_retry(SSL_get_rbio(ssl)))
            {
                return disconnect;
            }
            else
            {
                warning("Failed to read from socket.");
                disconnect = true;
                return disconnect;
            }
        }

        onReceive(string(reinterpret_cast<const char*>(buffer), result));
    }
}

void Client::send(const NetworkEvent& event)
{
    string data = messageBoundary(event.data());

    int result = SSL_write(ssl, data.data(), data.size());

    if (result < (int)data.size())
    {
        warning("Failed to write to socket.");
        disconnect = true;
    }
}

void Client::markExternallyDisconnected()
{
    externallyDisconnected = true;
}

void Client::onReceive(const string& data)
{
    for (const string& message : messageUnboundary(data, nextMessageSize, buffer))
    {
        NetworkEvent event(message);

        receiveCallback(event);
    }
}

Server::Server(
    const string& certFile,
    const string& keyFile,
    const function<void(NetID)>& connectCallback,
    const function<void(NetID, const NetworkEvent&)>& receiveCallback,
    const function<void(NetID)>& disconnectCallback
)
    : connectCallback(connectCallback)
    , receiveCallback(receiveCallback)
    , disconnectCallback(disconnectCallback)
    , ctx(nullptr)
    , nextID(1)
    , holePunchRunning(false)
#if defined(LMOD_WEBSOCKET)
    , webSocketServer(certFile, keyFile, connectCallback, receiveCallback, disconnectCallback)
#endif
{
    log("Initializing networking...");

    int result;

    initOpenSSL();

    const SSL_METHOD* method = TLS_server_method();

    if (!method)
    {
        fatal("Failed to get OpenSSL method: " + getOpenSSLError());
    }

    ctx = SSL_CTX_new(method);

    if (!ctx)
    {
        fatal("Failed to create OpenSSL context: " + getOpenSSLError());
    }

    SSL_CTX_set_min_proto_version(ctx, TLS1_3_VERSION);
    SSL_CTX_set_max_proto_version(ctx, 0);

    BIO* certReader = BIO_new_mem_buf(certFile.data(), -1);

    if (!certReader)
    {
        fatal("Failed to create OpenSSL certificate reader: " + getOpenSSLError());
    }

    X509* cert = PEM_read_bio_X509(certReader, nullptr, nullptr, 0);
    BIO_free_all(certReader);

    if (!cert)
    {
        fatal("Failed to create OpenSSL certificate: " + getOpenSSLError());
    }

    result = SSL_CTX_use_certificate(ctx, cert);
    X509_free(cert);

    if (!result)
    {
        fatal("Failed to load OpenSSL certificate: " + getOpenSSLError());
    }

    BIO* keyReader = BIO_new_mem_buf(keyFile.data(), -1);

    if (!keyReader)
    {
        fatal("Failed to create OpenSSL private key reader: " + getOpenSSLError());
    }

    EVP_PKEY* key = PEM_read_bio_PrivateKey(keyReader, nullptr, nullptr, 0);
    BIO_free_all(keyReader);

    if (!key)
    {
        fatal("Failed to create OpenSSL private key: " + getOpenSSLError());
    }

    result = SSL_CTX_use_PrivateKey(ctx, key);
    EVP_PKEY_free(key);

    if (!result)
    {
        fatal("Failed to load OpenSSL private key: " + getOpenSSLError());
    }
}

Server::~Server()
{
    if (holePunchThread)
    {
        holePunchRunning = false;
        holePunchThread->join();
    }

    if (listenThread)
    {
        listenThread->join();
    }

    // important to clear this because some SSL objects can exist only in the closures of the task queue
    for (const function<void()>& task : tasks)
    {
        task();
    }

    for (const auto& [ addr, client ] : clients)
    {
        delete client;
    }

    if (ctx)
    {
        SSL_CTX_free(ctx);
    }

    log("Networking shut down.");
}

void Server::step()
{
    vector<function<void()>> tasks;

    {
        lock_guard<mutex> lock(tasksLock);

        tasks = this->tasks;
        this->tasks.clear();
    }

    for (const function<void()>& task : tasks)
    {
        task();
    }

    for (auto it = clients.begin(); it != clients.end();)
    {
        Address address = it->first;
        bool disconnect = it->second->step();

        if (disconnect)
        {
            addresses.erase(ids.at(address));
            ids.erase(address);

            delete it->second;
            clients.erase(it++);
        }
        else
        {
            it++;
        }
    }

#if defined(LMOD_WEBSOCKET)
    webSocketServer.step();
#endif
}

void Server::listen(const Address& address)
{
    listenThread = thread(bind(&Server::acceptConnections, this, address));

#if defined(LMOD_WEBSOCKET)
    webSocketServer.listen({ g_config.webSocketHost, g_config.webSocketPort });
#endif
}

void Server::holePunch(const string& sessionID)
{
    holePunchRunning = true;
    holePunchThread = thread(bind(&Server::holePunchConnections, this, sessionID));
}

void Server::send(NetID id, const NetworkEvent& event)
{
    auto it = addresses.find(id);

    if (it == addresses.end())
    {
#if defined(LMOD_WEBSOCKET)
        webSocketServer.send(id, event);
#endif
        return;
    }

    Address address = it->second;

    Client* client = clients.at(address);

    client->send(event);
}

void Server::disconnect(NetID id)
{
    auto it = addresses.find(id);

    if (it == addresses.end())
    {
#if defined(LMOD_WEBSOCKET)
        webSocketServer.disconnect(id);
#endif
        return;
    }

    Address address = it->second;

    ids.erase(address);
    addresses.erase(id);

    auto it2 = clients.find(address);

    it2->second->markExternallyDisconnected();

    delete it2->second;
    clients.erase(it2);
}

void Server::createClient(const Address& address, SSL* ssl)
{
    NetID id = nextID++;

    clients.insert({
        address,
        new Client(
            address,
            ssl,
            bind(connectCallback, id),
            bind(receiveCallback, id, placeholders::_1),
            bind(disconnectCallback, id)
        )
    });

    ids.insert({ address, id });
    addresses.insert({ id, address });
}
