/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "controller.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "cryptography.hpp"
#include "url.hpp"
#include "upnp.hpp"
#include "mesh_cache.hpp"

#include "platform/server_posix.hpp"
#include "platform/server_windows.hpp"

#include <csignal>

static Server* server = nullptr;
static bool quit = false;
static Controller* controller = nullptr;

static void onConnect(NetID id)
{
    // Do nothing
}

static void onReceive(NetID id, const NetworkEvent& event)
{
    controller->onReceive(id, event);
}

static void onDisconnect(NetID id)
{
    controller->onLeave(id);
}

static void onInterrupt(int sig)
{
    warning("Interrupt received, shutting down.");
    quit = true;
}

static void displayArgumentError(int argc, char** argv)
{
    cerr << "Usage: " << argv[0] << " [host port password adminPassword savePath]" << endl;
}

static tuple<optional<string>, optional<string>> setupConnectionMode(u16 port, const string& password)
{
    switch (g_config.connectionMode)
    {
    case Server_Connection_Direct :
        break;
    case Server_Connection_Invite :
    {
        optional<string> joinURL = generateJoinURL(port, password);

        if (joinURL)
        {
            log("Join URL available: " + *joinURL);
        }

        return { optional<string>(), joinURL };
    }
    case Server_Connection_Session :
    {
        optional<string> sessionID = createSession(password);

        if (sessionID)
        {
            string joinURL = string(routingServerPrefix) + "/invite?session-id=" + *sessionID;

            log("Join URL available: " + joinURL);

            return { sessionID, joinURL };
        }
        else
        {
            return { optional<string>(), optional<string>() };
        }
    }
    }

    return {};
}

static string getKey()
{
    string key;

    if (!g_config.keyFilePath.empty())
    {
        key = getFile(g_config.keyFilePath);
    }
    else
    {
        auto [ publicKey, privateKey ] = randomRSAKeyPair();

        key = privateKey;
    }

    return key;
}

static string getCertificate(const string& key)
{
    string cert;

    if (!g_config.certFilePath.empty())
    {
        cert = getFile(g_config.certFilePath);
    }
    else
    {
        cert = certificateFromKey(key);
    }

    return cert;
}

static int setup()
{
    try
    {
        g_config = loadConfig();
    }
    catch (const exception& e)
    {
        error("Failed to load config: " + string(e.what()));
    }

    string key = getKey();
    string cert = getCertificate(key);

#if defined(__unix__) || defined(__APPLE__)
    server = new ServerPOSIX(
        cert,
        key,
        onConnect,
        onReceive,
        onDisconnect
    );
#elif defined(WIN32)
    server = new ServerWindows(
        cert,
        key,
        onConnect,
        onReceive,
        onDisconnect
    );
#else
#error Unknown operating system encountered.
#endif

    return 0;
}

static int setupDefault()
{
    int result = setup();

    if (result)
    {
        return result;
    }

    auto [ sessionID, joinURL ] = setupConnectionMode(g_config.port, g_config.password);

    if (sessionID)
    {
        server->holePunch(*sessionID);
    }
    else
    {
#ifdef LMOD_UPNP
        if (g_config.upnp)
        {
            optional<Address> externalAddress = upnpForward(g_config.port, g_config.upnpIPv6);

            if (externalAddress)
            {
                log("UPnP gateway reported its external address as " + externalAddress->toString() + ".");
            }
        }
#endif

        server->listen({ g_config.host, g_config.port });
    }

    controller = new Controller(server, g_config.password, g_config.adminPassword, g_config.savePath, joinURL);

    return 0;
}

static int setupCustom(int argc, char** argv)
{
    string host;
    u16 port;
    string password;
    string adminPassword;
    string savePath;

    try
    {
        host = argv[1];
        port = stoi(argv[2]);
        password = argv[3];
        adminPassword = argv[4];
        savePath = argv[5];
    }
    catch (const exception& e)
    {
        displayArgumentError(argc, argv);
        return 1;
    }

    int result = setup();

    if (result)
    {
        return result;
    }

    auto [ sessionID, joinURL ] = setupConnectionMode(port, password);

    if (sessionID)
    {
        server->holePunch(*sessionID);
    }
    else
    {
#ifdef LMOD_UPNP
        if (g_config.upnp)
        {
            optional<Address> externalAddress = upnpForward(port, g_config.upnpIPv6);

            if (externalAddress)
            {
                log("UPnP gateway reported its external address as " + externalAddress->toString() + ".");
            }
        }
#endif

        server->listen({ host, port });
    }

    controller = new Controller(server, password, adminPassword, savePath, joinURL);

    return 0;
}

#if defined(WIN32)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    int argc = __argc;
    char** argv = __argv;
#else
int main(int argc, char** argv)
{
#endif
    cout << gameName << " Server " << gameVersion << endl;
    cout << "Copyright 2025 Amini Allight" << endl << endl;
    cout << "This program comes with ABSOLUTELY NO WARRANTY; This is free software, and you are welcome to redistribute it under certain conditions. See the included license for further details." << endl << endl;

    meshCache = new MeshCache();

    if (argc == 1)
    {
        int result = setupDefault();

        if (result)
        {
            return result;
        }
    }
    else if (argc == 6)
    {
        int result = setupCustom(argc, argv);

        if (result)
        {
            return result;
        }
    }
    else
    {
        displayArgumentError(argc, argv);
        return 1;
    }

    signal(SIGINT, onInterrupt);
#if defined(__unix__) || defined(__APPLE__)
    signal(SIGPIPE, SIG_IGN);
#endif

    log("Server online.");

    chrono::microseconds lastStepTime = currentTime<chrono::microseconds>();
    chrono::microseconds minStepInterval(static_cast<u64>(1'000'000.0 / g_config.stepRate));

    while (!quit)
    {
        if (chrono::microseconds nextStepTime = lastStepTime + minStepInterval; nextStepTime > currentTime())
        {
            this_thread::sleep_for(nextStepTime - currentTime());
        }

        lastStepTime += minStepInterval;

        controller->run();
    }

    // Deliberately backwards because server shutdown can access controller in disconnect callbacks
    delete server;

    delete controller;

    delete meshCache;

    return 0;
}
