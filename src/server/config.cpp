/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "config.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "yaml_tools.hpp"

static ServerConnectionMode connectionModeFromString(const string& s)
{
    if (s == "direct")
    {
        return Server_Connection_Direct;
    }
    else if (s == "invite")
    {
        return Server_Connection_Invite;
    }
    else if (s == "session")
    {
        return Server_Connection_Session;
    }
    else
    {
        warning("Encountered unknown server connection mode '" + s + "', assuming direct.");
        return Server_Connection_Direct;
    }
}

static string connectionModeToString(ServerConnectionMode mode)
{
    switch (mode)
    {
    case Server_Connection_Direct : return "direct";
    case Server_Connection_Invite : return "invite";
    case Server_Connection_Session : return "session";
    default :
        warning("Encountered unknown server connection mode '" + to_string(mode) + "', assuming direct.");
        return "direct";
    }
}

Config::Config()
{
    host = "0.0.0.0";
    port = 7443;
    password = "password";
    adminPassword = "secretpassword";
    savePath = "./save";
    stepRate = 100;
    historyWindow = 10;
    saveHistory = false;
    connectionMode = Server_Connection_Direct;
    saveInterval = 10;
    maxUndoHistory = 128;
    maxRecursionDepth = 4096;
    maxMemoryUsage = 128 * 1024 * 1024;
    maxLoopDuration = 0.02;
    upnp = true;
    upnpIPv6 = false;
    webSocketHost = "0.0.0.0";
    webSocketPort = 7444;
}

template<>
Config YAMLNode::convert() const
{
    Config config;

    if (has("host"))
    {
        config.host = at("host").as<string>();
    }

    if (has("port"))
    {
        config.port = at("port").as<u16>();

        if (config.port == 0)
        {
            warning("Invalid 'port' config value: " + to_string(config.port));
            config.port = Config().port;
        }
    }

    if (has("password"))
    {
        config.password = at("password").as<string>();
    }

    if (has("adminPassword"))
    {
        config.adminPassword = at("adminPassword").as<string>();
    }

    if (has("savePath"))
    {
        config.savePath = at("savePath").as<string>();

        if (config.savePath.empty())
        {
            warning("Invalid 'savePath' config value: " + config.savePath);
            config.savePath = Config().savePath;
        }
    }

    if (has("stepRate"))
    {
        config.stepRate = at("stepRate").as<u64>();

        if (config.stepRate == 0)
        {
            warning("Invalid 'stepRate' config value: " + to_string(config.stepRate));
            config.stepRate = Config().stepRate;
        }
    }

    if (has("historyWindow"))
    {
        config.historyWindow = at("historyWindow").as<f64>();

        if (config.historyWindow <= 0)
        {
            warning("Invalid 'historyWindow' config value: " + to_string(config.historyWindow));
            config.historyWindow = Config().historyWindow;
        }
    }

    if (has("saveHistory"))
    {
        config.saveHistory = at("saveHistory").as<bool>();
    }

    if (has("certFilePath"))
    {
        config.certFilePath = at("certFilePath").as<string>();
    }

    if (has("keyFilePath"))
    {
        config.keyFilePath = at("keyFilePath").as<string>();
    }

    if (has("connectionMode"))
    {
        config.connectionMode = connectionModeFromString(at("connectionMode").as<string>());
    }

    if (has("saveInterval"))
    {
        config.saveInterval = at("saveInterval").as<f64>();

        if (config.saveInterval < 0)
        {
            warning("Invalid 'saveInterval' config value: " + to_string(config.saveInterval));
            config.saveInterval = Config().saveInterval;
        }
    }

    if (has("maxUndoHistory"))
    {
        config.maxUndoHistory = at("maxUndoHistory").as<u64>();
    }

    if (has("maxRecursionDepth"))
    {
        config.maxRecursionDepth = at("maxRecursionDepth").as<u64>();

        if (config.maxRecursionDepth == 0)
        {
            warning("Invalid 'maxRecursionDepth' config value: " + to_string(config.maxRecursionDepth));
            config.maxRecursionDepth = Config().maxRecursionDepth;
        }
    }

    if (has("maxMemoryUsage"))
    {
        config.maxMemoryUsage = at("maxMemoryUsage").as<u64>();

        if (config.maxMemoryUsage == 0)
        {
            warning("Invalid 'maxMemoryUsage' config value: " + to_string(config.maxMemoryUsage));
            config.maxMemoryUsage = Config().maxMemoryUsage;
        }
    }

    if (has("maxLoopDuration"))
    {
        config.maxLoopDuration = at("maxLoopDuration").as<f64>();

        if (config.maxLoopDuration == 0)
        {
            warning("Invalid 'maxLoopDuration' config value: " + to_string(config.maxLoopDuration));
            config.maxLoopDuration = Config().maxLoopDuration;
        }
    }

    if (has("upnp"))
    {
        config.upnp = at("upnp").as<bool>();
    }

    if (has("upnpIPv6"))
    {
        config.upnpIPv6 = at("upnpIPv6").as<bool>();
    }

    if (has("webSocketHost"))
    {
        config.webSocketHost = at("webSocketHost").as<string>();
    }

    if (has("webSocketPort"))
    {
        config.webSocketPort = at("webSocketPort").as<u16>();

        if (config.webSocketPort == 0)
        {
            warning("Invalid 'webSocketPort' config value: " + to_string(config.webSocketPort));
            config.webSocketPort = Config().webSocketPort;
        }
    }

    return config;
}

template<>
void YAMLSerializer::emit(Config v)
{
    startMapping();

    emitPair("host", v.host);
    emitPair("port", v.port);
    emitPair("password", v.password);
    emitPair("adminPassword", v.adminPassword);
    emitPair("savePath", v.savePath);
    emitPair("stepRate", v.stepRate);
    emitPair("historyWindow", v.historyWindow);
    emitPair("saveHistory", v.saveHistory);
    emitPair("certFilePath", v.certFilePath);
    emitPair("keyFilePath", v.keyFilePath);
    emit("connectionMode");
    emit(connectionModeToString(v.connectionMode));
    emitPair("saveInterval", v.saveInterval);
    emitPair("maxUndoHistory", v.maxUndoHistory);
    emitPair("maxRecursionDepth", v.maxRecursionDepth);
    emitPair("maxMemoryUsage", v.maxMemoryUsage);
    emitPair("maxLoopDuration", v.maxLoopDuration);
    emitPair("upnp", v.upnp);
    emitPair("upnpIPv6", v.upnpIPv6);
    emitPair("webSocketHost", v.webSocketHost);
    emitPair("webSocketPort", v.webSocketPort);

    endMapping();
}

static void ensureConfig(const string& path)
{
    if (fs::exists(path))
    {
        return;
    }

    if (!fs::exists(fs::path(path).parent_path()))
    {
        bool ok = fs::create_directories(fs::path(path).parent_path());

        if (!ok)
        {
            error("Failed to create config directories.");
            return;
        }
    }

    YAMLSerializer ctx(path);

    ctx.emit(Config());
}

Config loadConfig()
{
    string path = configPath() + serverConfigFileName;

    ensureConfig(path);

    YAMLParser ctx(path);

    return ctx.root().as<Config>();
}
