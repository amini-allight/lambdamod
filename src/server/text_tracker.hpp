/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "events.hpp"
#include "text.hpp"
#include "script_types.hpp"

class TextTracker
{
public:
    TextTracker(TextID id, UserID userID, const optional<Lambda>& lambda = {}, chrono::milliseconds startTime = chrono::milliseconds(0));
    template<typename T>
    TextTracker(
        TextID id,
        const T& data,
        UserID userID,
        const optional<Lambda>& lambda = {},
        chrono::milliseconds startTime = chrono::milliseconds(0)
    );

    bool operator==(const TextTracker& rhs) const;
    bool operator!=(const TextTracker& rhs) const;

    TextID id() const;
    TextType type() const;
    template<typename T>
    const T& data() const
    {
        return get<T>(_data);
    }

    UserID userID() const;
    const optional<Lambda>& lambda() const;
    chrono::milliseconds startTime() const;

    void start();
    bool matches(const TextResponse& response) const;
    bool timedOut() const;

    TextRequestEvent event() const;

private:
    TextID _id;
    TextType _type;
    variant<
        TextTitlecardRequest,
        TextTimeoutRequest,
        TextUnaryRequest,
        TextBinaryRequest,
        TextOptionsRequest,
        TextChooseRequest,
        TextAssignValuesRequest,
        TextSpendPointsRequest,
        TextAssignPointsRequest,
        TextVoteRequest,
        TextChooseMajorRequest
    > _data;
    UserID _userID;
    optional<Lambda> _lambda;
    chrono::milliseconds _startTime;
};

template<>
TextTracker::TextTracker(
    TextID id,
    const TextTitlecardRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextTimeoutRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextUnaryRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextBinaryRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextOptionsRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextChooseRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextAssignValuesRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextSpendPointsRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextAssignPointsRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextVoteRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);

template<>
TextTracker::TextTracker(
    TextID id,
    const TextChooseMajorRequest& data,
    UserID userID,
    const optional<Lambda>& lambda,
    chrono::milliseconds startTime
);
