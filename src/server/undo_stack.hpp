/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "undo_frame.hpp"

class World;
class Entity;

class UndoStack
{
public:
    explicit UndoStack(UserID userID);

    void undo(World* world);
    void redo(World* world);

    void add(const Entity* after);
    void remove(const Entity* before);
    void modify(const Entity* before, const Entity* after, bool mergeable);

private:
    UserID userID;

    deque<UndoFrame> undoFrames;
    deque<UndoFrame> redoFrames;
};
