/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "step_frame.hpp"

StepFrame::StepFrame()
{

}

void StepFrame::makeReplayOf(const StepFrame& previous)
{
    events = previous.events;
}

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> StepFrame::compare(const StepFrame& previous) const
{
    return state.compare(previous.state);
}

vector<NetworkEvent> StepFrame::compareUser(const StepFrame& previous, const User* user) const
{
    return state.compareUser(previous.state, user);
}

vector<NetworkEvent> StepFrame::initial() const
{
    return state.initial();
}

vector<NetworkEvent> StepFrame::initialUser(const User* user) const
{
    return state.initialUser(user);
}

void StepFrame::step()
{
    state.step();

    applyHybridEffects();

    for (const auto& [ id, event ] : events)
    {
        state.push(id, event);
    }
    events.clear();

    state.updateDynamics();
}

void StepFrame::applyHybridEffects()
{
    for (const function<bool()>& effect : hybridEffects)
    {
        bool shouldBreak = effect();

        if (shouldBreak)
        {
            break;
        }
    }
    hybridEffects.clear();
}

void StepFrame::push(const vector<function<bool()>>& hybridEffects)
{
    this->hybridEffects.insert(
        this->hybridEffects.end(),
        hybridEffects.begin(),
        hybridEffects.end()
    );
}

void StepFrame::push(UserID id, const NetworkEvent& event)
{
    events.push_back({ id, event });
}

void StepFrame::push(UserID id, const vector<NetworkEvent>& events)
{
    for (const NetworkEvent& event : events)
    {
        push(id, event);
    }
}

// Don't save events or inputs, no real point

template<>
StepFrame YAMLNode::convert() const
{
    StepFrame frame;

    frame.state = at("state").as<GameState>();

    return frame;
}

template<>
void YAMLSerializer::emit(StepFrame v)
{
    startMapping();

    emitPair("state", v.state);

    endMapping();
}
