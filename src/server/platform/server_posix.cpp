/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__unix__) || defined(__APPLE__)
#include "server_posix.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "ssl_tools.hpp"
#include "resolved_address.hpp"
#include "network_tools.hpp"
#include "url.hpp"

#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/socket.h>

ServerPOSIX::ServerPOSIX(
    const string& certFile,
    const string& keyFile,
    const function<void(NetID)>& connectCallback,
    const function<void(NetID, const NetworkEvent&)>& receiveCallback,
    const function<void(NetID)>& disconnectCallback
)
    : Server(
        certFile,
        keyFile,
        connectCallback,
        receiveCallback,
        disconnectCallback
    )
    , server(-1)
{
    log("Networking initialized.");
}

ServerPOSIX::~ServerPOSIX()
{
    log("Shutting down networking...");

    shutdown(server, SHUT_RD);
    close(server);
}

void ServerPOSIX::acceptConnections(const Address& address)
{
    int result;

    ResolvedAddress bindAddress(address);

    if (!bindAddress.info)
    {
        return;
    }

    server = socket(bindAddress.info->ai_family, SOCK_STREAM, 0);

    if (server < 0)
    {
        fatal("Failed to create socket: " + to_string(errno));
    }

    result = ::bind(server, bindAddress.info->ai_addr, bindAddress.info->ai_addrlen);

    if (result != 0)
    {
        fatal("Failed to bind socket: " + to_string(errno));
    }

    result = ::listen(server, 1);

    if (result != 0)
    {
        fatal("Failed to listen on socket: " + to_string(errno));
    }

    while (true)
    {
        struct sockaddr_in remoteAddress;
        unsigned int remoteAddressSize = sizeof(remoteAddress);

        int client = accept(server, reinterpret_cast<struct sockaddr*>(&remoteAddress), &remoteAddressSize);

        if (client < 0)
        {
            return;
        }

        char host[INET6_ADDRSTRLEN]{0};
        inet_ntop(remoteAddress.sin_family, &remoteAddress.sin_addr, host, INET6_ADDRSTRLEN);

        u16 port = ntohs(remoteAddress.sin_port);

        SSL* ssl = SSL_new(ctx);

        if (!ssl)
        {
            error("Failed to create OpenSSL connection: " + getOpenSSLError());
            close(client);
            continue;
        }

        result = SSL_set_fd(ssl, client);

        if (result <= 0)
        {
            error("Failed to assign file descriptor for OpenSSL: " + getOpenSSLError());
            SSL_free(ssl);
            close(client);
            continue;
        }

        result = SSL_accept(ssl);

        if (result <= 0)
        {
            error("Failed to accept OpenSSL connection: " + getOpenSSLError());
            SSL_free(ssl);
            continue;
        }

        result = setFileControl(client, O_NONBLOCK, true);

        if (result != 0)
        {
            SSL_free(ssl);
            continue;
        }

        result = setSocketOption(client, IPPROTO_TCP, TCP_NODELAY, 1);

        if (result != 0)
        {
            SSL_free(ssl);
            continue;
        }

        lock_guard<mutex> lock(tasksLock);

        tasks.push_back(bind(&ServerPOSIX::createClient, this, Address(host, port), ssl));
    }
}

void ServerPOSIX::holePunchConnections(const string& sessionID)
{
    int result;

    while (true)
    {
        string localHost = holePunchHost;
        u16 localPort = randomPort();

        ResolvedAddress bindAddress({ localHost, localPort });

        if (!bindAddress.info)
        {
            return;
        }

        int client = socket(bindAddress.info->ai_family, SOCK_STREAM, 0);

        if (client < 0)
        {
            error("Failed to create socket: " + to_string(errno));
            return;
        }

        result = setSocketOption(client, SOL_SOCKET, SO_REUSEADDR, 1);

        if (result != 0)
        {
            close(client);
            return;
        }

        result = ::bind(client, bindAddress.info->ai_addr, bindAddress.info->ai_addrlen);

        if (result != 0)
        {
            warning("Failed to bind hole-punching socket: " + to_string(errno));
            close(client);
            continue;
        }

        string remoteHost;
        u16 remotePort = 0;

        // Negotiation
        while (true)
        {
            optional<tuple<string, u16>> client = offerSession(sessionID, localPort);

            if (!client)
            {
                if (!holePunchRunning)
                {
                    break;
                }

                this_thread::sleep_for(chrono::seconds(1));
                continue;
            }

            remoteHost = get<0>(*client);
            remotePort = get<1>(*client);
            break;
        }

        if (!holePunchRunning)
        {
            close(client);
            return;
        }

        ResolvedAddress connectAddress({ remoteHost, remotePort });

        if (!connectAddress.info)
        {
            return;
        }

        log("Hole punching to " + remoteHost + ":" + to_string(remotePort) + ".");

        chrono::milliseconds startTime = currentTime();

        // Hole punching
        while (true)
        {
            result = connect(client, connectAddress.info->ai_addr, connectAddress.info->ai_addrlen);

            if (result != 0)
            {
                if (currentTime() - startTime > maxHolePunchDuration)
                {
                    error("Failed to hole punch: " + to_string(errno));
                    close(client);
                    break;
                }

                continue;
            }

            SSL* ssl = SSL_new(ctx);

            if (!ssl)
            {
                error("Failed to create OpenSSL connection: " + getOpenSSLError());
                close(client);
                break;
            }

            result = SSL_set_fd(ssl, client);

            if (result <= 0)
            {
                error("Failed to assign file descriptor for OpenSSL: " + getOpenSSLError());
                SSL_free(ssl);
                close(client);
                return;
            }

            result = SSL_accept(ssl);

            if (result <= 0)
            {
                error("Failed to accept OpenSSL connection: " + getOpenSSLError());
                SSL_free(ssl);
                break;
            }

            result = setFileControl(client, O_NONBLOCK, true);

            if (result != 0)
            {
                SSL_free(ssl);
                break;
            }

            result = setSocketOption(client, IPPROTO_TCP, TCP_NODELAY, 1);

            if (result != 0)
            {
                SSL_free(ssl);
                break;
            }

            lock_guard<mutex> lock(tasksLock);

            tasks.push_back(bind(&ServerPOSIX::createClient, this, Address(remoteHost, remotePort), ssl));
            break;
        }
    }
}
#endif
