/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

class World;
class Entity;

enum UndoFrameType : u8
{
    Undo_Frame_Add,
    Undo_Frame_Remove,
    Undo_Frame_Modify
};

class UndoFrame
{
public:
    UndoFrame();
    UndoFrame(const UndoFrame& rhs);
    UndoFrame(UndoFrame&& rhs) noexcept;
    ~UndoFrame();

    static UndoFrame addFrame(const Entity* after);
    static UndoFrame removeFrame(const Entity* before);
    static UndoFrame modifyFrame(const Entity* before, const Entity* after, bool mergeable);

    UndoFrame& operator=(const UndoFrame& rhs);
    UndoFrame& operator=(UndoFrame&& rhs) noexcept;

    bool mergeable() const;
    void merge(const UndoFrame& newer);

    void undo(World* world);
    void redo(World* world);

private:
    UndoFrameType type;
    EntityID parentID;
    EntityID id;
    Entity* before;
    Entity* after;
    bool _mergeable;

    void copy(const UndoFrame& rhs);
};
