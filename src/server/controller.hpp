/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "network_event.hpp"
#include "connection.hpp"
#include "step_frame_history.hpp"
#include "checkpoint.hpp"
#include "server.hpp"
#include "disconnection_reason.hpp"
#include "compare.hpp"

class Controller
{
public:
    Controller(
        Server* server,
        const string& password,
        const string& adminPassword,
        const string& savePath,
        const optional<string>& joinURL
    );
    Controller(const Controller& rhs) = delete;
    Controller(Controller&& rhs) = delete;
    ~Controller();

    Controller& operator=(const Controller& rhs) = delete;
    Controller& operator=(Controller&& rhs) = delete;

    void run();

    void onReceive(NetID id, const NetworkEvent& event);
    void onLeave(NetID id);

    void addCheckpoint(const string& name);
    void removeCheckpoint(const string& name);
    void revertCheckpoint(const string& name);
    const map<string, Checkpoint>& checkpoints() const;

    void setShouldSave();

    void sendOne(UserID id, const NetworkEvent& event) const;
    void sendAll(const NetworkEvent& event) const;
    void disconnect(NetID id, DisconnectionReason reason) const;

    GameState& game();

private:
    // Arguments
    Server* server;
    const string password;
    const string adminPassword;
    const string savePath;
    const string saveBackupPath;
    const optional<string> joinURL;

    // State
    map<string, Checkpoint> _checkpoints;

    u64 stepIndex;
    StepFrame now;
    StepFrameHistory history;
    vector<function<bool()>> replayableHybridEffects;

    // Ephemeral
    map<NetID, Connection> connections;
    vector<tuple<NetID, NetworkEvent>> systemEvents;

    map<NetID, UserID> userMapping;
    map<UserID, NetID> userReverseMapping;
    map<UserID, StepFrameHistory> userHistories;

    bool shouldSave;
    bool saving;
    chrono::milliseconds lastSaveTime;

    chrono::milliseconds pingSentTime;

    void handleNetworkEvents();
    void handleNetworkEventsForConnection(NetID id, Connection& connection);
    void handleNetworkEventBatch(UserID id, Connection& connection, const NetworkEventBatch& batch);
    void stepSimulation();
    void stepForwards();
    void stepBackwards();
    void stepSystemInputs();
    void stepSystemOutputs();

    bool handleNetworkEvent(NetID id, const NetworkEvent& event);

    void handleJoinEvent(NetID id, const JoinEvent& event);
    void handleLeaveEvent(NetID id, const LeaveEvent& event);
    void handleVoiceEvent(User* user, const VoiceEvent& event);
    void handlePingEvent(User* user, const PingEvent& event);
    void handleCheckpointEvent(User* user, const CheckpointEvent& event);
    void handleSaveEvent(User* user, const SaveEvent& event);

    void handleCheckpointAdd(User* user, const CheckpointEvent& event);
    void handleCheckpointRemove(User* user, const CheckpointEvent& event);
    void handleCheckpointRevert(User* user, const CheckpointEvent& event);

    bool load();
    void save() const;

    void loadState(const string& path);
    void saveState(const string& path) const;

    void disableSaving();

    void sendDelta(UserID id, const Connection& connection, const Compare& compare) const;
};
