/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "checkpoint.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

Checkpoint::Checkpoint()
{
    creationTime = currentTime();
}

Checkpoint::Checkpoint(const StepFrame& frame)
    : Checkpoint()
{
    state = frame.state;
}

StepFrame Checkpoint::get() const
{
    StepFrame frame;

    frame.state = state;

    return frame;
}

bool Checkpoint::newer(const Checkpoint& other) const
{
    return creationTime > other.creationTime;
}

template<>
Checkpoint YAMLNode::convert() const
{
    Checkpoint checkpoint;

    checkpoint.creationTime = currentTime() - chrono::milliseconds(at("age").as<u64>());
    checkpoint.state = at("state").as<GameState>();

    return checkpoint;
}

template<>
void YAMLSerializer::emit(Checkpoint v)
{
    startMapping();

    emitPair("age", static_cast<i64>((currentTime() - v.creationTime).count()));
    emitPair("state", v.state);

    endMapping();
}
