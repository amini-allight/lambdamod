/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum ServerConnectionMode : u8
{
    Server_Connection_Direct,
    Server_Connection_Invite,
    Server_Connection_Session
};

struct Config
{
    Config();

    string host;
    u16 port;
    string password;
    string adminPassword;
    string savePath;
    u64 stepRate;
    f64 historyWindow;
    bool saveHistory;
    string certFilePath;
    string keyFilePath;
    ServerConnectionMode connectionMode;
    f64 saveInterval;
    u64 maxUndoHistory;
    u64 maxRecursionDepth;
    u64 maxMemoryUsage;
    f64 maxLoopDuration;
    bool upnp;
    bool upnpIPv6;
    string webSocketHost;
    u16 webSocketPort;
};

Config loadConfig();
