/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_entity_body_texture.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"

RenderEntityBodyTexture::RenderEntityBodyTexture(const RenderContext* ctx, const BodyRenderMaterialTextureData& texture)
    : width(texture.width)
    , height(texture.height)
    , needsLayoutSetup(true)
{
    uvec2 tileLayout(
        ceil(width / static_cast<f64>(textureTileSize)),
        ceil(height / static_cast<f64>(textureTileSize))
    );

    for (u32 y = 0; y < tileLayout.y; y++)
    {
        for (u32 x = 0; x < tileLayout.x; x++)
        {
            if (tiles.size() == maxTextureTileCount)
            {
                break;
            }

            uvec2 offset(
                x * textureTileSize,
                y * textureTileSize
            );
            uvec2 extent(
                min(width - offset.x, textureTileSize),
                min(height - offset.y, textureTileSize)
            );

            tiles.push_back(new RenderEntityBodyTextureTile(ctx, texture, offset, extent));
        }
    }
}

RenderEntityBodyTexture::~RenderEntityBodyTexture()
{
    for (RenderEntityBodyTextureTile* tile : tiles)
    {
        delete tile;
    }
}

bool RenderEntityBodyTexture::supports(const BodyRenderMaterialTextureData& texture) const
{
    return texture.width == width && texture.height == height;
}

void RenderEntityBodyTexture::fill(const BodyRenderMaterialTextureData& texture)
{
    for (RenderEntityBodyTextureTile* tile : tiles)
    {
        tile->fill(texture);
    }

    needsLayoutSetup = true;
}

void RenderEntityBodyTexture::setupLayout(VkCommandBuffer commandBuffer)
{
    if (!needsLayoutSetup)
    {
        return;
    }

    for (RenderEntityBodyTextureTile* tile : tiles)
    {
        tile->setupLayout(commandBuffer);
    }

    needsLayoutSetup = false;
}

vector<VkImageView> RenderEntityBodyTexture::imageViews() const
{
    vector<VkImageView> imageViews;
    imageViews.reserve(tiles.size());

    for (RenderEntityBodyTextureTile* tile : tiles)
    {
        imageViews.push_back(tile->imageView);
    }

    return imageViews;
}
