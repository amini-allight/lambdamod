/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "flat_viewer.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "network_event.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "flat_render.hpp"
#include "sound.hpp"

#include "render_perspective_grid.hpp"
#include "render_orthographic_grid.hpp"

static vec3 pickOrthographicUp(const vec3& dir)
{
    if (roughly<f64>(abs(dir.dot(upDir)), 1))
    {
        return forwardDir;
    }
    else
    {
        return upDir;
    }
}

static constexpr f64 viewerSpeed = 1;
static constexpr f64 viewerSprintSpeed = 20;
static constexpr f64 viewerPanSpeed = 2;
static constexpr f64 viewerShiftSpeed = 400;
static constexpr f64 viewerZoomSpeed = 2;
static constexpr f64 orthoRotateIncrement = tau / 24;
static constexpr f64 gamepadPanSpeed = pi / 2;

FlatViewer::FlatViewer(ControlContext* ctx)
    : ctx(ctx)
    , width(g_config.width)
    , height(g_config.height)
{
    _mode = Flat_Viewer_Free;

    fly.position = { 2, 2, 2 };
    fly.x = radians(-35);
    fly.z = radians(135);

    perspective.x = radians(35);
    perspective.z = radians(-135);
    perspective.zoom = 1.792; // Same as fly camera position

    orthographic.zoom = 0;

    shifting = false;
    zooming = false;
    rotating = false;
    sprinting = false;

    for (i32 i = 0; i < 6; i++)
    {
        movements[i] = false;
    }

    perspectiveGridID = render()->addDecoration<RenderPerspectiveGrid>(
        ctx->viewerMode()->gridPosition(),
        ctx->viewerMode()->gridSize()
    );

    orthographicGridID = render()->addDecoration<RenderOrthographicGrid>(
        position(),
        rotation(),
        orthographicDistance() * 2,
        orthographicDistance() * 2 * (height / static_cast<f64>(width))
    );
    render()->setDecorationShown(orthographicGridID, false);

    updateViewer();
}

FlatViewer::~FlatViewer()
{
    render()->removeDecoration(orthographicGridID);
    render()->removeDecoration(perspectiveGridID);
}

void FlatViewer::focus()
{
    // Do nothing
}

void FlatViewer::unfocus()
{
    shifting = false;
    zooming = false;
    rotating = false;
    sprinting = false;

    for (i32 i = 0; i < 6; i++)
    {
        movements[i] = false;
    }
}

void FlatViewer::resize(f64 width, f64 height)
{
    this->width = width;
    this->height = height;

    updateMatrices();
}

optional<Point> FlatViewer::worldToScreen(const vec3& world) const
{
    vec3 viewerSpacePos = rotation().inverse().rotate(world - position());

    if (viewerSpacePos.y < 0)
    {
        return {};
    }

    vec4 screenPos = projection * view * vec4(world.x, world.y, world.z, 1);

    screenPos.x /= screenPos.w;
    screenPos.y /= screenPos.w;
    screenPos.z /= screenPos.w;

    return Point(
        ((screenPos.x + 1) / 2) * width,
        (((screenPos.y * (_mode == Flat_Viewer_Orthographic ? +1 : -1)) + 1) / 2) * height
    );
}

vec3 FlatViewer::screenToWorld(const Point& screen) const
{
    vec4 worldPos = (projection * view).inverse() * vec4(
        ((screen.x / width) * 2) - 1,
        (((screen.y / height) * 2) - 1) * (_mode == Flat_Viewer_Orthographic ? +1 : -1),
        0.5,
        1
    );

    worldPos.x /= worldPos.w;
    worldPos.y /= worldPos.w;
    worldPos.z /= worldPos.w;

    return (worldPos.toVec3() - position()).normalize();
}

void FlatViewer::move(const vec3& position, const quaternion& rotation)
{
    _mode = Flat_Viewer_Free;
    fly.position = position;
    fly.x = (angleBetween(upDir, rotation.forward()) - (pi / 2)) * -1;
    fly.z = -sign(rotation.forward().x) * angleBetween(forwardDir, vec3(rotation.forward().toVec2(), 0).normalize());
    lastPosition = position;
    lastRotation = rotation;

    updateViewer();
}

void FlatViewer::step()
{
    f64 stepInterval = 1.0 / g_config.stepRate;

    ctx->controller()->render()->setCameraSpace(ctx->viewerSpace().rotation());
    ctx->controller()->sound()->setSpeedOfSound(ctx->controller()->selfWorld()->speedOfSound(position()));

    f64 speed = sprinting ? viewerSprintSpeed : viewerSpeed;

    if (_mode != Flat_Viewer_Orthographic)
    {
        vec3 direction;

        if (movements[0])
        {
            _mode = Flat_Viewer_Free;

            direction += rotation().forward();
        }

        if (movements[1])
        {
            _mode = Flat_Viewer_Free;

            direction += rotation().back();
        }

        if (movements[2])
        {
            _mode = Flat_Viewer_Free;

            direction += rotation().left();
        }

        if (movements[3])
        {
            _mode = Flat_Viewer_Free;

            direction += rotation().right();
        }

        if (movements[4])
        {
            _mode = Flat_Viewer_Free;

            direction += rotation().up();
        }

        if (movements[5])
        {
            _mode = Flat_Viewer_Free;

            direction += rotation().down();
        }

        fly.position += direction.normalize() * speed * stepInterval;
    }

    // Gamepad move
    if (_moveStickMotion.x < 0)
    {
        vec3 linVPart = _moveStickMotion.x * rotation().right() * speed;
        fly.position += linVPart * stepInterval;
    }
    else if (_moveStickMotion.x > 0)
    {
        vec3 linVPart = _moveStickMotion.x * rotation().right() * speed;
        fly.position += linVPart * stepInterval;
    }

    if (_moveStickMotion.y < 0)
    {
        vec3 linVPart = _moveStickMotion.y * rotation().forward() * speed * -1;
        fly.position += linVPart * stepInterval;
    }
    else if (_moveStickMotion.y > 0)
    {
        vec3 linVPart = _moveStickMotion.y * rotation().forward() * speed * -1;
        fly.position += linVPart * stepInterval;
    }

    // Gamepad look
    if (_lookStickMotion.y != 0)
    {
        f64 value = _lookStickMotion.y * gamepadPanSpeed * g_config.gamepadViewerSensitivity.y * stepInterval * (g_config.invertGamepadViewerY ? +1 : -1);

        fly.x = clamp(fly.x + value, pi / -2, pi / +2);
    }

    if (_lookStickMotion.x != 0)
    {
        fly.z += _lookStickMotion.x * gamepadPanSpeed * g_config.gamepadViewerSensitivity.x * stepInterval * (g_config.invertGamepadViewerX ? +1 : -1);
    }

    updateViewer();
}

void FlatViewer::motion(const vec3& motion)
{
    if (shifting)
    {
        f64 x = -motion.x;
        f64 z = motion.y;

        vec3 shift = (rotation().right() * (x / height) * viewerShiftSpeed) + (rotation().up() * (z / height) * viewerShiftSpeed);

        switch (_mode)
        {
        case Flat_Viewer_Free :
            fly.position += shift;
            break;
        case Flat_Viewer_Perspective :
            perspective.position += shift * perspectiveDistance();
            break;
        case Flat_Viewer_Orthographic :
            orthographic.position += shift * orthographicDistance();
            break;
        }
    }
    else if (zooming)
    {
        f64 y = motion.y;

        f64 zoomDelta = y * viewerZoomSpeed;

        switch (_mode)
        {
        case Flat_Viewer_Free :
            return;
        case Flat_Viewer_Perspective :
            perspective.zoom += zoomDelta;
            break;
        case Flat_Viewer_Orthographic :
            orthographic.zoom += zoomDelta;
            break;
        }
    }
    else if (rotating)
    {
        if (_mode == Flat_Viewer_Orthographic)
        {
            _mode = Flat_Viewer_Free;
        }

        switch (_mode)
        {
        case Flat_Viewer_Free :
        {
            f64 x = motion.y * g_config.mouseViewerSensitivity.y * (g_config.invertMouseViewerY ? +1 : -1);
            f64 z = motion.x * g_config.mouseViewerSensitivity.x * (g_config.invertMouseViewerX ? +1 : -1);

            fly.x = clamp(fly.x + x * viewerPanSpeed, pi / -2, pi / +2);
            fly.z = fly.z + z * viewerPanSpeed;
            break;
        }
        case Flat_Viewer_Perspective :
        {
            f64 x = motion.y * g_config.mouseViewerSensitivity.y * (g_config.invertMouseViewerY ? -1 : +1);
            f64 z = motion.x * g_config.mouseViewerSensitivity.x * (g_config.invertMouseViewerX ? -1 : +1);

            perspective.x = clamp(perspective.x + x * viewerPanSpeed, pi / -2, pi / +2);
            perspective.z = perspective.z + z * viewerPanSpeed;
            break;
        }
        case Flat_Viewer_Orthographic :
            break;
        }
    }
    else
    {
        return;
    }

    updateViewer();
}

FlatViewerMode FlatViewer::mode() const
{
    return _mode;
}

vec3 FlatViewer::position() const
{
    switch (_mode)
    {
    default :
    case Flat_Viewer_Free :
        return fly.position;
    case Flat_Viewer_Perspective :
        return perspective.position + (-rotation().forward() * perspectiveDistance());
    case Flat_Viewer_Orthographic :
        return orthographic.position + (-orthographic.direction * orthographicDistance());
    }
}

quaternion FlatViewer::rotation() const
{
    switch (_mode)
    {
    default :
    case Flat_Viewer_Free :
        return quaternion(upDir, fly.z) * quaternion(rightDir, fly.x);
    case Flat_Viewer_Perspective :
        return quaternion(upDir, -perspective.z) * quaternion(rightDir, -perspective.x);
    case Flat_Viewer_Orthographic :
    {
        vec3 side = orthographic.direction.cross(orthographic.upDirection).normalize();
        vec3 up = side.cross(orthographic.direction).normalize();

        return quaternion(orthographic.direction, up);
    }
    }
}

void FlatViewer::shift(bool state)
{
    shifting = state;
}

bool FlatViewer::shift() const
{
    return shifting;
}

void FlatViewer::zoom(bool state)
{
    zooming = state;
}

bool FlatViewer::zoom() const
{
    return zooming;
}

void FlatViewer::rotate(bool state)
{
    rotating = state;
}

bool FlatViewer::rotate() const
{
    return rotating;
}

void FlatViewer::sprint(bool state)
{
    sprinting = state;
}

bool FlatViewer::sprint() const
{
    return sprinting;
}

void FlatViewer::forward(bool state)
{
    movements[0] = state;
}

bool FlatViewer::forward() const
{
    return movements[0];
}

void FlatViewer::back(bool state)
{
    movements[1] = state;
}

bool FlatViewer::back() const
{
    return movements[1];
}

void FlatViewer::left(bool state)
{
    movements[2] = state;
}

bool FlatViewer::left() const
{
    return movements[2];
}

void FlatViewer::right(bool state)
{
    movements[3] = state;
}

bool FlatViewer::right() const
{
    return movements[3];
}

void FlatViewer::up(bool state)
{
    movements[4] = state;
}

bool FlatViewer::up() const
{
    return movements[4];
}

void FlatViewer::down(bool state)
{
    movements[5] = state;
}

bool FlatViewer::down() const
{
    return movements[5];
}

void FlatViewer::moveStickMotion(const vec3& position)
{
    _moveStickMotion = position;
}

void FlatViewer::lookStickMotion(const vec3& position)
{
    _lookStickMotion = position;
}

void FlatViewer::freeLook()
{
    _mode = Flat_Viewer_Free;
}

void FlatViewer::perspLook(const vec3& pos)
{
    _mode = Flat_Viewer_Perspective;
    perspective.position = pos;

    updateViewer();
}

void FlatViewer::orthoLook(const vec3& pos, const vec3& dir)
{
    _mode = Flat_Viewer_Orthographic;
    orthographic.position = pos;
    orthographic.direction = dir;
    orthographic.upDirection = pickOrthographicUp(orthographic.direction);

    updateViewer();
}

void FlatViewer::orthoToggle()
{
    switch (_mode)
    {
    case Flat_Viewer_Free :
        _mode = Flat_Viewer_Orthographic;
        break;
    case Flat_Viewer_Perspective :
        _mode = Flat_Viewer_Orthographic;
        break;
    case Flat_Viewer_Orthographic :
        _mode = Flat_Viewer_Free;
        break;
    }

    updateViewer();
}

void FlatViewer::rotateUp()
{
    if (_mode != Flat_Viewer_Orthographic)
    {
        return;
    }

    quaternion rotation(orthographic.direction.cross(orthographic.upDirection).normalize(), -orthoRotateIncrement);

    orthographic.direction = rotation.rotate(orthographic.direction);
    orthographic.upDirection = rotation.rotate(orthographic.upDirection);

    updateViewer();
}

void FlatViewer::rotateDown()
{
    if (_mode != Flat_Viewer_Orthographic)
    {
        return;
    }

    quaternion rotation(orthographic.direction.cross(orthographic.upDirection).normalize(), +orthoRotateIncrement);

    orthographic.direction = rotation.rotate(orthographic.direction);
    orthographic.upDirection = rotation.rotate(orthographic.upDirection);

    updateViewer();
}

void FlatViewer::rotateLeft()
{
    if (_mode != Flat_Viewer_Orthographic)
    {
        return;
    }

    quaternion rotation(upDir, -orthoRotateIncrement);

    orthographic.direction = rotation.rotate(orthographic.direction);
    orthographic.upDirection = rotation.rotate(orthographic.upDirection);

    updateViewer();
}

void FlatViewer::rotateRight()
{
    if (_mode != Flat_Viewer_Orthographic)
    {
        return;
    }

    quaternion rotation(upDir, +orthoRotateIncrement);

    orthographic.direction = rotation.rotate(orthographic.direction);
    orthographic.upDirection = rotation.rotate(orthographic.upDirection);

    updateViewer();
}

void FlatViewer::invert()
{
    if (_mode != Flat_Viewer_Orthographic)
    {
        return;
    }

    if (orthographic.upDirection == upDir)
    {
        orthographic.direction *= -1;
    }
    else
    {
        orthographic.direction *= -1;
        orthographic.upDirection *= -1;
    }

    updateViewer();
}

void FlatViewer::zoomIn()
{
    switch (_mode)
    {
    case Flat_Viewer_Free :
        return;
    case Flat_Viewer_Perspective :
        perspective.zoom -= 1;
        break;
    case Flat_Viewer_Orthographic :
        orthographic.zoom -= 1;
        break;
    }

    updateViewer();
}

void FlatViewer::zoomOut()
{
    switch (_mode)
    {
    case Flat_Viewer_Free :
        return;
    case Flat_Viewer_Perspective :
        perspective.zoom += 1;
        break;
    case Flat_Viewer_Orthographic :
        orthographic.zoom += 1;
        break;
    }

    updateViewer();
}

void FlatViewer::updatePerspectiveGrid()
{
    render()->updateDecoration<RenderPerspectiveGrid>(
        perspectiveGridID,
        ctx->viewerMode()->gridPosition(),
        ctx->viewerMode()->gridSize()
    );
    render()->setDecorationShown(
        perspectiveGridID,
        ctx->viewerMode()->gridShown()
    );
}

void FlatViewer::updateViewer()
{
    if (_mode == Flat_Viewer_Orthographic)
    {
        render()->setCameraOrthographic(orthographicDistance());
    }
    else
    {
        render()->setCameraPerspective();
    }

    render()->setCamera(
        position(),
        rotation()
    );
    ctx->controller()->sound()->setMicrophone(
        position(),
        rotation()
    );

    updateMatrices();

    if (_mode == Flat_Viewer_Free && (position() != lastPosition || rotation() != lastRotation))
    {
        ctx->controller()->send(UserEvent(
            User_Event_Move_Viewer,
            ctx->controller()->selfID(),
            { position(), rotation(), vec3(1) }
        ));

        lastPosition = position();
        lastRotation = rotation();
    }

    if (_mode == Flat_Viewer_Orthographic)
    {
        render()->setDecorationShown(perspectiveGridID, false);
        render()->updateDecoration<RenderOrthographicGrid>(
            orthographicGridID,
            position(),
            rotation(),
            orthographicDistance() * 2,
            orthographicDistance() * 2 * (height / static_cast<f64>(width))
        );
    }
    else
    {
        render()->setDecorationShown(orthographicGridID, false);
        render()->setDecorationShown(perspectiveGridID, ctx->viewerMode()->gridShown() && !ctx->attached());
    }
}

void FlatViewer::updateMatrices()
{
    switch (_mode)
    {
    case Flat_Viewer_Free :
    case Flat_Viewer_Perspective :
        projection = createFlatPerspectiveMatrix(
            defaultCameraAngle,
            width / static_cast<f64>(height),
            defaultNearDistance,
            defaultFarDistance
        );
        break;
    case Flat_Viewer_Orthographic :
    {
        f64 zoom = orthographicDistance();

        f64 left = -zoom;
        f64 right = +zoom;
        f64 top = -(zoom / width) * height;
        f64 bottom = +(zoom / width) * height;

        projection = createFlatOrthographicMatrix(
            left,
            right,
            bottom,
            top,
            defaultNearDistance,
            defaultFarDistance
        );
        break;
    }
    }

    view = createViewMatrix(position(), rotation());
}

f64 FlatViewer::perspectiveDistance() const
{
    return pow(2, perspective.zoom);
}

f64 FlatViewer::orthographicDistance() const
{
    return pow(2, orthographic.zoom);
}

FlatRender* FlatViewer::render() const
{
    return dynamic_cast<FlatRender*>(ctx->controller()->render());
}
