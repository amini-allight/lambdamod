/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "syntax_highlighting.hpp"
#include "interface_constants.hpp"

bool SyntaxHighlightChunk::operator>(const SyntaxHighlightChunk& rhs) const
{
    return start > rhs.start;
}

bool SyntaxHighlightChunk::operator<(const SyntaxHighlightChunk& rhs) const
{
    return start < rhs.start;
}

static bool isBoundary(char c)
{
    return c == ' ' || c == '(' || c == ')' || c == '\'' || c == '"';
}

static bool isIsolated(const string& line, size_t start, size_t size)
{
    size_t prev = start - 1;

    if (prev < line.size() && !isBoundary(line[prev]))
    {
        return false;
    }

    size_t next = start + size;

    if (next < line.size() && !isBoundary(line[next]))
    {
        return false;
    }

    return true;
}

static vector<SyntaxHighlightChunk> findStrings(const string& line)
{
    vector<SyntaxHighlightChunk> chunks;

    string s = line;
    smatch m;
    regex r("\"(\\\\\"|[^\"])*\"");

    size_t i = 0;
    while (regex_search(s, m, r))
    {
        SyntaxHighlightChunk chunk;

        chunk.type = Syntax_Highlight_String;
        chunk.start = i + m.position();
        chunk.size = m.length();

        chunks.push_back(chunk);

        s = m.suffix().str();
        i += m.position() + m.length();
    }

    vector<SyntaxHighlightChunk> splitChunks;

    for (const SyntaxHighlightChunk& chunk : chunks)
    {
        i32 start = chunk.start;

        for (i32 i = chunk.start; i < chunk.size - 1; i++)
        {
            if (line[i] == '\\' && (line[i + 1] == '\\' || line[i + 1] == '"'))
            {
                splitChunks.push_back({ Syntax_Highlight_String, start, i - start });
                splitChunks.push_back({ Syntax_Highlight_Escaped, i, 2 });
                i++;
                start = i + 1;
            }
        }

        splitChunks.push_back({ Syntax_Highlight_String, start, (chunk.start + chunk.size) - start });
    }

    return splitChunks;
}

static vector<SyntaxHighlightChunk> findKeywords(const string& line)
{
    vector<SyntaxHighlightChunk> chunks;

    string s = line;
    smatch m;
    regex r("(define-restrict|define-control|define-find|define-list|define-exists\\?|define-syntax|undefine|define|get|set!|set|size|join|quote|list|eval|lambda|syntax|throw|catch|print|type|->)");

    size_t i = 0;
    while (regex_search(s, m, r))
    {
        if (isIsolated(line, i + m.position(), m.length()))
        {
            SyntaxHighlightChunk chunk;

            chunk.type = Syntax_Highlight_Keyword;
            chunk.start = i + m.position();
            chunk.size = m.length();

            chunks.push_back(chunk);
        }

        s = m.suffix().str();
        i += m.position() + m.length();
    }

    return chunks;
}

static vector<SyntaxHighlightChunk> findStdlib(const string& line)
{
    vector<SyntaxHighlightChunk> chunks;

    string s = line;
    smatch m;
    regex r("(\\+\\+|--|\\+|-|\\*\\*|//|\\*|/|%|\\!|\\&\\&|\\|\\||\\^\\^|\\~|&|\\||\\^|>>|<<|==|\\!=|>|<|>=|<=|<=>|modulo|not|and|or|xor|not-equal\\?|equal\\?|expt|root|sqrt|sq|let|begin|if|when|switch|condition|for-each|while|zero\\?|positive\\?|negative\\?|sin|cos|tan|asin|acos|atan|sinh|cosh|tanh|asinh|acosh|atanh|log|abs|ceil|floor|min-of|max-of|trunc|fractional|sign|round|min|max|clamp|void\\?|symbol\\?|int\\?|float\\?|string\\?|list\\?|lambda\\?|handle\\?|string->integer|string->float|string->symbol|integer->float|integer->string|float->integer|float->string|symbol->string|nullptr\\?|has\\?|exec|read|write|remove|apply|compose|first|last|empty\\?|insert|erase|contains\\?|index|replace|slice|zip|push-first|push-last|pop-first|pop-last|any-of\\?|all-of\\?|none-of\\?|sum-of|select|sort|shuffle|choose|reverse|generate|map|reduce|count|union|difference|intersection|range|inrange|string-empty\\?|string-insert|string-erase|string-format|string-starts-with\\?|string-ends-with\\?|string-contains\\?|string-index|string-replace|string-slice|string-upcase|string-downcase|random-engine|random-linear|random-gauss|degrees->radians|radians->degrees|struct-define|struct-undefine|table-set|table-insert|table-assign|table-erase|table-has\\?|table-get|set-insert|set-erase|set-has\\?|vec2\\?|vec2\\+|vec2-|vec2\\*|vec2/|vec2-dot|vec2-flip|vec2-distance|vec2-length|vec2-normalize|vec2|vec3\\?|vec3\\+|vec3-|vec3\\*|vec3/|vec3-dot|vec3-cross|vec3-distance|vec3-length|vec3-normalize|vec3|quaternion\\?|quaternion-from-direction|quaternion-from-axis-angle|quaternion\\*|quaternion/|quaternion-inverse|quaternion-rotate|quaternion-forward|quaternion-back|quaternion-left|quaternion-right|quaternion-up|quaternion-down|quaternion-axis|quaternion-angle|quaternion-length|quaternion-normalize|quaternion|screen-project|screen-unproject|repeater-start|repeater-stop|repeater-all|timer-start|timer-stop|timer-all|nullptr|true|false|pi|tau|phi|euler|forward|back|left|right|up|down|type-void|type-symbol|type-integer|type-float|type-string|type-list|type-lambda|type-handle|void|handle|for|do)");

    size_t i = 0;
    while (regex_search(s, m, r))
    {
        if (isIsolated(line, i + m.position(), m.length()))
        {
            SyntaxHighlightChunk chunk;

            chunk.type = Syntax_Highlight_Stdlib;
            chunk.start = i + m.position();
            chunk.size = m.length();

            chunks.push_back(chunk);
        }

        s = m.suffix().str();
        i += m.position() + m.length();
    }

    return chunks;
}

static vector<SyntaxHighlightChunk> findBrackets(const string& line)
{
    vector<SyntaxHighlightChunk> chunks;

    string s = line;
    smatch m;
    regex r("(\\(|\\))");

    size_t i = 0;
    while (regex_search(s, m, r))
    {
        SyntaxHighlightChunk chunk;

        chunk.type = Syntax_Highlight_Bracket;
        chunk.start = i + m.position();
        chunk.size = m.length();

        chunks.push_back(chunk);

        s = m.suffix().str();
        i += m.position() + m.length();
    }

    return chunks;
}

static vector<SyntaxHighlightChunk> findQuotes(const string& line)
{
    vector<SyntaxHighlightChunk> chunks;

    string s = line;
    smatch m;
    regex r("'");

    size_t i = 0;
    while (regex_search(s, m, r))
    {
        SyntaxHighlightChunk chunk;

        chunk.type = Syntax_Highlight_Quote;
        chunk.start = i + m.position();
        chunk.size = m.length();

        chunks.push_back(chunk);

        s = m.suffix().str();
        i += m.position() + m.length();
    }

    return chunks;
}

static vector<SyntaxHighlightChunk> findNumbers(const string& line)
{
    vector<SyntaxHighlightChunk> chunks;

    string s = line;
    smatch m;
    regex r("(-)?[0-9]+(\\.[0-9]+)?");

    size_t i = 0;
    while (regex_search(s, m, r))
    {
        if (isIsolated(line, i + m.position(), m.length()))
        {
            SyntaxHighlightChunk chunk;

            chunk.type = Syntax_Highlight_Number;
            chunk.start = i + m.position();
            chunk.size = m.length();

            chunks.push_back(chunk);
        }

        s = m.suffix().str();
        i += m.position() + m.length();
    }

    return chunks;
}

Color syntaxHighlightColor(SyntaxHighlightType type)
{
    switch (type)
    {
    case Syntax_Highlight_Escaped :
        return theme.escapedColor;
    case Syntax_Highlight_String :
        return theme.stringColor;
    case Syntax_Highlight_Keyword :
        return theme.keywordColor;
    case Syntax_Highlight_Stdlib :
        return theme.stdlibColor;
    case Syntax_Highlight_Bracket :
        return theme.bracketColor;
    case Syntax_Highlight_Quote :
        return theme.quoteColor;
    case Syntax_Highlight_Number :
        return theme.numberColor;
    default :
    case Syntax_Highlight_Text :
        return theme.textColor;
    }
}

static bool conflicts(const vector<SyntaxHighlightChunk>& hlLine, const SyntaxHighlightChunk& chunk)
{
    i32 first = chunk.start;
    i32 last = (chunk.start + chunk.size) - 1;

    for (const SyntaxHighlightChunk& other : hlLine)
    {
        i32 otherFirst = other.start;
        i32 otherLast = (other.start + other.size) - 1;

        if (first >= otherFirst && first <= otherLast)
        {
            return true;
        }
        else if (otherFirst >= first && otherFirst <= last)
        {
            return true;
        }
        else if (last >= otherFirst && last <= otherLast)
        {
            return true;
        }
        else if (otherLast >= first && otherLast <= last)
        {
            return true;
        }
    }

    return false;
}

vector<SyntaxHighlightChunk> calculateSyntaxHighlight(const string& line)
{
    vector<SyntaxHighlightChunk> hlLine;

    if (line.empty())
    {
        return hlLine;
    }

    vector<SyntaxHighlightChunk> strings = findStrings(line);

    for (const SyntaxHighlightChunk& chunk : strings)
    {
        hlLine.push_back(chunk);
    }

    vector<SyntaxHighlightChunk> keywords = findKeywords(line);

    for (const SyntaxHighlightChunk& chunk : keywords)
    {
        if (conflicts(hlLine, chunk))
        {
            continue;
        }

        hlLine.push_back(chunk);
    }

    vector<SyntaxHighlightChunk> stdlib = findStdlib(line);

    for (const SyntaxHighlightChunk& chunk : stdlib)
    {
        if (conflicts(hlLine, chunk))
        {
            continue;
        }

        hlLine.push_back(chunk);
    }

    vector<SyntaxHighlightChunk> brackets = findBrackets(line);

    for (const SyntaxHighlightChunk& chunk : brackets)
    {
        if (conflicts(hlLine, chunk))
        {
            continue;
        }

        hlLine.push_back(chunk);
    }

    vector<SyntaxHighlightChunk> quotes = findQuotes(line);

    for (const SyntaxHighlightChunk& chunk : quotes)
    {
        if (conflicts(hlLine, chunk))
        {
            continue;
        }

        hlLine.push_back(chunk);
    }

    vector<SyntaxHighlightChunk> numbers = findNumbers(line);

    for (const SyntaxHighlightChunk& chunk : numbers)
    {
        if (conflicts(hlLine, chunk))
        {
            continue;
        }

        hlLine.push_back(chunk);
    }

    sort(hlLine.begin(), hlLine.end());

    for (size_t i = 0; i < hlLine.size();)
    {
        const SyntaxHighlightChunk& chunk = hlLine[i];

        i32 end = chunk.start + chunk.size;

        i32 correctEnd = i + 1 == hlLine.size()
            ? line.size()
            : hlLine[i + 1].start;

        if (end == correctEnd)
        {
            i++;
        }
        // Fill in gap between chunks or to end of line
        else
        {
            SyntaxHighlightChunk newChunk;
            newChunk.type = Syntax_Highlight_Text;
            newChunk.start = end;
            newChunk.size = correctEnd - end;

            hlLine.insert(
                hlLine.begin() + i + 1,
                newChunk
            );
        }
    }

    // No highlights found
    if (hlLine.empty())
    {
        SyntaxHighlightChunk fullChunk;
        fullChunk.type = Syntax_Highlight_Text;
        fullChunk.start = 0;
        fullChunk.size = line.size();

        hlLine = { fullChunk };
    }
    // Initial segment not highlighted
    else if (hlLine.front().start != 0)
    {
        SyntaxHighlightChunk startChunk;
        startChunk.type = Syntax_Highlight_Text;
        startChunk.start = 0;
        startChunk.size = hlLine.front().start;

        hlLine.insert(hlLine.begin(), startChunk);
    }

    return hlLine;
}

tuple<i32, i32> bracketMatchHighlight(
    const string& text,
    i32 cursorIndex,
    i32 windowStart,
    i32 windowSize
)
{
    i32 windowEnd = windowStart + windowSize;

    if ((cursorIndex < static_cast<i32>(text.size()) && text[cursorIndex] == '(') ||
        (cursorIndex != 0 && text[cursorIndex - 1] == '('))
    {
        i32 depth = 1;

        i32 start = text[cursorIndex] == '(' ? cursorIndex : cursorIndex - 1;
        i32 end = text.size() - 1;

        for (i32 i = start + 1; i < static_cast<i32>(text.size()); i++)
        {
            char c = text[i];

            if (c == '(')
            {
                depth++;
            }
            else if (c == ')')
            {
                depth--;
            }

            if (depth == 0)
            {
                end = i;
                break;
            }
        }

        start = max(start, windowStart);
        end = min(end, windowEnd);

        return { start - windowStart, (end - start) + 1 };
    }
    else if ((cursorIndex < static_cast<i32>(text.size()) && text[cursorIndex] == ')') ||
        (cursorIndex != 0 && text[cursorIndex - 1] == ')'))
    {
        i32 depth = 1;

        i32 start = 0;
        i32 end = text[cursorIndex] == ')' ? cursorIndex : cursorIndex - 1;

        for (i32 i = end - 1; i >= 0; i--)
        {
            char c = text[i];

            if (c == '(')
            {
                depth--;
            }
            else if (c == ')')
            {
                depth++;
            }

            if (depth == 0)
            {
                start = i;
                break;
            }
        }

        start = max(start, windowStart);
        end = min(end, windowEnd);

        return { start - windowStart, (end - start) + 1 };
    }
    else
    {
        return { 0, 0 };
    }
}
