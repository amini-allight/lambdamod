/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "fixed_column.hpp"

FixedColumn::FixedColumn(InterfaceWidget* parent, i32 width)
    : Column(parent)
    , width(width)
{

}

SizeProperties FixedColumn::sizeProperties() const
{
    return {
        static_cast<f64>(width), 0,
        Scaling_Fixed, Scaling_Fill
    };
}
