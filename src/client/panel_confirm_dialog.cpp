/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_confirm_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "row.hpp"
#include "column.hpp"
#include "panel_label.hpp"
#include "panel_text_button.hpp"
#include "spacer.hpp"

static constexpr f32 overlayOpacity = 0.9;

PanelConfirmDialog::PanelConfirmDialog(InterfaceWidget* parent, const string& text, const function<void(bool)>& onDone)
    : InterfaceWidget(parent)
{
    auto column = new Column(this);

    new PanelLabel(column, text);

    auto row = new Row(column);

    MAKE_SPACER(row, UIScale::marginSize(this) * 4, 0);

    new PanelTextButton(
        row,
        localize("panel-confirm-dialog-no"),
        bind(onDone, false)
    );

    MAKE_SPACER(row, UIScale::marginSize(this) * 4, 0);

    new PanelTextButton(
        row,
        localize("panel-confirm-dialog-yes"),
        bind(onDone, true)
    );

    MAKE_SPACER(row, UIScale::marginSize(this) * 4, 0);
}

void PanelConfirmDialog::move(const Point& position)
{
    InterfaceWidget::move(parent()->position() + (parent()->size() - internalSize()) / 2);

    this->position(parent()->position());
}

void PanelConfirmDialog::resize(const Size& size)
{
    InterfaceWidget::resize(internalSize());

    this->size(parent()->size());
}

void PanelConfirmDialog::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        Color(0, 0, 0, 255 * overlayOpacity)
    );

    InterfaceWidget::draw(ctx);
}

Size PanelConfirmDialog::internalSize() const
{
    return Size(parent()->size().w / 2, UIScale::panelLineEditHeight(this) * 2);
}

SizeProperties PanelConfirmDialog::sizeProperties() const
{
    return sizePropertiesFillAll;
}
