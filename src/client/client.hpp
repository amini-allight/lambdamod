/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "network_event.hpp"
#include "address.hpp"
#include "lag_faker.hpp"

class Client
{
public:
    Client(
        const function<void()>& connectCallback,
        const function<void(const NetworkEvent&)>& receiveCallback,
        const function<void()>& disconnectCallback
    );
    Client(const Client& rhs) = delete;
    Client(Client&& rhs) = delete;
    virtual ~Client();

    Client& operator=(const Client& rhs) = delete;
    Client& operator=(Client&& rhs) = delete;

    virtual void step() = 0;

    virtual void connect(const Address& address) = 0;
    virtual void send(const NetworkEvent& event) = 0;

protected:
    function<void()> connectCallback;
    function<void(const NetworkEvent&)> receiveCallback;
    function<void()> disconnectCallback;

    u64 nextMessageSize;
    string buffer;

    bool connected;

#ifdef LMOD_FAKELAG
    LagFaker lagFaker;
#endif

    void onConnect();
    void onReceive(const string& data);
    void onDisconnect();
};
