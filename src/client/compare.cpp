/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "compare.hpp"

Compare::Compare(
    StepFrame& now,
    const function<vector<NetworkEvent>()>& step,
    size_t stepCount
)
{
    StepFrame initial = now;

    for (size_t i = 0; i < stepCount; i++)
    {
        pushSideEffectEvents(step());
    }

    tie(_deltaEvents, _sideEffects) = now.compare(initial);
}

const vector<NetworkEvent>& Compare::deltaEvents() const
{
    return _deltaEvents;
}

const vector<EntityUpdateSideEffect>& Compare::sideEffects() const
{
    return _sideEffects;
}

const vector<NetworkEvent>& Compare::sideEffectEvents() const
{
    return _sideEffectEvents;
}

void Compare::pushSideEffectEvents(const vector<NetworkEvent>& sideEffectEvents)
{
    _sideEffectEvents.insert(
        _sideEffectEvents.end(),
        sideEffectEvents.begin(),
        sideEffectEvents.end()
    );
}
