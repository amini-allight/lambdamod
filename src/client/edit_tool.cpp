/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "edit_tool.hpp"
#include "localization.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "theme.hpp"
#include "flat_render.hpp"
#include "render_axis.hpp"
#include "translate_tool.hpp"

EditTool::EditTool(
    ViewerModeContext* ctx,
    const mat4& localSpace,
    const function<void(size_t, const vec3&, const mat4&, const mat4&)>& setGlobal,
    const function<mat4(size_t)>& getGlobal,
    const function<mat4(size_t)>& getGlobalWithPromotion,
    const function<size_t()>& count,
    bool soft
)
    : ctx(ctx)
    , localSpace(localSpace)
    , setGlobal(setGlobal)
    , getGlobal(getGlobal)
    , getGlobalWithPromotion(getGlobalWithPromotion)
    , count(count)
    , _soft(soft)
    , _snapping(false)
    , toolAxis(Edit_Tool_Axis_None)
    , toolPivot(Edit_Tool_Pivot_Mean_Point)
    , numericInput(false)
{
    axisID = dynamic_cast<FlatRender*>(ctx->context()->controller()->render())->addDecoration<RenderAxis>(
        vec3(),
        forwardDir,
        axisColor()
    );
    ctx->context()->controller()->render()->setDecorationShown(axisID, false);

    for (size_t i = 0; i < count(); i++)
    {
        initialGlobalTransforms.push_back(getGlobal(i));
        initialGlobalTransformsWithPromotion.push_back(getGlobalWithPromotion(i));
    }

    for (size_t i = 0; i < count(); i++)
    {
        initialGlobalCenter += getGlobal(i).position();
    }

    initialGlobalCenter /= count();
}

EditTool::~EditTool()
{
    ctx->context()->controller()->render()->removeDecoration(axisID);
}

string EditTool::axisName() const
{
    switch (toolAxis)
    {
    default :
    case Edit_Tool_Axis_None :
        return "";
    case Edit_Tool_Axis_Global_X :
        return localize("edit-tool-global-x");
    case Edit_Tool_Axis_Global_Y :
        return localize("edit-tool-global-y");
    case Edit_Tool_Axis_Global_Z :
        return localize("edit-tool-global-z");
    case Edit_Tool_Axis_Local_X :
        return localize("edit-tool-local-x");
    case Edit_Tool_Axis_Local_Y :
        return localize("edit-tool-local-y");
    case Edit_Tool_Axis_Local_Z :
        return localize("edit-tool-local-z");
    case Edit_Tool_Axis_Self_X :
        return localize("edit-tool-self-x");
    case Edit_Tool_Axis_Self_Y :
        return localize("edit-tool-self-y");
    case Edit_Tool_Axis_Self_Z :
        return localize("edit-tool-self-z");
    }
}

Color EditTool::axisColor() const
{
    switch (toolAxis)
    {
    default :
        fatal("Encountered unknown tool axis '" + to_string(toolAxis) + "'.");
    case Edit_Tool_Axis_None :
        return white;
    case Edit_Tool_Axis_Global_X :
    case Edit_Tool_Axis_Local_X :
    case Edit_Tool_Axis_Self_X :
        return theme.xAxisColor;
    case Edit_Tool_Axis_Global_Y :
    case Edit_Tool_Axis_Local_Y :
    case Edit_Tool_Axis_Self_Y :
        return theme.yAxisColor;
    case Edit_Tool_Axis_Global_Z :
    case Edit_Tool_Axis_Local_Z :
    case Edit_Tool_Axis_Self_Z :
        return theme.zAxisColor;
    }
}

bool EditTool::axisX() const
{
    switch (toolAxis)
    {
    default :
        return false;
    case Edit_Tool_Axis_None :
    case Edit_Tool_Axis_Global_X :
    case Edit_Tool_Axis_Local_X :
    case Edit_Tool_Axis_Self_X :
        return true;
    }
}

bool EditTool::axisY() const
{
    switch (toolAxis)
    {
    default :
        return false;
    case Edit_Tool_Axis_None :
    case Edit_Tool_Axis_Global_Y :
    case Edit_Tool_Axis_Local_Y :
    case Edit_Tool_Axis_Self_Y :
        return true;
    }
}

bool EditTool::axisZ() const
{
    switch (toolAxis)
    {
    default :
        return false;
    case Edit_Tool_Axis_None :
    case Edit_Tool_Axis_Global_Z :
    case Edit_Tool_Axis_Local_Z :
    case Edit_Tool_Axis_Self_Z :
        return true;
    }
}

string EditTool::pivotName() const
{
    switch (toolPivot)
    {
    default :
        return "";
    case Edit_Tool_Pivot_Mean_Point :
        return localize("edit-tool-mean-point-pivot");
    case Edit_Tool_Pivot_Cursor :
        return localize("edit-tool-cursor-pivot");
    case Edit_Tool_Pivot_Individual_Origins :
        return localize("edit-tool-individual-origins-pivot");
    }
}

bool EditTool::pivotMeanPoint() const
{
    return toolPivot == Edit_Tool_Pivot_Mean_Point;
}

bool EditTool::pivotCursor() const
{
    return toolPivot == Edit_Tool_Pivot_Cursor;
}

bool EditTool::pivotIndividualOrigins() const
{
    return toolPivot == Edit_Tool_Pivot_Individual_Origins;
}

bool EditTool::soft() const
{
    return _soft;
}

bool EditTool::snapping() const
{
    return _snapping;
}

void EditTool::startSnap()
{
    bool updated = !_snapping;

    _snapping = true;

    if (updated)
    {
        update();
    }
}

void EditTool::endSnap()
{
    bool updated = _snapping;

    _snapping = false;

    if (updated)
    {
        update();
    }
}

bool EditTool::transformingSubpart() const
{
    return dynamic_cast<const TranslateTool*>(this) ||
        (count() == 1 && toolPivot == Edit_Tool_Pivot_Mean_Point) ||
        toolPivot == Edit_Tool_Pivot_Individual_Origins;
}

void EditTool::togglePivotMode(const Point& point)
{
    switch (toolPivot)
    {
    case Edit_Tool_Pivot_Mean_Point :
        toolPivot = Edit_Tool_Pivot_Cursor;
        break;
    case Edit_Tool_Pivot_Cursor :
        toolPivot = Edit_Tool_Pivot_Individual_Origins;
        break;
    case Edit_Tool_Pivot_Individual_Origins :
        toolPivot = Edit_Tool_Pivot_Mean_Point;
        break;
    }

    update();
}

void EditTool::toggleX()
{
    switch (toolAxis)
    {
    default :
        toolAxis = Edit_Tool_Axis_Global_X;
        break;
    case Edit_Tool_Axis_Global_X :
        toolAxis = Edit_Tool_Axis_Local_X;
        break;
    case Edit_Tool_Axis_Local_X :
        toolAxis = Edit_Tool_Axis_Self_X;
        break;
    case Edit_Tool_Axis_Self_X :
        toolAxis = Edit_Tool_Axis_None;
        break;
    }

    update();
    updateDecoration();
}

void EditTool::toggleY()
{
    switch (toolAxis)
    {
    default :
        toolAxis = Edit_Tool_Axis_Global_Y;
        break;
    case Edit_Tool_Axis_Global_Y :
        toolAxis = Edit_Tool_Axis_Local_Y;
        break;
    case Edit_Tool_Axis_Local_Y :
        toolAxis = Edit_Tool_Axis_Self_Y;
        break;
    case Edit_Tool_Axis_Self_Y :
        toolAxis = Edit_Tool_Axis_None;
        break;
    }

    update();
    updateDecoration();
}

void EditTool::toggleZ()
{
    switch (toolAxis)
    {
    default :
        toolAxis = Edit_Tool_Axis_Global_Z;
        break;
    case Edit_Tool_Axis_Global_Z :
        toolAxis = Edit_Tool_Axis_Local_Z;
        break;
    case Edit_Tool_Axis_Local_Z :
        toolAxis = Edit_Tool_Axis_Self_Z;
        break;
    case Edit_Tool_Axis_Self_Z :
        toolAxis = Edit_Tool_Axis_None;
        break;
    }

    update();
    updateDecoration();
}

void EditTool::setAxis(EditToolAxis axis)
{
    toolAxis = axis;

    update();
    updateDecoration();
}

void EditTool::addNumericInput(char c)
{
    numericInputText += c;
    numericInput = true;

    update();
}

void EditTool::removeNumericInput()
{
    if (!numericInputText.empty())
    {
        numericInputText = numericInputText.substr(0, numericInputText.size() - 1);

        if (numericInputText.empty())
        {
            numericInput = false;
        }

        update();
    }
}

void EditTool::toggleNumericInputSign()
{
    if (!numericInputText.empty() && numericInputText[0] == '-')
    {
        numericInputText = numericInputText.substr(1, numericInputText.size() - 1);
    }
    else
    {
        numericInputText = '-' + numericInputText;
    }

    update();
}

void EditTool::ensureNumericInputPeriod()
{
    if (numericInputText.find('.') == string::npos)
    {
        numericInputText += '.';

        update();
    }
}

void EditTool::updateDecoration()
{
    auto render = dynamic_cast<FlatRender*>(ctx->context()->controller()->render());

    switch (toolAxis)
    {
    case Edit_Tool_Axis_None :
        render->setDecorationShown(axisID, false);
        break;
    case Edit_Tool_Axis_Global_X :
    case Edit_Tool_Axis_Local_X :
    case Edit_Tool_Axis_Self_X :
    case Edit_Tool_Axis_Global_Y :
    case Edit_Tool_Axis_Local_Y :
    case Edit_Tool_Axis_Self_Y :
    case Edit_Tool_Axis_Global_Z :
    case Edit_Tool_Axis_Local_Z :
    case Edit_Tool_Axis_Self_Z :
        render->updateDecoration<RenderAxis>(
            axisID,
            globalPivot(),
            *globalAxis(),
            axisColor()
        );
        render->setDecorationShown(axisID, true);
        break;
    }
}

mat4 EditTool::initialGlobal(size_t i) const
{
    return transformingSubpart()
        ? initialGlobalTransforms.at(i)
        : initialGlobalTransformsWithPromotion.at(i);
}

vec3 EditTool::globalPivot() const
{
    switch (toolPivot)
    {
    default :
        fatal("Encountered unknown edit tool pivot '" + to_string(toolPivot) + "'.");
    case Edit_Tool_Pivot_Mean_Point :
    case Edit_Tool_Pivot_Individual_Origins :
        return initialGlobalCenter;
    case Edit_Tool_Pivot_Cursor :
        return ctx->cursorPosition();
    }
}

vec3 EditTool::globalPivot(size_t index) const
{
    switch (toolPivot)
    {
    default :
        fatal("Encountered unknown edit tool pivot '" + to_string(toolPivot) + "'.");
    case Edit_Tool_Pivot_Mean_Point :
        return initialGlobalCenter;
    case Edit_Tool_Pivot_Cursor :
        return ctx->cursorPosition();
    case Edit_Tool_Pivot_Individual_Origins :
        return initialGlobalTransforms.at(index).position();
    }
}

f64 EditTool::numericInputValue() const
{
    try
    {
        return stod(numericInputText);
    }
    catch (const exception& e)
    {
        return 0;
    }
}

Point EditTool::getOnScreenCenter() const
{
    optional<Point> oCenter = ctx->context()->flatViewer()->worldToScreen(globalPivot());
    Point defaultCenter(
        dynamic_cast<FlatRender*>(ctx->context()->controller()->render())->width() / 2,
        dynamic_cast<FlatRender*>(ctx->context()->controller()->render())->height() / 2
    );

    return oCenter ? *oCenter : defaultCenter;
}
