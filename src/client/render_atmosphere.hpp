/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_swapchain_elements.hpp"
#include "render_atmosphere_component.hpp"
#include "atmosphere_parameters.hpp"

class RenderAtmosphere
{
public:
    RenderAtmosphere(
        const RenderInterface* render,
        VkDescriptorSetLayout descriptorSetLayout,
        VkPipelineLayout lightningPipelineLayout,
        VkPipeline lightningPipeline,
        VkPipelineLayout linePipelineLayout,
        VkPipeline linePipeline,
        VkPipelineLayout solidPipelineLayout,
        VkPipeline solidPipeline,
        VkPipelineLayout cloudPipelineLayout,
        VkPipeline cloudPipeline,
        VkPipelineLayout streamerPipelineLayout,
        VkPipeline streamerPipeline,
        VkPipelineLayout warpPipelineLayout,
        VkPipeline warpPipeline
    );
    RenderAtmosphere(const RenderAtmosphere& rhs) = delete;
    RenderAtmosphere(RenderAtmosphere&& rhs) = delete;
    ~RenderAtmosphere();

    RenderAtmosphere& operator=(const RenderAtmosphere& rhs) = delete;
    RenderAtmosphere& operator=(RenderAtmosphere&& rhs) = delete;

protected:
    const RenderInterface* render;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout lightningPipelineLayout;
    VkPipeline lightningPipeline;
    VkPipelineLayout linePipelineLayout;
    VkPipeline linePipeline;
    VkPipelineLayout solidPipelineLayout;
    VkPipeline solidPipeline;
    VkPipelineLayout cloudPipelineLayout;
    VkPipeline cloudPipeline;
    VkPipelineLayout streamerPipelineLayout;
    VkPipeline streamerPipeline;
    VkPipelineLayout warpPipelineLayout;
    VkPipeline warpPipeline;

    mutable i64 lastComponentIndex;
    mt19937_64 engine;

    chrono::milliseconds lastLightningGenerateTime;
    u32 lightningCount;
    vector<AtmosphereLightningParticleGPU> lightningParticlesStaging;
    vector<chrono::milliseconds> lightningParticlesStartTimes;
    VmaBuffer lightningVertices;
    VmaMapping<fvec4>* lightningVerticesMapping;

    vector<RenderAtmosphereComponent*> components;

    optional<AtmosphereParameters> lastAtmosphere;

    virtual void createComponents() = 0;
    void destroyComponents(); 

    void update(
        const MainSwapchainElement* swapchainElement,
        const AtmosphereParameters& atmosphere,
        vec3 cameraPosition[eyeCount],
        const vec3& up,
        const vec3& gravity,
        const vec3& flowVelocity,
        f64 density
    );
    void fillCommandBuffer(
        const MainSwapchainElement* swapchainElement,
        const AtmosphereParameters& atmosphere,
        const vec3& gravity,
        f64 density
    );

    void updateLightningVertices(const AtmosphereParameters& atmosphere);
    void updateLightning(
        RenderAtmosphereComponent* component,
        const AtmosphereParameters& atmosphere,
        const fmat4& worldTransform
    );
    void addLightning(
        const AtmosphereParameters& atmosphere,
        const fmat4& worldTransform,
        uniform_real_distribution<f64>& distribution
    );
    void removeLightning();
    void fillUniformBuffer(
        RenderAtmosphereComponent* component,
        const AtmosphereParameters& atmosphere,
        vec3 cameraPosition[eyeCount],
        const fmat4& worldTransform,
        const vec3& worldGravity,
        const vec3& worldFlowVelocity,
        f64 worldDensity
    ) const;

    mat4 cameraLocalTransform(const vec3& cameraPosition, const vec3& effectPosition) const;

    VmaBuffer createStorageBuffer(size_t size) const;
};
