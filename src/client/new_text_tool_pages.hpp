/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "text.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class LineEdit;
class UIntEdit;
class UFloatEdit;
class Checkbox;

class TextTool : public InterfaceWidget, public TabContent
{
public:
    TextTool(InterfaceWidget* parent);

    void open() override;
    void close() override;

protected:
    void send(const TextRequest& request);
    virtual void reset();

    set<string> users;

    void onManageUsers();

    vector<string> usersSource();
    void onAddUser(const string& name);
    void onRemoveUser(const string& name);

    set<string> options;

    void onManageOptions();

    vector<string> optionsSource();
    void onAddOption(const string& name);
    void onRemoveOption(const string& name);
};

class TextSignalTool : public TextTool
{
public:
    TextSignalTool(InterfaceWidget* parent);

private:
    void reset() override;

    Checkbox* state;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextTitlecardTool : public TextTool
{
public:
    TextTitlecardTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    LineEdit* subtitle;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextTimeoutTool : public TextTool
{
public:
    TextTimeoutTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    LineEdit* text;
    UFloatEdit* timeout;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextUnaryTool : public TextTool
{
public:
    TextUnaryTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    LineEdit* text;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextBinaryTool : public TextTool
{
public:
    TextBinaryTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    LineEdit* text;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextOptionsTool : public TextTool
{
public:
    TextOptionsTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    LineEdit* text;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextChooseTool : public TextTool
{
public:
    TextChooseTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    UIntEdit* count;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextAssignValuesTool : public TextTool
{
public:
    TextAssignValuesTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;

    void onDone();

    set<string> keys;

    void onManageKeys();

    vector<string> keysSource();
    void onAddKey(const string& name);
    void onRemoveKey(const string& name);

    set<string> values;

    void onManageValues();

    vector<string> valuesSource();
    void onAddValue(const string& name);
    void onRemoveValue(const string& name);

    SizeProperties sizeProperties() const override;
};

class TextSpendPointsTool : public TextTool
{
public:
    TextSpendPointsTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    UIntEdit* count;

    void onDone();

    map<string, u64> prices;

    void onManagePrices();

    SizeProperties sizeProperties() const override;
};

class TextAssignPointsTool : public TextTool
{
public:
    TextAssignPointsTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    UIntEdit* count;

    void onDone();

    set<string> keys;

    void onManageKeys();

    vector<string> keysSource();
    void onAddKey(const string& name);
    void onRemoveKey(const string& name);

    SizeProperties sizeProperties() const override;
};

class TextVoteTool : public TextTool
{
public:
    TextVoteTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    UIntEdit* count;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextChooseMajorTool : public TextTool
{
public:
    TextChooseMajorTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;

    void onDone();

    vector<tuple<string, string, string>> options;

    void onManageOptions();

    SizeProperties sizeProperties() const override;
};

class TextKnowledgeTool : public TextTool
{
public:
    TextKnowledgeTool(InterfaceWidget* parent);

private:
    void reset() override;

    LineEdit* title;
    LineEdit* subtitle;
    LineEdit* text;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class TextClearTool : public TextTool
{
public:
    TextClearTool(InterfaceWidget* parent);

private:
    void reset() override;

    Checkbox* knowledge;

    void onDone();

    SizeProperties sizeProperties() const override;
};
