/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "internal_mode_context.hpp"
#include "body_part.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "interface_constants.hpp"
#include "tools.hpp"
#include "flat_render.hpp"
#include "render_marker.hpp"
#include "render_pivot.hpp"
#include "render_light_marker.hpp"
#include "render_force_marker.hpp"
#include "render_terrain_graph.hpp"
#include "render_area_bounds.hpp"

static mat4 applySubpartTransformToPart(const vec3& pivot, const quaternion& transformerRotation, mat4 transformer, mat4 target)
{
    mat4 transformerSpace(pivot, transformerRotation, vec3(1));

    target = transformerSpace.applyToTransform(target, false);

    target = transformer.applyToTransform(target);

    target = transformerSpace.applyToTransform(target, true);

    return target;
}

InternalModeContext::InternalModeContext(ViewerModeContext* ctx)
    : EditModeContext(ctx)
{

}

InternalModeContext::~InternalModeContext()
{
    for (const auto& [ partID, decorationID ] : constraintDecorations)
    {
        ctx->context()->controller()->render()->removeDecoration(decorationID);
    }

    for (const auto& [ partID, decorationID ] : decorations)
    {
        ctx->context()->controller()->render()->removeDecoration(decorationID);
    }
}

void InternalModeContext::step()
{
    EditModeContext::step();

    Entity* entity = activeEntity();

    if (!entity)
    {
        for (const auto& [ partID, decorationID ] : decorations)
        {
            ctx->context()->controller()->render()->removeDecoration(decorationID);
        }
        decorations.clear();

        for (const auto& [ partID, decorationID ] : constraintDecorations)
        {
            ctx->context()->controller()->render()->removeDecoration(decorationID);
        }
        constraintDecorations.clear();

        return;
    }

    auto render = dynamic_cast<FlatRender*>(ctx->context()->controller()->render());

    activeEntityID = entity->id();
    render->update(entity->parent() ? entity->parent()->id() : EntityID(), entity);

    map<BodyPartID, BodyPart*> parts;

    entity->body().traverse([&](BodyPart* part) -> void {
        parts.insert({ part->id(), part });
    });

    for (auto it = decorations.begin(); it != decorations.end();)
    {
        if (!parts.contains(it->first) || _hiddenBodyPartIDs.contains(it->first))
        {
            render->removeDecoration(it->second);
            decorations.erase(it++);
        }
        else
        {
            it++;
        }
    }

    for (auto it = constraintDecorations.begin(); it != constraintDecorations.end();)
    {
        if (!parts.contains(it->first) || _hiddenBodyPartIDs.contains(it->first))
        {
            render->removeDecoration(it->second);
            constraintDecorations.erase(it++);
        }
        else
        {
            it++;
        }
    }

    for (const auto& [ partID, part ] : parts)
    {
        if (_hiddenBodyPartIDs.contains(partID))
        {
            continue;
        }

        auto it = decorations.find(partID);

        mat4 transform = entity->globalTransform().applyToTransform(part->regionalTransform());

        switch (part->type())
        {
        case Body_Part_Line :
        case Body_Part_Plane :
        case Body_Part_Solid :
        case Body_Part_Text :
        case Body_Part_Symbol :
        case Body_Part_Canvas :
        case Body_Part_Structure :
        case Body_Part_Rope :
            break;
        case Body_Part_Terrain :
            if (it != decorations.end())
            {
                render->updateDecoration<RenderTerrainGraph>(
                    it->second,
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartTerrain>().nodes
                );
            }
            else
            {
                decorations.insert({ partID, render->addDecoration<RenderTerrainGraph>(
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartTerrain>().nodes
                ) });
            }
            break;
        case Body_Part_Anchor :
            if (it != decorations.end())
            {
                render->updateDecoration<RenderMarker>(
                    it->second,
                    transform.position(),
                    transform.rotation()
                );
            }
            else
            {
                decorations.insert({ partID, render->addDecoration<RenderMarker>(
                    transform.position(),
                    transform.rotation()
                ) });
            }
            break;
        case Body_Part_Light :
            if (it != decorations.end())
            {
                render->updateDecoration<RenderLightMarker>(
                    it->second,
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartLight>().angle,
                    part->get<BodyPartLight>().color
                );
            }
            else
            {
                decorations.insert({ partID, render->addDecoration<RenderLightMarker>(
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartLight>().angle,
                    part->get<BodyPartLight>().color
                ) });
            }
            break;
        case Body_Part_Force :
        {
            bool invertedForce = part->get<BodyPartForce>().type == Body_Force_Gravity
                || part->get<BodyPartForce>().type == Body_Force_Magnetism;

            // Default force direction inverted for gravity
            if (it != decorations.end())
            {
                render->updateDecoration<RenderForceMarker>(
                    it->second,
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartForce>().type,
                    part->get<BodyPartForce>().radius,
                    invertedForce ^ (part->get<BodyPartForce>().intensity < 0)
                );
            }
            else
            {
                decorations.insert({ partID, render->addDecoration<RenderForceMarker>(
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartForce>().type,
                    part->get<BodyPartForce>().radius,
                    invertedForce ^ (part->get<BodyPartForce>().intensity < 0)
                ) });
            }
            break;
        }
        case Body_Part_Area :
            if (it != decorations.end())
            {
                render->updateDecoration<RenderAreaBounds>(
                    it->second,
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartArea>().points,
                    part->get<BodyPartArea>().height
                );
            }
            else
            {
                decorations.insert({ partID, render->addDecoration<RenderAreaBounds>(
                    transform.position(),
                    transform.rotation(),
                    part->get<BodyPartArea>().points,
                    part->get<BodyPartArea>().height
                ) });
            }
            break;
        }
    }

    for (const auto& [ partID, part ] : parts)
    {
        if (_hiddenBodyPartIDs.contains(partID))
        {
            continue;
        }

        auto it = constraintDecorations.find(partID);

        mat4 transform = entity->globalTransform().applyToTransform(part->constraintTransform());

        vec3 min;
        vec3 max;

        switch (part->constraintType())
        {
        case Body_Constraint_Fixed :
            continue;
        case Body_Constraint_Ball :
        {
            const BodyConstraintBall& ball = part->getConstraint<BodyConstraintBall>();

            min = ball.minimum;
            max = ball.maximum;
            break;
        }
        case Body_Constraint_Cone :
        {
            const BodyConstraintCone& cone = part->getConstraint<BodyConstraintCone>();

            min = { cone.span.x / -2, cone.twistMinimum, cone.span.y / -2 };
            max = { cone.span.x / +2, cone.twistMaximum, cone.span.y / +2 };
            break;
        }
        case Body_Constraint_Hinge :
        {
            const BodyConstraintHinge& hinge = part->getConstraint<BodyConstraintHinge>();

            min = { hinge.minimum, 0, 0 };
            max = { hinge.maximum, 0, 0 };
            break;
        }
        }

        if (it != constraintDecorations.end())
        {
            render->updateDecoration<RenderPivot>(
                it->second,
                transform.position(),
                transform.rotation(),
                min,
                max
            );
        }
        else
        {
            constraintDecorations.insert({ partID, render->addDecoration<RenderPivot>(
                transform.position(),
                transform.rotation(),
                min,
                max
            ) });
        }
    }
}

void InternalModeContext::enter()
{
    EditModeContext::enter();

    Entity* entity = activeEntity();

    if (entity && entity->id() != activeEntityID)
    {
        _selectionIDs.clear();
    }
}

void InternalModeContext::exit()
{
    EditModeContext::exit();

    _selectionIDs.clear();

    auto render = dynamic_cast<FlatRender*>(ctx->context()->controller()->render());

    // Update body part hiding
    if (Entity* entity = ctx->context()->controller()->game().get(activeEntityID); entity)
    {
        render->update(entity->parent() ? entity->parent()->id() : EntityID(), entity);
    }

    for (const auto& [ partID, decorationID ] : decorations)
    {
        render->removeDecoration(decorationID);
    }
    decorations.clear();

    for (const auto& [ partID, decorationID ] : constraintDecorations)
    {
        render->removeDecoration(decorationID);
    }
    constraintDecorations.clear();
}

void InternalModeContext::cancelTool()
{
    EditModeContext::cancelTool();

    initialPartTransforms.clear();
}

void InternalModeContext::endTool()
{
    EditModeContext::endTool();

    initialPartTransforms.clear();
}

const set<BodyPartSelectionID>& InternalModeContext::selectionIDs() const
{
    return _selectionIDs;
}

set<BodyPartSelectionID> InternalModeContext::unselectionIDs() const
{
    set<BodyPartSelectionID> ids;

    Entity* entity = activeEntity();

    if (!entity)
    {
        return ids;
    }

    set<BodyPartID> selectedPartIDs;

    for (const BodyPartSelectionID& fullID : _selectionIDs)
    {
        selectedPartIDs.insert(fullID.id);
    }

    entity->body().traverse([&](const BodyPart* part) -> void {
        if (selectedPartIDs.contains(part->id()))
        {
            return;
        }

        ids.insert(BodyPartSelectionID(part->id(), BodyPartSubID(0)));
    });

    return ids;
}

set<BodyPartSelection> InternalModeContext::selection() const
{
    set<BodyPartSelection> selection;

    Entity* entity = activeEntity();

    if (!entity)
    {
        return selection;
    }

    for (const BodyPartSelectionID& selectionID : selectionIDs())
    {
        BodyPart* part = entity->body().get(selectionID.id);

        if (!part)
        {
            continue;
        }

        selection.insert({ part, selectionID.subID });
    }

    return selection;
}

set<BodyPartSelection> InternalModeContext::selectionRootsOnly() const
{
    set<BodyPartSelection> selection = this->selection();

    for (const BodyPartSelection& selected : this->selection())
    {
        bool found = false;

        for (const BodyPartSelection& otherSelected : selection)
        {
            if (selected.part->hasParent(otherSelected.part))
            {
                found = true;
                break;
            }
        }

        if (found)
        {
            selection.erase(selected);
        }
    }

    return selection;
}

BodyPartSelection InternalModeContext::selectionRootAtIndex(size_t index) const
{
    size_t i = 0;
    for (const BodyPartSelection& selection : selectionRootsOnly())
    {
        if (i != index)
        {
            i++;
            continue;
        }

        return selection;
    }

    fatal("Body root selection index out of range: " + to_string(index) + " > " + to_string(selectionRootsOnly().size()) + ".");
}

vec3 InternalModeContext::selectionCenter() const
{
    vec3 center;

    set<BodyPartSelection> selection = selectionRootsOnly();

    if (selection.empty())
    {
        return center;
    }

    for (const BodyPartSelection& selected : selection)
    {
        center += selected.subID == BodyPartSubID()
            ? selected.part->regionalPosition()
            : selected.part->subparts().at(selected.subID.value() - 1);
    }

    center /= selection.size();

    return regionalToGlobal(center);
}

void InternalModeContext::select(const BodyPartSelectionID& selection)
{
    for (auto it = _selectionIDs.begin(); it != _selectionIDs.end();)
    {
        if (it->id == selection.id)
        {
            _selectionIDs.erase(it++);
        }
        else
        {
            it++;
        }
    }

    _selectionIDs.insert(selection);
}

void InternalModeContext::select(const vector<BodyPartSelectionID>& selection)
{
    for (const BodyPartSelectionID& item : selection)
    {
        select(item);
    }
}

void InternalModeContext::selectAll()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    entity->body().traverse([&](const BodyPart* part) -> void {
        if (_hiddenBodyPartIDs.contains(part->id()))
        {
            return;
        }

        select({ part->id(), BodyPartSubID() });
    });
}

void InternalModeContext::deselect(const BodyPartSelectionID& selection)
{
    _selectionIDs.erase(selection);
}

void InternalModeContext::deselect(const vector<BodyPartSelectionID>& selection)
{
    for (const BodyPartSelectionID& item : selection)
    {
        deselect(item);
    }
}

void InternalModeContext::deselectAll()
{
    _selectionIDs.clear();
}

bool InternalModeContext::hasSelection() const
{
    return !_selectionIDs.empty();
}

void InternalModeContext::invertSelection()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    _selectionIDs = unselectionIDs();
}

void InternalModeContext::selectLinked()
{
    set<BodyPartSelectionID> selectionIDs = _selectionIDs;

    for (const BodyPartSelectionID& selectionID : selectionIDs)
    {
        if (_hiddenBodyPartIDs.contains(selectionID.id))
        {
            continue;
        }

        select({ selectionID.id, BodyPartSubID() });
    }
}

void InternalModeContext::translate(const Point& point, bool soft)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    if (selectionRootsOnly().empty())
    {
        return;
    }

    for (size_t i = 0; i < transformCount(); i++)
    {
        initialPartTransforms.push_back(transformGetWithPromotion(i));
    }

    EditModeContext::translate(point, soft);
}

void InternalModeContext::rotate(const Point& point, bool soft)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    if (selectionRootsOnly().empty())
    {
        return;
    }

    for (size_t i = 0; i < transformCount(); i++)
    {
        initialPartTransforms.push_back(transformGetWithPromotion(i));
    }

    EditModeContext::rotate(point, soft);
}

void InternalModeContext::scale(const Point& point, bool soft)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    if (selectionRootsOnly().empty())
    {
        return;
    }

    for (size_t i = 0; i < transformCount(); i++)
    {
        initialPartTransforms.push_back(transformGetWithPromotion(i));
    }

    EditModeContext::scale(point, soft);
}

vector<BodyPartSelection> InternalModeContext::bodyPartsUnderPoint(const Point& point) const
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return {};
    }

    vector<BodyPartSelection> bodyParts;

    entity->body().traverse([&](BodyPart* part) -> void {
        if (_hiddenBodyPartIDs.contains(part->id()))
        {
            return;
        }

        vector<vec3> controlPoints = ::join({ part->regionalPosition() }, part->subparts());

        for (u32 i = 0; i < controlPoints.size(); i++)
        {
            const vec3& controlPoint = controlPoints[i];

            optional<Point> screenPoint = ctx->context()->flatViewer()->worldToScreen(
                entity->globalTransform().applyToPosition(controlPoint)
            );

            if (!screenPoint)
            {
                continue;
            }

            Point start = *screenPoint - (UIScale::controlPointSize() / 2);
            Point end = *screenPoint + (UIScale::controlPointSize() / 2);

            if (start.x > point.x ||
                start.y > point.y ||
                end.x <= point.x ||
                end.y <= point.y
            )
            {
                continue;
            }

            bodyParts.push_back({ part, BodyPartSubID(i) });
        }
    });

    sort(
        bodyParts.begin(),
        bodyParts.end(),
        [this, entity](const BodyPartSelection& a, const BodyPartSelection& b) -> bool
        {
            vec3 aPosition = entity->globalTransform().applyToPosition(a.part->regionalPosition());
            vec3 bPosition = entity->globalTransform().applyToPosition(b.part->regionalPosition());

            return aPosition.distance(ctx->context()->flatViewer()->position()) < bPosition.distance(ctx->context()->flatViewer()->position());
        }
    );

    // Rotates the list so when single-selecting the selection will rotate through all possibilities
    for (size_t i = 0; i < bodyParts.size(); i++)
    {
        if (selection().contains(bodyParts.at(i)))
        {
            vector<BodyPartSelection> before = { bodyParts.begin(), bodyParts.begin() + i + 1 };
            vector<BodyPartSelection> after = { bodyParts.begin() + i + 1, bodyParts.end() };
            bodyParts = ::join(after, before);
            break;
        }
    }

    return bodyParts;
}

vector<BodyPartSelection> InternalModeContext::bodyPartsInRegion(const Point& start, const Point& end) const
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return {};
    }

    vector<BodyPartSelection> bodyParts;

    entity->body().traverse([&](BodyPart* part) -> void {
        if (_hiddenBodyPartIDs.contains(part->id()))
        {
            return;
        }

        vector<vec3> controlPoints = ::join({ part->regionalPosition() }, part->subparts());

        for (u32 i = 0; i < controlPoints.size(); i++)
        {
            const vec3& controlPoint = controlPoints[i];

            optional<Point> screenPoint = ctx->context()->flatViewer()->worldToScreen(
                entity->globalTransform().applyToPosition(controlPoint)
            );

            if (!screenPoint)
            {
                continue;
            }

            if (start.x > screenPoint->x ||
                start.y > screenPoint->y ||
                end.x <= screenPoint->x ||
                end.y <= screenPoint->y
            )
            {
                continue;
            }

            bodyParts.push_back({ part, BodyPartSubID(i) });
            break;
        }
    });

    return bodyParts;
}

Entity* InternalModeContext::activeEntity() const
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return nullptr;
    }

    return ctx->context()->controller()->game().get(editor->id());
}

bool InternalModeContext::gizmoShown() const
{
    return ctx->gizmoShown() && ctx->context()->controller()->ready() && !selection().empty();
}

bool InternalModeContext::gizmoSelectable(const Point& point) const
{
    return bodyPartsUnderPoint(point).empty();
}

void InternalModeContext::makeSelections(const Point& point, SelectMode mode)
{
    vector<BodyPartSelection> parts = bodyPartsUnderPoint(point);
    vector<BodyPartSelectionID> selections(parts.size());

    transform(
        parts.begin(),
        parts.end(),
        selections.begin(),
        [](const BodyPartSelection& x) -> BodyPartSelectionID { return { x.part->id(), x.subID }; }
    );

    switch (mode)
    {
    case Select_Replace :
        deselectAll();

        if (!parts.empty())
        {
            select(selections.front());
        }
        break;
    case Select_Add :
        if (!parts.empty())
        {
            select(selections.front());
        }
        break;
    case Select_Remove :
        if (!parts.empty())
        {
            deselect(selections.front());
        }
        break;
    }
}

void InternalModeContext::makeSelections(const Point& start, const Point& end, SelectMode mode)
{
    vector<BodyPartSelection> parts = bodyPartsInRegion(start, end);
    vector<BodyPartSelectionID> selections(parts.size());

    transform(
        parts.begin(),
        parts.end(),
        selections.begin(),
        [](const BodyPartSelection& x) -> BodyPartSelectionID { return { x.part->id(), x.subID }; }
    );

    switch (mode)
    {
    case Select_Replace :
        deselectAll();

        if (!parts.empty())
        {
            select(selections);
        }
        break;
    case Select_Add :
        if (!parts.empty())
        {
            select(selections);
        }
        break;
    case Select_Remove :
        if (!parts.empty())
        {
            deselect(selections);
        }
        break;
    }
}

size_t InternalModeContext::transformCount() const
{
    return selectionRootsOnly().size();
}

mat4 InternalModeContext::transformToolSpace() const
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return mat4();
    }

    return entity->globalTransform();
}

vec3 InternalModeContext::regionalToGlobal(const vec3& position) const
{
    Entity* entity = activeEntity();

    return entity->globalTransform().applyToPosition(position);
}

vec3 InternalModeContext::globalToRegional(const vec3& position) const
{
    Entity* entity = activeEntity();

    return entity->globalTransform().applyToPosition(position, false);
}

void InternalModeContext::applyTransformToPart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    if (selection.subID != BodyPartSubID() && (!_tool || _tool->transformingSubpart()))
    {
        switch (selection.part->type())
        {
        case Body_Part_Line :
            applyTransformToLineSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Plane :
            applyTransformToPlaneSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Solid :
            applyTransformToSolidSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Symbol :
            applyTransformToSymbolSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Canvas :
            applyTransformToCanvasSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Text :
            applyTransformToTextSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Terrain :
            applyTransformToTerrainSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Area :
            applyTransformToAreaSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        case Body_Part_Rope :
            applyTransformToRopeSubpart(
                selection,
                pivot,
                initialPartTransform,
                initialSubpartTransform,
                currentSubpartTransform
            );
            break;
        default :
            break;
        }

        return;
    }

    selection.part->setRegionalTransform(currentSubpartTransform);
}

void InternalModeContext::applyTransformToLineSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 initialSubpartPosition = initialSubpartTransform.position();
    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 initialOffset = initialSubpartPosition - initialPartTransform.position();
    vec3 currentOffset = currentSubpartPosition - initialPartTransform.position();

    vec3 initialOtherSubpartPosition = initialPartTransform.applyToPosition(initialPartTransform.applyToPosition(initialSubpartTransform.position(), false) * vec3(1, -1, 1));
    vec3 currentOtherSubpartPosition = currentSubpartPosition + rotationDelta.rotate(scaleDelta * (initialOtherSubpartPosition - currentSubpartPosition));

    vec3 initialDir = (initialSubpartPosition - initialOtherSubpartPosition).normalize();
    // second term here is to prevent the direction from flipping under some circumstances
    vec3 currentDir = (currentSubpartPosition - currentOtherSubpartPosition).normalize() * initialDir.dot(initialSubpartTransform.rotation().forward());

    vec3 up;

    if (roughly(abs(currentDir.dot(initialDir)), 1.0))
    {
        up = initialSubpartTransform.rotation().up();
    }
    else
    {
        up = currentDir.cross(initialDir).normalize();
    }

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(currentOffset - initialOffset, rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartLine>().length = currentSubpartPosition.distance(currentOtherSubpartPosition);
}

void InternalModeContext::applyTransformToPlaneSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 initialSubpartPosition = initialSubpartTransform.position();
    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 initialOffset = initialSubpartPosition - initialPartTransform.position();
    vec3 currentOffset = currentSubpartPosition - initialPartTransform.position();

    f64 width = selection.part->get<BodyPartPlane>().width;
    f64 height = selection.part->get<BodyPartPlane>().height;

    switch (selection.part->get<BodyPartPlane>().type)
    {
    case Body_Plane_Ellipse :
    case Body_Plane_Isosceles_Triangle :
    case Body_Plane_Right_Angle_Triangle :
    case Body_Plane_Rectangle :
    {
        vec3 initialLocal = initialPartTransform.applyToPosition(initialSubpartPosition, false);
        vec3 currentLocal = initialPartTransform.applyToPosition(currentSubpartPosition, false);

        if (!roughly(initialLocal.x, 0.0))
        {
            width = abs(currentLocal.x) * 2;
        }

        if (!roughly(initialLocal.y, 0.0))
        {
            height = abs(currentLocal.y) * 2;
        }
        break;
    }
    case Body_Plane_Equilateral_Triangle :
    case Body_Plane_Pentagon :
    case Body_Plane_Hexagon :
    case Body_Plane_Heptagon :
    case Body_Plane_Octagon :
        width = currentOffset.length() * 2;
        break;
    }

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(currentOffset - initialOffset, rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartPlane>().width = width;
    selection.part->get<BodyPartPlane>().height = height;
}

void InternalModeContext::applyTransformToSolidSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 initialSubpartPosition = initialSubpartTransform.position();
    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 initialOffset = initialSubpartPosition - initialPartTransform.position();
    vec3 currentOffset = currentSubpartPosition - initialPartTransform.position();

    vec3 size = selection.part->get<BodyPartSolid>().size;

    vec3 initialLocal = initialPartTransform.applyToPosition(initialSubpartPosition, false);
    vec3 currentLocal = initialPartTransform.applyToPosition(currentSubpartPosition, false);

    switch (selection.part->get<BodyPartSolid>().type)
    {
    case Body_Solid_Cuboid :
        size.x = abs(currentLocal.x) * 2;
        size.y = abs(currentLocal.y) * 2;
        size.z = abs(currentLocal.z) * 2;
        break;
    case Body_Solid_Sphere :
        size.x = currentOffset.length() * 2;
        break;
    case Body_Solid_Cylinder :
        switch (selection.subID.value())
        {
        case 0 :
        case 1 :
            size.z = abs(currentLocal.z) * 2;
            break;
        default :
            if (!roughly(initialLocal.x, 0.0))
            {
                size.x = abs(currentLocal.x) * 2;
            }

            if (!roughly(initialLocal.y, 0.0))
            {
                size.y = abs(currentLocal.y) * 2;
            }

            size.z = abs(currentLocal.z) * 2;
            break;
        }
        break;
    case Body_Solid_Cone :
        switch (selection.subID.value())
        {
        case 0 :
            size.z = abs(currentLocal.z);
            break;
        default :
            size.x = currentLocal.toVec2().length() * 2;
            break;
        }
        break;
    case Body_Solid_Capsule :
        switch (selection.subID.value())
        {
        case 0 :
        case 1 :
            size.z = abs(currentLocal.z) * 2;
            break;
        case 2 :
        case 3 :
            size.z = abs(currentLocal.z) * 2 - size.x;
            break;
        default :
            size.x = currentLocal.toVec2().length() * 2;
            size.z = abs(currentLocal.z) * 2;
            break;
        }
        break;
    case Body_Solid_Pyramid :
        switch (selection.subID.value())
        {
        case 0 :
            size.z = abs(currentLocal.z) * 2;
            break;
        default :
            size.x = max(abs(currentLocal.x), abs(currentLocal.y)) * 2;
            break;
        }
        break;
    case Body_Solid_Tetrahedron :
    case Body_Solid_Octahedron :
    case Body_Solid_Dodecahedron :
    case Body_Solid_Icosahedron :
        size.x = currentOffset.length() * 2;
        break;
    }

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(currentOffset - initialOffset, rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartSolid>().size = size;
}

void InternalModeContext::applyTransformToSymbolSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 initialSubpartPosition = initialSubpartTransform.position();
    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 initialOffset = initialSubpartPosition - initialPartTransform.position();
    vec3 currentOffset = currentSubpartPosition - initialPartTransform.position();

    f64 size = sqrt(sq(currentOffset.length()) / 2);

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(currentOffset - initialOffset, rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartSymbol>().size = size;
}

void InternalModeContext::applyTransformToCanvasSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 initialSubpartPosition = initialSubpartTransform.position();
    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 initialOffset = initialSubpartPosition - initialPartTransform.position();
    vec3 currentOffset = currentSubpartPosition - initialPartTransform.position();

    f64 size = sqrt(sq(currentOffset.length()) / 2);

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(currentOffset - initialOffset, rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartCanvas>().size = size;
}

void InternalModeContext::applyTransformToTextSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 initialSubpartPosition = initialSubpartTransform.position();
    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 initialOffset = initialSubpartPosition - initialPartTransform.position();
    vec3 currentOffset = currentSubpartPosition - initialPartTransform.position();

    vec3 initialLocal = initialPartTransform.applyToPosition(initialSubpartPosition, false);
    vec3 currentLocal = initialPartTransform.applyToPosition(currentSubpartPosition, false);

    if (!roughly(initialOffset.y, 0.0))
    {
        currentLocal.x = (currentLocal.y / initialLocal.y) * initialOffset.x;
    }

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(currentOffset - initialOffset, rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartText>().height = abs(currentOffset.y * 2);
}

void InternalModeContext::applyTransformToTerrainSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 currentLocal = initialPartTransform.applyToPosition(currentSubpartPosition, false);

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(vec3(), rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartTerrain>().nodes.at(selection.subID.value() - 1).position = currentLocal;
}

void InternalModeContext::applyTransformToAreaSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 currentLocal = initialPartTransform.applyToPosition(currentSubpartPosition, false);

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(vec3(), rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartArea>().height = abs(currentLocal.z * 2);
    selection.part->get<BodyPartArea>().points.at((selection.subID.value() - 1) / 2) = currentLocal.toVec2();
}

void InternalModeContext::applyTransformToRopeSubpart(
    const BodyPartSelection& selection,
    const vec3& pivot,
    const mat4& initialPartTransform,
    const mat4& initialSubpartTransform,
    const mat4& currentSubpartTransform
) const
{
    quaternion rotationDelta = currentSubpartTransform.rotation() / initialSubpartTransform.rotation();
    vec3 scaleDelta = currentSubpartTransform.scale() / initialSubpartTransform.scale();

    vec3 currentSubpartPosition = currentSubpartTransform.position();

    vec3 currentLocal = initialPartTransform.applyToPosition(currentSubpartPosition, false);

    mat4 finalPartTransform = applySubpartTransformToPart(
        pivot,
        initialPartTransform.rotation(),
        mat4(vec3(), rotationDelta, scaleDelta),
        initialPartTransform
    );

    selection.part->setRegionalTransform(finalPartTransform);
    selection.part->get<BodyPartRope>().points.at(selection.subID.value() - 1) = currentLocal;
}
