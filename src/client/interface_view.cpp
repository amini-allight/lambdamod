/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_view.hpp"

// Fix for Windows namespace pollution
#undef interface

InterfaceView::InterfaceView(Interface* interface, const string& name)
    : _interface(interface)
    , name(name)
{

}

InterfaceView::~InterfaceView()
{
    vector<InterfaceWidget*> children = _children;

    for (InterfaceWidget* child : children)
    {
        delete child;
    }
}

Interface* InterfaceView::interface() const
{
    return _interface;
}

const vector<InterfaceWidget*>& InterfaceView::children() const
{
    return _children;
}

void InterfaceView::step()
{
    for (InterfaceWidget* child : _children)
    {
        if (!child->shouldDraw())
        {
            continue;
        }

        child->step();
    }
}

void InterfaceView::draw(const DrawContext& ctx)
{
    for (InterfaceWidget* child : _children)
    {
        if (!child->shouldDraw())
        {
            continue;
        }

        child->draw(ctx);

        for (const function<void(const DrawContext&)>& delayedCommand : ctx.delayedCommands)
        {
            delayedCommand(ctx);
        }
        ctx.delayedCommands.clear();
    }
}

void InterfaceView::resize(const Size& size)
{
    for (InterfaceWidget* child : _children)
    {
        Size childSize;

        switch (child->sizeProperties().horizontal)
        {
        case Scaling_Fixed :
            childSize.w = child->sizeProperties().w;
            break;
        case Scaling_Fraction :
            childSize.w = child->sizeProperties().w * size.w;
            break;
        case Scaling_Fill :
            childSize.w = size.w;
            break;
        }

        switch (child->sizeProperties().vertical)
        {
        case Scaling_Fixed :
            childSize.h = child->sizeProperties().h;
            break;
        case Scaling_Fraction :
            childSize.h = child->sizeProperties().h * size.h;
            break;
        case Scaling_Fill :
            childSize.h = size.h;
            break;
        }

        Point childPosition;

        switch (child->positionProperties().horizontal)
        {
        case Positioning_Fixed :
            childPosition.x = child->positionProperties().x;
            break;
        case Positioning_Fraction :
            childPosition.x = child->positionProperties().x * size.w;
            break;
        case Positioning_Fixed_Inverted :
            childPosition.x = size.w - child->positionProperties().x;
            break;
        case Positioning_Fraction_Inverted :
            childPosition.x = size.w - (child->positionProperties().x * size.w);
            break;
        }

        switch (child->positionProperties().vertical)
        {
        case Positioning_Fixed :
            childPosition.y = child->positionProperties().y;
            break;
        case Positioning_Fraction :
            childPosition.y = child->positionProperties().y * size.h;
            break;
        case Positioning_Fixed_Inverted :
            childPosition.y = size.h - child->positionProperties().y;
            break;
        case Positioning_Fraction_Inverted :
            childPosition.y = size.h - (child->positionProperties().y * size.h);
            break;
        }

        child->resize(childSize);
        child->move(childPosition);
    }
}

void InterfaceView::unfocus()
{
    for (InterfaceWidget* child : _children)
    {
        child->clearFocus();
    }
}

void InterfaceView::add(InterfaceWidget* child)
{
    _children.push_back(child);
}

void InterfaceView::remove(InterfaceWidget* child)
{
    _children.erase(find(_children.begin(), _children.end(), child));
}
