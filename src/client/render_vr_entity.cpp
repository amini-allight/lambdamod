/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_entity.hpp"
#include "render_tools.hpp"
#include "tools.hpp"
#include "entity.hpp"
#include "render_generators.hpp"
#include "vr_render.hpp"

RenderVREntity::RenderVREntity(
    RenderInterface* render,

    vector<VRSwapchainElement*> swapchainElements,

    const RenderPipelineStore* pipelineStore,
    const Entity* entity,
    const BodyPartMask& bodyPartMask,
    vector<RenderDeletionTask<RenderVREntity>>* entitiesToDelete
)
    : RenderEntity(
        render,

        pipelineStore,
        entity,
        bodyPartMask,

        render->rayTracingEnabled()
    )
    , swapchainElements(swapchainElements)
    , entitiesToDelete(entitiesToDelete)
{
    createComponents();
}

RenderVREntity::~RenderVREntity()
{
    for (const auto& [ childID, child ] : _children)
    {
        delete child;
    }

    destroyComponents();
}

void RenderVREntity::work(
    const VRSwapchainElement* swapchainElement,
    vec3 cameraPosition[eyeCount],
    deque<RenderTransparentDraw>* transparentDraws,
    const set<EntityID>& hiddenEntityIDs
) const
{
    if (!hiddenEntityIDs.contains(id))
    {
        // Gated internally
        body->setupTextureLayouts(swapchainElement->transferCommandBuffer());

        RenderEntityComponent* component = components.at(swapchainElement->imageIndex());

        // Gated internally
        component->bind(
            body->materials,
            swapchainElements.at(swapchainElement->imageIndex())->worldCamera(),
            swapchainElements.at(swapchainElement->imageIndex())->sky()->imageView,
            swapchainElements.at(swapchainElement->imageIndex())->occlusionScene()
        );

        auto uniform = new EntityUniformGPU();

        for (u32 eye = 0; eye < eyeCount; eye++)
        {
            uniform->model[eye] = cameraLocalTransform(cameraPosition[eye]);
        }

        vector<RenderLight> lights = render->getLights(transform.position());
        uniform->lightCount = lights.size();

        for (size_t i = 0; i < lights.size(); i++)
        {
            uniform->lights[i] = lights.at(i).uniformData(cameraPosition);
        }

        uniform->fogDistance = render->fogDistance();
        uniform->timer = render->timer();

        memcpy(component->objectMapping->data, uniform, sizeof(EntityUniformGPU));

        delete uniform;

        fillCommandBuffer(
            swapchainElement,
            components.at(swapchainElement->imageIndex())->materialDescriptorSets,
            transparentDraws
        );

        RenderEntity::work();
    }

    for (const auto& [ childID, child ] : _children)
    {
        child->work(swapchainElement, cameraPosition, transparentDraws, hiddenEntityIDs);
    }
}

void RenderVREntity::update(const Entity* entity, const BodyPartMask& bodyPartMask)
{
    transform = entity->globalTransform();
    visible = entity->visible();

    if (entity->body() != lastBody)
    {
        auto [ maskedEntityID, hiddenBodyPartIDs ] = bodyPartMask;

        BodyRenderData renderData = entity->body().toRender(id == maskedEntityID ? hiddenBodyPartIDs : set<BodyPartID>());

        if (!body->supports(renderData.materials))
        {
            bodiesToDelete.push_back({ body, 0, components.size() });

            body = new RenderEntityBody(render->context(), pipelineStore, renderData.materials);
        }
        else
        {
            body->fill(renderData.materials);
        }

        if (!components.front()->supports(body->materials))
        {
            for (RenderEntityComponent* component : components)
            {
                componentsToDelete.push_back({ component, 0, components.size() });
            }

            components.clear();

            createComponents();
        }
        else
        {
            for (RenderEntityComponent* component : components)
            {
                component->needsRebind = true;
            }
        }

        lastBody = entity->body();

        setBodySideEffects(renderData, render->rayTracingEnabled());
    }
    else
    {
        updateBodySideEffects(transform);
    }

    for (const auto& [ childID, child ] : _children)
    {
        child->update(entity->children().at(childID), bodyPartMask);
    }
}

void RenderVREntity::refresh(vector<VRSwapchainElement*> swapchainElements)
{
    this->swapchainElements = swapchainElements;

    destroyComponents();

    createComponents();

    for (const auto& [ childID, child ] : _children)
    {
        child->refresh(swapchainElements);
    }
}

void RenderVREntity::add(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask)
{
    if (parentID == id)
    {
        auto it = _children.find(entity->id());

        if (it != _children.end())
        {
            return;
        }

        auto child = new RenderVREntity(
            render,

            swapchainElements,

            pipelineStore,
            entity,
            bodyPartMask,
            entitiesToDelete
        );

        render->addLights(child);

        _children.insert({
            entity->id(),
            child
        });
    }
    else
    {
        for (const auto& [ childID, child ] : _children)
        {
            child->add(parentID, entity, bodyPartMask);
        }
    }
}

void RenderVREntity::remove(EntityID parentID, EntityID id)
{
    if (parentID == this->id)
    {
        auto it = _children.find(id);

        if (it == _children.end())
        {
            return;
        }

        RenderVREntity* child = it->second;

        render->removeLights(child);

        _children.erase(it);
        entitiesToDelete->push_back(RenderDeletionTask(child));
    }
    else
    {
        for (const auto& [ childID, child ] : _children)
        {
            child->remove(parentID, id);
        }
    }
}

void RenderVREntity::update(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask)
{
    if (parentID == id)
    {
        auto [ maskedEntityID, hiddenBodyPartIDs ] = bodyPartMask;

        if (entity->body().empty(maskedEntityID == id ? hiddenBodyPartIDs : set<BodyPartID>()))
        {
            remove(parentID, entity->id());
            return;
        }

        auto it = _children.find(entity->id());

        if (it == _children.end())
        {
            add(parentID, entity, bodyPartMask);
            return;
        }

        RenderVREntity* child = it->second;

        child->update(entity, bodyPartMask);

        render->updateLights(child);
    }
    else
    {
        for (const auto& [ childID, child ] : _children)
        {
            child->update(parentID, entity, bodyPartMask);
        }
    }
}

const map<EntityID, RenderVREntity*>& RenderVREntity::children() const
{
    return _children;
}

void RenderVREntity::createComponents()
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        components.push_back(new RenderEntityComponent(
            render->context(),
            body->materials,
            swapchainElements.at(i)->worldCamera(),
            swapchainElements.at(i)->sky()->imageView,
            swapchainElements.at(i)->occlusionScene()
        ));
    }
}

void RenderVREntity::destroyComponents()
{
    for (u32 i = 0; i < components.size(); i++)
    {
        delete components.at(i);
    }

    components.clear();
}
#endif
