/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_node_format.hpp"
#include "script.hpp"
#include "script_tools.hpp"
#include "interface_constants.hpp"

static i32 maxVerticalExtent(const ScriptContext* ctx, const shared_ptr<ScriptNode>& node)
{
    i32 y = node->position().y + node->size(ctx).y;

    for (const shared_ptr<ScriptNode>& child : node->children())
    {
        y = max(y, maxVerticalExtent(ctx, child));
    }

    return y;
}

ScriptNodeFormat::ScriptNodeFormat()
    : nextID(1)
{

}

ScriptNodeFormat::ScriptNodeFormat(const ScriptContext* ctx, const vector<Value>& values)
    : ScriptNodeFormat()
{
    i32 y = 0;

    for (size_t i = 0; i < values.size(); i++)
    {
        const Value& value = values[i];

        shared_ptr<ScriptNode> node;

        if (i + 1 == values.size())
        {
            node = make_shared<ScriptNode>(ScriptNode::makeOutput(ctx, &nextID, ivec2(0, y), value));
        }
        else
        {
            node = make_shared<ScriptNode>(ctx, &nextID, ivec2(0, y), value, false);
        }

        _nodes.insert(node);

        y = max(y, maxVerticalExtent(ctx, node) + UIScale::scriptNodeDefaultVerticalSpacing());
    }
}

ScriptNodeFormat::ScriptNodeFormat(const ScriptContext* ctx, const string& code)
    : ScriptNodeFormat(ctx, parse(code))
{

}

ScriptNodeFormat::ScriptNodeFormat(const ScriptNodeFormat& rhs)
{
    nextID = rhs.nextID;
    for (const shared_ptr<ScriptNode>& node : rhs._nodes)
    {
        _nodes.insert(node->copyWithChildren());
    }
}

ScriptNodeFormat::ScriptNodeFormat(ScriptNodeFormat&& rhs)
{
    nextID = rhs.nextID;
    _nodes = rhs._nodes;
}

ScriptNodeFormat::~ScriptNodeFormat()
{

}

ScriptNodeFormat& ScriptNodeFormat::operator=(const ScriptNodeFormat& rhs)
{
    if (&rhs != this)
    {
        nextID = rhs.nextID;
        _nodes = {};
        for (const shared_ptr<ScriptNode>& node : rhs._nodes)
        {
            _nodes.insert(node->copyWithChildren());
        }
    }

    return *this;
}

ScriptNodeFormat& ScriptNodeFormat::operator=(ScriptNodeFormat&& rhs)
{
    if (&rhs != this)
    {
        nextID = rhs.nextID;
        _nodes = rhs._nodes;
    }

    return *this;
}

bool ScriptNodeFormat::operator==(const ScriptNodeFormat& rhs) const
{
    return _nodes == rhs._nodes;
}

bool ScriptNodeFormat::operator!=(const ScriptNodeFormat& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeFormat::operator>(const ScriptNodeFormat& rhs) const
{
    return _nodes > rhs._nodes;
}

bool ScriptNodeFormat::operator<(const ScriptNodeFormat& rhs) const
{
    return _nodes < rhs._nodes;
}

bool ScriptNodeFormat::operator>=(const ScriptNodeFormat& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeFormat::operator<=(const ScriptNodeFormat& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeFormat::operator<=>(const ScriptNodeFormat& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

const set<shared_ptr<ScriptNode>>& ScriptNodeFormat::nodes() const
{
    return _nodes;
}

set<shared_ptr<ScriptNode>> ScriptNodeFormat::allNodes() const
{
    set<shared_ptr<ScriptNode>> nodes;

    for (const shared_ptr<ScriptNode>& node : _nodes)
    {
        set<shared_ptr<ScriptNode>> subNodes = node->allNodes(node);

        nodes.insert(subNodes.begin(), subNodes.end());
    }

    return nodes;
}

shared_ptr<ScriptNode> ScriptNodeFormat::find(ScriptNodeID id) const
{
    shared_ptr<ScriptNode> result = nullptr;

    traverse([&result, id](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
        if (node->id() == id)
        {
            result = node;
        }
    });

    return result;
}

void ScriptNodeFormat::traverse(const function<void(shared_ptr<ScriptNode>, size_t, shared_ptr<ScriptNode>)>& behavior) const
{
    size_t i = 0;
    for (const shared_ptr<ScriptNode>& node : _nodes)
    {
        behavior(nullptr, i, node);

        node->traverse(node, behavior);

        i++;
    }
}

ScriptNodeID ScriptNodeFormat::addNew(const ScriptContext* ctx, ScriptNodeType type, const string& name, const ivec2& position)
{
    Value value;

    switch (type)
    {
    case Script_Node_Output :
    {
        ScriptNode node = ScriptNode::makeOutput(ctx, &nextID, position, {});

        _nodes.insert(make_shared<ScriptNode>(node));

        return node.id();
    }
    case Script_Node_Execution :
        value.type = Value_List;
        value.l = { symbol(name) };
        break;
    case Script_Node_Void :
        value.type = Value_Void;
        break;
    case Script_Node_Symbol :
        value.type = Value_Symbol;
        break;
    case Script_Node_Integer :
        value.type = Value_Integer;
        break;
    case Script_Node_Float :
        value.type = Value_Float;
        break;
    case Script_Node_String :
        value.type = Value_String;
        break;
    case Script_Node_List :
        value.type = Value_List;
        break;
    case Script_Node_Lambda :
        value.type = Value_Lambda;
        break;
    case Script_Node_Handle :
        value.type = Value_Handle;
        break;
    }

    ScriptNode node(ctx, &nextID, position, value, false);

    _nodes.insert(make_shared<ScriptNode>(node));

    return node.id();
}

set<ScriptNodeID> ScriptNodeFormat::add(shared_ptr<ScriptNode> node)
{
    set<ScriptNodeID> newIDs;

    node->setID(nextID++);
    newIDs.insert(node->id());

    node->traverse(node, [this, &newIDs](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
        node->setID(nextID++);
        newIDs.insert(node->id());
    });

    _nodes.insert(node);

    return newIDs;
}

void ScriptNodeFormat::remove(ScriptNodeID id)
{
    erase_if(_nodes, [id](const shared_ptr<ScriptNode>& node) -> bool { return node->id() == id; });

    for (const shared_ptr<ScriptNode>& node : _nodes)
    {
        node->remove(id);
    }
}

vector<Value> ScriptNodeFormat::values() const
{
    vector<Value> values;

    vector<shared_ptr<ScriptNode>> nodes = { _nodes.begin(), _nodes.end() };
    sort(
        nodes.begin(),
        nodes.end(),
        [](const shared_ptr<ScriptNode>& a, const shared_ptr<ScriptNode>& b) -> bool
        {
            if (a->type() == Script_Node_Output)
            {
                return false;
            }
            else if (b->type() == Script_Node_Output)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    );

    for (const shared_ptr<ScriptNode>& node : nodes)
    {
        values.push_back(node->value());
    }

    return values;
}

string ScriptNodeFormat::code() const
{
    string code;

    vector<shared_ptr<ScriptNode>> nodes = { _nodes.begin(), _nodes.end() };
    sort(
        nodes.begin(),
        nodes.end(),
        [](const shared_ptr<ScriptNode>& a, const shared_ptr<ScriptNode>& b) -> bool
        {
            if (a->type() == Script_Node_Output)
            {
                return false;
            }
            else if (b->type() == Script_Node_Output)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    );

    for (const shared_ptr<ScriptNode>& node : nodes)
    {
        code += node->code() + "\n\n";
    }

    return code;
}
