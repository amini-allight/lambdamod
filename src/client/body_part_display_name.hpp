/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_part.hpp"
#include "localization.hpp"

inline string bodyPartDisplayName(const BodyPart* part)
{
    if (part->type() == Body_Part_Anchor)
    {
        if (auto anchor = part->get<BodyPartAnchor>(); anchor.type == Body_Anchor_Generic)
        {
            return localize("body-part-anchor-with-name", to_string(part->id().value()), anchor.name);
        }
        else
        {
            return localize("body-part-anchor-with-name", to_string(part->id().value()), localize(bodyAnchorTypeToString(anchor.type)));
        }
    }
    else
    {
        return localize(bodyPartTypeToString(part->type()) + "-with-id", to_string(part->id().value()));
    }
}
