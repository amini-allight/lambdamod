/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "bindings_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "viewer_mode_context.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "column.hpp"
#include "row.hpp"
#include "searchable_option_select.hpp"
#include "close_button.hpp"
#include "label.hpp"
#include "spacer.hpp"

static string toName(const string& x)
{
    static const size_t prefixSize = strlen("input-");

    string result(x.size() - prefixSize, ' ');

    bool capitalize = true;

    for (size_t i = 0; i < result.size(); i++)
    {
        char c = x[prefixSize + i];

        if (c == '-')
        {
            capitalize = true;
        }
        else if (c >= 'a' && c <= 'z')
        {
            if (capitalize)
            {
                c -= 'a' - 'A';
                capitalize = false;
            }

            result[i] = c;
        }
        else
        {
            if (capitalize)
            {
                capitalize = false;
            }

            result[i] = c;
        }
    }

    return result;
}

BindingsEditor::BindingsEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
    , inputType(Input_Null)
{
    START_WIDGET_SCOPE("bindings-editor")
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
    END_SCOPE

    auto column = new Column(this);

    auto row = new Row(column);

    new SearchableOptionSelect(
        row,
        bind(&BindingsEditor::inputTypesSource, this),
        nullptr,
        bind(&BindingsEditor::onSelect, this, placeholders::_1)
    );
    lineEdit = new LineEdit(
        row,
        nullptr,
        [this](const string& text) -> void
        {
            onAdd(text);
            lineEdit->clear();
        }
    );
    lineEdit->setPlaceholder(localize("bindings-editor-field-name"));
    new IconButton(
        row,
        Icon_Add,
        [this]() -> void
        {
            onAdd(lineEdit->content());
            lineEdit->clear();
        }
    );

    listView = new ListView(column);
}

void BindingsEditor::step()
{
    InterfaceWidget::step();

    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    if (entity->bindings() != bindings)
    {
        bindings = entity->bindings();
        updateListView();
    }
}

vector<string> BindingsEditor::inputTypesSource()
{
    vector<string> inputTypes(Input_Count);

    for (size_t i = 0; i < inputTypes.size(); i++)
    {
        inputTypes[i] = toName(inputTypeToString(static_cast<InputType>(i)));
    }

    return inputTypes;
}

void BindingsEditor::onSelect(size_t index)
{
    inputType = static_cast<InputType>(index);
}

void BindingsEditor::onAdd(const string& text)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([this, entity, text]() -> void
    {
        entity->addBinding(inputType, { InputBinding(text) });
    });
}

void BindingsEditor::onRemove(InputType type)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, type]() -> void
    {
        entity->removeBinding(type);
    });
}

void BindingsEditor::updateListView()
{
    listView->clearChildren();

    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    for (const auto& [ input, binding ] : entity->bindings())
    {
        auto row = new Row(listView);

        new Label(
            row,
            inputTypeToString(input)
        );

        new Label(
            row,
            binding.input
        );

        InputType inputType = input;

        new CloseButton(
            row,
            [this, inputType]() -> void { onRemove(inputType); }
        );
    }

    listView->update();
}

void BindingsEditor::undo(const Input& input)
{
    context()->viewerMode()->undo();
}

void BindingsEditor::redo(const Input& input)
{
    context()->viewerMode()->redo();
}

SizeProperties BindingsEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
