/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_room_bounds.hpp"
#include "render_tools.hpp"
#include "theme.hpp"

static constexpr size_t vertexCount = 12 * 2;
static constexpr f64 vrBoundsHeight = 3;

RenderVRRoomBounds::RenderVRRoomBounds(
    const RenderContext* ctx,

    vector<VRSwapchainElement*> swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderVRPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    const vec2& dimensions
)
    : RenderVRDecoration(ctx, descriptorSetLayout, pipelineStore->themeGradient.pipelineLayout, pipelineStore->themeGradient.pipeline)
{
    transform = { position, rotation, vec3(dimensions.x, dimensions.y, vrBoundsHeight) };

    vertices = createVertexBuffer(vertexCount * sizeof(fvec3));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    fvec3 corners[8];
    corners[0] = fvec3(0.5f, 0.5f, 0);
    corners[1] = fvec3(0.5f, -0.5f, 0);
    corners[2] = fvec3(-0.5f, -0.5f, 0);
    corners[3] = fvec3(-0.5f, 0.5f, 0);
    corners[4] = fvec3(0.5f, 0.5f, 1);
    corners[5] = fvec3(0.5f, -0.5f, 1);
    corners[6] = fvec3(-0.5f, -0.5f, 1);
    corners[7] = fvec3(-0.5f, 0.5f, 1);

    vector<fvec3> vertexData;
    vertexData.reserve(vertexCount);

    vertexData.push_back(corners[0]);
    vertexData.push_back(corners[1]);

    vertexData.push_back(corners[1]);
    vertexData.push_back(corners[2]);

    vertexData.push_back(corners[2]);
    vertexData.push_back(corners[3]);

    vertexData.push_back(corners[3]);
    vertexData.push_back(corners[0]);

    vertexData.push_back(corners[0]);
    vertexData.push_back(corners[4]);

    vertexData.push_back(corners[1]);
    vertexData.push_back(corners[5]);

    vertexData.push_back(corners[2]);
    vertexData.push_back(corners[6]);

    vertexData.push_back(corners[3]);
    vertexData.push_back(corners[7]);

    vertexData.push_back(corners[4]);
    vertexData.push_back(corners[5]);

    vertexData.push_back(corners[5]);
    vertexData.push_back(corners[6]);

    vertexData.push_back(corners[6]);
    vertexData.push_back(corners[7]);

    vertexData.push_back(corners[7]);
    vertexData.push_back(corners[4]);

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(fvec3));

    createComponents(swapchainElements);
}

RenderVRRoomBounds::~RenderVRRoomBounds()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderVRRoomBounds::work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const
{
    RenderVRDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderVRRoomBounds::createUniformBuffer() const
{
    VmaBuffer buffer = RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount + sizeof(fvec4) * 4);

    VmaMapping<fvec4> mapping(ctx->allocator, buffer);

    mapping.data[8] = theme.accentGradientXStartColor.toVec4();
    mapping.data[9] = theme.accentGradientXEndColor.toVec4();
    mapping.data[10] = theme.accentGradientYStartColor.toVec4();
    mapping.data[11] = theme.accentGradientYEndColor.toVec4();

    return buffer;
}
#endif
