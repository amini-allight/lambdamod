/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_settings.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "checkbox.hpp"
#include "ufloat_edit.hpp"

InterfaceSettings::InterfaceSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "interface-settings-frame-counter", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InterfaceSettings::frameCounterSource, this),
            bind(&InterfaceSettings::onFrameCounter, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "interface-settings-ping-counter", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InterfaceSettings::pingCounterSource, this),
            bind(&InterfaceSettings::onPingCounter, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "interface-settings-ui-auto-scale", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InterfaceSettings::uiAutoScaleSource, this),
            bind(&InterfaceSettings::onUIAutoScale, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "interface-settings-ui-scale", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&InterfaceSettings::uiScaleSource, this),
            bind(&InterfaceSettings::onUIScale, this, placeholders::_1)
        );
    });
}

bool InterfaceSettings::frameCounterSource()
{
    return g_config.frameCounter;
}

void InterfaceSettings::onFrameCounter(bool state)
{
    g_config.frameCounter = state;

    saveConfig(g_config);
}

bool InterfaceSettings::pingCounterSource()
{
    return g_config.pingCounter;
}

void InterfaceSettings::onPingCounter(bool state)
{
    g_config.pingCounter = state;

    saveConfig(g_config);
}

bool InterfaceSettings::uiAutoScaleSource()
{
    return g_config.uiAutoScale;
}

void InterfaceSettings::onUIAutoScale(bool state)
{
    g_config.uiAutoScale = state;

    saveConfig(g_config);
}

f64 InterfaceSettings::uiScaleSource()
{
    return g_config.uiScale;
}

void InterfaceSettings::onUIScale(f64 scale)
{
    g_config.uiScale = scale;

    saveConfig(g_config);
}

SizeProperties InterfaceSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
