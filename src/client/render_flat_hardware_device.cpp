/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_hardware_device.hpp"
#include "render_tools.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "theme.hpp"

RenderFlatHardwareDevice::RenderFlatHardwareDevice(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    VRDevice deviceType,
    const vec3& position,
    const quaternion& rotation
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->themeGradient.pipelineLayout, pipelineStore->themeGradient.pipeline)
{
    transform = { position, rotation, vec3(1) };

    string mesh = getMesh(deviceType);

    vertexCount = mesh.size() / sizeof(fvec3);
    vertices = createVertexBuffer(vertexCount * sizeof(fvec3));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, mesh.data(), mesh.size());

    createComponents(swapchainElements);
}

RenderFlatHardwareDevice::~RenderFlatHardwareDevice()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderFlatHardwareDevice::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderFlatHardwareDevice::createUniformBuffer() const
{
    VmaBuffer buffer = RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount + sizeof(fvec4) * 4);

    VmaMapping<fvec4> mapping(ctx->allocator, buffer);

    mapping.data[8] = theme.accentGradientXStartColor.toVec4();
    mapping.data[9] = theme.accentGradientXEndColor.toVec4();
    mapping.data[10] = theme.accentGradientYStartColor.toVec4();
    mapping.data[11] = theme.accentGradientYEndColor.toVec4();

    return buffer;
}

string RenderFlatHardwareDevice::getMesh(VRDevice device) const
{
    switch (device)
    {
    default :
    case VR_Device_Head :
        return getFile(meshPath + "/vr-device-head" + meshExt);
    case VR_Device_Left_Hand :
        return getFile(meshPath + "/vr-device-left-hand" + meshExt);
    case VR_Device_Right_Hand :
        return getFile(meshPath + "/vr-device-right-hand" + meshExt);
    case VR_Device_Hips :
        return getFile(meshPath + "/vr-device-hips" + meshExt);
    case VR_Device_Left_Foot :
        return getFile(meshPath + "/vr-device-left-foot" + meshExt);
    case VR_Device_Right_Foot :
        return getFile(meshPath + "/vr-device-right-foot" + meshExt);
    case VR_Device_Chest :
        return getFile(meshPath + "/vr-device-chest" + meshExt);
    case VR_Device_Left_Elbow :
        return getFile(meshPath + "/vr-device-left-elbow" + meshExt);
    case VR_Device_Right_Elbow :
        return getFile(meshPath + "/vr-device-right-elbow" + meshExt);
    case VR_Device_Left_Knee :
        return getFile(meshPath + "/vr-device-left-knee" + meshExt);
    case VR_Device_Right_Knee :
        return getFile(meshPath + "/vr-device-right-knee" + meshExt);
    }
}
