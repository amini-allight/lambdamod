/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "vr_capable_panel.hpp"

class VRKeyboard : public VRCapablePanel
{
public:
    VRKeyboard(InterfaceView* view);

    void moveKeyboard(const vec3& position, const quaternion& rotation);

    void draw(const DrawContext& ctx) const override;
    bool shouldDraw() const override;

private:
    vec3 position;
    quaternion rotation;

    optional<Point> toLocal(const vec3& position, const quaternion& rotation) const;

    SizeProperties sizeProperties() const override;
};
#endif
