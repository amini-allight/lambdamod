/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "fuzzy_find.hpp"
#include "tools.hpp"

struct FuzzyFindMatch
{
    FuzzyFindMatch(const string& text)
        : text(text)
        , offset(numeric_limits<size_t>::max())
        , length(0)
    {

    }

    FuzzyFindMatch(const string& text, size_t offset, size_t length)
        : text(text)
        , offset(offset)
        , length(length)
    {

    }

    FuzzyFindMatch operator+(const FuzzyFindMatch& rhs) const
    {
        return {
            text,
            min(offset, rhs.offset),
            length + rhs.length
        };
    }

    FuzzyFindMatch& operator+=(const FuzzyFindMatch& rhs)
    {
        return *this = *this + rhs;
    }

    bool operator==(const FuzzyFindMatch& rhs) const
    {
        return text == rhs.text && offset == rhs.offset && length == rhs.length;
    }

    bool operator!=(const FuzzyFindMatch& rhs) const
    {
        return !(*this == rhs);
    }

    bool operator>(const FuzzyFindMatch& rhs) const
    {
        return !(*this < rhs || *this == rhs);
    }

    bool operator<(const FuzzyFindMatch& rhs) const
    {
        if (length > rhs.length)
        {
            return true;
        }
        else if (length < rhs.length)
        {
            return false;
        }
        else
        {
            if (offset < rhs.offset)
            {
                return true;
            }
            else if (offset > rhs.offset)
            {
                return false;
            }
            else
            {
                return text < rhs.text;
            }
        }
    }

    bool operator>=(const FuzzyFindMatch& rhs) const
    {
        return *this > rhs || *this == rhs;
    }

    bool operator<=(const FuzzyFindMatch& rhs) const
    {
        return *this < rhs || *this == rhs;
    }

    strong_ordering operator<=>(const FuzzyFindMatch& rhs) const
    {
        if (*this > rhs)
        {
            return strong_ordering::greater;
        }
        else if (*this == rhs)
        {
            return strong_ordering::equal;
        }
        else
        {
            return strong_ordering::less;
        }
    }

    string text;
    size_t offset;
    size_t length;
};

static vector<string> extractWords(const string& s)
{
    vector<string> words;

    string word;

    for (char c : s)
    {
        if (isWhitespace(c))
        {
            if (!word.empty())
            {
                words.push_back(word);
                word.clear();
            }
        }
        else
        {
            word += c;
        }
    }

    if (!word.empty())
    {
        words.push_back(word);
    }

    return words;
}

static FuzzyFindMatch fuzzyFindWordMatch(const string& value, const string& queryWord)
{
    FuzzyFindMatch match(value);

    for (size_t i = 0; i < value.size(); i++)
    {
        size_t length = 0;

        for (size_t j = 0; j < queryWord.size() && i + j < value.size(); j++)
        {
            if (value[i + j] == queryWord[j])
            {
                length++;
            }
            else
            {
                break;
            }
        }

        if (length > match.length)
        {
            match = { value, i, length };
        }
    }

    return match;
}

static FuzzyFindMatch fuzzyFindMatch(const string& value, const string& query)
{
    if (query.size() > value.size())
    {
        return FuzzyFindMatch(value);
    }

    FuzzyFindMatch wholeMatch = fuzzyFindWordMatch(value, query);

    FuzzyFindMatch wordMatch(value);

    for (const string& queryWord : extractWords(query))
    {
        wordMatch += fuzzyFindWordMatch(value, queryWord);
    }

    return wholeMatch < wordMatch ? wholeMatch : wordMatch;
}

bool fuzzyFindCompare(string a, string b, string query)
{
    a = toLowercase(a);
    b = toLowercase(b);
    query = toLowercase(query);

    FuzzyFindMatch aMatch = fuzzyFindMatch(a, query);
    FuzzyFindMatch bMatch = fuzzyFindMatch(b, query);

    return aMatch < bMatch;
}
