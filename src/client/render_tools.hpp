/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "render_types.hpp"

string vulkanError(VkResult result);

#ifdef LMOD_VK_VALIDATION
VkBool32 handleError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
);
#endif

VkImageMemoryBarrier imageMemoryBarrier(
    VkImage image,
    VkImageAspectFlagBits aspect,
    VkImageLayout prev,
    VkImageLayout next
);

VkBufferMemoryBarrier bufferMemoryBarrier(
    VkBuffer buffer,
    VkAccessFlags srcAccess,
    VkAccessFlags dstAccess,
    VkDeviceSize offset = 0,
    VkDeviceSize size = VK_WHOLE_SIZE
);

void copyImage(
    VkCommandBuffer commandBuffer,
    VkPipelineStageFlagBits srcStage,
    VkPipelineStageFlagBits dstStage,
    u32 width,
    u32 height,
    VkImageAspectFlagBits aspect,
    u32 layerCount,

    VkImage src,
    VkImageLayout srcPrev,
    VkImageLayout srcNext,

    VkImage dst,
    VkImageLayout dstPrev,
    VkImageLayout dstNext
);

void blitImage(
    VkCommandBuffer commandBuffer,
    VkPipelineStageFlagBits srcStage,
    VkPipelineStageFlagBits dstStage,
    VkImageAspectFlagBits aspect,
    u32 layerCount,
    VkFilter filter,

    VkImage src,
    u32 srcWidth,
    u32 srcHeight,
    VkImageLayout srcPrev,
    VkImageLayout srcNext,

    VkImage dst,
    u32 dstWidth,
    u32 dstHeight,
    VkImageLayout dstPrev,
    VkImageLayout dstNext
);

void preinitializeImage(
    VkDevice device,
    VmaAllocator allocator,
    VmaImage image,
    u32 width,
    u32 height,
    u32 pitch,
    const u32* pixels
);

void computeMipmaps(
    VkCommandBuffer commandBuffer,
    VkImage image,
    VkPipelineStageFlags srcStage,
    VkPipelineStageFlags dstStage,
    VkImageLayout srcLayout,
    VkImageLayout dstLayout,
    u32 width,
    u32 height,
    u32 layers = 1
);

void transitionLayout(
    VkCommandBuffer commandBuffer,
    VkImage image,
    VkPipelineStageFlags srcStage,
    VkPipelineStageFlags dstStage,
    VkImageLayout srcLayout,
    VkImageLayout dstLayout,
    u32 layers = 1
);

void barrierBuffer(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkPipelineStageFlags srcStage,
    VkPipelineStageFlags dstStage,
    VkAccessFlags srcAccess,
    VkAccessFlags dstAccess
);

VkClearValue colorToClearValue(const vec4& color);

template<typename T>
T getVKExtensionFunction(VkInstance instance, const char* name)
{
    T f = reinterpret_cast<T>(vkGetInstanceProcAddr(instance, name));

    if (!f)
    {
        fatal("Failed to get Vulkan extension function '" + std::string(name) + "'.");
    }

    return f;
}

#define GET_VK_EXTENSION_FUNCTION(_instance, _name) getVKExtensionFunction<PFN_##_name>(_instance, #_name)

VmaBuffer createBuffer(
    VmaAllocator allocator,
    size_t size,
    VkBufferUsageFlags bufferUsage,
    VmaMemoryUsage memoryUsage,
    VkDeviceSize alignment = 0
);
void destroyBuffer(VmaAllocator allocator, VmaBuffer buffer);

VmaImage createImage(
    VmaAllocator allocator,
    const uvec3& dimensions,
    VkFormat format,
    VkSampleCountFlagBits samples,
    VkImageUsageFlags imageUsage,
    VmaMemoryUsage memoryUsage,
    bool preinitialized,
    u32 mipLevels = 1,
    u32 layers = 1
);
void destroyImage(VmaAllocator allocator, VmaImage image);

VkImageView createImageView(
    VkDevice device,
    VkImage image,
    VkFormat format,
    VkImageAspectFlags aspect,
    VkImageViewType imageViewType = VK_IMAGE_VIEW_TYPE_2D,
    u32 mipLevels = 1,
    u32 layers = 1
);
void destroyImageView(VkDevice device, VkImageView imageView);

VkDescriptorSet createDescriptorSet(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    VkDescriptorSetLayout descriptorSetLayout
);
void destroyDescriptorSet(VkDevice device, VkDescriptorPool descriptorPool, VkDescriptorSet descriptorSet);

vector<VkDescriptorSet> createDescriptorSets(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    const vector<VkDescriptorSetLayout>& descriptorSetLayouts
);
void destroyDescriptorSets(VkDevice device, VkDescriptorPool descriptorPool, const vector<VkDescriptorSet>& descriptorSets);

enum ClearValueType : u8
{
    Clear_Value_Color,
    Clear_Value_Depth,
    Clear_Value_Stencil,
    Clear_Value_Alpha
};

VkClearValue fillClearValue(ClearValueType type);
vector<VkClearValue> fillClearValues(const vector<ClearValueType>& types);

template<typename T>
vector<T> extractImage(
    VkDevice device,
    VkQueue queue,
    VmaAllocator allocator,
    VkCommandPool commandPool,
    VkImage image,
    VkImageLayout imageLayout,
    u32 width,
    u32 height
)
{
    vkDeviceWaitIdle(device);

    VmaBuffer buffer = createBuffer(
        allocator,
        width * height * sizeof(T),
        VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_GPU_TO_CPU
    );

    runOneshotCommands(device, queue, commandPool, [&](VkCommandBuffer commandBuffer) -> void {
        VkImageMemoryBarrier beforeBarrier = imageMemoryBarrier(
            image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            imageLayout,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &beforeBarrier
        );

        VkBufferImageCopy region{};
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = 0;
        region.imageSubresource.layerCount = 1;
        region.imageExtent = { width, height, 1 };

        vkCmdCopyImageToBuffer(
            commandBuffer,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            buffer.buffer,
            1,
            &region
        );

        VkImageMemoryBarrier afterBarrier = imageMemoryBarrier(
            image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            imageLayout
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &afterBarrier
        );
    });

    vector<T> data(width * height);

    {
        VmaMapping<T> mapping(allocator, buffer);

        memcpy(data.data(), mapping.data, width * height * sizeof(T));
    }

    destroyBuffer(allocator, buffer);

    return data;
}

void runOneshotCommands(
    VkDevice device,
    VkQueue queue,
    VkCommandPool commandPool,
    const function<void(VkCommandBuffer)>& behavior
);

void extractAndSaveImage(
    VkDevice device,
    VkQueue queue,
    VmaAllocator allocator,
    VkCommandPool commandPool,
    VkImage image,
    VkImageLayout imageLayout,
    u32 width,
    u32 height
);

u32 maxMipLevels(u32 width, u32 height);

u32 luminanceImageSize(u32 width, u32 height);
f32 luminanceOccupancy(u32 width, u32 height);
