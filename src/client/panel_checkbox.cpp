/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_checkbox.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

PanelCheckbox::PanelCheckbox(
    InterfaceWidget* parent,
    const function<bool()>& source,
    const function<void(bool)>& onSet
)
    : Checkbox(parent, source, onSet)
{

}

void PanelCheckbox::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        UIScale::panelBorderSize(this),
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    drawColoredImage(
        ctx,
        this,
        Point(UIScale::panelBorderSize(this)),
        UIScale::panelCheckboxSize(this) - Size(UIScale::panelBorderSize(this) * 2),
        checked() ? Icon_Checked : Icon_None,
        focused() ? theme.accentColor : theme.panelForegroundColor
    );
}

SizeProperties PanelCheckbox::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::panelCheckboxSize(this).w), static_cast<f64>(UIScale::panelCheckboxSize(this).h),
        Scaling_Fixed, Scaling_Fixed
    };
}
