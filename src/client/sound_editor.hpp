/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

#include "item_list.hpp"
#include "sample_editor.hpp"

class SoundEditor : public InterfaceWidget, public TabContent
{
public:
    SoundEditor(InterfaceWidget* parent, EntityID id);

private:
    EntityID id;

    ItemList* sampleList;
    SampleEditor* editor;

    vector<string> itemSource();
    void onAdd(const string& name);
    void onRemove(const string& name);
    void onSelect(const string& name);
    void onRename(const string& oldName, const string& newName);
    void onClone(const string& oldName, const string& newName);

    void undo(const Input& input);
    void redo(const Input& input);

    SizeProperties sizeProperties() const override;
};
