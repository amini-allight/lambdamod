/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "world_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "units.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "checkbox.hpp"
#include "vec3_edit.hpp"
#include "ufloat_edit.hpp"
#include "percent_edit.hpp"
#include "text_button.hpp"
#include "sky_parameters_dialog.hpp"
#include "atmosphere_parameters_dialog.hpp"

WorldSettings::WorldSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "world-settings-speed-of-sound", speedSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&WorldSettings::speedOfSoundSource, this),
            bind(&WorldSettings::onSpeedOfSound, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-gravity", accelerationSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&WorldSettings::gravitySource, this),
            bind(&WorldSettings::onGravity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-flow-velocity", speedSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&WorldSettings::flowVelocitySource, this),
            bind(&WorldSettings::onFlowVelocity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-density", densitySuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&WorldSettings::densitySource, this),
            bind(&WorldSettings::onDensity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-up", [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&WorldSettings::upSource, this),
            bind(&WorldSettings::onUp, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&WorldSettings::shownSource, this),
            bind(&WorldSettings::onShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-fog-distance", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&WorldSettings::fogDistanceSource, this),
            bind(&WorldSettings::onFogDistance, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "world-settings-sky", [this](InterfaceWidget* parent) -> void {
        new TextButton(
            parent,
            localize("world-settings-open-parameters"),
            bind(&WorldSettings::onOpenSkyParameters, this)
        );
    });

    new EditorEntry(listView, "world-settings-atmosphere", [this](InterfaceWidget* parent) -> void {
        new TextButton(
            parent,
            localize("world-settings-open-parameters"),
            bind(&WorldSettings::onOpenAtmosphereParameters, this)
        );
    });
}

f64 WorldSettings::speedOfSoundSource()
{
    if (!context()->controller()->selfWorld())
    {
        return 0;
    }

    return localizeSpeed(context()->controller()->selfWorld()->speedOfSound());
}

void WorldSettings::onSpeedOfSound(f64 v)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setSpeedOfSound(delocalizeSpeed(v));
    });
}

vec3 WorldSettings::gravitySource()
{
    if (!context()->controller()->selfWorld())
    {
        return vec3();
    }

    return localizeAcceleration(context()->controller()->selfWorld()->gravity());
}

void WorldSettings::onGravity(const vec3& v)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setGravity(delocalizeAcceleration(v));
    });
}

vec3 WorldSettings::flowVelocitySource()
{
    if (!context()->controller()->selfWorld())
    {
        return vec3();
    }

    return localizeSpeed(context()->controller()->selfWorld()->flowVelocity());
}

void WorldSettings::onFlowVelocity(const vec3& v)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setFlowVelocity(delocalizeSpeed(v));
    });
}

f64 WorldSettings::densitySource()
{
    if (!context()->controller()->selfWorld())
    {
        return 0;
    }

    return localizeDensity(context()->controller()->selfWorld()->density());
}

void WorldSettings::onDensity(f64 v)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setDensity(delocalizeDensity(v));
    });
}

vec3 WorldSettings::upSource()
{
    if (!context()->controller()->selfWorld())
    {
        return vec3();
    }

    return context()->controller()->selfWorld()->up();
}

void WorldSettings::onUp(const vec3& v)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setUp(v);
    });
}

bool WorldSettings::shownSource()
{
    if (!context()->controller()->selfWorld())
    {
        return false;
    }

    return context()->controller()->selfWorld()->shown();
}

void WorldSettings::onShown(bool state)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setShown(state);
    });
}

f64 WorldSettings::fogDistanceSource()
{
    if (!context()->controller()->selfWorld())
    {
        return 0;
    }

    return localizeLength(context()->controller()->selfWorld()->fogDistance());
}

void WorldSettings::onFogDistance(f64 distance)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setFogDistance(delocalizeLength(distance));
    });
}

void WorldSettings::onOpenSkyParameters()
{
    view()->interface()->addWindow<SkyParametersDialog>(
        bind(&WorldSettings::skyParametersSource, this),
        bind(&WorldSettings::onSkyParameters, this, placeholders::_1)
    );
}

SkyParameters WorldSettings::skyParametersSource()
{
    if (!context()->controller()->selfWorld())
    {
        return SkyParameters();
    }

    return context()->controller()->selfWorld()->sky();
}

void WorldSettings::onSkyParameters(const SkyParameters& parameters)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setSky(parameters);
    });
}

void WorldSettings::onOpenAtmosphereParameters()
{
    view()->interface()->addWindow<AtmosphereParametersDialog>(
        bind(&WorldSettings::atmosphereParametersSource, this),
        bind(&WorldSettings::onAtmosphereParameters, this, placeholders::_1)
    );
}

AtmosphereParameters WorldSettings::atmosphereParametersSource()
{
    if (!context()->controller()->selfWorld())
    {
        return AtmosphereParameters();
    }

    return context()->controller()->selfWorld()->atmosphere();
}

void WorldSettings::onAtmosphereParameters(const AtmosphereParameters& parameters)
{
    context()->controller()->edit([&]() -> void {
        context()->controller()->selfWorld()->setAtmosphere(parameters);
    });
}

SizeProperties WorldSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
