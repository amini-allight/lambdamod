/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

// Fix for Windows namespace pollution
#undef interface

class Interface;

class InterfaceView
{
public:
    InterfaceView(Interface* interface, const string& name);
    InterfaceView(const InterfaceView& rhs) = delete;
    InterfaceView(InterfaceView&& rhs) = delete;
    virtual ~InterfaceView();

    InterfaceView& operator=(const InterfaceView& rhs) = delete;
    InterfaceView& operator=(InterfaceView&& rhs) = delete;

    Interface* interface() const;
    const vector<InterfaceWidget*>& children() const;

    virtual void step();
    virtual void draw(const DrawContext& ctx);
    virtual void resize(const Size& size);
    virtual void unfocus();

protected:
    friend class InterfaceWidget;

    void add(InterfaceWidget* child);
    void remove(InterfaceWidget* child);

protected:
    Interface* _interface;
    string name;
    vector<InterfaceWidget*> _children;
};
