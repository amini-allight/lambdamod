/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_tools.hpp"
#include "theme.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "ppm.hpp"
#include "render_constants.hpp"

string vulkanError(VkResult result)
{
    switch (result)
    {
    default : return "unknown error " + to_string(result) +  ".";
    case VK_ERROR_OUT_OF_HOST_MEMORY : return "out of host memory.";
    case VK_ERROR_OUT_OF_DEVICE_MEMORY : return "out of device memory.";
    case VK_ERROR_INITIALIZATION_FAILED : return "initialization failed.";
    case VK_ERROR_DEVICE_LOST : return "device lost.";
    case VK_ERROR_MEMORY_MAP_FAILED : return "memory map failed.";
    case VK_ERROR_LAYER_NOT_PRESENT : return "layer not present.";
    case VK_ERROR_EXTENSION_NOT_PRESENT : return "extension not present.";
    case VK_ERROR_FEATURE_NOT_PRESENT : return "feature not present.";
    case VK_ERROR_INCOMPATIBLE_DRIVER : return "incompatible driver.";
    case VK_ERROR_TOO_MANY_OBJECTS : return "too many objects.";
    case VK_ERROR_FORMAT_NOT_SUPPORTED : return "format not supported.";
    case VK_ERROR_FRAGMENTED_POOL : return "fragmented pool.";
    case VK_ERROR_UNKNOWN : return "unknown error.";
    case VK_ERROR_OUT_OF_POOL_MEMORY : return "out of pool memory.";
    case VK_ERROR_INVALID_EXTERNAL_HANDLE : return "invalid external handle.";
    case VK_ERROR_FRAGMENTATION : return "fragmentation.";
    case VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS : return "invalid opaque capture address.";
    case VK_ERROR_SURFACE_LOST_KHR : return "surface lost.";
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR : return "native window in use.";
    case VK_SUBOPTIMAL_KHR : return "suboptimal.";
    case VK_ERROR_OUT_OF_DATE_KHR : return "out of date.";
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR : return "incompatible display.";
    case VK_ERROR_VALIDATION_FAILED_EXT : return "validation failed.";
    case VK_ERROR_INVALID_SHADER_NV : return "invalid shader.";
    case VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT : return "invalid DRM format modifier plane layout.";
    case VK_ERROR_NOT_PERMITTED_EXT : return "not permitted";
    case VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT : return "full screen exclusive mode lost.";
    case VK_THREAD_IDLE_KHR : return "thread idle.";
    case VK_THREAD_DONE_KHR : return "thread done.";
    case VK_OPERATION_DEFERRED_KHR : return "operation deferred.";
    case VK_OPERATION_NOT_DEFERRED_KHR : return "operation not deferred.";
    case VK_PIPELINE_COMPILE_REQUIRED_EXT : return "pipeline compile required.";
    }
}

#ifdef LMOD_VK_VALIDATION
VkBool32 handleError(
    VkDebugUtilsMessageSeverityFlagBitsEXT severity,
    VkDebugUtilsMessageTypeFlagsEXT type,
    const VkDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    string sType;

    switch (type)
    {
    case VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        sType = "general";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        sType = "validation";
        break;
    case VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        sType = "performance";
        break;
    }

    switch (severity)
    {
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        log("Vulkan " + sType + ": " + string(callbackData->pMessage));
        break;
    default :
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        log("Vulkan " + sType + ": " + string(callbackData->pMessage));
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        warning("Vulkan " + sType + ": " + string(callbackData->pMessage));
        break;
    case VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        error("Vulkan " + sType + ": " + string(callbackData->pMessage));
        break;
    }

    return VK_FALSE;
}
#endif

VkImageMemoryBarrier imageMemoryBarrier(
    VkImage image,
    VkImageAspectFlagBits aspect,
    VkImageLayout prev,
    VkImageLayout next
)
{
    VkImageMemoryBarrier barrier{};

    barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    barrier.oldLayout = prev;
    barrier.newLayout = next;
    barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    barrier.image = image;
    barrier.subresourceRange.aspectMask = aspect;
    barrier.subresourceRange.baseMipLevel = 0;
    barrier.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;
    barrier.subresourceRange.baseArrayLayer = 0;
    barrier.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS;

    return barrier;
}

VkBufferMemoryBarrier bufferMemoryBarrier(
    VkBuffer buffer,
    VkAccessFlags srcAccess,
    VkAccessFlags dstAccess,
    VkDeviceSize offset,
    VkDeviceSize size
)
{
    VkBufferMemoryBarrier barrier{};

    barrier.sType = VK_STRUCTURE_TYPE_BUFFER_MEMORY_BARRIER;
    barrier.srcAccessMask = srcAccess;
    barrier.dstAccessMask = dstAccess;
    barrier.buffer = buffer;
    barrier.offset = offset;
    barrier.size = size;

    return barrier;
}

void copyImage(
    VkCommandBuffer commandBuffer,
    VkPipelineStageFlagBits srcStage,
    VkPipelineStageFlagBits dstStage,
    u32 width,
    u32 height,
    VkImageAspectFlagBits aspect,
    u32 layerCount,

    VkImage src,
    VkImageLayout srcPrev,
    VkImageLayout srcNext,

    VkImage dst,
    VkImageLayout dstPrev,
    VkImageLayout dstNext
)
{
    if (srcPrev != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            src,
            aspect,
            srcPrev,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            srcStage,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }

    if (dstPrev != VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            dst,
            aspect,
            dstPrev,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            srcStage,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }

    VkImageCopy region{};
    region.srcSubresource.aspectMask = aspect;
    region.srcSubresource.layerCount = layerCount;
    region.srcSubresource.mipLevel = 0;
    region.srcOffset = { 0, 0, 0 };
    region.dstSubresource.aspectMask = aspect;
    region.dstSubresource.layerCount = layerCount;
    region.dstSubresource.mipLevel = 0;
    region.dstOffset = { 0, 0, 0 };
    region.extent = { width, height, 1 };

    vkCmdCopyImage(
        commandBuffer,
        src,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        dst,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    if (srcNext != VK_IMAGE_LAYOUT_UNDEFINED && srcNext != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            src,
            aspect,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            srcNext
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            dstStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }

    if (dstNext != VK_IMAGE_LAYOUT_UNDEFINED && dstNext != VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            dst,
            aspect,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            dstNext
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            dstStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }
}

void blitImage(
    VkCommandBuffer commandBuffer,
    VkPipelineStageFlagBits srcStage,
    VkPipelineStageFlagBits dstStage,
    VkImageAspectFlagBits aspect,
    u32 layerCount,
    VkFilter filter,

    VkImage src,
    u32 srcWidth,
    u32 srcHeight,
    VkImageLayout srcPrev,
    VkImageLayout srcNext,

    VkImage dst,
    u32 dstWidth,
    u32 dstHeight,
    VkImageLayout dstPrev,
    VkImageLayout dstNext
)
{
    if (srcPrev != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            src,
            aspect,
            srcPrev,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            srcStage,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }

    if (dstPrev != VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            dst,
            aspect,
            dstPrev,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            srcStage,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }

    VkImageBlit region{};
    region.srcSubresource.aspectMask = aspect;
    region.srcSubresource.layerCount = layerCount;
    region.srcSubresource.mipLevel = 0;
    region.srcOffsets[0] = { 0, 0, 0 };
    region.srcOffsets[1] = { static_cast<i32>(srcWidth), static_cast<i32>(srcHeight), 1 };
    region.dstSubresource.aspectMask = aspect;
    region.dstSubresource.layerCount = layerCount;
    region.dstSubresource.mipLevel = 0;
    region.dstOffsets[0] = { 0, 0, 0 };
    region.dstOffsets[1] = { static_cast<i32>(dstWidth), static_cast<i32>(dstHeight), 1 };

    vkCmdBlitImage(
        commandBuffer,
        src,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        dst,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region,
        filter
    );

    if (srcNext != VK_IMAGE_LAYOUT_UNDEFINED && srcNext != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            src,
            aspect,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            srcNext
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            dstStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }

    if (dstNext != VK_IMAGE_LAYOUT_UNDEFINED && dstNext != VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            dst,
            aspect,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            dstNext
        );

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            dstStage,
            0,
            0, nullptr,
            0, nullptr,
            1, &barrier
        );
    }
}

void preinitializeImage(
    VkDevice device,
    VmaAllocator allocator,
    VmaImage image,
    u32 width,
    u32 height,
    u32 pitch,
    const u32* pixels
)
{
    VmaMapping<u32> mapping(allocator, image);

    VkImageSubresource subresource{};
    subresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    subresource.arrayLayer = 0;
    subresource.mipLevel = 0;
    VkSubresourceLayout layout{};
    vkGetImageSubresourceLayout(device, image.image, &subresource, &layout);

    u32 cpuPitch = pitch / sizeof(u32);
    u32 gpuPitch = layout.rowPitch / sizeof(u32);

    const u32* inPixels = pixels;
    u32* outPixels = mapping.data + (layout.offset / sizeof(u32));

    for (u32 y = 0; y < height; y++)
    {
        for (u32 x = 0; x < width; x++)
        {
            outPixels[(y * gpuPitch) + x] = inPixels[(y * cpuPitch) + x];
        }
    }
}

void computeMipmaps(
    VkCommandBuffer commandBuffer,
    VkImage image,
    VkPipelineStageFlags srcStage,
    VkPipelineStageFlags dstStage,
    VkImageLayout srcLayout,
    VkImageLayout dstLayout,
    u32 width,
    u32 height,
    u32 layers
)
{
    u32 srcMipmapLevel = 0;
    u32 srcWidth = width;
    u32 srcHeight = height;
    u32 dstMipmapLevel = srcMipmapLevel + 1;
    u32 dstWidth = max<u32>(srcWidth / 2, 1);
    u32 dstHeight = max<u32>(srcHeight / 2, 1);

    while (true)
    {
        if (srcMipmapLevel != 0 || srcLayout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
        {
            VkImageMemoryBarrier srcBarrier = imageMemoryBarrier(
                image,
                VK_IMAGE_ASPECT_COLOR_BIT,
                srcMipmapLevel == 0 ? srcLayout : VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
            );
            srcBarrier.subresourceRange.layerCount = layers;
            srcBarrier.subresourceRange.baseMipLevel = srcMipmapLevel;
            srcBarrier.subresourceRange.levelCount = 1;

            vkCmdPipelineBarrier(
                commandBuffer,
                srcMipmapLevel == 0 ? srcStage : VK_PIPELINE_STAGE_TRANSFER_BIT,
                VK_PIPELINE_STAGE_TRANSFER_BIT,
                0,
                0,
                nullptr,
                0,
                nullptr,
                1,
                &srcBarrier
            );
        }

        // break here to make sure we transition the bottom layer to TRANSFER_SRC_OPTIMAL before leaving
        if (srcWidth == dstWidth && srcHeight == dstHeight)
        {
            break;
        }

        VkImageMemoryBarrier dstBarrier = imageMemoryBarrier(
            image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
        );
        dstBarrier.subresourceRange.layerCount = layers;
        dstBarrier.subresourceRange.baseMipLevel = dstMipmapLevel;
        dstBarrier.subresourceRange.levelCount = 1;

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &dstBarrier
        );

        VkImageBlit region{};
        region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.srcSubresource.mipLevel = srcMipmapLevel;
        region.srcSubresource.layerCount = layers;
        region.srcOffsets[0] = { 0, 0, 0 };
        region.srcOffsets[1] = { static_cast<i32>(srcWidth), static_cast<i32>(srcHeight), 1 };
        region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.dstSubresource.mipLevel = dstMipmapLevel;
        region.dstSubresource.layerCount = layers;
        region.dstOffsets[0] = { 0, 0, 0 };
        region.dstOffsets[1] = { static_cast<i32>(dstWidth), static_cast<i32>(dstHeight), 1 };

        vkCmdBlitImage(
            commandBuffer,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            image,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            1,
            &region,
            VK_FILTER_LINEAR
        );

        srcMipmapLevel = dstMipmapLevel;
        srcWidth = dstWidth;
        srcHeight = dstHeight;
        dstMipmapLevel = srcMipmapLevel + 1;
        dstWidth = max<u32>(srcWidth / 2, 1);
        dstHeight = max<u32>(srcHeight / 2, 1);
    }

    if (dstLayout != VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL)
    {
        VkImageMemoryBarrier barrier = imageMemoryBarrier(
            image,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            dstLayout
        );
        barrier.subresourceRange.layerCount = layers;
        barrier.subresourceRange.levelCount = VK_REMAINING_MIP_LEVELS;

        vkCmdPipelineBarrier(
            commandBuffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            dstStage,
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &barrier
        );
    }
}

void transitionLayout(
    VkCommandBuffer commandBuffer,
    VkImage image,
    VkPipelineStageFlags srcStage,
    VkPipelineStageFlags dstStage,
    VkImageLayout srcLayout,
    VkImageLayout dstLayout,
    u32 layers
)
{
    VkImageMemoryBarrier barrier = imageMemoryBarrier(
        image,
        VK_IMAGE_ASPECT_COLOR_BIT,
        srcLayout,
        dstLayout
    );

    barrier.subresourceRange.layerCount = layers;

    vkCmdPipelineBarrier(
        commandBuffer,
        srcStage,
        dstStage,
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &barrier
    );
}

void barrierBuffer(
    VkCommandBuffer commandBuffer,
    VkBuffer buffer,
    VkPipelineStageFlags srcStage,
    VkPipelineStageFlags dstStage,
    VkAccessFlags srcAccess,
    VkAccessFlags dstAccess
)
{
    VkBufferMemoryBarrier barrier = bufferMemoryBarrier(buffer, srcAccess, dstAccess);

    vkCmdPipelineBarrier(
        commandBuffer,
        srcStage,
        dstStage,
        0,
        0,
        nullptr,
        1,
        &barrier,
        0,
        nullptr
    );
}

VkClearValue colorToClearValue(const vec4& color)
{
    VkClearValue clearValue{};

    clearValue.color = {{
        static_cast<f32>(color.x),
        static_cast<f32>(color.y),
        static_cast<f32>(color.z),
        static_cast<f32>(color.w)
    }};

    return clearValue;
}

VmaBuffer createBuffer(
    VmaAllocator allocator,
    size_t size,
    VkBufferUsageFlags bufferUsage,
    VmaMemoryUsage memoryUsage,
    VkDeviceSize alignment
)
{
    VkResult result;

    VmaBuffer buffer;

    VkBufferCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    createInfo.size = size;
    createInfo.usage = bufferUsage;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;

    VmaAllocationCreateInfo allocateInfo{};
    allocateInfo.usage = memoryUsage;

    if (alignment == 0)
    {
        result = vmaCreateBuffer(
            allocator,
            &createInfo,
            &allocateInfo,
            &buffer.buffer,
            &buffer.allocation,
            nullptr
        );
    }
    else
    {
        result = vmaCreateBufferWithAlignment(
            allocator,
            &createInfo,
            &allocateInfo,
            alignment,
            &buffer.buffer,
            &buffer.allocation,
            nullptr
        );
    }

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create buffer: " + vulkanError(result));
    }

    return buffer;
}

void destroyBuffer(VmaAllocator allocator, VmaBuffer buffer)
{
    vmaDestroyBuffer(allocator, buffer.buffer, buffer.allocation);
}

VmaImage createImage(
    VmaAllocator allocator,
    const uvec3& dimensions,
    VkFormat format,
    VkSampleCountFlagBits samples,
    VkImageUsageFlags imageUsage,
    VmaMemoryUsage memoryUsage,
    bool preinitialized,
    u32 mipLevels,
    u32 layers
)
{
    VkResult result;

    VmaImage image;

    VkImageCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    createInfo.imageType = VK_IMAGE_TYPE_2D;
    createInfo.extent = { dimensions.x, dimensions.y, dimensions.z };
    createInfo.mipLevels = mipLevels;
    createInfo.arrayLayers = layers;
    createInfo.samples = samples;
    createInfo.tiling = preinitialized ? VK_IMAGE_TILING_LINEAR : VK_IMAGE_TILING_OPTIMAL;
    createInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    createInfo.initialLayout = preinitialized ? VK_IMAGE_LAYOUT_PREINITIALIZED : VK_IMAGE_LAYOUT_UNDEFINED;
    createInfo.format = format;
    createInfo.usage = imageUsage;

    VmaAllocationCreateInfo allocateInfo{};
    allocateInfo.usage = memoryUsage;

    result = vmaCreateImage(allocator, &createInfo, &allocateInfo, &image.image, &image.allocation, nullptr);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create image: " + vulkanError(result));
    }

    return image;
}

void destroyImage(VmaAllocator allocator, VmaImage image)
{
    vmaDestroyImage(allocator, image.image, image.allocation);
}

VkImageView createImageView(
    VkDevice device,
    VkImage image,
    VkFormat format,
    VkImageAspectFlags aspect,
    VkImageViewType imageViewType,
    u32 mipLevels,
    u32 layers
)
{
    VkResult result;

    VkImageView imageView;

    VkImageViewCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    createInfo.viewType = imageViewType;
    createInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    createInfo.subresourceRange.baseMipLevel = 0;
    createInfo.subresourceRange.levelCount = mipLevels;
    createInfo.subresourceRange.baseArrayLayer = 0;
    createInfo.subresourceRange.layerCount = layers;
    createInfo.image = image;
    createInfo.format = format;
    createInfo.subresourceRange.aspectMask = aspect;

    result = vkCreateImageView(device, &createInfo, nullptr, &imageView);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create image view: " + vulkanError(result));
    }

    return imageView;
}

void destroyImageView(VkDevice device, VkImageView imageView)
{
    vkDestroyImageView(device, imageView, nullptr);
}

VkDescriptorSet createDescriptorSet(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    VkDescriptorSetLayout descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSet descriptorSet;

    VkDescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocateInfo.descriptorPool = descriptorPool;
    allocateInfo.descriptorSetCount = 1;
    allocateInfo.pSetLayouts = &descriptorSetLayout;

    result = vkAllocateDescriptorSets(device, &allocateInfo, &descriptorSet);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to allocate descriptor set: " + vulkanError(result));
    }

    return descriptorSet;
}

void destroyDescriptorSet(VkDevice device, VkDescriptorPool descriptorPool, VkDescriptorSet descriptorSet)
{
    vkFreeDescriptorSets(device, descriptorPool, 1, &descriptorSet);
}

vector<VkDescriptorSet> createDescriptorSets(
    VkDevice device,
    VkDescriptorPool descriptorPool,
    const vector<VkDescriptorSetLayout>& descriptorSetLayouts
)
{
    VkResult result;

    vector<VkDescriptorSet> descriptorSets(descriptorSetLayouts.size());

    VkDescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocateInfo.descriptorPool = descriptorPool;
    allocateInfo.descriptorSetCount = descriptorSetLayouts.size();
    allocateInfo.pSetLayouts = descriptorSetLayouts.data();

    result = vkAllocateDescriptorSets(device, &allocateInfo, descriptorSets.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to allocate descriptor sets: " + vulkanError(result));
    }

    return descriptorSets;
}

void destroyDescriptorSets(VkDevice device, VkDescriptorPool descriptorPool, const vector<VkDescriptorSet>& descriptorSets)
{
    vkFreeDescriptorSets(device, descriptorPool, descriptorSets.size(), descriptorSets.data());
}

VkClearValue fillClearValue(ClearValueType type)
{
    VkClearValue clearValue{};

    switch (type)
    {
    case Clear_Value_Color :
        clearValue = colorToClearValue(vec4(baseBackgroundColor, 1));
        break;
    case Clear_Value_Depth :
        clearValue.depthStencil.depth = 1.0;
        break;
    case Clear_Value_Stencil :
        clearValue.color.uint32[0] = 2;
        clearValue.color.uint32[1] = 0;
        clearValue.color.uint32[2] = 0;
        clearValue.color.uint32[3] = 1;
        break;
    case Clear_Value_Alpha :
        clearValue = colorToClearValue(pureAlpha.toVec4());
        break;
    }

    return clearValue;
}

vector<VkClearValue> fillClearValues(const vector<ClearValueType>& types)
{
    vector<VkClearValue> clearValues;
    clearValues.reserve(types.size());

    for (ClearValueType type : types)
    {
        clearValues.push_back(fillClearValue(type));
    }

    return clearValues;
}

void runOneshotCommands(
    VkDevice device,
    VkQueue queue,
    VkCommandPool commandPool,
    const function<void(VkCommandBuffer)>& behavior
)
{
    VkResult result;

    VkFenceCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;

    VkFence fence;
    result = vkCreateFence(device, &createInfo, nullptr, &fence);

    if (result != VK_SUCCESS)
    {
        error("Failed to create fence in oneshot commands execution: " + vulkanError(result));
    }

    VkCommandBufferAllocateInfo allocInfo{};
    allocInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocInfo.commandPool = commandPool;
    allocInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;
    result = vkAllocateCommandBuffers(device, &allocInfo, &commandBuffer);

    if (result != VK_SUCCESS)
    {
        error("Failed to allocate command buffer in oneshot commands execution: " + vulkanError(result));
    }

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        error("Failed to begin command buffer in oneshot commands execution: " + vulkanError(result));
    }

    behavior(commandBuffer);

    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        error("Failed to end command buffer in oneshot commands execution: " + vulkanError(result));
    }

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.commandBufferCount = 1;
    submitInfo.pCommandBuffers = &commandBuffer;

    result = vkQueueSubmit(
        queue,
        1,
        &submitInfo,
        fence
    );

    if (result != VK_SUCCESS)
    {
        error("Failed to submit command buffer in oneshot commands execution: " + vulkanError(result));
    }

    result = vkWaitForFences(device, 1, &fence, true, numeric_limits<u64>::max());

    if (result != VK_SUCCESS)
    {
        error("Failed to wait for fence in oneshot commands execution: " + vulkanError(result));
    }

    vkFreeCommandBuffers(device, commandPool, 1, &commandBuffer);

    vkDestroyFence(device, fence, nullptr);
}

void extractAndSaveImage(
    VkDevice device,
    VkQueue queue,
    VmaAllocator allocator,
    VkCommandPool commandPool,
    VkImage image,
    VkImageLayout imageLayout,
    u32 width,
    u32 height
)
{
    vector<fvec4> data = extractImage<fvec4>(
        device,
        queue,
        allocator,
        commandPool,
        image,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        width,
        height
    );

    vector<u8> clamped(width * height * 4, 255);

    for (u32 i = 0; i < width * height; i++)
    {
        clamped[i * 4 + 0] = clamp<f32>(data[i].x, 0, 1) * 255;
        clamped[i * 4 + 1] = clamp<f32>(data[i].y, 0, 1) * 255;
        clamped[i * 4 + 2] = clamp<f32>(data[i].z, 0, 1) * 255;
        clamped[i * 4 + 3] = clamp<f32>(data[i].w, 0, 1) * 255;
    }

    writePPM("screenshot.ppm", width, height, clamped.data());
}

u32 maxMipLevels(u32 width, u32 height)
{
    return floor(log(max(width, height), 2)) + 1;
}

u32 luminanceImageSize(u32 width, u32 height)
{
    return pow(2, ceil(log(max(width, height), 2)));
}

f32 luminanceOccupancy(u32 width, u32 height)
{
    return (width * height) / static_cast<f32>(pow(luminanceImageSize(width, height), 2));
}
