/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "sample.hpp"
#include "sample_part.hpp"

enum SampleTransformMode : u8
{
    Sample_Transform_None,
    Sample_Transform_Translate
};

enum SampleTransformSubMode : u8
{
    Sample_Transform_Sub_None,
    Sample_Transform_Sub_X,
    Sample_Transform_Sub_Y
};

class SampleTimeline : public InterfaceWidget
{
public:
    SampleTimeline(
        InterfaceWidget* parent,
        EntityID id,
        const function<u32()>& playPositionSource,
        const function<void()>& onPlayPause,
        const function<void(u32)>& onSeek,
        const function<void(const set<SamplePartID>&)>& onSelect
    );

    void open(const string& name);

    void draw(const DrawContext& ctx) const override;

private:
    EntityID id;
    string name;
    set<SamplePartID> activeParts;
    u32 scrollIndex;
    u32 panIndex;

    function<u32()> playPositionSource;
    function<void()> onPlayPause;
    function<void(u32)> onSeek;
    function<void(const set<SamplePartID>&)> onSelect;

    SampleTransformMode transformMode;
    SampleTransformSubMode transformSubMode;
    Point initialPointer;
    map<SamplePartID, SamplePart> initialParts;

    Point selectStart;
    Point selectEnd;
    bool selecting;

    Point panStart;
    u32 panStartScrollIndex;
    u32 panStartPanIndex;
    bool panning;

    void makeSelections(SelectMode mode);

    void add(const Point& pointer);
    void remove(const Point& pointer);

    void transform(const Point& pointer);
    void translate(i64 startDelta, i64 indexDelta);
    void startTransform(SampleTransformMode mode, const Point& pointer);
    void endTransform();
    void cancelTransform();

    void edit(const function<void(Sample&)>& behavior) const;

    optional<tuple<u32, u32>> positionUnderCursor(const Point& pointer) const;
    optional<SamplePartID> partUnderCursor(const Point& pointer) const;
    Point screenPosition(const SamplePart& note) const;

    const Sample& currentSample() const;
    const SamplePart& currentPart() const;

    void selectAll(const Input& input);
    void deselectAll(const Input& input);
    void invertSelection(const Input& input);
    void duplicate(const Input& input);
    void cut(const Input& input);
    void copy(const Input& input);
    void paste(const Input& input);
    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void panLeft(const Input& input);
    void panRight(const Input& input);
    void addNew(const Input& input);
    void removeSelected(const Input& input);
    void startSelect(const Input& input);
    void startPan(const Input& input);
    void translate(const Input& input);
    void togglePlay(const Input& input);
    void moveSelect(const Input& input);
    void endReplaceSelect(const Input& input);
    void endAddSelect(const Input& input);
    void endRemoveSelect(const Input& input);
    void pan(const Input& input);
    void endPan(const Input& input);
    void toggleX(const Input& input);
    void toggleY(const Input& input);
    void useTool(const Input& input);
    void cancelTool(const Input& input);
    void endTool(const Input& input);

    i32 millisecondsPerColumn() const;

    SizeProperties sizeProperties() const override;
};
