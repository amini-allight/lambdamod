/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_panel.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "row.hpp"
#include "spacer.hpp"
#include "panel_label.hpp"
#include "panel_dynamic_label.hpp"
#include "text_panel_icon.hpp"
#include "panel_list_view.hpp"
#include "panel_text_button.hpp"
#include "panel_bar_timer.hpp"
#include "panel_checkbox.hpp"
#include "panel_icon_button.hpp"
#include "panel_option_select.hpp"
#include "choose_major_block.hpp"
#include "panel_transforms.hpp"
#include "flat_render.hpp"
#include "flat_panel_input_scope.hpp"
#include "vr_panel_input_scope.hpp"

static const TextStyleOverride titleStyle = {
    {},
    {},
    Text_Center,
    Text_Bold,
    {}
};

TextPanel::TextPanel(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
    , pointerManager(this)
    , currentID(0)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        START_VR_PANEL_SCOPE(context()->input()->getScope("text"), "text-panel")

        END_SCOPE
    }
    else
    {
        START_FLAT_PANEL_SCOPE(context()->input()->getScope("text"), "text-panel")

        END_SCOPE
    }
#else
    START_FLAT_PANEL_SCOPE(context()->input()->getScope("text"), "text-panel")

    END_SCOPE
#endif

    pointerManager.init();

    auto row = new Row(this);

    new TextPanelIcon(row, context());

    listView = new PanelListView(row);

    MAKE_SPACER(row, UIScale::panelTextPanelMargin(this), 0);
}

void TextPanel::step()
{
    pointerManager.step();

    if (context()->text()->hasRequest() && currentID != context()->text()->requestID())
    {
        currentID = context()->text()->requestID();

        clearChildren();

        switch (context()->text()->requestType())
        {
        default :
            break;
        case Text_Timeout :
            addTimeoutChildren(context()->text()->requestData<TextTimeoutRequest>());
            break;
        case Text_Unary :
            addUnaryChildren(context()->text()->requestData<TextUnaryRequest>());
            break;
        case Text_Binary :
            addBinaryChildren(context()->text()->requestData<TextBinaryRequest>());
            break;
        case Text_Options :
            addOptionsChildren(context()->text()->requestData<TextOptionsRequest>());
            break;
        case Text_Choose :
            addChooseChildren(context()->text()->requestData<TextChooseRequest>());
            break;
        case Text_Assign_Values :
            addAssignValuesChildren(context()->text()->requestData<TextAssignValuesRequest>());
            break;
        case Text_Spend_Points :
            addSpendPointsChildren(context()->text()->requestData<TextSpendPointsRequest>());
            break;
        case Text_Assign_Points :
            addAssignPointsChildren(context()->text()->requestData<TextAssignPointsRequest>());
            break;
        case Text_Vote :
            addVoteChildren(context()->text()->requestData<TextVoteRequest>());
            break;
        case Text_Choose_Major :
            addChooseMajorChildren(context()->text()->requestData<TextChooseMajorRequest>());
            break;
        }

        update();
    }

    if (!context()->text()->hasRequest() && currentID != TextID())
    {
        currentID = TextID();

        clearChildren();

        update();
    }

    InterfaceWidget::step();
}

void TextPanel::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

#ifdef LMOD_VR
    if (g_vr)
    {
        drawPointer(ctx, this, leftPointer);
        drawPointer(ctx, this, rightPointer);
    }
    else
    {
        drawPointer(ctx, this, pointer);
    }
#else
    drawPointer(ctx, this, pointer);
#endif
}

bool TextPanel::shouldDraw() const
{
#ifdef LMOD_VR
    return VRCapablePanel::shouldDraw() && context()->text()->hasRequest();
#else
    return InterfaceWidget::shouldDraw() && context()->text()->hasRequest();
#endif
}

void TextPanel::addTimeoutChildren(const TextTimeoutRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    for (const string& line : splitLines(request.text))
    {
        new PanelLabel(listView, line);
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelBarTimer(listView, bind(&TextPanel::timeoutFractionSource, this));
}

void TextPanel::addUnaryChildren(const TextUnaryRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    for (const string& line : splitLines(request.text))
    {
        new PanelLabel(listView, line);
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelTextButton(listView, localize("text-panel-ok"), bind(&TextPanel::onOK, this));
}

void TextPanel::addBinaryChildren(const TextBinaryRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    for (const string& line : splitLines(request.text))
    {
        new PanelLabel(listView, line);
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    auto row = new Row(listView);

    new PanelTextButton(row, localize("text-panel-no"), bind(&TextPanel::onNo, this));
    MAKE_SPACER(row, UIScale::panelTextPanelItemSpacing(this), 0);
    new PanelTextButton(row, localize("text-panel-yes"), bind(&TextPanel::onYes, this));
}

void TextPanel::addOptionsChildren(const TextOptionsRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    for (const string& line : splitLines(request.text))
    {
        new PanelLabel(listView, line);
    }

    u64 i = 0;
    for (const string& option : request.options)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        new PanelTextButton(listView, option, bind(&TextPanel::onPickOption, this, i));

        i++;
    }
}

void TextPanel::addChooseChildren(const TextChooseRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    new PanelDynamicLabel(
        listView,
        bind(&TextPanel::chooseInstructionSource, this)
    );

    for (const string& option : request.options)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        auto row = new Row(listView);

        new PanelLabel(row, option);
        new PanelCheckbox(
            row,
            bind(&TextPanel::chosenSource, this, option),
            bind(static_cast<void (TextPanel::*)(const string&, bool)>(&TextPanel::onChoose), this, option, placeholders::_1)
        );
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelTextButton(listView, localize("text-panel-done"), bind(static_cast<void (TextPanel::*)(void)>(&TextPanel::onChoose), this));
}

void TextPanel::addAssignValuesChildren(const TextAssignValuesRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    new PanelDynamicLabel(
        listView,
        bind(&TextPanel::valueInstructionSource, this)
    );

    new PanelDynamicLabel(
        listView,
        bind(&TextPanel::remainingValuesSource, this)
    );

    for (const string& key : request.keys)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        auto row = new Row(listView);

        new PanelLabel(row, key);
        new PanelOptionSelect(
            row,
            bind(&TextPanel::valuesSource, this),
            bind(&TextPanel::valueSource, this, key),
            [this, key](size_t index) -> void
            {
                onAssignValue(key, valuesSource()[index]);
            }
        );
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelTextButton(listView, localize("text-panel-done"), bind(&TextPanel::onAssignValues, this));
}

void TextPanel::addSpendPointsChildren(const TextSpendPointsRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    new PanelDynamicLabel(
        listView,
        bind(&TextPanel::purchaseInstructionSource, this)
    );

    auto headerRow = new Row(listView);
    new PanelLabel(headerRow, localize("text-panel-name"), titleStyle);
    new PanelLabel(headerRow, localize("text-panel-price"), titleStyle);
    new PanelLabel(headerRow, localize("text-panel-purchased"), titleStyle);

    for (const auto& [ name, price ] : request.prices)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        auto row = new Row(listView);

        new PanelLabel(row, name);
        new PanelLabel(row, to_string(price));
        new PanelCheckbox(
            row,
            bind(&TextPanel::purchasedSource, this, name),
            bind(&TextPanel::onPurchase, this, name, placeholders::_1)
        );
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelTextButton(listView, localize("text-panel-done"), bind(&TextPanel::onSpendPoints, this));
}

void TextPanel::addAssignPointsChildren(const TextAssignPointsRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    new PanelDynamicLabel(
        listView,
        bind(&TextPanel::pointInstructionSource, this)
    );

    auto headerRow = new Row(listView);
    new PanelLabel(headerRow, localize("text-panel-name"), titleStyle);
    new PanelLabel(headerRow, localize("text-panel-points"), titleStyle);
    new PanelLabel(headerRow, "", titleStyle);
    MAKE_SPACER(headerRow, UIScale::panelTextPanelItemSpacing(this), UIScale::panelLabelHeight(this));
    new PanelLabel(headerRow, "", titleStyle);

    for (const string& key : request.keys)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        auto row = new Row(listView);

        new PanelLabel(row, key);
        new PanelDynamicLabel(row, bind(&TextPanel::pointsSource, this, key));
        new PanelIconButton(row, Icon_Add, bind(&TextPanel::onAddPoint, this, key));
        MAKE_SPACER(headerRow, UIScale::panelTextPanelItemSpacing(this), UIScale::panelLabelHeight(this));
        new PanelIconButton(row, Icon_Subtract, bind(&TextPanel::onRemovePoint, this, key));
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelTextButton(listView, localize("text-panel-done"), bind(&TextPanel::onAssignPoints, this));
}

void TextPanel::addVoteChildren(const TextVoteRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    new PanelDynamicLabel(
        listView,
        bind(&TextPanel::voteInstructionSource, this)
    );

    u64 i = 0;
    for (const string& option : request.options)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        auto row = new Row(listView);

        new PanelLabel(row, option);
        new PanelCheckbox(
            row,
            bind(&TextPanel::votedSource, this, i),
            bind(static_cast<void (TextPanel::*)(u64, bool)>(&TextPanel::onVote), this, i, placeholders::_1)
        );

        i++;
    }

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelTextButton(listView, localize("text-panel-done"), bind(static_cast<void (TextPanel::*)(void)>(&TextPanel::onVote), this));

    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
    new PanelBarTimer(listView, bind(&TextPanel::voteFractionSource, this));
}

void TextPanel::addChooseMajorChildren(const TextChooseMajorRequest& request)
{
    new PanelLabel(listView, request.title, titleStyle);
    MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));

    new PanelLabel(listView, localize("text-panel-choose-one"));

    u64 i = 0;
    for (const auto& [ title, subtitle, text ] : request.options)
    {
        MAKE_SPACER(listView, 0, UIScale::panelTextPanelItemSpacing(this));
        new ChooseMajorBlock(
            listView,
            title,
            subtitle,
            text,
            bind(&TextPanel::onChooseMajor, this, i)
        );
    }
}

void TextPanel::clearChildren()
{
    listView->clearChildren();
}

void TextPanel::onOK()
{
    context()->text()->respond();
}

void TextPanel::onNo()
{
    context()->text()->response<TextBinaryResponse>().choice = 0;

    context()->text()->respond();
}

void TextPanel::onYes()
{
    context()->text()->response<TextBinaryResponse>().choice = 1;

    context()->text()->respond();
}

void TextPanel::onPickOption(u64 choice)
{
    context()->text()->response<TextOptionsResponse>().choice = choice;

    context()->text()->respond();
}

void TextPanel::onChoose()
{
    context()->text()->respond();
}

void TextPanel::onAssignValues()
{
    context()->text()->respond();
}

void TextPanel::onSpendPoints()
{
    context()->text()->respond();
}

void TextPanel::onAssignPoints()
{
    context()->text()->respond();
}

void TextPanel::onVote()
{
    context()->text()->respond();
}

void TextPanel::onChooseMajor(u64 choice)
{
    context()->text()->response<TextChooseMajorResponse>().choice = choice;

    context()->text()->respond();
}

vector<string> TextPanel::splitLines(const string& text) const
{
    vector<string> lines;

    string remaining = text;

    while (!remaining.empty())
    {
        string chunk;

        for (size_t i = 0; i < remaining.size(); i++)
        {
            chunk += remaining[i];

            i32 w = TextStyle(this).getFont()->width(chunk);

            if (w == lineWidth())
            {
                break;
            }
            else if (w > lineWidth())
            {
                chunk = chunk.substr(0, chunk.size() - 1);
                break;
            }
        }

        remaining = remaining.substr(chunk.size(), remaining.size() - chunk.size());

        lines.push_back(chunk);
    }

    return lines;
}

i32 TextPanel::lineWidth() const
{
    return textWidth - (UIScale::panelTextPanelMargin(this) * 2 + UIScale::panelScrollbarWidth(this));
}

bool TextPanel::chosenSource(const string& name)
{
    const TextChooseResponse& response = context()->text()->response<TextChooseResponse>();

    return response.choices.contains(name);
}

void TextPanel::onChoose(const string& name, bool state)
{
    TextChooseResponse response = context()->text()->response<TextChooseResponse>();

    if (state)
    {
        response.choices.insert(name);
    }
    else
    {
        response.choices.erase(name);
    }

    if (!context()->text()->requestData<TextChooseRequest>().valid(response))
    {
        return;
    }

    context()->text()->response<TextChooseResponse>() = response;
}

string TextPanel::chooseInstructionSource() const
{
    u64 remainingChoices = context()->text()->requestData<TextChooseRequest>().count - context()->text()->response<TextChooseResponse>().choices.size();

    if (remainingChoices == 1)
    {
        return localize("text-panel-choose-item", to_string(remainingChoices));
    }
    else
    {
        return localize("text-panel-choose-items", to_string(remainingChoices));
    }
}

vector<string> TextPanel::valuesSource()
{
    const TextAssignValuesRequest& request = context()->text()->requestData<TextAssignValuesRequest>();

    return join(vector<string>({ "" }), vector<string>({ request.values.begin(), request.values.end() }));
}

size_t TextPanel::valueSource(const string& key)
{
    TextAssignValuesResponse response = context()->text()->response<TextAssignValuesResponse>();

    string value = response.assignment[key];

    vector<string> values = valuesSource();

    for (size_t i = 0; i < values.size(); i++)
    {
        if (values[i] == value)
        {
            return i;
        }
    }

    return 0;
}

void TextPanel::onAssignValue(const string& key, const string& value)
{
    TextAssignValuesResponse response = context()->text()->response<TextAssignValuesResponse>();

    response.assignment[key] = value;

    if (!context()->text()->requestData<TextAssignValuesRequest>().valid(response))
    {
        return;
    }

    context()->text()->response<TextAssignValuesResponse>() = response;
}

string TextPanel::valueInstructionSource() const
{
    set<string> remainingValues = context()->text()->requestData<TextAssignValuesRequest>().values;

    for (const auto& [ key, value ] : context()->text()->response<TextAssignValuesResponse>().assignment)
    {
        remainingValues.erase(value);
    }

    return remainingValues.empty()
        ? localize("text-panel-all-values-have-been-assigned")
        : localize("text-panel-assign-the-following");
}

string TextPanel::remainingValuesSource() const
{
    set<string> remainingValues = context()->text()->requestData<TextAssignValuesRequest>().values;

    for (const auto& [ key, value ] : context()->text()->response<TextAssignValuesResponse>().assignment)
    {
        remainingValues.erase(value);
    }

    return join(remainingValues, ", ");
}

bool TextPanel::purchasedSource(const string& name)
{
    const TextSpendPointsResponse& response = context()->text()->response<TextSpendPointsResponse>();

    return response.purchases.contains(name);
}

void TextPanel::onPurchase(const string& name, bool state)
{
    TextSpendPointsResponse response = context()->text()->response<TextSpendPointsResponse>();

    if (state)
    {
        response.purchases.insert(name);
    }
    else
    {
        response.purchases.erase(name);
    }

    if (!context()->text()->requestData<TextSpendPointsRequest>().valid(response))
    {
        return;
    }

    context()->text()->response<TextSpendPointsResponse>() = response;
}

string TextPanel::purchaseInstructionSource() const
{
    u64 totalSpent = 0;

    for (const string& purchase : context()->text()->response<TextSpendPointsResponse>().purchases)
    {
        totalSpent += context()->text()->requestData<TextSpendPointsRequest>().prices.at(purchase);
    }

    u64 remainingPoints = context()->text()->requestData<TextSpendPointsRequest>().points - totalSpent;

    if (remainingPoints == 1)
    {
        return localize("text-panel-spend-point", to_string(remainingPoints));
    }
    else
    {
        return localize("text-panel-spend-points", to_string(remainingPoints));
    }
}

string TextPanel::pointsSource(const string& key)
{
    const TextAssignPointsResponse& response = context()->text()->response<TextAssignPointsResponse>();

    return to_string(response.assignment.at(key));
}

void TextPanel::onAddPoint(const string& key)
{
    TextAssignPointsResponse response = context()->text()->response<TextAssignPointsResponse>();

    response.assignment[key]++;

    if (!context()->text()->requestData<TextAssignPointsRequest>().valid(response))
    {
        return;
    }

    context()->text()->response<TextAssignPointsResponse>() = response;
}

void TextPanel::onRemovePoint(const string& key)
{
    TextAssignPointsResponse response = context()->text()->response<TextAssignPointsResponse>();

    response.assignment[key]--;

    if (!context()->text()->requestData<TextAssignPointsRequest>().valid(response))
    {
        return;
    }

    context()->text()->response<TextAssignPointsResponse>() = response;
}

string TextPanel::pointInstructionSource() const
{
    u64 totalAssign = 0;

    for (const auto& [ key, value ] : context()->text()->response<TextAssignPointsResponse>().assignment)
    {
        totalAssign += value;
    }

    u64 remainingPoints = context()->text()->requestData<TextAssignPointsRequest>().points - totalAssign;

    if (remainingPoints == 1)
    {
        return localize("text-panel-assign-point", to_string(remainingPoints));
    }
    else
    {
        return localize("text-panel-assign-points", to_string(remainingPoints));
    }
}

bool TextPanel::votedSource(u64 index)
{
    const TextVoteResponse& response = context()->text()->response<TextVoteResponse>();

    return response.votes.contains(index);
}

void TextPanel::onVote(u64 index, bool state)
{
    TextVoteResponse response = context()->text()->response<TextVoteResponse>();

    if (state)
    {
        response.votes.insert(index);
    }
    else
    {
        response.votes.erase(index);
    }

    if (!context()->text()->requestData<TextVoteRequest>().valid(response))
    {
        return;
    }

    context()->text()->response<TextVoteResponse>() = response;
}

string TextPanel::voteInstructionSource() const
{
    u64 remainingVotes = context()->text()->requestData<TextVoteRequest>().votes - context()->text()->response<TextVoteResponse>().votes.size();

    if (remainingVotes == 1)
    {
        return localize("text-panel-vote-for-item", to_string(remainingVotes));
    }
    else
    {
        return localize("text-panel-vote-for-items", to_string(remainingVotes));
    }
}

f64 TextPanel::timeoutFractionSource() const
{
    return (context()->text()->requestElapsed() / 1000.0) / context()->text()->requestData<TextTimeoutRequest>().timeout;
}

f64 TextPanel::voteFractionSource() const
{
    return context()->text()->requestElapsed() / static_cast<f64>(voteDuration.count());
}

optional<Point> TextPanel::toLocal(const Point& pointer) const
{
    if (!shouldDraw())
    {
        return {};
    }

    mat4 toLocal = createFlatTextTransform(
        context()->flatViewer()->position(),
        context()->flatViewer()->rotation(),
        context()->viewerSpace().up(),
        static_cast<f64>(dynamic_cast<FlatRender*>(context()->controller()->render())->height())
    ).inverse();

    optional<vec3> pixelDir = context()->flatViewer()->screenToWorld(pointer);

    if (!pixelDir)
    {
        return {};
    }

    vec3 start = context()->flatViewer()->position();
    vec3 direction = *pixelDir;

    vec3 localStart = (toLocal * vec4(start, 1)).toVec3();
    vec3 localDirection = ((toLocal * vec4(direction, 1)).toVec3() - (toLocal * vec4(vec3(0), 1)).toVec3()).normalize();

    vec3 normal = upDir;

    if (roughly(localDirection.dot(normal), 0.0))
    {
        return {};
    }

    f64 distance = (vec3(0) - localStart).dot(normal) / localDirection.dot(normal);

    if (distance < 0)
    {
        return {};
    }

    vec3 intersection = localStart + (localDirection * distance);

    vec2 point = (intersection.toVec2() + 1) / 2;
    point.y = 1 - point.y;

    return Point(point.x * textWidth, point.y * textHeight);
}

#ifdef LMOD_VR
optional<Point> TextPanel::toLocal(const vec3& position, const quaternion& rotation) const
{
    if (!shouldDraw())
    {
        return {};
    }

    vec3 headRoomPosition = context()->controller()->vr()->worldToRoom(context()->vrViewer()->headTransform().position());
    vec3 headRoomDirection = vec3(context()->controller()->vr()->worldToRoom(context()->vrViewer()->headTransform().rotation()).forward().toVec2(), 0).normalize();

    mat4 toLocal = createVRTextTransform(
        headRoomPosition,
        headRoomDirection
    ).inverse();

    vec3 localStart = (toLocal * vec4(context()->controller()->vr()->worldToRoom(position), 1)).toVec3();
    vec3 localDirection = ((toLocal * vec4(context()->controller()->vr()->worldToRoom(rotation.forward()), 1)).toVec3() - (toLocal * vec4(context()->controller()->vr()->worldToRoom(vec3(0)), 1)).toVec3()).normalize();

    vec3 normal = upDir;

    if (roughly(localDirection.dot(normal), 0.0))
    {
        return {};
    }

    f64 distance = (vec3(0) - localStart).dot(normal) / localDirection.dot(normal);

    if (distance < 0)
    {
        return {};
    }

    vec3 intersection = localStart + (localDirection * distance);

    vec2 point = (intersection.toVec2() + 1) / 2;
    point.y = 1 - point.y;

    return Point(point.x * textWidth, point.y * textHeight);
}
#endif

SizeProperties TextPanel::sizeProperties() const
{
    return sizePropertiesFillAll;
}
