/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud_element_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"
#include "row.hpp"
#include "column.hpp"
#include "hud_element_editor_side_panel.hpp"
#include "hud_preview.hpp"
#include "label.hpp"
#include "float_edit.hpp"
#include "option_select.hpp"
#include "color_picker.hpp"
#include "spacer.hpp"

HUDElementEditor::HUDElementEditor(
    InterfaceWidget* parent,
    EntityID id,
    const function<void(size_t)>& onSelect
)
    : InterfaceWidget(parent)
    , id(id)
    , onSelect(onSelect)
{
    START_WIDGET_SCOPE("hud-element-editor")
        WIDGET_SLOT("toggle-side-panel", toggleSidePanel)
    END_SCOPE

    column = new Column(this);

    preview = new HUDPreview(
        column,
        id,
        onSelect
    );

    sidePanel = new HUDElementEditorSidePanel(this);
}

void HUDElementEditor::open(const string& name)
{
    this->name = name;
    preview->open(name);
}

void HUDElementEditor::move(const Point& position)
{
    this->position(position);

    column->move(position);

    sidePanel->move(position + Point(size().w - UIScale::sidePanelWidth(), 0));
}

void HUDElementEditor::resize(const Size& size)
{
    this->size(size);

    column->resize(size);

    sidePanel->resize(Size(UIScale::sidePanelWidth(), size.h));
}

void HUDElementEditor::updateEntity(const HUDElement& element)
{
    context()->controller()->edit([this, element]() -> void {
        Entity* entity = context()->controller()->selfWorld()->get(id);

        if (!entity)
        {
            return;
        }

        entity->addHUDElement(name, element);
    });
}

const HUDElement& HUDElementEditor::currentElement() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const HUDElement empty;
        return empty;
    }

    auto it = entity->hudElements().find(name);

    if (it == entity->hudElements().end())
    {
        static const HUDElement empty;
        return empty;
    }

    return it->second;
}

void HUDElementEditor::toggleSidePanel(const Input& input)
{
    sidePanel->toggle();
}

SizeProperties HUDElementEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
