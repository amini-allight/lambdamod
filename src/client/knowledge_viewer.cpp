/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "knowledge_viewer.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "panel_transforms.hpp"
#include "vr_render.hpp"
#include "vr_panel_input_scope.hpp"
#include "row.hpp"
#include "panel_read_only_item_list.hpp"
#include "knowledge_viewer_view.hpp"

KnowledgeViewer::KnowledgeViewer(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
    , pointerManager(this)
    , activeIndex(-1)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        START_VR_PANEL_SCOPE(context()->input()->getScope("under-screen"), "knowledge-viewer")
            WIDGET_SLOT("hide-knowledge-viewer", dismiss)
        END_SCOPE
    }
    else
    {
        START_WIDGET_SCOPE("knowledge-viewer")
            WIDGET_SLOT("hide-knowledge-viewer", dismiss)
        END_SCOPE
    }
#else
    START_WIDGET_SCOPE("knowledge-viewer")
        WIDGET_SLOT("hide-knowledge-viewer", dismiss)
    END_SCOPE
#endif

    auto row = new Row(this);

    new PanelReadOnlyItemList(
        row,
        bind(&KnowledgeViewer::itemsSource, this),
        bind(&KnowledgeViewer::onSelect, this, placeholders::_1)
    );

    new KnowledgeViewerView(
        row,
        bind(&KnowledgeViewer::activeItemSource, this)
    );

    pointerManager.init();
}

void KnowledgeViewer::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

#ifdef LMOD_VR
    if (g_vr)
    {
        drawPointer(ctx, this, leftPointer);
        drawPointer(ctx, this, rightPointer);
    }
    else
    {
        drawPointer(ctx, this, pointer);
    }
#else
    drawPointer(ctx, this, pointer);
#endif
}

bool KnowledgeViewer::shouldDraw() const
{
#ifdef LMOD_VR
    return VRCapablePanel::shouldDraw() && context()->attachedMode()->knowledgeViewer();
#else
    return InterfaceWidget::shouldDraw() && context()->attachedMode()->knowledgeViewer();
#endif
}

#ifdef LMOD_VR
optional<Point> KnowledgeViewer::toLocal(const vec3& position, const quaternion& rotation) const
{
    if (!shouldDraw())
    {
        return {};
    }

    const mat4& transform = context()->vrViewer()->headTransform();

    auto [ width, height ] = dynamic_cast<VRRender*>(context()->controller()->render())->size();

    mat4 model = createVRHUDTransform(
        context()->controller()->vr()->worldToRoom(transform.position()),
        static_cast<f64>(width),
        static_cast<f64>(height),
        dynamic_cast<VRRender*>(context()->controller()->render())->angle()
    );

    vec3 localStart = (model.inverse() * vec4(context()->controller()->vr()->worldToRoom(position), 1)).toVec3();
    vec3 localDirection = ((model.inverse() * vec4(context()->controller()->vr()->worldToRoom(rotation.forward()), 1)).toVec3() - (model.inverse() * vec4(context()->controller()->vr()->worldToRoom(vec3(0)), 1)).toVec3()).normalize();

    vec2 flatStart = vec2(localStart.x, localStart.z);
    vec2 flatDirection = vec2(localDirection.x, localDirection.z).normalize();

    f64 a = flatDirection.dot(flatDirection);
    f64 b = 2 * flatStart.dot(flatDirection);
    f64 c = flatStart.dot(flatStart) - sq(hudRadius);

    auto [ r0, r1 ] = quadratic(a, b, c);

    optional<Point> point;

    for (f64 distance : vector<f64>({ r0, r1 }))
    {
        if (isnan(distance) || distance < 0)
        {
            continue;
        }

        distance = hypot(distance, tan(angleBetween(localDirection, vec3(localDirection.x, 0, localDirection.z).normalize())) * distance);

        vec3 contact = localStart + (localDirection * distance);

        f64 angle = angleBetween(
            vec2(contact.x, contact.z).normalize(),
            vec2(-1, 0)
        );
        f64 elevation = contact.y;

        if (angle < pi * 0.2 ||
            angle >= pi * 0.8 ||
            elevation < -1 ||
            elevation >= +1
        )
        {
            continue;
        }

        f64 x = (angle - (pi * 0.2)) / (pi * 0.6);
        f64 y = ((elevation * -1) + 1) / 2;

        point = Point(x * hudWidth, y * hudHeight);
        break;
    }

    return point;
}
#endif

void KnowledgeViewer::dismiss(const Input& input)
{
    context()->attachedMode()->toggleKnowledgeViewer();
}

vector<string> KnowledgeViewer::itemsSource() const
{
    vector<string> items;
    items.reserve(context()->text()->knowledge().size());

    for (const TextKnowledgeRequest& knowledge : context()->text()->knowledge())
    {
        items.push_back(knowledge.title);
    }

    return items;
}

void KnowledgeViewer::onSelect(const string& title)
{
    i32 i = 0;
    for (const TextKnowledgeRequest& knowledge : context()->text()->knowledge())
    {
        if (knowledge.title == title)
        {
            activeIndex = i;
            return;
        }

        i++;
    }
}

tuple<string, string, string> KnowledgeViewer::activeItemSource()
{
    if (activeIndex < 0 || activeIndex >= static_cast<i32>(context()->text()->knowledge().size()))
    {
        return { "", "", "" };
    }

    const TextKnowledgeRequest& knowledge = context()->text()->knowledge().at(activeIndex);

    return { knowledge.title, knowledge.subtitle, knowledge.text };
}

PositionProperties KnowledgeViewer::positionProperties() const
{
    return {
        0.25, 0.25,
        Positioning_Fraction, Positioning_Fraction
    };
}

SizeProperties KnowledgeViewer::sizeProperties() const
{
    return {
        0.5, 0.5,
        Scaling_Fraction, Scaling_Fraction
    };
}
