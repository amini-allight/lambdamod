/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "help.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(760, 424) * uiScale();
}

Help::Help(InterfaceView* view)
    : InterfaceWindow(view, localize("help-help"))
{
    START_WIDGET_SCOPE("help")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-help", dismiss)
    END_SCOPE
}

void Help::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());
}

void Help::step()
{
    InterfaceWindow::step();

    if (!context()->attached())
    {
        hide();
    }
}

void Help::draw(const DrawContext& ctx) const
{
    if (!shown())
    {
        return;
    }

    InterfaceWindow::draw(ctx);

    Point point(UIScale::marginSize());
    Size size = this->size() - UIScale::windowBordersSize();

    TextStyle style;
    style.weight = Text_Bold;

    drawText(
        ctx,
        this,
        point,
        Size(size.w, UIScale::helpRowHeight()),
        localize("help-input-bindings"),
        style
    );

    drawSolid(
        ctx,
        this,
        point + Point(0, UIScale::helpRowHeight()),
        Size(size.w, UIScale::dividerWidth()),
        theme.dividerColor
    );

    i32 rows = size.h / UIScale::helpRowHeight();

    i32 x = 0;
    i32 y = 1;
    for (const auto& [ input, binding ] : context()->attachedMode()->bindings())
    {
        TextStyle style;
        style.weight = Text_Bold;

        drawText(
            ctx,
            this,
            point + Point(x * UIScale::helpColumnWidth(), y * UIScale::helpRowHeight()),
            Size(UIScale::helpColumnWidth() / 2, UIScale::helpRowHeight()),
            inputTypeToString(input),
            style
        );

        drawText(
            ctx,
            this,
            point + Point((x * UIScale::helpColumnWidth()) + (UIScale::helpColumnWidth() / 2), y * UIScale::helpRowHeight()),
            Size(UIScale::helpColumnWidth() / 2, UIScale::helpRowHeight()),
            binding.input,
            TextStyle()
        );

        y++;

        if (y == rows)
        {
            y = 0;
            x++;
        }
    }
}

void Help::dismiss(const Input& input)
{
    hide();
}
