/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tint_manager.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "vr_render.hpp"
#include "flat_render.hpp"

#include "render_flat_tint_effect.hpp"
#include "render_vr_tint_effect.hpp"

TintManager::TintManager(ControlContext* ctx)
    : ctx(ctx)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        _tintID = dynamic_cast<VRRender*>(ctx->controller()->render())->addPostProcessEffect<RenderVRTintEffect>(vec3(), 0);
    }
    else
    {
        _tintID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addPostProcessEffect<RenderFlatTintEffect>(vec3(), 0);
    }
#else
    _tintID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addPostProcessEffect<RenderFlatTintEffect>(vec3(), 0);
#endif

    ctx->controller()->render()->setPostProcessEffectActive(_tintID, false);
}

void TintManager::set(const vec3& color, f64 strength)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        dynamic_cast<VRRender*>(ctx->controller()->render())->updatePostProcessEffect<RenderVRTintEffect>(
            _tintID,
            color,
            strength
        );
    }
    else
    {
        dynamic_cast<FlatRender*>(ctx->controller()->render())->updatePostProcessEffect<RenderFlatTintEffect>(
            _tintID,
            color,
            strength
        );
    }
#else
    dynamic_cast<FlatRender*>(ctx->controller()->render())->updatePostProcessEffect<RenderFlatTintEffect>(
        _tintID,
        color,
        strength
    );
#endif
}

RenderPostProcessEffectID TintManager::postProcessID() const
{
    return _tintID;
}
