/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "right_clickable_widget.hpp"

RightClickableWidget::RightClickableWidget(InterfaceWidget* parent)
    : InterfaceWidget(parent)
    , rightClickMenu(nullptr)
{

}

RightClickableWidget::~RightClickableWidget()
{
    if (rightClickMenu)
    {
        delete rightClickMenu;
        rightClickMenu = nullptr;
    }
}

void RightClickableWidget::move(const Point& point)
{
    if (rightClickMenu)
    {
        rightClickMenu->move(point + (rightClickMenu->position() - this->position()));
    }

    InterfaceWidget::move(point);
}

void RightClickableWidget::onRightClick(i32 index, const Point& pointer)
{
    openRightClickMenu(index, pointer);
}

void RightClickableWidget::closeRightClickMenu()
{
    delete rightClickMenu;
    rightClickMenu = nullptr;
}
