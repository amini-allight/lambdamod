/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "tab_view.hpp"
#include "game_settings.hpp"
#include "world_settings.hpp"
#include "user_settings.hpp"
#include "team_settings.hpp"
#include "worlds_settings.hpp"
#include "entity_settings.hpp"
#include "checkpoints_settings.hpp"
#include "context_settings.hpp"
#include "editing_settings.hpp"

static Size windowSize()
{
    // More tabs than it actually has, otherwise the window is too short
    return UIScale::windowBordersSize() + Size(846 * uiScale(), UIScale::tabSize().h * 12);
}

Settings::Settings(InterfaceView* view)
    : InterfaceWindow(view, localize("settings-settings"))
{
    START_WIDGET_SCOPE("settings")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-settings", dismiss)
    END_SCOPE

    tabs = new TabView(
        this,
        {
            localize("settings-game"),
            localize("settings-world"),
            localize("settings-users"),
            localize("settings-teams"),
            localize("settings-worlds"),
            localize("settings-entities"),
            localize("settings-checkpoints"),
            localize("settings-context"),
            localize("settings-editing")
        },
        true
    );

    new GameSettings(tabs->tab(localize("settings-game")));

    new WorldSettings(tabs->tab(localize("settings-world")));

    new UserSettings(tabs->tab(localize("settings-users")));

    new TeamSettings(tabs->tab(localize("settings-teams")));

    new WorldsSettings(tabs->tab(localize("settings-worlds")));

    new EntitySettings(tabs->tab(localize("settings-entities")));

    new CheckpointsSettings(tabs->tab(localize("settings-checkpoints")));

    new ContextSettings(tabs->tab(localize("settings-context")));

    new EditingSettings(tabs->tab(localize("settings-editing")));

    resize(windowSize());
}

void Settings::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());
}

void Settings::open(const Point& point, const string& tabName)
{
    open(point);

    tabs->open(tabName);
}

void Settings::dismiss(const Input& input)
{
    hide();
}

SizeProperties Settings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
