/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_occlusion_primitive.hpp"
#include "render_occlusion_mesh.hpp"
#include "tools.hpp"

static constexpr u32 occlusionEllipseSegments = 64;
static constexpr u32 occlusionSphereRings = 32;
static constexpr u32 occlusionSphereSegments = 64;
static constexpr u32 occlusionCylinderSegments = 64;
static constexpr u32 occlusionConeSegments = 64;
static constexpr u32 occlusionCapsuleRings = 32;
static constexpr u32 occlusionCapsuleSegments = 64;

RenderOcclusionPrimitive::RenderOcclusionPrimitive(const mat4& entityTransform, const BodyRenderOcclusionPrimitive& primitive)
    : entityTransform(entityTransform)
    , transform(primitive.transform)
{
    _uniformData.width = primitive.dimensions.x;
    _uniformData.length = primitive.dimensions.y;
    _uniformData.height = primitive.dimensions.z;
    _uniformData.type = static_cast<OcclusionPrimitiveType>(primitive.type);
}

void RenderOcclusionPrimitive::update(const mat4& entityTransform)
{
    this->entityTransform = entityTransform;
}

OcclusionPrimitiveGPU RenderOcclusionPrimitive::uniformData(const vec3& cameraPosition) const
{
    OcclusionPrimitiveGPU data = _uniformData;

    mat4 transform = entityTransform * this->transform;
    transform.setPosition(transform.position() - cameraPosition);
    data.transform[0] = transform;

    return data;
}

OcclusionPrimitiveGPU RenderOcclusionPrimitive::uniformData(vec3 cameraPosition[eyeCount]) const
{
    OcclusionPrimitiveGPU data = _uniformData;

    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        mat4 transform = entityTransform * this->transform;
        transform.setPosition(transform.position() - cameraPosition[eye]);
        data.transform[eye] = transform;
    }

    return data;
}

void RenderOcclusionPrimitive::toMesh(RenderOcclusionMesh& out) const
{
    vector<fvec3> vertices;
    vector<u32> indices;

    switch (_uniformData.type)
    {
    case Occlusion_Primitive_Ellipse :
        generateEllipse(_uniformData.width / 2, _uniformData.height / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Equilateral_Triangle :
        generateRegularPolygon(3, _uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Isosceles_Triangle :
        generateIsoscelesTriangle(_uniformData.width, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Right_Angle_Triangle :
        generateRightAngleTriangle(_uniformData.width, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Rectangle :
        generateRectangle(_uniformData.width, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Pentagon :
        generateRegularPolygon(5, _uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Hexagon :
        generateRegularPolygon(6, _uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Heptagon :
        generateRegularPolygon(7, _uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Octagon :
        generateRegularPolygon(8, _uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Cuboid :
        generateCuboid(_uniformData.width, _uniformData.length, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Sphere :
        generateSphere(_uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Cylinder :
        generateCylinder(_uniformData.width / 2, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Cone :
        generateCone(_uniformData.width / 2, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Capsule :
        generateCapsule(_uniformData.width / 2, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Pyramid :
        generatePyramid(_uniformData.width / 2, _uniformData.height, vertices, indices);
        break;
    case Occlusion_Primitive_Tetrahedron :
        generateTetrahedron(_uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Octahedron :
        generateOctahedron(_uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Dodecahedron :
        generateDodecahedron(_uniformData.width / 2, vertices, indices);
        break;
    case Occlusion_Primitive_Icosahedron :
        generateIcosahedron(_uniformData.width / 2, vertices, indices);
        break;
    }

    for (fvec3& vertex : vertices)
    {
        vertex = transform.applyToPosition(vertex);
    }

    u32 indexOffset = out.vertices.size();

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    out.vertices.insert(out.vertices.end(), vertices.begin(), vertices.end());
    out.indices.insert(out.indices.end(), indices.begin(), indices.end());
}

void RenderOcclusionPrimitive::generateEllipse(f32 xRadius, f32 yRadius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 center;
    vertices.push_back(center);

    for (u32 i = 0; i < occlusionEllipseSegments; i++)
    {
        fvec3 a(
            xRadius * cos(i * (tau / occlusionEllipseSegments)),
            yRadius * sin(i * (tau / occlusionEllipseSegments)),
            0
        );
        vertices.push_back(a);

        fvec3 b(
            xRadius * cos((i + 1) * (tau / occlusionEllipseSegments)),
            yRadius * sin((i + 1) * (tau / occlusionEllipseSegments)),
            0
        );
        vertices.push_back(b);

        indices.push_back(vertices.size() - 1);
        indices.push_back(vertices.size() - 2);
        indices.push_back(0);
    }
}

void RenderOcclusionPrimitive::generateRegularPolygon(u32 sides, f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 center;
    vertices.push_back(center);

    fvec3 pos(0, radius, 0);

    for (u32 i = 0; i < sides; i++)
    {
        vertices.push_back(fquaternion(upDir, i * (tau / sides)).rotate(pos));
        vertices.push_back(fquaternion(upDir, (i + 1) * (tau / sides)).rotate(pos));

        indices.push_back(vertices.size() - 1);
        indices.push_back(vertices.size() - 2);
        indices.push_back(0);
    }
}

void RenderOcclusionPrimitive::generateIsoscelesTriangle(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    vertices.push_back(fvec3(-(width / 2), 0, 0));
    vertices.push_back(fvec3(+(width / 2), 0, 0));
    vertices.push_back(fvec3(0, height, 0));

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
}

void RenderOcclusionPrimitive::generateRightAngleTriangle(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    vertices.push_back(fvec3());
    vertices.push_back(fvec3(width, 0, 0));
    vertices.push_back(fvec3(0, height, 0));

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);
}

void RenderOcclusionPrimitive::generateRectangle(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    vertices.push_back(fvec3(-(width / 2), -(height / 2), 0));
    vertices.push_back(fvec3(-(width / 2), +(height / 2), 0));
    vertices.push_back(fvec3(+(width / 2), -(height / 2), 0));
    vertices.push_back(fvec3(+(width / 2), +(height / 2), 0));

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(3);

    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);
}

void RenderOcclusionPrimitive::generateCuboid(f32 width, f32 length, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    vertices.push_back(fvec3(-(width / 2), +(length / 2), +(height / 2)));
    vertices.push_back(fvec3(+(width / 2), +(length / 2), +(height / 2)));
    vertices.push_back(fvec3(-(width / 2), -(length / 2), +(height / 2)));
    vertices.push_back(fvec3(+(width / 2), -(length / 2), +(height / 2)));

    vertices.push_back(fvec3(-(width / 2), +(length / 2), -(height / 2)));
    vertices.push_back(fvec3(+(width / 2), +(length / 2), -(height / 2)));
    vertices.push_back(fvec3(-(width / 2), -(length / 2), -(height / 2)));
    vertices.push_back(fvec3(+(width / 2), -(length / 2), -(height / 2)));

    u32 topFrontLeft = 0;
    u32 topFrontRight = 1;
    u32 topBackLeft = 2;
    u32 topBackRight = 3;

    u32 botFrontLeft = 4;
    u32 botFrontRight = 5;
    u32 botBackLeft = 6;
    u32 botBackRight = 7;

    // z
    indices.push_back(topBackLeft);
    indices.push_back(topFrontLeft);
    indices.push_back(topFrontRight);

    indices.push_back(topBackLeft);
    indices.push_back(topBackRight);
    indices.push_back(topFrontRight);

    indices.push_back(botBackLeft);
    indices.push_back(botFrontLeft);
    indices.push_back(botFrontRight);

    indices.push_back(botBackLeft);
    indices.push_back(botBackRight);
    indices.push_back(botFrontRight);

    // y
    indices.push_back(botFrontLeft);
    indices.push_back(botFrontRight);
    indices.push_back(topFrontRight);

    indices.push_back(botFrontLeft);
    indices.push_back(topFrontLeft);
    indices.push_back(topFrontRight);

    indices.push_back(botBackLeft);
    indices.push_back(botBackRight);
    indices.push_back(topBackRight);

    indices.push_back(botBackLeft);
    indices.push_back(topBackLeft);
    indices.push_back(topBackRight);

    // x
    indices.push_back(botBackLeft);
    indices.push_back(botFrontLeft);
    indices.push_back(topFrontLeft);

    indices.push_back(botBackLeft);
    indices.push_back(topBackLeft);
    indices.push_back(topFrontLeft);

    indices.push_back(botBackRight);
    indices.push_back(botFrontRight);
    indices.push_back(topFrontRight);

    indices.push_back(botBackRight);
    indices.push_back(topBackRight);
    indices.push_back(topFrontRight);
}

void RenderOcclusionPrimitive::generateSphere(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 botCenter(0, 0, -radius);
    vertices.push_back(botCenter);

    fvec3 topCenter(0, 0, +radius);
    vertices.push_back(topCenter);

    for (u32 ring = 1; ring < occlusionSphereRings; ring++)
    {
        fquaternion toRing(rightDir, pi * (ring / static_cast<f32>(occlusionSphereRings)));

        for (u32 segment = 0; segment < occlusionSphereSegments; segment++)
        {
            fquaternion toSegment(upDir, tau * (segment / static_cast<f32>(occlusionSphereSegments)));

            vertices.push_back(toSegment.rotate(toRing.rotate(topCenter)));

            u32 a = vertices.size() - 1;
            u32 b = vertices.size() + (segment + 1 != occlusionSphereSegments ? 0 : -occlusionSphereSegments);
            u32 c = a - occlusionSphereSegments;
            u32 d = b - occlusionSphereSegments;

            switch (ring)
            {
            case 1 :
                indices.push_back(1);
                indices.push_back(a);
                indices.push_back(b);
                break;
            case occlusionSphereRings - 1 :
                indices.push_back(0);
                indices.push_back(a);
                indices.push_back(b);
                [[fallthrough]];
            default :
                indices.push_back(a);
                indices.push_back(b);
                indices.push_back(d);

                indices.push_back(a);
                indices.push_back(c);
                indices.push_back(d);
                break;
            }
        }
    }
}

void RenderOcclusionPrimitive::generateCylinder(f32 radius, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 botCenter(0, 0, -(height / 2));
    vertices.push_back(botCenter);

    fvec3 topCenter(0, 0, +(height / 2));
    vertices.push_back(topCenter);

    fvec3 botPos(0, radius, -(height / 2));
    fvec3 topPos(0, radius, +(height / 2));

    for (u32 i = 0; i < occlusionCylinderSegments; i++)
    {
        fvec3 a = fquaternion(upDir, i * (tau / occlusionCylinderSegments)).rotate(botPos);
        vertices.push_back(a);

        fvec3 c = fquaternion(upDir, i * (tau / occlusionCylinderSegments)).rotate(topPos);
        vertices.push_back(c);

        u32 aIndex = vertices.size() - 2;
        u32 bIndex = i + 1 != occlusionCylinderSegments ? vertices.size() : 2;
        u32 cIndex = vertices.size() - 1;
        u32 dIndex = i + 1 != occlusionCylinderSegments ? vertices.size() + 1 : 3;

        indices.push_back(aIndex);
        indices.push_back(bIndex);
        indices.push_back(0);

        indices.push_back(aIndex);
        indices.push_back(bIndex);
        indices.push_back(dIndex);

        indices.push_back(aIndex);
        indices.push_back(cIndex);
        indices.push_back(dIndex);

        indices.push_back(cIndex);
        indices.push_back(dIndex);
        indices.push_back(1);
    }
}

void RenderOcclusionPrimitive::generateCone(f32 radius, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 botCenter;
    vertices.push_back(botCenter);

    fvec3 topCenter(0, 0, height);
    vertices.push_back(topCenter);

    fvec3 pos(0, radius, 0);

    for (u32 i = 0; i < occlusionConeSegments; i++)
    {
        fvec3 a = fquaternion(upDir, i * (tau / occlusionConeSegments)).rotate(pos);
        vertices.push_back(a);

        u32 aIndex = vertices.size() - 1;
        u32 bIndex = i + 1 != occlusionConeSegments ? vertices.size() : 2;

        indices.push_back(aIndex);
        indices.push_back(bIndex);
        indices.push_back(0);

        indices.push_back(aIndex);
        indices.push_back(bIndex);
        indices.push_back(1);
    }
}

void RenderOcclusionPrimitive::generateCapsule(f32 radius, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 bot(0, 0, -((height / 2) + radius));
    vertices.push_back(bot);

    fvec3 top(0, 0, +((height / 2) + radius));
    vertices.push_back(top);

    fvec3 botPivot(0, 0, -height / 2);
    fvec3 topPivot(0, 0, +height / 2);

    fvec3 start(0, 0, radius);

    for (u32 ring = 1; ring < occlusionCapsuleRings + 1; ring++)
    {
        u32 effectiveRing = ring > occlusionCapsuleRings / 2 ? ring - 1 : ring;

        fmat4 toRing(
            ring <= occlusionCapsuleRings / 2 ? topPivot : botPivot,
            fquaternion(rightDir, pi * (effectiveRing / static_cast<f32>(occlusionSphereRings))),
            fvec3(1)
        );

        for (u32 segment = 0; segment < occlusionCapsuleSegments; segment++)
        {
            fquaternion toSegment(upDir, tau * (segment / static_cast<f32>(occlusionSphereSegments)));

            vertices.push_back(toSegment.rotate(toRing.applyToPosition(start)));

            u32 a = vertices.size() - 1;
            u32 b = vertices.size() + (segment + 1 != occlusionSphereSegments ? 0 : -occlusionSphereSegments);
            u32 c = a - occlusionSphereSegments;
            u32 d = b - occlusionSphereSegments;

            switch (ring)
            {
            case 1 :
                indices.push_back(1);
                indices.push_back(a);
                indices.push_back(b);
                break;
            case occlusionSphereRings :
                indices.push_back(0);
                indices.push_back(a);
                indices.push_back(b);
                [[fallthrough]];
            default :
                indices.push_back(a);
                indices.push_back(b);
                indices.push_back(d);

                indices.push_back(a);
                indices.push_back(c);
                indices.push_back(d);
                break;
            }
        }
    }
}

void RenderOcclusionPrimitive::generatePyramid(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const
{
    u32 offset = vertices.size();

    vertices.push_back(fvec3(0, 0, height));
    vertices.push_back(fvec3(+width / 2, +width / 2, 0));
    vertices.push_back(fvec3(+width / 2, -width / 2, 0));
    vertices.push_back(fvec3(-width / 2, +width / 2, 0));
    vertices.push_back(fvec3(-width / 2, -width / 2, 0));

    indices.push_back(offset + 0);
    indices.push_back(offset + 2);
    indices.push_back(offset + 1);

    indices.push_back(offset + 0);
    indices.push_back(offset + 4);
    indices.push_back(offset + 2);

    indices.push_back(offset + 0);
    indices.push_back(offset + 3);
    indices.push_back(offset + 4);

    indices.push_back(offset + 0);
    indices.push_back(offset + 1);
    indices.push_back(offset + 3);

    indices.push_back(offset + 1);
    indices.push_back(offset + 2);
    indices.push_back(offset + 4);

    indices.push_back(offset + 1);
    indices.push_back(offset + 4);
    indices.push_back(offset + 3);
}

void RenderOcclusionPrimitive::generateTetrahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 ringStart = fquaternion(leftDir, radians(120)).rotate(fvec3(0, 0, radius));

    u32 offset = vertices.size();

    vertices.push_back(fvec3(0, 0, radius));
    vertices.push_back(ringStart);
    vertices.push_back(fquaternion(upDir, radians(+120)).rotate(ringStart));
    vertices.push_back(fquaternion(upDir, radians(-120)).rotate(ringStart));

    indices.push_back(offset + 0);
    indices.push_back(offset + 1);
    indices.push_back(offset + 2);

    indices.push_back(offset + 0);
    indices.push_back(offset + 3);
    indices.push_back(offset + 1);

    indices.push_back(offset + 0);
    indices.push_back(offset + 2);
    indices.push_back(offset + 3);

    indices.push_back(offset + 1);
    indices.push_back(offset + 3);
    indices.push_back(offset + 2);
}

void RenderOcclusionPrimitive::generateOctahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    u32 offset = vertices.size();

    vertices.push_back(fvec3(+radius, 0, 0));
    vertices.push_back(fvec3(-radius, 0, 0));
    vertices.push_back(fvec3(0, +radius, 0));
    vertices.push_back(fvec3(0, -radius, 0));
    vertices.push_back(fvec3(0, 0, +radius));
    vertices.push_back(fvec3(0, 0, -radius));

    indices.push_back(offset + 0);
    indices.push_back(offset + 2);
    indices.push_back(offset + 4);

    indices.push_back(offset + 0);
    indices.push_back(offset + 4);
    indices.push_back(offset + 3);

    indices.push_back(offset + 0);
    indices.push_back(offset + 3);
    indices.push_back(offset + 5);

    indices.push_back(offset + 0);
    indices.push_back(offset + 5);
    indices.push_back(offset + 2);

    indices.push_back(offset + 1);
    indices.push_back(offset + 4);
    indices.push_back(offset + 2);

    indices.push_back(offset + 1);
    indices.push_back(offset + 3);
    indices.push_back(offset + 4);

    indices.push_back(offset + 1);
    indices.push_back(offset + 5);
    indices.push_back(offset + 3);

    indices.push_back(offset + 1);
    indices.push_back(offset + 2);
    indices.push_back(offset + 5);
}

static void assemblePentagon(u32 offset, u32 a, u32 b, u32 c, u32 d, u32 e, vector<u32>& indices)
{
    indices.push_back(offset + a);
    indices.push_back(offset + b);
    indices.push_back(offset + c);

    indices.push_back(offset + a);
    indices.push_back(offset + c);
    indices.push_back(offset + d);

    indices.push_back(offset + d);
    indices.push_back(offset + e);
    indices.push_back(offset + a);
}

void RenderOcclusionPrimitive::generateDodecahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 upperRingStart = radius * fvec3(+dodecahedronRingStart);
    fvec3 upperMiddleRingStart = radius * fvec3(+dodecahedronMiddleRingStart);
    fvec3 lowerMiddleRingStart = radius * fvec3(-dodecahedronMiddleRingStart);
    fvec3 lowerRingStart = radius * fvec3(-dodecahedronRingStart);

    u32 offset = vertices.size();

    vertices.push_back(upperRingStart);
    vertices.push_back(fquaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart));
    vertices.push_back(fquaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart));
    vertices.push_back(fquaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart));
    vertices.push_back(fquaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart));

    vertices.push_back(upperMiddleRingStart);
    vertices.push_back(fquaternion(upDir, 1 * (tau / 5)).rotate(upperMiddleRingStart));
    vertices.push_back(fquaternion(upDir, 2 * (tau / 5)).rotate(upperMiddleRingStart));
    vertices.push_back(fquaternion(upDir, 3 * (tau / 5)).rotate(upperMiddleRingStart));
    vertices.push_back(fquaternion(upDir, 4 * (tau / 5)).rotate(upperMiddleRingStart));

    vertices.push_back(lowerMiddleRingStart);
    vertices.push_back(fquaternion(upDir, 1 * (tau / 5)).rotate(lowerMiddleRingStart));
    vertices.push_back(fquaternion(upDir, 2 * (tau / 5)).rotate(lowerMiddleRingStart));
    vertices.push_back(fquaternion(upDir, 3 * (tau / 5)).rotate(lowerMiddleRingStart));
    vertices.push_back(fquaternion(upDir, 4 * (tau / 5)).rotate(lowerMiddleRingStart));

    vertices.push_back(lowerRingStart);
    vertices.push_back(fquaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart));
    vertices.push_back(fquaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart));
    vertices.push_back(fquaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart));
    vertices.push_back(fquaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart));

    assemblePentagon(offset, 0, 1, 2, 3, 4, indices);

    assemblePentagon(offset, 5, 13, 6, 1, 0, indices);
    assemblePentagon(offset, 6, 14, 7, 2, 1, indices);
    assemblePentagon(offset, 7, 10, 8, 3, 2, indices);
    assemblePentagon(offset, 8, 11, 9, 4, 3, indices);
    assemblePentagon(offset, 9, 12, 5, 0, 4, indices);

    assemblePentagon(offset, 15, 16, 11, 8, 10, indices);
    assemblePentagon(offset, 16, 17, 12, 9, 11, indices);
    assemblePentagon(offset, 17, 18, 13, 5, 12, indices);
    assemblePentagon(offset, 18, 19, 14, 6, 13, indices);
    assemblePentagon(offset, 19, 15, 11, 7, 14, indices);

    assemblePentagon(offset, 19, 18, 17, 16, 15, indices);
}

void RenderOcclusionPrimitive::generateIcosahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const
{
    fvec3 upperRingStart = radius * fvec3(+icosahedronRingStart);
    fvec3 lowerRingStart = radius * fvec3(-icosahedronRingStart);

    u32 offset = vertices.size();

    vertices.push_back(fvec3(0, 0, +radius));
    vertices.push_back(fvec3(0, 0, -radius));

    vertices.push_back(upperRingStart);
    vertices.push_back(fquaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart));
    vertices.push_back(fquaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart));
    vertices.push_back(fquaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart));
    vertices.push_back(fquaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart));

    vertices.push_back(lowerRingStart);
    vertices.push_back(fquaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart));
    vertices.push_back(fquaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart));
    vertices.push_back(fquaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart));
    vertices.push_back(fquaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart));

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(offset + 0);
        indices.push_back(offset + 2 + i);
        indices.push_back(offset + 2 + loopIndex<u32>(i + 1, 5));
    }

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(offset + 1);
        indices.push_back(offset + 7 + loopIndex<u32>(i + 1, 5));
        indices.push_back(offset + 7 + i);
    }

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(offset + 2 + i);
        indices.push_back(offset + 7 + loopIndex<u32>(3 + i, 5));
        indices.push_back(offset + 2 + loopIndex<u32>(i + 1, 5));
    }

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(offset + 7 + i);
        indices.push_back(offset + 7 + loopIndex<u32>(i + 1, 5));
        indices.push_back(offset + 2 + loopIndex<u32>(3 + i, 5));
    }
}
