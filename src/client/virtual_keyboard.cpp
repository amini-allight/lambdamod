/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "virtual_keyboard.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "control_context.hpp"

VirtualKeyboard::VirtualKeyboard(InterfaceWidget* parent)
    : Layout(parent)
    , shift(false)
    , capsLock(false)
{
    init();
}

VirtualKeyboard::VirtualKeyboard(InterfaceView* view)
    : Layout(view)
    , shift(false)
    , capsLock(false)
{
    init();
}

void VirtualKeyboard::move(const Point& position)
{
    this->position(position);

    f64 y = 0;
    for (const vector<VirtualKeyboardKey*>& row : rows)
    {
        f64 totalWidth = 0;
        for (VirtualKeyboardKey* key : row)
        {
            totalWidth += key->width();
        }

        f64 x = 0;
        for (VirtualKeyboardKey* key : row)
        {
            vec2 fractionalPosition(x, y);

            key->move(position + Point(fractionalPosition * size().toVec2()));

            x += key->width() / totalWidth;
        }

        y += 1.0 / rows.size();
    }
}

void VirtualKeyboard::resize(const Size& size)
{
    this->size(size);

    for (const vector<VirtualKeyboardKey*>& row : rows)
    {
        f64 totalWidth = 0;
        for (VirtualKeyboardKey* key : row)
        {
            totalWidth += key->width();
        }

        for (VirtualKeyboardKey* key : row)
        {
            vec2 fractionalSize(key->width() / totalWidth, 1.0 / rows.size());

            key->resize(Size(fractionalSize * this->size().toVec2()));
        }
    }
}

void VirtualKeyboard::init()
{
    rows = {
        {
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "`" : "~"; }, Input_Grave_Accent),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "1" : "!"; }, Input_1),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "2" : "@"; }, Input_2),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "3" : "#"; }, Input_3),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "4" : "$"; }, Input_4),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "5" : "%"; }, Input_5),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "6" : "^"; }, Input_6),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "7" : "&"; }, Input_7),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "8" : "*"; }, Input_8),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "9" : "("; }, Input_9),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "0" : ")"; }, Input_0),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "-" : "_"; }, Input_Minus),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "=" : "+"; }, Input_Equals),
            new VirtualKeyboardKey(this, [&](){ return "Backspace"; }, Input_Backspace, 3)
        },
        {
            new VirtualKeyboardKey(this, [&](){ return "Tab"; }, Input_Tab, 2),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "q" : "Q"; }, Input_Q),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "w" : "W"; }, Input_W),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "e" : "E"; }, Input_E),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "r" : "R"; }, Input_R),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "t" : "T"; }, Input_T),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "y" : "Y"; }, Input_Y),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "u" : "U"; }, Input_U),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "i" : "I"; }, Input_I),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "o" : "O"; }, Input_O),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "p" : "P"; }, Input_P),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "[" : "{"; }, Input_Left_Brace),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "]" : "}"; }, Input_Right_Brace),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "\\" : "|"; }, Input_Backslash, 2)
        },
        {
            new VirtualKeyboardKey(this, [&](){ return "Caps Lock"; }, Input_Caps_Lock, 2.5),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "a" : "A"; }, Input_A),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "s" : "S"; }, Input_S),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "d" : "D"; }, Input_D),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "f" : "F"; }, Input_F),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "g" : "G"; }, Input_G),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "h" : "H"; }, Input_H),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "j" : "J"; }, Input_J),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "k" : "K"; }, Input_K),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "l" : "L"; }, Input_L),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? ";" : ":"; }, Input_Semicolon),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "'" : "\""; }, Input_Quote),
            new VirtualKeyboardKey(this, [&](){ return "Return"; }, Input_Return, 2.5)
        },
        {
            new VirtualKeyboardKey(this, [&](){ return "Shift"; }, Input_Left_Shift, 3),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "z" : "Z"; }, Input_Z),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "x" : "X"; }, Input_X),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "c" : "C"; }, Input_C),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "v" : "V"; }, Input_V),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "b" : "B"; }, Input_B),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "n" : "N"; }, Input_N),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "m" : "M"; }, Input_M),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "," : "<"; }, Input_Comma),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "." : ">"; }, Input_Period),
            new VirtualKeyboardKey(this, [&](){ return !shifted() ? "/" : "?"; }, Input_Slash),
            new VirtualKeyboardKey(this, [&](){ return "Shift"; }, Input_Right_Shift, 3)
        },
        {
            new VirtualKeyboardKey(this, [&](){ return "Space"; }, Input_Space)
        }
    };
}

bool VirtualKeyboard::shifted() const
{
    return shift || capsLock;
}

void VirtualKeyboard::pressKey(InputType input)
{
    if (currentTime() - lastPressTime < minButtonInterval)
    {
        return;
    }

    switch (input)
    {
    case Input_Left_Shift :
    case Input_Right_Shift :
        shift = !shift;
        break;
    case Input_Caps_Lock :
        capsLock = !capsLock;
        break;
    default :
    {
        Input virtualDown;
        virtualDown.type = input;
        virtualDown.shift = shifted();
        context()->input()->handle(virtualDown);

        Input virtualUp;
        virtualUp.type = input;
        virtualUp.up = true;
        virtualUp.shift = shifted();
        context()->input()->handle(virtualUp);
        break;
    }
    }

    lastPressTime = currentTime();
}

SizeProperties VirtualKeyboard::sizeProperties() const
{
    return sizePropertiesFillAll;
}
