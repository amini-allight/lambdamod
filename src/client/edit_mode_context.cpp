/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "edit_mode_context.hpp"
#include "tools.hpp"
#include "interface_constants.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "translate_tool.hpp"
#include "rotate_tool.hpp"
#include "scale_tool.hpp"
#include "new_prefab_dialog.hpp"
#include "flat_render.hpp"
#include "render_gizmo.hpp"

EditModeContext::EditModeContext(ViewerModeContext* ctx)
    : ctx(ctx)
    , _selecting(false)
    , _specialEditMode(Special_Edit_None)
    , _tool(nullptr)
    , firstTransform(false)
{
#ifdef LMOD_VR
    if (!g_vr)
    {
        gizmoID = render()->addDecoration<RenderGizmo>(
            vec3(),
            quaternion(),
            array<bool, 9>({ false, false, false, false, false, false, false, false, false })
        );
        hideGizmo();
    }
#else
    gizmoID = render()->addDecoration<RenderGizmo>(
        vec3(),
        quaternion(),
        array<bool, 9>({ false, false, false, false, false, false, false, false, false })
    );
    hideGizmo();
#endif
}

EditModeContext::~EditModeContext()
{
    if (_tool)
    {
        delete _tool;
    }

#ifdef LMOD_VR
    if (!g_vr)
    {
        render()->removeDecoration(gizmoID);
    }
#else
    render()->removeDecoration(gizmoID);
#endif
}

void EditModeContext::step()
{
    updateGizmo();
}

void EditModeContext::enter()
{
    updateGizmo();
}

void EditModeContext::exit()
{
    cancelTool();
    cancelSelect();
    hideGizmo();
}

bool EditModeContext::toolActive() const
{
    return _tool;
}

string EditModeContext::toolName() const
{
    if (!_tool)
    {
        return "";
    }

    return _tool->name();
}

string EditModeContext::toolAxisName() const
{
    if (!_tool)
    {
        return "";
    }

    return _tool->axisName();
}

string EditModeContext::toolPivotName() const
{
    if (!_tool)
    {
        return "";
    }

    return _tool->pivotName();
}

string EditModeContext::extent() const
{
    if (!_tool)
    {
        return "";
    }

    return _tool->extent();
}

void EditModeContext::cancelTool()
{
    if (!_tool)
    {
        return;
    }

    _tool->cancel();
    delete _tool;
    _tool = nullptr;
}

void EditModeContext::endTool()
{
    if (!_tool)
    {
        return;
    }

    _tool->end();
    delete _tool;
    _tool = nullptr;
}

void EditModeContext::useTool(const Point& point)
{
    if (!_tool)
    {
        return;
    }

    _tool->use(point);
}

const EditTool* EditModeContext::tool() const
{
    return _tool;
}

void EditModeContext::addToolNumericInput(char c)
{
    if (!_tool)
    {
        return;
    }

    _tool->addNumericInput(c);
}

void EditModeContext::removeToolNumericInput()
{
    if (!_tool)
    {
        return;
    }

    _tool->removeNumericInput();
}

void EditModeContext::toggleToolNumericInputSign()
{
    if (!_tool)
    {
        return;
    }

    _tool->toggleNumericInputSign();
}

void EditModeContext::ensureToolNumericInputPeriod()
{
    if (!_tool)
    {
        return;
    }

    _tool->ensureNumericInputPeriod();
}

bool EditModeContext::snap() const
{
    if (!_tool)
    {
        return false;
    }

    return _tool->snapping();
}

void EditModeContext::startSnap()
{
    if (!_tool)
    {
        return;
    }

    _tool->startSnap();
}

void EditModeContext::endSnap()
{
    if (!_tool)
    {
        return;
    }

    _tool->endSnap();
}

void EditModeContext::togglePivotMode(const Point& point)
{
    if (!_tool)
    {
        return;
    }

    _tool->togglePivotMode(point);
}

void EditModeContext::toggleX()
{
    if (!_tool)
    {
        return;
    }

    _tool->toggleX();
}

void EditModeContext::toggleY()
{
    if (!_tool)
    {
        return;
    }

    _tool->toggleY();
}

void EditModeContext::toggleZ()
{
    if (!_tool)
    {
        return;
    }

    _tool->toggleZ();
}

void EditModeContext::addNewPrefab()
{
    ctx->context()->interface()->addWindow<NewPrefabDialog>();
}

void EditModeContext::startSelect(const Point& point)
{
    if (gizmoShown() && gizmoSelectable(point) && selectGizmo(point))
    {
        return;
    }

    _selecting = true;
    _selectStart = point;
    _selectEnd = point;
}

void EditModeContext::cancelSelect()
{
    _selecting = false;
}

void EditModeContext::endSelect(const Point& point, SelectMode mode)
{
    if (!_selecting)
    {
        return;
    }

    _selecting = false;
    _selectEnd = point;

    bool box = sqrt(sq(_selectEnd.x - _selectStart.x) + sq(_selectEnd.y - _selectStart.y)) >= (UIScale::minBoxSelectSize() / static_cast<f64>(render()->height()));

    i32 startX = _selectStart.x < _selectEnd.x ? _selectStart.x : _selectEnd.x;
    i32 startY = _selectStart.y < _selectEnd.y ? _selectStart.y : _selectEnd.y;

    i32 endX = _selectStart.x > _selectEnd.x ? _selectStart.x : _selectEnd.x;
    i32 endY = _selectStart.y > _selectEnd.y ? _selectStart.y : _selectEnd.y;

    if (box)
    {
        makeSelections(Point(startX, startY), Point(endX, endY), mode);
    }
    else
    {
        makeSelections(_selectEnd, mode);
    }
}

void EditModeContext::moveSelect(const Point& point)
{
    if (!_selecting)
    {
        return;
    }

    _selectEnd = point;
}

bool EditModeContext::selecting() const
{
    return _selecting;
}

const Point& EditModeContext::selectStart() const
{
    return _selectStart;
}

const Point& EditModeContext::selectEnd() const
{
    return _selectEnd;
}

void EditModeContext::startSpecialEdit(const Point& point)
{
    _specialEditMode = Special_Edit_Default;
    firstTransform = true;

    specialEdit(point);
}

void EditModeContext::startAltSpecialEdit(const Point& point)
{
    _specialEditMode = Special_Edit_Alt;
    firstTransform = true;

    specialEdit(point);
}

void EditModeContext::endSpecialEdit(const Point& point)
{
    specialEdit(point);

    _specialEditMode = Special_Edit_None;
}

void EditModeContext::moveSpecialEdit(const Point& point)
{
    if (!specialEditing())
    {
        return;
    }

    specialEdit(point);
}

bool EditModeContext::specialEditing() const
{
    return _specialEditMode != Special_Edit_None;
}

SpecialEditMode EditModeContext::specialEditMode() const
{
    return _specialEditMode;
}

bool EditModeContext::canvasPainting() const
{
    return false;
}

void EditModeContext::translate(const Point& point, bool soft)
{
    if (_tool)
    {
        return;
    }

    firstTransform = true;
    _tool = new TranslateTool(
        ctx,
        transformToolSpace(),
        bind(&EditModeContext::transformSet, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4),
        bind(&EditModeContext::transformGet, this, placeholders::_1),
        bind(&EditModeContext::transformGetWithPromotion, this, placeholders::_1),
        bind(&EditModeContext::transformCount, this),
        soft,
        point
    );
}

void EditModeContext::rotate(const Point& point, bool soft)
{
    if (_tool)
    {
        return;
    }

    firstTransform = true;
    _tool = new RotateTool(
        ctx,
        transformToolSpace(),
        bind(&EditModeContext::transformSet, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4),
        bind(&EditModeContext::transformGet, this, placeholders::_1),
        bind(&EditModeContext::transformGetWithPromotion, this, placeholders::_1),
        bind(&EditModeContext::transformCount, this),
        soft,
        point
    );
}

void EditModeContext::scale(const Point& point, bool soft)
{
    if (_tool)
    {
        return;
    }

    firstTransform = true;
    _tool = new ScaleTool(
        ctx,
        transformToolSpace(),
        bind(&EditModeContext::transformSet, this, placeholders::_1, placeholders::_2, placeholders::_3, placeholders::_4),
        bind(&EditModeContext::transformGet, this, placeholders::_1),
        bind(&EditModeContext::transformGetWithPromotion, this, placeholders::_1),
        bind(&EditModeContext::transformCount, this),
        soft,
        point
    );
}

void EditModeContext::specialEdit(const Point& point)
{
    // Do nothing
}

void EditModeContext::updateGizmo()
{
    // When invoked in the constructor this is called before the world exists, causing EntityModeContext::selection to fail
    bool shown = gizmoShown();

    if (shown)
    {
        bool xTranslation = true;
        bool yTranslation = true;
        bool zTranslation = true;

        bool xRotation = true;
        bool yRotation = true;
        bool zRotation = true;

        bool xScale = true;
        bool yScale = true;
        bool zScale = true;

        if (_tool)
        {
            auto translateTool = dynamic_cast<const TranslateTool*>(_tool);

            xTranslation = translateTool && translateTool->axisX();
            yTranslation = translateTool && translateTool->axisY();
            zTranslation = translateTool && translateTool->axisZ();

            auto rotateTool = dynamic_cast<const RotateTool*>(_tool);

            xRotation = rotateTool && rotateTool->axisX();
            yRotation = rotateTool && rotateTool->axisY();
            zRotation = rotateTool && rotateTool->axisZ();

            auto scaleTool = dynamic_cast<const ScaleTool*>(_tool);

            xScale = scaleTool && scaleTool->axisX();
            yScale = scaleTool && scaleTool->axisY();
            zScale = scaleTool && scaleTool->axisZ();
        }

        render()->updateDecoration<RenderGizmo>(
            gizmoID,
            selectionCenter(),
            quaternion(),
            array<bool, 9>({
                xTranslation,
                yTranslation,
                zTranslation,
                xRotation,
                yRotation,
                zRotation,
                xScale,
                yScale,
                zScale
            })
        );
    }

    render()->setDecorationShown(gizmoID, shown);
}

void EditModeContext::hideGizmo()
{
    render()->setDecorationShown(gizmoID, false);
}

bool EditModeContext::selectGizmo(const Point& point)
{
    optional<tuple<TransformMode, EditToolAxis>> selection = gizmoIntersector.select(
        selectionCenter(),
        quaternion(),
        ctx->context()->flatViewer()->position(),
        ctx->context()->flatViewer()->screenToWorld(point)
    );

    if (!selection)
    {
        return false;
    }

    const auto& [ mode, axis ] = *selection;

    switch (mode)
    {
    default :
        return false;
    case Transform_Translate :
        translate(point, true);
        break;
    case Transform_Rotate :
        rotate(point, true);
        break;
    case Transform_Scale :
        scale(point, true);
        break;
    }

    _tool->setAxis(axis);

    return true;
}

mat4 EditModeContext::transformGetWithPromotion(size_t i) const
{
    return transformGet(i);
}

FlatRender* EditModeContext::render() const
{
    return dynamic_cast<FlatRender*>(ctx->context()->controller()->render());
}
