/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_draw.hpp"
#include "interface_constants.hpp"
#include "interface_draw_objects.hpp"
#include "controller.hpp"
#include "render_tools.hpp"
#include "render_descriptor_set_write_components.hpp"
#include "image.hpp"
#include "constants.hpp"
#include "paths.hpp"
#include "log.hpp"
#include "symbol_resolver.hpp"
#include "flat_render.hpp"

static VkDevice device;
static VmaAllocator gpuAllocator;

#pragma pack(1)
struct SolidUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 color;
};

struct RoundedBorderUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 color;
    f32 width;
    f32 radius;
    fvec2 screenSize;
};

struct ImageUniform
{
    fvec2 position;
    fvec2 size;
    f32 alpha;
    f32 rotation;
};

struct ColoredImageUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 color;
    f32 rotation;
};

struct SymbolUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 color;
    f32 rotation;
};

struct Gradient1DUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 startColor;
    fvec4 endColor;
};

struct Gradient2DUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 startColor;
    fvec4 endColor;
};

struct CurveUniform
{
    fvec2 p0;
    fvec2 p1;
    fvec2 p2;
    fvec2 p3;
    fvec4 color;
    fvec2 screenSize;
    f32 width;
};

struct RoundedSolidUniform
{
    fvec2 position;
    fvec2 size;
    fvec4 color;
    fvec2 screenSize;
    f32 radius;
};

struct LineUniform
{
    fvec2 start;
    fvec2 end;
    fvec4 color;
    fvec2 screenSize;
};

struct DashedLineUniform
{
    fvec2 start;
    fvec2 end;
    fvec4 color;
    fvec2 screenSize;
};
#pragma pack()

DrawSupportKey::DrawSupportKey()
{
    type = Draw_Support_Solid;
}

bool DrawSupportKey::operator==(const DrawSupportKey& rhs) const
{
    if (type != rhs.type)
    {
        return false;
    }

    switch (type)
    {
    default :
        return false;
    case Draw_Support_Solid :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && colorA == rhs.colorA;
    case Draw_Support_Rounded_Border :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && colorA == rhs.colorA && width == rhs.width && radius == rhs.radius;
    case Draw_Support_Image :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && image == rhs.image && alpha == rhs.alpha && rotation == rhs.rotation;
    case Draw_Support_Colored_Image :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && image == rhs.image && colorA == rhs.colorA && rotation == rhs.rotation;
    case Draw_Support_Symbol :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && symbol == rhs.symbol && colorA == rhs.colorA && rotation == rhs.rotation;
    case Draw_Support_Text :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && text == rhs.text && style == rhs.style && width == rhs.width;
    case Draw_Support_Gradient_1D :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && colorA == rhs.colorA && colorB == rhs.colorB;
    case Draw_Support_Gradient_2D :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && colorA == rhs.colorA && colorB == rhs.colorB;
    case Draw_Support_Curve :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && pointB == rhs.pointB  && pointC == rhs.pointC && pointD == rhs.pointD && width == rhs.width && colorA == rhs.colorA;
    case Draw_Support_Rounded_Solid :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && size == rhs.size && colorA == rhs.colorA && radius == rhs.radius;
    case Draw_Support_Line :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && pointB == rhs.pointB && colorA == rhs.colorA;
    case Draw_Support_Dashed_Line :
        return screenSize == rhs.screenSize && pointA == rhs.pointA && pointB == rhs.pointB && colorA == rhs.colorA;
    }
}

bool DrawSupportKey::operator!=(const DrawSupportKey& rhs) const
{
    return !(*this == rhs);
}

DrawSupportKey solidSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Color color
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Solid;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.colorA = color;

    return key;
}

DrawSupportKey roundedBorderSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    i32 width,
    i32 radius,
    Color color
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Rounded_Border;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.colorA = color;
    key.width = width;
    key.radius = radius;

    return key;
}

DrawSupportKey imageSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Image* rawImage,
    f64 alpha,
    f64 rotation
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Image;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.image = rawImage;
    key.alpha = alpha;
    key.rotation = rotation;

    return key;
}

DrawSupportKey coloredImageSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Image* rawImage,
    Color color,
    f64 rotation
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Colored_Image;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.image = rawImage;
    key.colorA = color;
    key.rotation = rotation;

    return key;
}

DrawSupportKey symbolSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    SymbolType symbol,
    Color color,
    f64 rotation
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Symbol;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.symbol = symbol;
    key.colorA = color;
    key.rotation = rotation;

    return key;
}

DrawSupportKey textSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    const string& text,
    TextStyle style,
    i32 marginSize
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Text;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.text = text;
    key.style = style;
    key.width = marginSize;

    return key;
}

DrawSupportKey gradient1DSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Gradient_1D;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.colorA = start;
    key.colorB = end;

    return key;
}

DrawSupportKey gradient2DSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Gradient_2D;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.colorA = start;
    key.colorB = end;

    return key;
}

DrawSupportKey curveSupportKey(
    const Size& screenSize,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    Color color,
    f64 width
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Curve;
    key.screenSize = screenSize;
    key.pointA = p0;
    key.pointB = p1;
    key.pointC = p2;
    key.pointD = p3;
    key.colorA = color;
    key.width = width;

    return key;
}

DrawSupportKey roundedSolidSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    f64 radius,
    Color color
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Rounded_Solid;
    key.screenSize = screenSize;
    key.pointA = point;
    key.size = size;
    key.radius = radius;
    key.colorA = color;

    return key;
}

DrawSupportKey lineSupportKey(
    const Size& screenSize,
    const Point& start,
    const Point& end,
    Color color
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Line;
    key.screenSize = screenSize;
    key.pointA = start;
    key.pointB = end;
    key.colorA = color;

    return key;
}

DrawSupportKey dashedLineSupportKey(
    const Size& screenSize,
    const Point& start,
    const Point& end,
    Color color
)
{
    DrawSupportKey key;

    key.type = Draw_Support_Dashed_Line;
    key.screenSize = screenSize;
    key.pointA = start;
    key.pointB = end;
    key.colorA = color;

    return key;
}

DrawTexture::DrawTexture()
{
    key.isText = false;
    key.image = nullptr;

    image.image = nullptr;
    image.allocation = nullptr;
    imageView = nullptr;

    _marked = false;
}

DrawTexture::~DrawTexture()
{
    if (imageView != nullptr)
    {
        vkDestroyImageView(device, imageView, nullptr);
    }

    if (image.image != nullptr)
    {
        vmaDestroyImage(gpuAllocator, image.image, image.allocation);
    }
}

void DrawTexture::mark()
{
    _marked = true;
}

void DrawTexture::clearMark()
{
    _marked = false;
}

bool DrawTexture::marked() const
{
    return _marked;
}

bool DrawTexture::matches(Image* image) const
{
    return !key.isText && key.image == image;
}

bool DrawTexture::matches(const string& text, const TextStyle& style) const
{
    return key.isText && key.text == text && key.style == style;
}

DrawSupport::DrawSupport()
{
    descriptorSet = nullptr;
    buffer.buffer = nullptr;
    buffer.allocation = nullptr;
    texture = nullptr;

    _marked = false;
}

DrawSupport::~DrawSupport()
{
    if (buffer.buffer != nullptr)
    {
        destroyBuffer(gpuAllocator, buffer);
    }

    if (descriptorSet != nullptr)
    {
        destroyDescriptorSet(device, descriptorPool, descriptorSet);
    }
}

void DrawSupport::mark()
{
    _marked = true;

    if (texture)
    {
        texture->mark();
    }
}

void DrawSupport::clearMark()
{
    _marked = false;
}

bool DrawSupport::marked() const
{
    return _marked;
}

static struct {
    Image* none = nullptr;
    Image* checked = nullptr;
    Image* icon = nullptr;
    Image* title = nullptr;
    Image* aminiAllight = nullptr;
    Image* spinner = nullptr;
    Image* saving = nullptr;
    Image* parent = nullptr;
    Image* active = nullptr;
    Image* inactive = nullptr;
    Image* shared = nullptr;
    Image* attached = nullptr;
    Image* host = nullptr;
    Image* text = nullptr;
    Image* timeout = nullptr;
    Image* user = nullptr;
    Image* ping = nullptr;
    Image* brake = nullptr;
    Image* add = nullptr;
    Image* subtract = nullptr;
    Image* remove = nullptr;
    Image* up = nullptr;
    Image* down = nullptr;
    Image* left = nullptr;
    Image* right = nullptr;
    Image* yes = nullptr;
    Image* no = nullptr;
    Image* entityControlPoint = nullptr;
    Image* partControlPoint = nullptr;
    Image* subpartControlPoint = nullptr;
    Image* dot = nullptr;
    Image* anchor = nullptr;
    Image* light = nullptr;
    Image* force = nullptr;
    Image* area = nullptr;
    Image* frame = nullptr;
    Image* play = nullptr;
    Image* pause = nullptr;
    Image* stop = nullptr;
    Image* goToStart = nullptr;
    Image* goToEnd = nullptr;
    Image* online = nullptr;
    Image* offline = nullptr;
    Image* vr = nullptr;
    Image* flat = nullptr;
    Image* admin = nullptr;
    Image* shown = nullptr;
    Image* hidden = nullptr;
    Image* alpha = nullptr;
    Image* windowClose = nullptr;
    Image* windowMaximize = nullptr;
    Image* windowUnmaxmize = nullptr;
} icons;

static Image* pickIcon(Icon type)
{
    switch (type)
    {
    default :
    case Icon_None : return icons.none;
    case Icon_Checked : return icons.checked;
    case Icon_Icon : return icons.icon;
    case Icon_Title : return icons.title;
    case Icon_Amini_Allight : return icons.aminiAllight;
    case Icon_Spinner : return icons.spinner;
    case Icon_Saving : return icons.saving;
    case Icon_Parent : return icons.parent;
    case Icon_Active : return icons.active;
    case Icon_Inactive : return icons.inactive;
    case Icon_Shared : return icons.shared;
    case Icon_Attached : return icons.attached;
    case Icon_Host : return icons.host;
    case Icon_Text : return icons.text;
    case Icon_Timeout : return icons.timeout;
    case Icon_User : return icons.user;
    case Icon_Ping : return icons.ping;
    case Icon_Brake : return icons.brake;
    case Icon_Add : return icons.add;
    case Icon_Subtract : return icons.subtract;
    case Icon_Remove : return icons.remove;
    case Icon_Up : return icons.up;
    case Icon_Down : return icons.down;
    case Icon_Left : return icons.left;
    case Icon_Right : return icons.right;
    case Icon_Yes : return icons.yes;
    case Icon_No : return icons.no;
    case Icon_Entity_Control_Point : return icons.entityControlPoint;
    case Icon_Part_Control_Point : return icons.partControlPoint;
    case Icon_Subpart_Control_Point : return icons.subpartControlPoint;
    case Icon_Dot : return icons.dot;
    case Icon_Anchor : return icons.anchor;
    case Icon_Light : return icons.light;
    case Icon_Force : return icons.force;
    case Icon_Area : return icons.area;
    case Icon_Frame : return icons.frame;
    case Icon_Play : return icons.play;
    case Icon_Pause : return icons.pause;
    case Icon_Stop : return icons.stop;
    case Icon_Go_To_Start : return icons.goToStart;
    case Icon_Go_To_End : return icons.goToEnd;
    case Icon_Online : return icons.online;
    case Icon_Offline : return icons.offline;
    case Icon_VR : return icons.vr;
    case Icon_Flat : return icons.flat;
    case Icon_Admin : return icons.admin;
    case Icon_Shown : return icons.shown;
    case Icon_Hidden : return icons.hidden;
    case Icon_Alpha : return icons.alpha;
    case Icon_Window_Close : return icons.windowClose;
    case Icon_Window_Maximize : return icons.windowMaximize;
    case Icon_Window_Unmaximize : return icons.windowUnmaxmize;
    }
}

static vector<DrawTexture*> activeTextureSet;
static vector<vector<DrawTexture*>> outgoingTextureSets;

static vector<DrawSupport*> activeSupportSet;
static vector<vector<DrawSupport*>> outgoingSupportSets;

static void recordTexture(DrawTexture* texture)
{
    texture->mark();
    activeTextureSet.push_back(texture);
}

static DrawTexture* findTexture(Image* rawImage)
{
    for (vector<DrawTexture*>& textureSet : outgoingTextureSets)
    {
        for (size_t i = 0; i < textureSet.size(); i++)
        {
            DrawTexture* texture = textureSet[i];

            if (texture->matches(rawImage))
            {
                texture->mark();
                return texture;
            }
        }
    }

    for (DrawTexture* texture : activeTextureSet)
    {
        if (texture->matches(rawImage))
        {
            texture->mark();
            return texture;
        }
    }

    return nullptr;
}

static DrawTexture* findTexture(const string& text, const TextStyle& style)
{
    for (vector<DrawTexture*>& textureSet : outgoingTextureSets)
    {
        for (size_t i = 0; i < textureSet.size(); i++)
        {
            DrawTexture* texture = textureSet[i];

            if (texture->matches(text, style))
            {
                texture->mark();
                return texture;
            }
        }
    }

    for (DrawTexture* texture : activeTextureSet)
    {
        if (texture->matches(text, style))
        {
            texture->mark();
            return texture;
        }
    }

    return nullptr;
}

static void recordSupport(DrawSupport* support)
{
    support->mark();
    activeSupportSet.push_back(support);
}

static VkDescriptorSet* findSupport(const DrawSupportKey& key)
{
    for (vector<DrawSupport*>& supportSet : outgoingSupportSets)
    {
        for (size_t i = 0; i < supportSet.size(); i++)
        {
            DrawSupport* support = supportSet[i];

            if (support->key == key)
            {
                support->mark();
                return &support->descriptorSet;
            }
        }
    }

    for (DrawSupport* support : activeSupportSet)
    {
        if (support->key == key)
        {
            support->mark();
            return &support->descriptorSet;
        }
    }

    return nullptr;
}

static Point widgetOffset(const InterfaceWidget* widget)
{
    return widget
        ? widget->position()
        : Point();
}

static Size widgetSize(const DrawContext& ctx, const InterfaceWidget* widget)
{
    return widget
        ? widget->size()
        : ctx.size;
}

static VkDescriptorSet* createSolidDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Color color
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        solidDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(SolidUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    SolidUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();

    memcpy(mapping.data, &uniform, sizeof(SolidUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = solidSupportKey(ctx.size, point, size, color);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createRoundedBorderDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    i32 width,
    i32 radius,
    Color color
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        solidDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(RoundedBorderUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    RoundedBorderUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();
    uniform.width = width;
    uniform.radius = radius;
    uniform.screenSize = ctx.size.toVec2();

    memcpy(mapping.data, &uniform, sizeof(RoundedBorderUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = roundedBorderSupportKey(ctx.size, point, size, width, radius, color);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static DrawTexture* createImage(
    const DrawContext& ctx,
    Image* rawImage
)
{
    VmaImage image = createImage(
        gpuAllocator,
        { rawImage->width(), rawImage->height(), 1 },
        textureFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        true,
        maxMipLevels(rawImage->width(), rawImage->height())
    );

    VkImageView imageView = createImageView(
        device,
        image.image,
        textureFormat,
        VK_IMAGE_ASPECT_COLOR_BIT
    );

    preinitializeImage(
        device,
        gpuAllocator,
        image,
        rawImage->width(),
        rawImage->height(),
        rawImage->pitch(),
        reinterpret_cast<const u32*>(rawImage->pixels())
    );

    // Image layout & mipmaps
    computeMipmaps(
        ctx.transferCommandBuffer,
        image.image,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_IMAGE_LAYOUT_PREINITIALIZED,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        rawImage->width(),
        rawImage->height()
    );

    auto texture = new DrawTexture();
    texture->key = {
        false,
        rawImage,
        "",
        TextStyle()
    };
    texture->image = image;
    texture->imageView = imageView;
    texture->size = Size(rawImage->width(), rawImage->height());

    recordTexture(texture);

    return texture;
}

static DrawTexture* createText(
    const DrawContext& ctx,
    const string& text,
    const TextStyle& style
)
{
    Image* rawImage = style.getFont()->render(text, style.color);

    VmaImage image = createImage(
        gpuAllocator,
        { rawImage->width(), rawImage->height(), 1 },
        textureFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_USAGE_SAMPLED_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        true
    );

    VkImageView imageView = createImageView(
        device,
        image.image,
        textureFormat,
        VK_IMAGE_ASPECT_COLOR_BIT
    );

    preinitializeImage(
        device,
        gpuAllocator,
        image,
        rawImage->width(),
        rawImage->height(),
        rawImage->pitch(),
        reinterpret_cast<const u32*>(rawImage->pixels())
    );

    // Image format
    VkImageMemoryBarrier barrier = imageMemoryBarrier(
        image.image,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_LAYOUT_PREINITIALIZED,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    vkCmdPipelineBarrier(
        ctx.transferCommandBuffer,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        0,
        0, nullptr,
        0, nullptr,
        1, &barrier
    );

    auto texture = new DrawTexture();
    texture->key = {
        true,
        nullptr,
        text,
        style
    };
    texture->image = image;
    texture->imageView = imageView;
    texture->size = Size(rawImage->width(), rawImage->height());

    recordTexture(texture);

    delete rawImage;

    return texture;
}

static DrawTexture* ensureImage(
    const DrawContext& ctx,
    Image* rawImage
)
{
    DrawTexture* texture = findTexture(rawImage);

    if (texture)
    {
        return texture;
    }

    return createImage(ctx, rawImage);
}

static DrawTexture* ensureText(
    const DrawContext& ctx,
    const string& text,
    const TextStyle& style
)
{
    DrawTexture* texture = findTexture(text, style);

    if (texture)
    {
        return texture;
    }

    return createText(ctx, text, style);
}

static VkDescriptorSet* createImageDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Image* rawImage,
    f64 alpha,
    f64 rotation
)
{
    DrawTexture* texture = ensureImage(ctx, rawImage);

    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        imageDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(ImageUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    ImageUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.alpha = alpha;
    uniform.rotation = rotation;

    memcpy(mapping.data, &uniform, sizeof(ImageUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkDescriptorImageInfo imageInfo{};
    VkWriteDescriptorSet imageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, texture->imageView, sampler, &imageInfo, &imageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite,
        imageWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = imageSupportKey(ctx.size, point, size, rawImage, alpha, rotation);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;
    support->texture = texture;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createColoredImageDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Image* rawImage,
    Color color,
    f64 rotation
)
{
    DrawTexture* texture = ensureImage(ctx, rawImage);

    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        imageDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(ColoredImageUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    ColoredImageUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();
    uniform.rotation = rotation;

    memcpy(mapping.data, &uniform, sizeof(ColoredImageUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkDescriptorImageInfo imageInfo{};
    VkWriteDescriptorSet imageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, texture->imageView, sampler, &imageInfo, &imageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite,
        imageWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = coloredImageSupportKey(ctx.size, point, size, rawImage, color, rotation);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;
    support->texture = texture;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createSymbolDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Image* rawImage,
    SymbolType symbol,
    Color color,
    f64 rotation
)
{
    DrawTexture* texture = ensureImage(ctx, rawImage);

    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        imageDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(SymbolUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    SymbolUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();
    uniform.rotation = rotation;

    memcpy(mapping.data, &uniform, sizeof(SymbolUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkDescriptorImageInfo imageInfo{};
    VkWriteDescriptorSet imageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, texture->imageView, sampler, &imageInfo, &imageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite,
        imageWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = symbolSupportKey(ctx.size, point, size, symbol, color, rotation);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;
    support->texture = texture;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static Point pickTextOffset(
    TextAlignment alignment,
    const Size& regionSize,
    const Size& imageSize,
    i32 marginSize
)
{
    switch (alignment)
    {
    default :
    case Text_Left : return Point(
        marginSize,
        (regionSize.h / 2) - (imageSize.h / 2)
    );
    case Text_Center : return Point(
        (regionSize.w / 2) - (imageSize.w / 2),
        (regionSize.h / 2) - (imageSize.h / 2)
    );
    case Text_Right : return Point(
        (regionSize.w - marginSize) - imageSize.w,
        (regionSize.h / 2) - (imageSize.h / 2)
    );
    }
}

static VkDescriptorSet* createTextDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    const string& text,
    const TextStyle& style,
    i32 marginSize
)
{
    DrawTexture* texture = ensureText(ctx, text, style);

    Size imageSize = texture->size;
    Point imagePoint = point + pickTextOffset(style.alignment, size, imageSize, marginSize);

    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        imageDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(ImageUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    ImageUniform uniform;
    uniform.position = imagePoint.toVec2() / ctx.size.toVec2();
    uniform.size = imageSize.toVec2() / ctx.size.toVec2();
    uniform.alpha = 1;
    uniform.rotation = 0;

    memcpy(mapping.data, &uniform, sizeof(ImageUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkDescriptorImageInfo imageInfo{};
    VkWriteDescriptorSet imageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, texture->imageView, sampler, &imageInfo, &imageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite,
        imageWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = textSupportKey(ctx.size, point, size, text, style, marginSize);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;
    support->texture = texture;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createGradient1DDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        solidDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(Gradient1DUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    Gradient1DUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.startColor = start.toVec4();
    uniform.endColor = end.toVec4();

    memcpy(mapping.data, &uniform, sizeof(Gradient1DUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = gradient1DSupportKey(ctx.size, point, size, start, end);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createGradient2DDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        solidDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(Gradient2DUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    Gradient2DUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.startColor = start.toVec4();
    uniform.endColor = end.toVec4();

    memcpy(mapping.data, &uniform, sizeof(Gradient2DUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = gradient2DSupportKey(ctx.size, point, size, start, end);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createCurveDescriptorSet(
    const DrawContext& ctx,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    Color color,
    i32 width
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        curveDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(CurveUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<i32> mapping(gpuAllocator, buffer);

    CurveUniform uniform;
    uniform.p0 = p0.toVec2();
    uniform.p1 = p1.toVec2();
    uniform.p2 = p2.toVec2();
    uniform.p3 = p3.toVec2();
    uniform.color = color.toVec4();
    uniform.screenSize = ctx.size.toVec2();
    uniform.width = width;

    memcpy(mapping.data, &uniform, sizeof(CurveUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = curveSupportKey(
        ctx.size,
        p0,
        p1,
        p2,
        p3,
        color,
        width
    );
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createRoundedSolidDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    i32 radius,
    Color color
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        solidDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(RoundedSolidUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    RoundedSolidUniform uniform;
    uniform.position = point.toVec2() / ctx.size.toVec2();
    uniform.size = size.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();
    uniform.screenSize = ctx.size.toVec2();
    uniform.radius = radius;

    memcpy(mapping.data, &uniform, sizeof(RoundedSolidUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = roundedSolidSupportKey(
        ctx.size,
        point,
        size,
        radius,
        color
    );
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createLineDescriptorSet(
    const DrawContext& ctx,
    const Point& start,
    const Point& end,
    Color color
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        lineDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(LineUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    LineUniform uniform;
    uniform.start = start.toVec2() / ctx.size.toVec2();
    uniform.end = end.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();
    uniform.screenSize = ctx.size.toVec2();

    memcpy(mapping.data, &uniform, sizeof(LineUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = lineSupportKey(ctx.size, start, end, color);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* createDashedLineDescriptorSet(
    const DrawContext& ctx,
    const Point& start,
    const Point& end,
    Color color
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        device,
        descriptorPool,
        lineDescriptorSetLayout
    );

    VmaBuffer buffer = createBuffer(
        gpuAllocator,
        sizeof(DashedLineUniform),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(gpuAllocator, buffer);

    DashedLineUniform uniform;
    uniform.start = start.toVec2() / ctx.size.toVec2();
    uniform.end = end.toVec2() / ctx.size.toVec2();
    uniform.color = color.toVec4();
    uniform.screenSize = ctx.size.toVec2();

    memcpy(mapping.data, &uniform, sizeof(DashedLineUniform));

    VkDescriptorBufferInfo bufferInfo{};
    VkWriteDescriptorSet bufferWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer.buffer, &bufferInfo, &bufferWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        bufferWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    auto support = new DrawSupport();
    support->key = dashedLineSupportKey(ctx.size, start, end, color);
    support->descriptorSet = descriptorSet;
    support->buffer = buffer;

    recordSupport(support);

    return &activeSupportSet.back()->descriptorSet;
}

static VkDescriptorSet* ensureSolidDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Color color
)
{
    VkDescriptorSet* descriptorSet = findSupport(solidSupportKey(
        ctx.size,
        point,
        size,
        color
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createSolidDescriptorSet(
        ctx,
        point,
        size,
        color
    );
}

static VkDescriptorSet* ensureRoundedBorderDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    i32 width,
    i32 radius,
    Color color
)
{
    VkDescriptorSet* descriptorSet = findSupport(roundedBorderSupportKey(
        ctx.size,
        point,
        size,
        width,
        radius,
        color
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createRoundedBorderDescriptorSet(
        ctx,
        point,
        size,
        width,
        radius,
        color
    );
}

static VkDescriptorSet* ensureImageDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Image* rawImage,
    f64 alpha,
    f64 rotation
)
{
    VkDescriptorSet* descriptorSet = findSupport(imageSupportKey(
        ctx.size,
        point,
        size,
        rawImage,
        alpha,
        rotation
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createImageDescriptorSet(
        ctx,
        point,
        size,
        rawImage,
        alpha,
        rotation
    );
}

static VkDescriptorSet* ensureColoredImageDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Image* rawImage,
    Color color,
    f64 rotation
)
{
    VkDescriptorSet* descriptorSet = findSupport(coloredImageSupportKey(
        ctx.size,
        point,
        size,
        rawImage,
        color,
        rotation
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createColoredImageDescriptorSet(
        ctx,
        point,
        size,
        rawImage,
        color,
        rotation
    );
}

static VkDescriptorSet* ensureSymbolDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    SymbolType symbol,
    Color color,
    f64 rotation
)
{
    VkDescriptorSet* descriptorSet = findSupport(symbolSupportKey(
        ctx.size,
        point,
        size,
        symbol,
        color,
        rotation
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createSymbolDescriptorSet(
        ctx,
        point,
        size,
        symbolResolver->resolve(symbol, size.h),
        symbol,
        color,
        rotation
    );
}

static VkDescriptorSet* ensureTextDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    const string& text,
    TextStyle style,
    i32 marginSize
)
{
    VkDescriptorSet* descriptorSet = findSupport(textSupportKey(
        ctx.size,
        point,
        size,
        text,
        style,
        marginSize
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createTextDescriptorSet(
        ctx,
        point,
        size,
        text,
        style,
        marginSize
    );
}

static VkDescriptorSet* ensureGradient1DDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    VkDescriptorSet* descriptorSet = findSupport(gradient1DSupportKey(
        ctx.size,
        point,
        size,
        start,
        end
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createGradient1DDescriptorSet(
        ctx,
        point,
        size,
        start,
        end
    );
}

static VkDescriptorSet* ensureGradient2DDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    VkDescriptorSet* descriptorSet = findSupport(gradient2DSupportKey(
        ctx.size,
        point,
        size,
        start,
        end
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createGradient2DDescriptorSet(
        ctx,
        point,
        size,
        start,
        end
    );
}

static VkDescriptorSet* ensureCurveDescriptorSet(
    const DrawContext& ctx,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    Color color,
    i32 width
)
{
    VkDescriptorSet* descriptorSet = findSupport(curveSupportKey(
        ctx.size,
        p0,
        p1,
        p2,
        p3,
        color,
        width
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createCurveDescriptorSet(
        ctx,
        p0,
        p1,
        p2,
        p3,
        color,
        width
    );
}

static VkDescriptorSet* ensureRoundedSolidDescriptorSet(
    const DrawContext& ctx,
    const Point& point,
    const Size& size,
    i32 radius,
    Color color
)
{
    VkDescriptorSet* descriptorSet = findSupport(roundedSolidSupportKey(
        ctx.size,
        point,
        size,
        radius,
        color
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createRoundedSolidDescriptorSet(
        ctx,
        point,
        size,
        radius,
        color
    );
}

static VkDescriptorSet* ensureLineDescriptorSet(
    const DrawContext& ctx,
    const Point& start,
    const Point& end,
    Color color
)
{
    VkDescriptorSet* descriptorSet = findSupport(lineSupportKey(
        ctx.size,
        start,
        end,
        color
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createLineDescriptorSet(
        ctx,
        start,
        end,
        color
    );
}

static VkDescriptorSet* ensureDashedLineDescriptorSet(
    const DrawContext& ctx,
    const Point& start,
    const Point& end,
    Color color
)
{
    VkDescriptorSet* descriptorSet = findSupport(dashedLineSupportKey(
        ctx.size,
        start,
        end,
        color
    ));

    if (descriptorSet)
    {
        return descriptorSet;
    }

    return createDashedLineDescriptorSet(
        ctx,
        start,
        end,
        color
    );
}

void interfaceDrawInit(Controller* controller)
{
    device = controller->render()->context()->device;
    gpuAllocator = controller->render()->context()->allocator;

    icons.none = loadPNG(iconPath + "/none" + iconExt);
    icons.checked = loadPNG(iconPath + "/checked" + iconExt);
    icons.icon = loadPNG(resourcePath() + "/icon" + iconExt);
    icons.title = loadPNG(resourcePath() + "/lambdamod" + iconExt);
    icons.aminiAllight = loadPNG(resourcePath() + "/amini-allight" + iconExt);
    icons.spinner = loadPNG(iconPath + "/spinner" + iconExt);
    icons.saving = loadPNG(iconPath + "/saving" + iconExt);
    icons.parent = loadPNG(iconPath + "/parent" + iconExt);
    icons.active = loadPNG(iconPath + "/active" + iconExt);
    icons.inactive = loadPNG(iconPath + "/inactive" + iconExt);
    icons.shared = loadPNG(iconPath + "/shared" + iconExt);
    icons.attached = loadPNG(iconPath + "/attached" + iconExt);
    icons.host = loadPNG(iconPath + "/host" + iconExt);
    icons.text = loadPNG(iconPath + "/text" + iconExt);
    icons.timeout = loadPNG(iconPath + "/timed" + iconExt);
    icons.user = loadPNG(iconPath + "/user" + iconExt);
    icons.ping = loadPNG(iconPath + "/ping" + iconExt);
    icons.brake = loadPNG(iconPath + "/brake" + iconExt);
    icons.add = loadPNG(iconPath + "/add" + iconExt);
    icons.subtract = loadPNG(iconPath + "/subtract" + iconExt);
    icons.remove = loadPNG(iconPath + "/remove" + iconExt);
    icons.up = loadPNG(iconPath + "/up" + iconExt);
    icons.down = loadPNG(iconPath + "/down" + iconExt);
    icons.left = loadPNG(iconPath + "/left" + iconExt);
    icons.right = loadPNG(iconPath + "/right" + iconExt);
    icons.yes = loadPNG(iconPath + "/yes" + iconExt);
    icons.no = loadPNG(iconPath + "/no" + iconExt);
    icons.entityControlPoint = loadPNG(iconPath + "/entity-control-point" + iconExt);
    icons.partControlPoint = loadPNG(iconPath + "/part-control-point" + iconExt);
    icons.subpartControlPoint = loadPNG(iconPath + "/subpart-control-point" + iconExt);
    icons.dot = loadPNG(iconPath + "/dot" + iconExt);
    icons.anchor = loadPNG(iconPath + "/anchor" + iconExt);
    icons.light = loadPNG(iconPath + "/light" + iconExt);
    icons.force = loadPNG(iconPath + "/force" + iconExt);
    icons.area = loadPNG(iconPath + "/area" + iconExt);
    icons.frame = loadPNG(iconPath + "/frame" + iconExt);
    icons.play = loadPNG(iconPath + "/play" + iconExt);
    icons.pause = loadPNG(iconPath + "/pause" + iconExt);
    icons.stop = loadPNG(iconPath + "/stop" + iconExt);
    icons.goToStart = loadPNG(iconPath + "/go-to-start" + iconExt);
    icons.goToEnd = loadPNG(iconPath + "/go-to-end" + iconExt);
    icons.online = loadPNG(iconPath + "/online" + iconExt);
    icons.offline = loadPNG(iconPath + "/offline" + iconExt);
    icons.vr = loadPNG(iconPath + "/vr" + iconExt);
    icons.flat = loadPNG(iconPath + "/flat" + iconExt);
    icons.admin = loadPNG(iconPath + "/admin" + iconExt);
    icons.shown = loadPNG(iconPath + "/shown" + iconExt);
    icons.hidden = loadPNG(iconPath + "/hidden" + iconExt);
    icons.alpha = loadPNG(iconPath + "/alpha" + iconExt);
    icons.windowClose = loadPNG(iconPath + "/window-close" + iconExt);
    icons.windowMaximize = loadPNG(iconPath + "/window-maximize" + iconExt);
    icons.windowUnmaxmize = loadPNG(iconPath + "/window-unmaximize" + iconExt);

    auto flatRender = dynamic_cast<const FlatRender*>(controller->render());

    createDescriptorPool(device);
    createShaders(device);
    createDescriptorSetLayouts(device);
    createGraphicsPipelines(
        device,
        controller->render()->context()->pipelineCache,
        flatRender ? flatRender->uiRenderPass() : nullptr,
        controller->render()->panelRenderPass()
    );
    createSampler(device);

    outgoingTextureSets = vector<vector<DrawTexture*>>(controller->render()->imageCount());
    outgoingSupportSets = vector<vector<DrawSupport*>>(controller->render()->imageCount());
}

void interfaceDrawPreStep()
{
    for (DrawTexture* texture : activeTextureSet)
    {
        texture->clearMark();
    }

    for (DrawSupport* support : activeSupportSet)
    {
        support->clearMark();
    }
}

void interfaceDrawPostStep()
{
    // Support sets
    for (size_t i = 0; i < activeSupportSet.size();)
    {
        if (!activeSupportSet[i]->marked())
        {
            outgoingSupportSets.back().push_back(activeSupportSet[i]);
            activeSupportSet.erase(activeSupportSet.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (vector<DrawSupport*>& supportSet : outgoingSupportSets)
    {
        for (size_t i = 0; i < supportSet.size();)
        {
            DrawSupport* support = supportSet[i];

            if (support->marked())
            {
                supportSet.erase(supportSet.begin() + i);
                recordSupport(support);
            }
            else
            {
                i++;
            }
        }
    }

    for (const DrawSupport* support : outgoingSupportSets.front())
    {
        delete support;
    }

    outgoingSupportSets.front().clear();

    for (size_t i = 1; i < outgoingSupportSets.size(); i++)
    {
        outgoingSupportSets[i - 1] = outgoingSupportSets[i];
        outgoingSupportSets[i].clear();
    }

    // Texture sets
    for (size_t i = 0; i < activeTextureSet.size();)
    {
        if (!activeTextureSet[i]->marked())
        {
            outgoingTextureSets.back().push_back(activeTextureSet[i]);
            activeTextureSet.erase(activeTextureSet.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (vector<DrawTexture*>& textureSet : outgoingTextureSets)
    {
        for (size_t i = 0; i < textureSet.size();)
        {
            DrawTexture* texture = textureSet[i];

            if (texture->marked())
            {
                textureSet.erase(textureSet.begin() + i);
                recordTexture(texture);
            }
            else
            {
                i++;
            }
        }
    }

    for (const DrawTexture* texture : outgoingTextureSets.front())
    {
        delete texture;
    }

    outgoingTextureSets.front().clear();

    for (size_t i = 1; i < outgoingTextureSets.size(); i++)
    {
        outgoingTextureSets[i - 1] = outgoingTextureSets[i];
        outgoingTextureSets[i].clear();
    }
}

void interfaceDrawQuit()
{
    VkResult result;

    result = vkDeviceWaitIdle(device);

    if (result != VK_SUCCESS)
    {
        error("Failed to wait for device to idle: " + vulkanError(result));
    }

    // Support sets
    for (const vector<DrawSupport*>& supportSet : outgoingSupportSets)
    {
        for (const DrawSupport* support : supportSet)
        {
            delete support;
        }
    }

    outgoingSupportSets.clear();

    for (const DrawSupport* support : activeSupportSet)
    {
        delete support;
    }

    activeSupportSet.clear();

    // Texture sets
    for (const vector<DrawTexture*>& textureSet : outgoingTextureSets)
    {
        for (const DrawTexture* texture : textureSet)
        {
            delete texture;
        }
    }

    outgoingTextureSets.clear();

    for (const DrawTexture* texture : activeTextureSet)
    {
        delete texture;
    }

    activeTextureSet.clear();

    destroySampler(device);
    destroyGraphicsPipelines(device);
    destroyDescriptorSetLayouts(device);
    destroyShaders(device);
    destroyDescriptorPool(device);

    delete icons.windowUnmaxmize;
    delete icons.windowMaximize;
    delete icons.windowClose;
    delete icons.alpha;
    delete icons.hidden;
    delete icons.shown;
    delete icons.admin;
    delete icons.flat;
    delete icons.vr;
    delete icons.offline;
    delete icons.online;

    delete icons.goToEnd;
    delete icons.goToStart;
    delete icons.stop;
    delete icons.pause;
    delete icons.play;

    delete icons.frame;
    delete icons.light;
    delete icons.area;
    delete icons.anchor;
    delete icons.dot;
    delete icons.entityControlPoint;
    delete icons.partControlPoint;
    delete icons.subpartControlPoint;

    delete icons.no;
    delete icons.yes;

    delete icons.right;
    delete icons.left;
    delete icons.down;
    delete icons.up;

    delete icons.remove;
    delete icons.subtract;
    delete icons.add;

    delete icons.brake;
    delete icons.ping;
    delete icons.user;

    delete icons.timeout;
    delete icons.text;
    delete icons.attached;
    delete icons.host;
    delete icons.shared;
    delete icons.inactive;
    delete icons.active;
    delete icons.parent;

    delete icons.saving;
    delete icons.spinner;
    delete icons.aminiAllight;
    delete icons.title;
    delete icons.icon;
    delete icons.checked;
    delete icons.none;
}

static tuple<Point, Size> widgetScissor(
    const InterfaceWidget* widget,
    Point point,
    Size size
)
{
    if (!widget)
    {
        return { point, size };
    }

    if (point.x < widget->position().x)
    {
        point.x = widget->position().x;
    }

    if (point.y < widget->position().y)
    {
        point.y = widget->position().y;
    }

    if (point.x + size.w > widget->position().x + widget->size().w)
    {
        size.w -= (point.x + size.w) - (widget->position().x + widget->size().w);
    }

    if (point.y + size.h > widget->position().y + widget->size().h)
    {
        size.h -= (point.y + size.h) - (widget->position().y + widget->size().h);
    }

    if (dynamic_cast<const Popup*>(widget))
    {
        return { point, size };
    }
    else
    {
        return widgetScissor(widget->parent(), point, size);
    }
}

static VkRect2D clampScissor(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size
)
{
    auto [ widgetPoint, widgetSize ] = widgetScissor(widget, point, size);

    i32 x = widgetPoint.x;

    if (x < 0)
    {
        x = 0;
    }

    i32 y = widgetPoint.y;

    if (y < 0)
    {
        y = 0;
    }

    i32 w = widgetSize.w;

    if (x + w > ctx.size.w)
    {
        w -= (x + w) - ctx.size.w;
    }

    if (w < 0)
    {
        w = 0;
    }

    i32 h = widgetSize.h;

    if (y + h > ctx.size.h)
    {
        h -= (y + h) - ctx.size.h;
    }

    if (h < 0)
    {
        h = 0;
    }

    return {
        { x, y },
        { static_cast<u32>(w), static_cast<u32>(h) }
    };
}

void drawSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Color color
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, solid.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, solid.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        solid.pipelineLayout,
        0,
        1,
        ensureSolidDescriptorSet(
            ctx,
            globalPoint,
            size,
            color
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    i32 width,
    Color color
)
{
    // top
    drawSolid(
        ctx,
        widget,
        point + Point(width, 0),
        Size(size.w - (width * 2), width),
        color
    );

    // bottom
    drawSolid(
        ctx,
        widget,
        point + Point(width, size.h - width),
        Size(size.w - (width * 2), width),
        color
    );

    // left
    drawSolid(
        ctx,
        widget,
        point + Point(0, 0),
        Size(width, size.h),
        color
    );

    // right
    drawSolid(
        ctx,
        widget,
        point + Point(size.w - width, 0),
        Size(width, size.h),
        color
    );
}

void drawRoundedBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    i32 width,
    i32 radius,
    Color color
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, roundedBorder.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, roundedBorder.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        roundedBorder.pipelineLayout,
        0,
        1,
        ensureRoundedBorderDescriptorSet(
            ctx,
            globalPoint,
            size,
            width,
            radius,
            color
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Icon icon,
    f64 alpha,
    f64 rotation
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, image.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, image.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        image.pipelineLayout,
        0,
        1,
        ensureImageDescriptorSet(
            ctx,
            globalPoint,
            size,
            pickIcon(icon),
            alpha,
            rotation
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawColoredImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Icon icon,
    Color color,
    f64 rotation
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, coloredImage.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, coloredImage.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        coloredImage.pipelineLayout,
        0,
        1,
        ensureColoredImageDescriptorSet(
            ctx,
            globalPoint,
            size,
            pickIcon(icon),
            color,
            rotation
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawSymbol(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    SymbolType symbol,
    Color color,
    f64 rotation
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, coloredImage.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, coloredImage.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        coloredImage.pipelineLayout,
        0,
        1,
        ensureSymbolDescriptorSet(
            ctx,
            globalPoint,
            size,
            symbol,
            color,
            rotation
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawText(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    const string& text,
    const TextStyle& style
)
{
    if (text.empty())
    {
        return;
    }

    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ::image.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, ::image.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        ::image.pipelineLayout,
        0,
        1,
        ensureTextDescriptorSet(
            ctx,
            globalPoint,
            size,
            text,
            style,
            UIScale::marginSize(widget)
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawGradient1D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, gradient1D.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, gradient1D.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        gradient1D.pipelineLayout,
        0,
        1,
        ensureGradient1DDescriptorSet(
            ctx,
            globalPoint,
            size,
            start,
            end
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawGradient2D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Color start,
    Color end
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, gradient2D.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, gradient2D.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        gradient2D.pipelineLayout,
        0,
        1,
        ensureGradient2DDescriptorSet(
            ctx,
            globalPoint,
            size,
            start,
            end
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawCurve(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    i32 width,
    Color color
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, curve.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, curve.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        curve.pipelineLayout,
        0,
        1,
        ensureCurveDescriptorSet(
            ctx,
            p0,
            p1,
            p2,
            p3,
            color,
            width
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 1, 1, 0, 0);
}

void drawRoundedSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    i32 radius,
    Color color
)
{
    Point globalPoint = widgetOffset(widget) + point;

    if (globalPoint.x >= ctx.size.w || globalPoint.y >= ctx.size.h || size.w <= 0 || size.h <= 0)
    {
        return;
    }

    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, globalPoint, size);

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, roundedSolid.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, roundedSolid.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        roundedSolid.pipelineLayout,
        0,
        1,
        ensureRoundedSolidDescriptorSet(
            ctx,
            globalPoint,
            size,
            radius,
            color
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void drawSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Color color
)
{
    drawSolid(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        color
    );
}

void drawBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    i32 width,
    Color color
)
{
    drawBorder(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        width,
        color
    );
}

void drawRoundedBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    i32 width,
    i32 radius,
    Color color
)
{
    drawRoundedBorder(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        width,
        radius,
        color
    );
}

void drawImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Icon icon,
    f64 alpha,
    f64 rotation
)
{
    drawImage(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        icon,
        alpha,
        rotation
    );
}

void drawColoredImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Icon icon,
    Color color,
    f64 rotation
)
{
    drawColoredImage(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        icon,
        color,
        rotation
    );
}

void drawSymbol(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    SymbolType symbol,
    Color color,
    f64 rotation
)
{
    drawSymbol(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        symbol,
        color,
        rotation
    );
}

void drawText(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const string& text,
    const TextStyle& style
)
{
    drawText(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        text,
        style
    );
}

void drawGradient1D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Color start,
    Color end
)
{
    drawGradient1D(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        start,
        end
    );
}

void drawGradient2D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Color start,
    Color end
)
{
    drawGradient2D(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        start,
        end
    );
}

void drawCurve(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    i32 width,
    Color color
)
{
    drawCurve(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        p0,
        p1,
        p2,
        p3,
        width,
        color
    );
}

void drawRoundedSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    i32 radius,
    Color color
)
{
    drawRoundedSolid(
        ctx,
        widget,
        Point(0),
        widgetSize(ctx, widget),
        radius,
        color
    );
}

void drawLine(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& start,
    const Point& end,
    Color color
)
{
    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, widgetOffset(widget), widgetSize(ctx, widget));

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, line.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, line.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        line.pipelineLayout,
        0,
        1,
        ensureLineDescriptorSet(
            ctx,
            widgetOffset(widget) + start,
            widgetOffset(widget) + end,
            color
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 2, 1, 0, 0);
}

void drawDashedLine(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& start,
    const Point& end,
    Color color
)
{
    VkViewport viewport = {
        0, 0,
        static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = clampScissor(ctx, widget, widgetOffset(widget), widgetSize(ctx, widget));

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    if (ctx.ui)
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, dashedLine.uiPipeline);
    }
    else
    {
        vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, dashedLine.panelPipeline);
    }

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        dashedLine.pipelineLayout,
        0,
        1,
        ensureDashedLineDescriptorSet(
            ctx,
            widgetOffset(widget) + start,
            widgetOffset(widget) + end,
            color
        ),
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 2, 1, 0, 0);
}

void drawPointer(const DrawContext& ctx, const InterfaceWidget* widget, const optional<Point>& pointer)
{
    if (pointer.has_value())
    {
        drawBorder(
            ctx,
            widget,
            *pointer - Point(UIScale::marginSize(widget) * 2),
            Size(UIScale::marginSize(widget) * 4),
            UIScale::marginSize(widget),
            theme.accentColor
        );
    }
}
