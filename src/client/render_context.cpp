/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_context.hpp"
#include "render_constants.hpp"
#include "render_tools.hpp"
#include "render_objects.hpp"
#include "global.hpp"

RenderContext::RenderContext()
    : instance(nullptr)
#ifdef LMOD_VK_VALIDATION
    , debugMessenger(nullptr)
#endif
    , physDevice(nullptr)
    , device(nullptr)
    , graphicsQueueIndex(0)
    , graphicsQueue(nullptr)
    , presentQueueIndex(0)
    , presentQueue(nullptr)
    , allocator(nullptr)
    , pipelineCache(nullptr)
    , commandPool(nullptr)
    , descriptorPool(nullptr)
    , filteredSampler(nullptr)
    , unfilteredSampler(nullptr)
    , environmentMappingCameraUniformBuffer(nullptr, nullptr)
    , _initialized(false)
{

}

bool RenderContext::initialized() const
{
    return _initialized;
}

void RenderContext::init(
    const function<u32()>& pickAPIVersion,
    const function<set<string>()>& instanceExtensions,
    const function<VkPhysicalDevice()>& pickPhysicalDevice,
    const function<set<string>()>& deviceExtensions,
    const function<u32()>& prepareSwapchain
)
{
    apiVersion = pickAPIVersion();

    log("Using Vulkan API version " + to_string(VK_API_VERSION_MAJOR(apiVersion)) + "." + to_string(VK_API_VERSION_MINOR(apiVersion)));

#ifdef LMOD_VK_VALIDATION
    createInstance(instance, debugMessenger, apiVersion, instanceExtensions());
#else
    createInstance(instance, apiVersion, instanceExtensions());
#endif

    physDevice = pickPhysicalDevice();

    createSpecificDevice(
        apiVersion,
        instance,
        physDevice,
        deviceExtensions(),
        graphicsQueueIndex,
        graphicsQueue,
        device
    );
    createAllocator(apiVersion, instance, physDevice, device, allocator);
    createPipelineCache(device, pipelineCache);
    createCommandPool(device, graphicsQueueIndex, commandPool);

    u32 imageCount = prepareSwapchain();

    createDescriptorPool(device, imageCount, descriptorPool);
    createSampler(device, true, filteredSampler);
    createSampler(device, false, unfilteredSampler);
    createEnvironmentMappingCameraUniformBuffer(allocator, environmentMappingCameraUniformBuffer);

    runOneshotCommands(device, graphicsQueue, commandPool, [&](VkCommandBuffer commandBuffer) -> void {
        createAreaSurfaceNoiseImage(device, allocator, commandBuffer, areaSurfaceNoiseImage, areaSurfaceNoiseImageView);
    });

    _initialized = true;
}

void RenderContext::init(
    const function<u32()>& pickAPIVersion,
    const function<set<string>()>& instanceExtensions,
    const function<VkPhysicalDevice()>& pickPhysicalDevice,
    const function<VkSurfaceKHR()>& prepareSurface,
    const function<set<string>()>& deviceExtensions,
    const function<u32()>& prepareSwapchain
)
{
    apiVersion = pickAPIVersion();

    log("Using Vulkan API version " + to_string(VK_API_VERSION_MAJOR(apiVersion)) + "." + to_string(VK_API_VERSION_MINOR(apiVersion)));

#ifdef LMOD_VK_VALIDATION
    createInstance(instance, debugMessenger, apiVersion, instanceExtensions());
#else
    createInstance(instance, apiVersion, instanceExtensions());
#endif

    physDevice = pickPhysicalDevice();

    VkSurfaceKHR surface = prepareSurface();

    createSpecificPresentDevice(
        apiVersion,
        instance,
        physDevice,
        surface,
        deviceExtensions(),
        graphicsQueueIndex,
        graphicsQueue,
        presentQueueIndex,
        presentQueue,
        device
    );
    createAllocator(apiVersion, instance, physDevice, device, allocator);
    createPipelineCache(device, pipelineCache);
    createCommandPool(device, graphicsQueueIndex, commandPool);

    u32 imageCount = prepareSwapchain();

    createDescriptorPool(device, imageCount, descriptorPool);
    createSampler(device, true, filteredSampler);
    createSampler(device, false, unfilteredSampler);
    createEnvironmentMappingCameraUniformBuffer(allocator, environmentMappingCameraUniformBuffer);

    runOneshotCommands(device, graphicsQueue, commandPool, [&](VkCommandBuffer commandBuffer) -> void {
        createAreaSurfaceNoiseImage(device, allocator, commandBuffer, areaSurfaceNoiseImage, areaSurfaceNoiseImageView);
    });

    _initialized = true;
}

RenderContext::~RenderContext()
{
    destroyImageView(device, areaSurfaceNoiseImageView);
    destroyImage(allocator, areaSurfaceNoiseImage);
    destroyBuffer(allocator, environmentMappingCameraUniformBuffer);
    destroySampler(device, unfilteredSampler);
    destroySampler(device, filteredSampler);
    destroyDescriptorPool(device, descriptorPool);
    destroyCommandPool(device, commandPool);
    destroyPipelineCache(device, pipelineCache);
    destroyAllocator(allocator);
    destroyDevice(device);

#ifdef LMOD_VK_VALIDATION
    destroyInstance(instance, debugMessenger);
#else
    destroyInstance(instance);
#endif
}

set<string> RenderContext::rayTracingDeviceExtensions() const
{
    return ::rayTracingDeviceExtensions(apiVersion, physDevice);
}

bool RenderContext::rayTracingSupported() const
{
    return ::rayTracingSupported(apiVersion, physDevice);
}

bool RenderContext::rayTracingEnabled() const
{
    return ::rayTracingEnabled(apiVersion, physDevice);
}
