/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "scrollable.hpp"

Scrollable::Scrollable()
    : thumbGrabbed(false)
    , thumbGrabY(0)
{

}

bool Scrollable::needsScroll() const
{
    return viewHeight() < totalHeight();
}

i32 Scrollable::maxScrollIndex() const
{
    if (childHeight() == 0)
    {
        return 0;
    }

    return max<i32>(childCount() - (viewHeight() / childHeight()), 0);
}

i32 Scrollable::elapsedHeight() const
{
    return scrollIndex() * childHeight();
}

i32 Scrollable::totalHeight() const
{
    return childCount() * childHeight();
}

i32 Scrollable::thumbY() const
{
    if (totalHeight() == 0)
    {
        return 0;
    }

    return (elapsedHeight() / static_cast<f64>(totalHeight())) * viewHeight();
}

i32 Scrollable::thumbHeight() const
{
    if (totalHeight() == 0)
    {
        return 0;
    }

    return (viewHeight() / static_cast<f64>(totalHeight())) * viewHeight();
}

void Scrollable::grabThumb(const Point& local)
{
    thumbGrabbed = true;
    thumbGrabY = thumbY();
    thumbGrabPosition = local;
}

void Scrollable::releaseThumb()
{
    thumbGrabbed = false;
}

void Scrollable::dragThumb(const Point& local)
{
    if (viewHeight() == 0)
    {
        return;
    }

    if (thumbGrabbed)
    {
        i32 delta = (local - thumbGrabPosition).y;
        i32 targetThumbY = thumbGrabY + delta;

        setScrollIndexFromThumbY(targetThumbY);
    }
}

void Scrollable::setScrollIndexFromThumbY(i32 thumbY)
{
    setScrollIndex(clamp<i32>(
        childCount() * (thumbY / static_cast<f64>(viewHeight())),
        0,
        maxScrollIndex()
    ));
}
