/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_handling_result.hpp"
#include "tools.hpp"

InputHandlingResult::InputHandlingResult(ControlContext* ctx, const Input& input)
    : ctx(ctx)
    , input(input)
    , blocking(false)
{

}

void InputHandlingResult::setBlocking(bool state)
{
    blocking |= state;
}

void InputHandlingResult::merge(const InputHandlingResult& child)
{
    if (blocking)
    {
        return;
    }
    else if (child.blocking)
    {
        *this = child;
    }
    else
    {
        entries = join(entries, child.entries);
    }
}

void InputHandlingResult::add(const vector<string>& path, const InputScopeBinding& binding, const InputScopeSlot& slot, size_t depth)
{
    entries.push_back(InputHandlingResultEntry(path, binding, slot, depth));
}

bool InputHandlingResult::commit() const
{
    vector<InputHandlingResultEntry> entries = this->entries;

    if (entries.empty())
    {
        return false;
    }

    stable_sort(entries.begin(), entries.end());

    const InputHandlingResultEntry& entry = entries.front();

    entry.commit(ctx, input);

    for (size_t i = 1; i < entries.size(); i++)
    {
        const InputHandlingResultEntry& auxEntry = entries.at(i);

        if (auxEntry.wantsInclusion(entry))
        {
            auxEntry.commit(ctx, input);
        }
    }

    return true;
}
