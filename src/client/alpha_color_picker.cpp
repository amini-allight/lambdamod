/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "alpha_color_picker.hpp"
#include "localization.hpp"
#include "tools.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "label.hpp"
#include "percent_edit.hpp"
#include "unit_field.hpp"
#include "spacer.hpp"
#include "row.hpp"
#include "column.hpp"
#include "color_saturation_value_picker.hpp"
#include "color_hue_picker.hpp"
#include "units.hpp"
#include "fixed_row.hpp"

AlphaColorPickerPopup::AlphaColorPickerPopup(
    InterfaceWidget* parent,
    const Color& color,
    const function<void(const Color&)>& onSet
)
    : Popup(parent)
    , color(color)
    , onSet(onSet)
{
    auto column = new Column(this);

    auto row = new Row(column);

    new ColorSaturationValuePicker(
        row,
        bind(&AlphaColorPickerPopup::hueSource, this),
        bind(&AlphaColorPickerPopup::onSaturationValueSet, this, placeholders::_1)
    );

    MAKE_SPACER(row, UIScale::marginSize(), 0);

    new ColorHuePicker(
        row,
        bind(&AlphaColorPickerPopup::hueSource, this),
        bind(&AlphaColorPickerPopup::onHueSet, this, placeholders::_1)
    );

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto text = new LineEdit(
        column,
        bind(&AlphaColorPickerPopup::textSource, this),
        bind(&AlphaColorPickerPopup::onTextReturn, this, placeholders::_1)
    );
    text->setPlaceholder(localize("alpha-color-picker-hex-color"));

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto alpha = new FixedRow(column, UIScale::lineEditHeight());

    new Label(
        alpha,
        localize("alpha-color-picker-alpha")
    );

    new UnitField(alpha, percentSuffix(), [this](InterfaceWidget* parent) -> void {
        auto alphaEdit = new PercentEdit(
            parent,
            bind(&AlphaColorPickerPopup::alphaSource, this),
            bind(&AlphaColorPickerPopup::onAlpha, this, placeholders::_1)
        );
        alphaEdit->setPlaceholder(localize("alpha-color-picker-alpha"));
    });
}

void AlphaColorPickerPopup::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + UIScale::marginSize());
    }
}

void AlphaColorPickerPopup::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + UIScale::marginSize());
        child->resize(this->size() - (UIScale::marginSize() * 2));
    }
}

void AlphaColorPickerPopup::draw(const DrawContext& ctx) const
{
    ctx.delayedCommands.push_back([this](const DrawContext& ctx) -> void {
        drawSolid(
            ctx,
            this,
            theme.indentColor
        );
    });

    for (InterfaceWidget* child : children())
    {
        ctx.delayedCommands.push_back([child](const DrawContext& ctx) -> void {
            child->draw(ctx);
        });
    }
}

f64 AlphaColorPickerPopup::hueSource()
{
    return rgbToHSV(color.toVec3()).x;
}

void AlphaColorPickerPopup::onHueSet(f64 hue)
{
    vec3 hsv = rgbToHSV(color.toVec3());

    hsv.x = hue;

    if (roughly<f64>(hsv.y, 0))
    {
        hsv.y = 1;
    }

    if (roughly<f64>(hsv.z, 0))
    {
        hsv.z = 1;
    }

    onSet(hsvToRGB(hsv));
}

void AlphaColorPickerPopup::onSaturationValueSet(const vec2& saturationValue)
{
    vec3 hsv = rgbToHSV(color.toVec3());

    hsv.y = saturationValue.x;
    hsv.z = saturationValue.y;

    onSet(hsvToRGB(hsv));
}

string AlphaColorPickerPopup::textSource()
{
    return color.toString(true);
}

void AlphaColorPickerPopup::onTextReturn(const string& text)
{
    onSet(Color(text));
}

f64 AlphaColorPickerPopup::alphaSource()
{
    return color.a / 255.0;
}

void AlphaColorPickerPopup::onAlpha(f64 alpha)
{
    onSet(Color(vec4(color.toVec3(), alpha)));
}

SizeProperties AlphaColorPickerPopup::sizeProperties() const
{
    return sizePropertiesFillAll;
}

AlphaColorPicker::AlphaColorPicker(
    InterfaceWidget* parent,
    const function<Color()>& source,
    const function<void(const Color&)>& onSet
)
    : InterfaceWidget(parent)
    , source(source)
    , _onSet(onSet)
{
    START_WIDGET_SCOPE("alpha-color-picker")
        WIDGET_SLOT("interact", interact)
    END_SCOPE

    popup = new AlphaColorPickerPopup(
        this,
        color,
        bind(&AlphaColorPicker::onSet, this, placeholders::_1)
    );
}

AlphaColorPicker::~AlphaColorPicker()
{
    delete popup;
}

void AlphaColorPicker::move(const Point& position)
{
    this->position(position);

    popup->move(position + Point(0, UIScale::colorPickerHeight()));
}

void AlphaColorPicker::resize(const Size& size)
{
    this->size(size);

    popup->resize(Size(this->size().w, UIScale::alphaColorPickerPopupHeight()));
}

void AlphaColorPicker::step()
{
    InterfaceWidget::step();

    Color newColor = source();

    if (newColor != color)
    {
        color = newColor;
    }

    if (!focused() && !popup->focused() && popup->opened())
    {
        popup->close();
    }
}

void AlphaColorPicker::draw(const DrawContext& ctx) const
{
    for (size_t i = 0; i < ceil(size().w / static_cast<f64>(UIScale::colorPickerHeight())); i++)
    {
        drawImage(
            ctx,
            this,
            Point(i * UIScale::colorPickerHeight(), 0),
            Size(UIScale::colorPickerHeight()),
            Icon_Alpha
        );
    }

    drawSolid(
        ctx,
        this,
        color
    );

    drawBorder(
        ctx,
        this,
        UIScale::marginSize(),
        focused() ? theme.accentColor : theme.outdentColor
    );

    InterfaceWidget::draw(ctx);
}

void AlphaColorPicker::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void AlphaColorPicker::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void AlphaColorPicker::onSet(const Color& color)
{
    this->color = color;

    _onSet(color);
}

void AlphaColorPicker::interact(const Input& input)
{
    if (popup->opened())
    {
        popup->close();
        playNegativeActivateEffect();
    }
    else
    {
        popup->open();
        playPositiveActivateEffect();
    }

    if (popup->opened())
    {
        update();
    }
}

SizeProperties AlphaColorPicker::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::colorPickerHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
