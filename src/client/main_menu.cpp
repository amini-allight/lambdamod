/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "main_menu.hpp"
#include "control_context.hpp"
#include "global.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "localization.hpp"
#include "yaml_tools.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "vr_render.hpp"
#include "panel_transforms.hpp"
#include "row.hpp"
#include "column.hpp"
#include "fill_row.hpp"
#include "fill_column.hpp"
#include "title_image.hpp"
#include "panel_line_edit.hpp"
#include "panel_text_button.hpp"
#include "panel_tab_view.hpp"
#include "panel_confirm_dialog.hpp"
#include "spacer.hpp"
#include "list_view.hpp"
#include "vr_menu_input_scope.hpp"

#ifdef LMOD_VR
static constexpr chrono::seconds minClearDrawTime(1);
#endif

MainMenu::MainMenu(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
    , pointerManager(this)
    , dialog(nullptr)
    , destroyDialog(false)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        START_VR_MENU_SCOPE(context()->input()->getScope("menu"), "main-menu")
            WIDGET_INCLUSION_SLOT("interact", changeFocus, Input_Scope_Slot_Inclusion_With_Below)
        END_SCOPE
    }
    else
    {
        START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("menu"), "main-menu")

        END_SCOPE
    }
#else
    START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("menu"), "main-menu")

    END_SCOPE
#endif

    pointerManager.init();

    ConnectionDetails details = loadConnectionDetails();

    icon = new TitleImage(this, Icon_Icon, UIScale::panelMainMenuIconSize);
    title = new TitleImage(this, Icon_Title, UIScale::panelMainMenuTitleSize);
    column = new Column(this);

    auto tabs = new PanelTabView(column, { localize("main-menu-tab-connect"), localize("main-menu-tab-host") });

    {
        auto connectRow = new Row(tabs->tab(localize("main-menu-tab-connect")));

        auto leftColumn = new FillColumn(connectRow);

        connect.host = new PanelLineEdit(leftColumn->column(), localize("main-menu-host"), nullptr, [](const string& text) -> void {});
        connect.host->setPlaceholder(localize("main-menu-host"));
        connect.host->set(details.connect.host);

        connect.port = new PanelLineEdit(leftColumn->column(), localize("main-menu-port"), nullptr, [](const string& text) -> void {});
        connect.port->setPlaceholder(localize("main-menu-port"));
        connect.port->set(to_string(details.connect.port));

        connect.serverPassword = new PanelLineEdit(leftColumn->column(), localize("main-menu-server-password"), nullptr, [](const string& text) -> void {});
        connect.serverPassword->setPlaceholder(localize("main-menu-server-password"));
        connect.serverPassword->set(details.connect.serverPassword);
        connect.serverPassword->setHideContents(true);

        MAKE_SPACER(connectRow, UIScale::panelLineEditLabelHeight(this), 0);

        auto rightColumn = new FillColumn(connectRow);

        connect.name = new PanelLineEdit(rightColumn->column(), localize("main-menu-name"), nullptr, [](const string& text) -> void {});
        connect.name->setPlaceholder(localize("main-menu-name"));
        connect.name->set(details.connect.name);

        connect.password = new PanelLineEdit(rightColumn->column(), localize("main-menu-password"), nullptr, [](const string& text) -> void {});
        connect.password->setPlaceholder(localize("main-menu-password"));
        connect.password->set(details.connect.password);
        connect.password->setHideContents(true);

        MAKE_SPACER(rightColumn->column(), 0, UIScale::panelLineEditLabelHeight(this));
        new PanelTextButton(rightColumn->column(), localize("main-menu-button-connect"), bind(&MainMenu::onConnect, this));
    }

    {
        auto hostRow = new Row(tabs->tab(localize("main-menu-tab-host")));

        auto leftColumn = new FillColumn(hostRow);

        host.host = new PanelLineEdit(leftColumn->column(), localize("main-menu-host"), nullptr, [](const string& text) -> void {});
        host.host->setPlaceholder(localize("main-menu-host"));
        host.host->set(details.host.host);

        host.port = new PanelLineEdit(leftColumn->column(), localize("main-menu-port"), nullptr, [](const string& text) -> void {});
        host.port->setPlaceholder(localize("main-menu-port"));
        host.port->set(to_string(details.host.port));

        host.serverPassword = new PanelLineEdit(leftColumn->column(), localize("main-menu-server-password"), nullptr, [](const string& text) -> void {});
        host.serverPassword->setPlaceholder(localize("main-menu-server-password"));
        host.serverPassword->set(details.host.serverPassword);
        host.serverPassword->setHideContents(true);

        host.adminPassword = new PanelLineEdit(leftColumn->column(), localize("main-menu-admin-password"), nullptr, [](const string& text) -> void {});
        host.adminPassword->setPlaceholder(localize("main-menu-admin-password"));
        host.adminPassword->set(details.host.adminPassword);
        host.adminPassword->setHideContents(true);

        MAKE_SPACER(hostRow, UIScale::panelLineEditLabelHeight(this), 0);

        auto rightColumn = new FillColumn(hostRow);

        host.savePath = new PanelLineEdit(rightColumn->column(), localize("main-menu-save-path"), nullptr, [](const string& text) -> void {});
        host.savePath->setPlaceholder(localize("main-menu-save-path"));
        host.savePath->set(details.host.savePath);

        host.name = new PanelLineEdit(rightColumn->column(), localize("main-menu-name"), nullptr, [](const string& text) -> void {});
        host.name->setPlaceholder(localize("main-menu-name"));
        host.name->set(details.host.name);

        host.password = new PanelLineEdit(rightColumn->column(), localize("main-menu-password"), nullptr, [](const string& text) -> void {});
        host.password->setPlaceholder(localize("main-menu-password"));
        host.password->set(details.host.password);
        host.password->setHideContents(true);

        MAKE_SPACER(rightColumn->column(), 0, UIScale::panelLineEditLabelHeight(this));
        new PanelTextButton(rightColumn->column(), localize("main-menu-button-host"), bind(&MainMenu::onHost, this));
    }

    MAKE_SPACER(column, 0, UIScale::panelLineEditLabelHeight(this));
    new PanelTextButton(column, localize("main-menu-button-quit"), bind(&MainMenu::onQuit, this));

    background = new MainMenuBackground(context()->controller(), Size(shaderDefaultSize));
}

MainMenu::~MainMenu()
{
    delete background;
}

#ifdef LMOD_VR
bool MainMenu::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    if (!g_vr)
    {
        return InterfaceWidget::movePointer(type, pointer, consumed);
    }

    switch (type)
    {
    default :
        break;
    case Pointer_VR_Left :
        leftPointer = pointer;
        break;
    case Pointer_VR_Right :
        rightPointer = pointer;
        break;
    }

    if (!focused() && contains(pointer) && shouldDraw() && !consumed)
    {
        focus();
    }
    else if (focused() && (!contains(pointer) || !shouldDraw() || consumed))
    {
        unfocus();
    }

    return focused();
}
#endif

void MainMenu::move(const Point& position)
{
    this->position(position);

    Point vrOffset;

#ifdef LMOD_VR
    if (g_vr)
    {
        vrOffset = Point(0, (size().h - UIScale::panelMainMenuColumnSize(this).h) / 2);
    }
#endif

    icon->move(position + UIScale::panelMainMenuIconOffset(this) + vrOffset);
    icon->resize(UIScale::panelMainMenuIconSize(this));

    title->move(position + UIScale::panelMainMenuIconOffset(this) + vrOffset + Point(
        UIScale::panelMainMenuIconSize(this).w + UIScale::panelMainMenuIconOffset(this),
        (UIScale::panelMainMenuIconSize(this).h - UIScale::panelMainMenuTitleSize(this).h) / 2
    ));
    title->resize(UIScale::panelMainMenuTitleSize(this));

    column->move(position + (size() - UIScale::panelMainMenuColumnSize(this)) / 2);
    column->resize(UIScale::panelMainMenuColumnSize(this));
}

void MainMenu::resize(const Size& size)
{
    this->size(size);

    Point vrOffset;

#ifdef LMOD_VR
    if (g_vr)
    {
        vrOffset = Point(0, (size.h - UIScale::panelMainMenuColumnSize(this).h) / 2);
    }
#endif

    icon->move(position() + UIScale::panelMainMenuIconOffset(this) + vrOffset);
    icon->resize(UIScale::panelMainMenuIconSize(this));

    title->move(position() + UIScale::panelMainMenuIconOffset(this) + vrOffset + Point(
        UIScale::panelMainMenuIconSize(this).w + UIScale::panelMainMenuIconOffset(this),
        (UIScale::panelMainMenuIconSize(this).h - UIScale::panelMainMenuTitleSize(this).h) / 2
    ));
    title->resize(UIScale::panelMainMenuTitleSize(this));

    column->move(position() + (size - UIScale::panelMainMenuColumnSize(this)) / 2);
    column->resize(UIScale::panelMainMenuColumnSize(this));

    background->resize(size);
}

void MainMenu::step()
{
    background->step();

    pointerManager.step();

    if (destroyDialog)
    {
        delete dialog;
        dialog = nullptr;
        destroyDialog = false;
    }

    InterfaceWidget::step();
}

void MainMenu::draw(const DrawContext& ctx) const
{
#ifdef LMOD_VR
    if (!firstClearDrawTime)
    {
        for (InterfaceWidget* child : view()->children())
        {
            auto startupSplash = dynamic_cast<StartupSplash*>(child);

            if (!startupSplash)
            {
                continue;
            }

            if (!startupSplash->shouldDraw())
            {
                firstClearDrawTime = currentTime();
            }
        }
    }
#endif

    drawSolid(
        ctx,
        nullptr,
        Point(),
        ctx.size,
        theme.worldBackgroundColor
    );

    background->draw(ctx);

    InterfaceWidget::draw(ctx);

#ifdef LMOD_VR
    if (g_vr)
    {
        drawPointer(ctx, this, leftPointer);
        drawPointer(ctx, this, rightPointer);
    }
    else
    {
        drawPointer(ctx, this, pointer);
    }
#else
    drawPointer(ctx, this, pointer);
#endif
}

bool MainMenu::shouldDraw() const
{
#ifdef LMOD_VR
    return VRCapablePanel::shouldDraw() && context()->controller()->standby();
#else
    return InterfaceWidget::shouldDraw() && context()->controller()->standby();
#endif
}

void MainMenu::onConnect()
{
    try
    {
        context()->controller()->connect(
            { connect.host->content(), static_cast<u16>(stoi(connect.port->content())) },
            connect.serverPassword->content(),
            connect.name->content(),
            connect.password->content()
        );
    }
    catch (const exception& e)
    {

    }
}

void MainMenu::onHost()
{
    try
    {
        context()->controller()->host(
            { host.host->content(), static_cast<u16>(stoi(host.port->content())) },
            host.serverPassword->content(),
            host.adminPassword->content(),
            host.savePath->content(),
            host.name->content(),
            host.password->content()
        );
    }
    catch (const exception& e)
    {

    }
}

void MainMenu::onQuit()
{
    dialog = new PanelConfirmDialog(
        this,
        localize("main-menu-are-you-sure-you-want-to-quit"),
        [this](bool confirmed) -> void
        {
            if (confirmed)
            {
                context()->controller()->quit();
            }

            destroyDialog = true;
        }
    );
    dialog->resize(size());
    dialog->move(position());
}

ConnectionDetails MainMenu::loadConnectionDetails() const
{
    string path = configPath() + connectionFileName;

    if (!fs::exists(path))
    {
        return ConnectionDetails();
    }

    try
    {
        YAMLParser ctx(path);

        return ctx.root().as<ConnectionDetails>();
    }
    catch (const exception& e)
    {
        error("Failed to load connection details: " + string(e.what()));
        return ConnectionDetails();
    }
}

#ifdef LMOD_VR
optional<Point> MainMenu::toLocal(const vec3& position, const quaternion& rotation) const
{
    if (!firstClearDrawTime || currentTime() - *firstClearDrawTime < minClearDrawTime || !shouldDraw())
    {
        return {};
    }

    vec3 roomHeadPosition = context()->controller()->vr()->worldToRoom(context()->vrViewer()->headTransform().position());
    vec3 roomHandPosition = context()->controller()->vr()->worldToRoom(position);
    quaternion roomHandRotation = context()->controller()->vr()->worldToRoom(rotation);

    auto [ width, height ] = dynamic_cast<VRRender*>(context()->controller()->render())->size();

    mat4 model = createVRHUDTransform(
        roomHeadPosition,
        static_cast<f64>(width),
        static_cast<f64>(height),
        dynamic_cast<VRRender*>(context()->controller()->render())->angle()
    );

    vec3 localStart = model.applyToPosition(roomHandPosition, false);
    vec3 localDirection = model.applyToDirection(roomHandRotation.forward(), false);

    vec2 flatStart = vec2(localStart.x, localStart.z);
    vec2 flatDirection = vec2(localDirection.x, localDirection.z).normalize();

    f64 a = flatDirection.dot(flatDirection);
    f64 b = 2 * flatStart.dot(flatDirection);
    f64 c = flatStart.dot(flatStart) - sq(hudRadius);

    auto [ r0, r1 ] = quadratic(a, b, c);

    optional<Point> point;

    for (f64 distance : vector<f64>({ r0, r1 }))
    {
        if (isnan(distance) || distance < 0)
        {
            continue;
        }

        distance = hypot(distance, tan(angleBetween(localDirection, vec3(localDirection.x, 0, localDirection.z).normalize())) * distance);

        vec3 contact = localStart + (localDirection * distance);

        f64 angle = angleBetween(
            vec2(contact.x, contact.z).normalize(),
            vec2(-1, 0)
        );
        f64 elevation = contact.y;

        if (angle < pi * 0.2 ||
            angle >= pi * 0.8 ||
            elevation < -1 ||
            elevation >= +1
        )
        {
            continue;
        }

        f64 x = (angle - (pi * 0.2)) / (pi * 0.6);
        f64 y = ((elevation * -1) + 1) / 2;

        point = Point(x * hudWidth, y * hudHeight);
        break;
    }

    return point;
}

void MainMenu::changeFocus(const Input& input)
{
    if (input.isLeftHand())
    {
        scope()->movePointer(Pointer_VR_Left, *leftPointer, false);
    }
    else if (input.isRightHand())
    {
        scope()->movePointer(Pointer_VR_Right, *rightPointer, false);
    }
}
#endif

SizeProperties MainMenu::sizeProperties() const
{
    return sizePropertiesFillAll;
}
