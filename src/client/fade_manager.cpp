/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "fade_manager.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "vr_render.hpp"
#include "flat_render.hpp"

#include "render_vr_fade_effect.hpp"
#include "render_flat_fade_effect.hpp"
#include "sound_fade_effect.hpp"

static constexpr chrono::milliseconds fadeDuration = chrono::seconds(1);

FadeManager::FadeManager(ControlContext* ctx)
    : ctx(ctx)
    , fading(false)
    , startTime(0)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        _renderPostProcessID = dynamic_cast<VRRender*>(ctx->controller()->render())->addPostProcessEffect<RenderVRFadeEffect>(0);
    }
    else
    {
        _renderPostProcessID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addPostProcessEffect<RenderFlatFadeEffect>(0);
    }
#else
    _renderPostProcessID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addPostProcessEffect<RenderFlatFadeEffect>(0);
#endif

    _soundPostProcessID = ctx->controller()->sound()->addPostProcessEffect<SoundFadeEffect>(0);

    ctx->controller()->render()->setPostProcessEffectActive(_renderPostProcessID, false);
    ctx->controller()->sound()->setPostProcessEffectActive(_soundPostProcessID, false);
}

void FadeManager::step()
{
    optional<f64> strength;

    if (fading)
    {
        strength = min((currentTime() - startTime).count() / static_cast<f64>(fadeDuration.count()), 1.0);
    }
    else if (!fading && currentTime() - startTime < fadeDuration)
    {
        strength = max(1 - ((currentTime() - startTime).count() / static_cast<f64>(fadeDuration.count())), 0.0);
    }
    else
    {
        ctx->controller()->render()->setPostProcessEffectActive(_renderPostProcessID, false);
    }

    if (strength)
    {
#ifdef LMOD_VR
        if (g_vr)
        {
            dynamic_cast<VRRender*>(ctx->controller()->render())->updatePostProcessEffect<RenderVRFadeEffect>(
                _renderPostProcessID,
                *strength
            );
        }
        else
        {
            dynamic_cast<FlatRender*>(ctx->controller()->render())->updatePostProcessEffect<RenderFlatFadeEffect>(
                _renderPostProcessID,
                *strength
            );
        }
#else
        dynamic_cast<FlatRender*>(ctx->controller()->render())->updatePostProcessEffect<RenderFlatFadeEffect>(
            _renderPostProcessID,
            *strength
        );
#endif

        ctx->controller()->sound()->updatePostProcessEffect<SoundFadeEffect>(_soundPostProcessID, *strength);
    }
}

void FadeManager::fadeOut()
{
    fading = true;
    startTime = currentTime();

    ctx->controller()->render()->setPostProcessEffectActive(_renderPostProcessID, true);
    ctx->controller()->sound()->setPostProcessEffectActive(_soundPostProcessID, true);
}

void FadeManager::fadeIn()
{
    fading = false;
    startTime = currentTime();
}

RenderPostProcessEffectID FadeManager::renderPostProcessID() const
{
    return _renderPostProcessID;
}

SoundPostProcessEffectID FadeManager::soundPostProcessID() const
{
    return _soundPostProcessID;
}
