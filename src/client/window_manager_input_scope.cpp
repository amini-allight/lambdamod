/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "window_manager_input_scope.hpp"
#include "interface_widget.hpp"
#include "control_context.hpp"

WindowManagerInputScope::WindowManagerInputScope(
    ControlContext* ctx,
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<bool(const Input&)>& isActive,
    const function<void(InputScope*)>& block
)
    : InputScope(
        ctx,
        parent,
        name,
        mode,
        isActive,
        block
    )
{

}

void WindowManagerInputScope::sort()
{
    InputScope* activeScope = nullptr;

    for (size_t i = 0; i < children.size(); i++)
    {
        auto scope = dynamic_cast<WidgetInputScope*>(children.at(i));

        if (scope->widget()->focused())
        {
            activeScope = scope;
            children.erase(children.begin() + i);
            break;
        }
    }

    if (!activeScope)
    {
        return;
    }

    children.push_back(activeScope);
}
