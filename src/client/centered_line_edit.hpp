/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "line_edit.hpp"

class CenteredLineEdit : public LineEdit
{
public:
    CenteredLineEdit(
        InterfaceWidget* parent,
        const function<string()>& source,
        const function<void(const string&)>& onReturn
    );

    void draw(const DrawContext& ctx) const override;

private:
    i32 screenToIndex(const Point& point) const override;
    Point indexToScreen(i32 index) const;

    SizeProperties sizeProperties() const override;
};
