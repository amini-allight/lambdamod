/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_occlusion_scene_reference.hpp"

RenderOcclusionSceneReference::RenderOcclusionSceneReference(
    VkAccelerationStructureKHR leftScene,
    VkAccelerationStructureKHR rightScene
)
    : _hardwareAccelerated(true)
    , _hardwareScene{ leftScene, rightScene }
    , _softwareScene(nullptr)
{

}

RenderOcclusionSceneReference::RenderOcclusionSceneReference(VkBuffer scene)
    : _hardwareAccelerated(false)
    , _hardwareScene{ nullptr, nullptr }
    , _softwareScene(scene)
{

}

bool RenderOcclusionSceneReference::hardwareAccelerated() const
{
    return _hardwareAccelerated;
}

VkAccelerationStructureKHR RenderOcclusionSceneReference::hardwareScene(size_t index) const
{
    return _hardwareScene[index];
}

VkBuffer RenderOcclusionSceneReference::softwareScene() const
{
    return _softwareScene;
}
