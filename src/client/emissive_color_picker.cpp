/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "emissive_color_picker.hpp"
#include "localization.hpp"
#include "theme.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "control_context.hpp"
#include "label.hpp"
#include "checkbox.hpp"
#include "ufloat_edit.hpp"
#include "unit_field.hpp"
#include "spacer.hpp"
#include "row.hpp"
#include "column.hpp"
#include "color_saturation_value_picker.hpp"
#include "color_hue_picker.hpp"
#include "fixed_row.hpp"

EmissiveColorPickerPopup::EmissiveColorPickerPopup(
    InterfaceWidget* parent,
    const EmissiveColor& color,
    const function<void(const EmissiveColor&)>& onSet,
    bool infiniteDistance
)
    : Popup(parent)
    , color(color)
    , onSet(onSet)
{
    auto column = new Column(this);

    auto row = new Row(column);

    new ColorSaturationValuePicker(
        row,
        bind(&EmissiveColorPickerPopup::hueSource, this),
        bind(&EmissiveColorPickerPopup::onSaturationValueSet, this, placeholders::_1)
    );

    MAKE_SPACER(row, UIScale::marginSize(), 0);

    new ColorHuePicker(
        row,
        bind(&EmissiveColorPickerPopup::hueSource, this),
        bind(&EmissiveColorPickerPopup::onHueSet, this, placeholders::_1)
    );

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto text = new LineEdit(
        column,
        bind(&EmissiveColorPickerPopup::textSource, this),
        bind(&EmissiveColorPickerPopup::onTextReturn, this, placeholders::_1)
    );
    text->setPlaceholder(localize("hdr-color-picker-hex-color"));

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto intensity = new FixedRow(column, UIScale::lineEditHeight());

    new Label(
        intensity,
        localize("hdr-color-picker-intensity")
    );

    new UnitField(intensity, infiniteDistance ? illuminanceSuffix() : luminousFluxSuffix(), [this](InterfaceWidget* parent) -> void {
        auto intensityEdit = new UFloatEdit(
            parent,
            bind(&EmissiveColorPickerPopup::intensitySource, this),
            bind(&EmissiveColorPickerPopup::onIntensity, this, placeholders::_1)
        );
        intensityEdit->setPlaceholder(localize("hdr-color-picker-intensity"));
    });

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto emissive = new FixedRow(column, UIScale::lineEditHeight());

    new Label(
        emissive,
        localize("emissive-color-picker-emissive")
    );

    new Checkbox(
        emissive,
        bind(&EmissiveColorPickerPopup::emissiveSource, this),
        bind(&EmissiveColorPickerPopup::onEmissive, this, placeholders::_1)
    );
}

void EmissiveColorPickerPopup::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + UIScale::marginSize());
    }
}

void EmissiveColorPickerPopup::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + UIScale::marginSize());
        child->resize(this->size() - (UIScale::marginSize() * 2));
    }
}

void EmissiveColorPickerPopup::draw(const DrawContext& ctx) const
{
    ctx.delayedCommands.push_back([this](const DrawContext& ctx) -> void {
        drawSolid(
            ctx,
            this,
            theme.indentColor
        );
    });

    for (InterfaceWidget* child : children())
    {
        ctx.delayedCommands.push_back([child](const DrawContext& ctx) -> void {
            child->draw(ctx);
        });
    }
}

f64 EmissiveColorPickerPopup::hueSource()
{
    return rgbToHSV(color.color).x;
}

void EmissiveColorPickerPopup::onHueSet(f64 hue)
{
    vec3 hsv = rgbToHSV(color.color);

    hsv.x = hue;

    if (roughly<f64>(hsv.y, 0))
    {
        hsv.y = 1;
    }

    if (roughly<f64>(hsv.z, 0))
    {
        hsv.z = 1;
    }

    EmissiveColor color = this->color;
    color.color = hsvToRGB(hsv);

    onSet(color);
}

void EmissiveColorPickerPopup::onSaturationValueSet(const vec2& saturationValue)
{
    vec3 hsv = rgbToHSV(color.color);

    hsv.y = saturationValue.x;
    hsv.z = saturationValue.y;

    EmissiveColor color = this->color;
    color.color = hsvToRGB(hsv);

    onSet(color);
}

string EmissiveColorPickerPopup::textSource()
{
    return Color(color.color).toString(false);
}

void EmissiveColorPickerPopup::onTextReturn(const string& text)
{
    Color color(text);
    color.a = 255;

    onSet(EmissiveColor(color.toVec3(), this->color.intensity, this->color.emissive));
}

f64 EmissiveColorPickerPopup::intensitySource()
{
    return color.intensity;
}

void EmissiveColorPickerPopup::onIntensity(f64 intensity)
{
    onSet(EmissiveColor(color.color, intensity, color.emissive));
}

bool EmissiveColorPickerPopup::emissiveSource()
{
    return color.emissive;
}

void EmissiveColorPickerPopup::onEmissive(bool state)
{
    onSet(EmissiveColor(color.color, color.intensity, state));
}

SizeProperties EmissiveColorPickerPopup::sizeProperties() const
{
    return sizePropertiesFillAll;
}

EmissiveColorPicker::EmissiveColorPicker(
    InterfaceWidget* parent,
    const function<EmissiveColor()>& source,
    const function<void(const EmissiveColor&)>& onSet,
    bool infiniteDistance
)
    : InterfaceWidget(parent)
    , source(source)
    , _onSet(onSet)
{
    START_WIDGET_SCOPE("emissive-color-picker")
        WIDGET_SLOT("interact", interact)
    END_SCOPE

    popup = new EmissiveColorPickerPopup(
        this,
        color,
        bind(&EmissiveColorPicker::onSet, this, placeholders::_1),
        infiniteDistance
    );
}

EmissiveColorPicker::~EmissiveColorPicker()
{
    delete popup;
}

void EmissiveColorPicker::move(const Point& position)
{
    this->position(position);

    popup->move(position + Point(0, UIScale::colorPickerHeight()));
}

void EmissiveColorPicker::resize(const Size& size)
{
    this->size(size);

    popup->resize(Size(this->size().w, UIScale::emissiveColorPickerPopupHeight()));
}

void EmissiveColorPicker::step()
{
    InterfaceWidget::step();

    EmissiveColor newColor = source();

    if (newColor != color)
    {
        color = newColor;
    }

    if (!focused() && !popup->focused() && popup->opened())
    {
        popup->close();
    }
}

void EmissiveColorPicker::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        Color(color.color)
    );

    // NOTE: This doesn't use theme colors because it needs a color that's reliably very dark and another that's reliably very bright for superimposing over user-selected colors
    Color fgColor = rgbToHSV(color.color).z > 0.5 ? black : white;

    TextStyle style;
    style.alignment = Text_Center;
    style.color = fgColor;

    drawText(
        ctx,
        this,
        toPrettyString(color.intensity),
        style
    );

    if (color.emissive)
    {
        drawColoredImage(
            ctx,
            this,
            Point(UIScale::marginSize()),
            Size(UIScale::colorPickerHeight() - UIScale::marginSize() * 2),
            Icon_Light,
            fgColor
        );
    }

    drawBorder(
        ctx,
        this,
        UIScale::marginSize(),
        focused() ? theme.accentColor : theme.outdentColor
    );

    InterfaceWidget::draw(ctx);
}

void EmissiveColorPicker::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void EmissiveColorPicker::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void EmissiveColorPicker::onSet(const EmissiveColor& color)
{
    this->color = color;

    _onSet(color);
}

void EmissiveColorPicker::interact(const Input& input)
{
    if (popup->opened())
    {
        popup->close();
        playNegativeActivateEffect();
    }
    else
    {
        popup->open();
        playPositiveActivateEffect();
    }

    if (popup->opened())
    {
        update();
    }
}

SizeProperties EmissiveColorPicker::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::colorPickerHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
