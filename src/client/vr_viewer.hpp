/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "constants.hpp"
#include "vr_device.hpp"

class ControlContext;
class VRRender;

class VRViewer
{
public:
    VRViewer(ControlContext* ctx);
    VRViewer(const VRViewer& rhs) = delete;
    VRViewer(VRViewer&& rhs) = delete;
    ~VRViewer();

    VRViewer& operator=(const VRViewer& rhs) = delete;
    VRViewer& operator=(VRViewer&& rhs) = delete;

    void moveRoom(const vec3& position, const quaternion& rotation);

    void step();
    void moveDevice(VRDevice device, const vec3& position, const quaternion& rotation);
    void stickMotion(bool right, const vec3& position);
    void touchpadMotion(bool right, bool state, const vec3& position);

    void ascend(bool state);
    void descend(bool state);

    void sprint(bool state);
    void attach(bool right);

    mat4 roomTransform() const;

    mat4 headTransform() const;
    mat4 leftHandTransform() const;
    mat4 rightHandTransform() const;
    mat4 hipsTransform() const;
    mat4 leftFootTransform() const;
    mat4 rightFootTransform() const;
    mat4 chestTransform() const;
    mat4 leftShoulderTransform() const;
    mat4 rightShoulderTransform() const;
    mat4 leftElbowTransform() const;
    mat4 rightElbowTransform() const;
    mat4 leftWristTransform() const;
    mat4 rightWristTransform() const;
    mat4 leftKneeTransform() const;
    mat4 rightKneeTransform() const;
    mat4 leftAnkleTransform() const;
    mat4 rightAnkleTransform() const;

    map<VRDevice, mat4> mapping() const;

private:
    ControlContext* ctx;

    vec3 roomPosition;
    quaternion roomRotation;

    struct {
        bool headValid = false;
        mat4 head;
        bool handValid[bodyPartCount]{ false, false };
        mat4 hand[bodyPartCount];
        bool hipsValid = false;
        mat4 hips;
        bool footValid[bodyPartCount]{ false, false };
        mat4 foot[bodyPartCount];
        bool chestValid = false;
        mat4 chest;
        bool shoulderValid[bodyPartCount]{ false, false };
        mat4 shoulder[bodyPartCount];
        bool elbowValid[bodyPartCount]{ false, false };
        mat4 elbow[bodyPartCount];
        bool wristValid[bodyPartCount]{ false, false };
        mat4 wrist[bodyPartCount];
        bool kneeValid[bodyPartCount]{ false, false };
        mat4 knee[bodyPartCount];
        bool ankleValid[bodyPartCount]{ false, false };
        mat4 ankle[bodyPartCount];

    } transforms;
    vec3 stickMotions[handCount];
    chrono::milliseconds lastStickMotionTime[handCount];
    vec3 touchpadMotions[handCount];
    chrono::milliseconds lastTouchpadMotionTime[handCount];
    bool ascending;
    bool descending;
    bool sprinting;

    RenderDecorationID leftHandID;
    RenderDecorationID rightHandID;
    RenderDecorationID hipsID;
    RenderDecorationID leftFootID;
    RenderDecorationID rightFootID;
    RenderDecorationID chestID;
    RenderDecorationID leftShoulderID;
    RenderDecorationID rightShoulderID;
    RenderDecorationID leftElbowID;
    RenderDecorationID rightElbowID;
    RenderDecorationID leftWristID;
    RenderDecorationID rightWristID;
    RenderDecorationID leftKneeID;
    RenderDecorationID rightKneeID;
    RenderDecorationID leftAnkleID;
    RenderDecorationID rightAnkleID;
    RenderDecorationID leftLaserID;
    RenderDecorationID rightLaserID;
    RenderDecorationID trackingCalibratorID;
    RenderDecorationID gridID;
    RenderDecorationID rollCageID;

    void updateRoom();
    void updateServerRoom();
    void updateServerViewer(const vec3& position, const quaternion& rotation);

    VRRender* render() const;
};
#endif
