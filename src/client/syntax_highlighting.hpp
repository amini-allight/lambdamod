/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"

enum SyntaxHighlightType : u8
{
    Syntax_Highlight_Escaped,
    Syntax_Highlight_String,
    Syntax_Highlight_Keyword,
    Syntax_Highlight_Stdlib,
    Syntax_Highlight_Bracket,
    Syntax_Highlight_Quote,
    Syntax_Highlight_Number,
    Syntax_Highlight_Text
};

struct SyntaxHighlightChunk
{
    SyntaxHighlightType type;
    i32 start;
    i32 size;

    bool operator>(const SyntaxHighlightChunk& rhs) const;
    bool operator<(const SyntaxHighlightChunk& rhs) const;
};

Color syntaxHighlightColor(SyntaxHighlightType type);
vector<SyntaxHighlightChunk> calculateSyntaxHighlight(const string& line);

tuple<i32, i32> bracketMatchHighlight(
    const string& text,
    i32 cursorIndex,
    i32 windowStart,
    i32 windowSize
);
