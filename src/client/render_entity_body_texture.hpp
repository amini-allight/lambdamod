/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_context.hpp"
#include "body_render_data.hpp"
#include "render_entity_body_texture_tile.hpp"

class RenderEntityBodyTexture
{
public:
    RenderEntityBodyTexture(const RenderContext* ctx, const BodyRenderMaterialTextureData& texture);
    RenderEntityBodyTexture(const RenderEntityBodyTexture& rhs) = delete;
    RenderEntityBodyTexture(RenderEntityBodyTexture&& rhs) = delete;
    ~RenderEntityBodyTexture();

    RenderEntityBodyTexture& operator=(const RenderEntityBodyTexture& rhs) = delete;
    RenderEntityBodyTexture& operator=(RenderEntityBodyTexture&& rhs) = delete;

    bool supports(const BodyRenderMaterialTextureData& texture) const;
    void fill(const BodyRenderMaterialTextureData& texture);
    void setupLayout(VkCommandBuffer commandBuffer);

    vector<VkImageView> imageViews() const;

    vector<RenderEntityBodyTextureTile*> tiles;

private:
    u32 width;
    u32 height;
    bool needsLayoutSetup;
};
