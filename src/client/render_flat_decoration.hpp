/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "render_decoration.hpp"
#include "render_flat_pipeline_store.hpp"

class RenderFlatDecoration : public RenderDecoration
{
public:
    RenderFlatDecoration(
        const RenderContext* ctx,
        VkDescriptorSetLayout descriptorSetLayout,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    );
    virtual ~RenderFlatDecoration() = 0;

    virtual void work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const;
    void refresh(const vector<FlatSwapchainElement*>& swapchainElements);

protected:
    mutable mat4 transform;

    void fillCommandBuffer(
        const MainSwapchainElement* swapchainElement,
        VkDescriptorSet descriptorSet,
        VkBuffer vertices,
        u32 vertexCount
    ) const;

    virtual VmaBuffer createUniformBuffer() const = 0;

    void createComponents(const vector<FlatSwapchainElement*>& swapchainElements);
};
