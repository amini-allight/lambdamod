/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_part_selection.hpp"

BodyPartSelectionID::BodyPartSelectionID()
{

}

BodyPartSelectionID::BodyPartSelectionID(BodyPartID id, BodyPartSubID subID)
    : id(id)
    , subID(subID)
{

}

bool BodyPartSelectionID::operator==(const BodyPartSelectionID& rhs) const
{
    return id == rhs.id && subID == rhs.subID;
}

bool BodyPartSelectionID::operator!=(const BodyPartSelectionID& rhs) const
{
    return !(*this == rhs);
}

bool BodyPartSelectionID::operator>(const BodyPartSelectionID& rhs) const
{
    if (id > rhs.id)
    {
        return true;
    }
    else if (id < rhs.id)
    {
        return false;
    }
    else
    {
        if (subID > rhs.subID)
        {
            return true;
        }
        else if (subID < rhs.subID)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool BodyPartSelectionID::operator<(const BodyPartSelectionID& rhs) const
{
    if (id < rhs.id)
    {
        return true;
    }
    else if (id > rhs.id)
    {
        return false;
    }
    else
    {
        if (subID < rhs.subID)
        {
            return true;
        }
        else if (subID > rhs.subID)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool BodyPartSelectionID::operator>=(const BodyPartSelectionID& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool BodyPartSelectionID::operator<=(const BodyPartSelectionID& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering BodyPartSelectionID::operator<=>(const BodyPartSelectionID& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

BodyPartSelection::BodyPartSelection()
{
    part = nullptr;
}

BodyPartSelection::BodyPartSelection(BodyPart* part, BodyPartSubID subID)
    : part(part)
    , subID(subID)
{

}

bool BodyPartSelection::operator==(const BodyPartSelection& rhs) const
{
    return part->id() == rhs.part->id() &&
        subID == rhs.subID;
}

bool BodyPartSelection::operator!=(const BodyPartSelection& rhs) const
{
    return !(*this == rhs);
}

bool BodyPartSelection::operator>(const BodyPartSelection& rhs) const
{
    if (part->id() > rhs.part->id())
    {
        return true;
    }
    else if (part->id() < rhs.part->id())
    {
        return false;
    }
    else
    {
        if (subID > rhs.subID)
        {
            return true;
        }
        else if (subID < rhs.subID)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool BodyPartSelection::operator<(const BodyPartSelection& rhs) const
{
    if (part->id() < rhs.part->id())
    {
        return true;
    }
    else if (part->id() > rhs.part->id())
    {
        return false;
    }
    else
    {
        if (subID < rhs.subID)
        {
            return true;
        }
        else if (subID > rhs.subID)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool BodyPartSelection::operator>=(const BodyPartSelection& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool BodyPartSelection::operator<=(const BodyPartSelection& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering BodyPartSelection::operator<=>(const BodyPartSelection& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}
