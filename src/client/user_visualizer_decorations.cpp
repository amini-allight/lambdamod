/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_visualizer_decorations.hpp"
#include "controller.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "global.hpp"
#include "render_flat_viewer.hpp"
#include "render_vr_viewer.hpp"
#include "render_flat_room_bounds.hpp"
#include "render_vr_room_bounds.hpp"
#include "render_flat_hardware_device.hpp"
#include "render_vr_hardware_device.hpp"
#include "flat_render.hpp"
#include "vr_render.hpp"

UserVisualizerDecorations::UserVisualizerDecorations(ControlContext* ctx, const User* user)
    : ctx(ctx)
    , id(user->id())
    , viewerID(0)
    , roomID(0)
    , headID(0)
    , leftHandID(0)
    , rightHandID(0)
    , hipsID(0)
    , leftFootID(0)
    , rightFootID(0)
    , chestID(0)
    , leftShoulderID(0)
    , rightShoulderID(0)
    , leftElbowID(0)
    , rightElbowID(0)
    , leftWristID(0)
    , rightWristID(0)
    , leftKneeID(0)
    , rightKneeID(0)
    , leftAnkleID(0)
    , rightAnkleID(0)
{

}

UserVisualizerDecorations::~UserVisualizerDecorations()
{
    remove(viewerID);
    remove(roomID);
    remove(headID);
    remove(leftHandID);
    remove(rightHandID);
    remove(hipsID);
    remove(leftFootID);
    remove(rightFootID);
    remove(chestID);
    remove(leftShoulderID);
    remove(rightShoulderID);
    remove(leftElbowID);
    remove(rightElbowID);
    remove(leftWristID);
    remove(rightWristID);
    remove(leftKneeID);
    remove(rightKneeID);
    remove(leftAnkleID);
    remove(rightAnkleID);

    ctx->interface()->clearMarker(id.value());
}

void UserVisualizerDecorations::join()
{
    updateVisibility();
}

void UserVisualizerDecorations::leave()
{
    updateVisibility();
}

void UserVisualizerDecorations::setShownWorld(const string& name)
{
    updateVisibility();
}

void UserVisualizerDecorations::setShown(bool state)
{
    updateVisibility();
}

void UserVisualizerDecorations::setWorld(const string& name)
{
    updateVisibility();
}

void UserVisualizerDecorations::moveViewer(const mat4& transform)
{
    const User* user = getUser();

    if (visible())
    {
        ctx->interface()->setMarker(id.value(), Icon_User, user->name(), user->viewer().position(), user->color());
    }

    if (user->vr())
    {
        return;
    }

    if (viewerID)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatViewer, RenderVRViewer>(
            viewerID,
            transform.position(),
            transform.rotation(),
            *angle,
            *aspect
        );
#else
        updateDecoration<RenderFlatViewer>(
            viewerID,
            transform.position(),
            transform.rotation(),
            *angle,
            *aspect
        );
#endif
    }
    else if (angle && aspect)
    {
#ifdef LMOD_VR
        viewerID = addDecoration<RenderFlatViewer, RenderVRViewer>(
            transform.position(),
            transform.rotation(),
            *angle,
            *aspect
        );
#else
        viewerID = addDecoration<RenderFlatViewer>(
            transform.position(),
            transform.rotation(),
            *angle,
            *aspect
        );
#endif
    }

    updateVisibility(viewerID, visible());

    viewerTransform = transform;
}

void UserVisualizerDecorations::setViewerAngle(f64 angle)
{
    const User* user = getUser();

    if (user->vr())
    {
        return;
    }

    if (viewerID)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatViewer, RenderVRViewer>(
            viewerID,
            viewerTransform->position(),
            viewerTransform->rotation(),
            angle,
            *aspect
        );
#else
        updateDecoration<RenderFlatViewer>(
            viewerID,
            viewerTransform->position(),
            viewerTransform->rotation(),
            angle,
            *aspect
        );
#endif
    }
    else if (viewerTransform && aspect)
    {
#ifdef LMOD_VR
        viewerID = addDecoration<RenderFlatViewer, RenderVRViewer>(
            viewerTransform->position(),
            viewerTransform->rotation(),
            angle,
            *aspect
        );
#else
        viewerID = addDecoration<RenderFlatViewer>(
            viewerTransform->position(),
            viewerTransform->rotation(),
            angle,
            *aspect
        );
#endif
    }

    updateVisibility(viewerID, visible());

    this->angle = angle;
}

void UserVisualizerDecorations::setViewerAspect(f64 aspect)
{
    const User* user = getUser();

    if (user->vr())
    {
        return;
    }

    if (viewerID)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatViewer, RenderVRViewer>(
            viewerID,
            viewerTransform->position(),
            viewerTransform->rotation(),
            *angle,
            aspect
        );
#else
        updateDecoration<RenderFlatViewer>(
            viewerID,
            viewerTransform->position(),
            viewerTransform->rotation(),
            *angle,
            aspect
        );
#endif
    }
    else if (viewerTransform && angle)
    {
#ifdef LMOD_VR
        viewerID = addDecoration<RenderFlatViewer, RenderVRViewer>(
            viewerTransform->position(),
            viewerTransform->rotation(),
            *angle,
            aspect
        );
#else
        viewerID = addDecoration<RenderFlatViewer>(
            viewerTransform->position(),
            viewerTransform->rotation(),
            *angle,
            aspect
        );
#endif
    }

    updateVisibility(viewerID, visible());

    this->aspect = aspect;
}

void UserVisualizerDecorations::moveRoom(const mat4& transform)
{
    const User* user = getUser();

    if (!user->vr())
    {
        return;
    }

    if (roomID)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatRoomBounds, RenderVRRoomBounds>(
            roomID,
            transform.position() + roomOffset->position(),
            transform.rotation() * roomOffset->rotation(),
            *bounds
        );
#else
        updateDecoration<RenderFlatRoomBounds>(
            roomID,
            transform.position() + roomOffset->position(),
            transform.rotation() * roomOffset->rotation(),
            *bounds
        );
#endif
    }
    else if (bounds && roomOffset)
    {
#ifdef LMOD_VR
        roomID = addDecoration<RenderFlatRoomBounds, RenderVRRoomBounds>(
            transform.position() + roomOffset->position(),
            transform.rotation() * roomOffset->rotation(),
            *bounds
        );
#else
        roomID = addDecoration<RenderFlatRoomBounds>(
            transform.position() + roomOffset->position(),
            transform.rotation() * roomOffset->rotation(),
            *bounds
        );
#endif
    }

    updateVisibility(roomID, visible());

    roomTransform = transform;
}

void UserVisualizerDecorations::setRoomBounds(const vec2& bounds)
{
    const User* user = getUser();

    if (!user->vr())
    {
        return;
    }

    if (roomID)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatRoomBounds, RenderVRRoomBounds>(
            roomID,
            roomTransform->position() + roomOffset->position(),
            roomTransform->rotation() * roomOffset->rotation(),
            bounds
        );
#else
        updateDecoration<RenderFlatRoomBounds>(
            roomID,
            roomTransform->position() + roomOffset->position(),
            roomTransform->rotation() * roomOffset->rotation(),
            bounds
        );
#endif
    }
    else if (roomTransform && roomOffset)
    {
#ifdef LMOD_VR
        roomID = addDecoration<RenderFlatRoomBounds, RenderVRRoomBounds>(
            roomTransform->position() + roomOffset->position(),
            roomTransform->rotation() * roomOffset->rotation(),
            bounds
        );
#else
        roomID = addDecoration<RenderFlatRoomBounds>(
            roomTransform->position() + roomOffset->position(),
            roomTransform->rotation() * roomOffset->rotation(),
            bounds
        );
#endif
    }

    updateVisibility(roomID, visible());

    this->bounds = bounds;
}

void UserVisualizerDecorations::setRoomOffset(const mat4& offset)
{
    const User* user = getUser();

    if (!user->vr())
    {
        return;
    }

    if (roomID)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatRoomBounds, RenderVRRoomBounds>(
            roomID,
            roomTransform->position() + offset.position(),
            roomTransform->rotation() * offset.rotation(),
            *bounds
        );
#else

#endif
    }
    else if (roomTransform && bounds)
    {
#ifdef LMOD_VR
        roomID = addDecoration<RenderFlatRoomBounds, RenderVRRoomBounds>(
            roomTransform->position() + offset.position(),
            roomTransform->rotation() * offset.rotation(),
            *bounds
        );
#else
        roomID = addDecoration<RenderFlatRoomBounds>(
            roomTransform->position() + offset.position(),
            roomTransform->rotation() * offset.rotation(),
            *bounds
        );
#endif
    }

    updateVisibility(roomID, visible());

    this->roomOffset = offset;
}

void UserVisualizerDecorations::moveDevice(VRDevice device, const mat4& transform)
{
    const User* user = getUser();

    if (!user->vr())
    {
        return;
    }

    RenderDecorationID* id = nullptr;

    switch (device)
    {
    case VR_Device_Head :
        id = &headID;
        break;
    case VR_Device_Left_Hand :
        id = &leftHandID;
        break;
    case VR_Device_Right_Hand:
        id = &rightHandID;
        break;
    case VR_Device_Hips :
        id = &hipsID;
        break;
    case VR_Device_Left_Foot :
        id = &leftFootID;
        break;
    case VR_Device_Right_Foot :
        id = &rightFootID;
        break;
    case VR_Device_Chest :
        id = &chestID;
        break;
    case VR_Device_Left_Shoulder :
        id = &leftShoulderID;
        break;
    case VR_Device_Right_Shoulder :
        id = &rightShoulderID;
        break;
    case VR_Device_Left_Elbow :
        id = &leftElbowID;
        break;
    case VR_Device_Right_Elbow :
        id = &rightElbowID;
        break;
    case VR_Device_Left_Wrist :
        id = &leftWristID;
        break;
    case VR_Device_Right_Wrist :
        id = &rightWristID;
        break;
    case VR_Device_Left_Knee :
        id = &leftKneeID;
        break;
    case VR_Device_Right_Knee :
        id = &rightKneeID;
        break;
    case VR_Device_Left_Ankle :
        id = &leftAnkleID;
        break;
    case VR_Device_Right_Ankle :
        id = &rightAnkleID;
        break;
    }

    if (*id)
    {
#ifdef LMOD_VR
        updateDecoration<RenderFlatHardwareDevice, RenderVRHardwareDevice>(
            *id,
            device,
            transform.position(),
            transform.rotation()
        );
#else
        updateDecoration<RenderFlatHardwareDevice>(
            *id,
            device,
            transform.position(),
            transform.rotation()
        );
#endif
    }
    else
    {
#ifdef LMOD_VR
        *id = addDecoration<RenderFlatHardwareDevice, RenderVRHardwareDevice>(
            device,
            transform.position(),
            transform.rotation()
        );
#else
        *id = addDecoration<RenderFlatHardwareDevice>(
            device,
            transform.position(),
            transform.rotation()
        );
#endif
    }

    updateVisibility(*id, visible());
}

void UserVisualizerDecorations::remove(RenderDecorationID id) const
{
    if (!id)
    {
        return;
    }

    ctx->controller()->render()->removeDecoration(id);
}

bool UserVisualizerDecorations::visible() const
{
    const User* user = getUser();

    return user->active() &&
        user->world() == ctx->controller()->self()->world() &&
        !ctx->controller()->self()->attached();
}

void UserVisualizerDecorations::updateVisibility()
{
    const User* user = getUser();

    bool state = visible();

    updateVisibility(viewerID, state);
    updateVisibility(roomID, state);
    updateVisibility(headID, state);
    updateVisibility(leftHandID, state);
    updateVisibility(rightHandID, state);
    updateVisibility(hipsID, state);
    updateVisibility(leftFootID, state);
    updateVisibility(rightFootID, state);
    updateVisibility(chestID, state);
    updateVisibility(leftElbowID, state);
    updateVisibility(rightElbowID, state);
    updateVisibility(leftKneeID, state);
    updateVisibility(rightKneeID, state);

    if (state)
    {
        ctx->interface()->setMarker(id.value(), Icon_User, user->name(), user->viewer().position(), user->color());
    }
    else
    {
        ctx->interface()->clearMarker(id.value());
    }
}

void UserVisualizerDecorations::updateVisibility(RenderDecorationID id, bool state)
{
    if (!id)
    {
        return;
    }

    ctx->controller()->render()->setDecorationShown(id, state);
}

const User* UserVisualizerDecorations::getUser() const
{
    return ctx->controller()->game().getUser(id);
}

#ifdef LMOD_VR
template<typename FlatT, typename VRT, typename... Args>
RenderDecorationID UserVisualizerDecorations::addDecoration(Args... args)
{
    if (g_vr)
    {
        return dynamic_cast<VRRender*>(ctx->controller()->render())->addDecoration<VRT>(args...);
    }
    else
    {
        return dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<FlatT>(args...);
    }
}

template<typename FlatT, typename VRT, typename... Args>
void UserVisualizerDecorations::updateDecoration(RenderDecorationID id, Args... args)
{
    if (g_vr)
    {
        dynamic_cast<VRRender*>(ctx->controller()->render())->updateDecoration<VRT>(id, args...);
    }
    else
    {
        dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<FlatT>(id, args...);
    }
}
#else
template<typename T, typename... Args>
RenderDecorationID UserVisualizerDecorations::addDecoration(Args... args)
{
    return dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<T>(args...);
}

template<typename T, typename... Args>
void UserVisualizerDecorations::updateDecoration(RenderDecorationID id, Args... args)
{
    dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<T>(id, args...);
}
#endif
