/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class BindingsSettings : public InterfaceWidget, public TabContent
{
public:
    BindingsSettings(InterfaceWidget* parent);

private:
    string bindingSource(const string& name);
    void onBinding(const string& name, const string& value);

    string toString(const vector<InputScopeBinding>& bindings) const;
    vector<InputScopeBinding> fromString(const string& value) const;

    SizeProperties sizeProperties() const override;
};
