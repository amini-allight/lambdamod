/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "theme_settings.hpp"
#include "theme.hpp"
#include "theme_loader.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "alpha_color_picker.hpp"

ThemeSettings::ThemeSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "theme-settings-panel-foreground-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::panelForegroundColorSource, this),
            bind(&ThemeSettings::onPanelForegroundColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-world-background-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::worldBackgroundColorSource, this),
            bind(&ThemeSettings::onWorldBackgroundColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-background-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::backgroundColorSource, this),
            bind(&ThemeSettings::onBackgroundColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-alt-background-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::altBackgroundColorSource, this),
            bind(&ThemeSettings::onAltBackgroundColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-soft-indent-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::softIndentColorSource, this),
            bind(&ThemeSettings::onSoftIndentColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-indent-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::indentColorSource, this),
            bind(&ThemeSettings::onIndentColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-outdent-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::outdentColorSource, this),
            bind(&ThemeSettings::onOutdentColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-accent-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::accentColorSource, this),
            bind(&ThemeSettings::onAccentColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-active-text-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::activeTextColorSource, this),
            bind(&ThemeSettings::onActiveTextColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-text-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::textColorSource, this),
            bind(&ThemeSettings::onTextColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-inactive-text-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::inactiveTextColorSource, this),
            bind(&ThemeSettings::onInactiveTextColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-warning-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::warningColorSource, this),
            bind(&ThemeSettings::onWarningColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-error-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::errorColorSource, this),
            bind(&ThemeSettings::onErrorColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-divider-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::dividerColorSource, this),
            bind(&ThemeSettings::onDividerColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-positive-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::positiveColorSource, this),
            bind(&ThemeSettings::onPositiveColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-negative-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::negativeColorSource, this),
            bind(&ThemeSettings::onNegativeColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-keyword-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::keywordColorSource, this),
            bind(&ThemeSettings::onKeywordColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-stdlib-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::stdlibColorSource, this),
            bind(&ThemeSettings::onStdlibColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-bracket-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::bracketColorSource, this),
            bind(&ThemeSettings::onBracketColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-quote-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::quoteColorSource, this),
            bind(&ThemeSettings::onQuoteColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-escaped-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::escapedColorSource, this),
            bind(&ThemeSettings::onEscapedColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-string-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::stringColorSource, this),
            bind(&ThemeSettings::onStringColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-number-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::numberColorSource, this),
            bind(&ThemeSettings::onNumberColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-cursor-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::cursorColorSource, this),
            bind(&ThemeSettings::onCursorColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-bracket-highlight-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::bracketHighlightColorSource, this),
            bind(&ThemeSettings::onBracketHighlightColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-x-axis-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::xAxisColorSource, this),
            bind(&ThemeSettings::onXAxisColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-y-axis-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::yAxisColorSource, this),
            bind(&ThemeSettings::onYAxisColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-z-axis-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::zAxisColorSource, this),
            bind(&ThemeSettings::onZAxisColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-grid-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::gridColorSource, this),
            bind(&ThemeSettings::onGridColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-grid-secondary-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::gridSecondaryColorSource, this),
            bind(&ThemeSettings::onGridSecondaryColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-touch-interface-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::touchInterfaceColorSource, this),
            bind(&ThemeSettings::onTouchInterfaceColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-touch-interface-active-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::touchInterfaceActiveColorSource, this),
            bind(&ThemeSettings::onTouchInterfaceActiveColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-accent-gradient-x-start-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::accentGradientXStartColorSource, this),
            bind(&ThemeSettings::onAccentGradientXStartColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-accent-gradient-x-end-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::accentGradientXEndColorSource, this),
            bind(&ThemeSettings::onAccentGradientXEndColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-accent-gradient-y-start-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::accentGradientYStartColorSource, this),
            bind(&ThemeSettings::onAccentGradientYStartColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "theme-settings-accent-gradient-y-end-color", [this](InterfaceWidget* parent) -> void {
        new AlphaColorPicker(
            parent,
            bind(&ThemeSettings::accentGradientYEndColorSource, this),
            bind(&ThemeSettings::onAccentGradientYEndColor, this, placeholders::_1)
        );
    });
}

Color ThemeSettings::panelForegroundColorSource()
{
    return theme.panelForegroundColor;
}

void ThemeSettings::onPanelForegroundColor(const Color& color)
{
    theme.panelForegroundColor = color;

    saveTheme();
}

Color ThemeSettings::worldBackgroundColorSource()
{
    return theme.worldBackgroundColor;
}

void ThemeSettings::onWorldBackgroundColor(const Color& color)
{
    theme.worldBackgroundColor = color;

    saveTheme();
}

Color ThemeSettings::backgroundColorSource()
{
    return theme.backgroundColor;
}

void ThemeSettings::onBackgroundColor(const Color& color)
{
    theme.backgroundColor = color;

    saveTheme();
}

Color ThemeSettings::altBackgroundColorSource()
{
    return theme.altBackgroundColor;
}

void ThemeSettings::onAltBackgroundColor(const Color& color)
{
    theme.altBackgroundColor = color;

    saveTheme();
}

Color ThemeSettings::softIndentColorSource()
{
    return theme.softIndentColor;
}

void ThemeSettings::onSoftIndentColor(const Color& color)
{
    theme.softIndentColor = color;

    saveTheme();
}

Color ThemeSettings::indentColorSource()
{
    return theme.indentColor;
}

void ThemeSettings::onIndentColor(const Color& color)
{
    theme.indentColor = color;

    saveTheme();
}

Color ThemeSettings::outdentColorSource()
{
    return theme.outdentColor;
}

void ThemeSettings::onOutdentColor(const Color& color)
{
    theme.outdentColor = color;

    saveTheme();
}

Color ThemeSettings::accentColorSource()
{
    return theme.accentColor;
}

void ThemeSettings::onAccentColor(const Color& color)
{
    theme.accentColor = color;

    saveTheme();
}

Color ThemeSettings::activeTextColorSource()
{
    return theme.activeTextColor;
}

void ThemeSettings::onActiveTextColor(const Color& color)
{
    theme.activeTextColor = color;

    saveTheme();
}

Color ThemeSettings::textColorSource()
{
    return theme.textColor;
}

void ThemeSettings::onTextColor(const Color& color)
{
    theme.textColor = color;

    saveTheme();
}

Color ThemeSettings::inactiveTextColorSource()
{
    return theme.inactiveTextColor;
}

void ThemeSettings::onInactiveTextColor(const Color& color)
{
    theme.inactiveTextColor = color;

    saveTheme();
}

Color ThemeSettings::warningColorSource()
{
    return theme.warningColor;
}

void ThemeSettings::onWarningColor(const Color& color)
{
    theme.warningColor = color;

    saveTheme();
}

Color ThemeSettings::errorColorSource()
{
    return theme.errorColor;
}

void ThemeSettings::onErrorColor(const Color& color)
{
    theme.errorColor = color;

    saveTheme();
}

Color ThemeSettings::dividerColorSource()
{
    return theme.dividerColor;
}

void ThemeSettings::onDividerColor(const Color& color)
{
    theme.dividerColor = color;

    saveTheme();
}

Color ThemeSettings::positiveColorSource()
{
    return theme.positiveColor;
}

void ThemeSettings::onPositiveColor(const Color& color)
{
    theme.positiveColor = color;

    saveTheme();
}

Color ThemeSettings::negativeColorSource()
{
    return theme.negativeColor;
}

void ThemeSettings::onNegativeColor(const Color& color)
{
    theme.negativeColor = color;

    saveTheme();
}

Color ThemeSettings::keywordColorSource()
{
    return theme.keywordColor;
}

void ThemeSettings::onKeywordColor(const Color& color)
{
    theme.keywordColor = color;

    saveTheme();
}

Color ThemeSettings::stdlibColorSource()
{
    return theme.stdlibColor;
}

void ThemeSettings::onStdlibColor(const Color& color)
{
    theme.stdlibColor = color;

    saveTheme();
}

Color ThemeSettings::bracketColorSource()
{
    return theme.bracketColor;
}

void ThemeSettings::onBracketColor(const Color& color)
{
    theme.bracketColor = color;

    saveTheme();
}

Color ThemeSettings::quoteColorSource()
{
    return theme.quoteColor;
}

void ThemeSettings::onQuoteColor(const Color& color)
{
    theme.quoteColor = color;

    saveTheme();
}

Color ThemeSettings::escapedColorSource()
{
    return theme.escapedColor;
}

void ThemeSettings::onEscapedColor(const Color& color)
{
    theme.escapedColor = color;

    saveTheme();
}

Color ThemeSettings::stringColorSource()
{
    return theme.stringColor;
}

void ThemeSettings::onStringColor(const Color& color)
{
    theme.stringColor = color;

    saveTheme();
}

Color ThemeSettings::numberColorSource()
{
    return theme.numberColor;
}

void ThemeSettings::onNumberColor(const Color& color)
{
    theme.numberColor = color;

    saveTheme();
}

Color ThemeSettings::cursorColorSource()
{
    return theme.cursorColor;
}

void ThemeSettings::onCursorColor(const Color& color)
{
    theme.cursorColor = color;

    saveTheme();
}

Color ThemeSettings::bracketHighlightColorSource()
{
    return theme.bracketHighlightColor;
}

void ThemeSettings::onBracketHighlightColor(const Color& color)
{
    theme.bracketHighlightColor = color;

    saveTheme();
}

Color ThemeSettings::xAxisColorSource()
{
    return theme.xAxisColor;
}

void ThemeSettings::onXAxisColor(const Color& color)
{
    theme.xAxisColor = color;

    saveTheme();
}

Color ThemeSettings::yAxisColorSource()
{
    return theme.yAxisColor;
}

void ThemeSettings::onYAxisColor(const Color& color)
{
    theme.yAxisColor = color;

    saveTheme();
}

Color ThemeSettings::zAxisColorSource()
{
    return theme.zAxisColor;
}

void ThemeSettings::onZAxisColor(const Color& color)
{
    theme.zAxisColor = color;

    saveTheme();
}

Color ThemeSettings::gridColorSource()
{
    return theme.gridColor;
}

void ThemeSettings::onGridColor(const Color& color)
{
    theme.gridColor = color;

    saveTheme();
}

Color ThemeSettings::gridSecondaryColorSource()
{
    return theme.gridSecondaryColor;
}

void ThemeSettings::onGridSecondaryColor(const Color& color)
{
    theme.gridSecondaryColor = color;

    saveTheme();
}

Color ThemeSettings::touchInterfaceColorSource()
{
    return theme.touchInterfaceColor;
}

void ThemeSettings::onTouchInterfaceColor(const Color& color)
{
    theme.touchInterfaceColor = color;

    saveTheme();
}

Color ThemeSettings::touchInterfaceActiveColorSource()
{
    return theme.touchInterfaceActiveColor;
}

void ThemeSettings::onTouchInterfaceActiveColor(const Color& color)
{
    theme.touchInterfaceActiveColor = color;

    saveTheme();
}

Color ThemeSettings::accentGradientXStartColorSource()
{
    return theme.accentGradientXStartColor;
}

void ThemeSettings::onAccentGradientXStartColor(const Color& color)
{
    theme.accentGradientXStartColor = color;

    saveTheme();
}

Color ThemeSettings::accentGradientXEndColorSource()
{
    return theme.accentGradientXEndColor;
}

void ThemeSettings::onAccentGradientXEndColor(const Color& color)
{
    theme.accentGradientXEndColor = color;

    saveTheme();
}

Color ThemeSettings::accentGradientYStartColorSource()
{
    return theme.accentGradientYStartColor;
}

void ThemeSettings::onAccentGradientYStartColor(const Color& color)
{
    theme.accentGradientYStartColor = color;

    saveTheme();
}

Color ThemeSettings::accentGradientYEndColorSource()
{
    return theme.accentGradientYEndColor;
}

void ThemeSettings::onAccentGradientYEndColor(const Color& color)
{
    theme.accentGradientYEndColor = color;

    saveTheme();
}

SizeProperties ThemeSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
