/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "font_face.hpp"
#include "text_style.hpp"

struct FontKey
{
    TextFont font;
    TextWeight weight;
    u32 size;

    bool operator==(const FontKey& rhs) const;
    bool operator!=(const FontKey& rhs) const;
    bool operator>(const FontKey& rhs) const;
    bool operator<(const FontKey& rhs) const;
    bool operator>=(const FontKey& rhs) const;
    bool operator<=(const FontKey& rhs) const;
    strong_ordering operator<=>(const FontKey& rhs) const;
};

class FontStore
{
public:
    FontStore();
    FontStore(const FontStore& rhs) = delete;
    FontStore(FontStore&& rhs) = delete;
    ~FontStore();

    FontStore& operator=(const FontStore& rhs) = delete;
    FontStore& operator=(FontStore&& rhs) = delete;

    FontFace* get(TextFont font, TextWeight weight, u32 size);

private:
    map<FontKey, FontFace*> fonts;

    string pathFromProperties(TextFont font, TextWeight weight) const;
};

extern FontStore* fontStore;
