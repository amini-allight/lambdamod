/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "audio_settings.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "flat_render.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "checkbox.hpp"
#include "float_edit.hpp"
#include "percent_edit.hpp"
#include "option_select.hpp"

AudioSettings::AudioSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "audio-settings-mute-output", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&AudioSettings::outputMuteSource, this),
            bind(&AudioSettings::onOutputMute, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "audio-settings-output-volume", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&AudioSettings::outputVolumeSource, this),
            bind(&AudioSettings::onOutputVolume, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "audio-settings-mute-input", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&AudioSettings::inputMuteSource, this),
            bind(&AudioSettings::onInputMute, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "audio-settings-input-volume", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&AudioSettings::inputVolumeSource, this),
            bind(&AudioSettings::onInputVolume, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "audio-settings-sound-spatial-mode", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&AudioSettings::soundSpatialModeOptionsSource, this),
            bind(&AudioSettings::soundSpatialModeOptionTooltipsSource, this),
            bind(&AudioSettings::soundSpatialModeSource, this),
            bind(&AudioSettings::onSoundSpatialMode, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "audio-settings-sound-mono", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&AudioSettings::soundMonoSource, this),
            bind(&AudioSettings::onSoundMono, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "audio-settings-ui-sounds", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&AudioSettings::uiSoundsSource, this),
            bind(&AudioSettings::onUISounds, this, placeholders::_1)
        );
    });
}

bool AudioSettings::outputMuteSource()
{
    return context()->controller()->soundDevice()->outputMuted();
}

void AudioSettings::onOutputMute(bool state)
{
    context()->controller()->soundDevice()->setOutputMuted(state);
}

f64 AudioSettings::outputVolumeSource()
{
    return context()->controller()->soundDevice()->outputVolume();
}

void AudioSettings::onOutputVolume(f64 volume)
{
    context()->controller()->soundDevice()->setOutputVolume(volume);
}

bool AudioSettings::inputMuteSource()
{
    return context()->controller()->soundDevice()->inputMuted();
}

void AudioSettings::onInputMute(bool state)
{
    context()->controller()->soundDevice()->setInputMuted(state);
}

f64 AudioSettings::inputVolumeSource()
{
    return context()->controller()->soundDevice()->inputVolume();
}

void AudioSettings::onInputVolume(f64 volume)
{
    context()->controller()->soundDevice()->setInputVolume(volume);
}

vector<string> AudioSettings::soundSpatialModeOptionsSource()
{
    return {
        localize("audio-settings-sound-spatial-mode-headphones"),
        localize("audio-settings-sound-spatial-mode-speakers")
    };
}

vector<string> AudioSettings::soundSpatialModeOptionTooltipsSource()
{
    return {
        localize("audio-settings-sound-spatial-mode-headphones-tooltip"),
        localize("audio-settings-sound-spatial-mode-speakers-tooltip")
    };
}

size_t AudioSettings::soundSpatialModeSource()
{
    return static_cast<size_t>(g_config.soundSpatialMode);
}

void AudioSettings::onSoundSpatialMode(size_t index)
{
    g_config.soundSpatialMode = static_cast<SoundSpatialMode>(index);

    saveConfig(g_config);
}

bool AudioSettings::soundMonoSource()
{
    return g_config.soundMono;
}

void AudioSettings::onSoundMono(bool state)
{
    g_config.soundMono = state;

    saveConfig(g_config);
}

bool AudioSettings::uiSoundsSource()
{
    return g_config.uiSounds;
}

void AudioSettings::onUISounds(bool state)
{
    g_config.uiSounds = state;

    saveConfig(g_config);
}

SizeProperties AudioSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
