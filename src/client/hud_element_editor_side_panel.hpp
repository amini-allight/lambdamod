/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "side_panel.hpp"
#include "hud_element.hpp"

class ListView;
class HUDElementEditor;

class HUDElementEditorSidePanel : public SidePanel
{
public:
    HUDElementEditorSidePanel(InterfaceWidget* parent);

    void step() override;

private:
    ListView* listView;

    string name;
    HUDElementType type;

    void addCommonFields();
    void addLineFields();
    void addTextFields();
    void addBarFields();
    void addSymbolFields();
    void clearChildren();

    vector<string> typesSource();
    vector<string> typeTooltipsSource();
    size_t typeSource();
    void onType(size_t index);

    vector<string> anchorTypesSource();
    vector<string> anchorTypeTooltipsSource();
    size_t anchorTypeSource();
    void onAnchorType(size_t index);

    vec2 positionSource();
    void onPosition(vec2 value);

    vec3 colorSource();
    void onColor(const vec3& color);

    f64 lengthSource();
    void onLength(f64 value);

    f64 rotationSource();
    void onRotation(f64 value);

    string textSource();
    void onText(const string& text);

    f64 fractionSource();
    void onFraction(f64 value);

    bool rightToLeftSource();
    void onRightToLeft(bool state);

    vector<string> symbolTypesSource();
    vector<string> symbolTypeTooltipsSource();
    size_t symbolTypeSource();
    void onSymbolType(size_t index);

    f64 sizeSource();
    void onSize(f64 value);

    HUDElementEditor* editor() const;
};
