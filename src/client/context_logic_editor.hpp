/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "logic_editor.hpp"

class ContextLogicEditor final : public LogicEditor
{
public:
    ContextLogicEditor(
        InterfaceWidget* parent,
        const function<Value()>& source,
        const function<void(const Value&)>& onSave
    );

    void step() override;

private:
    function<Value()> source;
    function<void(const Value&)> onSave;

    Value lastNodeEditorAssignment;
    Value lastCodeEditorAssignment;

    ScriptNodeFormat nodeFormatSource() override;
    void onNodeFormatSave(const ScriptNodeFormat& format) override;

    string textSource() override;
    void onTextSave(const string& text) override;
};
