/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"
#include "hud_element.hpp"

enum HUDElementEditorMode : u8
{
    HUD_Element_Editor_No_Mode,
    HUD_Element_Editor_Translate,
    HUD_Element_Editor_Rotate,
    HUD_Element_Editor_Scale
};

enum HUDElementEditorSubMode : u8
{
    HUD_Element_Editor_Sub_None,
    HUD_Element_Editor_Sub_X,
    HUD_Element_Editor_Sub_Y
};

class Column;
class HUDPreview;
class HUDElementEditorSidePanel;

class HUDElementEditor : public InterfaceWidget
{
public:
    HUDElementEditor(
        InterfaceWidget* parent,
        EntityID id,
        const function<void(size_t)>& onSelect
    );

    void open(const string& name);

    void move(const Point& position) override;
    void resize(const Size& size) override;

private:
    friend class HUDElementEditorSidePanel;

    EntityID id;
    string name;

    Column* column;
    HUDPreview* preview;
    HUDElementEditorSidePanel* sidePanel;

    function<void(size_t)> onSelect;

    void updateEntity(const HUDElement& element);

    const HUDElement& currentElement() const;

    void toggleSidePanel(const Input& input);

    SizeProperties sizeProperties() const override;
};
