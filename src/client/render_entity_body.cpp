/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_entity_body.hpp"
#include "render_tools.hpp"

RenderEntityBody::RenderEntityBody(
    const RenderContext* ctx,
    const RenderPipelineStore* pipelineStore,
    const vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>>& renderData
)
{
    for (const auto& [ type, material ] : renderData)
    {
        materials.push_back({ type, new RenderEntityBodyMaterial(
            ctx,
            pipelineStore,
            type,
            material
        ) });
    }
}

RenderEntityBody::~RenderEntityBody()
{
    for (const auto& [ type, material ] : materials)
    {
        delete material;
    }
}

bool RenderEntityBody::supports(const vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>>& materials) const
{
    if (this->materials.size() != materials.size())
    {
        return false;
    }

    for (size_t i = 0; i < this->materials.size(); i++)
    {
        const auto& [ type, material ] = this->materials.at(i);

        if (type != get<0>(materials.at(i)) || !material->supports(get<1>(materials.at(i))))
        {
            return false;
        }
    }

    return true;
}

void RenderEntityBody::fill(const vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>>& materials)
{
    for (size_t i = 0; i < this->materials.size(); i++)
    {
        const auto& [ type, material ] = this->materials.at(i);

        material->fill(get<1>(materials.at(i)));
    }
}

void RenderEntityBody::setupTextureLayouts(VkCommandBuffer commandBuffer)
{
    for (const auto& [ type, material ] : this->materials)
    {
        material->setupTextureLayouts(commandBuffer);
    }
}
