/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_settings.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "checkbox.hpp"
#include "vec2_edit.hpp"
#include "vec2_percent_edit.hpp"

InputSettings::InputSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "input-settings-touch-input", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InputSettings::touchInputSource, this),
            bind(&InputSettings::onTouchInput, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-invert-mouse-viewer-x", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InputSettings::invertMouseViewerXSource, this),
            bind(&InputSettings::onInvertMouseViewerX, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-invert-mouse-viewer-y", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InputSettings::invertMouseViewerYSource, this),
            bind(&InputSettings::onInvertMouseViewerY, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-mouse-viewer-sensitivity", [this](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&InputSettings::mouseViewerSensitivitySource, this),
            bind(&InputSettings::onMouseViewerSensitivity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-invert-gamepad-viewer-x", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InputSettings::invertGamepadViewerXSource, this),
            bind(&InputSettings::onInvertGamepadViewerX, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-invert-gamepad-viewer-y", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&InputSettings::invertGamepadViewerYSource, this),
            bind(&InputSettings::onInvertGamepadViewerY, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-gamepad-viewer-sensitivity", [this](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&InputSettings::gamepadViewerSensitivitySource, this),
            bind(&InputSettings::onGamepadViewerSensitivity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-gamepad-left-dead-zone", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec2PercentEdit(
            parent,
            bind(&InputSettings::gamepadLeftDeadZoneSource, this),
            bind(&InputSettings::onGamepadLeftDeadZone, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "input-settings-gamepad-right-dead-zone", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec2PercentEdit(
            parent,
            bind(&InputSettings::gamepadRightDeadZoneSource, this),
            bind(&InputSettings::onGamepadRightDeadZone, this, placeholders::_1)
        );
    });
}

bool InputSettings::touchInputSource()
{
    return g_config.touchInput;
}

void InputSettings::onTouchInput(bool state)
{
    g_config.touchInput = state;

    saveConfig(g_config);
}

bool InputSettings::invertMouseViewerXSource()
{
    return g_config.invertMouseViewerX;
}

void InputSettings::onInvertMouseViewerX(bool state)
{
    g_config.invertMouseViewerX = state;

    saveConfig(g_config);
}

bool InputSettings::invertMouseViewerYSource()
{
    return g_config.invertMouseViewerY;
}

void InputSettings::onInvertMouseViewerY(bool state)
{
    g_config.invertMouseViewerY = state;

    saveConfig(g_config);
}

vec2 InputSettings::mouseViewerSensitivitySource()
{
    return g_config.mouseViewerSensitivity;
}

void InputSettings::onMouseViewerSensitivity(const vec2& sensitivity)
{
    g_config.mouseViewerSensitivity = sensitivity;

    saveConfig(g_config);
}

bool InputSettings::invertGamepadViewerXSource()
{
    return g_config.invertGamepadViewerX;
}

void InputSettings::onInvertGamepadViewerX(bool state)
{
    g_config.invertGamepadViewerX = state;

    saveConfig(g_config);
}

bool InputSettings::invertGamepadViewerYSource()
{
    return g_config.invertGamepadViewerY;
}

void InputSettings::onInvertGamepadViewerY(bool state)
{
    g_config.invertGamepadViewerY = state;

    saveConfig(g_config);
}

vec2 InputSettings::gamepadViewerSensitivitySource()
{
    return g_config.gamepadViewerSensitivity;
}

void InputSettings::onGamepadViewerSensitivity(const vec2& sensitivity)
{
    g_config.gamepadViewerSensitivity = sensitivity;

    saveConfig(g_config);
}

vec2 InputSettings::gamepadLeftDeadZoneSource()
{
    return g_config.gamepadLeftDeadZone;
}

void InputSettings::onGamepadLeftDeadZone(const vec2& deadZone)
{
    g_config.gamepadLeftDeadZone = vec2(
        clamp<f64>(deadZone.x, 0, 1),
        clamp<f64>(deadZone.y, 0, 1)
    );

    saveConfig(g_config);
}

vec2 InputSettings::gamepadRightDeadZoneSource()
{
    return g_config.gamepadRightDeadZone;
}

void InputSettings::onGamepadRightDeadZone(const vec2& deadZone)
{
    g_config.gamepadRightDeadZone = vec2(
        clamp<f64>(deadZone.x, 0, 1),
        clamp<f64>(deadZone.y, 0, 1)
    );

    saveConfig(g_config);
}

SizeProperties InputSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
