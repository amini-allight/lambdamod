/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tab_pane.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "tab_view.hpp"

TabPane::TabPane(InterfaceWidget* parent, size_t index)
    : InterfaceWidget(parent)
    , index(index)
{
    START_WIDGET_SCOPE("tab-pane")

    END_SCOPE
}

bool TabPane::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && dynamic_cast<TabView*>(parent())->activeIndex() == index;
}

SizeProperties TabPane::sizeProperties() const
{
    return sizePropertiesFillAll;
}
