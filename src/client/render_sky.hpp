/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_swapchain_elements.hpp"
#include "render_sky_component.hpp"
#include "sky_parameters.hpp"

class RenderSky
{
public:
    RenderSky(
        const RenderContext* ctx,
        VkDescriptorSetLayout descriptorSetLayout,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline,
        VkPipelineLayout starsPipelineLayout,
        VkPipeline starsPipeline,
        VkPipelineLayout circlePipelineLayout,
        VkPipeline circlePipeline,
        VkPipelineLayout cloudPipelineLayout,
        VkPipeline cloudPipeline,
        VkPipelineLayout auroraPipelineLayout,
        VkPipeline auroraPipeline,
        VkPipelineLayout cometPipelineLayout,
        VkPipeline cometPipeline,
        VkPipelineLayout vortexPipelineLayout,
        VkPipeline vortexPipeline,
        VkPipelineLayout envMapPipelineLayout,
        VkPipeline envMapPipeline,
        VkPipelineLayout envMapStarsPipelineLayout,
        VkPipeline envMapStarsPipeline,
        VkPipelineLayout envMapCirclePipelineLayout,
        VkPipeline envMapCirclePipeline,
        VkPipelineLayout envMapCloudPipelineLayout,
        VkPipeline envMapCloudPipeline,
        VkPipelineLayout envMapAuroraPipelineLayout,
        VkPipeline envMapAuroraPipeline,
        VkPipelineLayout envMapCometPipelineLayout,
        VkPipeline envMapCometPipeline,
        VkPipelineLayout envMapVortexPipelineLayout,
        VkPipeline envMapVortexPipeline
    );
    RenderSky(const RenderSky& rhs) = delete;
    RenderSky(RenderSky&& rhs) = delete;
    ~RenderSky();

    RenderSky& operator=(const RenderSky& rhs) = delete;
    RenderSky& operator=(RenderSky&& rhs) = delete;

protected:
    const RenderContext* ctx;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    VkPipelineLayout starsPipelineLayout;
    VkPipeline starsPipeline;
    VkPipelineLayout circlePipelineLayout;
    VkPipeline circlePipeline;
    VkPipelineLayout cloudPipelineLayout;
    VkPipeline cloudPipeline;
    VkPipelineLayout auroraPipelineLayout;
    VkPipeline auroraPipeline;
    VkPipelineLayout cometPipelineLayout;
    VkPipeline cometPipeline;
    VkPipelineLayout vortexPipelineLayout;
    VkPipeline vortexPipeline;
    VkPipelineLayout envMapPipelineLayout;
    VkPipeline envMapPipeline;
    VkPipelineLayout envMapStarsPipelineLayout;
    VkPipeline envMapStarsPipeline;
    VkPipelineLayout envMapCirclePipelineLayout;
    VkPipeline envMapCirclePipeline;
    VkPipelineLayout envMapCloudPipelineLayout;
    VkPipeline envMapCloudPipeline;
    VkPipelineLayout envMapAuroraPipelineLayout;
    VkPipeline envMapAuroraPipeline;
    VkPipelineLayout envMapCometPipelineLayout;
    VkPipeline envMapCometPipeline;
    VkPipelineLayout envMapVortexPipelineLayout;
    VkPipeline envMapVortexPipeline;

    vector<RenderSkyComponent*> components;

    virtual void createComponents() = 0;
    void destroyComponents();

    void fillUniformBuffer(
        RenderSkyComponent* component,
        const SkyParameters& sky,
        const vec3& up
    ) const;
    void fillCommandBuffer(
        const MainSwapchainElement* swapchainElement,
        const RenderSkyComponent* component
    ) const;
    void fillEnvMapCommandBuffer(
        const SkySwapchainElement* swapchainElement,
        const RenderSkyComponent* component
    ) const;
};
