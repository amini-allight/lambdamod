/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "message.hpp"
#include "interface_widget.hpp"
#include "list_view.hpp"
#include "syntax_highlighting.hpp"

class ConsoleOutput : public InterfaceWidget
{
public:
    ConsoleOutput(InterfaceWidget* parent);

    void push(Message message);

    void draw(const DrawContext& ctx) const override;

private:
    vector<Message> messages;
    size_t scrollIndex;

    map<size_t, vector<SyntaxHighlightChunk>> syntaxHighlight;

    i32 charWidth() const;
    i32 lineWidth() const;
    size_t pageHeight() const;

    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);
    void pageUp(const Input& input);
    void pageDown(const Input& input);

    SizeProperties sizeProperties() const override;
};
