/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

inline tuple<vec2, vec2> createScissor(
    const vec2& contentPoint,
    const vec2& contentSize,
    const vec2& point,
    const vec2& size
)
{
    i32 x = point.x;

    if (x < 0)
    {
        x = 0;
    }

    i32 y = point.y;

    if (y < 0)
    {
        y = 0;
    }

    x += contentPoint.x;

    if (x < 0)
    {
        x = 0;
    }

    y += contentPoint.y;

    if (y < 0)
    {
        y = 0;
    }

    u32 w = size.x;

    if (i32 overflow = (point.x + size.x) - contentSize.x; overflow > 0)
    {
        if (static_cast<u32>(overflow) > w)
        {
            w = 0;
        }
        else
        {
            w -= overflow;
        }
    }

    u32 h = size.y;

    if (i32 overflow = (point.y + size.y) - contentSize.y; overflow > 0)
    {
        if (static_cast<u32>(overflow) > h)
        {
            h = 0;
        }
        else
        {
            h -= overflow;
        }
    }

    return {
        vec2(x, y),
        vec2(w, h)
    };
}
