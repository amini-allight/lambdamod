/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "choose_major_block.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

ChooseMajorBlock::ChooseMajorBlock(
    InterfaceWidget* parent,
    const string& title,
    const string& subtitle,
    const string& text,
    const function<void()>& onActivate
)
    : InterfaceWidget(parent)
    , title(title)
    , subtitle(subtitle)
    , text(text)
    , onActivate(onActivate)
{
    START_WIDGET_SCOPE("choose-major-block")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void ChooseMajorBlock::draw(const DrawContext& ctx) const
{
    drawBorder(
        ctx,
        this,
        UIScale::marginSize(this),
        theme.panelForegroundColor
    );

    TextStyle titleStyle(this);
    titleStyle.weight = Text_Bold;

    drawText(
        ctx,
        this,
        Point(UIScale::marginSize(this) * 2),
        Size(ctx.size.w - (UIScale::marginSize(this) * 4), UIScale::panelChooseMajorBlockTitleHeight(this)),
        title,
        titleStyle
    );

    TextStyle subtitleStyle(this);
    subtitleStyle.size = UIScale::mediumFontSize(this);
    subtitleStyle.weight = Text_Bold;

    drawText(
        ctx,
        this,
        Point(UIScale::marginSize(this) * 2) + Point(0, UIScale::panelChooseMajorBlockTitleHeight(this)),
        Size(ctx.size.w - (UIScale::marginSize(this) * 4), UIScale::panelChooseMajorBlockSubtitleHeight(this)),
        subtitle,
        subtitleStyle
    );

    TextStyle textStyle(this);
    textStyle.size = UIScale::mediumFontSize(this);

    drawText(
        ctx,
        this,
        Point(UIScale::marginSize(this) * 2) + Point(0, UIScale::panelChooseMajorBlockTitleHeight(this) + UIScale::panelChooseMajorBlockSubtitleHeight(this)),
        Size(ctx.size.w - (UIScale::marginSize(this) * 4), UIScale::panelChooseMajorBlockTextHeight(this)),
        text,
        textStyle
    );
}

void ChooseMajorBlock::interact(const Input& input)
{
    onActivate();
}

SizeProperties ChooseMajorBlock::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::panelChooseMajorBlockHeight(this)),
        Scaling_Fill, Scaling_Fixed
    };
}
