/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_interface.hpp"
#include "render_context.hpp"
#include "render_deletion_task.hpp"
#include "render_flat_sky.hpp"
#include "render_flat_atmosphere.hpp"
#include "render_flat_pipeline_store.hpp"
#include "render_flat_entity.hpp"
#include "render_flat_decoration.hpp"
#include "render_flat_post_process_effect.hpp"

class Controller;

class FlatRender final : public RenderInterface
{
public:
    FlatRender(Controller* controller, RenderContext* ctx);
    ~FlatRender() override;

    void waitIdle() const override;

    void step() override;

    tuple<u32, u32> reinit(u32 width, u32 height) override;

    void setCameraSpace(const quaternion& space) override;
    void setCameraPerspective();
    void setCameraOrthographic(f64 size);
    void setCamera(const vec3& position, const quaternion& rotation);
    void setCameraAngle(f64 angle);

    void add(EntityID parentID, const Entity* entity) override;
    void remove(EntityID parentID, EntityID id) override;
    void update(EntityID parentID, const Entity* entity) override;

    template<typename T, typename... Args>
    RenderDecorationID addDecoration(Args... args)
    {
        auto decoration = new T(
            ctx,
            swapchainElements,
            pipelineStore->objectDescriptorSetLayout,
            pipelineStore,
            args...
        );

        RenderDecorationID id = nextDecorationID++;

        decorations.insert({ id, decoration });

        return id;
    }
    void removeDecoration(RenderDecorationID id) override;
    template<typename T, typename... Args>
    void updateDecoration(RenderDecorationID id, Args... args)
    {
        auto it = decorations.find(id);

        if (it == decorations.end())
        {
            return;
        }

        RenderFlatDecoration* previous = it->second;

        decorationsToDelete.push_back(RenderDeletionTask(previous));

        decorations.erase(it);

        auto decoration = new T(
            ctx,
            swapchainElements,
            pipelineStore->objectDescriptorSetLayout,
            pipelineStore,
            args...
        );

        decorations.insert({ id, decoration });
    }
    void setDecorationShown(RenderDecorationID id, bool state) override;

    template<typename T, typename... Args>
    RenderPostProcessEffectID addPostProcessEffect(Args... args)
    {
        auto effect = new T(
            ctx,
            pipelineStore,
            swapchainElements,
            args...
        );

        RenderPostProcessEffectID id = nextPostProcessEffectID++;

        postProcessEffects.insert({ id, effect });

        return id;
    }
    void removePostProcessEffect(RenderPostProcessEffectID id) override;
    template<typename T, typename... Args>
    void updatePostProcessEffect(RenderPostProcessEffectID id, Args... args)
    {
        auto it = postProcessEffects.find(id);

        if (it == postProcessEffects.end())
        {
            return;
        }

        dynamic_cast<T*>(it->second)->update(args...);
    }
    void setPostProcessEffectActive(RenderPostProcessEffectID id, bool state) override;
    void setPostProcessEffectOrder(const vector<RenderPostProcessEffectID>& order) override;

    RenderContext* context() const override;
    u32 imageCount() const override;
    u32 currentImage() const override;
    VkRenderPass panelRenderPass() const override;
    VkRenderPass uiRenderPass() const;
    VkFramebuffer textTitlecardFramebuffer() const override;
    VkFramebuffer textFramebuffer() const override;

    f32 timer() const override;

    bool rayTracingSupported() const override;
    bool rayTracingEnabled() const override;

    f32 fogDistance() const override;

    u32 width() const;
    u32 height() const;

private:
    Controller* controller;

    RenderContext* ctx;

    u32 _width;
    u32 _height;
    VkFormat format;

    VkSurfaceKHR surface;
    VkSwapchainKHR swapchain;

    VkRenderPass _panelRenderPass;
    VkRenderPass skyRenderPass;
    VkRenderPass mainRenderPass;
    VkRenderPass hdrRenderPass;
    VkRenderPass postProcessRenderPass;
    VkRenderPass overlayRenderPass;
    VkRenderPass independentDepthOverlayRenderPass;
    VkRenderPass _uiRenderPass;

    RenderFlatPipelineStore* pipelineStore;

    vector<FlatSwapchainElement*> swapchainElements;

    RenderFlatPostProcessEffect* hdr;

    RenderFlatSky* sky;
    RenderFlatAtmosphere* atmosphere;

    map<EntityID, RenderFlatEntity*> entities;
    vector<RenderDeletionTask<RenderFlatEntity>> entitiesToDelete;

    RenderDecorationID nextDecorationID;
    map<RenderDecorationID, RenderFlatDecoration*> decorations;
    vector<RenderDeletionTask<RenderFlatDecoration>> decorationsToDelete;

    RenderPostProcessEffectID nextPostProcessEffectID;
    map<RenderPostProcessEffectID, RenderFlatPostProcessEffect*> postProcessEffects;
    vector<RenderDeletionTask<RenderFlatPostProcessEffect>> postProcessEffectsToDelete;
    vector<RenderPostProcessEffectID> postProcessOrder;

    quaternion cameraSpace;
    f64 cameraSize;
    f64 cameraAngle;
    bool cameraOrthographic;
    vec3 cameraPosition;
    quaternion cameraRotation;

    u32 currentFrame;
    u32 imageIndex;

    f32 exposure;
    chrono::microseconds lastWriteTime;

    f32 _timer;
    bool _rayTracing;

    u32 pickAPIVersion() const;
    set<string> instanceExtensions() const;
    VkPhysicalDevice pickPhysicalDevice() const;
    VkSurfaceKHR prepareSurface();
    set<string> deviceExtensions() const;
    u32 prepareSwapchain();

    void updateCameraUniforms();
    void updateTextTitlecardUniforms(const quaternion& rotation, const quaternion& space);
    void updateTextUniforms(const quaternion& rotation, const quaternion& space);

    void renderTextTitlecard(VkCommandBuffer commandBuffer);
    void renderText(VkCommandBuffer commandBuffer);
    void renderSky(const FlatSwapchainElement* swapchainElement);

    void outputImage(VkCommandBuffer commandBuffer, VkImage inputImage, VkImage outputImage);

    void advanceDeletionQueues();

    void createSwapchain();
    void destroySwapchain();
    void createSwapchainElements();
    void destroySwapchainElements();

    VkSampleCountFlagBits pickMSAALevel(u32 level) const;

    void computeLuminance(FlatSwapchainElement* element) const;
    f32 getLuminance(const FlatSwapchainElement* element) const;
    void adjustExposure(const chrono::microseconds& elapsed, f32 luminance);
};
