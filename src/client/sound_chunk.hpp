/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "tools.hpp"
#include "sound_constants.hpp"

template<int N>
class SoundStereoChunk_t;

template<int N>
class SoundChunk_t
{
public:
    SoundChunk_t()
    {
        data = new SoundChunkContent();
    }

    explicit SoundChunk_t(const f32* samples)
        : SoundChunk_t()
    {
        memcpy(data->data(), samples, data->size() * sizeof(f32));
    }

    SoundChunk_t(const SoundChunk_t& rhs)
        : SoundChunk_t()
    {
        memcpy(data->data(), rhs.data->data(), data->size() * sizeof(f32));
    }

    SoundChunk_t(SoundChunk_t&& rhs)
        : SoundChunk_t()
    {
        memcpy(data->data(), rhs.data->data(), data->size() * sizeof(f32));
    }

    ~SoundChunk_t()
    {
        delete data;
    }

    SoundChunk_t& operator=(const SoundChunk_t& rhs)
    {
        if (this != &rhs)
        {
            memcpy(data->data(), rhs.data->data(), data->size() * sizeof(f32));
        }

        return *this;
    }

    SoundChunk_t& operator=(SoundChunk_t&& rhs)
    {
        if (this != &rhs)
        {
            memcpy(data->data(), rhs.data->data(), data->size() * sizeof(f32));
        }

        return *this;
    }

    SoundChunk_t operator+(const SoundChunk_t& rhs) const
    {
        SoundChunk_t result = *this;

        for (size_t i = 0; i < length(); i++)
        {
            result.sample(i) += rhs.sample(i);
        }

        return result;
    }

    SoundChunk_t operator-(const SoundChunk_t& rhs) const
    {
        SoundChunk_t result = *this;

        for (size_t i = 0; i < length(); i++)
        {
            result.sample(i) -= rhs.sample(i);
        }

        return result;
    }

    SoundChunk_t operator*(f32 x) const
    {
        SoundChunk_t result;

        for (size_t i = 0; i < length(); i++)
        {
            result.sample(i) *= x;
        }

        return result;
    }

    SoundChunk_t operator/(f32 x) const
    {
        SoundChunk_t result;

        for (size_t i = 0; i < length(); i++)
        {
            result.sample(i) /= x;
        }

        return result;
    }

    SoundChunk_t& operator+=(const SoundChunk_t& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    SoundChunk_t& operator-=(const SoundChunk_t& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    SoundChunk_t& operator*=(f32 x)
    {
        *this = *this * x;

        return *this;
    }

    SoundChunk_t& operator/=(f32 x)
    {
        *this = *this / x;

        return *this;
    }

    SoundStereoChunk_t<N> toStereo() const;

    const f32* samples() const
    {
        return data->data();
    }

    const f32& sample(size_t index) const
    {
        return (*data)[index];
    }

    f32* samples()
    {
        return data->data();
    }

    f32& sample(size_t index)
    {
        return (*data)[index];
    }

    constexpr static size_t length()
    {
        return N;
    }

    f32 maxAmplitude() const
    {
        f32 result = 0;

        for (f32 sample : *data)
        {
            result = max(result, sample);
        }

        return result;
    }

    void normalize()
    {
        f32 max = maxAmplitude();

        for (f32& sample : *data)
        {
            sample /= max;
        }
    }

    void expose(f32 exposure)
    {
        for (f32& sample : *data)
        {
            sample = sign(sample) * (1 - exp(-abs(sample) * exposure));
        }
    }

    void counterExpose(f32 exposure)
    {
        for (f32& sample : *data)
        {
            sample = sign(sample) * -log(-abs(sample) + 1) / exposure;
        }
    }

private:
    typedef array<f32, N> SoundChunkContent;

    SoundChunkContent* data;
};

typedef SoundChunk_t<soundChunkSamplesPerChannel> SoundChunk;
typedef SoundChunk_t<soundChunkSamplesPerChannel + soundChunkOverlap * 2> SoundOverlappedChunk;

SoundChunk removeOverlap(const SoundOverlappedChunk& chunk);
