/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "render_entity.hpp"
#include "render_deletion_task.hpp"

class VRRender;

class RenderVREntity final : public RenderEntity
{
public:
    RenderVREntity(
        RenderInterface* render,

        vector<VRSwapchainElement*> swapchainElements,

        const RenderPipelineStore* pipelineStore,
        const Entity* entity,
        const BodyPartMask& bodyPartMask,
        vector<RenderDeletionTask<RenderVREntity>>* entitiesToDelete
    );
    ~RenderVREntity();

    void work(
        const VRSwapchainElement* swapchainElement,
        vec3 cameraPosition[eyeCount],
        deque<RenderTransparentDraw>* transparentDraws,
        const set<EntityID>& hiddenEntityIDs
    ) const;
    void update(const Entity* entity, const BodyPartMask& bodyPartMask) override;
    void refresh(vector<VRSwapchainElement*> swapchainElements);

    void add(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask) override;
    void remove(EntityID parentID, EntityID id) override;
    void update(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask) override;

    const map<EntityID, RenderVREntity*>& children() const;

private:
    vector<VRSwapchainElement*> swapchainElements;
    vector<RenderDeletionTask<RenderVREntity>>* entitiesToDelete;
    vector<RenderEntityComponent*> components;

    map<EntityID, RenderVREntity*> _children;

    void createComponents();
    void destroyComponents();
};
#endif
