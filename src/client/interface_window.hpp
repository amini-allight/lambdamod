/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

enum InterfaceWindowTransform : u8
{
    Window_Transform_None,
    Window_Transform_Move,
    Window_Transform_Scale_Back,
    Window_Transform_Scale_Forward
};

class InterfaceWindow : public InterfaceWidget
{
public:
    InterfaceWindow(InterfaceView* view, const string& name);

    void toggle(const Point& point);
    virtual void open(const Point& point) = 0;
    virtual void close();
    void show();
    void hide();
    bool shown() const;

    bool shouldDestroy() const;

    bool movePointer(PointerType type, const Point& pointer, bool consumed) override;
    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;
    bool shouldDraw() const override;

    void maximize();
    void unmaximize();
    bool maximized() const;

protected:
    void open(const Point& point, const Size& size);

    void setName(const string& name);

    virtual void destroy();
    void cancelDestruction();

    virtual void dismiss(const Input& input) = 0;

private:
    friend class Dialog;

    InterfaceWindowTransform transform() const;

private:
    friend class Popup;

    void incrementPopupCount();
    void decrementPopupCount();

private:
    string name;
    bool _shown;
    bool _destroy;
    InterfaceWindowTransform _transform;
    Point lastPointer;
    u32 popupCount;
    tuple<Point, Size> preMaximizeTransform;

    bool isTargetingHeader() const;
    bool isTargetingMaximize() const;
    bool isTargetingClose() const;
    bool isTargetingBorder() const;

    void translate(const Point& delta);
    void scaleBack(const Point& delta);
    void scaleForward(const Point& delta);

    void toggleMaximized(const Input& input);
    void startResize(const Input& input);
    void startMove(const Input& input);
    void drag(const Input& input);
    void release(const Input& input);

    SizeProperties sizeProperties() const override;
};
