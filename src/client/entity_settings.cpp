/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "entity_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "user.hpp"
#include "fuzzy_find.hpp"
#include "column.hpp"
#include "row.hpp"
#include "spacer.hpp"
#include "label.hpp"
#include "indicator_icon.hpp"
#include "tree_view.hpp"
#include "tree_view_node.hpp"
#include "shift_dialog.hpp"
#include "entity_display_name.hpp"
#include "confirm_dialog.hpp"

EntityDetail::EntityDetail()
    : id(0)
{

}

bool EntityDetail::operator==(const EntityDetail& rhs) const
{
    return id == rhs.id &&
        name == rhs.name &&
        children == rhs.children;
}

bool EntityDetail::operator!=(const EntityDetail& rhs) const
{
    return !(*this == rhs);
}

EntitySettings::EntitySettings(InterfaceWidget* parent)
    : RightClickableWidget(parent)
{
    auto column = new Column(this);

    searchBar = new LineEdit(column, nullptr, [](const string& text) -> void {});
    searchBar->setPlaceholder(localize("entity-settings-type-to-search"));

    treeView = new TreeView(column);
}

void EntitySettings::step()
{
    RightClickableWidget::step();

    vector<EntityDetail> details = convert(context()->controller()->selfWorld()->entities());
    string search = searchBar->content();

    if (details != this->details || search != this->search)
    {
        this->details = details;
        this->search = search;
        updateTreeView();
    }
}

vector<EntityDetail> EntitySettings::convert(const map<EntityID, Entity*>& entities) const
{
    vector<EntityDetail> details;
    details.reserve(entities.size());

    for (const auto& [ id, entity ] : entities)
    {
        if (!context()->controller()->self()->admin() && !entity->shown())
        {
            continue;
        }

        EntityDetail detail;
        detail.id = id;
        detail.name = entity->name();
        detail.children = convert(entity->children());

        details.push_back(detail);
    }

    return details;
}

void EntitySettings::openRightClickMenu(i32 index, const Point& pointer)
{
    EntityID id(index);

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("entity-settings-open"), bind(&EntitySettings::onOpen, this, id, placeholders::_1) },
        { localize("entity-settings-remove"), bind(&EntitySettings::onRemove, this, id, placeholders::_1) },
        { localize("entity-settings-shift"), bind(&EntitySettings::onShift, this, id, placeholders::_1) },
        { localize("entity-settings-cursor-to-entity"), bind(&EntitySettings::onCursorToEntity, this, id, placeholders::_1) },
        { localize("entity-settings-entity-to-cursor"), bind(&EntitySettings::onEntityToCursor, this, id, placeholders::_1) }
    };

    rightClickMenu = new RightClickMenu(
        this,
        bind(&EntitySettings::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void EntitySettings::onOpen(EntityID id, const Point& point)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        closeRightClickMenu();
        return;
    }

    context()->viewerMode()->entityMode()->openEditor(id);

    closeRightClickMenu();
}

void EntitySettings::onRemove(EntityID id, const Point& point)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        closeRightClickMenu();
        return;
    }

    context()->interface()->addWindow<ConfirmDialog>(
        localize("entity-settings-are-you-sure-you-want-to-delete-entity", entityDisplayName(entity)),
        [this, id](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            context()->controller()->edit([this, id]() -> void {
                context()->controller()->selfWorld()->remove(id);
            });
        }
    );

    closeRightClickMenu();
}

void EntitySettings::onShift(EntityID id, const Point& point)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        closeRightClickMenu();
        return;
    }

    context()->interface()->addWindow<ShiftDialog>([this, id](const string& name) -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        context()->controller()->edit([this, name, entity]() -> void {
            Entity* copy = new Entity(*entity);

            context()->controller()->game().getWorld(name)->add(EntityID(), copy);
        });

        context()->controller()->edit([this, entity]() -> void {
            context()->controller()->selfWorld()->remove(entity->id());
        });
    });

    closeRightClickMenu();
}

void EntitySettings::onCursorToEntity(EntityID id, const Point& point)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        closeRightClickMenu();
        return;
    }

    context()->viewerMode()->setCursorPosition(entity->globalPosition());

    closeRightClickMenu();
}

void EntitySettings::onEntityToCursor(EntityID id, const Point& point)
{
    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([this, entity]() -> void {
        entity->setGlobalPosition(context()->viewerMode()->cursorPosition());
    });

    closeRightClickMenu();
}

static void recordExpansionStates(InterfaceWidget* widget, map<string, bool>& expanded)
{
    for (InterfaceWidget* child : widget->children())
    {
        auto node = dynamic_cast<TreeViewNode*>(child);

        expanded.insert_or_assign(node->name(), node->expanded());

        recordExpansionStates(node, expanded);
    }
}

void EntitySettings::updateTreeView()
{
    map<string, bool> expanded;

    recordExpansionStates(treeView, expanded);

    treeView->clearChildren();

    vector<EntityDetail> details = this->details;

    createNodes(treeView, details, expanded);

    treeView->update();
}

void EntitySettings::createNodes(InterfaceWidget* parent, const vector<EntityDetail>& details, const map<string, bool>& expanded)
{
    auto wasExpanded = [expanded](const string& name) -> bool
    {
        auto it = expanded.find(name);

        return it != expanded.end() && it->second;
    };

    for (const EntityDetail& detail : details)
    {
        string name = entityDisplayName(detail.id, detail.name);

        auto node = new TreeViewNode(
            parent,
            treeView,
            name,
            wasExpanded(name),
            [this]() -> bool { return !search.empty(); },
            [this, detail]() -> bool
            {
                return search.empty() || detail.name.find(search) != string::npos;
            },
            bind(&EntitySettings::onRightClick, this, detail.id.value(), placeholders::_1)
        );

        createNodes(node, detail.children, expanded);
    }
}

SizeProperties EntitySettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
