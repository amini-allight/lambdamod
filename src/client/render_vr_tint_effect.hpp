/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "render_vr_post_process_effect.hpp"

class RenderVRTintEffect final : public RenderVRPostProcessEffect
{
public:
    RenderVRTintEffect(
        const RenderContext* ctx,
        RenderVRPipelineStore* pipelineStore,
        vector<VRSwapchainElement*> swapchainElements,
        const vec3& color,
        f64 strength
    );
    ~RenderVRTintEffect();

    void update(const vec3& color, f64 strength);

private:
    fvec4 color;
    f32 strength;

    VmaBuffer createUniformBuffer() const override;
    void fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const override;
};
#endif
