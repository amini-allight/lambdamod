/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "wait_manager.hpp"
#include "control_context.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "controller.hpp"
#include "vr_render.hpp"
#include "flat_render.hpp"

#include "render_vr_waiting_indicator.hpp"
#include "render_flat_waiting_indicator.hpp"

static constexpr i64 waitingIndicatorSegments = 12;

WaitManager::WaitManager(ControlContext* ctx)
    : ctx(ctx)
{
    waiting = false;
    lastAngle = 0;

#ifdef LMOD_VR
    if (g_vr)
    {
        waitingIndicatorID = dynamic_cast<VRRender*>(ctx->controller()->render())->addDecoration<RenderVRWaitingIndicator>(
            0
        );
    }
    else
    {
        waitingIndicatorID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<RenderFlatWaitingIndicator>(
            dynamic_cast<FlatRender*>(ctx->controller()->render())->width(),
            dynamic_cast<FlatRender*>(ctx->controller()->render())->height(),
            0
        );
    }
#else
    waitingIndicatorID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<RenderFlatWaitingIndicator>(
        dynamic_cast<FlatRender*>(ctx->controller()->render())->width(),
        dynamic_cast<FlatRender*>(ctx->controller()->render())->height(),
        0
    );
#endif

    ctx->controller()->render()->setDecorationShown(waitingIndicatorID, false);
}

void WaitManager::step()
{
    if (!waiting)
    {
        return;
    }

    f64 angle = (static_cast<i64>(currentTime().count() / (1000 / waitingIndicatorSegments)) % waitingIndicatorSegments) * (tau / waitingIndicatorSegments);

    if (angle == lastAngle)
    {
        return;
    }

#ifdef LMOD_VR
    if (g_vr)
    {
        dynamic_cast<VRRender*>(ctx->controller()->render())->updateDecoration<RenderVRWaitingIndicator>(
            waitingIndicatorID,
            angle
        );
    }
    else
    {
        dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<RenderFlatWaitingIndicator>(
            waitingIndicatorID,
            dynamic_cast<FlatRender*>(ctx->controller()->render())->width(),
            dynamic_cast<FlatRender*>(ctx->controller()->render())->height(),
            angle
        );
    }
#else
    dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<RenderFlatWaitingIndicator>(
        waitingIndicatorID,
        dynamic_cast<FlatRender*>(ctx->controller()->render())->width(),
        dynamic_cast<FlatRender*>(ctx->controller()->render())->height(),
        angle
    );
#endif

    lastAngle = angle;
}

void WaitManager::start()
{
    waiting = true;

    ctx->controller()->render()->setDecorationShown(waitingIndicatorID, true);
}

void WaitManager::end()
{
    waiting = false;

    ctx->controller()->render()->setDecorationShown(waitingIndicatorID, false);
}
