/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_part_selection.hpp"
#include "internal_mode_context.hpp"

class ActionModeContext final : public InternalModeContext
{
public:
    ActionModeContext(ViewerModeContext* ctx);

    void addNewPrefab(const string& name) override;
    void addNew() override;
    void addNew(BodyPartType type);
    void removeSelected() override;

    void addChildren(const Point& point) override;
    void removeParent() override;

    void hideSelected() override;
    void hideUnselected() override;
    void unhideAll() override;

    void addKeyframes();
    void removeKeyframes();

    void resetPosition() override;
    void resetRotation() override;
    void resetScale() override;
    void resetLinearVelocity() override;
    void resetAngularVelocity() override;

    void selectionToCursor() override;

    void duplicate() override;
    void cut() override;
    void copy() override;
    void paste() override;

    void extrude() override;
    void split() override;
    void join(const Point& point) override;

private:
    void transformSet(size_t i, const vec3& pivot, const mat4& initial, const mat4& transform) override;
    mat4 transformGet(size_t i) const override;
    mat4 transformGetWithPromotion(size_t i) const override;
};
