/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "message.hpp"
#include "interface_window.hpp"
#include "console_output.hpp"
#include "console_input.hpp"

class Console : public InterfaceWindow
{
public:
    Console(InterfaceView* view);

    void log(const Message& message);
    void recordHistory(const string& history);

    void open(const Point& point) override;

private:
    ConsoleOutput* output;
    ConsoleInput* _input;

    void onChat(const string& message, const optional<ChatDestination>& destination);
    void onCommand(const string& command);

    void dismiss(const Input& input) override;
};
