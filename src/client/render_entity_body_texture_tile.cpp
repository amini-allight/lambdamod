/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_entity_body_texture_tile.hpp"
#include "render_constants.hpp"
#include "render_tools.hpp"

RenderEntityBodyTextureTile::RenderEntityBodyTextureTile(
    const RenderContext* ctx,
    const BodyRenderMaterialTextureData& texture,
    const uvec2& offset,
    const uvec2& extent
)
    : ctx(ctx)
    , offset(offset)
    , extent(extent)
{
    image = createImage(
        ctx->allocator,
        { extent.x, extent.y, 1 },
        maskTextureFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY,
        false
    );

    imageView = createImageView(
        ctx->device,
        image.image,
        maskTextureFormat,
        VK_IMAGE_ASPECT_COLOR_BIT
    );

    stagingBuffer = createBuffer(
        ctx->allocator,
        extent.x * extent.y * sizeof(u8),
        VK_BUFFER_USAGE_TRANSFER_SRC_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    stagingBufferMapping = new VmaMapping<u8>(ctx->allocator, stagingBuffer);

    fill(texture);
}

RenderEntityBodyTextureTile::~RenderEntityBodyTextureTile()
{
    delete stagingBufferMapping;

    destroyBuffer(ctx->allocator, stagingBuffer);

    vkDestroyImageView(ctx->device, imageView, nullptr);

    vmaDestroyImage(ctx->allocator, image.image, image.allocation);
}

void RenderEntityBodyTextureTile::fill(const BodyRenderMaterialTextureData& texture)
{
    const u32* src = reinterpret_cast<const u32*>(texture.data.data());
    u8* dst = stagingBufferMapping->data;

    for (u32 y = 0; y < extent.y; y++)
    {
        for (u32 x = 0; x < extent.x; x++)
        {
            u32 dstX = x;
            u32 dstY = y;

            u32 srcX = offset.x + dstX;
            u32 srcY = offset.y + dstY;

            dst[dstY * extent.x + dstX] = src[srcY * texture.width + srcX] >> 24;
        }
    }
}

void RenderEntityBodyTextureTile::setupLayout(VkCommandBuffer commandBuffer)
{
    transitionLayout(
        commandBuffer,
        image.image,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    VkBufferImageCopy region{};
    region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.imageSubresource.mipLevel = 0;
    region.imageSubresource.baseArrayLayer = 0;
    region.imageSubresource.layerCount = 1;
    region.imageOffset = { 0, 0, 0 };
    region.imageExtent = { extent.x, extent.y, 1 };

    vkCmdCopyBufferToImage(
        commandBuffer,
        stagingBuffer.buffer,
        image.image,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region
    );

    transitionLayout(
        commandBuffer,
        image.image,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );
}
