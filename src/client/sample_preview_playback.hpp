/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

class Entity;
class ViewerModeContext;

class SamplePreviewPlayback
{
public:
    SamplePreviewPlayback(ViewerModeContext* ctx, EntityID id);
    SamplePreviewPlayback(const SamplePreviewPlayback& rhs) = delete;
    SamplePreviewPlayback(SamplePreviewPlayback&& rhs) = delete;
    ~SamplePreviewPlayback();

    SamplePreviewPlayback& operator=(const SamplePreviewPlayback& rhs) = delete;
    SamplePreviewPlayback& operator=(SamplePreviewPlayback&& rhs) = delete;

    bool playing() const;
    u32 position() const;

    void open(const string& name);
    void close();
    void play();
    void pause();
    void seek(u32 position);

private:
    ViewerModeContext* ctx;
    EntityID id;
    string name;
    SamplePlaybackID playbackID;

    Entity* activeEntity() const;
};
