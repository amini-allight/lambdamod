/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "virtual_touch_trigger.hpp"
#include "interface_constants.hpp"
#include "interface_scale.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "flat_render.hpp"

VirtualTouchTrigger::VirtualTouchTrigger(
    InterfaceWidget* parent,
    const string& name,
    InputType output
)
    : InterfaceWidget(parent)
    , name(name)
    , output(output)
    , touched(false)
{
    START_WIDGET_SCOPE("virtual-touch-trigger")
        WIDGET_SLOT("touch", touch)
    END_SCOPE
}

void VirtualTouchTrigger::draw(const DrawContext& ctx) const
{
    drawRoundedBorder(
        ctx,
        this,
        UIScale::marginSize(),
        UIScale::virtualTouchControlRoundedCornerRadius(),
        touched ? theme.touchInterfaceActiveColor : theme.touchInterfaceColor
    );

    TextStyle style;
    style.alignment = Text_Center;
    style.size = UIScale::largeFontSize();
    style.weight = Text_Bold;
    style.font = Text_Sans_Serif;
    style.color = touched ? theme.touchInterfaceActiveColor : theme.touchInterfaceColor;

    drawText(
        ctx,
        this,
        name,
        style
    );
}

void VirtualTouchTrigger::touch(const Input& input)
{
    if (input.up && touched)
    {
        touched = false;

        Input virtualInput;
        virtualInput.type = output;
        virtualInput.intensity = 0;
        context()->input()->handle(virtualInput);
    }
    else if (!input.up)
    {
        touched = true;

        Input virtualInput;
        virtualInput.type = output;
        virtualInput.intensity = input.touch.force != 0 ? input.touch.force : 1;
        context()->input()->handle(virtualInput);
    }
}

SizeProperties VirtualTouchTrigger::sizeProperties() const
{
    return {
        virtualTouchTriggerSize.x * parent()->size().h,
        virtualTouchTriggerSize.y * parent()->size().h,
        Scaling_Fixed,
        Scaling_Fixed
    };
}
