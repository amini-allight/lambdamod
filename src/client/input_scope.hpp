/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "input_types.hpp"
#include "input_scope_binding.hpp"
#include "input_scope_slot.hpp"
#include "input_handling_result.hpp"

class ControlContext;

enum InputScopeMode : u8
{
    Input_Scope_Normal,
    Input_Scope_Independent,
    Input_Scope_Blocking
};

class InputScope
{
public:
    InputScope(
        ControlContext* ctx,
        InputScope* parent,
        const string& name,
        InputScopeMode mode,
        const function<bool(const Input&)>& isActive,
        const function<void(InputScope*)>& block
    );
    InputScope(const InputScope& rhs) = delete;
    InputScope(InputScope&& rhs) = delete;
    virtual ~InputScope();

    InputScope& operator=(const InputScope& rhs) = delete;
    InputScope& operator=(InputScope&& rhs) = delete;

    InputScope* parent() const;

    vector<string> path() const;
    void showActivePath(size_t indent = 0) const;

    virtual bool movePointer(PointerType type, const Point& point, bool consumed) const;
    virtual bool moveLeftHand(const vec3& position, const quaternion& rotation, bool consumed) const;
    virtual bool moveRightHand(const vec3& position, const quaternion& rotation, bool consumed) const;
    virtual InputHandlingResult handle(const Input& input, u32 repeats, size_t depth = 0) const;

    void bind(const string& bindingName, const function<void(const Input&)>& behavior);
    void replayableBind(const string& bindingName, const function<void(const Input&)>& behavior);
    void inclusionBind(
        const string& bindingName,
        const function<void(const Input&)>& behavior,
        InputScopeSlotInclusionMode inclusionMode
    );

    InputScope* get(const string& name);

protected:
    ControlContext* ctx;
    InputScope* _parent;
    string name;
    InputScopeMode mode;
    function<bool(const Input&)> isActive;
    map<string, InputScopeSlot> bindings;
    vector<InputScope*> children;

    void add(InputScope* child);
    void remove(InputScope* child);

    bool shouldHandleInput(const Input& input) const;
    bool shouldInvokeBindings(const Input& input) const;
    bool independentlyActive(const Input& input) const;
    bool dependentlyActive(const Input& input) const;
    bool hasActiveChild(const Input& input) const;

    map<InputScopeBinding, InputScopeSlot> allBindings() const;
};

#define START_GLOBAL_SCOPE \
globalScope = createScope(nullptr, "global", Input_Scope_Normal, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {

#define START_TOPLEVEL_WIDGET_SCOPE(_parent, _name) \
setScope<WidgetInputScope>(this, _parent, _name, Input_Scope_Normal, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {
#define START_TOPLEVEL_INDEPENDENT_WIDGET_SCOPE(_parent, _name) \
setScope<WidgetInputScope>(this, _parent, _name, Input_Scope_Independent, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {
#define START_TOPLEVEL_BLOCKING_WIDGET_SCOPE(_parent, _name) \
setScope<WidgetInputScope>(this, _parent, _name, Input_Scope_Blocking, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {

#define START_FILTERED_TOPLEVEL_WIDGET_SCOPE(_parent, _name, _expr) \
setScope<WidgetInputScope>(this, _parent, _name, Input_Scope_Normal, [](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {
#define START_FILTERED_TOPLEVEL_INDEPENDENT_WIDGET_SCOPE(_parent, _name, _expr) \
setScope<WidgetInputScope>(this, _parent, _name, Input_Scope_Independent, [](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {
#define START_FILTERED_TOPLEVEL_BLOCKING_WIDGET_SCOPE(_parent, _name, _expr) \
setScope<WidgetInputScope>(this, _parent, _name, Input_Scope_Blocking, [](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {

#define START_WIDGET_SCOPE(_name) \
setScope<WidgetInputScope>(this, _name, Input_Scope_Normal, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {
#define START_WIDGET_INDEPENDENT_SCOPE(_name) \
setScope<WidgetInputScope>(this, _name, Input_Scope_Independent, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {
#define START_WIDGET_BLOCKING_SCOPE(_name) \
setScope<WidgetInputScope>(this, _name, Input_Scope_Blocking, [](const Input& input) -> bool { return true; }, [&](InputScope* parent) -> void {

#define START_SCOPE(_name, _expr) \
createScope(parent, _name, Input_Scope_Normal, [&](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {
#define START_INDEPENDENT_SCOPE(_name, _expr) \
createScope(parent, _name, Input_Scope_Independent, [&](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {
#define START_BLOCKING_SCOPE(_name, _expr) \
createScope(parent, _name, Input_Scope_Blocking, [&](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {

#define START_WINDOW_MANAGER_SCOPE(_name, _expr) \
createScope<WindowManagerInputScope>(parent, _name, Input_Scope_Normal, [&](const Input& input) -> bool { return _expr; }, [&](InputScope* parent) -> void {

#define END_SCOPE \
});

#define WIDGET_SLOT(_binding_name, _function_name) \
parent->bind(_binding_name, bind(static_cast<void (std::decay_t<decltype(*this)>::*)(const Input&)>(&std::decay_t<decltype(*this)>::_function_name), this, placeholders::_1));
#define WIDGET_INCLUSION_SLOT(_binding_name, _function_name, _inclusion_mode) \
parent->inclusionBind(_binding_name, bind(static_cast<void (std::decay_t<decltype(*this)>::*)(const Input&)>(&std::decay_t<decltype(*this)>::_function_name), this, placeholders::_1), _inclusion_mode);
