/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "basic_text_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "clipboard.hpp"
#include "control_context.hpp"

static constexpr i32 maxUndoHistory = 1024;

TextEditorState::TextEditorState()
{
    lines = { "" };
    selected = false;
}

void TextEditorState::generateLines()
{
    lines.clear();

    string line;

    for (i32 i = 0; i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (c == '\n')
        {
            lines.push_back(line);
            line = "";
        }
        else
        {
            line += c;
        }
    }

    lines.push_back(line);
}

BasicTextEditor::BasicTextEditor(
    InterfaceWidget* parent,
    const function<string()>& source,
    const function<void(const string&)>& onSave,
    bool codeMode
)
    : InterfaceWidget(parent)
    , source(source)
    , onSave(onSave)
    , codeMode(codeMode)
    , _saved(true)
    , selecting(false)
    , lastClickTime(0)
{
    START_WIDGET_SCOPE("basic-text-editor")
        WIDGET_SLOT("text-select-all", selectAll)
        WIDGET_SLOT("cursor-to-previous-word", cursorToPreviousWord)
        WIDGET_SLOT("cursor-to-next-word", cursorToNextWord)
        WIDGET_SLOT("select-up", selectUp)
        WIDGET_SLOT("select-down", selectDown)
        WIDGET_SLOT("select-left", selectLeft)
        WIDGET_SLOT("select-right", selectRight)
        WIDGET_SLOT("select-to-previous-word", selectToPreviousWord)
        WIDGET_SLOT("select-to-next-word", selectToNextWord)
        WIDGET_SLOT("cursor-up", cursorUp)
        WIDGET_SLOT("cursor-down", cursorDown)
        WIDGET_SLOT("cursor-left", cursorLeft)
        WIDGET_SLOT("cursor-right", cursorRight)
        WIDGET_SLOT("go-to-start", goToLineStart)
        WIDGET_SLOT("go-to-end", goToLineEnd)
        WIDGET_SLOT("remove-previous-word", removePreviousWord)
        WIDGET_SLOT("remove-next-word", removeNextWord)
        WIDGET_SLOT("remove-to-start", removeToLineStart)
        WIDGET_SLOT("remove-to-end", removeToLineEnd)
        WIDGET_SLOT("remove-previous-character", removePreviousCharacter)
        WIDGET_SLOT("remove-next-character", removeNextCharacter)
        WIDGET_SLOT("upcase-next-word", upcaseNextWord)
        WIDGET_SLOT("downcase-next-word", downcaseNextWord)
        WIDGET_SLOT("swap-words", swapWords)
        WIDGET_SLOT("swap-characters", swapCharacters)
        WIDGET_SLOT("select-to", selectTo)
        WIDGET_SLOT("cursor-to", cursorTo)
        WIDGET_SLOT("document-go-to-start", goToStart)
        WIDGET_SLOT("document-go-to-end", goToEnd)
        WIDGET_SLOT("document-scroll-up", scrollUp)
        WIDGET_SLOT("document-scroll-down", scrollDown)
        WIDGET_SLOT("document-pan-left", panLeft)
        WIDGET_SLOT("document-pan-right", panRight)
        WIDGET_SLOT("page-up", pageUp)
        WIDGET_SLOT("page-down", pageDown)
        WIDGET_SLOT("add-text", addText)
        WIDGET_SLOT("cut", cut)
        WIDGET_SLOT("copy", copy)
        WIDGET_SLOT("paste", paste)
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
        WIDGET_SLOT("save", save)
        WIDGET_SLOT("reload", reload)
#if (defined(__linux__) || defined(__FreeBSD__))
        WIDGET_SLOT("paste-primary", pastePrimary)
#endif

        START_INDEPENDENT_SCOPE("selecting", selecting)
            WIDGET_SLOT("end-replace-select", endSelect)
            WIDGET_SLOT("move-select", moveSelect)
        END_SCOPE
    END_SCOPE

    states.push_back(TextEditorState());
}

bool BasicTextEditor::saved() const
{
    return _saved;
}

void BasicTextEditor::set(const string& text)
{
    setState(
        [&](TextEditorState& state) -> void
        {
            state.text = text;
            state.cursor = ivec2();
            state.select = ivec2();
            state.view = ivec2();
            state.selected = false;
        }
    );
    _saved = true;
}

void BasicTextEditor::notifyReload()
{
    vector<InputScopeBinding> bindings = context()->input()->getBindings("reload");

    string bindingsSummary;

    for (size_t i = 0; i < bindings.size(); i++)
    {
        bindingsSummary += bindings.at(i).toString();

        if (i + 1 != bindings.size())
        {
            bindingsSummary += ", ";
        }
    }

    statusMessage = localize("basic-text-editor-source-has-changed", bindingsSummary);
}

void BasicTextEditor::display(const string& message)
{
    statusMessage = message;
}

void BasicTextEditor::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.backgroundColor
    );

    drawLineNumbers(ctx);

    drawStatusLine(ctx);

    drawActiveLine(ctx);

    drawContent(ctx);

    drawCursor(ctx);
}

void BasicTextEditor::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void BasicTextEditor::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void BasicTextEditor::drawLineNumbers(const DrawContext& ctx) const
{
    for (i32 i = 0; i < visualLines(); i++)
    {
        i32 n = viewLine() + i + 1;

        if (n >= lines() + 1)
        {
            break;
        }

        stringstream ss;

        ss << setw(4) << setfill(' ') << to_string(n);

        TextStyle style;

        if (viewLine() + i == cursorLine())
        {
            style.color = theme.accentColor;
        }

        drawText(
            ctx,
            this,
            Point(0, i * charHeight()),
            Size(UIScale::lineNumberColumnWidth(), charHeight()),
            ss.str(),
            style
        );
    }
}

void BasicTextEditor::drawStatusLine(const DrawContext& ctx) const
{
    auto join = [](const vector<string>& parts) -> string
    {
        string s;

        for (i32 i = 0; i < static_cast<i32>(parts.size()); i++)
        {
            const string& part = parts[i];

            s += part;

            if (i + 1 != static_cast<i32>(parts.size()) && !part.empty())
            {
                s += " ";
            }
        }

        return s;
    };

    drawText(
        ctx,
        this,
        Point(0, visualLines() * charHeight()),
        Size(size().w, charHeight()),
        join({
            statusMessage,
            (_saved ? "" : "(" + localize("basic-text-editor-unsaved") + ")"),
            to_string(cursorLine() + 1) + ":" + to_string(cursorApparentColumn() + 1)
        }),
        TextStyle()
    );
}

void BasicTextEditor::drawActiveLine(const DrawContext& ctx) const
{
    i32 textAreaX = UIScale::lineNumberColumnWidth() + UIScale::marginSize();
    i32 textAreaW = size().w - (UIScale::lineNumberColumnWidth() + (UIScale::marginSize() * 2));

    if (
        cursorLine() >= viewLine() &&
        cursorLine() < viewLine() + visualLines()
    )
    {
        drawSolid(
            ctx,
            this,
            Point(textAreaX, (cursorLine() - viewLine()) * charHeight()),
            Size(textAreaW, charHeight()),
            theme.altBackgroundColor
        );
    }
}

void BasicTextEditor::drawContent(const DrawContext& ctx) const
{
    i32 textAreaX = UIScale::lineNumberColumnWidth() + UIScale::marginSize();

    for (i32 y = 0; y < visualLines(); y++)
    {
        i32 i = viewLine() + y;

        if (i >= lines())
        {
            continue;
        }

        i32 offset = viewColumn();

        const string& line = state().lines[i];

        // Selection range
        if (state().selected && selectionStart() != selectionEnd())
        {
            i32 start = selectionStart();
            i32 end = selectionEnd();

            drawRangeOnLine(ctx, y, start, end, theme.accentColor);
        }

        if (static_cast<i32>(line.size()) <= offset)
        {
            continue;
        }

        if (codeMode)
        {
            const vector<SyntaxHighlightChunk>& chunks = syntaxHighlight.at(i);

            for (const SyntaxHighlightChunk& chunk : chunks)
            {
                i32 start = chunk.start;
                i32 end = chunk.start + chunk.size;

                if (start < viewColumn() && end < viewColumn())
                {
                    continue;
                }

                if (start >= viewColumn() + visualColumns() && end >= viewColumn() + visualColumns())
                {
                    continue;
                }

                i32 startX = clamp(start - viewColumn(), 0, visualColumns());
                i32 endX = clamp(end - viewColumn(), 0, visualColumns());

                i32 textOffset = start + max(viewColumn() - start, 0);
                i32 textRemaining = min(end - viewColumn(), end - start);

                TextStyle style;
                style.color = syntaxHighlightColor(chunk.type);
                style.alignment = Text_Center;

                drawText(
                    ctx,
                    this,
                    Point(textAreaX + (startX * charWidth()), y * charHeight()),
                    Size((endX - startX) * charWidth(), charHeight()),
                    lineAt(i).substr(textOffset, textRemaining),
                    style
                );
            }

            auto [ start, size ] = bracketMatchHighlight(
                state().text,
                cursorIndex(),
                0,
                static_cast<i32>(state().text.size())
            );

            i32 end = start + size;

            drawRangeOnLine(ctx, y, start, end, theme.bracketHighlightColor);
        }
        else
        {
            drawText(
                ctx,
                this,
                Point(UIScale::lineNumberColumnWidth(), y * charHeight()),
                Size(size().w - UIScale::lineNumberColumnWidth(), charHeight()),
                line.substr(viewColumn(), static_cast<i32>(line.size()) - viewColumn()),
                TextStyle()
            );
        }
    }
}

void BasicTextEditor::drawCursor(const DrawContext& ctx) const
{
    if (
        cursorLine() >= viewLine() &&
        cursorLine() < viewLine() + visualLines() &&
        cursorApparentColumn() >= viewColumn() &&
        cursorApparentColumn() < viewColumn() + visualColumns()
    )
    {
        drawSolid(
            ctx,
            this,
            Point(
                UIScale::lineNumberColumnWidth() + UIScale::marginSize() + ((cursorApparentColumn() - viewColumn()) * charWidth()),
                (cursorLine() - viewLine()) * charHeight()
            ),
            Size(UIScale::textCursorWidth(), charHeight()),
            theme.cursorColor
        );
    }
}

void BasicTextEditor::drawRangeOnLine(const DrawContext& ctx, i32 y, i32 start, i32 end, const Color& color) const
{
    i32 i = viewLine() + y;

    i32 textAreaX = UIScale::lineNumberColumnWidth() + UIScale::marginSize();
    i32 textAreaW = size().w - (UIScale::lineNumberColumnWidth() + (UIScale::marginSize() * 2));

    // Within
    if (start >= lineStart(i) && end <= lineEnd(i))
    {
        i32 startX = max(start - (lineStart(i) + viewColumn()), 0) * charWidth();
        i32 endX = clamp(end - (lineStart(i) + viewColumn()), 0, visualColumns()) * charWidth();

        drawSolid(
            ctx,
            this,
            Point(textAreaX + startX, y * charHeight()),
            Size(endX - startX, charHeight()),
            color
        );
    }
    // Containing
    else if (start < lineStart(i) && end > lineEnd(i))
    {
        drawSolid(
            ctx,
            this,
            Point(textAreaX, y * charHeight()),
            Size(textAreaW, charHeight()),
            color
        );
    }
    // Starts
    else if (start >= lineStart(i) && (start < lineEnd(i) || (start == lineEnd(i) && lineStart(i) == lineEnd(i))) && end >= lineEnd(i))
    {
        i32 x = max(start - (lineStart(i) + viewColumn()), 0) * charWidth();

        drawSolid(
            ctx,
            this,
            Point(textAreaX + x, y * charHeight()),
            Size(textAreaW - x, charHeight()),
            color
        );
    }
    // Ends
    else if (start < lineStart(i) && end >= lineStart(i) && end <= lineEnd(i))
    {
        i32 x = clamp(end - (lineStart(i) + viewColumn()), 0, visualColumns()) * charWidth();

        drawSolid(
            ctx,
            this,
            Point(textAreaX, y * charHeight()),
            Size(x, charHeight()),
            color
        );
    }
}

void BasicTextEditor::cursorUp()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;
            state.cursor.y--;
        }
    );
}

void BasicTextEditor::cursorDown()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;
            state.cursor.y++;
        }
    );
}

void BasicTextEditor::cursorLeft()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;

            if (cursorApparentColumn() == 0 && cursorLine() != 0)
            {
                state.cursor.x = static_cast<i32>(lineAt(cursorLine() - 1).size());
                state.cursor.y--;
            }
            else if (cursorApparentColumn() != cursorColumn())
            {
                state.cursor.x = cursorApparentColumn() - 1;
            }
            else
            {
                state.cursor.x--;
            }
        }
    );
}

void BasicTextEditor::cursorRight()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;

            if (cursorApparentColumn() >= static_cast<i32>(lineAtCursor().size()) && cursorLine() != lines() - 1)
            {
                state.cursor.x = 0;
                state.cursor.y++;
            }
            else
            {
                state.cursor.x++;
            }
        }
    );
}

void BasicTextEditor::selectUp()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.cursor.y--;
        }
    );
}

void BasicTextEditor::selectDown()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.cursor.y++;
        }
    );
}

void BasicTextEditor::selectLeft()
{

}

void BasicTextEditor::selectRight()
{

}

void BasicTextEditor::goTo(i32 index)
{
    ivec2 position = indexToPosition(index);

    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;
            state.cursor = position;
        }
    );
}

void BasicTextEditor::selectTo(i32 index)
{
    ivec2 position = indexToPosition(index);

    updateState(
        [&](TextEditorState& state) -> void
        {
            if (!state.selected)
            {
                state.select = state.cursor;
            }

            state.selected = true;
            state.cursor = position;
        }
    );
}

void BasicTextEditor::scrollUp(i32 lines)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.view.y -= lines;
        }
    );
}

void BasicTextEditor::scrollDown(i32 lines)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.view.y += lines;
        }
    );
}

void BasicTextEditor::scrollLeft()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.view.x--;
        }
    );
}

void BasicTextEditor::scrollRight()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.view.x++;
        }
    );
}

void BasicTextEditor::cutSelection(const Input& input)
{
    if (!state().selected)
    {
        return;
    }

    cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
}

void BasicTextEditor::copySelection(const Input& input)
{
    if (!state().selected)
    {
        return;
    }

    copyRegion(input, selectionStart(), selectionEnd() - selectionStart());
}

void BasicTextEditor::pasteClipboard()
{
    add(cursorIndex(), getClipboard());
}

#if (defined(__linux__) || defined(__FreeBSD__))
void BasicTextEditor::pastePrimaryClipboard(const Point& pointer)
{
    goTo(screenToIndex(pointer));
    add(cursorIndex(), getClipboard(true));
}
#endif

void BasicTextEditor::add(i32 index, const string& s)
{
    bool after = index > cursorIndex();

    if (state().selected)
    {
        bool after = index >= cursorIndex();
        i32 beforeIndex = cursorIndex();

        remove(selectionStart(), selectionEnd() - selectionStart());

        i32 afterIndex = cursorIndex();
        if (after)
        {
            index += afterIndex - beforeIndex;
        }
    }

    string text = state().text;

    text = text.substr(0, index) + s + text.substr(index, static_cast<i32>(text.size()) - index);

    setState(
        [&](TextEditorState& state) -> void
        {
            if (!after)
            {
                i32 x = cursorApparentColumn();
                i32 y = cursorLine();

                for (i32 i = 0; i < static_cast<i32>(s.size()); i++)
                {
                    if (s[i] == '\n')
                    {
                        x = 0;
                        y++;
                    }
                    else
                    {
                        x++;
                    }
                }

                state.cursor = { x, y };
            }

            state.selected = false;
            state.text = text;
        }
    );
}

void BasicTextEditor::add(i32 index, char c)
{
    add(index, string() + c);
}

void BasicTextEditor::remove(i32 start, i32 count)
{
    bool after = start >= cursorIndex();

    // Remove selection
    if (state().selected)
    {
        start = selectionStart();
        count = selectionEnd() - selectionStart();
        after = cursorIndex() < positionToIndex(state().select);
    }

    string text = state().text;

    if (start < 0 || start + count > static_cast<i32>(text.size()))
    {
        return;
    }

    string s = text.substr(start, count);
    text = text.substr(0, start) + text.substr(start + count, static_cast<i32>(text.size()) - (start + count));

    setState(
        [&](TextEditorState& state) -> void
        {
            if (!after)
            {
                i32 x = cursorApparentColumn();
                i32 y = cursorLine();

                for (i32 i = 0; i < static_cast<i32>(s.size()); i++)
                {
                    if (s[i] == '\n')
                    {
                        x = static_cast<i32>(lineAt(y - 1).size());
                        y--;
                    }
                    else
                    {
                        x--;
                    }
                }

                state.cursor = { x, y };
            }

            state.selected = false;
            state.text = text;
        }
    );
}

void BasicTextEditor::replace(i32 index, char c)
{
    if (index < 0 || index >= static_cast<i32>(state().text.size()))
    {
        return;
    }

    setState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;
            state.text[index] = c;
        }
    );
}

void BasicTextEditor::replace(i32 index, const string& s)
{
    for (i32 i = 0; i < static_cast<i32>(s.size()); i++)
    {
        replace(i, s[i]);
    }
}

void BasicTextEditor::copyRegion(const Input& input, i32 start, i32 size)
{
    if (start < 0 || start + size > static_cast<i32>(state().text.size()))
    {
        return;
    }

    setClipboard(input, state().text.substr(start, size));
}

void BasicTextEditor::cutRegion(const Input& input, i32 start, i32 size)
{
    if (start < 0 || start + size > static_cast<i32>(state().text.size()))
    {
        return;
    }

    setClipboard(input, state().text.substr(start, size));
    remove(start, size);
}

void BasicTextEditor::undo()
{
    if (static_cast<i32>(states.size()) == 1)
    {
        return;
    }

    _saved = false;
    statusMessage = "";
    redoStates.push_back(states.back());
    states.pop_back();

    updateSyntaxHighlight(states.back());
}

void BasicTextEditor::redo()
{
    if (redoStates.empty())
    {
        return;
    }

    _saved = false;
    statusMessage = "";
    states.push_back(redoStates.back());
    redoStates.pop_back();

    updateSyntaxHighlight(states.back());
}

void BasicTextEditor::selectTo(const Point& pointer)
{
    updateState(
        [](TextEditorState& state) -> void
        {
            state.selected = true;
            state.select = state.cursor;
        }
    );

    goTo(screenToIndex(pointer));
}

void BasicTextEditor::startSelect(const Point& pointer)
{
    goTo(screenToIndex(pointer));

    updateState(
        [](TextEditorState& state) -> void
        {
            state.selected = true;
            state.select = state.cursor;
        }
    );

    selecting = true;
}

void BasicTextEditor::endSelect(const Input& input, const Point& pointer)
{
    if (!selecting)
    {
        return;
    }

    selecting = false;

    goTo(screenToIndex(pointer));

    updateState(
        [](TextEditorState& state) -> void
        {
            state.selected = state.cursor != state.select;
        }
    );

#if (defined(__linux__) || defined(__FreeBSD__))
    setClipboard(input, state().text.substr(selectionStart(), selectionEnd() - selectionStart()), true);
#endif
}

void BasicTextEditor::moveSelect(const Point& pointer)
{
    if (!selecting)
    {
        return;
    }

    goTo(screenToIndex(pointer));

    updateState(
        [](TextEditorState& state) -> void
        {
            state.selected = true;
        }
    );
}

i32 BasicTextEditor::lines() const
{
    return static_cast<i32>(state().lines.size());
}

i32 BasicTextEditor::columns() const
{
    i32 columns = 0;

    for (const string& line : state().lines)
    {
        if (static_cast<i32>(line.size()) > columns)
        {
            columns = static_cast<i32>(line.size());
        }
    }

    return columns;
}

i32 BasicTextEditor::visualLines() const
{
    return (size().h / charHeight()) - 1;
}

i32 BasicTextEditor::visualColumns() const
{
    return (size().w - UIScale::lineNumberColumnWidth()) / charWidth();
}

const string& BasicTextEditor::lineAt(i32 index) const
{
    return state().lines[index < 0 ? cursorLine() : index];
}

char BasicTextEditor::characterAt(i32 index) const
{
    if (index < 0)
    {
        index = cursorIndex();
    }

    if (index == static_cast<i32>(state().text.size()))
    {
        return '\0';
    }
    else
    {
        return state().text[index];
    }
}

const string& BasicTextEditor::lineAtCursor() const
{
    return lineAt();
}

string BasicTextEditor::wordAtCursor() const
{
    i32 start = wordStart();
    i32 end = wordEnd();

    return lineAtCursor().substr(start - lineStart(), end - start);
}

char BasicTextEditor::characterAtCursor() const
{
    return characterAt();
}

string BasicTextEditor::selectedText() const
{
    i32 start = selectionStart();
    i32 end = selectionEnd();

    return state().text.substr(start, end - start);
}

i32 BasicTextEditor::charWidth() const
{
    return TextStyle().getFont()->monoWidth();
}

i32 BasicTextEditor::charHeight() const
{
    return TextStyle().getFont()->height();
}

i32 BasicTextEditor::cursorIndex() const
{
    return lineStart() + cursorApparentColumn();
}

i32 BasicTextEditor::cursorLine() const
{
    return state().cursor.y;
}

i32 BasicTextEditor::cursorColumn() const
{
    return state().cursor.x;
}

i32 BasicTextEditor::cursorApparentColumn() const
{
    i32 column = cursorColumn();

    if (column > static_cast<i32>(lineAtCursor().size()))
    {
        return static_cast<i32>(lineAtCursor().size());
    }
    else
    {
        return column;
    }
}

i32 BasicTextEditor::viewLine() const
{
    return state().view.y;
}

i32 BasicTextEditor::viewColumn() const
{
    return state().view.x;
}

optional<i32> BasicTextEditor::cursorVisualLine() const
{
    i32 line = cursorLine() - viewLine();

    if (line > 0 && line < visualLines())
    {
        return line;
    }
    else
    {
        return {};
    }
}

optional<i32> BasicTextEditor::cursorVisualColumn() const
{
    i32 column = cursorColumn() - viewColumn();

    if (column > 0 && column < visualColumns())
    {
        return column;
    }
    else
    {
        return {};
    }
}

i32 BasicTextEditor::documentStart() const
{
    return 0;
}

i32 BasicTextEditor::documentEnd() const
{
    return static_cast<i32>(state().text.size());
}

i32 BasicTextEditor::pageStart() const
{
    i32 start = 0;

    for (i32 i = 0; i < viewLine(); i++)
    {
        start += static_cast<i32>(state().lines[i].size()) + 1;
    }

    return start;
}

i32 BasicTextEditor::pageMiddle() const
{
    return (pageStart() + pageEnd()) / 2;
}

i32 BasicTextEditor::pageEnd() const
{
    i32 end = 0;

    for (i32 i = 0; i < viewLine() + visualLines(); i++)
    {
        end += static_cast<i32>(state().lines[i].size()) + 1;
    }

    return end;
}

i32 BasicTextEditor::softLineStart(i32 index) const
{
    return lineStart(index) + lineIndent(index) * indentSize;
}

i32 BasicTextEditor::lineStart(i32 index) const
{
    i32 start = 0;

    for (i32 i = 0; i < (index < 0 ? cursorLine() : index); i++)
    {
        start += static_cast<i32>(state().lines[i].size()) + 1;
    }

    return start;
}

i32 BasicTextEditor::lineIndent(i32 index) const
{
    const string& line = lineAt(index);

    i32 count = 0;

    for (i32 i = 0; i < static_cast<i32>(line.size()); i++)
    {
        char c = line[i];

        if (c == ' ')
        {
            count++;
        }
        else
        {
            break;
        }
    }

    return count / indentSize;
}

i32 BasicTextEditor::lineEnd(i32 index) const
{
    return lineStart(index) + static_cast<i32>(lineAt(index).size());
}

i32 BasicTextEditor::previousWordStart() const
{
    bool word = false;

    for (i32 i = wordStart() - 1; i >= 0; i--)
    {
        char c = state().text[i];

        if (!isWordBoundary(c))
        {
            word = true;
        }
        else if (word && isWordBoundary(c))
        {
            return i + 1;
        }
    }

    return 0;
}

i32 BasicTextEditor::previousWordEnd() const
{
    for (i32 i = previousWordStart(); i < static_cast<i32>(state().text.size()); i++)
    {
        char c = state().text[i];

        if (isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(state().text.size());
}

i32 BasicTextEditor::wordStart(bool exact) const
{
    const string& text = state().text;

    bool word = false;

    for (i32 i = cursorIndex() + (exact ? 0 : -1); i >= 0; i--)
    {
        char c = text[i];

        if (!isWordBoundary(c))
        {
            word = true;
        }
        else if (word && isWordBoundary(c))
        {
            return i + 1;
        }
    }

    return 0;
}

i32 BasicTextEditor::wordEnd() const
{
    const string& text = state().text;

    bool word = false;

    for (i32 i = cursorIndex(); i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (!isWordBoundary(c))
        {
            word = true;
        }
        else if (word && isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(text.size());
}

i32 BasicTextEditor::nextWordStart() const
{
    const string& text = state().text;

    bool betweenWords = false;

    for (i32 i = wordEnd(); i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (isWordBoundary(c))
        {
            betweenWords = true;
        }
        else if (betweenWords && !isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(text.size());
}

i32 BasicTextEditor::nextWordEnd() const
{
    for (i32 i = nextWordStart(); i < static_cast<i32>(state().text.size()); i++)
    {
        char c = state().text[i];

        if (isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(state().text.size());
}

i32 BasicTextEditor::previousCharacter() const
{
    return cursorIndex() - 1;
}

i32 BasicTextEditor::nextCharacter() const
{
    return cursorIndex();
}

i32 BasicTextEditor::selectionStart() const
{
    i32 cursor = positionToIndex(state().cursor);
    i32 select = positionToIndex(state().select);

    return select < cursor ? select : cursor;
}

i32 BasicTextEditor::selectionEnd() const
{
    i32 cursor = positionToIndex(state().cursor);
    i32 select = positionToIndex(state().select);

    return select > cursor ? select : cursor;
}

i32 BasicTextEditor::selectionLineStart() const
{
    return indexToPosition(selectionStart()).y;
}

i32 BasicTextEditor::selectionLineEnd() const
{
    return indexToPosition(selectionEnd()).y;
}

ivec2 BasicTextEditor::indexToPosition(i32 index) const
{
    i32 start = 0;

    for (i32 i = 0; i < lines(); i++)
    {
        i32 size = static_cast<i32>(state().lines[i].size()) + 1;

        if (index >= start + size)
        {
            start += size;
        }
        else
        {
            return { index - start, i };
        }
    }

    return { static_cast<i32>(state().lines.back().size()), lines() - 1 };
}

i32 BasicTextEditor::positionToIndex(const ivec2& position) const
{
    i32 index = 0;

    for (i32 i = 0; i < position.y && i < lines(); i++)
    {
        index += static_cast<i32>(state().lines[i].size()) + 1;
    }

    i32 maxX = static_cast<i32>(state().lines[position.y].size());

    return index + min(position.x, maxX);
}

ivec2 BasicTextEditor::screenToPosition(const Point& point) const
{
    return {
        viewColumn() + ((point.x - (UIScale::lineNumberColumnWidth() + UIScale::marginSize())) / charWidth()),
        viewLine() + (point.y / charHeight())
    };
}

i32 BasicTextEditor::screenToIndex(const Point& point) const
{
    return positionToIndex(screenToPosition(point));
}

void BasicTextEditor::setState(const function<void(TextEditorState&)>& transform)
{
    TextEditorState state = states.back();
    TextEditorState old = state;

    transform(state);

    state.generateLines();

    _saved = false;
    statusMessage = "";
    states.push_back(state);
    if (static_cast<i32>(states.size()) > maxUndoHistory)
    {
        states.pop_front();
    }
    redoStates.clear();

    correctState(states.back(), old);

    updateSyntaxHighlight(states.back());
}

void BasicTextEditor::updateState(const function<void(TextEditorState&)>& transform)
{
    TextEditorState state = states.back();
    TextEditorState old = state;

    transform(state);

    states.back() = state;

    correctState(states.back(), old);
}

void BasicTextEditor::correctState(TextEditorState& state, const TextEditorState& old) const
{
    state.cursor.y = clamp(state.cursor.y, 0, lines() - 1);
    if (state.cursor.x != old.cursor.x)
    {
        state.cursor.x = clamp(state.cursor.x, 0, static_cast<i32>(lineAtCursor().size()));
    }

    state.select.y = clamp(state.select.y, 0, lines() - 1);
    state.select.x = clamp(state.select.x, 0, static_cast<i32>(lineAt(state.select.y).size()));

    state.view.y = clamp(state.view.y, 0, max(lines() - visualLines(), 0));
    state.view.x = clamp(state.view.x, 0, max((columns() - visualColumns()) + 2, 0));

    if (state.cursor.x != old.cursor.x || state.cursor.y != old.cursor.y)
    {
        i32 viewMinX = state.view.x;
        i32 viewMaxX = (state.view.x + visualColumns()) - 1;
        i32 viewMinY = state.view.y;
        i32 viewMaxY = state.view.y + visualLines();

        if (state.cursor.x < viewMinX)
        {
            state.view.x -= viewMinX - state.cursor.x;
        }
        else if (state.cursor.x >= viewMaxX)
        {
            state.view.x += (state.cursor.x - viewMaxX) + 1;
        }

        if (state.cursor.y < viewMinY)
        {
            state.view.y -= viewMinY - state.cursor.y;
        }
        else if (state.cursor.y >= viewMaxY)
        {
            state.view.y += (state.cursor.y - viewMaxY) + 1;
        }
    }
}

const TextEditorState& BasicTextEditor::state() const
{
    return states.back();
}

void BasicTextEditor::updateSyntaxHighlight(const TextEditorState& state)
{
    if (!codeMode)
    {
        return;
    }

    map<i32, vector<SyntaxHighlightChunk>> hlLines;

    for (i32 i = 0; i < static_cast<i32>(state.lines.size()); i++)
    {
        const string& line = state.lines[i];

        hlLines.insert({
            i,
            calculateSyntaxHighlight(line)
        });
    }

    syntaxHighlight = hlLines;
}

void BasicTextEditor::selectAll(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = true;
            state.select = ivec2();
            state.cursor.x = static_cast<i32>(this->state().lines.back().size());
            state.cursor.y = lines() - 1;
        }
    );
}

void BasicTextEditor::cursorToPreviousWord(const Input& input)
{
    goTo(wordStart());
}

void BasicTextEditor::cursorToNextWord(const Input& input)
{
    goTo(nextWordStart());
}

void BasicTextEditor::selectUp(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.cursor.y--;
        }
    );
}

void BasicTextEditor::selectDown(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.cursor.y++;
        }
    );
}

void BasicTextEditor::selectLeft(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            if (cursorApparentColumn() == 0 && cursorLine() != 0)
            {
                state.cursor.x = static_cast<i32>(lineAt(cursorLine() - 1).size());
                state.cursor.y--;
            }
            else if (cursorApparentColumn() != cursorColumn())
            {
                state.cursor.x = cursorApparentColumn() - 1;
            }
            else
            {
                state.cursor.x--;
            }
        }
    );
}

void BasicTextEditor::selectRight(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            if (cursorApparentColumn() >= static_cast<i32>(lineAtCursor().size()) && cursorLine() != lines() - 1)
            {
                state.cursor.x = 0;
                state.cursor.y++;
            }
            else
            {
                state.cursor.x++;
            }
        }
    );
}

void BasicTextEditor::selectToPreviousWord(const Input& input)
{
    selectTo(wordStart());
}

void BasicTextEditor::selectToNextWord(const Input& input)
{
    selectTo(nextWordStart());
}

void BasicTextEditor::cursorUp(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;
            state.cursor.y--;
        }
    );
}

void BasicTextEditor::cursorDown(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;
            state.cursor.y++;
        }
    );
}

void BasicTextEditor::cursorLeft(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;

            if (cursorApparentColumn() == 0 && cursorLine() != 0)
            {
                state.cursor.x = static_cast<i32>(lineAt(cursorLine() - 1).size());
                state.cursor.y--;
            }
            else if (cursorApparentColumn() != cursorColumn())
            {
                state.cursor.x = cursorApparentColumn() - 1;
            }
            else
            {
                state.cursor.x--;
            }
        }
    );
}

void BasicTextEditor::cursorRight(const Input& input)
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = false;

            if (cursorApparentColumn() >= static_cast<i32>(lineAtCursor().size()) && cursorLine() != lines() - 1)
            {
                state.cursor.x = 0;
                state.cursor.y++;
            }
            else
            {
                state.cursor.x++;
            }
        }
    );
}

void BasicTextEditor::goToLineStart(const Input& input)
{
    goTo(lineStart());
}

void BasicTextEditor::goToLineEnd(const Input& input)
{
    goTo(lineEnd());
}

void BasicTextEditor::removePreviousWord(const Input& input)
{
    remove(wordStart(), cursorIndex() - wordStart());
}

void BasicTextEditor::removeNextWord(const Input& input)
{
    remove(cursorIndex(), wordEnd() - cursorIndex());
}

void BasicTextEditor::removeToLineStart(const Input& input)
{
    remove(lineStart(), cursorIndex() - lineStart());
}

void BasicTextEditor::removeToLineEnd(const Input& input)
{
    remove(cursorIndex(), lineEnd() - cursorIndex());
}

void BasicTextEditor::removePreviousCharacter(const Input& input)
{
    bool unindent = cursorIndex() == softLineStart() && softLineStart() >= lineStart() + indentSize;

    remove(
        unindent ? cursorIndex() - indentSize : previousCharacter(),
        unindent ? indentSize : 1
    );
}

void BasicTextEditor::removeNextCharacter(const Input& input)
{
    remove(nextCharacter(), 1);
}

void BasicTextEditor::upcaseNextWord(const Input& input)
{
    for (i32 i = cursorIndex(); i < wordEnd(); i++)
    {
        char c = state().text[i];

        if (c >= 'a' && c <= 'z')
        {
            replace(i, c - ('a' - 'A'));
        }
    }

    goTo(wordEnd());
}

void BasicTextEditor::downcaseNextWord(const Input& input)
{
    for (i32 i = cursorIndex(); i < wordEnd(); i++)
    {
        char c = state().text[i];

        if (c >= 'A' && c <= 'Z')
        {
            replace(i, c + ('a' - 'A'));
        }
    }

    goTo(wordEnd());
}

void BasicTextEditor::swapWords(const Input& input)
{
    i32 end = wordEnd();

    string a = state().text.substr(previousWordStart(), previousWordEnd() - previousWordStart());
    string b = state().text.substr(wordStart(), wordEnd() - wordStart());
    string c = state().text.substr(previousWordEnd(), wordStart() - previousWordEnd());

    replace(previousWordStart(), b + c + a);
    goTo(end);
}

void BasicTextEditor::swapCharacters(const Input& input)
{
    if (cursorIndex() != 0 && cursorIndex() != static_cast<i32>(state().text.size()))
    {
        char a = state().text[cursorIndex() - 1];
        char b = state().text[cursorIndex()];
        replace(cursorIndex() - 1, b);
        replace(cursorIndex(), a);
        cursorRight();
    }
}

void BasicTextEditor::selectTo(const Input& input)
{
    Point local = pointer - position();

    selectTo(local);
}

void BasicTextEditor::cursorTo(const Input& input)
{
    Point local = pointer - position();

    if (currentTime() - lastClickTime <= multiClickInterval)
    {
        if (!state().selected)
        {
            updateState(
                [&](TextEditorState& state) -> void
                {
                    ivec2 select = indexToPosition(wordStart(true));
                    ivec2 cursor = indexToPosition(wordEnd());

                    state.selected = true;
                    state.select = select;
                    state.cursor = cursor;
                }
            );
        }
        else if (selectionStart() > lineStart() || selectionEnd() < lineEnd())
        {
            updateState(
                [&](TextEditorState& state) -> void
                {
                    ivec2 select = indexToPosition(lineStart());
                    ivec2 cursor = indexToPosition(lineEnd());

                    state.selected = true;
                    state.select = select;
                    state.cursor = cursor;
                }
            );
        }
        else
        {
            goTo(screenToIndex(local));
        }
    }
    else
    {
        startSelect(local);
    }

    lastClickTime = currentTime();
}

void BasicTextEditor::goToStart(const Input& input)
{
    goTo(documentStart());
}

void BasicTextEditor::goToEnd(const Input& input)
{
    goTo(documentEnd());
}

void BasicTextEditor::scrollUp(const Input& input)
{
    scrollUp();
}

void BasicTextEditor::scrollDown(const Input& input)
{
    scrollDown();
}

void BasicTextEditor::panLeft(const Input& input)
{
    scrollLeft();
}

void BasicTextEditor::panRight(const Input& input)
{
    scrollRight();
}

void BasicTextEditor::pageUp(const Input& input)
{
    scrollUp(visualLines());
}

void BasicTextEditor::pageDown(const Input& input)
{
    scrollDown(visualLines());
}

void BasicTextEditor::addText(const Input& input)
{
    if (input.text() == "\t")
    {
        add(cursorIndex(), string(indentSize, ' '));
    }
    else if (input.text() == "\n")
    {
        add(cursorIndex(), "\n" + string(lineIndent() * indentSize, ' '));
    }
    else
    {
        add(cursorIndex(), input.text());
    }
}

void BasicTextEditor::cut(const Input& input)
{
    cutSelection(input);
}

void BasicTextEditor::copy(const Input& input)
{
    copySelection(input);
}

void BasicTextEditor::paste(const Input& input)
{
    pasteClipboard();
}

void BasicTextEditor::undo(const Input& input)
{
    undo();
}

void BasicTextEditor::redo(const Input& input)
{
    redo();
}

void BasicTextEditor::save(const Input& input)
{
    _saved = true;
    onSave(state().text);
}

void BasicTextEditor::reload(const Input& input)
{
    if (source)
    {
        set(source());
    }
}

#if (defined(__linux__) || defined(__FreeBSD__))
void BasicTextEditor::pastePrimary(const Input& input)
{
    Point local = pointer - position();

    pastePrimaryClipboard(local);
}
#endif

void BasicTextEditor::endSelect(const Input& input)
{
    Point local = pointer - position();

    endSelect(input, local);
}

void BasicTextEditor::moveSelect(const Input& input)
{
    Point local = pointer - position();

    moveSelect(local);
}

SizeProperties BasicTextEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
