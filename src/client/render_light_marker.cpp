/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_light_marker.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr u32 ringResolution = 64;
static constexpr u32 spokeCount = 4;
static constexpr f32 spokeLength = 1; // m
static constexpr u32 directionalVertexCount = 2 * ringResolution + 2 * spokeCount + 2 * 3;
static constexpr u32 omnidirectionalVertexCount = 3 * 2 * ringResolution;
static constexpr f32 arrowLength = 0.1; // m

RenderLightMarker::RenderLightMarker(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    f64 angle,
    const HDRColor& color
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, rotation, vec3(1) };

    size_t writeHead = 0;
    vector<ColoredVertex> vertexData;

    if (!roughly(angle, tau))
    {
        vertexCount = directionalVertexCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        fvec3 ringStart = fquaternion(leftDir, angle / 2).rotate(forwardDir * spokeLength);

        for (u32 i = 0; i < ringResolution; i++)
        {
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / ringResolution)).rotate(ringStart), color.color };
            vertexData[writeHead++] = { fquaternion(forwardDir, (i + 1) * (tau / ringResolution)).rotate(ringStart), color.color };
        }

        for (u32 i = 0; i < spokeCount; i++)
        {
            vertexData[writeHead++] = { fvec3(), color.color };
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / spokeCount)).rotate(ringStart), color.color };
        }

        vertexData[writeHead++] = { arrowLength * fvec3(), color.color };
        vertexData[writeHead++] = { arrowLength * fvec3(0, 1, 0), color.color };

        vertexData[writeHead++] = { arrowLength * fvec3(0, 1, 0), color.color };
        vertexData[writeHead++] = { arrowLength * fvec3(-0.2, 0.8, 0), color.color };

        vertexData[writeHead++] = { arrowLength * fvec3(0, 1, 0), color.color };
        vertexData[writeHead++] = { arrowLength * fvec3(+0.2, 0.8, 0), color.color };
    }
    else
    {
        vertexCount = omnidirectionalVertexCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        static const fquaternion ringTransforms[] = {
            fquaternion(forwardDir, upDir),
            fquaternion(rightDir, upDir),
            fquaternion(upDir, backDir)
        };

        for (const fquaternion& ringTransform : ringTransforms)
        {
            for (u32 j = 0; j < ringResolution; j++)
            {
                vec3 a = fquaternion(forwardDir, j * (tau / ringResolution)).rotate(upDir);
                vec3 b = fquaternion(forwardDir, (j + 1) * (tau / ringResolution)).rotate(upDir);

                vertexData[writeHead++] = { ringTransform.rotate(a), color.color };
                vertexData[writeHead++] = { ringTransform.rotate(b), color.color };
            }
        }
    }

    VmaMapping<f32> mapping(ctx->allocator, vertices);
    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderLightMarker::~RenderLightMarker()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderLightMarker::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderLightMarker::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
