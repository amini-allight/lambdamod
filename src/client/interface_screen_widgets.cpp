/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_screen_widgets.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "user.hpp"
#include "tools.hpp"
#include "units.hpp"

void drawBoxSelect(const DrawContext& drawCtx, ControlContext* ctx, const Point& start, const Point& end)
{
    i32 minX = start.x < end.x ? start.x : end.x;
    i32 minY = start.y < end.y ? start.y : end.y;
    i32 maxX = start.x > end.x ? start.x : end.x;
    i32 maxY = start.y > end.y ? start.y : end.y;

    drawBorder(
        drawCtx,
        nullptr,
        Point(minX, minY),
        Size(maxX - minX, maxY - minY),
        UIScale::marginSize(),
        theme.accentColor
    );
}

void drawFrameCounter(const DrawContext& drawCtx, ControlContext* ctx)
{
    static constexpr size_t previousTimesCount = 100;
    static chrono::milliseconds previousTimes[previousTimesCount]{chrono::milliseconds(0)};

    chrono::milliseconds previousTime(0);

    f64 frameTime = 0;

    for (size_t i = 0; i < previousTimesCount - 1; i++)
    {
        previousTimes[i] = previousTimes[i + 1];

        if (previousTimes[0] != chrono::milliseconds(0) && i != 0)
        {
            frameTime += (previousTimes[i] - previousTime).count();
        }

        previousTime = previousTimes[i];
    }

    frameTime /= previousTimesCount - 2;

    f64 fps = frameTime != 0 ? 1000.0 / frameTime : 0;

    previousTimes[previousTimesCount - 1] = currentTime();

    TextStyle style;
    style.color = theme.panelForegroundColor;

    drawText(
        drawCtx,
        nullptr,
        Point(0),
        UIScale::diagnosticLineSize(),
        to_string(static_cast<u64>(ceil(fps))) + " fp" + timeSuffix(),
        style
    );
}

void drawPingCounter(const DrawContext& drawCtx, ControlContext* ctx, u32 ping)
{
    TextStyle style;
    style.color = theme.panelForegroundColor;

    drawText(
        drawCtx,
        nullptr,
        Point(0, UIScale::diagnosticLineSize().h),
        UIScale::diagnosticLineSize(),
        to_string(ping) + " m" + timeSuffix(),
        style
    );
}

void drawStatusLine(const DrawContext& drawCtx, ControlContext* ctx)
{
    string statusLine;

    statusLine += localize("interface-screen-widgets-world") + " " + ctx->controller()->selfWorld()->name();

    statusLine += " | ";

    if (ctx->viewerMode()->editing())
    {
        switch (ctx->viewerMode()->mode())
        {
        case Viewer_Sub_Mode_Entity :
            statusLine += localize("interface-screen-widgets-entity-mode");
            break;
        case Viewer_Sub_Mode_Body :
            statusLine += localize("interface-screen-widgets-body-mode");
            break;
        case Viewer_Sub_Mode_Action :
            statusLine += localize("interface-screen-widgets-action-mode");
            break;
        }
    }
    else
    {
        statusLine += localize("interface-screen-widgets-viewer-mode");
    }

    statusLine += " | ";

    switch (ctx->flatViewer()->mode())
    {
    case Flat_Viewer_Free :
        statusLine += localize("interface-screen-widgets-free-camera");
        break;
    case Flat_Viewer_Perspective :
        statusLine += localize("interface-screen-widgets-perspective-camera");
        break;
    case Flat_Viewer_Orthographic :
        statusLine += localize("interface-screen-widgets-orthographic-camera");
        if (angleBetween(ctx->flatViewer()->rotation().forward(), forwardDir) < radians(1))
        {
            statusLine += " (" + localize("interface-screen-widgets-back") + ")";
        }
        else if (angleBetween(ctx->flatViewer()->rotation().forward(), backDir) < radians(1))
        {
            statusLine += " (" + localize("interface-screen-widgets-front") + ")";
        }
        else if (angleBetween(ctx->flatViewer()->rotation().forward(), upDir) < radians(1))
        {
            statusLine += " (" + localize("interface-screen-widgets-bottom") + ")";
        }
        else if (angleBetween(ctx->flatViewer()->rotation().forward(), downDir) < radians(1))
        {
            statusLine += " (" + localize("interface-screen-widgets-top") + ")";
        }
        else if (angleBetween(ctx->flatViewer()->rotation().forward(), rightDir) < radians(1))
        {
            statusLine += " (" + localize("interface-screen-widgets-left") + ")";
        }
        else if (angleBetween(ctx->flatViewer()->rotation().forward(), leftDir) < radians(1))
        {
            statusLine += " (" + localize("interface-screen-widgets-right") + ")";
        }
        break;
    }

    if (ctx->viewerMode()->editMode()->toolActive())
    {
        statusLine += " | ";

        statusLine += ctx->viewerMode()->editMode()->toolName();
    }

    if (!ctx->viewerMode()->editMode()->toolAxisName().empty())
    {
        statusLine += " | ";

        statusLine += ctx->viewerMode()->editMode()->toolAxisName();
    }

    if (!ctx->viewerMode()->editMode()->toolPivotName().empty())
    {
        statusLine += " | ";

        statusLine += ctx->viewerMode()->editMode()->toolPivotName();
    }

    if (ctx->viewerMode()->editMode()->specialEditing())
    {
        if (ctx->viewerMode()->editMode()->canvasPainting())
        {
            statusLine += " | ";

            switch (ctx->viewerMode()->editMode()->specialEditMode())
            {
            case Special_Edit_None :
                break;
            case Special_Edit_Default :
                statusLine += localize("interface-screen-widgets-paint-canvas");
                break;
            case Special_Edit_Alt :
                statusLine += localize("interface-screen-widgets-erase-canvas");
                break;
            }
        }
    }

    if (ctx->viewerMode()->editMode()->snap())
    {
        statusLine += " | ";

        statusLine += localize("interface-screen-widgets-snap");
    }

    if (ctx->viewerMode()->mode() == Viewer_Sub_Mode_Entity && !ctx->viewerMode()->entityMode()->hiddenEntityIDs().empty())
    {
        statusLine += " | ";

        statusLine += to_string(ctx->viewerMode()->entityMode()->hiddenEntityIDs().size());

        statusLine += " ";

        statusLine += localize("interface-screen-widgets-entities-hidden");
    }

    if (ctx->viewerMode()->mode() == Viewer_Sub_Mode_Body && !ctx->viewerMode()->bodyMode()->hiddenBodyPartIDs().empty())
    {
        statusLine += " | ";

        statusLine += to_string(ctx->viewerMode()->bodyMode()->hiddenBodyPartIDs().size());

        statusLine += " ";

        statusLine += localize("interface-screen-widgets-parts-hidden");
    }

    if (ctx->viewerMode()->editMode()->toolActive())
    {
        statusLine += " | ";

        statusLine += ctx->viewerMode()->editMode()->extent();
    }

    TextStyle style;
    style.color = theme.panelForegroundColor;

    drawText(
        drawCtx,
        nullptr,
        Point(0, drawCtx.size.h - UIScale::diagnosticLineSize().h),
        Size(drawCtx.size.w, UIScale::diagnosticLineSize().h),
        statusLine,
        style
    );

    string gameStatusLine;

    if (ctx->controller()->game().attachmentsLocked())
    {
        gameStatusLine += localize("interface-screen-widgets-attachments-locked");

        gameStatusLine += " | ";
    }

    if (ctx->controller()->game().braking())
    {
        gameStatusLine += localize("interface-screen-widgets-braking");

        gameStatusLine += " | ";
    }

    gameStatusLine += localize("interface-screen-widgets-magnitude", to_string(ctx->controller()->game().magnitude()));

    style.alignment = Text_Right;

    drawText(
        drawCtx,
        nullptr,
        Point(0, drawCtx.size.h - UIScale::diagnosticLineSize().h),
        Size(drawCtx.size.w, UIScale::diagnosticLineSize().h),
        gameStatusLine,
        style
    );
}

void drawOneshot(const DrawContext& drawCtx, ControlContext* ctx, const InterfaceOneshot& oneshot)
{
    optional<Point> screenPos = ctx->flatViewer()->worldToScreen(oneshot.position);

    if (!screenPos)
    {
        return;
    }

    f64 opacity = oneshot.duration / oneshot.initialDuration;

    Color color(oneshot.color);
    color.a *= opacity;

    TextStyle style;
    style.color = color;
    style.weight = Text_Bold;

    drawColoredImage(
        drawCtx,
        nullptr,
        *screenPos - UIScale::markerSize() / 2,
        UIScale::markerSize(),
        oneshot.type,
        color
    );
    drawText(
        drawCtx,
        nullptr,
        *screenPos + UIScale::markerSize() / 2,
        UIScale::markerLineSize(),
        oneshot.name,
        style
    );
    drawText(
        drawCtx,
        nullptr,
        *screenPos + UIScale::markerSize() / 2 + Point(0, UIScale::markerLineSize().h),
        UIScale::markerLineSize(),
        to_string(static_cast<u64>(floor(localizeLength(ctx->flatViewer()->position().distance(oneshot.position))))) + " " + lengthSuffix(),
        style
    );
}

void drawMarker(const DrawContext& drawCtx, ControlContext* ctx, const InterfaceMarker& marker)
{
    optional<Point> screenPos = ctx->flatViewer()->worldToScreen(marker.position);

    if (!screenPos)
    {
        return;
    }

    TextStyle style;
    style.color = Color(marker.color);
    style.weight = Text_Bold;

    drawColoredImage(
        drawCtx,
        nullptr,
        *screenPos - UIScale::markerSize() / 2,
        UIScale::markerSize(),
        marker.type,
        Color(marker.color)
    );
    drawText(
        drawCtx,
        nullptr,
        *screenPos + UIScale::markerSize() / 2,
        UIScale::markerLineSize(),
        marker.name,
        style
    );
    drawText(
        drawCtx,
        nullptr,
        *screenPos + UIScale::markerSize() / 2 + Point(0, UIScale::markerLineSize().h),
        UIScale::markerLineSize(),
        to_string(static_cast<u64>(floor(localizeLength(ctx->flatViewer()->position().distance(marker.position))))) + " " + lengthSuffix(),
        style
    );
}

static void drawBodyPartParentLink(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const vec3& parent,
    const vec3& child
)
{
    optional<Point> screenParent = ctx->flatViewer()->worldToScreen(entity->globalTransform().applyToPosition(parent));
    optional<Point> screenChild = ctx->flatViewer()->worldToScreen(entity->globalTransform().applyToPosition(child));

    if (!screenParent || !screenChild)
    {
        return;
    }

    drawDashedLine(
        drawCtx,
        nullptr,
        *screenParent,
        *screenChild,
        theme.panelForegroundColor
    );
}

static void drawControlPoint(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const vec3& point,
    Icon icon,
    bool selected
)
{
    optional<Point> screen = ctx->flatViewer()->worldToScreen(entity->globalTransform().applyToPosition(point));

    if (!screen)
    {
        return;
    }

    drawColoredImage(
        drawCtx,
        nullptr,
        *screen - UIScale::controlPointSize() / 2,
        UIScale::controlPointSize(),
        icon,
        selected ? theme.accentColor : theme.panelForegroundColor
    );
}

static void drawControlPointText(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const vec3& point,
    const string& text
)
{
    optional<Point> screen = ctx->flatViewer()->worldToScreen(entity->globalTransform().applyToPosition(point));

    if (!screen)
    {
        return;
    }

    TextStyle style;
    style.color = theme.panelForegroundColor;

    drawText(
        drawCtx,
        nullptr,
        *screen + Point(UIScale::controlPointSize().w, -UIScale::controlPointTextSize().h / 2),
        UIScale::controlPointTextSize(),
        text,
        style
    );
}

static void drawAnchor(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const BodyPart* anchor,
    bool selected
)
{
    string name;

    const BodyPartAnchor& data = anchor->get<BodyPartAnchor>();

    switch (data.type)
    {
    default :
        name = bodyAnchorTypeToString(data.type);
        break;
    case Body_Anchor_Generic :
        name = data.name;
        break;
    }

    drawControlPoint(
        drawCtx,
        ctx,
        entity,
        anchor->regionalPosition(),
        Icon_Anchor,
        selected
    );
    drawControlPointText(
        drawCtx,
        ctx,
        entity,
        anchor->regionalPosition(),
        name
    );
}

static void drawLight(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const BodyPart* light,
    bool selected
)
{
    drawControlPoint(
        drawCtx,
        ctx,
        entity,
        light->regionalPosition(),
        Icon_Light,
        selected
    );
}

static void drawForce(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const BodyPart* force,
    bool selected
)
{
    drawControlPoint(
        drawCtx,
        ctx,
        entity,
        force->regionalPosition(),
        Icon_Force,
        selected
    );
}

static void drawArea(
    const DrawContext& drawCtx,
    const ControlContext* ctx,
    const Entity* entity,
    const BodyPart* anchor,
    bool selected
)
{
    drawControlPoint(
        drawCtx,
        ctx,
        entity,
        anchor->regionalPosition(),
        Icon_Area,
        selected
    );
}

static void drawBodyPart(
    const DrawContext& drawCtx,
    ControlContext* ctx,
    const Entity* entity,
    const BodyPart* part,
    bool drawSelected
)
{
    if (ctx->viewerMode()->bodyMode()->hiddenBodyPartIDs().contains(part->id()))
    {
        return;
    }

    if (part->parent())
    {
        drawBodyPartParentLink(
            drawCtx,
            ctx,
            entity,
            part->parentRegionalPosition(),
            part->constraintTransform().position()
        );
    }

    bool selected = ctx->viewerMode()->bodyMode()->selectionIDs().contains({ part->id(), BodyPartSubID() });

    if ((!drawSelected && !selected) || (drawSelected && selected))
    {
        switch (part->type())
        {
        case Body_Part_Anchor :
            drawAnchor(drawCtx, ctx, entity, part, selected);
            break;
        case Body_Part_Light :
            drawLight(drawCtx, ctx, entity, part, selected);
            break;
        case Body_Part_Force :
            drawForce(drawCtx, ctx, entity, part, selected);
            break;
        case Body_Part_Area :
            drawArea(drawCtx, ctx, entity, part, selected);
            break;
        default :
            drawControlPoint(
                drawCtx,
                ctx,
                entity,
                part->regionalPosition(),
                Icon_Part_Control_Point,
                selected
            );
            break;
        }
    }

    vector<vec3> subparts = part->subparts();

    for (u32 i = 0; i < subparts.size(); i++)
    {
        bool selected = ctx->viewerMode()->bodyMode()->selectionIDs().contains({ part->id(), BodyPartSubID(i + 1) });

        if ((!drawSelected && !selected) || (drawSelected && selected))
        {
            drawControlPoint(
                drawCtx,
                ctx,
                entity,
                subparts[i],
                Icon_Subpart_Control_Point,
                selected
            );
        }
    }

    for (const auto& [ id, child ] : part->parts())
    {
        drawBodyPart(drawCtx, ctx, entity, child, drawSelected);
    }
}

void drawBody(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity)
{
    for (const auto& [ id, part ] : entity->body().parts())
    {
        drawBodyPart(drawCtx, ctx, entity, part, false);
    }

    for (const auto& [ id, part ] : entity->body().parts())
    {
        drawBodyPart(drawCtx, ctx, entity, part, true);
    }
}

static void drawEntityParentLink(const DrawContext& drawCtx, ControlContext* ctx, const Entity* child)
{
    optional<Point> screenParent = ctx->flatViewer()->worldToScreen(child->parentGlobalPosition());
    optional<Point> screenChild = ctx->flatViewer()->worldToScreen(child->globalPosition());

    if (!screenParent || !screenChild)
    {
        return;
    }

    drawDashedLine(
        drawCtx,
        nullptr,
        *screenParent,
        *screenChild,
        theme.panelForegroundColor
    );
}

static void drawEntityCard(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity, const Point& screenPos)
{
    // Selection border
    Color selectColor = theme.outdentColor;

    if (entity->selectingUserID())
    {
        const User* selectingUser = ctx->controller()->game().getUser(entity->selectingUserID());

        if (selectingUser)
        {
            selectColor.r = 255 * selectingUser->color().x;
            selectColor.g = 255 * selectingUser->color().y;
            selectColor.b = 255 * selectingUser->color().z;
            selectColor.a = 255;
        }
    }

    Point point = screenPos - (UIScale::entityCardSize() / 2);

    // Background
    drawSolid(
        drawCtx,
        nullptr,
        point,
        UIScale::entityCardSize(),
        theme.backgroundColor
    );

    // Border
    drawBorder(
        drawCtx,
        nullptr,
        point,
        UIScale::entityCardSize(),
        UIScale::marginSize(),
        selectColor
    );

    point += UIScale::marginSize();

    // Draw name
    TextStyle nameStyle;
    nameStyle.weight = Text_Bold;
    nameStyle.alignment = Text_Center;

    drawText(
        drawCtx,
        nullptr,
        point,
        UIScale::entityCardRowSize(),
        entity->name(),
        nameStyle
    );

    // Draw owner
    const User* owner = ctx->controller()->game().getUser(entity->ownerID());

    string ownerText;

    if (owner)
    {
        ownerText = owner->name();
    }
    else
    {
        ownerText = "[" + localize("interface-screen-widgets-no-owner") + "]";
    }

    Color ownerColor = owner ? Color(owner->color()) : theme.inactiveTextColor;

    TextStyle ownerStyle;
    ownerStyle.color = ownerColor;
    ownerStyle.alignment = Text_Center;

    drawText(
        drawCtx,
        nullptr,
        point + Point(0, UIScale::entityCardRowSize().h),
        Size(UIScale::entityCardRowSize().w / 2, UIScale::entityCardRowSize().h),
        ownerText,
        ownerStyle
    );

    // Draw attached
    const User* attached = ctx->controller()->game().getUser(entity->attachedUserID());

    string attachedText;

    if (attached)
    {
        attachedText = attached->name();
    }
    else
    {
        attachedText = "[" + localize("interface-screen-widgets-no-user") + "]";
    }

    Color attachedColor = attached ? Color(attached->color()) : theme.inactiveTextColor;

    TextStyle attachedStyle;
    attachedStyle.color = attachedColor;
    attachedStyle.alignment = Text_Center;

    drawText(
        drawCtx,
        nullptr,
        point + Point(UIScale::entityCardRowSize().w / 2, UIScale::entityCardRowSize().h),
        Size(UIScale::entityCardRowSize().w / 2, UIScale::entityCardRowSize().h),
        attachedText,
        attachedStyle
    );

    // Indicator icons
    u32 indicatorCount = 0;

    if (entity->parent())
    {
        drawImage(
            drawCtx,
            nullptr,
            point + Point(UIScale::entityCardRowSize().h * indicatorCount, UIScale::entityCardRowSize().h * 2),
            Size(UIScale::entityCardRowSize().h),
            Icon_Parent
        );
        indicatorCount++;
    }

    drawImage(
        drawCtx,
        nullptr,
        point + Point(UIScale::entityCardRowSize().h * indicatorCount, UIScale::entityCardRowSize().h * 2),
        Size(UIScale::entityCardRowSize().h),
        entity->active() ? Icon_Active : Icon_Inactive
    );
    indicatorCount++;

    if (entity->shared())
    {
        drawImage(
            drawCtx,
            nullptr,
            point + Point(UIScale::entityCardRowSize().h * indicatorCount, UIScale::entityCardRowSize().h * 2),
            Size(UIScale::entityCardRowSize().h),
            Icon_Shared
        );
        indicatorCount++;
    }

    if (entity->attachedUserID())
    {
        drawImage(
            drawCtx,
            nullptr,
            point + Point(UIScale::entityCardRowSize().h * indicatorCount, UIScale::entityCardRowSize().h * 2),
            Size(UIScale::entityCardRowSize().h),
            Icon_Attached
        );
        indicatorCount++;
    }

    if (entity->host())
    {
        drawImage(
            drawCtx,
            nullptr,
            point + Point(UIScale::entityCardRowSize().h * indicatorCount, UIScale::entityCardRowSize().h * 2),
            Size(UIScale::entityCardRowSize().h),
            Icon_Host
        );
        indicatorCount++;
    }
}

static void drawEntityPoint(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity, const Point& screenPos)
{
    Color selectColor = theme.panelForegroundColor;

    if (entity->selectingUserID())
    {
        const User* selectingUser = ctx->controller()->game().getUser(entity->selectingUserID());

        if (selectingUser)
        {
            selectColor = selectingUser->color();
        }
    }

    drawColoredImage(
        drawCtx,
        nullptr,
        screenPos - UIScale::controlPointSize() / 2,
        UIScale::controlPointSize(),
        Icon_Entity_Control_Point,
        selectColor
    );
}

void drawEntityLinks(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity)
{
    if (!entity->parent())
    {
        return;
    }

    drawEntityParentLink(drawCtx, ctx, entity);
}

void drawEntity(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity, bool hovered)
{
    if (ctx->viewerMode()->entityMode()->hiddenEntityIDs().contains(entity->id()))
    {
        return;
    }

    optional<Point> screenPos = ctx->flatViewer()->worldToScreen(entity->globalPosition());

    if (!screenPos)
    {
        return;
    }

    if (hovered)
    {
        drawEntityCard(drawCtx, ctx, entity, *screenPos);
    }
    else
    {
        drawEntityPoint(drawCtx, ctx, entity, *screenPos);
    }
}

void drawLogOneshots(const DrawContext& drawCtx, ControlContext* ctx, const vector<InterfaceLogOneshot>& oneshots)
{
    size_t drawn = 0;
    for (size_t i = oneshots.size() - 1; i < oneshots.size(); i--)
    {
        const InterfaceLogOneshot& oneshot = oneshots[i];

        f64 opacity = oneshot.duration / oneshot.initialDuration;

        Color color = messageColor(oneshot.message);
        color.a *= opacity;

        TextStyle style;
        style.color = color;

        string message = oneshot.message.toString();

        message = message.substr(0, min<size_t>(message.size(), maxLogOneshotLength));

        drawText(
            drawCtx,
            nullptr,
            Point(0, drawCtx.size.h - ((drawn + 2) * UIScale::diagnosticLineSize().h)),
            UIScale::diagnosticLineSize(),
            message,
            style
        );

        drawn++;
        if (drawn == maxLogOneshots)
        {
            break;
        }
    }
}
