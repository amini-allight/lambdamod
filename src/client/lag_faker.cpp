/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_FAKELAG
#include "lag_faker.hpp"
#include "tools.hpp"

LagFaker::LagFaker(chrono::milliseconds lag)
    : lag(lag)
{

}

bool LagFaker::pushReceive(const string& message)
{
    if (lag == chrono::milliseconds(0))
    {
        return false;
    }

    lock_guard<mutex> lock(receivedMessagesLock);

    receivedMessages.push_back({ message, currentTime() + (lag / 2) });
    return true;
}

vector<string> LagFaker::pullReceive()
{
    lock_guard<mutex> lock(receivedMessagesLock);

    vector<string> ret;

    for (size_t i = 0; i < receivedMessages.size(); i++)
    {
        if (receivedMessages[i].readyTime > currentTime())
        {
            receivedMessages = { receivedMessages.begin() + i, receivedMessages.end() };
            break;
        }
        else
        {
            ret.push_back(receivedMessages[i].message);
        }
    }

    if (!receivedMessages.empty() && receivedMessages.back().readyTime <= currentTime())
    {
        receivedMessages.clear();
    }

    return ret;
}

bool LagFaker::pushSend(const string& message)
{
    if (lag == chrono::milliseconds(0))
    {
        return false;
    }

    lock_guard<mutex> lock(sentMessagesLock);

    sentMessages.push_back({ message, currentTime() + (lag / 2) });
    return true;
}

vector<string> LagFaker::pullSend()
{
    lock_guard<mutex> lock(sentMessagesLock);

    vector<string> ret;

    for (size_t i = 0; i < sentMessages.size(); i++)
    {
        if (sentMessages[i].readyTime > currentTime())
        {
            sentMessages = { sentMessages.begin() + i, sentMessages.end() };
            break;
        }
        else
        {
            ret.push_back(sentMessages[i].message);
        }
    }

    if (!sentMessages.empty() && sentMessages.back().readyTime <= currentTime())
    {
        sentMessages.clear();
    }

    return ret;
}
#endif
