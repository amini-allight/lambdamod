/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "option_select.hpp"

class PanelOptionSelectPopup : public OptionSelectPopup
{
public:
    PanelOptionSelectPopup(
        InterfaceWidget* parent,
        const vector<string>& options,
        const function<void(size_t)>& onSet
    );

    void draw(const DrawContext& ctx) const override;

private:
    i32 viewHeight() const override;
    i32 childHeight() const override;
};

class PanelOptionSelect : public OptionSelect
{
public:
    PanelOptionSelect(
        InterfaceWidget* parent,
        const function<vector<string>()>& optionsSource,
        const function<size_t()>& source,
        const function<void(size_t)>& onSet
    );

    void draw(const DrawContext& ctx) const override;

private:
    SizeProperties sizeProperties() const override;
};
