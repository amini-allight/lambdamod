/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "emissive_color.hpp"
#include "hdr_color.hpp"

inline fvec4 litColor(const EmissiveColor& color)
{
    return color.toVec4();
}

inline fvec4 unlitColor(fvec3 color)
{
    return fvec4(color, -1);
}

inline fvec4 litColor(const HDRColor& color)
{
    return fvec4(color.toVec3(), 1);
}
