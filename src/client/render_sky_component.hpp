/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.
LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_constants.hpp"
#include "render_sky_component_layer.hpp"
#include "render_context.hpp"

#pragma pack(1)
struct SkyBaseGPU
{
    fmat4 worldTransform;
    fvec4 backgroundColor;
    fvec4 horizonColor;
    fvec4 lowerZenithColor;
    fvec4 upperZenithColor;
    i32 type;
};

struct SkyLayerStarsGPU
{
    fmat4 transforms[skyStarsMaxCount];
    fvec4 colors[skyStarsMaxCount];
    fvec4 color;
};

struct SkyLayerCircleGPU
{
    fmat4 worldTransform;
    fvec4 backgroundColor;
    fvec4 color;
    fvec2 position;
    f32 size;
    f32 phase;
};

struct SkyLayerCloudGPU
{
    fmat4 transforms[skyCloudMaxParticleCount];
    fvec4 colors[skyCloudMaxParticleCount];
    fvec4 backgroundColor;
};

struct SkyLayerAuroraGPU
{
    fmat4 transforms[skyAuroraMaxParticleCount];
    fvec4 lowerColor;
    fvec4 upperColor;
};

struct SkyLayerCometGPU
{
    fmat4 transforms[skyCometMaxParticleCount];
    fvec4 colors[skyCometMaxParticleCount];
};

struct SkyLayerVortexGPU
{
    fmat4 transforms[skyVortexMaxParticleCount];
    fvec4 colors[skyVortexMaxParticleCount];
    fvec4 backgroundColor;
};
#pragma pack()

class RenderSkyComponent
{
public:
    RenderSkyComponent(
        const RenderContext* ctx,
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera
    );
    RenderSkyComponent(const RenderSkyComponent& rhs) = delete;
    RenderSkyComponent(RenderSkyComponent&& rhs) = delete;
    ~RenderSkyComponent();

    RenderSkyComponent& operator=(const RenderSkyComponent& rhs) = delete;
    RenderSkyComponent& operator=(RenderSkyComponent&& rhs) = delete;

    void refreshLayers(size_t count);

    VmaBuffer uniform;
    VmaMapping<f32>* uniformMapping;
    VkDescriptorSet descriptorSet;
    VkDescriptorSet envMapDescriptorSet;
    vector<RenderSkyComponentLayer*> layers;

    optional<SkyParameters> lastSky;
    optional<vec3> lastUp;

private:
    const RenderContext* ctx;
    VkDescriptorSetLayout descriptorSetLayout;
    VmaBuffer camera;

    VmaBuffer createUniformBuffer(size_t size) const;
    VkDescriptorSet createDescriptorSet(VmaBuffer camera) const;

    void createLayers(size_t count);
    void destroyLayers();
};
