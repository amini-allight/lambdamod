/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_tracker.hpp"
#include "tools.hpp"

TextTracker::TextTracker(TextID id)
    : _id(id)
    , _type(Text_Unary)
    , _data(TextUnaryRequest())
    , startTime(0)
{

}

TextID TextTracker::id() const
{
    return _id;
}

TextType TextTracker::type() const
{
    return _type;
}

void TextTracker::start()
{
    startTime = currentTime();
}

bool TextTracker::matches(const TextResponse& response) const
{
    return response.id() == _id && response.type() == _type;
}

bool TextTracker::timedOut() const
{
    switch (_type)
    {
    default :
    case Text_Signal :
    case Text_Knowledge :
    case Text_Clear :
        fatal("Cannot query non-tracked text type '" + to_string(_type) + "' for timeout.");
    case Text_Titlecard :
        return startTime != chrono::milliseconds(0)
            ? currentTime() - startTime > textTitlecardDuration
            : false;
    case Text_Timeout :
        return startTime != chrono::milliseconds(0)
            ? currentTime() - startTime > chrono::milliseconds(static_cast<u64>(data<TextTimeoutRequest>().timeout * 1000))
            : false;
    case Text_Unary :
    case Text_Binary :
    case Text_Options :
    case Text_Choose :
    case Text_Assign_Values :
    case Text_Spend_Points :
    case Text_Assign_Points :
        return false;
    case Text_Vote :
        return startTime != chrono::milliseconds(0)
            ? currentTime() - startTime > voteDuration
            : false;
    case Text_Choose_Major :
        return false;
    }
}

chrono::milliseconds TextTracker::elapsed() const
{
    return currentTime() - startTime;
}

TextResponse TextTracker::response() const
{
    switch (_type)
    {
    default :
    case Text_Signal :
    case Text_Titlecard :
    case Text_Timeout :
    case Text_Knowledge :
    case Text_Clear :
    case Text_Unary :
        return TextResponse(_id, TextUnaryResponse());
    case Text_Binary :
        return TextResponse(_id, TextBinaryResponse());
    case Text_Options :
        return TextResponse(_id, TextOptionsResponse());
    case Text_Choose :
        return TextResponse(_id, TextChooseResponse());
    case Text_Assign_Values :
    {
        TextAssignValuesResponse response;

        for (const string& key : data<TextAssignValuesRequest>().keys)
        {
            response.assignment.insert({ key, "" });
        }

        return TextResponse(_id, response);
    }
    case Text_Spend_Points :
        return TextResponse(_id, TextSpendPointsResponse());
    case Text_Assign_Points :
    {
        TextAssignPointsResponse response;

        for (const string& key : data<TextAssignPointsRequest>().keys)
        {
            response.assignment.insert({ key, 0 });
        }

        return TextResponse(_id, response);
    }
    case Text_Vote :
        return TextResponse(_id, TextVoteResponse());
    case Text_Choose_Major :
        return TextResponse(_id, TextChooseMajorResponse());
    }
}

template<>
TextTracker::TextTracker(TextID id, const TextTitlecardRequest& request)
    : TextTracker(id)
{
    _type = Text_Titlecard;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextTimeoutRequest& request)
    : TextTracker(id)
{
    _type = Text_Timeout;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextUnaryRequest& request)
    : TextTracker(id)
{
    _type = Text_Unary;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextBinaryRequest& request)
    : TextTracker(id)
{
    _type = Text_Binary;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextOptionsRequest& request)
    : TextTracker(id)
{
    _type = Text_Options;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextChooseRequest& request)
    : TextTracker(id)
{
    _type = Text_Choose;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextAssignValuesRequest& request)
    : TextTracker(id)
{
    _type = Text_Assign_Values;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextSpendPointsRequest& request)
    : TextTracker(id)
{
    _type = Text_Spend_Points;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextAssignPointsRequest& request)
    : TextTracker(id)
{
    _type = Text_Assign_Points;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextVoteRequest& request)
    : TextTracker(id)
{
    _type = Text_Vote;
    _data = request;
}

template<>
TextTracker::TextTracker(TextID id, const TextChooseMajorRequest& request)
    : TextTracker(id)
{
    _type = Text_Choose_Major;
    _data = request;
}
