/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "oracle_loader.hpp"
#include "oracles.hpp"
#include "yaml_tools.hpp"
#include "tools.hpp"
#include "paths.hpp"

void loadOracles()
{
    for (const fs::directory_entry& entry : fs::directory_iterator(oraclePath))
    {
        YAMLParser ctx(entry.path().string());

        Oracle oracle;
        oracle.name = ctx.root().at("name").as<string>();

        for (const YAMLNode& node : ctx.root().at("columns").sequence())
        {
            OracleColumn column;
            column.title = node.at("title").as<string>();
            column.options = node.at("options").as<vector<string>>();

            oracle.columns.push_back(column);
        }

        oracles.push_back(oracle);
    }
}
