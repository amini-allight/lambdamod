/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_context.hpp"

class RenderStoredDescriptorSetLayout
{
public:
    RenderStoredDescriptorSetLayout(
        const RenderContext* ctx,
        const function<void(VkDevice, VkDescriptorSetLayout&)>& constructor
    );
    RenderStoredDescriptorSetLayout(
        const RenderContext* ctx,
        const function<void(u32, VkPhysicalDevice, VkDevice, VkDescriptorSetLayout&)>& constructor
    );
    RenderStoredDescriptorSetLayout(const RenderStoredDescriptorSetLayout& rhs) = delete;
    RenderStoredDescriptorSetLayout(RenderStoredDescriptorSetLayout&& rhs) = delete;
    ~RenderStoredDescriptorSetLayout();

    RenderStoredDescriptorSetLayout& operator=(const RenderStoredDescriptorSetLayout& rhs) = delete;
    RenderStoredDescriptorSetLayout& operator=(RenderStoredDescriptorSetLayout&& rhs) = delete;

    operator VkDescriptorSetLayout() const;

    VkDescriptorSetLayout descriptorSetLayout;

private:
    const RenderContext* ctx;
};

class RenderStoredShader
{
public:
    RenderStoredShader(const RenderContext* ctx, const string& name);
    RenderStoredShader(const RenderStoredShader& rhs) = delete;
    RenderStoredShader(RenderStoredShader&& rhs) = delete;
    ~RenderStoredShader();

    RenderStoredShader& operator=(const RenderStoredShader& rhs) = delete;
    RenderStoredShader& operator=(RenderStoredShader&& rhs) = delete;

    operator VkShaderModule() const;

    VkShaderModule shader;

private:
    const RenderContext* ctx;
};

class RenderStoredPipeline
{
public:
    RenderStoredPipeline(const RenderContext* ctx, const tuple<VkPipeline, VkPipelineLayout>& objects);
    RenderStoredPipeline(const RenderStoredPipeline& rhs) = delete;
    RenderStoredPipeline(RenderStoredPipeline&& rhs) = delete;
    ~RenderStoredPipeline();

    RenderStoredPipeline& operator=(const RenderStoredPipeline& rhs) = delete;
    RenderStoredPipeline& operator=(RenderStoredPipeline&& rhs) = delete;

    VkPipeline pipeline;
    VkPipelineLayout pipelineLayout;

private:
    const RenderContext* ctx;
};

#define STORED_PIPELINE(_name, ...) \
([&]() -> tuple<VkPipeline, VkPipelineLayout> { \
    VkPipeline pipeline; \
    VkPipelineLayout pipelineLayout; \
    _name(ctx->device, ctx->pipelineCache, __VA_ARGS__, pipeline, pipelineLayout); \
    return { pipeline, pipelineLayout }; \
})()
