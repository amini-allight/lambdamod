/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_DEBUG
#include "rollback_diagnostic.hpp"
#include "controller.hpp"

RollbackDiagnostic::RollbackDiagnostic(Controller* controller)
    : controller(controller)
{

}

RollbackDiagnostic::~RollbackDiagnostic()
{
    ofstream file("rollback.diag");

    file.write(reinterpret_cast<const char*>(entries.data()), entries.size() * sizeof(RollbackDiagnosticEntry));
}

void RollbackDiagnostic::record(i64 index, const StepFrame& frame, const string& text)
{
    RollbackDiagnosticEntry entry{};
    entry.index = index;
    entry.value = value(frame);
    memcpy(entry.text, text.data(), text.size() < 15 ? text.size() : 15);

    entries.push_back(entry);
}

f64 RollbackDiagnostic::value(const StepFrame& frame) const
{
    const map<EntityID, Entity*>& entities = frame.state.getWorld(controller->self()->world())->entities();

    if (entities.empty())
    {
        return 0;
    }

    return entities.begin()->second->globalPosition().x;
}
#endif
