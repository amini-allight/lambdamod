/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "voice_decoder.hpp"
#include "sound_constants.hpp"
#include "log.hpp"
#include "voice_tools.hpp"

#include <opus/opus.h>

VoiceDecoder::VoiceDecoder()
    : decoder(nullptr)
{
    i32 status;

    decoder = opus_decoder_create(voiceFrequency, voiceChannels, &status);

    if (status != OPUS_OK)
    {
        fatal("Failed to initialize voice decoder: " + opusError(status));
    }
}

VoiceDecoder::~VoiceDecoder()
{
    if (decoder)
    {
        opus_decoder_destroy(static_cast<OpusDecoder*>(decoder));
    }
}

string VoiceDecoder::decode(const string& encoded)
{
    string raw(voiceChunkSamplesPerChannel * voiceChannels * sizeof(i16), '\0');

    i32 samplesDecoded = opus_decode(
        static_cast<OpusDecoder*>(decoder),

        reinterpret_cast<const u8*>(encoded.data()),
        encoded.size(),

        reinterpret_cast<i16*>(raw.data()),
        raw.size() / (voiceChannels * sizeof(i16)),

        false
    );

    if (samplesDecoded < 0)
    {
        error("Failed to decode voice: " + opusError(samplesDecoded));
        return "";
    }

    // Compression will convert a zero signal to a very slightly non-zero wave which registers as a buzz in quiet environments, this fixes that
    auto samples = reinterpret_cast<i16*>(raw.data());

    bool flat = true;

    for (i32 i = 0; i < samplesDecoded; i++)
    {
        if (abs(samples[i]) > 1)
        {
            flat = false;
        }
    }

    if (flat)
    {
        memset(raw.data(), 0, raw.size());
    }

    return string(raw.data(), samplesDecoded * sizeof(i16));
}
