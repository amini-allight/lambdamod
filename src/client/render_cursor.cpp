/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_cursor.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

RenderCursor::RenderCursor(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, quaternion(), vec3(1) };

    vertices = createVertexBuffer(18 * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData = {
        // x
        { { -0.1, 0, 0 }, theme.xAxisColor.toVec3() },
        { { 0.1, 0, 0 }, theme.xAxisColor.toVec3() },

        { { 0.1, 0, 0 }, theme.xAxisColor.toVec3() },
        { { 0.08, 0.02, 0 }, theme.xAxisColor.toVec3() },
        { { 0.1, 0, 0 }, theme.xAxisColor.toVec3() },
        { { 0.08, -0.02, 0 }, theme.xAxisColor.toVec3() },

        // y
        { { 0, -0.1, 0 }, theme.yAxisColor.toVec3() },
        { { 0, 0.1, 0 }, theme.yAxisColor.toVec3() },

        { { 0, 0.1, 0 }, theme.yAxisColor.toVec3() },
        { { 0.02, 0.08, 0 }, theme.yAxisColor.toVec3() },
        { { 0, 0.1, 0 }, theme.yAxisColor.toVec3() },
        { { -0.02, 0.08, 0 }, theme.yAxisColor.toVec3() },

        // z
        { { 0, 0, -0.1 }, theme.zAxisColor.toVec3() },
        { { 0, 0, 0.1 }, theme.zAxisColor.toVec3() },

        { { 0, 0, 0.1 }, theme.zAxisColor.toVec3() },
        { { 0.02, 0, 0.08 }, theme.zAxisColor.toVec3() },
        { { 0, 0, 0.1 }, theme.zAxisColor.toVec3() },
        { { -0.02, 0, 0.08 }, theme.zAxisColor.toVec3() }
    };

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderCursor::~RenderCursor()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderCursor::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        18
    );
}

VmaBuffer RenderCursor::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
