/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_icon_button.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "control_context.hpp"

PanelIconButton::PanelIconButton(
    InterfaceWidget* parent,
    Icon icon,
    const function<void()>& onActivate
)
    : IconButton(parent, icon, onActivate)
    , lastButtonTime(0)
{
    START_WIDGET_SCOPE("panel-icon-button")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void PanelIconButton::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        UIScale::panelBorderSize(this),
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    drawColoredImage(
        ctx,
        this,
        Point(UIScale::panelBorderSize(this)),
        UIScale::panelIconButtonSize(this) - Size(UIScale::panelBorderSize(this) * 2),
        icon,
        focused() ? theme.accentColor : theme.panelForegroundColor
    );
}

void PanelIconButton::interact(const Input& input)
{
    if (currentTime() - lastButtonTime < minButtonInterval)
    {
        return;
    }

    playPositiveActivateEffect();
    onActivate();

    lastButtonTime = currentTime();
}

SizeProperties PanelIconButton::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::panelIconButtonSize(this).w), static_cast<f64>(UIScale::panelIconButtonSize(this).h),
        Scaling_Fixed, Scaling_Fixed
    };
}
