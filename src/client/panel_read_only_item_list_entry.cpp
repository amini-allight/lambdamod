/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_read_only_item_list_entry.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

PanelReadOnlyItemListEntry::PanelReadOnlyItemListEntry(
    InterfaceWidget* parent,
    const string& name,
    bool active
)
    : InterfaceWidget(parent)
    , name(name)
    , active(active)
{

}

void PanelReadOnlyItemListEntry::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

    Color color = focused() && !active ? theme.accentColor : theme.panelForegroundColor;

    if (active)
    {
        drawSolid(
            ctx,
            this,
            theme.worldBackgroundColor
        );

        drawBorder(
            ctx,
            this,
            UIScale::panelBorderSize(this),
            color
        );
    }

    TextStyle style(this);
    style.alignment = Text_Center;
    style.size = UIScale::largeFontSize(this);
    style.weight = Text_Bold;
    style.color = color;

    drawText(
        ctx,
        this,
        name,
        style
    );
}

SizeProperties PanelReadOnlyItemListEntry::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::panelItemListRowHeight(this)),
        Scaling_Fill, Scaling_Fixed
    };
}
