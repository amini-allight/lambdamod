/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "direct_widget_input_scope.hpp"

DirectWidgetInputScope::DirectWidgetInputScope(
    DirectInputInterfaceWidget* widget,
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<bool(const Input&)>& isActive,
    const function<void(InputScope*)>& block
)
    : WidgetInputScope(
        dynamic_cast<InterfaceWidget*>(widget),
        parent,
        name,
        mode,
        isActive,
        block
    )
{

}

InputHandlingResult DirectWidgetInputScope::handle(const Input& input, u32 repeats, size_t depth) const
{
    if (shouldInvokeBindings(input))
    {
        dynamic_cast<DirectInputInterfaceWidget*>(_widget)->input(input, repeats);
    }

    InputHandlingResult result(ctx, input);

    result.setBlocking(mode == Input_Scope_Blocking);

    return result;
}
