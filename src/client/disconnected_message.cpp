/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "disconnected_message.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "input_scope.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "vr_panel_input_scope.hpp"

#ifdef LMOD_VR
static constexpr chrono::seconds minDrawTime(1);
#endif

DisconnectedMessage::DisconnectedMessage(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
{
#ifdef LMOD_VR
    if (g_vr)
    {
        START_VR_PANEL_SCOPE(context()->input()->getScope("disconnected"), "disconnected-message")
            WIDGET_SLOT("return-to-menu", reset)
        END_SCOPE
    }
    else
    {
        START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("disconnected"), "disconnected-message")
            WIDGET_SLOT("return-to-menu", reset)
        END_SCOPE
    }
#else
    START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("disconnected"), "disconnected-message")
        WIDGET_SLOT("return-to-menu", reset)
    END_SCOPE
#endif
}

void DisconnectedMessage::draw(const DrawContext& ctx) const
{
#ifdef LMOD_VR
    if (!firstDrawTime)
    {
        firstDrawTime = currentTime();
    }
#endif

    drawSolid(ctx, this, theme.worldBackgroundColor);

    TextStyle style;
    style.alignment = Text_Center;
    style.weight = Text_Bold;
    style.size = UIScale::mediumFontSize(this);
    style.color = theme.panelForegroundColor;

    drawText(
        ctx,
        this,
        localize("disconnected-message-disconnected", (context()->controller()->disconnectionReason().empty()
            ? localize("disconnected-message-connection-failed")
            : context()->controller()->disconnectionReason())),
        style
    );
}

bool DisconnectedMessage::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && context()->controller()->connectionState() == Connection_State_Disconnected;
}

void DisconnectedMessage::reset(const Input& input)
{
    context()->controller()->markForReset();
}

#ifdef LMOD_VR
optional<Point> DisconnectedMessage::toLocal(const vec3& position, const quaternion& rotation) const
{
    // This prevents input gobbling
    return shouldDraw() && firstDrawTime && currentTime() - *firstDrawTime >= minDrawTime ? Point() : optional<Point>();
}
#endif

SizeProperties DisconnectedMessage::sizeProperties() const
{
    return sizePropertiesFillAll;
}
