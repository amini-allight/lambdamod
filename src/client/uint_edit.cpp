/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "uint_edit.hpp"
#include "tools.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "centered_line_edit.hpp"

UIntEdit::UIntEdit(
    InterfaceWidget* parent,
    const function<u64()>& source,
    const function<void(const u64&)>& onSet
)
    : InterfaceWidget(parent)
{
    u = new CenteredLineEdit(
        this,
        source ? [source]() -> string
        {
            return to_string(source());
        } : function<string()>(nullptr),
        [onSet](const string& s) -> void
        {
            try
            {
                onSet(toU64(s));
            }
            catch (const exception& e)
            {
            }
        }
    );
}

void UIntEdit::set(u64 value)
{
    u->set(to_string(value));
}

u64 UIntEdit::value() const
{
    try
    {
        return toU64(u->content());
    }
    catch (const exception& e)
    {
        return 0;
    }
}

void UIntEdit::clear()
{
    u->clear();
}

void UIntEdit::setPlaceholder(const string& text)
{
    u->setPlaceholder(text);
}

SizeProperties UIntEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
