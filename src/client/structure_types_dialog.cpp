/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "structure_types_dialog.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "editor_multi_entry.hpp"
#include "editor_entry.hpp"
#include "text_button.hpp"
#include "spacer.hpp"
#include "ufloat_edit.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(384, 512) * uiScale();
}

StructureTypesDialog::StructureTypesDialog(
    InterfaceView* view,
    const function<StructureTypeID()>& nextTypeIDSource,
    const function<void(StructureTypeID)>& onNextTypeID,
    const function<vector<StructureType>()>& typesSource,
    const function<void(const vector<StructureType>&)>& onTypes
)
    : Dialog(view, dialogSize())
    , nextTypeIDSource(nextTypeIDSource)
    , onNextTypeID(onNextTypeID)
    , typesSource(typesSource)
    , onTypes(onTypes)
    , listView(new ListView(this))
{
    updateWidgets();
}

void StructureTypesDialog::step()
{
    if (needsWidgetUpdate())
    {
        updateWidgets();
    }

    InterfaceWidget::step();
}

bool StructureTypesDialog::needsWidgetUpdate() const
{
    if (!lastStructureTypes)
    {
        return true;
    }

    vector<StructureType> structureTypes = typesSource();

    if (lastStructureTypes->size() != structureTypes.size())
    {
        return true;
    }

    return false;
}

void StructureTypesDialog::updateWidgets()
{
    listView->clearChildren();

    addChildren();

    listView->update();

    lastStructureTypes = typesSource();
}

void StructureTypesDialog::addChildren()
{
    vector<StructureType> types = typesSource();

    for (size_t i = 0; i < types.size(); i++)
    {
        addStructureTypeChildren(i, types[i]);
    }

    addStructureTypeNewPrompt();
}

void StructureTypesDialog::addStructureTypeChildren(size_t index, const StructureType& type)
{
    MAKE_SPACER(listView, 0, UIScale::marginSize() * 8);

    new EditorEntry(listView, "structure-types-dialog-thickness", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&StructureTypesDialog::thicknessSource, this, index),
            bind(&StructureTypesDialog::onThickness, this, index, placeholders::_1)
        );
    });

    new EditorMultiEntry(
        listView,
        1,
        [this](InterfaceWidget* parent, size_t index) -> void
        {
            new TextButton(
                parent,
                localize("structure-types-dialog-remove-structure-type"),
                bind(&StructureTypesDialog::removeStructureType, this, index)
            );
        }
    );
}

void StructureTypesDialog::addStructureTypeNewPrompt()
{
    new EditorMultiEntry(
        listView,
        1,
        [this](InterfaceWidget* parent, size_t index) -> void
        {
            new TextButton(
                parent,
                localize("structure-types-dialog-add-structure-type"),
                bind(&StructureTypesDialog::addStructureType, this)
            );
        }
    );
}

void StructureTypesDialog::addStructureType()
{
    vector<StructureType> structureTypes = typesSource();
    StructureTypeID nextStructureTypeID = nextTypeIDSource();

    structureTypes.push_back(StructureType(nextStructureTypeID++));

    onNextTypeID(nextStructureTypeID);
    onTypes(structureTypes);
}

void StructureTypesDialog::removeStructureType(size_t index)
{
    vector<StructureType> structureTypes = typesSource();

    structureTypes.erase(structureTypes.begin() + index);

    onTypes(structureTypes);
}

f64 StructureTypesDialog::thicknessSource(size_t index)
{
    return localizeLength(typesSource().at(index).thickness);
}

void StructureTypesDialog::onThickness(size_t index, f64 thickness)
{
    vector<StructureType> structureTypes = typesSource();

    structureTypes.at(index).thickness = delocalizeLength(thickness);

    onTypes(structureTypes);
}
