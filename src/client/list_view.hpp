/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "layout.hpp"
#include "scrollable.hpp"

class ListView : public Layout, public Scrollable
{
public:
    ListView(
        InterfaceWidget* parent,
        const function<void(i32)>& onSelect = nullptr,
        const optional<Color>& color = {}
    );

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void draw(const DrawContext& ctx) const override;

    void setScrollbarEnabled(bool state);

    i32 activeIndex() const;

protected:
    function<void(i32)> onSelect;
    optional<Color> color;
    bool scrollbarEnabled;
    i32 _activeIndex;

    i32 viewHeight() const override;

    i32 scrollIndex() const override;
    void setScrollIndex(i32 index) override;

    i32 childCount() const override;
    i32 childHeight() const override;

    virtual void interact(const Input& input);
    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);
    void release(const Input& input);
    void drag(const Input& input);

    SizeProperties sizeProperties() const override;
};
