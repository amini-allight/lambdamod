/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_list_view.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

PanelListView::PanelListView(
    InterfaceWidget* parent,
    const function<void(i32)>& onSelect
)
    : ListView(parent, onSelect)
{

}

void PanelListView::move(const Point& position)
{
    this->position(position);

    Point p = position;
    p.y -= elapsedHeight();

    for (InterfaceWidget* child : children())
    {
        child->move(p);

        p.y += child->size().h;
    }
}

void PanelListView::resize(const Size& size)
{
    this->size(size);

    Size available = size;

    available.w -= UIScale::panelScrollbarWidth(this);

    for (InterfaceWidget* child : children())
    {
        i32 w = 0;

        switch (sizePropertiesOf(child).horizontal)
        {
        case Scaling_Fixed :
            w = sizePropertiesOf(child).w;
            break;
        case Scaling_Fraction :
            w = sizePropertiesOf(child).w * available.w;
            break;
        case Scaling_Fill :
            w = available.w;
            break;
        }

        i32 h = 0;

        switch (sizePropertiesOf(child).vertical)
        {
        case Scaling_Fixed :
            h = sizePropertiesOf(child).h;
            break;
        case Scaling_Fraction :
            h = sizePropertiesOf(child).h * available.h;
            break;
        case Scaling_Fill :
            h = available.h;
            break;
        }

        child->resize({ w, h });
    }
}

void PanelListView::draw(const DrawContext& ctx) const
{
    if (needsScroll())
    {
        drawSolid(
            ctx,
            this,
            Point((size().w - UIScale::panelScrollbarWidth(this)) + (UIScale::panelScrollbarWidth(this) - UIScale::panelScrollbarTrackWidth(this)) / 2, 0),
            Size(UIScale::panelScrollbarTrackWidth(this), size().h),
            theme.panelForegroundColor
        );

        drawSolid(
            ctx,
            this,
            Point((size().w - UIScale::panelScrollbarWidth(this)) + (UIScale::panelScrollbarWidth(this) - UIScale::panelScrollbarThumbWidth(this)) / 2, thumbY()),
            Size(UIScale::panelScrollbarThumbWidth(this), thumbHeight()),
            theme.panelForegroundColor
        );
    }

    InterfaceWidget::draw(ctx);
}

void PanelListView::interact(const Input& input)
{
    Point local = pointer - position();

    if (local.x < size().w - UIScale::panelScrollbarWidth(this))
    {
        i32 y = 0;
        for (i32 i = _activeIndex; i < static_cast<i32>(children().size()); i++)
        {
            InterfaceWidget* child = children()[i];

            y += child->size().h;

            if (y > local.y && onSelect)
            {
                onSelect(i);
                break;
            }
        }
    }
    else
    {
        if (local.y >= thumbY() && local.y < thumbY() + thumbHeight())
        {
            grabThumb(local);
        }
        else
        {
            i32 targetThumbY = local.y - (thumbHeight() / 2);

            setScrollIndexFromThumbY(targetThumbY);
        }
    }
}
