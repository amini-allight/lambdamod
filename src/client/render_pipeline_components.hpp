/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "render_types.hpp"

void createPipelineLayout(
    VkDevice device,
    VkDescriptorSetLayout descriptorSetLayout,
    VkPipelineLayout& pipelineLayout
);
VkPipelineVertexInputStateCreateInfo emptyVertexInputStage();
VkPipelineVertexInputStateCreateInfo basicVertexInputStage();
VkPipelineVertexInputStateCreateInfo coloredVertexInputStage();
VkPipelineVertexInputStateCreateInfo litColoredVertexInputStage();
VkPipelineVertexInputStateCreateInfo texturedVertexInputStage();
VkPipelineVertexInputStateCreateInfo litTexturedVertexInputStage();
VkPipelineVertexInputStateCreateInfo ropeVertexInputStage();
VkPipelineVertexInputStateCreateInfo litRopeVertexInputStage();
VkPipelineInputAssemblyStateCreateInfo pointInputAssemblyStage();
VkPipelineInputAssemblyStateCreateInfo lineInputAssemblyStage();
VkPipelineInputAssemblyStateCreateInfo triangleInputAssemblyStage();
VkPipelineShaderStageCreateInfo shaderStage(VkShaderStageFlagBits stage, VkShaderModule shader);
VkPipelineViewportStateCreateInfo defaultViewportStage();
VkPipelineRasterizationStateCreateInfo pointRasterizationStage(bool culling = false);
VkPipelineRasterizationStateCreateInfo lineRasterizationStage(f32 lineWidth = 2, bool culling = false);
VkPipelineRasterizationStateCreateInfo triangleRasterizationStage(bool culling = false);
VkPipelineMultisampleStateCreateInfo defaultMultisampleStage(VkSampleCountFlagBits sampleCount);
VkPipelineDepthStencilStateCreateInfo defaultDepthStencilStage();
VkPipelineDepthStencilStateCreateInfo ignoreDepthStencilStage();
VkPipelineDepthStencilStateCreateInfo readOnlyDepthStencilStage();
VkPipelineDepthStencilStateCreateInfo writeOnlyDepthStencilStage();
VkPipelineColorBlendStateCreateInfo defaultColorBlendStage();
VkPipelineColorBlendStateCreateInfo stencilColorBlendStage();
VkPipelineDynamicStateCreateInfo defaultDynamicState();
VkPipelineDynamicStateCreateInfo lineWidthDynamicState();
