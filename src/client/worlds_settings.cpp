/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "worlds_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "row.hpp"
#include "label.hpp"
#include "spacer.hpp"
#include "indicator_icon.hpp"
#include "world.hpp"
#include "fuzzy_find.hpp"

#include "new_world_dialog.hpp"
#include "confirm_dialog.hpp"
#include "rename_dialog.hpp"
#include "clone_dialog.hpp"

WorldDetail::WorldDetail()
{
    entityCount = 0;
    shown = false;
}

WorldDetail::WorldDetail(World* world)
    : WorldDetail()
{
    name = world->name();
    entityCount = world->entities().size();
    shown = world->shown();
}

bool WorldDetail::operator==(const WorldDetail& rhs) const
{
    return name == rhs.name &&
        entityCount == rhs.entityCount &&
        shown == rhs.shown;
}

bool WorldDetail::operator!=(const WorldDetail& rhs) const
{
    return !(*this == rhs);
}

WorldsSettings::WorldsSettings(InterfaceWidget* parent)
    : RightClickableWidget(parent)
{
    START_WIDGET_SCOPE("worlds-settings")
        WIDGET_SLOT("add-new", add)
    END_SCOPE

    auto column = new Column(this);

    auto header = new Row(column);

    TextStyleOverride style;
    style.weight = Text_Bold;
    style.color = &theme.activeTextColor;
    style.alignment = Text_Center;

    new Label(header, localize("worlds-settings-name"), style);
    new Label(header, localize("worlds-settings-shown"), style);
    new Label(header, localize("worlds-settings-entities"), style);
    MAKE_SPACER(header, UIScale::scrollbarWidth(), UIScale::labelHeight());

    searchBar = new LineEdit(column, nullptr, [](const string& text) -> void {});
    searchBar->setPlaceholder(localize("worlds-settings-type-to-search"));

    listView = new ListView(column);
}

void WorldsSettings::step()
{
    RightClickableWidget::step();

    vector<WorldDetail> details = convert(context()->controller()->game().worlds());
    string search = searchBar->content();

    if (details != this->details || search != this->search)
    {
        this->details = details;
        this->search = search;
        updateListView();
    }
}

vector<WorldDetail> WorldsSettings::convert(const map<string, World*>& worlds) const
{
    vector<WorldDetail> details;
    details.reserve(worlds.size());

    for (const auto& [ name, world ] : worlds)
    {
        if (!context()->controller()->self()->admin() && !world->shown())
        {
            continue;
        }

        details.push_back(WorldDetail(world));
    }

    return details;
}

void WorldsSettings::openRightClickMenu(i32 index, const Point& pointer)
{
    if (index >= static_cast<i32>(details.size()))
    {
        return;
    }

    const WorldDetail& detail = details[index];

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("worlds-settings-rename"), bind(&WorldsSettings::onRename, this, detail.name, placeholders::_1) },
        { localize("worlds-settings-clone"), bind(&WorldsSettings::onClone, this, detail.name, placeholders::_1) },
        { localize("worlds-settings-remove"), bind(&WorldsSettings::onRemove, this, detail.name, placeholders::_1) },
        { localize("worlds-settings-shift"), bind(&WorldsSettings::onShift, this, detail.name, placeholders::_1) }
    };

    rightClickMenu = new RightClickMenu(
        this,
        bind(&WorldsSettings::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void WorldsSettings::onRename(const string& name, const Point& point)
{
    context()->interface()->addWindow<RenameDialog>(
        [this, name](const string& newName) -> void {
            if (!context()->controller()->game().canDeleteWorld(name))
            {
                return;
            }

            context()->controller()->game().addWorld(
                newName,
                World(newName, *context()->controller()->game().getWorld(name))
            );
            context()->controller()->game().removeWorld(name);
        }
    );

    closeRightClickMenu();
}

void WorldsSettings::onClone(const string& name, const Point& point)
{
    context()->interface()->addWindow<CloneDialog>(
        [this, name](const string& newName) -> void {
            context()->controller()->game().addWorld(
                newName,
                World(newName, *context()->controller()->game().getWorld(name))
            );
        }
    );

    closeRightClickMenu();
}

void WorldsSettings::onRemove(const string& name, const Point& point)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("worlds-settings-are-you-sure-you-want-to-delete-world", name),
        [this, name](bool confirmed) -> void
        {
            if (!context()->controller()->game().canDeleteWorld(name))
            {
                return;
            }

            if (!confirmed)
            {
                return;
            }

            context()->controller()->game().removeWorld(name);
        }
    );

    closeRightClickMenu();
}

void WorldsSettings::onShift(const string& name, const Point& point)
{
    UserEvent event(User_Event_Shift);
    event.s = name;

    context()->controller()->send(event);

    closeRightClickMenu();
}

void WorldsSettings::updateListView()
{
    listView->clearChildren();

    // If we sort the member variable version it will cause an update on every WorldsSettings::step
    vector<WorldDetail> details = this->details;

    if (!search.empty())
    {
        sort(
            details.begin(),
            details.end(),
            [&](const WorldDetail& a, const WorldDetail& b) -> bool
            {
                return fuzzyFindCompare(a.name, b.name, search);
            }
        );
    }

    size_t i = 0;
    for (const WorldDetail& detail : details)
    {
        auto row = new Row(
            listView,
            {},
            [this, i](const Point& pointer) -> void
            {
                onRightClick(i, pointer);
            }
        );

        new Label(row, detail.name);

        new IndicatorIcon(row, detail.shown ? Icon_Shown : Icon_Hidden);

        TextStyleOverride style;
        style.alignment = Text_Center;

        new Label(row, to_string(detail.entityCount), style);

        i++;
    }

    listView->update();
}

void WorldsSettings::add(const Input& input)
{
    context()->interface()->addWindow<NewWorldDialog>();
}

SizeProperties WorldsSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
