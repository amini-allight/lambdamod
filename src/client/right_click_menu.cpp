/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "right_click_menu.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "flat_render.hpp"

RightClickMenu::RightClickMenu(
    InterfaceWidget* parent,
    const function<void()>& onClose,
    const vector<tuple<string, function<void(const Point&)>>>& entries
)
    : Popup(parent)
    , onClose(onClose)
    , entries(entries)
    , activeIndex(-1)
{
    START_WIDGET_SCOPE("right-click-menu")
        WIDGET_SLOT("close-context-menu", dismiss)
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("interact", interact)

        START_BLOCKING_SCOPE("unfocused-right-click-menu", !focused())
            WIDGET_SLOT("close-context-menu", dismiss)
            WIDGET_SLOT("close-dialog", dismiss)
            WIDGET_SLOT("interact", dismiss)
        END_SCOPE
    END_SCOPE
}

void RightClickMenu::open(const Point& position)
{
    resize(desiredSize());

    i32 width = dynamic_cast<const FlatRender*>(context()->controller()->render())->width();
    i32 height = dynamic_cast<const FlatRender*>(context()->controller()->render())->height();

    if (width - position.x < desiredSize().w && height - position.y < desiredSize().h)
    {
        move(position - desiredSize());
    }
    else if (width - position.x < desiredSize().w)
    {
        move(position - Size(desiredSize().w, 0));
    }
    else if (height - position.y < desiredSize().h)
    {
        move(position - Size(0, desiredSize().h));
    }
    else
    {
        move(position);
    }

    Popup::open();
}

void RightClickMenu::step()
{
    InterfaceWidget::step();

    Point local = pointer - position();

    activeIndex = clamp<i32>(local.y / UIScale::menuRowHeight(), 0, entries.size() - 1);
}

void RightClickMenu::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.indentColor
    );

    i32 y = 0;
    for (const auto& [ name, callback ] : entries)
    {
        if (focused() && y == activeIndex)
        {
            drawSolid(
                ctx,
                this,
                Point(0, y * UIScale::menuRowHeight()),
                Size(UIScale::menuWidth(), UIScale::menuRowHeight()),
                theme.accentColor
            );
        }

        TextStyle style;
        style.color = theme.textColor;

        drawText(
            ctx,
            this,
            Point(0, y * UIScale::menuRowHeight()),
            Size(UIScale::menuWidth(), UIScale::menuRowHeight()),
            name,
            style
        );

        y++;
    }
}

void RightClickMenu::interact(const Input& input)
{
    i32 y = 0;
    for (const auto& [ name, callback ] : entries)
    {
        if (y == activeIndex)
        {
            playPositiveActivateEffect();
            callback(pointer);
            return;
        }

        y++;
    }
}

void RightClickMenu::dismiss(const Input& input)
{
    onClose();
}

Size RightClickMenu::desiredSize() const
{
    return Size(
        UIScale::menuWidth(),
        UIScale::menuRowHeight() * entries.size()
    );
}

SizeProperties RightClickMenu::sizeProperties() const
{
    return {
        static_cast<f64>(desiredSize().w), static_cast<f64>(desiredSize().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
