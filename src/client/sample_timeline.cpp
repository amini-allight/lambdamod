/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_timeline.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"

SampleTimeline::SampleTimeline(
    InterfaceWidget* parent,
    EntityID id,
    const function<u32()>& playPositionSource,
    const function<void()>& onPlayPause,
    const function<void(u32)>& onSeek,
    const function<void(const set<SamplePartID>&)>& onSelect
)
    : InterfaceWidget(parent)
    , id(id)
    , scrollIndex(0)
    , panIndex(0)
    , playPositionSource(playPositionSource)
    , onPlayPause(onPlayPause)
    , onSeek(onSeek)
    , onSelect(onSelect)
    , transformMode(Sample_Transform_None)
    , transformSubMode(Sample_Transform_Sub_None)
    , selecting(false)
    , panStartScrollIndex(0)
    , panStartPanIndex(0)
    , panning(false)
{
    START_WIDGET_SCOPE("sample-timeline")
        WIDGET_SLOT("select-all", selectAll)
        WIDGET_SLOT("deselect-all", deselectAll)
        WIDGET_SLOT("invert-selection", invertSelection)
        WIDGET_SLOT("duplicate", duplicate)
        WIDGET_SLOT("cut", cut)
        WIDGET_SLOT("copy", copy)
        WIDGET_SLOT("paste", paste)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("pan-left", panLeft)
        WIDGET_SLOT("pan-right", panRight)
        WIDGET_SLOT("add-new", addNew)
        WIDGET_SLOT("remove-selected", removeSelected)
        WIDGET_SLOT("start-select", startSelect)
        WIDGET_SLOT("start-pan", startPan)
        WIDGET_SLOT("translate", translate)
        WIDGET_SLOT("toggle-play", togglePlay)

        START_INDEPENDENT_SCOPE("selecting", selecting)
            WIDGET_SLOT("move-select", moveSelect)
            WIDGET_SLOT("end-replace-select", endReplaceSelect)
            WIDGET_SLOT("end-add-select", endAddSelect)
            WIDGET_SLOT("end-remove-select", endRemoveSelect)
        END_SCOPE

        START_INDEPENDENT_SCOPE("panning", panning)
            WIDGET_SLOT("pan", pan)
            WIDGET_SLOT("end-pan", endPan)
        END_SCOPE

        START_BLOCKING_SCOPE("tool", transformMode != Sample_Transform_None)
            WIDGET_SLOT("toggle-x", toggleX)
            WIDGET_SLOT("toggle-y", toggleY)
            WIDGET_SLOT("use-tool", useTool)
            WIDGET_SLOT("cancel-tool", cancelTool)
            WIDGET_SLOT("end-tool", endTool)
        END_SCOPE
    END_SCOPE
}

void SampleTimeline::open(const string& name)
{
    this->name = name;
}

void SampleTimeline::draw(const DrawContext& ctx) const
{
    drawSolid(ctx, this, theme.softIndentColor);

    // Index lines
    for (size_t i = 0; i < static_cast<size_t>(((size().h - UIScale::timelineTopHeaderHeight()) / UIScale::timelineRowHeight()) + 1); i++)
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth(), size().h - ((i + 1) * UIScale::timelineRowHeight())),
            Size(size().w - UIScale::timelineLeftHeaderWidth(), 1),
            theme.indentColor
        );
    }

    // Time lines
    for (size_t i = 0; i < static_cast<size_t>(((size().w - UIScale::timelineLeftHeaderWidth()) / UIScale::timelineColumnWidth()) + 1); i++)
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth() + (i * UIScale::timelineColumnWidth()), UIScale::timelineTopHeaderHeight()),
            Size(1, size().h - UIScale::timelineTopHeaderHeight()),
            theme.indentColor
        );
    }

    // Notes
    for (const auto& [ id, part ] : currentSample().parts())
    {
        drawSolid(
            ctx,
            this,
            screenPosition(part),
            Size(part.length() / timelineMillisecondsPerPixel, UIScale::timelineRowHeight() * part.volume()),
            white
        );

        if (activeParts.contains(id))
        {
            drawBorder(
                ctx,
                this,
                screenPosition(part) - Point(UIScale::marginSize()),
                Size(part.length() / timelineMillisecondsPerPixel, UIScale::timelineRowHeight() * part.volume()) + Size(UIScale::marginSize() * 2),
                UIScale::marginSize(),
                theme.accentColor
            );
        }
    }

    // Current play position
    drawSolid(
        ctx,
        this,
        Point(UIScale::timelineLeftHeaderWidth() + (playPositionSource() - (panIndex * millisecondsPerColumn())) / timelineMillisecondsPerPixel, UIScale::timelineTopHeaderHeight()),
        Size(1, size().h - UIScale::timelineTopHeaderHeight()),
        theme.accentColor
    );

    // Index column
    drawSolid(
        ctx,
        this,
        Point(0, 0),
        Size(UIScale::timelineLeftHeaderWidth(), size().h),
        theme.backgroundColor
    );

    for (size_t i = 0; i < static_cast<size_t>(((size().h - UIScale::timelineTopHeaderHeight()) / UIScale::timelineRowHeight()) + 1); i++)
    {
        drawText(
            ctx,
            this,
            Point(0, size().h - ((i + 1) * UIScale::timelineRowHeight())),
            Size(UIScale::timelineLeftHeaderWidth(), UIScale::timelineRowHeight()),
            to_string(scrollIndex + i),
            TextStyle()
        );
    }

    // Time row
    drawSolid(
        ctx,
        this,
        Point(0, 0),
        Size(size().w, UIScale::timelineTopHeaderHeight()),
        theme.backgroundColor
    );

    for (size_t i = 0; i < static_cast<size_t>(((size().w - UIScale::timelineLeftHeaderWidth()) / UIScale::timelineColumnWidth()) + 1); i++)
    {
        drawText(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth() + (i * UIScale::timelineColumnWidth()), 0),
            Size(UIScale::timelineColumnWidth(), UIScale::timelineTopHeaderHeight()),
            toFixedPrecision(((panIndex + i) * millisecondsPerColumn()) / 1000.0, 1),
            TextStyle()
        );
    }

    if (transformSubMode == Sample_Transform_Sub_X)
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth(), size().h / 2),
            Size(size().w - UIScale::timelineLeftHeaderWidth(), 1),
            theme.xAxisColor
        );
    }
    else if (transformSubMode == Sample_Transform_Sub_Y)
    {
        drawSolid(
            ctx,
            this,
            Point(size().w / 2, UIScale::timelineTopHeaderHeight()),
            Size(1, size().h - UIScale::timelineTopHeaderHeight()),
            theme.yAxisColor
        );
    }

    bool box = sqrt(sq(selectEnd.x - selectStart.x) + sq(selectEnd.y - selectStart.y)) >= UIScale::minBoxSelectSize();

    if (selecting && box)
    {
        i32 minX = min(selectStart.x, selectEnd.x);
        i32 minY = min(selectStart.y, selectEnd.y);
        i32 maxX = max(selectStart.x, selectEnd.x);
        i32 maxY = max(selectStart.y, selectEnd.y);

        minX -= position().x;
        minY -= position().y;
        maxX -= position().x;
        maxY -= position().y;

        drawBorder(
            ctx,
            this,
            Point(minX, minY),
            Size(maxX - minX, maxY - minY),
            UIScale::marginSize(),
            theme.accentColor
        );
    }
}

void SampleTimeline::makeSelections(SelectMode mode)
{
    if (mode == Select_Replace)
    {
        activeParts.clear();
    }

    bool box = sqrt(sq(selectEnd.x - selectStart.x) + sq(selectEnd.y - selectStart.y)) >= UIScale::minBoxSelectSize();

    if (box)
    {
        i32 minX = selectStart.x < selectEnd.x ? selectStart.x : selectEnd.x;
        i32 minY = selectStart.y < selectEnd.y ? selectStart.y : selectEnd.y;
        i32 maxX = selectStart.x > selectEnd.x ? selectStart.x : selectEnd.x;
        i32 maxY = selectStart.y > selectEnd.y ? selectStart.y : selectEnd.y;

        minX -= position().x;
        minY -= position().y;
        maxX -= position().x;
        maxY -= position().y;

        for (const auto& [ id, part ] : currentSample().parts())
        {
            Point screenPos = screenPosition(part);

            if (screenPos.x >= minX && screenPos.y >= minY && screenPos.x < maxX && screenPos.y < maxY)
            {
                if (mode == Select_Remove)
                {
                    activeParts.erase(id);
                }
                else
                {
                    activeParts.insert(id);
                }
            }
        }
    }
    else
    {
        optional<SamplePartID> id = partUnderCursor(selectEnd);

        if (!id)
        {
            onSeek((panIndex * millisecondsPerColumn()) + ((selectEnd.x - (position().x + UIScale::timelineLeftHeaderWidth())) * timelineMillisecondsPerPixel));
        }
        else
        {
            if (mode == Select_Remove)
            {
                activeParts.erase(*id);
            }
            else
            {
                activeParts.insert(*id);
            }
        }
    }

    onSelect(activeParts);
}

void SampleTimeline::add(const Point& pointer)
{
    optional<tuple<u32, u32>> position = positionUnderCursor(pointer);

    if (!position)
    {
        return;
    }

    u32 start;
    u32 index;

    tie(start, index) = *position;

    SamplePartID id;

    edit([start, index, &id](Sample& sample) -> void {
        SamplePart part(sample.add());
        part.setStart(start);
        part.setIndex(index);

        sample.add(part);

        id = part.id();
    });

    activeParts = { id };
}

void SampleTimeline::remove(const Point& pointer)
{
    edit([this](Sample& sample) -> void {
        for (SamplePartID partID : activeParts)
        {
            sample.remove(partID);
        }

        activeParts.clear();
    });
}

void SampleTimeline::transform(const Point& pointer)
{
    switch (transformMode)
    {
    default :
    case Sample_Transform_None :
        return;
    case Sample_Transform_Translate :
    {
        Point delta = pointer - initialPointer;

        if (transformSubMode == Sample_Transform_Sub_X)
        {
            delta.y = 0;
        }
        else if (transformSubMode == Sample_Transform_Sub_Y)
        {
            delta.x = 0;
        }

        translate(delta.x * timelineMillisecondsPerPixel, -delta.y / UIScale::timelineRowHeight());
        break;
    }
    }
}

void SampleTimeline::translate(i64 startDelta, i64 indexDelta)
{
    edit([this, startDelta, indexDelta](Sample& sample) -> void {
        for (SamplePartID partID : activeParts)
        {
            auto it = sample.parts().find(partID);

            if (it == sample.parts().end())
            {
                continue;
            }

            SamplePart part = it->second;

            const SamplePart& initial = initialParts.at(partID);

            i64 start = max<i64>(initial.start() + startDelta, 0);
            i64 index = max<i64>(static_cast<i64>(initial.index()) + indexDelta, 0);

            part.setStart(start);
            part.setIndex(index);

            sample.add(part);
        }
    });
}

void SampleTimeline::startTransform(SampleTransformMode mode, const Point& pointer)
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    auto it = entity->samples().find(name);

    if (it == entity->samples().end())
    {
        return;
    }

    const Sample& sample = it->second;

    transformMode = mode;
    transformSubMode = Sample_Transform_Sub_None;
    initialPointer = pointer;

    for (SamplePartID partID : activeParts)
    {
        auto it = sample.parts().find(partID);

        if (it == sample.parts().end())
        {
            continue;
        }

        initialParts.insert({ it->first, it->second });
    }
}

void SampleTimeline::endTransform()
{
    initialParts.clear();
    transformMode = Sample_Transform_None;
    transformSubMode = Sample_Transform_Sub_None;
}

void SampleTimeline::cancelTransform()
{
    translate(0, 0);
    endTransform();
}

void SampleTimeline::edit(const function<void(Sample&)>& behavior) const
{
    context()->controller()->edit([this, behavior]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        auto it = entity->samples().find(name);

        if (it == entity->samples().end())
        {
            return;
        }

        Sample sample = it->second;

        behavior(sample);

        entity->addSample(name, sample);
    });
}

optional<tuple<u32, u32>> SampleTimeline::positionUnderCursor(const Point& pointer) const
{
    Point local = pointer - (position() + Point(UIScale::timelineLeftHeaderWidth(), UIScale::timelineTopHeaderHeight()));
    i32 height = size().h - UIScale::timelineTopHeaderHeight();

    if (local.x < 0 || local.y < 0)
    {
        return {};
    }

    u32 start = (panIndex * millisecondsPerColumn()) + (local.x * timelineMillisecondsPerPixel);
    u32 index = scrollIndex + ((height - local.y) / UIScale::timelineRowHeight());

    return tuple<u32, u32>(start, index);
}

optional<SamplePartID> SampleTimeline::partUnderCursor(const Point& pointer) const
{
    for (const auto& [ id, part ] : currentSample().parts())
    {
        if (contains(screenPosition(part), Size(part.length() / timelineMillisecondsPerPixel, part.volume() * UIScale::timelineRowHeight()), pointer))
        {
            return id;
        }
    }

    return {};
}

Point SampleTimeline::screenPosition(const SamplePart& note) const
{
    i32 height = size().h - UIScale::timelineTopHeaderHeight();

    i32 x = note.start() / timelineMillisecondsPerPixel;
    x -= panIndex * UIScale::timelineColumnWidth();

    i32 y = (note.index() + 1) * UIScale::timelineRowHeight();
    y += UIScale::timelineRowHeight() / 2; // To line up with the lines
    y -= scrollIndex * UIScale::timelineRowHeight();

    y = height - y;

    y += (note.volume() * UIScale::timelineRowHeight()) / 2;

    return Point(UIScale::timelineLeftHeaderWidth(), UIScale::timelineTopHeaderHeight()) + Point(x, y);
}

const Sample& SampleTimeline::currentSample() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const Sample empty;
        return empty;
    }

    auto it = entity->samples().find(name);

    if (it == entity->samples().end())
    {
        static const Sample empty;
        return empty;
    }

    return it->second;
}

const SamplePart& SampleTimeline::currentPart() const
{
    const Sample& sample = currentSample();

    if (activeParts.size() != 1)
    {
        static const SamplePart empty;
        return empty;
    }

    auto it = sample.parts().find(*activeParts.begin());

    if (it == sample.parts().end())
    {
        static const SamplePart empty;
        return empty;
    }

    return it->second;
}

void SampleTimeline::selectAll(const Input& input)
{
    const Sample& sample = currentSample();

    for (const auto& [ id, part ] : sample.parts())
    {
        activeParts.insert(id);
    }
}

void SampleTimeline::deselectAll(const Input& input)
{
    activeParts.clear();
}

void SampleTimeline::invertSelection(const Input& input)
{
    const Sample& sample = currentSample();

    set<SamplePartID> newActiveParts;

    for (const auto& [ id, part ] : sample.parts())
    {
        if (activeParts.contains(id))
        {
            continue;
        }

        newActiveParts.insert(id);
    }

    activeParts = newActiveParts;
}

void SampleTimeline::duplicate(const Input& input)
{
    const Sample& sample = currentSample();

    vector<SamplePart> parts;

    for (SamplePartID partID : activeParts)
    {
        auto it = sample.parts().find(partID);

        if (it == sample.parts().end())
        {
            continue;
        }

        parts.push_back(it->second);
    }

    activeParts.clear();

    edit([this, parts](Sample& sample) -> void {
        for (SamplePart part : parts)
        {
            part = SamplePart(sample.add(), part);
            sample.add(part);

            activeParts.insert(part.id());
        }
    });
}

void SampleTimeline::cut(const Input& input)
{
    copy(input);

    edit([this](Sample& sample) -> void {
        for (SamplePartID partID : activeParts)
        {
            sample.remove(partID);
        }

        activeParts.clear();
    });
}

void SampleTimeline::copy(const Input& input)
{
    const Sample& sample = currentSample();

    vector<SamplePart> parts;

    for (SamplePartID partID : activeParts)
    {
        auto it = sample.parts().find(partID);

        if (it == sample.parts().end())
        {
            continue;
        }

        parts.push_back(it->second);
    }

    context()->viewerMode()->entityMode()->sampleCopyBuffer().set(parts);
}

void SampleTimeline::paste(const Input& input)
{
    edit([this](Sample& sample) -> void {
        for (SamplePart part : context()->viewerMode()->entityMode()->sampleCopyBuffer().get())
        {
            part = SamplePart(sample.add(), part);
            sample.add(part);

            activeParts.insert(part.id());
        }
    });
}

void SampleTimeline::scrollUp(const Input& input)
{
    scrollIndex++;
}

void SampleTimeline::scrollDown(const Input& input)
{
    if (scrollIndex != 0)
    {
        scrollIndex--;
    }
}

void SampleTimeline::panLeft(const Input& input)
{
    if (panIndex != 0)
    {
        panIndex--;
    }
}

void SampleTimeline::panRight(const Input& input)
{
    panIndex++;
}

void SampleTimeline::addNew(const Input& input)
{
    add(pointer);
}

void SampleTimeline::removeSelected(const Input& input)
{
    remove(pointer);
}

void SampleTimeline::startSelect(const Input& input)
{
    selectStart = pointer;
    selectEnd = pointer;
    selecting = true;
}

void SampleTimeline::startPan(const Input& input)
{
    panStart = pointer;
    panStartScrollIndex = scrollIndex;
    panStartPanIndex = panIndex;
    panning = true;
}

void SampleTimeline::translate(const Input& input)
{
    startTransform(Sample_Transform_Translate, pointer);
}

void SampleTimeline::togglePlay(const Input& input)
{
    onPlayPause();
}

void SampleTimeline::moveSelect(const Input& input)
{
    selectEnd = pointer;
}

void SampleTimeline::endReplaceSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Replace);
}

void SampleTimeline::endAddSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Add);
}

void SampleTimeline::endRemoveSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Remove);
}

void SampleTimeline::pan(const Input& input)
{
    vec2 delta = (pointer - panStart).toVec2();

    f64 panDelta = (-delta.x / (1.0 / timelineMillisecondsPerPixel)) / millisecondsPerColumn();
    f64 scrollDelta = delta.y / UIScale::timelineRowHeight();

    panIndex = max(panStartPanIndex + panDelta, 0.0);
    scrollIndex = max(panStartScrollIndex + scrollDelta, 0.0);
}

void SampleTimeline::endPan(const Input& input)
{
    panning = false;
}

void SampleTimeline::toggleX(const Input& input)
{
    transformSubMode = transformSubMode != Sample_Transform_Sub_X
        ? Sample_Transform_Sub_X
        : Sample_Transform_Sub_None;
}

void SampleTimeline::toggleY(const Input& input)
{
    transformSubMode = transformSubMode != Sample_Transform_Sub_Y
        ? Sample_Transform_Sub_Y
        : Sample_Transform_Sub_None;
}

void SampleTimeline::useTool(const Input& input)
{
    transform(pointer);
}

void SampleTimeline::cancelTool(const Input& input)
{
    cancelTransform();
}

void SampleTimeline::endTool(const Input& input)
{
    endTransform();
}

i32 SampleTimeline::millisecondsPerColumn() const
{
    return UIScale::timelineColumnWidth() * timelineMillisecondsPerPixel;
}

SizeProperties SampleTimeline::sizeProperties() const
{
    return sizePropertiesFillAll;
}
