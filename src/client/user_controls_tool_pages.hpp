/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class Checkbox;
class LineEdit;
class UFloatEdit;
class Vec3Edit;

class UserControlTool : public InterfaceWidget, public TabContent
{
public:
    UserControlTool(
        InterfaceWidget* parent,
        ControlContext* ctx
    );

    void open() override;
    void close() override;

protected:
    ControlContext* ctx;

    virtual void reset() = 0;

    set<string> users;

    void onManageUsers();

    vector<string> usersSource();
    void onAddUser(const string& name);
    void onRemoveUser(const string& name);
};

class UserTetherTool : public UserControlTool
{
public:
    UserTetherTool(
        InterfaceWidget* parent,
        ControlContext* ctx
    );

private:
    void reset() override;

    Checkbox* enabled;
    UFloatEdit* distance;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class UserConfineTool : public UserControlTool
{
public:
    UserConfineTool(
        InterfaceWidget* parent,
        ControlContext* ctx
    );

private:
    void reset() override;

    Checkbox* enabled;
    Vec3Edit* position;
    UFloatEdit* distance;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class UserGotoTool : public UserControlTool
{
public:
    UserGotoTool(
        InterfaceWidget* parent,
        ControlContext* ctx
    );

private:
    void reset() override;

    Vec3Edit* position;
    UFloatEdit* radius;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class UserFadeTool : public UserControlTool
{
public:
    UserFadeTool(
        InterfaceWidget* parent,
        ControlContext* ctx
    );

private:
    void reset() override;

    Checkbox* enabled;

    void onDone();

    SizeProperties sizeProperties() const override;
};

class UserWaitTool : public UserControlTool
{
public:
    UserWaitTool(
        InterfaceWidget* parent,
        ControlContext* ctx
    );

private:
    void reset() override;

    Checkbox* enabled;

    void onDone();

    SizeProperties sizeProperties() const override;
};
