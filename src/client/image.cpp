/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "image.hpp"
#include "log.hpp"

#include <png.h>

Image::Image(u32 width, u32 height, u32 sampleSize, u8* pixels, u32 pitch)
    : _width(width)
    , _height(height)
    , _sampleSize(sampleSize)
    , _pixels(pixels)
    , _pitch(pitch == 0 ? width * sampleSize : pitch)
{

}

Image::~Image()
{
    delete[] _pixels;
}

size_t Image::size() const
{
    return _width * _height * _sampleSize;
}

Image* Image::copy() const
{
    auto pixels = new u8[size()];

    memcpy(pixels, _pixels, size());

    return new Image(_width, _height, _sampleSize, pixels);
}

u32 Image::width() const
{
    return _width;
}

u32 Image::height() const
{
    return _height;
}

u32 Image::sampleSize() const
{
    return _sampleSize;
}

u32 Image::pitch() const
{
    return _pitch;
}

const u8* Image::pixels() const
{
    return _pixels;
}

Image* loadPNG(const string& path)
{
    png_image png{};
    png.version = PNG_IMAGE_VERSION;

    int ok = png_image_begin_read_from_file(&png, path.c_str());

    if (!ok)
    {
        error("Failed to open PNG file '" + path + "'.");
        return nullptr;
    }

    png.format = PNG_FORMAT_BGRA;

    const u32 channelCount = 4;
    u32 width = png.width;
    u32 height = png.height;
    u32 sampleSize = PNG_IMAGE_PIXEL_COMPONENT_SIZE(png.format) * channelCount;
    u32 pitch = PNG_IMAGE_ROW_STRIDE(png);
    auto pixels = new u8[PNG_IMAGE_SIZE(png)];

    ok = png_image_finish_read(&png, nullptr, pixels, 0, nullptr);

    if (!ok)
    {
        error("Failed to read from PNG file '" + path + "'.");
        png_image_free(&png);
        return nullptr;
    }

    png_image_free(&png);

    return new Image(width, height, sampleSize, pixels, pitch);
}

Image* emptyImage(u32 sampleSize)
{
    u32 width = 32;
    u32 height = 32;
    u32 size = width * height * sampleSize;

    auto pixels = new u8[size];

    memset(pixels, 0, width * height * sampleSize);

    return new Image(width, height, sampleSize, pixels);
}
