/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "line_builder.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "vertex_color_encoding.hpp"

static optional<fvec3> intersectLineLine(
    const fvec3& posA,
    const fvec3& dirA,
    const fvec3& posB,
    const fvec3& dirB,
    f32 maxError
)
{
    f32 b = sq(dirA.cross(dirB).length());

    if (roughly<f32>(b, 0))
    {
        return {};
    }

    fvec3 posDelta = posB - posA;
    fvec3 crossProduct = dirA.cross(dirB);

    f32 t = fmat3(
        posDelta.x, posDelta.y, posDelta.z,
        dirB.x, dirB.y, dirB.z,
        crossProduct.x, crossProduct.y, crossProduct.z
    ).determinant() / b;
    f32 s = fmat3(
        posDelta.x, posDelta.y, posDelta.z,
        dirA.x, dirA.y, dirA.z,
        crossProduct.x, crossProduct.y, crossProduct.z
    ).determinant() / b;

    fvec3 closestA = posA + dirA * t;
    fvec3 closestB = posB + dirB * s;

    if (closestA.distance(closestB) > maxError)
    {
        return {};
    }

    return (closestA + closestB) / 2;
}

struct EdgeIntersection
{
    fvec3 position;
    f32 distance;
    u32 count;

    bool operator==(const EdgeIntersection& rhs) const
    {
        return position == rhs.position &&
            distance == rhs.distance &&
            count == rhs.count;
    }

    bool operator!=(const EdgeIntersection& rhs) const
    {
        return !(*this == rhs);
    }

    bool operator>(const EdgeIntersection& rhs) const
    {
        if (count > rhs.count)
        {
            return true;
        }
        else if (count < rhs.count)
        {
            return false;
        }
        else
        {
            if (distance > rhs.distance)
            {
                return true;
            }
            else if (distance < rhs.distance)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }

    bool operator<(const EdgeIntersection& rhs) const
    {
        if (count < rhs.count)
        {
            return true;
        }
        else if (count > rhs.count)
        {
            return false;
        }
        else
        {
            if (distance < rhs.distance)
            {
                return true;
            }
            else if (distance > rhs.distance)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }

    bool operator>=(const EdgeIntersection& rhs) const
    {
        return *this > rhs || *this == rhs;
    }

    bool operator<=(const EdgeIntersection& rhs) const
    {
        return *this < rhs || *this == rhs;
    }

    strong_ordering operator<=>(const EdgeIntersection& rhs) const
    {
        if (*this == rhs)
        {
            return strong_ordering::equal;
        }
        else if (*this > rhs)
        {
            return strong_ordering::greater;
        }
        else
        {
            return strong_ordering::less;
        }
    }
};

struct JoinableEdge
{
    fvec3 position;
    fvec3 direction;
    fvec3 normal;
};

// Assumes line transforms are facing +Y/forward
static optional<vector<LitColoredVertex>> findSeam(
    f32 maxDistance,
    const EmissiveColor& color,
    const vector<vector<JoinableEdge>>& edges,
    u32 primaryLineIndex,
    const vector<u32>& lineIndices
)
{
    vector<LitColoredVertex> vertices;
    vertices.reserve(edges.front().size());

    for (const JoinableEdge& primaryEdge : edges[primaryLineIndex])
    {
        vector<EdgeIntersection> intersections;
        intersections.reserve(edges.front().size());

        for (u32 secondaryLineIndex : lineIndices)
        {
            if (secondaryLineIndex == primaryLineIndex)
            {
                continue;
            }

            for (const JoinableEdge& secondaryEdge : edges[secondaryLineIndex])
            {
                optional<fvec3> position = intersectLineLine(
                    primaryEdge.position,
                    primaryEdge.direction,
                    secondaryEdge.position,
                    secondaryEdge.direction,
                    maxDistance
                );

                if (!position)
                {
                    continue;
                }

                f32 distance = primaryEdge.position.distance(*position);
                bool found = false;

                for (EdgeIntersection& intersection : intersections)
                {
                    if (position->distance(intersection.position) < maxDistance)
                    {
                        intersection.count++;
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    intersections.push_back({ *position, distance, 1 });
                }
            }
        }

        if (intersections.empty())
        {
            return {};
        }

        EdgeIntersection bestIntersection = intersections.front();

        for (size_t i = 1; i < intersections.size(); i++)
        {
            if (intersections[i] > bestIntersection)
            {
                bestIntersection = intersections[i];
            }
        }

        vertices.push_back({ bestIntersection.position, primaryEdge.normal, litColor(color) });
    }

    return vertices;
}

struct LineJoint
{
    LineJoint(const fvec3& position, u32 lineCount, u32 index)
        : position(position)
    {
        lineIndices.reserve(lineCount);
        lineIndices.push_back(index);
    }

    fvec3 position;
    vector<u32> lineIndices;
};

optional<BuiltLines> buildLines(u32 resolution, f32 radius, const EmissiveColor& color, const vector<BuildableLine>& lines)
{
    BuiltLines mesh;

    // Calculate edges of all lines
    vector<vector<JoinableEdge>> edges(lines.size(), vector<JoinableEdge>(resolution));

    for (u32 lineIndex = 0; lineIndex < lines.size(); lineIndex++)
    {
        const BuildableLine& line = lines[lineIndex];

        fvec3 position = (line.start + line.end) / 2;
        fvec3 direction = (line.end - line.start).normalize();

        fmat4 transform(position, fquaternion(direction, pickUpDirection(direction)), fvec3(1));

        for (u32 edgeIndex = 0; edgeIndex < resolution; edgeIndex++)
        {
            JoinableEdge& edge = edges[lineIndex][edgeIndex];

            fvec3 position = quaternion(forwardDir, (edgeIndex / static_cast<f32>(resolution)) * tau).rotate(rightDir * radius);
            fvec3 direction = forwardDir;

            edge.position = transform.applyToPosition(position);
            edge.direction = transform.applyToDirection(direction);
            edge.normal = transform.applyToDirection(position.normalize());
        }
    }

    // Calculate what joints are required and which lines are involved in each
    vector<LineJoint> joints;
    joints.reserve(lines.size() * 2);
    vector<pair<u32, u32>> lineJoints;
    lineJoints.reserve(lines.size());

    for (u32 lineIndex = 0; lineIndex < lines.size(); lineIndex++)
    {
        const BuildableLine& line = lines[lineIndex];

        u32 startJointIndex = numeric_limits<u32>::max();
        u32 endJointIndex = numeric_limits<u32>::max();

        for (u32 i = 0; i < joints.size(); i++)
        {
            if (roughly(joints[i].position, line.start))
            {
                startJointIndex = i;
                joints[i].lineIndices.push_back(lineIndex);
            }

            if (roughly(joints[i].position, line.end))
            {
                endJointIndex = i;
                joints[i].lineIndices.push_back(lineIndex);
            }

            if (startJointIndex != numeric_limits<u32>::max() &&
                endJointIndex != numeric_limits<u32>::max()
            )
            {
                break;
            }
        }

        if (startJointIndex == numeric_limits<u32>::max())
        {
            startJointIndex = joints.size();
            joints.push_back(LineJoint(line.start, lines.size(), lineIndex));
        }

        if (endJointIndex == numeric_limits<u32>::max())
        {
            endJointIndex = joints.size();
            joints.push_back(LineJoint(line.end, lines.size(), lineIndex));
        }

        lineJoints.push_back({ startJointIndex, endJointIndex });
    }

    // Get vertices and assemble faces
    f32 maxDistance = (radius / sin(pi / 2)) * sin(pi / resolution);

    for (u32 lineIndex = 0; lineIndex < lineJoints.size(); lineIndex++)
    {
        const auto& [ startJointIndex, endJointIndex ] = lineJoints[lineIndex];

        u32 indexOffset = mesh.vertices.size();

        optional<vector<LitColoredVertex>> seamVerticesA = findSeam(
            maxDistance,
            color,
            edges,
            lineIndex,
            joints[startJointIndex].lineIndices
        );

        if (!seamVerticesA)
        {
            return {};
        }

        mesh.vertices.insert(
            mesh.vertices.end(),
            seamVerticesA->begin(),
            seamVerticesA->end()
        );

        optional<vector<LitColoredVertex>> seamVerticesB = findSeam(
            maxDistance,
            color,
            edges,
            lineIndex,
            joints[endJointIndex].lineIndices
        );

        if (!seamVerticesB)
        {
            return {};
        }

        mesh.vertices.insert(
            mesh.vertices.end(),
            seamVerticesB->begin(),
            seamVerticesB->end()
        );

        for (u32 i = 0; i < resolution; i++)
        {
            u32 a = i;
            u32 b = loopIndex(i + 1, resolution);
            u32 c = resolution + i;
            u32 d = resolution + loopIndex(i + 1, resolution);

            mesh.indices.push_back(indexOffset + c);
            mesh.indices.push_back(indexOffset + a);
            mesh.indices.push_back(indexOffset + b);

            mesh.indices.push_back(indexOffset + c);
            mesh.indices.push_back(indexOffset + b);
            mesh.indices.push_back(indexOffset + d);
        }
    }

    return mesh;
}
