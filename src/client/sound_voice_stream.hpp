/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "sound_source.hpp"

class Sound;

class SoundVoiceStream final : public SoundSource
{
public:
    SoundVoiceStream(
        Sound* sound,
        const vec3& position,
        const quaternion& rotation,
        f64 volume,
        bool canBeSpatial
    );

    void update(const vec3& position, const quaternion& rotation, f64 volume, bool canBeSpatial);
    void push(const string& data);
    bool expired() const;

private:
    vector<f32> stream;
    chrono::milliseconds lastSeenTime;
    bool draining;
    f64 volume;

    variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> pull(f64 multiplier) override;
};
