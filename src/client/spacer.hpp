/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

class Spacer : public InterfaceWidget
{
public:
    Spacer(
        InterfaceWidget* parent,
        const function<i32()>& width,
        const function<i32()>& height,
        const optional<Color>& color = {}
    );

    void draw(const DrawContext& ctx) const override;

private:
    function<i32()> width;
    function<i32()> height;
    optional<Color> color;

    SizeProperties sizeProperties() const override;
};

#define MAKE_SPACER(_parent, _width, _height, ...) \
new Spacer(_parent, [&]() -> i32 { return _width; }, [&]() -> i32 { return _height; } __VA_OPT__(,) __VA_ARGS__);
