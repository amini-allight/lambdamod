/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "document_editor.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "column.hpp"
#include "advanced_text_editor.hpp"

DocumentEditor::DocumentEditor(
    InterfaceWidget* parent,
    const function<string()>& source,
    const function<void(const string&)>& onSave
)
    : InterfaceWidget(parent)
{
    auto column = new Column(this);

    if (g_config.advancedTextEditor)
    {
        editor = new AdvancedTextEditor(column, source, onSave, false);
    }
    else
    {
        editor = new BasicTextEditor(column, source, onSave, false);
    }
}

bool DocumentEditor::saved() const
{
    return editor->saved();
}

void DocumentEditor::set(const string& text)
{
    editor->set(text);
}

void DocumentEditor::notifyReload()
{
    editor->notifyReload();
}

void DocumentEditor::display(const string& message)
{
    editor->display(message);
}

SizeProperties DocumentEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
