/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_part_selection.hpp"
#include "edit_mode_context.hpp"

class FlatRender;

class InternalModeContext : public EditModeContext
{
public:
    InternalModeContext(ViewerModeContext* ctx);
    ~InternalModeContext() override;

    void step() override;

    void enter() override;
    void exit() override;

    void cancelTool() override;
    void endTool() override;

    const set<BodyPartSelectionID>& selectionIDs() const;
    set<BodyPartSelectionID> unselectionIDs() const;
    set<BodyPartSelection> selection() const;
    set<BodyPartSelection> selectionRootsOnly() const;
    BodyPartSelection selectionRootAtIndex(size_t index) const;
    vec3 selectionCenter() const override;
    void select(const BodyPartSelectionID& selection);
    void select(const vector<BodyPartSelectionID>& selection);
    void selectAll() override;
    void deselect(const BodyPartSelectionID& selection);
    void deselect(const vector<BodyPartSelectionID>& selection);
    void deselectAll() override;
    bool hasSelection() const override;
    void invertSelection() override;
    void selectLinked();

    void translate(const Point& point, bool soft = false) override;
    void rotate(const Point& point, bool soft = false) override;
    void scale(const Point& point, bool soft = false) override;

    vector<BodyPartSelection> bodyPartsUnderPoint(const Point& point) const;
    vector<BodyPartSelection> bodyPartsInRegion(const Point& start, const Point& end) const;

    Entity* activeEntity() const;

protected:
    set<BodyPartSelectionID> _selectionIDs;
    set<BodyPartID> _hiddenBodyPartIDs;
    EntityID activeEntityID;
    map<BodyPartID, RenderDecorationID> decorations;
    map<BodyPartID, RenderDecorationID> constraintDecorations;
    // Stores initial part transforms for use in subpart transformations
    vector<mat4> initialPartTransforms;

    bool gizmoShown() const override;
    bool gizmoSelectable(const Point& point) const override;

    void makeSelections(const Point& point, SelectMode mode) override;
    void makeSelections(const Point& start, const Point& end, SelectMode mode) override;

    size_t transformCount() const override;
    mat4 transformToolSpace() const override;

    vec3 regionalToGlobal(const vec3& position) const;
    vec3 globalToRegional(const vec3& position) const;

protected:
    friend class BodyEditor;

    void applyTransformToPart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;

private:
    void applyTransformToLineSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToPlaneSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToSolidSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToSymbolSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToCanvasSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToTextSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToTerrainSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToAreaSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
    void applyTransformToRopeSubpart(
        const BodyPartSelection& selection,
        const vec3& pivot,
        const mat4& initialPartTransform,
        const mat4& initialSubpartTransform,
        const mat4& currentSubpartTransform
    ) const;
};
