/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "sound_tools.hpp"
#include "sound_chunk.hpp"

template<int N>
class SoundStereoChunk_t
{
public:
    SoundStereoChunk_t()
    {

    }

    explicit SoundStereoChunk_t(const SoundChunk_t<N>& left, const SoundChunk_t<N>& right)
        : _channels{left, right}
    {

    }

    SoundStereoChunk_t operator+(const SoundStereoChunk_t& rhs) const
    {
        return SoundStereoChunk_t(left() + rhs.left(), right() + rhs.right());
    }

    SoundStereoChunk_t operator-(const SoundStereoChunk_t& rhs) const
    {
        return SoundStereoChunk_t(left() - rhs.left(), right() - rhs.right());
    }

    SoundStereoChunk_t operator*(f32 x) const
    {
        return SoundStereoChunk_t(left() * x, right() * x);
    }

    SoundStereoChunk_t operator/(f32 x) const
    {
        return SoundStereoChunk_t(left() / x, right() / x);
    }

    SoundStereoChunk_t& operator+=(const SoundStereoChunk_t& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    SoundStereoChunk_t& operator-=(const SoundStereoChunk_t& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    SoundStereoChunk_t& operator*=(f32 x)
    {
        *this = *this * x;

        return *this;
    }

    SoundStereoChunk_t& operator/=(f32 x)
    {
        *this = *this / x;

        return *this;
    }

    SoundChunk toMono() const
    {
        return (left() + right()) / soundChannels;
    }

    constexpr static size_t length()
    {
        return N;
    }

    const SoundChunk_t<N>* channels() const
    {
        return _channels;
    }

    const SoundChunk_t<N>& channel(size_t index) const
    {
        return _channels[index];
    }

    SoundChunk_t<N>* channels()
    {
        return _channels;
    }

    SoundChunk_t<N>& channel(size_t index)
    {
        return _channels[index];
    }

    const SoundChunk_t<N>& left() const
    {
        return _channels[0];
    }

    SoundChunk_t<N>& left()
    {
        return _channels[0];
    }

    const SoundChunk_t<N>& right() const
    {
        return _channels[1];
    }

    SoundChunk_t<N>& right()
    {
        return _channels[1];
    }

    size_t channelCount() const
    {
        return soundChannels;
    }

    vector<i16> toRaw() const
    {
        vector<i16> discrete(channelCount() * length());

        for (size_t i = 0; i < channelCount(); i++)
        {
            for (size_t j = 0; j < length(); j++)
            {
                discrete[j * channelCount() + i] = convertSampleToDiscrete(channel(i).sample(j));
            }
        }

        return discrete;
    }

    f32 maxAmplitude() const
    {
        return max(left().maxAmplitude(), right().maxAmplitude());
    }

    void normalize()
    {
        f32 max = maxAmplitude();

        for (size_t i = 0; i < channelCount(); i++)
        {
            for (size_t j = 0; j < length(); j++)
            {
                _channels[i].sample(j) /= max;
            }
        }
    }

    void expose(f32 exposure)
    {
        for (SoundChunk_t<N>& channel : _channels)
        {
            channel.expose(exposure);
        }
    }

    void counterExpose(f32 exposure)
    {
        for (SoundChunk_t<N>& channel : _channels)
        {
            channel.counterExpose(exposure);
        }
    }

private:
    SoundChunk_t<N> _channels[soundChannels];
};

typedef SoundStereoChunk_t<soundChunkSamplesPerChannel> SoundStereoChunk;
typedef SoundStereoChunk_t<soundChunkSamplesPerChannel + soundChunkOverlap * 2> SoundOverlappedStereoChunk;

SoundStereoChunk removeOverlap(const SoundOverlappedStereoChunk& chunk);
