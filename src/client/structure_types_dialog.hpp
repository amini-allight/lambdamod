/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "dialog.hpp"
#include "list_view.hpp"
#include "structure.hpp"

class StructureTypesDialog : public Dialog
{
public:
    StructureTypesDialog(
        InterfaceView* view,
        const function<StructureTypeID()>& nextTypeIDSource,
        const function<void(StructureTypeID)>& onNextTypeID,
        const function<vector<StructureType>()>& typesSource,
        const function<void(const vector<StructureType>&)>& onTypes
    );

    void step() override;

private:
    function<StructureTypeID()> nextTypeIDSource;
    function<void(StructureTypeID)> onNextTypeID;
    function<vector<StructureType>()> typesSource;
    function<void(const vector<StructureType>&)> onTypes;
    ListView* listView;
    optional<vector<StructureType>> lastStructureTypes;

    bool needsWidgetUpdate() const;
    void updateWidgets();

    void addChildren();
    void addStructureTypeChildren(size_t index, const StructureType& type);
    void addStructureTypeNewPrompt();

    void addStructureType();
    void removeStructureType(size_t index);

    f64 thicknessSource(size_t index);
    void onThickness(size_t index, f64 thickness);
};
