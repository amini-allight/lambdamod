/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_post_process_effect.hpp"
#include "render_tools.hpp"

RenderFlatPostProcessEffect::RenderFlatPostProcessEffect(
    const RenderContext* ctx,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
)
    : RenderPostProcessEffect(ctx, pipelineLayout, pipeline)
{

}

RenderFlatPostProcessEffect::~RenderFlatPostProcessEffect()
{

}

void RenderFlatPostProcessEffect::work(const FlatSwapchainElement* swapchainElement, f32 timer) const
{
    RenderPostProcessEffect::work(swapchainElement, timer);

    VkViewport viewport = {
        0, static_cast<f32>(swapchainElement->height()),
        static_cast<f32>(swapchainElement->width()), -static_cast<f32>(swapchainElement->height()),
        0, 1
    };

    vkCmdSetViewport(swapchainElement->commandBuffer(), 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { static_cast<u32>(swapchainElement->width()), static_cast<u32>(swapchainElement->height()) }
    };

    vkCmdSetScissor(swapchainElement->commandBuffer(), 0, 1, &scissor);

    vkCmdBindPipeline(swapchainElement->commandBuffer(), VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdBindDescriptorSets(
        swapchainElement->commandBuffer(),
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &components.at(swapchainElement->imageIndex())->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(swapchainElement->commandBuffer(), 6, 1, 0, 0);
}

void RenderFlatPostProcessEffect::refresh(const vector<FlatSwapchainElement*>& swapchainElements)
{
    if (components.empty())
    {
        return;
    }

    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        setDescriptorSet(
            components.at(i)->descriptorSet,
            components.at(i)->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->depthImageView()
        );
    }
}

void RenderFlatPostProcessEffect::createComponents(
    const vector<FlatSwapchainElement*>& swapchainElements,
    VkDescriptorSetLayout descriptorSetLayout
)
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        auto component = new RenderPostProcessEffectComponent(ctx, descriptorSetLayout, createUniformBuffer());

        setDescriptorSet(
            component->descriptorSet,
            component->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->depthImageView()
        );

        components.push_back(component);
    }
}
