/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "console_command_input.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "console_input.hpp"

ConsoleCommandInput::ConsoleCommandInput(InterfaceWidget* parent, const function<void(const string&)>& onReturn)
    : LineEdit(
        parent,
        nullptr,
        bind(&ConsoleCommandInput::onDone, this, placeholders::_1),
        true
    )
    , onReturn(onReturn)
{
    START_WIDGET_SCOPE("console-command-input")
        WIDGET_SLOT("previous-history", previousHistory)
        WIDGET_SLOT("next-history", nextHistory)
    END_SCOPE

    setPlaceholder(localize("console-command-input-switch-to-chat-mode"));
}

void ConsoleCommandInput::push(const string& history)
{
    this->history.push_back(history);
    historyIndex = 0;
}

bool ConsoleCommandInput::shouldDraw() const
{
    return LineEdit::shouldDraw() && !dynamic_cast<ConsoleInput*>(parent())->chatMode();
}

void ConsoleCommandInput::forceFocus()
{
    focus();
}

void ConsoleCommandInput::previousHistory(const Input& input)
{
    if (history.empty())
    {
        return;
    }

    set(history[history.size() - (historyIndex + 1)]);

    if (historyIndex + 1 >= history.size())
    {
        return;
    }

    historyIndex++;
}

void ConsoleCommandInput::nextHistory(const Input& input)
{
    if (history.empty())
    {
        return;
    }

    if (historyIndex == 0)
    {
        clear();
        return;
    }

    historyIndex--;

    set(history[history.size() - (historyIndex + 1)]);
}

void ConsoleCommandInput::onDone(const string& text)
{
    onReturn(text);
    clear();
}
