/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "spacer.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

Spacer::Spacer(
    InterfaceWidget* parent,
    const function<i32()>& width,
    const function<i32()>& height,
    const optional<Color>& color
)
    : InterfaceWidget(parent)
    , width(width)
    , height(height)
    , color(color)
{

}

void Spacer::draw(const DrawContext& ctx) const
{
    if (color)
    {
        drawSolid(ctx, this, *color);
    }

    InterfaceWidget::draw(ctx);
}

SizeProperties Spacer::sizeProperties() const
{
    return {
        static_cast<f64>(width()), static_cast<f64>(height()),
        width() == 0 ? Scaling_Fill : Scaling_Fixed, height() == 0 ? Scaling_Fill : Scaling_Fixed
    };
}
