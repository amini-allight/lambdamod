/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_context.hpp"
#include "body_render_data.hpp"
#include "render_entity_body_material.hpp"

// This is a container for entity rendering resources that are frame independent*, mesh data and textures
// * = these are "frame independent" because when they're changed this entire structure is thrown into a ToDelete queue
class RenderEntityBody
{
public:
    RenderEntityBody(
        const RenderContext* ctx,
        const RenderPipelineStore* pipelineStore,
        const vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>>& materials
    );
    RenderEntityBody(const RenderEntityBody& rhs) = delete;
    RenderEntityBody(RenderEntityBody&& rhs) = delete;
    ~RenderEntityBody();

    RenderEntityBody& operator=(const RenderEntityBody& rhs) = delete;
    RenderEntityBody& operator=(RenderEntityBody&& rhs) = delete;

    bool supports(const vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>>& materials) const;
    void fill(const vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>>& materials);
    void setupTextureLayouts(VkCommandBuffer commandBuffer);

    vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>> materials;
};
