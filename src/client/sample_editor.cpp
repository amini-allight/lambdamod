/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "row.hpp"
#include "column.hpp"
#include "sample_timeline.hpp"
#include "sample_editor_side_panel.hpp"
#include "timeline_controls.hpp"
#include "icon_button.hpp"
#include "dynamic_icon_button.hpp"
#include "side_panel.hpp"
#include "label.hpp"
#include "uint_edit.hpp"
#include "float_edit.hpp"
#include "color_picker.hpp"
#include "spacer.hpp"

SampleEditor::SampleEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
    , previewPlayback(context()->viewerMode(), id)
{
    START_WIDGET_SCOPE("sample-editor")
        WIDGET_SLOT("toggle-side-panel", toggleSidePanel)
    END_SCOPE

    column = new Column(this);

    timeline = new SampleTimeline(
        column,
        id,
        bind(&SampleEditor::playPositionSource, this),
        bind(&SampleEditor::onPlayPause, this),
        bind(&SampleEditor::onSeek, this, placeholders::_1),
        bind(&SampleEditor::onSelect, this, placeholders::_1)
    );

    new TimelineControls(
        column,
        bind(&SampleEditor::playingSource, this),
        bind(&SampleEditor::onGoToStart, this),
        bind(&SampleEditor::onPlayPause, this),
        bind(&SampleEditor::onStop, this),
        bind(&SampleEditor::onGoToEnd, this)
    );

    sidePanel = new SampleEditorSidePanel(this);
}

void SampleEditor::open(const string& name)
{
    this->name = name;
    timeline->open(name);
    previewPlayback.open(name);
}

void SampleEditor::move(const Point& position)
{
    this->position(position);

    column->move(position);

    sidePanel->move(position + Point(size().w - UIScale::sidePanelWidth(), 0));
}

void SampleEditor::resize(const Size& size)
{
    this->size(size);

    column->resize(size);

    sidePanel->resize(Size(UIScale::sidePanelWidth(), size.h));
}

u32 SampleEditor::playPositionSource()
{
    return previewPlayback.position();
}

void SampleEditor::onSeek(u32 position)
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.seek(min(position, currentSample().length() - 1));
}

void SampleEditor::onSelect(const set<SamplePartID>& parts)
{
    activeParts = parts;
}

bool SampleEditor::playingSource()
{
    return previewPlayback.playing();
}

void SampleEditor::onGoToStart()
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.seek(0);
}

void SampleEditor::onStop()
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.pause();
    previewPlayback.seek(0);
}

void SampleEditor::onPlayPause()
{
    if (name.empty())
    {
        return;
    }

    if (previewPlayback.playing())
    {
        previewPlayback.pause();
    }
    else
    {
        previewPlayback.play();
    }
}

void SampleEditor::onGoToEnd()
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.seek(currentSample().length() - 1);
}

void SampleEditor::edit(const function<void(Sample&)>& behavior) const
{
    context()->controller()->edit([this, behavior]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        auto it = entity->samples().find(name);

        if (it == entity->samples().end())
        {
            return;
        }

        Sample sample = it->second;

        behavior(sample);

        entity->addSample(name, sample);
    });
}

const Sample& SampleEditor::currentSample() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const Sample empty;
        return empty;
    }

    auto it = entity->samples().find(name);

    if (it == entity->samples().end())
    {
        static const Sample empty;
        return empty;
    }

    return it->second;
}

const SamplePart& SampleEditor::currentPart() const
{
    const Sample& sample = currentSample();

    if (activeParts.size() != 1)
    {
        static const SamplePart empty;
        return empty;
    }

    auto it = sample.parts().find(*activeParts.begin());

    if (it == sample.parts().end())
    {
        static const SamplePart empty;
        return empty;
    }

    return it->second;
}

void SampleEditor::toggleSidePanel(const Input& input)
{
    sidePanel->toggle();
}

SizeProperties SampleEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
