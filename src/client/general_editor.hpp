/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

#include "dynamic_label.hpp"

class GeneralEditor : public InterfaceWidget, public TabContent
{
public:
    GeneralEditor(InterfaceWidget* parent, EntityID id);

private:
    EntityID id;
    bool localSpace;

    string idSource();

    string nameSource();
    void onName(const string& name);

    vector<string> parentOptionsSource();
    size_t parentSource();
    void onParent(size_t index);

    vector<string> parentPartOptionsSource();
    size_t parentPartSource();
    void onParentPart(size_t index);

    vector<string> parentSubpartOptionsSource();
    size_t parentSubpartSource();
    void onParentSubpart(size_t index);

    string playPauseTextSource();
    void onPlayPause();

    string storeResetTextSource();
    void onStoreReset();

    size_t spaceSource();
    void onSpace(size_t index);

    vec3 positionSource();
    void onPosition(const vec3& position);

    quaternion rotationSource();
    void onRotation(const quaternion& rotation);

    vec3 scaleSource();
    void onScale(const vec3& scale);

    vec3 linearVelocitySource();
    void onLinearVelocity(const vec3& linV);

    vec3 angularVelocitySource();
    void onAngularVelocity(const vec3& angV);

    vector<string> ownerOptionsSource();
    size_t ownerSource();
    void onOwner(size_t index);

    bool sharedSource();
    void onShared(bool state);

    string attachedUserSource();

    string selectingUserSource();

    f64 massSource();
    void onMass(f64 mass);

    string volumeSource();

    string densitySource();

    f64 dragSource();
    void onDrag(f64 drag);

    f64 buoyancySource();
    void onBuoyancy(f64 buoyancy);

    f64 liftSource();
    void onLift(f64 lift);

    f64 magnetismSource();
    void onMagnetism(f64 magnetism);

    f64 frictionSource();
    void onFriction(f64 friction);

    f64 restitutionSource();
    void onRestitution(f64 restitution);

    bool hostSource();
    void onHost(bool state);

    bool shownSource();
    void onShown(bool state);

    bool physicalSource();
    void onPhysical(bool state);

    bool visibleSource();
    void onVisible(bool state);

    bool audibleSource();
    void onAudible(bool state);

    f64 fieldOfViewSource();
    void onFieldOfView(f64 fov);

    bool mouseLockedSource();
    void onMouseLocked(bool state);

    f64 voiceVolumeSource();
    void onVoiceVolume(f64 volume);

    vec3 tintColorSource();
    void onTintColor(const vec3& color);

    f64 tintStrengthSource();
    void onTintStrength(f64 strength);

    void undo(const Input& input);
    void redo(const Input& input);

    SizeProperties sizeProperties() const override;
};
