/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_post_process_effect.hpp"
#include "render_flat_pipeline_store.hpp"

class RenderFlatPostProcessEffect : public RenderPostProcessEffect
{
public:
    RenderFlatPostProcessEffect(
        const RenderContext* ctx,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    );
    ~RenderFlatPostProcessEffect();

    virtual void work(const FlatSwapchainElement* swapchainElement, f32 timer) const;
    virtual void refresh(const vector<FlatSwapchainElement*>& swapchainElements);

protected:
    void createComponents(
        const vector<FlatSwapchainElement*>& swapchainElements,
        VkDescriptorSetLayout descriptorSetLayout
    );
};
