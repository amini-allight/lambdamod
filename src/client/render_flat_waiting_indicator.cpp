/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_waiting_indicator.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "interface_constants.hpp"
#include "render_tools.hpp"

static const string meshFilePath = meshPath + "/waiting-indicator" + meshExt;

RenderFlatWaitingIndicator::RenderFlatWaitingIndicator(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    u32 width,
    u32 height,
    f64 angle
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->waitingIndicator.pipelineLayout, pipelineStore->waitingIndicator.pipeline)
    , width(width)
    , height(height)
{
    transform = { vec3(), quaternion(upDir, -angle), vec3(1) };

    string mesh = getFile(meshFilePath);

    vertexCount = mesh.size() / sizeof(fvec3);
    vertices = createVertexBuffer(vertexCount * sizeof(fvec3));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, mesh.data(), mesh.size());

    createComponents(swapchainElements);
}

RenderFlatWaitingIndicator::~RenderFlatWaitingIndicator()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderFlatWaitingIndicator::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    // Do not use RenderFlatDecoration::work because it assumes the decoration is in world space and this one is stuck to the screen
    fmat4 model = transform;
    memcpy(components.at(swapchainElement->imageIndex())->uniformMapping->data, &model, sizeof(fmat4));

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderFlatWaitingIndicator::createUniformBuffer() const
{
    VmaBuffer uniform = RenderDecoration::createUniformBuffer(sizeof(fmat4) + sizeof(fvec4) * 4 + sizeof(f32));

    VmaMapping<fvec4> mapping(ctx->allocator, uniform);

    mapping.data[4] = theme.accentGradientXStartColor.toVec4();
    mapping.data[5] = theme.accentGradientXEndColor.toVec4();
    mapping.data[6] = theme.accentGradientYStartColor.toVec4();
    mapping.data[7] = theme.accentGradientYEndColor.toVec4();

    reinterpret_cast<f32*>(mapping.data)[32] = static_cast<f32>(width) / static_cast<f32>(height);

    return uniform;
}
