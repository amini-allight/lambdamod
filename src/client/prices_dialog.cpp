/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "prices_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "list_view.hpp"
#include "line_edit.hpp"
#include "uint_edit.hpp"
#include "label.hpp"
#include "close_button.hpp"
#include "text_button.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(256, 384) * uiScale();
}

PricesDialog::PricesDialog(InterfaceView* view, map<string, u64>* prices)
    : Dialog(view, dialogSize())
    , prices(prices)
{
    auto column = new Column(this);

    new Label(
        column,
        localize("prices-dialog-prices")
    );

    listView = new ListView(
        column,
        [](size_t index) -> void {},
        theme.altBackgroundColor
    );

    new EditorEntry(column, "prices-dialog-name", [this](InterfaceWidget* parent) -> void {
        name = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        name->setPlaceholder(localize("prices-dialog-name"));
    });

    new EditorEntry(column, "prices-dialog-price", [this](InterfaceWidget* parent) -> void {
        price = new UIntEdit(
            parent,
            nullptr,
            [](u64 value) -> void {}
        );
        price->setPlaceholder(localize("prices-dialog-price"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("prices-dialog-add"),
            bind(&PricesDialog::onAdd, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());

    updateListView();
}

void PricesDialog::onAdd()
{
    prices->insert({
        name->content(),
        price->value()
    });

    updateListView();

    name->clear();
    price->clear();
}

void PricesDialog::onRemove(const string& name)
{
    prices->erase(name);

    updateListView();
}

void PricesDialog::updateListView()
{
    listView->clearChildren();

    for (const auto& [ name, price ] : *prices)
    {
        auto row = new Row(listView);

        new Label(row, name);
        new Label(row, to_string(price));
        new CloseButton(
            row,
            bind(&PricesDialog::onRemove, this, name)
        );
    }

    listView->update();
}
