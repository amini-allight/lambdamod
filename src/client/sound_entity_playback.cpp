/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_entity_playback.hpp"
#include "sound_tools.hpp"
#include "sound_constants.hpp"
#include "tools.hpp"
#include "entity.hpp"
#include "resample.hpp"

static constexpr f64 minSeekLength = (soundChunkSamplesPerChannel * 2) / static_cast<f64>(soundFrequency); // s

SoundEntityPlayback::SoundEntityPlayback(Sound* sound, const Entity* entity, SamplePlaybackID id)
    : SoundSource(
        sound,
        entity->globalPosition(),
        entity->globalRotation(),
        entity->samplePlaybacks().at(id).directional,
        entity->samplePlaybacks().at(id).angle
    )
    , audible(entity->audible())
    , id(id)
    , playing(entity->samplePlaybacks().at(id).playing)
    , loop(false)
    , volume(1)
    , readHead(0)
    , markedForRemoval(false)
{
    update(entity);
}

void SoundEntityPlayback::update(const Entity* entity)
{
    const SamplePlayback& playback = entity->samplePlaybacks().at(id);
    const Sample& sample = entity->samples().at(playback.name);

    audible = entity->audible();

    playing = playback.playing;
    loop = playback.loop;
    volume = playback.volume;

    if (sample != lastSample)
    {
        data = sample.toSound();
        lastSample = sample;
    }

    f64 soundElapsed = readHead / static_cast<f64>(soundFrequency);
    f64 entityElapsed = playback.elapsed / 1000.0;

    if (abs(entityElapsed - soundElapsed) > minSeekLength)
    {
        readHead = entityElapsed * soundFrequency;
    }

    const BodyPart* part = entity->body().get(Body_Anchor_Generic, playback.anchorName);

    vec3 position = part ? part->regionalPosition() : vec3();
    quaternion rotation = part ? part->regionalRotation() : quaternion();

    position = entity->globalTransform().applyToPosition(position);
    rotation = entity->globalTransform().applyToRotation(rotation);
}

void SoundEntityPlayback::markForRemoval()
{
    markedForRemoval = true;
}

variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> SoundEntityPlayback::pull(f64 multiplier)
{
    if (!audible || !playing || markedForRemoval)
    {
        return SoundOverlappedChunk();
    }

    auto [ chunk, advance ] = readSampleSource(data.samples, readHead, multiplier, loop);

    if (advance > 0 || abs(advance) >= readHead)
    {
        readHead += advance;
    }
    else
    {
        readHead = data.samples.size() - (abs(advance) - readHead);
    }

    return chunk;
}
