/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "lightning_builder.hpp"
#include "tools.hpp"

static constexpr fvec2 flowDir(0, 1);
static constexpr f32 coneAngle = radians(60);

static fvec4 toVec4(const fvec2& point)
{
    return fvec4(point.x, 0, point.y, 0);
}

vector<fvec4> buildLightning(mt19937_64& engine, size_t pointCount, f32 lowerHeight, f32 upperHeight, f32 thickness)
{
    assert(pointCount > 1);

    f32 halfThickness = thickness / 2;

    uniform_real_distribution<f32> angleDistribution(-coneAngle / 2, +coneAngle / 2);
    uniform_real_distribution<f32> thicknessDistribution(0.5, 1.5);

    vector<fvec2> points;
    points.reserve(pointCount);

    fvec2 point(0, -lowerHeight);
    points.push_back(point);

    for (size_t i = 1; i < pointCount; i++)
    {
        f32 remainingDistance = upperHeight - point.y;
        size_t remainingPoints = pointCount - i;

        f32 angle = angleDistribution(engine);

        fvec2 direction = rotate(flowDir, angle);
        f32 distance = remainingDistance / remainingPoints;

        point += direction * distance;

        points.push_back(point);
    }

    vector<fvec4> uniqueVertices;
    uniqueVertices.reserve(pointCount * 2);

    for (size_t i = 0; i < pointCount; i++)
    {
        const fvec2& point = points[i];

        if (i == 0)
        {
            fvec2 dir = (points[i + 1] - point).normalize();
            fvec2 sideDir = rotate(dir, static_cast<f32>(radians(90)));

            uniqueVertices.push_back(toVec4(point + -sideDir * halfThickness));
            uniqueVertices.push_back(toVec4(point + +sideDir * halfThickness));
        }
        else if (i + 1 == pointCount)
        {
            fvec2 dir = (point - points[i - 1]).normalize();
            fvec2 sideDir = rotate(dir, static_cast<f32>(radians(90)));

            uniqueVertices.push_back(toVec4(point + -sideDir * halfThickness));
            uniqueVertices.push_back(toVec4(point + +sideDir * halfThickness));
        }
        else
        {
            fvec2 inDir = (point - points[i - 1]).normalize();
            fvec2 outDir = (points[i + 1] - point).normalize();
            fvec2 sideDir;
            f32 thicknessFactor = thicknessDistribution(engine);

            if (roughly(abs(inDir.dot(outDir)), 0.0f))
            {
                sideDir = rotate(inDir, static_cast<f32>(radians(90)));
            }
            else
            {
                sideDir = (inDir - outDir).normalize();
            }

            f32 angle = pi - angleBetween(inDir, outDir);

            f32 localThicknessA = halfThickness / sin(angle / 2);
            f32 localThicknessB = halfThickness / sin((tau - angle) / 2);

            uniqueVertices.push_back(toVec4(point + -sideDir * localThicknessA * thicknessFactor));
            uniqueVertices.push_back(toVec4(point + +sideDir * localThicknessB * thicknessFactor));

            if (handedAngleBetween(inDir, outDir) > 0)
            {
                swap(uniqueVertices[uniqueVertices.size() - 1], uniqueVertices[uniqueVertices.size() - 2]);
            }
        }
    }

    vector<fvec4> vertices;
    vertices.reserve((pointCount - 1) * 6);

    for (size_t i = 0; i < pointCount - 1; i++)
    {
        const fvec4& a = uniqueVertices[i * 2 + 0];
        const fvec4& b = uniqueVertices[i * 2 + 1];
        const fvec4& c = uniqueVertices[(i + 1) * 2 + 0];
        const fvec4& d = uniqueVertices[(i + 1) * 2 + 1];

        vertices.push_back(a);
        vertices.push_back(b);
        vertices.push_back(d);

        vertices.push_back(a);
        vertices.push_back(c);
        vertices.push_back(d);
    }

    return vertices;
}
