/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "startup_splash.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "constants.hpp"
#include "control_context.hpp"
#include "vr_panel_input_scope.hpp"

#ifdef LMOD_VR
static constexpr chrono::seconds minDrawTime(1);
#endif

StartupSplash::StartupSplash(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
    , _hide(false)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        START_VR_PANEL_SCOPE(context()->input()->getScope("menu"), "startup-splash")
            WIDGET_SLOT("interact", dismiss)
        END_SCOPE
    }
    else
    {
        START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("menu"), "startup-splash")
            WIDGET_SLOT("interact", dismiss)
        END_SCOPE
    }
#else
    START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("menu"), "startup-splash")
        WIDGET_SLOT("interact", dismiss)
    END_SCOPE
#endif
}

void StartupSplash::draw(const DrawContext& ctx) const
{
    if (!firstDrawTime)
    {
        context()->controller()->sound()->addOneshot(Sound_Oneshot_Startup);
        firstDrawTime = currentTime();
    }

    Color fg = theme.panelForegroundColor;
    Color bg = theme.worldBackgroundColor;

    fg.a *= context()->splash()->splashContentOpacity();
    bg.a *= context()->splash()->splashOpacity();

    drawSolid(
        ctx,
        this,
        bg
    );

    i32 iconHeight = ctx.size.h / 2;
    i32 iconWidth = (iconHeight / 3) * 4;

    drawImage(
        ctx,
        this,
        Point((ctx.size.w - iconWidth) / 2, ctx.size.h / 4),
        Size(iconWidth, iconHeight),
        Icon_Amini_Allight,
        context()->splash()->splashContentOpacity()
    );

    TextStyle style;
    style.font = Text_Sans_Serif;
    style.size = UIScale::mediumFontSize(this);
    style.weight = Text_Semi_Bold;
    style.color = fg;
    style.alignment = Text_Center;

    u32 height = style.getFont()->height() * 2;

    drawText(
        ctx,
        this,
        Point(0, (ctx.size.h / 4) * 3),
        Size(ctx.size.w, height),
        string(gameName) + " " + gameVersion,
        style
    );

    drawText(
        ctx,
        this,
        Point(0, ((ctx.size.h / 4) * 3) + height),
        Size(ctx.size.w, height),
        localize("startup-splash-copyright"),
        style
    );

    drawText(
        ctx,
        this,
        Point(0, ((ctx.size.h / 4) * 3) + (height * 2)),
        Size(ctx.size.w, height),
        localize("startup-splash-license"),
        style
    );

    drawText(
        ctx,
        this,
        Point(0, ((ctx.size.h / 4) * 3) + (height * 3)),
        Size(ctx.size.w, height),
        localize("startup-splash-medical"),
        style
    );
}

bool StartupSplash::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && context()->controller()->standby() && !_hide && context()->splash()->splashOpacity() != 0;
}

void StartupSplash::hide()
{
    _hide = true;
}

void StartupSplash::dismiss(const Input& input)
{
    hide();
}

#ifdef LMOD_VR
optional<Point> StartupSplash::toLocal(const vec3& position, const quaternion& rotation) const
{
    // This prevents input gobbling
    return shouldDraw() && firstDrawTime && currentTime() - *firstDrawTime > minDrawTime ? Point() : optional<Point>();
}
#endif

SizeProperties StartupSplash::sizeProperties() const
{
    return sizePropertiesFillAll;
}
