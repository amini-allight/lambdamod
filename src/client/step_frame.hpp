/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "game_state.hpp"
#include "network_event.hpp"

class StepFrame
{
public:
    StepFrame();

    void enable();
    void disable();

    void makeReplayOf(const StepFrame& previous);
    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> compare(const StepFrame& previous) const;

    vector<NetworkEvent> step(EntityID hostEntityID);
    void push(const NetworkEvent& event);
    void push(const vector<NetworkEvent>& events);
    void push(const vector<tuple<function<void(const Input&)>, Input>>& editInputs);
    void push(const string& hook, const Input& input);

    GameState state;

private:
    vector<NetworkEvent> events;
    vector<tuple<function<void(const Input&)>, Input>> editInputs;
    vector<tuple<string, Input>> attachedInputs;
};
