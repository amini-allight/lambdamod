/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_flat_post_process_effect.hpp"
#include "render_context.hpp"

class RenderFlatHDR final : public RenderFlatPostProcessEffect
{
public:
    RenderFlatHDR(
        const RenderContext* ctx,
        RenderFlatPipelineStore* pipelineStore,
        const vector<FlatSwapchainElement*>& swapchainElements,
        f64 exposure,
        f64 gamma
    );
    ~RenderFlatHDR();

    void refresh(const vector<FlatSwapchainElement*>& swapchainElements) override;
    void update(f64 exposure, f64 gamma);

private:
    f32 exposure;
    f32 gamma;

    VmaBuffer createUniformBuffer() const override;
    void fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const override;

    void setDescriptorSet(
        VkDescriptorSet descriptorSet,
        VkBuffer buffer,
        VkImageView colorImageView,
        VkImageView stencilImageView
    ) const;
};
