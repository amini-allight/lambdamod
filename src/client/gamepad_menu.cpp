/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "gamepad_menu.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "control_context.hpp"

GamepadMenu::GamepadMenu(InterfaceView* view)
    : InterfaceWidget(view)
    , pointerManager(this)
{
    START_TOPLEVEL_BLOCKING_WIDGET_SCOPE(context()->input()->getScope("under-screen"), "gamepad-menu")
        WIDGET_SLOT("hide-gamepad-menu", dismiss)
    END_SCOPE

    pointerManager.init();
}

void GamepadMenu::step()
{
    pointerManager.step();

    InterfaceWidget::step();
}

void GamepadMenu::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

    drawPointer(ctx, this, pointer);
}

void GamepadMenu::dismiss(const Input& input)
{
    if (context()->attached())
    {
        context()->attachedMode()->toggleGamepadMenu();
    }
    else
    {
        context()->viewerMode()->toggleGamepadMenu();
    }
}

PositionProperties GamepadMenu::positionProperties() const
{
    return {
        0.25, 0.25,
        Positioning_Fraction, Positioning_Fraction
    };
}

SizeProperties GamepadMenu::sizeProperties() const
{
    return {
        0.5, 0.5,
        Scaling_Fraction, Scaling_Fraction
    };
}
