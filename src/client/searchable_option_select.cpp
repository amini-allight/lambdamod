/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "searchable_option_select.hpp"
#include "control_context.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_window.hpp"
#include "interface_window_view.hpp"
#include "fuzzy_find.hpp"

SearchableOptionSelectPopup::SearchableOptionSelectPopup(
    InterfaceWidget* parent,
    const vector<string>& options,
    const vector<string>& optionTooltips,
    const function<void(size_t)>& onSet
)
    : Popup(parent)
    , options(options)
    , optionTooltips(optionTooltips)
    , onSet(onSet)
    , _scrollIndex(0)
    , activeIndex(0)
{
    START_WIDGET_SCOPE("searchable-option-select-popup")
        WIDGET_SLOT("interact", interact)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)

        START_INDEPENDENT_SCOPE("thumb-grabbed", thumbGrabbed)
            WIDGET_SLOT("drag", drag)
            WIDGET_SLOT("release", release)
        END_SCOPE
    END_SCOPE

    searchBar = new LineEdit(this, nullptr, [](const string& text) -> void {});
    searchBar->setPlaceholder(localize("searchable-option-select-type-to-search"));
}

void SearchableOptionSelectPopup::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position());
    }
}

void SearchableOptionSelectPopup::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position());
        child->resize(Size(this->size().w, UIScale::lineEditHeight()));
    }
}

void SearchableOptionSelectPopup::step()
{
    InterfaceWidget::step();

    Point local = pointer - position();

    if (local.x < size().w - UIScale::scrollbarWidth())
    {
        size_t index = _scrollIndex + ((local.y - UIScale::lineEditHeight()) / UIScale::optionSelectEntryHeight());

        if (index < options.size())
        {
            activeIndex = index;
        }

        if (focused() && activeIndex < static_cast<i32>(optionTooltips.size()))
        {
            dynamic_cast<InterfaceWindowView*>(view())->setTooltip(optionTooltips.at(activeIndex), pointer);
        }
    }
}

void SearchableOptionSelectPopup::draw(const DrawContext& ctx) const
{
    ctx.delayedCommands.push_back([this](const DrawContext& ctx) -> void {
        drawSolid(
            ctx,
            this,
            theme.indentColor
        );

        drawSolid(
            ctx,
            this,
            Point(size().w - UIScale::scrollbarWidth(), UIScale::lineEditHeight()),
            Size(UIScale::scrollbarWidth(), size().h - UIScale::lineEditHeight()),
            theme.softIndentColor
        );

        if (needsScroll())
        {
            drawSolid(
                ctx,
                this,
                Point(size().w - UIScale::scrollbarWidth(), UIScale::lineEditHeight() + thumbY()),
                Size(UIScale::scrollbarWidth(), thumbHeight()),
                theme.outdentColor
            );
        }
    });

    for (InterfaceWidget* child : children())
    {
        ctx.delayedCommands.push_back([child](const DrawContext& ctx) -> void {
            child->draw(ctx);
        });
    }

    i32 maxRows = static_cast<i32>(ceil(size().h / static_cast<f64>(UIScale::optionSelectEntryHeight())) - 1);

    for (i32 i = _scrollIndex; i < static_cast<i32>(options.size()) && i - _scrollIndex < maxRows; i++)
    {
        i32 y = UIScale::lineEditHeight() + (i - _scrollIndex) * UIScale::optionSelectEntryHeight();

        if (focused() && !searchBar->focused() && i == activeIndex)
        {
            ctx.delayedCommands.push_back([this, y](const DrawContext& ctx) -> void {
                drawSolid(
                    ctx,
                    this,
                    Point(0, y),
                    Size(size().w - UIScale::scrollbarWidth(), UIScale::optionSelectEntryHeight()),
                    theme.accentColor
                );
            });
        }

        ctx.delayedCommands.push_back([this, i, y](const DrawContext& ctx) -> void {
            drawText(
                ctx,
                this,
                Point(0, y),
                Size(size().w - UIScale::scrollbarWidth(), UIScale::optionSelectEntryHeight()),
                options[i],
                TextStyle()
            );
        });
    }
}

bool SearchableOptionSelectPopup::canClose() const
{
    return !focused() && !thumbGrabbed;
}

i32 SearchableOptionSelectPopup::viewHeight() const
{
    return size().h - UIScale::lineEditHeight();
}

i32 SearchableOptionSelectPopup::scrollIndex() const
{
    return _scrollIndex;
}

void SearchableOptionSelectPopup::setScrollIndex(i32 index)
{
    _scrollIndex = index;
}

i32 SearchableOptionSelectPopup::childCount() const
{
    return options.size();
}

i32 SearchableOptionSelectPopup::childHeight() const
{
    return UIScale::optionSelectEntryHeight();
}

void SearchableOptionSelectPopup::interact(const Input& input)
{
    Point local = pointer - position();

    local.y -= UIScale::lineEditHeight();

    if (local.x < size().w - UIScale::scrollbarWidth())
    {
        size_t index = _scrollIndex + (local.y / UIScale::optionSelectEntryHeight());

        if (index < options.size())
        {
            playPositiveActivateEffect();
            onSet(index);
        }
    }
    else
    {
        if (local.y >= thumbY() && local.y < thumbY() + thumbHeight())
        {
            grabThumb(local);
        }
        else
        {
            setScrollIndexFromThumbY(local.y);
        }
    }
}

void SearchableOptionSelectPopup::scrollUp(const Input& input)
{
    if (_scrollIndex > 0)
    {
        _scrollIndex--;
    }
}

void SearchableOptionSelectPopup::scrollDown(const Input& input)
{
    if (_scrollIndex < maxScrollIndex())
    {
        _scrollIndex++;
    }
}

void SearchableOptionSelectPopup::goToStart(const Input& input)
{
    _scrollIndex = 0;
}

void SearchableOptionSelectPopup::goToEnd(const Input& input)
{
    _scrollIndex = maxScrollIndex();
}

void SearchableOptionSelectPopup::release(const Input& input)
{
    releaseThumb();
}

void SearchableOptionSelectPopup::drag(const Input& input)
{
    Point local = pointer - position();

    local.y -= UIScale::lineEditHeight();

    dragThumb(local);
}

SizeProperties SearchableOptionSelectPopup::sizeProperties() const
{
    return sizePropertiesFillAll;
}

SearchableOptionSelect::SearchableOptionSelect(
    InterfaceWidget* parent,
    const function<vector<string>()>& optionsSource,
    const function<size_t()>& source,
    const function<void(size_t)>& onSet
)
    : SearchableOptionSelect(
        parent,
        optionsSource,
        nullptr,
        source,
        onSet
    )
{

}

SearchableOptionSelect::SearchableOptionSelect(
    InterfaceWidget* parent,
    const function<vector<string>()>& optionsSource,
    const function<vector<string>()>& optionTooltipsSource,
    const function<size_t()>& source,
    const function<void(size_t)>& onSet
)
    : InterfaceWidget(parent)
    , options(optionsSource())
    , optionTooltips(optionTooltipsSource ? optionTooltipsSource() : vector<string>())
    , index(0)
    , optionsSource(optionsSource)
    , optionTooltipsSource(optionTooltipsSource)
    , source(source)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("searchable-option-select")
        WIDGET_SLOT("interact", interact)
    END_SCOPE

    sortedOptions = options;
    sortedOptionTooltips = optionTooltips;

    popup = new SearchableOptionSelectPopup(
        this,
        sortedOptions,
        sortedOptionTooltips,
        bind(&SearchableOptionSelect::onSelect, this, placeholders::_1)
    );
}

SearchableOptionSelect::~SearchableOptionSelect()
{
    delete popup;
}

void SearchableOptionSelect::move(const Point& position)
{
    this->position(position);

    popup->move(position + Point(0, size().h));
}

void SearchableOptionSelect::resize(const Size& size)
{
    this->size(size);

    popup->resize(Size(
        this->size().w,
        UIScale::lineEditHeight() + min<i32>(optionsSource().size() * UIScale::optionSelectEntryHeight(), UIScale::optionSelectPopupMaxHeight())
    ));
}

void SearchableOptionSelect::step()
{
    InterfaceWidget::step();

    if (source)
    {
        index = source();
    }

    if (optionTooltipsSource)
    {
        optionTooltips = optionTooltipsSource();
    }

    if (optionsSource)
    {
        vector<string> newOptions = optionsSource();

        string search = popup->searchBar->content();

        if (newOptions != options || search != this->search)
        {
            options = newOptions;
            this->search = search;
            index = 0;
            popup->_scrollIndex = 0;
            popup->activeIndex = 0;

            if (!search.empty())
            {
                vector<size_t> indices(options.size());

                size_t i = 0;
                generate(indices.begin(), indices.end(), [&i]() -> size_t { return i++; });

                sort(
                    indices.begin(),
                    indices.end(),
                    [&](size_t aIndex, size_t bIndex) -> bool
                    {
                        return fuzzyFindCompare(options[aIndex], options[bIndex], search);
                    }
                );

                sortedOptions = vector<string>(options.size());
                sortedOptionTooltips = vector<string>(optionTooltips.size());

                for (size_t i = 0; i < indices.size(); i++)
                {
                    sortedOptions[i] = options[indices[i]];

                    if (i < optionTooltips.size())
                    {
                        sortedOptionTooltips[i] = optionTooltips[indices[i]];
                    }
                }
            }
            else
            {
                sortedOptions = options;
                sortedOptionTooltips = optionTooltips;
            }
        }
    }

    if (!focused() && popup->canClose() && popup->opened())
    {
        popup->close();
    }
}

void SearchableOptionSelect::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        focused() ? theme.accentColor : theme.outdentColor
    );

    drawText(
        ctx,
        this,
        Point(0),
        Size(size().w - UIScale::iconButtonSize().w, size().h),
        index < options.size() ? options[index] : "",
        TextStyle()
    );

    drawImage(
        ctx,
        this,
        Point(size().w - UIScale::iconButtonSize().w, 0),
        UIScale::iconButtonSize(),
        Icon_Down
    );

    InterfaceWidget::draw(ctx);
}

void SearchableOptionSelect::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void SearchableOptionSelect::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void SearchableOptionSelect::onSelect(size_t index)
{
    popup->close();

    size_t originalIndex = 0;

    for (size_t i = 0; i < options.size(); i++)
    {
        if (options[i] == sortedOptions[index])
        {
            originalIndex = i;
            break;
        }
    }

    this->index = originalIndex;
    onSet(originalIndex);
}

void SearchableOptionSelect::interact(const Input& input)
{
    if (popup->opened())
    {
        popup->close();
        playNegativeActivateEffect();
    }
    else
    {
        popup->open();
        playPositiveActivateEffect();
    }

    if (popup->opened())
    {
        update();
    }
}

SizeProperties SearchableOptionSelect::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::optionSelectHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
