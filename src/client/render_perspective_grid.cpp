/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_perspective_grid.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr f64 perspectiveGridSpacing = 0.1;

RenderPerspectiveGrid::RenderPerspectiveGrid(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    f64 size
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->perspectiveGrid.pipelineLayout, pipelineStore->perspectiveGrid.pipeline)
    , size(size)
{
    transform = { position, quaternion(), vec3(1) };

    bool axises = static_cast<u64>(floor(size / perspectiveGridSpacing)) % 2 == 0;
    size_t divisions = floor(size / perspectiveGridSpacing) + 1;
    size_t vertexCount = (2 * 2 * divisions) + (axises ? 2 : 0);

    vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData;
    vertexData.reserve(vertexCount);

    for (size_t x = 0; x < divisions; x++)
    {
        f64 xPos = x * perspectiveGridSpacing;
        vec3 color = theme.gridSecondaryColor.toVec3();

        if (axises && x == divisions / 2)
        {
            color = theme.yAxisColor.toVec3();
        }
        else if (x % 10 == 0)
        {
            color = theme.gridColor.toVec3();
        }

        vertexData.push_back({ vec3(xPos, 0, 0), color });
        vertexData.push_back({ vec3(xPos, size, 0), color });
    }

    for (size_t y = 0; y < divisions; y++)
    {
        f64 yPos = y * perspectiveGridSpacing;
        vec3 color = theme.gridSecondaryColor.toVec3();

        if (axises && y == divisions / 2)
        {
            color = theme.xAxisColor.toVec3();
        }
        else if (y % 10 == 0)
        {
            color = theme.gridColor.toVec3();
        }

        vertexData.push_back({ vec3(0, yPos, 0), color });
        vertexData.push_back({ vec3(size, yPos, 0), color });
    }

    if (axises)
    {
        vertexData.push_back({
            vec3((divisions / 2) * perspectiveGridSpacing, (divisions / 2) * perspectiveGridSpacing, -(size / 2)),
            theme.zAxisColor.toVec3()
        });
        vertexData.push_back({
            vec3((divisions / 2) * perspectiveGridSpacing, (divisions / 2) * perspectiveGridSpacing, +(size / 2)),
            theme.zAxisColor.toVec3()
        });
    }

    for (ColoredVertex& vertex : vertexData)
    {
        vertex.position -= vec3(size / 2, size / 2, 0);
    }

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderPerspectiveGrid::~RenderPerspectiveGrid()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderPerspectiveGrid::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    bool axises = static_cast<u64>(floor(size / perspectiveGridSpacing)) % 2 == 0;
    size_t divisions = floor(size / perspectiveGridSpacing) + 1;

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        (2 * 2 * divisions) + (axises ? 2 : 0)
    );
}

VmaBuffer RenderPerspectiveGrid::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4));
}
