/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "font_store.hpp"
#include "paths.hpp"

bool FontKey::operator==(const FontKey& rhs) const
{
    return font == rhs.font && weight == rhs.weight && size == rhs.size;
}

bool FontKey::operator!=(const FontKey& rhs) const
{
    return !(*this == rhs);
}

bool FontKey::operator>(const FontKey& rhs) const
{
    if (font > rhs.font)
    {
        return true;
    }
    else if (font < rhs.font)
    {
        return false;
    }
    else
    {
        if (weight > rhs.weight)
        {
            return true;
        }
        else if (weight < rhs.weight)
        {
            return false;
        }
        else
        {
            if (size > rhs.size)
            {
                return true;
            }
            else if (size < rhs.size)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}

bool FontKey::operator<(const FontKey& rhs) const
{
    if (font < rhs.font)
    {
        return true;
    }
    else if (font > rhs.font)
    {
        return false;
    }
    else
    {
        if (weight < rhs.weight)
        {
            return true;
        }
        else if (weight > rhs.weight)
        {
            return false;
        }
        else
        {
            if (size < rhs.size)
            {
                return true;
            }
            else if (size > rhs.size)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}

bool FontKey::operator>=(const FontKey& rhs) const
{
    return (*this > rhs) || (*this == rhs);
}

bool FontKey::operator<=(const FontKey& rhs) const
{
    return (*this < rhs) || (*this == rhs);
}

strong_ordering FontKey::operator<=>(const FontKey& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

FontStore::FontStore()
{
    fontInit();
}

FontStore::~FontStore()
{
    for (const auto& [ key, font ] : fonts)
    {
        delete font;
    }

    fontQuit();
}

FontFace* FontStore::get(TextFont font, TextWeight weight, u32 size)
{
    FontKey key = { font, weight, size };

    auto it = fonts.find(key);

    if (it == fonts.end())
    {
        FontFace* newFont = loadTTF(pathFromProperties(font, weight), size);

        fonts.insert({ key, newFont });

        return newFont;
    }
    else
    {
        return it->second;
    }
}

string FontStore::pathFromProperties(TextFont font, TextWeight weight) const
{
    string path = fontPath + "/";

    switch (font)
    {
    case Text_Monospace :
        path += "liberation-mono";
        break;
    case Text_Sans_Serif :
        path += "open-sans";
        break;
    }

    switch (weight)
    {
    case Text_Regular :
        path += "-regular";
        break;
    case Text_Semi_Bold :
        path += "-semi-bold";
        break;
    case Text_Bold :
        path += "-bold";
        break;
    case Text_Extra_Bold :
        path += "-extra-bold";
        break;
    }

    path += fontExt;

    return path;
}

FontStore* fontStore = nullptr;
