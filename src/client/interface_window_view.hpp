/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_view.hpp"

// Fix for Windows namespace pollution
#undef interface

class InterfaceWindowView : public InterfaceView
{
public:
    InterfaceWindowView(Interface* interface, const string& name);
    ~InterfaceWindowView() override;

    void step() override;
    void draw(const DrawContext& ctx) override;
    void resize(const Size& size) override;
    void unfocus() override;

    const Size& size() const;

    void sort();
    void clean();

    void setTooltip(const string& tooltip, const Point& position);

private:
    Size _size;

    bool tooltipActive;
    string tooltip;
    Point tooltipPosition;
    chrono::milliseconds tooltipStartTime;

    bool tooltipShown() const;
    bool tooltipReady() const;
    void drawTooltip(const DrawContext& ctx);
};
