/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_mode_context.hpp"
#include "localization.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "apply_prefab.hpp"
#include "confirm_dialog.hpp"
#include "new_body_part_dialog.hpp"
#include "intersections.hpp"
#include "bresenham.hpp"

// TODO: EDITING: Hiding parts doesn't stop them drawing and isn't per-entity

BodyModeContext::BodyModeContext(ViewerModeContext* ctx)
    : InternalModeContext(ctx)
{

}

void BodyModeContext::addNewPrefab(const string& name)
{
    set<BodyPartID> previousIDs = allPartIDs();

    ctx->context()->controller()->edit([this, name]() -> void {
        Entity* entity = activeEntity();

        if (!entity)
        {
            return;
        }

        applyPrefabByName(entity, name, {
            ctx->cursorPosition() - entity->globalPosition(),
            entity->globalRotation().inverse(),
            1.0 / entity->globalScale()
        });
    });

    selectNew(previousIDs);
}

void BodyModeContext::addNew()
{
    if (addNewSubpart())
    {
        return;
    }

    ctx->context()->interface()->addWindow<NewBodyPartDialog>();
}

void BodyModeContext::addNew(BodyPartType type)
{
    set<BodyPartID> previousIDs = allPartIDs();

    BodyPart* part;

    ctx->context()->controller()->edit([this, type, &part]() -> void {
        Entity* entity = activeEntity();

        if (!entity)
        {
            return;
        }

        part = new BodyPart(nullptr, entity->body().nextPartID());

        part->setType(type);
        part->setLocalPosition(globalToRegional(ctx->cursorPosition()));

        entity->body().add(BodyPartID(), part);
    });

    selectNew(previousIDs);
}

void BodyModeContext::removeSelected()
{
    if (removeSelectedSubpart())
    {
        return;
    }

    string text;

    if (selectionRootsOnly().empty())
    {
        return;
    }

    size_t partCount = 0;

    for (const BodyPartSelection& selection : selectionRootsOnly())
    {
        selection.part->traverse([&](const BodyPart* part) -> void {
            partCount++;
        });
    }

    if (partCount == 1)
    {
        text = localize("body-mode-context-are-you-sure-you-want-to-delete-body-part", to_string(partCount));
    }
    else
    {
        text = localize("body-mode-context-are-you-sure-you-want-to-delete-body-parts", to_string(partCount));
    }

    ctx->context()->interface()->addWindow<ConfirmDialog>(
        text,
        [this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            ctx->context()->controller()->edit([this]() -> void {
                Entity* entity = activeEntity();

                for (const BodyPartSelection& selection : selectionRootsOnly())
                {
                    entity->body().remove(selection.part->id());
                }
            });

            deselectAll();
        }
    );
}

void BodyModeContext::addChildren(const Point& point)
{
    ctx->context()->controller()->edit([this, point]() -> void {
        Entity* entity = activeEntity();

        vector<BodyPartSelection> parentCandidates = bodyPartsUnderPoint(point);

        if (!parentCandidates.empty())
        {
            return;
        }

        const BodyPartSelection& parent = parentCandidates.front();

        for (const BodyPartSelection& selection : selection())
        {
            entity->body().setParent(parent.part, parent.subID, selection.part, selection.subID);
        }
    });
}

void BodyModeContext::removeParent()
{
    ctx->context()->controller()->edit([this]() -> void {
        Entity* entity = activeEntity();

        for (const BodyPartSelection& selection : selection())
        {
            entity->body().clearParent(selection.part);
        }
    });
}

void BodyModeContext::hideSelected()
{
    for (const BodyPartSelectionID& id : _selectionIDs)
    {
        _hiddenBodyPartIDs.insert(id.id);
    }

    deselectAll();
}

void BodyModeContext::hideUnselected()
{
    for (const BodyPartSelectionID& id : unselectionIDs())
    {
        _hiddenBodyPartIDs.insert(id.id);
    }
}

void BodyModeContext::unhideAll()
{
    _hiddenBodyPartIDs.clear();
}

const set<BodyPartID>& BodyModeContext::hiddenBodyPartIDs() const
{
    return _hiddenBodyPartIDs;
}

void BodyModeContext::endSpecialEdit(const Point& point)
{
    EditModeContext::endSpecialEdit(point);
    lastPaintCanvasPixelOffset = {};
}

bool BodyModeContext::canvasPainting() const
{
    set<BodyPartSelection> selection = this->selection();

    return specialEditing() && selection.size() == 1 && selection.begin()->part->type() == Body_Part_Canvas;
}

void BodyModeContext::resetPosition()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            mat4 previous = selection.part->regionalTransform();

            if (selection.subID != BodyPartSubID() && !selection.part->subparts().empty())
            {
                previous.setPosition(selection.part->subparts().at(selection.subID.value() - 1));
            }

            mat4 current = previous;
            current.setPosition(vec3());

            applyTransformToPart(
                selection,
                vec3(),
                selection.part->regionalTransform(),
                previous,
                current
            );
        }
    });
}

void BodyModeContext::resetRotation()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            mat4 transform = selection.part->regionalTransform();

            if (selection.subID != BodyPartSubID())
            {
                continue;
            }

            transform.setRotation(quaternion());

            selection.part->setRegionalTransform(transform);
        }
    });
}

void BodyModeContext::resetScale()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            mat4 transform = selection.part->regionalTransform();

            if (selection.subID != BodyPartSubID())
            {
                continue;
            }

            transform.setScale(vec3(1));

            selection.part->setRegionalTransform(transform);
        }
    });
}

void BodyModeContext::resetLinearVelocity()
{
    // Do nothing
}

void BodyModeContext::resetAngularVelocity()
{
    // Do nothing
}

void BodyModeContext::selectionToCursor()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            mat4 previous = selection.part->regionalTransform();

            if (selection.subID != BodyPartSubID() && !selection.part->subparts().empty())
            {
                previous.setPosition(selection.part->subparts().at(selection.subID.value() - 1));
            }

            mat4 current = previous;
            current.setPosition(globalToRegional(ctx->cursorPosition()));

            applyTransformToPart(
                selection,
                vec3(),
                selection.part->regionalTransform(),
                previous,
                current
            );
        }
    });
}

void BodyModeContext::duplicate()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    set<BodyPartID> previousIDs = allPartIDs();

    vector<BodyPart> parts;

    for (const BodyPartSelection& selection : selectionRootsOnly())
    {
        parts.push_back(*selection.part);
    }

    ctx->context()->controller()->edit([entity, parts]() -> void {
        for (const BodyPart& part : parts)
        {
            auto newPart = new BodyPart(nullptr, entity->body().nextPartID(), part);

            entity->body().add(BodyPartID(), newPart);
        }
    });

    selectNew(previousIDs);
}

void BodyModeContext::cut()
{
    copy();

    ctx->context()->controller()->edit([this]() -> void {
        Entity* entity = activeEntity();

        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            entity->body().remove(selection.part->id());
        }
    });

    deselectAll();
}

void BodyModeContext::copy()
{
    set<BodyPart*> parts;

    for (const BodyPartSelection& selection : selectionRootsOnly())
    {
        parts.insert(selection.part);
    }

    copyBuffer.copy(
        ctx->context()->flatViewer()->position(),
        ctx->context()->flatViewer()->rotation(),
        activeEntity(),
        parts
    );
}

void BodyModeContext::paste()
{
    set<BodyPartID> previousIDs = allPartIDs();

    copyBuffer.paste(
        ctx->context()->flatViewer()->position(),
        ctx->context()->flatViewer()->rotation(),
        activeEntity()
    );

    selectNew(previousIDs);
}

void BodyModeContext::extrude()
{
    if (extrudeSubpart())
    {
        return;
    }

    ctx->context()->controller()->edit([this]() -> void {
        Entity* entity = activeEntity();

        if (!entity)
        {
            return;
        }

        for (const BodyPartSelection& selection : selection())
        {
            vec3 position = selection.subID == BodyPartSubID()
                ? selection.part->localPosition()
                : selection.part->regionalTransform().applyToPosition(selection.part->subparts().at(selection.subID.value() - 1), false);

            auto child = new BodyPart(selection.part, entity->body().nextPartID());

            child->setLocalPosition(position);
            child->get<BodyPartLine>().length = 0;

            selection.part->add(selection.part->id(), child);

            select({ child->id(), BodyPartSubID(2) });
        }
    });
}

void BodyModeContext::split()
{
    if (splitSubpart())
    {
        return;
    }

    ctx->context()->controller()->edit([this]() -> void {
        Entity* entity = activeEntity();

        if (!entity)
        {
            return;
        }

        Body body;

        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            if (selection.subID != BodyPartSubID())
            {
                continue;
            }

            body.add(BodyPartID(), new BodyPart(*selection.part));
        }

        if (entity->parent())
        {
            auto newEntity = new Entity(entity->parent(), ctx->context()->controller()->game().nextEntityID(), *entity);

            newEntity->body() = body;

            entity->parent()->add(entity->parent()->id(), newEntity);
        }
        else
        {
            auto newEntity = new Entity(entity->world(), ctx->context()->controller()->game().nextEntityID(), *entity);

            newEntity->body() = body;

            entity->world()->add(EntityID(), newEntity);
        }

        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            entity->body().remove(selection.part->id());
        }
    });

    deselectAll();
}

void BodyModeContext::join(const Point& point)
{
    joinSubpart(point);
}

set<BodyPartID> BodyModeContext::allPartIDs() const
{
    set<BodyPartID> allPartIDs;

    Entity* entity = activeEntity();

    if (!entity)
    {
        return allPartIDs;
    }

    entity->body().traverse([&](const BodyPart* part) -> void {
        allPartIDs.insert(part->id());
    });

    return allPartIDs;
}

void BodyModeContext::selectNew(const set<BodyPartID>& previousIDs)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    deselectAll();
    entity->body().traverse([&](const BodyPart* part) -> void {
        if (previousIDs.contains(part->id()))
        {
            return;
        }

        select(BodyPartSelectionID(part->id(), BodyPartSubID()));
    });
}

bool BodyModeContext::addNewSubpart()
{
    if (selection().size() != 1)
    {
        return false;
    }

    BodyPartSelection selection = *this->selection().begin();

    switch (selection.part->type())
    {
    default :
        break;
    case Body_Part_Terrain :
        addTerrainNode(selection);
        return true;
    case Body_Part_Area :
        if (selection.subID != BodyPartSubID())
        {
            addAreaPoint(selection);
            return true;
        }
        break;
    }

    return false;
}

bool BodyModeContext::removeSelectedSubpart()
{
    if (selection().size() != 1)
    {
        return false;
    }

    BodyPartSelection selection = *this->selection().begin();

    switch (selection.part->type())
    {
    default :
        if (selection.subID != BodyPartSubID())
        {
            removeTerrainNode(selection);
            return true;
        }
        break;
    case Body_Part_Area :
        if (selection.subID != BodyPartSubID())
        {
            removeAreaPoint(selection);
            return true;
        }
        break;
    }

    return false;
}

bool BodyModeContext::extrudeSubpart()
{
    if (selection().size() != 1)
    {
        return false;
    }

    BodyPartSelection selection = *this->selection().begin();

    switch (selection.part->type())
    {
    default :
        break;
    case Body_Part_Terrain :
        if (selection.subID != BodyPartSubID())
        {
            extrudeTerrainNode(selection);
            return true;
        }
        break;
    }

    return false;
}

bool BodyModeContext::splitSubpart()
{
    if (selection().size() != 1)
    {
        return false;
    }

    BodyPartSelection selection = *this->selection().begin();

    switch (selection.part->type())
    {
    default :
        break;
    case Body_Part_Terrain :
        if (selection.subID != BodyPartSubID())
        {
            splitTerrainNode(selection);
            return true;
        }
        break;
    }

    return false;
}

bool BodyModeContext::joinSubpart(const Point& point)
{
    if (selection().size() != 1)
    {
        return false;
    }

    BodyPartSelection selection = *this->selection().begin();

    switch (selection.part->type())
    {
    default :
        break;
    case Body_Part_Terrain :
        if (selection.subID != BodyPartSubID())
        {
            joinTerrainNode(selection, point);
            return true;
        }
        break;
    }

    return false;
}

void BodyModeContext::specialEdit(const Point& point)
{
    if (canvasPainting())
    {
        paintCanvas(point);
    }
}

void BodyModeContext::paintCanvas(const Point& point)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    set<BodyPartSelection> selection = this->selection();

    if (selection.size() != 1 || selection.begin()->part->type() != Body_Part_Canvas)
    {
        return;
    }

    BodyPart* part = selection.begin()->part;
    BodyPartCanvas& canvas = part->get<BodyPartCanvas>();

    vec3 position = part->regionalTransform().applyToPosition(
        entity->globalTransform().applyToPosition(ctx->context()->flatViewer()->position(), false),
        false
    );
    vec3 direction = part->regionalTransform().applyToDirection(
        entity->globalTransform().applyToDirection(ctx->context()->flatViewer()->screenToWorld(point), false),
        false
    );

    vector<vec2> intersections = intersectRectangle(canvas.size, canvas.size, position, direction, numeric_limits<f64>::max());

    if (intersections.empty())
    {
        return;
    }

    vec2 normalizedDeviceCoordinates = (intersections.front() / (canvas.size / 2)) * vec2(1, -1);

    vec2 continuousOffset = (normalizedDeviceCoordinates + 1) / 2;

    ivec2 endPixelOffset = continuousOffset * canvasResolution;
    ivec2 startPixelOffset = lastPaintCanvasPixelOffset ? *lastPaintCanvasPixelOffset : endPixelOffset;

    u8 value;

    switch (_specialEditMode)
    {
    default :
    case Special_Edit_None :
        return;
    case Special_Edit_Default :
        value = 1;
        break;
    case Special_Edit_Alt :
        value = 0;
        break;
    }

    bresenham2D(startPixelOffset, endPixelOffset, [&](const ivec2& pixelOffset) -> void {
        ctx->context()->controller()->edit([this, &canvas, pixelOffset, value]() -> void {
            for (u32 y = 0; y < canvasResolution; y++)
            {
                for (u32 x = 0; x < canvasResolution; x++)
                {
                    if (vec2(pixelOffset).distance(vec2(x, y)) <= ctx->canvasPaintBrushRadius())
                    {
                        canvas.set(uvec2(x, y), value);
                    }
                }
            }
        }, !firstTransform);
        firstTransform = false;
    });

    lastPaintCanvasPixelOffset = endPixelOffset;
}

void BodyModeContext::addTerrainNode(const BodyPartSelection& selection)
{
    BodyPartTerrain& terrain = selection.part->get<BodyPartTerrain>();

    ctx->context()->controller()->edit([&terrain]() -> void {
        terrain.nodes.push_back(TerrainNode(terrain.nextNodeID++));
    });

    deselectAll();
    select({ selection.part->id(), BodyPartSubID(terrain.nodes.size()) });
}

void BodyModeContext::removeTerrainNode(const BodyPartSelection& selection)
{
    BodyPartTerrain& terrain = selection.part->get<BodyPartTerrain>();

    ctx->context()->controller()->edit([selection, &terrain]() -> void {
        terrain.nodes.erase(terrain.nodes.begin() + (selection.subID.value() - 1));
    });

    deselectAll();
}

void BodyModeContext::extrudeTerrainNode(const BodyPartSelection& selection)
{
    BodyPartTerrain& terrain = selection.part->get<BodyPartTerrain>();

    TerrainNode& previous = terrain.nodes.at(selection.subID.value() - 1);

    ctx->context()->controller()->edit([&terrain, &previous]() -> void {
        TerrainNode next(terrain.nextNodeID++);
        next.position = previous.position;
        next.linkedIDs.insert(previous.id);

        terrain.nodes.push_back(next);
        previous.linkedIDs.insert(next.id);
    });

    deselectAll();
    select({ selection.part->id(), BodyPartSubID(terrain.nodes.size()) });
}

void BodyModeContext::splitTerrainNode(const BodyPartSelection& selection)
{
    BodyPartTerrain& terrain = selection.part->get<BodyPartTerrain>();

    TerrainNode& node = terrain.nodes.at(selection.subID.value() - 1);

    ctx->context()->controller()->edit([&terrain, &node]() -> void {
        for (TerrainNode& other : terrain.nodes)
        {
            if (node.linkedIDs.contains(other.id))
            {
                other.linkedIDs.erase(node.id);
            }
        }

        node.linkedIDs.clear();
    });
}

void BodyModeContext::joinTerrainNode(const BodyPartSelection& selection, const Point& point)
{
    BodyPartTerrain& terrain = selection.part->get<BodyPartTerrain>();

    TerrainNode& node = terrain.nodes.at(selection.subID.value() - 1);

    for (const BodyPartSelection& candidateSelection : bodyPartsUnderPoint(point))
    {
        if (candidateSelection.part != selection.part)
        {
            continue;
        }

        if (candidateSelection.subID == BodyPartSubID() || candidateSelection.subID == selection.subID)
        {
            continue;
        }

        TerrainNode& candidate = terrain.nodes.at(candidateSelection.subID.value() - 1);

        ctx->context()->controller()->edit([&node, &candidate]() -> void {
            node.linkedIDs.insert(candidate.id);
            candidate.linkedIDs.insert(node.id);
        });
        break;
    }
}

void BodyModeContext::addAreaPoint(const BodyPartSelection& selection)
{
    BodyPartArea& area = selection.part->get<BodyPartArea>();
    size_t index = (selection.subID.value() - 1) / 2;
    size_t nextIndex = loopIndex(index + 1, area.points.size());

    ctx->context()->controller()->edit([&area, index, nextIndex]() -> void {
        vec2 newPoint = (area.points.at(index) + area.points.at(nextIndex)) / 2;

        area.points.insert(area.points.begin() + nextIndex, newPoint);
    });

    u32 nextSubID;

    if (nextIndex < index)
    {
        nextSubID = selection.subID.value() - ((selection.subID.value() / 2) * 2);
    }
    else
    {
        nextSubID = selection.subID.value() + 2;
    }

    deselectAll();
    select({ selection.part->id(), BodyPartSubID(nextSubID) });
}

void BodyModeContext::removeAreaPoint(const BodyPartSelection& selection)
{
    BodyPartArea& area = selection.part->get<BodyPartArea>();
    size_t index = (selection.subID.value() - 1) / 2;

    if (area.points.size() <= minAreaPoints)
    {
        return;
    }

    ctx->context()->controller()->edit([&area, index]() -> void {
        area.points.erase(area.points.begin() + index);
    });

    i64 nextSubID = static_cast<i64>(selection.subID.value()) - 2;

    if (nextSubID < 0)
    {
        nextSubID = selection.part->subparts().size() + nextSubID;
    }

    deselectAll();
    select({ selection.part->id(), BodyPartSubID(nextSubID) });
}

void BodyModeContext::transformSet(size_t i, const vec3& pivot, const mat4& initial, const mat4& transform)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    ctx->context()->controller()->edit([this, entity, i, pivot, initial, transform]() -> void {
        BodyPartSelection selection = selectionRootAtIndex(i);

        mat4 current = transform;

        applyTransformToPart(
            selection,
            entity->globalTransform().applyToPosition(pivot, false),
            entity->globalTransform().applyToTransform(initialPartTransforms.at(i), false),
            entity->globalTransform().applyToTransform(initial, false),
            entity->globalTransform().applyToTransform(current, false)
        );
    }, !firstTransform);
    firstTransform = false;
}

mat4 BodyModeContext::transformGet(size_t i) const
{
    BodyPartSelection selection = selectionRootAtIndex(i);

    mat4 transform = selection.part->regionalTransform();

    if (selection.subID != BodyPartSubID() && !selection.part->subparts().empty())
    {
        transform.setPosition(selection.part->subparts().at(selection.subID.value() - 1));
    }

    return activeEntity()->globalTransform().applyToTransform(transform);
}

mat4 BodyModeContext::transformGetWithPromotion(size_t i) const
{
    BodyPartSelection selection = selectionRootAtIndex(i);

    return activeEntity()->globalTransform().applyToTransform(selection.part->regionalTransform());
}
