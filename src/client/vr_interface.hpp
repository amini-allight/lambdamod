/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "input_interface.hpp"

class VRInterface
{
public:
    VRInterface();
    VRInterface(const VRInterface& rhs) = delete;
    VRInterface(VRInterface&& rhs) = delete;
    virtual ~VRInterface() = 0;

    VRInterface& operator=(const VRInterface& rhs) = delete;
    VRInterface& operator=(VRInterface&& rhs) = delete;

    virtual void requestExit() = 0;

    virtual bool step() = 0;

    virtual void setRoom(const vec3& position, const quaternion& rotation) = 0;
    virtual void resetRoom() = 0;
    virtual vec3 roomToWorld(const vec3& v) const = 0;
    virtual quaternion roomToWorld(const quaternion& q) const = 0;
    virtual vec3 worldToRoom(const vec3& v) const = 0;
    virtual quaternion worldToRoom(const quaternion& q) const = 0;

    virtual bool running() const = 0;
    virtual bool shouldRender() const = 0;

    virtual InputInterface* input() const = 0;
};
#endif
