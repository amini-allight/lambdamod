/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_confinement_indicator.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "theme.hpp"

static const string meshFilePath = meshPath + "/confinement-indicator" + meshExt;

RenderFlatConfinementIndicator::RenderFlatConfinementIndicator(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    f64 radius,
    f64 strength
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->confinementIndicator.pipelineLayout, pipelineStore->confinementIndicator.pipeline)
    , strength(strength)
{
    transform = { position, quaternion(), vec3(radius) };

    string mesh = getFile(meshFilePath);

    vertexCount = mesh.size() / sizeof(fvec3);
    vertices = createVertexBuffer(vertexCount * sizeof(fvec3));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, mesh.data(), vertexCount * sizeof(fvec3));

    createComponents(swapchainElements);
}

RenderFlatConfinementIndicator::~RenderFlatConfinementIndicator()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderFlatConfinementIndicator::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderFlatConfinementIndicator::createUniformBuffer() const
{
    VmaBuffer uniform = RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount + sizeof(fvec4) * 4 + sizeof(f32));

    VmaMapping<fvec4> mapping(ctx->allocator, uniform);

    mapping.data[8] = theme.accentGradientXStartColor.toVec4();
    mapping.data[9] = theme.accentGradientXEndColor.toVec4();
    mapping.data[10] = theme.accentGradientYStartColor.toVec4();
    mapping.data[11] = theme.accentGradientYEndColor.toVec4();

    reinterpret_cast<f32*>(mapping.data)[48] = strength;

    return uniform;
}
