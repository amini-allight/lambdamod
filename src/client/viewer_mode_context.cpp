/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "viewer_mode_context.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "entity_editor.hpp"
#include "flat_render.hpp"
#include "render_cursor.hpp"
#include "render_space_graph_visualizer.hpp"

ViewerModeContext::ViewerModeContext(ControlContext* context)
    : _context(context)
    , _mode(Viewer_Sub_Mode_Entity)
    , _entityMode(new EntityModeContext(this))
    , _bodyMode(new BodyModeContext(this))
    , _actionMode(new ActionModeContext(this))
    , _editing(false)
    , _cursorShown(true)
    , _gridShown(true)
    , _gridSize(32)
    , _entityPointsShown(true)
    , _gizmoShown(true)
    , _spaceGraphVisualizerShown(false)
    , _canvasPaintBrushRadius(3)
    , _translationStep(0)
    , _rotationStep(0)
    , _scaleStep(0)
    , _gamepadMenu(false)
{
#ifdef LMOD_VR
    if (!g_vr)
    {
        cursorID = render()->addDecoration<RenderCursor>(_cursorPosition);
        spaceGraphVisualizerID = render()->addDecoration<RenderSpaceGraphVisualizer>(nullptr);
        render()->setDecorationShown(spaceGraphVisualizerID, false);
    }
#else
    cursorID = render()->addDecoration<RenderCursor>(_cursorPosition);
    spaceGraphVisualizerID = render()->addDecoration<RenderSpaceGraphVisualizer>(nullptr);
    render()->setDecorationShown(spaceGraphVisualizerID, false);
#endif
}

ViewerModeContext::~ViewerModeContext()
{
#ifdef LMOD_VR
    if (!g_vr)
    {
        render()->removeDecoration(spaceGraphVisualizerID);
        render()->removeDecoration(cursorID);
    }
#else
    render()->removeDecoration(spaceGraphVisualizerID);
    render()->removeDecoration(cursorID);
#endif

    delete _actionMode;
    delete _bodyMode;
    delete _entityMode;
}

ControlContext* ViewerModeContext::context() const
{
    return _context;
}

void ViewerModeContext::step()
{
    if (_spaceGraphVisualizerShown && context()->controller()->selfWorld())
    {
        render()->updateDecoration<RenderSpaceGraphVisualizer>(spaceGraphVisualizerID, &context()->controller()->selfWorld()->spaceGraph());
    }

    if (modeHasChanged())
    {
        ViewerSubMode previousMode = _mode;

        if (inActionMode())
        {
            _mode = Viewer_Sub_Mode_Action;
        }
        else if (inBodyMode())
        {
            _mode = Viewer_Sub_Mode_Body;
        }
        else
        {
            _mode = Viewer_Sub_Mode_Entity;
        }

        switch (previousMode)
        {
        case Viewer_Sub_Mode_Entity :
            _entityMode->exit();
            break;
        case Viewer_Sub_Mode_Body :
            _bodyMode->exit();
            break;
        case Viewer_Sub_Mode_Action :
            _actionMode->exit();
            break;
        }

        switch (_mode)
        {
        case Viewer_Sub_Mode_Entity :
            _entityMode->enter();
            break;
        case Viewer_Sub_Mode_Body :
            _bodyMode->enter();
            break;
        case Viewer_Sub_Mode_Action :
            _actionMode->enter();
            break;
        }
    }

    switch (_mode)
    {
    case Viewer_Sub_Mode_Entity :
        _entityMode->step();
        break;
    case Viewer_Sub_Mode_Body :
        _bodyMode->step();
        break;
    case Viewer_Sub_Mode_Action :
        _actionMode->step();
        break;
    }
}

void ViewerModeContext::enter()
{
    render()->setDecorationShown(cursorID, true);

    if (inBodyMode())
    {
        _bodyMode->enter();
    }
    else
    {
        _entityMode->enter();
    }
}

void ViewerModeContext::exit()
{
    render()->setDecorationShown(cursorID, false);

    if (inBodyMode())
    {
        _bodyMode->exit();
    }
    else
    {
        _entityMode->exit();
    }
}

ViewerSubMode ViewerModeContext::mode() const
{
    return _mode;
}

EntityModeContext* ViewerModeContext::entityMode() const
{
    return _entityMode;
}

BodyModeContext* ViewerModeContext::bodyMode() const
{
    return _bodyMode;
}

ActionModeContext* ViewerModeContext::actionMode() const
{
    return _actionMode;
}

EditModeContext* ViewerModeContext::editMode() const
{
    switch (_mode)
    {
    default :
        fatal("Encountered unknown viewer mode edit mode '" + to_string(_mode) + "'.");
    case Viewer_Sub_Mode_Entity :
        return _entityMode;
    case Viewer_Sub_Mode_Body :
        return _bodyMode;
    case Viewer_Sub_Mode_Action :
        return _actionMode;
    }
}

void ViewerModeContext::attach(EntityID id)
{
    GameEvent event(Game_Event_Attach);
    event.u = id.value();

    _context->controller()->send(event);
}

bool ViewerModeContext::editing() const
{
    return _editing;
}

void ViewerModeContext::toggleEditMode()
{
    _editing = !_editing;
}

bool ViewerModeContext::cursorShown() const
{
    return _cursorShown;
}

void ViewerModeContext::setCursorShown(bool state)
{
    _cursorShown = state;

    render()->setDecorationShown(cursorID, _cursorShown);
}

const vec3& ViewerModeContext::cursorPosition() const
{
    return _cursorPosition;
}

void ViewerModeContext::setCursorPosition(const vec3& position)
{
    _cursorPosition = position;

    render()->updateDecoration<RenderCursor>(
        cursorID,
        _cursorPosition
    );
}

void ViewerModeContext::moveCursor(const Point& point)
{
    vec3 cameraPosition = _context->flatViewer()->position();
    vec3 cameraDirection = _context->flatViewer()->rotation().forward();

    vec3 direction = _context->flatViewer()->screenToWorld(point);

    if (roughly(direction.dot(cameraDirection), 0.0))
    {
        return;
    }

    f64 distance = (editMode()->selectionCenter() - cameraPosition).dot(cameraDirection) / direction.dot(cameraDirection);

    _cursorPosition = cameraPosition + direction * distance;

    render()->updateDecoration<RenderCursor>(
        cursorID,
        _cursorPosition
    );
}

bool ViewerModeContext::gridShown() const
{
    return _gridShown;
}

void ViewerModeContext::setGridShown(bool state)
{
    _gridShown = state;

    _context->flatViewer()->updatePerspectiveGrid();
}

const vec3& ViewerModeContext::gridPosition() const
{
    return _gridPosition;
}

void ViewerModeContext::setGridPosition(const vec3& grid)
{
    _gridPosition = grid;

    _context->flatViewer()->updatePerspectiveGrid();
}

f64 ViewerModeContext::gridSize() const
{
    return _gridSize;
}

void ViewerModeContext::setGridSize(f64 size)
{
    _gridSize = size;

    _context->flatViewer()->updatePerspectiveGrid();
}

bool ViewerModeContext::entityPointsShown() const
{
    return _entityPointsShown;
}

void ViewerModeContext::setEntityPointsShown(bool state)
{
    _entityPointsShown = state;
}

bool ViewerModeContext::gizmoShown() const
{
    return _gizmoShown;
}

void ViewerModeContext::setGizmoShown(bool state)
{
    _gizmoShown = state;
}

bool ViewerModeContext::spaceGraphVisualizerShown() const
{
    return _spaceGraphVisualizerShown;
}

void ViewerModeContext::setSpaceGraphVisualizerShown(bool state)
{
    _spaceGraphVisualizerShown = state;
    render()->setDecorationShown(spaceGraphVisualizerID, _spaceGraphVisualizerShown);
}

f64 ViewerModeContext::canvasPaintBrushRadius() const
{
    return _canvasPaintBrushRadius;
}

void ViewerModeContext::setCanvasPaintBrushRadius(f64 radius)
{
    _canvasPaintBrushRadius = radius;
}

f64 ViewerModeContext::translationStep() const
{
    return _translationStep;
}

void ViewerModeContext::setTranslationStep(f64 size)
{
    _translationStep = size;
}

f64 ViewerModeContext::rotationStep() const
{
    return _rotationStep;
}

void ViewerModeContext::setRotationStep(f64 size)
{
    _rotationStep = size;
}

f64 ViewerModeContext::scaleStep() const
{
    return _scaleStep;
}

void ViewerModeContext::setScaleStep(f64 size)
{
    _scaleStep = size;
}

void ViewerModeContext::undo()
{
    _context->controller()->send(WorldEvent(
        World_Event_Undo,
        _context->controller()->self()->world()
    ));
}

void ViewerModeContext::redo()
{
    _context->controller()->send(WorldEvent(
        World_Event_Redo,
        _context->controller()->self()->world()
    ));
}

bool ViewerModeContext::gamepadMenu() const
{
    return _gamepadMenu;
}

void ViewerModeContext::toggleGamepadMenu()
{
    _gamepadMenu = !_gamepadMenu;
}

FlatRender* ViewerModeContext::render() const
{
    return dynamic_cast<FlatRender*>(context()->controller()->render());
}

bool ViewerModeContext::modeHasChanged() const
{
    return (_mode == Viewer_Sub_Mode_Body && !inBodyMode()) ||
        (_mode == Viewer_Sub_Mode_Action && !inActionMode()) ||
        (_mode == Viewer_Sub_Mode_Entity && (inBodyMode() || inActionMode()));
}

bool ViewerModeContext::inBodyMode() const
{
    EntityEditor* editor = _context->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return false;
    }

    return editor->inBodyMode();
}

bool ViewerModeContext::inActionMode() const
{
    EntityEditor* editor = _context->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return false;
    }

    return editor->inActionMode();
}
