/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_types.hpp"
#include "input_scope.hpp"
#include "input_repeat.hpp"
#include "interface_types.hpp"

class ControlContext;

class InputManager
{
public:
    InputManager(ControlContext* ctx);
    InputManager(const InputManager& rhs) = delete;
    InputManager(InputManager&& rhs) = delete;
    ~InputManager();

    InputManager& operator=(const InputManager& rhs) = delete;
    InputManager& operator=(InputManager&& rhs) = delete;

    void init();

    void handle(const Input& input);

    vector<string> allBindings() const;
    InputScope* getScope(const string& name) const;
    void setBindings(const string& name, const vector<InputScopeBinding>& bindings);
    vector<InputScopeBinding> getBindings(const string& name) const;

private:
    ControlContext* ctx;
    InputScope* globalScope;
    map<string, vector<InputScopeBinding>> bindings;
    vector<InputRepeat> repeats;

    void applyBindings();
    void applyDefaultBindings();
    void applyNumpadAlternativeBindings();
    void applyCustomBindings();

    void createScopes();
    void destroyScopes();
    template<typename T = InputScope>
    InputScope* createScope(
        InputScope* parent,
        const string& name,
        InputScopeMode mode,
        const function<bool(const Input&)>& isActive,
        const function<void(InputScope*)>& block
    )
    {
        return new T(ctx, parent, name, mode, isActive, block);
    }

    u32 monitorRepeats(const Input& input);
    Point pointerPoint(const Input& input) const;

    void quit(const Input& input) const;
    void increaseVolume(const Input& input) const;
    void decreaseVolume(const Input& input) const;
    void toggleMicrophone(const Input& input) const;

#ifdef LMOD_VR
    void confirmTrackingCalibration(const Input& input) const;
    void cancelTrackingCalibration(const Input& input) const;
#endif

    void showKnowledgeViewer(const Input& input) const;

#ifdef LMOD_VR
    void startShowVRMenuLeft(const Input& input) const;
    void startShowVRMenuRight(const Input& input) const;
    void endShowVRMenuLeft(const Input& input) const;
    void endShowVRMenuRight(const Input& input) const;
#endif

    void showGamepadMenu(const Input& input) const;

    void toggleConsole(const Input& input) const;
    void toggleHelp(const Input& input) const;
    void toggleSettings(const Input& input) const;
    void toggleSystemSettings(const Input& input) const;
    void toggleNewTextTool(const Input& input) const;
    void toggleUserControlsTool(const Input& input) const;
    void toggleOracleManager(const Input& input) const;
    void toggleDocumentManager(const Input& input) const;
    void toggleQuickAccessor(const Input& input) const;
    void openEditor(const Input& input) const;

    void setMagnitude0(const Input& input) const;
    void setMagnitude1(const Input& input) const;
    void setMagnitude2(const Input& input) const;
    void setMagnitude3(const Input& input) const;

    void attach(const Input& input) const;
    void detach(const Input& input) const;

    void brake(const Input& input) const;
    void unbrake(const Input& input) const;

    void ping(const Input& input) const;

    void moveInterfacePointer(const Input& input) const;

#ifdef LMOD_VR
    void moveHead(const Input& input) const;
    void moveLeftHand(const Input& input) const;
    void moveRightHand(const Input& input) const;
    void moveHips(const Input& input) const;
    void moveLeftFoot(const Input& input) const;
    void moveRightFoot(const Input& input) const;
    void moveChest(const Input& input) const;
    void moveLeftShoulder(const Input& input) const;
    void moveRightShoulder(const Input& input) const;
    void moveLeftElbow(const Input& input) const;
    void moveRightElbow(const Input& input) const;
    void moveLeftWrist(const Input& input) const;
    void moveRightWrist(const Input& input) const;
    void moveLeftKnee(const Input& input) const;
    void moveRightKnee(const Input& input) const;
    void moveLeftAnkle(const Input& input) const;
    void moveRightAnkle(const Input& input) const;
    void vrStickMotion(const Input& input) const;
    void vrTouchpadMotion(const Input& input) const;
#endif

    void resetCamera(const Input& input) const;
    void resetCursor(const Input& input) const;

    void viewerStartForward(const Input& input) const;
    void viewerEndForward(const Input& input) const;
    void viewerStartBack(const Input& input) const;
    void viewerEndBack(const Input& input) const;
    void viewerStartLeft(const Input& input) const;
    void viewerEndLeft(const Input& input) const;
    void viewerStartRight(const Input& input) const;
    void viewerEndRight(const Input& input) const;
    void viewerStartUp(const Input& input) const;
    void viewerEndUp(const Input& input) const;
    void viewerStartDown(const Input& input) const;
    void viewerEndDown(const Input& input) const;
    void viewerStartShift(const Input& input) const;
    void viewerEndShift(const Input& input) const;
    void viewerStartZoom(const Input& input) const;
    void viewerEndZoom(const Input& input) const;
    void viewerStartRotate(const Input& input) const;
    void viewerEndRotate(const Input& input) const;
    void viewerStartSprint(const Input& input) const;
    void viewerEndSprint(const Input& input) const;
    void mouseMotion(const Input& input) const;
    void moveStickMotion(const Input& input) const;
    void lookStickMotion(const Input& input) const;

    void orthoLookUp(const Input& input) const;
    void orthoLookDown(const Input& input) const;
    void orthoLookLeft(const Input& input) const;
    void orthoLookRight(const Input& input) const;
    void orthoLookForward(const Input& input) const;
    void orthoLookBack(const Input& input) const;
    void orthoZoomIn(const Input& input) const;
    void orthoZoomOut(const Input& input) const;
    void orthoInvert(const Input& input) const;
    void orthoRotateUp(const Input& input) const;
    void orthoRotateDown(const Input& input) const;
    void orthoRotateLeft(const Input& input) const;
    void orthoRotateRight(const Input& input) const;
    void toggleOrthographic(const Input& input) const;
    void cameraToCenterOfSelection(const Input& input) const;
    void toFreeLook(const Input& input) const;

    void startSnap(const Input& input) const;
    void endSnap(const Input& input) const;

    void startSelect(const Input& input) const;
    void endAddSelect(const Input& input) const;
    void endRemoveSelect(const Input& input) const;
    void endReplaceSelect(const Input& input) const;
    void moveSelect(const Input& input) const;

    void startSpecialEdit(const Input& input) const;
    void startAltSpecialEdit(const Input& input) const;
    void endSpecialEdit(const Input& input) const;
    void moveSpecialEdit(const Input& input) const;

    void selectLinked(const Input& input) const;
    void selectAll(const Input& input) const;
    void deselectAll(const Input& input) const;
    void invertSelection(const Input& input) const;

    void toggleEditMode(const Input& input) const;
    void moveCursor(const Input& input) const;

    void translate(const Input& input) const;
    void rotate(const Input& input) const;
    void scale(const Input& input) const;
    void store(const Input& input) const;
    void reset(const Input& input) const;
    void originTo3DCursor(const Input& input) const;
    void originToBody(const Input& input) const;
    void bodyToOrigin(const Input& input) const;
    void resetPosition(const Input& input) const;
    void resetRotation(const Input& input) const;
    void resetScale(const Input& input) const;
    void resetLinearVelocity(const Input& input) const;
    void resetAngularVelocity(const Input& input) const;
    void addKeyframes(const Input& input) const;
    void removeKeyframes(const Input& input) const;
    void cancelTool(const Input& input) const;
    void endTool(const Input& input) const;
    void useTool(const Input& input) const;
    void toggleX(const Input& input) const;
    void toggleY(const Input& input) const;
    void toggleZ(const Input& input) const;
    void togglePivotMode(const Input& input) const;

    void cursorToSelection(const Input& input) const;
    void selectionToCursor(const Input& input) const;
    void duplicate(const Input& input) const;
    void extrude(const Input& input) const;
    void split(const Input& input) const;
    void join(const Input& input) const;
    void addChildren(const Input& input) const;
    void removeParent(const Input& input) const;
    void hideSelected(const Input& input) const;
    void hideUnselected(const Input& input) const;
    void unhideAll(const Input& input) const;
    void save(const Input& input) const;
    void redo(const Input& input) const;
    void undo(const Input& input) const;
    void cut(const Input& input) const;
    void copy(const Input& input) const;
    void paste(const Input& input) const;
    void addNewPrefab(const Input& input) const;
    void addNew(const Input& input) const;
    void removeSelected(const Input& input) const;

    void addNumericInput0(const Input& input) const;
    void addNumericInput1(const Input& input) const;
    void addNumericInput2(const Input& input) const;
    void addNumericInput3(const Input& input) const;
    void addNumericInput4(const Input& input) const;
    void addNumericInput5(const Input& input) const;
    void addNumericInput6(const Input& input) const;
    void addNumericInput7(const Input& input) const;
    void addNumericInput8(const Input& input) const;
    void addNumericInput9(const Input& input) const;
    void removeNumericInput(const Input& input) const;
    void toggleNumericInputSign(const Input& input) const;
    void ensureNumericInputPeriod(const Input& input) const;

    void toggleHUD(const Input& input) const;
    void toggleFullscreen(const Input& input) const;
};
