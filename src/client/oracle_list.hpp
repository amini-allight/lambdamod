/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

#include "list_view.hpp"

class OracleList : public InterfaceWidget
{
public:
    OracleList(
        InterfaceWidget* parent,
        const function<vector<string>()>& source,
        const function<void(const string&)>& onSelect,
        const function<void()>& onRoll
    );

    void select(i32 index);

    void step() override;

private:
    vector<string> items;
    i32 activeIndex;

    ListView* listView;

    function<vector<string>()> source;
    function<void(const string&)> _onSelect;

    void updateListView();

    void onSelect(i32 index);

    SizeProperties sizeProperties() const override;
};
