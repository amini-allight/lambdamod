/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "oracle_manager.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "list_view.hpp"
#include "row.hpp"
#include "control_context.hpp"
#include "oracles.hpp"
#include "oracle_list.hpp"
#include "oracle_table.hpp"
#include "spacer.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(760, 424) * uiScale();
}

OracleManager::OracleManager(InterfaceView* view)
    : InterfaceWindow(view, localize("oracle-manager-oracle-manager"))
    , activeIndex(0)
{
    START_WIDGET_SCOPE("oracle-manager")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-oracle-manager", dismiss)
    END_SCOPE

    auto row = new Row(this);

    new OracleList(
        row,
        bind(&OracleManager::oraclesSource, this),
        bind(&OracleManager::onSelect, this, placeholders::_1),
        bind(&OracleManager::onRoll, this)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    oracleTable = new OracleTable(
        row,
        bind(&OracleManager::oracleSource, this)
    );
}

void OracleManager::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());
}

void OracleManager::open(const Point& point, const string& oracleName)
{
    open(point);

    onSelect(oracleName);
}

vector<string> OracleManager::oraclesSource()
{
    vector<string> names;
    names.reserve(oracles.size());

    for (const Oracle& oracle : oracles)
    {
        names.push_back(oracle.name);
    }

    return names;
}

Oracle OracleManager::oracleSource()
{
    if (oracles.empty())
    {
        return Oracle();
    }

    return oracles[activeIndex];
}

void OracleManager::onSelect(const string& name)
{
    for (size_t i = 0; i < oracles.size(); i++)
    {
        if (oracles[i].name == name)
        {
            activeIndex = i;
            break;
        }
    }
}

void OracleManager::onRoll()
{
    oracleTable->roll();
}

void OracleManager::dismiss(const Input& input)
{
    hide();
}
