/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_parts.hpp"
#include "body_render_data.hpp"

BodyRenderData generateLine(const mat4& transform, const BodyPartLine& line);
BodyRenderData generatePlane(const mat4& transform, const BodyPartPlane& plane);
BodyRenderData generateSolid(const mat4& transform, const BodyPartSolid& solid);
BodyRenderData generateText(const mat4& transform, const BodyPartText& text);
BodyRenderData generateSymbol(const mat4& transform, const BodyPartSymbol& symbol);
BodyRenderData generateCanvas(const mat4& transform, const BodyPartCanvas& canvas);
BodyRenderData generateStructure(const mat4& transform, const BodyPartStructure& structure);
BodyRenderData generateTerrain(const mat4& transform, const BodyPartTerrain& terrain);
BodyRenderData generateLight(const mat4& transform, const BodyPartLight& light);
BodyRenderData generateArea(const mat4& transform, const BodyPartArea& area);
BodyRenderData generateRope(const mat4& transform, const BodyPartRope& rope);
