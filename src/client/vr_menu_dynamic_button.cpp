/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_menu_dynamic_button.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "theme.hpp"
#include "tools.hpp"
#include "control_context.hpp"

VRMenuDynamicButton::VRMenuDynamicButton(
    InterfaceWidget* parent,
    const function<string()>& textSource,
    const function<void()>& onActivate,
    const function<bool()>& emphasisSource
)
    : InterfaceWidget(parent)
    , textSource(textSource)
    , onActivate(onActivate)
    , emphasisSource(emphasisSource)
    , lastButtonTime(0)
{
    START_WIDGET_SCOPE("vr-menu-button")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void VRMenuDynamicButton::draw(const DrawContext& ctx) const
{
    Point point(UIScale::panelBorderSize(this));
    Size size = this->size() - (UIScale::panelBorderSize(this) * 2);

    drawSolid(
        ctx,
        this,
        point,
        size,
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        point,
        size,
        UIScale::panelBorderSize(this),
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    TextStyle style(this);
    style.alignment = Text_Center;
    style.size = UIScale::vrMenuButtonFontSize(this);
    style.weight = emphasisSource && emphasisSource() ? Text_Bold : Text_Regular;
    style.color = focused() ? theme.accentColor : theme.panelForegroundColor;

    drawText(
        ctx,
        this,
        point,
        size,
        textSource(),
        style
    );
}

void VRMenuDynamicButton::interact(const Input& input)
{
    if (currentTime() - lastButtonTime < minButtonInterval)
    {
        return;
    }

    playPositiveActivateEffect();
    onActivate();

    lastButtonTime = currentTime();
}

SizeProperties VRMenuDynamicButton::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::panelVRMenuButtonSize(this).w), static_cast<f64>(UIScale::panelVRMenuButtonSize(this).h),
        Scaling_Fixed, Scaling_Fill
    };
}
#endif
