/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "sound_source.hpp"
#include "sound_entity_playback.hpp"

class Sound;
class Entity;

class SoundEntity
{
public:
    SoundEntity(Sound* sound, const Entity* entity);

    void update(const Entity* entity);
    void output(f64 multiplier);
    void recordLastDistance();

    void add(EntityID parentID, const Entity* entity);
    void remove(EntityID parentID, EntityID id);
    void update(EntityID parentID, const Entity* entity);

private:
    Sound* sound;
    EntityID id;

    bool audible;
    map<SamplePlaybackID, SoundEntityPlayback> playbacks;

    map<EntityID, SoundEntity> children;
};
