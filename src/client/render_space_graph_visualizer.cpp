/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_space_graph_visualizer.hpp"
#include "render_constants.hpp"
#include "render_tools.hpp"
#include "space_graph.hpp"
#include "theme.hpp"

RenderSpaceGraphVisualizer::RenderSpaceGraphVisualizer(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const SpaceGraph* spaceGraph 
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    vector<ColoredVertex> vertexData;

    auto addBoxMesh = [&vertexData](const vec3& minExtent, const vec3& maxExtent) -> void
    {
        fvec3 topFrontLeft(minExtent.x, maxExtent.y, maxExtent.z);
        fvec3 topFrontRight(maxExtent.x, maxExtent.y, maxExtent.z);
        fvec3 topBackRight(maxExtent.x, minExtent.y, maxExtent.z);
        fvec3 topBackLeft(minExtent.x, minExtent.y, maxExtent.z);

        fvec3 bottomFrontLeft(minExtent.x, maxExtent.y, minExtent.z);
        fvec3 bottomFrontRight(maxExtent.x, maxExtent.y, minExtent.z);
        fvec3 bottomBackRight(maxExtent.x, minExtent.y, minExtent.z);
        fvec3 bottomBackLeft(minExtent.x, minExtent.y, minExtent.z);

        vertexData.push_back({ topFrontLeft, theme.accentColor.toVec3() });
        vertexData.push_back({ topFrontRight, theme.accentColor.toVec3() });

        vertexData.push_back({ topFrontRight, theme.accentColor.toVec3() });
        vertexData.push_back({ topBackRight, theme.accentColor.toVec3() });

        vertexData.push_back({ topBackRight, theme.accentColor.toVec3() });
        vertexData.push_back({ topBackLeft, theme.accentColor.toVec3() });

        vertexData.push_back({ topBackLeft, theme.accentColor.toVec3() });
        vertexData.push_back({ topFrontLeft, theme.accentColor.toVec3() });

        vertexData.push_back({ topFrontLeft, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomFrontLeft, theme.accentColor.toVec3() });

        vertexData.push_back({ topFrontRight, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomFrontRight, theme.accentColor.toVec3() });

        vertexData.push_back({ topBackRight, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomBackRight, theme.accentColor.toVec3() });

        vertexData.push_back({ topBackLeft, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomBackLeft, theme.accentColor.toVec3() });

        vertexData.push_back({ bottomFrontLeft, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomFrontRight, theme.accentColor.toVec3() });

        vertexData.push_back({ bottomFrontRight, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomBackRight, theme.accentColor.toVec3() });

        vertexData.push_back({ bottomBackRight, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomBackLeft, theme.accentColor.toVec3() });

        vertexData.push_back({ bottomBackLeft, theme.accentColor.toVec3() });
        vertexData.push_back({ bottomFrontLeft, theme.accentColor.toVec3() });
    };

    if (spaceGraph)
    {
        spaceGraph->traverse([addBoxMesh](const uvec3& index, const SpaceGraphNode* node) -> void {
            vec3 minExtent = node->minExtent();
            vec3 maxExtent = node->maxExtent();

            addBoxMesh(minExtent, maxExtent);
        });
    }
    else
    {
        addBoxMesh(vec3(numeric_limits<f64>::lowest()), vec3(numeric_limits<f64>::max()));
    }

    vertices = createVertexBuffer(vertexData.size() * sizeof(ColoredVertex));
    vertexCount = vertexData.size();

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderSpaceGraphVisualizer::~RenderSpaceGraphVisualizer()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderSpaceGraphVisualizer::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderSpaceGraphVisualizer::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
