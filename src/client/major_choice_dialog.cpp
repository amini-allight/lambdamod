/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "major_choice_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "list_view.hpp"
#include "line_edit.hpp"
#include "label.hpp"
#include "close_button.hpp"
#include "text_button.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(256, 384) * uiScale();
}

MajorChoiceDialog::MajorChoiceDialog(InterfaceView* view, vector<tuple<string, string, string>>* options)
    : Dialog(view, dialogSize())
    , options(options)
{
    auto column = new Column(this);

    new Label(
        column,
        localize("major-choice-dialog-choices")
    );

    listView = new ListView(
        column,
        [](size_t index) -> void {},
        theme.altBackgroundColor
    );

    new EditorEntry(column, "major-choice-dialog-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("major-choice-dialog-title"));
    });

    new EditorEntry(column, "major-choice-dialog-subtitle", [this](InterfaceWidget* parent) -> void {
        subtitle = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        subtitle->setPlaceholder(localize("major-choice-dialog-subtitle"));
    });

    new EditorEntry(column, "major-choice-dialog-text", [this](InterfaceWidget* parent) -> void {
        text = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        text->setPlaceholder(localize("major-choice-dialog-text"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("major-choice-dialog-add"),
            bind(&MajorChoiceDialog::onAdd, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());

    updateListView();
}

void MajorChoiceDialog::onAdd()
{
    options->push_back({
        title->content(),
        subtitle->content(),
        text->content()
    });

    updateListView();

    title->clear();
    subtitle->clear();
    text->clear();
}

void MajorChoiceDialog::onRemove(size_t index)
{
    if (index >= options->size())
    {
        return;
    }

    options->erase(options->begin() + index);

    updateListView();
}

void MajorChoiceDialog::updateListView()
{
    listView->clearChildren();

    size_t i = 0;
    for (const auto& [ title, subtitle, text ] : *options)
    {
        auto row = new Row(listView);

        new Label(
            row,
            title
        );
        new Label(
            row,
            subtitle
        );
        new Label(
            row,
            text
        );
        new CloseButton(
            row,
            bind(&MajorChoiceDialog::onRemove, this, i)
        );

        i++;
    }

    listView->update();
}
