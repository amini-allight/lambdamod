/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_pivot.hpp"
#include "tools.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr u32 curveSegments = 32;
static constexpr f64 indicatorRadius = 0.1;
static constexpr f64 indicatorHeight = 0.02;

static vector<ColoredVertex> generateIndicator(f64 min, f64 max, const vec3& color)
{
    vector<ColoredVertex> vertices;

    {
        fquaternion rotation(forwardDir, min);

        vec3 top = { 0, +indicatorHeight / 2, -indicatorRadius };
        vec3 bottom = { 0, -indicatorHeight / 2, -indicatorRadius };

        vertices.push_back({ rotation.rotate(top), color });
        vertices.push_back({ rotation.rotate(bottom), color });
    }

    f64 totalAngle = abs(min) + abs(max);

    for (u32 i = 0; i < curveSegments; i++)
    {
        f64 currAngle = min + (i * (totalAngle / curveSegments));
        f64 nextAngle = min + ((i + 1) * (totalAngle / curveSegments));

        vertices.push_back({
            fquaternion(forwardDir, currAngle).rotate(fvec3(0, 0, -indicatorRadius)),
            color
        });

        vertices.push_back({
            fquaternion(forwardDir, nextAngle).rotate(fvec3(0, 0, -indicatorRadius)),
            color
        });
    }

    {
        fquaternion rotation(forwardDir, max);

        fvec3 top(0, +indicatorHeight / 2, -indicatorRadius);
        fvec3 bottom(0, -indicatorHeight / 2, -indicatorRadius);

        vertices.push_back({ rotation.rotate(top), color });
        vertices.push_back({ rotation.rotate(bottom), color });
    }

    return vertices;
}

RenderPivot::RenderPivot(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    const vec3& min,
    const vec3& max
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
    , min(min)
    , max(max)

{
    transform = { position, rotation, vec3(1) };

    vertexCount = ((min.x != max.x) + (min.y != max.y) + (min.z != max.z)) * (2 + (curveSegments * 2) + 2);

    if (vertexCount != 0)
    {
        vertices = createVertexBuffer(vertexCount * sizeof(fvec3));

        VmaMapping<f32> mapping(ctx->allocator, vertices);

        vector<ColoredVertex> vertexData;

        if (min.x != max.x)
        {
            vector<ColoredVertex> indicator = generateIndicator(min.x, max.x, theme.xAxisColor.toVec3());

            for (ColoredVertex& vertex : indicator)
            {
                vertex.position = fquaternion(upDir, radians(90)).rotate(vertex.position);
            }

            vertexData.insert(
                vertexData.end(),
                indicator.begin(),
                indicator.end()
            );
        }

        if (min.y != max.y)
        {
            vector<ColoredVertex> indicator = generateIndicator(min.y, max.y, theme.yAxisColor.toVec3());

            // No need to rotate, this is the default

            vertexData.insert(
                vertexData.end(),
                indicator.begin(),
                indicator.end()
            );
        }

        if (min.z != max.z)
        {
            vector<ColoredVertex> indicator = generateIndicator(min.z, max.z, theme.zAxisColor.toVec3());

            for (ColoredVertex& vertex : indicator)
            {
                vertex.position = fquaternion(rightDir, radians(90)).rotate(vertex.position);
            }

            vertexData.insert(
                vertexData.end(),
                indicator.begin(),
                indicator.end()
            );
        }

        memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));
    }

    createComponents(swapchainElements);
}

RenderPivot::~RenderPivot()
{
    destroyComponents();

    if (vertices.buffer != nullptr)
    {
        destroyBuffer(ctx->allocator, vertices);
    }
}

void RenderPivot::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderPivot::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
