/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "line_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "tools.hpp"
#include "clipboard.hpp"

LineEdit::LineEdit(
    InterfaceWidget* parent,
    const function<string()>& source,
    const function<void(const string&)>& onReturn,
    bool codeMode
)
    : InterfaceWidget(parent)
    , source(source)
    , onReturn(onReturn)
    , codeMode(codeMode)
    , text(source ? source() : "")
    , _cursorIndex(static_cast<i32>(text.size()))
    , _selectIndex(0)
    , _scrollIndex(0)
    , selected(false)
    , selecting(false)
    , lastClickTime(0)
{
    START_WIDGET_SCOPE("line-edit")
        WIDGET_SLOT("text-select-all", selectAll)
        WIDGET_SLOT("cursor-to-previous-word", cursorToPreviousWord)
        WIDGET_SLOT("cursor-to-next-word", cursorToNextWord)
        WIDGET_SLOT("select-left", selectLeft)
        WIDGET_SLOT("select-right", selectRight)
        WIDGET_SLOT("select-to-previous-word", selectToPreviousWord)
        WIDGET_SLOT("select-to-next-word", selectToNextWord)
        WIDGET_SLOT("cursor-left", cursorLeft)
        WIDGET_SLOT("cursor-right", cursorRight)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)
        WIDGET_SLOT("remove-previous-word", removePreviousWord)
        WIDGET_SLOT("remove-next-word", removeNextWord)
        WIDGET_SLOT("remove-to-start", removeToStart)
        WIDGET_SLOT("remove-to-end", removeToEnd)
        WIDGET_SLOT("remove-previous-character", removePreviousCharacter)
        WIDGET_SLOT("remove-next-character", removeNextCharacter)
        WIDGET_SLOT("upcase-next-word", upcaseNextWord)
        WIDGET_SLOT("downcase-next-word", downcaseNextWord)
        WIDGET_SLOT("swap-words", swapWords)
        WIDGET_SLOT("swap-characters", swapCharacters)
        WIDGET_SLOT("select-to", selectTo)
        WIDGET_SLOT("cursor-to", cursorTo)
        WIDGET_SLOT("add-text", addText)
        WIDGET_SLOT("cut", cut)
        WIDGET_SLOT("copy", copy)
        WIDGET_SLOT("paste", paste)
#if (defined(__linux__) || defined(__FreeBSD__))
        WIDGET_SLOT("paste-primary", pastePrimary)
#endif

        START_INDEPENDENT_SCOPE("selecting", selecting)
            WIDGET_SLOT("end-replace-select", endSelect)
            WIDGET_SLOT("move-select", moveSelect)
        END_SCOPE
    END_SCOPE

    if (codeMode)
    {
        updateSyntaxHighlight();
    }
}

void LineEdit::set(const string& text)
{
    setText(text);
}

const string& LineEdit::content() const
{
    return text;
}

i32 LineEdit::cursorIndex() const
{
    return _cursorIndex;
}

i32 LineEdit::scrollIndex() const
{
    return _scrollIndex;
}

bool LineEdit::empty() const
{
    return text.empty();
}

void LineEdit::clear()
{
    setText("");
}

void LineEdit::setPlaceholder(const string& text)
{
    placeholder = text;
}

void LineEdit::resize(const Size& size)
{
    InterfaceWidget::resize(size);
    correct();
}

void LineEdit::step()
{
    InterfaceWidget::step();

    // Prevent update while editing
    if (focused())
    {
        return;
    }

    if (source)
    {
        string newText = source();

        if (newText != text)
        {
            setText(newText);
        }
    }
}

void LineEdit::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.indentColor
    );

    if (selected)
    {
        i32 start = clamp(selectionStart(), _scrollIndex, _scrollIndex + visualColumns()) - _scrollIndex;
        i32 end = clamp(selectionEnd(), _scrollIndex, _scrollIndex + visualColumns()) - _scrollIndex;

        drawSolid(
            ctx,
            this,
            Point(UIScale::marginSize() + (start * charWidth()), (size().h - charHeight()) / 2),
            Size((end - start) * charWidth(), charHeight()),
            theme.accentColor
        );
    }

    TextStyle style;

    if (text.empty())
    {
        style.color = theme.inactiveTextColor;
    }

    if (codeMode && !text.empty())
    {
        for (const SyntaxHighlightChunk& chunk : syntaxHighlight)
        {
            i32 start = chunk.start;
            i32 end = chunk.start + chunk.size;

            if (start < _scrollIndex && end < _scrollIndex)
            {
                continue;
            }

            if (start >= _scrollIndex + visualColumns() && end >= _scrollIndex + visualColumns())
            {
                continue;
            }

            i32 startX = clamp(start - _scrollIndex, 0, visualColumns());
            i32 endX = clamp(end - _scrollIndex, 0, visualColumns());

            i32 textOffset = start + max(_scrollIndex - start, 0);
            i32 textRemaining = min(end - _scrollIndex, end - start);

            TextStyle style;
            style.color = syntaxHighlightColor(chunk.type);
            style.alignment = Text_Center;

            drawText(
                ctx,
                this,
                Point(UIScale::marginSize() + (startX * charWidth()), 0),
                Size((endX - startX) * charWidth(), size().h),
                text.substr(textOffset, textRemaining),
                style
            );
        }

        auto [ start, size ] = bracketMatchHighlight(
            text,
            _cursorIndex,
            0,
            static_cast<i32>(text.size())
        );

        i32 end = start + size;

        i32 startX = clamp(start - _scrollIndex, 0, visualColumns());
        i32 endX = clamp(end - _scrollIndex, 0, visualColumns());

        drawSolid(
            ctx,
            this,
            Point(UIScale::marginSize() + (startX * charWidth()), (this->size().h - charHeight()) / 2),
            Size((endX - startX) * charWidth(), charHeight()),
            theme.bracketHighlightColor
        );
    }
    else
    {
        drawText(
            ctx,
            this,
            text.empty() ? placeholder : text.substr(_scrollIndex, static_cast<i32>(text.size()) - _scrollIndex),
            style
        );
    }

    if (focused() && _cursorIndex >= _scrollIndex && _cursorIndex < _scrollIndex + visualColumns())
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::marginSize() + (charWidth() * (_cursorIndex - _scrollIndex)), (size().h - charHeight()) / 2),
            Size(UIScale::textCursorWidth(), charHeight()),
            theme.textColor
        );
    }
}

void LineEdit::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void LineEdit::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void LineEdit::add(i32 index, const string& s)
{
    bool after = index > _cursorIndex;

    if (selected)
    {
        bool after = index >= _cursorIndex;
        i32 beforeIndex = _cursorIndex;

        remove(selectionStart(), selectionEnd() - selectionStart());

        i32 afterIndex = _cursorIndex;
        if (after)
        {
            index += afterIndex - beforeIndex;
        }
    }

    setText(text.substr(0, index) + s + text.substr(index, static_cast<i32>(text.size()) - index), after);
}

void LineEdit::add(i32 index, char c)
{
    add(index, string() + c);
}

void LineEdit::remove(i32 start, i32 count)
{
    bool after = start >= _cursorIndex;

    // Remove selection
    if (selected)
    {
        start = selectionStart();
        count = selectionEnd() - selectionStart();
        after = _cursorIndex < _selectIndex;
        selected = false;
    }

    if (start < 0 || start + count > static_cast<i32>(text.size()))
    {
        return;
    }

    setText(text.substr(0, start) + text.substr(start + count, static_cast<i32>(text.size()) - (start + count)), after);
}

void LineEdit::cursorLeft()
{
    if (_cursorIndex == 0)
    {
        return;
    }

    selected = false;
    _cursorIndex--;
    correct();
}

void LineEdit::cursorRight()
{
    if (_cursorIndex == static_cast<i32>(text.size()))
    {
        return;
    }

    selected = false;
    _cursorIndex++;
    correct();
}

void LineEdit::selectLeft()
{
    if (_cursorIndex == 0)
    {
        return;
    }

    _cursorIndex--;
    correct();
}

void LineEdit::selectRight()
{
    if (_cursorIndex == static_cast<i32>(text.size()))
    {
        return;
    }

    _cursorIndex++;
    correct();
}

void LineEdit::goTo(i32 index)
{
    selected = false;
    _cursorIndex = index;
    correct();
}

void LineEdit::selectTo(i32 index)
{
    _cursorIndex = index;
    correct();
}

void LineEdit::startSelect(const Point& pointer)
{
    selecting = true;

    selectTo(screenToIndex(pointer));

    selected = true;
    _selectIndex = _cursorIndex;
    correct();
}

void LineEdit::endSelect(const Input& input, const Point& pointer)
{
    if (!selecting)
    {
        return;
    }

    selected = _cursorIndex != _selectIndex;

    selectTo(screenToIndex(pointer));

    selecting = false;

#if (defined(__linux__) || defined(__FreeBSD__))
    setClipboard(input, text.substr(selectionStart(), selectionEnd() - selectionStart()), true);
#endif
}

void LineEdit::moveSelect(const Point& pointer)
{
    if (!selecting)
    {
        return;
    }

    selectTo(screenToIndex(pointer));

    selected = true;
}

i32 LineEdit::charWidth() const
{
    return TextStyle().getFont()->monoWidth();
}

i32 LineEdit::charHeight() const
{
    return TextStyle().getFont()->height();
}

i32 LineEdit::visualColumns() const
{
    return (size().w - (UIScale::marginSize() * 2)) / charWidth();
}

string LineEdit::wordAtCursor() const
{
    i32 start = wordStart();
    i32 end = wordEnd();

    return text.substr(start - lineStart(), end - start);
}

i32 LineEdit::softLineStart() const
{
    return lineIndent() * indentSize;
}

i32 LineEdit::lineStart() const
{
    return 0;
}

i32 LineEdit::lineEnd() const
{
    return text.size();
}

i32 LineEdit::lineIndent() const
{
    i32 count = 0;

    for (i32 i = 0; i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (c == ' ')
        {
            count++;
        }
        else
        {
            break;
        }
    }

    return count / indentSize;
}

i32 LineEdit::previousWordStart() const
{
    bool word = false;

    for (i32 i = wordStart() - 1; i >= 0; i--)
    {
        char c = text[i];

        if (!isWordBoundary(c))
        {
            word = true;
        }
        else if (word && isWordBoundary(c))
        {
            return i + 1;
        }
    }

    return 0;
}

i32 LineEdit::previousWordEnd() const
{
    for (i32 i = previousWordStart(); i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(text.size());
}

i32 LineEdit::wordStart() const
{
    bool word = false;

    for (i32 i = _cursorIndex - 1; i >= 0; i--)
    {
        char c = text[i];

        if (!isWordBoundary(c))
        {
            word = true;
        }
        else if (word && isWordBoundary(c))
        {
            return i + 1;
        }
    }

    return 0;
}

i32 LineEdit::wordEnd() const
{
    bool word = false;

    for (i32 i = _cursorIndex; i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (!isWordBoundary(c))
        {
            word = true;
        }
        else if (word && isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(text.size());
}

i32 LineEdit::nextWordStart() const
{
    bool betweenWords = false;

    for (i32 i = wordEnd(); i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (isWordBoundary(c))
        {
            betweenWords = true;
        }
        else if (betweenWords && !isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(text.size());
}

i32 LineEdit::nextWordEnd() const
{
    for (i32 i = nextWordStart(); i < static_cast<i32>(text.size()); i++)
    {
        char c = text[i];

        if (isWordBoundary(c))
        {
            return i;
        }
    }

    return static_cast<i32>(text.size());
}

i32 LineEdit::previousCharacter() const
{
    return _cursorIndex - 1;
}

i32 LineEdit::nextCharacter() const
{
    return _cursorIndex;
}

i32 LineEdit::selectionStart() const
{
    return _cursorIndex < _selectIndex ? _cursorIndex : _selectIndex;
}

i32 LineEdit::selectionEnd() const
{
    return _cursorIndex > _selectIndex ? _cursorIndex : _selectIndex;
}

i32 LineEdit::screenToIndex(const Point& point) const
{
    return _scrollIndex + (point.x - UIScale::marginSize()) / charWidth();
}

void LineEdit::setText(const string& text, bool after)
{
    i32 sizeChange = static_cast<i32>(text.size()) - static_cast<i32>(this->text.size());

    this->text = text;

    if (!after)
    {
        _cursorIndex += sizeChange;
    }

    correct();

    updateSyntaxHighlight();
}

void LineEdit::correct()
{
    _cursorIndex = clamp(_cursorIndex, 0, static_cast<i32>(text.size()));
    _selectIndex = clamp(_selectIndex, 0, static_cast<i32>(text.size()));
    _scrollIndex = clamp(_scrollIndex, 0, max((static_cast<i32>(text.size()) - visualColumns()) + 1, 0));

    if (_cursorIndex >= (_scrollIndex + visualColumns()) - 1)
    {
        _scrollIndex = (_cursorIndex - visualColumns()) + 1;
    }
    else if (_cursorIndex < _scrollIndex)
    {
        _scrollIndex = _cursorIndex;
    }
}

void LineEdit::updateSyntaxHighlight()
{
    if (!codeMode)
    {
        return;
    }

    syntaxHighlight = calculateSyntaxHighlight(text);
}

void LineEdit::selectAll(const Input& input)
{
    selected = true;
    _selectIndex = 0;
    _cursorIndex = text.size();
    correct();
}

void LineEdit::cursorToPreviousWord(const Input& input)
{
    goTo(wordStart());
}

void LineEdit::cursorToNextWord(const Input& input)
{
    goTo(nextWordStart());
}

void LineEdit::selectLeft(const Input& input)
{
    selectLeft();
}

void LineEdit::selectRight(const Input& input)
{
    selectRight();
}

void LineEdit::selectToPreviousWord(const Input& input)
{
    selectTo(wordStart());
}

void LineEdit::selectToNextWord(const Input& input)
{
    selectTo(nextWordStart());
}

void LineEdit::cursorLeft(const Input& input)
{
    cursorLeft();
}

void LineEdit::cursorRight(const Input& input)
{
    cursorRight();
}

void LineEdit::goToStart(const Input& input)
{
    goTo(lineStart());
}

void LineEdit::goToEnd(const Input& input)
{
    goTo(lineEnd());
}

void LineEdit::removePreviousWord(const Input& input)
{
    remove(wordStart(), static_cast<i32>(wordAtCursor().size()));
}

void LineEdit::removeNextWord(const Input& input)
{
    remove(_cursorIndex, wordEnd() - _cursorIndex);
}

void LineEdit::removeToStart(const Input& input)
{
    remove(0, _cursorIndex);
}

void LineEdit::removeToEnd(const Input& input)
{
    remove(_cursorIndex, static_cast<i32>(text.size()) - _cursorIndex);
}

void LineEdit::removePreviousCharacter(const Input& input)
{
    bool unindent = _cursorIndex == softLineStart() && softLineStart() >= lineStart() + indentSize;

    remove(
        unindent ? _cursorIndex - indentSize : previousCharacter(),
        unindent ? indentSize : 1
    );
}

void LineEdit::removeNextCharacter(const Input& input)
{
    remove(nextCharacter(), 1);
}

void LineEdit::upcaseNextWord(const Input& input)
{
    selected = false;
    for (i32 i = _cursorIndex; i < wordEnd(); i++)
    {
        char c = text[i];

        if (c >= 'a' && c <= 'z')
        {
            text[i] = c - ('a' - 'A');
        }
    }

    goTo(wordEnd());
}

void LineEdit::downcaseNextWord(const Input& input)
{
    selected = false;
    for (i32 i = _cursorIndex; i < wordEnd(); i++)
    {
        char c = text[i];

        if (c >= 'A' && c <= 'Z')
        {
            text[i] = c + ('a' - 'A');
        }
    }

    goTo(wordEnd());
}

void LineEdit::swapWords(const Input& input)
{
    selected = false;
    i32 end = wordEnd();

    string a = text.substr(previousWordStart(), previousWordEnd() - previousWordStart());
    string b = text.substr(wordStart(), wordEnd() - wordStart());
    string c = text.substr(previousWordEnd(), wordStart() - previousWordEnd());

    text = text.substr(0, previousWordStart()) + b + c + a + text.substr(wordEnd(), text.size() - wordEnd());
    goTo(end);
}

void LineEdit::swapCharacters(const Input& input)
{
    selected = false;
    if (_cursorIndex != 0 && _cursorIndex != static_cast<i32>(text.size()))
    {
        swap(text[_cursorIndex - 1], text[_cursorIndex]);
        cursorRight();
    }
}

void LineEdit::selectTo(const Input& input)
{
    Point local = pointer - position();

    selectTo(screenToIndex(local));
}

void LineEdit::cursorTo(const Input& input)
{
    Point local = pointer - position();

    if (currentTime() - lastClickTime <= multiClickInterval)
    {
        if (!selected)
        {
            selected = true;
            _selectIndex = wordStart();
            _cursorIndex = wordEnd();
            correct();
        }
        else if (selectionStart() > lineStart() || selectionEnd() < lineEnd())
        {
            selected = true;
            _selectIndex = lineStart();
            _cursorIndex = lineEnd();
            correct();
        }
        else
        {
            goTo(screenToIndex(local));
        }
    }
    else
    {
        startSelect(local);
    }

    lastClickTime = currentTime();
}

void LineEdit::endSelect(const Input& input)
{
    Point local = pointer - position();

    endSelect(input, local);
}

void LineEdit::moveSelect(const Input& input)
{
    Point local = pointer - position();

    moveSelect(local);
}

void LineEdit::addText(const Input& input)
{
    if (input.text() == "\t")
    {
        add(_cursorIndex, string(indentSize, ' '));
    }
    else if (input.text() == "\n")
    {
        if (onReturn)
        {
            onReturn(text);
        }
    }
    else
    {
        add(_cursorIndex, input.text());
    }
}

void LineEdit::cut(const Input& input)
{
    setClipboard(input, text.substr(selectionStart(), selectionEnd() - selectionStart()));
    remove(selectionStart(), selectionEnd() - selectionStart());
}

void LineEdit::copy(const Input& input)
{
    setClipboard(input, text.substr(selectionStart(), selectionEnd() - selectionStart()));
}

void LineEdit::paste(const Input& input)
{
    add(_cursorIndex, getClipboard());
}

#if (defined(__linux__) || defined(__FreeBSD__))
void LineEdit::pastePrimary(const Input& input)
{
    Point local = pointer - position();

    goTo(screenToIndex(local));
    add(_cursorIndex, getClipboard(true));
}
#endif

SizeProperties LineEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
