/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "sample.hpp"
#include "sample_part.hpp"
#include "sample_preview_playback.hpp"

class Column;
class SampleTimeline;
class SampleEditorSidePanel;

class SampleEditor : public InterfaceWidget
{
public:
    SampleEditor(InterfaceWidget* parent, EntityID id);

    void open(const string& name);

    void move(const Point& position) override;
    void resize(const Size& size) override;

private:
    friend class SampleEditorSidePanel;

    EntityID id;
    string name;
    set<SamplePartID> activeParts;

    Column* column;
    SampleTimeline* timeline;
    SampleEditorSidePanel* sidePanel;
    SamplePreviewPlayback previewPlayback;

    vec3 lastPosition;
    quaternion lastRotation;
    vec3 lastLinearVelocity;
    map<SamplePartID, SamplePart> lastParts;

    u32 playPositionSource();
    void onSeek(u32 position);
    void onSelect(const set<SamplePartID>& parts);

    bool playingSource();

    void onGoToStart();
    void onStop();
    void onPlayPause();
    void onGoToEnd();

    void edit(const function<void(Sample&)>& behavior) const;

    const Sample& currentSample() const;
    const SamplePart& currentPart() const;

    void toggleSidePanel(const Input& input);

    SizeProperties sizeProperties() const override;
};
