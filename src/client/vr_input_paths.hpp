/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"

#include <openxr/openxr.h>

struct EyeTrackingPaths
{
    XrPath eyesPath;

    XrPath interactionProfile;
};

struct GenericTrackingPaths
{
    XrPath leftHandPath;
    XrPath rightHandPath;

    XrPath interactionProfile;
};

struct HTCFullBodyTrackingPaths
{
    XrPath waistPath;
    XrPath leftFootPath;
    XrPath rightFootPath;
    XrPath chestPath;
    XrPath leftShoulderPath;
    XrPath rightShoulderPath;
    XrPath leftElbowPath;
    XrPath rightElbowPath;
    XrPath leftWristPath;
    XrPath rightWristPath;
    XrPath leftKneePath;
    XrPath rightKneePath;
    XrPath leftAnklePath;
    XrPath rightAnklePath;

    XrPath interactionProfile;
};

struct GoogleDaydreamPaths
{
    XrPath selectClickPath;
    XrPath touchpadXPath;
    XrPath touchpadYPath;
    XrPath touchpadTouchPath;
    XrPath touchpadClickPath;

    XrPath interactionProfile;
};

struct HTCVivePaths
{
    XrPath leftSystemClickPath;
    XrPath leftSqueezeClickPath;
    XrPath leftMenuClickPath;

    XrPath leftTouchpadXPath;
    XrPath leftTouchpadYPath;
    XrPath leftTouchpadTouchPath;
    XrPath leftTouchpadClickPath;

    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftHandHapticPath;

    XrPath rightSystemClickPath;
    XrPath rightSqueezeClickPath;
    XrPath rightMenuClickPath;

    XrPath rightTouchpadXPath;
    XrPath rightTouchpadYPath;
    XrPath rightTouchpadTouchPath;
    XrPath rightTouchpadClickPath;

    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct HTCViveProPaths
{
    XrPath systemClickPath;
    XrPath volumeUpClickPath;
    XrPath volumeDownClickPath;
    XrPath muteMicrophoneClickPath;

    XrPath interactionProfile;
};

struct HTCViveCosmosPaths
{
    XrPath leftXClickPath;
    XrPath leftYClickPath;
    XrPath leftMenuClickPath;
    XrPath leftShoulderClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftHandHapticPath;

    XrPath rightAClickPath;
    XrPath rightBClickPath;
    XrPath rightSystemClickPath;
    XrPath rightShoulderClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct HTCViveFocus3Paths
{
    XrPath leftXClickPath;
    XrPath leftYClickPath;
    XrPath leftMenuClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftSqueezeClickPath;
    XrPath leftSqueezeTouchPath;

    XrPath leftThumbrestTouchPath;

    XrPath leftHandHapticPath;

    XrPath rightAClickPath;
    XrPath rightBClickPath;
    XrPath rightSystemClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightSqueezeClickPath;
    XrPath rightSqueezeTouchPath;

    XrPath rightThumbrestTouchPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct HTCHandPaths
{
    XrPath leftHandSelectPath;
    XrPath leftHandSqueezePath;

    XrPath rightHandSelectPath;
    XrPath rightHandSqueezePath;

    XrPath interactionProfile;
};

struct MicrosoftMixedRealityMotionPaths
{
    XrPath leftSqueezeClickPath;
    XrPath leftMenuClickPath;

    XrPath leftTouchpadXPath;
    XrPath leftTouchpadYPath;
    XrPath leftTouchpadTouchPath;
    XrPath leftTouchpadClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickClickPath;

    XrPath leftTriggerSqueezePath;

    XrPath leftHandHapticPath;

    XrPath rightSqueezeClickPath;
    XrPath rightMenuClickPath;

    XrPath rightTouchpadXPath;
    XrPath rightTouchpadYPath;
    XrPath rightTouchpadTouchPath;
    XrPath rightTouchpadClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickClickPath;

    XrPath rightTriggerSqueezePath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct MicrosoftXboxPaths
{
    XrPath dpadLeftClickPath;
    XrPath dpadRightClickPath;
    XrPath dpadUpClickPath;
    XrPath dpadDownClickPath;

    XrPath aClickPath;
    XrPath bClickPath;
    XrPath xClickPath;
    XrPath yClickPath;

    XrPath backClickPath;
    XrPath startClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickClickPath;
    XrPath leftShoulderClickPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftHapticPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickClickPath;
    XrPath rightShoulderClickPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightHapticPath;

    XrPath interactionProfile;
};

struct MicrosoftHandPaths
{
    XrPath leftHandSelectPath;
    XrPath leftHandSqueezePath;

    XrPath rightHandSelectPath;
    XrPath rightHandSqueezePath;

    XrPath interactionProfile;
};

struct HPMixedRealityPaths
{
    XrPath leftXClickPath;
    XrPath leftYClickPath;
    XrPath leftMenuClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickClickPath;

    XrPath leftTriggerSqueezePath;

    XrPath leftSqueezePath;

    XrPath leftHandHapticPath;

    XrPath rightAClickPath;
    XrPath rightBClickPath;
    XrPath rightMenuClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickClickPath;

    XrPath rightTriggerSqueezePath;

    XrPath rightSqueezePath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct HuaweiPaths
{
    XrPath leftHomeClickPath;
    XrPath leftBackClickPath;
    XrPath leftVolumeUpClickPath;
    XrPath leftVolumeDownClickPath;

    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftTouchpadXPath;
    XrPath leftTouchpadYPath;
    XrPath leftTouchpadTouchPath;
    XrPath leftTouchpadClickPath;

    XrPath leftHandHapticPath;

    XrPath rightHomeClickPath;
    XrPath rightBackClickPath;
    XrPath rightVolumeUpClickPath;
    XrPath rightVolumeDownClickPath;

    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightTouchpadXPath;
    XrPath rightTouchpadYPath;
    XrPath rightTouchpadTouchPath;
    XrPath rightTouchpadClickPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct OculusGoPaths
{
    XrPath leftSystemClickPath;
    XrPath leftBackClickPath;

    XrPath leftTouchpadXPath;
    XrPath leftTouchpadYPath;
    XrPath leftTouchpadTouchPath;
    XrPath leftTouchpadClickPath;

    XrPath leftTriggerClickPath;

    XrPath rightSystemClickPath;
    XrPath rightBackClickPath;

    XrPath rightTouchpadXPath;
    XrPath rightTouchpadYPath;
    XrPath rightTouchpadTouchPath;
    XrPath rightTouchpadClickPath;

    XrPath rightTriggerClickPath;

    XrPath interactionProfile;
};

struct OculusTouchPaths
{
    XrPath leftMenuClickPath;
    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftSqueezePath;

    XrPath leftThumbrestTouchPath;

    XrPath leftHandHapticPath;

    XrPath rightSystemClickPath;
    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightSqueezePath;

    XrPath rightThumbrestTouchPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct ValveIndexPaths
{
    XrPath leftSystemTouchPath;
    XrPath leftSystemClickPath;
    XrPath leftATouchPath;
    XrPath leftAClickPath;
    XrPath leftBTouchPath;
    XrPath leftBClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftTouchpadXPath;
    XrPath leftTouchpadYPath;
    XrPath leftTouchpadTouchPath;
    XrPath leftTouchpadSqueezePath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;

    XrPath leftHandHapticPath;

    XrPath rightSystemTouchPath;
    XrPath rightSystemClickPath;
    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightTouchpadXPath;
    XrPath rightTouchpadYPath;
    XrPath rightTouchpadTouchPath;
    XrPath rightTouchpadSqueezePath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct MagicLeap2Paths
{
    XrPath leftMenuClickPath;
    XrPath leftHomeClickPath;

    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftTouchpadXPath;
    XrPath leftTouchpadYPath;
    XrPath leftTouchpadTouchPath;
    XrPath leftTouchpadSqueezePath;
    XrPath leftTouchpadClickPath;

    XrPath leftShoulderClickPath;

    XrPath leftHandHapticPath;

    XrPath rightMenuClickPath;
    XrPath rightHomeClickPath;

    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightTouchpadXPath;
    XrPath rightTouchpadYPath;
    XrPath rightTouchpadTouchPath;
    XrPath rightTouchpadSqueezePath;
    XrPath rightTouchpadClickPath;

    XrPath rightShoulderClickPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct PicoNeo3Paths
{
    XrPath leftMenuClickPath;
    XrPath leftSystemClickPath;

    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;
    XrPath leftSqueezeClickPath;

    XrPath leftHapticPath;

    XrPath rightMenuClickPath;
    XrPath rightSystemClickPath;

    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;
    XrPath rightSqueezeClickPath;

    XrPath rightHapticPath;

    XrPath interactionProfile;
};

struct Pico4Paths
{
    XrPath leftMenuClickPath;
    XrPath leftSystemClickPath;

    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;
    XrPath leftTriggerClickPath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;
    XrPath leftSqueezeClickPath;

    XrPath leftHapticPath;

    XrPath rightSystemClickPath;

    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;
    XrPath rightTriggerClickPath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;
    XrPath rightSqueezeClickPath;

    XrPath rightHapticPath;

    XrPath interactionProfile;
};

struct PicoG3Paths
{
    XrPath menuClickPath;

    XrPath triggerSqueezePath;
    XrPath triggerClickPath;

    XrPath stickXPath;
    XrPath stickYPath;
    XrPath stickClickPath;

    XrPath interactionProfile;
};

struct QuestTouchProPaths
{
    XrPath leftMenuClickPath;

    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;

    XrPath leftThumbrestTouchPath;
    XrPath leftThumbrestSqueezePath;

    XrPath leftHapticPath;

    XrPath rightSystemClickPath;

    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;

    XrPath rightThumbrestTouchPath;
    XrPath rightThumbrestSqueezePath;

    XrPath rightHapticPath;

    XrPath interactionProfile;
};

struct QuestTouchPlusPaths
{
    XrPath leftMenuClickPath;

    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;

    XrPath leftThumbrestTouchPath;

    XrPath leftHapticPath;

    XrPath rightSystemClickPath;

    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;

    XrPath rightThumbrestTouchPath;

    XrPath rightHapticPath;

    XrPath interactionProfile;
};

struct OppoPaths
{
    XrPath heartratePath;

    XrPath leftMenuClickPath;

    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;

    XrPath leftHapticPath;

    XrPath rightHomeClickPath;

    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;

    XrPath rightHapticPath;

    XrPath interactionProfile;
};

struct YVRPaths
{
    XrPath leftMenuClickPath;
    XrPath leftXTouchPath;
    XrPath leftXClickPath;
    XrPath leftYTouchPath;
    XrPath leftYClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezePath;

    XrPath leftHandHapticPath;

    XrPath rightSystemClickPath;
    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezePath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};

struct VarjoXR4Paths
{
    XrPath leftMenuClickPath;
    XrPath leftATouchPath;
    XrPath leftAClickPath;
    XrPath leftBTouchPath;
    XrPath leftBClickPath;

    XrPath leftTriggerTouchPath;
    XrPath leftTriggerSqueezePath;

    XrPath leftStickXPath;
    XrPath leftStickYPath;
    XrPath leftStickTouchPath;
    XrPath leftStickClickPath;

    XrPath leftSqueezeTouchPath;
    XrPath leftSqueezeClickPath;

    XrPath leftHandHapticPath;

    XrPath rightSystemClickPath;
    XrPath rightATouchPath;
    XrPath rightAClickPath;
    XrPath rightBTouchPath;
    XrPath rightBClickPath;

    XrPath rightTriggerTouchPath;
    XrPath rightTriggerSqueezePath;

    XrPath rightStickXPath;
    XrPath rightStickYPath;
    XrPath rightStickTouchPath;
    XrPath rightStickClickPath;

    XrPath rightSqueezeTouchPath;
    XrPath rightSqueezeClickPath;

    XrPath rightHandHapticPath;

    XrPath interactionProfile;
};
#endif
