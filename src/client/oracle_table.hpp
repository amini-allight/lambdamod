/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "oracle.hpp"

class OracleTable : public InterfaceWidget
{
public:
    OracleTable(
        InterfaceWidget* parent,
        const function<Oracle()>& source
    );

    void step() override;
    void draw(const DrawContext& ctx) const override;

    void roll();

private:
    Oracle oracle;
    vector<size_t> rolls;
    i32 panIndex;
    i32 scrollIndex;
    chrono::milliseconds rollTime;

    function<Oracle()> source;

    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void panLeft(const Input& input);
    void panRight(const Input& input);

    SizeProperties sizeProperties() const override;
};
