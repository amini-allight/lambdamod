/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "node_editor_node.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "theme.hpp"
#include "node_editor.hpp"
#include "line_edit.hpp"
#include "int_edit.hpp"
#include "float_edit.hpp"
#include "searchable_option_select.hpp"
#include "script_tools.hpp"
#include "entity_display_name.hpp"

NodeEditorNode::NodeEditorNode(InterfaceWidget* parent, ScriptNodeID id)
    : InterfaceWidget(parent)
    , value(nullptr)
    , _id(id)
{
    START_WIDGET_SCOPE("node-editor-node")

    END_SCOPE

    switch (node()->type())
    {
    case Script_Node_Output :
    case Script_Node_Execution :
    case Script_Node_Void :
    case Script_Node_List :
        break;
    case Script_Node_Symbol :
    {
        auto edit = new LineEdit(this, nullptr, nullptr);
        edit->set(node()->get<ScriptNodeSymbol>().value);

        value = edit;
        break;
    }
    case Script_Node_Integer :
    {
        auto edit = new IntEdit(this, nullptr, nullptr);
        edit->set(node()->get<ScriptNodeInteger>().value);

        value = edit;
        break;
    }
    case Script_Node_Float :
    {
        auto edit = new FloatEdit(this, nullptr, nullptr);
        edit->set(node()->get<ScriptNodeFloat>().value);

        value = edit;
        break;
    }
    case Script_Node_String :
    {
        auto edit = new LineEdit(this, nullptr, nullptr);
        edit->set(node()->get<ScriptNodeString>().value);

        value = edit;
        break;
    }
    case Script_Node_Lambda :
        new LineEdit(
            this,
            bind(&NodeEditorNode::lambdaSource, this),
            bind(&NodeEditorNode::onLambda, this, placeholders::_1),
            true
        );
        break;
    case Script_Node_Handle :
        new SearchableOptionSelect(
            this,
            bind(&NodeEditorNode::entityOptionsSource, this),
            bind(&NodeEditorNode::entitySource, this),
            bind(&NodeEditorNode::onEntity, this, placeholders::_1)
        );
        break;
    }
}

void NodeEditorNode::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        child->move(position + Point(size().w / 4, UIScale::scriptNodeCapHeight()));
    }
}

void NodeEditorNode::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->move(position() + Point(size.w / 4, UIScale::scriptNodeCapHeight()));
        child->resize(Size(size.w / 2, UIScale::lineEditHeight()));
    }
}

void NodeEditorNode::step()
{
    InterfaceWidget::step();

    Point point = node()->screenPosition(editor()->position().toVec2(), editor()->state().view, editor()->size().toVec2());
    Size size = node()->size(context()->controller()->game().context());

    move(point);
    resize(size);

    switch (node()->type())
    {
    case Script_Node_Output :
    case Script_Node_Execution :
    case Script_Node_Void :
    case Script_Node_List :
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    case Script_Node_Symbol :
    {
        string value = dynamic_cast<LineEdit*>(this->value)->content();

        if (value != node()->get<ScriptNodeSymbol>().value)
        {
            editor()->setState([this, value](NodeEditorState& state) -> void {
                state.nodes.find(_id)->get<ScriptNodeSymbol>().value = value;
            }, false);
        }
        break;
    }
    case Script_Node_Integer :
    {
        i64 value = dynamic_cast<IntEdit*>(this->value)->value();

        if (value != node()->get<ScriptNodeInteger>().value)
        {
            editor()->setState([this, value](NodeEditorState& state) -> void {
                state.nodes.find(_id)->get<ScriptNodeInteger>().value = value;
            }, false);
        }
        break;
    }
    case Script_Node_Float :
    {
        f64 value = dynamic_cast<FloatEdit*>(this->value)->value();

        if (value != node()->get<ScriptNodeFloat>().value)
        {
            editor()->setState([this, value](NodeEditorState& state) -> void {
                state.nodes.find(_id)->get<ScriptNodeFloat>().value = value;
            }, false);
        }
        break;
    }
    case Script_Node_String :
    {
        string value = dynamic_cast<LineEdit*>(this->value)->content();

        if (value != node()->get<ScriptNodeString>().value)
        {
            editor()->setState([this, value](NodeEditorState& state) -> void {
                state.nodes.find(_id)->get<ScriptNodeString>().value = value;
            }, false);
        }
        break;
    }
    }
}

void NodeEditorNode::draw(const DrawContext& ctx) const
{
    drawRoundedSolid(
        ctx,
        this,
        UIScale::scriptNodeBorderRadius(),
        theme.backgroundColor
    );

    if (selected())
    {
        drawRoundedBorder(
            ctx,
            this,
            UIScale::marginSize(),
            UIScale::scriptNodeBorderRadius(),
            theme.accentColor
        );
    }

    string name;

    switch (node()->type())
    {
    case Script_Node_Output :
        name = localize("node-editor-node-output");
        break;
    case Script_Node_Execution :
        name = node()->get<ScriptNodeExecution>().name;
        break;
    case Script_Node_Void :
        name = localize("node-editor-node-void");
        break;
    case Script_Node_Symbol :
        name = localize("node-editor-node-symbol");
        break;
    case Script_Node_Integer :
        name = localize("node-editor-node-integer");
        break;
    case Script_Node_Float :
        name = localize("node-editor-node-float");
        break;
    case Script_Node_String :
        name = localize("node-editor-node-string");
        break;
    case Script_Node_List :
        name = localize("node-editor-node-list");
        break;
    case Script_Node_Lambda :
        name = localize("node-editor-node-lambda");
        break;
    case Script_Node_Handle :
        name = localize("node-editor-node-handle");
        break;
    }

    TextStyle style;
    style.weight = Text_Bold;
    style.alignment = Text_Center;

    drawText(
        ctx,
        this,
        Point(),
        Size(size().w, UIScale::scriptNodeHeaderHeight()),
        name,
        style
    );

    InterfaceWidget::draw(ctx);

    style.weight = Text_Regular;

    auto [ inputPoints, outputPoints ] = interactionPoints();

    vector<string> inputs = node()->inputs(context()->controller()->game().context());

    if (!inputs.empty())
    {
        style.alignment = Text_Left;

        for (size_t i = 0; i < inputs.size(); i++)
        {
            Point center = inputPoints.at(i);
            Point topLeft = center - Point(UIScale::scriptNodeLinkConnectorHeight() / 2);

            drawText(
                ctx,
                this,
                Point(UIScale::scriptNodeLinkConnectorHeight() / 2, topLeft.y),
                Size((size().w - UIScale::scriptNodeLinkConnectorHeight()) / 2, UIScale::scriptNodeLinkConnectorHeight()),
                inputs.at(i),
                style
            );

            bool focused = (position() + center).toVec2().distance(pointer.toVec2()) < UIScale::scriptNodeLinkConnectorHeight() / 2;

            drawRoundedSolid(
                ctx,
                editor(),
                (position() - editor()->position()) + topLeft,
                Size(UIScale::scriptNodeLinkConnectorHeight()),
                UIScale::scriptNodeLinkConnectorHeight() / 2,
                focused ? theme.accentColor : theme.outdentColor
            );
        }
    }

    vector<string> outputs = node()->outputs();

    if (!outputs.empty())
    {
        style.alignment = Text_Right;

        for (size_t i = 0; i < outputs.size(); i++)
        {
            Point center = outputPoints.at(i);
            Point topLeft = center - Point(UIScale::scriptNodeLinkConnectorHeight() / 2);

            drawText(
                ctx,
                this,
                Point(size().w / 2, topLeft.y),
                Size((size().w - UIScale::scriptNodeLinkConnectorHeight()) / 2, UIScale::scriptNodeLinkConnectorHeight()),
                outputs.at(i),
                style
            );

            bool focused = (position() + center).toVec2().distance(pointer.toVec2()) < UIScale::scriptNodeLinkConnectorHeight() / 2;

            drawRoundedSolid(
                ctx,
                editor(),
                (position() - editor()->position()) + topLeft,
                Size(UIScale::scriptNodeLinkConnectorHeight()),
                UIScale::scriptNodeLinkConnectorHeight() / 2,
                focused ? theme.accentColor : theme.outdentColor
            );
        }
    }
}

ScriptNodeID NodeEditorNode::id() const
{
    return _id;
}

bool NodeEditorNode::selected() const
{
    return editor()->state().selection.contains(_id);
}

pair<vector<Point>, vector<Point>> NodeEditorNode::interactionPoints() const
{
    vector<Point> inputs(node()->inputs(context()->controller()->game().context()).size());

    if (!inputs.empty())
    {
        i32 inputStepSize = size().h / inputs.size();

        for (size_t i = 0; i < inputs.size(); i++)
        {
            i32 y = (i * inputStepSize) + (inputStepSize / 2);

            inputs.at(i) = Point(0, y);
        }
    }

    vector<Point> outputs(node()->outputs().size());

    if (!outputs.empty())
    {
        i32 outputStepSize = size().h / outputs.size();

        for (size_t i = 0; i < outputs.size(); i++)
        {
            i32 y = (i * outputStepSize) + (outputStepSize / 2);

            outputs.at(i) = Point(size().w, y);
        }
    }

    return { inputs, outputs };
}

shared_ptr<ScriptNode> NodeEditorNode::node() const
{
    return editor()->state().nodes.find(_id);
}

NodeEditor* NodeEditorNode::editor() const
{
    return dynamic_cast<NodeEditor*>(parent());
}

string NodeEditorNode::lambdaSource()
{
    return node()->get<ScriptNodeLambda>().lambda.toString();
}

void NodeEditorNode::onLambda(const string& value)
{
    editor()->setState([this, value](NodeEditorState& state) -> void {
        ScriptContext context;

        state.nodes.find(_id)->get<ScriptNodeLambda>().lambda = runSafely(
            &context,
            value,
            "parsing node editor lambda",
            this->context()->controller()
        )->front().lambda;
    });
}

vector<string> NodeEditorNode::entityOptionsSource()
{
    vector<string> names;

    names.push_back(localize("node-editor-node-none"));

    context()->controller()->game().traverse([&](const Entity* entity) -> void {
        names.push_back(entityDisplayName(entity));
    });

    return names;
}

size_t NodeEditorNode::entitySource()
{
    if (!node()->get<ScriptNodeHandle>().id)
    {
        return 0;
    }

    size_t index = 0;

    size_t i = 1;
    context()->controller()->game().traverse([&](const Entity* entity) -> void {
        if (entity->id() == node()->get<ScriptNodeHandle>().id)
        {
            index = i;
        }

        i++;
    });

    // Don't warn here, failure to find will often happen here

    return index;
}

void NodeEditorNode::onEntity(size_t index)
{
    editor()->setState([this, index](NodeEditorState& state) -> void {
        size_t i = 1;
        context()->controller()->game().partiallyTraverse([&](const Entity* entity) -> bool {
            if (i == index)
            {
                state.nodes.find(_id)->get<ScriptNodeHandle>().id = entity->id();
                return true;
            }

            i++;
            return false;
        });
    });
}

SizeProperties NodeEditorNode::sizeProperties() const
{
    return {
        static_cast<f64>(size().w), static_cast<f64>(size().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
