/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "connection.hpp"
#include "connection_state.hpp"
#include "step_frame_history.hpp"
#include "game_state.hpp"
#include "user.hpp"
#include "team.hpp"
#include "voice_encoder.hpp"
#include "voice_decoder.hpp"
#include "area_effect_cache.hpp"
#include "compare.hpp"

#include "client.hpp"

#include "vr.hpp"
#include "window_interface.hpp"
#include "sound_device_interface.hpp"
#include "input_interface.hpp"

#include "render_context.hpp"
#include "render_interface.hpp"
#include "sound.hpp"
#include "interface.hpp"

#include "server_runner.hpp"

class ControlContext;

class Controller
{
public:
    Controller();
    Controller(const Controller& rhs) = delete;
    Controller(Controller&& rhs) = delete;
    ~Controller();

    Controller& operator=(const Controller& rhs) = delete;
    Controller& operator=(Controller&& rhs) = delete;

    bool run();

    void onVoice(const vector<i16>& samples);
    void onInput(const Input& input);
    void onGrip(bool right, f64 strength, const mat4& transform, const Velocities& velocities);
    void onBounds(const vec2& bounds);
    void onResize(u32 width, u32 height);
    void onFocus();
    void onUnfocus();

#ifdef LMOD_VR
    VRInterface* vr() const;
#endif
    WindowInterface* window() const;
    SoundDeviceInterface* soundDevice() const;
    RenderInterface* render() const;
    Sound* sound() const;
    InputInterface* input() const;
    ControlContext* context() const;

    UserID selfID() const;
    const User* self() const;
    User* self();
    Entity* host() const;
    World* selfWorld() const;

    const string& splashMessage() const;
    const string& voiceChannel() const;
    const map<string, string>& documents() const;
    const set<string>& checkpoints() const;
    const string& serverJoinURL() const;
    bool serverSaving() const;

    GameState& game();

    void edit(const function<void()>& behavior, bool mergeUndo = false);
    void pushEditInput(const function<void(const Input&)>& handler, const Input& input);
    void sendAttachedInput(const string& hook, const Input& input);

    void connect(
        const Address& address,
        const string& serverPassword,
        const string& name,
        const string& password
    );
    void host(
        const Address& address,
        const string& serverPassword,
        const string& adminPassword,
        const string& savePath,
        const string& name,
        const string& password
    );
#if !defined(__EMSCRIPTEN__)
    void holePunch(
        const Address& clientAddress,
        const Address& serverAddress,
        const string& serverPassword,
        const string& name,
        const string& password
    );
#endif
    void disconnect();
    void send(const NetworkEvent& event);
    void markForReset();
    ConnectionState connectionState() const;
    chrono::microseconds lastStepTime() const;
    bool connected() const;
    bool standby() const;
    const string& disconnectionReason() const;
    u64 roundTripTime() const;
    bool ready() const;

    void quit();

private:
    // Submodules
#ifdef LMOD_VR
    VRInterface* _vr;
#endif
    WindowInterface* _window;
    RenderContext* renderContext;
    RenderInterface* _render;
    SoundDeviceInterface* _soundDevice;
    Sound* _sound;
    ControlContext* _context;

    // Server meta state
    UserID _selfID;
    string _splashMessage;
    string _voiceChannel;
    map<string, string> _documents;
    set<string> _checkpoints;
    string _serverJoinURL;
    bool _serverSaving;

    // Game state
    u64 stepIndex;
    StepFrame now;
    StepFrameHistory history;
    bool inReplayMode;
    vector<tuple<function<void(const Input&)>, Input>> replayableEditInputs;

    // Ephemeral
    bool _quit;
    bool shouldReset;
    chrono::microseconds _lastStepTime;

    Connection connection;
    vector<NetworkEvent> systemEvents;

    ConnectionState _connectionState;
    string _disconnectionReason;
    chrono::milliseconds pingSentTime;

    Client* client;
    VoiceEncoder* voiceEncoder;
    map<UserID, VoiceDecoder*> voiceDecoders;

    ServerRunner* serverRunner;

    void handleNetworkEvents();
    void handleNetworkEventBatch(const NetworkEventBatch& batch);
    vector<NetworkEvent> stepSimulation();
    bool stepSystemInputs();
    bool stepSystemOutputs();

    void enterReplay();
    void exitReplay();

    void afterCompare(
        const Compare& compare,
        const function<bool(const NetworkEvent&)>& hot = [](const NetworkEvent& event) -> bool { return true; }
    );

    void createClient(const string& serverPassword, const string& name, const string& password);

    void onConnect(const string& serverPassword, const string& name, const string& password);
    void onReceive(const NetworkEvent& event);
    void onDisconnect();

    void handleNetworkEvent(const NetworkEvent& event, bool hot);

    void handleWelcomeEvent(const WelcomeEvent& event, bool hot);
    void handleKickEvent(const KickEvent& event, bool hot);
    void handlePingEvent(const PingEvent& event, bool hot);
    void handleCheckpointEvent(const CheckpointEvent& event, bool hot);
    void handleDocumentEvent(const DocumentEvent& event, bool hot);
    void handleAttachEvent(const AttachEvent& event, bool hot);
    void handleDetachEvent(const DetachEvent& event, bool hot);
    void handleMoveEvent(const MoveEvent& event, bool hot);
    void handleHapticEvent(const HapticEvent& event, bool hot);
    void handleLogEvent(const LogEvent& event, bool hot);
    void handleHistoryEvent(const HistoryEvent& event, bool hot);
    void handleTextRequestEvent(const TextRequestEvent& event, bool hot);
    void handleTetherEvent(const TetherEvent& event, bool hot);
    void handleConfinementEvent(const ConfinementEvent& event, bool hot);
    void handleFadeEvent(const FadeEvent& event, bool hot);
    void handleWaitEvent(const WaitEvent& event, bool hot);
    void handleVoiceEvent(const VoiceEvent& event, bool hot);
    void handleVoiceChannelEvent(const VoiceChannelEvent& event, bool hot);
    void handleURLEvent(const URLEvent& event);
    void handleSaveEvent(const SaveEvent& event);

    void handleCheckpointAdd(const CheckpointEvent& event);
    void handleCheckpointRemove(const CheckpointEvent& event);

    void handleDocumentAdd(const DocumentEvent& event);
    void handleDocumentRemove(const DocumentEvent& event);
    void handleDocumentUpdate(const DocumentEvent& event);

    void handleSideEffect(const EntityUpdateSideEffect& effect);
    void handleSideEffect(const NetworkEvent& event, bool hot);

    void saveConnectionDetails(
        const Address& address,
        const string& serverPassword,
        const string& name,
        const string& password
    ) const;
    void saveConnectionDetails(
        const Address& address,
        const string& serverPassword,
        const string& adminPassword,
        const string& savePath,
        const string& connectHost,
        const string& name,
        const string& password
    ) const;

    void clear();
    void reset();
};
