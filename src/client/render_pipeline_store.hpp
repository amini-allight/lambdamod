/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_render_data.hpp"
#include "render_context.hpp"
#include "render_pipeline_store_types.hpp"

#include <vulkan/vulkan.h>

struct RenderEntityPipelines
{
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
};

class RenderPipelineStore
{
public:
    RenderPipelineStore(const RenderContext* ctx);
    RenderPipelineStore(const RenderPipelineStore& rhs) = delete;
    RenderPipelineStore(RenderPipelineStore&& rhs) = delete;
    virtual ~RenderPipelineStore() = 0;

    RenderPipelineStore& operator=(const RenderPipelineStore& rhs) = delete;
    RenderPipelineStore& operator=(RenderPipelineStore&& rhs) = delete;

    virtual RenderEntityPipelines get(BodyRenderMaterialType type) const = 0;

protected:
    const RenderContext* ctx;

    string selectRayTracing(const string& name) const;
};
