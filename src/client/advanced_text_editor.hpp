/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "basic_text_editor.hpp"
#include "direct_input_interface_widget.hpp"

enum TextEditorMode : u8
{
    Text_Editor_Command_Mode,
    Text_Editor_Insert_Mode,
    Text_Editor_Replace_Mode,
    Text_Editor_Visual_Mode,
    Text_Editor_Search_Mode
};

class LineEdit;

class AdvancedTextEditor : public BasicTextEditor, public DirectInputInterfaceWidget
{
public:
    AdvancedTextEditor(
        InterfaceWidget* parent,
        const function<string()>& source,
        const function<void(const string&)>& onSave,
        bool codeMode = true
    );

    void input(const Input& input, u32 repeats) override;
    void unfocus() override;

private:
    TextEditorMode mode;
    function<void(const Input&)> partial;
    function<optional<i32>(const Input&)> subPartial;

    bool searchForwards;
    std::string search;

    void drawStatusLine(const DrawContext& ctx) const override;
    void drawActiveLine(const DrawContext& ctx) const override;
    void drawContent(const DrawContext& ctx) const override;
    void drawCursor(const DrawContext& ctx) const override;

    void drawBoxCursor(const DrawContext& ctx) const;
    void drawUnderlineCursor(const DrawContext& ctx) const;

    void commandModeInput(const Input& input);
    void insertModeInput(const Input& input, u32 repeats);
    void replaceModeInput(const Input& input);
    void visualModeInput(const Input& input);
    void searchModeInput(const Input& input);

    void replace(const Input& input);
    void yank(const Input& input);
    void untilNext(const Input& input);
    void untilPrevious(const Input& input);
    void findNext(const Input& input);
    void findPrevious(const Input& input);
    void remove(const Input& input);
    void misc(const Input& input);
    void change(const Input& input);
    void indent(const Input& input);
    void unindent(const Input& input);

    optional<i32> endPoint(const Input& input);
    optional<char> symbol(const Input& input) const;

    void enterCommandMode();
    void enterInsertMode();
    void enterReplaceMode();
    void enterVisualMode();
    void enterSearchMode(bool reverse = false);

    void replace(char c);

    void invertSelection();

    void indentLine(i32 index, i32 indent);
    void unindentLine(i32 index, i32 indent);

    i32 paragraphStart() const;
    i32 paragraphEnd() const;

    i32 sentenceStart() const;
    i32 sentenceEnd() const;

    i32 matchingBracket(i32 index = -1) const;

    i32 previousOccurrence(const std::string& pattern) const;
    i32 nextOccurrence(const std::string& pattern) const;

    vector<tuple<i32, i32>> allSearchResults() const;

    void endCommandModeSelection(const Input& input, const Point& local);
    void endVisualModeSelection(const Input& input, const Point& local);

    SizeProperties sizeProperties() const override;
};
