/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_render.hpp"

#include "tools.hpp"
#include "paths.hpp"
#include "global.hpp"
#include "constants.hpp"
#include "theme.hpp"

#include "vr_tools.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_objects.hpp"
#include "render_vr_hdr.hpp"
#include "render_vr_steamvr_color_adjustment.hpp"

#include "controller.hpp"
#include "control_context.hpp"
#include "interface.hpp"
#include "panel_transforms.hpp"

#include "render_vr_chromatic_aberration_effect.hpp"
#include "render_vr_depth_of_field_effect.hpp"
#include "render_vr_motion_blur_effect.hpp"

VRRender::VRRender(Controller* controller, RenderContext* ctx)
    : RenderInterface()
    , controller(controller)
    , ctx(ctx)
    , _panelRenderPass(nullptr)
    , skyRenderPass(nullptr)
    , mainRenderPass(nullptr)
    , hdrRenderPass(nullptr)
    , postProcessRenderPass(nullptr)
    , overlayRenderPass(nullptr)
    , independentDepthOverlayRenderPass(nullptr)
    , pipelineStore(nullptr)
    , swapchain(nullptr)
    , hudVertexBuffer(nullptr, nullptr)
    , imageIndex(0)
    , hdr(nullptr)
    , sky(nullptr)
    , atmosphere(nullptr)
    , steamvrColorAdjustment(nullptr)
    , nextDecorationID(1)
    , nextPostProcessEffectID(1)
    , width(0)
    , height(0)
    , _angle(radians(70))
    , format(VK_FORMAT_R32G32B32A32_SFLOAT)
    , sampleCount(VK_SAMPLE_COUNT_1_BIT)
    , keyboard(false)
    , exposure(1)
    , lastWriteTime(currentTime())
    , _timer(0)
    , _rayTracing(false)
{
    log("Initializing render system...");

    // Initialize
    if (!ctx->initialized())
    {
        ctx->init(
            bind(&VRRender::pickAPIVersion, this),
            bind(&VRRender::instanceExtensions, this),
            bind(&VRRender::pickPhysicalDevice, this),
            bind(&VRRender::deviceExtensions, this),
            bind(&VRRender::prepareSwapchain, this)
        );
    }

    _rayTracing = ctx->rayTracingEnabled();

    createPanelRenderPass(this, _panelRenderPass);
    createSkyRenderPass(this, skyRenderPass);

    if (sampleCount == VK_SAMPLE_COUNT_1_BIT)
    {
        createMonosampleMainRenderPass(this, mainRenderPass);
        createMonosampleHDRRenderPass(this, hdrRenderPass);
        createMonosamplePostProcessRenderPass(this, postProcessRenderPass);
        createMonosampleOverlayRenderPass(this, overlayRenderPass);
        createMonosampleIndependentDepthOverlayRenderPass(this, independentDepthOverlayRenderPass);
    }
    else
    {
        createMultisampleMainRenderPass(this, sampleCount, mainRenderPass);
        createMultisampleHDRRenderPass(this, sampleCount, hdrRenderPass);
        createMultisamplePostProcessRenderPass(this, sampleCount, postProcessRenderPass);
        createMultisampleOverlayRenderPass(this, sampleCount, overlayRenderPass);
        createMultisampleIndependentDepthOverlayRenderPass(this, sampleCount, independentDepthOverlayRenderPass);
    }

    pipelineStore = new RenderVRPipelineStore(
        ctx,
        skyRenderPass,
        mainRenderPass,
        hdrRenderPass,
        postProcessRenderPass,
        overlayRenderPass,
        independentDepthOverlayRenderPass,
        sampleCount
    );

    createSwapchainElements();

    createCurvedPanelBuffer(ctx->allocator, hudVertexBuffer);

    hdr = new RenderVRHDR(
        ctx,
        pipelineStore,
        swapchainElements,
        exposure,
        hdrGamma
    );

    sky = new RenderVRSky(
        ctx,

        swapchainElements,

        pipelineStore->skyDescriptorSetLayout,
        pipelineStore
    );

    atmosphere = new RenderVRAtmosphere(
        this,

        swapchainElements,

        pipelineStore->atmosphereDescriptorSetLayout,
        pipelineStore
    );

    steamvrColorAdjustment = new RenderVRSteamVRColorAdjustment(
        ctx,
        pipelineStore,
        swapchainElements
    );

    RenderPostProcessEffectID motionBlurID = addPostProcessEffect<RenderVRMotionBlurEffect>(motionBlurStrength);
    setPostProcessEffectActive(motionBlurID, g_config.vrMotionBlur);
    RenderPostProcessEffectID depthOfFieldID = addPostProcessEffect<RenderVRDepthOfFieldEffect>(depthOfFieldRange);
    setPostProcessEffectActive(depthOfFieldID, g_config.vrDepthOfField);
    RenderPostProcessEffectID chromaticAberrationID = addPostProcessEffect<RenderVRChromaticAberrationEffect>(chromaticAberrationStrength);
    setPostProcessEffectActive(chromaticAberrationID, g_config.vrChromaticAberration);

    postProcessOrder = { motionBlurID, depthOfFieldID, chromaticAberrationID };

    log("Render system initialized.");
}

VRRender::~VRRender()
{
    log("Shutting down render system...");

    waitIdle();

    for (const auto& [ id, element ] : entities)
    {
        delete element;
    }
    entities.clear();

    for (const RenderDeletionTask<RenderVREntity>& task : entitiesToDelete)
    {
        task.destroy();
    }
    entitiesToDelete.clear();

    for (const auto& [ id, decoration ] : decorations)
    {
        delete decoration;
    }
    decorations.clear();

    for (const RenderDeletionTask<RenderVRDecoration>& task : decorationsToDelete)
    {
        task.destroy();
    }
    decorationsToDelete.clear();

    for (const auto& [ id, postProcessEffect ] : postProcessEffects)
    {
        delete postProcessEffect;
    }
    postProcessEffects.clear();

    for (const RenderDeletionTask<RenderVRPostProcessEffect>& task : postProcessEffectsToDelete)
    {
        task.destroy();
    }
    postProcessEffectsToDelete.clear();

    delete steamvrColorAdjustment;
    delete atmosphere;
    delete sky;
    delete hdr;

    destroyBuffer(ctx->allocator, hudVertexBuffer);

    destroySwapchainElements();
    destroySwapchain();

    delete pipelineStore;

    destroyRenderPass(ctx->device, independentDepthOverlayRenderPass);
    destroyRenderPass(ctx->device, overlayRenderPass);
    destroyRenderPass(ctx->device, postProcessRenderPass);
    destroyRenderPass(ctx->device, hdrRenderPass);
    destroyRenderPass(ctx->device, mainRenderPass);
    destroyRenderPass(ctx->device, skyRenderPass);
    destroyRenderPass(ctx->device, _panelRenderPass);

    dynamic_cast<VR*>(controller->vr())->quitSession();

    log("Render system shut down.");
}

void VRRender::waitIdle() const
{
    VkResult result;

    result = vkDeviceWaitIdle(ctx->device);

    if (result != VK_SUCCESS)
    {
        error("Failed to wait for device to idle: " + vulkanError(result));
    }
}

void VRRender::step()
{
    if (!controller->vr()->running())
    {
        return;
    }

    XrResult result;

    chrono::microseconds elapsed = currentTime<chrono::microseconds>() - lastWriteTime;
    lastWriteTime = currentTime<chrono::microseconds>();

    _timer += elapsed.count() / 1'000'000.0;

    XrFrameBeginInfo beginFrameInfo{};
    beginFrameInfo.type = XR_TYPE_FRAME_BEGIN_INFO;

    result = xrBeginFrame(dynamic_cast<VR*>(controller->vr())->session(), &beginFrameInfo);

    if (result != XR_SUCCESS)
    {
        error("Failed to begin frame: " + openxrError(result));
        return;
    }

    XrViewLocateInfo viewLocateInfo{};
    viewLocateInfo.type = XR_TYPE_VIEW_LOCATE_INFO;
    viewLocateInfo.viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
    viewLocateInfo.displayTime = dynamic_cast<VR*>(controller->vr())->predictedDisplayTime();
    viewLocateInfo.space = dynamic_cast<VR*>(controller->vr())->roomSpace();

    XrViewState viewState{};
    viewState.type = XR_TYPE_VIEW_STATE;

    u32 viewCount = eyeCount;
    vector<XrView> views(
        viewCount,
        { .type = XR_TYPE_VIEW }
    );

    result = xrLocateViews(
        dynamic_cast<VR*>(controller->vr())->session(),
        &viewLocateInfo,
        &viewState,
        viewCount,
        &viewCount,
        views.data()
    );

    if (result != XR_SUCCESS)
    {
        error("Failed to locate views: " + openxrError(result));
        return;
    }

    // Panels
    worldSpaceEyePosition = controller->context()->vrViewer()->headTransform().position();
    worldSpaceEyeRotation = controller->context()->vrViewer()->headTransform().rotation();
    _angle = views.front().fov.angleRight - views.front().fov.angleLeft;

    vec3 roomSpaceEyePosition = controller->vr()->worldToRoom(worldSpaceEyePosition);
    quaternion roomSpaceEyeRotation = controller->vr()->worldToRoom(worldSpaceEyeRotation);

    f32 luminance = -1;

    dynamic_cast<RenderVRHDR*>(hdr)->update(exposure, hdrGamma);

    u32 activeIndex;

    XrSwapchainImageAcquireInfo acquireImageInfo{};
    acquireImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO;

    result = xrAcquireSwapchainImage(swapchain, &acquireImageInfo, &activeIndex);

    if (result != XR_SUCCESS)
    {
        error("Failed to acquire swapchain image: " + openxrError(result));
        return;
    }

    XrSwapchainImageWaitInfo waitImageInfo{};
    waitImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO;
    waitImageInfo.timeout = numeric_limits<i64>::max();

    result = xrWaitSwapchainImage(swapchain, &waitImageInfo);

    if (result != XR_SUCCESS)
    {
        error("Failed to wait for swapchain image: " + openxrError(result));
        return;
    }

    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        cameraPosition[eye] = controller->vr()->roomToWorld({
            views.at(eye).pose.position.x,
            views.at(eye).pose.position.z * -1,
            views.at(eye).pose.position.y
        });
    }

    imageIndex = activeIndex;

    updateCameraUniforms(views);
    updateTextTitlecardUniforms(roomSpaceEyePosition, vec3(roomSpaceEyeRotation.forward().toVec2(), 0).normalize());
    updateTextUniforms(roomSpaceEyePosition, vec3(roomSpaceEyeRotation.forward().toVec2(), 0).normalize());
    updateHUDUniforms(roomSpaceEyePosition);
    updateKeyboardUniforms();

    VRSwapchainElement* swapchainElement = swapchainElements.at(imageIndex);

    if (controller->vr()->shouldRender())
    {
        // Have to do this here because the interface can't record into the correct command buffer until imageIndex has been set
        controller->context()->interface()->step();

        if (swapchainElement->luminanceReady())
        {
            luminance = getLuminance(swapchainElement);
        }

        render(swapchainElement);

        {
            const VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_TRANSFER_BIT;

            vector<VkCommandBuffer> commandBuffers = controller->context()->interface()->panelBuffers();
            commandBuffers.push_back(swapchainElement->transferCommandBuffer());
            commandBuffers.push_back(swapchainElement->commandBuffer());

            VkSubmitInfo submitInfo{};
            submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
            submitInfo.pWaitDstStageMask = &waitStage;
            submitInfo.commandBufferCount = commandBuffers.size();
            submitInfo.pCommandBuffers = commandBuffers.data();

            VkResult result = vkQueueSubmit(ctx->graphicsQueue, 1, &submitInfo, nullptr);

            if (result != VK_SUCCESS)
            {
                error("Failed to submit command buffers: " + vulkanError(result));
                return;
            }
        }
    }

    XrSwapchainImageReleaseInfo releaseImageInfo{};
    releaseImageInfo.type = XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO;

    result = xrReleaseSwapchainImage(swapchain, &releaseImageInfo);

    if (result != XR_SUCCESS)
    {
        error("Failed to release swapchain image: " + openxrError(result));
        return;
    }

    XrCompositionLayerProjectionView projectedViews[eyeCount]{};

    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        XrCompositionLayerProjectionView* view = &projectedViews[eye];
        view->type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
        view->pose = views.at(eye).pose;
        view->fov = views.at(eye).fov;
        view->subImage = {
            swapchain,
            {
                { 0, 0 },
                { static_cast<i32>(width), static_cast<i32>(height) }
            },
            eye
        };
    }

    XrCompositionLayerProjection layer{};
    layer.type = XR_TYPE_COMPOSITION_LAYER_PROJECTION;
    layer.layerFlags = 0;
    layer.space = dynamic_cast<VR*>(controller->vr())->roomSpace();
    layer.viewCount = eyeCount;
    layer.views = projectedViews;

    auto pLayer = reinterpret_cast<const XrCompositionLayerBaseHeader*>(&layer);

    XrFrameEndInfo endFrameInfo{};
    endFrameInfo.type = XR_TYPE_FRAME_END_INFO;
    endFrameInfo.displayTime = dynamic_cast<VR*>(controller->vr())->predictedDisplayTime();
    endFrameInfo.environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE;
    endFrameInfo.layerCount = controller->vr()->shouldRender() ? 1 : 0;
    endFrameInfo.layers = controller->vr()->shouldRender() ? &pLayer : nullptr;

    result = xrEndFrame(dynamic_cast<VR*>(controller->vr())->session(), &endFrameInfo);

    if (result != XR_SUCCESS)
    {
        error("Failed to end frame: " + openxrError(result));
        return;
    }

    advanceDeletionQueues();

    if (luminance >= 0)
    {
        adjustExposure(elapsed, luminance);
    }
}

tuple<u32, u32> VRRender::reinit(u32 width, u32 height)
{
    waitIdle();

    destroySwapchainElements();
    destroySwapchain();

    if (width == 0 || height == 0)
    {
        fatal("Swapchain cannot be recreated with size of 0.");
    }

    this->width = width;
    this->height = height;

    createSwapchain();
    createSwapchainElements();

    steamvrColorAdjustment->refresh(swapchainElements);

    hdr->refresh(swapchainElements);

    sky->refresh(swapchainElements);

    atmosphere->refresh(swapchainElements);

    for (const auto& [ id, element ] : entities)
    {
        element->refresh(swapchainElements);
    }

    for (const auto& [ id, decoration ] : decorations)
    {
        decoration->refresh(swapchainElements);
    }

    for (const auto& [ id, postProcessEffect ] : postProcessEffects)
    {
        postProcessEffect->refresh(swapchainElements);
    }

    imageIndex = 0;

    return { width, height };
}

void VRRender::setCameraSpace(const quaternion& space)
{
    cameraSpace = space;
}

void VRRender::add(EntityID parentID, const Entity* entity)
{
    BodyPartMask bodyPartMask;

    if (
        controller->context()->viewerMode()->mode() == Viewer_Sub_Mode_Body &&
        controller->context()->viewerMode()->bodyMode()->activeEntity() &&
        !controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs().empty()
    )
    {
        bodyPartMask = {
            controller->context()->viewerMode()->bodyMode()->activeEntity()->id(),
            controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs()
        };
    }

    if (parentID == EntityID())
    {
        auto it = entities.find(entity->id());

        if (it != entities.end())
        {
            return;
        }

        auto renderEntity = new RenderVREntity(
            this,

            swapchainElements,

            pipelineStore,
            entity,
            bodyPartMask,
            &entitiesToDelete
        );

        addLights(renderEntity);

        entities.insert({
            entity->id(),
            renderEntity
        });
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child->add(parentID, entity, bodyPartMask);
        }
    }
}

void VRRender::remove(EntityID parentID, EntityID id)
{
    if (parentID == EntityID())
    {
        auto it = entities.find(id);

        if (it == entities.end())
        {
            return;
        }

        RenderVREntity* renderEntity = it->second;

        removeLights(renderEntity);

        entities.erase(it);
        entitiesToDelete.push_back(RenderDeletionTask(renderEntity));
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child->remove(parentID, id);
        }
    }
}

void VRRender::update(EntityID parentID, const Entity* entity)
{
    BodyPartMask bodyPartMask;

    if (
        controller->context()->viewerMode()->mode() == Viewer_Sub_Mode_Body &&
        controller->context()->viewerMode()->bodyMode()->activeEntity() &&
        !controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs().empty()
    )
    {
        bodyPartMask = {
            controller->context()->viewerMode()->bodyMode()->activeEntity()->id(),
            controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs()
        };
    }

    if (parentID == EntityID())
    {
        auto it = entities.find(entity->id());

        if (it == entities.end())
        {
            add(parentID, entity);
            return;
        }

        RenderVREntity* renderEntity = it->second;

        renderEntity->update(entity, bodyPartMask);

        updateLights(renderEntity);
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child->update(parentID, entity, bodyPartMask);
        }
    }
}

void VRRender::removeDecoration(RenderDecorationID id)
{
    auto it = decorations.find(id);

    if (it == decorations.end())
    {
        return;
    }

    decorationsToDelete.push_back(RenderDeletionTask(it->second));

    decorations.erase(it);
}

void VRRender::setDecorationShown(RenderDecorationID id, bool state)
{
    auto it = decorations.find(id);

    if (it == decorations.end())
    {
        return;
    }

    it->second->show(state);
}

void VRRender::removePostProcessEffect(RenderPostProcessEffectID id)
{
    auto it = postProcessEffects.find(id);

    if (it == postProcessEffects.end())
    {
        return;
    }

    postProcessEffectsToDelete.push_back(RenderDeletionTask(it->second));

    postProcessEffects.erase(it);
}

void VRRender::setPostProcessEffectActive(RenderPostProcessEffectID id, bool state)
{
    auto it = postProcessEffects.find(id);

    if (it == postProcessEffects.end())
    {
        return;
    }

    it->second->show(state);
}

void VRRender::setPostProcessEffectOrder(const vector<RenderPostProcessEffectID>& order)
{
    postProcessOrder = join({ postProcessOrder.begin(), postProcessOrder.begin() + fixedPostProcessStageCount }, order);
}

RenderContext* VRRender::context() const
{
    return ctx;
}

u32 VRRender::imageCount() const
{
    return swapchainElements.size();
}

u32 VRRender::currentImage() const
{
    return imageIndex;
}

VkRenderPass VRRender::panelRenderPass() const
{
    return _panelRenderPass;
}

VkFramebuffer VRRender::textTitlecardFramebuffer() const
{
    return swapchainElements.at(imageIndex)->textTitlecard()->framebuffer;
}

VkFramebuffer VRRender::textFramebuffer() const
{
    return swapchainElements.at(imageIndex)->text()->framebuffer;
}

VkFramebuffer VRRender::hudFramebuffer() const
{
    return swapchainElements.at(imageIndex)->hud()->framebuffer;
}

VkFramebuffer VRRender::keyboardFramebuffer() const
{
    return swapchainElements.at(imageIndex)->keyboard()->framebuffer;
}

f32 VRRender::timer() const
{
    return _timer;
}

bool VRRender::rayTracingSupported() const
{
    return ctx->rayTracingSupported();
}

bool VRRender::rayTracingEnabled() const
{
    return _rayTracing;
}

f32 VRRender::fogDistance() const
{
    if (controller->selfWorld())
    {
        return controller->selfWorld()->fogDistance(averageCameraPosition());
    }
    else
    {
        return 0;
    }
}

mat4 VRRender::worldSpaceEyeTransform() const
{
    return { worldSpaceEyePosition, worldSpaceEyeRotation, vec3(1) };
}

tuple<u32, u32> VRRender::size() const
{
    return { width, height };
}

f64 VRRender::angle() const
{
    return _angle;
}

void VRRender::showKeyboard(bool state)
{
    keyboard = state;
}

void VRRender::setKeyboard(const vec3& position, const quaternion& rotation)
{
    keyboardPosition = position;
    keyboardRotation = rotation;
}

u32 VRRender::pickAPIVersion() const
{
    XrResult result;

    XrGraphicsRequirementsVulkanKHR graphicsRequirements{};
    graphicsRequirements.type = XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR;

    result = GET_XR_EXTENSION_FUNCTION(dynamic_cast<VR*>(controller->vr())->instance(), xrGetVulkanGraphicsRequirementsKHR)(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        &graphicsRequirements
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to get Vulkan graphics requirements: " + openxrError(result));
    }

    u32 minMajorVulkanVersion = XR_VERSION_MAJOR(graphicsRequirements.minApiVersionSupported);
    u32 minMinorVulkanVersion = XR_VERSION_MINOR(graphicsRequirements.minApiVersionSupported);

    u32 maxMajorVulkanVersion = XR_VERSION_MAJOR(graphicsRequirements.maxApiVersionSupported);
    u32 maxMinorVulkanVersion = XR_VERSION_MINOR(graphicsRequirements.maxApiVersionSupported);

    u32 majorVulkanVersion;
    u32 minorVulkanVersion;

    if (desiredMajorVulkanVersion < minMajorVulkanVersion)
    {
        majorVulkanVersion = minMajorVulkanVersion;
        minorVulkanVersion = minMinorVulkanVersion;
    }
    else if (desiredMajorVulkanVersion == minMajorVulkanVersion)
    {
        majorVulkanVersion = desiredMajorVulkanVersion;
        minorVulkanVersion = clamp(
            desiredMinorVulkanVersion,
            minMinorVulkanVersion,
            minMajorVulkanVersion == maxMajorVulkanVersion ? maxMinorVulkanVersion : numeric_limits<u32>::max()
        );
    }
    else if (desiredMajorVulkanVersion == maxMajorVulkanVersion)
    {
        majorVulkanVersion = desiredMajorVulkanVersion;
        minorVulkanVersion = clamp(
            desiredMinorVulkanVersion,
            minMajorVulkanVersion == maxMajorVulkanVersion ? minMinorVulkanVersion : 0,
            maxMajorVulkanVersion
        );
    }
    else if (desiredMajorVulkanVersion > maxMajorVulkanVersion)
    {
        majorVulkanVersion = maxMajorVulkanVersion;
        minorVulkanVersion = maxMinorVulkanVersion;
    }
    else
    {
        majorVulkanVersion = desiredMajorVulkanVersion;
        minorVulkanVersion = desiredMinorVulkanVersion;
    }

    return VK_MAKE_API_VERSION(0, majorVulkanVersion, minorVulkanVersion, 0);
}

set<string> VRRender::instanceExtensions() const
{
    XrResult result;

    u32 instanceExtensionsSize;
    char* instanceExtensionsData;

    result = GET_XR_EXTENSION_FUNCTION(dynamic_cast<VR*>(controller->vr())->instance(), xrGetVulkanInstanceExtensionsKHR)( 
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        0,
        &instanceExtensionsSize,
        nullptr
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate required Vulkan extensions: " + openxrError(result));
    }

    instanceExtensionsData = new char[instanceExtensionsSize];

    result = GET_XR_EXTENSION_FUNCTION(dynamic_cast<VR*>(controller->vr())->instance(), xrGetVulkanInstanceExtensionsKHR)(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        instanceExtensionsSize,
        &instanceExtensionsSize,
        instanceExtensionsData
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate required Vulkan extensions: " + openxrError(result));
    }

    set<string> instanceExtensions;

    u32 last = 0;
    for (u32 i = 0; i <= instanceExtensionsSize; i++)
    {
        if (i == instanceExtensionsSize || instanceExtensionsData[i] == ' ')
        {
            instanceExtensions.insert(string(instanceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] instanceExtensionsData;

    instanceExtensions.insert("VK_KHR_get_physical_device_properties2");

    return instanceExtensions;
}

VkPhysicalDevice VRRender::pickPhysicalDevice()
{
    VkPhysicalDevice physDevice;

    XrResult result = GET_XR_EXTENSION_FUNCTION(dynamic_cast<VR*>(controller->vr())->instance(), xrGetVulkanGraphicsDeviceKHR)(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        ctx->instance,
        &physDevice
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to identify Vulkan device: " + openxrError(result));
    }

    return physDevice;
}

set<string> VRRender::deviceExtensions() const
{
    u32 deviceExtensionsSize;
    char* deviceExtensionsData;

    XrResult result = GET_XR_EXTENSION_FUNCTION(dynamic_cast<VR*>(controller->vr())->instance(), xrGetVulkanDeviceExtensionsKHR)(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        0,
        &deviceExtensionsSize,
        nullptr
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to identify Vulkan device extensions: " + openxrError(result));
    }

    deviceExtensionsData = new char[deviceExtensionsSize];

    result = GET_XR_EXTENSION_FUNCTION(dynamic_cast<VR*>(controller->vr())->instance(), xrGetVulkanDeviceExtensionsKHR)(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        deviceExtensionsSize,
        &deviceExtensionsSize,
        deviceExtensionsData
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to identify Vulkan device extensions: " + openxrError(result));
    }

    set<string> deviceExtensions;

    // Compatibility with 1.1
    if (VK_API_VERSION_MAJOR(ctx->apiVersion) <= 1 && VK_API_VERSION_MINOR(ctx->apiVersion) < 2)
    {
        deviceExtensions.insert("VK_KHR_create_renderpass2");
        deviceExtensions.insert("VK_KHR_depth_stencil_resolve");

        if (ctx->rayTracingEnabled())
        {
            deviceExtensions.insert("VK_KHR_buffer_device_address");
        }
    }

    // Compatibility with 1.0
    if (VK_API_VERSION_MAJOR(ctx->apiVersion) <= 1 && VK_API_VERSION_MINOR(ctx->apiVersion) < 1)
    {
        deviceExtensions.insert("VK_KHR_multiview");
        deviceExtensions.insert("VK_KHR_maintenance1");
        deviceExtensions.insert("VK_KHR_maintenance2");
    }

    u32 last = 0;
    for (u32 i = 0; i <= deviceExtensionsSize; i++)
    {
        if (i == deviceExtensionsSize || deviceExtensionsData[i] == ' ')
        {
            deviceExtensions.insert(string(deviceExtensionsData + last, i - last));
            last = i + 1;
        }
    }

    delete[] deviceExtensionsData;

    return join(deviceExtensions, ctx->rayTracingDeviceExtensions());
}

u32 VRRender::prepareSwapchain()
{
    // This must be initialized after the instance, physical device and logical device, but before the swapchain
    dynamic_cast<VR*>(controller->vr())->initSession(ctx->instance, ctx->physDevice, ctx->device, ctx->graphicsQueueIndex);

    createSwapchain();

    u32 configViewCount = eyeCount;
    vector<XrViewConfigurationView> configViews(
        configViewCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    XrResult result = xrEnumerateViewConfigurationViews(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewCount,
        &configViewCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate view configuration views: " + openxrError(result));
    }

    u32 imageCount;

    result = xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate swapchain images: " + openxrError(result));
    }

    return imageCount;
}

void VRRender::updateCameraUniforms(const vector<XrView>& eyeViews)
{
    swapchainElements.at(imageIndex)->worldCameraMapping()->data[0] = projectionMatrix(eyeViews.at(0));
    swapchainElements.at(imageIndex)->worldCameraMapping()->data[1] = projectionMatrix(eyeViews.at(1));
    swapchainElements.at(imageIndex)->worldCameraMapping()->data[2] = worldViewMatrix(eyeViews.at(0));
    swapchainElements.at(imageIndex)->worldCameraMapping()->data[3] = worldViewMatrix(eyeViews.at(1));

    swapchainElements.at(imageIndex)->roomCameraMapping()->data[0] = projectionMatrix(eyeViews.at(0));
    swapchainElements.at(imageIndex)->roomCameraMapping()->data[1] = projectionMatrix(eyeViews.at(1));
    swapchainElements.at(imageIndex)->roomCameraMapping()->data[2] = roomViewMatrix(eyeViews.at(0));
    swapchainElements.at(imageIndex)->roomCameraMapping()->data[3] = roomViewMatrix(eyeViews.at(1));
}

void VRRender::updateTextTitlecardUniforms(const vec3& position, const vec3& direction)
{
    fmat4 model = createVRTextTitlecardTransform<f32>(position, direction);
    memcpy(swapchainElements.at(imageIndex)->textTitlecard()->uniformMapping->data, &model, sizeof(model));
}

void VRRender::updateTextUniforms(const vec3& position, const vec3& direction)
{
    fmat4 model = createVRTextTransform<f32>(position, direction);
    memcpy(swapchainElements.at(imageIndex)->text()->uniformMapping->data, &model, sizeof(model));
}

void VRRender::updateHUDUniforms(const vec3& position)
{
    fmat4 model = createVRHUDTransform<f32>(
        position,
        width,
        height,
        _angle
    );
    memcpy(swapchainElements.at(imageIndex)->hud()->uniformMapping->data, &model, sizeof(model));

    reinterpret_cast<f32*>(swapchainElements.at(imageIndex)->hud()->uniformMapping->data)[sizeof(fmat4) / sizeof(f32)] = 1;
}

void VRRender::updateKeyboardUniforms()
{
    fmat4 model = createVRKeyboardTransform<f32>(
        keyboardPosition,
        keyboardRotation
    );
    memcpy(swapchainElements.at(imageIndex)->keyboard()->uniformMapping->data, &model, sizeof(model));
}

void VRRender::render(VRSwapchainElement* swapchainElement)
{
    VkResult result;

    VkCommandBuffer transferCommandBuffer = swapchainElement->transferCommandBuffer();
    VkCommandBuffer commandBuffer = swapchainElement->commandBuffer();

    // Begin
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(transferCommandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    renderSky(swapchainElement);

    // Main
    if (_rayTracing)
    {
        swapchainElement->hardwareOcclusionScene()->update(commandBuffer, cameraPosition, entities);
    }
    else
    {
        swapchainElement->softwareOcclusionScene()->update(cameraPosition, entities);
    }

    vector<VkClearValue> clearValues;

    if (sampleCount == VK_SAMPLE_COUNT_1_BIT)
    {
        clearValues = fillClearValues({ Clear_Value_Color, Clear_Value_Depth, Clear_Value_Stencil });
    }
    else
    {
        clearValues = fillClearValues({ Clear_Value_Color, Clear_Value_Depth, Clear_Value_Stencil, Clear_Value_Color, Clear_Value_Depth, Clear_Value_Stencil });
    }

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = mainRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->mainFramebuffer();
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { width, height }
    };
    beginRenderPassInfo.clearValueCount = clearValues.size();
    beginRenderPassInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    if (controller->selfWorld())
    {
        sky->work(
            swapchainElement,
            controller->selfWorld()->sky(averageCameraPosition()),
            controller->selfWorld()->up(averageCameraPosition())
        );

        atmosphere->work(
            swapchainElement,
            controller->selfWorld()->atmosphere(averageCameraPosition()),
            cameraPosition,
            controller->selfWorld()->up(averageCameraPosition()),
            controller->selfWorld()->gravity(averageCameraPosition()),
            controller->selfWorld()->flowVelocity(averageCameraPosition()),
            controller->selfWorld()->density(averageCameraPosition())
        );
    }

    deque<RenderTransparentDraw> transparentDraws;

    for (const auto& [ id, entity ] : entities)
    {
        entity->work(
            swapchainElement,
            cameraPosition,
            &transparentDraws,
            controller->context()->viewerMode()->entityMode()->hiddenEntityIDs()
        );
    }

    sort(
        transparentDraws.begin(),
        transparentDraws.end(),
        [this](const RenderTransparentDraw& a, const RenderTransparentDraw& b) -> bool
        {
            return a.position().distance(averageCameraPosition()) > b.position().distance(averageCameraPosition());
        }
    );

    for (const RenderTransparentDraw& transparentDraw : transparentDraws)
    {
        transparentDraw.draw();
    }

    vkCmdEndRenderPass(commandBuffer);

    // Luminance computation for next frame
    computeLuminance(swapchainElement);

    // Clear value because multisampled must clear when doing post-process/HDR passes
    if (g_config.antiAliasingLevel == 1)
    {
        beginRenderPassInfo.clearValueCount = 0;
        beginRenderPassInfo.pClearValues = nullptr;
    }
    else
    {
        clearValues = fillClearValues({ Clear_Value_Color });
        beginRenderPassInfo.clearValueCount = clearValues.size();
        beginRenderPassInfo.pClearValues = clearValues.data();
    }

    // HDR
    copyImage(
        commandBuffer,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        width,
        height,
        VK_IMAGE_ASPECT_COLOR_BIT,
        eyeCount,

        swapchainElement->colorImage(),
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,

        swapchainElement->sideImage(),
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    beginRenderPassInfo.renderPass = hdrRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->hdrFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    hdr->work(swapchainElement, _timer);

    vkCmdEndRenderPass(commandBuffer);

    // Post process
    for (RenderPostProcessEffectID id : postProcessOrder)
    {
        RenderVRPostProcessEffect* postProcessEffect = postProcessEffects.at(id);

        if (!postProcessEffect->shown())
        {
            continue;
        }

        copyImage(
            commandBuffer,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            width,
            height,
            VK_IMAGE_ASPECT_COLOR_BIT,
            eyeCount,

            swapchainElement->colorImage(),
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,

            swapchainElement->sideImage(),
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );

        beginRenderPassInfo.renderPass = postProcessRenderPass;
        beginRenderPassInfo.framebuffer = swapchainElement->postProcessFramebuffer();
        vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        postProcessEffect->work(swapchainElement, _timer);

        vkCmdEndRenderPass(commandBuffer);
    }

    // Overlay
    beginRenderPassInfo.renderPass = overlayRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->overlayFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (const auto& [ id, decoration ] : decorations)
    {
        if (!decoration->shown() || decoration->drawType() != Render_Decoration_Default)
        {
            continue;
        }

        decoration->work(swapchainElement, cameraPosition);
    }

    renderTextTitlecard(commandBuffer);

    renderHUD(commandBuffer);

    renderText(commandBuffer);

    if (keyboard)
    {
        renderKeyboard(commandBuffer);
    }

    for (const auto& [ id, decoration ] : decorations)
    {
        if (!decoration->shown() || decoration->drawType() != Render_Decoration_Over_Panels)
        {
            continue;
        }

        decoration->work(swapchainElement, cameraPosition);
    }

    vkCmdEndRenderPass(commandBuffer);

    // Overlay independent depth
    clearValues = fillClearValues({ Clear_Value_Color, Clear_Value_Depth });

    beginRenderPassInfo.clearValueCount = clearValues.size();
    beginRenderPassInfo.pClearValues = clearValues.data();

    beginRenderPassInfo.renderPass = independentDepthOverlayRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->independentDepthOverlayFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (const auto& [ id, decoration ] : decorations)
    {
        if (!decoration->shown() || decoration->drawType() != Render_Decoration_Independent_Depth)
        {
            continue;
        }

        decoration->work(swapchainElement, cameraPosition);
    }

    vkCmdEndRenderPass(commandBuffer);

    // Output
    outputImage(swapchainElement);

    // End
    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }

    result = vkEndCommandBuffer(transferCommandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }
}

void VRRender::renderTextTitlecard(VkCommandBuffer commandBuffer)
{
    VkViewport viewport = {
        0, 0,
        static_cast<f32>(width), static_cast<f32>(height),
        0, 1
    };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineStore->panel.pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineStore->panel.pipelineLayout,
        0,
        1,
        &swapchainElements.at(imageIndex)->textTitlecard()->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, 6, 1, 0, 0);
}

void VRRender::renderText(VkCommandBuffer commandBuffer)
{
    VkViewport viewport = {
        0, 0,
        static_cast<f32>(width), static_cast<f32>(height),
        0, 1
    };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineStore->panel.pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineStore->panel.pipelineLayout,
        0,
        1,
        &swapchainElements.at(imageIndex)->text()->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, 6, 1, 0, 0);
}

void VRRender::renderHUD(VkCommandBuffer commandBuffer)
{
    VkViewport viewport = {
        0, 0,
        static_cast<f32>(width), static_cast<f32>(height),
        0, 1
    };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineStore->hud.pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineStore->hud.pipelineLayout,
        0,
        1,
        &swapchainElements.at(imageIndex)->hud()->descriptorSet,
        0,
        nullptr
    );

    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, &hudVertexBuffer.buffer, &offset);

    vkCmdDraw(commandBuffer, 6 * 24, 1, 0, 0);
}

void VRRender::renderKeyboard(VkCommandBuffer commandBuffer)
{
    VkViewport viewport = {
        0, 0,
        static_cast<f32>(width), static_cast<f32>(height),
        0, 1
    };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineStore->panel.pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineStore->panel.pipelineLayout,
        0,
        1,
        &swapchainElements.at(imageIndex)->keyboard()->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, 6, 1, 0, 0);
}

void VRRender::renderSky(const VRSwapchainElement* swapchainElement)
{
    vector<VkClearValue> clearValues = fillClearValues({ Clear_Value_Color });

    VkRenderPassBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginInfo.renderPass = skyRenderPass;
    beginInfo.framebuffer = swapchainElement->sky()->framebuffer;
    beginInfo.renderArea = {
        { 0, 0 },
        { skyWidth, skyHeight }
    };
    beginInfo.clearValueCount = clearValues.size();
    beginInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass(swapchainElement->commandBuffer(), &beginInfo, VK_SUBPASS_CONTENTS_INLINE);

    if (controller->selfWorld())
    {
        sky->renderEnvironmentMap(
            swapchainElement->sky(),
            controller->selfWorld()->sky(averageCameraPosition()),
            controller->selfWorld()->up(averageCameraPosition())
        );
    }

    vkCmdEndRenderPass(swapchainElement->commandBuffer());
}

void VRRender::outputImage(const VRSwapchainElement* swapchainElement)
{
    VkImageLayout previousInputLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

    if (dynamic_cast<VR*>(controller->vr())->steamvr())
    {
        applySteamVRColorAdjustment(swapchainElement);
        previousInputLayout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    }

    VkImage inputImage = swapchainElement->colorImage();
    VkImage outputImage = swapchainElement->outputImage();

    VkImageMemoryBarrier beforeBarriers[2];

    beforeBarriers[0] = imageMemoryBarrier(
        inputImage,
        VK_IMAGE_ASPECT_COLOR_BIT,
        previousInputLayout,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );

    beforeBarriers[1] = imageMemoryBarrier(
        outputImage,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    vkCmdPipelineBarrier(
        swapchainElement->commandBuffer(),
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0,
        nullptr,
        0,
        nullptr,
        sizeof(beforeBarriers) / sizeof(VkImageMemoryBarrier),
        beforeBarriers
    );

    VkImageBlit region{};
    region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.srcSubresource.mipLevel = 0;
    region.srcSubresource.baseArrayLayer = 0;
    region.srcSubresource.layerCount = eyeCount;
    region.srcOffsets[0] = { 0, 0, 0 };
    region.srcOffsets[1] = { static_cast<i32>(width), static_cast<i32>(height), 1 };
    region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.dstSubresource.mipLevel = 0;
    region.dstSubresource.baseArrayLayer = 0;
    region.dstSubresource.layerCount = eyeCount;
    region.dstOffsets[0] = { 0, 0, 0 };
    region.dstOffsets[1] = { static_cast<i32>(width), static_cast<i32>(height), 1 };

    vkCmdBlitImage(
        swapchainElement->commandBuffer(),
        inputImage,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        outputImage,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region,
        VK_FILTER_NEAREST
    );

    // This is to work around an issue in SteamVR where the right eye image is expected in VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    if (dynamic_cast<VR*>(controller->vr())->steamvr())
    {
        VkImageMemoryBarrier leftEyeBarrier = imageMemoryBarrier(
            outputImage,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        );
        leftEyeBarrier.subresourceRange.layerCount = 1;

        VkImageMemoryBarrier rightEyeBarrier = imageMemoryBarrier(
            outputImage,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        );
        rightEyeBarrier.subresourceRange.baseArrayLayer = 1;
        rightEyeBarrier.subresourceRange.layerCount = 1;

        VkImageMemoryBarrier afterBarriers[] = {
            leftEyeBarrier,
            rightEyeBarrier
        };

        vkCmdPipelineBarrier(
            swapchainElement->commandBuffer(),
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0,
            nullptr,
            0,
            nullptr,
            sizeof(afterBarriers) / sizeof(VkImageMemoryBarrier),
            afterBarriers
        );
    }
    else
    {
        VkImageMemoryBarrier afterBarrier = imageMemoryBarrier(
            outputImage,
            VK_IMAGE_ASPECT_COLOR_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            VK_IMAGE_LAYOUT_GENERAL
        );

        vkCmdPipelineBarrier(
            swapchainElement->commandBuffer(),
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            0,
            0,
            nullptr,
            0,
            nullptr,
            1,
            &afterBarrier
        );
    }
}

void VRRender::applySteamVRColorAdjustment(const VRSwapchainElement* swapchainElement)
{
    copyImage(
        swapchainElement->commandBuffer(),
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        width,
        height,
        VK_IMAGE_ASPECT_COLOR_BIT,
        eyeCount,

        swapchainElement->colorImage(),
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,

        swapchainElement->sideImage(),
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    VkRenderPassBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginInfo.renderPass = postProcessRenderPass;
    beginInfo.framebuffer = swapchainElement->postProcessFramebuffer();
    beginInfo.renderArea = {
        { 0, 0 },
        { width, height }
    };

    vkCmdBeginRenderPass(swapchainElement->commandBuffer(), &beginInfo, VK_SUBPASS_CONTENTS_INLINE);

    steamvrColorAdjustment->work(swapchainElement, _timer);

    vkCmdEndRenderPass(swapchainElement->commandBuffer());
}

void VRRender::advanceDeletionQueues()
{
    for (size_t i = 0; i < entitiesToDelete.size();)
    {
        auto& [ element, stepCount ] = entitiesToDelete[i];
        stepCount++;

        if (stepCount == swapchainElements.size())
        {
            delete element;
            entitiesToDelete.erase(entitiesToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (size_t i = 0; i < decorationsToDelete.size();)
    {
        auto& [ decoration, stepCount ] = decorationsToDelete[i];
        stepCount++;

        if (stepCount == swapchainElements.size())
        {
            delete decoration;
            decorationsToDelete.erase(decorationsToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (size_t i = 0; i < postProcessEffectsToDelete.size();)
    {
        auto& [ postProcessEffect, stepCount ] = postProcessEffectsToDelete[i];
        stepCount++;

        if (stepCount == swapchainElements.size())
        {
            delete postProcessEffect;
            postProcessEffectsToDelete.erase(postProcessEffectsToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }
}

void VRRender::createSwapchain()
{
    XrResult result;

    u32 configViewCount = eyeCount;
    vector<XrViewConfigurationView> configViews(
        configViewCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    result = xrEnumerateViewConfigurationViews(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewCount,
        &configViewCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate view configuration views: " + openxrError(result));
    }

    u32 formatCount = 0;

    result = xrEnumerateSwapchainFormats(dynamic_cast<VR*>(controller->vr())->session(), 0, &formatCount, nullptr);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate swapchain formats: " + openxrError(result));
    }

    vector<i64> formats(formatCount);

    result = xrEnumerateSwapchainFormats(dynamic_cast<VR*>(controller->vr())->session(), formatCount, &formatCount, formats.data());

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate swapchain formats: " + openxrError(result));
    }

    i64 chosenFormat = formats.front();
    f32 bestScore = numeric_limits<f32>::lowest();

    for (i64 format : formats)
    {
        f32 score;

        switch (format)
        {
        default :
            score = 0;
            break;
        case VK_FORMAT_R8G8B8A8_SRGB :
        case VK_FORMAT_B8G8R8A8_SRGB :
            score = 1;
            break;
        case VK_FORMAT_R8G8B8A8_UNORM :
        case VK_FORMAT_B8G8R8A8_UNORM :
            score = 2;
            break;
        case VK_FORMAT_R16G16B16A16_SFLOAT :
            score = 3;
            break;
        case VK_FORMAT_R32G32B32A32_SFLOAT :
            score = 4;
            break;
        }

        if (score > bestScore)
        {
            chosenFormat = format;
            bestScore = score;
        }
    }

    width = configViews.front().recommendedImageRectWidth;
    height = configViews.front().recommendedImageRectHeight;
    format = static_cast<VkFormat>(chosenFormat);
    sampleCount = static_cast<VkSampleCountFlagBits>(configViews.front().recommendedSwapchainSampleCount);

    XrSwapchainCreateInfo swapchainCreateInfo{};
    swapchainCreateInfo.type = XR_TYPE_SWAPCHAIN_CREATE_INFO;
    swapchainCreateInfo.createFlags = 0;
    swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_TRANSFER_DST_BIT;
    swapchainCreateInfo.format = chosenFormat;
    swapchainCreateInfo.sampleCount = VK_SAMPLE_COUNT_1_BIT;
    swapchainCreateInfo.width = configViews.front().recommendedImageRectWidth;
    swapchainCreateInfo.height = configViews.front().recommendedImageRectHeight;
    swapchainCreateInfo.faceCount = 1;
    swapchainCreateInfo.arraySize = configViews.size();
    swapchainCreateInfo.mipCount = 1;

    if (dynamic_cast<VR*>(controller->vr())->monado())
    {
        swapchainCreateInfo.usageFlags |= XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
    }

    result = xrCreateSwapchain(dynamic_cast<VR*>(controller->vr())->session(), &swapchainCreateInfo, &swapchain);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create swapchain: " + openxrError(result));
    }
}

void VRRender::destroySwapchain()
{
    xrDestroySwapchain(swapchain);
}

void VRRender::createSwapchainElements()
{
    XrResult result;

    u32 configViewCount = eyeCount;
    vector<XrViewConfigurationView> configViews(
        configViewCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    result = xrEnumerateViewConfigurationViews(
        dynamic_cast<VR*>(controller->vr())->instance(),
        dynamic_cast<VR*>(controller->vr())->systemID(),
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewCount,
        &configViewCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate view configuration views: " + openxrError(result));
    }

    u32 imageCount;

    result = xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate swapchain images: " + openxrError(result));
    }

    vector<XrSwapchainImageVulkanKHR> images(
        imageCount,
        { .type = XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR }
    );

    for (u32 eye = 0; eye < configViews.size(); eye++)
    {
        result = xrEnumerateSwapchainImages(
            swapchain,
            imageCount,
            &imageCount,
            reinterpret_cast<XrSwapchainImageBaseHeader*>(images.data())
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to enumerate swapchain images: " + openxrError(result));
        }
    }

    for (u32 i = 0; i < imageCount; i++)
    {
        const XrSwapchainImageVulkanKHR& image = images.at(i);

        if (sampleCount == VK_SAMPLE_COUNT_1_BIT)
        {
            swapchainElements.push_back(new VRMonosampleSwapchainElement(
                ctx,
                i,
                width,
                height,
                _panelRenderPass,
                skyRenderPass,
                mainRenderPass,
                hdrRenderPass,
                postProcessRenderPass,
                overlayRenderPass,
                independentDepthOverlayRenderPass,
                pipelineStore->panelDescriptorSetLayout,
                format,
                image.image
            ));
        }
        else
        {
            swapchainElements.push_back(new VRMultisampleSwapchainElement(
                ctx,
                i,
                width,
                height,
                sampleCount,
                _panelRenderPass,
                skyRenderPass,
                mainRenderPass,
                hdrRenderPass,
                postProcessRenderPass,
                overlayRenderPass,
                independentDepthOverlayRenderPass,
                pipelineStore->panelDescriptorSetLayout,
                format,
                image.image
            ));
        }
    }
}

void VRRender::destroySwapchainElements()
{
    for (VRSwapchainElement* element : swapchainElements)
    {
        delete element;
    }

    swapchainElements.clear();
}

vec3 VRRender::averageCameraPosition() const
{
    return (cameraPosition[0] + cameraPosition[1]) / 2;
}

fmat4 VRRender::projectionMatrix(XrView eyeView) const
{
    return createVRPerspectiveMatrix<f32>(
        eyeView.fov.angleUp,
        eyeView.fov.angleDown,
        eyeView.fov.angleLeft,
        eyeView.fov.angleRight,
        defaultNearDistance,
        defaultFarDistance
    );
}

fmat4 VRRender::worldViewMatrix(XrView eyeView) const
{
    quaternion rotation = controller->vr()->roomToWorld({
        eyeView.pose.orientation.w,
        eyeView.pose.orientation.x,
        eyeView.pose.orientation.z * -1,
        eyeView.pose.orientation.y
    });

    return createViewMatrix<f32>(
        fvec3(),
        rotation
    );
}

fmat4 VRRender::roomViewMatrix(XrView eyeView) const
{
    vec3 position = {
        eyeView.pose.position.x,
        eyeView.pose.position.z * -1,
        eyeView.pose.position.y
    };
    quaternion rotation = {
        eyeView.pose.orientation.w,
        eyeView.pose.orientation.x,
        eyeView.pose.orientation.z * -1,
        eyeView.pose.orientation.y
    };

    return createViewMatrix<f32>(
        position,
        rotation
    );
}

void VRRender::computeLuminance(VRSwapchainElement* element) const
{
    u32 scaledWidth = width * g_config.luminanceScale;
    u32 scaledHeight = height * g_config.luminanceScale;

    u32 luminanceSize = luminanceImageSize(scaledWidth, scaledHeight);

    transitionLayout(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        eyeCount
    );

    VkClearColorValue color{};

    VkImageSubresourceRange range{};
    range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    range.baseMipLevel = 0;
    range.levelCount = 1;
    range.baseArrayLayer = 0;
    range.layerCount = eyeCount;

    vkCmdClearColorImage(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        &color,
        1,
        &range
    );

    blitImage(
        element->commandBuffer(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        eyeCount,
        VK_FILTER_LINEAR,

        element->colorImage(),
        width,
        height,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,

        element->luminanceImage(),
        scaledWidth,
        scaledHeight,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    computeMipmaps(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        luminanceSize,
        luminanceSize,
        eyeCount
    );

    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        VkBufferImageCopy region{};
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = maxMipLevels(luminanceSize, luminanceSize) - 1;
        region.imageSubresource.baseArrayLayer = eye;
        region.imageSubresource.layerCount = 1;
        region.imageExtent = { 1, 1, 1 };
        region.bufferOffset = eye * sizeof(fvec4);

        vkCmdCopyImageToBuffer(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            element->luminance().buffer,
            1,
            &region
        );
    }

    transitionLayout(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        eyeCount
    );

    {
        VkClearColorValue color{};
        color.float32[0] = -1;
        color.float32[1] = 0;
        color.float32[2] = 0;
        color.float32[3] = +1;

        VkImageSubresourceRange range{};
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.baseArrayLayer = 0;
        range.layerCount = eyeCount;

        vkCmdClearColorImage(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            &color,
            1,
            &range
        );
    }

    blitImage(
        element->commandBuffer(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        eyeCount,
        VK_FILTER_LINEAR,

        element->stencilImage(),
        width,
        height,
        VK_IMAGE_LAYOUT_GENERAL,
        VK_IMAGE_LAYOUT_GENERAL,

        element->luminanceImage(),
        scaledWidth,
        scaledHeight,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    computeMipmaps(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        luminanceSize,
        luminanceSize,
        eyeCount
    );

    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        VkBufferImageCopy region{};
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = maxMipLevels(luminanceSize, luminanceSize) - 1;
        region.imageSubresource.baseArrayLayer = eye;
        region.imageSubresource.layerCount = 1;
        region.imageExtent = { 1, 1, 1 };
        region.bufferOffset = eyeCount * sizeof(fvec4) + eye * sizeof(fvec4);

        vkCmdCopyImageToBuffer(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            element->luminance().buffer,
            1,
            &region
        );
    }

    element->setLuminanceReady();
}

f32 VRRender::getLuminance(const VRSwapchainElement* element) const
{
    u32 scaledWidth = width * g_config.luminanceScale;
    u32 scaledHeight = height * g_config.luminanceScale;

    fvec4 color = (element->luminanceMapping()->data[0] + element->luminanceMapping()->data[1]) / 2;
    fvec4 stencil = (element->luminanceMapping()->data[2] + element->luminanceMapping()->data[3]) / 2;

    f32 stencilOccupancy = (stencil.x + 1) / 2;

    return color.toVec3().dot(luminanceFactor) * (1 / stencilOccupancy) * (1 / luminanceOccupancy(scaledWidth, scaledHeight));
}

void VRRender::adjustExposure(const chrono::microseconds& elapsed, f32 luminance)
{
    static constexpr f32 s = 100;
    static constexpr f32 k = 12.5;
    static constexpr f32 q = 0.65;

    luminance = clamp(luminance, renderAutoExposureMinLuminance, renderAutoExposureMaxLuminance);

    f32 lmax = (78 / (q * s)) * pow(2, log(luminance * (s / k), 2));

    f32 targetExposure = 1 / lmax;

    targetExposure *= renderAutoExposureMultiplier;

    exposure = exposure + (1 - exp(-(elapsed.count() / static_cast<f32>(renderAutoExposureDelay.count())))) * (targetExposure - exposure);
}
#endif
