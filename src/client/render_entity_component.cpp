/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_entity_component.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_descriptor_set_write_components.hpp"
#include "render_entity.hpp"

RenderEntityComponent::RenderEntityComponent(
    const RenderContext* ctx,
    const vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>>& materials,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
)
    // Binding can fail if the acceleration structure for this swapchain element hasn't yet been initialized, requiring a rebind at first render time
    : needsRebind(false)
    , ctx(ctx)
    , lastAccelerationStructure(nullptr)
{
    object = createBuffer(
        ctx->allocator,
        sizeof(EntityUniformGPU),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    objectMapping = new VmaMapping<f32>(ctx->allocator, object);

    for (const auto& [ type, material ] : materials)
    {
        createMaterialDescriptorSet(
            type,
            material,
            camera,
            environmentImageView,
            occlusionScene
        );
    }
}

RenderEntityComponent::~RenderEntityComponent()
{
    for (const auto& [ type, descriptorSet ] : materialDescriptorSets)
    {
        destroyDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSet);
    }

    delete objectMapping;

    destroyBuffer(ctx->allocator, object);
}

bool RenderEntityComponent::supports(const vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>>& materials) const
{
    if (materials.size() != materialDescriptorSets.size())
    {
        return false;
    }

    for (size_t i = 0; i < materials.size(); i++)
    {
        const auto& [ type, material ] = materials.at(i);

        if (type != get<0>(materialDescriptorSets.at(i)))
        {
            return false;
        }
    }

    return true;
}

void RenderEntityComponent::bind(
    const vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>>& materials,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
)
{
    // Acceleration structures are only created when they're first used so very early bindings will be prevented below
    // Acceleration structures are also re-created every single time the number of overall entities in the scene changes
    if (!needsRebind && !(
        occlusionScene.hardwareAccelerated() &&
        occlusionScene.hardwareScene(0) != lastAccelerationStructure
    ))
    {
        return;
    }

    for (size_t i = 0; i < materials.size(); i++)
    {
        const auto& [ type, material ] = materials.at(i);

        bind(get<1>(materialDescriptorSets.at(i)), type, material, camera, environmentImageView, occlusionScene);
    }

    needsRebind = false;
}

void RenderEntityComponent::createMaterialDescriptorSet(
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
)
{
    VkDescriptorSet descriptorSet = createDescriptorSet(
        ctx->device,
        ctx->descriptorPool,
        material->pipelines.descriptorSetLayout
    );

    bind(descriptorSet, type, material, camera, environmentImageView, occlusionScene);

    materialDescriptorSets.push_back({ type, descriptorSet });
}

void RenderEntityComponent::bind(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    switch (material->type)
    {
    case Body_Render_Material_Vertex :
        bindVertex(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Texture :
        bindTexture(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Area :
        bindArea(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Terrain :
        bindTerrain(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Rope :
        bindRope(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Shadeless_Vertex :
        bindShadelessVertex(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Shadeless_Texture :
        bindShadelessTexture(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Shadeless_Area :
        bindShadelessArea(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Shadeless_Terrain :
        bindShadelessTerrain(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    case Body_Render_Material_Shadeless_Rope :
        bindShadelessRope(descriptorSet, type, material, camera, environmentImageView, occlusionScene);
        break;
    }

    if (occlusionScene.hardwareAccelerated())
    {
        lastAccelerationStructure = occlusionScene.hardwareScene(0);
    }
}

void RenderEntityComponent::bindVertex(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorImageInfo environmentWriteImage{};
    VkWriteDescriptorSet environmentWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 2, environmentImageView, ctx->unfilteredSampler, &environmentWriteImage, &environmentWrite);

    VkAccelerationStructureKHR accelerationStructures[eyeCount];
    VkWriteDescriptorSetAccelerationStructureKHR occlusionSceneAccelerationStructure{};
    VkDescriptorBufferInfo occlusionSceneBufferInfo{};
    VkWriteDescriptorSet occlusionSceneWrite{};

    if (occlusionScene.hardwareAccelerated())
    {
        accelerationStructures[0] = occlusionScene.hardwareScene(0);
        accelerationStructures[1] = occlusionScene.hardwareScene(1);

        occlusionSceneAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        occlusionSceneAccelerationStructure.accelerationStructureCount = eyeCount;
        occlusionSceneAccelerationStructure.pAccelerationStructures = accelerationStructures;

        occlusionSceneWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        occlusionSceneWrite.pNext = &occlusionSceneAccelerationStructure;
        occlusionSceneWrite.dstSet = descriptorSet;
        occlusionSceneWrite.dstBinding = 3;
        occlusionSceneWrite.dstArrayElement = 0;
        occlusionSceneWrite.descriptorCount = eyeCount;
        occlusionSceneWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    }
    else
    {
        uniformBufferDescriptorSetWrite(descriptorSet, 3, occlusionScene.softwareScene(), &occlusionSceneBufferInfo, &occlusionSceneWrite);
    }

    vector<VkWriteDescriptorSet> descriptorWrites = {
        cameraWrite,
        objectWrite,
        environmentWrite
    };

    // Only write if we have a non-null value, if we have a null value this will be redone before render anyway
    if (
        !occlusionScene.hardwareAccelerated() ||
        accelerationStructures[0]
    )
    {
        descriptorWrites.push_back(occlusionSceneWrite);
    }

    vkUpdateDescriptorSets(
        ctx->device,
        descriptorWrites.size(),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

void RenderEntityComponent::bindTexture(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo materialWriteBuffer{};
    VkWriteDescriptorSet materialWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 2, material->uniform.buffer, &materialWriteBuffer, &materialWrite);

    vector<VkDescriptorImageInfo> imageWriteImages(maxTextureTileCount, VkDescriptorImageInfo{});
    VkWriteDescriptorSet imageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 3, material->textures.front()->imageViews(), ctx->unfilteredSampler, &imageWriteImages, &imageWrite);

    VkDescriptorImageInfo environmentWriteImage{};
    VkWriteDescriptorSet environmentWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 4, environmentImageView, ctx->unfilteredSampler, &environmentWriteImage, &environmentWrite);

    VkAccelerationStructureKHR accelerationStructures[eyeCount];
    VkWriteDescriptorSetAccelerationStructureKHR occlusionSceneAccelerationStructure{};
    VkDescriptorBufferInfo occlusionSceneBufferInfo{};
    VkWriteDescriptorSet occlusionSceneWrite{};

    if (occlusionScene.hardwareAccelerated())
    {
        accelerationStructures[0] = occlusionScene.hardwareScene(0);
        accelerationStructures[1] = occlusionScene.hardwareScene(1);

        occlusionSceneAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        occlusionSceneAccelerationStructure.accelerationStructureCount = eyeCount;
        occlusionSceneAccelerationStructure.pAccelerationStructures = accelerationStructures;

        occlusionSceneWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        occlusionSceneWrite.pNext = &occlusionSceneAccelerationStructure;
        occlusionSceneWrite.dstSet = descriptorSet;
        occlusionSceneWrite.dstBinding = 5;
        occlusionSceneWrite.dstArrayElement = 0;
        occlusionSceneWrite.descriptorCount = eyeCount;
        occlusionSceneWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    }
    else
    {
        uniformBufferDescriptorSetWrite(descriptorSet, 5, occlusionScene.softwareScene(), &occlusionSceneBufferInfo, &occlusionSceneWrite);
    }

    vector<VkWriteDescriptorSet> descriptorWrites = {
        cameraWrite,
        objectWrite,
        materialWrite,
        imageWrite,
        environmentWrite
    };

    // Only write if we have a non-null value, if we have a null value this will be redone before render anyway
    if (
        !occlusionScene.hardwareAccelerated() ||
        accelerationStructures[0]
    )
    {
        descriptorWrites.push_back(occlusionSceneWrite);
    }

    vkUpdateDescriptorSets(
        ctx->device,
        descriptorWrites.size(),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

void RenderEntityComponent::bindArea(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo materialWriteBuffer{};
    VkWriteDescriptorSet materialWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 2, material->uniform.buffer, &materialWriteBuffer, &materialWrite);

    VkDescriptorImageInfo noiseWriteImage{};
    VkWriteDescriptorSet noiseWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 3, ctx->areaSurfaceNoiseImageView, ctx->filteredSampler, &noiseWriteImage, &noiseWrite);

    VkDescriptorImageInfo environmentWriteImage{};
    VkWriteDescriptorSet environmentWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 4, environmentImageView, ctx->unfilteredSampler, &environmentWriteImage, &environmentWrite);

    VkAccelerationStructureKHR accelerationStructures[eyeCount];
    VkWriteDescriptorSetAccelerationStructureKHR occlusionSceneAccelerationStructure{};
    VkDescriptorBufferInfo occlusionSceneBufferInfo{};
    VkWriteDescriptorSet occlusionSceneWrite{};

    if (occlusionScene.hardwareAccelerated())
    {
        accelerationStructures[0] = occlusionScene.hardwareScene(0);
        accelerationStructures[1] = occlusionScene.hardwareScene(1);

        occlusionSceneAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        occlusionSceneAccelerationStructure.accelerationStructureCount = eyeCount;
        occlusionSceneAccelerationStructure.pAccelerationStructures = accelerationStructures;

        occlusionSceneWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        occlusionSceneWrite.pNext = &occlusionSceneAccelerationStructure;
        occlusionSceneWrite.dstSet = descriptorSet;
        occlusionSceneWrite.dstBinding = 5;
        occlusionSceneWrite.dstArrayElement = 0;
        occlusionSceneWrite.descriptorCount = eyeCount;
        occlusionSceneWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    }
    else
    {
        uniformBufferDescriptorSetWrite(descriptorSet, 4, occlusionScene.softwareScene(), &occlusionSceneBufferInfo, &occlusionSceneWrite);
    }

    vector<VkWriteDescriptorSet> descriptorWrites = {
        cameraWrite,
        objectWrite,
        materialWrite,
        noiseWrite,
        environmentWrite
    };

    // Only write if we have a non-null value, if we have a null value this will be redone before render anyway
    if (
        !occlusionScene.hardwareAccelerated() ||
        accelerationStructures[0]
    )
    {
        descriptorWrites.push_back(occlusionSceneWrite);
    }

    vkUpdateDescriptorSets(
        ctx->device,
        descriptorWrites.size(),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

void RenderEntityComponent::bindTerrain(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorImageInfo environmentWriteImage{};
    VkWriteDescriptorSet environmentWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 2, environmentImageView, ctx->unfilteredSampler, &environmentWriteImage, &environmentWrite);

    VkAccelerationStructureKHR accelerationStructures[eyeCount];
    VkWriteDescriptorSetAccelerationStructureKHR occlusionSceneAccelerationStructure{};
    VkDescriptorBufferInfo occlusionSceneBufferInfo{};
    VkWriteDescriptorSet occlusionSceneWrite{};

    if (occlusionScene.hardwareAccelerated())
    {
        accelerationStructures[0] = occlusionScene.hardwareScene(0);
        accelerationStructures[1] = occlusionScene.hardwareScene(1);

        occlusionSceneAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        occlusionSceneAccelerationStructure.accelerationStructureCount = eyeCount;
        occlusionSceneAccelerationStructure.pAccelerationStructures = accelerationStructures;

        occlusionSceneWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        occlusionSceneWrite.pNext = &occlusionSceneAccelerationStructure;
        occlusionSceneWrite.dstSet = descriptorSet;
        occlusionSceneWrite.dstBinding = 3;
        occlusionSceneWrite.dstArrayElement = 0;
        occlusionSceneWrite.descriptorCount = eyeCount;
        occlusionSceneWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    }
    else
    {
        uniformBufferDescriptorSetWrite(descriptorSet, 3, occlusionScene.softwareScene(), &occlusionSceneBufferInfo, &occlusionSceneWrite);
    }

    vector<VkWriteDescriptorSet> descriptorWrites = {
        cameraWrite,
        objectWrite,
        environmentWrite
    };

    // Only write if we have a non-null value, if we have a null value this will be redone before render anyway
    if (
        !occlusionScene.hardwareAccelerated() ||
        accelerationStructures[0]
    )
    {
        descriptorWrites.push_back(occlusionSceneWrite);
    }

    vkUpdateDescriptorSets(
        ctx->device,
        descriptorWrites.size(),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

void RenderEntityComponent::bindRope(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo materialWriteBuffer{};
    VkWriteDescriptorSet materialWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 2, material->uniform.buffer, &materialWriteBuffer, &materialWrite);

    VkDescriptorImageInfo environmentWriteImage{};
    VkWriteDescriptorSet environmentWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 3, environmentImageView, ctx->unfilteredSampler, &environmentWriteImage, &environmentWrite);

    VkAccelerationStructureKHR accelerationStructures[eyeCount];
    VkWriteDescriptorSetAccelerationStructureKHR occlusionSceneAccelerationStructure{};
    VkDescriptorBufferInfo occlusionSceneBufferInfo{};
    VkWriteDescriptorSet occlusionSceneWrite{};

    if (occlusionScene.hardwareAccelerated())
    {
        accelerationStructures[0] = occlusionScene.hardwareScene(0);
        accelerationStructures[1] = occlusionScene.hardwareScene(1);

        occlusionSceneAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        occlusionSceneAccelerationStructure.accelerationStructureCount = eyeCount;
        occlusionSceneAccelerationStructure.pAccelerationStructures = accelerationStructures;

        occlusionSceneWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        occlusionSceneWrite.pNext = &occlusionSceneAccelerationStructure;
        occlusionSceneWrite.dstSet = descriptorSet;
        occlusionSceneWrite.dstBinding = 4;
        occlusionSceneWrite.dstArrayElement = 0;
        occlusionSceneWrite.descriptorCount = eyeCount;
        occlusionSceneWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    }
    else
    {
        uniformBufferDescriptorSetWrite(descriptorSet, 4, occlusionScene.softwareScene(), &occlusionSceneBufferInfo, &occlusionSceneWrite);
    }

    vector<VkWriteDescriptorSet> descriptorWrites = {
        cameraWrite,
        objectWrite,
        materialWrite,
        environmentWrite
    };

    // Only write if we have a non-null value, if we have a null value this will be redone before render anyway
    if (
        !occlusionScene.hardwareAccelerated() ||
        accelerationStructures[0]
    )
    {
        descriptorWrites.push_back(occlusionSceneWrite);
    }

    vkUpdateDescriptorSets(
        ctx->device,
        descriptorWrites.size(),
        descriptorWrites.data(),
        0,
        nullptr
    );
}

void RenderEntityComponent::bindShadelessVertex(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

void RenderEntityComponent::bindShadelessTexture(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo materialWriteBuffer{};
    VkWriteDescriptorSet materialWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 2, material->uniform.buffer, &materialWriteBuffer, &materialWrite);

    vector<VkDescriptorImageInfo> imageWriteImages(maxTextureTileCount, VkDescriptorImageInfo{});
    VkWriteDescriptorSet imageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 3, material->textures.front()->imageViews(), ctx->unfilteredSampler, &imageWriteImages, &imageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite,
        materialWrite,
        imageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

void RenderEntityComponent::bindShadelessArea(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo materialWriteBuffer{};
    VkWriteDescriptorSet materialWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 2, material->uniform.buffer, &materialWriteBuffer, &materialWrite);

    VkDescriptorImageInfo noiseWriteImage{};
    VkWriteDescriptorSet noiseWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 3, ctx->areaSurfaceNoiseImageView, ctx->filteredSampler, &noiseWriteImage, &noiseWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite,
        materialWrite,
        noiseWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

void RenderEntityComponent::bindShadelessTerrain(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

void RenderEntityComponent::bindShadelessRope(
    VkDescriptorSet descriptorSet,
    BodyRenderMaterialType type,
    const RenderEntityBodyMaterial* material,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo materialWriteBuffer{};
    VkWriteDescriptorSet materialWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 2, material->uniform.buffer, &materialWriteBuffer, &materialWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite,
        materialWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}
