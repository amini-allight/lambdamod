/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_tools.hpp"
#include "sound_constants.hpp"
#include "resample.hpp"
#include "log.hpp"
#include "tools.hpp"

i16 convertSampleToDiscrete(f32 continuous)
{
    return continuous * 32767;
}

vector<i16> convertSamplesToDiscrete(const vector<f32>& continuous)
{
    vector<i16> discrete(continuous.size(), 0);

    for (size_t i = 0; i < continuous.size(); i++)
    {
        discrete[i] = convertSampleToDiscrete(continuous[i]);
    }

    return discrete;
}

f32 convertSampleToContinuous(i16 discrete)
{
    return discrete / 32768.0f;
}

vector<f32> convertSamplesToContinuous(const vector<i16>& discrete)
{
    vector<f32> continuous(discrete.size(), 0);

    for (size_t i = 0; i < discrete.size(); i++)
    {
        continuous[i] = convertSampleToContinuous(discrete[i]);
    }

    return continuous;
}

vector<f32> reverseSamples(vector<f32> samples)
{
    reverse(samples.begin(), samples.end());

    return samples;
}

static f32 readSample(const vector<f32>& samples, i64 index, bool looping)
{
    if (index < 0)
    {
        if (looping)
        {
            index -= (index / samples.size()) * samples.size();

            return samples[index];
        }
        else
        {
            return 0;
        }
    }
    else if (index >= static_cast<i64>(samples.size()))
    {
        if (looping)
        {
            index -= (index / samples.size()) * samples.size();

            return samples[index];
        }
        else
        {
            return 0;
        }
    }
    else
    {
        return samples[index];
    }
}

static vector<f32> readSampleRange(const vector<f32>& samples, i64 start, i64 end, bool looping)
{
    vector<f32> result(abs(start - end), 0);

    i64 step = sign(end - start);

    i64 writeHead = 0;
    for (i64 readHead = start; readHead != end; readHead += step)
    {
        result[writeHead] = readSample(samples, readHead, looping);
        writeHead++;
    }

    return result;
}

tuple<SoundOverlappedChunk, i64> readSampleSource(const vector<f32>& samples, i64 readHead, f64 multiplier, bool looping)
{
    SoundOverlappedChunk result;

    if (samples.empty() || roughly(multiplier, 0.0))
    {
        return { result, 0 };
    }

    i64 start = readHead - roundAwayFromZero(soundChunkOverlap * multiplier);
    i64 end = start + roundAwayFromZero(result.length() * multiplier);

    vector<f32> acquired = readSampleRange(samples, start, end, looping);

    acquired = hermiteResample(acquired, static_cast<f64>(result.length()) / abs(start - end));

    memcpy(result.samples(), acquired.data(), min(result.length(), acquired.size()) * sizeof(f32));

    return { result, static_cast<i64>(roundAwayFromZero(soundChunkSamplesPerChannel * multiplier)) };
}

#pragma pack(1)
struct RIFFHeader
{
    char headerID[4]{ 'R', 'I', 'F', 'F' };
    i32 totalFileSize;
    char fileType[4]{ 'W', 'A', 'V', 'E' };

    void setSize(i32 size)
    {
        totalFileSize = size - (sizeof(headerID) + sizeof(totalFileSize));
    }
};

struct WAVEFormat
{
    char headerID[4]{ 'f', 'm', 't', ' ' };
    i32 sectionSize = sizeof(WAVEFormat) - (sizeof(headerID) + sizeof(sectionSize));
    i16 audioFormat;
    i16 channelCount;
    i32 sampleRate;
    i32 byteRate;
    i16 blockAlign;
    i16 bitsPerSample;
};

struct WAVEData
{
    char headerID[4]{ 'd', 'a', 't', 'a' };
    i32 sectionSize;

    void setSize(i32 size)
    {
        sectionSize = size - (sizeof(headerID) + sizeof(sectionSize));
    }
};

struct WAVPreamble
{
    RIFFHeader riff;
    WAVEFormat format;
    WAVEData data;
};
#pragma pack()

void writeWAV(const string& path, u32 frequency, u32 channels, const i16* samples, u32 samplesPerChannel)
{
    WAVPreamble preamble;
    preamble.riff.setSize(sizeof(WAVPreamble) + samplesPerChannel * channels * sizeof(i16));
    preamble.format.audioFormat = 1;
    preamble.format.channelCount = channels;
    preamble.format.sampleRate = frequency;
    preamble.format.byteRate = frequency * channels * sizeof(i16);
    preamble.format.blockAlign = sizeof(i16) * channels;
    preamble.format.bitsPerSample = sizeof(i16) * 8;
    preamble.data.setSize(sizeof(WAVEData) + samplesPerChannel * channels * sizeof(i16));

    FILE* file = fopen(path.c_str(), "wb");

    if (!file)
    {
        fatal("Failed to open '" + path + "' for writing WAV file.");
    }

    fwrite(&preamble, sizeof(WAVPreamble), 1, file);

    fwrite(samples, sizeof(i16), samplesPerChannel * channels, file);

    fclose(file);
}
