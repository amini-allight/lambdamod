/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_interface.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_entity.hpp"

RenderInterface::RenderInterface()
{

}

RenderInterface::~RenderInterface()
{

}

void RenderInterface::addLights(RenderEntity* entity)
{
    vector<RenderLight> entityLights = entity->lights();

    lights.insert(lights.end(), entityLights.begin(), entityLights.end());
    entityLightRanges.insert_or_assign(entity, tuple<size_t, size_t>(lights.size() - entityLights.size(), lights.size()));
}

void RenderInterface::removeLights(RenderEntity* entity)
{
    const auto [ start, end ] = entityLightRanges.at(entity);

    lights.erase(lights.begin() + start, lights.begin() + end);
    entityLightRanges.erase(entity);
}

void RenderInterface::updateLights(RenderEntity* entity)
{
    vector<RenderLight> entityLights = entity->lights();

    const auto [ start, end ] = entityLightRanges.at(entity);

    size_t previousSize = end - start;

    if (previousSize == entityLights.size())
    {
        for (size_t i = 0; i < entityLights.size(); i++)
        {
            lights.at(start + i) = entityLights.at(i);
        }
    }
    else if (previousSize < entityLights.size())
    {
        for (size_t i = 0; i < previousSize; i++)
        {
            lights.at(start + i) = entityLights.at(i);
        }

        lights.insert(lights.begin() + end, entityLights.begin() + previousSize, entityLights.end());
        entityLightRanges.insert_or_assign(entity, tuple<size_t, size_t>(start, start + entityLights.size()));
    }
    else
    {
        for (size_t i = 0; i < entityLights.size(); i++)
        {
            lights.at(start + i) = entityLights.at(i);
        }

        lights.erase(lights.begin() + entityLights.size(), lights.begin() + end);
        entityLightRanges.insert_or_assign(entity, tuple<size_t, size_t>(start, start + entityLights.size()));
    }
}

vector<RenderLight> RenderInterface::getLights(const vec3& position) const
{
    vector<RenderLight> lights;

    for (const RenderLight& light : this->lights)
    {
        if (!light.needed(position))
        {
            continue;
        }

        lights.push_back(light);

        if (lights.size() == maxLightCount)
        {
            break;
        }
    }

    return lights;
}
