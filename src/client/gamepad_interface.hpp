/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_types.hpp"

class GamepadInterface
{
public:
    GamepadInterface();
    GamepadInterface(const GamepadInterface& rhs) = delete;
    GamepadInterface(GamepadInterface&& rhs) = delete;
    virtual ~GamepadInterface() = 0;

    GamepadInterface& operator=(const GamepadInterface& rhs) = delete;
    GamepadInterface& operator=(GamepadInterface&& rhs) = delete;

    virtual bool hasGamepad() const = 0;

    virtual void step() = 0;

    virtual void haptic(const HapticEffect& effect) = 0;
};
