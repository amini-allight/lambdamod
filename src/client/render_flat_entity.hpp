/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "render_entity.hpp"
#include "render_deletion_task.hpp"

class FlatRender;

class RenderFlatEntity final : public RenderEntity
{
public:
    RenderFlatEntity(
        RenderInterface* render,

        const vector<FlatSwapchainElement*>& swapchainElements,

        const RenderPipelineStore* pipelineStore,
        const Entity* entity,
        const BodyPartMask& bodyPartMask,
        vector<RenderDeletionTask<RenderFlatEntity>>* entitiesToDelete
    );
    ~RenderFlatEntity();

    void work(
        const FlatSwapchainElement* swapchainElement,
        const vec3& cameraPosition,
        deque<RenderTransparentDraw>* transparentDraws,
        const set<EntityID>& hiddenEntityIDs
    ) const;
    void update(const Entity* entity, const tuple<EntityID, set<BodyPartID>>& bodyPartMask) override;
    void refresh(const vector<FlatSwapchainElement*>& swapchainElements);

    void add(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask) override;
    void remove(EntityID parentID, EntityID id) override;
    void update(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask) override;

    const map<EntityID, RenderFlatEntity*>& children() const;

private:
    vector<FlatSwapchainElement*> swapchainElements;
    vector<RenderDeletionTask<RenderFlatEntity>>* entitiesToDelete;
    vector<RenderEntityComponent*> components;

    map<EntityID, RenderFlatEntity*> _children;

    void createComponents();
    void destroyComponents();
};
