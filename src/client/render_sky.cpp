/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_sky.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "render_constants.hpp"
#include "render_tools.hpp"

RenderSky::RenderSky(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline,
    VkPipelineLayout starsPipelineLayout,
    VkPipeline starsPipeline,
    VkPipelineLayout circlePipelineLayout,
    VkPipeline circlePipeline,
    VkPipelineLayout cloudPipelineLayout,
    VkPipeline cloudPipeline,
    VkPipelineLayout auroraPipelineLayout,
    VkPipeline auroraPipeline,
    VkPipelineLayout cometPipelineLayout,
    VkPipeline cometPipeline,
    VkPipelineLayout vortexPipelineLayout,
    VkPipeline vortexPipeline,
    VkPipelineLayout envMapPipelineLayout,
    VkPipeline envMapPipeline,
    VkPipelineLayout envMapStarsPipelineLayout,
    VkPipeline envMapStarsPipeline,
    VkPipelineLayout envMapCirclePipelineLayout,
    VkPipeline envMapCirclePipeline,
    VkPipelineLayout envMapCloudPipelineLayout,
    VkPipeline envMapCloudPipeline,
    VkPipelineLayout envMapAuroraPipelineLayout,
    VkPipeline envMapAuroraPipeline,
    VkPipelineLayout envMapCometPipelineLayout,
    VkPipeline envMapCometPipeline,
    VkPipelineLayout envMapVortexPipelineLayout,
    VkPipeline envMapVortexPipeline
)
    : ctx(ctx)
    , descriptorSetLayout(descriptorSetLayout)
    , pipelineLayout(pipelineLayout)
    , pipeline(pipeline)
    , starsPipelineLayout(starsPipelineLayout)
    , starsPipeline(starsPipeline)
    , circlePipelineLayout(circlePipelineLayout)
    , circlePipeline(circlePipeline)
    , cloudPipelineLayout(cloudPipelineLayout)
    , cloudPipeline(cloudPipeline)
    , auroraPipelineLayout(auroraPipelineLayout)
    , auroraPipeline(auroraPipeline)
    , cometPipelineLayout(cometPipelineLayout)
    , cometPipeline(cometPipeline)
    , vortexPipelineLayout(vortexPipelineLayout)
    , vortexPipeline(vortexPipeline)
    , envMapPipelineLayout(envMapPipelineLayout)
    , envMapPipeline(envMapPipeline)
    , envMapStarsPipelineLayout(envMapStarsPipelineLayout)
    , envMapStarsPipeline(envMapStarsPipeline)
    , envMapCirclePipelineLayout(envMapCirclePipelineLayout)
    , envMapCirclePipeline(envMapCirclePipeline)
    , envMapCloudPipelineLayout(envMapCloudPipelineLayout)
    , envMapCloudPipeline(envMapCloudPipeline)
    , envMapAuroraPipelineLayout(envMapAuroraPipelineLayout)
    , envMapAuroraPipeline(envMapAuroraPipeline)
    , envMapCometPipelineLayout(envMapCometPipelineLayout)
    , envMapCometPipeline(envMapCometPipeline)
    , envMapVortexPipelineLayout(envMapVortexPipelineLayout)
    , envMapVortexPipeline(envMapVortexPipeline)
{

}

RenderSky::~RenderSky()
{

}

void RenderSky::destroyComponents()
{
    for (u32 i = 0; i < components.size(); i++)
    {
        delete components.at(i);
    }

    components.clear();
}

void RenderSky::fillUniformBuffer(
    RenderSkyComponent* component,
    const SkyParameters& sky,
    const vec3& up
) const
{
    if (component->lastSky && *component->lastSky == sky &&
        component->lastUp && *component->lastUp == up
    )
    {
        return;
    }

    fmat4 worldTransform = mat4(quaternion(pickForwardDirection(up), up)).inverse();

    memset(component->uniformMapping->data, 0, sizeof(SkyBaseGPU));

    SkyBaseGPU skyGPU;
    skyGPU.worldTransform = worldTransform;
    skyGPU.backgroundColor = vec4(baseBackgroundColor, 1);
    skyGPU.type = sky.base.type;

    switch (sky.base.type)
    {
    case Sky_Base_Blank :
        break;
    case Sky_Base_Unidirectional :
    {
        const SkyBaseUnidirectional& unidirectional = sky.base.get<SkyBaseUnidirectional>();

        skyGPU.horizonColor = unidirectional.horizonColor.toVec4();
        skyGPU.lowerZenithColor = unidirectional.zenithColor.toVec4();
        skyGPU.upperZenithColor = unidirectional.zenithColor.toVec4();
        break;
    }
    case Sky_Base_Bidirectional :
    {
        const SkyBaseBidirectional& bidirectional = sky.base.get<SkyBaseBidirectional>();

        skyGPU.horizonColor = bidirectional.horizonColor.toVec4();
        skyGPU.lowerZenithColor = bidirectional.lowerZenithColor.toVec4();
        skyGPU.upperZenithColor = bidirectional.upperZenithColor.toVec4();
        break;
    }
    case Sky_Base_Omnidirectional :
    {
        const SkyBaseOmnidirectional& omnidirectional = sky.base.get<SkyBaseOmnidirectional>();

        skyGPU.horizonColor = omnidirectional.color.toVec4();
        skyGPU.lowerZenithColor = omnidirectional.color.toVec4();
        skyGPU.upperZenithColor = omnidirectional.color.toVec4();
        break;
    }
    }

    memcpy(component->uniformMapping->data, &skyGPU, sizeof(skyGPU));

    if (sky.layers.size() != component->layers.size())
    {
        component->refreshLayers(sky.layers.size());
    }

    mt19937_64 engine(sky.seed);
    uniform_real_distribution<f32> distribution(-1, +1);

    for (size_t i = 0; i < sky.layers.size(); i++)
    {
        const SkyLayer& inLayer = sky.layers.at(i);
        RenderSkyComponentLayer* outLayer = component->layers.at(i);

        outLayer->fillUniformBuffer(up, worldTransform, engine, distribution, inLayer);
    }

    component->lastSky = sky;
    component->lastUp = up;
}

void RenderSky::fillCommandBuffer(
    const MainSwapchainElement* swapchainElement,
    const RenderSkyComponent* component
) const
{
    VkCommandBuffer commandBuffer = swapchainElement->commandBuffer();
    u32 width = swapchainElement->width();
    u32 height = swapchainElement->height();

    VkViewport viewport;

    if (g_vr)
    {
        viewport = {
            0, 0,
            static_cast<f32>(width), static_cast<f32>(height),
            0, 1
        };
    }
    else
    {
        viewport = {
            0, static_cast<f32>(height),
            static_cast<f32>(width), -static_cast<f32>(height),
            0, 1
        };
    }

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &component->descriptorSet,
        0,
        nullptr
    );

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdDraw(commandBuffer, skyboxVertexCount, 1, 0, 0);

    for (const RenderSkyComponentLayer* layer : component->layers)
    {
        VkPipelineLayout layerPipelineLayout;
        VkPipeline layerPipeline;

        switch (layer->type)
        {
        default :
            throw runtime_error("Encountered unknown sky layer type + '" + to_string(layer->type) + "'.");
        case Sky_Layer_Stars :
            layerPipelineLayout = starsPipelineLayout;
            layerPipeline = starsPipeline;
            break;
        case Sky_Layer_Circle :
            layerPipelineLayout = circlePipelineLayout;
            layerPipeline = circlePipeline;
            break;
        case Sky_Layer_Cloud :
            layerPipelineLayout = cloudPipelineLayout;
            layerPipeline = cloudPipeline;
            break;
        case Sky_Layer_Aurora :
            layerPipelineLayout = auroraPipelineLayout;
            layerPipeline = auroraPipeline;
            break;
        case Sky_Layer_Comet :
            layerPipelineLayout = cometPipelineLayout;
            layerPipeline = cometPipeline;
            break;
        case Sky_Layer_Vortex :
            layerPipelineLayout = vortexPipelineLayout;
            layerPipeline = vortexPipeline;
            break;
        case Sky_Layer_Meteor_Shower :
            // TODO: Meteor shower
            continue;
        }

        layer->fillCommandBuffer(commandBuffer, layerPipelineLayout, layerPipeline);

        switch (layer->type)
        {
        default :
            break;
        case Sky_Layer_Aurora :
        {
            VkClearAttachment clearTarget{};
            clearTarget.aspectMask = VK_IMAGE_ASPECT_DEPTH_BIT;
            clearTarget.clearValue = fillClearValue(Clear_Value_Depth);

            VkClearRect clearRect{};
            clearRect.baseArrayLayer = 0;
#ifdef LMOD_VR
            clearRect.layerCount = g_vr ? eyeCount : 1;
#else
            clearRect.layerCount = 1;
#endif
            clearRect.rect = {
                { 0, 0 },
                { width, height }
            };

            vkCmdClearAttachments(
                commandBuffer,
                1,
                &clearTarget,
                1,
                &clearRect
            );
            break;
        }
        }
    }
}

void RenderSky::fillEnvMapCommandBuffer(
    const SkySwapchainElement* swapchainElement,
    const RenderSkyComponent* component
) const
{
    VkCommandBuffer commandBuffer = swapchainElement->parent->commandBuffer();
    u32 width = swapchainElement->width();
    u32 height = swapchainElement->height();

    VkViewport viewport;

    if (g_vr)
    {
        viewport = {
            0, 0,
            static_cast<f32>(width), static_cast<f32>(height),
            0, 1
        };
    }
    else
    {
        viewport = {
            0, static_cast<f32>(height),
            static_cast<f32>(width), -static_cast<f32>(height),
            0, 1
        };
    }

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        envMapPipelineLayout,
        0,
        1,
        &component->envMapDescriptorSet,
        0,
        nullptr
    );

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, envMapPipeline);

    vkCmdDraw(commandBuffer, skyboxVertexCount, 1, 0, 0);

    for (const RenderSkyComponentLayer* layer : component->layers)
    {
        VkPipelineLayout layerPipelineLayout;
        VkPipeline layerPipeline;

        switch (layer->type)
        {
        default :
            throw runtime_error("Encountered unknown sky layer type + '" + to_string(layer->type) + "'.");
        case Sky_Layer_Stars :
            layerPipelineLayout = envMapStarsPipelineLayout;
            layerPipeline = envMapStarsPipeline;
            break;
        case Sky_Layer_Circle :
            layerPipelineLayout = envMapCirclePipelineLayout;
            layerPipeline = envMapCirclePipeline;
            break;
        case Sky_Layer_Cloud :
            layerPipelineLayout = envMapCloudPipelineLayout;
            layerPipeline = envMapCloudPipeline;
            break;
        case Sky_Layer_Aurora :
            layerPipelineLayout = envMapAuroraPipelineLayout;
            layerPipeline = envMapAuroraPipeline;
            break;
        case Sky_Layer_Comet :
            layerPipelineLayout = envMapCometPipelineLayout;
            layerPipeline = envMapCometPipeline;
            break;
        case Sky_Layer_Vortex :
            layerPipelineLayout = envMapVortexPipelineLayout;
            layerPipeline = envMapVortexPipeline;
            break;
        case Sky_Layer_Meteor_Shower :
            // TODO: Meteor shower
            continue;
        }

        layer->fillEnvMapCommandBuffer(commandBuffer, layerPipelineLayout, layerPipeline);

        // Don't clear depth because sky doesn't have a depth buffer, just accept the errors
    }
}
