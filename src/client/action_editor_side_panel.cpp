/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_editor_side_panel.hpp"
#include "localization.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "theme.hpp"
#include "action_editor.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "dynamic_label.hpp"
#include "uint_edit.hpp"
#include "ufloat_edit.hpp"
#include "vec3_edit.hpp"
#include "rotation_edit.hpp"
#include "option_select.hpp"
#include "units.hpp"

ActionEditorSidePanel::ActionEditorSidePanel(InterfaceWidget* parent)
    : SidePanel(parent)
{
    listView = new ListView(
        this,
        [](size_t index) -> void {},
        theme.backgroundColor
    );
}

void ActionEditorSidePanel::step()
{
    InterfaceWidget::step();

    if (editor()->activeParts.size() != 1 && id != ActionPartID())
    {
        id = ActionPartID();
        clearChildren();
        update();
    }
    else if (editor()->activeParts.size() == 1 && id != *editor()->activeParts.begin())
    {
        id = *editor()->activeParts.begin();
        clearChildren();
        addCommonFields();
        
        switch (editor()->currentPart().type())
        {
        case Action_Part_IK :
            addIKFields();
            break;
        }

        update();
    }
}

void ActionEditorSidePanel::addCommonFields()
{
    new EditorEntry(listView, "action-editor-side-panel-id", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&ActionEditorSidePanel::idSource, this)
        );
    });

    new EditorEntry(listView, "action-editor-side-panel-end", timeSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&ActionEditorSidePanel::endSource, this),
            bind(&ActionEditorSidePanel::onEnd, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "action-editor-side-panel-target-id", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&ActionEditorSidePanel::targetIDSource, this),
            bind(&ActionEditorSidePanel::onTargetID, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "action-editor-side-panel-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&ActionEditorSidePanel::typeOptionsSource, this),
            bind(&ActionEditorSidePanel::typeOptionTooltipsSource, this),
            bind(&ActionEditorSidePanel::typeSource, this),
            bind(&ActionEditorSidePanel::onType, this, placeholders::_1)
        );
    });
}

void ActionEditorSidePanel::addIKFields()
{
    new EditorEntry(listView, "action-editor-side-panel-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&ActionEditorSidePanel::positionSource, this),
            bind(&ActionEditorSidePanel::onPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "action-editor-side-panel-rotation", [this](InterfaceWidget* parent) -> void {
        new RotationEdit(
            parent,
            bind(&ActionEditorSidePanel::rotationSource, this),
            bind(&ActionEditorSidePanel::onRotation, this, placeholders::_1)
        );
    });
}

void ActionEditorSidePanel::clearChildren()
{
    listView->clearChildren();
}

string ActionEditorSidePanel::idSource() const
{
    return to_string(editor()->currentPart().id().value());
}

f64 ActionEditorSidePanel::endSource() const
{
    return editor()->currentPart().end() / 1000.0;
}

void ActionEditorSidePanel::onEnd(f64 end)
{
    if (editor()->activeParts.size() != 1)
    {
        return;
    }

    editor()->edit([this, end](Action& action) -> void {
        auto it = action.parts().find(*editor()->activeParts.begin());

        if (it == action.parts().end())
        {
            return;
        }

        ActionPart part = it->second;

        part.setEnd(end * 1000);

        action.add(part);
    });
}

u64 ActionEditorSidePanel::targetIDSource() const
{
    return editor()->currentPart().targetID().value();
}

void ActionEditorSidePanel::onTargetID(u64 targetID)
{
    if (editor()->activeParts.size() != 1)
    {
        return;
    }

    editor()->edit([this, targetID](Action& action) -> void {
        auto it = action.parts().find(*editor()->activeParts.begin());

        if (it == action.parts().end())
        {
            return;
        }

        ActionPart part = it->second;

        part.setTargetID(BodyPartID(targetID));

        action.add(part);
    });
}

vector<string> ActionEditorSidePanel::typeOptionsSource()
{
    // NOTE: Update this when adding new action part types
    return {
        localize("action-editor-side-panel-ik")
    };
}

vector<string> ActionEditorSidePanel::typeOptionTooltipsSource()
{
    // NOTE: Update this when adding new action part types
    return {
        localize("action-editor-side-panel-ik-tooltip")
    };
}

size_t ActionEditorSidePanel::typeSource()
{
    return static_cast<size_t>(editor()->currentPart().type());
}

void ActionEditorSidePanel::onType(size_t index)
{
    editor()->edit([this, index](Action& action) -> void {
        auto it = action.parts().find(id);

        if (it == action.parts().end())
        {
            return;
        }

        ActionPart part = it->second;
        part.setType(static_cast<ActionPartType>(index));

        action.add(part);
    });
}

vec3 ActionEditorSidePanel::positionSource() const
{
    return localizeLength(editor()->currentPart().get<ActionPartIK>().transform.position());
}

void ActionEditorSidePanel::onPosition(const vec3& position)
{
    if (editor()->activeParts.size() != 1)
    {
        return;
    }

    editor()->edit([this, position](Action& action) -> void {
        auto it = action.parts().find(*editor()->activeParts.begin());

        if (it == action.parts().end())
        {
            return;
        }

        ActionPart part = it->second;

        part.get<ActionPartIK>().transform.setPosition(delocalizeLength(position));

        action.add(part);
    });
}

quaternion ActionEditorSidePanel::rotationSource() const
{
    return editor()->currentPart().get<ActionPartIK>().transform.rotation();
}

void ActionEditorSidePanel::onRotation(const quaternion& rotation)
{
    if (editor()->activeParts.size() != 1)
    {
        return;
    }

    editor()->edit([this, rotation](Action& action) -> void {
        auto it = action.parts().find(*editor()->activeParts.begin());

        if (it == action.parts().end())
        {
            return;
        }

        ActionPart part = it->second;

        part.get<ActionPartIK>().transform.setRotation(rotation);

        action.add(part);
    });
}

ActionEditor* ActionEditorSidePanel::editor() const
{
    return dynamic_cast<ActionEditor*>(parent());
}
