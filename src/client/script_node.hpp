/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script_nodes.hpp"

class ScriptContext;

class ScriptNode
{
public:
    ScriptNode(const ScriptContext* ctx, ScriptNodeID* id, const ivec2& position, const Value& value, bool quote);

    static ScriptNode makeOutput(const ScriptContext* ctx, ScriptNodeID* id, const ivec2& position, const optional<Value>& value);

    ScriptNodeID id() const;

    bool operator==(const ScriptNode& rhs) const;
    bool operator!=(const ScriptNode& rhs) const;
    bool operator>(const ScriptNode& rhs) const;
    bool operator<(const ScriptNode& rhs) const;
    bool operator>=(const ScriptNode& rhs) const;
    bool operator<=(const ScriptNode& rhs) const;
    strong_ordering operator<=>(const ScriptNode& rhs) const;

    Value value() const;
    string code() const;

    vector<string> inputs(const ScriptContext* ctx) const;
    vector<string> outputs() const;

    void setPosition(const ivec2& position);
    const ivec2& position() const;
    ivec2 size(const ScriptContext* ctx) const;
    ivec2 screenPosition(const ivec2& topLeft, const ivec2& viewportCenter, const ivec2& viewportSize) const;
    ScriptNodeType type() const;
    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }
    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }
    vector<shared_ptr<ScriptNode>> children() const;
    set<shared_ptr<ScriptNode>> allNodes(const shared_ptr<ScriptNode>& self) const;
    vector<shared_ptr<ScriptNode>> inputNodes() const;
    shared_ptr<ScriptNode> copyWithChildren() const;
    shared_ptr<ScriptNode> copyWithoutChildren() const;
    void traverse(
        const shared_ptr<ScriptNode>& self,
        const function<void(shared_ptr<ScriptNode>, size_t, shared_ptr<ScriptNode>)>& behavior
    ) const;

    void setInputNode(size_t index, const shared_ptr<ScriptNode>& node);
    void clearInputNode(const ScriptContext* ctx, size_t index);
    shared_ptr<ScriptNode> getInputNode(size_t index) const;
    void remove(ScriptNodeID id);

private:
    friend class ScriptNodeFormat;

    void setID(ScriptNodeID id);

private:
    explicit ScriptNode(ScriptNodeID* id, const ivec2& position);

    ScriptNodeID _id;
    ivec2 _position;
    ScriptNodeType _type;
    ScriptNodeData data;

    void compactInputs(const ScriptContext* ctx);
};
