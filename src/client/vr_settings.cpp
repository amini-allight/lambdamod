/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "vr_settings.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "option_select.hpp"
#include "checkbox.hpp"
#include "ufloat_edit.hpp"
#include "vec2_edit.hpp"
#include "vec2_percent_edit.hpp"

VRSettings::VRSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "vr-settings-motion-mode", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&VRSettings::vrMotionModeOptionsSource, this),
            bind(&VRSettings::vrMotionModeOptionTooltipsSource, this),
            bind(&VRSettings::vrMotionModeSource, this),
            bind(&VRSettings::onVRMotionMode, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-smooth-turning", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&VRSettings::vrSmoothTurningSource, this),
            bind(&VRSettings::onVRSmoothTurning, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-smooth-turn-speed", rotationalSpeedSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&VRSettings::vrSmoothTurnSpeedSource, this),
            bind(&VRSettings::onVRSmoothTurnSpeed, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-snap-turn-increment", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&VRSettings::vrSnapTurnIncrementSource, this),
            bind(&VRSettings::onVRSnapTurnIncrement, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-keyboard-right-hand", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&VRSettings::vrKeyboardRightHandSource, this),
            bind(&VRSettings::onVRKeyboardRightHand, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-roll-cage", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&VRSettings::vrRollCageSource, this),
            bind(&VRSettings::onVRRollCage, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-left-dead-zone", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec2PercentEdit(
            parent,
            bind(&VRSettings::vrLeftDeadZoneSource, this),
            bind(&VRSettings::onVRLeftDeadZone, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "vr-settings-right-dead-zone", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec2PercentEdit(
            parent,
            bind(&VRSettings::vrRightDeadZoneSource, this),
            bind(&VRSettings::onVRRightDeadZone, this, placeholders::_1)
        );
    });
}

vector<string> VRSettings::vrMotionModeOptionsSource()
{
    return {
        localize("vr-settings-head"),
        localize("vr-settings-hand"),
        localize("vr-settings-hip")
    };
}

vector<string> VRSettings::vrMotionModeOptionTooltipsSource()
{
    return {
        localize("vr-settings-head-tooltip"),
        localize("vr-settings-hand-tooltip"),
        localize("vr-settings-hip-tooltip")
    };
}

size_t VRSettings::vrMotionModeSource()
{
    return static_cast<size_t>(g_config.vrMotionMode);
}

void VRSettings::onVRMotionMode(size_t index)
{
    g_config.vrMotionMode = static_cast<VRMotionMode>(index);
}

bool VRSettings::vrSmoothTurningSource()
{
    return g_config.vrSmoothTurning;
}

void VRSettings::onVRSmoothTurning(bool state)
{
    g_config.vrSmoothTurning = state;

    saveConfig(g_config);
}

f64 VRSettings::vrSmoothTurnSpeedSource()
{
    return localizeAngle(g_config.vrSmoothTurnSpeed);
}

void VRSettings::onVRSmoothTurnSpeed(f64 speed)
{
    g_config.vrSmoothTurnSpeed = delocalizeAngle(speed);

    saveConfig(g_config);
}

f64 VRSettings::vrSnapTurnIncrementSource()
{
    return localizeAngle(g_config.vrSnapTurnIncrement);
}

void VRSettings::onVRSnapTurnIncrement(f64 increment)
{
    g_config.vrSnapTurnIncrement = delocalizeAngle(increment);

    saveConfig(g_config);
}

bool VRSettings::vrKeyboardRightHandSource()
{
    return g_config.vrKeyboardRightHand;
}

void VRSettings::onVRKeyboardRightHand(bool state)
{
    g_config.vrKeyboardRightHand = state;

    saveConfig(g_config);
}

bool VRSettings::vrRollCageSource()
{
    return g_config.vrRollCage;
}

void VRSettings::onVRRollCage(bool state)
{
    g_config.vrRollCage = state;

    saveConfig(g_config);
}

vec2 VRSettings::vrLeftDeadZoneSource()
{
    return g_config.vrLeftDeadZone;
}

void VRSettings::onVRLeftDeadZone(const vec2& zone)
{
    g_config.vrLeftDeadZone = zone;

    saveConfig(g_config);
}

vec2 VRSettings::vrRightDeadZoneSource()
{
    return g_config.vrRightDeadZone;
}

void VRSettings::onVRRightDeadZone(const vec2& zone)
{
    g_config.vrRightDeadZone = zone;

    saveConfig(g_config);
}

SizeProperties VRSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
