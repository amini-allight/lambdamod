/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "quaternion_edit.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "row.hpp"
#include "float_edit.hpp"

QuaternionEdit::QuaternionEdit(
    InterfaceWidget* parent,
    const function<quaternion()>& source,
    const function<void(const quaternion&)>& onSet
)
    : InterfaceWidget(parent)
{
    auto row = new Row(this);

    w = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().w;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            quaternion q = source();

            q.w = value;

            onSet(q);
        }
    );
    w->setPlaceholder(localize("quaternion-edit-w"));

    x = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().x;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            try
            {
                quaternion q = source();

                q.x = value;

                onSet(q);
            }
            catch (const exception& e)
            {

            }
        }
    );
    x->setPlaceholder(localize("quaternion-edit-x"));

    y = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().y;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            quaternion q = source();

            q.y = value;

            onSet(q);
        }
    );
    y->setPlaceholder(localize("quaternion-edit-y"));

    z = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().z;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            quaternion q = source();

            q.z = value;

            onSet(q);
        }
    );
    z->setPlaceholder(localize("quaternion-edit-z"));
}

void QuaternionEdit::set(const quaternion& value)
{
    w->set(value.w);
    x->set(value.x);
    y->set(value.y);
    z->set(value.z);
}

quaternion QuaternionEdit::value() const
{
    try
    {
        return quaternion(
            w->value(),
            x->value(),
            y->value(),
            z->value()
        );
    }
    catch (const exception& e)
    {
        return quaternion();
    }
}

void QuaternionEdit::clear()
{
    w->clear();
    x->clear();
    y->clear();
    z->clear();
}

SizeProperties QuaternionEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
