/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "edit_tool.hpp"
#include "gizmo_intersector.hpp"

enum SpecialEditMode : u8
{
    Special_Edit_None,
    Special_Edit_Default,
    Special_Edit_Alt
};

class Entity;
class FlatRender;

class EditModeContext
{
public:
    EditModeContext(ViewerModeContext* ctx);
    virtual ~EditModeContext();

    virtual void step();

    virtual void enter();
    virtual void exit();

    bool toolActive() const;
    string toolName() const;
    string toolAxisName() const;
    string toolPivotName() const;
    string extent() const;
    virtual void cancelTool();
    virtual void endTool();
    void useTool(const Point& point);
    const EditTool* tool() const;

    void addToolNumericInput(char c);
    void removeToolNumericInput();
    void toggleToolNumericInputSign();
    void ensureToolNumericInputPeriod();

    bool snap() const;
    void startSnap();
    void endSnap();

    void togglePivotMode(const Point& point);

    void toggleX();
    void toggleY();
    void toggleZ();

    void addNewPrefab();
    virtual void addNewPrefab(const string& name) = 0;
    virtual void addNew() = 0;
    virtual void removeSelected() = 0;

    virtual void addChildren(const Point& point) = 0;
    virtual void removeParent() = 0;

    virtual void hideSelected() = 0;
    virtual void hideUnselected() = 0;
    virtual void unhideAll() = 0;

    virtual vec3 selectionCenter() const = 0;
    void startSelect(const Point& point);
    void cancelSelect();
    void endSelect(const Point& point, SelectMode mode);
    void moveSelect(const Point& point);
    virtual void selectAll() = 0;
    virtual void deselectAll() = 0;
    virtual void invertSelection() = 0;
    bool selecting() const;
    virtual bool hasSelection() const = 0;
    const Point& selectStart() const;
    const Point& selectEnd() const;

    void startSpecialEdit(const Point& point);
    void startAltSpecialEdit(const Point& point);
    virtual void endSpecialEdit(const Point& point);
    void moveSpecialEdit(const Point& point);
    bool specialEditing() const;
    SpecialEditMode specialEditMode() const;
    virtual bool canvasPainting() const;

    virtual void translate(const Point& point, bool soft = false);
    virtual void rotate(const Point& point, bool soft = false);
    virtual void scale(const Point& point, bool soft = false);
    virtual void resetPosition() = 0;
    virtual void resetRotation() = 0;
    virtual void resetScale() = 0;
    virtual void resetLinearVelocity() = 0;
    virtual void resetAngularVelocity() = 0;

    virtual void selectionToCursor() = 0;

    virtual void duplicate() = 0;
    virtual void cut() = 0;
    virtual void copy() = 0;
    virtual void paste() = 0;

    virtual void extrude() = 0;
    virtual void split() = 0;
    virtual void join(const Point& point) = 0;

protected:
    ViewerModeContext* ctx;
    bool _selecting;
    Point _selectStart;
    Point _selectEnd;
    SpecialEditMode _specialEditMode;
    EditTool* _tool;
    bool firstTransform;
    RenderDecorationID gizmoID;
    GizmoIntersector gizmoIntersector;

    virtual void specialEdit(const Point& point);

    virtual bool gizmoShown() const = 0;
    virtual bool gizmoSelectable(const Point& point) const = 0;
    void updateGizmo();
    void hideGizmo();
    bool selectGizmo(const Point& point);

    virtual void makeSelections(const Point& point, SelectMode mode) = 0;
    virtual void makeSelections(const Point& start, const Point& end, SelectMode mode) = 0;

    virtual void transformSet(size_t i, const vec3& pivot, const mat4& initial, const mat4& transform) = 0;
    virtual mat4 transformGet(size_t i) const = 0;
    /* Returns the same as transformGet, except for subparts where it returns the part's transform instead
     * For use when an incompatible transform combination has been selected (e.g. subpart + cursor pivot)
     * and a fallback transform as though the part was selected must be performed
     */
    virtual mat4 transformGetWithPromotion(size_t i) const;
    virtual size_t transformCount() const = 0;
    virtual mat4 transformToolSpace() const = 0;

    FlatRender* render() const;
};
