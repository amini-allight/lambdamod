/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

class SoundEffect
{
public:
    SoundEffect(u8* data, size_t size, u32 channels);
    SoundEffect(const SoundEffect& rhs) = delete;
    SoundEffect(SoundEffect&& rhs) = delete;
    ~SoundEffect();

    SoundEffect& operator=(const SoundEffect& rhs) = delete;
    SoundEffect& operator=(SoundEffect&& rhs) = delete;

    const vector<vector<f32>>& channels() const;
    size_t length() const;

private:
    vector<vector<f32>> _channels;
};

SoundEffect* loadWAV(const string& path);
