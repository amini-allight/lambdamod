/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_software_occlusion_scene.hpp"
#include "render_tools.hpp"
#include "render_flat_entity.hpp"
#ifdef LMOD_VR
#include "render_vr_entity.hpp"
#endif

RenderSoftwareOcclusionScene::RenderSoftwareOcclusionScene(const RenderContext* ctx)
    : ctx(ctx)
    , bufferMapping(nullptr)
{
    createPrimitiveBuffer();
}

RenderSoftwareOcclusionScene::~RenderSoftwareOcclusionScene()
{
    destroyPrimitiveBuffer();
}

void RenderSoftwareOcclusionScene::update(const vec3& cameraPosition, const map<EntityID, RenderFlatEntity*>& entities)
{
    vector<OcclusionPrimitiveGPU> primitives;
    primitives.reserve(maxPrimitiveCount);

    recordPrimitives(cameraPosition, entities, primitives);

    populatePrimitiveBuffer(primitives);
}

#ifdef LMOD_VR
void RenderSoftwareOcclusionScene::update(vec3 cameraPosition[eyeCount], const map<EntityID, RenderVREntity*>& entities)
{
    vector<OcclusionPrimitiveGPU> primitives;
    primitives.reserve(maxPrimitiveCount);

    recordPrimitives(cameraPosition, entities, primitives);

    populatePrimitiveBuffer(primitives);
}
#endif

RenderOcclusionSceneReference RenderSoftwareOcclusionScene::get() const
{
    return RenderOcclusionSceneReference(buffer.buffer);
}

void RenderSoftwareOcclusionScene::recordPrimitives(
    const vec3& cameraPosition,
    const map<EntityID, RenderFlatEntity*>& entities,
    vector<OcclusionPrimitiveGPU>& primitives
)
{
    for (const auto& [ id, entity ] : entities)
    {
        for (const RenderOcclusionPrimitive& primitive : entity->occlusionPrimitives())
        {
            primitives.push_back(primitive.uniformData(cameraPosition));
        }

        recordPrimitives(cameraPosition, entity->children(), primitives);
    }
}

#ifdef LMOD_VR
void RenderSoftwareOcclusionScene::recordPrimitives(
    vec3 cameraPosition[eyeCount],
    const map<EntityID, RenderVREntity*>& entities,
    vector<OcclusionPrimitiveGPU>& primitives
)
{
    for (const auto& [ id, entity ] : entities)
    {
        for (const RenderOcclusionPrimitive& primitive : entity->occlusionPrimitives())
        {
            primitives.push_back(primitive.uniformData(cameraPosition));
        }

        recordPrimitives(cameraPosition, entity->children(), primitives);
    }
}
#endif

void RenderSoftwareOcclusionScene::createPrimitiveBuffer()
{
    buffer = createBuffer(
        ctx->allocator,
        maxPrimitiveCount * sizeof(OcclusionPrimitiveGPU) + sizeof(i32),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
    bufferMapping = new VmaMapping<OcclusionSceneGPU>(ctx->allocator, buffer);
}

void RenderSoftwareOcclusionScene::destroyPrimitiveBuffer()
{
    delete bufferMapping;
    destroyBuffer(ctx->allocator, buffer);
}

void RenderSoftwareOcclusionScene::populatePrimitiveBuffer(const vector<OcclusionPrimitiveGPU>& primitives)
{
    OcclusionSceneGPU scene{};
    scene.primitiveCount = min<size_t>(primitives.size(), maxPrimitiveCount);
    memcpy(scene.primitives, primitives.data(), scene.primitiveCount * sizeof(OcclusionPrimitiveGPU));

    bufferMapping->data[0] = scene;
}
