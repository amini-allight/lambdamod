/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_entity_body_material.hpp"
#include "render_tools.hpp"

RenderEntityBodyMaterial::RenderEntityBodyMaterial(
    const RenderContext* ctx,
    const RenderPipelineStore* pipelineStore,
    BodyRenderMaterialType type,
    const BodyRenderMaterialData& material
)
    : type(type)
    , pipelines(pipelineStore->get(type))
    , ctx(ctx)
{

    if (!material.indices.empty())
    {
        size_t indicesSize = material.indices.size() * sizeof(u32);
        indices = createBuffer(
            ctx->allocator,
            indicesSize,
            VK_BUFFER_USAGE_INDEX_BUFFER_BIT,
            VMA_MEMORY_USAGE_CPU_TO_GPU
        );
        indicesMapping = new VmaMapping<u32>(ctx->allocator, indices);
        indexCount = material.indices.size();
        memcpy(indicesMapping->data, material.indices.data(), indicesSize);
    }
    else
    {
        indices.buffer = nullptr;
        indices.allocation = nullptr;
        indexCount = 0;
    }

    if (!material.vertexData.empty())
    {
        size_t verticesSize = material.vertexData.size() * sizeof(f32);
        vertices = createBuffer(
            ctx->allocator,
            verticesSize,
            VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
            VMA_MEMORY_USAGE_CPU_TO_GPU
        );
        verticesMapping = new VmaMapping<f32>(ctx->allocator, vertices);
        vertexCount = material.vertexCount;
        memcpy(verticesMapping->data, material.vertexData.data(), verticesSize);
    }
    else
    {
        vertices.buffer = nullptr;
        vertices.allocation = nullptr;
        vertexCount = 0;
    }

    if (!material.uniformData.empty())
    {
        uniformSize = material.uniformData.size() * sizeof(f32);
        uniform = createBuffer(
            ctx->allocator,
            uniformSize,
            VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
            VMA_MEMORY_USAGE_CPU_TO_GPU
        );
        uniformMapping = new VmaMapping<f32>(ctx->allocator, uniform);
        memcpy(uniformMapping->data, material.uniformData.data(), uniformSize);
    }
    else
    {
        uniform.buffer = nullptr;
        uniform.allocation = nullptr;
    }

    for (const BodyRenderMaterialTextureData& texture : material.textures)
    {
        textures.push_back(new RenderEntityBodyTexture(ctx, texture));
    }

    position = material.position;
}

RenderEntityBodyMaterial::~RenderEntityBodyMaterial()
{
    for (RenderEntityBodyTexture* texture : textures)
    {
        delete texture;
    }

    if (uniform.buffer != nullptr)
    {
        delete uniformMapping;
        destroyBuffer(ctx->allocator, uniform);
    }

    if (vertices.buffer != nullptr)
    {
        delete verticesMapping;
        destroyBuffer(ctx->allocator, vertices);
    }

    if (indices.buffer != nullptr)
    {
        delete indicesMapping;
        destroyBuffer(ctx->allocator, indices);
    }
}

bool RenderEntityBodyMaterial::supports(const BodyRenderMaterialData& material) const
{
    if (material.indices.size() != indexCount)
    {
        return false;
    }

    if (material.vertexCount != vertexCount)
    {
        return false;
    }

    if (material.uniformData.size() * sizeof(f32) != uniformSize)
    {
        return false;
    }

    if (material.textures.size() != textures.size())
    {
        return false;
    }

    for (size_t i = 0; i < material.textures.size(); i++)
    {
        if (!textures.at(i)->supports(material.textures.at(i)))
        {
            return false;
        }
    }

    return true;
}

void RenderEntityBodyMaterial::fill(const BodyRenderMaterialData& material)
{
    if (indices.buffer != nullptr)
    {
        size_t indicesSize = material.indices.size() * sizeof(u32);
        memcpy(indicesMapping->data, material.indices.data(), indicesSize);
    }

    if (vertices.buffer != nullptr)
    {
        size_t verticesSize = material.vertexData.size() * sizeof(f32);
        memcpy(verticesMapping->data, material.vertexData.data(), verticesSize);
    }

    if (uniform.buffer != nullptr)
    {
        size_t uniformSize = material.uniformData.size() * sizeof(f32);
        memcpy(uniformMapping->data, material.uniformData.data(), uniformSize);
    }

    for (size_t i = 0; i < material.textures.size(); i++)
    {
        textures.at(i)->fill(material.textures.at(i));
    }
}

void RenderEntityBodyMaterial::setupTextureLayouts(VkCommandBuffer commandBuffer)
{
    for (RenderEntityBodyTexture* texture : textures)
    {
        texture->setupLayout(commandBuffer);
    }
}

bool RenderEntityBodyMaterial::shouldDraw() const
{
    return indexCount != 0;
}
