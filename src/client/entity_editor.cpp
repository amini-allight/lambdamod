/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "entity_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "column.hpp"
#include "tab_view.hpp"
#include "color_indicator_bar.hpp"
#include "control_context.hpp"
#include "entity_logic_editor.hpp"

// Fix for Windows namespace pollution
#undef interface

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(
        UIScale::tabSize().w * 8,
        UIScale::tabSize().h + UIScale::dividerWidth() + (10 * (UIScale::labelHeight() + UIScale::marginSize() * 2))
    );
}

static string entityWindowTitle(InterfaceView* view, EntityID id)
{
    Entity* entity = view->interface()->context()->controller()->game().get(id);

    if (entity)
    {
        return entity->name();
    }
    else
    {
        return "[" + localize("entity-editor-entity-not-found") + "]";
    }
}

EntityEditor::EntityEditor(InterfaceView* view, EntityID id)
    : InterfaceWindow(view, entityWindowTitle(view, id))
    , _id(id)
{
    START_WIDGET_SCOPE("entity-editor")
        WIDGET_SLOT("close-editor", dismiss)
        WIDGET_SLOT("force-close-editor", forceDismiss)
    END_SCOPE

    auto column = new Column(this);

    tabs = new TabView(
        column,
        {
            localize("entity-editor-general"),
            localize("entity-editor-bindings"),
            localize("entity-editor-hud"),
            localize("entity-editor-logic"),
            localize("entity-editor-body"),
            localize("entity-editor-animation"),
            localize("entity-editor-sound")
        }
    );

    general = new GeneralEditor(
        tabs->tab(localize("entity-editor-general")),
        id
    );
    bindings = new BindingsEditor(
        tabs->tab(localize("entity-editor-bindings")),
        id
    );
    hud = new HUDEditor(
        tabs->tab(localize("entity-editor-hud")),
        id
    );
    logic = new EntityLogicEditor(
        tabs->tab(localize("entity-editor-logic")),
        id
    );
    body = new BodyEditor(
        tabs->tab(localize("entity-editor-body")),
        id
    );
    animation = new AnimationEditor(
        tabs->tab(localize("entity-editor-animation")),
        id
    );
    sound = new SoundEditor(
        tabs->tab(localize("entity-editor-sound")),
        id
    );

    new ColorIndicatorBar(
        column,
        [this]() -> Color {
            const Entity* entity = this->entity();

            if (!entity)
            {
                return theme.positiveColor;
            }

            return entity->active()
                ? theme.negativeColor
                : theme.positiveColor;
        },
        [this]() -> string {
            const Entity* entity = this->entity();

            if (!entity)
            {
                return localize("entity-editor-inactive");
            }

            return entity->active()
                ? localize("entity-editor-active")
                : localize("entity-editor-inactive");
        }
    );

    resize(windowSize());
}

EntityEditor::~EntityEditor()
{
    closeAll();
}

void EntityEditor::step()
{
    if (!entity())
    {
        return;
    }

    InterfaceWindow::step();

    setName(entityWindowTitle(view(), _id));
}

EntityID EntityEditor::id() const
{
    return _id;
}

Entity* EntityEditor::entity() const
{
    return context()->controller()->game().get(_id);
}

bool EntityEditor::inBodyMode() const
{
    return tabs->activeIndex() == 4;
}

bool EntityEditor::inActionMode() const
{
    return tabs->activeIndex() == 5;
}

void EntityEditor::addActionFrame(BodyPartID id)
{
    animation->addActionFrame(id);
}

void EntityEditor::removeActionFrame(BodyPartID id)
{
    animation->removeActionFrame(id);
}

mat4 EntityEditor::getActionTransform(BodyPartID id) const
{
    return animation->getTransform(id);
}

void EntityEditor::setActionTransform(BodyPartID id, const mat4& transform)
{
    animation->setTransform(id, transform);
}

void EntityEditor::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());
}

void EntityEditor::dismiss(const Input& input)
{
    if (logic->saved())
    {
        destroy();
    }
}

void EntityEditor::forceDismiss(const Input& input)
{
    destroy();
}

void EntityEditor::closeAll()
{
    general->close();
    bindings->close();
    hud->close();
    logic->close();
    body->close();
    animation->close();
    sound->close();
}
