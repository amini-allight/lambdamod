/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_post_process_effect_component.hpp"
#include "render_tools.hpp"

RenderPostProcessEffectComponent::RenderPostProcessEffectComponent(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer uniform
)
    : uniform(uniform)
    , ctx(ctx)
{
    uniformMapping = new VmaMapping<f32>(ctx->allocator, uniform);
    descriptorSet = createDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSetLayout);
}

RenderPostProcessEffectComponent::~RenderPostProcessEffectComponent()
{
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSet);
    delete uniformMapping;
    destroyBuffer(ctx->allocator, uniform);
}
