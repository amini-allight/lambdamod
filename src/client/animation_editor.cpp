/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "animation_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "viewer_mode_context.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"

#include "confirm_dialog.hpp"

AnimationEditor::AnimationEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
{
    START_WIDGET_SCOPE("animation-editor")
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
    END_SCOPE

    auto column = new Column(this);

    auto row = new Row(column);

    actionList = new ItemList(
        row,
        bind(&AnimationEditor::itemSource, this),
        bind(&AnimationEditor::onAdd, this, placeholders::_1),
        bind(&AnimationEditor::onRemove, this, placeholders::_1),
        bind(&AnimationEditor::onSelect, this, placeholders::_1),
        bind(&AnimationEditor::onRename, this, placeholders::_1, placeholders::_2),
        bind(&AnimationEditor::onClone, this, placeholders::_1, placeholders::_2)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    editor = new ActionEditor(row, id);
}

void AnimationEditor::addActionFrame(BodyPartID id)
{
    editor->addActionPart(id);
}

void AnimationEditor::removeActionFrame(BodyPartID id)
{
    editor->removeActionPart(id);
}

mat4 AnimationEditor::getTransform(BodyPartID id) const
{
    return editor->getTransform(id);
}

void AnimationEditor::setTransform(BodyPartID id, const mat4& transform)
{
    editor->setTransform(id, transform);
}

vector<string> AnimationEditor::itemSource()
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return {};
    }

    vector<string> names;
    names.reserve(entity->actions().size());

    for (const auto& [ name, action ] : entity->actions())
    {
        names.push_back(name);
    }

    return names;
}

void AnimationEditor::onAdd(const string& name)
{
    context()->controller()->edit([this, name]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addAction(name, Action());
    });
}

void AnimationEditor::onRemove(const string& name)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("animation-editor-are-you-sure-you-want-to-delete-action", name),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            context()->controller()->edit([this, name]() -> void {
                Entity* entity = context()->controller()->game().get(id);

                if (!entity)
                {
                    return;
                }

                entity->removeAction(name);
            });
        }
    );
}

void AnimationEditor::onSelect(const string& name)
{
    editor->open(name);
}

void AnimationEditor::onRename(const string& oldName, const string& newName)
{
    context()->controller()->edit([this, oldName, newName]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addAction(newName, entity->actions().at(oldName));
        entity->removeAction(oldName);
    });
}

void AnimationEditor::onClone(const string& oldName, const string& newName)
{
    context()->controller()->edit([this, oldName, newName]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addAction(newName, entity->actions().at(oldName));
    });
}

void AnimationEditor::undo(const Input& input)
{
    context()->viewerMode()->undo();
}

void AnimationEditor::redo(const Input& input)
{
    context()->viewerMode()->redo();
}

SizeProperties AnimationEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
