/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_window.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_window_view.hpp"
#include "control_context.hpp"

InterfaceWindow::InterfaceWindow(InterfaceView* view, const string& name)
    : InterfaceWidget(view)
    , name(name)
    , _shown(false)
    , _destroy(false)
    , _transform(Window_Transform_None)
    , popupCount(0)
{
    START_WIDGET_BLOCKING_SCOPE("window")
        START_SCOPE("window-header", isTargetingHeader())
            START_SCOPE("window-maximize", isTargetingMaximize())
                WIDGET_SLOT("interact", toggleMaximized)
            END_SCOPE
            START_SCOPE("window-close", isTargetingClose())
                WIDGET_SLOT("interact", dismiss)
            END_SCOPE
            WIDGET_SLOT("interact", startMove)
        END_SCOPE
        START_SCOPE("window-border", isTargetingBorder())
            WIDGET_SLOT("interact", startResize)
        END_SCOPE
        WIDGET_SLOT("move-window", startMove)

        START_INDEPENDENT_SCOPE("transform", _transform != Window_Transform_None)
            WIDGET_SLOT("release", release)
            WIDGET_SLOT("drag", drag)
        END_SCOPE
    END_SCOPE
}

void InterfaceWindow::toggle(const Point& point)
{
    if (_shown)
    {
        close();
    }
    else
    {
        open(point);
    }
}

void InterfaceWindow::close()
{
    hide();
}

void InterfaceWindow::show()
{
    _shown = true;

    view()->interface()->updateWindowFocus();
}

void InterfaceWindow::hide()
{
    _shown = false;

    view()->interface()->updateWindowFocus();
}

bool InterfaceWindow::shown() const
{
    return _shown;
}

bool InterfaceWindow::shouldDestroy() const
{
    return _destroy && popupCount == 0;
}

bool InterfaceWindow::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    bool consumedLocally = InterfaceWidget::movePointer(type, pointer, consumed);

    return consumedLocally || _transform != Window_Transform_None;
}

void InterfaceWindow::move(const Point& position)
{
    this->position(position - (this->size() / 2));

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + (name.empty() ? UIScale::dialogBordersOffset() : UIScale::windowBordersOffset()));
    }
}

void InterfaceWindow::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->resize(this->size() - (name.empty() ? UIScale::dialogBordersSize() : UIScale::windowBordersSize()));
    }
}

void InterfaceWindow::step()
{
    InterfaceWidget::step();
}

void InterfaceWindow::draw(const DrawContext& ctx) const
{
    bool active = focused() || _transform != Window_Transform_None || popupCount != 0;

    drawSolid(
        ctx,
        this,
        theme.backgroundColor
    );

    drawBorder(
        ctx,
        this,
        UIScale::marginSize(),
        active ? theme.accentColor : theme.outdentColor
    );

    if (!name.empty())
    {
        drawSolid(
            ctx,
            this,
            Point(),
            Size(size().w, UIScale::headerSize()),
            active ? theme.accentColor : theme.outdentColor
        );

        TextStyle style;
        style.weight = Text_Bold;
        style.alignment = Text_Center;
        style.color = theme.activeTextColor;

        drawText(
            ctx,
            this,
            Point(),
            Size(size().w, UIScale::headerSize()),
            name,
            style
        );

        drawColoredImage(
            ctx,
            this,
            Point(size().w - UIScale::headerSize() * 2, 0),
            Size(UIScale::headerSize()),
            maximized() ? Icon_Window_Unmaximize : Icon_Window_Maximize,
            isTargetingMaximize() ? theme.accentColor : theme.activeTextColor
        );

        drawColoredImage(
            ctx,
            this,
            Point(size().w - UIScale::headerSize(), 0),
            Size(UIScale::headerSize()),
            Icon_Window_Close,
            isTargetingClose() ? theme.accentColor : theme.activeTextColor
        );
    }

    for (InterfaceWidget* child : children())
    {
        child->draw(ctx);
    }
}

bool InterfaceWindow::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && _shown;
}

void InterfaceWindow::maximize()
{
    if (!maximized())
    {
        preMaximizeTransform = { position() + size() / 2, size() };
    }

    resize(dynamic_cast<const InterfaceWindowView*>(view())->size());
    move((dynamic_cast<const InterfaceWindowView*>(view())->size() / 2).toPoint());
}

void InterfaceWindow::unmaximize()
{
    resize(get<1>(preMaximizeTransform));
    move(get<0>(preMaximizeTransform));
}

bool InterfaceWindow::maximized() const
{
    return position() == Point() && size() == dynamic_cast<const InterfaceWindowView*>(view())->size();
}

void InterfaceWindow::setName(const string& name)
{
    this->name = name;
}

void InterfaceWindow::open(const Point& point, const Size& size)
{
    if (_shown)
    {
        return;
    }

    resize(size);
    move(point);
    show();
}

void InterfaceWindow::destroy()
{
    _destroy = true;
}

void InterfaceWindow::cancelDestruction()
{
    _destroy = false;
}

InterfaceWindowTransform InterfaceWindow::transform() const
{
    return _transform;
}

void InterfaceWindow::incrementPopupCount()
{
    popupCount++;
}

void InterfaceWindow::decrementPopupCount()
{
    popupCount--;

    if (popupCount == 0 && _destroy)
    {
        _destroy = false;
    }
}

bool InterfaceWindow::isTargetingHeader() const
{
    Point local = pointer - position();

    if (name.empty())
    {
        return false;
    }
    else
    {
        return local.y < UIScale::headerSize();
    }
}

bool InterfaceWindow::isTargetingMaximize() const
{
    Point local = pointer - position();

    if (name.empty())
    {
        return false;
    }
    else
    {
        return
            local.y >= 0 &&
            local.y < UIScale::headerSize() &&
            local.x >= size().w - UIScale::headerSize() * 2 &&
            local.x < size().w - UIScale::headerSize();
    }
}

bool InterfaceWindow::isTargetingClose() const
{
    Point local = pointer - position();

    if (name.empty())
    {
        return false;
    }
    else
    {
        return
            local.y >= 0 &&
            local.y < UIScale::headerSize() &&
            local.x >= size().w - UIScale::headerSize() &&
            local.x < size().w;
    }
}

bool InterfaceWindow::isTargetingBorder() const
{
    Point local = pointer - position();

    if (name.empty())
    {
        return
            local.x < UIScale::marginSize() ||
            local.y < UIScale::marginSize() ||
            local.x >= size().w - UIScale::marginSize() ||
            local.y >= size().h - UIScale::marginSize();
    }
    else
    {
        return
            local.x < UIScale::marginSize() ||
            local.x >= size().w - UIScale::marginSize() ||
            local.y >= size().h - UIScale::marginSize();
    }
}

void InterfaceWindow::translate(const Point& delta)
{
    move(position() + (size() / 2) + delta);
}

void InterfaceWindow::scaleBack(const Point& delta)
{
    move(position() + (size() / 2) + delta);
    resize(size() - delta);
}

void InterfaceWindow::scaleForward(const Point& delta)
{
    resize(size() + delta);
}

void InterfaceWindow::toggleMaximized(const Input& input)
{
    maximized() ? unmaximize() : maximize();
}

void InterfaceWindow::startResize(const Input& input)
{
    Point local = pointer - position();

    if (_transform == Window_Transform_None)
    {
        _transform = (local.x < UIScale::marginSize() || local.y < UIScale::marginSize())
            ? Window_Transform_Scale_Back
            : Window_Transform_Scale_Forward;
        lastPointer = pointer;
    }
}

void InterfaceWindow::startMove(const Input& input)
{
    if (maximized())
    {
        unmaximize();
    }

    if (_transform == Window_Transform_None)
    {
        _transform = Window_Transform_Move;
        lastPointer = pointer;
    }
}

void InterfaceWindow::drag(const Input& input)
{
    if (_transform != Window_Transform_None)
    {
        switch (_transform)
        {
        case Window_Transform_None :
            break;
        case Window_Transform_Move :
            translate(pointer - lastPointer);
            break;
        case Window_Transform_Scale_Back :
            scaleBack(pointer - lastPointer);
            break;
        case Window_Transform_Scale_Forward :
            scaleForward(pointer - lastPointer);
            break;
        }
        lastPointer = pointer;
    }
}

void InterfaceWindow::release(const Input& input)
{
    if (_transform != Window_Transform_None)
    {
        _transform = Window_Transform_None;
        lastPointer = pointer;
    }
}

SizeProperties InterfaceWindow::sizeProperties() const
{
    return sizePropertiesFillAll;
}
