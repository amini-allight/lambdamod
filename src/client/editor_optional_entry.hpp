/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_window_view.hpp"
#include "column.hpp"
#include "row.hpp"
#include "label.hpp"
#include "spacer.hpp"
#include "unit_field.hpp"
#include "optional_field.hpp"

template<typename T>
class EditorOptionalEntry : public InterfaceWidget
{
public:
    EditorOptionalEntry(
        InterfaceWidget* parent,
        const string& name,
        const OptionalField<T>::OptionalSource& optionalSource,
        const OptionalField<T>::OnOptional& onOptional,
        const OptionalField<T>::CreateWidget& createWidget,
        const T& defaultValue = T(),
        bool shouldLocalize = true
    )
        : InterfaceWidget(parent)
        , tooltip(!shouldLocalize ? "" : getTooltip(name))
    {
        START_WIDGET_SCOPE("editor-optional-entry")
        END_SCOPE

        auto column = new Column(this);

        MAKE_SPACER(column, 0, UIScale::marginSize());

        auto row = new Row(column);
        MAKE_SPACER(row, UIScale::marginSize(), 0);
        new Label(
            row,
            shouldLocalize ? localize(name) : name
        );

        new OptionalField<T>(
            row,
            optionalSource,
            onOptional,
            createWidget,
            defaultValue
        );

        MAKE_SPACER(row, UIScale::marginSize(), 0);

        MAKE_SPACER(column, 0, UIScale::marginSize());
    }

    EditorOptionalEntry(
        InterfaceWidget* parent,
        const string& name,
        const string& unit,
        const OptionalField<T>::OptionalSource& optionalSource,
        const OptionalField<T>::OnOptional& onOptional,
        const OptionalField<T>::CreateWidget& createWidget,
        const T& defaultValue = T(),
        bool shouldLocalize = true
    )
        : InterfaceWidget(parent)
        , tooltip(!shouldLocalize ? "" : getTooltip(name))
    {
        START_WIDGET_SCOPE("editor-optional-entry")
        END_SCOPE

        auto column = new Column(this);

        MAKE_SPACER(column, 0, UIScale::marginSize());

        auto row = new Row(column);
        MAKE_SPACER(row, UIScale::marginSize(), 0);
        new Label(row, shouldLocalize ? localize(name) : name);

        new OptionalField<T>(
            row,
            optionalSource,
            onOptional,
            [unit, createWidget](
                InterfaceWidget* parent,
                const OptionalField<T>::ValueSource& valueSource,
                const OptionalField<T>::OnValue& onValue
            ) -> void {
                new UnitField(parent, unit, bind(createWidget, placeholders::_1, valueSource, onValue));
            },
            defaultValue
        );

        MAKE_SPACER(row, UIScale::marginSize(), 0);

        MAKE_SPACER(column, 0, UIScale::marginSize());
    }

    void step()
    {
        if (focused() && !tooltip.empty())
        {
            dynamic_cast<InterfaceWindowView*>(view())->setTooltip(tooltip, pointer);
        }

        InterfaceWidget::step();
    }

private:
    string tooltip;

    SizeProperties sizeProperties() const
    {
        return {
            0, static_cast<f64>(UIScale::labelHeight() + UIScale::marginSize() * 2),
            Scaling_Fill, Scaling_Fixed
        };
    }

    string getTooltip(const string& name) const
    {
        string keyName = name + "-tooltip";

        return hasLocalization(keyName)
            ? localize(keyName)
            : "";
    }
};
