/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_button.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

TextButton::TextButton(
    InterfaceWidget* parent,
    const string& text,
    const function<void()>& onActivate
)
    : InterfaceWidget(parent)
    , text(text)
    , onActivate(onActivate)
{
    START_WIDGET_SCOPE("text-button")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void TextButton::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        focused() ? theme.accentColor : theme.outdentColor
    );

    TextStyle style;
    style.alignment = Text_Center;

    drawText(
        ctx,
        this,
        text,
        style
    );
}

void TextButton::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void TextButton::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void TextButton::interact(const Input& input)
{
    playPositiveActivateEffect();
    onActivate();
}

SizeProperties TextButton::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::textButtonHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
