/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "render_constants.hpp"
#include "message.hpp"

#include "theme.hpp"

class InterfaceWidget;
class HUD;

static constexpr i32 baseWindowHeight = 900;

static constexpr f64 hudRadius = 1; // m
static constexpr chrono::milliseconds spinnerRevolutionInterval(2000);
static constexpr chrono::milliseconds multiClickInterval(500);
static constexpr chrono::milliseconds oracleTableFadeDuration(250);
static constexpr chrono::seconds tooltipDelay(1);
static constexpr chrono::seconds pingOneshotDuration(3);
static constexpr chrono::seconds brakeOneshotDuration(5);
static constexpr chrono::seconds logOneshotDuration(6);
static constexpr i32 scriptNodeLinkCurveResolution = 128;
static constexpr size_t maxLogOneshots = 8;
// This is the fundamental unit other parts of the timeline are dimensioned from so it should be okay to stay non-scaling
static constexpr i32 timelineMillisecondsPerPixel = 10;
// Very long log oneshots can cause the system to try allocate an enormous texture to draw them into, which vulkan dislikes
static constexpr i32 maxLogOneshotLength = 256; // characters

static constexpr f64 virtualTouchThumbstickSize = 0.4;
// As a fraction of thumbstick size
static constexpr f64 virtualTouchThumbstickHatSize = 1.0 / 3.0;
static constexpr vec2 virtualTouchTriggerSize = { 0.125, 0.1 };
static constexpr vec2 virtualTouchShoulderButtonSize = { 0.125, 0.075 };
static constexpr vec2 virtualTouchSmallButtonSize = { 0.1, 0.05 };
static constexpr vec2 virtualTouchRoundButtonSize = { 0.1, 0.1 };
static constexpr vec2 virtualTouchDpadButtonSize = { 0.1, 0.1 };

static constexpr vec2 virtualTouchDpadCenter = { 0.25, 0.25 };
static constexpr f64 virtualTouchDpadOffset = 0.125;

static constexpr vec2 virtualTouchLeftTriggerCenter = { 0.05, 0.06 };
static constexpr vec2 virtualTouchLeftShoulderCenter = { 0.05, 0.16 };

static constexpr vec2 virtualTouchRightTriggerCenter = { 0.95, 0.06 };
static constexpr vec2 virtualTouchRightShoulderCenter = { 0.95, 0.16 };

static constexpr vec2 virtualTouchLeftStickCenter = { 0.2, 0.79 };
static constexpr vec2 virtualTouchRightStickCenter = { 0.8, 0.79 };
static constexpr vec2 virtualTouchRightStickOffset = { 0, -0.3 };

static constexpr vec2 virtualTouchBackButtonCenter = { 0.4, (virtualTouchRoundButtonSize.y / 2) + 0.01 };
static constexpr vec2 virtualTouchGuideButtonCenter = { 0.5, (virtualTouchRoundButtonSize.y / 2) + 0.01 };
static constexpr vec2 virtualTouchStartButtonCenter = { 0.6, (virtualTouchRoundButtonSize.y / 2) + 0.01 };

static constexpr f64 virtualTouchButtonRotations[] = { 0, -30, -60, -90 };
static constexpr vec2 virtualTouchButtonOffset = { 0, -0.01 };

constexpr Color messageColor(const Message& message)
{
    switch (message.type)
    {
    default :
    case Message_Direct_Chat_In :
    case Message_Direct_Chat_Out :
    case Message_Team_Chat_In :
    case Message_Team_Chat_Out :
    case Message_Chat :
    case Message_Script :
        return theme.textColor;
    case Message_Log :
        return theme.inactiveTextColor;
    case Message_Warning :
        return theme.warningColor;
    case Message_Error :
        return theme.errorColor;
    }
}

namespace UIScale
{

Point windowBordersOffset();
Size windowBordersSize();
Point dialogBordersOffset();
Size dialogBordersSize();

i32 smallFontSize(const InterfaceWidget* widget = nullptr);
i32 mediumFontSize(const InterfaceWidget* widget = nullptr);
i32 largeFontSize(const InterfaceWidget* widget = nullptr);
i32 vrMenuButtonFontSize(const InterfaceWidget* widget);
i32 vrKeyboardFontSize(const InterfaceWidget* widget);
i32 textTitlecardTitleFontSize(const InterfaceWidget* widget);
i32 textTitlecardSubtitleFontSize(const InterfaceWidget* widget);

i32 headerSize();
i32 marginSize(const InterfaceWidget* widget = nullptr);
Size checkboxSize();
Size iconButtonSize();
Size indicatorIconSize();
Size tabSize();
i32 labelHeight();
i32 radioBarHeight();
i32 textButtonHeight();
i32 lineEditHeight();
i32 scrollbarWidth();
i32 optionSelectHeight();
i32 optionSelectEntryHeight();
i32 optionSelectPopupMaxHeight();
i32 colorPickerHeight();
i32 colorPickerPopupHeight();
i32 alphaColorPickerPopupHeight();
i32 emissiveColorPickerPopupHeight();
i32 hdrColorPickerPopupHeight();
i32 hueBarWidth();
Size hueSliderSize();
i32 sliderHeight();
i32 sliderTrackHeight();
i32 sliderThumbSize();
i32 colorIndicatorBarHeight();
i32 consoleOutputRowHeight();
i32 menuWidth();
i32 menuRowHeight();
Size destinationSelectorSize();
i32 destinationSelectorRowHeight();
i32 destinationSelectorRows();
i32 destinationSelectorPopupWidth();
i32 destinationSelectorPopupHeight();
i32 consoleInputHeight();
Size saveIndicatorSize();
i32 itemListWidth();
i32 treeViewPanIncrement();
i32 treeViewScrollIncrement();
i32 treeViewNodeIndent();
i32 treeViewNodeHeight();
i32 virtualTouchControlRoundedCornerRadius();
i32 helpColumnWidth();
i32 helpRowHeight();
i32 dividerWidth();
Size spinnerSize();
i32 sidePanelWidth();
i32 scriptNodeLinkConnectorHeight();
i32 scriptNodeCapHeight();
i32 scriptNodeLinkWidth();
i32 scriptNodeLinkCurveResolution();
i32 scriptNodeWidth();
i32 scriptNodeDefaultHorizontalSpacing();
i32 scriptNodeDefaultVerticalSpacing();
i32 scriptNodeBorderRadius();
i32 scriptNodeHeaderHeight();
i32 widgetToBottomSpacing();
Size controlPointSize();
Size controlPointTextSize();
Size entityCardSize();
Size entityCardRowSize();
Size diagnosticLineSize();
Size markerSize();
Size markerLineSize();
i32 lineNumberColumnWidth();
i32 oracleTableColumnWidth();
i32 oracleTableRowHeight();
i32 oracleListWidth();
i32 timelineTopHeaderHeight();
i32 timelineLeftHeaderWidth();
i32 timelineColumnWidth();
i32 timelineRowHeight();
Size actionTimelineMarkerSize();
i32 timelineEditorButtonSpacing();
i32 tooltipHeight();
i32 unitIndicatorWidth();
i32 textCursorWidth();
i32 minBoxSelectSize();

i32 panelBorderSize(const InterfaceWidget* widget);
i32 panelScrollbarWidth(const InterfaceWidget* widget);
i32 panelScrollbarTrackWidth(const InterfaceWidget* widget);
i32 panelScrollbarThumbWidth(const InterfaceWidget* widget);
Size panelIconButtonSize(const InterfaceWidget* widget);
i32 panelTextButtonHeight(const InterfaceWidget* widget);
i32 panelOptionSelectHeight(const InterfaceWidget* widget);
i32 panelOptionSelectEntryHeight(const InterfaceWidget* widget);
i32 panelOptionSelectPopupMaxHeight(const InterfaceWidget* widget);
Size panelTabSize(const InterfaceWidget* widget);
Size panelCheckboxSize(const InterfaceWidget* widget);
i32 panelLineEditHeight(const InterfaceWidget* widget);
i32 panelLineEditLabelHeight(const InterfaceWidget* widget);
i32 panelLabelHeight(const InterfaceWidget* widget);
i32 panelBarTimerHeight(const InterfaceWidget* widget);
i32 panelGamepadMagnitudeToolHeight(const InterfaceWidget* widget);
i32 panelTextPanelMargin(const InterfaceWidget* widget);
Size panelVRMenuButtonSize(const InterfaceWidget* widget);
i32 panelGamepadMenuItemSpacing(const InterfaceWidget* widget);
i32 panelTextPanelItemSpacing(const InterfaceWidget* widget);
i32 panelTextPanelIconSize(const InterfaceWidget* widget);
i32 panelChooseMajorBlockTitleHeight(const InterfaceWidget* widget);
i32 panelChooseMajorBlockSubtitleHeight(const InterfaceWidget* widget);
i32 panelChooseMajorBlockTextHeight(const InterfaceWidget* widget);
i32 panelChooseMajorBlockHeight(const InterfaceWidget* widget);
i32 panelHUDTextWidth(const InterfaceWidget* widget);
i32 panelItemListWidth(const InterfaceWidget* widget);
i32 panelItemListRowHeight(const InterfaceWidget* widget);
i32 panelTextTitlecardTopPadding(const InterfaceWidget* widget);
i32 panelTextTitlecardTitleHeight(const InterfaceWidget* widget);
i32 panelTextTitlecardSubtitleHeight(const InterfaceWidget* widget);
Size panelMainMenuColumnSize(const InterfaceWidget* widget);
Size panelMainMenuIconSize(const InterfaceWidget* widget);
Size panelMainMenuTitleSize(const InterfaceWidget* widget);
i32 panelMainMenuIconOffset(const InterfaceWidget* widget);
i32 panelTextCursorWidth(const InterfaceWidget* widget);

i32 hudFontSize(const HUD* hud);
i32 hudBarHeight(const HUD* hud);
i32 hudMarginSize(const HUD* hud);

}
