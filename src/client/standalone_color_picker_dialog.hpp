/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "dialog.hpp"

#include "line_edit.hpp"
#include "text_button.hpp"

class StandaloneColorPickerDialog : public Dialog
{
public:
    StandaloneColorPickerDialog(
        InterfaceView* view,
        const Color& color,
        const function<void(const Color&)>& onDone
    );

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;

private:
    Color color;
    function<void(const Color&)> onDone;

    LineEdit* lineEdit;
    TextButton* button;

    string hexSource();
    void onSetHex(const string& text);
    void onSubmit();

    Point huePosition() const;
    Size hueSize() const;
    Point saturationValuePosition() const;
    Size saturationValueSize() const;

    void setHue(const Input& input);
    void setSaturationValue(const Input& input);
};
