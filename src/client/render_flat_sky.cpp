/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_sky.hpp"

RenderFlatSky::RenderFlatSky(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    const RenderFlatPipelineStore* pipelineStore
)
    : RenderSky(
        ctx,
        descriptorSetLayout,
        pipelineStore->sky.pipelineLayout,
        pipelineStore->sky.pipeline,
        pipelineStore->skyStars.pipelineLayout,
        pipelineStore->skyStars.pipeline,
        pipelineStore->skyCircle.pipelineLayout,
        pipelineStore->skyCircle.pipeline,
        pipelineStore->skyCloud.pipelineLayout,
        pipelineStore->skyCloud.pipeline,
        pipelineStore->skyAurora.pipelineLayout,
        pipelineStore->skyAurora.pipeline,
        pipelineStore->skyComet.pipelineLayout,
        pipelineStore->skyComet.pipeline,
        pipelineStore->skyVortex.pipelineLayout,
        pipelineStore->skyVortex.pipeline,
        pipelineStore->skyEnvMap.pipelineLayout,
        pipelineStore->skyEnvMap.pipeline,
        pipelineStore->skyEnvMapStars.pipelineLayout,
        pipelineStore->skyEnvMapStars.pipeline,
        pipelineStore->skyEnvMapCircle.pipelineLayout,
        pipelineStore->skyEnvMapCircle.pipeline,
        pipelineStore->skyEnvMapCloud.pipelineLayout,
        pipelineStore->skyEnvMapCloud.pipeline,
        pipelineStore->skyEnvMapAurora.pipelineLayout,
        pipelineStore->skyEnvMapAurora.pipeline,
        pipelineStore->skyEnvMapComet.pipelineLayout,
        pipelineStore->skyEnvMapComet.pipeline,
        pipelineStore->skyEnvMapVortex.pipelineLayout,
        pipelineStore->skyEnvMapVortex.pipeline
    )
    , swapchainElements(swapchainElements)
{
    createComponents();
}

RenderFlatSky::~RenderFlatSky()
{
    destroyComponents();
}

void RenderFlatSky::work(const FlatSwapchainElement* swapchainElement, const SkyParameters& sky, const vec3& up) const
{
    if (!sky.enabled)
    {
        return;
    }

    fillUniformBuffer(
        components.at(swapchainElement->imageIndex()),
        sky,
        up
    );

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())
    );
}

void RenderFlatSky::renderEnvironmentMap(const SkySwapchainElement* swapchainElement, const SkyParameters& sky, const vec3& up) const
{
    if (!sky.enabled)
    {
        return;
    }

    fillUniformBuffer(
        components.at(swapchainElement->imageIndex()),
        sky,
        up
    );

    fillEnvMapCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())
    );
}

void RenderFlatSky::refresh(const vector<FlatSwapchainElement*>& swapchainElements)
{
    this->swapchainElements = swapchainElements;

    destroyComponents();

    createComponents();
}

void RenderFlatSky::createComponents()
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        components.push_back(new RenderSkyComponent(
            ctx,
            descriptorSetLayout,
            swapchainElements.at(i)->camera()
        ));
    }
}
