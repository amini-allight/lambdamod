/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "side_panel.hpp"

class ListView;
class ActionEditor;

class ActionEditorSidePanel : public SidePanel
{
public:
    ActionEditorSidePanel(InterfaceWidget* parent);

    void step() override;

private:
    ListView* listView;

    ActionPartID id;

    void addCommonFields();
    void addIKFields();
    void clearChildren();

    string idSource() const;

    f64 endSource() const;
    void onEnd(f64 end);

    u64 targetIDSource() const;
    void onTargetID(u64 targetID);

    vector<string> typeOptionsSource();
    vector<string> typeOptionTooltipsSource();
    size_t typeSource();
    void onType(size_t index);

    vec3 positionSource() const;
    void onPosition(const vec3& position);

    quaternion rotationSource() const;
    void onRotation(const quaternion& rotation);

    ActionEditor* editor() const;
};
