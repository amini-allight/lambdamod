/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "transform_mode.hpp"
#include "edit_tool_axis.hpp"

#pragma pack(1)
struct GizmoIntersectorTriangle
{
    fvec3 a;
    fvec3 b;
    fvec3 c;

    optional<vec3> intersect(const vec3& cameraPosition, const vec3& selectionDirection) const;
};
#pragma pack()

struct GizmoIntersectorElement
{
    TransformMode mode;
    EditToolAxis axis;
    vector<GizmoIntersectorTriangle> triangles;
};

class GizmoIntersector
{
public:
    GizmoIntersector();
    GizmoIntersector(const GizmoIntersector& rhs) = delete;
    GizmoIntersector(GizmoIntersector&& rhs) = delete;
    ~GizmoIntersector();

    GizmoIntersector& operator=(const GizmoIntersector& rhs) = delete;
    GizmoIntersector& operator=(GizmoIntersector&& rhs) = delete;

    optional<tuple<TransformMode, EditToolAxis>> select(
        const vec3& gizmoPosition,
        const quaternion& gizmoRotation,
        const vec3& cameraPosition,
        const vec3& selectionDirection
    );

private:
    vector<GizmoIntersectorElement> elements;

    vector<GizmoIntersectorTriangle> transformTriangles(
        const fmat4& transform,
        vector<GizmoIntersectorTriangle> triangles
    ) const;
};
