/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "vr_device.hpp"

class User;
class ControlContext;

class UserVisualizerDecorations
{
public:
    UserVisualizerDecorations(ControlContext* ctx, const User* user);
    UserVisualizerDecorations(const UserVisualizerDecorations& rhs) = delete;
    UserVisualizerDecorations(UserVisualizerDecorations&& rhs) = delete;
    ~UserVisualizerDecorations();

    UserVisualizerDecorations& operator=(const UserVisualizerDecorations& rhs) = delete;
    UserVisualizerDecorations& operator=(UserVisualizerDecorations&& rhs) = delete;

    void join();
    void leave();

    void setShownWorld(const string& name);
    void setShown(bool state);

    void setWorld(const string& name);

    void moveViewer(const mat4& transform);
    void setViewerAngle(f64 angle);
    void setViewerAspect(f64 aspect);

    void moveRoom(const mat4& transform);
    void setRoomBounds(const vec2& bounds);
    void setRoomOffset(const mat4& offset);

    void moveDevice(VRDevice device, const mat4& transform);

private:
    ControlContext* ctx;
    UserID id;

    void remove(RenderDecorationID id) const;

    RenderDecorationID viewerID;
    RenderDecorationID roomID;
    RenderDecorationID headID;
    RenderDecorationID leftHandID;
    RenderDecorationID rightHandID;
    RenderDecorationID hipsID;
    RenderDecorationID leftFootID;
    RenderDecorationID rightFootID;
    RenderDecorationID chestID;
    RenderDecorationID leftShoulderID;
    RenderDecorationID rightShoulderID;
    RenderDecorationID leftElbowID;
    RenderDecorationID rightElbowID;
    RenderDecorationID leftWristID;
    RenderDecorationID rightWristID;
    RenderDecorationID leftKneeID;
    RenderDecorationID rightKneeID;
    RenderDecorationID leftAnkleID;
    RenderDecorationID rightAnkleID;

    bool visible() const;

    void updateVisibility();
    void updateVisibility(RenderDecorationID id, bool state);

    const User* getUser() const;

    optional<mat4> viewerTransform;
    optional<f64> angle;
    optional<f64> aspect;
    optional<mat4> roomTransform;
    optional<vec2> bounds;
    optional<mat4> roomOffset;

#ifdef LMOD_VR
    template<typename FlatT, typename VRT, typename... Args>
    RenderDecorationID addDecoration(Args... args);

    template<typename FlatT, typename VRT, typename... Args>
    void updateDecoration(RenderDecorationID id, Args... args);
#else
    template<typename T, typename... Args>
    RenderDecorationID addDecoration(Args... args);

    template<typename T, typename... Args>
    void updateDecoration(RenderDecorationID id, Args... args);
#endif
};
