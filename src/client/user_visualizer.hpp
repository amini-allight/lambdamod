/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "user_visualizer_decorations.hpp"

class ControlContext;

class UserVisualizer
{
public:
    UserVisualizer(ControlContext* ctx);
    UserVisualizer(const UserVisualizer& rhs) = delete;
    UserVisualizer(UserVisualizer&& rhs) = delete;
    ~UserVisualizer();

    UserVisualizer& operator=(const UserVisualizer& rhs) = delete;
    UserVisualizer& operator=(UserVisualizer&& rhs) = delete;

    void setShown(bool state); 

    void add(UserID id);
    void remove(UserID id);

    void join(UserID id);
    void leave(UserID id);

    void setWorld(UserID id, const string& name);

    void moveViewer(UserID id, const mat4& transform);
    void setViewerAngle(UserID id, f64 angle);
    void setViewerAspect(UserID id, f64 aspect);

    void moveRoom(UserID id, const mat4& transform);
    void setRoomBounds(UserID id, const vec2& bounds);

    void moveDevice(UserID id, VRDevice device, const mat4& transform);

private:
    ControlContext* ctx;
    map<UserID, UserVisualizerDecorations*> users;
};
