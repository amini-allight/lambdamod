/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_mode_context.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "entity_editor.hpp"

ActionModeContext::ActionModeContext(ViewerModeContext* ctx)
    : InternalModeContext(ctx)
{

}

void ActionModeContext::addNewPrefab(const string& name)
{
    // Do nothing
}

void ActionModeContext::addNew()
{
    // Do nothing
}

void ActionModeContext::addNew(BodyPartType type)
{
    // Do nothing
}

void ActionModeContext::removeSelected()
{
    // Do nothing
}

void ActionModeContext::addChildren(const Point& point)
{
    // Do nothing
}

void ActionModeContext::removeParent()
{
    // Do nothing
}

void ActionModeContext::hideSelected()
{
    // Do nothing
}

void ActionModeContext::hideUnselected()
{
    // Do nothing
}

void ActionModeContext::unhideAll()
{
    // Do nothing
}

void ActionModeContext::addKeyframes()
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    for (const BodyPartSelectionID& part : selectionIDs())
    {
        editor->addActionFrame(part.id);
    }
}

void ActionModeContext::removeKeyframes()
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    for (const BodyPartSelectionID& part : selectionIDs())
    {
        editor->removeActionFrame(part.id);
    }
}

void ActionModeContext::resetPosition()
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    ctx->context()->controller()->edit([this, editor]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            mat4 previous = editor->getActionTransform(selection.part->id());

            if (selection.subID != BodyPartSubID() && !selection.part->subparts().empty())
            {
                previous.setPosition(selection.part->subparts().at(selection.subID.value() - 1));
            }

            mat4 current = previous;
            current.setPosition(vec3());

            if (selection.subID != BodyPartSubID())
            {
                return;
            }

            editor->setActionTransform(selection.part->id(), current);
        }
    });
}

void ActionModeContext::resetRotation()
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    ctx->context()->controller()->edit([this, editor]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            if (selection.subID != BodyPartSubID())
            {
                continue;
            }

            mat4 transform = editor->getActionTransform(selection.part->id());

            transform.setRotation(quaternion());

            editor->setActionTransform(selection.part->id(), transform);
        }
    });
}

void ActionModeContext::resetScale()
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    ctx->context()->controller()->edit([this, editor]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            if (selection.subID != BodyPartSubID())
            {
                continue;
            }

            mat4 transform = editor->getActionTransform(selection.part->id());

            transform.setScale(vec3(1));

            editor->setActionTransform(selection.part->id(), transform);
        }
    });
}

void ActionModeContext::resetLinearVelocity()
{
    // Do nothing
}

void ActionModeContext::resetAngularVelocity()
{
    // Do nothing
}

void ActionModeContext::selectionToCursor()
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    ctx->context()->controller()->edit([this, editor]() -> void {
        for (const BodyPartSelection& selection : selectionRootsOnly())
        {
            mat4 previous = editor->getActionTransform(selection.part->id());

            if (selection.subID != BodyPartSubID() && !selection.part->subparts().empty())
            {
                previous.setPosition(selection.part->subparts().at(selection.subID.value() - 1));
            }

            mat4 current = previous;
            current.setPosition(globalToRegional(ctx->cursorPosition()));

            if (selection.subID != BodyPartSubID())
            {
                return;
            }

            editor->setActionTransform(selection.part->id(), current);
        }
    });
}

void ActionModeContext::duplicate()
{
    // Do nothing
}

void ActionModeContext::cut()
{
    // Do nothing
}

void ActionModeContext::copy()
{
    // Do nothing
}

void ActionModeContext::paste()
{
    // Do nothing
}

void ActionModeContext::extrude()
{
    // Do nothing
}

void ActionModeContext::split()
{
    // Do nothing
}

void ActionModeContext::join(const Point& point)
{
    // Do nothing
}

void ActionModeContext::transformSet(size_t i, const vec3& pivot, const mat4& initial, const mat4& transform)
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return;
    }

    ctx->context()->controller()->edit([this, editor, i, transform]() -> void {
        BodyPartSelection selection = selectionRootAtIndex(i);

        mat4 current = transform;

        if (selection.subID != BodyPartSubID())
        {
            return;
        }

        current = activeEntity()->globalTransform().applyToTransform(transform, false);

        if (selection.part->parent())
        {
            current = selection.part->parent()->regionalTransform().applyToTransform(current, false);
        }

        editor->setActionTransform(selection.part->id(), current);
    }, firstTransform);
    firstTransform = false;
}

mat4 ActionModeContext::transformGet(size_t i) const
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return mat4();
    }

    BodyPartSelection selection = selectionRootAtIndex(i);

    mat4 transform = editor->getActionTransform(selection.part->id());

    if (selection.subID != BodyPartSubID() && !selection.part->subparts().empty())
    {
        transform.setPosition(transform.applyToPosition(selection.part->subparts().at(selection.subID.value() - 1)));
    }

    if (selection.part->parent())
    {
        transform = selection.part->parent()->regionalTransform().applyToTransform(transform);
    }

    return activeEntity()->globalTransform().applyToTransform(transform);
}

mat4 ActionModeContext::transformGetWithPromotion(size_t i) const
{
    EntityEditor* editor = ctx->context()->interface()->getWindow<EntityEditor>();

    if (!editor)
    {
        return mat4();
    }

    BodyPartSelection selection = selectionRootAtIndex(i);

    mat4 transform = editor->getActionTransform(selection.part->id());

    if (selection.part->parent())
    {
        transform = selection.part->parent()->regionalTransform().applyToTransform(transform);
    }

    return activeEntity()->globalTransform().applyToTransform(transform);
}
