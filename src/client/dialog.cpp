/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dialog.hpp"

Dialog::Dialog(InterfaceView* view, const Size& defaultSize)
    : InterfaceWindow(view, "")
    , defaultSize(defaultSize)
{
    START_WIDGET_SCOPE("dialog")
        WIDGET_SLOT("close-dialog", dismiss)
    END_SCOPE
}

void Dialog::open(const Point& point)
{
    InterfaceWindow::open(point, defaultSize);
}

void Dialog::focus()
{
    if (focused())
    {
        return;
    }

    InterfaceWidget::focus();

    cancelDestruction();
}

void Dialog::unfocus()
{
    if (!focused())
    {
        return;
    }

    InterfaceWidget::unfocus();

    if (transform() == Window_Transform_None)
    {
        destroy();
    }
}

void Dialog::clearFocus()
{
    if (!focused())
    {
        return;
    }

    InterfaceWidget::clearFocus();

    if (transform() == Window_Transform_None)
    {
        destroy();
    }
}

void Dialog::dismiss(const Input& input)
{
    destroy();
}
