/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_line_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

PanelLineEdit::PanelLineEdit(
    InterfaceWidget* parent,
    const string& label,
    const function<string()>& source,
    const function<void(const string&)>& onReturn
)
    : LineEdit(parent, source, onReturn)
    , label(label)
    , hideContents(false)
{

}

void PanelLineEdit::draw(const DrawContext& ctx) const
{
    // Uses monospace to make cursor positioning easier
    TextStyle labelStyle(this);
    labelStyle.font = Text_Monospace;
    labelStyle.weight = Text_Bold;
    labelStyle.size = UIScale::mediumFontSize(this);

    drawText(
        ctx,
        this,
        Point(),
        Size(size().w, UIScale::panelLineEditLabelHeight(this)),
        label,
        labelStyle
    );

    drawSolid(
        ctx,
        this,
        Point(0, UIScale::panelLineEditLabelHeight(this)),
        size() - Size(0, UIScale::panelLineEditLabelHeight(this)),
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        Point(0, UIScale::panelLineEditLabelHeight(this)),
        size() - Size(0, UIScale::panelLineEditLabelHeight(this)),
        UIScale::panelBorderSize(this),
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    if (selected)
    {
        i32 start = clamp(selectionStart(), _scrollIndex, _scrollIndex + visualColumns()) - _scrollIndex;
        i32 end = clamp(selectionEnd(), _scrollIndex, _scrollIndex + visualColumns()) - _scrollIndex;

        drawSolid(
            ctx,
            this,
            Point(
                (UIScale::panelBorderSize(this) * 2) + UIScale::marginSize(this) + (start * style().getFont()->monoWidth()),
                UIScale::panelBorderSize(this) + UIScale::panelLineEditLabelHeight(this) + (size().h - (style().getFont()->height() + (UIScale::panelBorderSize(this) * 2) + UIScale::panelLineEditLabelHeight(this))) / 2
            ),
            Size((end - start) * style().getFont()->monoWidth(), style().getFont()->height()),
            focused() ? theme.panelForegroundColor : theme.accentColor
        );
    }

    if (!hideContents)
    {
        drawText(
            ctx,
            this,
            Point(UIScale::panelBorderSize(this) * 2, UIScale::panelBorderSize(this) + UIScale::panelLineEditLabelHeight(this)),
            size() - Size(UIScale::panelBorderSize(this) * 2, (UIScale::panelBorderSize(this) * 2) + UIScale::panelLineEditLabelHeight(this)),
            content(),
            style()
        );
    }
    else
    {
        string content = this->content();
        u32 width = style().getFont()->monoWidth();
        u32 height = (size() - Size(UIScale::panelBorderSize(this) * 2, (UIScale::panelBorderSize(this) * 2) + UIScale::panelLineEditLabelHeight(this))).h;
        u32 yOffset = (height - width) / 2;
        u32 radius = width / 2;

        for (size_t i = 0; i < content.size(); i++)
        {
            drawColoredImage(
                ctx,
                this,
                Point(
                    UIScale::panelBorderSize(this) * 2 + UIScale::marginSize(this) + i * width,
                    UIScale::panelBorderSize(this) + UIScale::panelLineEditLabelHeight(this) + yOffset
                ),
                Size(radius * 2),
                Icon_Dot,
                focused() ? theme.accentColor : theme.panelForegroundColor
            );
        }
    }

    u32 textHeight = style().getFont()->height();

    if (focused() && _cursorIndex >= _scrollIndex && _cursorIndex < _scrollIndex + visualColumns())
    {
        drawSolid(
            ctx,
            this,
            Point(
                UIScale::panelBorderSize(this) * 2 + UIScale::marginSize(this) + style().getFont()->width(content().substr(0, cursorIndex())),
                UIScale::panelLineEditLabelHeight(this) + (size().h - (UIScale::panelLineEditLabelHeight(this) + textHeight)) / 2
            ),
            Size(UIScale::panelTextCursorWidth(this), textHeight),
            theme.accentColor
        );
    }
}

void PanelLineEdit::setHideContents(bool state)
{
    hideContents = state;
}

TextStyle PanelLineEdit::style() const
{
    TextStyle style;
    style.size = UIScale::mediumFontSize(this);
    style.color = focused() ? theme.accentColor : theme.panelForegroundColor;

    return style;
}

i32 PanelLineEdit::screenToIndex(const Point& point) const
{
    return (point.x - ((UIScale::panelBorderSize(this) * 2) + UIScale::marginSize(this))) / style().getFont()->monoWidth();
}

SizeProperties PanelLineEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::panelLineEditHeight(this)),
        Scaling_Fill, Scaling_Fixed
    };
}
