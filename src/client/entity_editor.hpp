/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_window.hpp"
#include "input_types.hpp"

#include "general_editor.hpp"
#include "bindings_editor.hpp"
#include "hud_editor.hpp"
#include "logic_editor.hpp"
#include "body_editor.hpp"
#include "animation_editor.hpp"
#include "sound_editor.hpp"

class TabView;

class EntityEditor : public InterfaceWindow
{
public:
    EntityEditor(InterfaceView* view, EntityID id);
    ~EntityEditor();

    void step() override;

    EntityID id() const;
    Entity* entity() const;
    bool inBodyMode() const;
    bool inActionMode() const;

    void addActionFrame(BodyPartID id);
    void removeActionFrame(BodyPartID id);
    mat4 getActionTransform(BodyPartID id) const;
    void setActionTransform(BodyPartID id, const mat4& transform);

    void open(const Point& point) override;

private:
    EntityID _id;

    TabView* tabs;
    GeneralEditor* general;
    BindingsEditor* bindings;
    HUDEditor* hud;
    LogicEditor* logic;
    BodyEditor* body;
    AnimationEditor* animation;
    SoundEditor* sound;

    void dismiss(const Input& input) override;
    void forceDismiss(const Input& input);

    void closeAll();
};
