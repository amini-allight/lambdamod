/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"

#include "confirm_dialog.hpp"

SoundEditor::SoundEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
{
    START_WIDGET_SCOPE("sound-editor")
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
    END_SCOPE

    auto column = new Column(this);

    auto row = new Row(column);

    sampleList = new ItemList(
        row,
        bind(&SoundEditor::itemSource, this),
        bind(&SoundEditor::onAdd, this, placeholders::_1),
        bind(&SoundEditor::onRemove, this, placeholders::_1),
        bind(&SoundEditor::onSelect, this, placeholders::_1),
        bind(&SoundEditor::onRename, this, placeholders::_1, placeholders::_2),
        bind(&SoundEditor::onClone, this, placeholders::_1, placeholders::_2)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    editor = new SampleEditor(row, id);
}

vector<string> SoundEditor::itemSource()
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return {};
    }

    vector<string> names;
    names.reserve(entity->samples().size());

    for (const auto& [ name, sample ] : entity->samples())
    {
        names.push_back(name);
    }

    return names;
}

void SoundEditor::onAdd(const string& name)
{
    context()->controller()->edit([this, name]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addSample(name, Sample());
    });
}

void SoundEditor::onRemove(const string& name)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("sound-editor-are-you-sure-you-want-to-delete-sample", name),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            context()->controller()->edit([this, name]() -> void {
                Entity* entity = context()->controller()->game().get(id);

                if (!entity)
                {
                    return;
                }

                entity->removeSample(name);
            });
        }
    );
}

void SoundEditor::onSelect(const string& name)
{
    editor->open(name);
}

void SoundEditor::onRename(const string& oldName, const string& newName)
{
    context()->controller()->edit([this, oldName, newName]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addSample(newName, entity->samples().at(oldName));
        entity->removeSample(oldName);
    });
}

void SoundEditor::onClone(const string& oldName, const string& newName)
{
    context()->controller()->edit([this, oldName, newName]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addSample(newName, entity->samples().at(oldName));
    });
}

void SoundEditor::undo(const Input& input)
{
    context()->viewerMode()->undo();
}

void SoundEditor::redo(const Input& input)
{
    context()->viewerMode()->redo();
}

SizeProperties SoundEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
