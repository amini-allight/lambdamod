/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_terrain_graph.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

RenderTerrainGraph::RenderTerrainGraph(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    const vector<TerrainNode>& nodes
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, rotation, vec3(1) };

    fvec3 color = theme.panelForegroundColor.toVec3();

    vector<ColoredVertex> vertexData;
    vertexData.reserve(nodes.size() * 2);

    map<TerrainNodeID, vec3> positionsByID;

    for (const TerrainNode& node : nodes)
    {
        positionsByID.insert({ node.id, node.position });
    }

    set<TerrainNodeID> visitedIDs;

    for (const TerrainNode& node : nodes)
    {
        for (TerrainNodeID linkedID : node.linkedIDs)
        {
            if (visitedIDs.contains(linkedID))
            {
                continue;
            }
            
            vertexData.push_back({ node.position, color });
            vertexData.push_back({ positionsByID.at(linkedID), color });
        }

        visitedIDs.insert(node.id);
    }

    vertexCount = vertexData.size();

    vertices = createVertexBuffer(vertexData.size() * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderTerrainGraph::~RenderTerrainGraph()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderTerrainGraph::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderTerrainGraph::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
