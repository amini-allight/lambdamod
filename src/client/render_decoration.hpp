/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_swapchain_elements.hpp"
#include "render_decoration_component.hpp"

enum RenderDecorationType : u8
{
    Render_Decoration_Default,
    Render_Decoration_Over_Panels,
    Render_Decoration_Independent_Depth
};

class RenderDecoration
{
public:
    RenderDecoration(
        const RenderContext* ctx,
        VkDescriptorSetLayout descriptorSetLayout,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    );
    RenderDecoration(const RenderDecoration& rhs) = delete;
    RenderDecoration(RenderDecoration&& rhs) = delete;
    virtual ~RenderDecoration() = 0;

    RenderDecoration& operator=(const RenderDecoration& rhs) = delete;
    RenderDecoration& operator=(RenderDecoration&& rhs) = delete;

    void show(bool state);
    bool shown() const;

    virtual RenderDecorationType drawType() const;

protected:
    bool _shown;
    const RenderContext* ctx;
    VkDescriptorSetLayout descriptorSetLayout;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    vector<RenderDecorationComponent*> components;

    VmaBuffer createVertexBuffer(size_t size) const;
    VmaBuffer createUniformBuffer(size_t size) const;

    void destroyComponents();
};
