/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"

#include <vulkan/vulkan.h>

static constexpr u32 desiredMajorVulkanVersion = 1;
static constexpr u32 desiredMinorVulkanVersion = 2;

static constexpr u32 shaderDefaultSize = 1024;
static constexpr u32 maxRenderCount = 1024 * 1024;
static constexpr size_t maxDescriptorCount = 8192;

static constexpr u32 minWidth = 1280;
static constexpr u32 minHeight = 720;

static constexpr u32 maxWidth = 7680;
static constexpr u32 maxHeight = 4320;

static constexpr u32 cursorWidth = 24;
static constexpr u32 cursorHeight = 24;

static constexpr i32 textTitlecardWidth = 1024;
static constexpr i32 textTitlecardHeight = 512;
static constexpr i32 textWidth = 1024;
static constexpr i32 textHeight = 2048;
static constexpr i32 hudWidth = 3072;
static constexpr i32 hudHeight = 3072;
static constexpr i32 vrKeyboardWidth = 2048;
static constexpr i32 vrKeyboardHeight = 1024;
static constexpr i32 skyWidth = 1024;
static constexpr i32 skyHeight = 1024;

static constexpr u32 skyboxVertexCount = 6 * 2 * 3;

static constexpr VkFormat colorFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
static constexpr VkFormat depthFormat = VK_FORMAT_D32_SFLOAT;
static constexpr VkFormat stencilFormat = VK_FORMAT_R32G32B32A32_SFLOAT;
static constexpr VkFormat panelFormat = VK_FORMAT_B8G8R8A8_UNORM;
static constexpr VkFormat textureFormat = VK_FORMAT_B8G8R8A8_UNORM;
static constexpr VkFormat maskTextureFormat = VK_FORMAT_R8_UNORM;

static constexpr f32 hdrGamma = 2.2f;

static constexpr fvec3 luminanceFactor = { 0.2126, 0.7152, 0.0722 };

static constexpr chrono::microseconds renderAutoExposureDelay(chrono::duration_cast<chrono::microseconds>(chrono::seconds(1)));
static constexpr f32 renderAutoExposureMinLuminance = 0.01;
static constexpr f32 renderAutoExposureMaxLuminance = 100'000;
static constexpr f32 renderAutoExposureMultiplier = 0.75;

static constexpr f64 minLightIntensity = 0.1;

static constexpr u32 maxLightCount = 128;
static constexpr u32 maxPrimitiveCount = 16 * 1024;

static constexpr f32 motionBlurStrength = 1;
static constexpr f32 depthOfFieldRange = 5;
static constexpr f32 chromaticAberrationStrength = 0.15;

static constexpr u32 fixedPostProcessStageCount = 3;

static constexpr u32 cubemapImageCount = 6;

static constexpr vec3 baseBackgroundColor = vec3();

static constexpr u32 textPixelsPerUnit = 5000;
static constexpr u32 maxTextPixels = 1024;
static constexpr u32 symbolPixelsPerUnit = 5000;
static constexpr u32 maxSymbolPixels = 8192;
static constexpr u32 textureTileSize = 8192;
static constexpr u32 maxTextureTileCount = 64;

static constexpr u32 skyStarsMaxCount = 512;
static constexpr f32 skyStarsScale = 0.005;

static constexpr u32 skyCloudMaxParticleCount = 512;
static constexpr f32 skyCloudParticleScale = 0.025;

static constexpr u32 skyAuroraMaxParticleCount = 1024;
static constexpr u32 skyAuroraLinePartCount = 64;
static constexpr f64 skyAuroraAltitude = 100;
static constexpr f64 skyAuroraRadius = 100;
static constexpr f64 skyAuroraScale = 20;
static constexpr f64 skyAuroraSpacing = 0.3;
static constexpr f64 skyAuroraMaxAngleChange = pi * 0.05;

static constexpr u32 skyCometMaxParticleCount = 256;
static constexpr f32 skyCometMinParticleSize = pi / 900; // 0.2 deg
static constexpr f32 skyCometParticleCountStep = pi / 12; // 15 deg
static constexpr u32 skyCometParticlesPerStep = 32;

static constexpr u32 skyVortexMaxParticleCount = 512;

static constexpr u32 atmosphereLightningMaxParticleCount = 8;
static constexpr u32 atmosphereLightningVarietyCount = 8;
static constexpr f32 atmosphereLightningThickness = 1; // m
static constexpr u32 atmosphereLightningPointCount = 8;
static constexpr u32 atmosphereLightningVertexCount = (atmosphereLightningPointCount - 1) * 6;
static constexpr chrono::milliseconds atmosphereLightningDuration(500);
static constexpr chrono::milliseconds atmosphereLightningGenerateInterval(10);

static constexpr u32 atmosphereEffectParticleCount = 8192;
static constexpr u32 atmosphereEffectRotationCount = 16;
static constexpr f64 atmosphereEffectMinAreaRadius = 10; // m
static constexpr f64 atmosphereEffectMaxAreaRadius = 100; // m

static constexpr u32 atmosphereEffectCloudVarietyCount = 8;
static constexpr u32 atmosphereEffectCloudSubParticleCount = 32;
static constexpr f64 atmosphereEffectCloudRadius = 2; // m

static constexpr u32 atmosphereEffectStreamerDisplayRadius = 64;
static constexpr u32 atmosphereEffectStreamerCount = 128;

static constexpr chrono::milliseconds maxSwapchainAcquireDelay(1);

static constexpr u32 renderPassVRViewMask = 0b11;

static const fmat4 gizmoElementTransforms[] = {
    fmat4(fvec3(), fquaternion(rightDir, upDir), fvec3(1)),
    fmat4(fvec3(), fquaternion(forwardDir, upDir), fvec3(1)),
    fmat4(fvec3(), fquaternion(upDir, backDir), fvec3(1))
};

static constexpr f64 gizmoBaseDistance = 10;

static constexpr string gizmoArrowFileName = "gizmo-arrow";
static constexpr string gizmoRingFileName = "gizmo-ring";
static constexpr string gizmoPlanesFileName = "gizmo-planes";

static constexpr u32 noiseTextureSize = 256;
static constexpr u64 noiseSeed = 0;

static constexpr u32 areaSurfaceNoiseTextureSize = 256;
static constexpr u64 areaSurfaceNoiseTextureSeed = 0;
static constexpr f64 areaSurfaceNoiseTextureFrequency = 1.0 / 64.0;
static constexpr f64 areaSurfaceNoiseTextureDepth = 4;
