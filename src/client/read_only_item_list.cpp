/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "read_only_item_list.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "row.hpp"
#include "label.hpp"

ReadOnlyItemList::ReadOnlyItemList(
    InterfaceWidget* parent,
    const function<vector<string>()>& source,
    const function<void(string)>& onSelect
)
    : InterfaceWidget(parent)
    , activeIndex(-1)
    , source(source)
    , _onSelect(onSelect)
{
    listView = new ListView(this, bind(&ReadOnlyItemList::onSelect, this, placeholders::_1));
}

void ReadOnlyItemList::step()
{
    vector<string> items = source();

    if (items != this->items)
    {
        this->items = items;
        updateListView();
    }
}

void ReadOnlyItemList::updateListView()
{
    listView->clearChildren();

    i32 i = 0;
    for (const string& item : items)
    {
        bool focused = i == activeIndex;

        auto row = new Row(
            listView,
            focused ? theme.accentColor : optional<Color>()
        );

        TextStyleOverride style;
        style.weight = focused ? Text_Bold : Text_Regular;
        style.color = focused ? &theme.activeTextColor : &theme.textColor;

        new Label(
            row,
            item,
            style
        );

        i++;
    }

    listView->update();
}

void ReadOnlyItemList::onSelect(size_t index)
{
    if (static_cast<i32>(index) == activeIndex)
    {
        return;
    }

    activeIndex = index;
    updateListView();

    _onSelect(source().at(index));
}

SizeProperties ReadOnlyItemList::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::itemListWidth()), 0,
        Scaling_Fixed, Scaling_Fill
    };
}
