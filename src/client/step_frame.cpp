/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "step_frame.hpp"

StepFrame::StepFrame()
{

}

void StepFrame::enable()
{
    state.enable();
}

void StepFrame::disable()
{
    state.disable();
}

void StepFrame::makeReplayOf(const StepFrame& previous)
{
    events = previous.events;
    editInputs = previous.editInputs;
    attachedInputs = previous.attachedInputs;
}

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> StepFrame::compare(const StepFrame& previous) const
{
    return state.compare(previous.state);
}

vector<NetworkEvent> StepFrame::step(EntityID hostEntityID)
{
    state.step();

    vector<NetworkEvent> sideEffects;

    for (const NetworkEvent& event : events)
    {
        bool hasSideEffect = state.push(event);

        if (hasSideEffect)
        {
            sideEffects.push_back(event);
        }
    }
    events.clear();

    for (const auto& [ handler, input ] : editInputs)
    {
        handler(input);
    }
    editInputs.clear();

    for (const auto& [ hook, input ] : attachedInputs)
    {
        state.pushInput(hostEntityID, hook, input);
    }
    attachedInputs.clear();

    state.updateDynamics();

    return sideEffects;
}

void StepFrame::push(const NetworkEvent& event)
{
    events.push_back(event);
}

void StepFrame::push(const vector<NetworkEvent>& events)
{
    this->events.insert(
        this->events.end(),
        events.begin(),
        events.end()
    );
}

void StepFrame::push(const vector<tuple<function<void(const Input&)>, Input>>& editInputs)
{
    this->editInputs.insert(
        this->editInputs.end(),
        editInputs.begin(),
        editInputs.end()
    );
}

void StepFrame::push(const string& hook, const Input& input)
{
    attachedInputs.push_back({ hook, input });
}

// Only here in the client to make StepFrameHistory build

template<>
StepFrame YAMLNode::convert() const
{
    StepFrame frame;

    frame.state = at("state").as<GameState>();

    return frame;
}

template<>
void YAMLSerializer::emit(StepFrame v)
{
    startMapping();

    emitPair("state", v.state);

    endMapping();
}
