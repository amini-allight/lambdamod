/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "entity_mode_context.hpp"
#include "body_mode_context.hpp"
#include "action_mode_context.hpp"

class ControlContext;
class Entity;
class FlatRender;

enum ViewerSubMode : u8
{
    Viewer_Sub_Mode_Entity,
    Viewer_Sub_Mode_Body,
    Viewer_Sub_Mode_Action
};

class ViewerModeContext
{
public:
    ViewerModeContext(ControlContext* context);
    ViewerModeContext(const ViewerModeContext& rhs) = delete;
    ViewerModeContext(ViewerModeContext&& rhs) = delete;
    ~ViewerModeContext();

    ViewerModeContext& operator=(const ViewerModeContext& rhs) = delete;
    ViewerModeContext& operator=(ViewerModeContext&& rhs) = delete;

    ControlContext* context() const;

    void step();

    void enter();
    void exit();

    ViewerSubMode mode() const;

    EntityModeContext* entityMode() const;
    BodyModeContext* bodyMode() const;
    ActionModeContext* actionMode() const;
    EditModeContext* editMode() const;

    void attach(EntityID id);

    bool editing() const;
    void toggleEditMode();

    bool cursorShown() const;
    void setCursorShown(bool state);
    const vec3& cursorPosition() const;
    void setCursorPosition(const vec3& position);
    void moveCursor(const Point& point);

    bool gridShown() const;
    void setGridShown(bool state);
    const vec3& gridPosition() const;
    void setGridPosition(const vec3& grid);

    f64 gridSize() const;
    void setGridSize(f64 size);

    bool entityPointsShown() const;
    void setEntityPointsShown(bool state);

    bool gizmoShown() const;
    void setGizmoShown(bool state);

    bool spaceGraphVisualizerShown() const;
    void setSpaceGraphVisualizerShown(bool state);

    f64 canvasPaintBrushRadius() const;
    void setCanvasPaintBrushRadius(f64 radius);

    f64 translationStep() const;
    void setTranslationStep(f64 size);

    f64 rotationStep() const;
    void setRotationStep(f64 size);

    f64 scaleStep() const;
    void setScaleStep(f64 size);

    void undo();
    void redo();

    bool gamepadMenu() const;
    void toggleGamepadMenu();

private:
    ControlContext* _context;

    ViewerSubMode _mode;
    EntityModeContext* _entityMode;
    BodyModeContext* _bodyMode;
    ActionModeContext* _actionMode;

    RenderDecorationID cursorID;
    RenderDecorationID spaceGraphVisualizerID;

    bool _editing;
    bool _cursorShown;
    vec3 _cursorPosition;
    bool _gridShown;
    vec3 _gridPosition;
    f64 _gridSize;
    bool _entityPointsShown;
    bool _gizmoShown;
    bool _spaceGraphVisualizerShown;
    f64 _canvasPaintBrushRadius;
    f64 _translationStep;
    f64 _rotationStep;
    f64 _scaleStep;
    bool _gamepadMenu;

    FlatRender* render() const;

    bool modeHasChanged() const;
    bool inBodyMode() const;
    bool inActionMode() const;
};
