/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "layout.hpp"

class TreeView;

class TreeViewNode : public Layout
{
public:
    TreeViewNode(
        InterfaceWidget* parent,
        TreeView* root,
        const string& name,
        bool expanded,
        const function<bool()>& filtering,
        const function<bool()>& filter,
        const function<void(const Point&)>& onRightClick
    );

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void draw(const DrawContext& ctx) const override;

    const string& name() const;

    i32 rowCount() const;
    i32 columnCount() const;

    bool expanded() const;
    bool filtered() const;
    bool hidden() const;

private:
    TreeView* root;
    string _name;
    function<bool()> filtering;
    function<bool()> filter;
    function<void(const Point&)> onRightClick;
    bool _expanded;

    void toggleExpansion(const Input& input);
    void openContextMenu(const Input& input);

    SizeProperties sizeProperties() const override;
};
