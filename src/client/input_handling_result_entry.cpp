/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_handling_result_entry.hpp"
#include "control_context.hpp"

InputHandlingResultEntry::InputHandlingResultEntry(
    const vector<string>& path,
    const InputScopeBinding& binding,
    const InputScopeSlot& slot,
    size_t depth
)
    : path(path)
    , binding(binding)
    , slot(slot)
    , depth(depth)
{

}

bool InputHandlingResultEntry::operator<(const InputHandlingResultEntry& rhs) const
{
    if (binding < rhs.binding)
    {
        return true;
    }
    else if (binding > rhs.binding)
    {
        return false;
    }
    else
    {
        return depth > rhs.depth;
    }
}

bool InputHandlingResultEntry::operator>(const InputHandlingResultEntry& rhs) const
{
    if (binding > rhs.binding)
    {
        return true;
    }
    else if (binding < rhs.binding)
    {
        return false;
    }
    else
    {
        return depth < rhs.depth;
    }
}

void InputHandlingResultEntry::commit(ControlContext* ctx, const Input& input) const
{
    slot.behavior(input);

    if (slot.replayable)
    {
        ctx->controller()->pushEditInput(slot.behavior, input);
    }
}

bool InputHandlingResultEntry::wantsInclusion(const InputHandlingResultEntry& previous) const
{
    switch (slot.inclusionMode)
    {
    default :
        fatal("Encountered unknown input scope slot inclusion mode '" + to_string(slot.inclusionMode) + "' checking inclusion behavior.");
    case Input_Scope_Slot_Inclusion_None :
        return false;
    case Input_Scope_Slot_Inclusion_With_Above :
        return isAbove(previous);
    case Input_Scope_Slot_Inclusion_With_Below :
        return isBelow(previous);
    case Input_Scope_Slot_Inclusion_With_Both :
        return isAbove(previous) || isBelow(previous);
    }
}

bool InputHandlingResultEntry::isAbove(const InputHandlingResultEntry& previous) const
{
    if (previous.path.size() > path.size())
    {
        return false;
    }

    for (size_t i = 0; i < previous.path.size(); i++)
    {
        if (previous.path.at(i) != path.at(i))
        {
            return false;
        }
    }

    return true;
}

bool InputHandlingResultEntry::isBelow(const InputHandlingResultEntry& previous) const
{
    if (previous.path.size() < path.size())
    {
        return false;
    }

    for (size_t i = 0; i < path.size(); i++)
    {
        if (path.at(i) != previous.path.at(i))
        {
            return false;
        }
    }

    return true;
}
