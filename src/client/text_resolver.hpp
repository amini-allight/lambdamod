/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "tools.hpp"
#include "body_parts.hpp"
#include "image.hpp"

class TextResolverKey
{
public:
    TextResolverKey(BodyTextType type, const string& text, u32 resolution);

    bool operator==(const TextResolverKey& rhs) const;
    bool operator!=(const TextResolverKey& rhs) const;
    bool operator>(const TextResolverKey& rhs) const;
    bool operator<(const TextResolverKey& rhs) const;
    bool operator>=(const TextResolverKey& rhs) const;
    bool operator<=(const TextResolverKey& rhs) const;
    strong_ordering operator<=>(const TextResolverKey& rhs) const;

private:
    BodyTextType type;
    string text;
    u32 resolution;
};

template<typename T>
class TextResolverCacheEntry
{
public:
    TextResolverCacheEntry(const T& value, chrono::milliseconds timeout)
        : value(value)
        , lastAccessed(currentTime())
        , timeout(timeout)
    {

    }

    T get() const
    {
        lastAccessed = currentTime();
        return value;
    }

    bool expired() const
    {
        return currentTime() - lastAccessed > timeout;
    }

private:
    T value;
    mutable chrono::milliseconds lastAccessed;
    chrono::milliseconds timeout;
};

// This is a caching layer for text generated for text body parts, it is not used for interface text (which has its own more advanced caching system, shared with many other things)
class TextResolver
{
public:
    TextResolver();
    TextResolver(const TextResolver& rhs) = delete;
    TextResolver(TextResolver&& rhs) = delete;
    ~TextResolver();

    TextResolver& operator=(const TextResolver& rhs) = delete;
    TextResolver& operator=(TextResolver&& rhs) = delete;

    Image* resolve(const BodyPartText& part) const;

private:
    mutable map<TextResolverKey, TextResolverCacheEntry<Image*>> cachedImages;

    void clean() const;
};

extern TextResolver* textResolver;
