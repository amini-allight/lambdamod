/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "controller.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "constants.hpp"
#include "url_tools.hpp"
#include "theme_loader.hpp"
#include "localization_loader.hpp"
#include "oracle_loader.hpp"
#include "url.hpp"
#include "font_store.hpp"
#include "symbol_resolver.hpp"
#include "text_resolver.hpp"
#include "mesh_cache.hpp"

#include <csignal>

// TODO: UI: Why are icons jaggy?
// TODO: NETWORK: Slowdown when rotating camera: this is because every couple frames we are asked to resimulate 10 - 15 frames via Controller::handleNetworkEventBatch
// why is the number so large and why only sometimes?
// TODO: NETWORK: Possibly related, there are network state flickers when cancelling edits and also some parts of canvas edits seem to be rewound out of existence
// TODO: RENDER: In some places the edge lines on a cube manage to get occluded by the cube itself, on both hardware and software occlusion systems

static constexpr chrono::seconds maxSessionJoinTime = chrono::seconds(5);

static bool quit = false;
static Controller* controller = nullptr;

static void displayPlatformInfo()
{
    string platform;

#if defined(__ANDROID__)
    platform += "Android";
#elif defined(__APPLE__) && defined(TARGET_OS_IPHONE)
    platform += "iOS";
#elif defined(WIN32)
    platform += "Windows";
#elif defined(__APPLE__) && defined(TARGET_OS_MAC)
    platform += "MacOS";
#elif defined(__linux__)
    platform += "Linux";
#elif defined(__FreeBSD__)
    platform += "FreeBSD";
#elif defined(__EMSCRIPTEN__)
    platform += "Web";
#else
#error Unknown operating system encountered.
#endif

#if defined(__linux__) || defined(__FreeBSD__)
#if defined(LMOD_WAYLAND)
    platform += " Wayland";
#else
    platform += " X11";
#endif
#if defined(LMOD_PIPEWIRE)
    platform += " PipeWire";
#else
    platform += " PulseAudio";
#endif
#endif

#ifdef LMOD_VR
    platform += " - VR Support Enabled";
#else
    platform += " - VR Support Disabled";
#endif

#ifdef LMOD_DEBUG
    platform += " - Development";
#else
    platform += " - Deployment";
#endif

    log(platform);
}

static void onInterrupt(int sig)
{
    warning("Interrupt received, shutting down.");

    controller->quit();
}

static void displayArgumentError(int argc, char** argv)
{
    cerr << "Usage: " << argv[0] << " [url|(host port serverPassword name password)] [vr]" << endl;
}

static int setup()
{
    try
    {
        g_config = loadConfig();
    }
    catch (const exception& e)
    {
        error("Failed to load config: " + string(e.what()));
    }

    if (g_config.vrForced)
    {
        g_vr = true;
    }

#if !defined(LMOD_VR)
    if (g_vr)
    {
        error("VR mode cannot be used unless LMOD was compiled with OpenXR bindings.");
        return 1;
    }
#endif

    controller = new Controller();

    return 0;
}

static int openMainMenu(int argc, char** argv)
{
    g_vr = argc == 2 ? stoi(argv[1]) : false;

    return setup();
}

static int urlConnect(int argc, char** argv)
{
    if (isDirectURL(argv[1]))
    {
        string username;
        string password;
        string host;
        u16 port;
        string serverPassword;

        try
        {
            tuple<string, string, string, u16, string> parts = parseDirectURL(argv[1]);

            username = get<0>(parts);
            password = get<1>(parts);
            host = get<2>(parts);
            port = get<3>(parts);
            serverPassword = get<4>(parts);
        }
        catch (const exception& e)
        {
            error("URL parsing failed: " + string(e.what()));
            return 1;
        }

        g_vr = argc == 3 ? stoi(argv[2]) : false;

        int result = setup();

        if (result)
        {
            return result;
        }

        controller->connect({ host, port }, serverPassword, username, password);
    }
    else
    {
#if defined(__EMSCRIPTEN__)
        error("Session URLs are not supported on web for technical reasons.");
        return 1;
#else
        string username;
        string password;
        string sessionID;
        string adminPassword;

        try
        {
            tuple<string, string, string, string> parts = parseSessionURL(argv[1]);

            username = get<0>(parts);
            password = get<1>(parts);
            sessionID = get<2>(parts);
            adminPassword = get<3>(parts);
        }
        catch (const exception& e)
        {
            error("URL parsing failed: " + string(e.what()));
            return 1;
        }

        string clientHost = "0.0.0.0";
        u16 clientPort = randomPort();

        string serverHost;
        u16 serverPort;
        string serverPassword;

        chrono::milliseconds startTime = currentTime();

        while (true)
        {
            optional<tuple<string, u16, string>> connectionDetails = joinSession(sessionID, clientPort);

            if (!connectionDetails)
            {
                if (currentTime() - startTime > maxSessionJoinTime)
                {
                    error("Joining session timed out.");
                    return 1;
                }
                else
                {
                    this_thread::sleep_for(chrono::seconds(1));
                    continue;
                }
            }

            serverHost = get<0>(*connectionDetails);
            serverPort = get<1>(*connectionDetails);
            serverPassword = get<2>(*connectionDetails);
            break;
        }

        g_vr = argc == 3 ? stoi(argv[2]) : false;

        int result = setup();

        if (result)
        {
            return result;
        }

        controller->holePunch(
            { clientHost, clientPort },
            { serverHost, serverPort },
            !adminPassword.empty() ? adminPassword : serverPassword,
            username,
            password
        );
#endif
    }

    return 0;
}

static int autoConnect(int argc, char** argv)
{
    string host;
    u16 port;
    string serverPassword;
    string username;
    string password;

    try
    {
        host = argv[1];
        port = stoi(argv[2]);
        serverPassword = argv[3];
        username = argv[4];
        password = argv[5];

        g_vr = argc == 7 ? stoi(argv[6]) : false;
    }
    catch (const exception& e)
    {
        displayArgumentError(argc, argv);
        return 1;
    }

    int result = setup();

    if (result)
    {
        return result;
    }

    controller->connect({ host, port }, serverPassword, username, password);

    return 0;
}

#if defined(WIN32)
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
    int argc = __argc;
    char** argv = __argv;
#else
int main(int argc, char** argv)
{
#endif
    cout << gameName << " Client " << gameVersion << endl;
    cout << "Copyright 2025 Amini Allight" << endl << endl;
    cout << "This program comes with ABSOLUTELY NO WARRANTY; This is free software, and you are welcome to redistribute it under certain conditions. See the included license for further details." << endl << endl;

    displayPlatformInfo();

    try
    {
        loadTheme();
    }
    catch (const exception& e)
    {
        error("Failed to load theme: " + string(e.what()));
    }

    try
    {
        loadLocalization();
    }
    catch (const exception& e)
    {
        error("Failed to load localization: " + string(e.what()));
    }

    try
    {
        loadOracles();
    }
    catch (const exception& e)
    {
        error("Failed to load oracles: " + string(e.what()));
    }

    fontStore = new FontStore();
    symbolResolver = new SymbolResolver();
    textResolver = new TextResolver();
    meshCache = new MeshCache();

    if (argc == 1 || (argc == 2 && !isURL(argv[1])))
    {
        int result = openMainMenu(argc, argv);

        if (result)
        {
            return result;
        }
    }
    else if (argc == 2 || argc == 3)
    {
        int result = urlConnect(argc, argv);

        if (result)
        {
            return result;
        }
    }
    else if (argc == 6 || argc == 7)
    {
        int result = autoConnect(argc, argv);

        if (result)
        {
            return result;
        }
    }
    else
    {
        displayArgumentError(argc, argv);
        return 1;
    }

    signal(SIGINT, onInterrupt);
#if defined(__unix__) || defined(__APPLE__)
    signal(SIGPIPE, SIG_IGN);
#endif

    while (!quit)
    {
        bool quit = controller->run();

        if (quit)
        {
            break;
        }
    }

    controller->disconnect();

    delete controller;

    delete meshCache;
    delete textResolver;
    delete symbolResolver;
    delete fontStore;

    return 0;
}
