/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_entity_body_material.hpp"
#include "render_occlusion_scene_reference.hpp"

// This is a container for per-frame uniform buffers (containing entity transform info) and per-frame per-material descriptor sets that combine those with material-specific information (e.g. textures)
class RenderEntityComponent
{
public:
    RenderEntityComponent(
        const RenderContext* ctx,
        const vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>>& materials,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    );
    RenderEntityComponent(const RenderEntityComponent& rhs) = delete;
    RenderEntityComponent(RenderEntityComponent&& rhs) = delete;
    ~RenderEntityComponent();

    RenderEntityComponent& operator=(const RenderEntityComponent& rhs) = delete;
    RenderEntityComponent& operator=(RenderEntityComponent&& rhs) = delete;

    // True if the component can be rebound without recreation (because it uses the same materials)
    bool supports(const vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>>& materials) const;
    void bind(
        const vector<tuple<BodyRenderMaterialType, RenderEntityBodyMaterial*>>& materials,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    );

    VmaBuffer object;
    VmaMapping<f32>* objectMapping;
    vector<tuple<BodyRenderMaterialType, VkDescriptorSet>> materialDescriptorSets;
    // This flag is set when the mesh has been destroyed and any textures contained within need to be rebound to our descriptor sets
    bool needsRebind;

private:
    const RenderContext* ctx;
    mutable VkAccelerationStructureKHR lastAccelerationStructure;

    void createMaterialDescriptorSet(
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    );

    void bind(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindVertex(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindTexture(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindArea(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindTerrain(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindRope(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindShadelessVertex(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindShadelessTexture(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindShadelessArea(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindShadelessTerrain(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;
    void bindShadelessRope(
        VkDescriptorSet descriptorSet,
        BodyRenderMaterialType type,
        const RenderEntityBodyMaterial* material,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;

};
