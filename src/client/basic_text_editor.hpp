/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "syntax_highlighting.hpp"

struct TextEditorState
{
    TextEditorState();

    void generateLines();

    string text;
    vector<string> lines;
    ivec2 cursor;
    ivec2 select;
    ivec2 view;
    bool selected;
};

class BasicTextEditor : public InterfaceWidget
{
public:
    BasicTextEditor(
        InterfaceWidget* parent,
        const function<string()>& source,
        const function<void(const string&)>& onSave,
        bool codeMode = true
    );

    bool saved() const;
    void set(const string& text);
    void notifyReload();
    void display(const string& message);

    void draw(const DrawContext& ctx) const override;
    void focus() override;
    void unfocus() override;

protected:
    function<string()> source;
    function<void(const string&)> onSave;
    bool codeMode;

    bool _saved;
    string statusMessage;
    deque<TextEditorState> states;
    deque<TextEditorState> redoStates;
    bool selecting;
    chrono::milliseconds lastClickTime;

    map<i32, vector<SyntaxHighlightChunk>> syntaxHighlight;

    virtual void drawLineNumbers(const DrawContext& ctx) const;
    virtual void drawStatusLine(const DrawContext& ctx) const;
    virtual void drawActiveLine(const DrawContext& ctx) const;
    virtual void drawContent(const DrawContext& ctx) const;
    virtual void drawCursor(const DrawContext& ctx) const;

    void drawRangeOnLine(const DrawContext& ctx, i32 i, i32 start, i32 end, const Color& color) const;

    void cursorUp();
    void cursorDown();
    void cursorLeft();
    void cursorRight();
    void selectUp();
    void selectDown();
    void selectLeft();
    void selectRight();
    void goTo(i32 index);
    void selectTo(i32 index);
    void scrollUp(i32 lines = 1);
    void scrollDown(i32 lines = 1);
    void scrollLeft();
    void scrollRight();
    void cutSelection(const Input& input);
    void copySelection(const Input& input);
    void pasteClipboard();
#if (defined(__linux__) || defined(__FreeBSD__))
    void pastePrimaryClipboard(const Point& pointer);
#endif
    void add(i32 index, const string& s);
    void add(i32 index, char c);
    void remove(i32 start, i32 count);
    void replace(i32 index, char c);
    void replace(i32 index, const string& s);
    void copyRegion(const Input& input, i32 start, i32 size);
    void cutRegion(const Input& input, i32 start, i32 size);
    void undo();
    void redo();
    void selectTo(const Point& pointer);
    void startSelect(const Point& pointer);
    void endSelect(const Input& input, const Point& pointer);
    void moveSelect(const Point& pointer);

    i32 lines() const;
    i32 columns() const;

    i32 visualLines() const;
    i32 visualColumns() const;

    const string& lineAt(i32 index = -1) const;
    char characterAt(i32 index = -1) const;
    const string& lineAtCursor() const;
    string wordAtCursor() const;
    char characterAtCursor() const;
    string selectedText() const;

    i32 charWidth() const;
    i32 charHeight() const;

    i32 cursorIndex() const;
    i32 cursorLine() const;
    i32 cursorColumn() const;
    i32 cursorApparentColumn() const;

    i32 viewLine() const;
    i32 viewColumn() const;

    optional<i32> cursorVisualLine() const;
    optional<i32> cursorVisualColumn() const;

    i32 documentStart() const;
    i32 documentEnd() const;

    i32 pageStart() const;
    i32 pageMiddle() const;
    i32 pageEnd() const;

    i32 softLineStart(i32 index = -1) const;
    i32 lineStart(i32 index = -1) const;
    i32 lineEnd(i32 index = -1) const;
    i32 lineIndent(i32 index = -1) const;

    i32 previousWordStart() const;
    i32 previousWordEnd() const;

    i32 wordStart(bool exact = false) const;
    i32 wordEnd() const;

    i32 nextWordStart() const;
    i32 nextWordEnd() const;

    i32 previousCharacter() const;
    i32 nextCharacter() const;

    i32 selectionStart() const;
    i32 selectionEnd() const;

    i32 selectionLineStart() const;
    i32 selectionLineEnd() const;

    ivec2 indexToPosition(i32 index) const;
    i32 positionToIndex(const ivec2& position) const;
    ivec2 screenToPosition(const Point& point) const;
    i32 screenToIndex(const Point& point) const;

    void setState(const function<void(TextEditorState&)>& transform);
    void updateState(const function<void(TextEditorState&)>& transform);
    void correctState(TextEditorState& state, const TextEditorState& old) const;
    const TextEditorState& state() const;

    void updateSyntaxHighlight(const TextEditorState& state);

    void selectAll(const Input& input);
    void cursorToPreviousWord(const Input& input);
    void cursorToNextWord(const Input& input);
    void selectUp(const Input& input);
    void selectDown(const Input& input);
    void selectLeft(const Input& input);
    void selectRight(const Input& input);
    void selectToPreviousWord(const Input& input);
    void selectToNextWord(const Input& input);
    void cursorUp(const Input& input);
    void cursorDown(const Input& input);
    void cursorLeft(const Input& input);
    void cursorRight(const Input& input);
    void goToLineStart(const Input& input);
    void goToLineEnd(const Input& input);
    void removePreviousWord(const Input& input);
    void removeNextWord(const Input& input);
    void removeToLineStart(const Input& input);
    void removeToLineEnd(const Input& input);
    void removePreviousCharacter(const Input& input);
    void removeNextCharacter(const Input& input);
    void upcaseNextWord(const Input& input);
    void downcaseNextWord(const Input& input);
    void swapWords(const Input& input);
    void swapCharacters(const Input& input);
    void selectTo(const Input& input);
    void cursorTo(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);
    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void panLeft(const Input& input);
    void panRight(const Input& input);
    void pageUp(const Input& input);
    void pageDown(const Input& input);
    void addText(const Input& input);
    void cut(const Input& input);
    void copy(const Input& input);
    void paste(const Input& input);
    void undo(const Input& input);
    void redo(const Input& input);
    void save(const Input& input);
    void reload(const Input& input);
#if (defined(__linux__) || defined(__FreeBSD__))
    void pastePrimary(const Input& input);
#endif
    void endSelect(const Input& input);
    void moveSelect(const Input& input);

    SizeProperties sizeProperties() const override;
};
