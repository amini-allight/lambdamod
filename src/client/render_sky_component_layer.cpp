/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_sky_component_layer.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_sky_component.hpp"
#include "render_descriptor_set_write_components.hpp"

static constexpr size_t maxUniformBufferSize = max(
    sizeof(SkyLayerStarsGPU),
    max(
        sizeof(SkyLayerCircleGPU),
        max(
            sizeof(SkyLayerCloudGPU),
            sizeof(SkyLayerAuroraGPU)
        )
    )
);

RenderSkyComponentLayer::RenderSkyComponentLayer(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera
)
    : type(Sky_Layer_Stars)
    , billboardCount(0)
    , ctx(ctx)
    , descriptorSetLayout(descriptorSetLayout)
{
    uniform = createUniformBuffer(maxUniformBufferSize);
    uniformMapping = new VmaMapping<f32>(ctx->allocator, uniform);
    descriptorSet = createDescriptorSet(camera);
    envMapDescriptorSet = createDescriptorSet(ctx->environmentMappingCameraUniformBuffer);
}

RenderSkyComponentLayer::~RenderSkyComponentLayer()
{
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, envMapDescriptorSet);
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSet);
    delete uniformMapping;
    destroyBuffer(ctx->allocator, uniform);
}

void RenderSkyComponentLayer::fillUniformBuffer(
    const vec3& up,
    const fmat4& worldTransform,
    mt19937_64& engine,
    uniform_real_distribution<f32>& distribution,
    const SkyLayer& layer
)
{
    type = layer.type;

    switch (layer.type)
    {
    case Sky_Layer_Stars :
    {
        const SkyLayerStars& stars = layer.get<SkyLayerStars>();

        u32 count = stars.density * skyStarsMaxCount;

        SkyLayerStarsGPU skyLayerGPU;
        skyLayerGPU.color = stars.color.toVec4();

        for (u32 i = 0; i < count; i++)
        {
            f32 azimuth = distribution(engine) * pi;
            f32 elevation = asin(distribution(engine));

            fquaternion azimuthRotation(downDir, azimuth);
            fquaternion elevationRotation(rightDir, elevation);

            fvec3 direction = worldTransform.applyToDirection(azimuthRotation.rotate(elevationRotation.rotate(forwardDir)));

            fvec3 position = direction;
            fquaternion rotation = fquaternion(pickForwardDirection(-direction), -direction) * fquaternion(downDir, distribution(engine) * pi);

            skyLayerGPU.transforms[i] = fmat4(position, rotation, fvec3(skyStarsScale));
        }

        memcpy(uniformMapping->data, &skyLayerGPU, sizeof(skyLayerGPU));
        billboardCount = count;
        break;
    }
    case Sky_Layer_Circle :
    {
        const SkyLayerCircle& circle = layer.get<SkyLayerCircle>();

        SkyLayerCircleGPU skyLayerGPU;
        skyLayerGPU.worldTransform = fmat4(quaternion(pickForwardDirection(up), up)).inverse();
        skyLayerGPU.backgroundColor = vec4(baseBackgroundColor, 1);
        skyLayerGPU.position = circle.position;
        skyLayerGPU.color = circle.color.toVec4();
        skyLayerGPU.size = circle.size;
        skyLayerGPU.phase = circle.phase;

        memcpy(uniformMapping->data, &skyLayerGPU, sizeof(skyLayerGPU));
        billboardCount = 0;
        break;
    }
    case Sky_Layer_Cloud :
    {
        const SkyLayerCloud& cloud = layer.get<SkyLayerCloud>();

        u32 count = cloud.density * skyCloudMaxParticleCount;

        SkyLayerCloudGPU skyLayerGPU;
        skyLayerGPU.backgroundColor = vec4(baseBackgroundColor, 1);

        fmat4 transform = fmat4(
            vec3(),
            worldTransform.rotation() * fquaternion(downDir, cloud.position.x) * fquaternion(rightDir, cloud.position.y),
            vec3(1)
        );

        for (u32 i = 0; i < count; i++)
        {
            f32 azimuth = distribution(engine) * cloud.size.x;
            f32 elevation = asin(distribution(engine)) * (cloud.size.y / (pi / 2));

            fquaternion azimuthRotation(downDir, azimuth);
            fquaternion elevationRotation(rightDir, elevation);

            f32 factor = (1 - min(abs(azimuthRotation.angle()) / cloud.size.x, abs(elevationRotation.angle()) / cloud.size.y));

            fvec3 forward = azimuthRotation.rotate(elevationRotation.rotate(upDir));
            fvec3 up = azimuthRotation.rotate(elevationRotation.rotate(backDir));

            fvec3 position = -up;
            fquaternion rotation = fquaternion(forward, up);
            fvec3 scale = fvec3(skyCloudParticleScale) * factor;

            skyLayerGPU.transforms[i] = transform * fmat4(position, rotation, scale);
            skyLayerGPU.colors[i] = cloud.innerColor.toVec4() * factor + cloud.outerColor.toVec4() * (1 - factor);
        }

        memcpy(uniformMapping->data, &skyLayerGPU, sizeof(skyLayerGPU));
        billboardCount = count;
        break;
    }
    case Sky_Layer_Aurora :
    {
        const SkyLayerAurora& aurora = layer.get<SkyLayerAurora>();

        SkyLayerAuroraGPU skyLayerGPU;
        skyLayerGPU.lowerColor = aurora.lowerColor.toVec4();
        skyLayerGPU.upperColor = aurora.upperColor.toVec4();

        u32 lineCount = ceil((aurora.density * skyAuroraMaxParticleCount) / skyAuroraLinePartCount);

        vec3 forward;

        if (roughly(abs(forwardDir.dot(up)), 1.0))
        {
            forward = upDir;
        }
        else
        {
            forward = forwardDir;
        }

        for (u32 line = 0; line < lineCount; line++)
        {
            vec3 position;
            vec3 direction;

            position = quaternion(up, distribution(engine) * pi).rotate(forward) * distribution(engine) * skyAuroraRadius;
            direction = quaternion(up, distribution(engine) * pi).rotate(forward);

            for (u32 part = 0; part < skyAuroraLinePartCount; part++)
            {
                position += direction * skyAuroraSpacing * skyAuroraScale;
                direction = quaternion(up, distribution(engine) * skyAuroraMaxAngleChange).rotate(direction);

                u32 index = line * skyAuroraLinePartCount + part;

                skyLayerGPU.transforms[index] = fmat4(
                    position + up * skyAuroraAltitude,
                    fquaternion(direction, up),
                    fvec3(skyAuroraScale)
                );
            }
        }

        memcpy(uniformMapping->data, &skyLayerGPU, sizeof(skyLayerGPU));
        billboardCount = lineCount * skyAuroraLinePartCount;
        break;
    }
    case Sky_Layer_Comet :
    {
        const SkyLayerComet& comet = layer.get<SkyLayerComet>();

        SkyLayerCometGPU skyLayerGPU;

        fquaternion toSkyPosition = fquaternion(downDir, comet.position.x) * fquaternion(rightDir, comet.position.y);
        fquaternion toRotation(forwardDir, comet.rotation);

        u32 count = min<u32>((comet.length / skyCometParticleCountStep) * skyCometParticlesPerStep, skyCometMaxParticleCount);
        u32 i = 0;

        while (i < skyCometMaxParticleCount)
        {
            fvec2 angularPosition(
                distribution(engine) * (comet.size / 2),
                (comet.length / 2) + (distribution(engine) * (comet.length / 2))
            );

            f32 progress = angularPosition.y / comet.length;

            if (angularPosition.x > comet.size * (1 - progress))
            {
                continue;
            }

            fvec3 scale(max<f32>(actualDiameter(comet.size * (1 - progress), 1), skyCometMinParticleSize));

            fquaternion toTail(rightDir, angularPosition.y);
            fquaternion toOffset(downDir, angularPosition.x);

            fquaternion rotation = toSkyPosition * toRotation * toTail * toOffset * fquaternion(upDir, forwardDir);
            fvec3 position = rotation.up();

            skyLayerGPU.transforms[i] = fmat4(position, rotation, scale);
            skyLayerGPU.colors[i] = comet.tailColor.toVec4() * progress + comet.headColor.toVec4() * (1 - progress);

            i++;
        }

        memcpy(uniformMapping->data, &skyLayerGPU, sizeof(skyLayerGPU));
        billboardCount = count;
        break;
    }
    case Sky_Layer_Vortex :
    {
        // TODO: Vortex

        const SkyLayerVortex& vortex = layer.get<SkyLayerVortex>();

        SkyLayerVortexGPU skyLayerGPU;

        u32 count = skyVortexMaxParticleCount;
        u32 i = 0;

        while (i < count)
        {
            skyLayerGPU.transforms[i] = fmat4(upDir, quaternion(), vec3(1));
            skyLayerGPU.colors[i] = vortex.innerColor.toVec4();

            i++;
        }

        memcpy(uniformMapping->data, &skyLayerGPU, sizeof(skyLayerGPU));
        billboardCount = count;
        break;
    }
    case Sky_Layer_Meteor_Shower :
    {
        // TODO: Meteor shower
        break;
    }
    }
}

void RenderSkyComponentLayer::fillCommandBuffer(
    VkCommandBuffer commandBuffer,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
) const
{
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, type == Sky_Layer_Circle ? skyboxVertexCount : billboardCount, 1, 0, 0);
}

void RenderSkyComponentLayer::fillEnvMapCommandBuffer(
    VkCommandBuffer commandBuffer,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
) const
{
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &envMapDescriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, type == Sky_Layer_Circle ? skyboxVertexCount : billboardCount, 1, 0, 0);
}

VmaBuffer RenderSkyComponentLayer::createUniformBuffer(size_t size) const
{
    return createBuffer(
        ctx->allocator,
        size,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VkDescriptorSet RenderSkyComponentLayer::createDescriptorSet(VmaBuffer camera) const
{
    VkDescriptorSet descriptorSet = ::createDescriptorSet(
        ctx->device,
        ctx->descriptorPool,
        descriptorSetLayout
    );

    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, uniform.buffer, &parametersWriteBuffer, &parametersWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        parametersWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    return descriptorSet;
}
