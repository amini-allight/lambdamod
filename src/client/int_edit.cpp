/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "int_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "centered_line_edit.hpp"

IntEdit::IntEdit(
    InterfaceWidget* parent,
    const function<i64()>& source,
    const function<void(const i64&)>& onSet
)
    : InterfaceWidget(parent)
{
    i = new CenteredLineEdit(
        this,
        source ? [source]() -> string
        {
            return to_string(source());
        } : function<string()>(nullptr),
        [onSet](const string& s) -> void
        {
            try
            {
                onSet(stoll(s));
            }
            catch (const exception& e)
            {

            }
        }
    );
}

void IntEdit::set(i64 value)
{
    i->set(to_string(value));
}

i64 IntEdit::value() const
{
    try
    {
        return stoll(i->content());
    }
    catch (const exception& e)
    {
        return 0;
    }
}

void IntEdit::clear()
{
    i->clear();
}

void IntEdit::setPlaceholder(const string& text)
{
    i->setPlaceholder(text);
}

SizeProperties IntEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
