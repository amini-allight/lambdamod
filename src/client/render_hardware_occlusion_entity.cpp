/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_hardware_occlusion_entity.hpp"
#include "render_hardware_occlusion_scene.hpp"
#include "render_tools.hpp"

RenderHardwareOcclusionEntity::RenderHardwareOcclusionEntity(
    RenderHardwareOcclusionScene* scene,
    VkCommandBuffer commandBuffer,
    const RenderOcclusionMesh& mesh,
    const Body& body
)
    : scene(scene)
    , vkCreateAccelerationStructureKHR(scene->vkCreateAccelerationStructureKHR)
    , vkDestroyAccelerationStructureKHR(scene->vkDestroyAccelerationStructureKHR)
    , vkGetAccelerationStructureBuildSizesKHR(scene->vkGetAccelerationStructureBuildSizesKHR)
    , vkGetAccelerationStructureDeviceAddressKHR(scene->vkGetAccelerationStructureDeviceAddressKHR)
    , vkCmdBuildAccelerationStructuresKHR(scene->vkCmdBuildAccelerationStructuresKHR)
    , vkGetBufferDeviceAddressKHR(scene->vkGetBufferDeviceAddressKHR)
    , vertexCount(0)
    , triangleCount(0)
    , initialized(false)
    , lastBody(body)
{
    tie(vertexBuffer, vertexBufferAddress) = createVertexBuffer(mesh.vertices.size());
    vertexBufferMapping = new VmaMapping<fvec3>(scene->ctx->allocator, vertexBuffer);
    tie(indexBuffer, indexBufferAddress) = createIndexBuffer(mesh.indices.size());
    indexBufferMapping = new VmaMapping<u32>(scene->ctx->allocator, indexBuffer);

    build(commandBuffer, mesh);
}

RenderHardwareOcclusionEntity::~RenderHardwareOcclusionEntity()
{
    vkDestroyAccelerationStructureKHR(scene->ctx->device, accelerationStructure, nullptr);

    destroyBuffer(scene->ctx->allocator, scratchBuffer);
    destroyBuffer(scene->ctx->allocator, backingBuffer);

    delete indexBufferMapping;
    destroyBuffer(scene->ctx->allocator, indexBuffer);
    delete vertexBufferMapping;
    destroyBuffer(scene->ctx->allocator, vertexBuffer);
}

VkDeviceAddress RenderHardwareOcclusionEntity::address() const
{
    return accelerationStructureAddress;
}

bool RenderHardwareOcclusionEntity::needsUpdate(const RenderOcclusionMesh& mesh, const Body& body) const
{
    return body != lastBody;
}

bool RenderHardwareOcclusionEntity::needsReinit(const RenderOcclusionMesh& mesh) const
{
    return mesh.vertices.size() != vertexCount || mesh.indices.size() / 3 != triangleCount;
}

void RenderHardwareOcclusionEntity::update(VkCommandBuffer commandBuffer, const RenderOcclusionMesh& mesh, const Body& body)
{
    build(commandBuffer, mesh);
    lastBody = body;
}

void RenderHardwareOcclusionEntity::build(VkCommandBuffer commandBuffer, const RenderOcclusionMesh& mesh)
{
    VkResult result;

    vertexCount = mesh.vertices.size();
    triangleCount = mesh.indices.size() / 3;

    memcpy(vertexBufferMapping->data, mesh.vertices.data(), mesh.vertices.size() * sizeof(fvec3));
    memcpy(indexBufferMapping->data, mesh.indices.data(), mesh.indices.size() * sizeof(u32));

    VkAccelerationStructureGeometryKHR geometry{};
    geometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    geometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    geometry.geometryType = VK_GEOMETRY_TYPE_TRIANGLES_KHR;
    geometry.geometry.triangles.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_TRIANGLES_DATA_KHR;
    geometry.geometry.triangles.vertexFormat = VK_FORMAT_R32G32B32_SFLOAT;
    geometry.geometry.triangles.vertexData = VkDeviceOrHostAddressConstKHR{
        .deviceAddress = vertexBufferAddress
    };
    geometry.geometry.triangles.maxVertex = vertexCount;
    geometry.geometry.triangles.vertexStride = sizeof(fvec3);
    geometry.geometry.triangles.indexType = VK_INDEX_TYPE_UINT32;
    geometry.geometry.triangles.indexData = VkDeviceOrHostAddressConstKHR{
        .deviceAddress = indexBufferAddress
    };
    geometry.geometry.triangles.transformData.deviceAddress = 0;
    geometry.geometry.triangles.transformData.hostAddress = nullptr;
    
    VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo{};
    buildGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    buildGeometryInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;
    buildGeometryInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR;
    buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    buildGeometryInfo.geometryCount = 1;
    buildGeometryInfo.pGeometries = &geometry;

    VkAccelerationStructureBuildSizesInfoKHR buildSizesInfo{};
    buildSizesInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        scene->ctx->device,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &buildGeometryInfo,
        &triangleCount,
        &buildSizesInfo
    );

    if (scratchBuffer.buffer)
    {
        destroyBuffer(scene->ctx->allocator, scratchBuffer);
    }

    tie(scratchBuffer, scratchBufferAddress) = createScratchBuffer(buildSizesInfo);

    if (!initialized)
    {
        backingBuffer = createAccelerationStructureBuffer(buildSizesInfo);

        VkAccelerationStructureCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
        createInfo.buffer = backingBuffer.buffer;
        createInfo.size = buildSizesInfo.accelerationStructureSize;
        createInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_BOTTOM_LEVEL_KHR;

        result = vkCreateAccelerationStructureKHR(scene->ctx->device, &createInfo, nullptr, &accelerationStructure);

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create ray-tracing bottom-level acceleration structure: " + vulkanError(result));
        }

        buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        buildGeometryInfo.srcAccelerationStructure = nullptr;
        buildGeometryInfo.dstAccelerationStructure = accelerationStructure;
        buildGeometryInfo.scratchData.deviceAddress = scratchBufferAddress;
    }
    else
    {
        buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
        buildGeometryInfo.srcAccelerationStructure = accelerationStructure;
        buildGeometryInfo.dstAccelerationStructure = accelerationStructure;
        buildGeometryInfo.scratchData.deviceAddress = scratchBufferAddress;
    }

    VkAccelerationStructureBuildRangeInfoKHR buildRangeInfo{};
    buildRangeInfo.primitiveCount = triangleCount;
    buildRangeInfo.primitiveOffset = 0;
    buildRangeInfo.firstVertex = 0;
    buildRangeInfo.transformOffset = 0;

    VkAccelerationStructureBuildRangeInfoKHR* buildRangeInfos[] = {
        &buildRangeInfo
    };

    vkCmdBuildAccelerationStructuresKHR(
        commandBuffer,
        1,
        &buildGeometryInfo,
        buildRangeInfos
    );

    VkAccelerationStructureDeviceAddressInfoKHR deviceAddressInfo{};
    deviceAddressInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_DEVICE_ADDRESS_INFO_KHR;
    deviceAddressInfo.accelerationStructure = accelerationStructure;

    accelerationStructureAddress = vkGetAccelerationStructureDeviceAddressKHR(scene->ctx->device, &deviceAddressInfo);

    initialized = true;
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionEntity::createVertexBuffer(u32 vertexCount)
{
    return createBufferAndAddress(
        vertexCount * sizeof(fvec3),
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionEntity::createIndexBuffer(u32 triangleCount)
{
    return createBufferAndAddress(
        triangleCount * 3 * sizeof(u32),
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionEntity::createTransformBuffer()
{
    return createBufferAndAddress(
        sizeof(VkTransformMatrixKHR),
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VmaBuffer RenderHardwareOcclusionEntity::createAccelerationStructureBuffer(
    const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo
) const
{
    return createBuffer(
        scene->ctx->allocator,
        buildSizesInfo.accelerationStructureSize,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY
    );
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionEntity::createScratchBuffer(
    const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo
) const
{
    return createBufferAndAddress(
        buildSizesInfo.buildScratchSize,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY,
        scene->accelerationStructureProperties.minAccelerationStructureScratchOffsetAlignment
    );
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionEntity::createBufferAndAddress(
    size_t size,
    VkBufferUsageFlags bufferUsage,
    VmaMemoryUsage memoryUsage,
    VkDeviceSize alignment
) const
{
    VmaBuffer buffer = createBuffer(
        scene->ctx->allocator,
        size,
        bufferUsage,
        memoryUsage,
        alignment
    );

    VkBufferDeviceAddressInfo deviceAddressInfo{};
    deviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    deviceAddressInfo.buffer = buffer.buffer;

    auto f = vkGetBufferDeviceAddressKHR ? vkGetBufferDeviceAddressKHR : vkGetBufferDeviceAddress;

    VkDeviceAddress address = f(scene->ctx->device, &deviceAddressInfo);

    return { buffer, address };
}
