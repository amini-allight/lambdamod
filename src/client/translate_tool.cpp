/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "translate_tool.hpp"
#include "tools.hpp"
#include "localization.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "units.hpp"

TranslateTool::TranslateTool(
    ViewerModeContext* ctx,
    const mat4& localSpace,
    const function<void(size_t, const vec3&, const mat4&, const mat4&)>& setGlobal,
    const function<mat4(size_t)>& getGlobal,
    const function<mat4(size_t)>& getGlobalWithPromotion,
    const function<size_t()>& count,
    bool soft,
    const Point& point
)
    : EditTool(ctx, localSpace, setGlobal, getGlobal, getGlobalWithPromotion, count, soft)
    , onScreenPoint(point)
    , onScreenOffset(getOnScreenCenter() - point)
{

}

TranslateTool::~TranslateTool()
{

}

string TranslateTool::name() const
{
    return localize("translate-tool-translate");
}

string TranslateTool::extent() const
{
    return toPrettyString(localizeLength(distance())) + " " + lengthSuffix();
}

void TranslateTool::use(const Point& point)
{
    onScreenPoint = point;

    update();
}

void TranslateTool::update()
{
    for (size_t i = 0; i < count(); i++)
    {
        setGlobal(i, globalPivot(i), initialGlobal(i), apply(initialGlobal(i)));
    }
}

void TranslateTool::cancel()
{
    for (size_t i = 0; i < count(); i++)
    {
        setGlobal(i, globalPivot(i), initialGlobal(i), initialGlobal(i));
    }
}

void TranslateTool::end()
{
    // Do nothing
}

optional<vec3> TranslateTool::globalAxis() const
{
    switch (toolAxis)
    {
    default :
    case Edit_Tool_Axis_None : return {};
    case Edit_Tool_Axis_Global_X : return rightDir;
    case Edit_Tool_Axis_Global_Y : return forwardDir;
    case Edit_Tool_Axis_Global_Z : return upDir;
    case Edit_Tool_Axis_Local_X : return localSpace.right();
    case Edit_Tool_Axis_Local_Y : return localSpace.forward();
    case Edit_Tool_Axis_Local_Z : return localSpace.up();
    case Edit_Tool_Axis_Self_X :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).right();
        }

        if (dir.length() == 0)
        {
            return rightDir;
        }
        else
        {
            return dir.normalize();
        }
    }
    case Edit_Tool_Axis_Self_Y :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).forward();
        }

        if (dir.length() == 0)
        {
            return forwardDir;
        }
        else
        {
            return dir.normalize();
        }
    }
    case Edit_Tool_Axis_Self_Z :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).up();
        }

        if (dir.length() == 0)
        {
            return upDir;
        }
        else
        {
            return dir.normalize();
        }
    }
    }
}

f64 TranslateTool::distance() const
{
    if (numericInput)
    {
        return snap(numericInputValue());
    }
    else
    {
        return initialGlobalCenter.distance(newGlobalCenter());
    }
}

vec3 TranslateTool::newGlobalCenter() const
{
    vec3 linePos = ctx->context()->flatViewer()->position();
    vec3 lineDir = ctx->context()->flatViewer()->screenToWorld(onScreenPoint + onScreenOffset);

    vec3 planeNormal = ctx->context()->flatViewer()->rotation().forward();
    vec3 planePos = initialGlobalCenter;

    if (roughly(lineDir.dot(planeNormal), 0.0))
    {
        return initialGlobalCenter;
    }

    f64 t = (planePos - linePos).dot(planeNormal) / lineDir.dot(planeNormal);

    vec3 intersectPos = linePos + lineDir * t;

    if (globalAxis())
    {
        if (numericInput)
        {
            return initialGlobalCenter + *globalAxis() * numericInputValue();
        }
        else
        {
            quaternion axisSpace(*globalAxis(), pickSideDirection(*globalAxis()));

            return initialGlobalCenter + axisSpace.rotate(vec3(0, axisSpace.inverse().rotate(intersectPos - initialGlobalCenter).y, 0));
        }
    }
    else
    {
        return snap(intersectPos);
    }
}

f64 TranslateTool::snap(f64 v) const
{
    if (roughly<f64>(ctx->translationStep(), 0))
    {
        return v;
    }

    return floor(v / ctx->translationStep()) * ctx->translationStep();
}

vec3 TranslateTool::snap(const vec3& v) const
{
    if (!snapping() || roughly<f64>(ctx->translationStep(), 0))
    {
        return v;
    }

    vec3 offset = v - globalPivot();

    return globalPivot() + vec3(
        floor(offset.x / ctx->translationStep()) * ctx->translationStep(),
        floor(offset.y / ctx->translationStep()) * ctx->translationStep(),
        floor(offset.z / ctx->translationStep()) * ctx->translationStep()
    );
}

mat4 TranslateTool::apply(mat4 transform) const
{
    transform.setPosition(newGlobalCenter() + (transform.position() - initialGlobalCenter));

    return transform;
}
