/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "right_clickable_widget.hpp"

#include "line_edit.hpp"
#include "list_view.hpp"

class ItemList : public RightClickableWidget
{
public:
    ItemList(
        InterfaceWidget* parent,
        const function<vector<string>()>& source,
        const function<void(const string&)>& onAdd,
        const function<void(const string&)>& onRemove,
        const function<void(const string&)>& onSelect,
        const function<void(const string&, const string&)>& onRename,
        const function<void(const string&, const string&)>& onClone,
        const function<vector<tuple<string, function<void(const Point&)>>>(i32)>& onRightClick = nullptr
    );

    void select(i32 index);
    void clearSelection();
    i32 activeIndex() const;

    void step() override;

private:
    vector<string> items;
    i32 _activeIndex;

    LineEdit* lineEdit;
    ListView* listView;
    
    function<vector<string>()> source;
    function<void(const string&)> _onAdd;
    function<void(const string&)> onRemove;
    function<void(const string&)> _onSelect;
    function<vector<tuple<string, function<void(const Point&)>>>(i32)> _onRightClick;
    function<void(const string&, const string&)> _onRename;
    function<void(const string&, const string&)> _onClone;

    void updateListView();

    void onSelect(i32 index);

    void openRightClickMenu(i32 index, const Point& pointer) override;

    void onAdd(const string& name);
    void onRename(const string& name, const Point& pointer);
    void onClone(const string& name, const Point& pointer);

    SizeProperties sizeProperties() const override;
};
