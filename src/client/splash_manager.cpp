/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "splash_manager.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "control_context.hpp"

static constexpr u32 splashDuration = 4000; // ms

SplashManager::SplashManager()
    : startTime(currentTime())
    , _splashMode(g_config.splash ? Splash_System : Splash_Server)
{

}

f64 SplashManager::splashOpacity() const
{
    if (!g_config.startupSplash)
    {
        return 0;
    }

    f64 duration = splashDuration;

    f64 progress = min((currentTime() - startTime).count() / duration, 1.0);

    if (progress < 0.75)
    {
        return 1;
    }
    else
    {
        return 1 - ((progress - 0.75) / 0.25);
    }
}

f64 SplashManager::splashContentOpacity() const
{
    if (!g_config.startupSplash)
    {
        return 0;
    }

    f64 duration = splashDuration;

    f64 progress = min((currentTime() - startTime).count() / duration, 1.0);

    if (progress < 0.25)
    {
        return progress / 0.25;
    }
    else if (progress < 0.5)
    {
        return 1;
    }
    else if (progress < 0.75)
    {
        return 1 - ((progress - 0.5) / 0.25);
    }
    else
    {
        return 0;
    }
}

SplashMode SplashManager::splashMode() const
{
    return _splashMode;
}

void SplashManager::dismissSplash()
{
    switch (_splashMode)
    {
    default :
    case Splash_None :
        _splashMode = Splash_None;
        break;
    case Splash_System :
        _splashMode = Splash_Server;
        break;
    case Splash_Server :
        _splashMode = Splash_None;
        break;
    }
}
