/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_nodes.hpp"
#include "script_tools.hpp"
#include "script_node.hpp"
#include "interface_constants.hpp"

ScriptNodeOutput::ScriptNodeOutput()
{

}

ScriptNodeOutput::ScriptNodeOutput(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const optional<Value>& value
)
{
    if (value)
    {
        this->value = make_shared<ScriptNode>(
            ctx,
            id,
            self.position() + ivec2(-UIScale::scriptNodeDefaultHorizontalSpacing(), 0),
            *value,
            false
        );
    }
}

bool ScriptNodeOutput::operator==(const ScriptNodeOutput& rhs) const
{
    return value == rhs.value;
}

bool ScriptNodeOutput::operator!=(const ScriptNodeOutput& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeOutput::operator>(const ScriptNodeOutput& rhs) const
{
    return value > rhs.value;
}

bool ScriptNodeOutput::operator<(const ScriptNodeOutput& rhs) const
{
    return value < rhs.value;
}

bool ScriptNodeOutput::operator>=(const ScriptNodeOutput& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeOutput::operator<=(const ScriptNodeOutput& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeOutput::operator<=>(const ScriptNodeOutput& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeExecution::ScriptNodeExecution()
{

}

ScriptNodeExecution::ScriptNodeExecution(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeExecution()
{
    name = value.l.front().s;

    i32 y = 0;

    for (size_t i = 1; i < value.l.size(); i++)
    {
        inputs.push_back(make_shared<ScriptNode>(
            ctx,
            id,
            self.position() + ivec2(-UIScale::scriptNodeDefaultHorizontalSpacing(), y),
            value.l.at(i),
            quote
        ));
        y += UIScale::scriptNodeDefaultVerticalSpacing() + inputs.back()->size(ctx).y;
    }
}

bool ScriptNodeExecution::operator==(const ScriptNodeExecution& rhs) const
{
    return name == rhs.name && inputs == rhs.inputs;
}

bool ScriptNodeExecution::operator!=(const ScriptNodeExecution& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeExecution::operator>(const ScriptNodeExecution& rhs) const
{
    if (name > rhs.name)
    {
        return true;
    }
    else if (name < rhs.name)
    {
        return false;
    }
    else
    {
        if (inputs > rhs.inputs)
        {
            return true;
        }
        else if (inputs < rhs.inputs)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool ScriptNodeExecution::operator<(const ScriptNodeExecution& rhs) const
{
    if (name < rhs.name)
    {
        return true;
    }
    else if (name > rhs.name)
    {
        return false;
    }
    else
    {
        if (inputs < rhs.inputs)
        {
            return true;
        }
        else if (inputs > rhs.inputs)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool ScriptNodeExecution::operator>=(const ScriptNodeExecution& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeExecution::operator<=(const ScriptNodeExecution& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeExecution::operator<=>(const ScriptNodeExecution& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeVoid::ScriptNodeVoid()
{

}

ScriptNodeVoid::ScriptNodeVoid(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeVoid()
{

}

bool ScriptNodeVoid::operator==(const ScriptNodeVoid& rhs) const
{
    return true;
}

bool ScriptNodeVoid::operator!=(const ScriptNodeVoid& rhs) const
{
    return false;
}

bool ScriptNodeVoid::operator>(const ScriptNodeVoid& rhs) const
{
    return false;
}

bool ScriptNodeVoid::operator<(const ScriptNodeVoid& rhs) const
{
    return false;
}

bool ScriptNodeVoid::operator>=(const ScriptNodeVoid& rhs) const
{
    return true;
}

bool ScriptNodeVoid::operator<=(const ScriptNodeVoid& rhs) const
{
    return true;
}

strong_ordering ScriptNodeVoid::operator<=>(const ScriptNodeVoid& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeSymbol::ScriptNodeSymbol()
{

}

ScriptNodeSymbol::ScriptNodeSymbol(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeSymbol()
{
    this->value = value.s;
}

bool ScriptNodeSymbol::operator==(const ScriptNodeSymbol& rhs) const
{
    return value == rhs.value;
}

bool ScriptNodeSymbol::operator!=(const ScriptNodeSymbol& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeSymbol::operator>(const ScriptNodeSymbol& rhs) const
{
    return value > rhs.value;
}

bool ScriptNodeSymbol::operator<(const ScriptNodeSymbol& rhs) const
{
    return value < rhs.value;
}

bool ScriptNodeSymbol::operator>=(const ScriptNodeSymbol& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeSymbol::operator<=(const ScriptNodeSymbol& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeSymbol::operator<=>(const ScriptNodeSymbol& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeInteger::ScriptNodeInteger()
{
    value = 0;
}

ScriptNodeInteger::ScriptNodeInteger(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeInteger()
{
    this->value = value.i;
}

bool ScriptNodeInteger::operator==(const ScriptNodeInteger& rhs) const
{
    return value == rhs.value;
}

bool ScriptNodeInteger::operator!=(const ScriptNodeInteger& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeInteger::operator>(const ScriptNodeInteger& rhs) const
{
    return value > rhs.value;
}

bool ScriptNodeInteger::operator<(const ScriptNodeInteger& rhs) const
{
    return value < rhs.value;
}

bool ScriptNodeInteger::operator>=(const ScriptNodeInteger& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeInteger::operator<=(const ScriptNodeInteger& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeInteger::operator<=>(const ScriptNodeInteger& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeFloat::ScriptNodeFloat()
{
    value = 0;
}

ScriptNodeFloat::ScriptNodeFloat(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeFloat()
{
    this->value = value.f;
}

bool ScriptNodeFloat::operator==(const ScriptNodeFloat& rhs) const
{
    return value == rhs.value;
}

bool ScriptNodeFloat::operator!=(const ScriptNodeFloat& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeFloat::operator>(const ScriptNodeFloat& rhs) const
{
    return value > rhs.value;
}

bool ScriptNodeFloat::operator<(const ScriptNodeFloat& rhs) const
{
    return value < rhs.value;
}

bool ScriptNodeFloat::operator>=(const ScriptNodeFloat& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeFloat::operator<=(const ScriptNodeFloat& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeFloat::operator<=>(const ScriptNodeFloat& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeString::ScriptNodeString()
{

}

ScriptNodeString::ScriptNodeString(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeString()
{
    this->value = value.s;
}

bool ScriptNodeString::operator==(const ScriptNodeString& rhs) const
{
    return value == rhs.value;
}

bool ScriptNodeString::operator!=(const ScriptNodeString& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeString::operator>(const ScriptNodeString& rhs) const
{
    return value > rhs.value;
}

bool ScriptNodeString::operator<(const ScriptNodeString& rhs) const
{
    return value < rhs.value;
}

bool ScriptNodeString::operator>=(const ScriptNodeString& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeString::operator<=(const ScriptNodeString& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeString::operator<=>(const ScriptNodeString& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeList::ScriptNodeList()
{

}

ScriptNodeList::ScriptNodeList(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeList()
{
    i32 y = 0;

    for (size_t i = 0; i < value.l.size(); i++)
    {
        values.push_back(make_shared<ScriptNode>(ctx, id, self.position() + ivec2(-UIScale::scriptNodeDefaultHorizontalSpacing(), y), value.l.at(i), quote));
        y += UIScale::scriptNodeDefaultVerticalSpacing() + values.back()->size(ctx).y;
    }
}

bool ScriptNodeList::operator==(const ScriptNodeList& rhs) const
{
    return values == rhs.values;
}

bool ScriptNodeList::operator!=(const ScriptNodeList& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeList::operator>(const ScriptNodeList& rhs) const
{
    return values > rhs.values;
}

bool ScriptNodeList::operator<(const ScriptNodeList& rhs) const
{
    return values < rhs.values;
}

bool ScriptNodeList::operator>=(const ScriptNodeList& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeList::operator<=(const ScriptNodeList& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeList::operator<=>(const ScriptNodeList& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeLambda::ScriptNodeLambda()
{

}

ScriptNodeLambda::ScriptNodeLambda(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeLambda()
{
    lambda = value.lambda;
}

bool ScriptNodeLambda::operator==(const ScriptNodeLambda& rhs) const
{
    return lambda == rhs.lambda;
}

bool ScriptNodeLambda::operator!=(const ScriptNodeLambda& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeLambda::operator>(const ScriptNodeLambda& rhs) const
{
    return lambda > rhs.lambda;
}

bool ScriptNodeLambda::operator<(const ScriptNodeLambda& rhs) const
{
    return lambda < rhs.lambda;
}

bool ScriptNodeLambda::operator>=(const ScriptNodeLambda& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNodeLambda::operator<=(const ScriptNodeLambda& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNodeLambda::operator<=>(const ScriptNodeLambda& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

ScriptNodeHandle::ScriptNodeHandle()
{
    id = EntityID();
}

ScriptNodeHandle::ScriptNodeHandle(
    const ScriptContext* ctx,
    ScriptNodeID* id,
    const ScriptNode& self,
    const Value& value,
    bool quote
)
    : ScriptNodeHandle()
{
    this->id = value.id;
}

bool ScriptNodeHandle::operator==(const ScriptNodeHandle& rhs) const
{
    return id == rhs.id;
}

bool ScriptNodeHandle::operator!=(const ScriptNodeHandle& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNodeHandle::operator>(const ScriptNodeHandle& rhs) const
{
    return id > rhs.id;
}

bool ScriptNodeHandle::operator<(const ScriptNodeHandle& rhs) const
{
    return id < rhs.id;
}

bool ScriptNodeHandle::operator>=(const ScriptNodeHandle& rhs) const
{
    return id >= rhs.id;
}

bool ScriptNodeHandle::operator<=(const ScriptNodeHandle& rhs) const
{
    return id <= rhs.id;
}

strong_ordering ScriptNodeHandle::operator<=>(const ScriptNodeHandle& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}
