/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "text_style.hpp"
#include "text.hpp"
#include "gamepad_pointer_manager.hpp"
#include "vr_capable_panel.hpp"

class PanelListView;

#ifdef LMOD_VR
class TextPanel : public VRCapablePanel
#else
class TextPanel : public InterfaceWidget
#endif
{
public:
    TextPanel(InterfaceView* view);

    void step() override;
    void draw(const DrawContext& ctx) const override;

    bool shouldDraw() const override;

private:
    GamepadPointerManager pointerManager;

    TextID currentID;

    PanelListView* listView;

    void addTimeoutChildren(const TextTimeoutRequest& request);
    void addUnaryChildren(const TextUnaryRequest& request);
    void addBinaryChildren(const TextBinaryRequest& request);
    void addOptionsChildren(const TextOptionsRequest& request);
    void addChooseChildren(const TextChooseRequest& request);
    void addAssignValuesChildren(const TextAssignValuesRequest& request);
    void addSpendPointsChildren(const TextSpendPointsRequest& request);
    void addAssignPointsChildren(const TextAssignPointsRequest& request);
    void addVoteChildren(const TextVoteRequest& request);
    void addChooseMajorChildren(const TextChooseMajorRequest& request);

    void clearChildren();

    void onOK();
    void onNo();
    void onYes();
    void onPickOption(u64 choice);

    void onChoose();
    void onAssignValues();
    void onSpendPoints();
    void onAssignPoints();
    void onVote();
    void onChooseMajor(u64 choice);

    vector<string> splitLines(const string& text) const;
    i32 lineWidth() const;

    bool chosenSource(const string& name);
    void onChoose(const string& name, bool state);
    string chooseInstructionSource() const;

    vector<string> valuesSource();
    size_t valueSource(const string& key);
    void onAssignValue(const string& key, const string& value);
    string valueInstructionSource() const;
    string remainingValuesSource() const;

    bool purchasedSource(const string& name);
    void onPurchase(const string& name, bool state);
    string purchaseInstructionSource() const;

    string pointsSource(const string& key);
    void onAddPoint(const string& key);
    void onRemovePoint(const string& key);
    string pointInstructionSource() const;

    bool votedSource(u64 index);
    void onVote(u64 index, bool state);
    string voteInstructionSource() const;

    f64 timeoutFractionSource() const;
    f64 voteFractionSource() const;

    optional<Point> toLocal(const Point& pointer) const;
#ifdef LMOD_VR
    optional<Point> toLocal(const vec3& position, const quaternion& rotation) const;
#endif

    SizeProperties sizeProperties() const override;
};
