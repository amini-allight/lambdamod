/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_interface.hpp"

#include <vulkan/vulkan.h>

class WindowInterface
{
public:
    WindowInterface();
    WindowInterface(const WindowInterface& rhs) = delete;
    WindowInterface(WindowInterface&& rhs) = delete;
    virtual ~WindowInterface() = 0;

    WindowInterface& operator=(const WindowInterface& rhs) = delete;
    WindowInterface& operator=(WindowInterface&& rhs) = delete;

    virtual InputInterface* input() const = 0;

    virtual void step() = 0;

    virtual void lockCursor(bool state) = 0;
    virtual void showCursor(bool state) = 0;

    virtual void setSize(u32 width, u32 height) = 0;
    virtual void setFullscreen(bool state) = 0;
    virtual bool fullscreen() const = 0;
    virtual bool focused() const = 0;

    virtual set<string> vulkanExtensions() const = 0;
    virtual VkSurfaceKHR createVulkanSurface(VkInstance instance) const = 0;
};
