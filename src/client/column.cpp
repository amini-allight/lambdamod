/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "column.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

Column::Column(
    InterfaceWidget* parent,
    const optional<Color>& color,
    const function<void(const Point&)>& onRightClick
)
    : Layout(parent)
    , color(color)
    , onRightClick(onRightClick)
{
    START_WIDGET_SCOPE("column")
        START_SCOPE("interactive", this->onRightClick != nullptr)
            WIDGET_SLOT("open-context-menu", openContextMenu)
        END_SCOPE
    END_SCOPE
}

void Column::move(const Point& position)
{
    this->position(position);

    i32 y = 0;

    for (InterfaceWidget* child : children())
    {
        child->move(position + Point(0, y));

        Size size = child->size();

        y += size.h;
    }
}

static i32 pickHorizontalSize(const SizeProperties& props, const Size& parentSize)
{
    switch (props.horizontal)
    {
    default :
    case Scaling_Fixed :
        return props.w;
    case Scaling_Fraction :
        return props.w * parentSize.w;
    case Scaling_Fill :
        return parentSize.w;
    }
}

void Column::resize(const Size& size)
{
    this->size(size);

    map<InterfaceWidget*, Size> sizes;

    i32 maxW = 0;
    i32 total = size.h;
    i32 fillCount = 0;

    // fixed or fraction
    for (InterfaceWidget* child : children())
    {
        SizeProperties props = sizePropertiesOf(child);

        Size size;

        switch (props.vertical)
        {
        case Scaling_Fixed :
            if (props.h != 0)
            {
                size.h = props.h;
            }
            else
            {
                continue;
            }
            break;
        case Scaling_Fraction :
            size.h = props.h * this->size().h;
            break;
        case Scaling_Fill :
            fillCount++;
            continue;
        }

        size.w = pickHorizontalSize(props, this->size());

        if (size.w > maxW)
        {
            maxW = size.w;
        }

        total -= size.h;
        sizes.insert({ child, size });
    }

    for (InterfaceWidget* child : children())
    {
        SizeProperties props = sizePropertiesOf(child);

        Size size;

        switch (props.vertical)
        {
        default :
            continue;
        case Scaling_Fixed :
            if (props.h == 0)
            {
                child->resize(Size(this->size().w, total));

                size.h = child->size().h;
            }
            else
            {
                continue;
            }
            break;
        }

        size.w = pickHorizontalSize(props, this->size());

        if (size.w > maxW)
        {
            maxW = size.w;
        }

        total -= size.h;
        sizes.insert({ child, size });
    }

    // fill
    if (fillCount != 0)
    {
        i32 fraction = total / fillCount;
        i32 remainder = total - (fraction * fillCount);

        i32 i = 0;
        for (InterfaceWidget* child : children())
        {
            SizeProperties props = sizePropertiesOf(child);

            Size size;

            switch (props.vertical)
            {
            case Scaling_Fill :
                size.h = fraction + (i + 1 == fillCount ? remainder : 0);
                break;
            default :
                continue;
            }

            i++;

            size.w = pickHorizontalSize(props, this->size());

            if (size.w > maxW)
            {
                maxW = size.w;
            }

            sizes.insert({ child, size });
        }
    }

    // set
    i32 y = 0;

    for (InterfaceWidget* child : children())
    {
        Size size = sizes[child];

        child->move(this->position() + Point(0, y));
        child->resize(size);

        y += size.h;
    }

    this->size(Size(maxW, size.h));
}

void Column::draw(const DrawContext& ctx) const
{
    if (color)
    {
        drawSolid(
            ctx,
            this,
            *color
        );
    }

    InterfaceWidget::draw(ctx);
}

void Column::openContextMenu(const Input& input)
{
    onRightClick(pointer);
}

SizeProperties Column::sizeProperties() const
{
    return {
        0, 0,
        Scaling_Fixed, Scaling_Fill
    };
}
