/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_visualizer.hpp"
#include "control_context.hpp"
#include "controller.hpp"
#include "user.hpp"

UserVisualizer::UserVisualizer(ControlContext* ctx)
    : ctx(ctx)
{

}

UserVisualizer::~UserVisualizer()
{
    for (const auto& [ id, decorations ] : users)
    {
        delete decorations;
    }
}

void UserVisualizer::setShown(bool state)
{
    for (const auto& [ id, decorations ] : users)
    {
        decorations->setShown(state);
    }
}

void UserVisualizer::add(UserID id)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it != users.end())
    {
        return;
    }

    users.insert({ id, new UserVisualizerDecorations(ctx, ctx->controller()->game().getUser(id)) });
}

void UserVisualizer::remove(UserID id)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    delete it->second;
    users.erase(it);
}

void UserVisualizer::join(UserID id)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->join();
}

void UserVisualizer::leave(UserID id)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->leave();
}

void UserVisualizer::setWorld(UserID id, const string& name)
{
    if (id == ctx->controller()->selfID())
    {
        for (const auto& [ id, decorations ] : users)
        {
            decorations->setShownWorld(name);
        }
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->setWorld(name);
}

void UserVisualizer::moveViewer(UserID id, const mat4& transform)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->moveViewer(transform);
}

void UserVisualizer::setViewerAngle(UserID id, f64 angle)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->setViewerAngle(angle);
}

void UserVisualizer::setViewerAspect(UserID id, f64 aspect)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->setViewerAspect(aspect);
}

void UserVisualizer::moveRoom(UserID id, const mat4& transform)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->moveRoom(transform);
}

void UserVisualizer::setRoomBounds(UserID id, const vec2& bounds)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->setRoomBounds(bounds);
}

void UserVisualizer::moveDevice(UserID id, VRDevice device, const mat4& transform)
{
    if (id == ctx->controller()->selfID())
    {
        return;
    }

    auto it = users.find(id);

    if (it == users.end())
    {
        return;
    }

    UserVisualizerDecorations* decorations = it->second;

    decorations->moveDevice(device, transform);
}
