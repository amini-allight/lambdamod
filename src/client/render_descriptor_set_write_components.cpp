/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_descriptor_set_write_components.hpp"

void uniformBufferDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkBuffer buffer,
    VkDescriptorBufferInfo* info,
    VkWriteDescriptorSet* write
)
{
    info->buffer = buffer;
    info->offset = 0;
    info->range = VK_WHOLE_SIZE;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = 1;
    write->descriptorType = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    write->pBufferInfo = info;
}

void storageBufferDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkBuffer buffer,
    VkDescriptorBufferInfo* info,
    VkWriteDescriptorSet* write
)
{
    info->buffer = buffer;
    info->offset = 0;
    info->range = VK_WHOLE_SIZE;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = 1;
    write->descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_BUFFER;
    write->pBufferInfo = info;
}

void combinedSamplerDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkImageView imageView,
    VkSampler sampler,
    VkDescriptorImageInfo* info,
    VkWriteDescriptorSet* write
)
{
    info->sampler = sampler;
    info->imageView = imageView;
    info->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = 1;
    write->descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write->pImageInfo = info;
}

void combinedSamplerDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    const vector<VkImageView>& imageViews,
    VkSampler sampler,
    vector<VkDescriptorImageInfo>* info,
    VkWriteDescriptorSet* write
)
{
    *info = vector<VkDescriptorImageInfo>(imageViews.size());

    for (size_t i = 0; i < imageViews.size(); i++)
    {
        info->at(i).sampler = sampler;
        info->at(i).imageView = imageViews[i];
        info->at(i).imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = imageViews.size();
    write->descriptorType = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    write->pImageInfo = info->data();
}

void sampledImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkImageView imageView,
    VkDescriptorImageInfo* info,
    VkWriteDescriptorSet* write
)
{
    info->sampler = nullptr;
    info->imageView = imageView;
    info->imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = 1;
    write->descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    write->pImageInfo = info;
}

void sampledImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    const vector<VkImageView>& imageViews,
    vector<VkDescriptorImageInfo>* info,
    VkWriteDescriptorSet* write
)
{
    *info = vector<VkDescriptorImageInfo>(imageViews.size());

    for (size_t i = 0; i < imageViews.size(); i++)
    {
        info->at(i).sampler = nullptr;
        info->at(i).imageView = imageViews[i];
        info->at(i).imageLayout = VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL;
    }

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = imageViews.size();
    write->descriptorType = VK_DESCRIPTOR_TYPE_SAMPLED_IMAGE;
    write->pImageInfo = info->data();
}

void storageImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkImageView imageView,
    VkDescriptorImageInfo* info,
    VkWriteDescriptorSet* write
)
{
    info->sampler = nullptr;
    info->imageView = imageView;
    info->imageLayout = VK_IMAGE_LAYOUT_GENERAL;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = 1;
    write->descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    write->pImageInfo = info;
}

void storageImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    const vector<VkImageView>& imageViews,
    vector<VkDescriptorImageInfo>* info,
    VkWriteDescriptorSet* write
)
{
    *info = vector<VkDescriptorImageInfo>(imageViews.size());

    for (size_t i = 0; i < imageViews.size(); i++)
    {
        info->at(i).sampler = nullptr;
        info->at(i).imageView = imageViews[i];
        info->at(i).imageLayout = VK_IMAGE_LAYOUT_GENERAL;
    }

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = imageViews.size();
    write->descriptorType = VK_DESCRIPTOR_TYPE_STORAGE_IMAGE;
    write->pImageInfo = info->data();
}

void accelerationStructureDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkAccelerationStructureKHR* accelerationStructure,
    VkWriteDescriptorSetAccelerationStructureKHR* info,
    VkWriteDescriptorSet* write
)
{
    info->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
    info->accelerationStructureCount = 1;
    info->pAccelerationStructures = accelerationStructure;

    write->sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
    write->pNext = &info;
    write->dstSet = descriptorSet;
    write->dstBinding = index;
    write->dstArrayElement = 0;
    write->descriptorCount = 1;
    write->descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
}
