/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_orthographic_grid.hpp"
#include "theme.hpp"
#include "render_tools.hpp"
#include "tools.hpp"

static f32 pickSpacing(f32 viewWidth)
{
    const f32 idealDivisions = 128;
    const f32 start = 0.001;
    const f32 end = 100'000.0;

    f32 closest = start;
    f32 closestScore = numeric_limits<f32>::max();

    f32 i = start;
    while (i <= end)
    {
        f32 diff = abs(idealDivisions - (viewWidth / i));

        if (diff < closestScore)
        {
            closest = i;
            closestScore = diff;
        }

        i *= 10;
    }

    return closest;
}

static vec3 nearestPoint(const vec3& origin, const vec3& direction, const vec3& point)
{
    return origin + direction * (point - origin).dot(direction);
}

static f64 pickHorizontal(const quaternion& rotation, const vec3& position)
{
    return rotation.right().dot(position);
}

static f64 pickVertical(const quaternion& rotation, const vec3& position)
{
    return rotation.up().dot(position);
}

struct RenderOrthographicGridParameters
{
    RenderOrthographicGridParameters(const vec3& position, const quaternion& rotation, f64 viewWidth, f64 viewHeight)
    {
        horizontal = rightDir;
        vertical = upDir;
        spacing = pickSpacing(viewWidth);
        superSpacing = spacing * 10;

        f64 horizontalPosition = pickHorizontal(rotation, position);
        f64 verticalPosition = pickVertical(rotation, position);

        f64 horizontalOffset = horizontalPosition - (floor(horizontalPosition / superSpacing) * superSpacing);
        f64 verticalOffset = verticalPosition - (floor(verticalPosition / superSpacing) * superSpacing);

        vec3 offset = horizontalOffset * horizontal + verticalOffset * vertical;

        start = -((horizontal * (viewWidth / 2)) + (vertical * (viewHeight / 2)));
        start = floor(start / superSpacing) * superSpacing;
        start -= offset;

        end = +((horizontal * (viewWidth / 2)) + (vertical * (viewHeight / 2)));
        end = (ceil(end / superSpacing) + 2) * superSpacing;
        end -= offset;

        gridWidth = start.distance(nearestPoint(start, horizontal, end));
        gridHeight = start.distance(nearestPoint(start, vertical, end));

        horizontalDivisions = ceil(gridWidth / spacing);
        verticalDivisions = ceil(gridHeight / spacing);
        vertexCount = horizontalDivisions * 2 + verticalDivisions * 2;
    }

    vec3 horizontal;
    vec3 vertical;
    f64 spacing;
    f64 superSpacing;

    vec3 start;
    vec3 end;

    f64 gridWidth;
    f64 gridHeight;

    size_t horizontalDivisions;
    size_t verticalDivisions;
    size_t vertexCount;
};

RenderOrthographicGrid::RenderOrthographicGrid(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    f64 viewWidth,
    f64 viewHeight
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->orthographicGrid.pipelineLayout, pipelineStore->orthographicGrid.pipeline)
    , position(position)
    , rotation(rotation)
    , viewWidth(viewWidth)
    , viewHeight(viewHeight)
{
    transform = { position, rotation, vec3(1) };

    RenderOrthographicGridParameters params(position, rotation, viewWidth, viewHeight);

    vertices = createVertexBuffer(params.vertexCount * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData;
    vertexData.reserve(params.vertexCount);

    for (size_t h = 0; h < params.horizontalDivisions; h++)
    {
        f64 hPos = h * params.spacing;

        vec3 color;

        if (h % 10 == 0)
        {
            color = theme.gridColor.toVec3();
        }
        else
        {
            color = theme.gridSecondaryColor.toVec3();
        }

        vec3 pos = params.start + params.horizontal * hPos;

        vertexData.push_back({ pos, color });
        vertexData.push_back({ pos + params.vertical * ((params.verticalDivisions - 1) * params.spacing), color });
    }

    for (size_t v = 0; v < params.verticalDivisions; v++)
    {
        f64 vPos = v * params.spacing;

        vec3 color;

        if (v % 10 == 0)
        {
            color = theme.gridColor.toVec3();
        }
        else
        {
            color = theme.gridSecondaryColor.toVec3();
        }

        vec3 pos = params.start + params.vertical * vPos;

        vertexData.push_back({ pos, color });
        vertexData.push_back({ pos + params.horizontal * ((params.horizontalDivisions - 1) * params.spacing), color });
    }

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderOrthographicGrid::~RenderOrthographicGrid()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderOrthographicGrid::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    if (angleBetween(rotation.forward(), forwardDir) > radians(1) &&
        angleBetween(rotation.forward(), backDir) > radians(1) &&
        angleBetween(rotation.forward(), upDir) > radians(1) &&
        angleBetween(rotation.forward(), downDir) > radians(1) &&
        angleBetween(rotation.forward(), rightDir) > radians(1) &&
        angleBetween(rotation.forward(), leftDir) > radians(1)
    )
    {
        return;
    }

    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    RenderOrthographicGridParameters params(position, rotation, viewWidth, viewHeight);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        params.vertexCount
    );
}

VmaBuffer RenderOrthographicGrid::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4));
}
