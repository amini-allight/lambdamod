/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_interface.hpp"
#include "render_types.hpp"
#include "render_constants.hpp"
#include "render_swapchain_elements.hpp"
#include "render_entity_body.hpp"
#include "render_entity_component.hpp"
#include "render_pipeline_store.hpp"
#include "render_transparent_draw.hpp"
#include "body.hpp"

typedef tuple<EntityID, set<BodyPartID>> BodyPartMask;

#pragma pack(1)
struct EntityUniformGPU
{
    fmat4 model[eyeCount];
    LightGPU lights[maxLightCount];
    i32 lightCount;
    f32 fogDistance;
    f32 timer;
};
#pragma pack()

class RenderEntity
{
public:
    RenderEntity(
        RenderInterface* render,

        const RenderPipelineStore* pipelineStore,
        const Entity* entity,

        const BodyPartMask& bodyPartMask,
        bool rayTracing
    );
    RenderEntity(const RenderEntity& rhs) = delete;
    RenderEntity(RenderEntity&& rhs) = delete;
    virtual ~RenderEntity();

    RenderEntity& operator=(const RenderEntity& rhs) = delete;
    RenderEntity& operator=(RenderEntity&& rhs) = delete;

    void work() const;
    virtual void update(const Entity* entity, const BodyPartMask& bodyPartMask) = 0;

    virtual void add(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask) = 0;
    virtual void remove(EntityID parentID, EntityID id) = 0;
    virtual void update(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask) = 0;

    const vector<RenderLight>& lights() const;
    const vector<RenderOcclusionPrimitive>& occlusionPrimitives() const;
    const RenderOcclusionMesh& occlusionMesh() const;
    const Body& occlusionMeshSource() const;

    mat4 cameraLocalTransform(const vec3& cameraPosition) const;

protected:
    RenderInterface* render;
    VkSampler sampler;

    const RenderPipelineStore* pipelineStore;

    EntityID id;
    mat4 transform;
    bool visible;
    Body lastBody;
    vector<RenderLight> _lights;
    vector<RenderOcclusionPrimitive> _occlusionPrimitives;
    RenderOcclusionMesh _occlusionMesh;

    RenderEntityBody* body;
    mutable vector<tuple<RenderEntityBody*, u32, u32>> bodiesToDelete;
    mutable vector<tuple<RenderEntityComponent*, u32, u32>> componentsToDelete;

    void setBodySideEffects(const BodyRenderData& renderData, bool rayTracing);
    void updateBodySideEffects(const mat4& transform);

    void fillCommandBuffer(
        const MainSwapchainElement* swapchainElement,
        const vector<tuple<BodyRenderMaterialType, VkDescriptorSet>>& descriptorSets,
        deque<RenderTransparentDraw>* transparentDraws
    ) const;

private:
    void fillCommandBuffer(
        const MainSwapchainElement* swapchainElement,
        VkDescriptorSet descriptorSet,
        const RenderEntityBodyMaterial* material,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    ) const;
};
