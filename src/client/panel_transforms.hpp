/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "tools.hpp"
#include "render_constants.hpp"

static constexpr f64 textDistance = 5;
static constexpr f64 textOffset = -2;

#ifdef LMOD_VR
static constexpr f64 vrTextTitlecardDistance = 2;

static constexpr f64 vrTextDistance = 0.45;
static constexpr f64 vrTextOffset = -1;

static constexpr f64 hudOffset = -0.25;
static constexpr f64 hudDistance = 0;

static constexpr f64 keyboardOffset = 0.2;
#endif

template<typename T>
mat4_t<T> createFlatTextTitlecardTransform(
    const vec3_t<T>& position,
    const quaternion_t<T>& rotation,
    const vec3_t<T>& up
)
{
    vec3_t<T> side = rotation.forward().cross(up).normalize();
    vec3_t<T> forward = up.cross(side).normalize();

    vec3_t<T> textTitlecardPos = position + (forward * textDistance);
    quaternion_t<T> textTitlecardRot = rotation * quaternion_t<T>(vec3_t<T>(rightDir), (T)radians(90));
    vec3_t<T> textTitlecardSca = vec3_t<T>(1, 0.5, 0.5);

    return { textTitlecardPos, textTitlecardRot, textTitlecardSca };
}

template<typename T>
mat4_t<T> createFlatTextTransform(
    const vec3_t<T>& position,
    const quaternion_t<T>& rotation,
    const vec3_t<T>& up,
    T height
)
{
    // Not sure why this needs to be 8, 4 seems like it makes more sense
    T pixelSize = 8.0 / textHeight;
    T pixelAngle = defaultCameraAngle / height;

    T distance = (pixelSize / tan(pixelAngle / 2)) / 2;

    vec3_t<T> side = rotation.forward().cross(up).normalize();
    vec3_t<T> forward = up.cross(side).normalize();

    vec3_t<T> textPos = position + (forward * distance) + (up * textOffset);
    quaternion_t<T> textRot = rotation * quaternion_t<T>(vec3_t<T>(rightDir), (T)radians(90));
    vec3_t<T> textSca = vec3_t<T>(1, 2, 1);

    return { textPos, textRot, textSca };
}

#ifdef LMOD_VR
template<typename T>
mat4_t<T> createVRTextTitlecardTransform(const vec3_t<T>& position, const vec3_t<T>& direction)
{
    vec3_t<T> textTitlecardPos = position + (direction * vrTextTitlecardDistance);
    quaternion_t<T> textTitlecardRot = quaternion_t<T>(upDir, handedAngleBetween<T>(forwardDir.toVec2(), direction.toVec2())) * quaternion_t<T>(rightDir, radians(90));
    vec3_t<T> textTitlecardSca = vec3_t<T>(0.5, 0.25, 0.25);

    return { textTitlecardPos, textTitlecardRot, textTitlecardSca };
}

template<typename T>
mat4_t<T> createVRTextTransform(const vec3_t<T>& position, const vec3_t<T>& direction)
{
    quaternion_t<T> adjust = quaternion_t<T>(direction.cross(upDir).normalize(), radians(45));

    vec3_t<T> textPos = position + (direction * vrTextDistance) + (upDir * vrTextOffset);
    quaternion_t<T> textRot = adjust * quaternion_t<T>(direction, upDir);
    vec3_t<T> textSca = vec3_t<T>(0.5, 1, 0.5);

    return { textPos, textRot, textSca };
}

template<typename T>
mat4_t<T> createVRHUDTransform(
    const vec3_t<T>& position,
    T width,
    T height,
    T angle
)
{
    vec3_t<T> hudPos = position + (forwardDir * hudDistance) + (upDir * hudOffset);
    quaternion_t<T> hudRot = quaternion_t<T>(rightDir, radians(90));
    vec3_t<T> hudSca = vec3_t<T>(1, 1, 1);

    return { hudPos, hudRot, hudSca };
}

template<typename T>
mat4_t<T> createVRKeyboardTransform(const vec3_t<T>& position, const quaternion_t<T>& rotation)
{
    quaternion_t<T> adjust(rotation.right(), radians(90));

    vec3_t<T> keyboardPos = position + (rotation.up() * keyboardOffset);
    quaternion_t<T> keyboardRot = adjust * rotation;
    vec3_t<T> keyboardSca = vec3_t<T>(0.2, 0.1, 0.2);

    return { keyboardPos, keyboardRot, keyboardSca };
}
#endif
