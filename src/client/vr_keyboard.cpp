/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_keyboard.hpp"
#include "tools.hpp"
#include "control_context.hpp"
#include "interface_draw.hpp"
#include "panel_transforms.hpp"
#include "vr_panel_input_scope.hpp"
#include "virtual_keyboard.hpp"

VRKeyboard::VRKeyboard(InterfaceView* view)
    : VRCapablePanel(view)
{
    START_VR_PANEL_SCOPE(context()->input()->getScope("menu"), "vr-keyboard")

    END_SCOPE

    new VirtualKeyboard(this);
}

void VRKeyboard::moveKeyboard(const vec3& position, const quaternion& rotation)
{
    this->position = position;
    this->rotation = rotation;
}

void VRKeyboard::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

    drawPointer(ctx, this, leftPointer);
    drawPointer(ctx, this, rightPointer);
}

bool VRKeyboard::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && context()->controller()->standby();
}

optional<Point> VRKeyboard::toLocal(const vec3& position, const quaternion& rotation) const
{
    if (!shouldDraw())
    {
        return {};
    }

    mat4 self = createVRKeyboardTransform(this->position, this->rotation);

    mat4 ray = {
        context()->controller()->vr()->worldToRoom(position),
        context()->controller()->vr()->worldToRoom(rotation),
        vec3(1)
    };

    mat4 toLocal = self.inverse();

    vec3 localStart = (toLocal * vec4(ray.position(), 1)).toVec3();
    vec3 localDirection = (toLocal * vec4(ray.forward(), 1)).toVec3() - (toLocal * vec4(vec3(0), 1)).toVec3();

    vec3 origin;
    vec3 normal = upDir;

    if (roughly(localDirection.dot(normal), 0.0))
    {
        return {};
    }

    f64 distance = (origin - localStart).dot(normal) / localDirection.dot(normal);

    if (distance < 0)
    {
        return {};
    }

    vec3 intersection = localStart + (localDirection * distance);

    vec2 point = (intersection.toVec2() + 1) / 2;
    point.y = 1 - point.y;

    return Point(point.x * vrKeyboardWidth, point.y * vrKeyboardHeight);
}

SizeProperties VRKeyboard::sizeProperties() const
{
    return sizePropertiesFillAll;
}
#endif
