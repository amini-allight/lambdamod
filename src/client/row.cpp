/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "row.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

Row::Row(
    InterfaceWidget* parent,
    const optional<Color>& color,
    const function<void(const Point&)>& onRightClick
)
    : Layout(parent)
    , color(color)
    , onRightClick(onRightClick)
{
    START_WIDGET_SCOPE("row")
        START_SCOPE("interactive", this->onRightClick != nullptr)
            WIDGET_SLOT("open-context-menu", openContextMenu)
        END_SCOPE
    END_SCOPE
}

void Row::move(const Point& position)
{
    this->position(position);

    i32 x = 0;

    for (InterfaceWidget* child : children())
    {
        child->move(position + Point(x, 0));

        Size size = child->size();

        x += size.w;
    }
}

static i32 pickVerticalSize(const SizeProperties& props, const Size& parentSize)
{
    switch (props.vertical)
    {
    default :
    case Scaling_Fixed :
        return props.h;
    case Scaling_Fraction :
        return props.h * parentSize.h;
    case Scaling_Fill :
        return parentSize.h;
    }
}

void Row::resize(const Size& size)
{
    this->size(size);

    map<InterfaceWidget*, Size> sizes;

    i32 maxH = 0;
    i32 total = size.w;
    i32 fillCount = 0;

    // fixed or fraction
    for (InterfaceWidget* child : children())
    {
        SizeProperties props = sizePropertiesOf(child);

        Size size;

        switch (props.horizontal)
        {
        case Scaling_Fixed :
            if (props.w != 0)
            {
                size.w = props.w;
            }
            else
            {
                continue;
            }
            break;
        case Scaling_Fraction :
            size.w = props.w * this->size().w;
            break;
        case Scaling_Fill :
            fillCount++;
            continue;
        }

        size.h = pickVerticalSize(props, this->size());

        if (size.h > maxH)
        {
            maxH = size.h;
        }

        total -= size.w;
        sizes.insert({ child, size });
    }

    for (InterfaceWidget* child : children())
    {
        SizeProperties props = sizePropertiesOf(child);

        Size size;

        switch (props.horizontal)
        {
        default :
            continue;
        case Scaling_Fixed :
            if (props.w == 0)
            {
                child->resize(Size(total, this->size().h));

                size.w = child->size().w;
            }
            else
            {
                continue;
            }
            break;
        }

        size.h = pickVerticalSize(props, this->size());

        if (size.h > maxH)
        {
            maxH = size.h;
        }

        total -= size.w;
        sizes.insert({ child, size });
    }

    // fill
    if (fillCount != 0)
    {
        i32 fraction = total / fillCount;
        i32 remainder = total - (fraction * fillCount);

        i32 i = 0;
        for (InterfaceWidget* child : children())
        {
            SizeProperties props = sizePropertiesOf(child);

            Size size;

            switch (props.horizontal)
            {
            case Scaling_Fill :
                size.w = fraction + (i + 1 == fillCount ? remainder : 0);
                break;
            default :
                continue;
            }

            i++;

            size.h = pickVerticalSize(props, this->size());

            if (size.h > maxH)
            {
                maxH = size.h;
            }

            sizes.insert({ child, size });
        }
    }

    // set
    i32 x = 0;

    for (InterfaceWidget* child : children())
    {
        Size size = sizes[child];

        child->move(this->position() + Point(x, 0));
        child->resize(size);

        x += size.w;
    }

    this->size(Size(size.w, maxH));
}

void Row::draw(const DrawContext& ctx) const
{
    if (color)
    {
        drawSolid(
            ctx,
            this,
            *color
        );
    }

    InterfaceWidget::draw(ctx);
}

void Row::openContextMenu(const Input& input)
{
    onRightClick(pointer);
}

SizeProperties Row::sizeProperties() const
{
    return {
        0, 0,
        Scaling_Fill, Scaling_Fixed
    };
}
