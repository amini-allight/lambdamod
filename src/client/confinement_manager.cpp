/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "confinement_manager.hpp"
#include "control_context.hpp"
#include "global.hpp"
#include "vr_render.hpp"
#include "flat_render.hpp"

#include "render_vr_confinement_indicator.hpp"
#include "render_flat_confinement_indicator.hpp"

ConfinementManager::ConfinementManager(ControlContext* ctx)
    : ctx(ctx)
{
    confined = false;
    distance = 0;

#ifdef LMOD_VR
    if (g_vr)
    {
        confinementIndicatorID = dynamic_cast<VRRender*>(ctx->controller()->render())->addDecoration<RenderVRConfinementIndicator>(vec3(), 1, 0);
    }
    else
    {
        confinementIndicatorID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<RenderFlatConfinementIndicator>(vec3(), 1, 0);
    }
#else
    confinementIndicatorID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<RenderFlatConfinementIndicator>(vec3(), 1, 0);
#endif

    ctx->controller()->render()->setDecorationShown(confinementIndicatorID, false);
}

void ConfinementManager::step()
{
    if (!confined)
    {
        return;
    }

    const User* self = ctx->controller()->self();

    if (!self)
    {
        return;
    }

    vec3 selfPosition;

    if (self->attached())
    {
        selfPosition = ctx->controller()->host()->globalPosition();
    }
#ifdef LMOD_VR
    else if (g_vr)
    {
        selfPosition = ctx->vrViewer()->roomTransform().position();
    }
#endif
    else
    {
        selfPosition = ctx->flatViewer()->position();
    }

    f64 selfDistance = position.distance(selfPosition);

    if (!self->attached() && selfDistance > distance)
    {
        vec3 newPosition = position + (selfPosition - position).normalize() * distance;

#ifdef LMOD_VR
        if (g_vr)
        {
            ctx->vrViewer()->moveRoom(newPosition, ctx->vrViewer()->roomTransform().rotation());
        }
        else
        {
            ctx->flatViewer()->move(newPosition, ctx->flatViewer()->rotation());
        }
#else
        ctx->flatViewer()->move(newPosition, ctx->flatViewer()->rotation());
#endif
    }

    f64 strength = min(selfDistance / distance, 1.0);

#ifdef LMOD_VR
    if (g_vr)
    {
        dynamic_cast<VRRender*>(ctx->controller()->render())->updateDecoration<RenderVRConfinementIndicator>(
            confinementIndicatorID,
            position,
            distance,
            strength
        );
    }
    else
    {
        dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<RenderFlatConfinementIndicator>(
            confinementIndicatorID,
            position,
            distance,
            strength
        );
    }
#else
    dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<RenderFlatConfinementIndicator>(
        confinementIndicatorID,
        position,
        distance,
        strength
    );
#endif
}

void ConfinementManager::start(const vec3& position, f64 distance)
{
    confined = true;
    this->position = position;
    this->distance = distance;

    ctx->controller()->render()->setDecorationShown(confinementIndicatorID, true);
}

void ConfinementManager::end()
{
    confined = false;

    ctx->controller()->render()->setDecorationShown(confinementIndicatorID, false);
}
