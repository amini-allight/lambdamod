/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "dialog.hpp"
#include "list_view.hpp"
#include "atmosphere_parameters.hpp"

class World;

class AtmosphereParametersDialog : public Dialog
{
public:
    AtmosphereParametersDialog(
        InterfaceView* view,
        const function<AtmosphereParameters()>& parametersSource,
        const function<void(const AtmosphereParameters&)>& onParameters
    );

    void step() override;

private:
    function<AtmosphereParameters()> parametersSource;
    function<void(const AtmosphereParameters&)> onParameters;
    ListView* listView;
    optional<AtmosphereParameters> lastAtmosphereParameters;

    bool needsWidgetUpdate() const;
    void updateWidgets();

    void addAtmosphereChildren();
    void addAtmosphereLightningChildren(const AtmosphereLightning& lightning);
    void addAtmosphereEffectChildren(size_t index, const AtmosphereEffect& effect);
    void addAtmosphereEffectNewPrompt();

    void addAtmosphereEffect();
    void moveAtmosphereEffectDown(size_t index);
    void moveAtmosphereEffectUp(size_t index);
    void removeAtmosphereEffect(size_t index);

    bool atmosphereEnabledSource();
    void onAtmosphereEnabled(bool enabled);

    u64 atmosphereSeedSource();
    void onAtmosphereSeed(u64 seed);

    f64 atmosphereLightningFrequencySource();
    void onAtmosphereLightningFrequency(f64 frequency);
    f64 atmosphereLightningMinDistanceSource();
    void onAtmosphereLightningMinDistance(f64 distance);
    f64 atmosphereLightningMaxDistanceSource();
    void onAtmosphereLightningMaxDistance(f64 distance);
    f64 atmosphereLightningLowerHeightSource();
    void onAtmosphereLightningLowerHeight(f64 height);
    f64 atmosphereLightningUpperHeightSource();
    void onAtmosphereLightningUpperHeight(f64 height);
    HDRColor atmosphereLightningColorSource();
    void onAtmosphereLightningColor(const HDRColor& color);

    vector<string> atmosphereEffectTypeOptionsSource();
    size_t atmosphereEffectTypeSource(size_t index);
    void onAtmosphereEffectType(size_t index, size_t typeIndex);
    f64 atmosphereEffectDensitySource(size_t index);
    void onAtmosphereEffectDensity(size_t index, f64 density);
    f64 atmosphereEffectSizeSource(size_t index);
    void onAtmosphereEffectSize(size_t index, f64 size);
    f64 atmosphereEffectRadiusSource(size_t index);
    void onAtmosphereEffectRadius(size_t index, f64 radius);
    f64 atmosphereEffectMassSource(size_t index);
    void onAtmosphereEffectMass(size_t index, f64 mass);
    vec3 atmosphereEffectFlowVelocitySource(size_t index);
    void onAtmosphereEffectFlowVelocity(size_t index, const vec3& velocity);
    EmissiveColor atmosphereEffectColorSource(size_t index);
    void onAtmosphereEffectColor(size_t index, const EmissiveColor& color);
};
