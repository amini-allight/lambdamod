/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "sound_source.hpp"
#include "sample_sound_data.hpp"
#include "sample.hpp"

class Entity;

class SoundEntityPlayback final : public SoundSource
{
public:
    SoundEntityPlayback(Sound* sound, const Entity* entity, SamplePlaybackID id);

    void update(const Entity* entity);

    void markForRemoval();

private:
    bool audible;
    SamplePlaybackID id;
    bool playing;
    bool loop;
    f64 volume;
    Sample lastSample;
    SampleSoundData data;
    i64 readHead;
    bool markedForRemoval;
    
    variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> pull(f64 multiplier) override;
};
