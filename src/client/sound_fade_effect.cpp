/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_fade_effect.hpp"

SoundFadeEffect::SoundFadeEffect(f64 strength)
    : SoundPostProcessEffect()
    , strength(strength)
{

}

void SoundFadeEffect::apply(SoundStereoChunk& chunk) const
{
    f32 volume = 1 - strength;

    for (size_t i = 0; i < chunk.channelCount(); i++)
    {
        SoundChunk& channel = chunk.channel(i);

        for (size_t j = 0; j < channel.length(); j++)
        {
            channel.sample(j) *= volume;
        }
    }
}

void SoundFadeEffect::update(f64 strength)
{
    this->strength = strength;
}
