/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_force_marker.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr u32 ringResolution = 64;
static constexpr u32 arrowVertexCount = 3 * 2;
static constexpr u32 vortexArrowCount = 6;
static constexpr u32 vortexArrowSegmentCount = 8;
static constexpr u32 vortexArrowVertexCount = (vortexArrowSegmentCount + 2) * 2;
static constexpr u32 fanBladeCount = 6;
static constexpr u32 fanBladeSegmentCount = 8;
static constexpr f32 fanBladeDrift = radians(5);
static constexpr u32 rocketLineCount = 6;
static constexpr u32 rocketLineVertexCount = 2;
static constexpr u32 magnetNLineCount = 3;
static constexpr u32 magnetSLineCount = 17;
static constexpr u32 magnetArrowLineCount = 5;

RenderForceMarker::RenderForceMarker(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    BodyForceType type,
    f32 radius,
    bool negative
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, rotation, vec3(1) };

    size_t writeHead = 0;
    vector<ColoredVertex> vertexData;
    vec3 color = theme.panelForegroundColor.toVec3();

    switch (type)
    {
    case Body_Force_Linear :
    {
        vertexCount = ringResolution * 2 + arrowVertexCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        for (u32 i = 0; i < ringResolution; i++)
        {
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / ringResolution)).rotate(upDir * radius), color };
            vertexData[writeHead++] = { fquaternion(forwardDir, (i + 1) * (tau / ringResolution)).rotate(upDir * radius), color };
        }

        vertexData[writeHead++] = { (negative ? -1 : +1) * radius * fvec3(), color };
        vertexData[writeHead++] = { (negative ? -1 : +1) * radius * fvec3(0, 1, 0), color };

        vertexData[writeHead++] = { (negative ? -1 : +1) * radius * fvec3(0, 1, 0), color };
        vertexData[writeHead++] = { (negative ? -1 : +1) * radius * fvec3(-0.2, 0.8, 0), color };

        vertexData[writeHead++] = { (negative ? -1 : +1) * radius * fvec3(0, 1, 0), color };
        vertexData[writeHead++] = { (negative ? -1 : +1) * radius * fvec3(+0.2, 0.8, 0), color };
        break;
    }
    case Body_Force_Vortex :
    {
        vertexCount = vortexArrowCount * vortexArrowVertexCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        for (u32 i = 0; i < vortexArrowCount; i++)
        {
            for (u32 j = 0; j < vortexArrowSegmentCount; j++)
            {
                f32 angle = i * (tau / vortexArrowCount) + j * (tau / ringResolution);
                f32 nextAngle = i * (tau / vortexArrowCount) + (j + 1) * (tau / ringResolution);

                if (negative)
                {
                    angle = -angle;
                    nextAngle = -nextAngle;
                }

                vertexData[writeHead++] = { fquaternion(forwardDir, angle).rotate(upDir * radius), color };
                vertexData[writeHead++] = { fquaternion(forwardDir, nextAngle).rotate(upDir * radius), color };
            }

            fvec3 head = vertexData[writeHead - 1].position;
            fvec3 heading = (vertexData[writeHead - 1].position - vertexData[writeHead - 2].position).normalize();

            vertexData[writeHead++] = { head, color };
            vertexData[writeHead++] = { head + fquaternion(forwardDir, radians(-135)).rotate(heading * (radius / 10)), color };

            vertexData[writeHead++] = { head, color };
            vertexData[writeHead++] = { head + fquaternion(forwardDir, radians(+135)).rotate(heading * (radius / 10)), color };
        }
        break;
    }
    case Body_Force_Omnidirectional :
    case Body_Force_Gravity :
    {
        vertexCount = 3 * 2 * ringResolution + 6 * arrowVertexCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        static const fquaternion ringTransforms[] = {
            fquaternion(forwardDir, upDir),
            fquaternion(rightDir, upDir),
            fquaternion(upDir, backDir)
        };

        for (const fquaternion& ringTransform : ringTransforms)
        {
            for (u32 j = 0; j < ringResolution; j++)
            {
                vec3 a = fquaternion(forwardDir, j * (tau / ringResolution)).rotate(upDir * radius);
                vec3 b = fquaternion(forwardDir, (j + 1) * (tau / ringResolution)).rotate(upDir * radius);

                vertexData[writeHead++] = { ringTransform.rotate(a), color };
                vertexData[writeHead++] = { ringTransform.rotate(b), color };
            }
        }

        static const fquaternion arrowTransforms[] = {
            fquaternion(forwardDir, upDir),
            fquaternion(rightDir, upDir),
            fquaternion(backDir, upDir),
            fquaternion(leftDir, upDir),
            fquaternion(upDir, backDir),
            fquaternion(downDir, forwardDir)
        };

        f32 arrowSize = radius;
        fvec3 offset = negative ? fvec3(0, -(radius + arrowSize), 0) : fvec3(0, radius, 0);

        for (const fquaternion& arrowTransform : arrowTransforms)
        {
            vertexData[writeHead++] = { arrowTransform.rotate((arrowSize * fvec3()) + offset), color };
            vertexData[writeHead++] = { arrowTransform.rotate((arrowSize * fvec3(0, 1, 0)) + offset), color };

            vertexData[writeHead++] = { arrowTransform.rotate((arrowSize * fvec3(0, 1, 0)) + offset), color };
            vertexData[writeHead++] = { arrowTransform.rotate((arrowSize * fvec3(-0.2, 0.8, 0)) + offset), color };

            vertexData[writeHead++] = { arrowTransform.rotate((arrowSize * fvec3(0, 1, 0)) + offset), color };
            vertexData[writeHead++] = { arrowTransform.rotate((arrowSize * fvec3(+0.2, 0.8, 0)) + offset), color };
        }
        break;
    }
    case Body_Force_Fan :
    {
        vertexCount = 2 * ringResolution + 2 * fanBladeCount * fanBladeSegmentCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        for (u32 i = 0; i < ringResolution; i++)
        {
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / ringResolution)).rotate(upDir * radius), color };
            vertexData[writeHead++] = { fquaternion(forwardDir, (i + 1) * (tau / ringResolution)).rotate(upDir * radius), color };
        }

        for (u32 i = 0; i < fanBladeCount; i++)
        {
            for (u32 j = 0; j < fanBladeSegmentCount; j++)
            {
                f32 angle = i * (tau / fanBladeCount) + j * fanBladeDrift;
                f32 nextAngle = i * (tau / fanBladeCount) + (j + 1) * fanBladeDrift;

                f32 offset = radius / 4 + j * (radius / (2 * fanBladeSegmentCount));
                f32 nextOffset = radius / 4 + (j + 1) * (radius / (2 * fanBladeSegmentCount));

                vertexData[writeHead++] = { fquaternion(forwardDir, angle).rotate(upDir * offset), color };
                vertexData[writeHead++] = { fquaternion(forwardDir, nextAngle).rotate(upDir * nextOffset), color };
            }
        }
        break;
    }
    case Body_Force_Rocket :
    {
        vertexCount = 2 * ringResolution + rocketLineCount * rocketLineVertexCount;
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        for (u32 i = 0; i < ringResolution; i++)
        {
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / ringResolution)).rotate(upDir * radius), color };
            vertexData[writeHead++] = { fquaternion(forwardDir, (i + 1) * (tau / ringResolution)).rotate(upDir * radius), color };
        }

        for (u32 i = 0; i < rocketLineCount; i++)
        {
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / rocketLineCount)).rotate(upDir * (radius / 4)), color };
            vertexData[writeHead++] = { fquaternion(forwardDir, i * (tau / rocketLineCount)).rotate(upDir * (radius / 4) * 3), color };
        }
        break;
    }
    case Body_Force_Magnetism :
    {
        vertexCount = 2 * (magnetNLineCount + magnetSLineCount + magnetArrowLineCount);
        vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));
        vertexData = vector<ColoredVertex>(vertexCount);

        vec3 top;
        vec3 topLeft;
        vec3 topRight;
        vec3 bot;
        vec3 botLeft;
        vec3 botRight;

        if (negative)
        {
            top = forwardDir * (radius - (radius / 5));
            bot = backDir * (radius - (radius / 5));

            topLeft = top + vec3(-1, +1, 0) * (radius / 5);
            topRight = top + vec3(+1, +1, 0) * (radius / 5);

            botLeft = bot + vec3(-1, -1, 0) * (radius / 5);
            botRight = bot + vec3(+1, -1, 0) * (radius / 5);
        }
        else
        {
            top = forwardDir * radius;
            bot = backDir * radius;

            topLeft = top + vec3(-1, -1, 0) * (radius / 5);
            topRight = top + vec3(+1, -1, 0) * (radius / 5);

            botLeft = bot + vec3(-1, +1, 0) * (radius / 5);
            botRight = bot + vec3(+1, +1, 0) * (radius / 5);
        }

        vertexData[writeHead++] = { top, color };
        vertexData[writeHead++] = { bot, color };

        vertexData[writeHead++] = { top, color };
        vertexData[writeHead++] = { topLeft, color };

        vertexData[writeHead++] = { top, color };
        vertexData[writeHead++] = { topRight, color };

        vertexData[writeHead++] = { bot, color };
        vertexData[writeHead++] = { botLeft, color };

        vertexData[writeHead++] = { bot, color };
        vertexData[writeHead++] = { botRight, color };

        {
            vec3 nOffset = forwardDir * radius * 1.5;
            f64 size = radius / 5;

            vec3 a = vec3(-1, -1, 0) * size;
            vec3 b = vec3(-1, +1, 0) * size;
            vec3 c = vec3(+1, -1, 0) * size;
            vec3 d = vec3(+1, +1, 0) * size;

            vertexData[writeHead++] = { nOffset + a, color };
            vertexData[writeHead++] = { nOffset + b, color };

            vertexData[writeHead++] = { nOffset + b, color };
            vertexData[writeHead++] = { nOffset + c, color };

            vertexData[writeHead++] = { nOffset + c, color };
            vertexData[writeHead++] = { nOffset + d, color };
        }

        {
            vec3 sOffset = backDir * radius * 1.5;
            f64 size = radius / 5;

            size_t i = 0;
            vector<vec3> sVertices(magnetSLineCount + 1);

            sVertices[i++] = vec3(+1, +1, 0) * size;

            for (size_t j = 0; j < 8; j++)
            {
                sVertices[i++] = vec3(-1, +1, 0) * (radius / 10) + quaternion(upDir, (j / 16.0) * tau).rotate(forwardDir * (radius / 10));
            }

            for (size_t j = 0; j < 8; j++)
            {
                sVertices[i++] = vec3(+1, -1, 0) * (radius / 10) + quaternion(downDir, (j / 16.0) * tau).rotate(forwardDir * (radius / 10));
            }

            sVertices[i++] = vec3(-1, -1, 0) * size;

            for (size_t j = 0; j < sVertices.size() - 1; j++)
            {
                vertexData[writeHead++] = { sOffset + sVertices[j], color };
                vertexData[writeHead++] = { sOffset + sVertices[j + 1], color };
            }
        }
        break;
    }
    }

    VmaMapping<f32> mapping(ctx->allocator, vertices);
    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderForceMarker::~RenderForceMarker()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderForceMarker::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderForceMarker::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
