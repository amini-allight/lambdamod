/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "right_clickable_widget.hpp"
#include "tab_content.hpp"

#include "line_edit.hpp"
#include "list_view.hpp"

class User;

struct UserDetail
{
    UserDetail();
    UserDetail(UserID id, const User& user);

    bool operator==(const UserDetail& rhs) const;
    bool operator!=(const UserDetail& rhs) const;

    UserID id;
    string name;
    vec3 color;
    bool active;
    bool admin;
    bool vr;
    bool attached;
    u32 magnitude;
    i32 advantage;
    bool doom;
    u32 ping;
};

class UserSettings : public RightClickableWidget, public TabContent
{
public:
    UserSettings(InterfaceWidget* parent);

    void step() override;

private:
    vector<UserDetail> details;
    string search;
    LineEdit* searchBar;
    ListView* listView;

    vector<UserDetail> convert(const map<UserID, User>& users) const;

    void openRightClickMenu(i32 index, const Point& pointer) override;

    void onRemove(UserID id, const string& name, const Point& point);
    void onKick(UserID id, const Point& point);
    void onSetColor(UserID id, const Point& point);
    void onManageTeams(UserID id, const Point& point);
    void onTeleportToUser(UserID id, const Point& point);
    void onTeleportUserToMe(UserID id, const Point& point);
    void onAdvantageUp(UserID id, const Point& point);
    void onAdvantageDown(UserID id, const Point& point);
    void onDoomFlag(UserID id, const Point& point);
    void onDoomClear(UserID id, const Point& point);

    vector<string> teamsSource(UserID id);
    void onAddTeam(UserID id, const string& name);
    void onRemoveTeam(UserID id, const string& name);

    void onColor(UserID id, const Color& color);

    void updateListView();

    SizeProperties sizeProperties() const override;
};
