/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_window.hpp"

#include "item_list.hpp"
#include "stack_view.hpp"

class DocumentManager : public InterfaceWindow
{
public:
    DocumentManager(InterfaceView* view);

    void open(const Point& point) override;
    void open(const Point& point, const string& name);

    void step() override;

private:
    ItemList* list;
    StackView* stack;

    // Second argument is necessary because during rename and clone the object doesn't have a name yet
    void openDocument(const string& name, const string& text);
    void closeDocument(const string& name);

    vector<string> itemSource() const;
    void onAdd(const string& name);
    void onRemove(const string& name);
    void onSelect(const string& name);
    void onRename(const string& oldName, const string& newName);
    void onClone(const string& oldName, const string& newName);

    string textSource(const string& name) const;
    void onSave(const string& name, const string& text);

    void dismiss(const Input& input) override;
};
