/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_hardware_occlusion_scene.hpp"
#include "render_tools.hpp"
#include "render_flat_entity.hpp"
#include "render_vr_entity.hpp"
#include "global.hpp"

static constexpr u32 maxInstanceCount = 1024 * 1024;

RenderHardwareOcclusionScene::RenderHardwareOcclusionScene(const RenderContext* ctx)
    : ctx(ctx)
    , vkCreateAccelerationStructureKHR(GET_VK_EXTENSION_FUNCTION(ctx->instance, vkCreateAccelerationStructureKHR))
    , vkDestroyAccelerationStructureKHR(GET_VK_EXTENSION_FUNCTION(ctx->instance, vkDestroyAccelerationStructureKHR))
    , vkGetAccelerationStructureBuildSizesKHR(GET_VK_EXTENSION_FUNCTION(ctx->instance, vkGetAccelerationStructureBuildSizesKHR))
    , vkGetAccelerationStructureDeviceAddressKHR(GET_VK_EXTENSION_FUNCTION(ctx->instance, vkGetAccelerationStructureDeviceAddressKHR))
    , vkCmdBuildAccelerationStructuresKHR(GET_VK_EXTENSION_FUNCTION(ctx->instance, vkCmdBuildAccelerationStructuresKHR))
    , vkGetBufferDeviceAddressKHR(
        VK_API_VERSION_MAJOR(ctx->apiVersion) > 1 || VK_API_VERSION_MINOR(ctx->apiVersion) > 1
            ? nullptr
            : GET_VK_EXTENSION_FUNCTION(ctx->instance, vkGetBufferDeviceAddressKHR)
    )
    , instanceBufferMapping{nullptr}
    , instanceBufferAddress{0}
    , scratchBufferAddress{0}
    , accelerationStructure{nullptr}
    , initialized{false, false}
    , lastInstanceCount(0)
{
    accelerationStructureProperties = VkPhysicalDeviceAccelerationStructurePropertiesKHR{};
    accelerationStructureProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_PROPERTIES_KHR;

    physDeviceProperties.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_PROPERTIES_2;
    physDeviceProperties.pNext = &accelerationStructureProperties;

    vkGetPhysicalDeviceProperties2(ctx->physDevice, &physDeviceProperties);

#ifdef LMOD_VR
    if (g_vr)
    {
        for (u32 eye = 0; eye < eyeCount; eye++)
        {
            tie(instanceBuffer[eye], instanceBufferAddress[eye]) = createInstanceBuffer();
            instanceBufferMapping[eye] = new VmaMapping<VkAccelerationStructureInstanceKHR>(ctx->allocator, instanceBuffer[eye]);
        }
    }
    else
    {
        tie(instanceBuffer[0], instanceBufferAddress[0]) = createInstanceBuffer();
        instanceBufferMapping[0] = new VmaMapping<VkAccelerationStructureInstanceKHR>(ctx->allocator, instanceBuffer[0]);
    }
#else
    tie(instanceBuffer[0], instanceBufferAddress[0]) = createInstanceBuffer();
    instanceBufferMapping[0] = new VmaMapping<VkAccelerationStructureInstanceKHR>(ctx->allocator, instanceBuffer[0]);
#endif
}

RenderHardwareOcclusionScene::~RenderHardwareOcclusionScene()
{
#ifdef LMOD_VR
    if (g_vr)
    {
        for (u32 eye = 0; eye < eyeCount; eye++)
        {
            vkDestroyAccelerationStructureKHR(ctx->device, accelerationStructure[eye], nullptr);

            destroyBuffer(ctx->allocator, scratchBuffer[eye]);
            destroyBuffer(ctx->allocator, backingBuffer[eye]);

            delete instanceBufferMapping[eye];
            destroyBuffer(ctx->allocator, instanceBuffer[eye]);
        }
    }
    else
    {
        vkDestroyAccelerationStructureKHR(ctx->device, accelerationStructure[0], nullptr);

        destroyBuffer(ctx->allocator, scratchBuffer[0]);
        destroyBuffer(ctx->allocator, backingBuffer[0]);

        delete instanceBufferMapping[0];
        destroyBuffer(ctx->allocator, instanceBuffer[0]);
    }
#else
    vkDestroyAccelerationStructureKHR(ctx->device, accelerationStructure[0], nullptr);

    destroyBuffer(ctx->allocator, scratchBuffer[0]);
    destroyBuffer(ctx->allocator, backingBuffer[0]);

    delete instanceBufferMapping[0];
    destroyBuffer(ctx->allocator, instanceBuffer[0]);
#endif

    for (const auto& [ id, entity ] : entities)
    {
        delete entity;
    }
}

void RenderHardwareOcclusionScene::update(
    VkCommandBuffer commandBuffer,
    const vec3& cameraPosition,
    const map<EntityID, RenderFlatEntity*>& entities
)
{
    vector<VkAccelerationStructureInstanceKHR> instances;
    instances.reserve(entities.size());

    recordInstances(commandBuffer, cameraPosition, entities, instances);

    build(commandBuffer, instances);
}

#ifdef LMOD_VR
void RenderHardwareOcclusionScene::update(
    VkCommandBuffer commandBuffer,
    vec3 cameraPosition[eyeCount],
    const map<EntityID, RenderVREntity*>& entities
)
{
    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        vector<VkAccelerationStructureInstanceKHR> instances;
        instances.reserve(entities.size());

        recordInstances(commandBuffer, cameraPosition, entities, eye, instances);

        build(commandBuffer, instances, eye);
    }
}
#endif

RenderOcclusionSceneReference RenderHardwareOcclusionScene::get() const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        return RenderOcclusionSceneReference(accelerationStructure[0], accelerationStructure[1]);
    }
    else
    {
        return RenderOcclusionSceneReference(accelerationStructure[0], accelerationStructure[0]);
    }
#else
    return RenderOcclusionSceneReference(accelerationStructure[0], accelerationStructure[0]);
#endif
}

void RenderHardwareOcclusionScene::build(
    VkCommandBuffer commandBuffer,
    const vector<VkAccelerationStructureInstanceKHR>& instances,
    u32 eye
)
{
    VkResult result;

    u32 instanceCount = instances.size();

    memcpy(
        instanceBufferMapping[eye]->data,
        instances.data(),
        min(maxInstanceCount, instanceCount) * sizeof(VkAccelerationStructureInstanceKHR)
    );

    VkAccelerationStructureGeometryKHR geometry{};
    geometry.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_KHR;
    geometry.flags = VK_GEOMETRY_OPAQUE_BIT_KHR;
    geometry.geometryType = VK_GEOMETRY_TYPE_INSTANCES_KHR;
    geometry.geometry.instances.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_GEOMETRY_INSTANCES_DATA_KHR;
    geometry.geometry.instances.arrayOfPointers = VK_FALSE;
    geometry.geometry.instances.data = VkDeviceOrHostAddressConstKHR{
        .deviceAddress = instanceBufferAddress[eye]
    };

    VkAccelerationStructureBuildGeometryInfoKHR buildGeometryInfo{};
    buildGeometryInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_GEOMETRY_INFO_KHR;
    buildGeometryInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;
    buildGeometryInfo.flags = VK_BUILD_ACCELERATION_STRUCTURE_ALLOW_UPDATE_BIT_KHR | VK_BUILD_ACCELERATION_STRUCTURE_PREFER_FAST_BUILD_BIT_KHR;
    buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
    buildGeometryInfo.geometryCount = 1;
    buildGeometryInfo.pGeometries = &geometry;

    VkAccelerationStructureBuildSizesInfoKHR buildSizesInfo{};
    buildSizesInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_BUILD_SIZES_INFO_KHR;
    vkGetAccelerationStructureBuildSizesKHR(
        ctx->device,
        VK_ACCELERATION_STRUCTURE_BUILD_TYPE_DEVICE_KHR,
        &buildGeometryInfo,
        &maxInstanceCount,
        &buildSizesInfo
    );

    if (instances.size() != lastInstanceCount)
    {
        vkDestroyAccelerationStructureKHR(ctx->device, accelerationStructure[eye], nullptr);
        destroyBuffer(ctx->allocator, backingBuffer[eye]);
        destroyBuffer(ctx->allocator, scratchBuffer[eye]);
        initialized[eye] = false;
    }

    if (!initialized[eye])
    {
        tie(scratchBuffer[eye], scratchBufferAddress[eye]) = createScratchBuffer(buildSizesInfo);

        backingBuffer[eye] = createAccelerationStructureBuffer(buildSizesInfo);

        VkAccelerationStructureCreateInfoKHR createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_ACCELERATION_STRUCTURE_CREATE_INFO_KHR;
        createInfo.buffer = backingBuffer[eye].buffer;
        createInfo.size = buildSizesInfo.accelerationStructureSize;
        createInfo.type = VK_ACCELERATION_STRUCTURE_TYPE_TOP_LEVEL_KHR;

        result = vkCreateAccelerationStructureKHR(ctx->device, &createInfo, nullptr, &accelerationStructure[eye]);

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create ray-tracing bottom-level acceleration structure: " + vulkanError(result));
        }

        buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_BUILD_KHR;
        buildGeometryInfo.srcAccelerationStructure = nullptr;
        buildGeometryInfo.dstAccelerationStructure = accelerationStructure[eye];
        buildGeometryInfo.scratchData.deviceAddress = scratchBufferAddress[eye];
    }
    else
    {
        buildGeometryInfo.mode = VK_BUILD_ACCELERATION_STRUCTURE_MODE_UPDATE_KHR;
        buildGeometryInfo.srcAccelerationStructure = accelerationStructure[eye];
        buildGeometryInfo.dstAccelerationStructure = accelerationStructure[eye];
        buildGeometryInfo.scratchData.deviceAddress = scratchBufferAddress[eye];
    }

    vkCmdPipelineBarrier(
        commandBuffer,
        VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR,
        VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR,
        0,
        0,
        nullptr,
        0,
        nullptr,
        0,
        nullptr
    );

    VkAccelerationStructureBuildRangeInfoKHR buildRangeInfo{};
    buildRangeInfo.primitiveCount = min(maxInstanceCount, instanceCount);
    buildRangeInfo.primitiveOffset = 0;
    buildRangeInfo.firstVertex = 0;
    buildRangeInfo.transformOffset = 0;

    VkAccelerationStructureBuildRangeInfoKHR* buildRangeInfos[] = {
        &buildRangeInfo
    };

    vkCmdBuildAccelerationStructuresKHR(
        commandBuffer,
        1,
        &buildGeometryInfo,
        buildRangeInfos
    );

    vkCmdPipelineBarrier(
        commandBuffer,
        VK_PIPELINE_STAGE_ACCELERATION_STRUCTURE_BUILD_BIT_KHR,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        0,
        0,
        nullptr,
        0,
        nullptr,
        0,
        nullptr
    );

    initialized[eye] = true;
    lastInstanceCount = instances.size();
}

void RenderHardwareOcclusionScene::recordInstances(
    VkCommandBuffer commandBuffer,
    const vec3& cameraPosition,
    const map<EntityID, RenderFlatEntity*>& entities,
    vector<VkAccelerationStructureInstanceKHR>& instances
)
{
    for (const auto& [ id, entity ] : entities)
    {
        if (entity->occlusionMesh().vertices.empty())
        {
            auto it = this->entities.find(id);

            if (it != this->entities.end())
            {
                delete it->second;
                this->entities.erase(it);
            }

            recordInstances(
                commandBuffer,
                cameraPosition,
                entity->children(),
                instances
            );
            continue;
        }

        RenderHardwareOcclusionEntity* occlusionEntity;

        auto it = this->entities.find(id);

        if (it != this->entities.end())
        {
            if (it->second->needsReinit(entity->occlusionMesh()))
            {
                delete it->second;
                it->second = new RenderHardwareOcclusionEntity(
                    this,
                    commandBuffer,
                    entity->occlusionMesh(),
                    entity->occlusionMeshSource()
                );
            }
            else if (it->second->needsUpdate(entity->occlusionMesh(), entity->occlusionMeshSource()))
            {
                it->second->update(commandBuffer, entity->occlusionMesh(), entity->occlusionMeshSource());
            }

            occlusionEntity = it->second;
        }
        else
        {
            occlusionEntity = new RenderHardwareOcclusionEntity(
                this,
                commandBuffer,
                entity->occlusionMesh(),
                entity->occlusionMeshSource()
            );

            this->entities.insert_or_assign(id, occlusionEntity);
        }

        VkAccelerationStructureInstanceKHR instance{};
        instance.instanceCustomIndex = 0;
        instance.mask = 0xff;
        instance.instanceShaderBindingTableRecordOffset = 0;
        instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
        instance.accelerationStructureReference = occlusionEntity->address();

        array<f32, 3 * 4> transform = fmat4(entity->cameraLocalTransform(cameraPosition)).toMat3x4();
        memcpy(&instance.transform, &transform, sizeof(transform));

        instances.push_back(instance);

        recordInstances(
            commandBuffer,
            cameraPosition,
            entity->children(),
            instances
        );
    }
}

#ifdef LMOD_VR
void RenderHardwareOcclusionScene::recordInstances(
    VkCommandBuffer commandBuffer,
    vec3 cameraPosition[eyeCount],
    const map<EntityID, RenderVREntity*>& entities,
    u32 eye,
    vector<VkAccelerationStructureInstanceKHR>& instances
)
{
    for (const auto& [ id, entity ] : entities)
    {
        if (entity->occlusionMesh().vertices.empty())
        {
            auto it = this->entities.find(id);

            if (it != this->entities.end())
            {
                delete it->second;
                this->entities.erase(it);
            }

            recordInstances(
                commandBuffer,
                cameraPosition,
                entity->children(),
                eye,
                instances
            );
            continue;
        }

        RenderHardwareOcclusionEntity* occlusionEntity;

        auto it = this->entities.find(id);

        if (it != this->entities.end())
        {
            if (it->second->needsReinit(entity->occlusionMesh()))
            {
                delete it->second;
                it->second = new RenderHardwareOcclusionEntity(
                    this,
                    commandBuffer,
                    entity->occlusionMesh(),
                    entity->occlusionMeshSource()
                );
            }
            else if (it->second->needsUpdate(entity->occlusionMesh(), entity->occlusionMeshSource()))
            {
                it->second->update(commandBuffer, entity->occlusionMesh(), entity->occlusionMeshSource());
            }

            occlusionEntity = it->second;
        }
        else
        {
            occlusionEntity = new RenderHardwareOcclusionEntity(
                this,
                commandBuffer,
                entity->occlusionMesh(),
                entity->occlusionMeshSource()
            );

            this->entities.insert_or_assign(id, occlusionEntity);
        }

        VkAccelerationStructureInstanceKHR instance{};
        instance.instanceCustomIndex = 0;
        instance.mask = 0xff;
        instance.instanceShaderBindingTableRecordOffset = 0;
        instance.flags = VK_GEOMETRY_INSTANCE_TRIANGLE_FACING_CULL_DISABLE_BIT_KHR;
        instance.accelerationStructureReference = occlusionEntity->address();

        array<f32, 3 * 4> transform = fmat4(entity->cameraLocalTransform(cameraPosition[eye])).toMat3x4();
        memcpy(&instance.transform, &transform, sizeof(transform));

        instances.push_back(instance);

        recordInstances(
            commandBuffer,
            cameraPosition,
            entity->children(),
            eye,
            instances
        );
    }
}
#endif

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionScene::createInstanceBuffer()
{
    return createBufferAndAddress(
        maxInstanceCount * sizeof(VkAccelerationStructureInstanceKHR),
        VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT | VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_BUILD_INPUT_READ_ONLY_BIT_KHR,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VmaBuffer RenderHardwareOcclusionScene::createAccelerationStructureBuffer(const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo) const
{
    return createBuffer(
        ctx->allocator,
        buildSizesInfo.accelerationStructureSize,
        VK_BUFFER_USAGE_ACCELERATION_STRUCTURE_STORAGE_BIT_KHR | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY
    );
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionScene::createScratchBuffer(const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo) const
{
    return createBufferAndAddress(
        buildSizesInfo.buildScratchSize,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_SHADER_DEVICE_ADDRESS_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY,
        accelerationStructureProperties.minAccelerationStructureScratchOffsetAlignment
    );
}

tuple<VmaBuffer, VkDeviceAddress> RenderHardwareOcclusionScene::createBufferAndAddress(
    size_t size,
    VkBufferUsageFlags bufferUsage,
    VmaMemoryUsage memoryUsage,
    VkDeviceSize alignment
) const
{
    VmaBuffer buffer = createBuffer(
        ctx->allocator,
        size,
        bufferUsage,
        memoryUsage,
        alignment
    );

    VkBufferDeviceAddressInfo deviceAddressInfo{};
    deviceAddressInfo.sType = VK_STRUCTURE_TYPE_BUFFER_DEVICE_ADDRESS_INFO;
    deviceAddressInfo.buffer = buffer.buffer;

    auto f = vkGetBufferDeviceAddressKHR ? vkGetBufferDeviceAddressKHR : vkGetBufferDeviceAddress;

    VkDeviceAddress address = f(ctx->device, &deviceAddressInfo);

    return { buffer, address };
}
