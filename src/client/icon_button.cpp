/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "icon_button.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

IconButton::IconButton(
    InterfaceWidget* parent,
    Icon icon,
    const function<void()>& onActivate
)
    : InterfaceWidget(parent)
    , icon(icon)
    , onActivate(onActivate)
{
    START_WIDGET_SCOPE("icon-button")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void IconButton::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        focused() ? theme.accentColor : theme.outdentColor
    );

    drawImage(
        ctx,
        this,
        icon
    );
}

void IconButton::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void IconButton::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void IconButton::interact(const Input& input)
{
    playPositiveActivateEffect();
    onActivate();
}

SizeProperties IconButton::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::iconButtonSize().w), static_cast<f64>(UIScale::iconButtonSize().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
