/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "new_text_tool_pages.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "row.hpp"
#include "spacer.hpp"
#include "label.hpp"
#include "line_edit.hpp"
#include "uint_edit.hpp"
#include "ufloat_edit.hpp"
#include "checkbox.hpp"
#include "text_button.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"

#include "list_dialog.hpp"
#include "prices_dialog.hpp"
#include "major_choice_dialog.hpp"

TextTool::TextTool(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{

}

void TextTool::open()
{
    users.clear();

    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        users.insert(user.name());
    }

    reset();
}

void TextTool::close()
{

}

void TextTool::send(const TextRequest& request)
{
    TextRequestEvent event(request);

    for (const string& name : users)
    {
        const User* user = context()->controller()->game().getUserByName(name);

        if (!user)
        {
            continue;
        }

        event.userIDs.insert(user->id());
    }

    context()->controller()->send(event);
}

void TextTool::reset()
{
    options.clear();
}

void TextTool::onManageUsers()
{
    context()->interface()->addWindow<ListDialog>(
        localize("new-text-tool-pages-users"),
        bind(&TextTool::usersSource, this),
        bind(&TextTool::onAddUser, this, placeholders::_1),
        bind(&TextTool::onRemoveUser, this, placeholders::_1)
    );
}

vector<string> TextTool::usersSource()
{
    return { users.begin(), users.end() };
}

void TextTool::onAddUser(const string& name)
{
    if (!context()->controller()->game().getUserByName(name))
    {
        return;
    }

    users.insert(name);
}

void TextTool::onRemoveUser(const string& name)
{
    users.erase(name);
}

void TextTool::onManageOptions()
{
    context()->interface()->addWindow<ListDialog>(
        localize("new-text-tool-pages-options"),
        bind(&TextOptionsTool::optionsSource, this),
        bind(&TextOptionsTool::onAddOption, this, placeholders::_1),
        bind(&TextOptionsTool::onRemoveOption, this, placeholders::_1)
    );
}

vector<string> TextTool::optionsSource()
{
    return { options.begin(), options.end() };
}

void TextTool::onAddOption(const string& name)
{
    options.insert(name);
}

void TextTool::onRemoveOption(const string& name)
{
    options.erase(name);
}

TextSignalTool::TextSignalTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-signal", [this](InterfaceWidget* parent) -> void {
        state = new Checkbox(parent, nullptr, [](bool state) -> void {});
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextSignalTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextSignalTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextSignalTool::reset()
{
    state->set(false);
    TextTool::reset();
}

void TextSignalTool::onDone()
{
    TextSignalRequest signal;
    signal.state = state->checked();

    send(TextRequest(TextID(), signal));
}

SizeProperties TextSignalTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextTitlecardTool::TextTitlecardTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-subtitle", [this](InterfaceWidget* parent) -> void {
        subtitle = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        subtitle->setPlaceholder(localize("new-text-tool-pages-subtitle"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextTitlecardTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextTitlecardTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextTitlecardTool::reset()
{
    title->clear();
    subtitle->clear();
    TextTool::reset();
}

void TextTitlecardTool::onDone()
{
    TextTitlecardRequest titlecard;
    titlecard.title = title->content();
    titlecard.subtitle = subtitle->content();

    send(TextRequest(TextID(), titlecard));
}

SizeProperties TextTitlecardTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextTimeoutTool::TextTimeoutTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-text", [this](InterfaceWidget* parent) -> void {
        text = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        text->setPlaceholder(localize("new-text-tool-pages-text"));
    });

    new EditorEntry(column, "new-text-tool-pages-timeout", timeSuffix(), [this](InterfaceWidget* parent) -> void {
        timeout = new UFloatEdit(
            parent,
            nullptr,
            [](f64 value) -> void {}
        );
        timeout->setPlaceholder(localize("new-text-tool-pages-timeout"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextTimeoutTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextTimeoutTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextTimeoutTool::reset()
{
    title->clear();
    text->clear();
    timeout->clear();
    TextTool::reset();
}

void TextTimeoutTool::onDone()
{
    try
    {
        TextTimeoutRequest timeout;
        timeout.title = title->content();
        timeout.text = text->content();
        timeout.timeout = this->timeout->value();

        send(TextRequest(TextID(), timeout));
    }
    catch (const exception& e)
    {

    }
}

SizeProperties TextTimeoutTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextUnaryTool::TextUnaryTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-text", [this](InterfaceWidget* parent) -> void {
        text = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        text->setPlaceholder(localize("new-text-tool-pages-text"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextUnaryTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextUnaryTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextUnaryTool::reset()
{
    title->clear();
    text->clear();
    TextTool::reset();
}

void TextUnaryTool::onDone()
{
    TextUnaryRequest unary;
    unary.title = title->content();
    unary.text = text->content();

    send(TextRequest(TextID(), unary));
}

SizeProperties TextUnaryTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextBinaryTool::TextBinaryTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-text", [this](InterfaceWidget* parent) -> void {
        text = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        text->setPlaceholder(localize("new-text-tool-pages-text"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextBinaryTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextBinaryTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextBinaryTool::reset()
{
    title->clear();
    text->clear();
    TextTool::reset();
}

void TextBinaryTool::onDone()
{
    TextBinaryRequest binary;
    binary.title = title->content();
    binary.text = text->content();

    send(TextRequest(TextID(), binary));
}

SizeProperties TextBinaryTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextOptionsTool::TextOptionsTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-text", [this](InterfaceWidget* parent) -> void {
        text = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        text->setPlaceholder(localize("new-text-tool-pages-text"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-options"),
            bind(&TextOptionsTool::onManageOptions, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextOptionsTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextOptionsTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextOptionsTool::reset()
{
    title->clear();
    text->clear();
    TextTool::reset();
}

void TextOptionsTool::onDone()
{
    TextOptionsRequest options;
    options.title = title->content();
    options.text = text->content();
    options.options = this->options;

    send(TextRequest(TextID(), options));
}

SizeProperties TextOptionsTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextChooseTool::TextChooseTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-choose-count", [this](InterfaceWidget* parent) -> void {
        count = new UIntEdit(
            parent,
            nullptr,
            [](u64 value) -> void {}
        );
        count->setPlaceholder(localize("new-text-tool-pages-choose-count"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-options"),
            bind(&TextChooseTool::onManageOptions, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextChooseTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextChooseTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextChooseTool::reset()
{
    title->clear();
    count->clear();
    TextTool::reset();
}

void TextChooseTool::onDone()
{
    try
    {
        TextChooseRequest choose;
        choose.title = title->content();
        choose.options = options;
        choose.count = count->value();

        send(TextRequest(TextID(), choose));
    }
    catch (const exception& e)
    {

    }
}

SizeProperties TextChooseTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextAssignValuesTool::TextAssignValuesTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-keys"),
            bind(&TextAssignValuesTool::onManageKeys, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-values"),
            bind(&TextAssignValuesTool::onManageValues, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextAssignValuesTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextAssignValuesTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextAssignValuesTool::reset()
{
    title->clear();
    keys.clear();
    values.clear();
    TextTool::reset();
}

void TextAssignValuesTool::onDone()
{
    TextAssignValuesRequest assignValues;
    assignValues.title = title->content();
    assignValues.keys = keys;
    assignValues.values = values;

    send(TextRequest(TextID(), assignValues));
}

void TextAssignValuesTool::onManageKeys()
{
    context()->interface()->addWindow<ListDialog>(
        localize("new-text-tool-pages-keys"),
        bind(&TextAssignValuesTool::keysSource, this),
        bind(&TextAssignValuesTool::onAddKey, this, placeholders::_1),
        bind(&TextAssignValuesTool::onRemoveKey, this, placeholders::_1)
    );
}

vector<string> TextAssignValuesTool::keysSource()
{
    return { keys.begin(), keys.end() };
}

void TextAssignValuesTool::onAddKey(const string& name)
{
    keys.insert(name);
}

void TextAssignValuesTool::onRemoveKey(const string& name)
{
    keys.erase(name);
}

void TextAssignValuesTool::onManageValues()
{
    context()->interface()->addWindow<ListDialog>(
        localize("new-text-tool-pages-values"),
        bind(&TextAssignValuesTool::valuesSource, this),
        bind(&TextAssignValuesTool::onAddValue, this, placeholders::_1),
        bind(&TextAssignValuesTool::onRemoveValue, this, placeholders::_1)
    );
}

vector<string> TextAssignValuesTool::valuesSource()
{
    return { values.begin(), values.end() };
}

void TextAssignValuesTool::onAddValue(const string& name)
{
    values.insert(name);
}

void TextAssignValuesTool::onRemoveValue(const string& name)
{
    values.erase(name);
}

SizeProperties TextAssignValuesTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextSpendPointsTool::TextSpendPointsTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-spend-points-count", [this](InterfaceWidget* parent) -> void {
        count = new UIntEdit(
            parent,
            nullptr,
            [](u64 value) -> void {}
        );
        count->setPlaceholder(localize("new-text-tool-pages-spend-points-count"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-prices"),
            bind(&TextSpendPointsTool::onManagePrices, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextSpendPointsTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextSpendPointsTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextSpendPointsTool::reset()
{
    title->clear();
    count->clear();
    prices.clear();
    TextTool::reset();
}

void TextSpendPointsTool::onDone()
{
    try
    {
        TextSpendPointsRequest spendPoints;
        spendPoints.title = title->content();
        spendPoints.prices = prices;
        spendPoints.points = count->value();

        send(TextRequest(TextID(), spendPoints));
    }
    catch (const exception& e)
    {

    }
}

void TextSpendPointsTool::onManagePrices()
{
    context()->interface()->addWindow<PricesDialog>(&prices);
}

SizeProperties TextSpendPointsTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextAssignPointsTool::TextAssignPointsTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-assign-points-count", [this](InterfaceWidget* parent) -> void {
        count = new UIntEdit(
            parent,
            nullptr,
            [](u64 value) -> void {}
        );
        count->setPlaceholder(localize("new-text-tool-pages-assign-points-count"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-keys"),
            bind(&TextAssignPointsTool::onManageKeys, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextAssignPointsTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextAssignPointsTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextAssignPointsTool::reset()
{
    title->clear();
    count->clear();
    keys.clear();
    TextTool::reset();
}

void TextAssignPointsTool::onDone()
{
    try
    {
        TextAssignPointsRequest assignPoints;
        assignPoints.title = title->content();
        assignPoints.keys = keys;
        assignPoints.points = count->value();

        send(TextRequest(TextID(), assignPoints));
    }
    catch (const exception& e)
    {

    }
}

void TextAssignPointsTool::onManageKeys()
{
    context()->interface()->addWindow<ListDialog>(
        localize("new-text-tool-pages-keys"),
        bind(&TextAssignPointsTool::keysSource, this),
        bind(&TextAssignPointsTool::onAddKey, this, placeholders::_1),
        bind(&TextAssignPointsTool::onRemoveKey, this, placeholders::_1)
    );
}

vector<string> TextAssignPointsTool::keysSource()
{
    return { keys.begin(), keys.end() };
}

void TextAssignPointsTool::onAddKey(const string& name)
{
    keys.insert(name);
}

void TextAssignPointsTool::onRemoveKey(const string& name)
{
    keys.erase(name);
}

SizeProperties TextAssignPointsTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextVoteTool::TextVoteTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-vote-count", [this](InterfaceWidget* parent) -> void {
        count = new UIntEdit(
            parent,
            nullptr,
            [](u64 value) -> void {}
        );
        count->setPlaceholder(localize("new-text-tool-pages-vote-count"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-options"),
            bind(&TextVoteTool::onManageOptions, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextVoteTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextVoteTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextVoteTool::reset()
{
    title->clear();
    count->clear();
    TextTool::reset();
}

void TextVoteTool::onDone()
{
    try
    {
        TextVoteRequest vote;
        vote.title = title->content();
        vote.options = { options.begin(), options.end() };
        vote.votes = count->value();

        send(TextRequest(TextID(), vote));
    }
    catch (const exception& e)
    {

    }
}

SizeProperties TextVoteTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextChooseMajorTool::TextChooseMajorTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-options"),
            bind(&TextChooseMajorTool::onManageOptions, this)
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextChooseMajorTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextChooseMajorTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextChooseMajorTool::reset()
{
    title->clear();
    options.clear();
    TextTool::reset();
}

void TextChooseMajorTool::onDone()
{
    TextChooseMajorRequest chooseMajor;
    chooseMajor.title = title->content();
    chooseMajor.options = options;

    send(TextRequest(TextID(), chooseMajor));
}

void TextChooseMajorTool::onManageOptions()
{
    context()->interface()->addWindow<MajorChoiceDialog>(&options);
}

SizeProperties TextChooseMajorTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextKnowledgeTool::TextKnowledgeTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-title", [this](InterfaceWidget* parent) -> void {
        title = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        title->setPlaceholder(localize("new-text-tool-pages-title"));
    });

    new EditorEntry(column, "new-text-tool-pages-subtitle", [this](InterfaceWidget* parent) -> void {
        subtitle = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        subtitle->setPlaceholder(localize("new-text-tool-pages-subtitle"));
    });

    new EditorEntry(column, "new-text-tool-pages-text", [this](InterfaceWidget* parent) -> void {
        text = new LineEdit(
            parent,
            nullptr,
            [](const string& text) -> void {}
        );
        text->setPlaceholder(localize("new-text-tool-pages-text"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextKnowledgeTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextKnowledgeTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextKnowledgeTool::reset()
{
    title->clear();
    subtitle->clear();
    text->clear();
    TextTool::reset();
}

void TextKnowledgeTool::onDone()
{
    TextKnowledgeRequest knowledge;
    knowledge.title = title->content();
    knowledge.subtitle = subtitle->content();
    knowledge.text = text->content();

    send(TextRequest(TextID(), knowledge));
}

SizeProperties TextKnowledgeTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

TextClearTool::TextClearTool(InterfaceWidget* parent)
    : TextTool(parent)
{
    auto column = new Column(this);

    new EditorEntry(column, "new-text-tool-pages-clear-knowledge", [this](InterfaceWidget* parent) -> void {
        knowledge = new Checkbox(
            parent,
            nullptr,
            [](bool state) -> void {}
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-manage-users"),
            bind(&TextClearTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("new-text-tool-pages-submit"),
            bind(&TextClearTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void TextClearTool::reset()
{
    knowledge->set(false);
    TextTool::reset();
}

void TextClearTool::onDone()
{
    TextClearRequest clear;
    clear.type = knowledge->checked() ? Text_Clear_All : Text_Clear_All_But_Knowledge;

    send(TextRequest(TextID(), clear));
}

SizeProperties TextClearTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}
