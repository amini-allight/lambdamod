/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "system_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "tab_view.hpp"
#include "video_settings.hpp"
#include "audio_settings.hpp"
#include "input_settings.hpp"
#include "interface_settings.hpp"
#include "script_settings.hpp"
#include "vr_settings.hpp"
#include "bindings_settings.hpp"
#include "theme_settings.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(846 * uiScale(), UIScale::tabSize().h * 9);
}

SystemSettings::SystemSettings(InterfaceView* view)
    : InterfaceWindow(view, localize("system-settings-system-settings"))
{
    START_WIDGET_SCOPE("system-settings")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-system-settings", dismiss)
    END_SCOPE

    tabs = new TabView(
        this,
        {
            localize("system-settings-video"),
            localize("system-settings-audio"),
            localize("system-settings-input"),
            localize("system-settings-interface"),
            localize("system-settings-script"),
            localize("system-settings-vr"),
            localize("system-settings-bindings"),
            localize("system-settings-theme")
        },
        true
    );

    new VideoSettings(tabs->tab(localize("system-settings-video")));

    new AudioSettings(tabs->tab(localize("system-settings-audio")));

    new InputSettings(tabs->tab(localize("system-settings-input")));

    new InterfaceSettings(tabs->tab(localize("system-settings-interface")));

    new ScriptSettings(tabs->tab(localize("system-settings-script")));

    new VRSettings(tabs->tab(localize("system-settings-vr")));

    new BindingsSettings(tabs->tab(localize("system-settings-bindings")));

    new ThemeSettings(tabs->tab(localize("system-settings-theme")));

    resize(windowSize());
}

void SystemSettings::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());
}

void SystemSettings::open(const Point& point, const string& tabName)
{
    open(point);

    tabs->open(tabName);
}

void SystemSettings::dismiss(const Input& input)
{
    hide();
}

SizeProperties SystemSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
