/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud_preview.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"
#include "hud.hpp"

HUDPreview::HUDPreview(
    InterfaceWidget* parent,
    EntityID id,
    const function<void(size_t)>& onSelect
)
    : InterfaceWidget(parent)
    , id(id)
    , onSelect(onSelect)
    , transformMode(HUD_Transform_None)
    , transformSubMode(HUD_Transform_Sub_None)
{
    START_WIDGET_SCOPE("hud-preview")
        WIDGET_SLOT("interact", select)
        WIDGET_SLOT("translate", translate)
        WIDGET_SLOT("rotate", rotate)
        WIDGET_SLOT("scale", scale)

        START_BLOCKING_SCOPE("tool", transformMode != HUD_Transform_None)
            WIDGET_SLOT("toggle-x", toggleX)
            WIDGET_SLOT("toggle-y", toggleY)
            WIDGET_SLOT("use-tool", useTool)
            WIDGET_SLOT("cancel-tool", cancelTool)
            WIDGET_SLOT("end-tool", endTool)
        END_SCOPE
    END_SCOPE

    hud = new HUD(this, bind(&HUDPreview::hudSource, this));
}

void HUDPreview::open(const string& name)
{
    this->name = name;
}

void HUDPreview::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.worldBackgroundColor
    );

    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    hud->draw(ctx);

    for (const auto& [ name, element ] : entity->hudElements())
    {
        if (name == this->name)
        {
            auto [ point, size ] = elementExtent(element);

            drawBorder(
                ctx,
                this,
                point - Point(UIScale::marginSize()),
                size + Size(UIScale::marginSize() * 2),
                UIScale::marginSize(),
                theme.accentColor
            );
        }
    }

    if (transformMode == HUD_Transform_Translate)
    {
        if (transformSubMode == HUD_Transform_Sub_X)
        {
            drawSolid(
                ctx,
                this,
                Point(0, size().h / 2),
                Size(size().w, 1),
                theme.xAxisColor
            );
        }
        else if (transformSubMode == HUD_Transform_Sub_Y)
        {
            drawSolid(
                ctx,
                this,
                Point(size().w / 2, 0),
                Size(1, size().h),
                theme.yAxisColor
            );
        }
    }
}

tuple<Point, Size> HUDPreview::elementExtent(const HUDElement& element) const
{
    Point point;
    Size size;

    switch (element.type())
    {
    case HUD_Element_Line :
    {
        const HUDElementLine& line = element.get<HUDElementLine>();

        Point start = vec2(-(line.length * this->size().w) / 2, 0).rotate(line.rotation);
        Point end = vec2(+(line.length * this->size().w) / 2, 0).rotate(line.rotation);

        size = Size(
            start.x < end.x ? end.x - start.x : start.x - end.x,
            start.y < end.y ? end.y - start.y : start.y - end.y
        );
        break;
    }
    case HUD_Element_Text :
    {
        const HUDElementText& text = element.get<HUDElementText>();

        TextStyle style;
        style.font = Text_Sans_Serif;
        style.weight = Text_Bold;
        style.size = UIScale::hudFontSize(hud);
        style.color = Color(element.color());

        size = Size(
            style.getFont()->width(text.text) + (UIScale::marginSize() * 2),
            style.getFont()->height() + (UIScale::marginSize() * 2)
        );
        break;
    }
    case HUD_Element_Bar :
    {
        const HUDElementBar& bar = element.get<HUDElementBar>();

        size = Size(
            bar.length * this->size().w,
            UIScale::hudBarHeight(hud)
        );
        break;
    }
    case HUD_Element_Symbol :
    {
        const HUDElementSymbol& symbol = element.get<HUDElementSymbol>();

        size = Size(
            symbol.size * this->size().w,
            symbol.size * this->size().w
        );
        break;
    }
    }

    point = anchorToTopLeft(element.anchor(), this->size().toVec2(), element.position(), size.toVec2());

    point -= Point(UIScale::marginSize());
    size += Size(UIScale::marginSize() * 2);

    return { point, size };
}

void HUDPreview::transform(const Point& pointer)
{
    switch (transformMode)
    {
    case HUD_Transform_None :
        return;
    case HUD_Transform_Translate :
    {
        Point delta = pointer - initialPointer;

        if (transformSubMode == HUD_Transform_Sub_X)
        {
            delta.y = 0;
        }
        else if (transformSubMode == HUD_Transform_Sub_Y)
        {
            delta.x = 0;
        }

        translate(initialState.position() + vec2(static_cast<f64>(delta.x) / size().w, static_cast<f64>(delta.y) / size().h));
        break;
    }
    case HUD_Transform_Rotate :
    {
        Point initialLocal = initialPointer - position();
        Point currentLocal = pointer - position();

        vec2 initial = (initialState.position() - vec2(static_cast<f64>(initialLocal.x) / size().w, static_cast<f64>(initialLocal.y) / size().h)).normalize();
        vec2 current = (initialState.position() - vec2(static_cast<f64>(currentLocal.x) / size().w, static_cast<f64>(currentLocal.y) / size().h)).normalize();

        rotate(initialState.get<HUDElementLine>().rotation + handedAngleBetween(initial, current));
        break;
    }
    case HUD_Transform_Scale :
    {
        Point delta = pointer - initialPointer;

        switch (initialState.type())
        {
        default :
            break;
        case HUD_Element_Line :
            scale(initialState.get<HUDElementLine>().length + (delta.x / static_cast<f64>(size().w)));
            break;
        case HUD_Element_Symbol :
            scale(initialState.get<HUDElementSymbol>().size + (delta.x / static_cast<f64>(size().w)));
            break;
        }
        break;
    }
    }
}

void HUDPreview::translate(const vec2& position)
{
    HUDElement element = currentElement();
    element.setPosition(position);

    updateEntity(element);
}

void HUDPreview::rotate(f64 rotation)
{
    HUDElement element = currentElement();
    element.get<HUDElementLine>().rotation = rotation;

    updateEntity(element);
}

void HUDPreview::scale(f64 scale)
{
    HUDElement element = currentElement();

    switch (element.type())
    {
    default :
        break;
    case HUD_Element_Line :
        element.get<HUDElementLine>().length = scale;
        break;
    case HUD_Element_Symbol :
        element.get<HUDElementSymbol>().size = scale;
        break;
    }

    updateEntity(element);
}

void HUDPreview::startTransform(HUDTransformMode mode, const Point& pointer)
{
    transformMode = mode;
    transformSubMode = HUD_Transform_Sub_None;
    initialPointer = pointer;
    initialState = currentElement();
}

void HUDPreview::endTransform()
{
    transformMode = HUD_Transform_None;
    transformSubMode = HUD_Transform_Sub_None;
}

void HUDPreview::cancelTransform()
{
    switch (transformMode)
    {
    case HUD_Transform_None :
        break;
    case HUD_Transform_Translate :
        translate(initialState.position());
        break;
    case HUD_Transform_Rotate :
        rotate(initialState.get<HUDElementLine>().rotation);
        break;
    case HUD_Transform_Scale :
        switch (initialState.type())
        {
        default :
            break;
        case HUD_Element_Line :
            scale(initialState.get<HUDElementLine>().length);
            break;
        case HUD_Element_Symbol :
            scale(initialState.get<HUDElementSymbol>().size);
            break;
        }
        break;
    }
    endTransform();
}

void HUDPreview::updateEntity(const HUDElement& element)
{
    context()->controller()->edit([this, element]() -> void {
        Entity* entity = context()->controller()->selfWorld()->get(id);

        if (!entity)
        {
            return;
        }

        entity->addHUDElement(name, element);
    });
}

const HUDElement& HUDPreview::currentElement() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const HUDElement empty;
        return empty;
    }

    auto it = entity->hudElements().find(name);

    if (it == entity->hudElements().end())
    {
        static const HUDElement empty;
        return empty;
    }

    return it->second;
}

const map<string, HUDElement>& HUDPreview::hudSource() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const map<string, HUDElement> empty;
        return empty;
    }

    return entity->hudElements();
}

void HUDPreview::select(const Input& input)
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    size_t i = 0;
    for (const auto& [ name, element ] : entity->hudElements())
    {
        auto [ point, size ] = elementExtent(element);

        if (contains(point, size, pointer))
        {
            onSelect(i);
            break;
        }

        i++;
    }
}

void HUDPreview::translate(const Input& input)
{
    startTransform(HUD_Transform_Translate, pointer);
}

void HUDPreview::rotate(const Input& input)
{
    startTransform(HUD_Transform_Rotate, pointer);
}

void HUDPreview::scale(const Input& input)
{
    startTransform(HUD_Transform_Scale, pointer);
}

void HUDPreview::toggleX(const Input& input)
{
    transformSubMode = transformSubMode != HUD_Transform_Sub_X
        ? HUD_Transform_Sub_X
        : HUD_Transform_Sub_None;
}

void HUDPreview::toggleY(const Input& input)
{
    transformSubMode = transformSubMode != HUD_Transform_Sub_Y
        ? HUD_Transform_Sub_Y
        : HUD_Transform_Sub_None;
}

void HUDPreview::useTool(const Input& input)
{
    transform(pointer);
}

void HUDPreview::cancelTool(const Input& input)
{
    cancelTransform();
}

void HUDPreview::endTool(const Input& input)
{
    endTransform();
}

SizeProperties HUDPreview::sizeProperties() const
{
    return sizePropertiesFillAll;
}
