/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script_node.hpp"

class ScriptNodeFormat
{
public:
    ScriptNodeFormat();
    explicit ScriptNodeFormat(const ScriptContext* ctx, const vector<Value>& values);
    explicit ScriptNodeFormat(const ScriptContext* ctx, const string& code);
    ScriptNodeFormat(const ScriptNodeFormat& rhs);
    ScriptNodeFormat(ScriptNodeFormat&& rhs);
    ~ScriptNodeFormat();

    ScriptNodeFormat& operator=(const ScriptNodeFormat& rhs);
    ScriptNodeFormat& operator=(ScriptNodeFormat&& rhs);

    bool operator==(const ScriptNodeFormat& rhs) const;
    bool operator!=(const ScriptNodeFormat& rhs) const;
    bool operator>(const ScriptNodeFormat& rhs) const;
    bool operator<(const ScriptNodeFormat& rhs) const;
    bool operator>=(const ScriptNodeFormat& rhs) const;
    bool operator<=(const ScriptNodeFormat& rhs) const;
    strong_ordering operator<=>(const ScriptNodeFormat& rhs) const;

    const set<shared_ptr<ScriptNode>>& nodes() const;
    set<shared_ptr<ScriptNode>> allNodes() const;
    shared_ptr<ScriptNode> find(ScriptNodeID id) const;
    void traverse(const function<void(shared_ptr<ScriptNode>, size_t, shared_ptr<ScriptNode>)>& behavior) const;

    // Updates ID
    ScriptNodeID addNew(const ScriptContext* ctx, ScriptNodeType type, const string& name, const ivec2& position);
    set<ScriptNodeID> add(shared_ptr<ScriptNode> node);
    void remove(ScriptNodeID id);

    vector<Value> values() const;
    string code() const;

private:
    ScriptNodeID nextID;
    set<shared_ptr<ScriptNode>> _nodes;
};
