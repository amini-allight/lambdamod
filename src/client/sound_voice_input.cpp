/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_voice_input.hpp"
#include "sound_constants.hpp"
#include "tools.hpp"
#include "controller.hpp"

SoundVoiceInput::SoundVoiceInput(Controller* controller)
    : controller(controller)
{

}

void SoundVoiceInput::step()
{
    read();

    output();
}

void SoundVoiceInput::read()
{
    u32 count = sizeof(data) / sizeof(i16);
    controller->soundDevice()->read(data, &count);

    buffer += string(reinterpret_cast<const char*>(data), count * sizeof(i16));
}

void SoundVoiceInput::output()
{
    size_t chunkSize = voiceChunkSamplesPerChannel * voiceChannels * sizeof(i16);

    while (buffer.size() >= chunkSize)
    {
        vector<i16> chunk(voiceChunkSamplesPerChannel * voiceChannels, 0);

        memcpy(chunk.data(), buffer.data(), chunkSize);
        buffer = buffer.substr(chunkSize, buffer.size() - chunkSize);

        controller->onVoice(chunk);
    }
}
