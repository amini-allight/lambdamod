/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tree_view.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tree_view_node.hpp"
#include "control_context.hpp"

TreeView::TreeView(
    InterfaceWidget* parent,
    const optional<Color>& color
)
    : Layout(parent)
    , color(color)
    , panIndex(0)
    , scrollIndex(0)
{
    START_WIDGET_SCOPE("tree-view")
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("pan-left", panLeft)
        WIDGET_SLOT("pan-right", panRight)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)
    END_SCOPE
}

void TreeView::move(const Point& position)
{
    this->position(position);

    Point p = position;

    p.x += panIndex * UIScale::treeViewNodeIndent();
    p.y += scrollIndex * UIScale::treeViewNodeHeight();

    for (InterfaceWidget* child : children())
    {
        auto node = dynamic_cast<TreeViewNode*>(child);

        node->move(p);

        p.y += node->size().h;
    }
}

void TreeView::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        i32 w = 0;

        switch (sizePropertiesOf(child).horizontal)
        {
        case Scaling_Fixed :
            w = sizePropertiesOf(child).w;
            break;
        case Scaling_Fraction :
            w = sizePropertiesOf(child).w * size.w;
            break;
        case Scaling_Fill :
            w = size.w;
            break;
        }

        i32 h = 0;

        switch (sizePropertiesOf(child).vertical)
        {
        case Scaling_Fixed :
            h = sizePropertiesOf(child).h;
            break;
        case Scaling_Fraction :
            h = sizePropertiesOf(child).h * size.h;
            break;
        case Scaling_Fill :
            h = size.h;
            break;
        }

        child->resize({ w, h });
    }
}

void TreeView::draw(const DrawContext& ctx) const
{
    if (color)
    {
        drawSolid(
            ctx,
            this,
            *color
        );
    }

    InterfaceWidget::draw(ctx);
}

size_t TreeView::maxPanIndex() const
{
    i32 maxColumns = 0;

    for (InterfaceWidget* child : children())
    {
        i32 columns = dynamic_cast<TreeViewNode*>(child)->columnCount();

        if (columns > maxColumns)
        {
            maxColumns = columns;
        }
    }

    return max(maxColumns - (size().w / UIScale::treeViewPanIncrement()), 0);
}

size_t TreeView::maxScrollIndex() const
{
    i32 rows = 0;

    for (InterfaceWidget* child : children())
    {
        rows += dynamic_cast<TreeViewNode*>(child)->rowCount();
    }

    return max(rows - (size().h / UIScale::treeViewScrollIncrement()), 0);
}

void TreeView::scrollUp(const Input& input)
{
    if (scrollIndex > 0)
    {
        scrollIndex--;
        update();
    }
}

void TreeView::scrollDown(const Input& input)
{
    if (scrollIndex < maxScrollIndex())
    {
        scrollIndex++;
        update();
    }
}

void TreeView::panLeft(const Input& input)
{
    if (panIndex > 0)
    {
        panIndex--;
        update();
    }
}

void TreeView::panRight(const Input& input)
{
    if (panIndex < maxPanIndex())
    {
        panIndex++;
        update();
    }
}

void TreeView::goToStart(const Input& input)
{
    scrollIndex = 0;
    update();
}

void TreeView::goToEnd(const Input& input)
{
    scrollIndex = maxScrollIndex();
    update();
}

SizeProperties TreeView::sizeProperties() const
{
    return sizePropertiesFillAll;
}
