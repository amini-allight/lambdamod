/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "system_hud.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "global.hpp"

SystemHUD::SystemHUD(InterfaceView* view)
    : InterfaceWidget(view)
{

}

void SystemHUD::draw(const DrawContext& ctx) const
{
    if (context()->controller()->standby())
    {
        return;
    }

    Point offset = Point(UIScale::marginSize(this) * 2, ctx.size.h * 0.25);

    if (!context()->hudShown())
    {
        return;
    }

    TextStyle style(this);
    style.weight = Text_Bold;
    style.size = g_vr ? UIScale::largeFontSize(this) : UIScale::mediumFontSize(this);

    i32 rowHeight = style.getFont()->height() * 1.5;

    if (context()->attached())
    {
        drawText(
            ctx,
            this,
            offset,
            Size(UIScale::panelHUDTextWidth(this), rowHeight),
            localize("system-hud-attached"),
            style
        );

        drawText(
            ctx,
            this,
            offset + Point(0, rowHeight),
            Size(UIScale::panelHUDTextWidth(this), rowHeight),
            localize("system-hud-magnitude", to_string(context()->controller()->game().magnitude()), to_string(context()->controller()->game().magnitudeLimit())),
            style
        );

        if (const User* user = context()->controller()->self(); user)
        {
            drawText(
                ctx,
                this,
                offset + Point(0, rowHeight * 2),
                Size(UIScale::panelHUDTextWidth(this), rowHeight),
                localize("system-hud-advantage", (user->advantage() > 0 ? "+" : "") + to_string(user->advantage())),
                style
            );

            if (user->doom())
            {
                drawText(
                    ctx,
                    this,
                    offset + Point(0, rowHeight * 3),
                    Size(UIScale::panelHUDTextWidth(this), rowHeight),
                    localize("system-hud-doom"),
                    style
                );
            }
        }

        if (context()->controller()->game().attachmentsLocked())
        {
            drawText(
                ctx,
                this,
                offset + Point(0, rowHeight * 4),
                Size(UIScale::panelHUDTextWidth(this), rowHeight),
                localize("system-hud-attachments-locked"),
                style
            );
        }

        if (context()->controller()->game().braking())
        {
            drawText(
                ctx,
                this,
                offset + Point(0, rowHeight * 5),
                Size(UIScale::panelHUDTextWidth(this), rowHeight),
                localize("system-hud-braking"),
                style
            );
        }
    }
}

SizeProperties SystemHUD::sizeProperties() const
{
    return sizePropertiesFillAll;
}
