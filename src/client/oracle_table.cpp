/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "oracle_table.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "control_context.hpp"

OracleTable::OracleTable(
    InterfaceWidget* parent,
    const function<Oracle()>& source
)
    : InterfaceWidget(parent)
    , panIndex(0)
    , scrollIndex(0)
    , rollTime(0)
    , source(source)
{
    START_WIDGET_SCOPE("oracle-table")
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("pan-left", panLeft)
        WIDGET_SLOT("pan-right", panRight)
    END_SCOPE
}

void OracleTable::step()
{
    Oracle newOracle = source();

    if (newOracle != oracle)
    {
        oracle = newOracle;
        rolls.clear();
    }
}

void OracleTable::draw(const DrawContext& ctx) const
{
    TextStyle style;
    style.alignment = Text_Center;

    for (i32 i = panIndex; i < static_cast<i32>(oracle.columns.size()); i++)
    {
        i32 x = i - panIndex;

        const OracleColumn& column = oracle.columns[i];

        TextStyle titleStyle;
        titleStyle.alignment = Text_Center;
        titleStyle.weight = Text_Bold;

        drawText(
            ctx,
            this,
            Point(UIScale::oracleTableColumnWidth() * x, 0),
            Size(UIScale::oracleTableColumnWidth(), UIScale::oracleTableRowHeight()),
            column.title,
            titleStyle
        );

        for (i32 j = scrollIndex; j < static_cast<i32>(column.options.size()); j++)
        {
            i32 y = j - scrollIndex;

            const string& option = column.options[j];

            drawSolid(
                ctx,
                this,
                Point(UIScale::oracleTableColumnWidth() * x, UIScale::oracleTableRowHeight() * (y + 1)),
                Size(UIScale::oracleTableColumnWidth(), 1),
                theme.altBackgroundColor
            );

            drawText(
                ctx,
                this,
                Point(UIScale::oracleTableColumnWidth() * x, UIScale::oracleTableRowHeight() * (y + 1)),
                Size(UIScale::oracleTableColumnWidth(), UIScale::oracleTableRowHeight()),
                option,
                style
            );
        }

        drawSolid(
            ctx,
            this,
            Point(UIScale::oracleTableColumnWidth() * (x + 1), 0),
            Size(UIScale::dividerWidth(), size().h),
            theme.dividerColor
        );
    }

    for (i32 i = panIndex; i < static_cast<i32>(oracle.columns.size()); i++)
    {
        i32 x = i - panIndex;

        const OracleColumn& column = oracle.columns[i];

        for (i32 j = scrollIndex; j < static_cast<i32>(column.options.size()); j++)
        {
            i32 y = j - scrollIndex;

            if (!rolls.empty() && static_cast<i32>(rolls[i]) == j)
            {
                Color color = theme.accentColor;
                color.a *= min((currentTime() - rollTime).count() / static_cast<f64>(oracleTableFadeDuration.count()), 1.0);

                drawBorder(
                    ctx,
                    this,
                    Point(UIScale::oracleTableColumnWidth() * x, UIScale::oracleTableRowHeight() * (y + 1)),
                    Size(UIScale::oracleTableColumnWidth(), UIScale::oracleTableRowHeight()),
                    UIScale::marginSize(),
                    color
                );
            }
        }
    }
}

void OracleTable::roll()
{
    rollTime = currentTime();

    random_device device;
    mt19937 engine(device());

    rolls.clear();

    for (size_t i = 0; i < oracle.columns.size(); i++)
    {
        uniform_int_distribution<size_t> distribution(0, oracle.columns[i].options.size() - 1);

        rolls.push_back(distribution(engine));
    }
}

void OracleTable::scrollUp(const Input& input)
{
    if (scrollIndex != 0)
    {
        scrollIndex--;
    }
}

void OracleTable::scrollDown(const Input& input)
{
    i32 rowCount = 0;

    for (const OracleColumn& column : oracle.columns)
    {
        if (static_cast<i32>(column.options.size()) > rowCount)
        {
            rowCount = column.options.size();
        }
    }

    if (scrollIndex < rowCount - (size().h / UIScale::oracleTableRowHeight()))
    {
        scrollIndex++;
    }
}

void OracleTable::panLeft(const Input& input)
{
    if (panIndex != 0)
    {
        panIndex--;
    }
}

void OracleTable::panRight(const Input& input)
{
    if (panIndex < static_cast<i32>(oracle.columns.size()) - (size().w / UIScale::oracleTableColumnWidth()))
    {
        panIndex++;
    }
}

SizeProperties OracleTable::sizeProperties() const
{
    return sizePropertiesFillAll;
}
