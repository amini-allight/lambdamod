/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "sound_source.hpp"
#include "sound_effect.hpp"

enum SoundOneshotType : u8
{
    Sound_Oneshot_Startup,
    Sound_Oneshot_Ping,
    Sound_Oneshot_Brake,
    Sound_Oneshot_Chat,
    Sound_Oneshot_Error,
    Sound_Oneshot_Title,
    Sound_Oneshot_Attach,
    Sound_Oneshot_Detach,
    Sound_Oneshot_Focus,
    Sound_Oneshot_Unfocus,
    Sound_Oneshot_Positive_Activate,
    Sound_Oneshot_Negative_Activate
};

class Sound;

class SoundOneshot final : public SoundSource
{
public:
    SoundOneshot(Sound* sound, SoundEffect* soundEffect, const vec3& position);

    bool completed() const;

private:
    SoundEffect* soundEffect;
    i64 readHead;

    variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> pull(f64 multiplier) override;
};
