/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_atmosphere.hpp"

RenderFlatAtmosphere::RenderFlatAtmosphere(
    const RenderInterface* render,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    const RenderFlatPipelineStore* pipelineStore
)
    : RenderAtmosphere(
        render,
        descriptorSetLayout,
        pipelineStore->atmosphereLightning.pipelineLayout,
        pipelineStore->atmosphereLightning.pipeline,
        pipelineStore->atmosphereLine.pipelineLayout,
        pipelineStore->atmosphereLine.pipeline,
        pipelineStore->atmosphereSolid.pipelineLayout,
        pipelineStore->atmosphereSolid.pipeline,
        pipelineStore->atmosphereCloud.pipelineLayout,
        pipelineStore->atmosphereCloud.pipeline,
        pipelineStore->atmosphereStreamer.pipelineLayout,
        pipelineStore->atmosphereStreamer.pipeline,
        pipelineStore->atmosphereWarp.pipelineLayout,
        pipelineStore->atmosphereWarp.pipeline
    )
    , swapchainElements(swapchainElements)
{
    createComponents();
}

RenderFlatAtmosphere::~RenderFlatAtmosphere()
{
    destroyComponents();
}

void RenderFlatAtmosphere::work(
    const FlatSwapchainElement* swapchainElement,
    const AtmosphereParameters& atmosphere,
    const vec3& cameraPosition,
    const vec3& up,
    const vec3& gravity,
    const vec3& flowVelocity,
    f64 density
)
{
    if (!atmosphere.enabled)
    {
        return;
    }

    vec3 cameraPositions[eyeCount]{
        cameraPosition,
        cameraPosition
    };

    update(
        swapchainElement,
        atmosphere,
        cameraPositions,
        up,
        gravity,
        flowVelocity,
        density
    );

    fillCommandBuffer(
        swapchainElement,
        atmosphere,
        gravity,
        density
    );
}

void RenderFlatAtmosphere::refresh(const vector<FlatSwapchainElement*>& swapchainElements)
{
    this->swapchainElements = swapchainElements;

    destroyComponents();

    createComponents();
}

void RenderFlatAtmosphere::createComponents()
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        components.push_back(new RenderAtmosphereComponent(
            render,
            descriptorSetLayout,
            swapchainElements.at(i)->camera(),
            lightningVertices
        ));
    }
}
