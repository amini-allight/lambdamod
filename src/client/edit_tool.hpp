/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_types.hpp"
#include "edit_tool_axis.hpp"
#include "edit_tool_pivot.hpp"

class ViewerModeContext;

class EditTool
{
public:
    EditTool(
        ViewerModeContext* ctx,
        const mat4& localSpace,
        const function<void(size_t, const vec3&, const mat4&, const mat4&)>& setGlobal,
        const function<mat4(size_t)>& getGlobal,
        const function<mat4(size_t)>& getGlobalForPivot,
        const function<size_t()>& count,
        bool soft
    );
    EditTool(const EditTool& rhs) = delete;
    EditTool(EditTool&& rhs) = delete;
    virtual ~EditTool() = 0;

    EditTool& operator=(const EditTool& rhs) = delete;
    EditTool& operator=(EditTool&& rhs) = delete;

    virtual string name() const = 0;
    string axisName() const;
    Color axisColor() const;
    bool axisX() const;
    bool axisY() const;
    bool axisZ() const;
    string pivotName() const;
    bool pivotMeanPoint() const;
    bool pivotCursor() const;
    bool pivotIndividualOrigins() const;
    virtual string extent() const = 0;

    virtual void use(const Point& point) = 0;
    virtual void update() = 0;

    virtual void cancel() = 0;
    virtual void end() = 0;

    bool soft() const;

    bool snapping() const;
    void startSnap();
    void endSnap();

    bool transformingSubpart() const;

    virtual void togglePivotMode(const Point& point);

    void toggleX();
    void toggleY();
    void toggleZ();
    void setAxis(EditToolAxis axis);

    void addNumericInput(char c);
    void removeNumericInput();
    void toggleNumericInputSign();
    void ensureNumericInputPeriod();

protected:
    ViewerModeContext* ctx;
    mat4 localSpace;
    function<void(size_t, const vec3&, const mat4&, const mat4&)> setGlobal;
    function<mat4(size_t)> getGlobal;
    function<mat4(size_t)> getGlobalWithPromotion;
    function<size_t()> count;
    bool _soft;
    vector<mat4> initialGlobalTransforms;
    vector<mat4> initialGlobalTransformsWithPromotion;
    vec3 initialGlobalCenter;
    bool _snapping;
    EditToolAxis toolAxis;
    EditToolPivot toolPivot;
    bool numericInput;
    string numericInputText;
    RenderDecorationID axisID;

    void updateDecoration();

    mat4 initialGlobal(size_t i) const;
    virtual optional<vec3> globalAxis() const = 0;
    vec3 globalPivot() const;
    vec3 globalPivot(size_t index) const;

    f64 numericInputValue() const;

    Point getOnScreenCenter() const;
};
