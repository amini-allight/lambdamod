/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "widget_input_scope.hpp"
#include "direct_input_interface_widget.hpp"

class DirectWidgetInputScope : public WidgetInputScope
{
public:
    DirectWidgetInputScope(
        DirectInputInterfaceWidget* widget,
        InputScope* parent,
        const string& name,
        InputScopeMode mode,
        const function<bool(const Input&)>& isActive,
        const function<void(InputScope*)>& block
    );

    InputHandlingResult handle(const Input& input, u32 repeats, size_t depth = 1) const override;
};

#define START_DIRECT_WIDGET_INPUT_SCOPE(_parent, _name, _expr) \
setScope<DirectWidgetInputScope>(this, _parent, _name, Input_Scope_Normal, [&](const Input& input)-> bool { return _expr; }, [&](InputScope* parent) -> void {
