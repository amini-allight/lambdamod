/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_constants.hpp"
#include "interface_widget.hpp"
#include "row.hpp"
#include "icon_button.hpp"
#include "spacer.hpp"
#include "tribool.hpp"

template<typename T>
class OptionalField : public InterfaceWidget
{
public:
    typedef function<optional<T>()> OptionalSource;
    typedef function<void(const optional<T>&)> OnOptional;
    typedef function<T()> ValueSource;
    typedef function<void(const T&)> OnValue;
    typedef function<void(InterfaceWidget*, const ValueSource&, const OnValue&)> CreateWidget;

    OptionalField(
        InterfaceWidget* parent,
        const OptionalSource& optionalSource,
        const OnOptional& onOptional,
        const CreateWidget& createWidget,
        const T& defaultValue = T()
    )
        : InterfaceWidget(parent)
        , optionalSource(optionalSource)
        , onOptional(onOptional)
        , createWidget(createWidget)
        , defaultValue(defaultValue)
    {
        updateChildren();
    }

    void step() override
    {
        updateChildren();

        InterfaceWidget::step();
    }

private:
    OptionalSource optionalSource;
    OnOptional onOptional;
    CreateWidget createWidget;
    T defaultValue;
    tribool enabled;

    void updateChildren()
    {
        optional<T> opt = optionalSource();

        if (opt && !enabled.truthy())
        {
            clearChildren();

            auto row = new Row(this);

            auto valueSource = [this]() -> T { return *optionalSource(); };
            auto onValue = [this](const T& value) -> void { onOptional(value); };
            createWidget(row, valueSource, onValue);

            new IconButton(row, Icon_Remove, bind(&OptionalField::onDisable, this));

            enabled = true;

            update();
        }
        else if (!opt && !enabled.falsey())
        {
            clearChildren();

            auto row = new Row(this);

            MAKE_SPACER(row, 0, 0);

            new IconButton(row, Icon_Add, bind(&OptionalField::onEnable, this));

            MAKE_SPACER(row, 0, 0);

            enabled = false;

            update();
        }
    }

    void onEnable()
    {
        onOptional(defaultValue);
        updateChildren();
    }

    void onDisable()
    {
        onOptional({});
        updateChildren();
    }

    SizeProperties sizeProperties() const override
    {
        return {
            0, static_cast<f64>(UIScale::labelHeight()),
            Scaling_Fill, Scaling_Fixed
        };
    }
};
