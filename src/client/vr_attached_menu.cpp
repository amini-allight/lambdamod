/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_attached_menu.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "vr_render.hpp"
#include "panel_transforms.hpp"
#include "fixed_row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "vr_menu_button.hpp"
#include "vr_menu_dynamic_button.hpp"

static constexpr chrono::seconds orientDelay(3);

VRAttachedMenu::VRAttachedMenu(InterfaceView* view)
    : VRMenu(view)
    , orienting(false)
{
    auto column = new Column(this);

    MAKE_SPACER(column, 0, 0);

    auto firstRow = new FixedRow(column, UIScale::panelVRMenuButtonSize(this).h);

    new VRMenuButton(
        firstRow,
        localize("vr-attached-menu-disconnect"),
        bind(&VRAttachedMenu::onDisconnect, this)
    );

    new VRMenuButton(
        firstRow,
        localize("vr-attached-menu-quit"),
        bind(&VRAttachedMenu::onQuit, this)
    );

    auto secondRow = new FixedRow(column, UIScale::panelVRMenuButtonSize(this).h);

    new VRMenuButton(
        secondRow,
        localize("vr-attached-menu-detach"),
        bind(&VRAttachedMenu::onDetach, this)
    );

    auto magnitudeColumn = new Column(secondRow);

    for (u32 i = 0; i < 4; i++)
    {
        new VRMenuButton(
            magnitudeColumn,
            localize("vr-attached-menu-magnitude", to_string(i)),
            bind(&VRAttachedMenu::onMagnitude, this, i),
            [this, i]() -> bool {
                return context()->controller()->self()->magnitude() == i;
            }
        );
    }

    new VRMenuButton(
        secondRow,
        localize("vr-attached-menu-ping"),
        bind(&VRAttachedMenu::onPing, this)
    );

    new VRMenuButton(
        secondRow,
        localize("vr-attached-menu-brake"),
        bind(&VRAttachedMenu::onBrake, this)
    );

    new VRMenuDynamicButton(
        secondRow,
        [this]()-> string {
            return context()->controller()->self()->anchored()
                ? localize("vr-attached-menu-unanchor")
                : localize("vr-attached-menu-anchor");
        },
        bind(&VRAttachedMenu::onToggleAnchor, this)
    );

    auto scaleColumn = new Column(secondRow);

    new VRMenuButton(
        scaleColumn,
        localize("vr-attached-menu-scale-up"),
        bind(&VRAttachedMenu::onScaleUp, this)
    );

    new VRMenuDynamicButton(
        scaleColumn,
        [this]() -> string { return toPrettyString(context()->attachedMode()->vrScale()); },
        []() -> void {}
    );

    new VRMenuButton(
        scaleColumn,
        localize("vr-attached-menu-scale-down"),
        bind(&VRAttachedMenu::onScaleDown, this)
    );

    new VRMenuButton(
        secondRow,
        localize("vr-attached-menu-recenter"),
        bind(&VRAttachedMenu::onRecenter, this)
    );

    new VRMenuDynamicButton(
        secondRow,
        [this]() -> string {
            return orienting
                ? to_string(chrono::duration_cast<chrono::seconds>(orientDelay - (currentTime() - orientStartTime)).count())
                : localize("vr-attached-menu-orient-room");
        },
        bind(&VRAttachedMenu::onOrientRoom, this)
    );

    new VRMenuButton(
        secondRow,
        localize("vr-attached-menu-reset-room"),
        bind(&VRAttachedMenu::onResetRoom, this)
    );

    new VRMenuButton(
        secondRow,
        localize("vr-attached-menu-knowledge"),
        bind(&VRAttachedMenu::onKnowledge, this)
    );

    MAKE_SPACER(column, 0, 0);
}

void VRAttachedMenu::step()
{
    VRMenu::step();

    if (orienting && currentTime() - orientStartTime >= orientDelay)
    {
        context()->attachedMode()->orientRoom();
        orienting = false;
    }
}

bool VRAttachedMenu::shouldDraw() const
{
    return VRMenu::shouldDraw() && context()->attached() && context()->vrMenuButtonHandler()->vrMenu();
}

void VRAttachedMenu::draw(const DrawContext& ctx) const
{
    VRMenu::draw(ctx);

    drawPointer(ctx, nullptr, leftPointer);
    drawPointer(ctx, nullptr, rightPointer);
}

void VRAttachedMenu::onDisconnect()
{
    context()->vrMenuButtonHandler()->closeVRMenu();
    context()->controller()->disconnect();
}

void VRAttachedMenu::onQuit()
{
    context()->vrMenuButtonHandler()->closeVRMenu();
    context()->controller()->quit();
}

void VRAttachedMenu::onDetach()
{
    context()->vrMenuButtonHandler()->closeVRMenu();
    context()->attachedMode()->detach();
}

void VRAttachedMenu::onMagnitude(u32 magnitude)
{
    context()->setMagnitude(magnitude);
}

void VRAttachedMenu::onPing()
{
    context()->attachedMode()->ping();
}

void VRAttachedMenu::onBrake()
{
    context()->attachedMode()->brake();
}

void VRAttachedMenu::onToggleAnchor()
{
    if (!context()->controller()->self()->anchored())
    {
        context()->vrMenuButtonHandler()->closeVRMenu();
        context()->attachedMode()->enableTrackingCalibrator();
    }
    else
    {
        context()->attachedMode()->clearTrackingCalibration();
    }
}

void VRAttachedMenu::onScaleUp()
{
    context()->attachedMode()->scalePlayerUp();
}

void VRAttachedMenu::onScaleDown()
{
    context()->attachedMode()->scalePlayerDown();
}

void VRAttachedMenu::onRecenter()
{
    context()->attachedMode()->recenter();
}

void VRAttachedMenu::onOrientRoom()
{
    orienting = true;
    orientStartTime = currentTime();
}

void VRAttachedMenu::onResetRoom()
{
    context()->attachedMode()->resetRoom();
}

void VRAttachedMenu::onKnowledge()
{
    context()->vrMenuButtonHandler()->closeVRMenu();
    context()->attachedMode()->toggleKnowledgeViewer();
}
#endif
