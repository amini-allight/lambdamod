/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "stack_view.hpp"

StackView::StackView(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{

}

void StackView::remove(const string& pageName)
{
    delete pages.at(pageName);
    pages.erase(pageName);
}

void StackView::open(const string& pageName)
{
    _activePageName = pageName;
}

void StackView::close()
{
    _activePageName = "";
}

InterfaceWidget* StackView::page(const string& name) const
{
    if (pages.contains(name))
    {
        return pages.at(name)->children().front();
    }
    else
    {
        return nullptr;
    }
}

void StackView::step()
{
    if (pages.contains(_activePageName))
    {
        pages.at(_activePageName)->step();
    }
}

void StackView::draw(const DrawContext& ctx) const
{
    if (pages.contains(_activePageName))
    {
        pages.at(_activePageName)->draw(ctx);
    }
}

const string& StackView::activePageName() const
{
    return _activePageName;
}

vector<string> StackView::pageNames() const
{
    vector<string> pageNames;
    pageNames.reserve(pages.size());

    for (const auto& [ name, page ] : pages)
    {
        pageNames.push_back(name);
    }

    return pageNames;
}

InterfaceWidget* StackView::activePage() const
{
    if (!pages.empty())
    {
        return pages.at(_activePageName);
    }
    else
    {
        return nullptr;
    }
}

SizeProperties StackView::sizeProperties() const
{
    return sizePropertiesFillAll;
}
