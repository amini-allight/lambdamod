/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "script_context.hpp"

class ScriptNode;

enum ScriptNodeType : u8
{
    Script_Node_Output,
    Script_Node_Execution,
    Script_Node_Void,
    Script_Node_Symbol,
    Script_Node_Integer,
    Script_Node_Float,
    Script_Node_String,
    Script_Node_List,
    Script_Node_Lambda,
    Script_Node_Handle
};

struct ScriptNodeOutput
{
    ScriptNodeOutput();
    ScriptNodeOutput(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const optional<Value>& value
    );

    bool operator==(const ScriptNodeOutput& rhs) const;
    bool operator!=(const ScriptNodeOutput& rhs) const;
    bool operator>(const ScriptNodeOutput& rhs) const;
    bool operator<(const ScriptNodeOutput& rhs) const;
    bool operator>=(const ScriptNodeOutput& rhs) const;
    bool operator<=(const ScriptNodeOutput& rhs) const;
    strong_ordering operator<=>(const ScriptNodeOutput& rhs) const;

    shared_ptr<ScriptNode> value;
};

struct ScriptNodeExecution
{
    ScriptNodeExecution();
    ScriptNodeExecution(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeExecution& rhs) const;
    bool operator!=(const ScriptNodeExecution& rhs) const;
    bool operator>(const ScriptNodeExecution& rhs) const;
    bool operator<(const ScriptNodeExecution& rhs) const;
    bool operator>=(const ScriptNodeExecution& rhs) const;
    bool operator<=(const ScriptNodeExecution& rhs) const;
    strong_ordering operator<=>(const ScriptNodeExecution& rhs) const;

    string name;
    vector<shared_ptr<ScriptNode>> inputs;
};

struct ScriptNodeVoid
{
    ScriptNodeVoid();
    ScriptNodeVoid(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeVoid& rhs) const;
    bool operator!=(const ScriptNodeVoid& rhs) const;
    bool operator>(const ScriptNodeVoid& rhs) const;
    bool operator<(const ScriptNodeVoid& rhs) const;
    bool operator>=(const ScriptNodeVoid& rhs) const;
    bool operator<=(const ScriptNodeVoid& rhs) const;
    strong_ordering operator<=>(const ScriptNodeVoid& rhs) const;
};

struct ScriptNodeSymbol
{
    ScriptNodeSymbol();
    ScriptNodeSymbol(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeSymbol& rhs) const;
    bool operator!=(const ScriptNodeSymbol& rhs) const;
    bool operator>(const ScriptNodeSymbol& rhs) const;
    bool operator<(const ScriptNodeSymbol& rhs) const;
    bool operator>=(const ScriptNodeSymbol& rhs) const;
    bool operator<=(const ScriptNodeSymbol& rhs) const;
    strong_ordering operator<=>(const ScriptNodeSymbol& rhs) const;

    string value;
};

struct ScriptNodeInteger
{
    ScriptNodeInteger();
    ScriptNodeInteger(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeInteger& rhs) const;
    bool operator!=(const ScriptNodeInteger& rhs) const;
    bool operator>(const ScriptNodeInteger& rhs) const;
    bool operator<(const ScriptNodeInteger& rhs) const;
    bool operator>=(const ScriptNodeInteger& rhs) const;
    bool operator<=(const ScriptNodeInteger& rhs) const;
    strong_ordering operator<=>(const ScriptNodeInteger& rhs) const;

    i64 value;
};

struct ScriptNodeFloat
{
    ScriptNodeFloat();
    ScriptNodeFloat(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeFloat& rhs) const;
    bool operator!=(const ScriptNodeFloat& rhs) const;
    bool operator>(const ScriptNodeFloat& rhs) const;
    bool operator<(const ScriptNodeFloat& rhs) const;
    bool operator>=(const ScriptNodeFloat& rhs) const;
    bool operator<=(const ScriptNodeFloat& rhs) const;
    strong_ordering operator<=>(const ScriptNodeFloat& rhs) const;

    f64 value;
};

struct ScriptNodeString
{
    ScriptNodeString();
    ScriptNodeString(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeString& rhs) const;
    bool operator!=(const ScriptNodeString& rhs) const;
    bool operator>(const ScriptNodeString& rhs) const;
    bool operator<(const ScriptNodeString& rhs) const;
    bool operator>=(const ScriptNodeString& rhs) const;
    bool operator<=(const ScriptNodeString& rhs) const;
    strong_ordering operator<=>(const ScriptNodeString& rhs) const;

    string value;
};

struct ScriptNodeList
{
    ScriptNodeList();
    ScriptNodeList(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeList& rhs) const;
    bool operator!=(const ScriptNodeList& rhs) const;
    bool operator>(const ScriptNodeList& rhs) const;
    bool operator<(const ScriptNodeList& rhs) const;
    bool operator>=(const ScriptNodeList& rhs) const;
    bool operator<=(const ScriptNodeList& rhs) const;
    strong_ordering operator<=>(const ScriptNodeList& rhs) const;

    vector<shared_ptr<ScriptNode>> values;
};

struct ScriptNodeLambda
{
    ScriptNodeLambda();
    ScriptNodeLambda(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeLambda& rhs) const;
    bool operator!=(const ScriptNodeLambda& rhs) const;
    bool operator>(const ScriptNodeLambda& rhs) const;
    bool operator<(const ScriptNodeLambda& rhs) const;
    bool operator>=(const ScriptNodeLambda& rhs) const;
    bool operator<=(const ScriptNodeLambda& rhs) const;
    strong_ordering operator<=>(const ScriptNodeLambda& rhs) const;

    Lambda lambda;
};

struct ScriptNodeHandle
{
    ScriptNodeHandle();
    ScriptNodeHandle(
        const ScriptContext* ctx,
        ScriptNodeID* id,
        const ScriptNode& self,
        const Value& value,
        bool quote
    );

    bool operator==(const ScriptNodeHandle& rhs) const;
    bool operator!=(const ScriptNodeHandle& rhs) const;
    bool operator>(const ScriptNodeHandle& rhs) const;
    bool operator<(const ScriptNodeHandle& rhs) const;
    bool operator>=(const ScriptNodeHandle& rhs) const;
    bool operator<=(const ScriptNodeHandle& rhs) const;
    strong_ordering operator<=>(const ScriptNodeHandle& rhs) const;

    EntityID id;
};

typedef variant<
    ScriptNodeOutput,
    ScriptNodeExecution,
    ScriptNodeVoid,
    ScriptNodeSymbol,
    ScriptNodeInteger,
    ScriptNodeFloat,
    ScriptNodeString,
    ScriptNodeList,
    ScriptNodeLambda,
    ScriptNodeHandle
> ScriptNodeData;
