/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_interface.hpp"

#ifdef LMOD_VK_VALIDATION
void createInstance(VkInstance& instance, VkDebugUtilsMessengerEXT& debugMessenger, u32 apiVersion, const set<string>& extensionNames);
void destroyInstance(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger);
#else
void createInstance(VkInstance& instance, u32 apiVersion, const set<string>& extensionNames);
void destroyInstance(VkInstance instance);
#endif

void createSpecificDevice(
    u32 apiVersion,
    VkInstance instance,
    VkPhysicalDevice physDevice,
    const set<string>& extensionNames,
    i32& graphicsQueueIndex,
    VkQueue& graphicsQueue,
    VkDevice& device
);
void createSpecificPresentDevice(
    u32 apiVersion,
    VkInstance instance,
    VkPhysicalDevice physDevice,
    VkSurfaceKHR surface,
    const set<string>& extensionNames,
    i32& graphicsQueueIndex,
    VkQueue& graphicsQueue,
    i32& presentQueueIndex,
    VkQueue& presentQueue,
    VkDevice& device
);
void destroyDevice(VkDevice device);

void createAllocator(u32 apiVersion, VkInstance instance, VkPhysicalDevice physDevice, VkDevice device, VmaAllocator& allocator);
void destroyAllocator(VmaAllocator allocator);

void createPipelineCache(VkDevice device, VkPipelineCache& pipelineCache);
void destroyPipelineCache(VkDevice device, VkPipelineCache pipelineCache);

tuple<VkFormat, u32, u32> createSwapchain(
    VkDevice device,
    u32 width,
    u32 height,
    i32 graphicsQueueIndex,
    i32 presentQueueIndex,
    VkPhysicalDevice physDevice,
    VkSurfaceKHR surface,
    VkSwapchainKHR& swapchain
);
void destroySwapchain(
    VkDevice device,
    VkSwapchainKHR swapchain
);
size_t getSwapchainImageCount(
    VkDevice device,
    VkSwapchainKHR swapchain
);
vector<VkImage> getSwapchainImages(
    VkDevice device,
    VkSwapchainKHR swapchain
);

void createCommandPool(VkDevice device, i32 graphicsQueueIndex, VkCommandPool& commandPool);
void destroyCommandPool(VkDevice device, VkCommandPool commandPool);

void createObjectDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createLitObjectDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createTexturedObjectDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createLitTexturedObjectDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createMaterialedObjectDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createLitMaterialedObjectDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createAreaDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createLitAreaDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createTextDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createHDRDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createPostProcessDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createDepthOfFieldDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createMotionBlurDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createSkyDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void createAtmosphereDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
);
void destroyDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout descriptorSetLayout);

void createShader(VkDevice device, const string& source, VkShaderModule& shader);
void destroyShader(VkDevice device, VkShaderModule shader);

void createHUDGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule hudVertexShader,
    VkShaderModule hudFragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createTextGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule textVertexShader,
    VkShaderModule textFragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createVertexColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createLitVertexColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createTexturedColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    bool culling,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createLitTexturedColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    bool culling,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createAreaGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createLitAreaGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createRopeGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createLitRopeGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createColoredLineGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createGizmoGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createTetherIndicatorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createConfinementIndicatorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createWaitingIndicatorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createGridGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createThemeGradientGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createHDRGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createPostProcessGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createSkyGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createSkyBillboardGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createSkyAuroraGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createSkyEnvMapGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createSkyEnvMapBillboardGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createAtmosphereLightningGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createAtmosphereEffectGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void createComputePipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkShaderModule computeShader,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
);
void destroyPipeline(VkDevice device, VkPipeline pipeline, VkPipelineLayout pipelineLayout);

tuple<VkAttachmentDescription2, VkAttachmentReference2> renderPassAttachment(
    u32 index,
    VkFormat format,
    VkSampleCountFlagBits samples,
    VkAttachmentLoadOp loadOp,
    VkAttachmentStoreOp storeOp,
    VkImageLayout before,
    VkImageLayout during,
    VkImageLayout after
);
VkSubpassDescription2 renderPassSubpass();
tuple<VkSubpassDependency2, VkSubpassDependency2> renderPassDependencies(
    VkPipelineStageFlags before,
    VkAccessFlags beforeAccess,
    VkPipelineStageFlags entrance,
    VkAccessFlags entranceAccess,
    VkPipelineStageFlags exit,
    VkAccessFlags exitAccess,
    VkPipelineStageFlags after,
    VkAccessFlags afterAccess
);

void createPanelRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createSkyRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createUIRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createMonosampleMainRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createMonosampleHDRRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createMonosamplePostProcessRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createMonosampleOverlayRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createMonosampleIndependentDepthOverlayRenderPass(RenderInterface* render, VkRenderPass& renderPass);
void createMultisampleMainRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass);
void createMultisampleHDRRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass);
void createMultisamplePostProcessRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass);
void createMultisampleOverlayRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass);
void createMultisampleIndependentDepthOverlayRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass);
void destroyRenderPass(VkDevice device, VkRenderPass renderPass);

void createSampler(VkDevice device, bool filtered, VkSampler& sampler);
void destroySampler(VkDevice device, VkSampler sampler);

void createReferenceTexture(
    VkDevice device,
    VkQueue queue,
    VmaAllocator allocator,
    VkCommandPool commandPool,
    const string& name,
    VmaImage& texture
);

void createCurvedPanelBuffer(VmaAllocator allocator, VmaBuffer& hudVertexBuffer);

void createEnvironmentMappingCameraUniformBuffer(VmaAllocator allocator, VmaBuffer& environmentMapCameraUniformBuffer);

void createAreaSurfaceNoiseImage(VkDevice device, VmaAllocator allocator, VkCommandBuffer commandBuffer, VmaImage& image, VkImageView& imageView);

void createDescriptorPool(VkDevice device, size_t imageCount, VkDescriptorPool& descriptorPool);
void destroyDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool);

set<string> rayTracingDeviceExtensions(u32 apiVersion, VkPhysicalDevice physDevice);
bool rayTracingSupported(u32 apiVersion, VkPhysicalDevice physDevice);
bool rayTracingEnabled(u32 apiVersion, VkPhysicalDevice physDevice);
