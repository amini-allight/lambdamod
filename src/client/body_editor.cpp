/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_editor.hpp"
#include "body_part.hpp"
#include "localization.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "theme.hpp"
#include "column.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "editor_optional_entry.hpp"
#include "editor_multi_entry.hpp"
#include "optional_field.hpp"
#include "radio_bar.hpp"
#include "label.hpp"
#include "line_edit.hpp"
#include "vec2_edit.hpp"
#include "vec3_edit.hpp"
#include "uint_edit.hpp"
#include "rotation_edit.hpp"
#include "float_edit.hpp"
#include "ufloat_edit.hpp"
#include "option_select.hpp"
#include "emissive_color_picker.hpp"
#include "hdr_color_picker.hpp"
#include "checkbox.hpp"
#include "text_button.hpp"
#include "sky_parameters_dialog.hpp"
#include "atmosphere_parameters_dialog.hpp"
#include "structure_types_dialog.hpp"
#include "body_part_display_name.hpp"

BodyEditor::BodyEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
    , spaceType(Body_Space_Regional)
{
    START_WIDGET_SCOPE("body-editor")
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
    END_SCOPE

    content = new ListView(
        this,
        [](size_t index) -> void {},
        theme.backgroundColor
    );

    addNoPartChildren();
}

void BodyEditor::step()
{
    if (needsModeUpdate())
    {
        updateMode();
    }

    InterfaceWidget::step();
}

bool BodyEditor::needsModeUpdate() const
{
    set<BodyPartSelectionID> selection = context()->viewerMode()->bodyMode()->selectionIDs();

    if (selection != lastSelection)
    {
        return true;
    }

    if (selection.size() != 1)
    {
        return false;
    }

    if (activePart()->type() != lastActivePart.type())
    {
        return true;
    }

    if (activePart()->constraintType() != lastActivePart.constraintType())
    {
        return true;
    }

    switch (activePart()->type())
    {
    case Body_Part_Line :
        return false;
    case Body_Part_Plane :
        return activePart()->get<BodyPartPlane>().type != lastActivePart.get<BodyPartPlane>().type;
    case Body_Part_Anchor :
        return false;
    case Body_Part_Solid :
        return activePart()->get<BodyPartSolid>().type != lastActivePart.get<BodyPartSolid>().type;
    case Body_Part_Text :
        return false;
    case Body_Part_Symbol :
        return false;
    case Body_Part_Canvas :
        return false;
    case Body_Part_Structure :
        return false;
    case Body_Part_Terrain :
        return false;
    case Body_Part_Light :
        return false;
    case Body_Part_Force :
        return activePart()->get<BodyPartForce>().type != lastActivePart.get<BodyPartForce>().type;
    case Body_Part_Area :
        return false;
    case Body_Part_Rope :
        return false;
    }

    return false;
}

void BodyEditor::updateMode()
{
    clearChildren();

    set<BodyPartSelection> selection = context()->viewerMode()->bodyMode()->selection();

    switch (selection.size())
    {
    case 0 :
        addNoPartChildren();
        break;
    case 1 :
        if (selection.begin()->subID == BodyPartSubID())
        {
            switch (selection.begin()->part->type())
            {
            case Body_Part_Line :
                addLineChildren();
                break;
            case Body_Part_Plane :
                addPlaneChildren();
                break;
            case Body_Part_Anchor :
                addAnchorChildren();
                break;
            case Body_Part_Solid :
                addSolidChildren();
                break;
            case Body_Part_Text :
                addTextChildren();
                break;
            case Body_Part_Symbol :
                addSymbolChildren();
                break;
            case Body_Part_Canvas :
                addCanvasChildren();
                break;
            case Body_Part_Structure :
                addStructureChildren();
                break;
            case Body_Part_Terrain :
                addTerrainChildren();
                break;
            case Body_Part_Light :
                addLightChildren();
                break;
            case Body_Part_Force :
                addForceChildren();
                break;
            case Body_Part_Area :
                addAreaChildren();
                break;
            case Body_Part_Rope :
                addRopeChildren();
                break;
            }
        }
        else
        {
            addOtherChildren();

            switch (selection.begin()->part->type())
            {
            default :
                break;
            case Body_Part_Structure :
                addStructureSubpartChildren();
                break;
            case Body_Part_Terrain :
                addTerrainSubpartChildren();
                break;
            }
        }
        break;
    default :
        addOtherChildren();
        break;
    }

    update();

    lastSelection = context()->viewerMode()->bodyMode()->selectionIDs();

    if (!selection.empty())
    {
        lastActivePart = *selection.begin()->part;
    }
}

void BodyEditor::addNoPartChildren()
{
    TextStyleOverride style;
    style.alignment = Text_Center;

    new Label(
        content,
        localize("body-editor-select-a-body-part-to-edit-it"),
        style
    );
}

void BodyEditor::addOtherChildren()
{
    new EditorMultiEntry(content, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new RadioBar(
            parent,
            { localize("body-editor-global"), localize("body-editor-regional"), localize("body-editor-local"), localize("body-editor-part") },
            bind(&BodyEditor::spaceSource, this),
            bind(&BodyEditor::onSpace, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&BodyEditor::otherPositionSource, this),
            bind(&BodyEditor::onOtherPosition, this, placeholders::_1)
        );
    });
}

void BodyEditor::addLineChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-diameter", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::lineDiameterSource, this),
            bind(&BodyEditor::onLineDiameter, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-length", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::lineLengthSource, this),
            bind(&BodyEditor::onLineLength, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-start-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::lineStartColorSource, this),
            bind(&BodyEditor::onLineStartColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-end-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::lineEndColorSource, this),
            bind(&BodyEditor::onLineEndColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-constraint-at-end", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::lineConstraintAtEndSource, this),
            bind(&BodyEditor::onLineConstraintAtEnd, this, placeholders::_1)
        );
    });
}

void BodyEditor::addPlaneChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-plane-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::planeTypeOptionsSource, this),
            bind(&BodyEditor::planeTypeOptionTooltipsSource, this),
            bind(&BodyEditor::planeTypeSource, this),
            bind(&BodyEditor::onPlaneType, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-thickness", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::planeThicknessSource, this),
            bind(&BodyEditor::onPlaneThickness, this, placeholders::_1)
        );
    });

    switch (activePart()->get<BodyPartPlane>().type)
    {
    case Body_Plane_Ellipse :
    case Body_Plane_Isosceles_Triangle :
    case Body_Plane_Right_Angle_Triangle :
    case Body_Plane_Rectangle :
        addPlaneDimensionChildren(2);
        break;
    case Body_Plane_Equilateral_Triangle :
    case Body_Plane_Pentagon :
    case Body_Plane_Hexagon :
    case Body_Plane_Heptagon :
    case Body_Plane_Octagon :
        addPlaneDimensionChildren(1);
        break;
    }

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::planeColorSource, this),
            bind(&BodyEditor::onPlaneColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-constraint-subpart", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::planeConstraintSubpartOptionsSource, this),
            bind(&BodyEditor::planeConstraintSubpartSource, this),
            bind(&BodyEditor::onPlaneConstraintSubpart, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-empty", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::planeEmptySource, this),
            bind(&BodyEditor::onPlaneEmpty, this, placeholders::_1)
        );
    });
}

void BodyEditor::addAnchorChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-anchor-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::anchorTypeOptionsSource, this),
            bind(&BodyEditor::anchorTypeSource, this),
            bind(&BodyEditor::onAnchorType, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-name", [this](InterfaceWidget* parent) -> void {
        new LineEdit(
            parent,
            bind(&BodyEditor::anchorNameSource, this),
            bind(&BodyEditor::onAnchorName, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-mass-limit", massSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::anchorMassLimitSource, this),
            bind(&BodyEditor::onAnchorMassLimit, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-pull-distance", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::anchorPullDistanceSource, this),
            bind(&BodyEditor::onAnchorPullDistance, this, placeholders::_1)
        );
    });
}

void BodyEditor::addSolidChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-solid-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::solidTypeOptionsSource, this),
            bind(&BodyEditor::solidTypeOptionTooltipsSource, this),
            bind(&BodyEditor::solidTypeSource, this),
            bind(&BodyEditor::onSolidType, this, placeholders::_1)
        );
    });

    switch (activePart()->get<BodyPartSolid>().type)
    {
    case Body_Solid_Cuboid :
        addSolidDimensionChildren(3);
        break;
    case Body_Solid_Sphere :
        addSolidDimensionChildren(1);
        break;
    case Body_Solid_Cylinder :
    case Body_Solid_Cone :
    case Body_Solid_Capsule :
        addSolidDimensionChildren(2);
        break;
    case Body_Solid_Pyramid :
        addSolidDimensionChildren(2);
        break;
    case Body_Solid_Tetrahedron :
    case Body_Solid_Octahedron :
    case Body_Solid_Dodecahedron :
    case Body_Solid_Icosahedron :
        addSolidDimensionChildren(1);
        break;
    }

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::solidColorSource, this),
            bind(&BodyEditor::onSolidColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-constraint-subpart", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::solidConstraintSubpartOptionsSource, this),
            bind(&BodyEditor::solidConstraintSubpartSource, this),
            bind(&BodyEditor::onSolidConstraintSubpart, this, placeholders::_1)
        );
    });
}

void BodyEditor::addTextChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-text-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::textTypeOptionsSource, this),
            bind(&BodyEditor::textTypeOptionTooltipsSource, this),
            bind(&BodyEditor::textTypeSource, this),
            bind(&BodyEditor::onTextType, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-text", [this](InterfaceWidget* parent) -> void {
        new LineEdit(
            parent,
            bind(&BodyEditor::textTextSource, this),
            bind(&BodyEditor::onTextText, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-height", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::textHeightSource, this),
            bind(&BodyEditor::onTextHeight, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::textColorSource, this),
            bind(&BodyEditor::onTextColor, this, placeholders::_1),
            false
        );
    });
}

void BodyEditor::addSymbolChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-symbol-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::symbolTypeOptionsSource, this),
            bind(&BodyEditor::symbolTypeOptionTooltipsSource, this),
            bind(&BodyEditor::symbolTypeSource, this),
            bind(&BodyEditor::onSymbolType, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-size", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::symbolSizeSource, this),
            bind(&BodyEditor::onSymbolSize, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::symbolColorSource, this),
            bind(&BodyEditor::onSymbolColor, this, placeholders::_1),
            false
        );
    });
}

void BodyEditor::addCanvasChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-size", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::canvasSizeSource, this),
            bind(&BodyEditor::onCanvasSize, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::canvasColorSource, this),
            bind(&BodyEditor::onCanvasColor, this, placeholders::_1),
            false
        );
    });
}

void BodyEditor::addStructureChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-positive", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structurePositiveSource, this),
            bind(&BodyEditor::onStructurePositive, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-types", [this](InterfaceWidget* parent) -> void {
        new TextButton(
            parent,
            localize("body-editor-open-types"),
            bind(&BodyEditor::onOpenStructureTypes, this)
        );
    });

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::structureColorSource, this),
            bind(&BodyEditor::onStructureColor, this, placeholders::_1),
            false
        );
    });
}

void BodyEditor::addTerrainChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-terrain-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::terrainColorSource, this),
            bind(&BodyEditor::onTerrainColor, this, placeholders::_1),
            false
        );
    });
}

void BodyEditor::addLightChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-light-enabled", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::lightEnabledSource, this),
            bind(&BodyEditor::onLightEnabled, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&BodyEditor::lightColorSource, this),
            bind(&BodyEditor::onLightColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-angle", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::lightAngleSource, this),
            bind(&BodyEditor::onLightAngle, this, placeholders::_1)
        );
    });
}

void BodyEditor::addForceChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-force-enabled", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::forceEnabledSource, this),
            bind(&BodyEditor::onForceEnabled, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-force-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::forceTypeOptionsSource, this),
            bind(&BodyEditor::forceTypeOptionTooltipsSource, this),
            bind(&BodyEditor::forceTypeSource, this),
            bind(&BodyEditor::onForceType, this, placeholders::_1)
        );
    });

    BodyForceType type = activePart()->get<BodyPartForce>().type;

    switch (type)
    {
    case Body_Force_Vortex :
        new EditorEntry(content, "body-editor-force-intensity", torqueSuffix(), [this](InterfaceWidget* parent) -> void {
            new FloatEdit(
                parent,
                bind(&BodyEditor::forceIntensitySource, this),
                bind(&BodyEditor::onForceIntensity, this, placeholders::_1)
            );
        });
        break;
    case Body_Force_Fan :
        new EditorEntry(content, "body-editor-force-intensity", powerSuffix(), [this](InterfaceWidget* parent) -> void {
            new FloatEdit(
                parent,
                bind(&BodyEditor::forceFanIntensitySource, this),
                bind(&BodyEditor::onForceFanIntensity, this, placeholders::_1)
            );
        });
        break;
    case Body_Force_Linear :
    case Body_Force_Omnidirectional :
    case Body_Force_Rocket :
    case Body_Force_Gravity :
    case Body_Force_Magnetism :
        new EditorEntry(content, "body-editor-force-intensity", forceSuffix(), [this](InterfaceWidget* parent) -> void {
            new FloatEdit(
                parent,
                bind(&BodyEditor::forceIntensitySource, this),
                bind(&BodyEditor::onForceIntensity, this, placeholders::_1)
            );
        });
        break;
    }

    if (type != Body_Force_Vortex && type != Body_Force_Magnetism)
    {
        new EditorEntry(content, "body-editor-force-radius", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::forceRadiusSource, this),
                bind(&BodyEditor::onForceRadius, this, placeholders::_1)
            );
        });
    }

    if (type == Body_Force_Rocket)
    {
        new EditorEntry(content, "body-editor-mass-flow-rate", massRateSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::forceMassFlowRateSource, this),
                bind(&BodyEditor::onForceMassFlowRate, this, placeholders::_1)
            );
        });
    }

    new EditorEntry(content, "body-editor-reaction", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::forceReactionSource, this),
            bind(&BodyEditor::onForceReaction, this, placeholders::_1)
        );
    });
}

void BodyEditor::addAreaChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-height", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::areaHeightSource, this),
            bind(&BodyEditor::onAreaHeight, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-area-enabled", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::areaEnabledSource, this),
            bind(&BodyEditor::onAreaEnabled, this, placeholders::_1)
        );
    });

    new EditorOptionalEntry<f64>(content, "body-editor-speed-of-sound", speedSuffix(), bind(&BodyEditor::areaSpeedOfSoundSource, this), bind(&BodyEditor::onAreaSpeedOfSound, this, placeholders::_1), [](InterfaceWidget* parent, const function<f64()>& valueSource, const function<void(f64)>& onValue) -> void {
        new UFloatEdit(
            parent,
            valueSource,
            onValue
        );
    }, defaultSpeedOfSound);

    new EditorOptionalEntry<vec3>(content, "body-editor-gravity", accelerationSuffix(), bind(&BodyEditor::areaGravitySource, this), bind(&BodyEditor::onAreaGravity, this, placeholders::_1), [](InterfaceWidget* parent, const function<vec3()>& valueSource, const function<void(const vec3&)>& onValue) -> void {
        new Vec3Edit(
            parent,
            valueSource,
            onValue
        );
    }, defaultGravity);

    new EditorOptionalEntry<vec3>(content, "body-editor-flow-velocity", speedSuffix(), bind(&BodyEditor::areaFlowVelocitySource, this), bind(&BodyEditor::onAreaFlowVelocity, this, placeholders::_1), [](InterfaceWidget* parent, const function<vec3()>& valueSource, const function<void(const vec3&)>& onValue) -> void {
        new Vec3Edit(
            parent,
            valueSource,
            onValue
        );
    });

    new EditorOptionalEntry<f64>(content, "body-editor-density", densitySuffix(), bind(&BodyEditor::areaDensitySource, this), bind(&BodyEditor::onAreaDensity, this, placeholders::_1), [](InterfaceWidget* parent, const function<f64()>& valueSource, const function<void(f64)>& onValue) -> void {
        new UFloatEdit(
            parent,
            valueSource,
            onValue
        );
    }, defaultDensity);

    new EditorOptionalEntry<vec3>(content, "body-editor-up", bind(&BodyEditor::areaUpSource, this), bind(&BodyEditor::onAreaUp, this, placeholders::_1), [](InterfaceWidget* parent, const function<vec3()>& valueSource, const function<void(const vec3&)>& onValue) -> void {
        new Vec3Edit(
            parent,
            valueSource,
            onValue
        );
    }, upDir);

    new EditorOptionalEntry<f64>(content, "body-editor-fog-distance", lengthSuffix(), bind(&BodyEditor::areaFogDistanceSource, this), bind(&BodyEditor::onAreaFogDistance, this, placeholders::_1), [](InterfaceWidget* parent, const function<f64()>& valueSource, const function<void(f64)>& onValue) -> void {
        new UFloatEdit(
            parent,
            valueSource,
            onValue
        );
    });

    new EditorOptionalEntry<SkyParameters>(content, "body-editor-sky", bind(&BodyEditor::areaSkyParametersSource, this), bind(&BodyEditor::onAreaSkyParameters, this, placeholders::_1), [this](InterfaceWidget* parent, const function<SkyParameters()>& valueSource, const function<void(const SkyParameters&)>& onValue) -> void {
        new TextButton(
            parent,
            localize("body-editor-open-parameters"),
            bind(&BodyEditor::onAreaOpenSkyParameters, this, valueSource, onValue)
        );
    });

    new EditorOptionalEntry<AtmosphereParameters>(content, "body-editor-atmosphere", bind(&BodyEditor::areaAtmosphereParametersSource, this), bind(&BodyEditor::onAreaAtmosphereParameters, this, placeholders::_1), [this](InterfaceWidget* parent, const function<AtmosphereParameters()>& valueSource, const function<void(const AtmosphereParameters&)>& onValue) -> void {
        new TextButton(
            parent,
            localize("body-editor-open-parameters"),
            bind(&BodyEditor::onAreaOpenAtmosphereParameters, this, valueSource, onValue)
        );
    });

    new EditorEntry(content, "body-editor-surface", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::areaSurfaceSource, this),
            bind(&BodyEditor::onAreaSurface, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::areaColorSource, this),
            bind(&BodyEditor::onAreaColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-opaque", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::areaOpaqueSource, this),
            bind(&BodyEditor::onAreaOpaque, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-animation-speed", speedSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::areaAnimationSpeedSource, this),
            bind(&BodyEditor::onAreaAnimationSpeed, this, placeholders::_1)
        );
    });
}

void BodyEditor::addRopeChildren()
{
    addDefaultChildren();

    new EditorEntry(content, "body-editor-rope-segment-count", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&BodyEditor::ropeSegmentCountSource, this),
            bind(&BodyEditor::onRopeSegmentCount, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-length", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::ropeLengthSource, this),
            bind(&BodyEditor::onRopeLength, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-radius", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::ropeRadiusSource, this),
            bind(&BodyEditor::onRopeRadius, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-rope-thin", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::ropeThinSource, this),
            bind(&BodyEditor::onRopeThin, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-start-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::ropeStartColorSource, this),
            bind(&BodyEditor::onRopeStartColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-end-color", [this](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&BodyEditor::ropeEndColorSource, this),
            bind(&BodyEditor::onRopeEndColor, this, placeholders::_1),
            false
        );
    });

    new EditorEntry(content, "body-editor-constraint-subpart", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::ropeConstraintSubpartOptionsSource, this),
            bind(&BodyEditor::ropeConstraintSubpartSource, this),
            bind(&BodyEditor::onRopeConstraintSubpart, this, placeholders::_1)
        );
    });
}

void BodyEditor::addPlaneDimensionChildren(u32 count)
{
    if (count >= 1)
    {
        new EditorEntry(content, "body-editor-width", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::planeWidthSource, this),
                bind(&BodyEditor::onPlaneWidth, this, placeholders::_1)
            );
        });
    }

    if (count >= 2)
    {
        new EditorEntry(content, "body-editor-height", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::planeHeightSource, this),
                bind(&BodyEditor::onPlaneHeight, this, placeholders::_1)
            );
        });
    }
}

void BodyEditor::addSolidDimensionChildren(u32 count)
{
    if (count >= 1)
    {
        new EditorEntry(content, "body-editor-width", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::solidWidthSource, this),
                bind(&BodyEditor::onSolidWidth, this, placeholders::_1)
            );
        });
    }

    if (count >= 2)
    {
        new EditorEntry(content, "body-editor-length", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::solidLengthSource, this),
                bind(&BodyEditor::onSolidLength, this, placeholders::_1)
            );
        });
    }

    if (count >= 3)
    {
        new EditorEntry(content, "body-editor-height", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&BodyEditor::solidHeightSource, this),
                bind(&BodyEditor::onSolidHeight, this, placeholders::_1)
            );
        });
    }
}

void BodyEditor::addBallConstraintChildren()
{
    new EditorEntry(content, "body-editor-constraint-minimum", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&BodyEditor::ballConstraintMinimumSource, this),
            bind(&BodyEditor::onBallConstraintMinimum, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-constraint-maximum", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&BodyEditor::ballConstraintMaximumSource, this),
            bind(&BodyEditor::onBallConstraintMaximum, this, placeholders::_1)
        );
    });
}

void BodyEditor::addConeConstraintChildren()
{
    new EditorEntry(content, "body-editor-constraint-span", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::coneConstraintSpanSource, this),
            bind(&BodyEditor::onConeConstraintSpan, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-constraint-twist-minimum", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&BodyEditor::coneConstraintTwistMinimumSource, this),
            bind(&BodyEditor::onConeConstraintTwistMinimum, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-constraint-twist-maximum", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&BodyEditor::coneConstraintTwistMaximumSource, this),
            bind(&BodyEditor::onConeConstraintTwistMaximum, this, placeholders::_1)
        );
    });
}

void BodyEditor::addHingeConstraintChildren()
{
    new EditorEntry(content, "body-editor-constraint-minimum", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&BodyEditor::hingeConstraintMinimumSource, this),
            bind(&BodyEditor::onHingeConstraintMinimum, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-constraint-maximum", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&BodyEditor::hingeConstraintMaximumSource, this),
            bind(&BodyEditor::onHingeConstraintMaximum, this, placeholders::_1)
        );
    });
}

void BodyEditor::clearChildren()
{
    content->clearChildren();
}

void BodyEditor::addDefaultChildren()
{
    new EditorEntry(content, "body-editor-id", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&BodyEditor::idSource, this)
        );
    });

    new EditorEntry(content, "body-editor-parent", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::parentOptionsSource, this),
            bind(&BodyEditor::parentSource, this),
            bind(&BodyEditor::onParent, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-parent-subpart", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::parentSubpartOptionsSource, this),
            bind(&BodyEditor::parentSubpartSource, this),
            bind(&BodyEditor::onParentSubpart, this, placeholders::_1)
        );
    });

    new EditorMultiEntry(content, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new RadioBar(
            parent,
            { localize("body-editor-global"), localize("body-editor-regional"), localize("body-editor-local"), localize("body-editor-part") },
            bind(&BodyEditor::spaceSource, this),
            bind(&BodyEditor::onSpace, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&BodyEditor::positionSource, this),
            bind(&BodyEditor::onPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-rotation", [this](InterfaceWidget* parent) -> void {
        new RotationEdit(
            parent,
            bind(&BodyEditor::rotationSource, this),
            bind(&BodyEditor::onRotation, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-scale", [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&BodyEditor::scaleSource, this),
            bind(&BodyEditor::onScale, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-mass", massSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::massSource, this),
            bind(&BodyEditor::onMass, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-max-force", forceSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::maxForceSource, this),
            bind(&BodyEditor::onMaxForce, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-max-velocity", rotationalSpeedSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::maxVelocitySource, this),
            bind(&BodyEditor::onMaxVelocity, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-part-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::typeOptionsSource, this),
            bind(&BodyEditor::typeOptionTooltipsSource, this),
            bind(&BodyEditor::typeSource, this),
            bind(&BodyEditor::onType, this, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-constraint-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::constraintTypeOptionsSource, this),
            bind(&BodyEditor::constraintTypeOptionTooltipsSource, this),
            bind(&BodyEditor::constraintTypeSource, this),
            bind(&BodyEditor::onConstraintType, this, placeholders::_1)
        );
    });

    switch (activePart()->constraintType())
    {
    case Body_Constraint_Fixed :
        break;
    case Body_Constraint_Ball :
        addBallConstraintChildren();
        break;
    case Body_Constraint_Cone :
        addConeConstraintChildren();
        break;
    case Body_Constraint_Hinge :
        addHingeConstraintChildren();
        break;
    }
}

void BodyEditor::addStructureSubpartChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-id", [this, index](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&BodyEditor::structureSubpartIDSource, this, index)
        );
    });

    new EditorEntry(content, "body-editor-structure-type", [this, index](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&BodyEditor::structureSubpartTypeSource, this, index)
        );
    });

    const StructureNode* node = activePart()->get<BodyPartStructure>().getNodeByIndex(
        context()->viewerMode()->bodyMode()->selection().begin()->subID
    );

    if (!node)
    {
        return;
    }

    switch (node->type)
    {
    case Structure_Node_Block :
        addStructureSubpartBlockChildren();
        break;
    case Structure_Node_Rectangle :
        addStructureSubpartRectangleChildren();
        break;
    case Structure_Node_Circle :
        addStructureSubpartCircleChildren();
        break;
    case Structure_Node_Line :
        addStructureSubpartLineChildren();
        break;
    case Structure_Node_Wall :
        addStructureSubpartWallChildren();
        break;
    }
}

void BodyEditor::addStructureSubpartBlockChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-structure-block-position-a", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionASource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionA, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-block-position-b", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionBSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionB, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-length", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartLengthSource, this, index),
            bind(&BodyEditor::onStructureSubpartLength, this, index, placeholders::_1)
        );
    });
}

void BodyEditor::addStructureSubpartRectangleChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-structure-rectangle-position-a", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionASource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionA, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-rectangle-position-b", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionBSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionB, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-length", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartLengthSource, this, index),
            bind(&BodyEditor::onStructureSubpartLength, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-positive", [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartPositiveSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositive, this, index, placeholders::_1)
        );
    });
}

void BodyEditor::addStructureSubpartCircleChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-structure-circle-position", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionASource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionA, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-circle-radius", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::structureSubpartRadiusSource, this, index),
            bind(&BodyEditor::onStructureSubpartRadius, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-length", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartLengthSource, this, index),
            bind(&BodyEditor::onStructureSubpartLength, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-positive", [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartPositiveSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositive, this, index, placeholders::_1)
        );
    });
}

void BodyEditor::addStructureSubpartLineChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-structure-line-position-a", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionASource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionA, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-line-position-b", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionBSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionB, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-length", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartLengthSource, this, index),
            bind(&BodyEditor::onStructureSubpartLength, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-positive", [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartPositiveSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositive, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-line-inverted", [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartInvertedSource, this, index),
            bind(&BodyEditor::onStructureSubpartInverted, this, index, placeholders::_1)
        );
    });
}

void BodyEditor::addStructureSubpartWallChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-structure-wall-type", [this, index](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&BodyEditor::structureSubpartTypeIDOptionsSource, this, index),
            bind(&BodyEditor::structureSubpartTypeIDSource, this, index),
            bind(&BodyEditor::onStructureSubpartTypeID, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-wall-position-a", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionASource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionA, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-wall-position-b", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&BodyEditor::structureSubpartPositionBSource, this, index),
            bind(&BodyEditor::onStructureSubpartPositionB, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-structure-length", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::structureSubpartLengthSource, this, index),
            bind(&BodyEditor::onStructureSubpartLength, this, index, placeholders::_1)
        );
    });
}

void BodyEditor::addTerrainSubpartChildren()
{
    size_t index = static_cast<size_t>(context()->viewerMode()->bodyMode()->selection().begin()->subID.value());

    new EditorEntry(content, "body-editor-id", [this, index](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&BodyEditor::terrainSubpartIDSource, this, index)
        );
    });

    new EditorOptionalEntry<EmissiveColor>(content, "body-editor-terrain-node-color", bind(&BodyEditor::terrainSubpartColorSource, this, index), bind(&BodyEditor::onTerrainSubpartColor, this, index, placeholders::_1), [](InterfaceWidget* parent, const function<EmissiveColor()>& valueSource, const function<void(const EmissiveColor&)>& onValue) -> void {
        new EmissiveColorPicker(
            parent,
            valueSource,
            onValue,
            false
        );
    });

    new EditorEntry(content, "body-editor-terrain-radius", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&BodyEditor::terrainSubpartRadiusSource, this, index),
            bind(&BodyEditor::onTerrainSubpartRadius, this, index, placeholders::_1)
        );
    });

    new EditorEntry(content, "body-editor-terrain-one-sided", [this, index](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&BodyEditor::terrainSubpartOneSidedSource, this, index),
            bind(&BodyEditor::onTerrainSubpartOneSided, this, index, placeholders::_1)
        );
    });
}

vec3 BodyEditor::otherPositionSource()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return vec3();
    }

    return localizeLength(toSpaceMatrix().applyToPosition(selectionCenter()));
}

void BodyEditor::onOtherPosition(const vec3& position)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    set<BodyPartSelection> selection = context()->viewerMode()->bodyMode()->selection();

    if (selection.size() == 0)
    {
        return;
    }
    else if (selection.size() == 1)
    {
        const BodyPartSelection& target = *selection.begin();

        mat4 initialSubpartTransform(target.part->subparts().at(target.subID.value() - 1), quaternion(), vec3(1));
        mat4 currentSubpartTransform(fromSpaceMatrix().applyToPosition(position), quaternion(), vec3(1));

        context()->viewerMode()->bodyMode()->applyTransformToPart(
            target,
            vec3(),
            target.part->regionalTransform(),
            initialSubpartTransform,
            currentSubpartTransform
        );
    }
    else
    {
        vec3 initialSelectionCenter = selectionCenter();
        vec3 currentSelectionCenter = fromSpaceMatrix().applyToPosition(position);

        for (const BodyPartSelection& target : selection)
        {
            mat4 initialSubpartTransform;
            mat4 currentSubpartTransform;

            if (target.subID == BodyPartSubID())
            {
                initialSubpartTransform = target.part->regionalTransform();
                currentSubpartTransform = initialSubpartTransform;
                currentSubpartTransform.setPosition(currentSubpartTransform.position() + (currentSelectionCenter - initialSelectionCenter));
            }
            else
            {
                initialSubpartTransform = mat4(target.part->subparts().at(target.subID.value() - 1), quaternion(), vec3(1));
                currentSubpartTransform = initialSubpartTransform;
                currentSubpartTransform.setPosition(currentSubpartTransform.position() + (currentSelectionCenter - initialSelectionCenter));
            }

            context()->viewerMode()->bodyMode()->applyTransformToPart(
                target,
                vec3(),
                target.part->regionalTransform(),
                initialSubpartTransform,
                currentSubpartTransform
            );
        }
    }
}

string BodyEditor::idSource()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return "";
    }

    BodyPart* part = activePart();

    return to_string(part->id().value());
}

vector<string> BodyEditor::parentOptionsSource()
{
    const Entity* entity = activeEntity();

    if (!entity)
    {
        return {};
    }

    BodyPart* part = this->activePart();

    if (!part)
    {
        return {};
    }

    vector<string> partNames = { localize("body-editor-none") };

    entity->body().traverse([&](const BodyPart* candidate) -> void {
        // skip self
        if (candidate->id() == part->id())
        {
            return;
        }

        partNames.push_back(bodyPartDisplayName(candidate));
    });

    return partNames;
}

size_t BodyEditor::parentSource()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return 0;
    }

    BodyPart* part = activePart();

    if (!part || !part->parent())
    {
        return 0;
    }

    size_t index = 0;

    size_t i = 1;
    entity->body().partiallyTraverse([&](const BodyPart* candidate) -> bool {
        // skip self
        if (candidate->id() == part->id())
        {
            return false;
        }

        // found
        if (candidate == part->parent())
        {
            index = i;
            return true;
        }

        i++;
        return false;
    });

    if (index == 0)
    {
        warning("Failed to find body part's parent.");
    }

    return index;
}

void BodyEditor::onParent(size_t index)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    BodyPart* part = activePart();

    BodyPart* previousParent = part->parent();
    BodyPart* nextParent = nullptr;

    if (index != 0)
    {
        size_t i = 1;
        entity->body().partiallyTraverse([&](BodyPart* candidate) -> bool {
            // skip self
            if (candidate->id() == part->id())
            {
                return false;
            }

            // found
            if (i == index)
            {
                nextParent = candidate;
                return true;
            }

            i++;
            return false;
        });

        if (!nextParent)
        {
            warning("Failed to find body part's new parent.");
        }
    }

    if (previousParent == nextParent)
    {
        return;
    }

    context()->controller()->edit([entity, part, nextParent]() -> void {
        if (nextParent)
        {
            entity->body().setParent(part, BodyPartSubID(), nextParent, BodyPartSubID());
        }
        else
        {
            entity->body().clearParent(part);
        }
    });
}

vector<string> BodyEditor::parentSubpartOptionsSource()
{
    const Entity* entity = activeEntity();

    if (!entity)
    {
        return {};
    }

    BodyPart* part = activePart();

    if (!part)
    {
        return {};
    }

    if (!part->parent())
    {
        return {};
    }

    vector<string> subPartNames = { localize("body-editor-none") };

    size_t subpartCount = part->subparts().size();

    for (size_t i = 0; i < subpartCount; i++)
    {
        subPartNames.push_back(to_string(i + 1));
    }

    return subPartNames;
}

size_t BodyEditor::parentSubpartSource()
{
    const Entity* entity = activeEntity();

    if (!entity)
    {
        return 0;
    }

    BodyPart* part = activePart();

    if (!part)
    {
        return 0;
    }

    if (!part->parent())
    {
        return 0;
    }

    return part->constraintParentSubID().value();
}

void BodyEditor::onParentSubpart(size_t index)
{
    const Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    BodyPart* part = activePart();

    if (!part)
    {
        return;
    }

    if (!part->parent())
    {
        return;
    }

    context()->controller()->edit([part, index]() -> void {
        part->setConstraintParentSubID(BodyPartSubID(index));
    });
}

size_t BodyEditor::spaceSource()
{
    return static_cast<size_t>(spaceType);
}

void BodyEditor::onSpace(size_t index)
{
    spaceType = static_cast<BodySpaceType>(index);
}

vec3 BodyEditor::positionSource()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return vec3();
    }

    BodyPart* part = activePart();

    return toSpaceMatrix().applyToPosition(part->regionalPosition());
}

void BodyEditor::onPosition(const vec3& position)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    BodyPart* part = activePart();

    context()->controller()->edit([this, part, position]() -> void {
        part->setRegionalPosition(fromSpaceMatrix().applyToPosition(position));
    });
}

quaternion BodyEditor::rotationSource()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return quaternion();
    }

    BodyPart* part = activePart();

    return toSpaceMatrix().applyToRotation(part->regionalTransform().rotation());
}

void BodyEditor::onRotation(const quaternion& rotation)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    BodyPart* part = activePart();

    context()->controller()->edit([this, part, rotation]() -> void {
        part->setRegionalRotation(fromSpaceMatrix().applyToRotation(rotation));
    });
}

vec3 BodyEditor::scaleSource()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return vec3(1);
    }

    BodyPart* part = activePart();

    return toSpaceMatrix().applyToTransform(part->regionalTransform()).scale();
}

void BodyEditor::onScale(const vec3& scale)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    BodyPart* part = activePart();

    context()->controller()->edit([this, part, scale]() -> void {
        part->setRegionalScale(fromSpaceMatrix().applyToTransform(mat4(vec3(), quaternion(), scale)).scale());
    });
}

f64 BodyEditor::massSource()
{
    BodyPart* part = activePart();

    return localizeMass(part->mass());
}

void BodyEditor::onMass(f64 mass)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, mass]() -> void {
        part->setMass(delocalizeMass(mass));
    });
}

f64 BodyEditor::maxForceSource()
{
    BodyPart* part = activePart();

    return localizeForce(part->maxForce());
}

void BodyEditor::onMaxForce(f64 force)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, force]() -> void {
        part->setMaxForce(delocalizeForce(force));
    });
}

f64 BodyEditor::maxVelocitySource()
{
    BodyPart* part = activePart();

    return localizeSpeed(part->maxVelocity());
}

void BodyEditor::onMaxVelocity(f64 velocity)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, velocity]() -> void {
        part->setMaxVelocity(delocalizeSpeed(velocity));
    });
}

vector<string> BodyEditor::typeOptionsSource()
{
    // NOTE: Update this when adding new body part types
    return {
        localize("body-editor-line"),
        localize("body-editor-plane"),
        localize("body-editor-anchor"),
        localize("body-editor-solid"),
        localize("body-editor-text"),
        localize("body-editor-symbol"),
        localize("body-editor-canvas"),
        localize("body-editor-structure"),
        localize("body-editor-terrain"),
        localize("body-editor-light"),
        localize("body-editor-force"),
        localize("body-editor-area"),
        localize("body-editor-rope")
    };
}

vector<string> BodyEditor::typeOptionTooltipsSource()
{
    // NOTE: Update this when adding new body part types
    return {
        localize("body-editor-line-tooltip"),
        localize("body-editor-plane-tooltip"),
        localize("body-editor-anchor-tooltip"),
        localize("body-editor-solid-tooltip"),
        localize("body-editor-text-tooltip"),
        localize("body-editor-symbol-tooltip"),
        localize("body-editor-canvas-tooltip"),
        localize("body-editor-structure-tooltip"),
        localize("body-editor-terrain-tooltip"),
        localize("body-editor-light-tooltip"),
        localize("body-editor-force-tooltip"),
        localize("body-editor-area-tooltip"),
        localize("body-editor-rope-tooltip")
    };
}

size_t BodyEditor::typeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->type());
}

void BodyEditor::onType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->setType(static_cast<BodyPartType>(index));
    });

    updateMode();
}

vector<string> BodyEditor::constraintTypeOptionsSource()
{
    // NOTE: Update this when adding new constraint types
    return {
        localize("body-editor-fixed"),
        localize("body-editor-ball"),
        localize("body-editor-cone"),
        localize("body-editor-hinge")
    };
}

vector<string> BodyEditor::constraintTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new constraint types
    return {
        localize("body-editor-fixed-tooltip"),
        localize("body-editor-ball-tooltip"),
        localize("body-editor-cone-tooltip"),
        localize("body-editor-hinge-tooltip")
    };
}

size_t BodyEditor::constraintTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->constraintType());
}

void BodyEditor::onConstraintType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->setConstraintType(static_cast<BodyConstraintType>(index));
    });

    updateMode();
}

vec3 BodyEditor::ballConstraintMinimumSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintBall>().minimum);
}

void BodyEditor::onBallConstraintMinimum(const vec3& min)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, min]() -> void {
        part->getConstraint<BodyConstraintBall>().minimum = delocalizeAngle(min);
    });
}

vec3 BodyEditor::ballConstraintMaximumSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintBall>().maximum);
}

void BodyEditor::onBallConstraintMaximum(const vec3& max)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, max]() -> void {
        part->getConstraint<BodyConstraintBall>().maximum = delocalizeAngle(max);
    });
}

vec2 BodyEditor::coneConstraintSpanSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintCone>().span);
}

void BodyEditor::onConeConstraintSpan(const vec2& span)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, span]() -> void {
        part->getConstraint<BodyConstraintCone>().span = delocalizeAngle(span);
    });
}

f64 BodyEditor::coneConstraintTwistMinimumSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintCone>().twistMinimum);
}

void BodyEditor::onConeConstraintTwistMinimum(const f64& min)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, min]() -> void {
        part->getConstraint<BodyConstraintCone>().twistMinimum = delocalizeAngle(min);
    });
}

f64 BodyEditor::coneConstraintTwistMaximumSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintCone>().twistMaximum);
}

void BodyEditor::onConeConstraintTwistMaximum(const f64& max)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, max]() -> void {
        part->getConstraint<BodyConstraintCone>().twistMaximum = delocalizeAngle(max);
    });
}

f64 BodyEditor::hingeConstraintMinimumSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintHinge>().minimum);
}

void BodyEditor::onHingeConstraintMinimum(const f64& min)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, min]() -> void {
        part->getConstraint<BodyConstraintHinge>().minimum = delocalizeAngle(min);
    });
}

f64 BodyEditor::hingeConstraintMaximumSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->getConstraint<BodyConstraintHinge>().maximum);
}

void BodyEditor::onHingeConstraintMaximum(const f64& max)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, max]() -> void {
        part->getConstraint<BodyConstraintHinge>().maximum = delocalizeAngle(max);
    });
}

f64 BodyEditor::lineDiameterSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartLine>().diameter);
}

void BodyEditor::onLineDiameter(f64 diameter)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, diameter]() -> void {
        part->get<BodyPartLine>().diameter = delocalizeLength(diameter);
    });
}

f64 BodyEditor::lineLengthSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartLine>().length);
}

void BodyEditor::onLineLength(f64 length)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, length]() -> void {
        part->get<BodyPartLine>().length = delocalizeLength(length);
    });
}

EmissiveColor BodyEditor::lineStartColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartLine>().startColor;
}

void BodyEditor::onLineStartColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartLine>().startColor = color;
    });
}

EmissiveColor BodyEditor::lineEndColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartLine>().endColor;
}

void BodyEditor::onLineEndColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartLine>().endColor = color;
    });
}

bool BodyEditor::lineConstraintAtEndSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartLine>().constraintAtEnd;
}

void BodyEditor::onLineConstraintAtEnd(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartLine>().constraintAtEnd = state;
    });
}

f64 BodyEditor::planeThicknessSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartPlane>().thickness);
}

void BodyEditor::onPlaneThickness(f64 thickness)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, thickness]() -> void {
        part->get<BodyPartPlane>().thickness = delocalizeLength(thickness);
    });
}

f64 BodyEditor::planeWidthSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartPlane>().width);
}

void BodyEditor::onPlaneWidth(f64 width)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, width]() -> void {
        part->get<BodyPartPlane>().width = delocalizeLength(width);
    });
}

f64 BodyEditor::planeHeightSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartPlane>().height);
}

void BodyEditor::onPlaneHeight(f64 height)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, height]() -> void {
        part->get<BodyPartPlane>().height = delocalizeLength(height);
    });
}

vector<string> BodyEditor::planeTypeOptionsSource()
{
    // NOTE: Update this when adding new plane types
    return {
        localize("body-editor-ellipse"),
        localize("body-editor-equilateral-triangle"),
        localize("body-editor-isosceles-triangle"),
        localize("body-editor-right-angle-triangle"),
        localize("body-editor-rectangle"),
        localize("body-editor-pentagon"),
        localize("body-editor-hexagon"),
        localize("body-editor-heptagon"),
        localize("body-editor-octagon")
    };
}

vector<string> BodyEditor::planeTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new plane types
    return {
        localize("body-editor-ellipse-tooltip"),
        localize("body-editor-equilateral-triangle-tooltip"),
        localize("body-editor-isosceles-triangle-tooltip"),
        localize("body-editor-right-angle-triangle-tooltip"),
        localize("body-editor-rectangle-tooltip"),
        localize("body-editor-pentagon-tooltip"),
        localize("body-editor-hexagon-tooltip"),
        localize("body-editor-heptagon-tooltip"),
        localize("body-editor-octagon-tooltip")
    };
}

size_t BodyEditor::planeTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartPlane>().type);
}

void BodyEditor::onPlaneType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartPlane>().type = static_cast<BodyPlaneType>(index);
    });
}

EmissiveColor BodyEditor::planeColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartPlane>().color;
}

void BodyEditor::onPlaneColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartPlane>().color = color;
    });
}

vector<string> BodyEditor::planeConstraintSubpartOptionsSource()
{
    BodyPart* part = activePart();

    vector<string> options = { localize("body-editor-none") };

    switch (part->get<BodyPartPlane>().type)
    {
    case Body_Plane_Ellipse :
        options.reserve(4);
        for (u32 i = 1; i <= 4; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Equilateral_Triangle :
        options.reserve(3);
        for (u32 i = 1; i <= 3; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Isosceles_Triangle :
        options.reserve(3);
        for (u32 i = 1; i <= 3; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Right_Angle_Triangle :
        options.reserve(3);
        for (u32 i = 1; i <= 3; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Rectangle :
        options.reserve(4);
        for (u32 i = 1; i <= 4; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Pentagon :
        options.reserve(5);
        for (u32 i = 1; i <= 5; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Hexagon :
        options.reserve(6);
        for (u32 i = 1; i <= 6; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Heptagon :
        options.reserve(7);
        for (u32 i = 1; i <= 7; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    case Body_Plane_Octagon :
        options.reserve(8);
        for (u32 i = 1; i <= 8; i++)
        {
            options.push_back(to_string(i));
        }
        break;
    }

    return options;
}

size_t BodyEditor::planeConstraintSubpartSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartPlane>().constraintSubID.value());
}

void BodyEditor::onPlaneConstraintSubpart(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartPlane>().constraintSubID = BodyPartSubID(static_cast<u32>(index));
    });
}

bool BodyEditor::planeEmptySource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartPlane>().empty;
}

void BodyEditor::onPlaneEmpty(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartPlane>().empty = state;
    });
}

vector<string> BodyEditor::anchorTypeOptionsSource()
{
    // NOTE: Update this when adding new anchor types
    return {
        localize("body-editor-generic"),
        localize("body-editor-grip"),
        localize("body-editor-viewer"),
        localize("body-editor-room"),
        localize("body-editor-head"),
        localize("body-editor-hips"),
        localize("body-editor-left-hand"),
        localize("body-editor-right-hand"),
        localize("body-editor-left-foot"),
        localize("body-editor-right-foot"),
        localize("body-editor-chest"),
        localize("body-editor-left-elbow"),
        localize("body-editor-right-elbow"),
        localize("body-editor-left-knee"),
        localize("body-editor-right-knee"),
        localize("body-editor-eyes"),
        localize("body-editor-left-eye"),
        localize("body-editor-left-eyebrow"),
        localize("body-editor-right-eye"),
        localize("body-editor-right-eyebrow"),
        localize("body-editor-right-eyelid"),
        localize("body-editor-mouth"),
        localize("body-editor-left-thumb"),
        localize("body-editor-left-index-finger"),
        localize("body-editor-left-middle-finger"),
        localize("body-editor-left-ring-finger"),
        localize("body-editor-left-little-finger"),
        localize("body-editor-right-thumb"),
        localize("body-editor-right-index-finger"),
        localize("body-editor-right-middle-finger"),
        localize("body-editor-right-ring-finger"),
        localize("body-editor-right-little-finger")
    };
}

size_t BodyEditor::anchorTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartAnchor>().type);
}

void BodyEditor::onAnchorType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartAnchor>().type = static_cast<BodyAnchorType>(index);
    });
}

string BodyEditor::anchorNameSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartAnchor>().name;
}

void BodyEditor::onAnchorName(const string& name)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, name]() -> void {
        part->get<BodyPartAnchor>().name = name;
    });
}

f64 BodyEditor::anchorMassLimitSource()
{
    BodyPart* part = activePart();

    return localizeMass(part->get<BodyPartAnchor>().massLimit);
}

void BodyEditor::onAnchorMassLimit(f64 limit)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, limit]() -> void {
        part->get<BodyPartAnchor>().massLimit = delocalizeMass(limit);
    });
}

f64 BodyEditor::anchorPullDistanceSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartAnchor>().pullDistance);
}

void BodyEditor::onAnchorPullDistance(f64 distance)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, distance]() -> void {
        part->get<BodyPartAnchor>().pullDistance = delocalizeLength(distance);
    });
}

f64 BodyEditor::solidWidthSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartSolid>().size.x);
}

void BodyEditor::onSolidWidth(f64 width)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, width]() -> void {
        part->get<BodyPartSolid>().size.x = delocalizeLength(width);
    });
}

f64 BodyEditor::solidLengthSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartSolid>().size.y);
}

void BodyEditor::onSolidLength(f64 length)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, length]() -> void {
        part->get<BodyPartSolid>().size.y = delocalizeLength(length);
    });
}

f64 BodyEditor::solidHeightSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartSolid>().size.z);
}

void BodyEditor::onSolidHeight(f64 height)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, height]() -> void {
        part->get<BodyPartSolid>().size.z = delocalizeLength(height);
    });
}

vector<string> BodyEditor::solidTypeOptionsSource()
{
    // NOTE: Update this when adding new solid types
    return {
        localize("body-editor-cuboid"),
        localize("body-editor-sphere"),
        localize("body-editor-cylinder"),
        localize("body-editor-cone"),
        localize("body-editor-capsule"),
        localize("body-editor-pyramid"),
        localize("body-editor-tetrahedron"),
        localize("body-editor-octahedron"),
        localize("body-editor-dodecahedron"),
        localize("body-editor-icosahedron"),
    };
}

vector<string> BodyEditor::solidTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new solid types
    return {
        localize("body-editor-cuboid-tooltip"),
        localize("body-editor-sphere-tooltip"),
        localize("body-editor-cylinder-tooltip"),
        localize("body-editor-cone-tooltip"),
        localize("body-editor-capsule-tooltip"),
        localize("body-editor-pyramid-tooltip"),
        localize("body-editor-tetrahedron-tooltip"),
        localize("body-editor-octahedron-tooltip"),
        localize("body-editor-dodecahedron-tooltip"),
        localize("body-editor-icosahedron-tooltip")
    };
}

size_t BodyEditor::solidTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartSolid>().type);
}

void BodyEditor::onSolidType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartSolid>().type = static_cast<BodySolidType>(index);
    });
}

EmissiveColor BodyEditor::solidColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartSolid>().color;
}

void BodyEditor::onSolidColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartSolid>().color = color;
    });
}

vector<string> BodyEditor::solidConstraintSubpartOptionsSource()
{
    BodyPart* part = activePart();

    vector<string> options(part->get<BodyPartSolid>().subparts().size() + 1);

    options[0] = localize("body-editor-none");

    for (size_t i = 1; i < options.size(); i++)
    {
        options[i] = to_string(i);
    }

    return options;
}

size_t BodyEditor::solidConstraintSubpartSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartSolid>().constraintSubID);
}

void BodyEditor::onSolidConstraintSubpart(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartSolid>().constraintSubID = BodyPartSubID(static_cast<u32>(index));
    });
}

vector<string> BodyEditor::textTypeOptionsSource()
{
    // NOTE: Update this when adding new text types
    return {
        localize("body-editor-default")
    };
}

vector<string> BodyEditor::textTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new text types
    return {
        localize("body-editor-default-tooltip")
    };
}

size_t BodyEditor::textTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartText>().type);
}

void BodyEditor::onTextType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartText>().type = static_cast<BodyTextType>(index);
    });
}

string BodyEditor::textTextSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartText>().text;
}

void BodyEditor::onTextText(const string& text)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, text]() -> void {
        part->get<BodyPartText>().text = text;
    });
}

f64 BodyEditor::textHeightSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartText>().height);
}

void BodyEditor::onTextHeight(f64 height)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, height]() -> void {
        part->get<BodyPartText>().height = delocalizeLength(height);
    });
}

EmissiveColor BodyEditor::textColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartText>().color;
}

void BodyEditor::onTextColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartText>().color = color;
    });
}

vector<string> BodyEditor::symbolTypeOptionsSource()
{
    // NOTE: Update this when adding new symbol types
    return {
        localize("symbol-star")
    };
}

vector<string> BodyEditor::symbolTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new symbol types
    return {
        localize("symbol-star-tooltip")
    };
}

size_t BodyEditor::symbolTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartSymbol>().type);
}

void BodyEditor::onSymbolType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartSymbol>().type = static_cast<SymbolType>(index);
    });
}

f64 BodyEditor::symbolSizeSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartSymbol>().size);
}

void BodyEditor::onSymbolSize(f64 size)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, size]() -> void {
        part->get<BodyPartSymbol>().size = delocalizeLength(size);
    });
}

EmissiveColor BodyEditor::symbolColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartSymbol>().color;
}

void BodyEditor::onSymbolColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartSymbol>().color = color;
    });
}

f64 BodyEditor::canvasSizeSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartCanvas>().size;
}

void BodyEditor::onCanvasSize(f64 size)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, size]() -> void {
        part->get<BodyPartCanvas>().size = size;
    });
}

EmissiveColor BodyEditor::canvasColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartCanvas>().color;
}

void BodyEditor::onCanvasColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartCanvas>().color = color;
    });
}

bool BodyEditor::structurePositiveSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartStructure>().positive;
}

void BodyEditor::onStructurePositive(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartStructure>().positive = state;
    });
}

void BodyEditor::onOpenStructureTypes()
{
    view()->interface()->addWindow<StructureTypesDialog>(
        bind(&BodyEditor::structureNextTypeIDSource, this),
        bind(&BodyEditor::onStructureNextTypeID, this, placeholders::_1),
        bind(&BodyEditor::structureTypesSource, this),
        bind(&BodyEditor::onStructureTypes, this, placeholders::_1)
    );
}

StructureTypeID BodyEditor::structureNextTypeIDSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartStructure>().nextTypeID;
}

void BodyEditor::onStructureNextTypeID(StructureTypeID id)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, id]() -> void {
        part->get<BodyPartStructure>().nextTypeID = id;
    });
}

vector<StructureType> BodyEditor::structureTypesSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartStructure>().types;
}

void BodyEditor::onStructureTypes(const vector<StructureType>& types)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, types]() -> void {
        part->get<BodyPartStructure>().types = types;
    });
}

EmissiveColor BodyEditor::structureColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartStructure>().color;
}

void BodyEditor::onStructureColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartStructure>().color = color;
    });
}

string BodyEditor::structureSubpartIDSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return "";
    }

    return to_string(node->id.value());
}

string BodyEditor::structureSubpartTypeSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return "";
    }

    return localize("body-editor-" + structureNodeTypeToString(node->type));
}

vector<string> BodyEditor::structureSubpartTypeIDOptionsSource(size_t index)
{
    BodyPart* part = activePart();

    vector<string> options;
    options.reserve(part->get<BodyPartStructure>().types.size());

    for (const StructureType& type : part->get<BodyPartStructure>().types)
    {
        options.push_back(toPrettyString(localizeLength(type.thickness)) + " " + lengthSuffix());
    }

    return options;
}

size_t BodyEditor::structureSubpartTypeIDSource(size_t index)
{
    BodyPart* part = activePart();

    const BodyPartStructure& structure = part->get<BodyPartStructure>();

    const StructureNode* node = structure.getNodeByIndex(index);

    if (!node)
    {
        return 0;
    }

    for (size_t i = 0; i < structure.types.size(); i++)
    {
        if (structure.types.at(i).id == node->typeID)
        {
            return i;
        }
    }

    return 0;
}

void BodyEditor::onStructureSubpartTypeID(size_t index, size_t typeIndex)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([part, node, typeIndex]() -> void {
        node->typeID = part->get<BodyPartStructure>().types.at(typeIndex).id;
    });
}

bool BodyEditor::structureSubpartPositiveSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return false;
    }

    return node->positive;
}

void BodyEditor::onStructureSubpartPositive(size_t index, bool positive)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([node, positive]() -> void {
        node->positive = positive;
    });
}

bool BodyEditor::structureSubpartInvertedSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return false;
    }

    return node->inverted;
}

void BodyEditor::onStructureSubpartInverted(size_t index, bool inverted)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([node, inverted]() -> void {
        node->inverted = inverted;
    });
}

vec2 BodyEditor::structureSubpartPositionASource(size_t index)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return vec2();
    }

    return localizeLength(node->positionA);
}

void BodyEditor::onStructureSubpartPositionA(size_t index, const vec2& positionA)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([node, positionA]() -> void {
        node->positionA = delocalizeLength(positionA);
    });
}

vec2 BodyEditor::structureSubpartPositionBSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return vec2();
    }

    return localizeLength(node->positionB);
}

void BodyEditor::onStructureSubpartPositionB(size_t index, const vec2& positionB)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([node, positionB]() -> void {
        node->positionB = delocalizeLength(positionB);
    });
}

f64 BodyEditor::structureSubpartRadiusSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return 0;
    }

    return localizeLength(node->radius);
}

void BodyEditor::onStructureSubpartRadius(size_t index, f64 radius)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([node, radius]() -> void {
        node->radius = delocalizeLength(radius);
    });
}

f64 BodyEditor::structureSubpartLengthSource(size_t index)
{
    BodyPart* part = activePart();

    const StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return 0;
    }

    return localizeLength(node->length);
}

void BodyEditor::onStructureSubpartLength(size_t index, f64 length)
{
    BodyPart* part = activePart();

    StructureNode* node = part->get<BodyPartStructure>().getNodeByIndex(index);

    if (!node)
    {
        return;
    }

    context()->controller()->edit([node, length]() -> void {
        node->length = delocalizeLength(length);
    });
}

EmissiveColor BodyEditor::terrainColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartTerrain>().color;
}

void BodyEditor::onTerrainColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartTerrain>().color = color;
    });
}

string BodyEditor::terrainSubpartIDSource(size_t index)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return "";
    }

    return to_string(part->get<BodyPartTerrain>().nodes.at(index).id.value());
}

optional<EmissiveColor> BodyEditor::terrainSubpartColorSource(size_t index)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return {};
    }

    return part->get<BodyPartTerrain>().nodes.at(index).color;
}

void BodyEditor::onTerrainSubpartColor(size_t index, const optional<EmissiveColor>& color)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return;
    }

    context()->controller()->edit([part, index, color]() -> void {
        part->get<BodyPartTerrain>().nodes.at(index).color = color;
    });
}

f64 BodyEditor::terrainSubpartRadiusSource(size_t index)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return 0;
    }

    return localizeLength(part->get<BodyPartTerrain>().nodes.at(index).radius);
}

void BodyEditor::onTerrainSubpartRadius(size_t index, f64 radius)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return;
    }

    context()->controller()->edit([part, index, radius]() -> void {
        part->get<BodyPartTerrain>().nodes.at(index).radius = delocalizeLength(radius);
    });
}

bool BodyEditor::terrainSubpartOneSidedSource(size_t index)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return false;
    }

    return part->get<BodyPartTerrain>().nodes.at(index).oneSided;
}

void BodyEditor::onTerrainSubpartOneSided(size_t index, bool state)
{
    BodyPart* part = activePart();

    if (index >= part->get<BodyPartTerrain>().nodes.size())
    {
        return;
    }

    context()->controller()->edit([part, index, state]() -> void {
        part->get<BodyPartTerrain>().nodes.at(index).oneSided = state;
    });
}

bool BodyEditor::lightEnabledSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartLight>().enabled;
}

void BodyEditor::onLightEnabled(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartLight>().enabled = state;
    });
}

HDRColor BodyEditor::lightColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartLight>().color;
}

void BodyEditor::onLightColor(const HDRColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartLight>().color = color;
    });
}

f64 BodyEditor::lightAngleSource()
{
    BodyPart* part = activePart();

    return localizeAngle(part->get<BodyPartLight>().angle);
}

void BodyEditor::onLightAngle(f64 angle)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, angle]() -> void {
        part->get<BodyPartLight>().angle = delocalizeAngle(angle);
    });
}

bool BodyEditor::forceEnabledSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartForce>().enabled;
}

void BodyEditor::onForceEnabled(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartForce>().enabled = state;
    });
}

vector<string> BodyEditor::forceTypeOptionsSource()
{
    // NOTE: Update this when adding new force types
    return {
        localize("body-editor-linear"),
        localize("body-editor-vortex"),
        localize("body-editor-omnidirectional"),
        localize("body-editor-fan"),
        localize("body-editor-rocket"),
        localize("body-editor-gravity"),
        localize("body-editor-magnetism")
    };
}

vector<string> BodyEditor::forceTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new force types
    return {
        localize("body-editor-linear-tooltip"),
        localize("body-editor-vortex-tooltip"),
        localize("body-editor-omnidirectional-tooltip"),
        localize("body-editor-fan-tooltip"),
        localize("body-editor-rocket-tooltip"),
        localize("body-editor-gravity-tooltip"),
        localize("body-editor-magnetism-tooltip")
    };
}

size_t BodyEditor::forceTypeSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartForce>().type);
}

void BodyEditor::onForceType(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartForce>().type = static_cast<BodyForceType>(index);
    });
}

f64 BodyEditor::forceFanIntensitySource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartForce>().intensity;
}

void BodyEditor::onForceFanIntensity(f64 intensity)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, intensity]() -> void {
        part->get<BodyPartForce>().intensity = intensity;
    });
}

f64 BodyEditor::forceIntensitySource()
{
    BodyPart* part = activePart();

    return localizeForce(part->get<BodyPartForce>().intensity);
}

void BodyEditor::onForceIntensity(f64 intensity)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, intensity]() -> void {
        part->get<BodyPartForce>().intensity = delocalizeForce(intensity);
    });
}

f64 BodyEditor::forceRadiusSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartForce>().radius);
}

void BodyEditor::onForceRadius(f64 radius)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, radius]() -> void {
        part->get<BodyPartForce>().radius = delocalizeLength(radius);
    });
}

f64 BodyEditor::forceMassFlowRateSource()
{
    BodyPart* part = activePart();

    return localizeMass(part->get<BodyPartForce>().massFlowRate);
}

void BodyEditor::onForceMassFlowRate(f64 rate)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, rate]() -> void {
        part->get<BodyPartForce>().massFlowRate = delocalizeMass(rate);
    });
}

bool BodyEditor::forceReactionSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartForce>().reaction;
}

void BodyEditor::onForceReaction(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartForce>().reaction = state;
    });
}

f64 BodyEditor::areaHeightSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartArea>().height);
}

void BodyEditor::onAreaHeight(f64 height)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, height]() -> void {
        part->get<BodyPartArea>().height = delocalizeLength(height);
    });
}

f64 BodyEditor::areaEnabledSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().enabled;
}

void BodyEditor::onAreaEnabled(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartArea>().enabled = state;
    });
}

optional<f64> BodyEditor::areaSpeedOfSoundSource()
{
    BodyPart* part = activePart();

    return optionalBind<f64>(
        [](f64 x) -> f64 { return localizeSpeed(x); },
        part->get<BodyPartArea>().speedOfSound
    );
}

void BodyEditor::onAreaSpeedOfSound(const optional<f64>& speedOfSound)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, speedOfSound]() -> void {
        part->get<BodyPartArea>().speedOfSound = optionalBind<f64>(
            [](f64 x) -> f64 { return delocalizeSpeed(x); },
            speedOfSound
        );
    });
}

optional<vec3> BodyEditor::areaGravitySource()
{
    BodyPart* part = activePart();

    return optionalBind<vec3>(
        [](vec3 x) -> vec3 { return localizeAcceleration(x); },
        part->get<BodyPartArea>().gravity
    );
}

void BodyEditor::onAreaGravity(const optional<vec3>& gravity)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, gravity]() -> void {
        part->get<BodyPartArea>().gravity = optionalBind<vec3>(
            [](vec3 x) -> vec3 { return delocalizeAcceleration(x); },
            gravity
        );
    });
}

optional<vec3> BodyEditor::areaFlowVelocitySource()
{
    BodyPart* part = activePart();

    return optionalBind<vec3>(
        [](vec3 x) -> vec3 { return localizeSpeed(x); },
        part->get<BodyPartArea>().flowVelocity
    );
}

void BodyEditor::onAreaFlowVelocity(const optional<vec3>& flowVelocity)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, flowVelocity]() -> void {
        part->get<BodyPartArea>().flowVelocity = optionalBind<vec3>(
            [](vec3 x) -> vec3 { return delocalizeSpeed(x); },
            flowVelocity
        );
    });
}

optional<f64> BodyEditor::areaDensitySource()
{
    BodyPart* part = activePart();

    return optionalBind<f64>(
        [](f64 x) -> f64 { return localizeDensity(x); },
        part->get<BodyPartArea>().density
    );
}

void BodyEditor::onAreaDensity(const optional<f64>& density)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, density]() -> void {
        part->get<BodyPartArea>().density = optionalBind<f64>(
            [](f64 x) -> f64 { return delocalizeDensity(x); },
            density
        );
    });
}

optional<vec3> BodyEditor::areaUpSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().up;
}

void BodyEditor::onAreaUp(const optional<vec3>& up)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, up]() -> void {
        part->get<BodyPartArea>().up = up;
    });
}

optional<f64> BodyEditor::areaFogDistanceSource()
{
    BodyPart* part = activePart();

    return optionalBind<f64>(
        [](f64 x) -> f64 { return localizeLength(x); },
        part->get<BodyPartArea>().fogDistance
    );
}

void BodyEditor::onAreaFogDistance(const optional<f64>& distance)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, distance]() -> void {
        part->get<BodyPartArea>().fogDistance = optionalBind<f64>(
            [](f64 x) -> f64 { return delocalizeLength(x); },
            distance
        );
    });
}

void BodyEditor::onAreaOpenSkyParameters(
    const function<SkyParameters()>& valueSource,
    const function<void(const SkyParameters&)>& onValue
)
{
    view()->interface()->addWindow<SkyParametersDialog>(
        valueSource,
        onValue
    );
}

optional<SkyParameters> BodyEditor::areaSkyParametersSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().sky;
}

void BodyEditor::onAreaSkyParameters(const optional<SkyParameters>& parameters)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, parameters]() -> void {
        part->get<BodyPartArea>().sky = parameters;
    });
}

void BodyEditor::onAreaOpenAtmosphereParameters(
    const function<AtmosphereParameters()>& valueSource,
    const function<void(const AtmosphereParameters&)>& onValue
)
{
    view()->interface()->addWindow<AtmosphereParametersDialog>(
        valueSource,
        onValue
    );
}

optional<AtmosphereParameters> BodyEditor::areaAtmosphereParametersSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().atmosphere;
}

void BodyEditor::onAreaAtmosphereParameters(const optional<AtmosphereParameters>& parameters)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, parameters]() -> void {
        part->get<BodyPartArea>().atmosphere = parameters;
    });
}

bool BodyEditor::areaSurfaceSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().surface;
}

void BodyEditor::onAreaSurface(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartArea>().surface = state;
    });
}

EmissiveColor BodyEditor::areaColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().color;
}

void BodyEditor::onAreaColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartArea>().color = color;
    });
}

bool BodyEditor::areaOpaqueSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartArea>().opaque;
}

void BodyEditor::onAreaOpaque(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartArea>().opaque = state;
    });
}

f64 BodyEditor::areaAnimationSpeedSource()
{
    BodyPart* part = activePart();

    return localizeSpeed(part->get<BodyPartArea>().animationSpeed);
}

void BodyEditor::onAreaAnimationSpeed(f64 speed)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, speed]() -> void {
        part->get<BodyPartArea>().animationSpeed = delocalizeSpeed(speed);
    });
}

u32 BodyEditor::ropeSegmentCountSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartRope>().segmentCount;
}

void BodyEditor::onRopeSegmentCount(u32 count)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, count]() -> void {
        part->get<BodyPartRope>().segmentCount = count;
    });
}

f64 BodyEditor::ropeLengthSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartRope>().length);
}

void BodyEditor::onRopeLength(f64 length)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, length]() -> void {
        part->get<BodyPartRope>().length = delocalizeLength(length);
    });
}

f64 BodyEditor::ropeRadiusSource()
{
    BodyPart* part = activePart();

    return localizeLength(part->get<BodyPartRope>().radius);
}

void BodyEditor::onRopeRadius(f64 radius)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, radius]() -> void {
        part->get<BodyPartRope>().radius = delocalizeLength(radius);
    });
}

bool BodyEditor::ropeThinSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartRope>().thin;
}

void BodyEditor::onRopeThin(bool state)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, state]() -> void {
        part->get<BodyPartRope>().thin = state;
    });
}

EmissiveColor BodyEditor::ropeStartColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartRope>().startColor;
}

void BodyEditor::onRopeStartColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartRope>().startColor = color;
    });
}

EmissiveColor BodyEditor::ropeEndColorSource()
{
    BodyPart* part = activePart();

    return part->get<BodyPartRope>().endColor;
}

void BodyEditor::onRopeEndColor(const EmissiveColor& color)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, color]() -> void {
        part->get<BodyPartRope>().endColor = color;
    });
}

vector<string> BodyEditor::ropeConstraintSubpartOptionsSource()
{
    BodyPart* part = activePart();

    vector<string> options(part->get<BodyPartRope>().subparts().size() + 1);

    options[0] = localize("body-editor-none");

    for (size_t i = 1; i < options.size(); i++)
    {
        options[i] = to_string(i);
    }

    return options;
}

size_t BodyEditor::ropeConstraintSubpartSource()
{
    BodyPart* part = activePart();

    return static_cast<size_t>(part->get<BodyPartRope>().constraintSubID.value());
}

void BodyEditor::onRopeConstraintSubpart(size_t index)
{
    BodyPart* part = activePart();

    context()->controller()->edit([part, index]() -> void {
        part->get<BodyPartRope>().constraintSubID = BodyPartSubID(static_cast<u32>(index));
    });
}

Entity* BodyEditor::activeEntity() const
{
    return context()->controller()->selfWorld()->get(id);
}

BodyPart* BodyEditor::activePart() const
{
    return context()->viewerMode()->bodyMode()->selection().begin()->part;
}

vec3 BodyEditor::selectionCenter() const
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return vec3();
    }

    return entity->globalTransform().applyToPosition(context()->viewerMode()->bodyMode()->selectionCenter(), false);
}

mat4 BodyEditor::toSpaceMatrix() const
{
    BodyPart* part = activePart();

    switch (spaceType)
    {
    default :
        throw runtime_error("Encountered unknown body space type '" + to_string(spaceType) + "'");
    case Body_Space_Part :
        return part->regionalTransform();
    case Body_Space_Local :
        return context()->viewerMode()->bodyMode()->selectionIDs().size() == 1 && part->parent()
            ? part->parent()->regionalTransform()
            : mat4();
    case Body_Space_Regional :
        return mat4();
    case Body_Space_Global :
        return activeEntity()->globalTransform();
    }
}

mat4 BodyEditor::fromSpaceMatrix() const
{
    BodyPart* part = activePart();

    switch (spaceType)
    {
    default :
        throw runtime_error("Encountered unknown body space type '" + to_string(spaceType) + "'");
    case Body_Space_Part :
        return part->regionalTransform().inverse();
    case Body_Space_Local :
        return context()->viewerMode()->bodyMode()->selectionIDs().size() == 1 && part->parent()
            ? part->parent()->regionalTransform().inverse()
            : mat4();
    case Body_Space_Regional :
        return mat4();
    case Body_Space_Global :
        return activeEntity()->globalTransform().inverse();
    }
}

void BodyEditor::undo(const Input& input)
{
    context()->viewerMode()->undo();
}

void BodyEditor::redo(const Input& input)
{
    context()->viewerMode()->redo();
}

SizeProperties BodyEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
