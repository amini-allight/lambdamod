/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_panel_input_scope.hpp"
#include "interface_widget.hpp"
#include "vr_capable_panel.hpp"

VRPanelInputScope::VRPanelInputScope(
    InterfaceWidget* widget,
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<optional<Point>(const vec3&, const quaternion&)>& toLocal,
    const function<void(InputScope*)>& block
)
    : InputScope(
        widget->context(),
        parent,
        name,
        mode,
        std::bind(&VRPanelInputScope::isTargeted, this, placeholders::_1),
        block
    )
    , widget(widget)
    , toLocal(toLocal)
{

}

bool VRPanelInputScope::movePointer(PointerType type, const Point& pointer, bool consumed) const
{
    if (type == Pointer_Normal)
    {
        return false;
    }

    bool consumedLocally = widget->shouldDraw() && widget->movePointer(type, pointer, consumed);
    bool consumedByChild = InputScope::movePointer(type, pointer, consumed);

    return consumedLocally || consumedByChild;
}

bool VRPanelInputScope::moveLeftHand(const vec3& position, const quaternion& rotation, bool consumed) const
{
    optional<Point> point = toLocal(position, rotation);

    if (!point)
    {
        dynamic_cast<VRCapablePanel*>(widget)->clearPointer(Pointer_VR_Left);
        return false;
    }
    else
    {
        return movePointer(Pointer_VR_Left, *point, consumed);
    }
}

bool VRPanelInputScope::moveRightHand(const vec3& position, const quaternion& rotation, bool consumed) const
{
    optional<Point> point = toLocal(position, rotation);

    if (!point)
    {
        dynamic_cast<VRCapablePanel*>(widget)->clearPointer(Pointer_VR_Right);
        return false;
    }
    else
    {
        return movePointer(Pointer_VR_Right, *point, consumed);
    }
}

bool VRPanelInputScope::isTargeted(const Input& input) const
{
    return
        (input.isLeftHand() && dynamic_cast<VRCapablePanel*>(widget)->hasPointer(Pointer_VR_Left)) ||
        (input.isRightHand() && dynamic_cast<VRCapablePanel*>(widget)->hasPointer(Pointer_VR_Right));
}
#endif
