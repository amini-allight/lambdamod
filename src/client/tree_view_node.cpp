/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tree_view_node.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "theme.hpp"
#include "tree_view.hpp"
#include "control_context.hpp"

TreeViewNode::TreeViewNode(
    InterfaceWidget* parent,
    TreeView* root,
    const string& name,
    bool expanded,
    const function<bool()>& filtering,
    const function<bool()>& filter,
    const function<void(const Point&)>& onRightClick
)
    : Layout(parent)
    , root(root)
    , _name(name)
    , filtering(filtering)
    , filter(filter)
    , onRightClick(onRightClick)
    , _expanded(expanded)
{
    START_WIDGET_SCOPE("tree-view-node")
        START_SCOPE("visible-and-targeted", !hidden() && !filtered() && (this->pointer - position()).y < UIScale::treeViewNodeHeight() + UIScale::marginSize() / 2)
            WIDGET_SLOT("interact", toggleExpansion)
            WIDGET_SLOT("open-context-menu", openContextMenu)
        END_SCOPE
    END_SCOPE
}

void TreeViewNode::move(const Point& position)
{
    this->position(position);

    Point p = position;

    p.x += UIScale::treeViewNodeIndent();
    p.y += !filtered() ? UIScale::treeViewNodeHeight() : 0;

    for (InterfaceWidget* child : children())
    {
        child->move(p);

        p.y += child->size().h;
    }
}

void TreeViewNode::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        i32 w = 0;

        switch (sizePropertiesOf(child).horizontal)
        {
        case Scaling_Fixed :
            w = sizePropertiesOf(child).w;
            break;
        case Scaling_Fraction :
            w = sizePropertiesOf(child).w * size.w;
            break;
        case Scaling_Fill :
            w = size.w;
            break;
        }

        i32 h = 0;

        switch (sizePropertiesOf(child).vertical)
        {
        case Scaling_Fixed :
            h = sizePropertiesOf(child).h;
            break;
        case Scaling_Fraction :
            h = sizePropertiesOf(child).h * size.h;
            break;
        case Scaling_Fill :
            h = size.h;
            break;
        }

        child->resize({ w, h });
    }
}

void TreeViewNode::draw(const DrawContext& ctx) const
{
    if (!filtered())
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::marginSize() / 2),
            Size(size().w, UIScale::treeViewNodeHeight()) - Size(UIScale::marginSize()),
            theme.outdentColor
        );

        if (!children().empty())
        {
            drawColoredImage(
                ctx,
                this,
                Point(UIScale::marginSize() / 2),
                Size(UIScale::treeViewNodeHeight()) - Size(UIScale::marginSize()),
                expanded() ? Icon_Down : Icon_Right,
                focused() ? theme.accentColor : theme.textColor
            );
        }

        drawText(
            ctx,
            this,
            Point(UIScale::treeViewNodeIndent(), 0) + Point(UIScale::marginSize() / 2),
            Size(size().w - UIScale::treeViewNodeIndent(), UIScale::treeViewNodeHeight()) - Size(UIScale::marginSize()),
            _name,
            TextStyle()
        );
    }

    if (expanded())
    {
        InterfaceWidget::draw(ctx);
    }
}

const string& TreeViewNode::name() const
{
    return _name;
}

i32 TreeViewNode::rowCount() const
{
    i32 rows = !filtered();

    if (expanded())
    {
        for (InterfaceWidget* child : children())
        {
            auto node = dynamic_cast<TreeViewNode*>(child);

            rows += node->rowCount();
        }
    }

    return rows;
}

i32 TreeViewNode::columnCount() const
{
    i32 maxColumns = 0;

    if (expanded())
    {
        for (InterfaceWidget* child : children())
        {
            i32 columns = dynamic_cast<TreeViewNode*>(child)->columnCount() + 1;

            if (columns > maxColumns)
            {
                maxColumns = columns;
            }
        }
    }

    return maxColumns;
}

bool TreeViewNode::expanded() const
{
    return _expanded || filtering();
}

bool TreeViewNode::filtered() const
{
    return filter && !filter();
}

bool TreeViewNode::hidden() const
{
    auto parent = dynamic_cast<TreeViewNode*>(this->parent());

    if (!parent)
    {
        return false;
    }

    return !parent->expanded() || parent->hidden();
}

void TreeViewNode::toggleExpansion(const Input& input)
{
    if (children().empty())
    {
        return;
    }

    _expanded = !_expanded;
    // Has to update from the root down to facilitate changing height
    root->update();
}

void TreeViewNode::openContextMenu(const Input& input)
{
    onRightClick(pointer);
}

SizeProperties TreeViewNode::sizeProperties() const
{
    return {
        0, static_cast<f64>(rowCount() * UIScale::treeViewNodeHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
