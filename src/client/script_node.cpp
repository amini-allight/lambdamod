/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_node.hpp"
#include "script_tools.hpp"
#include "tools.hpp"
#include "interface_constants.hpp"

static bool isQuote(const Value& value)
{
    return value.l.size() == 2 && value.l.front() == symbol("quote");
}

static bool isExecution(const Value& value)
{
    return !value.l.empty() && value.l.front().type == Value_Symbol;
}

ScriptNode::ScriptNode(const ScriptContext* ctx, ScriptNodeID* id, const ivec2& position, const Value& value, bool quote)
    : ScriptNode(id, position)
{
    switch (value.type)
    {
    case Value_Void :
        _type = Script_Node_Void;
        data = ScriptNodeVoid(ctx, id, *this, value, quote);
        break;
    case Value_Symbol :
        _type = Script_Node_Symbol;
        data = ScriptNodeSymbol(ctx, id, *this, value, quote);
        break;
    case Value_Integer :
        _type = Script_Node_Integer;
        data = ScriptNodeInteger(ctx, id, *this, value, quote);
        break;
    case Value_Float :
        _type = Script_Node_Float;
        data = ScriptNodeFloat(ctx, id, *this, value, quote);
        break;
    case Value_String :
        _type = Script_Node_String;
        data = ScriptNodeString(ctx, id, *this, value, quote);
        break;
    case Value_List :
        if (quote)
        {
            _type = Script_Node_List;
            data = ScriptNodeList(ctx, id, *this, value, quote);
        }
        else
        {
            if (isQuote(value))
            {
                _type = Script_Node_Execution;
                data = ScriptNodeExecution(ctx, id, *this, value, true);
            }
            else if (isExecution(value))
            {
                _type = Script_Node_Execution;
                data = ScriptNodeExecution(ctx, id, *this, value, quote);
            }
            else
            {
                _type = Script_Node_List;
                data = ScriptNodeList(ctx, id, *this, value, quote);
            }
        }
        break;
    case Value_Lambda :
        _type = Script_Node_Lambda;
        data = ScriptNodeLambda(ctx, id, *this, value, quote);
        break;
    case Value_Handle :
        _type = Script_Node_Handle;
        data = ScriptNodeHandle(ctx, id, *this, value, quote);
        break;
    }
}

ScriptNode ScriptNode::makeOutput(const ScriptContext* ctx, ScriptNodeID* id, const ivec2& position, const optional<Value>& value)
{
    ScriptNode node(id, position);

    node._type = Script_Node_Output;
    node.data = ScriptNodeOutput(ctx, id, node, value);

    return node;
}

ScriptNodeID ScriptNode::id() const
{
    return _id;
}

bool ScriptNode::operator==(const ScriptNode& rhs) const
{
    if (_id != rhs._id)
    {
        return false;
    }

    if (_position != rhs._position)
    {
        return false;
    }

    if (_type != rhs._type)
    {
        return false;
    }

    switch (_type)
    {
    case Script_Node_Output :
        if (get<ScriptNodeOutput>() != rhs.get<ScriptNodeOutput>())
        {
            return false;
        }
        break;
    case Script_Node_Execution :
        if (get<ScriptNodeExecution>() != rhs.get<ScriptNodeExecution>())
        {
            return false;
        }
        break;
    case Script_Node_Void :
        if (get<ScriptNodeVoid>() != rhs.get<ScriptNodeVoid>())
        {
            return false;
        }
        break;
    case Script_Node_Symbol :
        if (get<ScriptNodeSymbol>() != rhs.get<ScriptNodeSymbol>())
        {
            return false;
        }
        break;
    case Script_Node_Integer :
        if (get<ScriptNodeInteger>() != rhs.get<ScriptNodeInteger>())
        {
            return false;
        }
        break;
    case Script_Node_Float :
        if (get<ScriptNodeFloat>() != rhs.get<ScriptNodeFloat>())
        {
            return false;
        }
        break;
    case Script_Node_String :
        if (get<ScriptNodeString>() != rhs.get<ScriptNodeString>())
        {
            return false;
        }
        break;
    case Script_Node_List :
        if (get<ScriptNodeList>() != rhs.get<ScriptNodeList>())
        {
            return false;
        }
        break;
    case Script_Node_Lambda :
        if (get<ScriptNodeLambda>() != rhs.get<ScriptNodeLambda>())
        {
            return false;
        }
        break;
    case Script_Node_Handle :
        if (get<ScriptNodeHandle>() != rhs.get<ScriptNodeHandle>())
        {
            return false;
        }
        break;
    }

    return true;
}

bool ScriptNode::operator!=(const ScriptNode& rhs) const
{
    return !(*this == rhs);
}

bool ScriptNode::operator>(const ScriptNode& rhs) const
{
    return _id > rhs._id;
}

bool ScriptNode::operator<(const ScriptNode& rhs) const
{
    return _id < rhs._id;
}

bool ScriptNode::operator>=(const ScriptNode& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool ScriptNode::operator<=(const ScriptNode& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering ScriptNode::operator<=>(const ScriptNode& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

Value ScriptNode::value() const
{
    Value value;

    switch (_type)
    {
    case Script_Node_Output :
        if (get<ScriptNodeOutput>().value)
        {
            value = get<ScriptNodeOutput>().value->value();
        }
        break;
    case Script_Node_Execution :
        value.type = Value_List;
        value.l.reserve(1 + get<ScriptNodeExecution>().inputs.size());
        value.l.push_back(symbol(get<ScriptNodeExecution>().name));
        for (const shared_ptr<ScriptNode>& input : get<ScriptNodeExecution>().inputs)
        {
            value.l.push_back(input ? input->value() : Value());
        }
        break;
    case Script_Node_Void :
        value.type = Value_Void;
        break;
    case Script_Node_Symbol :
        value.type = Value_Symbol;
        value.s = get<ScriptNodeSymbol>().value;
        break;
    case Script_Node_Integer :
        value.type = Value_Integer;
        value.i = get<ScriptNodeInteger>().value;
        break;
    case Script_Node_Float :
        value.type = Value_Float;
        value.f = get<ScriptNodeFloat>().value;
        break;
    case Script_Node_String :
        value.type = Value_String;
        value.s = get<ScriptNodeString>().value;
        break;
    case Script_Node_List :
        value.type = Value_List;
        value.l.reserve(get<ScriptNodeList>().values.size());
        for (const shared_ptr<ScriptNode>& item : get<ScriptNodeList>().values)
        {
            value.l.push_back(item ? item->value() : Value());
        }
        break;
    case Script_Node_Lambda :
        value.type = Value_Lambda;
        value.lambda = get<ScriptNodeLambda>().lambda;
        break;
    case Script_Node_Handle :
        value.type = Value_Handle;
        value.id = get<ScriptNodeHandle>().id;
        break;
    }

    return value;
}

string ScriptNode::code() const
{
    return value().toPrettyString();
}

vector<string> ScriptNode::inputs(const ScriptContext* ctx) const
{
    switch (_type)
    {
    default :
        fatal("Encountered unknown script node type '" + to_string(_type) + "' while enumerating script node inputs.");
    case Script_Node_Output :
        return { "in" };
    case Script_Node_Execution :
    {
        optional<Value> definition = ctx->get(get<ScriptNodeExecution>().name);

        if (definition)
        {
            switch (definition->lambda.type)
            {
            default :
                fatal("Encountered unknown lambda type '" + to_string(definition->lambda.type) + "' while enumerating script node inputs.");
            case Lambda_Normal :
                return definition->lambda.argNames;
            case Lambda_Syntax :
            {
                // Not perfect, this doesn't deal with nested syntax
                vector<Value> argLayout = definition->lambda.argLayout;

                vector<string> inputs;
                inputs.reserve(argLayout.size());

                bool recurring =
                    find(argLayout.begin(), argLayout.end(), symbol("...")) != argLayout.end() ||
                    find(argLayout.begin(), argLayout.end(), symbol("...+")) != argLayout.end();

                for (size_t i = 0; recurring ? i <= definition->lambda.argLayout.size() : i < definition->lambda.argLayout.size(); i++)
                {
                    inputs.push_back(to_string(i));
                }

                return inputs;
            }
            case Lambda_Builtin :
            {
                const ScriptBuiltin* builtin = ctx->getBuiltin(definition->lambda.builtinName);

                switch (builtin->type())
                {
                default :
                    fatal("Encountered unknown script builtin type '" + to_string(builtin->type()) + "' enumerating script node inputs.");
                case Script_Builtin_Normal :
                    return builtin->argNames();
                case Script_Builtin_Syntax :
                case Script_Builtin_Variadic :
                {
                    vector<string> inputs;
                    inputs.reserve(get<ScriptNodeExecution>().inputs.size() + 1);

                    for (size_t i = 0; i <= get<ScriptNodeExecution>().inputs.size(); i++)
                    {
                        inputs.push_back(to_string(i));
                    }

                    return inputs;
                }
                }
            }
            }
        }
        else
        {
            vector<string> inputs;
            inputs.reserve(get<ScriptNodeExecution>().inputs.size() + 1);

            for (size_t i = 0; i <= get<ScriptNodeExecution>().inputs.size(); i++)
            {
                inputs.push_back(to_string(i));
            }

            return inputs;
        }
    }
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
    case Script_Node_Handle :
    case Script_Node_Lambda :
        return {};
    case Script_Node_List :
    {
        size_t count = get<ScriptNodeList>().values.size();

        vector<string> inputs;
        inputs.reserve(count + 1);

        inputs.push_back(to_string(count));

        bool found = false;

        for (size_t i = count - 1; i < count; i--)
        {
            if (!found && !get<ScriptNodeList>().values.at(i))
            {
                continue;
            }

            found |= get<ScriptNodeList>().values.at(i) != nullptr;

            inputs.insert(inputs.begin(), to_string(i));
        }

        return inputs;
    }
    }
}

vector<string> ScriptNode::outputs() const
{
    switch (_type)
    {
    default :
        fatal("Encountered unknown script node type '" + to_string(_type) + "' while enumerating script node outputs.");
    case Script_Node_Output :
        return {};
    case Script_Node_Execution :
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
    case Script_Node_List :
    case Script_Node_Lambda :
    case Script_Node_Handle :
        return { "out" };
    }
}

void ScriptNode::setPosition(const ivec2& position)
{
    _position = position;
}

const ivec2& ScriptNode::position() const
{
    return _position;
}

ivec2 ScriptNode::size(const ScriptContext* ctx) const
{
    i32 minHeight = UIScale::scriptNodeCapHeight() * 2 + UIScale::lineEditHeight();

    i32 height = UIScale::scriptNodeCapHeight() * 2 + max(inputs(ctx).size(), outputs().size()) * UIScale::scriptNodeLinkConnectorHeight();

    return ivec2(UIScale::scriptNodeWidth(), max(minHeight, height));
}

ivec2 ScriptNode::screenPosition(const ivec2& topLeft, const ivec2& viewportCenter, const ivec2& viewportSize) const
{
    ivec2 offset = viewportSize / 2;

    return topLeft + offset + (_position - viewportCenter);
}

ScriptNodeType ScriptNode::type() const
{
    return _type;
}

vector<shared_ptr<ScriptNode>> ScriptNode::children() const
{
    vector<shared_ptr<ScriptNode>> children;

    switch (_type)
    {
    case Script_Node_Output :
        if (get<ScriptNodeOutput>().value)
        {
            children = { get<ScriptNodeOutput>().value };
        }
        break;
    case Script_Node_Execution :
        for (const shared_ptr<ScriptNode>& input : get<ScriptNodeExecution>().inputs)
        {
            if (!input)
            {
                continue;
            }

            children.push_back(input);
        }
        break;
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
        break;
    case Script_Node_List :
        for (const shared_ptr<ScriptNode>& value : get<ScriptNodeList>().values)
        {
            if (!value)
            {
                continue;
            }

            children.push_back(value);
        }
        break;
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }

    return children;
}

set<shared_ptr<ScriptNode>> ScriptNode::allNodes(const shared_ptr<ScriptNode>& self) const
{
    set<shared_ptr<ScriptNode>> nodes = { self };

    switch (_type)
    {
    case Script_Node_Output :
        if (get<ScriptNodeOutput>().value)
        {
            nodes = join(nodes, get<ScriptNodeOutput>().value->allNodes(get<ScriptNodeOutput>().value));
        }
        break;
    case Script_Node_Execution :
        for (const shared_ptr<ScriptNode>& input : get<ScriptNodeExecution>().inputs)
        {
            if (!input)
            {
                continue;
            }

            set<shared_ptr<ScriptNode>> subNodes = input->allNodes(input);

            nodes.insert(subNodes.begin(), subNodes.end());
        }
        break;
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
        break;
    case Script_Node_List :
        for (const shared_ptr<ScriptNode>& value : get<ScriptNodeList>().values)
        {
            if (!value)
            {
                continue;
            }

            set<shared_ptr<ScriptNode>> subNodes = value->allNodes(value);

            nodes.insert(subNodes.begin(), subNodes.end());
        }
        break;
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }

    return nodes;
}

vector<shared_ptr<ScriptNode>> ScriptNode::inputNodes() const
{
    vector<shared_ptr<ScriptNode>> nodes;

    switch (_type)
    {
    case Script_Node_Output :
        nodes = { get<ScriptNodeOutput>().value };
        break;
    case Script_Node_Execution :
        nodes = get<ScriptNodeExecution>().inputs;
        break;
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
        break;
    case Script_Node_List :
        nodes = get<ScriptNodeList>().values;
        break;
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }

    return nodes;
}

shared_ptr<ScriptNode> ScriptNode::copyWithChildren() const
{
    ScriptNode copy = *this;

    switch (_type)
    {
    default :
        break;
    case Script_Node_Output :
        if (copy.get<ScriptNodeOutput>().value)
        {
            copy.get<ScriptNodeOutput>().value = copy.get<ScriptNodeOutput>().value->copyWithChildren();
        }
        break;
    case Script_Node_Execution :
        for (shared_ptr<ScriptNode>& input : copy.get<ScriptNodeExecution>().inputs)
        {
            if (input)
            {
                input = input->copyWithChildren();
            }
        }
        break;
    case Script_Node_List :
        for (shared_ptr<ScriptNode>& value : copy.get<ScriptNodeList>().values)
        {
            if (value)
            {
                value = value->copyWithChildren();
            }
        }
        break;
    }

    return make_shared<ScriptNode>(copy);
}

shared_ptr<ScriptNode> ScriptNode::copyWithoutChildren() const
{
    ScriptNode withoutChildren = *this;

    switch (_type)
    {
    case Script_Node_Output :
        withoutChildren.get<ScriptNodeOutput>().value = nullptr;
        break;
    case Script_Node_Execution :
        withoutChildren.get<ScriptNodeExecution>().inputs.clear();
        break;
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
        break;
    case Script_Node_List :
        withoutChildren.get<ScriptNodeList>().values.clear();
        break;
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }

    return make_shared<ScriptNode>(withoutChildren);
}

void ScriptNode::traverse(
    const shared_ptr<ScriptNode>& self,
    const function<void(shared_ptr<ScriptNode>, size_t, shared_ptr<ScriptNode>)>& behavior
) const
{
    switch (_type)
    {
    case Script_Node_Output :
        if (!get<ScriptNodeOutput>().value)
        {
            return;
        }

        behavior(self, 0, get<ScriptNodeOutput>().value);

        get<ScriptNodeOutput>().value->traverse(get<ScriptNodeOutput>().value, behavior);
        break;
    case Script_Node_Execution :
    {
        size_t i = 0;
        for (const shared_ptr<ScriptNode>& input : get<ScriptNodeExecution>().inputs)
        {
            if (!input)
            {
                i++;
                continue;
            }

            behavior(self, i, input);

            input->traverse(input, behavior);

            i++;
        }
        break;
    }
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
        break;
    case Script_Node_List :
    {
        size_t i = 0;
        for (const shared_ptr<ScriptNode>& value : get<ScriptNodeList>().values)
        {
            if (!value)
            {
                i++;
                continue;
            }

            behavior(self, i, value);

            value->traverse(value, behavior);

            i++;
        }
        break;
    }
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }
}

void ScriptNode::setInputNode(size_t index, const shared_ptr<ScriptNode>& node)
{
    switch (_type)
    {
    case Script_Node_Output :
        get<ScriptNodeOutput>().value = node;
        break;
    case Script_Node_Execution :
        while (index >= get<ScriptNodeExecution>().inputs.size())
        {
            get<ScriptNodeExecution>().inputs.push_back(nullptr);
        }

        get<ScriptNodeExecution>().inputs.at(index) = node;
        break;
    case Script_Node_List :
        while (index >= get<ScriptNodeList>().values.size())
        {
            get<ScriptNodeList>().values.push_back(nullptr);
        }

        get<ScriptNodeList>().values.at(index) = node;
        break;
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }
}

void ScriptNode::clearInputNode(const ScriptContext* ctx, size_t index)
{
    switch (_type)
    {
    case Script_Node_Output :
        get<ScriptNodeOutput>().value = nullptr;
        break;
    case Script_Node_Execution :
        while (index >= get<ScriptNodeExecution>().inputs.size())
        {
            get<ScriptNodeExecution>().inputs.push_back(nullptr);
        }

        get<ScriptNodeExecution>().inputs.at(index) = nullptr;
        break;
    case Script_Node_List :
        while (index >= get<ScriptNodeList>().values.size())
        {
            get<ScriptNodeList>().values.push_back(nullptr);
        }

        get<ScriptNodeList>().values.at(index) = nullptr;
        break;
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }

    compactInputs(ctx);
}

shared_ptr<ScriptNode> ScriptNode::getInputNode(size_t index) const
{
    switch (_type)
    {
    default :
        fatal("Encountered unknown script node type '" + to_string(_type) + "' while getting script node input node.");
    case Script_Node_Output :
        if (index == 0)
        {
            return get<ScriptNodeOutput>().value;
        }
        else
        {
            return nullptr;
        }
    case Script_Node_Execution :
        if (index < get<ScriptNodeExecution>().inputs.size())
        {
            return get<ScriptNodeExecution>().inputs.at(index);
        }
        else
        {
            return nullptr;
        }
    case Script_Node_List :
        if (index < get<ScriptNodeList>().values.size())
        {
            return get<ScriptNodeList>().values.at(index);
        }
        else
        {
            return nullptr;
        }
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
    case Script_Node_Lambda :
    case Script_Node_Handle :
        return nullptr;
    }
}

void ScriptNode::remove(ScriptNodeID id)
{
    switch (_type)
    {
    case Script_Node_Output :
    {
        auto& output = get<ScriptNodeOutput>();

        if (!output.value)
        {
            return;
        }

        if (output.value->id() == id)
        {
            output.value = nullptr;
        }
        else
        {
            output.value->remove(id);
        }
        break;
    }
    case Script_Node_Execution :
    {
        auto& execution = get<ScriptNodeExecution>();

        for (size_t i = 0; i < execution.inputs.size(); i++)
        {
            if (execution.inputs.at(i) && execution.inputs.at(i)->id() == id)
            {
                execution.inputs.at(i) = {};
            }
        }

        for (shared_ptr<ScriptNode>& node : execution.inputs)
        {
            if (!node)
            {
                continue;
            }

            node->remove(id);
        }
        break;
    }
    case Script_Node_Void :
    case Script_Node_Symbol :
    case Script_Node_Integer :
    case Script_Node_Float :
    case Script_Node_String :
        break;
    case Script_Node_List :
    {
        auto& list = get<ScriptNodeList>();

        for (size_t i = 0; i < list.values.size(); i++)
        {
            if (list.values.at(i) && list.values.at(i)->id() == id)
            {
                list.values.at(i) = {};
            }
        }

        for (shared_ptr<ScriptNode>& node : list.values)
        {
            if (!node)
            {
                continue;
            }

            node->remove(id);
        }
        break;
    }
    case Script_Node_Lambda :
    case Script_Node_Handle :
        break;
    }
}

void ScriptNode::setID(ScriptNodeID id)
{
    _id = id;
}

ScriptNode::ScriptNode(ScriptNodeID* id, const ivec2& position)
    : _id((*id)++)
    , _position(position)
    , _type(Script_Node_Output)
{

}

void ScriptNode::compactInputs(const ScriptContext* ctx)
{
    switch (_type)
    {
    default :
        break;
    case Script_Node_Execution :
    {
        auto& execution = get<ScriptNodeExecution>();

        auto compactInputs = [&execution](i64 min = 0) -> void
        {
            for (i64 i = execution.inputs.size() - 1; i >= min; i--)
            {
                if (execution.inputs.at(i))
                {
                    execution.inputs.erase(execution.inputs.begin() + i + 1, execution.inputs.end());
                    return;
                }
            }

            execution.inputs = {};
        };

        optional<Value> definition = ctx->get(execution.name);

        if (definition)
        {
            switch (definition->lambda.type)
            {
            case Lambda_Normal :
                break;
            case Lambda_Syntax :
            {
                // Not perfect, this doesn't deal with nested syntax
                vector<Value> argLayout = definition->lambda.argLayout;

                bool recurring =
                    find(argLayout.begin(), argLayout.end(), symbol("...")) != argLayout.end() ||
                    find(argLayout.begin(), argLayout.end(), symbol("...+")) != argLayout.end();

                if (recurring)
                {
                    compactInputs();
                }
                else
                {
                    compactInputs(argLayout.size());
                }
                break;
            }
            case Lambda_Builtin :
                const ScriptBuiltin* builtin = ctx->getBuiltin(definition->lambda.builtinName);

                switch (builtin->type())
                {
                case Script_Builtin_Normal :
                    break;
                case Script_Builtin_Syntax :
                case Script_Builtin_Variadic :
                    compactInputs();
                    break;
                }
            }
        }
        else
        {
            compactInputs();
        }
        break;
    }
    case Script_Node_List :
    {
        auto& list = get<ScriptNodeList>();

        for (size_t i = list.values.size() - 1; i < list.values.size(); i--)
        {
            if (list.values.at(i))
            {
                list.values.erase(list.values.begin() + i + 1, list.values.end());
                return;
            }
        }

        list.values = {};
        break;
    }
    }
}
