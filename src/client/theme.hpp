/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"

static constexpr Color white = { 255, 255, 255, 255 };
static constexpr Color black = { 0, 0, 0, 255 };
static constexpr Color pureAlpha = { 0, 0, 0, 0 };

struct Theme
{
    Color panelForegroundColor = white;
    Color worldBackgroundColor = black;

    Color backgroundColor = { 41, 43, 52, 255 };
    Color altBackgroundColor = { 34, 35, 43, 255 };
    Color softIndentColor = { 30, 31, 39, 255 };
    Color indentColor = { 22, 24, 29, 255 };
    Color outdentColor = { 56, 59, 71, 255 };
    Color accentColor = { 226, 95, 107, 255 };
    Color activeTextColor = white;
    Color textColor = { 220, 220, 220, 255 };
    Color inactiveTextColor = { 180, 180, 180, 255 };
    Color warningColor = { 231, 156, 34, 255 };
    Color errorColor = { 232, 23, 23, 255 };
    Color dividerColor = { 11, 12, 15, 255 };

    Color positiveColor = { 112, 186, 43 };
    Color negativeColor = { 255, 77, 65 };

    Color keywordColor = { 1, 164, 255, 255 };
    Color stdlibColor = { 0, 183, 181, 255 };
    Color bracketColor = { 150, 150, 150, 255 };
    Color quoteColor = { 192, 133, 230, 255 };
    Color escapedColor = { 255, 36, 36, 255 };
    Color stringColor = { 249, 114, 36, 255 };
    Color numberColor = { 3, 184, 0, 255 };

    Color cursorColor = { 200, 200, 200, 150 };
    Color bracketHighlightColor = { 255, 255, 255, 25 };

    Color xAxisColor = { 255, 0, 0, 255 };
    Color yAxisColor = { 0, 255, 0, 255 };
    Color zAxisColor = { 0, 0, 255, 255 };

    Color gridColor = { 102, 102, 102, 255 };
    Color gridSecondaryColor = { 76, 76, 76, 255 };

    Color touchInterfaceColor = { 76, 76, 76, 255 };
    Color touchInterfaceActiveColor = { 102, 102, 102, 255 };

    Color accentGradientXStartColor = { 201, 44, 102, 255 };
    Color accentGradientXEndColor = { 253, 162, 145, 255 };

    Color accentGradientYStartColor = { 216, 103, 59, 255 };
    Color accentGradientYEndColor = { 235, 72, 123, 255 };
};

extern Theme theme;
