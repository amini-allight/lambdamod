/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "theme_loader.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "theme.hpp"
#include "yaml_tools.hpp"

void loadTheme()
{
    string path = configPath() + themeFileName;

    if (!fs::exists(path))
    {
        return;
    }

    YAMLParser ctx(path);

    theme = ctx.root().as<Theme>();
}

void saveTheme()
{
    string path = configPath() + themeFileName;

    YAMLSerializer ctx(path);

    ctx.emit(theme);
}
