/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_entity.hpp"
#include "entity.hpp"

SoundEntity::SoundEntity(Sound* sound, const Entity* entity)
    : sound(sound)
    , id(entity->id())
    , audible(true)
{
    update(entity);
}

void SoundEntity::update(const Entity* entity)
{
    audible = entity->audible();

    for (auto it = playbacks.begin(); it != playbacks.end();)
    {
        if (!entity->samplePlaybacks().contains(it->first))
        {
            if (!it->second.depleted())
            {
                it->second.markForRemoval();
                it++;
            }
            else
            {
                playbacks.erase(it++);
            }
        }
        else
        {
            it++;
        }
    }

    for (const auto& [ id, playback ] : entity->samplePlaybacks())
    {
        auto it = playbacks.find(id);

        if (it == playbacks.end())
        {
            playbacks.insert({ id, SoundEntityPlayback(sound, entity, id) });
        }
        else
        {
            it->second.update(entity);
        }
    }

    for (auto& [ id, child ] : children)
    {
        child.update(entity->children().at(id));
    }
}

void SoundEntity::output(f64 multiplier)
{
    for (auto& [ id, playback ] : playbacks)
    {
        playback.output(multiplier);
    }

    for (auto& [ id, child ] : children)
    {
        child.output(multiplier);
    }
}

void SoundEntity::recordLastDistance()
{
    for (auto& [ id, playback ] : playbacks)
    {
        playback.recordLastDistance();
    }

    for (auto& [ id, child ] : children)
    {
        child.recordLastDistance();
    }
}

void SoundEntity::add(EntityID parentID, const Entity* entity)
{
    if (parentID == id)
    {
        auto it = children.find(entity->id());

        if (it != children.end())
        {
            return;
        }

        children.insert({ entity->id(), SoundEntity(sound, entity) });
    }
    else
    {
        for (auto& [ childID, child ] : children)
        {
            child.add(parentID, entity);
        }
    }
}

void SoundEntity::remove(EntityID parentID, EntityID id)
{
    if (parentID == this->id)
    {
        children.erase(id);
    }
    else
    {
        for (auto& [ childID, child ] : children)
        {
            child.remove(parentID, id);
        }
    }
}

void SoundEntity::update(EntityID parentID, const Entity* entity)
{
    if (parentID == id)
    {
        auto it = children.find(entity->id());

        if (it == children.end())
        {
            add(parentID, entity);
            return;
        }

        SoundEntity& child = it->second;

        child.update(entity);
    }
    else
    {
        for (auto& [ childID, child ] : children)
        {
            child.add(parentID, entity);
        }
    }
}
