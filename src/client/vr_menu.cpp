/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_menu.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "control_context.hpp"
#include "vr_render.hpp"
#include "panel_transforms.hpp"
#include "vr_panel_input_scope.hpp"

VRMenu::VRMenu(InterfaceView* view)
    : VRCapablePanel(view)
{
    START_BLOCKING_VR_PANEL_SCOPE(context()->input()->getScope("under-screen"), "vr-menu")
        WIDGET_SLOT("hide-vr-menu", dismiss)
    END_SCOPE
}

void VRMenu::dismiss(const Input& input)
{
    context()->vrMenuButtonHandler()->closeVRMenu();
}

optional<Point> VRMenu::toLocal(const vec3& position, const quaternion& rotation) const
{
    if (!shouldDraw())
    {
        return {};
    }

    vec3 roomHeadPosition = context()->controller()->vr()->worldToRoom(context()->vrViewer()->headTransform().position());
    vec3 roomHandPosition = context()->controller()->vr()->worldToRoom(position);
    quaternion roomHandRotation = context()->controller()->vr()->worldToRoom(rotation);

    auto [ width, height ] = dynamic_cast<VRRender*>(context()->controller()->render())->size();

    mat4 model = createVRHUDTransform(
        roomHeadPosition,
        static_cast<f64>(width),
        static_cast<f64>(height),
        dynamic_cast<VRRender*>(context()->controller()->render())->angle()
    );

    vec3 localStart = model.applyToPosition(roomHandPosition, false);
    vec3 localDirection = model.applyToDirection(roomHandRotation.forward(), false);

    vec2 flatStart = vec2(localStart.x, localStart.z);
    vec2 flatDirection = vec2(localDirection.x, localDirection.z).normalize();

    f64 a = flatDirection.dot(flatDirection);
    f64 b = 2 * flatStart.dot(flatDirection);
    f64 c = flatStart.dot(flatStart) - sq(hudRadius);

    auto [ r0, r1 ] = quadratic(a, b, c);

    optional<Point> point;

    for (f64 distance : vector<f64>({ r0, r1 }))
    {
        if (isnan(distance) || distance < 0)
        {
            continue;
        }

        distance = hypot(distance, tan(angleBetween(localDirection, vec3(localDirection.x, 0, localDirection.z).normalize())) * distance);

        vec3 contact = localStart + (localDirection * distance);

        f64 angle = angleBetween(
            vec2(contact.x, contact.z).normalize(),
            vec2(-1, 0)
        );
        f64 elevation = contact.y;

        if (angle < pi * 0.2 ||
            angle >= pi * 0.8 ||
            elevation < -1 ||
            elevation >= +1
        )
        {
            continue;
        }

        f64 x = (angle - (pi * 0.2)) / (pi * 0.6);
        f64 y = ((elevation * -1) + 1) / 2;

        point = Point(x * hudWidth, y * hudHeight);
        break;
    }

    return point;
}

SizeProperties VRMenu::sizeProperties() const
{
    return sizePropertiesFillAll;
}
#endif
