/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_gizmo.hpp"
#include "theme.hpp"
#include "paths.hpp"
#include "render_tools.hpp"

RenderGizmo::RenderGizmo(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    const array<bool, 9>& active
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->gizmo.pipelineLayout, pipelineStore->gizmo.pipeline)
{
    transform = mat4(position, rotation, vec3(1));

    string arrow = getFile(meshPath + "/" + gizmoArrowFileName + meshExt);
    const fvec3* arrowVertices = reinterpret_cast<const fvec3*>(arrow.data());
    u32 arrowVertexCount = arrow.size() / sizeof(fvec3);

    string ring = getFile(meshPath + "/" + gizmoRingFileName + meshExt);
    const fvec3* ringVertices = reinterpret_cast<const fvec3*>(ring.data());
    u32 ringVertexCount = ring.size() / sizeof(fvec3);

    string planes = getFile(meshPath + "/" + gizmoPlanesFileName + meshExt);
    const fvec3* planesVertices = reinterpret_cast<const fvec3*>(planes.data());
    u32 planesVertexCount = planes.size() / sizeof(fvec3);

    vertexCount = arrowVertexCount * 3 + ringVertexCount * 3 + planesVertexCount * 3;

    vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));

    size_t writeHead = 0;

    fvec3 activeColors[] = {
        theme.xAxisColor.toVec3(),
        theme.yAxisColor.toVec3(),
        theme.zAxisColor.toVec3()
    };

    vector<ColoredVertex> vertices(vertexCount);

    for (u32 dimension = 0; dimension < 3; dimension++)
    {
        for (u32 j = 0; j < arrowVertexCount; j++)
        {
            vertices[writeHead] = ColoredVertex(
                gizmoElementTransforms[dimension].applyToPosition(arrowVertices[j]),
                active[dimension] ? activeColors[dimension] : fvec3(theme.gridColor.toVec3())
            );
            writeHead++;
        }

        for (u32 j = 0; j < ringVertexCount; j++)
        {
            vertices[writeHead] = ColoredVertex(
                gizmoElementTransforms[dimension].applyToPosition(ringVertices[j]),
                active[3 + dimension] ? activeColors[dimension] : fvec3(theme.gridColor.toVec3())
            );
            writeHead++;
        }

        for (u32 j = 0; j < planesVertexCount; j++)
        {
            vertices[writeHead] = ColoredVertex(
                gizmoElementTransforms[dimension].applyToPosition(planesVertices[j]),
                active[6 + dimension] ? activeColors[dimension] : fvec3(theme.gridColor.toVec3())
            );
            writeHead++;
        }
    }

    for (size_t i = 0; i < vertexCount; i += 3)
    {
        swap(vertices[i + 1], vertices[i + 2]);
    }

    VmaMapping<ColoredVertex> mapping(ctx->allocator, this->vertices);

    memcpy(mapping.data, vertices.data(), vertices.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderGizmo::~RenderGizmo()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderGizmo::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    transform.setScale(vec3(cameraPosition.distance(transform.position()) / gizmoBaseDistance));

    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

RenderDecorationType RenderGizmo::drawType() const
{
    return Render_Decoration_Independent_Depth;
}

VmaBuffer RenderGizmo::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
