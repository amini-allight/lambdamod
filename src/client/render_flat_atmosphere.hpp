/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_atmosphere.hpp"
#include "render_flat_pipeline_store.hpp"
#include "render_context.hpp"

class RenderFlatAtmosphere final : RenderAtmosphere
{
public:
    RenderFlatAtmosphere(
        const RenderInterface* render,

        const vector<FlatSwapchainElement*>& swapchainElements,

        VkDescriptorSetLayout descriptorSetLayout,
        const RenderFlatPipelineStore* pipelineStore
    );
    ~RenderFlatAtmosphere();

    void work(
        const FlatSwapchainElement* swapchainElement,
        const AtmosphereParameters& atmosphere,
        const vec3& cameraPosition,
        const vec3& up,
        const vec3& gravity,
        const vec3& flowVelocity,
        f64 density
    );
    void refresh(const vector<FlatSwapchainElement*>& swapchainElements);

private:
    vector<FlatSwapchainElement*> swapchainElements;

    void createComponents() override;
};
