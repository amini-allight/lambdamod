/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

#pragma pack(1)
struct ColoredVertex
{
    ColoredVertex();
    ColoredVertex(const fvec3& position, const fvec3& color);

    fvec3 position;
    fvec3 color;
};

struct TexturedVertex
{
    TexturedVertex();
    TexturedVertex(const fvec3& position, const fvec2& uv);

    fvec3 position;
    fvec2 uv;
};

struct RopeVertex
{
    RopeVertex();
    RopeVertex(const fvec3& position, const fvec4& rotation, const fvec3& scale, const fvec3& color);

    fvec3 position;
    fvec4 rotation;
    fvec3 scale;
    fvec3 color;
};

struct LitColoredVertex
{
    LitColoredVertex();
    LitColoredVertex(const fvec3& position, const fvec3& normal, const fvec4& color);

    fvec3 position;
    fvec3 normal;
    fvec4 color;
};

struct LitTexturedVertex
{
    LitTexturedVertex();
    LitTexturedVertex(const fvec3& position, const fvec2& uv, const fvec3& normal);

    fvec3 position;
    fvec2 uv;
    fvec3 normal;
};

struct LitRopeVertex
{
    LitRopeVertex();
    LitRopeVertex(const fvec3& position, const fvec4& rotation, const fvec3& scale, const fvec4& color);

    fvec3 position;
    fvec4 rotation;
    fvec3 scale;
    fvec4 color;
};
#pragma pack()
