/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "color_hue_picker.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

ColorHuePicker::ColorHuePicker(
    InterfaceWidget* parent,
    const function<f64()>& source,
    const function<void(f64)>& onSet
)
    : InterfaceWidget(parent)
    , source(source)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("color-hue-picker")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void ColorHuePicker::draw(const DrawContext& ctx) const
{
    static constexpr Color hues[]{
        Color(255, 0, 0),
        Color(255, 255, 0),
        Color(0, 255, 0),
        Color(0, 255, 255),
        Color(0, 0, 255),
        Color(255, 0, 255),
        Color(255, 0, 0)
    };
    static constexpr size_t hueCount = (sizeof(hues) / sizeof(Color)) - 1;

    for (size_t i = 0; i < hueCount; i++)
    {
        drawGradient1D(
            ctx,
            this,
            Point(0, i * (size().h / hueCount)),
            Size(size().w, size().h / hueCount),
            hues[i],
            hues[i + 1]
        );
    }

    drawSolid(
        ctx,
        nullptr,
        position() + Point(-UIScale::marginSize() / 2, source() * size().h - UIScale::hueSliderSize().h / 2),
        UIScale::hueSliderSize(),
        theme.outdentColor
    );
}

void ColorHuePicker::interact(const Input& input)
{
    Point local = pointer - position();

    f64 hue = static_cast<f64>(local.y) / size().h;

    playPositiveActivateEffect();
    onSet(hue);
}

SizeProperties ColorHuePicker::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::hueBarWidth()), 0,
        Scaling_Fixed, Scaling_Fill
    };
}
