/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "quaternion_edit.hpp"
#include "euler_edit.hpp"

class RotationEdit : public InterfaceWidget
{
public:
    RotationEdit(
        InterfaceWidget* parent,
        const function<quaternion()>& source,
        const function<void(const quaternion&)>& onSet
    );

    void set(const quaternion& value);
    quaternion value() const;
    void clear();

private:
    QuaternionEdit* quaternionEdit;
    EulerEdit* eulerEdit;

    SizeProperties sizeProperties() const override;
};
