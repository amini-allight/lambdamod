/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_repeat.hpp"
#include "tools.hpp"

static constexpr chrono::milliseconds maxRepeatInterval(500);

InputRepeat::InputRepeat(const Input& input)
    : type(input.type)
    , up(input.up)
    , shift(input.shift)
    , control(input.control)
    , alt(input.alt)
    , super(input.super)
    , _count(0)
{
    increment();
}

bool InputRepeat::match(const Input& input) const
{
    return input.type == type &&
        input.up == up &&
        input.shift == shift &&
        input.control == control &&
        input.alt == alt &&
        input.super == super;
}

bool InputRepeat::expired() const
{
    return currentTime() - lastTime > maxRepeatInterval;
}

void InputRepeat::increment()
{
    _count++;
    lastTime = currentTime();
}

u32 InputRepeat::count() const
{
    return _count;
}
