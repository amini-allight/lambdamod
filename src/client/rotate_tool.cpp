/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "rotate_tool.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "localization.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"

RotateTool::RotateTool(
    ViewerModeContext* ctx,
    const mat4& localSpace,
    const function<void(size_t, const vec3&, const mat4&, const mat4&)>& setGlobal,
    const function<mat4(size_t)>& getGlobal,
    const function<mat4(size_t)>& getGlobalWithPromotion,
    const function<size_t()>& count,
    bool soft,
    const Point& point
)
    : EditTool(ctx, localSpace, setGlobal, getGlobal, getGlobalWithPromotion, count, soft)
    , cameraPosition(ctx->context()->flatViewer()->position())
    , defaultAxis(ctx->context()->flatViewer()->rotation().forward())
    , onScreenAngle(0)
    , onScreenCenter(getOnScreenCenter())
    , onScreenCursor(point)
    , onScreenStart(point)
{

}

RotateTool::~RotateTool()
{

}

string RotateTool::name() const
{
    return localize("rotate-tool-rotate");
}

string RotateTool::extent() const
{
    return toPrettyString(localizeAngle(angle())) + " " + angleSuffix();
}

void RotateTool::use(const Point& point)
{
    onScreenCursor = point;
    onScreenAngle = handedAngleBetween(
        (onScreenStart.toVec2() - onScreenCenter.toVec2()).normalize(),
        (point.toVec2() - onScreenCenter.toVec2()).normalize()
    );

    update();
}

void RotateTool::update()
{
    for (size_t i = 0; i < count(); i++)
    {
        setGlobal(i, globalPivot(i), initialGlobal(i), apply(globalPivot(i), initialGlobal(i)));
    }
}

void RotateTool::cancel()
{
    for (size_t i = 0; i < count(); i++)
    {
        setGlobal(i, globalPivot(i), initialGlobal(i), initialGlobal(i));
    }
}

void RotateTool::end()
{
    // Do nothing
}

void RotateTool::togglePivotMode(const Point& point)
{
    EditTool::togglePivotMode(point);

    onScreenAngle = 0;
    onScreenCenter = getOnScreenCenter();
    onScreenCursor = point;
    onScreenStart = point;

    update();
}

Point RotateTool::pivotPoint() const
{
    return onScreenCenter;
}

Point RotateTool::cursorPoint() const
{
    return onScreenCursor;
}

optional<vec3> RotateTool::globalAxis() const
{
    switch (toolAxis)
    {
    default :
    case Edit_Tool_Axis_None : return defaultAxis;
    case Edit_Tool_Axis_Global_X : return correctAxisOrientation(rightDir);
    case Edit_Tool_Axis_Global_Y : return correctAxisOrientation(forwardDir);
    case Edit_Tool_Axis_Global_Z : return correctAxisOrientation(upDir);
    case Edit_Tool_Axis_Local_X : return correctAxisOrientation(localSpace.right());
    case Edit_Tool_Axis_Local_Y : return correctAxisOrientation(localSpace.forward());
    case Edit_Tool_Axis_Local_Z : return correctAxisOrientation(localSpace.up());
    case Edit_Tool_Axis_Self_X :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).right();
        }

        if (dir.length() == 0)
        {
            return correctAxisOrientation(rightDir);
        }
        else
        {
            return correctAxisOrientation(dir.normalize());
        }
    }
    case Edit_Tool_Axis_Self_Y :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).forward();
        }

        if (dir.length() == 0)
        {
            return correctAxisOrientation(forwardDir);
        }
        else
        {
            return correctAxisOrientation(dir.normalize());
        }
    }
    case Edit_Tool_Axis_Self_Z :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).up();
        }

        if (dir.length() == 0)
        {
            return correctAxisOrientation(upDir);
        }
        else
        {
            return correctAxisOrientation(dir.normalize());
        }
    }
    }
}

f64 RotateTool::angle() const
{
    f64 angle;

    if (numericInput)
    {
        angle = radians(numericInputValue());
    }
    else
    {
        angle = onScreenAngle;
    }

    return snap(angle);
}

f64 RotateTool::snap(f64 v) const
{
    if (!snapping() || roughly<f64>(ctx->rotationStep(), 0))
    {
        return v;
    }

    return floor(v / ctx->rotationStep()) * ctx->rotationStep();
}

mat4 RotateTool::apply(const vec3& pivot, mat4 transform) const
{
    quaternion rotation(*globalAxis(), angle());

    transform.setPosition(pivot + rotation.rotate(transform.position() - pivot));
    transform.setRotation(rotation * transform.rotation());

    return transform;
}

vec3 RotateTool::correctAxisOrientation(const vec3& axis) const
{
    // Flips axises as required so the rotation direction is always clockwise from the camera's perspective
    return sign((globalPivot() - cameraPosition).normalize().dot(axis)) * axis;
}
