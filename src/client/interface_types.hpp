/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "tools.hpp"

#include <vulkan/vulkan.h>

enum Icon : u8
{
    Icon_None,
    Icon_Checked,
    Icon_Icon,
    Icon_Title,
    Icon_Amini_Allight,
    Icon_Spinner,
    Icon_Saving,
    Icon_Parent,
    Icon_Active,
    Icon_Inactive,
    Icon_Shared,
    Icon_Attached,
    Icon_Host,
    Icon_Text,
    Icon_Timeout,
    Icon_User,
    Icon_Ping,
    Icon_Brake,
    Icon_Add,
    Icon_Subtract,
    Icon_Remove,
    Icon_Up,
    Icon_Down,
    Icon_Left,
    Icon_Right,
    Icon_Yes,
    Icon_No,
    Icon_Entity_Control_Point,
    Icon_Part_Control_Point,
    Icon_Subpart_Control_Point,
    Icon_Dot,
    Icon_Anchor,
    Icon_Light,
    Icon_Force,
    Icon_Area,
    Icon_Frame,
    Icon_Play,
    Icon_Pause,
    Icon_Stop,
    Icon_Go_To_Start,
    Icon_Go_To_End,
    Icon_Online,
    Icon_Offline,
    Icon_VR,
    Icon_Flat,
    Icon_Admin,
    Icon_Shown,
    Icon_Hidden,
    Icon_Alpha,
    Icon_Window_Close,
    Icon_Window_Maximize,
    Icon_Window_Unmaximize
};

struct Size;
struct Point;
struct Color;

struct Size
{
    constexpr Size()
    {
        w = 0;
        h = 0;
    }

    explicit constexpr Size(i32 v)
    {
        w = v;
        h = v;
    }

    constexpr Size(i32 w, i32 h)
    {
        this->w = w;
        this->h = h;
    }

    constexpr Size(const vec2& v)
    {
        w = v.x;
        h = v.y;
    }

    constexpr Size(const ivec2& v)
    {
        w = v.x;
        h = v.y;
    }

    constexpr Point toPoint() const;

    constexpr vec2 toVec2() const
    {
        return { static_cast<f64>(w), static_cast<f64>(h) };
    }

    i32 w;
    i32 h;

    constexpr bool operator==(const Size& rhs) const
    {
        return w == rhs.w && h == rhs.h;
    }

    constexpr bool operator!=(const Size& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr Size operator+(const Size& rhs) const
    {
        return {
            w + rhs.w,
            h + rhs.h
        };
    }

    constexpr Size operator-(const Size& rhs) const
    {
        return {
            w - rhs.w,
            h - rhs.h
        };
    }

    constexpr Size operator*(const Size& rhs) const
    {
        return {
            w * rhs.w,
            h * rhs.h
        };
    }

    constexpr Size operator/(const Size& rhs) const
    {
        return {
            w / rhs.w,
            h / rhs.h
        };
    }

    constexpr Size operator+(const Point& rhs) const;
    constexpr Size operator-(const Point& rhs) const;
    constexpr Size operator*(const Point& rhs) const;
    constexpr Size operator/(const Point& rhs) const;

    constexpr Size operator+(i32 v) const
    {
        return {
            w + v,
            h + v
        };
    }

    constexpr Size operator-(i32 v) const
    {
        return {
            w - v,
            h - v
        };
    }

    constexpr Size operator*(i32 v) const
    {
        return {
            w * v,
            h * v
        };
    }

    constexpr Size operator/(i32 v) const
    {
        return {
            w / v,
            h / v
        };
    }

    constexpr Size operator+(f64 v) const
    {
        return Size(
            w + v,
            h + v
        );
    }

    constexpr Size operator-(f64 v) const
    {
        return Size(
            w - v,
            h - v
        );
    }

    constexpr Size operator*(f64 v) const
    {
        return Size(
            w * v,
            h * v
        );
    }

    constexpr Size operator/(f64 v) const
    {
        return Size(
            w / v,
            h / v
        );
    }

    constexpr Size& operator+=(const Size& rhs)
    {
        w += rhs.w;
        h += rhs.h;

        return *this;
    }

    constexpr Size& operator-=(const Size& rhs)
    {
        w -= rhs.w;
        h -= rhs.h;

        return *this;
    }

    constexpr Size& operator*=(const Size& rhs)
    {
        w *= rhs.w;
        h *= rhs.h;

        return *this;
    }

    constexpr Size& operator/=(const Size& rhs)
    {
        w /= rhs.w;
        h /= rhs.h;

        return *this;
    }

    constexpr Size& operator+=(const Point& rhs);
    constexpr Size& operator-=(const Point& rhs);
    constexpr Size& operator*=(const Point& rhs);
    constexpr Size& operator/=(const Point& rhs);

    constexpr Size& operator+=(i32 v)
    {
        w += v;
        h += v;

        return *this;
    }

    constexpr Size& operator-=(i32 v)
    {
        w -= v;
        h -= v;

        return *this;
    }

    constexpr Size& operator*=(i32 v)
    {
        w *= v;
        h *= v;

        return *this;
    }

    constexpr Size& operator/=(i32 v)
    {
        w /= v;
        h /= v;

        return *this;
    }

    constexpr Size& operator+=(f64 v)
    {
        w += v;
        h += v;

        return *this;
    }

    constexpr Size& operator-=(f64 v)
    {
        w -= v;
        h -= v;

        return *this;
    }

    constexpr Size& operator*=(f64 v)
    {
        w *= v;
        h *= v;

        return *this;
    }

    constexpr Size& operator/=(f64 v)
    {
        w /= v;
        h /= v;

        return *this;
    }

    constexpr Size clamp(const Size& size) const
    {
        return {
            w < size.w ? w : size.w,
            h < size.h ? h : size.h
        };
    }
};

constexpr Size operator+(i32 v, const Size& size)
{
    return {
        v + size.w,
        v + size.h
    };
}

constexpr Size operator-(i32 v, const Size& size)
{
    return {
        v - size.w,
        v - size.h
    };
}

constexpr Size operator*(i32 v, const Size& size)
{
    return {
        v * size.w,
        v * size.h
    };
}

constexpr Size operator/(i32 v, const Size& size)
{
    return {
        v / size.w,
        v / size.h
    };
}

constexpr Size operator+(f64 v, const Size& size)
{
    return Size(
        v + size.w,
        v + size.h
    );
}

constexpr Size operator-(f64 v, const Size& size)
{
    return Size(
        v - size.w,
        v - size.h
    );
}

constexpr Size operator*(f64 v, const Size& size)
{
    return Size(
        v * size.w,
        v * size.h
    );
}

constexpr Size operator/(f64 v, const Size& size)
{
    return Size(
        v / size.w,
        v / size.h
    );
}

inline ostream& operator<<(ostream& out, const Size& size)
{
    out << "(" << size.w << ", " << size.h << ")";

    return out;
}

struct Point
{
    constexpr Point()
    {
        x = 0;
        y = 0;
    }

    explicit constexpr Point(i32 v)
    {
        x = v;
        y = v;
    }

    constexpr Point(i32 x, i32 y)
    {
        this->x = x;
        this->y = y;
    }

    constexpr Point(const vec2& v)
    {
        x = v.x;
        y = v.y;
    }

    constexpr Point(const ivec2& v)
    {
        x = v.x;
        y = v.y;
    }

    constexpr Size toSize() const
    {
        return { x, y };
    }

    constexpr vec2 toVec2() const
    {
        return { static_cast<f64>(x), static_cast<f64>(y) };
    }

    i32 x;
    i32 y;

    constexpr bool operator==(const Point& rhs) const
    {
        return x == rhs.x && y == rhs.y;
    }

    constexpr bool operator!=(const Point& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr Point operator+(const Point& rhs) const
    {
        return {
            x + rhs.x,
            y + rhs.y
        };
    }

    constexpr Point operator-(const Point& rhs) const
    {
        return {
            x - rhs.x,
            y - rhs.y
        };
    }

    constexpr Point operator*(const Point& rhs) const
    {
        return {
            x * rhs.x,
            y * rhs.y
        };
    }

    constexpr Point operator/(const Point& rhs) const
    {
        return {
            x / rhs.x,
            y / rhs.y
        };
    }

    constexpr Point operator+(const Size& rhs) const
    {
        return {
            x + rhs.w,
            y + rhs.h
        };
    }

    constexpr Point operator-(const Size& rhs) const
    {
        return {
            x - rhs.w,
            y - rhs.h
        };
    }

    constexpr Point operator*(const Size& rhs) const
    {
        return {
            x * rhs.w,
            y * rhs.h
        };
    }

    constexpr Point operator/(const Size& rhs) const
    {
        return {
            x / rhs.w,
            y / rhs.h
        };
    }

    constexpr Point operator+(i32 v) const
    {
        return {
            x + v,
            y + v
        };
    }

    constexpr Point operator-(i32 v) const
    {
        return {
            x - v,
            y - v
        };
    }

    constexpr Point operator*(i32 v) const
    {
        return {
            x * v,
            y * v
        };
    }

    constexpr Point operator/(i32 v) const
    {
        return {
            x / v,
            y / v
        };
    }

    constexpr Point operator+(f64 v) const
    {
        return Point(
            x + v,
            y + v
        );
    }

    constexpr Point operator-(f64 v) const
    {
        return Point(
            x - v,
            y - v
        );
    }

    constexpr Point operator*(f64 v) const
    {
        return Point(
            x * v,
            y * v
        );
    }

    constexpr Point operator/(f64 v) const
    {
        return Point(
            x / v,
            y / v
        );
    }

    constexpr Point& operator+=(const Point& rhs)
    {
        x += rhs.x;
        y += rhs.y;

        return *this;
    }

    constexpr Point& operator-=(const Point& rhs)
    {
        x -= rhs.x;
        y -= rhs.y;

        return *this;
    }

    constexpr Point& operator*=(const Point& rhs)
    {
        x *= rhs.x;
        y *= rhs.y;

        return *this;
    }

    constexpr Point& operator/=(const Point& rhs)
    {
        x /= rhs.x;
        y /= rhs.y;

        return *this;
    }

    constexpr Point& operator+=(const Size& rhs)
    {
        x += rhs.w;
        y += rhs.h;

        return *this;
    }

    constexpr Point& operator-=(const Size& rhs)
    {
        x -= rhs.w;
        y -= rhs.h;

        return *this;
    }

    constexpr Point& operator*=(const Size& rhs)
    {
        x *= rhs.w;
        y *= rhs.h;

        return *this;
    }

    constexpr Point& operator/=(const Size& rhs)
    {
        x /= rhs.w;
        y /= rhs.h;

        return *this;
    }

    constexpr Point& operator+=(i32 v)
    {
        x += v;
        y += v;

        return *this;
    }

    constexpr Point& operator-=(i32 v)
    {
        x -= v;
        y -= v;

        return *this;
    }

    constexpr Point& operator*=(i32 v)
    {
        x *= v;
        y *= v;

        return *this;
    }

    constexpr Point& operator/=(i32 v)
    {
        x /= v;
        y /= v;

        return *this;
    }

    constexpr Point& operator+=(f64 v)
    {
        x += v;
        y += v;

        return *this;
    }

    constexpr Point& operator-=(f64 v)
    {
        x -= v;
        y -= v;

        return *this;
    }

    constexpr Point& operator*=(f64 v)
    {
        x *= v;
        y *= v;

        return *this;
    }

    constexpr Point& operator/=(f64 v)
    {
        x /= v;
        y /= v;

        return *this;
    }
};

constexpr Point operator+(i32 v, const Point& point)
{
    return {
        v + point.x,
        v + point.y
    };
}

constexpr Point operator-(i32 v, const Point& point)
{
    return {
        v - point.x,
        v - point.y
    };
}

constexpr Point operator*(i32 v, const Point& point)
{
    return {
        v * point.x,
        v * point.y
    };
}

constexpr Point operator/(i32 v, const Point& point)
{
    return {
        v / point.x,
        v / point.y
    };
}

constexpr Point operator+(f64 v, const Point& point)
{
    return Point(
        v + point.x,
        v + point.y
    );
}

constexpr Point operator-(f64 v, const Point& point)
{
    return Point(
        v - point.x,
        v - point.y
    );
}

constexpr Point operator*(f64 v, const Point& point)
{
    return Point(
        v * point.x,
        v * point.y
    );
}

constexpr Point operator/(f64 v, const Point& point)
{
    return Point(
        v / point.x,
        v / point.y
    );
}

constexpr Point Size::toPoint() const
{
    return { w, h };
}

constexpr Size Size::operator+(const Point& rhs) const
{
    return {
        w + rhs.x,
        h + rhs.y
    };
}

constexpr Size Size::operator-(const Point& rhs) const
{
    return {
        w - rhs.x,
        h - rhs.y
    };
}

constexpr Size Size::operator*(const Point& rhs) const
{
    return {
        w * rhs.x,
        h * rhs.y
    };
}

constexpr Size Size::operator/(const Point& rhs) const
{
    return {
        w / rhs.x,
        h / rhs.y
    };
}

constexpr Size& Size::operator+=(const Point& rhs)
{
    w += rhs.x;
    h += rhs.y;

    return *this;
}

constexpr Size& Size::operator-=(const Point& rhs)
{
    w -= rhs.x;
    h -= rhs.y;

    return *this;
}

constexpr Size& Size::operator*=(const Point& rhs)
{
    w *= rhs.x;
    h *= rhs.y;

    return *this;
}

constexpr Size& Size::operator/=(const Point& rhs)
{
    w /= rhs.x;
    h /= rhs.y;

    return *this;
}

inline ostream& operator<<(ostream& out, const Point& point)
{
    out << "(" << point.x << ", " << point.y << ")";

    return out;
}

struct Color;

constexpr string colorToHex(const Color& color, bool alpha);
constexpr Color colorFromHex(const string& s);

struct Color
{
    constexpr Color()
    {
        r = 255;
        g = 255;
        b = 255;
        a = 255;
    }

    explicit constexpr Color(u8 v)
    {
        r = v;
        g = v;
        b = v;
        a = v;
    }

    constexpr Color(u8 r, u8 g, u8 b)
    {
        this->r = r;
        this->g = g;
        this->b = b;
        a = 255;
    }

    constexpr Color(u8 r, u8 g, u8 b, u8 a)
    {
        this->r = r;
        this->g = g;
        this->b = b;
        this->a = a;
    }

    constexpr Color(const vec3& v)
    {
        this->r = v.x * 255;
        this->g = v.y * 255;
        this->b = v.z * 255;
        a = 255;
    }

    constexpr Color(const vec4& v)
    {
        this->r = v.x * 255;
        this->g = v.y * 255;
        this->b = v.z * 255;
        this->a = v.w * 255;
    }

    constexpr Color(const string& s)
    {
        *this = Color(colorFromHex(s));
    }

    constexpr vec3 toVec3() const
    {
        return vec3(r / 255.0, g / 255.0, b / 255.0);
    }

    constexpr vec4 toVec4() const
    {
        return vec4(r / 255.0, g / 255.0, b / 255.0, a / 255.0);
    }

    constexpr string toString(bool alpha) const
    {
        return colorToHex(*this, alpha);
    }

    u8 r;
    u8 g;
    u8 b;
    u8 a;

    constexpr bool operator==(const Color& rhs) const
    {
        return r == rhs.r && g == rhs.g && b == rhs.b && a == rhs.a;
    }

    constexpr bool operator!=(const Color& rhs) const
    {
        return !(*this == rhs);
    }
};

inline ostream& operator<<(ostream& out, const Color& color)
{
    out << colorToHex(color, true);

    return out;
}

constexpr string colorToHex(const Color& color, bool alpha)
{
    string s = "#";

    s += byteToHex(color.r);
    s += byteToHex(color.g);
    s += byteToHex(color.b);

    if (alpha)
    {
        s += byteToHex(color.a);
    }

    return s;
}

constexpr Color colorFromHex(const string& s)
{
    Color color;

    if ((s.size() != 7 && s.size() != 9) || s[0] != '#')
    {
        return color;
    }

    color.r = stoi(s.substr(1, 2), 0, 16);
    color.g = stoi(s.substr(3, 2), 0, 16);
    color.b = stoi(s.substr(5, 2), 0, 16);

    if (s.size() == 9)
    {
        color.a = stoi(s.substr(7, 2), 0, 16);
    }

    return color;
}

struct DrawContext
{
    VkCommandBuffer commandBuffer;
    VkCommandBuffer transferCommandBuffer;
    Size size;
    bool ui;
    mutable vector<function<void(const DrawContext&)>> delayedCommands;
};

enum SelectMode : u8
{
    Select_Replace,
    Select_Add,
    Select_Remove
};

enum PointerType : u8
{
    Pointer_Normal,
    Pointer_VR_Left,
    Pointer_VR_Right
};
