/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "interface_oneshot.hpp"
#include "interface_marker.hpp"
#include "interface_log_oneshot.hpp"

class ControlContext;
class Entity;

void drawBoxSelect(const DrawContext& drawCtx, ControlContext* ctx, const Point& start, const Point& end);
void drawFrameCounter(const DrawContext& drawCtx, ControlContext* ctx);
void drawPingCounter(const DrawContext& drawCtx, ControlContext* ctx, u32 ping);
void drawStatusLine(const DrawContext& drawCtx, ControlContext* ctx);
void drawOneshot(const DrawContext& drawCtx, ControlContext* ctx, const InterfaceOneshot& oneshot);
void drawMarker(const DrawContext& drawCtx, ControlContext* ctx, const InterfaceMarker& marker);
void drawBody(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity);
void drawEntityLinks(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity);
void drawEntity(const DrawContext& drawCtx, ControlContext* ctx, const Entity* entity, bool hovered);
void drawLogOneshots(const DrawContext& drawCtx, ControlContext* ctx, const vector<InterfaceLogOneshot>& oneshots);
