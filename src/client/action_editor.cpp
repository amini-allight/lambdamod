/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "viewer_mode_context.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "row.hpp"
#include "column.hpp"
#include "action_timeline.hpp"
#include "action_editor_side_panel.hpp"
#include "timeline_controls.hpp"
#include "icon_button.hpp"
#include "dynamic_icon_button.hpp"
#include "side_panel.hpp"
#include "label.hpp"
#include "uint_edit.hpp"
#include "spacer.hpp"

ActionEditor::ActionEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
    , previewPlayback(context()->viewerMode(), id)
{
    START_WIDGET_SCOPE("action-editor")
        WIDGET_SLOT("toggle-side-panel", toggleSidePanel)
    END_SCOPE

    column = new Column(this);

    timeline = new ActionTimeline(
        column,
        id,
        bind(&ActionEditor::playPositionSource, this),
        bind(&ActionEditor::onPlayPause, this),
        bind(&ActionEditor::onSeek, this, placeholders::_1),
        bind(&ActionEditor::onSelect, this, placeholders::_1)
    );

    new TimelineControls(
        column,
        bind(&ActionEditor::playingSource, this),
        bind(&ActionEditor::onGoToStart, this),
        bind(&ActionEditor::onPlayPause, this),
        bind(&ActionEditor::onStop, this),
        bind(&ActionEditor::onGoToEnd, this)
    );

    sidePanel = new ActionEditorSidePanel(this);
}

void ActionEditor::addActionPart(BodyPartID id)
{
    edit([this, id](Action& action) -> void {
        ActionPartID partID = action.add();

        ActionPart part(partID);
        part.setEnd(previewPlayback.position());
        part.setTargetID(id);

        action.add(part);
    });
}

void ActionEditor::removeActionPart(BodyPartID id)
{
    vector<ActionPartID> deletions;

    for (const auto& [ partID, part ] : currentAction().parts())
    {
        if (part.targetID() == id && part.end() == previewPlayback.position())
        {
            deletions.push_back(partID);
        }
    }

    edit([deletions](Action& action) -> void {
        for (ActionPartID deletion : deletions)
        {
            action.remove(deletion);
        }
    });
}

mat4 ActionEditor::getTransform(BodyPartID id) const
{
    const Action& action = currentAction();

    for (const auto& [ partID, part ] : action.parts())
    {
        if (part.targetID() == id && part.end() == previewPlayback.position())
        {
            switch (part.type())
            {
            case Action_Part_IK :
                return part.get<ActionPartIK>().transform;
            }
        }
    }

    return mat4();
}

void ActionEditor::setTransform(BodyPartID id, const mat4& transform)
{
    // edit for this is called externally
    Entity* entity = context()->controller()->game().get(this->id);

    if (!entity)
    {
        return;
    }

    auto it = entity->actions().find(name);

    if (it == entity->actions().end())
    {
        return;
    }

    Action action = it->second;

    for (auto [ partID, part ] : action.parts())
    {
        if (part.targetID() == id && part.end() == previewPlayback.position())
        {
            switch (part.type())
            {
            case Action_Part_IK :
                part.get<ActionPartIK>().transform = transform;
                break;
            }

            action.add(part);
        }
    }

    entity->addAction(name, action);
}

void ActionEditor::open(const string& name)
{
    this->name = name;
    timeline->open(name);
    previewPlayback.open(name);
}

void ActionEditor::move(const Point& position)
{
    this->position(position);

    column->move(position);

    sidePanel->move(position + Point(size().w - UIScale::sidePanelWidth(), 0));
}

void ActionEditor::resize(const Size& size)
{
    this->size(size);

    column->resize(size);

    sidePanel->resize(Size(UIScale::sidePanelWidth(), size.h));
}

u32 ActionEditor::playPositionSource()
{
    return previewPlayback.position();
}

void ActionEditor::onSeek(u32 position)
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.seek(min(position, currentAction().length() - 1));
}

void ActionEditor::onSelect(const set<ActionPartID>& parts)
{
    activeParts = parts;
}

bool ActionEditor::playingSource()
{
    return previewPlayback.playing();
}

void ActionEditor::onGoToStart()
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.seek(0);
}

void ActionEditor::onStop()
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.pause();
    previewPlayback.seek(0);
}

void ActionEditor::onPlayPause()
{
    if (name.empty())
    {
        return;
    }

    if (previewPlayback.playing())
    {
        previewPlayback.pause();
    }
    else
    {
        previewPlayback.play();
    }
}

void ActionEditor::onGoToEnd()
{
    if (name.empty())
    {
        return;
    }

    previewPlayback.seek(currentAction().length() - 1);
}

void ActionEditor::edit(const function<void(Action&)>& behavior) const
{
    context()->controller()->edit([this, behavior]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        auto it = entity->actions().find(name);

        if (it == entity->actions().end())
        {
            return;
        }

        Action action = it->second;

        behavior(action);

        entity->addAction(name, action);
    });
}

const Action& ActionEditor::currentAction() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const Action empty;
        return empty;
    }

    auto it = entity->actions().find(name);

    if (it == entity->actions().end())
    {
        static const Action empty;
        return empty;
    }

    return it->second;
}

const ActionPart& ActionEditor::currentPart() const
{
    const Action& action = currentAction();

    if (activeParts.size() != 1)
    {
        static const ActionPart empty;
        return empty;
    }

    auto it = action.parts().find(*activeParts.begin());

    if (it == action.parts().end())
    {
        static const ActionPart empty;
        return empty;
    }

    return it->second;
}

void ActionEditor::toggleSidePanel(const Input& input)
{
    sidePanel->toggle();
}

SizeProperties ActionEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
