/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__ANDROID__)
#pragma once

#include "types.hpp"
#include "../window_interface.hpp"

class Controller;

class WindowAndroid final : public WindowInterface
{
public:
    WindowAndroid(Controller* controller);
    ~WindowAndroid() override;

    InputInterface* input() const override;

    void step() override;

    void lockCursor(bool state) override;
    void showCursor(bool state) override;

    void setSize(u32 width, u32 height) override;
    void setFullscreen(bool state) override;
    bool fullscreen() const override;
    bool focused() const override;

    set<string> vulkanExtensions() const override;
    VkSurfaceKHR createVulkanSurface(VkInstance instance) const override;



private:

};
#endif
