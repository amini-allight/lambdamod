/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#include "gamepad_web.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "../global.hpp"

EM_JS(i32, navigatorGetGamepadCount, (), {
    return navigator.getGamepads().length;
});

EM_JS(const char*, navigatorGetGamepadName, (i32 index), {
    return navigator.getGamepads()[index].id;
});

EM_JS(f64, navigatorGetGamepadAxis, (i32 gamepadIndex, i32 axisIndex) {
    return navigator.getGamepads()[gamepadIndex].axes[axisIndex];
});

EM_JS(f64, navigatorGetGamepadButton, (i32 gamepadIndex, i32 buttonIndex) {
    return navigator.getGamepads()[gamepadIndex].buttons[buttonIndex];
});

EM_JS(void, navigatorPulseHaptics, (i32 gamepadIndex, f64 amplitude, f64 duration) {
    for (var i = 0; i < navigator.getGamepads()[gamepadIndex].hapticActuators.length; i++)
    {
        var actuator = navigator.getGamepads()[gamepadIndex].hapticActuators[i];

        actuator.pulse(amplitude, duration);
    }
});

static constexpr i32 dpadUpButtonIndex = 12;
static constexpr i32 dpadDownButtonIndex = 13;
static constexpr i32 dpadLeftButtonIndex = 14;
static constexpr i32 dpadRightButtonIndex = 15;
static constexpr i32 startButtonIndex = 9;
static constexpr i32 guideButtonIndex = 16;
static constexpr i32 backButtonIndex = 8;
static constexpr i32 leftStickClickButtonIndex = 10;
static constexpr i32 rightStickClickButtonIndex = 11;
static constexpr i32 leftShoulderButtonIndex = 4;
static constexpr i32 rightShoulderButtonIndex = 5;
static constexpr i32 aButtonIndex = 0;
static constexpr i32 bButtonIndex = 1;
static constexpr i32 xButtonIndex = 2;
static constexpr i32 yButtonIndex = 3;
static constexpr i32 leftXAxisIndex = 0;
static constexpr i32 leftYAxisIndex = 1;
static constexpr i32 rightXAxisIndex = 2;
static constexpr i32 rightYAxisIndex = 3;
static constexpr i32 leftTriggerButtonIndex = 6;
static constexpr i32 rightTriggerButtonIndex = 7;

GamepadWeb::GamepadWeb(const function<void(const Input&)>& inputCallback)
    : GamepadInterface()
    , inputCallback(inputCallback)
    , index(-1)
    , states{
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0,
        0.0
    }
{
    for (i32 i = 0; i < navigatorGetGamepadCount(); i++)
    {
        if (navigatorGetGamepadName(i) == g_config.gamepadDevice || g_config.gamepadDevice.empty())
        {
            index = i;
            break;
        }
    }

    if (index <= 0)
    {
        log("No gamepad detected.");
    }
    else
    {
        log("Opened gamepad " + to_string(index) + ".");
    }
}

GamepadWeb::~GamepadWeb()
{

}

bool GamepadWeb::hasGamepad() const
{
    return index >= 0;
}

void GamepadWeb::step()
{
    if (!hasGamepad())
    {
        return;
    }

    checkInput(Input_Gamepad_DPad_Up, navigatorGetGamepadButton(index, dpadUpButtonIndex), states.a);
    checkInput(Input_Gamepad_DPad_Down, navigatorGetGamepadButton(index, dpadDownButtonIndex), states.a);
    checkInput(Input_Gamepad_DPad_Left, navigatorGetGamepadButton(index, dpadLeftButtonIndex), states.a);
    checkInput(Input_Gamepad_DPad_Right, navigatorGetGamepadButton(index, dpadRightButtonIndex), states.a);
    checkInput(Input_Gamepad_Start, navigatorGetGamepadButton(index, startButtonIndex), states.a);
    checkInput(Input_Gamepad_Guide, navigatorGetGamepadButton(index, guideButtonIndex), states.a);
    checkInput(Input_Gamepad_Back, navigatorGetGamepadButton(index, backButtonIndex), states.a);
    checkInput(Input_Gamepad_Left_Stick_Click, navigatorGetGamepadButton(index, leftStickClickButtonIndex), states.a);
    checkInput(Input_Gamepad_Right_Stick_Click, navigatorGetGamepadButton(index, rightStickClickButtonIndex), states.a);
    checkInput(Input_Gamepad_Left_Bumper, navigatorGetGamepadButton(index, leftShoulderButtonIndex), states.a);
    checkInput(Input_Gamepad_Right_Bumper, navigatorGetGamepadButton(index, rightShoulderButtonIndex), states.a);
    checkInput(Input_Gamepad_A, navigatorGetGamepadButton(index, aButtonIndex), states.a);
    checkInput(Input_Gamepad_B, navigatorGetGamepadButton(index, bButtonIndex), states.a);
    checkInput(Input_Gamepad_X, navigatorGetGamepadButton(index, xButtonIndex), states.a);
    checkInput(Input_Gamepad_Y, navigatorGetGamepadButton(index, yButtonIndex), states.a);

    f64 leftX = navigatorGetGamepadAxis(index, leftXAxisIndex);
    f64 leftY = navigatorGetGamepadAxis(index, YAxisIndex);

    if (leftX != states.leftStickX || leftY != states.leftStickY)
    {
        Input input;
        input.type = Input_Gamepad_Left_Stick;
        input.position = {
            applyDeadZone(leftX, g_config.gamepadLeftDeadZone.x),
            applyDeadZone(leftY, g_config.gamepadLeftDeadZone.y),
            0
        };
        inputCalback(input);

        states.leftStickX = leftX;
        states.leftStickY = leftY;
    }

    f64 rightX = navigatorGetGamepadAxis(index, rightXAxisIndex);
    f64 rightY = navigatorGetGamepadAxis(index, rightYAxisIndex);

    if (rightX != states.rightStickX || rightY != states.rightStickY)
    {
        Input input;
        input.type = Input_Gamepad_Right_Stick;
        input.position = {
            applyDeadZone(rightX, g_config.gamepadRightDeadZone.x),
            applyDeadZone(rightY, g_config.gamepadRightDeadZone.y),
            0
        };
        inputCalback(input);

        states.rightStickX = rightX;
        states.rightStickY = rightY;
    }

    f64 leftTrigger = navigatorGetGamepadButton(index, leftTriggerButtonIndex);

    if (leftTrigger != states.leftTrigger)
    {
        Input input;
        input.type = Input_Gamepad_Right_Trigger;
        input.intensity = leftTrigger;
        inputCalback(input);

        states.leftTrigger = leftTrigger;
    }

    f64 rightTrigger = navigatorGetGamepadButton(index, rightTriggerButtonIndex);

    if (rightTrigger != states.rightTrigger)
    {
        Input input;
        input.type = Input_Gamepad_Right_Trigger;
        input.intensity = rightTrigger;
        inputCalback(input);

        states.rightTrigger = rightTrigger;
    }
}

void GamepadWeb::haptic(const HapticEffect& effect)
{
    if (!hasGamepad())
    {
        return;
    }

    navigatorPulseHaptics(index, effect.amplitude, effect.duration);
}

void GamepadWeb::checkInput(InputType type, bool current, bool& previous) const
{
    Input input;
    input.type = type;

    if (current && !previous)
    {
        input.up = false;
        inputCallback(input);
        previous = true;
    }
    else if (!current && previous)
    {
        input.up = true;
        inputCallback(input);
        previous = false;
    }
}
#endif
