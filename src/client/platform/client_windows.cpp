/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "client_windows.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "ssl_tools.hpp"
#include "resolved_address.hpp"
#include "network_tools.hpp"

ClientWindows::ClientWindows(
    const string& caFile,
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : ClientNative(
        caFile,
        connectCallback,
        receiveCallback,
        disconnectCallback
    )
{
    WSAData data;
    int result = WSAStartup(MAKEWORD(2, 2), &data);

    if (result != 0)
    {
        fatal("Failed to initialize Windows sockets: " + to_string(result));
    }

    log("Networking initialized.");
}

ClientWindows::~ClientWindows()
{
    log("Shutting down networking...");

    WSACleanup();
}

void ClientWindows::connect(const Address& address)
{
    int result;

    ResolvedAddress connectAddress(address);

    if (!connectAddress.info)
    {
        onDisconnect();
        return;
    }

    SOCKET client = socket(connectAddress.info->ai_family, SOCK_STREAM, 0);

    if (client == INVALID_SOCKET)
    {
        error("Failed to create socket: " + to_string(WSAGetLastError()));
        onDisconnect();
        return;
    }

    log("Connecting to " + address.toString() + ".");

    result = ::connect(client, connectAddress.info->ai_addr, connectAddress.info->ai_addrlen);

    if (result == SOCKET_ERROR)
    {
        error("Failed to connect socket: " + to_string(WSAGetLastError()));
        onDisconnect();
        closesocket(client);
        return;
    }

    result = SSL_set_fd(ssl, client);

    if (result <= 0)
    {
        error("Failed to assign file descriptor for OpenSSL: " + getOpenSSLError());
        onDisconnect();
        closesocket(client);
        return;
    }

    result = SSL_connect(ssl);

    if (result <= 0)
    {
        error("Failed to establish OpenSSL connection: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    result = SSL_do_handshake(ssl);

    if (result <= 0)
    {
        error("Failed to perform OpenSSL handshake with server: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    X509* peerCertificate = SSL_get1_peer_certificate(ssl);

    if (verify && !peerCertificate)
    {
        error("Failed to verify server: no certificate presented.");
        onDisconnect();
        return;
    }

    X509_free(peerCertificate);

    long verifyResult = SSL_get_verify_result(ssl);

    if (verify && verifyResult != X509_V_OK)
    {
        error("Failed to verify server: " + to_string(verifyResult) + ".");
        onDisconnect();
        return;
    }

    result = ioctlSocket(client, FIONBIO, 1);

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    bool flag = true;
    result = setSocketOption(
        client,
        IPPROTO_TCP,
        TCP_NODELAY,
        reinterpret_cast<const char*>(&flag),
        sizeof(bool)
    );

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    connected = true;
    onConnect();
}

void ClientWindows::holePunch(const Address& localAddress, const Address& remoteAddress)
{
    int result;

    ResolvedAddress bindAddress(localAddress);

    if (!bindAddress.info)
    {
        onDisconnect();
        return;
    }

    ResolvedAddress connectAddress(remoteAddress);

    if (!connectAddress.info)
    {
        onDisconnect();
        return;
    }

    SOCKET client = socket(bindAddress.info->ai_family, SOCK_STREAM, 0);

    if (client < 0)
    {
        error("Failed to create socket: " + to_string(WSAGetLastError()));
        onDisconnect();
        return;
    }

    bool flag = true;
    result = setSocketOption(
        client,
        SOL_SOCKET,
        SO_REUSEADDR,
        reinterpret_cast<const char*>(&flag),
        sizeof(bool)
    );

    if (result != 0)
    {
        onDisconnect();
        closesocket(client);
        return;
    }

    result = bind(client, bindAddress.info->ai_addr, bindAddress.info->ai_addrlen);

    if (result == SOCKET_ERROR)
    {
        error("Failed to bind socket: " + to_string(WSAGetLastError()));
        onDisconnect();
        closesocket(client);
        return;
    }

    log("Hole punching to " + remoteAddress.toString() + ".");

    chrono::milliseconds startTime = currentTime();

    while (true)
    {
        result = ::connect(client, connectAddress.info->ai_addr, connectAddress.info->ai_addrlen);

        if (result == SOCKET_ERROR)
        {
            if (currentTime() - startTime > maxHolePunchDuration)
            {
                error("Failed to hole punch: " + to_string(WSAGetLastError()));
                onDisconnect();
                closesocket(client);
                return;
            }

            continue;
        }

        break;
    }

    result = SSL_set_fd(ssl, client);

    if (result <= 0)
    {
        error("Failed to assign file descriptor for OpenSSL: " + getOpenSSLError());
        onDisconnect();
        closesocket(client);
        return;
    }

    result = SSL_connect(ssl);

    if (result <= 0)
    {
        error("Failed to establish OpenSSL connection: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    result = SSL_do_handshake(ssl);

    if (result <= 0)
    {
        error("Failed to perform OpenSSL handshake with server: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    X509* peerCertificate = SSL_get_peer_certificate(ssl);

    if (verify && !peerCertificate)
    {
        error("Failed to verify server: no certificate presented.");
        onDisconnect();
        return;
    }

    X509_free(peerCertificate);

    long verifyResult = SSL_get_verify_result(ssl);

    if (verify && verifyResult != X509_V_OK)
    {
        error("Failed to verify server: " + to_string(verifyResult) + ".");
        onDisconnect();
        return;
    }

    result = ioctlSocket(client, FIONBIO, 1);

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    flag = true;
    result = setSocketOption(
        client,
        IPPROTO_TCP,
        TCP_NODELAY,
        reinterpret_cast<const char*>(&flag),
        sizeof(bool)
    );

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    connected = true;
    onConnect();
}
#endif
