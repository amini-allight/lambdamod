/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#pragma once

#include "types.hpp"
#include "../vr_interface.hpp"

class Controller;

class VRWeb final : public VRInterface
{
public:
    VRWeb(Controller* controller);
    ~VRWeb();

    void requestExit() override;

    bool step() override;

    void setRoom(const vec3& position, const quaternion& rotation) override;
    vec3 roomToWorld(const vec3& v) const override;
    quaternion roomToWorld(const quaternion& q) const override;
    vec3 worldToRoom(const vec3& v) const override;
    quaternion worldToRoom(const quaternion& q) const override;

    bool running() const override;
    bool shouldRender() const override;

    InputInterface* input() const override;

private:
    Controller* controller;
};
#endif
