/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_X11)
#pragma once

#include "types.hpp"
#include "../input_interface.hpp"
#include "gamepad_evdev.hpp"
#include "touch_evdev.hpp"

#include <X11/Xlib.h>
#include <X11/Xatom.h>
#include <X11/XKBlib.h>
#include <X11/Intrinsic.h>

class Controller;
class WindowX11;

class FlatInputX11 final : public InputInterface
{
public:
    FlatInputX11(Controller* controller, WindowX11* window);
    ~FlatInputX11() override;

    bool step() override;

    void haptic(const HapticEffect& effect) override;

    bool hasGamepad() const override;
    bool hasTouch() const override;

    bool handleEvent(const XEvent& event);

private:
    Controller* controller;
    WindowX11* window;
    GamepadEvdev* gamepad;
    TouchEvdev* touch;

    i32 currentX;
    i32 currentY;
    i32 lastX;
    i32 lastY;
    bool leftShift;
    bool leftControl;
    bool leftAlt;
    bool leftSuper;
    bool rightShift;
    bool rightControl;
    bool rightAlt;
    bool rightSuper;
    u32 width;
    u32 height;

    void handleClipboardRequest(const XEvent& event) const;

    void updateModifiers(const XEvent& event, bool up);
    InputType identifyKey(i32 key) const;
    InputType identifyButton(i32 button) const;
};
#endif
