/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_X11)
#include "flat_input_x11.hpp"
#include "../controller.hpp"
#include "window_x11.hpp"
#include "log.hpp"
#include "../global.hpp"
#include "constants.hpp"

FlatInputX11* x11Input = nullptr;

FlatInputX11::FlatInputX11(Controller* controller, WindowX11* window)
    : InputInterface()
    , controller(controller)
    , window(window)
    , gamepad(nullptr)
    , touch(nullptr)
    , currentX(0)
    , currentY(0)
    , lastX(0)
    , lastY(0)
    , leftShift(false)
    , leftControl(false)
    , leftAlt(false)
    , leftSuper(false)
    , rightShift(false)
    , rightControl(false)
    , rightAlt(false)
    , rightSuper(false)
    , width(g_config.width)
    , height(g_config.height)
{
    gamepad = new GamepadEvdev(bind(&Controller::onInput, controller, placeholders::_1));
    touch = new TouchEvdev(bind(&Controller::onInput, controller, placeholders::_1));

    x11Input = this;
}

FlatInputX11::~FlatInputX11()
{
    x11Input = nullptr;

    delete touch;
    delete gamepad;
}

bool FlatInputX11::step()
{
    bool quit = false;

    Display* display = window->display();

    while (XPending(display))
    {
        XEvent event;
        int fail = XNextEvent(display, &event);

        if (fail)
        {
            error("Failed to get X11 event.");
        }

        quit |= handleEvent(event);
    }

    // Done here to prevent large numbers of unnecessary mouse event repeats
    if (currentX != lastX || currentY != lastY)
    {
        Input input;
        input.type = Input_Mouse_Move;
        input.up = false;
        input.intensity = 0;
        input.position = { static_cast<f64>(currentX) / width, static_cast<f64>(currentY) / height, 0 };
        input.rotation = quaternion();
        input.linearMotion = { static_cast<f64>(lastX - currentX) / height * -1, static_cast<f64>(lastY - currentY) / height * -1, 0 };
        input.angularMotion = vec3();
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        controller->onInput(input);

        lastX = currentX;
        lastY = currentY;
    }

    gamepad->step();

    return quit;
}

void FlatInputX11::haptic(const HapticEffect& effect)
{
    gamepad->haptic(effect);
}

bool FlatInputX11::hasGamepad() const
{
    return gamepad->hasGamepad();
}

bool FlatInputX11::hasTouch() const
{
    return touch->hasTouch();
}

bool FlatInputX11::handleEvent(const XEvent& event)
{
    bool quit = false;
    Input input;

    switch (event.type)
    {
    case KeyPress :
    case KeyRelease :
        input.type = identifyKey(event.xkey.keycode);
        input.up = event.type == KeyRelease;
        input.intensity = 0;
        input.position = { static_cast<f64>(event.xkey.x) / width, static_cast<f64>(event.xkey.y) / height, 0 };
        input.rotation = quaternion();
        input.linearMotion = vec3();
        input.angularMotion = vec3();
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        controller->onInput(input);
        updateModifiers(event, event.type == KeyRelease);
        break;
    case ButtonPress :
    case ButtonRelease :
        input.type = identifyButton(event.xbutton.button);
        input.up = event.xbutton.type == ButtonRelease;
        input.intensity = 0;
        input.position = { static_cast<f64>(event.xbutton.x) / width, static_cast<f64>(event.xbutton.y) / height, 0 };
        input.rotation = quaternion();
        input.linearMotion = vec3();
        input.angularMotion = vec3();
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        controller->onInput(input);
        break;
    case MotionNotify :
        currentX = event.xmotion.x;
        currentY = event.xmotion.y;
        break;
    case ConfigureNotify :
        width = event.xconfigure.width;
        height = event.xconfigure.height;
        controller->onResize(event.xconfigure.width, event.xconfigure.height);
        break;
    case DestroyNotify :
        quit = true;
        break;
    case ClientMessage :
        if (static_cast<u32>(event.xclient.data.l[0]) == XInternAtom(window->display(), "WM_DELETE_WINDOW", false))
        {
            quit = true;
        }
        break;
    case FocusIn :
        window->_focused = true;

        controller->onFocus();

        leftShift = false;
        leftControl = false;
        leftAlt = false;
        leftSuper = false;
        rightShift = false;
        rightControl = false;
        rightAlt = false;
        rightSuper = false;
        break;
    case FocusOut :
        window->_focused = false;

        controller->onUnfocus();
        break;
    case SelectionRequest :
        handleClipboardRequest(event);
        break;
    }

    return quit;
}

void FlatInputX11::handleClipboardRequest(const XEvent& event) const
{
    int ok;

    Display* display = window->display();

    Atom clipboardID = XInternAtom(display, "CLIPBOARD", false);
    Atom storageID = XInternAtom(display, event.xselectionrequest.selection == clipboardID ? "LMOD_CLIPBOARD" : "LMOD_PRIMARY", false);
    Atom targetsID = XInternAtom(display, "TARGETS", false);
    Atom stringID = XInternAtom(display, "STRING", false);

    if (event.xselectionrequest.target == targetsID)
    {
        Atom supportedTypes[] = { targetsID, stringID };

        ok = XChangeProperty(
            display,
            event.xselectionrequest.requestor,
            event.xselectionrequest.property,
            XA_ATOM,
            32,
            PropModeReplace,
            reinterpret_cast<const u8*>(supportedTypes),
            4
        );

        if (!ok)
        {
            error("Failed to change X11 property.");
        }
    }
    else
    {
        Atom target;
        i32 format;
        u64 size;
        u64 remaining;
        u8* data;

        int fail = XGetWindowProperty(
            display,
            DefaultRootWindow(display),
            storageID,
            0,
            numeric_limits<i64>::max() / sizeof(u32),
            False,
            stringID,
            &target,
            &format,
            &size,
            &remaining,
            &data
        );

        if (fail)
        {
            error("Failed to get X11 window property.");
        }

        ok = XChangeProperty(
            display,
            event.xselectionrequest.requestor,
            event.xselectionrequest.property,
            event.xselectionrequest.target,
            format,
            PropModeReplace,
            data,
            size
        );

        if (!ok)
        {
            error("Failed to change X11 property.");
        }

        XFree(data);
    }

    XEvent outEvent{};

    outEvent.xany.type = SelectionNotify;
    outEvent.xselection.selection = event.xselectionrequest.selection;
    outEvent.xselection.target = event.xselectionrequest.target;
    outEvent.xselection.property = event.xselectionrequest.property;
    outEvent.xselection.requestor = event.xselectionrequest.requestor;
    outEvent.xselection.time = event.xselectionrequest.time;

    ok = XSendEvent(display, event.xselectionrequest.requestor, True, NoEventMask, &outEvent);

    if (!ok)
    {
        error("Failed to send X11 event.");
    }
}

void FlatInputX11::updateModifiers(const XEvent& event, bool up)
{
    switch (XkbKeycodeToKeysym(window->display(), event.xkey.keycode, 0, 0))
    {
    case XK_Shift_L :
        leftShift = !up;
        break;
    case XK_Shift_R :
        rightShift = !up;
        break;
    case XK_Control_L :
        leftControl = !up;
        break;
    case XK_Control_R :
        rightControl = !up;
        break;
    case XK_Alt_L :
        leftAlt = !up;
        break;
    case XK_Alt_R :
        rightAlt = !up;
        break;
    case XK_Super_L :
        leftSuper = !up;
        break;
    case XK_Super_R :
        rightSuper = !up;
        break;
    }
}

InputType FlatInputX11::identifyKey(i32 key) const
{
    switch (XkbKeycodeToKeysym(window->display(), key, 0, 0))
    {
    case XK_0 : return Input_0;
    case XK_1 : return Input_1;
    case XK_2 : return Input_2;
    case XK_3 : return Input_3;
    case XK_4 : return Input_4;
    case XK_5 : return Input_5;
    case XK_6 : return Input_6;
    case XK_7 : return Input_7;
    case XK_8 : return Input_8;
    case XK_9 : return Input_9;
    case XK_a : return Input_A;
    case XK_b : return Input_B;
    case XK_c : return Input_C;
    case XK_d : return Input_D;
    case XK_e : return Input_E;
    case XK_f : return Input_F;
    case XK_g : return Input_G;
    case XK_h : return Input_H;
    case XK_i : return Input_I;
    case XK_j : return Input_J;
    case XK_k : return Input_K;
    case XK_l : return Input_L;
    case XK_m : return Input_M;
    case XK_n : return Input_N;
    case XK_o : return Input_O;
    case XK_p : return Input_P;
    case XK_q : return Input_Q;
    case XK_r : return Input_R;
    case XK_s : return Input_S;
    case XK_t : return Input_T;
    case XK_u : return Input_U;
    case XK_v : return Input_V;
    case XK_w : return Input_W;
    case XK_x : return Input_X;
    case XK_y : return Input_Y;
    case XK_z : return Input_Z;
    case XK_F1 : return Input_F1;
    case XK_F2 : return Input_F2;
    case XK_F3 : return Input_F3;
    case XK_F4 : return Input_F4;
    case XK_F5 : return Input_F5;
    case XK_F6 : return Input_F6;
    case XK_F7 : return Input_F7;
    case XK_F8 : return Input_F8;
    case XK_F9 : return Input_F9;
    case XK_F10 : return Input_F10;
    case XK_F11 : return Input_F11;
    case XK_F12 : return Input_F12;
    case XK_Escape : return Input_Escape;
    case XK_BackSpace : return Input_Backspace;
    case XK_Tab : return Input_Tab;
    case XK_Caps_Lock : return Input_Caps_Lock;
    case XK_Return : return Input_Return;
    case XK_space : return Input_Space;
    case XK_Menu : return Input_Menu;
    case XK_grave : return Input_Grave_Accent;
    case XK_minus : return Input_Minus;
    case XK_equal : return Input_Equals;
    case XK_bracketleft : return Input_Left_Brace;
    case XK_bracketright : return Input_Right_Brace;
    case XK_backslash : return Input_Backslash;
    case XK_semicolon : return Input_Semicolon;
    case XK_apostrophe : return Input_Quote;
    case XK_period : return Input_Period;
    case XK_comma : return Input_Comma;
    case XK_slash : return Input_Slash;
    case XK_Shift_L : return Input_Left_Shift;
    case XK_Control_L : return Input_Left_Control;
    case XK_Super_L : return Input_Left_Super;
    case XK_Alt_L : return Input_Left_Alt;
    case XK_Shift_R : return Input_Right_Shift;
    case XK_Control_R : return Input_Right_Control;
    case XK_Super_R : return Input_Right_Super;
    case XK_Alt_R : return Input_Right_Alt;
    case XK_Print : return Input_Print_Screen;
    case XK_Insert : return Input_Insert;
    case XK_Delete : return Input_Delete;
    case XK_Scroll_Lock : return Input_Scroll_Lock;
    case XK_Home : return Input_Home;
    case XK_End : return Input_End;
    case XK_Pause : return Input_Pause;
    case XK_Page_Up : return Input_Page_Up;
    case XK_Page_Down : return Input_Page_Down;
    case XK_Left : return Input_Left;
    case XK_Right : return Input_Right;
    case XK_Up : return Input_Up;
    case XK_Down : return Input_Down;
    case XK_Num_Lock : return Input_Num_Lock;
    case XK_KP_Add : return Input_Num_Add;
    case XK_KP_Subtract : return Input_Num_Subtract;
    case XK_KP_Multiply : return Input_Num_Multiply;
    case XK_KP_Divide : return Input_Num_Divide;
    case XK_KP_Insert :
    case XK_KP_0 : return Input_Num_0;
    case XK_KP_End :
    case XK_KP_1 : return Input_Num_1;
    case XK_KP_Down :
    case XK_KP_2 : return Input_Num_2;
    case XK_KP_Page_Down :
    case XK_KP_3 : return Input_Num_3;
    case XK_KP_Left :
    case XK_KP_4 : return Input_Num_4;
    case XK_KP_Begin :
    case XK_KP_5 : return Input_Num_5;
    case XK_KP_Right :
    case XK_KP_6 : return Input_Num_6;
    case XK_KP_Home :
    case XK_KP_7 : return Input_Num_7;
    case XK_KP_Up :
    case XK_KP_8 : return Input_Num_8;
    case XK_KP_Page_Up :
    case XK_KP_9 : return Input_Num_9;
    case XK_KP_Delete :
    case XK_KP_Separator : return Input_Num_Period;
    case XK_KP_Enter : return Input_Num_Return;
    default :
        error("Encountered unknown key " + to_string(key) + " pressed.");
        return Input_Null;
    }
}

InputType FlatInputX11::identifyButton(i32 button) const
{
    switch (button)
    {
    case 1 : return Input_Left_Mouse;
    case 2 : return Input_Middle_Mouse;
    case 3 : return Input_Right_Mouse;
    case 4 : return Input_Mouse_Wheel_Up;
    case 5 : return Input_Mouse_Wheel_Down;
    case 6 : return Input_Mouse_Wheel_Left;
    case 7 : return Input_Mouse_Wheel_Right;
    case 8 : return Input_X1_Mouse;
    case 9 : return Input_X2_Mouse;
    default :
        error("Encountered unknown mouse button " + to_string(button) + " pressed.");
        return Input_Null;
    }
}
#endif
