/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#pragma once

#include "types.hpp"
#include "../gamepad_interface.hpp"

class GamepadWindows final : public GamepadInterface
{
public:
    GamepadWindows(const function<void(const Input&)>& inputCallback);
    ~GamepadWindows() override;

    bool hasGamepad() const override;

    void step() override;

    void haptic(const HapticEffect& effect) override;

private:
    function<void(const Input&)> inputCallback;
    DWORD index;

    struct {
        bool dpadUp;
        bool dpadDown;
        bool dpadLeft;
        bool dpadRight;
        bool start;
        bool back;
        bool leftThumbstick;
        bool rightThumbstick;
        bool leftShoulder;
        bool rightShoulder;
        bool a;
        bool b;
        bool x;
        bool y;
        u8 leftTrigger;
        u8 rightTrigger;
        i16 leftStickX;
        i16 leftStickY;
        i16 rightStickX;
        i16 rightStickY;
    } states;

    f64 hapticAmplitude;
    chrono::milliseconds hapticEndTime;

    void checkInput(InputType type, bool current, bool& previous) const;
};
#endif
