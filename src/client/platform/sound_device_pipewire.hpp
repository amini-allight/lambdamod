/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_PIPEWIRE)
#pragma once

#include "types.hpp"
#include "sound_constants.hpp"
#include "../sound_device_interface.hpp"

#include <pipewire/pipewire.h>
#include <spa/param/audio/format-utils.h>

class SoundDevicePipeWire final : public SoundDeviceInterface
{
public:
    SoundDevicePipeWire();
    ~SoundDevicePipeWire() override;

private:
    pw_thread_loop* loop;
    pw_context* context;
    pw_core* core;
    pw_registry* registry;
    spa_hook registryHook;
    pw_stream* output;
    spa_hook outputHook;
    pw_stream* input;
    spa_hook inputHook;
    map<u32, string> nodes;
    map<u32, u32> ports;
    map<u32, pair<u32, u32>> links;
    bool outputDeviceIdentified;
    bool inputDeviceIdentified;

    bool identifyConnectedDevice(const string& name, const pw_stream* stream) const;

public:
    static void onGlobalAdd(void* userdata, u32 id, u32 permissions, const char* type, u32 version, const spa_dict* props);
    static void onGlobalRemove(void* userdata, u32 id);

    static void onOutputProcess(void* userdata);
    static void onInputProcess(void* userdata);
};

#endif
