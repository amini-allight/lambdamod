/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "../server_runner.hpp"
#include "tools.hpp"
#include "unicode_windows.hpp"

ServerRunner::ServerRunner(
    const string& host,
    u16 port,
    const string& password,
    const string& adminPassword,
    const string& savePath
)
{
    string commandLine;

    commandLine += "./lambdamod-server";
    commandLine += " \"";
    commandLine += host;
    commandLine += "\" ";
    commandLine += to_string(port);
    commandLine += " \"";
    commandLine += password;
    commandLine += "\" \"";
    commandLine += adminPassword;
    commandLine += "\" \"";
    commandLine += savePath;
    commandLine += "\"";

    STARTUPINFOW startupInfo{};
    startupInfo.cb = sizeof(STARTUPINFOW);

    bool ok = CreateProcessW(
        utf8ToUTF16(".\\lambdamod-server.exe").c_str(),
        const_cast<wchar_t*>(utf8ToUTF16(commandLine).c_str()),
        nullptr,
        nullptr,
        false,
        0,
        nullptr,
        nullptr,
        &startupInfo,
        &procInfo
    );

    if (!ok)
    {
        error("Failed to launch server process: " + to_string(GetLastError()));
    }
}

ServerRunner::~ServerRunner()
{
    bool ok = TerminateProcess(procInfo.hProcess, 0);

    if (!ok)
    {
        error("Failed to terminate server process: " + to_string(GetLastError()));
    }

    CloseHandle(procInfo.hThread);
    CloseHandle(procInfo.hProcess);
}

void ServerRunner::step()
{
    DWORD result = WaitForSingleObject(procInfo.hProcess, 0);

    if (result == WAIT_OBJECT_0)
    {
        DWORD status;
        GetExitCodeProcess(procInfo.hProcess, &status);
    }
}
#endif
