/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_PULSEAUDIO)
#pragma once

#include "types.hpp"
#include "sound_constants.hpp"
#include "../sound_device_interface.hpp"

#include <pulse/pulseaudio.h>

class SoundDevicePulseAudio final : public SoundDeviceInterface
{
public:
    SoundDevicePulseAudio();
    ~SoundDevicePulseAudio() override;

private:
    pa_mainloop* mainloop;
    pa_mainloop_api* api;
    pa_context* context;
    pa_stream* output;
    pa_stream* input;

    thread runThread;

    void run();

    static void onContextState(pa_context* context, void* userdata);
    static void onOutputState(pa_stream* stream, void* userdata);
    static void onOutputWrite(pa_stream* stream, size_t length, void* userdata);
    static void onInputState(pa_stream* stream, void* userdata);
    static void onInputRead(pa_stream* stream, size_t length, void* userdata);
};
#endif
