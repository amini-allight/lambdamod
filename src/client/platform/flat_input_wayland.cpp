/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_WAYLAND)
#include "flat_input_wayland.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "../global.hpp"
#include "../controller.hpp"
#include "window_wayland.hpp"

static constexpr i32 wheelDelta = 32 * 120;
static constexpr i32 screenLocalToPixels = 256;

static constexpr wl_pointer_listener pointerListener = {
    .enter = FlatInputWayland::handlePointerEnter,
    .leave = FlatInputWayland::handlePointerLeave,
    .motion = FlatInputWayland::handlePointerMotion,
    .button = FlatInputWayland::handlePointerButton,
    .axis = FlatInputWayland::handlePointerAxis
};

static constexpr wl_keyboard_listener keyboardListener = {
    .keymap = FlatInputWayland::handleKeyboardKeymap,
    .enter = FlatInputWayland::handleKeyboardEnter,
    .leave = FlatInputWayland::handleKeyboardLeave,
    .key = FlatInputWayland::handleKeyboardKey,
    .modifiers = FlatInputWayland::handleKeyboardModifiers,
    .repeat_info = FlatInputWayland::handleKeyboardRepeatInfo
};

static constexpr wl_touch_listener touchListener = {
    .down = FlatInputWayland::handleTouchDown,
    .up = FlatInputWayland::handleTouchUp,
    .motion = FlatInputWayland::handleTouchMotion,
    .frame = FlatInputWayland::handleTouchFrame,
    .cancel = FlatInputWayland::handleTouchCancel,
    .shape = FlatInputWayland::handleTouchShape,
    .orientation = FlatInputWayland::handleTouchOrientation
};

extern const wl_seat_listener seatListener = {
    .capabilities = FlatInputWayland::handleSeatCapabilities
};

extern const xdg_toplevel_listener toplevelListener = {
    .configure = FlatInputWayland::handleToplevelConfigure,
    .close = FlatInputWayland::handleToplevelClose
};

extern const zwp_relative_pointer_v1_listener relativePointerListener = {
    .relative_motion = FlatInputWayland::handleRelativeMotion
};

WaylandInputRepeat::WaylandInputRepeat(const Input& input)
    : input(input)
    , lastTime(currentTime())
    , first(true)
{

}

FlatInputWayland::FlatInputWayland(Controller* controller, WindowWayland* window)
    : InputInterface()
    , controller(controller)
    , window(window)
    , gamepad(nullptr)
    , currentX(0)
    , currentY(0)
    , lastX(0)
    , lastY(0)
    , leftShift(false)
    , leftControl(false)
    , leftAlt(false)
    , leftSuper(false)
    , rightShift(false)
    , rightControl(false)
    , rightAlt(false)
    , rightSuper(false)
    , width(g_config.width)
    , height(g_config.height)
    , shouldResize(false)
    , shouldQuit(false)
    , keyboardInterval(chrono::milliseconds(1000 / 25))
    , keyboardDelay(chrono::milliseconds(600))
{
    gamepad = new GamepadEvdev(bind(&Controller::onInput, controller, placeholders::_1));
}

FlatInputWayland::~FlatInputWayland()
{
    delete gamepad;
}

bool FlatInputWayland::step()
{
    if (window->readyToResize && shouldResize)
    {
        resize(width, height);

        window->readyToResize = false;
        shouldResize = false;
    }

    vector<Input> events;
    events = this->events;
    this->events.clear();

    for (const Input& event : events)
    {
        controller->onInput(event);
    }

    for (auto& [ type, repeat ] : repeats)
    {
        if ((repeat.first && currentTime() - repeat.lastTime >= keyboardDelay) ||
            (!repeat.first && currentTime() - repeat.lastTime >= keyboardInterval)
        )
        {
            repeat.input.position = { static_cast<f64>(lastX) / width, static_cast<f64>(lastY) / height, 0 };
            repeat.input.shift = leftShift || rightShift;
            repeat.input.control = leftControl || rightControl;
            repeat.input.alt = leftAlt || rightAlt;
            repeat.input.super = leftSuper || rightSuper;

            controller->onInput(repeat.input);
            repeat.first = false;
            repeat.lastTime = currentTime();
        }
    }

    shouldQuit |= wl_display_roundtrip(window->display()) == -1;

    if (currentX != lastX || currentY != lastY)
    {
        Input input;
        input.type = Input_Mouse_Move;
        input.up = false;
        input.intensity = 0;
        input.position = { static_cast<f64>(currentX) / width, static_cast<f64>(currentY) / height, 0 };
        input.rotation = quaternion();
        input.linearMotion = vec3(static_cast<f64>(currentX - lastX) / height, static_cast<f64>(currentY - lastY) / height, 0);
        input.angularMotion = vec3();
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        this->events.push_back(input);

        lastX = currentX;
        lastY = currentY;
    }

    gamepad->step();

    return shouldQuit;
}

void FlatInputWayland::haptic(const HapticEffect& effect)
{
    gamepad->haptic(effect);
}

bool FlatInputWayland::hasGamepad() const
{
    return gamepad->hasGamepad();
}

bool FlatInputWayland::hasTouch() const
{
    return window->touch != nullptr;
}

void FlatInputWayland::updateModifiers(u32 key, bool up)
{
    switch (key)
    {
    case KEY_LEFTSHIFT :
        leftShift = !up;
        break;
    case KEY_RIGHTSHIFT :
        rightShift = !up;
        break;
    case KEY_LEFTCTRL :
        leftControl = !up;
        break;
    case KEY_RIGHTCTRL :
        rightControl = !up;
        break;
    case KEY_LEFTALT :
        leftAlt = !up;
        break;
    case KEY_RIGHTALT :
        rightAlt = !up;
        break;
    case KEY_LEFTMETA :
        leftSuper = !up;
        break;
    case KEY_RIGHTMETA :
        rightSuper = !up;
        break;
    }
}

InputType FlatInputWayland::identifyButton(u32 button) const
{
    switch (button)
    {
    default :
        error("Encountered unknown mouse button pressed " + to_string(button) + ".");
        return Input_Null;
    case BTN_LEFT : return Input_Left_Mouse;
    case BTN_RIGHT : return Input_Right_Mouse;
    case BTN_MIDDLE : return Input_Middle_Mouse;
    case BTN_EXTRA : return Input_X1_Mouse;
    case BTN_SIDE : return Input_X2_Mouse;
    }
}

InputType FlatInputWayland::identifyKey(u32 key) const
{
    switch (key)
    {
    default :
        error("Encountered unknown key pressed " + to_string(key) + ".");
        return Input_Null;
    case KEY_0 : return Input_0;
    case KEY_1 : return Input_1;
    case KEY_2 : return Input_2;
    case KEY_3 : return Input_3;
    case KEY_4 : return Input_4;
    case KEY_5 : return Input_5;
    case KEY_6 : return Input_6;
    case KEY_7 : return Input_7;
    case KEY_8 : return Input_8;
    case KEY_9 : return Input_9;
    case KEY_A : return Input_A;
    case KEY_B : return Input_B;
    case KEY_C : return Input_C;
    case KEY_D : return Input_D;
    case KEY_E : return Input_E;
    case KEY_F : return Input_F;
    case KEY_G : return Input_G;
    case KEY_H : return Input_H;
    case KEY_I : return Input_I;
    case KEY_J : return Input_J;
    case KEY_K : return Input_K;
    case KEY_L : return Input_L;
    case KEY_M : return Input_M;
    case KEY_N : return Input_N;
    case KEY_O : return Input_O;
    case KEY_P : return Input_P;
    case KEY_Q : return Input_Q;
    case KEY_R : return Input_R;
    case KEY_S : return Input_S;
    case KEY_T : return Input_T;
    case KEY_U : return Input_U;
    case KEY_V : return Input_V;
    case KEY_W : return Input_W;
    case KEY_X : return Input_X;
    case KEY_Y : return Input_Y;
    case KEY_Z : return Input_Z;
    case KEY_F1 : return Input_F1;
    case KEY_F2 : return Input_F2;
    case KEY_F3 : return Input_F3;
    case KEY_F4 : return Input_F4;
    case KEY_F5 : return Input_F5;
    case KEY_F6 : return Input_F6;
    case KEY_F7 : return Input_F7;
    case KEY_F8 : return Input_F8;
    case KEY_F9 : return Input_F9;
    case KEY_F10 : return Input_F10;
    case KEY_F11 : return Input_F11;
    case KEY_F12 : return Input_F12;
    case KEY_ESC : return Input_Escape;
    case KEY_BACKSPACE : return Input_Backspace;
    case KEY_TAB : return Input_Tab;
    case KEY_CAPSLOCK : return Input_Caps_Lock;
    case KEY_ENTER : return Input_Return;
    case KEY_SPACE : return Input_Space;
    case KEY_MENU : return Input_Menu;
    case KEY_GRAVE : return Input_Grave_Accent;
    case KEY_MINUS : return Input_Minus;
    case KEY_EQUAL : return Input_Equals;
    case KEY_LEFTBRACE : return Input_Left_Brace;
    case KEY_RIGHTBRACE : return Input_Right_Brace;
    case KEY_BACKSLASH : return Input_Backslash;
    case KEY_SEMICOLON : return Input_Semicolon;
    case KEY_APOSTROPHE : return Input_Quote;
    case KEY_DOT : return Input_Period;
    case KEY_COMMA : return Input_Comma;
    case KEY_SLASH : return Input_Slash;
    case KEY_LEFTSHIFT : return Input_Left_Shift;
    case KEY_LEFTCTRL : return Input_Left_Control;
    case KEY_LEFTMETA : return Input_Left_Super;
    case KEY_LEFTALT : return Input_Left_Alt;
    case KEY_RIGHTSHIFT : return Input_Right_Shift;
    case KEY_RIGHTCTRL : return Input_Right_Control;
    case KEY_RIGHTMETA : return Input_Right_Super;
    case KEY_RIGHTALT : return Input_Right_Alt;
    case KEY_PRINT : return Input_Print_Screen;
    case KEY_INSERT : return Input_Insert;
    case KEY_DELETE : return Input_Delete;
    case KEY_SCROLLLOCK : return Input_Scroll_Lock;
    case KEY_HOME : return Input_Home;
    case KEY_END : return Input_End;
    case KEY_PAUSE : return Input_Pause;
    case KEY_PAGEUP : return Input_Page_Up;
    case KEY_PAGEDOWN : return Input_Page_Down;
    case KEY_LEFT : return Input_Left;
    case KEY_RIGHT : return Input_Right;
    case KEY_UP : return Input_Up;
    case KEY_DOWN : return Input_Down;
    case KEY_NUMLOCK : return Input_Num_Lock;
    case KEY_KPPLUS : return Input_Num_Add;
    case KEY_KPMINUS : return Input_Num_Subtract;
    case KEY_KPASTERISK : return Input_Num_Multiply;
    case KEY_KPSLASH : return Input_Num_Divide;
    case KEY_KP0 : return Input_Num_0;
    case KEY_KP1 : return Input_Num_1;
    case KEY_KP2 : return Input_Num_2;
    case KEY_KP3 : return Input_Num_3;
    case KEY_KP4 : return Input_Num_4;
    case KEY_KP5 : return Input_Num_5;
    case KEY_KP6 : return Input_Num_6;
    case KEY_KP7 : return Input_Num_7;
    case KEY_KP8 : return Input_Num_8;
    case KEY_KP9 : return Input_Num_9;
    case KEY_KPDOT : return Input_Num_Period;
    case KEY_KPENTER : return Input_Num_Return;
    }
}

InputType FlatInputWayland::identifyAxis(u32 axis, wl_fixed_t value) const
{
    switch (axis)
    {
    default :
        return Input_Null;
    case WL_POINTER_AXIS_VERTICAL_SCROLL :
        return value > 0 ? Input_Mouse_Wheel_Down : Input_Mouse_Wheel_Up;
    case WL_POINTER_AXIS_HORIZONTAL_SCROLL :
        return value < 0 ? Input_Mouse_Wheel_Left : Input_Mouse_Wheel_Right;
    }
}

Input& FlatInputWayland::ensureTouchEvent(i32 id)
{
    auto it = find_if(
        touchEvents.begin(),
        touchEvents.end(),
        [id](const Input& event) -> bool { return event.touch.id == static_cast<u32>(id); }
    );

    if (it == touchEvents.end())
    {
        Input event;
        event.type = Input_Touch;
        event.touch.id = id;

        touchEvents.push_back(event);

        return touchEvents.back();
    }
    else
    {
        return *it;
    }
}

optional<vec3> FlatInputWayland::getLastTouchPosition(i32 id) const
{
    auto it = lastTouchPositions.find(id);

    if (it == lastTouchPositions.end())
    {
        return {};
    }
    else
    {
        return it->second;
    }
}

void FlatInputWayland::setLastTouchPosition(i32 id, const vec3& position)
{
    lastTouchPositions.insert_or_assign(id, position);
}

void FlatInputWayland::clearLastTouchPosition(i32 id)
{
    lastTouchPositions.erase(id);
}

void FlatInputWayland::resize(u32 width, u32 height)
{
    controller->onResize(width, height);

    window->resize(width, height);
}

void FlatInputWayland::handleSeatCapabilities(
    void* data,
    wl_seat* seat,
    u32 capabilities
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    if (capabilities & WL_SEAT_CAPABILITY_POINTER)
    {
        self->window->pointer = wl_seat_get_pointer(seat);

        wl_pointer_add_listener(self->window->pointer, &pointerListener, data);
    }

    if (capabilities & WL_SEAT_CAPABILITY_KEYBOARD)
    {
        self->window->keyboard = wl_seat_get_keyboard(seat);

        wl_keyboard_add_listener(self->window->keyboard, &keyboardListener, data);
    }

    if (capabilities & WL_SEAT_CAPABILITY_TOUCH)
    {
        self->window->touch = wl_seat_get_touch(seat);

        wl_touch_add_listener(self->window->touch, &touchListener, data);
    }
}

void FlatInputWayland::handleToplevelConfigure(
    void* data,
    xdg_toplevel* toplevel,
    i32 width,
    i32 height,
    wl_array* states
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    if (width == 0 || height == 0 || (static_cast<u32>(width) == self->width && static_cast<u32>(height) == self->height))
    {
        return;
    }

    self->shouldResize = true;
    self->width = width;
    self->height = height;

    self->window->_fullscreen = false;

    auto stateValues = static_cast<xdg_toplevel_state*>(states->data);

    for (size_t i = 0; i < states->size / sizeof(xdg_toplevel_state); i++)
    {
        if (stateValues[i] == XDG_TOPLEVEL_STATE_FULLSCREEN)
        {
            self->window->_fullscreen = true;
            break;
        }
    }
}

void FlatInputWayland::handleToplevelClose(
    void* data,
    xdg_toplevel* toplevel
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    self->shouldQuit = true;
}

void FlatInputWayland::handlePointerEnter(
    void* data,
    wl_pointer* pointer,
    u32 serial,
    wl_surface* surface,
    wl_fixed_t x,
    wl_fixed_t y
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    self->window->_focused = true;

    self->controller->onFocus();

    self->leftShift = false;
    self->leftControl = false;
    self->leftAlt = false;
    self->leftSuper = false;
    self->rightShift = false;
    self->rightControl = false;
    self->rightAlt = false;
    self->rightSuper = false;

    if (self->window->cursorShown)
    {
        wl_pointer_set_cursor(self->window->pointer, serial, self->window->cursor, 0, 0);
    }
    else
    {
        wl_pointer_set_cursor(self->window->pointer, serial, nullptr, 0, 0);
    }
}

void FlatInputWayland::handlePointerLeave(
    void* data,
    wl_pointer* pointer,
    u32 serial,
    wl_surface* surface
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    self->window->_focused = false;

    self->controller->onUnfocus();
}

void FlatInputWayland::handlePointerMotion(
    void* data,
    wl_pointer* pointer,
    u32 time,
    wl_fixed_t x,
    wl_fixed_t y
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    self->currentX = x / screenLocalToPixels;
    self->currentY = y / screenLocalToPixels;
}

void FlatInputWayland::handlePointerButton(
    void* data,
    wl_pointer* pointer,
    u32 serial,
    u32 time,
    u32 button,
    u32 state
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input input;
    input.type = self->identifyButton(button);
    input.up = state == WL_POINTER_BUTTON_STATE_RELEASED;
    input.intensity = 0;
    input.position = { static_cast<f64>(self->lastX) / self->width, static_cast<f64>(self->lastY) / self->height, 0 };
    input.rotation = quaternion();
    input.linearMotion = vec3();
    input.angularMotion = vec3();
    input.shift = self->leftShift || self->rightShift;
    input.control = self->leftControl || self->rightControl;
    input.alt = self->leftAlt || self->rightAlt;
    input.super = self->leftSuper || self->rightSuper;
    input.serial = serial;

    self->events.push_back(input);
}

void FlatInputWayland::handlePointerAxis(
    void* data,
    wl_pointer* pointer,
    u32 time,
    u32 axis,
    wl_fixed_t value
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    for (i32 i = 0; i < abs(value / wheelDelta); i++)
    {
        Input input;
        input.type = self->identifyAxis(axis, value);
        input.up = false;
        input.intensity = 0;
        input.position = { static_cast<f64>(self->lastX) / self->width, static_cast<f64>(self->lastY) / self->height, 0 };
        input.rotation = quaternion();
        input.linearMotion = vec3();
        input.angularMotion = vec3();
        input.shift = self->leftShift || self->rightShift;
        input.control = self->leftControl || self->rightControl;
        input.alt = self->leftAlt || self->rightAlt;
        input.super = self->leftSuper || self->rightSuper;

        self->events.push_back(input);
    }
}

void FlatInputWayland::handleKeyboardKeymap(
    void* data,
    wl_keyboard* keyboard,
    u32 format,
    i32 fd,
    u32 size
)
{
    // Do nothing
}

void FlatInputWayland::handleKeyboardEnter(
    void* data,
    wl_keyboard* keyboard,
    u32 serial,
    wl_surface* surface,
    wl_array* keys
)
{
    // Do nothing
}

void FlatInputWayland::handleKeyboardLeave(
    void* data,
    wl_keyboard* keyboard,
    u32 serial,
    wl_surface* surface
)
{
    // Do nothing
}

void FlatInputWayland::handleKeyboardKey(
    void* data,
    wl_keyboard* keyboard,
    u32 serial,
    u32 time,
    u32 key,
    u32 state
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input input;
    input.type = self->identifyKey(key);
    input.up = state == WL_KEYBOARD_KEY_STATE_RELEASED;
    input.intensity = 0;
    input.position = { static_cast<f64>(self->lastX) / self->width, static_cast<f64>(self->lastY) / self->height, 0 };
    input.rotation = quaternion();
    input.linearMotion = vec3();
    input.angularMotion = vec3();
    input.shift = self->leftShift || self->rightShift;
    input.control = self->leftControl || self->rightControl;
    input.alt = self->leftAlt || self->rightAlt;
    input.super = self->leftSuper || self->rightSuper;
    input.serial = serial;

    self->events.push_back(input);

    self->updateModifiers(key, state == WL_KEYBOARD_KEY_STATE_RELEASED);

    if (input.up)
    {
        self->repeats.erase(input.type);
    }
    else
    {
        self->repeats.insert_or_assign(
            input.type,
            WaylandInputRepeat(input)
        );
    }
}

void FlatInputWayland::handleKeyboardModifiers(
    void* data,
    wl_keyboard* keyboard,
    u32 serial,
    u32 modsDepressed,
    u32 modsLatched,
    u32 modsLocked,
    u32 group
)
{
    // Do nothing
}

void FlatInputWayland::handleKeyboardRepeatInfo(
    void* data,
    wl_keyboard* keyboard,
    i32 rate,
    i32 delay
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    // Supplied in characters per second so we convert to microseconds per character
    self->keyboardInterval = chrono::milliseconds(1000 / rate);
    self->keyboardDelay = chrono::milliseconds(delay);
}

void FlatInputWayland::handleTouchDown(
    void* data,
    wl_touch* touch,
    u32 serial,
    u32 time,
    wl_surface* surface,
    i32 id,
    wl_fixed_t x,
    wl_fixed_t y
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input& input = self->ensureTouchEvent(id);

    input.up = false;
    input.position = { x / static_cast<f64>(self->width), y / static_cast<f64>(self->height), 0 };

    self->setLastTouchPosition(id, input.position);
}

void FlatInputWayland::handleTouchUp(
    void* data,
    wl_touch* touch,
    u32 serial,
    u32 time,
    i32 id
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input& input = self->ensureTouchEvent(id);
    optional<vec3> lastPosition = self->getLastTouchPosition(id);

    input.up = true;
    if (lastPosition)
    {
        input.position = *lastPosition;
    }

    self->clearLastTouchPosition(id);
}

void FlatInputWayland::handleTouchMotion(
    void* data,
    wl_touch* touch,
    u32 time,
    i32 id,
    wl_fixed_t x,
    wl_fixed_t y
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input& input = self->ensureTouchEvent(id);
    optional<vec3> lastPosition = self->getLastTouchPosition(id);

    input.position = { x / static_cast<f64>(self->width), y / static_cast<f64>(self->height), 0 };

    if (lastPosition)
    {
        input.linearMotion = { x - lastPosition->x, y - lastPosition->y, 0 };
    }

    self->setLastTouchPosition(id, input.position);
}

void FlatInputWayland::handleTouchFrame(
    void* data,
    wl_touch* touch
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    for (Input event : self->touchEvents)
    {
        event.shift = self->leftShift || self->rightShift;
        event.control = self->leftControl || self->rightControl;
        event.alt = self->leftAlt || self->rightAlt;
        event.super = self->leftSuper || self->rightSuper;

        self->events.push_back(event);
    }

    self->touchEvents.clear();
}

void FlatInputWayland::handleTouchCancel(
    void* data,
    wl_touch* touch
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    self->touchEvents.clear();
    self->lastTouchPositions.clear();
}

void FlatInputWayland::handleTouchShape(
    void* data,
    wl_touch* touch,
    i32 id,
    wl_fixed_t major,
    wl_fixed_t minor
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input& input = self->ensureTouchEvent(id);

    input.touch.major = major / static_cast<f64>(self->height);
    input.touch.minor = minor / static_cast<f64>(self->height);
}

void FlatInputWayland::handleTouchOrientation(
    void* data,
    wl_touch* touch,
    i32 id,
    wl_fixed_t orientation
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    Input& input = self->ensureTouchEvent(id);

    input.touch.orientation = radians(orientation);
}

void FlatInputWayland::handleRelativeMotion(
    void* data,
    zwp_relative_pointer_v1* zwp_relative_pointer_v1,
    u32 uTimeHi,
    u32 uTimeLo,
    wl_fixed_t dx,
    wl_fixed_t dy,
    wl_fixed_t dxUnaccel,
    wl_fixed_t dyUnaccel
)
{
    auto self = static_cast<FlatInputWayland*>(data);

    if (!self->window->locked())
    {
        return;
    }

    dxUnaccel /= screenLocalToPixels;
    dyUnaccel /= screenLocalToPixels;

    Input input;
    input.type = Input_Mouse_Move;
    input.up = false;
    input.intensity = 0;
    input.position = { self->width / 2.0, self->height / 2.0, 0 };
    input.rotation = quaternion();
    input.linearMotion = vec3(static_cast<f64>(dxUnaccel) / self->height, static_cast<f64>(dyUnaccel) / self->height, 0);
    input.angularMotion = vec3();
    input.shift = self->leftShift || self->rightShift;
    input.control = self->leftControl || self->rightControl;
    input.alt = self->leftAlt || self->rightAlt;
    input.super = self->leftSuper || self->rightSuper;

    self->events.push_back(input);
}
#endif
