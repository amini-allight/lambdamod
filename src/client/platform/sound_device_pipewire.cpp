/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_PIPEWIRE)
#include "sound_device_pipewire.hpp"
#include "constants.hpp"
#include "../global.hpp"
#include "log.hpp"

static constexpr pw_registry_events registryListener = {
    .version = PW_VERSION_REGISTRY_EVENTS,
    .global = SoundDevicePipeWire::onGlobalAdd,
    .global_remove = SoundDevicePipeWire::onGlobalRemove
};

static constexpr pw_stream_events outputStreamListener = {
    .version = PW_VERSION_STREAM_EVENTS,
    .process = SoundDevicePipeWire::onOutputProcess
};

static constexpr pw_stream_events inputStreamListener = {
    .version = PW_VERSION_STREAM_EVENTS,
    .process = SoundDevicePipeWire::onInputProcess
};

SoundDevicePipeWire::SoundDevicePipeWire()
    : SoundDeviceInterface()
    , loop(nullptr)
    , context(nullptr)
    , core(nullptr)
    , registry(nullptr)
    , output(nullptr)
    , input(nullptr)
    , outputDeviceIdentified(false)
    , inputDeviceIdentified(false)
{
    log("Initializing sound devices...");

    int result;

    u8 buffer[1024];
    spa_pod_builder builder = SPA_POD_BUILDER_INIT(buffer, sizeof(buffer));

    pw_init(0, nullptr);

    loop = pw_thread_loop_new(nullptr, nullptr);

    if (!loop)
    {
        fatal("Failed to initialize PipeWire main loop.");
    }

    context = pw_context_new(pw_thread_loop_get_loop(loop), nullptr, 0);

    if (!context)
    {
        fatal("Failed to initialize PipeWire context.");
    }

    core = pw_context_connect(context, nullptr, 0);

    if (!core)
    {
        fatal("Failed to connect to PipeWire server.");
    }

    registry = pw_core_get_registry(core, PW_VERSION_REGISTRY, 0);
    pw_registry_add_listener(registry, &registryHook, &registryListener, this);

    pw_properties* outputProps;

    if (g_config.soundOutputDevice.empty())
    {
        outputProps = pw_properties_new(
            PW_KEY_MEDIA_TYPE, "Audio",
            PW_KEY_MEDIA_CATEGORY, "Playback",
            PW_KEY_MEDIA_ROLE, "Game",
            PW_KEY_APP_NAME, appID,
            PW_KEY_APP_ID, appClass,
            PW_KEY_NODE_NAME, "Playback",
            PW_KEY_NODE_DESCRIPTION, "Playback",
            nullptr
        );
    }
    else
    {
        outputProps = pw_properties_new(
            PW_KEY_MEDIA_TYPE, "Audio",
            PW_KEY_MEDIA_CATEGORY, "Playback",
            PW_KEY_MEDIA_ROLE, "Game",
            PW_KEY_APP_NAME, appID,
            PW_KEY_APP_ID, appClass,
            PW_KEY_NODE_NAME, "Playback",
            PW_KEY_NODE_DESCRIPTION, "Playback",
            PW_KEY_TARGET_OBJECT, g_config.soundOutputDevice.c_str(),
            nullptr
        );
    }

    pw_properties_setf(outputProps, PW_KEY_NODE_LATENCY, "%u/%u", soundChunkSamplesPerChannel, soundFrequency);
    pw_properties_setf(outputProps, PW_KEY_NODE_RATE, "1/%u", soundFrequency);

    if (!outputProps)
    {
        fatal("Failed to create PipeWire output properties.");
    }

    spa_audio_info_raw outputInfo{
        .format = SPA_AUDIO_FORMAT_S16,
        .rate = soundFrequency,
        .channels = soundChannels
    };

    const spa_pod* outputFormat = spa_format_audio_raw_build(
        &builder,
        SPA_PARAM_EnumFormat,
        &outputInfo
    );

    if (!outputFormat)
    {
        fatal("Failed to create PipeWire output format.");
    }

    output = pw_stream_new(core, gameName, outputProps);

    if (!output)
    {
        fatal("Failed to create PipeWire output stream.");
    }

    pw_stream_add_listener(output, &outputHook, &outputStreamListener, this);

    result = pw_stream_connect(
        output,
        PW_DIRECTION_OUTPUT,
        PW_ID_ANY,
        static_cast<pw_stream_flags>(PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_MAP_BUFFERS),
        &outputFormat,
        1
    );

    if (result < 0)
    {
        fatal("Failed to connect PipeWire output stream.");
    }

    pw_properties* inputProps;

    if (g_config.soundInputDevice.empty())
    {
        inputProps = pw_properties_new(
            PW_KEY_MEDIA_TYPE, "Audio",
            PW_KEY_MEDIA_CATEGORY, "Capture",
            PW_KEY_MEDIA_ROLE, "Game",
            PW_KEY_APP_NAME, appID,
            PW_KEY_APP_ID, appClass,
            PW_KEY_NODE_NAME, "Capture",
            PW_KEY_NODE_DESCRIPTION, "Capture",
            nullptr
        );
    }
    else
    {
        inputProps = pw_properties_new(
            PW_KEY_MEDIA_TYPE, "Audio",
            PW_KEY_MEDIA_CATEGORY, "Capture",
            PW_KEY_MEDIA_ROLE, "Game",
            PW_KEY_APP_NAME, appID,
            PW_KEY_APP_ID, appClass,
            PW_KEY_NODE_NAME, "Capture",
            PW_KEY_NODE_DESCRIPTION, "Capture",
            PW_KEY_TARGET_OBJECT, g_config.soundInputDevice.c_str(),
            nullptr
        );
    }

    pw_properties_setf(inputProps, PW_KEY_NODE_LATENCY, "%u/%u", voiceChunkSamplesPerChannel, voiceFrequency);
    pw_properties_setf(inputProps, PW_KEY_NODE_RATE, "1/%u", voiceFrequency);

    if (!inputProps)
    {
        fatal("Failed to create PipeWire input properties.");
    }

    input = pw_stream_new(core, gameName, inputProps);

    if (!input)
    {
        fatal("Failed to create PipeWire input stream.");
    }

    pw_stream_add_listener(input, &inputHook, &inputStreamListener, this);

    spa_audio_info_raw inputInfo{
        .format = SPA_AUDIO_FORMAT_S16,
        .rate = voiceFrequency,
        .channels = voiceChannels
    };

    const spa_pod* inputFormat = spa_format_audio_raw_build(
        &builder,
        SPA_PARAM_EnumFormat,
        &inputInfo
    );

    if (!inputFormat)
    {
        fatal("Failed to create PipeWire input format.");
    }

    result = pw_stream_connect(
        input,
        PW_DIRECTION_INPUT,
        PW_ID_ANY,
        static_cast<pw_stream_flags>(PW_STREAM_FLAG_AUTOCONNECT | PW_STREAM_FLAG_MAP_BUFFERS),
        &inputFormat,
        1
    );

    if (result < 0)
    {
        fatal("Failed to connect PipeWire input stream.");
    }

    pw_thread_loop_start(loop);

    log("Sound devices initialized.");
}

SoundDevicePipeWire::~SoundDevicePipeWire()
{
    log("Shutting down sound devices...");

    pw_thread_loop_stop(loop);

    pw_stream_disconnect(output);
    pw_stream_destroy(output);

    pw_stream_disconnect(input);
    pw_stream_destroy(input);

    pw_core_disconnect(core);

    pw_context_destroy(context);

    pw_thread_loop_destroy(loop);

    pw_deinit();

    log("Sound devices shut down.");
}

bool SoundDevicePipeWire::identifyConnectedDevice(const string& name, const pw_stream* stream) const
{
    optional<u32> otherPortID;

    for (const auto& [ portID, nodeID ] : ports)
    {
        if (nodeID != pw_stream_get_node_id(input))
        {
            continue;
        }

        for (const auto& [ linkID, link ] : links)
        {
            if (link.first == portID)
            {
                otherPortID = link.second;
            }
            else if (link.second == portID)
            {
                otherPortID = link.first;
            }
        }
    }

    if (*otherPortID)
    {
        auto it = ports.find(*otherPortID);

        if (it != ports.end())
        {
            auto it2 = nodes.find(it->second);

            if (it2 != nodes.end())
            {
                log("Using sound " + name + " device '" + it2->second + "'.");
                return true;
            }
        }
    }

    return false;
}

void SoundDevicePipeWire::onGlobalAdd(
    void* userdata,
    u32 id,
    u32 permissions,
    const char* type,
    u32 version,
    const spa_dict* props
)
{
    auto self = static_cast<SoundDevicePipeWire*>(userdata);

    if (strcmp(type, PW_TYPE_INTERFACE_Port) == 0)
    {
        u32 nodeID = stoi(spa_dict_lookup_item(props, PW_KEY_NODE_ID)->value);

        self->ports.insert_or_assign(id, nodeID);
    }
    else if (strcmp(type, PW_TYPE_INTERFACE_Link) == 0)
    {
        u32 srcID = stoi(spa_dict_lookup_item(props, PW_KEY_LINK_INPUT_PORT)->value);
        u32 dstID = stoi(spa_dict_lookup_item(props, PW_KEY_LINK_OUTPUT_PORT)->value);

        self->links.insert_or_assign(id, pair<u32, u32>(srcID, dstID));
    }
    else if (strcmp(type, PW_TYPE_INTERFACE_Node) == 0)
    {
        string name = spa_dict_lookup_item(props, PW_KEY_NODE_NAME)->value;

        self->nodes.insert_or_assign(id, name);
    }

    if (!self->inputDeviceIdentified)
    {
        self->inputDeviceIdentified = self->identifyConnectedDevice("input", self->input);
    }

    if (!self->outputDeviceIdentified)
    {
        self->outputDeviceIdentified = self->identifyConnectedDevice("output", self->output);
    }
}

void SoundDevicePipeWire::onGlobalRemove(void* userdata, u32 id)
{
    auto self = static_cast<SoundDevicePipeWire*>(userdata);

    self->nodes.erase(id);
    self->ports.erase(id);
    self->links.erase(id);
}

void SoundDevicePipeWire::onOutputProcess(void* userdata)
{
    auto self = static_cast<SoundDevicePipeWire*>(userdata);

    pw_buffer* buffer = pw_stream_dequeue_buffer(self->output);

    if (!buffer)
    {
        return;
    }

    i16* data = static_cast<i16*>(buffer->buffer->datas[0].data);
    u32 count = buffer->buffer->datas[0].maxsize / sizeof(i16);

    if (!data)
    {
        return;
    }

    memset(data, 0, buffer->buffer->datas[0].maxsize);

    count = self->outputBuffer.pop(data, count);

    // Overread doesn't seem to matter here

    buffer->buffer->datas[0].chunk->offset = 0;
    buffer->buffer->datas[0].chunk->stride = soundChannels * sizeof(i16);
    buffer->buffer->datas[0].chunk->size = count * sizeof(i16);

    pw_stream_queue_buffer(self->output, buffer);
}

void SoundDevicePipeWire::onInputProcess(void* userdata)
{
    auto self = static_cast<SoundDevicePipeWire*>(userdata);

    pw_buffer* buffer = pw_stream_dequeue_buffer(self->input);

    if (!buffer)
    {
        return;
    }

    i16* data = static_cast<i16*>(buffer->buffer->datas[0].data);
    u32 count = buffer->buffer->datas[0].chunk->size / sizeof(i16);

    if (!data)
    {
        return;
    }

    self->inputBuffer.push(data, count);

    pw_stream_queue_buffer(self->input, buffer);
}
#endif
