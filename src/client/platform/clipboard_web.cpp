/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#include "clipboard_web.hpp"

#include <emscripten/emscripten.h>

EM_ASYNC_JS(const char*, navigatorClipboardReadText, () {
    return await navigator.clipboard.readText();
});

EM_JS(void, navigatorClipboardwriteText, (const char* text) {
    navigator.clipboard.writeText(text);
});

string getClipboard()
{
    return navigatorClipboardReadText();
}

void setClipboard(const Input& input, const string& text)
{
    navigatorClipboardWriteText(text.c_str());
}
#endif
