/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#include "client_web.hpp"

ClientWeb::ClientWeb(
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : Client(
        connectCallback,
        receiveCallback,
        disconnectCallback
    )
    , client(0)
    , connected(false)
{
    log("Initializing networking...");



    log("Networking initialized.");
}

ClientWeb::~ClientWeb()
{
    log("Shutting down networking...");

    if (socket)
    {
        emscripten_websocket_delete(socket);
    }

    log("Networking shut down.");
}

void ClientWeb::step()
{
    bool internalConnected = internal.connected;

    if (internalConnected && !connected)
    {
        onConnect();
    }
    else if (!internalConnected && connected)
    {
        onDisconnect();
    }

    if (!connected)
    {
        return;
    }

#ifdef LMOD_FAKELAG
    for (const string& message : lagFaker.pullReceive())
    {
        receiveCallback(NetworkEvent(message));
    }

    for (const string& message : lagFaker.pullSend())
    {
        if (!connected)
        {
            continue;
        }

        string data = messageBoundary(message);

        EMSCRIPTEN_RESULT result = emscripten_websocket_send_binary(client, data.data(), data.size());

        if (result != EMSCRIPTEN_RESULT_SUCCESS)
        {
            warning("Failed to write to websocket: " + to_string(result));
            onDisconnect();
            connected = false;
            return;
        }
    }
#endif

    string buffer;

    {
        lock_guard<mutex> lock(internal.lock);

        buffer = internal.buffer;
    }

    onReceive(buffer);
}

void ClientWeb::connect(const Address& address)
{
    EmscriptenWebSocketCreateAttributes attributes{};
    attributes.url = "ws://" + address.host + ":" + to_string(address.port);
    attributes.protocols = "binary";
    attributes.createOnMainThread = true;

    client = emscripten_websocket_new(&attributes);

    if (client < 0)
    {
        error("Failed to connect websocket: " + to_string(client));
        onDisconnect();
        return;
    }

    emscripten_websocket_set_onopen_callback(client, nullptr, &ClientWeb::onOpen);
    emscripten_websocket_set_onclose_callback(client, nullptr, &ClientWeb::onOpen);
    emscripten_websocket_set_onmessage_callback(client, nullptr, &ClientWeb::onMessage);
    emscripten_websocket_set_onerror_callback(client, nullptr, &ClientWeb::onError);
}

void ClientWeb::send(const NetworkEvent& event)
{
    if (!connected)
    {
        return;
    }

    string message = event.data();

#ifdef LMOD_FAKELAG
    if (lagFaker.pushSend(message))
    {
        return;
    }
#endif

    string data = messageBoundary(message);

    EMSCRIPTEN_RESULT result = emscripten_websocket_send_binary(client, data.data(), data.size());

    if (result != EMSCRIPTEN_RESULT_SUCCESS)
    {
        warning("Failed to write to websocket: " + to_string(result));
        onDisconnect();
        connected = false;
    }
}

void ClientWeb::onOpen(int type, const EmscriptenWebSocketOpenEvent* event, void* userdata)
{
    auto self = static_cast<ClientWeb*>(userdata);

    internal.connected = true;
}

void ClientWeb::onClose(int type, const EmscriptenWebSocketCloseEvent* event, void* userdata)
{
    auto self = static_cast<ClientWeb*>(userdata);

    internal.connected = false;
}

void ClientWeb::onMessage(int type, const EmscriptenWebSocketMessageEvent* event, void* userdata)
{
    auto self = static_cast<ClientWeb*>(userdata);

    lock_guard<mutex> lock(internal.lock);

    internal.buffer += string(reinterpret_cast<const char*>(event->data), event->numBytes);
}

void ClientWeb::onError(int type, const EmscriptenWebSocketErrorEvent* event, void* userdata)
{
    auto self = static_cast<ClientWeb*>(userdata);

    internal.connected = false;

    warning("WebSocket connection closed due to an error.");
}
#endif
