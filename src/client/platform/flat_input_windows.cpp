/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "flat_input_windows.hpp"
#include "window_windows.hpp"
#include "log.hpp"
#include "../global.hpp"
#include "../controller.hpp"

#include <winuser.h>
#include <windowsx.h>

static constexpr f64 wheelDelta = 120;

FlatInputWindows::FlatInputWindows(Controller* controller, WindowWindows* window)
    : InputInterface()
    , controller(controller)
    , window(window)
    , currentX(0)
    , currentY(0)
    , lastX(0)
    , lastY(0)
    , leftShift(false)
    , leftControl(false)
    , leftAlt(false)
    , leftSuper(false)
    , rightShift(false)
    , rightControl(false)
    , rightAlt(false)
    , rightSuper(false)
    , width(g_config.width)
    , height(g_config.height)
    , quit(false)
{
    if (hasTouch())
    {
        RegisterTouchWindow(window->hwnd, 0);
    }

    gamepad = new GamepadWindows(bind(&Controller::onInput, controller, placeholders::_1));
}

FlatInputWindows::~FlatInputWindows()
{
    delete gamepad;

    if (hasTouch())
    {
        UnregisterTouchWindow(window->hwnd);
    }
}

bool FlatInputWindows::step()
{
    MSG message;

    while (PeekMessageW(&message, window->hwnd, 0, 0, PM_REMOVE))
    {
        TranslateMessage(&message);
        DispatchMessageW(&message);
    }

    // Done here to prevent large numbers of unnecessary mouse event repeats
    if (currentX != lastX || currentY != lastY)
    {
        Input input;
        input.type = Input_Mouse_Move;
        input.up = false;
        input.intensity = 0;
        input.position = { static_cast<f64>(currentX / width), static_cast<f64>(currentY / height), 0 };
        input.rotation = quaternion();
        input.linearMotion = { static_cast<f64>(lastX - currentX) / height * -1, static_cast<f64>(lastY - currentY) / height * -1, 0 };
        input.angularMotion = vec3();
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        controller->onInput(input);

        lastX = currentX;
        lastY = currentY;
    }

    gamepad->step();

    return quit;
}

void FlatInputWindows::haptic(const HapticEffect& effect)
{
    gamepad->haptic(effect);
}

bool FlatInputWindows::hasGamepad() const
{
    return gamepad->hasGamepad();
}

bool FlatInputWindows::hasTouch() const
{
    return GetSystemMetrics(SM_DIGITIZER) & NID_READY;
}

void FlatInputWindows::updateModifiers(InputType type, bool up)
{
    switch (type)
    {
    default :
        break;
    case Input_Left_Shift :
        leftShift = !up;
        break;
    case Input_Left_Control :
        leftControl = !up;
        break;
    case Input_Left_Super :
        leftSuper = !up;
        break;
    case Input_Left_Alt :
        leftAlt = !up;
        break;
    case Input_Right_Shift :
        rightShift = !up;
        break;
    case Input_Right_Control :
        rightControl = !up;
        break;
    case Input_Right_Super :
        rightSuper = !up;
        break;
    case Input_Right_Alt :
        rightAlt = !up;
        break;
    }
}

InputType FlatInputWindows::identifyButton(WPARAM wParam) const
{
    if (wParam & MK_LBUTTON)
    {
        return Input_Left_Mouse;
    }
    else if (wParam & MK_MBUTTON)
    {
        return Input_Middle_Mouse;
    }
    else if (wParam & MK_RBUTTON)
    {
        return Input_Right_Mouse;
    }
    else if (wParam & MK_XBUTTON1)
    {
        return Input_X1_Mouse;
    }
    else if (wParam & MK_XBUTTON2)
    {
        return Input_X2_Mouse;
    }
    else
    {
        error("Encountered unknown mouse button " + to_string(wParam) + " pressed.");
        return Input_Null;
    }
}

InputType FlatInputWindows::identifyKey(WPARAM wParam) const
{
    switch (wParam)
    {
    case 0x30 : return Input_0;
    case 0x31 : return Input_1;
    case 0x32 : return Input_2;
    case 0x33 : return Input_3;
    case 0x34 : return Input_4;
    case 0x35 : return Input_5;
    case 0x36 : return Input_6;
    case 0x37 : return Input_7;
    case 0x38 : return Input_8;
    case 0x39 : return Input_9;
    case 0x41 : return Input_A;
    case 0x42 : return Input_B;
    case 0x43 : return Input_C;
    case 0x44 : return Input_D;
    case 0x45 : return Input_E;
    case 0x46 : return Input_F;
    case 0x47 : return Input_G;
    case 0x48 : return Input_H;
    case 0x49 : return Input_I;
    case 0x4a : return Input_J;
    case 0x4b : return Input_K;
    case 0x4c : return Input_L;
    case 0x4d : return Input_M;
    case 0x4e : return Input_N;
    case 0x4f : return Input_O;
    case 0x50 : return Input_P;
    case 0x51 : return Input_Q;
    case 0x52 : return Input_R;
    case 0x53 : return Input_S;
    case 0x54 : return Input_T;
    case 0x55 : return Input_U;
    case 0x56 : return Input_V;
    case 0x57 : return Input_W;
    case 0x58 : return Input_X;
    case 0x59 : return Input_Y;
    case 0x5a : return Input_Z;
    case VK_F1 : return Input_F1;
    case VK_F2 : return Input_F2;
    case VK_F3 : return Input_F3;
    case VK_F4 : return Input_F4;
    case VK_F5 : return Input_F5;
    case VK_F6 : return Input_F6;
    case VK_F7 : return Input_F7;
    case VK_F8 : return Input_F8;
    case VK_F9 : return Input_F9;
    case VK_F10 : return Input_F10;
    case VK_F11 : return Input_F11;
    case VK_F12 : return Input_F12;
    case VK_ESCAPE : return Input_Escape;
    case VK_BACK : return Input_Backspace;
    case VK_TAB : return Input_Tab;
    case VK_CAPITAL : return Input_Caps_Lock;
    case VK_RETURN : return Input_Return;
    case VK_SPACE : return Input_Space;
    case VK_MENU : return Input_Menu;
    case VK_OEM_MINUS : return Input_Minus;
    case VK_OEM_PLUS : return Input_Equals;
    case VK_OEM_1 : return Input_Semicolon;
    case VK_OEM_2 : return Input_Slash;
    case VK_OEM_3 : return Input_Grave_Accent;
    case VK_OEM_4 : return Input_Left_Brace;
    case VK_OEM_5 : return Input_Backslash;
    case VK_OEM_6 : return Input_Right_Brace;
    case VK_OEM_7 : return Input_Quote;
    case VK_OEM_PERIOD : return Input_Period;
    case VK_OEM_COMMA : return Input_Comma;
    case VK_LSHIFT : return Input_Left_Shift;
    case VK_LCONTROL : return Input_Left_Control;
    case VK_LWIN : return Input_Left_Super;
    case VK_LMENU : return Input_Left_Alt;
    case VK_RSHIFT : return Input_Right_Shift;
    case VK_RCONTROL : return Input_Right_Control;
    case VK_RWIN : return Input_Right_Super;
    case VK_RMENU : return Input_Right_Alt;
    case VK_SNAPSHOT : return Input_Print_Screen;
    case VK_INSERT : return Input_Insert;
    case VK_DELETE : return Input_Delete;
    case VK_SCROLL : return Input_Scroll_Lock;
    case VK_HOME : return Input_Home;
    case VK_END : return Input_End;
    case VK_PAUSE : return Input_Pause;
    case VK_PRIOR : return Input_Page_Up;
    case VK_NEXT : return Input_Page_Down;
    case VK_LEFT : return Input_Left;
    case VK_RIGHT : return Input_Right;
    case VK_UP : return Input_Up;
    case VK_DOWN : return Input_Down;
    case VK_NUMLOCK : return Input_Num_Lock;
    case VK_ADD : return Input_Num_Add;
    case VK_SUBTRACT : return Input_Num_Subtract;
    case VK_MULTIPLY : return Input_Num_Multiply;
    case VK_DIVIDE : return Input_Num_Divide;
    case VK_NUMPAD0 : return Input_Num_0;
    case VK_NUMPAD1 : return Input_Num_1;
    case VK_NUMPAD2 : return Input_Num_2;
    case VK_NUMPAD3 : return Input_Num_3;
    case VK_NUMPAD4 : return Input_Num_4;
    case VK_NUMPAD5 : return Input_Num_5;
    case VK_NUMPAD6 : return Input_Num_6;
    case VK_NUMPAD7 : return Input_Num_7;
    case VK_NUMPAD8 : return Input_Num_8;
    case VK_NUMPAD9 : return Input_Num_9;
    case VK_DECIMAL : return Input_Num_Period;
    default :
        error("Encountered unknown key " + to_string(wParam) + " pressed.");
        return Input_Null;
    }
}

void FlatInputWindows::handle(HWND hwnd, u32 message, WPARAM wParam, LPARAM lParam)
{
    Input input;

    switch (message)
    {
    case WM_SIZE :
        width = LOWORD(lParam);
        height = HIWORD(lParam);
        controller->onResize(width, height);
        break;
    case WM_QUIT :
        quit = true;
        break;
    case WM_KEYUP :
    case WM_KEYDOWN :
        input.type = identifyKey(wParam);
        input.position.x = currentX / static_cast<f64>(width);
        input.position.y = currentY / static_cast<f64>(height);
        input.up = message == WM_KEYUP;
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;
        updateModifiers(input.type, input.up);
        break;
    case WM_LBUTTONUP :
    case WM_LBUTTONDOWN :
    case WM_MBUTTONUP :
    case WM_MBUTTONDOWN :
    case WM_RBUTTONUP :
    case WM_RBUTTONDOWN :
    case WM_XBUTTONUP :
    case WM_XBUTTONDOWN :
        input.type = identifyButton(wParam);
        input.position.x = GET_X_LPARAM(lParam) / static_cast<f64>(width);
        input.position.y = GET_Y_LPARAM(lParam) / static_cast<f64>(height);
        input.up =
            message == WM_LBUTTONUP ||
            message == WM_MBUTTONUP ||
            message == WM_RBUTTONUP ||
            message == WM_XBUTTONUP;
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        controller->onInput(input);
        break;
    case WM_MOUSEMOVE :
        currentX = GET_X_LPARAM(lParam) / static_cast<f64>(width);
        currentY = GET_Y_LPARAM(lParam) / static_cast<f64>(height);
        break;
    case WM_MOUSEWHEEL :
    {
        i32 steps = GET_WHEEL_DELTA_WPARAM(wParam) / wheelDelta;
        input.type = steps < 0 ? Input_Mouse_Wheel_Down : Input_Mouse_Wheel_Up;
        input.position.x = GET_X_LPARAM(lParam) / static_cast<f64>(width);
        input.position.y = GET_Y_LPARAM(lParam) / static_cast<f64>(height);
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        for (i32 i = 0; i < abs(steps); i++)
        {
            controller->onInput(input);
        }
        break;
    }
    case WM_MOUSEHWHEEL :
    {
        i32 steps = GET_WHEEL_DELTA_WPARAM(wParam) / wheelDelta;
        input.type = steps < 0 ? Input_Mouse_Wheel_Left : Input_Mouse_Wheel_Right;
        input.position.x = GET_X_LPARAM(lParam) / static_cast<f64>(width);
        input.position.y = GET_Y_LPARAM(lParam) / static_cast<f64>(height);

        for (i32 i = 0; i < abs(steps); i++)
        {
            controller->onInput(input);
        }
        break;
    }
    case WM_TOUCH :
    {
        u32 touchCount = LOWORD(wParam);

        vector<TOUCHINPUT> touches(touchCount);

        bool ok = GetTouchInputInfo(
            reinterpret_cast<HTOUCHINPUT>(lParam),
            touches.size(),
            touches.data(),
            sizeof(TOUCHINPUT)
        );

        if (!ok)
        {
            error("Failed to get Windows touch input info: " + to_string(GetLastError()));
        }

        map<u32, vec3> newLastTouches;

        for (TOUCHINPUT touch : touches)
        {
            f64 touchWidth = touch.cxContact / static_cast<f64>(width * 100);
            f64 touchHeight = touch.cyContact / static_cast<f64>(width * 100);

            Input input;
            input.type = Input_Touch;
            input.up = false;
            input.position.x = touch.x / static_cast<f64>(width * 100);
            input.position.y = touch.y / static_cast<f64>(height * 100);
            input.touch.id = touch.dwID;
            input.touch.major = touchWidth > touchHeight ? touchWidth : touchHeight;
            input.touch.minor = touchWidth > touchHeight ? touchHeight : touchWidth;

            newLastTouches.insert_or_assign(input.touch.id, input.position);

            controller->onInput(input);
        }

        for (const auto& [ id, position ] : lastTouches)
        {
            if (newLastTouches.contains(id))
            {
                continue;
            }

            Input input;
            input.type = Input_Touch;
            input.up = true;
            input.position = position;
            input.touch.id = id;

            controller->onInput(input);
        }

        lastTouches = newLastTouches;

        CloseTouchInputHandle(reinterpret_cast<HTOUCHINPUT>(lParam));
        break;
    }
    case WM_SETFOCUS :
        window->_focused = true;

        controller->onFocus();

        leftShift = false;
        leftControl = false;
        leftAlt = false;
        leftSuper = false;
        rightShift = false;
        rightControl = false;
        rightAlt = false;
        rightSuper = false;
        break;
    case WM_KILLFOCUS :
        window->_focused = false;

        controller->onUnfocus();
        break;
    default :
        DefWindowProcW(hwnd, message, wParam, lParam);
    }
}
#endif
