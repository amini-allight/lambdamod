/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#include "sound_device_web.hpp"

#include <emscripten/val.h>

static SoundDeviceWeb* webSoundDevice = nullptr;

extern "C" void onUserMedia()
{
    log("Using sound input device 'UserMedia'.");
}

extern "C" void pushAudioInputFrame(const string& data)
{
    webSoundDevice->pushInput(data);
}

EM_JS(void, initAudioOutput, (), {
    audioOutputContext = new AudioContext();

    audioOutputGenerator = new MediaStreamTrackGenerator({
        kind: "audio"
    });

    audioOutputStream = new MediaStream([ audioOutputGenerator ]);

    audioOutputSource = audioOutputContext.createMediaStreamSource(audioOutputStream );

    audioOutputSource.connect(audioOutputContext.destination);
});

EM_JS(void, quitAudioOutput, (), {
    delete audioOutputSource;
    delete audioOutputStream;
    delete audioOutputGenerator;
    delete audioOutputContext;
});

EM_JS(void, initAudioInput, (), {
    navigator.getUserMedia({
        audio: true
    }).then(stream => {
        audioInputStream = stream;

        audioInputProcessor = new MediaStreamTrackProcessor({
            track: stream.getAudioTracks[0]
        });

        audioInputProcessor.readable.getReader().read().then(_pushAudioInputFrame);

        _onUserMedia();
    });
});

EM_JS(void, quitAudioInput, (), {
    delete audioInputProcessor;
    delete audioInputStream;
});

EM_JS(void, pushAudioOutputFrame, (data), {
    audioOutputGenerator.writable.getWriter().write(data);
});

SoundDeviceWeb::SoundDeviceWeb()
{
    log("Initializing sound devices...");

    webSoundDevice = this;

    initAudioOutput();
    initAudioInput();

    log("Using sound output device 'WebAudio'.");

    log("Sound devices initialized.");
}

SoundDeviceWeb::~SoundDeviceWeb()
{
    log("Shutting down sound devices...");

    quitAudioInput();
    quitAudioOutput();

    webSoundDevice = nullptr;

    log("Sound devices shut down.");
}

void SoundDeviceWeb::write(const i16* data, u32 count)
{
    i16* volumeAdjusted = new i16[count];

    for (u32 i = 0; i < count; i++)
    {
        volumeAdjusted[i] = data[i] * _outputVolume * !_outputMuted;
    }

    pushAudioOutputFrame(emscripten::val(emscripted::typed_memory_view(
        count * sizeof(i16),
        static_cast<void*>(volumeAdjusted)
    )));

    delete[] volumeAdjusted;
}

void SoundDeviceWeb::pushInput(const string& data)
{
    webSoundDevice->inputBuffer.push(
        reinterpret_cast<const i16*>(data.data()),
        data.size() / sizeof(i16)
    );
}
#endif
