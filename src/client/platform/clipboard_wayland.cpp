/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_WAYLAND)
#include "../clipboard.hpp"
#include "log.hpp"
#include "../controller.hpp"
#include "window_wayland.hpp"

#include <wayland-client.h>
#include <unistd.h>

extern WindowWayland* waylandWindow;

static optional<const char*> mimeMatch;
static string pasteBuffer;
static optional<const char*> primaryMimeMatch;
static string primaryPasteBuffer;
static string copyBuffer;
static string primaryCopyBuffer;

static constexpr size_t bufferSize = 1024;
static constexpr const char* const mimeTypes[] = {
    "text/plain",
    "TEXT",
    "STRING",
    nullptr
};

static void handleDataOfferOffer(
    void* data,
    wl_data_offer* offer,
    const char* mimeType
)
{
    size_t i = 0;
    while (mimeTypes[i])
    {
        if (string(mimeTypes[i]) == mimeType)
        {
            mimeMatch = mimeType;
            break;
        }

        i++;
    }
}

static void handleDataOfferSourceActions(
    void* data,
    wl_data_offer* offer,
    u32 sourceActions
)
{
    // Do nothing
}

static void handleDataOfferAction(
    void* data,
    wl_data_offer* offer,
    u32 dndAction
)
{
    // Do nothing
}

static constexpr wl_data_offer_listener dataOfferListener = {
    .offer = handleDataOfferOffer,
    .source_actions = handleDataOfferSourceActions,
    .action = handleDataOfferAction
};

static void handleDataDeviceDataOffer(
    void* data,
    wl_data_device* dataDevice,
    wl_data_offer* id
)
{
    wl_data_offer_add_listener(id, &dataOfferListener, nullptr);
}

static void handleDataDeviceEnter(
    void* data,
    wl_data_device* dataDevice,
    u32 serial,
    wl_surface* surface,
    wl_fixed_t x,
    wl_fixed_t y,
    wl_data_offer* id
)
{
    // Do nothing
}

static void handleDataDeviceLeave(
    void* data,
    wl_data_device* dataDevice
)
{
    // Do nothing
}

static void handleDataDeviceMotion(
    void* data,
    wl_data_device* dataDevice,
    u32 time,
    wl_fixed_t x,
    wl_fixed_t y
)
{
    // Do nothing
}

static void handleDataDeviceDrop(
    void* data,
    wl_data_device* dataDevice
)
{
    // Do nothing
}

static void handleDataDeviceSelection(
    void* data,
    wl_data_device* dataDevice,
    wl_data_offer* id
)
{
    if (!mimeMatch)
    {
        return;
    }

    pasteBuffer.clear();

    if (!id)
    {

    }

    int pipefd[2];
    int result = pipe(pipefd);

    if (result)
    {
        error("Failed to create Wayland clipboard pipe: " + to_string(errno));
    }

    wl_data_offer_receive(id, *mimeMatch, pipefd[1]);
    result = close(pipefd[1]);

    if (result)
    {
        error("Failed to close Wayland clipboard pipe: " + to_string(errno));
    }

    wl_display_roundtrip(waylandWindow->display());

    while (true)
    {
        char buffer[bufferSize];

        ssize_t readSize = read(pipefd[0], buffer, bufferSize);

        if (readSize < 0)
        {
            error("Failed to read from Wayland clipboard pipe: " + to_string(errno));
            break;
        }
        else if (readSize == 0)
        {
            break;
        }

        pasteBuffer += string(buffer, readSize);
    }

    result = close(pipefd[0]);

    if (result)
    {
        error("Failed to close Wayland clipboard pipe: " + to_string(errno));
    }

    mimeMatch = {};
}

extern const wl_data_device_listener dataDeviceListener = {
    .data_offer = handleDataDeviceDataOffer,
    .enter = handleDataDeviceEnter,
    .leave = handleDataDeviceLeave,
    .motion = handleDataDeviceMotion,
    .drop = handleDataDeviceDrop,
    .selection = handleDataDeviceSelection
};

static void handleDataSourceTarget(
    void* data,
    wl_data_source* dataSource,
    const char* mimeType
)
{
    // Do nothing, used for multiple MIME types
}

static void handleDataSourceSend(
    void* data,
    wl_data_source* dataSource,
    const char* mimeType,
    i32 fd
)
{
    ssize_t writeSize = write(fd, copyBuffer.data(), copyBuffer.size());

    if (writeSize != static_cast<ssize_t>(copyBuffer.size()))
    {
        error("Failed to write to Wayland clipboard pipe: " + to_string(errno));
    }

    int result = close(fd);

    if (result)
    {
        error("Failed to close Wayland clipboard pipe: " + to_string(errno));
    }
}

static void handleDataSourceCancelled(
    void* data,
    wl_data_source* dataSource
)
{
    copyBuffer.clear();
}

static void handleDataSourceDNDDropPerformed(
    void* data,
    wl_data_source* dataSource
)
{
    // Do nothing
}

static void handleDataSourceDNDFinished(
    void* data,
    wl_data_source* dataSource
)
{
    // Do nothing
}

static void handleDataSourceAction(
    void* data,
    wl_data_source* dataSource,
    u32 dndAction
)
{
    // Do nothing
}

extern const wl_data_source_listener dataSourceListener = {
    .target = handleDataSourceTarget,
    .send = handleDataSourceSend,
    .cancelled = handleDataSourceCancelled,
    .dnd_drop_performed = handleDataSourceDNDDropPerformed,
    .dnd_finished = handleDataSourceDNDFinished,
    .action = handleDataSourceAction
};

static void handlePrimarySelectionSourceSend(
    void* data,
    zwp_primary_selection_source_v1* primarySelectionSource,
    const char* mimeType,
    i32 fd
)
{
    ssize_t writeSize = write(fd, primaryCopyBuffer.data(), primaryCopyBuffer.size());

    if (writeSize != static_cast<ssize_t>(primaryCopyBuffer.size()))
    {
        error("Failed to write to Wayland primary selection clipboard pipe: " + to_string(errno));
    }

    int result = close(fd);

    if (result)
    {
        error("Failed to close Wayland primary selection clipboard pipe: " + to_string(errno));
    }
}

static void handlePrimarySelectionSourceCancelled(
    void* data,
    zwp_primary_selection_source_v1* primarySelectionSource
)
{
    copyBuffer.clear();
}

extern const zwp_primary_selection_source_v1_listener primarySelectionSourceListener = {
    .send = handlePrimarySelectionSourceSend,
    .cancelled = handlePrimarySelectionSourceCancelled
};

static void handlePrimarySelectionOfferOffer(
    void* data,
    zwp_primary_selection_offer_v1* offer,
    const char* mimeType
)
{
    size_t i = 0;
    while (mimeTypes[i])
    {
        if (string(mimeTypes[i]) == mimeType)
        {
            primaryMimeMatch = mimeType;
            break;
        }

        i++;
    }
}

static constexpr zwp_primary_selection_offer_v1_listener primarySelectionOfferListener = {
    .offer = handlePrimarySelectionOfferOffer
};

static void handlePrimarySelectionDeviceDataOffer(
    void* data,
    zwp_primary_selection_device_v1* dataDevice,
    zwp_primary_selection_offer_v1* id
)
{
    zwp_primary_selection_offer_v1_add_listener(id, &primarySelectionOfferListener, nullptr);
}

static void handlePrimarySelectionDeviceSelection(
    void* data,
    zwp_primary_selection_device_v1* dataDevice,
    zwp_primary_selection_offer_v1* id
)
{
    if (!primaryMimeMatch)
    {
        return;
    }

    primaryPasteBuffer.clear();

    if (!id)
    {
        return;
    }

    int pipefd[2];
    int result = pipe(pipefd);

    if (result)
    {
        error("Failed to create Wayland clipboard pipe: " + to_string(errno));
    }

    zwp_primary_selection_offer_v1_receive(id, *primaryMimeMatch, pipefd[1]);
    result = close(pipefd[1]);

    if (result)
    {
        error("Failed to close Wayland clipboard pipe: " + to_string(errno));
    }

    wl_display_roundtrip(waylandWindow->display());

    while (true)
    {
        char buffer[bufferSize];

        ssize_t readSize = read(pipefd[0], buffer, bufferSize);

        if (readSize < 0)
        {
            error("Failed to read from Wayland clipboard pipe: " + to_string(errno));
            break;
        }
        else if (readSize == 0)
        {
            break;
        }

        primaryPasteBuffer += string(buffer, readSize);
    }

    result = close(pipefd[0]);

    if (result)
    {
        error("Failed to close Wayland clipboard pipe: " + to_string(errno));
    }

    primaryMimeMatch = {};
}

extern const zwp_primary_selection_device_v1_listener primarySelectionDeviceListener = {
    .data_offer = handlePrimarySelectionDeviceDataOffer,
    .selection = handlePrimarySelectionDeviceSelection
};

string getClipboard(bool primary)
{
    return primary ? primaryPasteBuffer : pasteBuffer;
}

void setClipboard(const Input& input, const string& text, bool primary)
{
    if (!primary)
    {
        copyBuffer = text;

        size_t i = 0;
        while (mimeTypes[i])
        {
            wl_data_source_offer(waylandWindow->dataSource(), mimeTypes[i]);

            i++;
        }

        wl_data_device_set_selection(waylandWindow->dataDevice(), waylandWindow->dataSource(), input.serial);
    }
    else
    {
        primaryCopyBuffer = text;

        size_t i = 0;
        while (mimeTypes[i])
        {
            zwp_primary_selection_source_v1_offer(waylandWindow->primarySelectionSource(), mimeTypes[i]);

            i++;
        }

        zwp_primary_selection_device_v1_set_selection(waylandWindow->primarySelectionDevice(), waylandWindow->primarySelectionSource(), input.serial);
    }
}
#endif
