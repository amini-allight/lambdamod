/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "unicode_windows.hpp"
#include "log.hpp"

#include <stringapiset.h>

string utf16ToUTF8(const wstring& input)
{
    int size = WideCharToMultiByte(
        CP_UTF8,
        0,
        input.data(),
        input.size(),
        nullptr,
        0,
        nullptr,
        nullptr
    );

    string output(size, 0);

    int result = WideCharToMultiByte(
        CP_UTF8,
        0,
        input.data(),
        input.size(),
        output.data(),
        output.size(),
        nullptr,
        nullptr
    );

    if (result == 0)
    {
        error("Failed to convert UTF-16 to UTF-8: " + to_string(GetLastError()));
    }

    return output;
}

wstring utf8ToUTF16(const string& input)
{
    int size = MultiByteToWideChar(
        CP_UTF8,
        0,
        input.data(),
        input.size(),
        nullptr,
        0
    );

    wstring output(size, 0);

    int result = MultiByteToWideChar(
        CP_UTF8,
        0,
        input.data(),
        input.size(),
        output.data(),
        output.size()
    );

    if (result == 0)
    {
        error("Failed to convert UTF-16 to UTF-8: " + to_string(GetLastError()));
    }

    return output;
}
#endif
