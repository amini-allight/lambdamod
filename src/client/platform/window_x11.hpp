/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_X11)
#pragma once

#include "types.hpp"
#include "../window_interface.hpp"
#include "flat_input_x11.hpp"

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <X11/extensions/Xrender.h>
#include <vulkan/vulkan_xlib.h>

class Controller;

class WindowX11 final : public WindowInterface
{
public:
    WindowX11(Controller* controller);
    ~WindowX11() override;

    InputInterface* input() const override;

    void step() override;

    void lockCursor(bool state) override;
    void showCursor(bool state) override;

    void setSize(u32 width, u32 height) override;
    void setFullscreen(bool state) override;
    bool fullscreen() const override;
    bool focused() const override;

    set<string> vulkanExtensions() const override;
    VkSurfaceKHR createVulkanSurface(VkInstance instance) const override;

    Display* display() const;
    Window window() const;

private:
    friend class FlatInputX11;

    Display* _display;
    i32 screen;
    Visual* visual;
    Window _window;
    Cursor cursor;
    Cursor emptyCursor;
    bool locked;
    bool _focused;
    FlatInputX11* _input;
};

#endif
