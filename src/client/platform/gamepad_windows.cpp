/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "gamepad_windows.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "../global.hpp"

#include <xinput.h>

GamepadWindows::GamepadWindows(const function<void(const Input&)>& inputCallback)
    : GamepadInterface()
    , inputCallback(inputCallback)
    , index(numeric_limits<DWORD>::max())
    , states{
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        false,
        0,
        0,
        0,
        0,
        0,
        0
    }
    , hapticAmplitude(0)
    , hapticEndTime(0)
{
    XINPUT_STATE state;

    for (DWORD i = 0; i < 4; i++)
    {
        DWORD result = XInputGetState(i, &state);

        if (result == ERROR_DEVICE_NOT_CONNECTED)
        {
            continue;
        }
        else if (result != ERROR_SUCCESS)
        {
            error("Failed to query Windows gamepad state: " + to_string(result));
        }

        index = i;
        log("Opened gamepad " + to_string(index) + ".");
        break;
    }

    if (index == numeric_limits<DWORD>::max())
    {
        log("No gamepad detected.");
    }
}

GamepadWindows::~GamepadWindows()
{

}

bool GamepadWindows::hasGamepad() const
{
    return index != numeric_limits<DWORD>::max();
}

void GamepadWindows::step()
{
    if (!hasGamepad())
    {
        return;
    }

    XINPUT_STATE state;

    DWORD result = XInputGetState(index, &state);

    if (result != ERROR_SUCCESS)
    {
        error("Failed to query Windows gamepad state: " + to_string(result));
    }

    checkInput(
        Input_Gamepad_DPad_Up,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP,
        states.dpadUp
    );

    checkInput(
        Input_Gamepad_DPad_Down,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN,
        states.dpadDown
    );

    checkInput(
        Input_Gamepad_DPad_Left,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT,
        states.dpadLeft
    );

    checkInput(
        Input_Gamepad_DPad_Right,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT,
        states.dpadRight
    );

    checkInput(
        Input_Gamepad_Start,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_START,
        states.start
    );

    checkInput(
        Input_Gamepad_Back,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_BACK,
        states.back
    );

    checkInput(
        Input_Gamepad_Left_Stick_Click,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_THUMB,
        states.leftThumbstick
    );

    checkInput(
        Input_Gamepad_Right_Stick_Click,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_THUMB,
        states.rightThumbstick
    );

    checkInput(
        Input_Gamepad_Left_Bumper,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_LEFT_SHOULDER,
        states.leftShoulder
    );

    checkInput(
        Input_Gamepad_Right_Bumper,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_RIGHT_SHOULDER,
        states.rightShoulder
    );

    checkInput(
        Input_Gamepad_A,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_A,
        states.a
    );

    checkInput(
        Input_Gamepad_B,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_B,
        states.b
    );

    checkInput(
        Input_Gamepad_X,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_X,
        states.x
    );

    checkInput(
        Input_Gamepad_Y,
        state.Gamepad.wButtons & XINPUT_GAMEPAD_Y,
        states.y
    );

    if (state.Gamepad.bLeftTrigger != states.leftTrigger)
    {
        Input input;
        input.type = Input_Gamepad_Left_Trigger;
        input.intensity = state.Gamepad.bLeftTrigger / 255.0;
        inputCallback(input);

        states.leftTrigger = state.Gamepad.bLeftTrigger;
    }

    if (state.Gamepad.bRightTrigger != states.rightTrigger)
    {
        Input input;
        input.type = Input_Gamepad_Right_Trigger;
        input.intensity = state.Gamepad.bRightTrigger / 255.0;
        inputCallback(input);

        states.rightTrigger = state.Gamepad.bRightTrigger;
    }

    if (state.Gamepad.sThumbLX != states.leftStickX || state.Gamepad.sThumbLY != states.leftStickY)
    {
        Input input;
        input.type = Input_Gamepad_Left_Stick;
        input.position = {
            applyDeadZone(state.Gamepad.sThumbLX / 32768.0, g_config.gamepadLeftDeadZone.x),
            applyDeadZone(state.Gamepad.sThumbLY / 32768.0, g_config.gamepadLeftDeadZone.y),
            0
        };
        inputCallback(input);

        states.leftStickX = state.Gamepad.sThumbLX;
        states.leftStickY = state.Gamepad.sThumbLY;
    }

    if (state.Gamepad.sThumbRX != states.rightStickX || state.Gamepad.sThumbRY != states.rightStickY)
    {
        Input input;
        input.type = Input_Gamepad_Right_Stick;
        input.position = {
            applyDeadZone(state.Gamepad.sThumbRX / 32768.0, g_config.gamepadRightDeadZone.x),
            applyDeadZone(state.Gamepad.sThumbRY / 32768.0, g_config.gamepadRightDeadZone.y),
            0
        };
        inputCallback(input);

        states.rightStickX = state.Gamepad.sThumbRX;
        states.rightStickY = state.Gamepad.sThumbRY;
    }

    if (currentTime() < hapticEndTime)
    {
        XINPUT_VIBRATION vibration{};
        vibration.wLeftMotorSpeed = 65535 * hapticAmplitude;
        vibration.wRightMotorSpeed = 65535 * hapticAmplitude;

        DWORD result = XInputSetState(index, &vibration);

        if (result != ERROR_SUCCESS)
        {
            error("Failed to set Windows gamepad state: " + to_string(result));
        }
    }
}

void GamepadWindows::haptic(const HapticEffect& effect)
{
    if (!hasGamepad())
    {
        return;
    }

    hapticAmplitude = effect.amplitude;
    hapticEndTime = currentTime() + chrono::milliseconds(effect.duration);
}

void GamepadWindows::checkInput(InputType type, bool current, bool& previous) const
{
    Input input;
    input.type = type;

    if (current && !previous)
    {
        input.up = false;
        inputCallback(input);
        previous = true;
    }
    else if (!current && previous)
    {
        input.up = true;
        inputCallback(input);
        previous = false;
    }
}
#endif
