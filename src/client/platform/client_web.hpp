/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#pragma once

#include "types.hpp"
#include "../client.hpp"

#include <emscripten/websocket.h>

class ClientWeb final : public Client
{
public:
    ClientWeb(
        const function<void()>& connectCallback,
        const function<void(const NetworkEvent&)>& receiveCallback,
        const function<void()>& disconnectCallback
    );
    ~ClientWeb() override;

    void step() override;

    void connect(const Address& address) override;
    void send(const NetworkEvent& event) override;

private:
    EMSCRIPTEN_WEBSOCKET_T client;

    bool connected;

    struct {
        bool connected;
        mutex lock;
        string buffer;
    };

    static void onOpen(int type, const EmscriptenWebSocketOpenEvent* event, void* userdata);
    static void onClose(int type, const EmscriptenWebSocketCloseEvent* event, void* userdata);
    static void onMessage(int type, const EmscriptenWebSocketMessageEvent* event, void* userdata);
    static void onError(int type, const EmscriptenWebSocketErrorEvent* event, void* userdata);
};
#endif
