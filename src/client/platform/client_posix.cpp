/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__unix__) || defined(__APPLE__)
#include "client_posix.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "ssl_tools.hpp"
#include "resolved_address.hpp"
#include "network_tools.hpp"

#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/socket.h>

ClientPOSIX::ClientPOSIX(
    const string& caFile,
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : ClientNative(
        caFile,
        connectCallback,
        receiveCallback,
        disconnectCallback
    )
{
    log("Networking initialized.");
}

ClientPOSIX::~ClientPOSIX()
{
    log("Shutting down networking...");
}

void ClientPOSIX::connect(const Address& address)
{
    int result;

    ResolvedAddress connectAddress(address);

    if (!connectAddress.info)
    {
        onDisconnect();
        return;
    }

    int client = socket(connectAddress.info->ai_family, SOCK_STREAM, 0);

    if (client < 0)
    {
        error("Failed to create socket: " + to_string(errno));
        onDisconnect();
        return;
    }

    log("Connecting to " + address.toString() + ".");

    result = ::connect(client, connectAddress.info->ai_addr, connectAddress.info->ai_addrlen);

    if (result != 0)
    {
        error("Failed to connect socket: " + to_string(errno));
        onDisconnect();
        close(client);
        return;
    }

    result = SSL_set_fd(ssl, client);

    if (result <= 0)
    {
        error("Failed to assign file descriptor for OpenSSL: " + getOpenSSLError());
        onDisconnect();
        close(client);
        return;
    }

    result = SSL_connect(ssl);

    if (result <= 0)
    {
        error("Failed to establish OpenSSL connection: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    result = SSL_do_handshake(ssl);

    if (result <= 0)
    {
        error("Failed to perform OpenSSL handshake with server: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    X509* peerCertificate = SSL_get1_peer_certificate(ssl);

    if (verify && !peerCertificate)
    {
        error("Failed to verify server: no certificate presented.");
        onDisconnect();
        return;
    }

    X509_free(peerCertificate);

    long verifyResult = SSL_get_verify_result(ssl);

    if (verify && verifyResult != X509_V_OK)
    {
        error("Failed to verify server: " + to_string(verifyResult) + ".");
        onDisconnect();
        return;
    }

    result = setFileControl(client, O_NONBLOCK, true);

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    result = setSocketOption(client, IPPROTO_TCP, TCP_NODELAY, 1);

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    connected = true;
    onConnect();
}

void ClientPOSIX::holePunch(const Address& localAddress, const Address& remoteAddress)
{
    int result;

    ResolvedAddress bindAddress(localAddress);

    if (!bindAddress.info)
    {
        onDisconnect();
        return;
    }

    ResolvedAddress connectAddress(remoteAddress);

    if (!connectAddress.info)
    {
        onDisconnect();
        return;
    }

    int client = socket(bindAddress.info->ai_family, SOCK_STREAM, 0);

    if (client < 0)
    {
        error("Failed to create socket: " + to_string(errno));
        onDisconnect();
        return;
    }

    result = setSocketOption(client, SOL_SOCKET, SO_REUSEADDR, 1);

    if (result != 0)
    {
        onDisconnect();
        close(client);
        return;
    }

    result = ::bind(client, bindAddress.info->ai_addr, bindAddress.info->ai_addrlen);

    if (result != 0)
    {
        error("Failed to bind socket: " + to_string(errno));
        onDisconnect();
        close(client);
        return;
    }

    log("Hole punching to " + remoteAddress.toString() + ".");

    chrono::milliseconds startTime = currentTime();

    while (true)
    {
        result = ::connect(client, connectAddress.info->ai_addr, connectAddress.info->ai_addrlen);

        if (result != 0)
        {
            if (currentTime() - startTime > maxHolePunchDuration)
            {
                error("Failed to hole punch: " + to_string(errno));
                onDisconnect();
                close(client);
                return;
            }

            continue;
        }

        break;
    }

    result = SSL_set_fd(ssl, client);

    if (result <= 0)
    {
        error("Failed to assign file descriptor for OpenSSL: " + getOpenSSLError());
        onDisconnect();
        close(client);
        return;
    }

    result = SSL_connect(ssl);

    if (result <= 0)
    {
        error("Failed to establish OpenSSL connection: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    result = SSL_do_handshake(ssl);

    if (result <= 0)
    {
        error("Failed to perform OpenSSL handshake with server: " + getOpenSSLError());
        onDisconnect();
        return;
    }

    X509* peerCertificate = SSL_get_peer_certificate(ssl);

    if (verify && !peerCertificate)
    {
        error("Failed to verify server: no certificate presented.");
        onDisconnect();
        return;
    }

    X509_free(peerCertificate);

    long verifyResult = SSL_get_verify_result(ssl);

    if (verify && verifyResult != X509_V_OK)
    {
        error("Failed to verify server: " + to_string(verifyResult) + ".");
        onDisconnect();
        return;
    }

    result = setFileControl(client, O_NONBLOCK, true);

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    result = setSocketOption(client, IPPROTO_TCP, TCP_NODELAY, 1);

    if (result != 0)
    {
        onDisconnect();
        return;
    }

    connected = true;
    onConnect();
}
#endif
