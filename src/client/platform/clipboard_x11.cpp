/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_X11)
#include "../clipboard.hpp"
#include "../controller.hpp"
#include "window_x11.hpp"
#include "flat_input_x11.hpp"
#include "log.hpp"

extern WindowX11* x11Window;
extern FlatInputX11* x11Input;

string getClipboard(bool primary)
{
    Display* display = x11Window->display();
    Window window = x11Window->window();

    Atom clipboardID = XInternAtom(display, primary ? "PRIMARY" : "CLIPBOARD", false);
    Atom formatID = XInternAtom(display, "STRING", false);
    Atom propertyID = XInternAtom(display, "XSEL_DATA", false);
    Atom incrementID = XInternAtom(display, "INCR", false);

    int ok = XConvertSelection(display, clipboardID, formatID, propertyID, window, CurrentTime);

    if (!ok)
    {
        error("Failed to convert X11 selection.");
    }

    XEvent event;

    while (true)
    {
        int fail = XNextEvent(display, &event);

        if (fail)
        {
            error("Failed to get X11 event.");
        }

        if (event.type == SelectionNotify && event.xselection.selection == clipboardID)
        {
            break;
        }

        x11Input->handleEvent(event);
    }

    u8* data = nullptr;
    i32 format;
    u64 size = 0;
    u64 remaining = 0;

    string s;

    if (event.xselection.property)
    {
        int fail = XGetWindowProperty(
            display,
            window,
            propertyID,
            0,
            numeric_limits<i64>::max() / 4,
            True,
            AnyPropertyType,
            &formatID,
            &format,
            &size,
            &remaining,
            static_cast<u8**>(&data)
        );

        if (fail)
        {
            error("Failed to get X11 window property.");
        }

        if (formatID != incrementID)
        {
            s += string(reinterpret_cast<const char*>(data), size);

            XFree(data);
        }
        else
        {
            do
            {
                while (true)
                {
                    int fail = XNextEvent(display, &event);

                    if (fail)
                    {
                        error("Failed to get X11 event.");
                    }

                    if (
                        event.type == PropertyNotify &&
                        event.xproperty.atom == propertyID &&
                        event.xproperty.state == PropertyNewValue
                    )
                    {
                        break;
                    }

                    x11Input->handleEvent(event);
                }

                fail = XGetWindowProperty(
                    display,
                    window,
                    propertyID,
                    0,
                    numeric_limits<i64>::max() / 4,
                    True,
                    AnyPropertyType,
                    &formatID,
                    &format,
                    &size,
                    &remaining,
                    static_cast<u8**>(&data)
                );

                if (fail)
                {
                    error("Failed to get X11 window property.");
                }

                s += string(reinterpret_cast<const char*>(data), size);

                XFree(data);
            } while (size > 0);
        }
    }

    return s;
}

void setClipboard(const Input& input, const string& text, bool primary)
{
    Display* display = x11Window->display();
    Window window = x11Window->window();

    Atom clipboardID = XInternAtom(display, primary ? "PRIMARY" : "CLIPBOARD", false);
    Atom storageID = XInternAtom(display, primary ? "LMOD_PRIMARY" : "LMOD_CLIPBOARD", false);
    Atom formatID = XInternAtom(display, "STRING", false);

    Window rootWindow = DefaultRootWindow(display);

    int ok = XChangeProperty(
        display,
        rootWindow,
        storageID,
        formatID,
        8,
        PropModeReplace,
        reinterpret_cast<const u8*>(text.c_str()),
        text.size()
    );

    if (!ok)
    {
        error("Failed to change X11 property.");
    }

    ok = XSetSelectionOwner(display, clipboardID, window, CurrentTime);

    if (!ok)
    {
        error("Failed to set X11 selection owner.");
    }
}
#endif
