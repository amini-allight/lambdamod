/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__linux__) || defined(__FreeBSD__)
#include "gamepad_evdev.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "../global.hpp"

#include <sys/stat.h>
#include <fcntl.h>
#if defined(__linux__)
#include <linux/input.h>
#elif defined(__FreeBSD__)
#include <dev/evdev/input.h>
#endif

GamepadEvdev::GamepadEvdev(const function<void(const Input&)>& inputCallback)
    : GamepadInterface()
    , inputCallback(inputCallback)
{
    leftTriggerMax = 0;
    rightTriggerMax = 0;

    file = -1;
    lastDPadX = Input_Null;
    lastDPadY = Input_Null;

    if (!g_config.gamepadDevice.empty())
    {
        string path = "/dev/input/by-id/" + g_config.gamepadDevice;

        file = open(path.c_str(), O_RDWR | O_NONBLOCK);

        if (file < 0)
        {
            error("Failed to open gamepad device: " + to_string(errno));
        }

        log("Opened gamepad '" + path + "'.");
    }
    else
    {
        for (const fs::directory_entry& entry : fs::directory_iterator("/dev/input/"))
        {
            if (!entry.path().stem().string().starts_with("event"))
            {
                continue;
            }

            if (!isGamepad(entry.path().string()))
            {
                continue;
            }

            file = open(entry.path().string().c_str(), O_RDWR | O_NONBLOCK);

            if (file < 0)
            {
                error("Failed to open gamepad device '" + entry.path().string() + "': " + to_string(errno));
                continue;
            }

            log("Opened gamepad '" + entry.path().string() + "'.");
            break;
        }
    }

    if (file < 0)
    {
        log("No gamepad detected.");
        return;
    }

    leftStickMax.x = getMaximum(ABS_X);
    leftStickMax.y = getMaximum(ABS_Y);
    leftTriggerMax = getMaximum(ABS_Z);
    rightStickMax.x = getMaximum(ABS_RX);
    rightStickMax.y = getMaximum(ABS_RY);
    rightTriggerMax = getMaximum(ABS_RZ);
}

GamepadEvdev::~GamepadEvdev()
{
    if (hasGamepad())
    {
        close(file);
    }
}

bool GamepadEvdev::hasGamepad() const
{
    return file >= 0;
}

void GamepadEvdev::step()
{
    if (!hasGamepad())
    {
        return;
    }

    while (true)
    {
        input_event event;

        ssize_t result = read(file, &event, sizeof(input_event));

        if (result != sizeof(input_event) && errno == EAGAIN)
        {
            break;
        }

        if (result != sizeof(input_event))
        {
            error("Failed to read gamepad event: " + to_string(errno));
            break;
        }

        if (event.type == 0)
        {
            break;
        }

        Input input;

        switch (event.code)
        {
        default :
            error("Encountered unknown gamepad input code received: " + to_string(event.code));
            continue;
        case BTN_NORTH :
            input.type = Input_Gamepad_Y;
            input.up = !event.value;
            break;
        case BTN_SOUTH :
            input.type = Input_Gamepad_A;
            input.up = !event.value;
            break;
        case BTN_WEST :
            input.type = Input_Gamepad_X;
            input.up = !event.value;
            break;
        case BTN_EAST :
            input.type = Input_Gamepad_B;
            input.up = !event.value;
            break;
        case BTN_DPAD_UP :
            input.type = Input_Gamepad_DPad_Up;
            input.up = !event.value;
            break;
        case BTN_DPAD_DOWN :
            input.type = Input_Gamepad_DPad_Down;
            input.up = !event.value;
            break;
        case BTN_DPAD_LEFT :
            input.type = Input_Gamepad_DPad_Left;
            input.up = !event.value;
            break;
        case BTN_DPAD_RIGHT :
            input.type = Input_Gamepad_DPad_Right;
            input.up = !event.value;
            break;
        case BTN_THUMBL :
            input.type = Input_Gamepad_Left_Stick_Click;
            input.up = !event.value;
            break;
        case BTN_THUMBR :
            input.type = Input_Gamepad_Right_Stick_Click;
            input.up = !event.value;
            break;
        case BTN_TL :
            input.type = Input_Gamepad_Left_Bumper;
            input.up = !event.value;
            break;
        case BTN_TL2 :
            input.type = Input_Gamepad_Left_Trigger;
            input.up = !event.value;
            break;
        case BTN_TR :
            input.type = Input_Gamepad_Right_Bumper;
            input.up = !event.value;
            break;
        case BTN_TR2 :
            input.type = Input_Gamepad_Right_Trigger;
            input.up = !event.value;
            break;
        case BTN_MODE :
            input.type = Input_Gamepad_Guide;
            input.up = !event.value;
            break;
        case BTN_START :
            input.type = Input_Gamepad_Start;
            input.up = !event.value;
            break;
        case BTN_SELECT :
            input.type = Input_Gamepad_Back;
            input.up = !event.value;
            break;
        case ABS_HAT0X :
            if (event.value > 0)
            {
                input.type = Input_Gamepad_DPad_Right;
                input.up = false;
                lastDPadX = input.type;
            }
            else if (event.value < 0)
            {
                input.type = Input_Gamepad_DPad_Left;
                input.up = false;
                lastDPadX = input.type;
            }
            else if (lastDPadX != Input_Null)
            {
                input.type = lastDPadX;
                input.up = true;
                lastDPadX = Input_Null;
            }
            break;
        case ABS_HAT0Y :
            if (event.value > 0)
            {
                input.type = Input_Gamepad_DPad_Down;
                input.up = false;
                lastDPadY = input.type;
            }
            else if (event.value < 0)
            {
                input.type = Input_Gamepad_DPad_Up;
                input.up = false;
                lastDPadY = input.type;
            }
            else if (lastDPadY != Input_Null)
            {
                input.type = lastDPadY;
                input.up = true;
                lastDPadY = Input_Null;
            }
            break;
        case ABS_X :
            input.type = Input_Gamepad_Left_Stick;
            input.position = {
                applyDeadZone(event.value / leftStickMax.x, g_config.gamepadLeftDeadZone.x),
                lastLeft.y,
                0
            };
            lastLeft.x = input.position.x;
            break;
        case ABS_Y :
            input.type = Input_Gamepad_Left_Stick;
            input.position = {
                lastLeft.x,
                applyDeadZone(event.value / leftStickMax.y, g_config.gamepadLeftDeadZone.y),
                0
            };
            lastLeft.y = input.position.y;
            break;
        case ABS_RX :
            input.type = Input_Gamepad_Right_Stick;
            input.position = {
                applyDeadZone(event.value / rightStickMax.x, g_config.gamepadRightDeadZone.x),
                lastRight.y,
                0
            };
            lastRight.x = input.position.x;
            break;
        case ABS_RY :
            input.type = Input_Gamepad_Right_Stick;
            input.position = {
                lastRight.x,
                applyDeadZone(event.value / rightStickMax.y, g_config.gamepadRightDeadZone.y),
                0
            };
            lastRight.y = input.position.y;
            break;
        case ABS_HAT1X :
            input.type = Input_Gamepad_Left_Bumper;
            input.up = event.value == 0;
            break;
        case ABS_HAT1Y :
            input.type = Input_Gamepad_Right_Bumper;
            input.up = event.value == 0;
            break;
        case ABS_HAT2X :
        case ABS_Z :
            input.type = Input_Gamepad_Left_Trigger;
            input.intensity = event.value / leftTriggerMax;
            break;
        case ABS_HAT2Y :
        case ABS_RZ :
            input.type = Input_Gamepad_Right_Trigger;
            input.intensity = event.value / rightTriggerMax;
            break;
        }

        inputCallback(input);
    }
}

void GamepadEvdev::haptic(const HapticEffect& effect)
{
    if (!hasGamepad())
    {
        return;
    }

    i32 result;

    if (lastEffect)
    {
        result = ioctl(file, EVIOCRMFF, *lastEffect);

        if (result < 0)
        {
            error("Failed to remove haptic effect from gamepad device: " + to_string(errno));
        }

        lastEffect = {};
    }

    ff_effect inputEffect{};
    inputEffect.type = FF_PERIODIC;
    inputEffect.id = -1;
    inputEffect.direction = effect.target == Haptic_Target_Left_Hand ? 0x4000 : 0xc000;
    inputEffect.u.periodic.waveform = FF_SINE;
    inputEffect.u.periodic.magnitude = pow(2, 15) * effect.amplitude;
    inputEffect.u.periodic.period = 1000 / effect.frequency;
    inputEffect.u.periodic.offset = 0;
    inputEffect.u.periodic.phase = 0;
    inputEffect.trigger.button = 0;
    inputEffect.trigger.interval = 0;
    inputEffect.replay.length = effect.duration;
    inputEffect.replay.delay = 0;

    result = ioctl(file, EVIOCSFF, &inputEffect);

    if (result < 0)
    {
        error("Failed to add haptic effect to gamepad device: " + to_string(errno));
    }

    lastEffect = result;

    input_event event;
    event.type = EV_FF;
    event.code = 0;
    event.value = 1;

    {
        ssize_t result = write(file, &event, sizeof(input_event));

        if (result != sizeof(input_event))
        {
            error("Failed to play haptic effect on gamepad device: " + to_string(errno));
        }
    }
}

i32 GamepadEvdev::getMaximum(i32 index) const
{
    input_absinfo parameters;

    i32 result = ioctl(file, EVIOCGABS(index), &parameters);

    if (result < 0)
    {
        error("Failed to read gamepad device limits: " + to_string(errno));
        return 1;
    }

    return parameters.maximum;
}

bool GamepadEvdev::isGamepad(const string& path) const
{
    int file = open(path.c_str(), O_RDWR | O_NONBLOCK);

    if (file < 0)
    {
        warning("Failed to open potential gamepad device '" + path + "': " + to_string(errno));
        return false;
    }

    u64 bits;
    i32 result = ioctl(file, EVIOCGBIT(0, sizeof(bits)), &bits);

    if (result != sizeof(bits))
    {
        error("Failed to evaluate potential gamepad device '" + path + "': " + to_string(errno));
        close(file);
        return false;
    }

    vector<u8> keyBits(KEY_MAX, 0);
    result = ioctl(file, EVIOCGBIT(EV_KEY, KEY_MAX), keyBits.data());

    if (result < 0)
    {
        error("Failed to evaluate potential gamepad device '" + path + "': " + to_string(errno));
        close(file);
        return false;
    }

    if (!(keyBits.data()[BTN_NORTH / 8] & (1 << (BTN_NORTH % 8))))
    {
        close(file);
        return false;
    }

    close(file);
    return true;
}
#endif
