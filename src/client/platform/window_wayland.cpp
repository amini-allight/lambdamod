/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_WAYLAND)
#include "window_wayland.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "../global.hpp"
#include "../render_tools.hpp"
#include "../render_constants.hpp"
#include "../image.hpp"
#include "flat_input_wayland.hpp"

WindowWayland* waylandWindow = nullptr;

static constexpr wl_output_listener outputListener = {
    .geometry = WindowWayland::handleOutputGeometry,
    .mode = WindowWayland::handleOutputMode
};

static constexpr xdg_surface_listener shellSurfaceListener = {
    .configure = WindowWayland::handleSurfaceConfigure
};

extern const xdg_toplevel_listener toplevelListener;
extern const wl_seat_listener seatListener;
extern const zwp_relative_pointer_v1_listener relativePointerListener;
extern const wl_data_device_listener dataDeviceListener;
extern const wl_data_source_listener dataSourceListener;
extern const zwp_primary_selection_device_v1_listener primarySelectionDeviceListener;
extern const zwp_primary_selection_source_v1_listener primarySelectionSourceListener;

static constexpr xdg_wm_base_listener shellListener = {
    .ping = WindowWayland::handleShellPing
};

static constexpr wl_registry_listener registryListener = {
    .global = WindowWayland::handleRegistry,
    .global_remove = WindowWayland::handleLoseRegistry
};

static constexpr zxdg_toplevel_decoration_v1_listener toplevelDecorationListener = {
    .configure = WindowWayland::handleToplevelDecorationConfigure
};

static constexpr u32 cursorFormat = WL_SHM_FORMAT_ARGB8888;
static constexpr size_t cursorPixelSize = sizeof(u32);

WindowWayland::WindowWayland(Controller* controller)
    : WindowInterface()
    , readyToResize(false)
    , _fullscreen(false)
    , _focused(true)
    , _locked(false)
    , screenX(0)
    , screenY(0)
    , screenWidth(0)
    , screenHeight(0)
    , width(0)
    , height(0)
    , cursorShown(true)
    , _display(nullptr)
    , registry(nullptr)
    , compositor(nullptr)
    , output(nullptr)
    , shell(nullptr)
    , shm(nullptr)
    , seat(nullptr)
    , pool(nullptr)
    , surface(nullptr)
    , shellSurface(nullptr)
    , toplevel(nullptr)
    , dataDeviceManager(nullptr)
    , _dataDevice(nullptr)
    , _dataSource(nullptr)
    , primarySelectionDeviceManager(nullptr)
    , _primarySelectionDevice(nullptr)
    , _primarySelectionSource(nullptr)
    , pointer(nullptr)
    , keyboard(nullptr)
    , touch(nullptr)
    , pointerConstraints(nullptr)
    , lockedPointer(nullptr)
    , relativePointerManager(nullptr)
    , relativePointer(nullptr)
    , decorationManager(nullptr)
    , toplevelDecoration(nullptr)
    , cursorBuffer(nullptr)
    , cursor(nullptr)
{
    log("Initializing window...");

    waylandWindow = this;

    _input = new FlatInputWayland(controller, this);

    // Acquire display
    _display = wl_display_connect(nullptr);

    if (!_display)
    {
        fatal("Failed to connect to Wayland server.");
    }

    // Acquire compositor and shell
    registry = wl_display_get_registry(_display);
    wl_registry_add_listener(registry, &registryListener, this);
    wl_display_roundtrip(_display);

    if (!compositor)
    {
        fatal("Failed to find Wayland compositor.");
    }

    if (!shell)
    {
        fatal("Failed to find Wayland shell.");
    }

    if (!shm)
    {
        fatal("Failed to find Wayland shared memory.");
    }

    if (!seat)
    {
        fatal("Failed to find Wayland seat.");
    }

    if (!dataDeviceManager)
    {
        fatal("Failed to find Wayland data device manager.");
    }

    if (!primarySelectionDeviceManager)
    {
        fatal("Failed to find Wayland primary selection device manager.");
    }

    if (!pointerConstraints)
    {
        fatal("Failed to find Wayland pointer constraints manager.");
    }

    if (!relativePointerManager)
    {
        fatal("Failed to find Wayland relative pointer manager.");
    }

    if (!decorationManager)
    {
        fatal("Failed to find Wayland decoration manager.");
    }

    wl_display_roundtrip(_display);

    if (screenWidth == 0 || screenHeight == 0)
    {
        error("Failed to get screen dimensions.");
    }

    width = g_config.fullscreen ? screenWidth : g_config.width;
    height = g_config.fullscreen ? screenHeight : g_config.height;

    createSharedMemory();

    // Surface
    surface = wl_compositor_create_surface(compositor);

    if (!surface)
    {
        fatal("Failed to create Wayland surface.");
    }

    shellSurface = xdg_wm_base_get_xdg_surface(shell, surface);
    xdg_surface_add_listener(shellSurface, &shellSurfaceListener, this);

    if (!shellSurface)
    {
        fatal("Failed to get Wayland shell surface.");
    }

    toplevel = xdg_surface_get_toplevel(shellSurface);
    xdg_toplevel_add_listener(toplevel, &toplevelListener, _input);

    xdg_toplevel_set_min_size(toplevel, minWidth, minHeight);
    xdg_toplevel_set_max_size(toplevel, maxWidth, maxHeight);

    xdg_toplevel_set_title(toplevel, gameName);
    xdg_toplevel_set_app_id(toplevel, appID);

    if (g_config.fullscreen)
    {
        xdg_toplevel_set_fullscreen(toplevel, output);
    }

    wl_surface_commit(surface);

    // Relative pointer (for mouse locking)
    relativePointer = zwp_relative_pointer_manager_v1_get_relative_pointer(relativePointerManager, pointer);

    if (!relativePointer)
    {
        fatal("Failed to get relative pointer.");
    }

    zwp_relative_pointer_v1_add_listener(relativePointer, &relativePointerListener, _input);

    // Data device (for clipboard)
    _dataDevice = wl_data_device_manager_get_data_device(dataDeviceManager, seat);

    if (!_dataDevice)
    {
        fatal("Failed to get Wayland data device.");
    }

    wl_data_device_add_listener(_dataDevice, &dataDeviceListener, nullptr);

    _dataSource = wl_data_device_manager_create_data_source(dataDeviceManager);

    if (!_dataSource)
    {
        fatal("Failed to create Wayland data source.");
    }

    wl_data_source_add_listener(_dataSource, &dataSourceListener, nullptr);

    // Primary selection device (for primary clipboard)
    _primarySelectionDevice = zwp_primary_selection_device_manager_v1_get_device(primarySelectionDeviceManager, seat);

    if (!_primarySelectionDevice)
    {
        fatal("Failed to get Wayland primary selection device.");
    }

    zwp_primary_selection_device_v1_add_listener(_primarySelectionDevice, &primarySelectionDeviceListener, nullptr);

    _primarySelectionSource = zwp_primary_selection_device_manager_v1_create_source(primarySelectionDeviceManager);

    if (!_primarySelectionSource)
    {
        fatal("Failed to create Wayland primary selection source.");
    }

    zwp_primary_selection_source_v1_add_listener(_primarySelectionSource, &primarySelectionSourceListener, nullptr);

    // Toplevel decoration (for window borders)
    toplevelDecoration = zxdg_decoration_manager_v1_get_toplevel_decoration(decorationManager, toplevel);

    if (!toplevelDecoration)
    {
        fatal("Failed to get Wayland toplevel decoration.");
    }

    zxdg_toplevel_decoration_v1_add_listener(toplevelDecoration, &toplevelDecorationListener, this);
    zxdg_toplevel_decoration_v1_set_mode(toplevelDecoration, ZXDG_TOPLEVEL_DECORATION_V1_MODE_SERVER_SIDE);

    // Custom cursor
    createCursor();

    log("Window initialized.");
}

WindowWayland::~WindowWayland()
{
    log("Shutting down window...");

    destroyCursor();
    if (touch)
    {
        wl_touch_destroy(touch);
    }
    if (keyboard)
    {
        wl_keyboard_destroy(keyboard);
    }
    if (pointer)
    {
        wl_pointer_destroy(pointer);
    }
    zxdg_toplevel_decoration_v1_destroy(toplevelDecoration);
    zxdg_decoration_manager_v1_destroy(decorationManager);
    zwp_relative_pointer_v1_destroy(relativePointer);
    zwp_relative_pointer_manager_v1_destroy(relativePointerManager);
    if (lockedPointer)
    {
        zwp_locked_pointer_v1_destroy(lockedPointer);
    }
    zwp_pointer_constraints_v1_destroy(pointerConstraints);
    zwp_primary_selection_source_v1_destroy(_primarySelectionSource);
    zwp_primary_selection_device_v1_destroy(_primarySelectionDevice);
    zwp_primary_selection_device_manager_v1_destroy(primarySelectionDeviceManager);
    wl_data_source_destroy(_dataSource);
    wl_data_device_destroy(_dataDevice);
    wl_data_device_manager_destroy(dataDeviceManager);
    xdg_toplevel_destroy(toplevel);
    xdg_surface_destroy(shellSurface);
    wl_surface_destroy(surface);
    destroySharedMemory();
    wl_seat_destroy(seat);
    wl_shm_destroy(shm);
    xdg_wm_base_destroy(shell);
    wl_output_destroy(output);
    wl_compositor_destroy(compositor);
    wl_registry_destroy(registry);
    wl_display_disconnect(_display);
    delete _input;

    waylandWindow = nullptr;

    log("Window shut down.");
}

InputInterface* WindowWayland::input() const
{
    return _input;
}

void WindowWayland::step()
{
    // Do nothing, events handled in input
}

void WindowWayland::lockCursor(bool state)
{
    _locked = state;

    if (state)
    {
        if (!pointerConstraints)
        {
            return;
        }

        lockedPointer = zwp_pointer_constraints_v1_lock_pointer(
            pointerConstraints,
            surface,
            pointer,
            nullptr,
            ZWP_POINTER_CONSTRAINTS_V1_LIFETIME_ONESHOT
        );

        if (!lockedPointer)
        {
            error("Failed to lock pointer.");
        }
    }
    else
    {
        if (!lockedPointer)
        {
            return;
        }

        zwp_locked_pointer_v1_destroy(lockedPointer);
        lockedPointer = nullptr;
    }
}

void WindowWayland::showCursor(bool state)
{
    cursorShown = state;

    if (state)
    {
        wl_pointer_set_cursor(pointer, 0, cursor, 0, 0);
    }
    else
    {
        wl_pointer_set_cursor(pointer, 0, nullptr, 0, 0);
    }
}

void WindowWayland::setSize(u32 width, u32 height)
{
    // This operation does not appear to be supported under wayland
}

void WindowWayland::setFullscreen(bool state)
{
    if (state)
    {
        xdg_toplevel_set_fullscreen(toplevel, output);
    }
    else
    {
        xdg_toplevel_unset_fullscreen(toplevel);
    }
}

bool WindowWayland::fullscreen() const
{
    return _fullscreen;
}

bool WindowWayland::focused() const
{
    return _focused;
}

set<string> WindowWayland::vulkanExtensions() const
{
    static const set<string> extensionNames = {
        "VK_KHR_surface",
        "VK_KHR_wayland_surface"
    };

    return extensionNames;
}

VkSurfaceKHR WindowWayland::createVulkanSurface(VkInstance instance) const
{
    VkSurfaceKHR surface;

    VkWaylandSurfaceCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_WAYLAND_SURFACE_CREATE_INFO_KHR;
    createInfo.display = _display;
    createInfo.surface = this->surface;

    VkResult result = vkCreateWaylandSurfaceKHR(instance, &createInfo, nullptr, &surface);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan surface: " + vulkanError(result));
    }

    return surface;
}

wl_display* WindowWayland::display() const
{
    return _display;
}

wl_data_device* WindowWayland::dataDevice() const
{
    return _dataDevice;
}

wl_data_source* WindowWayland::dataSource() const
{
    return _dataSource;
}

zwp_primary_selection_device_v1* WindowWayland::primarySelectionDevice() const
{
    return _primarySelectionDevice;
}

zwp_primary_selection_source_v1* WindowWayland::primarySelectionSource() const
{
    return _primarySelectionSource;
}

bool WindowWayland::locked() const
{
    return _locked;
}

void WindowWayland::resize(u32 width, u32 height)
{
    this->width = width;
    this->height = height;

    wl_surface_commit(surface);
}

void WindowWayland::createSharedMemory()
{
    int result;

    shmFile = shm_open("/lmod-cursor", O_RDWR | O_CREAT, S_IRUSR | S_IWUSR);

    if (shmFile < 0)
    {
        fatal("Failed to create shared memory file: " + to_string(errno));
    }

    result = shm_unlink("/lmod-cursor");

    if (result)
    {
        error("Failed to unlink shared memory file: " + to_string(errno));
    }

    result = ftruncate(shmFile, cursorSize());

    if (result)
    {
        fatal("Failed to resize shared memory file: " + to_string(errno));
    }

    fillSharedMemory();

    pool = wl_shm_create_pool(shm, shmFile, cursorSize());

    if (!pool)
    {
        fatal("Failed to create Wayland shared memory pool.");
    }
}

void WindowWayland::destroySharedMemory()
{
    wl_shm_pool_destroy(pool);

    int result = close(shmFile);

    if (result)
    {
        error("Failed to close shared memory file: " + to_string(errno));
    }
}

void WindowWayland::fillSharedMemory()
{
    int result;

    auto memory = static_cast<u8*>(mmap(
        nullptr,
        cursorSize(),
        PROT_READ | PROT_WRITE,
        MAP_SHARED,
        shmFile,
        0
    ));

    if (memory == MAP_FAILED)
    {
        fatal("Failed to map shared memory file: " + to_string(errno));
    }

    memset(memory, 0, cursorSize());

    Image* cursorImage = loadPNG(resourcePath() + "/cursor" + iconExt);

    auto outPixels = reinterpret_cast<u32*>(memory);
    auto inPixels = reinterpret_cast<const u32*>(cursorImage->pixels());

    for (u32 i = 0; i < cursorWidth * cursorHeight; i++)
    {
        u32 b = (inPixels[i] >> 0) & 0xff;
        u32 g = (inPixels[i] >> 8) & 0xff;
        u32 r = (inPixels[i] >> 16) & 0xff;
        u32 a = (inPixels[i] >> 24) & 0xff;

        b *= a / 255.0;
        g *= a / 255.0;
        r *= a / 255.0;

        outPixels[i] = (a << 24) | (r << 16) | (g << 8) | (b << 0);
    }

    delete cursorImage;

    result = munmap(memory, cursorSize());

    if (result)
    {
        error("Failed to unmap shared memory file: " + to_string(errno));
    }
}

void WindowWayland::createCursor()
{
    cursorBuffer = wl_shm_pool_create_buffer(
        pool,
        0,
        cursorWidth,
        cursorHeight,
        cursorWidth * cursorPixelSize,
        cursorFormat
    );

    if (!cursorBuffer)
    {
        fatal("Failed to create cursor buffer.");
        return;
    }

    cursor = wl_compositor_create_surface(compositor);

    if (!cursor)
    {
        fatal("Failed to create cursor surface.");
        return;
    }

    wl_surface_attach(cursor, cursorBuffer, 0, 0);
    wl_surface_commit(cursor);

    wl_pointer_set_cursor(pointer, 0, cursor, 0, 0);
}

void WindowWayland::destroyCursor()
{
    wl_surface_destroy(cursor);
    wl_buffer_destroy(cursorBuffer);
}

size_t WindowWayland::cursorSize() const
{
    return cursorWidth * cursorHeight * cursorPixelSize;
}

void WindowWayland::handleOutputGeometry(
    void* data,
    wl_output* wl_output,
    i32 x,
    i32 y,
    i32 physicalWidth,
    i32 physicalHeight,
    i32 subpixel,
    const char* make,
    const char* model,
    i32 transform
)
{
    auto self = static_cast<WindowWayland*>(data);

    self->screenX = x;
    self->screenY = y;
}

void WindowWayland::handleOutputMode(
    void* data,
    wl_output* wl_output,
    u32 flags,
    i32 width,
    i32 height,
    i32 refresh
)
{
    auto self = static_cast<WindowWayland*>(data);

    self->screenWidth = width;
    self->screenHeight = height;
}

void WindowWayland::handleSurfaceConfigure(
    void* data,
    xdg_surface* surface,
    u32 serial
)
{
    auto self = static_cast<WindowWayland*>(data);

    if (self->_input->shouldResize)
    {
        self->readyToResize = true;
    }

    xdg_surface_ack_configure(surface, serial);
}

void WindowWayland::handleShellPing(
    void* data,
    xdg_wm_base* shell,
    u32 serial
)
{
    xdg_wm_base_pong(shell, serial);
}

void WindowWayland::handleRegistry(
    void* data,
    wl_registry* registry,
    u32 id,
    const char* interface,
    u32 version
)
{
    auto self = static_cast<WindowWayland*>(data);

    if (string(interface) == wl_compositor_interface.name)
    {
        self->compositor = static_cast<wl_compositor*>(wl_registry_bind(
            registry,
            id,
            &wl_compositor_interface,
            1
        ));
    }
    else if (string(interface) == wl_output_interface.name)
    {
        self->output = static_cast<wl_output*>(wl_registry_bind(
            registry,
            id,
            &wl_output_interface,
            1
        ));
        wl_output_add_listener(self->output, &outputListener, data);
    }
    else if (string(interface) == xdg_wm_base_interface.name)
    {
        self->shell = static_cast<xdg_wm_base*>(wl_registry_bind(
            registry,
            id,
            &xdg_wm_base_interface,
            1
        ));
        xdg_wm_base_add_listener(self->shell, &shellListener, data);
    }
    else if (string(interface) == wl_shm_interface.name)
    {
        self->shm = static_cast<wl_shm*>(wl_registry_bind(
            registry,
            id,
            &wl_shm_interface,
            1
        ));
    }
    else if (string(interface) == wl_seat_interface.name)
    {
        self->seat = static_cast<wl_seat*>(wl_registry_bind(
            registry,
            id,
            &wl_seat_interface,
            1
        ));
        wl_seat_add_listener(self->seat, &seatListener, self->_input);
    }
    else if (string(interface) == wl_data_device_manager_interface.name)
    {
        self->dataDeviceManager = static_cast<wl_data_device_manager*>(wl_registry_bind(
            registry,
            id,
            &wl_data_device_manager_interface,
            1
        ));
    }
    else if (string(interface) == zwp_primary_selection_device_manager_v1_interface.name)
    {
        self->primarySelectionDeviceManager = static_cast<zwp_primary_selection_device_manager_v1*>(wl_registry_bind(
            registry,
            id,
            &zwp_primary_selection_device_manager_v1_interface,
            1
        ));
    }
    else if (string(interface) == zwp_pointer_constraints_v1_interface.name)
    {
        self->pointerConstraints = static_cast<zwp_pointer_constraints_v1*>(wl_registry_bind(
            registry,
            id,
            &zwp_pointer_constraints_v1_interface,
            1
        ));
    }
    else if (string(interface) == zwp_relative_pointer_manager_v1_interface.name)
    {
        self->relativePointerManager = static_cast<zwp_relative_pointer_manager_v1*>(wl_registry_bind(
            registry,
            id,
            &zwp_relative_pointer_manager_v1_interface,
            1
        ));
    }
    else if (string(interface) == zxdg_decoration_manager_v1_interface.name)
    {
        self->decorationManager = static_cast<zxdg_decoration_manager_v1*>(wl_registry_bind(
            registry,
            id,
            &zxdg_decoration_manager_v1_interface,
            1
        ));
    }
}

void WindowWayland::handleLoseRegistry(
    void* data,
    wl_registry* registry,
    u32 id
)
{
    // Do nothing
}

void WindowWayland::handleToplevelDecorationConfigure(
    void* data,
    zxdg_toplevel_decoration_v1* decoration,
    u32 mode
)
{
    // Do nothing
}
#endif
