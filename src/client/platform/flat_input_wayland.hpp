/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_WAYLAND)
#pragma once

#include "types.hpp"
#include "../input_interface.hpp"
#include "gamepad_evdev.hpp"

#include <wayland-client.h>
#include <xdg-shell.h>
#include <pointer-constraints.h>
#include <relative-pointer.h>
#if defined(__linux__)
#include <linux/input-event-codes.h>
#elif defined(__FreeBSD__)
#include <dev/evdev/input-event-codes.h>
#endif

struct WaylandInputRepeat
{
    WaylandInputRepeat(const Input& input);

    Input input;
    chrono::milliseconds lastTime;
    bool first;
};

class Controller;
class WindowWayland;

class FlatInputWayland final : public InputInterface
{
public:
    FlatInputWayland(Controller* controller, WindowWayland* window);
    ~FlatInputWayland() override;

    bool step() override;

    void haptic(const HapticEffect& effect) override;

    bool hasGamepad() const override;
    bool hasTouch() const override;

private:
    friend class WindowWayland;

    Controller* controller;
    WindowWayland* window;
    GamepadEvdev* gamepad;

    i32 currentX;
    i32 currentY;
    i32 lastX;
    i32 lastY;
    bool leftShift;
    bool leftControl;
    bool leftAlt;
    bool leftSuper;
    bool rightShift;
    bool rightControl;
    bool rightAlt;
    bool rightSuper;
    u32 width;
    u32 height;
    bool shouldResize;
    bool shouldQuit;
    vector<Input> events;
    vector<Input> touchEvents;
    map<InputType, WaylandInputRepeat> repeats;
    map<i32, vec3> lastTouchPositions;

    chrono::milliseconds keyboardInterval;
    chrono::milliseconds keyboardDelay;

    void updateModifiers(u32 key, bool up);
    InputType identifyButton(u32 button) const;
    InputType identifyKey(u32 key) const;
    InputType identifyAxis(u32 axis, wl_fixed_t value) const;

    Input& ensureTouchEvent(i32 id);

    optional<vec3> getLastTouchPosition(i32 id) const;
    void setLastTouchPosition(i32 id, const vec3& position);
    void clearLastTouchPosition(i32 id);

    void resize(u32 width, u32 height);

public:
    static void handleSeatCapabilities(
        void* data,
        wl_seat* seat,
        u32 capabilities
    );
    static void handleToplevelConfigure(
        void* data,
        xdg_toplevel* toplevel,
        i32 width,
        i32 height,
        wl_array* states
    );
    static void handleToplevelClose(
        void* data,
        xdg_toplevel* toplevel
    );
    static void handlePointerEnter(
        void* data,
        wl_pointer* pointer,
        u32 serial,
        wl_surface* surface,
        wl_fixed_t x,
        wl_fixed_t y
    );
    static void handlePointerLeave(
        void* data,
        wl_pointer* pointer,
        u32 serial,
        wl_surface* surface
    );
    static void handlePointerMotion(
        void* data,
        wl_pointer* pointer,
        u32 time,
        wl_fixed_t x,
        wl_fixed_t y
    );
    static void handlePointerButton(
        void* data,
        wl_pointer* pointer,
        u32 serial,
        u32 time,
        u32 button,
        u32 state
    );
    static void handlePointerAxis(
        void* data,
        wl_pointer* pointer,
        u32 time,
        u32 axis,
        wl_fixed_t value
    );
    static void handleKeyboardKeymap(
        void* data,
        wl_keyboard* keyboard,
        u32 format,
        i32 fd,
        u32 size
    );
    static void handleKeyboardEnter(
        void* data,
        wl_keyboard* keyboard,
        u32 serial,
        wl_surface* surface,
        wl_array* keys
    );
    static void handleKeyboardLeave(
        void* data,
        wl_keyboard* keyboard,
        u32 serial,
        wl_surface* surface
    );
    static void handleKeyboardKey(
        void* data,
        wl_keyboard* keyboard,
        u32 serial,
        u32 time,
        u32 key,
        u32 state
    );
    static void handleKeyboardModifiers(
        void* data,
        wl_keyboard* keyboard,
        u32 serial,
        u32 modsDepressed,
        u32 modsLatched,
        u32 modsLocked,
        u32 group
    );
    static void handleKeyboardRepeatInfo(
        void* data,
        wl_keyboard* keyboard,
        i32 rate,
        i32 delay
    );
    static void handleTouchDown(
        void* data,
        wl_touch* touch,
        u32 serial,
        u32 time,
        wl_surface* surface,
        i32 id,
        wl_fixed_t x,
        wl_fixed_t y
    );
    static void handleTouchUp(
        void* data,
        wl_touch* touch,
        u32 serial,
        u32 time,
        i32 id
    );
    static void handleTouchMotion(
        void* data,
        wl_touch* touch,
        u32 time,
        i32 id,
        wl_fixed_t x,
        wl_fixed_t y
    );
    static void handleTouchFrame(
        void* data,
        wl_touch* touch
    );
    static void handleTouchCancel(
        void* data,
        wl_touch* touch
    );
    static void handleTouchShape(
        void* data,
        wl_touch* touch,
        i32 id,
        wl_fixed_t major,
        wl_fixed_t minor
    );
    static void handleTouchOrientation(
        void* data,
        wl_touch* touch,
        i32 id,
        wl_fixed_t orientation
    );
    static void handleRelativeMotion(
        void* data,
        zwp_relative_pointer_v1* zwp_relative_pointer_v1,
        u32 uTimeHi,
        u32 uTimeLo,
        wl_fixed_t dx,
        wl_fixed_t dy,
        wl_fixed_t dxUnaccel,
        wl_fixed_t dyUnaccel
    );
};
#endif
