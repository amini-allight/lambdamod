/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__linux__) && defined(LMOD_WAYLAND)
#pragma once

#include "types.hpp"
#include "../window_interface.hpp"
#include "flat_input_wayland.hpp"

#include <wayland-client.h>
#include <xdg-shell.h>
#include <pointer-constraints.h>
#include <relative-pointer.h>
#include <xdg-decoration.h>
#include <primary-selection.h>
#include <vulkan/vulkan_wayland.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

class Controller;

class WindowWayland final : public WindowInterface
{
public:
    WindowWayland(Controller* controller);
    ~WindowWayland() override;

    InputInterface* input() const override;

    void step() override;

    void lockCursor(bool state) override;
    void showCursor(bool state) override;

    void setSize(u32 width, u32 height) override;
    void setFullscreen(bool state) override;
    bool fullscreen() const override;
    bool focused() const override;

    set<string> vulkanExtensions() const override;
    VkSurfaceKHR createVulkanSurface(VkInstance instance) const override;

    wl_display* display() const;
    wl_data_device* dataDevice() const;
    wl_data_source* dataSource() const;
    zwp_primary_selection_device_v1* primarySelectionDevice() const;
    zwp_primary_selection_source_v1* primarySelectionSource() const;

    bool locked() const;
    void resize(u32 width, u32 height);

private:
    friend class FlatInputWayland;

    bool readyToResize;

    bool _fullscreen;
    bool _focused;
    bool _locked;
    i32 screenX;
    i32 screenY;
    u32 screenWidth;
    u32 screenHeight;
    u32 width;
    u32 height;
    bool cursorShown;

    wl_display* _display;
    wl_registry* registry;
    wl_compositor* compositor;
    wl_output* output;
    xdg_wm_base* shell;
    wl_shm* shm;
    wl_seat* seat;
    int shmFile;
    wl_shm_pool* pool;
    wl_surface* surface;
    xdg_surface* shellSurface;
    xdg_toplevel* toplevel;
    wl_data_device_manager* dataDeviceManager;
    wl_data_device* _dataDevice;
    wl_data_source* _dataSource;
    zwp_primary_selection_device_manager_v1* primarySelectionDeviceManager;
    zwp_primary_selection_device_v1* _primarySelectionDevice;
    zwp_primary_selection_source_v1* _primarySelectionSource;

    wl_pointer* pointer;
    wl_keyboard* keyboard;
    wl_touch* touch;

    zwp_pointer_constraints_v1* pointerConstraints;
    zwp_locked_pointer_v1* lockedPointer;

    zwp_relative_pointer_manager_v1* relativePointerManager;
    zwp_relative_pointer_v1* relativePointer;

    zxdg_decoration_manager_v1* decorationManager;
    zxdg_toplevel_decoration_v1* toplevelDecoration;

    wl_buffer* cursorBuffer;
    wl_surface* cursor;

    FlatInputWayland* _input;

    void createSharedMemory();
    void destroySharedMemory();
    void fillSharedMemory();
    void createCursor();
    void destroyCursor();

    size_t cursorSize() const;

public:
    static void handleOutputGeometry(
        void* data,
        wl_output* wl_output,
        i32 x,
        i32 y,
        i32 physicalWidth,
        i32 physicalHeight,
        i32 subpixel,
        const char* make,
        const char* model,
        i32 transform
    );
    static void handleOutputMode(
        void* data,
        wl_output* wl_output,
        u32 flags,
        i32 width,
        i32 height,
        i32 refresh
    );
    static void handleSurfaceConfigure(
        void* data,
        xdg_surface* surface,
        u32 serial
    );
    static void handleShellPing(
        void* data,
        xdg_wm_base* shell,
        u32 serial
    );
    static void handleRegistry(
        void* data,
        wl_registry* registry,
        u32 id,
        const char* i32erface,
        u32 version
    );
    static void handleLoseRegistry(
        void* data,
        wl_registry* registry,
        u32 id
    );
    static void handleFrameReady(
        void* data,
        wl_callback* callback,
        u32 cb_data
    );
    static void handleToplevelDecorationConfigure(
        void* data,
        zxdg_toplevel_decoration_v1* decoration,
        u32 mode
    );
};

#endif
