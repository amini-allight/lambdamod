/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__APPLE__) && defined(TARGET_OS_IPHONE)
#pragma once

#include "types.hpp"
#include "../input_interface.hpp"

class Controller;
class WindowIOS;

class FlatInputIOS final : public InputInterface
{
public:
    FlatInputIOS(Controller* controller, WindowIOS* window);
    ~FlatInputIOS() override;

    bool step() override;

    void haptic(const HapticEffect& effect) override;

    bool hasGamepad() const override;
    bool hasTouch() const override;

    bool handleEvent(const XEvent& event);

private:
    Controller* controller;
    WindowIOS* window;
    GamepadIOS* gamepad;
};
#endif
