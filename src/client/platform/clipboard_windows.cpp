/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "../clipboard.hpp"
#include "log.hpp"

#include <winuser.h>

string getClipboard()
{
    bool success = OpenClipboard(nullptr);

    if (!success)
    {
        error("Failed to open clipboard: " + to_string(GetLastError()));
        return "";
    }

    HANDLE handle = GetClipboardData(CF_TEXT);

    if (!handle)
    {
        error("Failed to get clipboard data: " + to_string(GetLastError()));
        return "";
    }

    string s = static_cast<const char*>(handle);

    CloseClipboard();

    return s;
}

void setClipboard(const Input& input, const string& text)
{
    bool success = OpenClipboard(nullptr);

    if (!success)
    {
        error("Failed to open clipboard: " + to_string(GetLastError()));
        return;
    }

    HANDLE handle = SetClipboardData(CF_TEXT, const_cast<char*>(text.c_str()));

    if (!handle)
    {
        error("Failed to set clipboard data: " + to_string(GetLastError()));
        return;
    }

    CloseClipboard();
}
#endif
