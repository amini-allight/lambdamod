/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__APPLE__) && defined(TARGET_OS_MAC) && !defined(TARGET_OS_IPHONE)
#include "flat_input_macos.hpp"

FlatInputMacOS::FlatInputMacOS(Controller* controller, WindowMacOS* window)
    : controller(controller)
    , window(window)
    , gamepad(nullptr)
{


    gamepad = new GamepadApple(bind(&Controller::handleInput, controller, placeholders::_1));
}

FlatInputMacOS::~FlatInputMacOS()
{
    delete gamepad;


}

bool FlatInputMacOS::step()
{

}

void FlatInputMacOS::haptic(const HapticEffect& effect)
{
    gamepad->haptic(effect);
}

bool FlatInputMacOS::hasGamepad() const
{
    return gamepad->hasGamepad();
}

bool FlatInputMacOS::hasTouch() const
{
    return false;
}

#endif
