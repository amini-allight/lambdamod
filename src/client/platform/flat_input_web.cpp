/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#include "flat_input_web.hpp"
#include "../controller.hpp"
#include "window_web.hpp"
#include "log.hpp"
#include "../global.hpp"

static FlatInputWeb* webInput = nullptr;

extern "C" void pushWebKeyDown(const char* key)
{
    webInput->key(key, false);
}

extern "C" void pushWebKeyUp(const char* key)
{
    webInput->key(key, true);
}

extern "C" void pushWebMouseDown(i32 button)
{
    webInput->mouseButton(button, false);
}

extern "C" void pushWebMouseUp(i32 button)
{
    webInput->mouseButton(button, true);
}

extern "C" void pushWebMouseMove(i32 x, i32 y)
{
    webInput->mouseMove(x, y);
}

extern "C" void pushWebWheel(i32 x, i32 y)
{
    webInput->mouseWheel(x, y);
}

extern "C" void pushWebTouchStart(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    webInput->touchStart(id, pageX, pageY, radiusX, radiusY, rotationAngle, force);
}

extern "C" void pushWebTouchEnd(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    webInput->touchEnd(id, pageX, pageY, radiusX, radiusY, rotationAngle, force);
}

extern "C" void pushWebTouchMove(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    webInput->touchMove(id, pageX, pageY, radiusX, radiusY, rotationAngle, force);
}

extern "C" void pushWebTouchCancel(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    webInput->touchCancel(id, pageX, pageY, radiusX, radiusY, rotationAngle, force);
}

extern "C" void pushWebQuit()
{
    webInput->quit();
}

extern "C" void pushWebFocus(bool state)
{
    webInput->focus(state);
}

extern "C" void pushWebResize(i32 width, i32 height)
{
    webInput->resize(width, height);
}

EM_JS(u32, navigatorGetMaxTouchPoints, (), {
    return navigator.maxTouchPoints;
});

EM_JS(void, addKeyDownEventListener, (), {
    addEventListener("keydown", function (event) {
        _pushWebKeyDown(event.code);
    });
});

EM_JS(void, addKeyUpEventListener, (), {
    addEventListener("keyup", function (event) {
        _pushWebKeyUp(event.code);
    });
});

EM_JS(void, addMouseDownEventListener, (), {
    addEventListener("mousedown", function (event) {
        _pushWebMouseDown(event.button);
    });
});

EM_JS(void, addMouseUpEventListener, (), {
    addEventListener("mouseup", function (event) {
        _pushWebMouseUp(event.button);
    });
});

EM_JS(void, addMouseMoveEventListener, (), {
    addEventListener("mousemove", function (event) {
        _pushWebMouseMove(
            event.pageX
            event.pageY
        );
    });
});

EM_JS(void, addWheelEventListener, (), {
    addEventListener("wheel", function (event) {
        _pushWebWheel(event.deltaX, event.deltaY);
    });
});

EM_JS(void, addTouchStartEventListener, (), {
    addEventListener("touchstart", function (event) {
        for (var i = 0; i < event.changedTouches.length; i++)
        {
            var touch = event.changedTouches[i];

            _pushWebTouchStart(
                touch.identifier,
                touch.pageX,
                touch.pageY,
                touch.radiusX,
                touch.radiusY,
                touch.rotationAngle,
                touch.force
            );
        }
    });
});

EM_JS(void, addTouchEndEventListener, (), {
    addEventListener("touchend", function (event) {
        for (var i = 0; i < event.changedTouches.length; i++)
        {
            var touch = event.changedTouches[i];

            _pushWebTouchEnd(
                touch.identifier,
                touch.pageX,
                touch.pageY,
                touch.radiusX,
                touch.radiusY,
                touch.rotationAngle,
                touch.force
            );
        }
    });
});

EM_JS(void, addTouchMoveEventListener, (), {
    addEventListener("touchmove", function (event) {
        for (var i = 0; i < event.changedTouches.length; i++)
        {
            var touch = event.changedTouches[i];

            _pushWebTouchMove(
                touch.identifier,
                touch.pageX,
                touch.pageY,
                touch.radiusX,
                touch.radiusY,
                touch.rotationAngle,
                touch.force
            );
        }
    });
});

EM_JS(void, addTouchCancelEventListener, (), {
    addEventListener("touchcancel", function (event) {
        for (var i = 0; i < event.changedTouches.length; i++)
        {
            var touch = event.changedTouches[i];

            _pushWebTouchCancel(
                touch.identifier,
                touch.pageX,
                touch.pageY,
                touch.radiusX,
                touch.radiusY,
                touch.rotationAngle,
                touch.force
            );
        }
    });
});

EM_JS(void, addBeforeUnloadEventListener, (), {
    addEventListener("beforeunload", function (event) {
        _pushWebSetQuit();
    });
});

EM_JS(void, addFocusEventListener, (), {
    addEventListener("focus", function (event) {
        _pushWebFocus(true);
    });
});

EM_JS(void, addBlurEventListener, (), {
    addEventListener("blur", function (event) {
        _pushWebFocus(false);
    });
});

EM_JS(void, addResizeEventListener, (), {
    addEventListener("resize", function (event) {
        _pushWebResize(window.innerWidth, window.innerHeight);
    });
});

FlatInputWeb::FlatInputWeb(Controller* controller, WindowWeb* window)
    : InputInterface()
    , controller(controller)
    , window(window)
    , gamepad(nullptr)
    , currentX(0)
    , currentY(0)
    , lastX(0)
    , lastY(0)
    , leftShift(false)
    , leftControl(false)
    , leftAlt(false)
    , leftSuper(false)
    , rightShift(false)
    , rightControl(false)
    , rightAlt(false)
    , rightSuper(false)
    , _quit(false)
    , focused(false)
    , width(g_config.width)
    , height(g_config.height)
{
    webInput = this;

    addKeyDownEventListener();
    addKeyUpEventListener();
    addMouseDownEventListener();
    addMouseUpEventListener();
    addMouseMoveEventListener();
    addWheelEventListener();
    addTouchStartEventListener();
    addTouchEndEventListener();
    addTouchMoveEventListener();
    addTouchCancelEventListener();
    addBeforeUnloadEventListener();
    addFocusEventListener();
    addBlurEventListener();
    addResizeEventListener();

    gamepad = new GamepadWeb(bind(&Controller::onInput, controller, placeholders::_1));
}

FlatInputWeb::~FlatInputWeb()
{
    delete gamepad;

    webInput = nullptr;
}

bool FlatInputWeb::step()
{
    for (Input input : inputs)
    {
        controller->onInput(input);
    }
    inputs.clear();

    if (currentX != lastX || currentY != lastY)
    {
        Input input;
        input.type = Input_Mouse_Move;
        input.up = false;
        input.intensity = 0;
        input.position = { static_cast<f64>(currentX) / width, static_cast<f64>(currentY) / height, 0 };
        input.rotation = quaternion();
        input.linearMotion = { static_cast<f64>(lastX - currentX) / height * -1, static_cast<f64>(lastY - currentY) / height * -1, 0 };
        input.angularMotion = vec3();
        input.shift = leftShift || rightShift;
        input.control = leftControl || rightControl;
        input.alt = leftAlt || rightAlt;
        input.super = leftSuper || rightSuper;

        controller->onInput(input);

        lastX = currentX;
        lastY = currentY;
    }

    gamepad->step();

    return _quit;
}

void FlatInputWeb::haptic(const HapticEffect& effect)
{
    gamepad->haptic(effect);
}

bool FlatInputWeb::hasGamepad() const
{
    return gamepad->hasGamepad();
}

bool FlatInputWeb::hasTouch() const
{
    return navigatorGetMaxTouchPoints() > 0;
}

void FlatInputWeb::updateModifiers(InputType type, bool up)
{
    switch (type)
    {
    default :
        break;
    case Input_Left_Shift :
        leftShift = !up;
        break;
    case Input_Left_Control :
        leftControl = !up;
        break;
    case Input_Left_Super :
        leftSuper = !up;
        break;
    case Input_Left_Alt :
        leftAlt = !up;
        break;
    case Input_Right_Shift :
        rightShift = !up;
        break;
    case Input_Right_Control :
        rightControl = !up;
        break;
    case Input_Right_Super :
        rightSuper = !up;
        break;
    case Input_Right_Alt :
        rightAlt = !up;
        break;
    }
}

InputType FlatInputWeb::identifyKey(const string& key) const
{
    if (key == "Digit0")
    {
        return Input_0;
    }
    else if (key == "Digit1")
    {
        return Input_1;
    }
    else if (key == "Digit2")
    {
        return Input_2;
    }
    else if (key == "Digit3")
    {
        return Input_3;
    }
    else if (key == "Digit4")
    {
        return Input_4;
    }
    else if (key == "Digit5")
    {
        return Input_5;
    }
    else if (key == "Digit6")
    {
        return Input_6;
    }
    else if (key == "Digit7")
    {
        return Input_7;
    }
    else if (key == "Digit8")
    {
        return Input_8;
    }
    else if (key == "Digit9")
    {
        return Input_9;
    }
    else if (key == "KeyA")
    {
        return Input_A;
    }
    else if (key == "KeyB")
    {
        return Input_B;
    }
    else if (key == "KeyC")
    {
        return Input_C;
    }
    else if (key == "KeyD")
    {
        return Input_D;
    }
    else if (key == "KeyE")
    {
        return Input_E;
    }
    else if (key == "KeyF")
    {
        return Input_F;
    }
    else if (key == "KeyG")
    {
        return Input_G;
    }
    else if (key == "KeyH")
    {
        return Input_H;
    }
    else if (key == "KeyI")
    {
        return Input_I;
    }
    else if (key == "KeyJ")
    {
        return Input_J;
    }
    else if (key == "KeyK")
    {
        return Input_K;
    }
    else if (key == "KeyL")
    {
        return Input_L;
    }
    else if (key == "KeyM")
    {
        return Input_M;
    }
    else if (key == "KeyN")
    {
        return Input_N;
    }
    else if (key == "KeyO")
    {
        return Input_O;
    }
    else if (key == "KeyP")
    {
        return Input_P;
    }
    else if (key == "KeyQ")
    {
        return Input_Q;
    }
    else if (key == "KeyR")
    {
        return Input_R;
    }
    else if (key == "KeyS")
    {
        return Input_S;
    }
    else if (key == "KeyT")
    {
        return Input_T;
    }
    else if (key == "KeyU")
    {
        return Input_U;
    }
    else if (key == "KeyV")
    {
        return Input_V;
    }
    else if (key == "KeyW")
    {
        return Input_W;
    }
    else if (key == "KeyX")
    {
        return Input_X;
    }
    else if (key == "KeyY")
    {
        return Input_Y;
    }
    else if (key == "KeyZ")
    {
        return Input_Z;
    }
    else if (key == "F1")
    {
        return Input_F1;
    }
    else if (key == "F2")
    {
        return Input_F2;
    }
    else if (key == "F3")
    {
        return Input_F3;
    }
    else if (key == "F4")
    {
        return Input_F4;
    }
    else if (key == "F5")
    {
        return Input_F5;
    }
    else if (key == "F6")
    {
        return Input_F6;
    }
    else if (key == "F7")
    {
        return Input_F7;
    }
    else if (key == "F8")
    {
        return Input_F8;
    }
    else if (key == "F9")
    {
        return Input_F9;
    }
    else if (key == "F10")
    {
        return Input_F10;
    }
    else if (key == "F11")
    {
        return Input_F11;
    }
    else if (key == "F12")
    {
        return Input_F12;
    }
    else if (key == "Escape")
    {
        return Input_Escape;
    }
    else if (key == "Backspace")
    {
        return Input_Backspace;
    }
    else if (key == "Tab")
    {
        return Input_Tab;
    }
    else if (key == "CapsLock")
    {
        return Input_Caps_Lock;
    }
    else if (key == "Enter")
    {
        return Input_Return;
    }
    else if (key == "Space")
    {
        return Input_Space;
    }
    else if (key == "ContextMenu")
    {
        return Input_Menu;
    }
    else if (key == "Backquote")
    {
        return Input_Grave_Accent;
    }
    else if (key == "Minus")
    {
        return Input_Minus;
    }
    else if (key == "Equal")
    {
        return Input_Equals;
    }
    else if (key == "BracketLeft")
    {
        return Input_Left_Brace;
    }
    else if (key == "BracketRight")
    {
        return Input_Right_Brace;
    }
    else if (key == "Backslash")
    {
        return Input_Backslash;
    }
    else if (key == "Semicolon")
    {
        return Input_Semicolon;
    }
    else if (key == "Quote")
    {
        return Input_Quote;
    }
    else if (key == "Period")
    {
        return Input_Period;
    }
    else if (key == "Comma")
    {
        return Input_Comma;
    }
    else if (key == "Slash")
    {
        return Input_Slash;
    }
    else if (key == "ShiftLeft")
    {
        return Input_Left_Shift;
    }
    else if (key == "ControlLeft")
    {
        return Input_Left_Control;
    }
    else if (key == "OSLeft" || key == "MetaLeft")
    {
        return Input_Left_Super;
    }
    else if (key == "AltLeft")
    {
        return Input_Left_Alt;
    }
    else if (key == "ShiftRight")
    {
        return Input_Right_Shift;
    }
    else if (key == "ControlRight")
    {
        return Input_Right_Control;
    }
    else if (key == "OSRight" || key == "MetaRight")
    {
        return Input_Right_Super;
    }
    else if (key == "AltRight")
    {
        return Input_Right_Alt;
    }
    else if (key == "PrintScreen")
    {
        return Input_Print_Screen;
    }
    else if (key == "Insert")
    {
        return Input_Insert;
    }
    else if (key == "Delete")
    {
        return Input_Delete;
    }
    else if (key == "ScrollLock")
    {
        return Input_Scroll_Lock;
    }
    else if (key == "Home")
    {
        return Input_Home;
    }
    else if (key == "End")
    {
        return Input_End;
    }
    else if (key == "Pause")
    {
        return Input_Pause;
    }
    else if (key == "PageUp")
    {
        return Input_Page_Up;
    }
    else if (key == "PageDown")
    {
        return Input_Page_Down;
    }
    else if (key == "ArrowLeft")
    {
        return Input_Left;
    }
    else if (key == "ArrowRight")
    {
        return Input_Right;
    }
    else if (key == "ArrowUp")
    {
        return Input_Up;
    }
    else if (key == "ArrowDown")
    {
        return Input_Down;
    }
    else if (key == "NumLock")
    {
        return Input_Num_Lock;
    }
    else if (key == "NumpadAdd")
    {
        return Input_Num_Add;
    }
    else if (key == "NumpadSubtract")
    {
        return Input_Num_Subtract;
    }
    else if (key == "NumpadMultiply")
    {
        return Input_Num_Multiply;
    }
    else if (key == "NumpadDivide")
    {
        return Input_Num_Divide;
    }
    else if (key == "Numpad0")
    {
        return Input_Num_0;
    }
    else if (key == "Numpad1")
    {
        return Input_Num_1;
    }
    else if (key == "Numpad2")
    {
        return Input_Num_2;
    }
    else if (key == "Numpad3")
    {
        return Input_Num_3;
    }
    else if (key == "Numpad4")
    {
        return Input_Num_4;
    }
    else if (key == "Numpad5")
    {
        return Input_Num_5;
    }
    else if (key == "Numpad6")
    {
        return Input_Num_6;
    }
    else if (key == "Numpad7")
    {
        return Input_Num_7;
    }
    else if (key == "Numpad8")
    {
        return Input_Num_8;
    }
    else if (key == "Numpad9")
    {
        return Input_Num_9;
    }
    else if (key == "NumpadDecimal")
    {
        return Input_Num_Period;
    }
    else if (key == "NumpadEnter")
    {
        return Input_Num_Return;
    }
    else
    {
        error("Encountered unknown web key '" + string(key) + "' pressed.");
        return Input_Null;
    }
}

InputType FlatInputWeb::identifyButton(i32 button) const
{
    switch (button)
    {
    case 0 :
        input.type = Input_Left_Mouse;
        break;
    case 1 :
        input.type = Input_Middle_Mouse;
        break;
    case 2 :
        input.type = Input_Right_Mouse;
        break;
    case 3 :
        input.type = Input_X1_Mouse;
        break;
    case 4 :
        input.type = Input_X2_Mouse;
        break;
    default :
        error("Encountered unknown web mouse button " + to_string(button) + " pressed.");
        return Input_Null;
    }
}

void FlatInputWeb::key(const string& key, bool up)
{
    Input input;
    input.type = identifyKey(key);
    input.up = up;
    input.position = { static_cast<f64>(currentX) / width, static_cast<f64>(currentY) / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;

    if (input.type == Input_Null)
    {
        return;
    }

    inputs.push_back(input);

    updateModifiers(input.type, input.up);
}

void FlatInputWeb::mouseButton(i32 button, bool up)
{
    Input input;
    input.type = identifyButton(button);
    input.up = up;
    input.position = { static_cast<f64>(currentX) / width, static_cast<f64>(currentY) / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;

    if (input.type == Input_Null)
    {
        return;
    }

    inputs.push_back(input);
}

void FlatInputWeb::mouseMove(i32 x, i32 y)
{
    currentX = x;
    currentY = y;
}

void FlatInputWeb::mouseWheel(i32 x, i32 y)
{
    Input input;
    input.position = { static_cast<f64>(currentX) / width, static_cast<f64>(currentY) / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;

    if (x < 0)
    {
        input.type = Input_Mouse_Wheel_Left;
        inputs.push_back(input);
    }
    else if (x > 0)
    {
        input.type = Input_Mouse_Wheel_Right;
        inputs.push_back(input);
    }

    if (y < 0)
    {
        input.type = Input_Mouse_Wheel_Left;
        inputs.push_back(input);
    }
    else if (y > 0)
    {
        input.type = Input_Mouse_Wheel_Left;
        inputs.push_back(input);
    }
}

void FlatInputWeb::touchStart(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    Input input;
    input.type = Input_Touch;
    input.up = false;
    input.position = { pageX / width, pageY / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;
    input.touch.id = id;
    input.touch.major = (radiusX < radiusY ? radiusY : radiusX) / width;
    input.touch.minor = (radiusX < radiusY ? radiusX : radiusY) / width;
    input.touch.orientation = radians(rotationAngle);
    input.touch.force = force;

    inputs.push_back(input);
}

void FlatInputWeb::touchEnd(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    Input input;
    input.type = Input_Touch;
    input.up = true;
    input.position = { pageX / width, pageY / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;
    input.touch.id = id;
    input.touch.major = (radiusX < radiusY ? radiusY : radiusX) / width;
    input.touch.minor = (radiusX < radiusY ? radiusX : radiusY) / width;
    input.touch.orientation = radians(rotationAngle);
    input.touch.force = force;

    inputs.push_back(input);
}

void FlatInputWeb::touchMove(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    Input input;
    input.type = Input_Touch;
    input.up = false;
    input.position = { pageX / width, pageY / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;
    input.touch.id = id;
    input.touch.major = (radiusX < radiusY ? radiusY : radiusX) / width;
    input.touch.minor = (radiusX < radiusY ? radiusX : radiusY) / width;
    input.touch.orientation = radians(rotationAngle);
    input.touch.force = force;

    inputs.push_back(input);
}

void FlatInputWeb::touchCancel(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force)
{
    Input input;
    input.type = Input_Touch;
    input.up = true;
    input.position = { pageX / width, pageY / height, 0 };
    input.shift = leftShift || rightShift;
    input.control = leftControl || rightControl;
    input.alt = leftAlt || rightAlt;
    input.super = leftSuper || rightSuper;
    input.touch.id = id;
    input.touch.major = (radiusX < radiusY ? radiusY : radiusX) / width;
    input.touch.minor = (radiusX < radiusY ? radiusX : radiusY) / width;
    input.touch.orientation = radians(rotationAngle);
    input.touch.force = force;

    inputs.push_back(input);
}

void FlatInputWeb::quit()
{
    _quit = true;
}

void FlatInputWeb::focus(bool state)
{
    focused = state;

    if (focused)
    {
        controller->onFocus();
    }
    else
    {
        controller->onUnfocus();
    }
}

void FlatInputWeb::resize(i32 width, i32 height)
{
    this->width = width;
    this->height = height;

    controller->onResize(width, height);
}
#endif
