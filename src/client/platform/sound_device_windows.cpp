/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "sound_device_windows.hpp"
#include "log.hpp"
#include "../global.hpp"
#include "unicode_windows.hpp"

static constexpr u64 nanosecondsPerReferenceTime = 100;

static u64 toReferenceTime(f64 seconds)
{
    return (seconds * 1'000'000'000) / nanosecondsPerReferenceTime;
}

SoundDeviceWindows::SoundDeviceWindows()
    : SoundDeviceInterface()
    , enumerator(nullptr)
    , outputDevice(nullptr)
    , outputClient(nullptr)
    , renderClient(nullptr)
    , inputDevice(nullptr)
    , inputClient(nullptr)
    , captureClient(nullptr)
    , outputRunning(false)
    , inputRunning(false)
{
    log("Initializing sound devices...");

    HRESULT result;

    result = CoCreateInstance(
        __uuidof(MMDeviceEnumerator),
        nullptr,
        CLSCTX_ALL,
        __uuidof(IMMDeviceEnumerator),
        reinterpret_cast<void**>(&enumerator)
    );

    if (result != S_OK)
    {
        fatal("Failed to create Windows sound instance: " + to_string(result));
    }

    outputDevice = getDevice(eRender, eConsole, g_config.soundOutputDevice);

    result = outputDevice->Activate(
        __uuidof(IAudioClient),
        CLSCTX_ALL,
        nullptr,
        reinterpret_cast<void**>(&outputClient)
    );

    if (result != S_OK)
    {
        fatal("Failed to create Windows sound output client: " + to_string(result));
    }

    WAVEFORMATEX outputFormat{};
    outputFormat.wFormatTag = WAVE_FORMAT_PCM;
    outputFormat.nChannels = soundChannels;
    outputFormat.nSamplesPerSec = soundFrequency;
    outputFormat.nAvgBytesPerSec = soundFrequency * soundChannels * sizeof(i16);
    outputFormat.nBlockAlign = soundChannels * sizeof(i16);
    outputFormat.wBitsPerSample = sizeof(i16) * 8;
    outputFormat.cbSize = sizeof(WAVEFORMATEX);

    result = outputClient->Initialize(
        AUDCLNT_SHAREMODE_SHARED,
        0,
        toReferenceTime(soundChunkSamplesPerChannel / static_cast<f64>(soundFrequency)),
        0,
        &outputFormat,
        nullptr
    );

    if (result != S_OK)
    {
        fatal("Failed to initialize Windows sound output client: " + to_string(result));
    }

    result = outputClient->GetService(
        __uuidof(IAudioRenderClient),
        reinterpret_cast<void**>(&renderClient)
    );

    if (result != S_OK)
    {
        fatal("Failed to initialize Windows sound render client: " + to_string(result));
    }

    result = outputClient->Start();

    if (result != S_OK)
    {
        fatal("Failed to start Windows sound render client: " + to_string(result));
    }

    inputDevice = getDevice(eCapture, eConsole, g_config.soundInputDevice);

    result = inputDevice->Activate(
        __uuidof(IAudioClient),
        CLSCTX_ALL,
        nullptr,
        reinterpret_cast<void**>(&inputClient)
    );

    if (result != S_OK)
    {
        fatal("Failed to create Windows sound input client: " + to_string(result));
    }

    WAVEFORMATEX inputFormat{};
    inputFormat.wFormatTag = WAVE_FORMAT_PCM;
    inputFormat.nChannels = voiceChannels;
    inputFormat.nSamplesPerSec = voiceFrequency;
    inputFormat.nAvgBytesPerSec = voiceFrequency * voiceChannels * sizeof(i16);
    inputFormat.nBlockAlign = voiceChannels * sizeof(i16);
    inputFormat.wBitsPerSample = sizeof(i16) * 8;
    inputFormat.cbSize = sizeof(WAVEFORMATEX);

    result = inputClient->Initialize(
        AUDCLNT_SHAREMODE_SHARED,
        0,
        toReferenceTime(voiceChunkSamplesPerChannel / static_cast<f64>(voiceFrequency)),
        0,
        &inputFormat,
        nullptr
    );

    if (result != S_OK)
    {
        fatal("Failed to initialize Windows sound input client: " + to_string(result));
    }

    result = inputClient->GetService(
        __uuidof(IAudioCaptureClient),
        reinterpret_cast<void**>(&captureClient)
    );

    if (result != S_OK)
    {
        fatal("Failed to initialize Windows sound capture client: " + to_string(result));
    }

    result = inputClient->Start();

    if (result != S_OK)
    {
        fatal("Failed to start Windows sound capture client: " + to_string(result));
    }

    wchar_t* wideOutputDeviceName = nullptr;
    result = outputDevice->GetId(&wideOutputDeviceName);

    if (result != S_OK)
    {
        fatal("Failed to get Windows sound device name: " + to_string(result));
    }

    log("Using sound output device '" + utf16ToUTF8(wideOutputDeviceName) + "'.");

    wchar_t* wideInputDeviceName = nullptr;
    result = inputDevice->GetId(&wideInputDeviceName);

    if (result != S_OK)
    {
        fatal("Failed to get Windows sound device name: " + to_string(result));
    }

    log("Using sound input device '" + utf16ToUTF8(wideInputDeviceName) + "'.");

    outputRunning = true;
    outputRunThread = thread(bind(&SoundDeviceWindows::runOutput, this));

    inputRunning = true;
    inputRunThread = thread(bind(&SoundDeviceWindows::runInput, this));

    log("Sound devices initialized.");
}

SoundDeviceWindows::~SoundDeviceWindows()
{
    log("Shutting down sound devices...");

    HRESULT result;

    inputRunning = false;
    inputRunThread.join();

    outputRunning = false;
    outputRunThread.join();

    result = inputClient->Stop();

    if (result != S_OK)
    {
        error("Failed to stop Windows sound capture client: " + to_string(result));
    }

    result = outputClient->Stop();

    if (result != S_OK)
    {
        error("Failed to stop Windows sound render client: " + to_string(result));
    }

    captureClient->Release();
    inputClient->Release();
    inputDevice->Release();

    renderClient->Release();
    outputClient->Release();
    outputDevice->Release();

    enumerator->Release();

    log("Sound devices shut down.");
}

void SoundDeviceWindows::runOutput()
{
    HRESULT result;

    u32 bufferFrameCount = 0;
    result = outputClient->GetBufferSize(&bufferFrameCount);

    if (result != S_OK)
    {
        error("Failed to get Windows sound output buffer size: " + to_string(result));
    }

    chrono::microseconds bufferDuration(static_cast<u64>(1'000'000 * (bufferFrameCount / static_cast<f64>(soundFrequency))));

    while (outputRunning)
    {
        this_thread::sleep_for(bufferDuration / 2);

        u32 frameCount = 0;
        result = outputClient->GetCurrentPadding(&frameCount);

        if (result != S_OK)
        {
            error("Failed to get Windows sound output frame count: " + to_string(result));
            continue;
        }

        u32 sampleCount = frameCount * soundChannels;

        i16* data = nullptr;
        result = renderClient->GetBuffer(frameCount, reinterpret_cast<BYTE**>(&data));

        if (result != S_OK)
        {
            error("Failed to get Windows sound output buffer: " + to_string(result));
            continue;
        }

        memset(data, 0, sampleCount * sizeof(i16));
        u32 previousSampleCount = sampleCount;
        sampleCount = outputBuffer.pop(data, sampleCount);

        if (sampleCount < previousSampleCount)
        {
            debug("Sound output overread occurred, some silence has been emitted.");
        }

        frameCount = sampleCount / soundChannels;

        result = renderClient->ReleaseBuffer(frameCount, 0);

        if (result != S_OK)
        {
            error("Failed to release Windows sound output buffer: " + to_string(result));
        }
    }
}

void SoundDeviceWindows::runInput()
{
    HRESULT result;

    u32 bufferFrameCount = 0;
    result = inputClient->GetBufferSize(&bufferFrameCount);

    if (result != S_OK)
    {
        error("Failed to get Windows sound input buffer size: " + to_string(result));
    }

    chrono::microseconds bufferDuration(static_cast<u64>(1'000'000 * (bufferFrameCount / static_cast<f64>(voiceFrequency))));

    while (inputRunning)
    {
        this_thread::sleep_for(chrono::microseconds(bufferDuration / 2));

        u32 frameCount = 0;
        result = captureClient->GetNextPacketSize(&frameCount);

        if (result != S_OK)
        {
            error("Failed to get Windows sound input frame count: " + to_string(result));
            continue;
        }

        while (frameCount != 0)
        {
            i16* data = nullptr;
            DWORD flags = 0;
            result = captureClient->GetBuffer(
                reinterpret_cast<BYTE**>(&data),
                &frameCount,
                &flags,
                nullptr,
                nullptr
            );

            if (result != S_OK)
            {
                error("Failed to get Windows sound input buffer: " + to_string(result));
                break;
            }

            u32 sampleCount = frameCount * voiceChannels;
            u32 nextSampleCount = inputBuffer.push(data, sampleCount);

            if (nextSampleCount < sampleCount)
            {
                debug("Sound input overwrite occurred, some data has been lost.");
            }

            result = captureClient->ReleaseBuffer(frameCount);

            if (result != S_OK)
            {
                error("Failed to release Windows sound input buffer: " + to_string(result));
                break;
            }

            result = captureClient->GetNextPacketSize(&frameCount);

            if (result != S_OK)
            {
                error("Failed to get Windows sound input frame count: " + to_string(result));
                break;
            }
        }
    }
}

IMMDevice* SoundDeviceWindows::getDevice(EDataFlow type, ERole role, const string& name) const
{
    HRESULT result;

    if (name.empty())
    {
        IMMDevice* device = nullptr;

        result = enumerator->GetDefaultAudioEndpoint(
            type,
            role,
            &device
        );

        if (result != S_OK)
        {
            fatal("Failed to get Windows sound device: " + to_string(result));
        }

        return device;
    }
    else
    {
        IMMDeviceCollection* devices = nullptr;

        result = enumerator->EnumAudioEndpoints(
            type,
            DEVICE_STATE_ACTIVE,
            &devices
        );

        if (result != S_OK)
        {
            fatal("Failed to enumerate Windows sound devices: " + to_string(result));
        }

        UINT deviceCount = 0;

        result = devices->GetCount(&deviceCount);

        if (result != S_OK)
        {
            fatal("Failed to get Windows sound device count: " + to_string(result));
        }

        for (UINT i = 0; i < deviceCount; i++)
        {
            IMMDevice* device = nullptr;

            result = devices->Item(i, &device);

            if (result != S_OK)
            {
                fatal("Failed to get Windows sound device: " + to_string(result));
            }

            wchar_t* wideDeviceName = nullptr;

            result = device->GetId(&wideDeviceName);

            if (result != S_OK)
            {
                fatal("Failed to get Windows sound device name: " + to_string(result));
            }

            string deviceName = utf16ToUTF8(wideDeviceName);

            if (deviceName == name)
            {
                return device;
            }
        }

        fatal("Failed to find Windows sound device '" + name + "'.");
        return nullptr;
    }
}
#endif
