/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__unix__) || defined(__APPLE__)
#include "../server_runner.hpp"
#include "tools.hpp"

#include <unistd.h>
#include <sys/wait.h>
#include <csignal>

ServerRunner::ServerRunner(
    const string& host,
    u16 port,
    const string& password,
    const string& adminPassword,
    const string& savePath
)
{
    pid = fork();

    if (pid == 0)
    {
        execl(
            "./lambdamod-server",
            "./lambdamod-server",
            host.c_str(),
            to_string(port).c_str(),
            password.c_str(),
            adminPassword.c_str(),
            savePath.c_str(),
            static_cast<char*>(nullptr)
        );
    }
    else
    {
        this_thread::sleep_for(maxSetupTime);
    }
}

ServerRunner::~ServerRunner()
{
    int result = kill(pid, SIGINT);

    if (result)
    {
        int status;
        waitpid(pid, &status, 0);
        return;
    }

    chrono::milliseconds startTime = currentTime();

    while (currentTime() - startTime < maxShutdownTime)
    {
        result = kill(pid, 0);

        if (result)
        {
            int status;
            waitpid(pid, &status, 0);
            return;
        }

        this_thread::sleep_for(chrono::milliseconds(10));
    }

    kill(pid, SIGKILL);

    int status;
    waitpid(pid, &status, 0);
}

void ServerRunner::step()
{
    int result = kill(pid, 0);

    if (result)
    {
        int status;
        waitpid(pid, &status, 0);
    }
}
#endif
