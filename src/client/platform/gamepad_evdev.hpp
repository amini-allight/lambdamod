/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__linux__) || defined(__FreeBSD__)
#pragma once

#include "types.hpp"
#include "../gamepad_interface.hpp"

class GamepadEvdev final : public GamepadInterface
{
public:
    GamepadEvdev(const function<void(const Input&)>& inputCallback);
    ~GamepadEvdev() override;

    bool hasGamepad() const override;

    void step() override;

    void haptic(const HapticEffect& effect) override;

private:
    function<void(const Input&)> inputCallback;

    vec2 leftStickMax;
    vec2 rightStickMax;
    f64 leftTriggerMax;
    f64 rightTriggerMax;

    int file;
    vec2 lastLeft;
    vec2 lastRight;
    InputType lastDPadX;
    InputType lastDPadY;
    optional<i16> lastEffect;

    i32 getMaximum(i32 index) const;
    bool isGamepad(const string& path) const;
};
#endif
