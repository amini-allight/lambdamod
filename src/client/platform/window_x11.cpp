/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_X11)
#include "window_x11.hpp"
#include "log.hpp"
#include "../global.hpp"
#include "paths.hpp"
#include "constants.hpp"
#include "../render_tools.hpp"
#include "../render_constants.hpp"
#include "../image.hpp"

WindowX11* x11Window = nullptr;

WindowX11::WindowX11(Controller* controller)
    : WindowInterface()
    , _display(nullptr)
    , screen(0)
    , visual(nullptr)
    , _window(0)
    , locked(false)
    , _focused(true)
{
    log("Initializing window...");

    int ok = XInitThreads();

    if (!ok)
    {
        error("Failed to initialize X11 threading support.");
    }

    _display = XOpenDisplay(nullptr);

    if (!_display)
    {
        fatal("Failed to open X11 display.");
    }

    screen = DefaultScreen(_display);
    
    visual = DefaultVisual(_display, screen);

    u32 screenWidth = DisplayWidth(_display, screen);
    u32 screenHeight = DisplayHeight(_display, screen);

    u32 width = g_config.fullscreen ? screenWidth : g_config.width;
    u32 height = g_config.fullscreen ? screenHeight : g_config.height;

    u32 x = (screenWidth - width) / 2;
    u32 y = (screenHeight - height) / 2;

    XSetWindowAttributes attributes;
    attributes.colormap = XCreateColormap(_display, DefaultRootWindow(_display), visual, AllocNone);
    attributes.background_pixel = 0;
    attributes.border_pixel = 0;

    _window = XCreateWindow(
        _display,
        DefaultRootWindow(_display),
        x, y,
        width, height,
        0,
        24,
        InputOutput,
        visual,
        CWColormap | CWBackPixel | CWBorderPixel,
        &attributes
    );

    if (!_window)
    {
        fatal("Failed to create X11 window.");
    }

    // Size limits
    XSizeHints* sizeHints = XAllocSizeHints();
    
    sizeHints->flags = PMinSize | PMaxSize;
    sizeHints->min_width = minWidth;
    sizeHints->min_height = minHeight;
    sizeHints->max_width = maxWidth;
    sizeHints->max_height = maxHeight;

    Atom normalHints = XInternAtom(_display, "WM_NORMAL_HINTS", false);
    XSetWMSizeHints(_display, _window, sizeHints, normalHints);

    XFree(sizeHints);

    // Icon & title
    Image* iconImage = loadPNG(resourcePath() + "/icon" + iconExt);

    auto xpmData = new u64[2 + (iconImage->width() * iconImage->height())];
    xpmData[0] = iconImage->width();
    xpmData[1] = iconImage->height();
    
    for (u32 i = 0; i < iconImage->width() * iconImage->height(); i++)
    {
        xpmData[2 + i] = reinterpret_cast<const u32*>(iconImage->pixels())[i];
    }

    Atom netWMIcon = XInternAtom(_display, "_NET_WM_ICON", false);
    Atom cardinal = XInternAtom(_display, "CARDINAL", false);

    ok = XChangeProperty(
        _display,
        _window,
        netWMIcon,
        cardinal,
        32,
        PropModeReplace,
        reinterpret_cast<const u8*>(xpmData),
        2 + (iconImage->width() * iconImage->height())
    );

    if (!ok)
    {
        error("Failed to change X11 property.");
    }

    delete[] xpmData;

    auto iconData = static_cast<char*>(malloc(iconImage->size()));
    memcpy(iconData, iconImage->pixels(), iconImage->size());

    XImage* ximage = XCreateImage(
        _display,
        visual,
        32,
        ZPixmap,
        0,
        iconData,
        iconImage->width(), iconImage->height(),
        32,
        0
    );

    if (!ximage)
    {
        error("Failed to create X11 image.");
        free(iconData);
    }

    Pixmap iconPixmap = XCreatePixmap(
        _display,
        _window,
        iconImage->width(), iconImage->height(),
        32
    );

    GC gc = XCreateGC(_display, iconPixmap, 0, nullptr);

    XPutImage(
        _display,
        iconPixmap,
        gc,
        ximage,
        0, 0,
        0, 0,
        iconImage->width(), iconImage->height()
    );

    ok = XSetStandardProperties(
        _display,
        _window,
        gameName,
        appClass,
        iconPixmap,
        nullptr,
        0,
        nullptr
    );

    if (!ok)
    {
        error("Failed to set X11 window properties.");
    }

    XWMHints* wmHints = XAllocWMHints();

    wmHints->flags = IconPixmapHint;
    wmHints->icon_pixmap = iconPixmap;

    ok = XSetWMHints(_display, _window, wmHints);

    if (!ok)
    {
        error("Failed to set X11 window manager hints.");
    }

    XFree(wmHints);

    XFreeGC(_display, gc);
    XFreePixmap(_display, iconPixmap);
    XDestroyImage(ximage);

    delete iconImage;

    // Class
    XClassHint* classHint = XAllocClassHint();

    classHint->res_name = const_cast<char*>(appID);
    classHint->res_class = const_cast<char*>(appClass);

    ok = XSetClassHint(_display, _window, classHint);

    if (!ok)
    {
        error("Failed to set X11 window class hint.");
    }

    XFree(classHint);

    // Setup I/O
    ok = XSelectInput(
        _display,
        _window,
        KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask | PropertyChangeMask | FocusChangeMask
    );

    if (!ok)
    {
        error("Failed to select X11 window input.");
    }

    ok = XMapWindow(_display, _window);

    if (!ok)
    {
        fatal("Failed to map X11 window.");
    }
 
    // Deletion support
    Atom deleteWindow = XInternAtom(_display, "WM_DELETE_WINDOW", false);

    ok = XSetWMProtocols(_display, _window, &deleteWindow, 1);

    if (!ok)
    {
        error("Failed to set window manager protocols.");
    }

    // Custom cursor
    Image* customCursorImage = loadPNG(resourcePath() + "/cursor" + iconExt);

    auto customCursorData = static_cast<char*>(malloc(customCursorImage->size()));
    memcpy(customCursorData, customCursorImage->pixels(), customCursorImage->size());

    ximage = XCreateImage(
        _display,
        visual,
        32,
        ZPixmap,
        0,
        customCursorData,
        customCursorImage->width(), customCursorImage->height(),
        32,
        0
    );

    if (!ximage)
    {
        error("Failed to create X11 image.");
        free(customCursorData);
    }

    Pixmap customCursorPixmap = XCreatePixmap(
        _display,
        _window,
        customCursorImage->width(), customCursorImage->height(),
        32
    );

    gc = XCreateGC(_display, customCursorPixmap, 0, 0);

    XPutImage(
        _display,
        customCursorPixmap,
        gc,
        ximage,
        0, 0,
        0, 0,
        customCursorImage->width(), customCursorImage->height()
    );

    Picture picture = XRenderCreatePicture(
        _display,
        customCursorPixmap,
        XRenderFindStandardFormat(_display, PictStandardARGB32),
        0,
        nullptr
    );

    cursor = XRenderCreateCursor(_display, picture, 0, 0);
    ok = XDefineCursor(_display, _window, cursor);

    if (!ok)
    {
        error("Failed to define X11 cursor.");
    }

    XRenderFreePicture(_display, picture);
    XFreeGC(_display, gc);
    XFreePixmap(_display, customCursorPixmap);
    XDestroyImage(ximage);

    delete customCursorImage;

    // Empty cursor
    char emptyCursorData[1 * 1];
    memset(emptyCursorData, 0, sizeof(emptyCursorData));

    Pixmap emptyCursorPixmap = XCreateBitmapFromData(_display, DefaultRootWindow(_display), emptyCursorData, 1, 1);

    XColor color;
    memset(&color, 0, sizeof(color));

    emptyCursor = XCreatePixmapCursor(_display, emptyCursorPixmap, emptyCursorPixmap, &color, &color, 0, 0);

    XFreePixmap(_display, emptyCursorPixmap);

    _input = new FlatInputX11(controller, this);

    x11Window = this;

    log("Window initialized.");
}

WindowX11::~WindowX11()
{
    log("Shutting down window...");

    x11Window = nullptr;

    delete _input;

    XFreeCursor(_display, emptyCursor);
    XFreeCursor(_display, cursor);
    XDestroyWindow(_display, _window);
    XCloseDisplay(_display);

    log("Window shut down.");
}

InputInterface* WindowX11::input() const
{
    return _input;
}

void WindowX11::step()
{
    if (locked)
    {
        Window root;
        i32 x;
        i32 y;
        u32 w;
        u32 h;
        u32 borderWidth;
        u32 depth;

        int ok = XGetGeometry(_display, _window, &root, &x, &y, &w, &h, &borderWidth, &depth);

        if (!ok)
        {
            return;
        }

        ok = XWarpPointer(_display, _window, _window, 0, 0, 0, 0, w / 2, h / 2);

        if (!ok)
        {
            error("Failed to warp X11 pointer.");
        }
    }
}

void WindowX11::lockCursor(bool state)
{
    locked = state;
}

void WindowX11::showCursor(bool state)
{
    int ok;

    if (state)
    {
        ok = XDefineCursor(_display, _window, cursor);
    }
    else
    {
        ok = XDefineCursor(_display, _window, emptyCursor);
    }

    if (!ok)
    {
        error("Failed to set X11 cursor.");
    }
}

void WindowX11::setSize(u32 width, u32 height)
{
    u32 screenWidth = DisplayWidth(_display, screen);
    u32 screenHeight = DisplayHeight(_display, screen);

    int ok = XMoveResizeWindow(
        _display,
        _window,
        (screenWidth - width) / 2,
        (screenHeight - height) / 2,
        width,
        height
    );

    if (!ok)
    {
        error("Failed to resize X11 window.");
    }
}

void WindowX11::setFullscreen(bool state)
{
    Atom wmState = XInternAtom(_display, "_NET_WM_STATE", false);
    Atom wmStateFullscreen = XInternAtom(_display, "_NET_WM_STATE_FULLSCREEN", false);

    XEvent event{};
    event.type = ClientMessage;
    event.xclient.window = _window;
    event.xclient.message_type = wmState;
    event.xclient.format  = 32;
    event.xclient.data.l[0] = state;
    event.xclient.data.l[1] = wmStateFullscreen;

    int ok = XSendEvent(_display, DefaultRootWindow(_display), 0, SubstructureRedirectMask | SubstructureNotifyMask, &event);

    if (!ok)
    {
        error("Failed to send X11 event.");
    }
}

bool WindowX11::fullscreen() const
{
    Atom wmState = XInternAtom(_display, "_NET_WM_STATE", false);
    Atom wmStateFullscreen = XInternAtom(_display, "_NET_WM_STATE_FULLSCREEN", false);

    u8* data = nullptr;
    Atom type;
    i32 format;
    u64 size = 0;
    u64 remaining = 0;

    int fail = XGetWindowProperty(
        _display,
        _window,
        wmState,
        0,
        2,
        False,
        XA_ATOM,
        &type,
        &format,
        &size,
        &remaining,
        &data
    );

    if (fail)
    {
        error("Failed to get X11 window property.");
    }

    auto atoms = reinterpret_cast<const Atom*>(data);

    bool fullscreen = false;

    for (u64 i = 0; i < size; i++)
    {
        if (atoms[i] == wmStateFullscreen)
        {
            fullscreen = true;
            break;
        }
    }

    XFree(data);

    return fullscreen;
}

bool WindowX11::focused() const
{
    return _focused;
}

set<string> WindowX11::vulkanExtensions() const
{
    static const set<string> extensionNames = {
        "VK_KHR_surface",
        "VK_KHR_xlib_surface"
    };

    return extensionNames;
}

VkSurfaceKHR WindowX11::createVulkanSurface(VkInstance instance) const
{
    VkSurfaceKHR surface;

    VkXlibSurfaceCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
    createInfo.dpy = _display;
    createInfo.window = _window; 

    VkResult result = vkCreateXlibSurfaceKHR(instance, &createInfo, nullptr, &surface);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan surface: " + vulkanError(result));
    }

    return surface;
}

Display* WindowX11::display() const
{
    return _display;
}

Window WindowX11::window() const
{
    return _window;
}
#endif
