/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_PULSEAUDIO)
#include "sound_device_pulseaudio.hpp"
#include "constants.hpp"
#include "../global.hpp"
#include "log.hpp"

SoundDevicePulseAudio::SoundDevicePulseAudio()
    : SoundDeviceInterface()
    , mainloop(nullptr)
    , api(nullptr)
    , context(nullptr)
    , output(nullptr)
    , input(nullptr)
{
    log("Initializing sound devices...");

    int result;

    mainloop = pa_mainloop_new();

    if (!mainloop)
    {
        fatal("Failed to initialize PulseAudio main loop.");
    }

    api = pa_mainloop_get_api(mainloop);

    context = pa_context_new(api, gameName);

    if (!context)
    {
        fatal("Failed to initialize PulseAudio context.");
    }

    pa_context_set_state_callback(context, SoundDevicePulseAudio::onContextState, this);

    result = pa_context_connect(context, nullptr, PA_CONTEXT_NOFLAGS, nullptr);

    if (result < 0)
    {
        fatal("Failed to connect to PulseAudio server.");
    }

    while ((!output || !pa_stream_get_device_name(output)) && (!input || !pa_stream_get_device_name(input)))
    {
        pa_mainloop_iterate(mainloop, 0, nullptr);
    }

    if (pa_stream_get_device_name(output))
    {
        log("Using sound output device '" + string(pa_stream_get_device_name(output)) + "'.");
    }

    if (pa_stream_get_device_name(input))
    {
        log("Using sound input device '" + string(pa_stream_get_device_name(input)) + "'.");
    }

    runThread = thread(bind(&SoundDevicePulseAudio::run, this));

    log("Sound devices initialized.");
}

SoundDevicePulseAudio::~SoundDevicePulseAudio()
{
    log("Shutting down sound devices...");

    pa_mainloop_quit(mainloop, 0);
    runThread.join();

    if (input)
    {
        pa_stream_unref(input);
    }

    if (output)
    {
        pa_stream_unref(output);
    }

    pa_context_unref(context);
    pa_mainloop_free(mainloop);

    log("Sound devices shut down.");
}

void SoundDevicePulseAudio::run()
{
    int retval;
    pa_mainloop_run(mainloop, &retval);
}

void SoundDevicePulseAudio::onContextState(pa_context* context, void* userdata)
{
    auto self = static_cast<SoundDevicePulseAudio*>(userdata);

    switch (pa_context_get_state(context))
    {
    case PA_CONTEXT_CONNECTING :
    case PA_CONTEXT_AUTHORIZING :
    case PA_CONTEXT_SETTING_NAME :
        break;
    case PA_CONTEXT_READY :
    {
        pa_sample_spec outputSampleSpec;
        outputSampleSpec.format = PA_SAMPLE_S16NE;
        outputSampleSpec.channels = soundChannels;
        outputSampleSpec.rate = soundFrequency;

        self->output = pa_stream_new(context, gameName, &outputSampleSpec, nullptr);

        if (!self->output)
        {
            error("Failed to initialize PulseAudio output stream.");
        }

        pa_buffer_attr outputBufferAttributes;
        outputBufferAttributes.maxlength = soundChannels * soundChunkSamplesPerChannel * sizeof(i16);
        outputBufferAttributes.tlength = soundChannels * soundChunkSamplesPerChannel * sizeof(i16);
        outputBufferAttributes.prebuf = static_cast<u32>(-1);
        outputBufferAttributes.minreq = static_cast<u32>(-1);
        outputBufferAttributes.fragsize = static_cast<u32>(-1);

        pa_stream_set_state_callback(self->output, SoundDevicePulseAudio::onOutputState, userdata);
        pa_stream_set_write_callback(self->output, SoundDevicePulseAudio::onOutputWrite, userdata);
        pa_stream_connect_playback(
            self->output,
            g_config.soundOutputDevice.empty() ? nullptr : g_config.soundOutputDevice.c_str(),
            &outputBufferAttributes,
            PA_STREAM_NOFLAGS,
            nullptr,
            nullptr
        );

        pa_sample_spec inputSampleSpec;
        inputSampleSpec.format = PA_SAMPLE_S16NE;
        inputSampleSpec.channels = voiceChannels;
        inputSampleSpec.rate = voiceFrequency;

        self->input = pa_stream_new(context, gameName, &inputSampleSpec, nullptr);

        if (!self->input)
        {
            error("Failed to initialize PulseAudio input stream.");
        }

        pa_buffer_attr inputBufferAttributes;
        inputBufferAttributes.maxlength = voiceChannels * voiceChunkSamplesPerChannel * sizeof(i16);
        inputBufferAttributes.tlength = voiceChannels * voiceChunkSamplesPerChannel * sizeof(i16);
        inputBufferAttributes.prebuf = static_cast<u32>(-1);
        inputBufferAttributes.minreq = static_cast<u32>(-1);
        inputBufferAttributes.fragsize = static_cast<u32>(-1);

        pa_stream_set_state_callback(self->input, SoundDevicePulseAudio::onInputState, userdata);
        pa_stream_set_read_callback(self->input, SoundDevicePulseAudio::onInputRead, userdata);
        pa_stream_connect_record(
            self->input,
            g_config.soundInputDevice.empty() ? nullptr : g_config.soundInputDevice.c_str(),
            &inputBufferAttributes,
            PA_STREAM_NOFLAGS
        );
        break;
    }
    case PA_CONTEXT_TERMINATED :
        break;
    case PA_CONTEXT_FAILED :
    default :
        error("PulseAudio connection failed: " + string(pa_strerror(pa_context_errno(context))));
        break;
    }
}

void SoundDevicePulseAudio::onOutputState(pa_stream* stream, void* userdata)
{
    auto self = static_cast<SoundDevicePulseAudio*>(userdata);

    switch (pa_stream_get_state(stream))
    {
    case PA_STREAM_CREATING :
    case PA_STREAM_TERMINATED :
    case PA_STREAM_READY :
        break;
    case PA_STREAM_FAILED :
    default:
        error("PulseAudio output stream setup failed: " + string(pa_strerror(pa_context_errno(self->context))));
        break;
    }
}

void SoundDevicePulseAudio::onOutputWrite(pa_stream* stream, size_t length, void* userdata)
{
    auto self = static_cast<SoundDevicePulseAudio*>(userdata);

    int result;

    auto data = static_cast<i16*>(pa_xmalloc(length));
    u32 count = length / sizeof(i16);

    memset(data, 0, length);

    u32 previousCount = count;
    count = self->outputBuffer.pop(data, count);

    if (count < previousCount)
    {
        debug("Sound output overread occurred, some silence has been emitted.");
    }

    result = pa_stream_write(stream, data, length, pa_xfree, 0, PA_SEEK_RELATIVE);

    if (result < 0)
    {
        error("Failed to write to PulseAudio output stream: " + string(pa_strerror(pa_context_errno(self->context))));
        return;
    }
}

void SoundDevicePulseAudio::onInputState(pa_stream* stream, void* userdata)
{
    auto self = static_cast<SoundDevicePulseAudio*>(userdata);

    switch (pa_stream_get_state(stream))
    {
    case PA_STREAM_CREATING :
    case PA_STREAM_TERMINATED :
    case PA_STREAM_READY :
        break;
    case PA_STREAM_FAILED :
    default:
        error("PulseAudio input stream setup failed: " + string(pa_strerror(pa_context_errno(self->context))));
        break;
    }
}

void SoundDevicePulseAudio::onInputRead(pa_stream* stream, size_t length, void* userdata)
{
    auto self = static_cast<SoundDevicePulseAudio*>(userdata);

    int result;

    const i16* data = nullptr;
    result = pa_stream_peek(stream, reinterpret_cast<const void**>(&data), &length);

    if (result < 0)
    {
        error("Failed to read from PulseAudio input stream: " + string(pa_strerror(pa_context_errno(self->context))));
        return;
    }

    u32 count = length / sizeof(i16);
    self->inputBuffer.push(data, count);

    if (length != 0)
    {
        result = pa_stream_drop(stream);

        if (result < 0)
        {
            error("Failed to read from PulseAudio input stream: " + string(pa_strerror(pa_context_errno(self->context))));
            return;
        }
    }
}
#endif
