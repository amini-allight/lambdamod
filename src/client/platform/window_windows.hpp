/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#pragma once

#include "types.hpp"
#include "../window_interface.hpp"
#include "flat_input_windows.hpp"

#include <windows.h>
#include <vulkan/vulkan_win32.h>

class Controller;

class WindowWindows final : public WindowInterface
{
public:
    WindowWindows(Controller* controller);
    ~WindowWindows() override;

    InputInterface* input() const override;

    void step() override;

    void lockCursor(bool state) override;
    void showCursor(bool state) override;

    void setSize(u32 width, u32 height) override;
    void setFullscreen(bool state) override;
    bool fullscreen() const override;
    bool focused() const override;

    set<string> vulkanExtensions() const override;
    VkSurfaceKHR createVulkanSurface(VkInstance instance) const override;

private:
    friend class FlatInputWindows;

    HINSTANCE hInstance;
    HICON icon;
    HICON cursor;
    ATOM _class;
    HWND hwnd;
    bool locked;
    bool _focused;
    FlatInputWindows* _input;

    DWORD getStyle(bool fullscreen) const;
    DWORD getExtendedStyle(bool fullscreen) const;

public:
    static LRESULT handleWindowEvent(HWND hwnd, u32 message, WPARAM wParam, LPARAM lParam);
};
#endif
