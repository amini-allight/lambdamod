/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#pragma once

#include "types.hpp"
#include "../input_interface.hpp"
#include "gamepad_windows.hpp"

class Controller;
class WindowWindows;

class FlatInputWindows final : public InputInterface
{
public:
    FlatInputWindows(Controller* controller, WindowWindows* window);
    ~FlatInputWindows() override;

    bool step() override;

    void haptic(const HapticEffect& effect) override;

    bool hasGamepad() const override;
    bool hasTouch() const override;

private:
    Controller* controller;
    WindowWindows* window;
    GamepadWindows* gamepad;

    i32 currentX;
    i32 currentY;
    i32 lastX;
    i32 lastY;
    bool leftShift;
    bool leftControl;
    bool leftAlt;
    bool leftSuper;
    bool rightShift;
    bool rightControl;
    bool rightAlt;
    bool rightSuper;
    u32 width;
    u32 height;

    bool quit;

    map<u32, vec3> lastTouches;

    void updateModifiers(InputType type, bool up);
    InputType identifyButton(WPARAM wParam) const;
    InputType identifyKey(WPARAM wParam) const;

private:
    friend class WindowWindows;

    void handle(HWND hwnd, u32 message, WPARAM wParam, LPARAM lParam);
};
#endif
