/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#include "window_web.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "../global.hpp"

static i32 mouseX = 0;
static i32 mouseY = 0;

extern "C" setWebMouseMove(i32 x, i32 y)
{
    mouseX = x;
    mouseY = y;
}

EM_JS(void, documentSetTitle, (const char* title), {
    document.title = title;
});

EM_JS(void, documentSetFavicon, (const char* data), {
    document.querySelector("link[rel~='icon']").href = "data:image/png;base64," + btoa(data);
});

EM_JS(void, cursorSetIcon, (const char* data), {
    document.getElementById("cursor").src = "data:image/png;base64," + btoa(data);
});

EM_JS(bool, documentIsFullscreen, (), {
    return document.fullscreenElement != null;
});

EM_JS(bool, documentHasFocus, (), {
    return document.hasFocus();
});

EM_JS(void, htmlRequestFullscreen, (), {
    document.children[0].requestFullscreen();
});

EM_JS(void, htmlExitFullscreen, (), {
    document.children[0].exitFullscreen();
});

EM_JS(void, htmlRequestPointerLock, (), {
    document.children[0].requestPointerLock();
});

EM_JS(void, documentExitFullscreen, (), {
    document.exitPointerLock();
});

EM_JS(void, htmlSetCursorStyle, (const char* value), {
    document.children[0].style.cursor = value;
});

EM_JS(void, htmlSetMinWidth, (u32 width), {
    document.children[0].style.minWidth = width;
});

EM_JS(void, htmlSetMinHeight, (u32 height), {
    document.children[0].style.minHeight = height;
});

EM_JS(void, htmlSetMaxWidth, (u32 width), {
    document.children[0].style.maxWidth = width;
});

EM_JS(void, htmlSetMaxHeight, (u32 height), {
    document.children[0].style.maxHeight = height;
});

EM_JS(void, addMouseMoveEventListener, (), {
    addEventListener("mousemove", function (event) {
        _setWebMouseMove(
            event.pageX
            event.pageY
        );
    });
});

EM_JS(void, setCursorPosition, (i32 x, i32 y), {
    document.getElementById("cursor").style.top = y + "px";
    document.getElementById("cursor").style.left = x + "px";
});

EM_JS(void, cursorSetShown, (const char* value), {
    document.getElementById("cursor").style.display = value;
});

WindowWeb::WindowWeb(Controller* controller)
    : WindowInterface()
{
    log("Initializing window...");

    documentSetTitle(gameName);
    documentSetFavicon(getFile(resourcePath() + "/icon" + iconExt));

    htmlSetCursorStyle("none");
    cursorSetIcon(getFile(resourcePath() + "/cursor" + iconExt));

    htmlSetMinWidth(minWidth);
    htmlSetMinHeight(minHeight);
    htmlSetMaxWidth(maxWidth);
    htmlSetMaxHeight(maxHeight);

    addMouseMoveEventListener();

    _input = new FlatInputWeb(controller, this);

    log("Window initialized.");
}

WindowWeb::~WindowWeb()
{
    log("Shutting down window...");

    delete _input;

    log("Window shut down.");
}

InputInterface* WindowWeb::input() const
{
    return _input;
}

void WindowWeb::step()
{
    setCursorPosition(mouseX, mouseY);
}

void WindowWeb::lockCursor(bool state)
{
    if (state)
    {
        htmlRequestPointerLock();
    }
    else
    {
        documentExitPointerLock();
    }
}

void WindowWeb::showCursor(bool state)
{
    cursorSetShown(state ? "block" : "none");
}

void WindowWeb::setSize(u32 width, u32 height)
{
    windowResizeTo(width, height);
}

void WindowWeb::setFullscreen(bool state)
{
    state
        ? htmlRequestFullscreen()
        : htmlExitFullscreen();
}

bool WindowWeb::fullscreen() const
{
    return documentIsFullscreen();
}

bool WindowWeb::focused() const
{
    return documentHasFocus();
}

set<string> WindowWeb::vulkanExtensions() const
{
    return {
        "VK_KHR_surface"
    };
}
#endif
