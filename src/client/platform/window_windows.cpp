/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "window_windows.hpp"
#include "log.hpp"
#include "paths.hpp"
#include "constants.hpp"
#include "unicode_windows.hpp"
#include "../global.hpp"
#include "../image.hpp"
#include "../render_tools.hpp"
#include "../render_constants.hpp"

#include <winuser.h>
#include <wingdi.h>

WindowWindows* windowWindows = nullptr;

WindowWindows::WindowWindows(Controller* controller)
    : WindowInterface()
    , locked(false)
    , _focused(true)
{
    hInstance = GetModuleHandle(nullptr);

    if (!hInstance)
    {
        fatal("Failed to get current HINSTANCE in Windows window initialization: " + to_string(GetLastError()));
    }

    i32 screenWidth = GetSystemMetrics(SM_CXSCREEN);
    i32 screenHeight = GetSystemMetrics(SM_CYSCREEN);

    i32 width = g_config.fullscreen ? screenWidth : g_config.width;
    i32 height = g_config.fullscreen ? screenHeight : g_config.height;

    Image* customIconImage = loadPNG(resourcePath() + "/icon" + iconExt);

    HBITMAP iconColorBitmap = CreateBitmap(
        customIconImage->width(),
        customIconImage->height(),
        1,
        32,
        customIconImage->pixels()
    );

    if (!iconColorBitmap)
    {
        stringstream ss;
        ss << static_cast<void*>(iconColorBitmap);
        fatal("Failed to create Windows bitmap: " + ss.str());
    }

    auto iconMaskData = new u8[(customIconImage->width() * customIconImage->height()) / 8]{0};

    for (i32 y = 0; y < customIconImage->height(); y++)
    {
        for (i32 x = 0; x < customIconImage->width(); x++)
        {
            i32 index = y * customIconImage->width() + x;
            i32 byteIndex = index / 8;
            i32 bitIndex = index - (byteIndex * 8);

            iconMaskData[byteIndex] |= 1 << bitIndex;
        }
    }

    HBITMAP iconMaskBitmap = CreateBitmap(
        customIconImage->width(),
        customIconImage->height(),
        1,
        1,
        iconMaskData
    );

    delete[] iconMaskData;

    if (!iconMaskBitmap)
    {
        stringstream ss;
        ss << static_cast<void*>(iconMaskBitmap);
        fatal("Failed to create Windows bitmap: " + ss.str());
    }

    ICONINFO iconInfo{};
    iconInfo.hbmColor = iconColorBitmap;
    iconInfo.hbmMask = iconMaskBitmap;

    icon = CreateIconIndirect(&iconInfo);

    if (!icon)
    {
        fatal("Failed to create Windows window icon: " + to_string(GetLastError()));
    }

    DeleteObject(iconColorBitmap);
    DeleteObject(iconMaskBitmap);

    Image* customCursorImage = loadPNG(resourcePath() + "/cursor" + iconExt);

    HBITMAP cursorColorBitmap = CreateBitmap(
        customCursorImage->width(),
        customCursorImage->height(),
        1,
        32,
        customCursorImage->pixels()
    );

    if (!cursorColorBitmap)
    {
        stringstream ss;
        ss << static_cast<void*>(cursorColorBitmap);
        fatal("Failed to create Windows bitmap: " + ss.str());
    }

    auto cursorMaskData = new u8[(customCursorImage->width() * customCursorImage->height()) / 8]{0};

    for (i32 y = 0; y < customCursorImage->height(); y++)
    {
        for (i32 x = 0; x < customCursorImage->width(); x++)
        {
            i32 index = y * customCursorImage->width() + x;
            i32 byteIndex = index / 8;
            i32 bitIndex = index - (byteIndex * 8);

            cursorMaskData[byteIndex] |= 1 << bitIndex;
        }
    }

    HBITMAP cursorMaskBitmap = CreateBitmap(
        customCursorImage->width(),
        customCursorImage->height(),
        1,
        1,
        cursorMaskData
    );

    delete[] cursorMaskData;

    if (!cursorMaskBitmap)
    {
        stringstream ss;
        ss << static_cast<void*>(cursorMaskBitmap);
        fatal("Failed to create Windows bitmap: " + ss.str());
    }

    ICONINFO cursorInfo{};
    cursorInfo.fIcon = false;
    cursorInfo.xHotspot = 0;
    cursorInfo.yHotspot = 0;
    cursorInfo.hbmColor = cursorColorBitmap;
    cursorInfo.hbmMask = cursorMaskBitmap;

    cursor = CreateIconIndirect(&cursorInfo);

    if (!cursor)
    {
        fatal("Failed to create Windows window cursor: " + to_string(GetLastError()));
    }

    DeleteObject(cursorColorBitmap);
    DeleteObject(cursorMaskBitmap);

    WNDCLASSEXW windowClass{};
    windowClass.cbSize = sizeof(WNDCLASSEXW);
    windowClass.style = CS_OWNDC;
    windowClass.lpfnWndProc = handleWindowEvent;
    windowClass.hInstance = hInstance;
    windowClass.hIcon = icon;
    windowClass.hCursor = cursor;
    windowClass.hbrBackground = reinterpret_cast<HBRUSH>(COLOR_BACKGROUND + 1);
    windowClass.lpszClassName = utf8ToUTF16(appClass).c_str();
    windowClass.cbWndExtra = sizeof(WindowWindows*);

    _class = RegisterClassExW(&windowClass);

    hwnd = CreateWindowExW(
        getExtendedStyle(g_config.fullscreen),
        utf8ToUTF16(appClass).c_str(),
        utf8ToUTF16(gameName).c_str(),
        getStyle(g_config.fullscreen),
        (screenWidth - width) / 2,
        (screenHeight - height) / 2,
        width,
        height,
        nullptr,
        nullptr,
        hInstance,
        nullptr
    );

    if (!hwnd)
    {
        fatal("Failed to create Windows window: " + to_string(GetLastError()));
    }

    SetWindowLongPtr(hwnd, 0, reinterpret_cast<LONG_PTR>(this));

    _input = new FlatInputWindows(controller, this);

    windowWindows = this;
}

WindowWindows::~WindowWindows()
{
    windowWindows = nullptr;

    delete _input;

    DestroyWindow(hwnd);
    UnregisterClassW(utf8ToUTF16(appClass).c_str(), hInstance);
    DestroyIcon(cursor);
    DestroyIcon(icon);
}

InputInterface* WindowWindows::input() const
{
    return _input;
}

void WindowWindows::step()
{
    if (locked)
    {
        RECT rect;

        bool fail = GetWindowRect(hwnd, &rect);

        if (!fail)
        {
            error("Failed to get Windows window position: " + to_string(GetLastError()));
            return;
        }

        i32 width = rect.right - rect.left;
        i32 height = rect.bottom - rect.top;

        bool ok = SetCursorPos(
            rect.left + (width / 2),
            rect.top + (height / 2)
        );

        if (!ok)
        {
            error("Failed to warp Windows pointer: " + to_string(GetLastError()));
        }
    }
}

void WindowWindows::lockCursor(bool state)
{
    locked = state;
}

void WindowWindows::showCursor(bool state)
{
    ShowCursor(state);
}

void WindowWindows::setSize(u32 width, u32 height)
{
    i32 screenWidth = GetSystemMetrics(SM_CXSCREEN);
    i32 screenHeight = GetSystemMetrics(SM_CYSCREEN);

    bool ok = SetWindowPos(
        hwnd,
        HWND_TOP,
        (screenWidth - width) / 2,
        (screenHeight - height) / 2,
        width,
        height,
        SWP_NOZORDER
    );

    if (!ok)
    {
        error("Failed to set Windows window position: " + to_string(GetLastError()));
    }
}

void WindowWindows::setFullscreen(bool state)
{
    i32 flag = state ? SW_SHOWMAXIMIZED : SW_SHOWNORMAL;

    SetWindowLong(hwnd, GWL_STYLE, getStyle(state));
    SetWindowLong(hwnd, GWL_EXSTYLE, getExtendedStyle(state));
    ShowWindow(hwnd, flag);
}

bool WindowWindows::fullscreen() const
{
    i32 screenWidth = GetSystemMetrics(SM_CXSCREEN);
    i32 screenHeight = GetSystemMetrics(SM_CYSCREEN);

    RECT rect;

    bool fail = GetWindowRect(hwnd, &rect);

    if (!fail)
    {
        error("Failed to get Windows window position: " + to_string(GetLastError()));
        return false;
    }

    return
        rect.top == 0 &&
        rect.left == 0 &&
        rect.right - rect.left == screenWidth &&
        rect.bottom - rect.top == screenHeight;
}

bool WindowWindows::focused() const
{
    return _focused;
}

set<string> WindowWindows::vulkanExtensions() const
{
    static const set<string> extensionNames = {
        "VK_KHR_surface",
        "VK_KHR_win32_surface"
    };

    return extensionNames;
}

VkSurfaceKHR WindowWindows::createVulkanSurface(VkInstance instance) const
{
    VkSurfaceKHR surface;

    VkWin32SurfaceCreateInfoKHR createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_WIN32_SURFACE_CREATE_INFO_KHR;
    createInfo.hinstance = hInstance;
    createInfo.hwnd = hwnd;

    VkResult result = vkCreateWin32SurfaceKHR(instance, &createInfo, nullptr, &surface);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan surface: " + vulkanError(result));
    }

    return surface;
}

DWORD WindowWindows::getStyle(bool fullscreen) const
{
    if (fullscreen)
    {
        return WS_VISIBLE | WS_MAXIMIZE;
    }
    else
    {
        return WS_VISIBLE | WS_CAPTION | WS_MAXIMIZEBOX | WS_MINIMIZEBOX | WS_SIZEBOX;
    }
}

DWORD WindowWindows::getExtendedStyle(bool fullscreen) const
{
    return 0;
}

LRESULT WindowWindows::handleWindowEvent(HWND hwnd, u32 message, WPARAM wParam, LPARAM lParam)
{
    auto self = reinterpret_cast<WindowWindows*>(GetWindowLongPtr(hwnd, 0));

    switch (message)
    {
    case WM_GETMINMAXINFO :
    {
        auto info = reinterpret_cast<MINMAXINFO*>(lParam);

        info->ptMinTrackSize = { minWidth, minHeight };
        info->ptMaxTrackSize = { maxWidth, maxHeight };
        break;
    }
    default :
        self->_input->handle(hwnd, message, wParam, lParam);
    }

    return 0;
}
#endif
