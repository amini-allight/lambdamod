/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__APPLE__) && defined(TARGET_OS_MAC) && !defined(TARGET_OS_IPHONE)
#include "window_macos.hpp"

WindowMacOS::WindowMacOS(Controller* controller)
{


    _input = new FlatInputMacOS(controller, this);
}

WindowMacOS::~WindowMacOS()
{
    delete _input;


}

InputInterface* WindowMacOS::input() const
{
    return _input;
}

void WindowMacOS::step()
{

}

void WindowMacOS::lockCursor(bool state)
{

}

void WindowMacOS::showCursor(bool state)
{

}

void WindowMacOS::setSize(u32 width, u32 height)
{

}

void WindowMacOS::setFullscreen(bool state)
{

}

bool WindowMacOS::fullscreen() const
{

}

bool WindowMacOS::focused() const
{

}

set<string> WindowMacOS::vulkanExtensions() const
{

}

VkSurfaceKHR WindowMacOS::createVulkanSurface(VkInstance instance) const
{

}

#endif
