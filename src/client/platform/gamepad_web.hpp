/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#pragma once

#include "types.hpp"
#include "../gamepad_interface.hpp"

class GamepadWeb final : public GamepadInterface
{
public:
    GamepadWeb(const function<void(const Input&)>& inputCallback);
    ~GamepadWeb() override;

    bool hasGamepad() const override;

    void step() override;

    void haptic(const HapticEffect& effect) override;

private:
    function<void(const Input&)> inputCallback;
    i32 index;

    struct {
        bool dpadUp;
        bool dpadDown;
        bool dpadLeft;
        bool dpadRight;
        bool start;
        bool back;
        bool leftThumbstick;
        bool rightThumbstick;
        bool leftShoulder;
        bool rightShoulder;
        bool a;
        bool b;
        bool x;
        bool y;
        f64 leftTrigger;
        f64 rightTrigger;
        f64 leftStickX;
        f64 leftStickY;
        f64 rightStickX;
        f64 rightStickY;
    } states;

    void checkInput(InputType type, bool current, bool& previous) const;
};
#endif
