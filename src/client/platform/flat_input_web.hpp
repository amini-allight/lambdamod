/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__EMSCRIPTEN__)
#pragma once

#include "types.hpp"
#include "../input_interface.hpp"
#include "gamepad_web.hpp"

class Controller;
class WindowWeb;

class FlatInputWeb final : public InputInterface
{
public:
    FlatInputWeb(Controller* controller, WindowWeb* window);
    ~FlatInputWeb() override;

    bool step() override;

    void haptic(const HapticEffect& effect) override;

    bool hasGamepad() const override;
    bool hasTouch() const override;

private:
    Controller* controller;
    WindowWeb* window;
    GamepadWeb* gamepad;

    i32 currentX;
    i32 currentY;
    i32 lastX;
    i32 lastY;
    bool leftShift;
    bool leftControl;
    bool leftAlt;
    bool leftSuper;
    bool rightShift;
    bool rightControl;
    bool rightAlt;
    bool rightSuper;
    bool _quit;
    bool focused;
    u32 width;
    u32 height;
    vector<Input> inputs;

    void updateModifiers(InputType type, bool up);
    InputType identifyKey(const string& key) const;
    InputType identifyButton(i32 button) const;

public:
    void key(const string& key, bool up);
    void mouseButton(i32 button, bool up);
    void mouseMove(i32 x, i32 y);
    void mouseWheel(i32 x, i32 y);
    void touchStart(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force);
    void touchEnd(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force);
    void touchMove(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force);
    void touchCancel(i32 id, f64 pageX, f64 pageY, f64 radiusX, f64 radiusY, f64 rotationAngle, f64 force);
    void quit();
    void focus(bool state);
    void resize(i32 width, i32 height);
};
#endif
