/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__linux__) || defined(__FreeBSD__)
#include "touch_evdev.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "../global.hpp"

#include <sys/stat.h>
#include <fcntl.h>
#if defined(__linux__)
#include <linux/input.h>
#elif defined(__FreeBSD__)
#include <dev/evdev/input.h>
#endif

TouchEvdev::TouchEvdev(const function<void(const Input&)>& inputCallback)
    : inputCallback(inputCallback)
    , file(-1)
{
    if (!g_config.touchDevice.empty())
    {
        string path = "/dev/input/by-id/" + g_config.touchDevice;

        file = open(path.c_str(), O_RDWR | O_NONBLOCK);

        if (file < 0)
        {
            error("Failed to open touch device: " + to_string(errno));
        }

        log("Opened touch device '" + path + "'.");
    }
    else
    {
        for (const fs::directory_entry& entry : fs::directory_iterator("/dev/input/"))
        {
            if (!entry.path().stem().string().starts_with("event"))
            {
                continue;
            }

            if (!isTouchDevice(entry.path().string()))
            {
                continue;
            }

            file = open(entry.path().string().c_str(), O_RDWR | O_NONBLOCK);

            if (file < 0)
            {
                error("Failed to open touch device '" + entry.path().string() + "': " + to_string(errno));
                continue;
            }

            log("Opened touch device '" + entry.path().string() + "'.");
            break;
        }
    }
    
    if (file < 0)
    {
        log("No touch device detected.");
        return;
    }

    touchMax.x = getMaximum(ABS_MT_POSITION_X);
    touchMax.y = getMaximum(ABS_MT_POSITION_Y);
    pressureMax = getMaximum(ABS_MT_PRESSURE);
}

TouchEvdev::~TouchEvdev()
{
    if (hasTouch())
    {
        close(file);
    }
}

bool TouchEvdev::hasTouch() const
{
    return file >= 0;
}

void TouchEvdev::step()
{
    if (!hasTouch())
    {
        return;
    }

    vector<Input> inputs;

    while (true)
    {
        input_event event;

        ssize_t result = read(file, &event, sizeof(input_event));

        if (result != sizeof(input_event) && errno == EAGAIN)
        {
            break;
        }

        if (result != sizeof(input_event))
        {
            error("Failed to read touch event: " + to_string(errno));
            break;
        }

        if (event.type == 0)
        {
            break;
        }

        if (event.code == ABS_MT_SLOT)
        {
            Input input;
            input.type = Input_Touch;
            input.touch.id = event.value;

            inputs.push_back(input);
        }

        Input& input = inputs.back();

        switch (event.code)
        {
        default :
            error("Encountered unknown touch input code received: " + to_string(event.code));
            break;
        case ABS_MT_TRACKING_ID :
            input.up = event.value < 0;
            break;
        case ABS_MT_POSITION_X :
            input.position.x = event.value / touchMax.x;
            break;
        case ABS_MT_POSITION_Y :
            input.position.y = event.value / touchMax.y;
            break;
        case ABS_MT_TOUCH_MAJOR :
            input.touch.major = event.value / touchMax.x;
            break;
        case ABS_MT_TOUCH_MINOR :
            input.touch.minor = event.value / touchMax.x;
            break;
        case ABS_MT_ORIENTATION :
            input.touch.orientation = radians(event.value);
            break;
        case ABS_MT_PRESSURE :
            input.touch.force = event.value / pressureMax;
            break;
        } 
    }

    for (const Input& input : inputs)
    {
        inputCallback(input);
    }
}

i32 TouchEvdev::getMaximum(i32 index) const
{
    input_absinfo parameters;

    i32 result = ioctl(file, EVIOCGABS(index), &parameters);

    if (result < 0)
    {
        error("Failed to read touch device limits: " + to_string(errno));
        return 1;
    }

    return parameters.maximum;
}

bool TouchEvdev::isTouchDevice(const string& path) const
{
    i32 result;

    int file = open(path.c_str(), O_RDWR | O_NONBLOCK);

    if (file < 0)
    {
        warning("Failed to open potential touch device '" + path + "': " + to_string(errno));
        return false;
    }

    u64 eventBits = 0;
    result = ioctl(file, EVIOCGBIT(0, sizeof(eventBits)), &eventBits);

    if (result != sizeof(eventBits))
    {
        error("Failed to evaluate potential touch device '" + path + "': " + to_string(errno));
        close(file);
        return false;
    }

    if (!(eventBits & (1 << EV_KEY)) || !(eventBits & (1 << EV_ABS)))
    {
        close(file);
        return false;
    }

    vector<u8> absBits(ABS_MAX / 8, 0);
    result = ioctl(file, EVIOCGBIT(EV_ABS, ABS_MAX), absBits.data());

    if (result < 0)
    {
        error("Failed to evaluate potential touch device '" + path + "': " + to_string(errno));
        close(file);
        return false;
    }

    if (!(absBits.data()[ABS_MT_SLOT / 8] & (1 << (ABS_MT_SLOT % 8))))
    {
        close(file);
        return false;
    }

    close(file);
    return true;
}
#endif
