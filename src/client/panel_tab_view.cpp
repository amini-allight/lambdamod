/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_tab_view.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "tab_pane.hpp"

PanelTabView::PanelTabView(InterfaceWidget* parent, const vector<string>& tabNames, bool vertical)
    : TabView(parent, tabNames, vertical)
{
    // tab panes must be destroyed and re-created because their input scopes reference the input scope we're about to delete and replace
    clearChildren();

    replaceScope<WidgetInputScope>(
        this,
        "panel-tab-view",
        Input_Scope_Normal,
        [](const Input& input) -> bool { return true; },
        [&](InputScope* parent) -> void {
            WIDGET_SLOT("previous-tab", previousTab)
            WIDGET_SLOT("next-tab", nextTab)

            START_SCOPE("tab-view-bar", tabBarFocused())
                WIDGET_SLOT("interact", interact)
            END_SCOPE
        }
    );

    for (size_t i = 0; i < tabNames.size(); i++)
    {
        new TabPane(this, i);
    }
}

void PanelTabView::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        Point offset = vertical ? Point(UIScale::panelTabSize(this).w, 0) : Point(0, UIScale::panelTabSize(this).h);

        child->move(position + offset);
    }
}

void PanelTabView::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        Size offset = vertical ? Size(UIScale::panelTabSize(this).w, 0) : Size(0, UIScale::panelTabSize(this).h);

        child->resize(size - offset);
    }
}

void PanelTabView::draw(const DrawContext& ctx) const
{
    size_t i = 0;
    for (const string& tabName : tabNames)
    {
        Point point = vertical ? Point(0, i * UIScale::panelTabSize(this).h) : Point(i * UIScale::panelTabSize(this).w, 0);
        Color color = i != index && focused() && contains(point, UIScale::panelTabSize(this), pointer)
            ? theme.accentColor
            : theme.panelForegroundColor;

        if (i == index)
        {
            drawSolid(
                ctx,
                this,
                point,
                UIScale::panelTabSize(this),
                theme.worldBackgroundColor
            );

            drawBorder(
                ctx,
                this,
                point,
                UIScale::panelTabSize(this),
                UIScale::panelBorderSize(this),
                color
            );
        }

        TextStyle style(this);
        style.alignment = Text_Center;
        style.size = UIScale::largeFontSize(this);
        style.weight = Text_Bold;
        style.color = color;

        drawText(
            ctx,
            this,
            point,
            UIScale::panelTabSize(this),
            tabName,
            style
        );

        i++;
    }

    children()[index]->draw(ctx);
}

void PanelTabView::interact(const Input& input)
{
    Point local = pointer - position();

    closeTab();
    index = vertical ? local.y / UIScale::panelTabSize(this).h : local.x / UIScale::panelTabSize(this).w;
    if (index >= tabNames.size())
    {
        index = 0;
    }
    openTab();
}

bool PanelTabView::tabBarFocused() const
{
    return vertical
        ? (pointer - position()).x < UIScale::panelTabSize(this).w
        : (pointer - position()).y < UIScale::panelTabSize(this).h;
}

SizeProperties PanelTabView::sizeProperties() const
{
    return sizePropertiesFillAll;
}
