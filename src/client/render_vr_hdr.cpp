/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_hdr.hpp"
#include "render_tools.hpp"
#include "render_descriptor_set_write_components.hpp"
#include "theme.hpp"
#include "global.hpp"

RenderVRHDR::RenderVRHDR(
    const RenderContext* ctx,
    RenderVRPipelineStore* pipelineStore,
    vector<VRSwapchainElement*> swapchainElements,
    f64 exposure,
    f64 gamma
)
    : RenderVRPostProcessEffect(
        ctx,
        pipelineStore->hdr.pipelineLayout,
        pipelineStore->hdr.pipeline
    )
    , exposure(exposure)
    , gamma(gamma)
{
    for (size_t i = 0; i < swapchainElements.size(); i++)
    {
        auto component = new RenderPostProcessEffectComponent(ctx, pipelineStore->hdrDescriptorSetLayout, createUniformBuffer());

        setDescriptorSet(
            component->descriptorSet,
            component->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->stencilImageView()
        );

        components.push_back(component);
    }
}

RenderVRHDR::~RenderVRHDR()
{
    destroyComponents();
}

void RenderVRHDR::refresh(vector<VRSwapchainElement*> swapchainElements)
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        setDescriptorSet(
            components.at(i)->descriptorSet,
            components.at(i)->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->stencilImageView()
        );
    }
}

void RenderVRHDR::update(f64 exposure, f64 gamma)
{
    this->exposure = exposure;
    this->gamma = gamma;
}

VmaBuffer RenderVRHDR::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(fvec4) + sizeof(f32) + sizeof(f32) + sizeof(i32),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderVRHDR::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    fvec4 color = theme.worldBackgroundColor.toVec4();

    memcpy(mapping->data, &color, sizeof(fvec4));
    memcpy(mapping->data + 4, &exposure, sizeof(f32));
    memcpy(mapping->data + 4 + 1, &gamma, sizeof(f32));
    *reinterpret_cast<i32*>(mapping->data + 4 + 1 + 1) = g_config.vrBloom;
}

void RenderVRHDR::setDescriptorSet(
    VkDescriptorSet descriptorSet,
    VkBuffer buffer,
    VkImageView colorImageView,
    VkImageView stencilImageView
) const
{
    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer, &parametersWriteBuffer, &parametersWrite);

    VkDescriptorImageInfo colorImageWriteImage{};
    VkWriteDescriptorSet colorImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, colorImageView, ctx->unfilteredSampler, &colorImageWriteImage, &colorImageWrite);

    VkDescriptorImageInfo stencilImageWriteImage{};
    VkWriteDescriptorSet stencilImageWrite{};

    storageImageDescriptorSetWrite(descriptorSet, 2, stencilImageView, &stencilImageWriteImage, &stencilImageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        parametersWrite,
        colorImageWrite,
        stencilImageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}
#endif
