/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "edit_mode_context.hpp"
#include "entity_copy_buffer.hpp"
#include "sample_copy_buffer.hpp"
#include "action_copy_buffer.hpp"

class ViewerModeContext;

class EntityModeContext final : public EditModeContext
{
public:
    EntityModeContext(ViewerModeContext* ctx);
    ~EntityModeContext() override;

    void enter() override;
    void exit() override;

    void addNewPrefab(const string& name) override;
    void addNew() override;
    void addNew(const string& name);
    void removeSelected() override;

    void addChildren(const Point& point) override;
    void removeParent() override;

    void hideSelected() override;
    void hideUnselected() override;
    void unhideAll() override;
    const set<EntityID>& hiddenEntityIDs() const;

    set<EntityID> selectionIDs() const;
    set<EntityID> unselectionIDs() const;
    set<Entity*> selection() const;
    set<EntityID> selectionRootIDsOnly() const;
    set<Entity*> selectionRootsOnly() const;
    Entity* selectionRootAtIndex(size_t index) const;
    vec3 selectionCenter() const override;
    void select(Entity* entity);
    void select(const vector<Entity*>& entities);
    void selectAll() override;
    void deselect(Entity* entity);
    void deselect(const vector<Entity*>& entities);
    void deselectAll() override;
    bool hasSelection() const override;
    void invertSelection() override;

    void translate(const Point& point, bool soft = false) override;
    void rotate(const Point& point, bool soft = false) override;
    void scale(const Point& point, bool soft = false) override;
    void store();
    void reset();
    void resetPosition() override;
    void resetRotation() override;
    void resetScale() override;
    void resetLinearVelocity() override;
    void resetAngularVelocity() override;

    void selectionToCursor() override;

    void duplicate() override;
    void cut() override;
    void copy() override;
    void paste() override;

    void extrude() override;
    void split() override;
    void join(const Point& point) override;

    void originTo3DCursor();
    void originToBody();
    void bodyToOrigin();

    void openEditor(EntityID id);
    vector<Entity*> entitiesUnderPoint(const Point& point) const;
    vector<Entity*> entitiesInRegion(const Point& start, const Point& end) const;

    SampleCopyBuffer& sampleCopyBuffer();
    ActionCopyBuffer& actionCopyBuffer();

private:
    EntityCopyBuffer copyBuffer;
    SampleCopyBuffer _sampleCopyBuffer;
    ActionCopyBuffer _actionCopyBuffer;
    set<EntityID> _hiddenEntityIDs;

    set<EntityID> allIDs() const;
    void selectNew(const vector<Entity*>& newEntities);
    void selectNew(const set<EntityID>& previousIDs);

    bool gizmoShown() const override;
    bool gizmoSelectable(const Point& point) const override;

    void makeSelections(const Point& point, SelectMode mode) override;
    void makeSelections(const Point& start, const Point& end, SelectMode mode) override;

    void transformSet(size_t i, const vec3& pivot, const mat4& initial, const mat4& transform) override;
    mat4 transformGet(size_t i) const override;
    size_t transformCount() const override;
    mat4 transformToolSpace() const override;
};
