/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_device_interface.hpp"
#include "log.hpp"
#include "global.hpp"

SoundDeviceInterface::SoundDeviceInterface()
    : _outputMuted(g_config.outputMuted)
    , _outputVolume(g_config.outputVolume)
    , _inputMuted(g_config.inputMuted)
    , _inputVolume(g_config.inputVolume)
{

}

SoundDeviceInterface::~SoundDeviceInterface()
{

}

void SoundDeviceInterface::write(const i16* data, u32 count)
{
    i16* volumeAdjusted = new i16[count];

    for (u32 i = 0; i < count; i++)
    {
        volumeAdjusted[i] = data[i] * _outputVolume * !_outputMuted;
    }

    outputBuffer.push(data, count);

    delete[] volumeAdjusted;
}

void SoundDeviceInterface::read(i16* data, u32* count)
{
    *count = inputBuffer.pop(data, *count);

    for (u32 i = 0; i < *count; i++)
    {
        data[i] *= _inputVolume * !_inputMuted;
    }
}

void SoundDeviceInterface::setOutputMuted(bool state)
{
    _outputMuted = state;
}

void SoundDeviceInterface::setOutputVolume(f64 volume)
{
    _outputVolume = volume;
}

void SoundDeviceInterface::setInputMuted(bool state)
{
    _inputMuted = state;
}

void SoundDeviceInterface::setInputVolume(f64 volume)
{
    _inputVolume = volume;
}

bool SoundDeviceInterface::outputMuted() const
{
    return _outputMuted;
}

f64 SoundDeviceInterface::outputVolume() const
{
    return _outputVolume;
}

bool SoundDeviceInterface::inputMuted() const
{
    return _inputMuted;
}

f64 SoundDeviceInterface::inputVolume() const
{
    return _inputVolume;
}
