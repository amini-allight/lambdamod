/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "ufloat_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "centered_line_edit.hpp"
#include "tools.hpp"

UFloatEdit::UFloatEdit(
    InterfaceWidget* parent,
    const function<f64()>& source,
    const function<void(const f64&)>& onSet
)
    : InterfaceWidget(parent)
{
    f = new CenteredLineEdit(
        this,
        source ? [source]() -> string
        {
            return toPrettyString(source());
        } : function<string()>(nullptr),
        [onSet](const string& s) -> void
        {
            try
            {
                f64 v = stod(s);

                if (v < 0)
                {
                    throw invalid_argument("UFloatEdit");
                }

                onSet(v);
            }
            catch (const exception& e)
            {

            }
        }
    );
}

void UFloatEdit::set(f64 value)
{
    f->set(toPrettyString(max<f64>(value, 0)));
}

f64 UFloatEdit::value() const
{
    try
    {
        f64 v = stod(f->content());

        if (v < 0)
        {
            throw invalid_argument("UFloatEdit");
        }

        return v;
    }
    catch (const exception& e)
    {
        return 0;
    }
}

void UFloatEdit::clear()
{
    f->clear();
}

void UFloatEdit::setPlaceholder(const string& text)
{
    f->setPlaceholder(text);
}

SizeProperties UFloatEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
