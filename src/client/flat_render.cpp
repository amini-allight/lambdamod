/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "flat_render.hpp"

#include "tools.hpp"
#include "paths.hpp"
#include "global.hpp"
#include "config.hpp"
#include "theme.hpp"

#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_objects.hpp"
#include "render_flat_hdr.hpp"

#include "controller.hpp"
#include "control_context.hpp"
#include "interface.hpp"
#include "panel_transforms.hpp"

#include "render_flat_chromatic_aberration_effect.hpp"
#include "render_flat_depth_of_field_effect.hpp"
#include "render_flat_motion_blur_effect.hpp"

FlatRender::FlatRender(Controller* controller, RenderContext* ctx)
    : RenderInterface()
    , controller(controller)
    , ctx(ctx)
    , _width(g_config.width)
    , _height(g_config.height)
    , format(VK_FORMAT_UNDEFINED)
    , surface(nullptr)
    , swapchain(nullptr)
    , _panelRenderPass(nullptr)
    , skyRenderPass(nullptr)
    , mainRenderPass(nullptr)
    , hdrRenderPass(nullptr)
    , postProcessRenderPass(nullptr)
    , overlayRenderPass(nullptr)
    , independentDepthOverlayRenderPass(nullptr)
    , _uiRenderPass(nullptr)
    , pipelineStore(nullptr)
    , hdr(nullptr)
    , sky(nullptr)
    , atmosphere(nullptr)
    , nextDecorationID(1)
    , nextPostProcessEffectID(1)
    , cameraSize(1)
    , cameraAngle(defaultCameraAngle)
    , cameraOrthographic(false)
    , cameraPosition(2, 2, 2)
    , cameraRotation(
        vec3(-1, -1, -1).normalize(),
        vec3(-1, -1, -1).cross(vec3(-1, -1, -1).normalize().cross(upDir).normalize()).normalize()
    )
    , currentFrame(0)
    , imageIndex(0)
    , exposure(1)
    , lastWriteTime(currentTime<chrono::microseconds>())
    , _timer(0)
    , _rayTracing(false)
{
    log("Initializing render system...");

    if (!ctx->initialized())
    {
        ctx->init(
            bind(&FlatRender::pickAPIVersion, this),
            bind(&FlatRender::instanceExtensions, this),
            bind(&FlatRender::pickPhysicalDevice, this),
            bind(&FlatRender::prepareSurface, this),
            bind(&FlatRender::deviceExtensions, this),
            bind(&FlatRender::prepareSwapchain, this)
        );
    }

    _rayTracing = ctx->rayTracingEnabled();

    createPanelRenderPass(this, _panelRenderPass);
    createSkyRenderPass(this, skyRenderPass);

    if (g_config.antiAliasingLevel == 1)
    {
        createMonosampleMainRenderPass(this, mainRenderPass);
        createMonosampleHDRRenderPass(this, hdrRenderPass);
        createMonosamplePostProcessRenderPass(this, postProcessRenderPass);
        createMonosampleOverlayRenderPass(this, overlayRenderPass);
        createMonosampleIndependentDepthOverlayRenderPass(this, independentDepthOverlayRenderPass);
        createUIRenderPass(this, _uiRenderPass);
    }
    else
    {
        VkSampleCountFlagBits sampleCount = pickMSAALevel(g_config.antiAliasingLevel);

        createMultisampleMainRenderPass(this, sampleCount, mainRenderPass);
        createMultisampleHDRRenderPass(this, sampleCount, hdrRenderPass);
        createMultisamplePostProcessRenderPass(this, sampleCount, postProcessRenderPass);
        createMultisampleOverlayRenderPass(this, sampleCount, overlayRenderPass);
        createMultisampleIndependentDepthOverlayRenderPass(this, sampleCount, independentDepthOverlayRenderPass);
        createUIRenderPass(this, _uiRenderPass);
    }

    pipelineStore = new RenderFlatPipelineStore(
        ctx,
        skyRenderPass,
        mainRenderPass,
        hdrRenderPass,
        postProcessRenderPass,
        overlayRenderPass,
        independentDepthOverlayRenderPass,
        pickMSAALevel(g_config.antiAliasingLevel)
    );

    createSwapchainElements();

    hdr = new RenderFlatHDR(
        ctx,
        pipelineStore,
        swapchainElements,
        exposure,
        hdrGamma
    );

    sky = new RenderFlatSky(
        ctx,

        swapchainElements,

        pipelineStore->skyDescriptorSetLayout,
        pipelineStore
    );

    atmosphere = new RenderFlatAtmosphere(
        this,

        swapchainElements,

        pipelineStore->atmosphereDescriptorSetLayout,
        pipelineStore
    );

    RenderPostProcessEffectID motionBlurID = addPostProcessEffect<RenderFlatMotionBlurEffect>(motionBlurStrength);
    setPostProcessEffectActive(motionBlurID, g_config.flatMotionBlur);
    RenderPostProcessEffectID depthOfFieldID = addPostProcessEffect<RenderFlatDepthOfFieldEffect>(depthOfFieldRange);
    setPostProcessEffectActive(depthOfFieldID, g_config.flatDepthOfField);
    RenderPostProcessEffectID chromaticAberrationID = addPostProcessEffect<RenderFlatChromaticAberrationEffect>(chromaticAberrationStrength);
    setPostProcessEffectActive(chromaticAberrationID, g_config.flatChromaticAberration);

    postProcessOrder = { motionBlurID, depthOfFieldID, chromaticAberrationID };

    log("Render system initialized.");
}

FlatRender::~FlatRender()
{
    log("Shutting down render system...");

    waitIdle();

    for (const auto& [ id, entity ] : entities)
    {
        delete entity;
    }
    entities.clear();

    for (const RenderDeletionTask<RenderFlatEntity>& task : entitiesToDelete)
    {
        task.destroy();
    }
    entitiesToDelete.clear();

    for (const auto& [ id, decoration ] : decorations)
    {
        delete decoration;
    }
    decorations.clear();

    for (const RenderDeletionTask<RenderFlatDecoration>& task : decorationsToDelete)
    {
        task.destroy();
    }
    decorationsToDelete.clear();

    for (const auto& [ id, postProcessEffect ] : postProcessEffects)
    {
        delete postProcessEffect;
    }
    postProcessEffects.clear();

    for (const RenderDeletionTask<RenderFlatPostProcessEffect>& task : postProcessEffectsToDelete)
    {
        task.destroy();
    }
    postProcessEffectsToDelete.clear();

    delete atmosphere;
    delete sky;
    delete hdr;

    destroySwapchainElements();
    destroySwapchain();

    delete pipelineStore;

    destroyRenderPass(ctx->device, _uiRenderPass);
    destroyRenderPass(ctx->device, independentDepthOverlayRenderPass);
    destroyRenderPass(ctx->device, overlayRenderPass);
    destroyRenderPass(ctx->device, postProcessRenderPass);
    destroyRenderPass(ctx->device, hdrRenderPass);
    destroyRenderPass(ctx->device, mainRenderPass);
    destroyRenderPass(ctx->device, skyRenderPass);
    destroyRenderPass(ctx->device, _panelRenderPass);

    vkDestroySurfaceKHR(ctx->instance, surface, nullptr);

    log("Render system shut down.");
}

void FlatRender::waitIdle() const
{
    VkResult result;

    result = vkDeviceWaitIdle(ctx->device);

    if (result != VK_SUCCESS)
    {
        error("Failed to wait for device to idle: " + vulkanError(result));
    }
}

void FlatRender::step()
{
    VkResult result;

    chrono::microseconds elapsed = currentTime<chrono::microseconds>() - lastWriteTime;
    chrono::microseconds minFrameInterval(static_cast<u64>(1'000'000.0 / g_config.frameLimit));

    if (g_config.frameLimit != 0 && elapsed < minFrameInterval)
    {
        return;
    }

    if (g_config.frameLimit != 0)
    {
        lastWriteTime += minFrameInterval;
    }
    else
    {
        lastWriteTime = currentTime<chrono::microseconds>();
    }

    _timer += elapsed.count() / 1'000'000.0;

    const FlatSwapchainElement* currentElement = swapchainElements[currentFrame];

    // Wait
    result = vkWaitForFences(ctx->device, 1, &currentElement->fence(), true, numeric_limits<u64>::max());

    if (result != VK_SUCCESS)
    {
        error("Failed to wait for fences: " + vulkanError(result));
    }

    // Acquire
    // We do this because otherwise window managers will drive our frame rate very low, potentially driving audio skips and disconnection
    result = vkAcquireNextImageKHR(
        ctx->device,
        swapchain,
        chrono::duration_cast<chrono::nanoseconds>(maxSwapchainAcquireDelay).count(), // Timeout
        currentElement->start(),
        nullptr,
        &imageIndex
    );

    if (result == VK_TIMEOUT)
    {
        return;
    }
    else if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    {
        reinit(_width, _height);
        return;
    }
    else if (result < 0)
    {
        error("Failed to acquire next image: " + vulkanError(result));
        return;
    }

    FlatSwapchainElement* swapchainElement = swapchainElements.at(imageIndex);

    VkCommandBuffer transferCommandBuffer = swapchainElement->transferCommandBuffer();
    VkCommandBuffer commandBuffer = swapchainElement->commandBuffer();

    f32 luminance = -1;

    if (swapchainElement->luminanceReady())
    {
        luminance = getLuminance(swapchainElement);
    }

    // Wait
    if (swapchainElement->lastFence() != nullptr)
    {
        result = vkWaitForFences(ctx->device, 1, &swapchainElement->lastFence(), VK_TRUE, UINT64_MAX);

        if (result != VK_SUCCESS)
        {
            error("Failed to wait for fences: " + vulkanError(result));
        }
    }
    swapchainElement->lastFence() = currentElement->fence();

    // Reset
    result = vkResetFences(ctx->device, 1, &currentElement->fence());

    if (result != VK_SUCCESS)
    {
        error("Failed to reset fences: " + vulkanError(result));
    }

    // This is done here instead of in the controller in order to wait for frame sync
    controller->context()->interface()->step();

    // Update various camera-related uniforms
    dynamic_cast<RenderFlatHDR*>(hdr)->update(exposure, hdrGamma);
    updateCameraUniforms();
    updateTextTitlecardUniforms(cameraRotation, cameraSpace);
    updateTextUniforms(cameraRotation, cameraSpace);

    // Begin
    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(transferCommandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    renderSky(swapchainElement);

    vector<VkClearValue> clearValues;

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = mainRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->mainFramebuffer();
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { _width, _height }
    };

    // Main
    if (_rayTracing)
    {
        swapchainElement->hardwareOcclusionScene()->update(commandBuffer, cameraPosition, entities);
    }
    else
    {
        swapchainElement->softwareOcclusionScene()->update(cameraPosition, entities);
    }

    if (g_config.antiAliasingLevel == 1)
    {
        clearValues = fillClearValues({ Clear_Value_Color, Clear_Value_Depth, Clear_Value_Stencil });
    }
    else
    {
        clearValues = fillClearValues({ Clear_Value_Color, Clear_Value_Depth, Clear_Value_Stencil, Clear_Value_Color, Clear_Value_Depth, Clear_Value_Stencil });
    }
    beginRenderPassInfo.clearValueCount = clearValues.size();
    beginRenderPassInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    if (controller->selfWorld() && !cameraOrthographic)
    {
        sky->work(
            swapchainElement,
            controller->selfWorld()->sky(cameraPosition),
            controller->selfWorld()->up(cameraPosition)
        );

        atmosphere->work(
            swapchainElement,
            controller->selfWorld()->atmosphere(cameraPosition),
            cameraPosition,
            controller->selfWorld()->up(cameraPosition),
            controller->selfWorld()->gravity(cameraPosition),
            controller->selfWorld()->flowVelocity(cameraPosition),
            controller->selfWorld()->density(cameraPosition)
        );
    }

    deque<RenderTransparentDraw> transparentDraws;

    for (auto& [ id, entity ] : entities)
    {
        entity->work(
            swapchainElement,
            cameraPosition,
            &transparentDraws,
            controller->context()->viewerMode()->entityMode()->hiddenEntityIDs()
        );
    }

    sort(
        transparentDraws.begin(),
        transparentDraws.end(),
        [this](const RenderTransparentDraw& a, const RenderTransparentDraw& b) -> bool
        {
            return a.position().distance(cameraPosition) > b.position().distance(cameraPosition);
        }
    );

    for (const RenderTransparentDraw& transparentDraw : transparentDraws)
    {
        transparentDraw.draw();
    }

    vkCmdEndRenderPass(commandBuffer);

    // Luminance computation for next frame
    computeLuminance(swapchainElement);

    // Clear value because multisampled must clear when doing post-process/HDR passes
    if (g_config.antiAliasingLevel == 1)
    {
        beginRenderPassInfo.clearValueCount = 0;
        beginRenderPassInfo.pClearValues = nullptr;
    }
    else
    {
        clearValues = fillClearValues({ Clear_Value_Color });
        beginRenderPassInfo.clearValueCount = clearValues.size();
        beginRenderPassInfo.pClearValues = clearValues.data();
    }

    // HDR
    copyImage(
        commandBuffer,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        _width,
        _height,
        VK_IMAGE_ASPECT_COLOR_BIT,
        1,

        swapchainElement->colorImage(),
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,

        swapchainElement->sideImage(),
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    beginRenderPassInfo.renderPass = hdrRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->hdrFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    hdr->work(swapchainElement, _timer);

    vkCmdEndRenderPass(commandBuffer);

    // Post process
    for (RenderPostProcessEffectID id : postProcessOrder)
    {
        RenderFlatPostProcessEffect* postProcessEffect = postProcessEffects.at(id);

        if (!postProcessEffect->shown())
        {
            continue;
        }

        copyImage(
            commandBuffer,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            _width,
            _height,
            VK_IMAGE_ASPECT_COLOR_BIT,
            1,

            swapchainElement->colorImage(),
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,

            swapchainElement->sideImage(),
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );

        beginRenderPassInfo.renderPass = postProcessRenderPass;
        beginRenderPassInfo.framebuffer = swapchainElement->postProcessFramebuffer();
        vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

        postProcessEffect->work(swapchainElement, _timer);

        vkCmdEndRenderPass(commandBuffer);
    }

    // Overlay
    beginRenderPassInfo.clearValueCount = 0;
    beginRenderPassInfo.pClearValues = nullptr;

    beginRenderPassInfo.renderPass = overlayRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->overlayFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (const auto& [ id, decoration ] : decorations)
    {
        if (!decoration->shown() || decoration->drawType() != Render_Decoration_Default)
        {
            continue;
        }

        decoration->work(swapchainElement, cameraPosition);
    }

    if (!cameraOrthographic)
    {
        renderTextTitlecard(commandBuffer);

        renderText(commandBuffer);
    }

    for (const auto& [ id, decoration ] : decorations)
    {
        if (!decoration->shown() || decoration->drawType() != Render_Decoration_Over_Panels)
        {
            continue;
        }

        decoration->work(swapchainElement, cameraPosition);
    }

    vkCmdEndRenderPass(commandBuffer);

    // Overlay independent depth
    clearValues = fillClearValues({ Clear_Value_Color, Clear_Value_Depth });

    beginRenderPassInfo.clearValueCount = clearValues.size();
    beginRenderPassInfo.pClearValues = clearValues.data();

    beginRenderPassInfo.renderPass = independentDepthOverlayRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->independentDepthOverlayFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    for (const auto& [ id, decoration ] : decorations)
    {
        if (!decoration->shown() || decoration->drawType() != Render_Decoration_Independent_Depth)
        {
            continue;
        }

        decoration->work(swapchainElement, cameraPosition);
    }

    vkCmdEndRenderPass(commandBuffer);

    // UI
    beginRenderPassInfo.renderPass = _uiRenderPass;
    beginRenderPassInfo.framebuffer = swapchainElement->uiFramebuffer();
    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_SECONDARY_COMMAND_BUFFERS);

    VkCommandBuffer uiBuffer = controller->context()->interface()->uiBuffer();
    vkCmdExecuteCommands(commandBuffer, 1, &uiBuffer);

    vkCmdEndRenderPass(commandBuffer);

    // Output
    outputImage(commandBuffer, swapchainElement->colorImage(), swapchainElement->outputImage());

    // End
    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }

    result = vkEndCommandBuffer(transferCommandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }

    // Submit
    vector<VkCommandBuffer> commandBuffers = controller->context()->interface()->panelBuffers();
    commandBuffers.push_back(transferCommandBuffer);
    commandBuffers.push_back(commandBuffer);

    const VkPipelineStageFlags waitStage = VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT;

    VkSubmitInfo submitInfo{};
    submitInfo.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submitInfo.waitSemaphoreCount = 1;
    submitInfo.pWaitSemaphores = &currentElement->start();
    submitInfo.pWaitDstStageMask = &waitStage;
    submitInfo.commandBufferCount = commandBuffers.size();
    submitInfo.pCommandBuffers = commandBuffers.data();
    submitInfo.signalSemaphoreCount = 1;
    submitInfo.pSignalSemaphores = &currentElement->end();

    result = vkQueueSubmit(ctx->graphicsQueue, 1, &submitInfo, currentElement->fence());

    if (result != VK_SUCCESS)
    {
        error("Failed to submit command buffers: " + vulkanError(result));
    }

    // Present
    VkPresentInfoKHR presentInfo{};
    presentInfo.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    presentInfo.waitSemaphoreCount = 1;
    presentInfo.pWaitSemaphores = &currentElement->end();
    presentInfo.swapchainCount = 1;
    presentInfo.pSwapchains = &swapchain;
    presentInfo.pImageIndices = &imageIndex;

    result = vkQueuePresentKHR(ctx->presentQueue, &presentInfo);

    if (result == VK_ERROR_OUT_OF_DATE_KHR || result == VK_SUBOPTIMAL_KHR)
    {
        // Do nothing
    }
    else if (result < 0)
    {
        error("Failed to present swapchain image: " + vulkanError(result));
    }

    advanceDeletionQueues();

    if (luminance >= 0)
    {
        adjustExposure(elapsed, luminance);
    }

    currentFrame = (currentFrame + 1) % swapchainElements.size();
}

tuple<u32, u32> FlatRender::reinit(u32 width, u32 height)
{
    waitIdle();

    destroySwapchainElements();
    destroySwapchain();

    VkSurfaceCapabilitiesKHR surfaceCapabilities{};

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(ctx->physDevice, surface, &surfaceCapabilities);

    if (surfaceCapabilities.currentExtent.width >= surfaceCapabilities.minImageExtent.width &&
        surfaceCapabilities.currentExtent.height >= surfaceCapabilities.minImageExtent.height &&
        surfaceCapabilities.currentExtent.width <= surfaceCapabilities.maxImageExtent.width &&
        surfaceCapabilities.currentExtent.height <= surfaceCapabilities.maxImageExtent.height
    )
    {
        width = surfaceCapabilities.currentExtent.width;
        height = surfaceCapabilities.currentExtent.height;
    }

    _width = width;
    _height = height;
    // For UI scaling
    g_windowHeight = _height;

    if (_width == 0 || _height == 0)
    {
        fatal("Swapchain cannot be recreated with size of 0.");
    }

    createSwapchain();
    createSwapchainElements();

    hdr->refresh(swapchainElements);

    sky->refresh(swapchainElements);

    atmosphere->refresh(swapchainElements);

    for (const auto& [ id, entity ] : entities)
    {
        entity->refresh(swapchainElements);
    }

    for (const auto& [ id, decoration ] : decorations)
    {
        decoration->refresh(swapchainElements);
    }

    for (const auto& [ id, postProcessEffect ] : postProcessEffects)
    {
        postProcessEffect->refresh(swapchainElements);
    }

    currentFrame = 0;
    imageIndex = 0;

    return { width, height };
}

void FlatRender::setCameraSpace(const quaternion& space)
{
    cameraSpace = space;
}

void FlatRender::setCameraPerspective()
{
    cameraOrthographic = false;
}

void FlatRender::setCameraOrthographic(f64 size)
{
    cameraOrthographic = true;
    cameraSize = size;
}

void FlatRender::setCamera(const vec3& position, const quaternion& rotation)
{
    cameraPosition = position;
    cameraRotation = rotation;
}

void FlatRender::setCameraAngle(f64 angle)
{
    cameraAngle = angle;
}

void FlatRender::add(EntityID parentID, const Entity* entity)
{
    BodyPartMask bodyPartMask;

    if (
        controller->context()->viewerMode()->mode() == Viewer_Sub_Mode_Body &&
        controller->context()->viewerMode()->bodyMode()->activeEntity() &&
        !controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs().empty()
    )
    {
        bodyPartMask = {
            controller->context()->viewerMode()->bodyMode()->activeEntity()->id(),
            controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs()
        };
    }

    if (parentID == EntityID())
    {
        auto it = entities.find(entity->id());

        if (it != entities.end())
        {
            return;
        }

        auto renderEntity = new RenderFlatEntity(
            this,

            swapchainElements,

            pipelineStore,
            entity,
            bodyPartMask,
            &entitiesToDelete
        );

        addLights(renderEntity);

        entities.insert({
            entity->id(),
            renderEntity
        });
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child->add(parentID, entity, bodyPartMask);
        }
    }
}

void FlatRender::remove(EntityID parentID, EntityID id)
{
    if (parentID == EntityID())
    {
        auto it = entities.find(id);

        if (it == entities.end())
        {
            return;
        }

        RenderFlatEntity* renderEntity = it->second;
        entities.erase(it);

        removeLights(renderEntity);

        entitiesToDelete.push_back(RenderDeletionTask(renderEntity));
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child->remove(parentID, id);
        }
    }
}

void FlatRender::update(EntityID parentID, const Entity* entity)
{
    BodyPartMask bodyPartMask;

    if (
        controller->context()->viewerMode()->mode() == Viewer_Sub_Mode_Body &&
        controller->context()->viewerMode()->bodyMode()->activeEntity() &&
        !controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs().empty()
    )
    {
        bodyPartMask = {
            controller->context()->viewerMode()->bodyMode()->activeEntity()->id(),
            controller->context()->viewerMode()->bodyMode()->hiddenBodyPartIDs()
        };
    }

    if (parentID == EntityID())
    {
        auto it = entities.find(entity->id());

        if (it == entities.end())
        {
            add(parentID, entity);
            return;
        }

        RenderFlatEntity* renderEntity = it->second;

        renderEntity->update(entity, bodyPartMask);

        updateLights(renderEntity);
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child->update(parentID, entity, bodyPartMask);
        }
    }
}

void FlatRender::removeDecoration(RenderDecorationID id)
{
    auto it = decorations.find(id);

    if (it == decorations.end())
    {
        return;
    }

    decorationsToDelete.push_back(RenderDeletionTask(it->second));

    decorations.erase(it);
}

void FlatRender::setDecorationShown(RenderDecorationID id, bool state)
{
    auto it = decorations.find(id);

    if (it == decorations.end())
    {
        return;
    }

    it->second->show(state);
}

void FlatRender::removePostProcessEffect(RenderPostProcessEffectID id)
{
    auto it = postProcessEffects.find(id);

    if (it == postProcessEffects.end())
    {
        return;
    }

    postProcessEffectsToDelete.push_back(RenderDeletionTask(it->second));

    postProcessEffects.erase(it);
}

void FlatRender::setPostProcessEffectActive(RenderPostProcessEffectID id, bool state)
{
    auto it = postProcessEffects.find(id);

    if (it == postProcessEffects.end())
    {
        return;
    }

    it->second->show(state);
}

void FlatRender::setPostProcessEffectOrder(const vector<RenderPostProcessEffectID>& order)
{
    postProcessOrder = join({ postProcessOrder.begin(), postProcessOrder.begin() + fixedPostProcessStageCount }, order);
}

RenderContext* FlatRender::context() const
{
    return ctx;
}

u32 FlatRender::imageCount() const
{
    return swapchainElements.size();
}

u32 FlatRender::currentImage() const
{
    return imageIndex;
}

VkRenderPass FlatRender::panelRenderPass() const
{
    return _panelRenderPass;
}

VkRenderPass FlatRender::uiRenderPass() const
{
    return _uiRenderPass;
}

VkFramebuffer FlatRender::textTitlecardFramebuffer() const
{
    return swapchainElements.at(imageIndex)->textTitlecard()->framebuffer;
}

VkFramebuffer FlatRender::textFramebuffer() const
{
    return swapchainElements.at(imageIndex)->text()->framebuffer;
}

f32 FlatRender::timer() const
{
    return _timer;
}

bool FlatRender::rayTracingSupported() const
{
    return ctx->rayTracingSupported();
}

bool FlatRender::rayTracingEnabled() const
{
    return _rayTracing;
}

f32 FlatRender::fogDistance() const
{
    if (controller->selfWorld())
    {
        return controller->selfWorld()->fogDistance(cameraPosition);
    }
    else
    {
        return 0;
    }
}

u32 FlatRender::width() const
{
    return _width;
}

u32 FlatRender::height() const
{
    return _height;
}

u32 FlatRender::pickAPIVersion() const
{
    u32 maxVulkanVersion;

    VkResult result = vkEnumerateInstanceVersion(&maxVulkanVersion);

    if (result != VK_SUCCESS)
    {
        error("Failed to get maximum Vulkan API version: " + vulkanError(result));
    }

    u32 maxMajorVulkanVersion = VK_API_VERSION_MAJOR(maxVulkanVersion);
    u32 maxMinorVulkanVersion = VK_API_VERSION_MINOR(maxVulkanVersion);

    u32 majorVulkanVersion;
    u32 minorVulkanVersion;

    if (desiredMajorVulkanVersion < maxMajorVulkanVersion)
    {
        majorVulkanVersion = desiredMajorVulkanVersion;
        minorVulkanVersion = desiredMinorVulkanVersion;
    }
    else if (desiredMajorVulkanVersion == maxMajorVulkanVersion)
    {
        majorVulkanVersion = desiredMajorVulkanVersion;
        minorVulkanVersion = min(desiredMinorVulkanVersion, maxMinorVulkanVersion);
    }
    else
    {
        majorVulkanVersion = maxMajorVulkanVersion;
        minorVulkanVersion = maxMinorVulkanVersion;
    }

    return VK_MAKE_API_VERSION(0, majorVulkanVersion, minorVulkanVersion, 0);
}

set<string> FlatRender::instanceExtensions() const
{
    set<string> instanceExtensions = controller->window()->vulkanExtensions();

    instanceExtensions.insert("VK_KHR_get_physical_device_properties2");

    return instanceExtensions;
}

VkPhysicalDevice FlatRender::pickPhysicalDevice() const
{
    VkResult result;

    u32 physDeviceCount = 0;

    if (!g_config.graphicsDevice.empty())
    {
        result = vkEnumeratePhysicalDevices(ctx->instance, &physDeviceCount, nullptr);

        if (result != VK_SUCCESS)
        {
            fatal("Failed to enumerate Vulkan physical devices: " + vulkanError(result));
        }

        vector<VkPhysicalDevice> physDevices(physDeviceCount);

        result = vkEnumeratePhysicalDevices(ctx->instance, &physDeviceCount, physDevices.data());

        if (result != VK_SUCCESS)
        {
            fatal("Failed to enumerate Vulkan physical devices: " + vulkanError(result));
        }

        VkPhysicalDevice pickedDevice = nullptr;

        for (const VkPhysicalDevice& physDevice : physDevices)
        {
            VkPhysicalDeviceProperties properties{};

            vkGetPhysicalDeviceProperties(physDevice, &properties);

            if (properties.deviceName == g_config.graphicsDevice)
            {
                pickedDevice = physDevice;
                break;
            }
        }

        if (pickedDevice == nullptr)
        {
            fatal("No suitable Vulkan physical devices found.");
        }

        return pickedDevice;
    }
    else
    {
        result = vkEnumeratePhysicalDevices(ctx->instance, &physDeviceCount, nullptr);

        if (result != VK_SUCCESS)
        {
            fatal("Failed to enumerate Vulkan physical devices: " + vulkanError(result));
        }

        vector<VkPhysicalDevice> physDevices(physDeviceCount);

        result = vkEnumeratePhysicalDevices(ctx->instance, &physDeviceCount, physDevices.data());

        if (result != VK_SUCCESS)
        {
            fatal("Failed to enumerate Vulkan physical devices: " + vulkanError(result));
        }

        VkPhysicalDevice bestPhysDevice = nullptr;
        string bestName;
        u64 bestScore = 0;

        for (const VkPhysicalDevice& physDevice : physDevices)
        {
            VkPhysicalDeviceProperties properties{};

            vkGetPhysicalDeviceProperties(physDevice, &properties);

            u64 score = 0;

            switch (properties.deviceType)
            {
            default :
            case VK_PHYSICAL_DEVICE_TYPE_OTHER :
                score = 1;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_CPU :
                score = 2;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU :
                score = 3;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU :
                score = 4;
                break;
            case VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU :
                score = 5;
                break;
            }

            if (score > bestScore)
            {
                bestPhysDevice = physDevice;
                bestName = properties.deviceName;
                bestScore = score;
            }
        }

        if (bestScore == 0)
        {
            fatal("No suitable Vulkan physical devices found.");
        }

        return bestPhysDevice;
    }
}

VkSurfaceKHR FlatRender::prepareSurface()
{
    surface = controller->window()->createVulkanSurface(ctx->instance);

    return surface;
}

set<string> FlatRender::deviceExtensions() const
{
    set<string> deviceExtensions = { "VK_KHR_swapchain" };

    // Compatibility with 1.1
    if (VK_API_VERSION_MAJOR(ctx->apiVersion) <= 1 && VK_API_VERSION_MINOR(ctx->apiVersion) < 2)
    {
        deviceExtensions.insert("VK_KHR_create_renderpass2");
        deviceExtensions.insert("VK_KHR_depth_stencil_resolve");

        if (ctx->rayTracingEnabled())
        {
            deviceExtensions.insert("VK_KHR_buffer_device_address");
        }
    }

    // Compatibility with 1.0
    if (VK_API_VERSION_MAJOR(ctx->apiVersion) <= 1 && VK_API_VERSION_MINOR(ctx->apiVersion) < 1)
    {
        deviceExtensions.insert("VK_KHR_multiview");
        deviceExtensions.insert("VK_KHR_maintenance1");
        deviceExtensions.insert("VK_KHR_maintenance2");
    }

    return join(deviceExtensions, ctx->rayTracingDeviceExtensions());
}

u32 FlatRender::prepareSwapchain()
{
    createSwapchain();

    return getSwapchainImageCount(ctx->device, swapchain);
}

void FlatRender::updateCameraUniforms()
{
    fmat4 projection;

    if (cameraOrthographic)
    {
        f32 left = -cameraSize;
        f32 right = +cameraSize;
        f32 top = -(cameraSize / _width) * _height;
        f32 bottom = +(cameraSize / _width) * _height;

        projection = createFlatOrthographicMatrix(
            left,
            right,
            top,
            bottom,
            static_cast<f32>(defaultNearDistance),
            static_cast<f32>(defaultFarDistance)
        );
    }
    else
    {
        projection = createFlatPerspectiveMatrix(
            static_cast<f32>(cameraAngle),
            static_cast<f32>(_width) / static_cast<f32>(_height),
            static_cast<f32>(defaultNearDistance),
            static_cast<f32>(defaultFarDistance)
        );
    }

    swapchainElements.at(imageIndex)->cameraMapping()->data[0] = projection;

    swapchainElements.at(imageIndex)->cameraMapping()->data[2] = createViewMatrix<f32>(
        fvec3(),
        cameraRotation
    );
}

void FlatRender::updateTextTitlecardUniforms(const quaternion& rotation, const quaternion& space)
{
    fmat4 model = createFlatTextTitlecardTransform<f32>(
        fvec3(),
        rotation,
        space.up()
    );

    memcpy(swapchainElements.at(imageIndex)->textTitlecard()->uniformMapping->data, &model, sizeof(model));
}

void FlatRender::updateTextUniforms(const quaternion& rotation, const quaternion& space)
{
    fmat4 model = createFlatTextTransform<f32>(
        fvec3(),
        rotation,
        space.up(),
        _height
    );

    memcpy(swapchainElements.at(imageIndex)->text()->uniformMapping->data, &model, sizeof(model));
}

void FlatRender::renderTextTitlecard(VkCommandBuffer commandBuffer)
{
    VkViewport viewport = {
        0, static_cast<f32>(_height),
        static_cast<f32>(_width), -static_cast<f32>(_height),
        0, 1
    };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { _width, _height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineStore->panel.pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineStore->panel.pipelineLayout,
        0,
        1,
        &swapchainElements.at(imageIndex)->textTitlecard()->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, 6, 1, 0, 0);
}

void FlatRender::renderText(VkCommandBuffer commandBuffer)
{
    VkViewport viewport = {
        0, static_cast<f32>(_height),
        static_cast<f32>(_width), -static_cast<f32>(_height),
        0, 1
    };

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { _width, _height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipelineStore->panel.pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineStore->panel.pipelineLayout,
        0,
        1,
        &swapchainElements.at(imageIndex)->text()->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(commandBuffer, 6, 1, 0, 0);
}

void FlatRender::renderSky(const FlatSwapchainElement* swapchainElement)
{
    vector<VkClearValue> clearValues = fillClearValues({ Clear_Value_Color });

    VkRenderPassBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginInfo.renderPass = skyRenderPass;
    beginInfo.framebuffer = swapchainElement->sky()->framebuffer;
    beginInfo.renderArea = {
        { 0, 0 },
        { skyWidth, skyHeight }
    };
    beginInfo.clearValueCount = clearValues.size();
    beginInfo.pClearValues = clearValues.data();

    vkCmdBeginRenderPass(swapchainElement->commandBuffer(), &beginInfo, VK_SUBPASS_CONTENTS_INLINE);

    if (controller->selfWorld())
    {
        sky->renderEnvironmentMap(
            swapchainElement->sky(),
            controller->selfWorld()->sky(cameraPosition),
            controller->selfWorld()->up(cameraPosition)
        );
    }

    vkCmdEndRenderPass(swapchainElement->commandBuffer());
}

void FlatRender::outputImage(VkCommandBuffer commandBuffer, VkImage inputImage, VkImage outputImage)
{
    VkImageMemoryBarrier beforeBarrier = imageMemoryBarrier(
        outputImage,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    vkCmdPipelineBarrier(
        commandBuffer,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &beforeBarrier
    );

    VkImageBlit region{};
    region.srcSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.srcSubresource.mipLevel = 0;
    region.srcSubresource.layerCount = 1;
    region.srcOffsets[0] = { 0, 0, 0 };
    region.srcOffsets[1] = { static_cast<i32>(_width), static_cast<i32>(_height), 1 };
    region.dstSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    region.dstSubresource.mipLevel = 0;
    region.dstSubresource.layerCount = 1;
    region.dstOffsets[0] = { 0, 0, 0 };
    region.dstOffsets[1] = { static_cast<i32>(_width), static_cast<i32>(_height), 1 };

    vkCmdBlitImage(
        commandBuffer,
        inputImage,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        outputImage,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        1,
        &region,
        VK_FILTER_NEAREST
    );

    VkImageMemoryBarrier afterBarrier = imageMemoryBarrier(
        outputImage,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR
    );

    vkCmdPipelineBarrier(
        commandBuffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        0,
        0,
        nullptr,
        0,
        nullptr,
        1,
        &afterBarrier
    );
}

void FlatRender::advanceDeletionQueues()
{
    for (size_t i = 0; i < entitiesToDelete.size();)
    {
        RenderDeletionTask<RenderFlatEntity>& task = entitiesToDelete[i];
        task.increment();

        if (task.ready(swapchainElements.size()))
        {
            task.destroy();
            entitiesToDelete.erase(entitiesToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (size_t i = 0; i < decorationsToDelete.size();)
    {
        RenderDeletionTask<RenderFlatDecoration>& task = decorationsToDelete[i];
        task.increment();

        if (task.ready(swapchainElements.size()))
        {
            task.destroy();
            decorationsToDelete.erase(decorationsToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (size_t i = 0; i < postProcessEffectsToDelete.size();)
    {
        RenderDeletionTask<RenderFlatPostProcessEffect>& task = postProcessEffectsToDelete[i];
        task.increment();

        if (task.ready(swapchainElements.size()))
        {
            task.destroy();
            postProcessEffectsToDelete.erase(postProcessEffectsToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }
}

void FlatRender::createSwapchain()
{
    tie(format, _width, _height) = ::createSwapchain(
        ctx->device,
        _width,
        _height,
        ctx->graphicsQueueIndex,
        ctx->presentQueueIndex,
        ctx->physDevice,
        surface,
        swapchain
    );

    if (_width == 0 || _height == 0)
    {
        fatal("Swapchain reported size of 0.");
    }
}

void FlatRender::destroySwapchain()
{
    ::destroySwapchain(ctx->device, swapchain);
}

void FlatRender::createSwapchainElements()
{
    vector<VkImage> images = getSwapchainImages(ctx->device, swapchain);

    for (u32 i = 0; i < images.size(); i++)
    {
        VkImage image = images.at(i);

        if (g_config.antiAliasingLevel == 1)
        {
            swapchainElements.push_back(new FlatMonosampleSwapchainElement(
                ctx,
                i,
                _width,
                _height,
                _panelRenderPass,
                skyRenderPass,
                mainRenderPass,
                hdrRenderPass,
                postProcessRenderPass,
                overlayRenderPass,
                independentDepthOverlayRenderPass,
                _uiRenderPass,
                pipelineStore->panelDescriptorSetLayout,
                format,
                image
            ));
        }
        else
        {
            swapchainElements.push_back(new FlatMultisampleSwapchainElement(
                ctx,
                i,
                _width,
                _height,
                pickMSAALevel(g_config.antiAliasingLevel),
                _panelRenderPass,
                skyRenderPass,
                mainRenderPass,
                hdrRenderPass,
                postProcessRenderPass,
                overlayRenderPass,
                independentDepthOverlayRenderPass,
                _uiRenderPass,
                pipelineStore->panelDescriptorSetLayout,
                format,
                image
            ));
        }
    }
}

void FlatRender::destroySwapchainElements()
{
    for (FlatSwapchainElement* element : swapchainElements)
    {
        delete element;
    }

    swapchainElements.clear();
}

VkSampleCountFlagBits FlatRender::pickMSAALevel(u32 level) const
{
    switch (level)
    {
    default :
    case 1 :
        return VK_SAMPLE_COUNT_1_BIT;
    case 2 :
        return VK_SAMPLE_COUNT_2_BIT;
    case 4 :
        return VK_SAMPLE_COUNT_4_BIT;
    case 8 :
        return VK_SAMPLE_COUNT_8_BIT;
    case 16 :
        return VK_SAMPLE_COUNT_16_BIT;
    case 32 :
        return VK_SAMPLE_COUNT_32_BIT;
    case 64 :
        return VK_SAMPLE_COUNT_64_BIT;
    }
}

void FlatRender::computeLuminance(FlatSwapchainElement* element) const
{
    u32 scaledWidth = _width * g_config.luminanceScale;
    u32 scaledHeight = _height * g_config.luminanceScale;

    u32 luminanceSize = luminanceImageSize(scaledWidth, scaledHeight);

    transitionLayout(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    {
        VkClearColorValue color{};

        VkImageSubresourceRange range{};
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.baseArrayLayer = 0;
        range.layerCount = 1;

        vkCmdClearColorImage(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            &color,
            1,
            &range
        );
    }

    blitImage(
        element->commandBuffer(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        1,
        VK_FILTER_LINEAR,

        element->colorImage(),
        _width,
        _height,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,

        element->luminanceImage(),
        scaledWidth,
        scaledHeight,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    computeMipmaps(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        luminanceSize,
        luminanceSize
    );

    {
        VkBufferImageCopy region{};
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = maxMipLevels(luminanceSize, luminanceSize) - 1;
        region.imageSubresource.layerCount = 1;
        region.imageExtent = { 1, 1, 1 };

        vkCmdCopyImageToBuffer(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            element->luminance().buffer,
            1,
            &region
        );
    }

    transitionLayout(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    {
        VkClearColorValue color{};
        color.float32[0] = -1;
        color.float32[1] = 0;
        color.float32[2] = 0;
        color.float32[3] = +1;

        VkImageSubresourceRange range{};
        range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        range.baseMipLevel = 0;
        range.levelCount = 1;
        range.baseArrayLayer = 0;
        range.layerCount = 1;

        vkCmdClearColorImage(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            &color,
            1,
            &range
        );
    }

    blitImage(
        element->commandBuffer(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_ASPECT_COLOR_BIT,
        1,
        VK_FILTER_LINEAR,

        element->stencilImage(),
        _width,
        _height,
        VK_IMAGE_LAYOUT_GENERAL,
        VK_IMAGE_LAYOUT_GENERAL,

        element->luminanceImage(),
        scaledWidth,
        scaledHeight,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL
    );

    computeMipmaps(
        element->commandBuffer(),
        element->luminanceImage(),
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        luminanceSize,
        luminanceSize
    );

    {
        VkBufferImageCopy region{};
        region.imageSubresource.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
        region.imageSubresource.mipLevel = maxMipLevels(luminanceSize, luminanceSize) - 1;
        region.imageSubresource.layerCount = 1;
        region.imageExtent = { 1, 1, 1 };
        region.bufferOffset = sizeof(fvec4);

        vkCmdCopyImageToBuffer(
            element->commandBuffer(),
            element->luminanceImage(),
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            element->luminance().buffer,
            1,
            &region
        );
    }

    element->setLuminanceReady();
}

f32 FlatRender::getLuminance(const FlatSwapchainElement* element) const
{
    u32 scaledWidth = _width * g_config.luminanceScale;
    u32 scaledHeight = _height * g_config.luminanceScale;

    fvec4 color = element->luminanceMapping()->data[0];
    fvec4 stencil = element->luminanceMapping()->data[1];

    f32 stencilOccupancy = (stencil.x + 1) / 2;

    return color.toVec3().dot(luminanceFactor) * (1 / stencilOccupancy) * (1 / luminanceOccupancy(scaledWidth, scaledHeight));
}

void FlatRender::adjustExposure(const chrono::microseconds& elapsed, f32 luminance)
{
    static constexpr f32 s = 100;
    static constexpr f32 k = 12.5;
    static constexpr f32 q = 0.65;

    luminance = clamp(luminance, renderAutoExposureMinLuminance, renderAutoExposureMaxLuminance);

    f32 lmax = (78 / (q * s)) * pow(2, log(luminance * (s / k), 2));

    f32 targetExposure = 1 / lmax;

    targetExposure *= renderAutoExposureMultiplier;

    exposure = exposure + (1 - exp(-(elapsed.count() / static_cast<f32>(renderAutoExposureDelay.count())))) * (targetExposure - exposure);
}
