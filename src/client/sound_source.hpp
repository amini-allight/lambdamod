/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "sound_constants.hpp"
#include "sound_stereo_chunk.hpp"

class Sound;

struct DelayedSoundChunk
{
    SoundChunk chunk;
    chrono::microseconds delay;
};

class SoundSource
{
public:
    SoundSource(
        Sound* sound,
        const vec3& position,
        const quaternion& rotation,
        bool directional = false,
        f64 angle = tau,
        bool canBeSpatial = true
    );
    virtual ~SoundSource();

    bool depleted() const;

    void output(f64 multiplier);
    void recordLastDistance();

protected:
    Sound* sound;
    vec3 position;
    quaternion rotation;
    bool directional;
    f64 angle;
    bool canBeSpatial;

    virtual variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> pull(f64 multiplier) = 0;
    void filter(variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk);

private:
    deque<f32> delayedSamples[earCount];
    size_t delayedSamplesWriteHead[earCount];
    f64 delayedEnergy[earCount];
    f64 lastDistance[earCount];
    chrono::microseconds lastDistanceUpdateTime;
    deque<f32> reverbSamples;
    f64 reverbEnergy;

    void spatialize(variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk);
    void stabilizeVolume(variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk);

    void pushSamples(u32 ear, const vec3& earPosition, const vector<f32>& chunk);
    vector<f32> pullSamples(u32 ear);

    void pushReverb(const vector<f32>& chunk);
    void peekReverb(vector<f32>& samples) const;
    void popReverb();

    void applyDirectionality(vector<f32>& samples, const vec3& earPosition) const;
    void applyDistanceFalloff(vector<f32>& lo, vector<f32>& hi, const vec3& earPosition) const;
    void applyOcclusion(vector<f32>& lo, vector<f32>& hi, const vec3& earPosition) const;
    void applyDopplerEffect(vector<f32>& samples, f64 doppler) const;
    void applyHRTF(vector<f32>& lo, vector<f32>& hi, i32 ear, const vec3& dir) const;
};
