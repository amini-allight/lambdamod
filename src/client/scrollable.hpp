/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

class Scrollable
{
public:
    Scrollable();

protected:
    bool thumbGrabbed;

    bool needsScroll() const;

    i32 maxScrollIndex() const;

    i32 elapsedHeight() const;
    i32 totalHeight() const;
    virtual i32 viewHeight() const = 0;

    virtual i32 scrollIndex() const = 0;
    virtual void setScrollIndex(i32 index) = 0;

    virtual i32 childCount() const = 0;
    virtual i32 childHeight() const = 0;

    i32 thumbY() const;
    i32 thumbHeight() const;

    void grabThumb(const Point& local);
    void releaseThumb();
    void dragThumb(const Point& local);

    void setScrollIndexFromThumbY(i32 thumbY);

private:
    i32 thumbGrabY;
    Point thumbGrabPosition;
};
