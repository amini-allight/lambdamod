/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "advanced_text_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "constants.hpp"
#include "line_edit.hpp"
#include "clipboard.hpp"
#include "direct_widget_input_scope.hpp"

AdvancedTextEditor::AdvancedTextEditor(
    InterfaceWidget* parent,
    const function<string()>& source,
    const function<void(const string&)>& onSave,
    bool codeMode
)
    : BasicTextEditor(parent, source, onSave, codeMode)
    , mode(Text_Editor_Command_Mode)
    , partial(nullptr)
    , subPartial(nullptr)
    , searchForwards(true)
{
    START_DIRECT_WIDGET_INPUT_SCOPE(scope(), "advanced-text-editor", true)

    END_SCOPE
}

void AdvancedTextEditor::input(const Input& input, u32 repeats)
{
    switch (mode)
    {
    case Text_Editor_Command_Mode :
        commandModeInput(input);
        break;
    case Text_Editor_Insert_Mode :
        insertModeInput(input, repeats);
        break;
    case Text_Editor_Replace_Mode :
        replaceModeInput(input);
        break;
    case Text_Editor_Visual_Mode :
        visualModeInput(input);
        break;
    case Text_Editor_Search_Mode :
        searchModeInput(input);
        break;
    }
}

void AdvancedTextEditor::unfocus()
{
    InterfaceWidget::unfocus();

    if (selecting && mode == Text_Editor_Command_Mode)
    {
        endCommandModeSelection(Input(), pointer - position());
    }
    else if (selecting && mode == Text_Editor_Visual_Mode)
    {
        endVisualModeSelection(Input(), pointer - position());
    }
}

void AdvancedTextEditor::drawStatusLine(const DrawContext& ctx) const
{
    string mode;

    auto join = [](const vector<string>& parts) -> string
    {
        string s;

        for (i32 i = 0; i < static_cast<i32>(parts.size()); i++)
        {
            const string& part = parts[i];

            s += part;

            if (i + 1 != static_cast<i32>(parts.size()) && !part.empty())
            {
                s += " ";
            }
        }

        return s;
    };

    switch (this->mode)
    {
    case Text_Editor_Command_Mode :
        mode = "";
        break;
    case Text_Editor_Insert_Mode :
        mode = "-- " + localize("advanced-text-editor-insert-mode") + " --";
        break;
    case Text_Editor_Replace_Mode :
        mode = "-- " + localize("advanced-text-editor-replace-mode") + " --";
        break;
    case Text_Editor_Visual_Mode :
        mode = "-- " + localize("advanced-text-editor-visual-mode") + " --";
        break;
    case Text_Editor_Search_Mode :
        mode = "-- " + localize("advanced-text-editor-search-mode") + " --";
        break;
    }

    string saveMarker = _saved ? "" : "(" + localize("advanced-text-editor-unsaved") + ")";

    drawText(
        ctx,
        this,
        Point(0, visualLines() * charHeight()),
        Size(size().w, charHeight()),
        join({
            mode,
            statusMessage,
            saveMarker,
            search,
            to_string(cursorLine() + 1) + ":" + to_string(cursorApparentColumn() + 1)
        }),
        TextStyle()
    );

    if (this->mode == Text_Editor_Search_Mode && !search.empty())
    {
        i32 x = static_cast<i32>(mode.size()) + static_cast<i32>(statusMessage.size()) + static_cast<i32>(saveMarker.size());
        x += !mode.empty() + !statusMessage.empty() + !saveMarker.empty();
        x += static_cast<i32>(search.size());

        drawSolid(
            ctx,
            this,
            Point(UIScale::marginSize() + (x * charWidth()), visualLines() * charHeight()),
            Size(2, charHeight()),
            theme.cursorColor
        );
    }
}

void AdvancedTextEditor::drawActiveLine(const DrawContext& ctx) const
{
    if (mode == Text_Editor_Visual_Mode || mode == Text_Editor_Search_Mode)
    {
        return;
    }

    BasicTextEditor::drawActiveLine(ctx);
}

void AdvancedTextEditor::drawContent(const DrawContext& ctx) const
{
    if (!search.empty())
    {
        for (const auto& [ start, size ] : allSearchResults())
        {
            i32 y = indexToPosition(start).y;

            if (y < viewLine() || y >= viewLine() + visualLines())
            {
                continue;
            }

            drawRangeOnLine(ctx, y - viewLine(), start, start + size, theme.accentColor);
        }
    }

    BasicTextEditor::drawContent(ctx);
}

void AdvancedTextEditor::drawCursor(const DrawContext& ctx) const
{
    switch (mode)
    {
    case Text_Editor_Command_Mode :
        drawBoxCursor(ctx);
        break;
    case Text_Editor_Insert_Mode :
    case Text_Editor_Visual_Mode :
        BasicTextEditor::drawCursor(ctx);
        break;
    case Text_Editor_Replace_Mode :
        drawUnderlineCursor(ctx);
        break;
    case Text_Editor_Search_Mode :
        break;
    }
}

void AdvancedTextEditor::drawBoxCursor(const DrawContext& ctx) const
{
    if (
        cursorLine() >= viewLine() &&
        cursorLine() < viewLine() + visualLines() &&
        cursorApparentColumn() >= viewColumn() &&
        cursorApparentColumn() < viewColumn() + visualColumns()
    )
    {
        drawSolid(
            ctx,
            this,
            Point(
                UIScale::lineNumberColumnWidth() + UIScale::marginSize() + ((cursorApparentColumn() - viewColumn()) * charWidth()),
                (cursorLine() - viewLine()) * charHeight()
            ),
            Size(charWidth(), charHeight()),
            theme.cursorColor
        );
    }
}

void AdvancedTextEditor::drawUnderlineCursor(const DrawContext& ctx) const
{
    if (
        cursorLine() >= viewLine() &&
        cursorLine() < viewLine() + visualLines() &&
        cursorApparentColumn() >= viewColumn() &&
        cursorApparentColumn() < viewColumn() + visualColumns()
    )
    {
        drawSolid(
            ctx,
            this,
            Point(
                UIScale::lineNumberColumnWidth() + UIScale::marginSize() + ((cursorApparentColumn() - viewColumn()) * charWidth()),
                ((cursorLine() - viewLine()) * charHeight()) + (charHeight() - UIScale::textCursorWidth())
            ),
            Size(charWidth(), UIScale::textCursorWidth()),
            theme.cursorColor
        );
    }
}

void AdvancedTextEditor::commandModeInput(const Input& input)
{
    Point local = pointer - position();

    if (input.up && input.type == Input_Left_Mouse)
    {
        endCommandModeSelection(input, local);
        return;
    }
    else if (input.type == Input_Mouse_Move)
    {
        moveSelect(local);
        return;
    }

    if (input.up)
    {
        return;
    }

    if (partial)
    {
        partial(input);
        return;
    }

    switch (input.type)
    {
    default :
        break;
    case Input_3 :
        if (input.shift)
        {
            searchForwards = false;
            search = wordAtCursor();
            goTo(previousOccurrence(search));
        }
        break;
    case Input_4 :
        if (input.shift)
        {
            goTo(lineEnd());
        }
        break;
    case Input_5 :
        if (input.shift)
        {
            goTo(matchingBracket());
        }
        break;
    case Input_6 :
        if (input.shift)
        {
            goTo(softLineStart());
        }
        break;
    case Input_8 :
        if (input.shift)
        {
            searchForwards = true;
            search = wordAtCursor();
            goTo(nextOccurrence(search));
        }
        break;
    case Input_9 :
        if (input.shift)
        {
            goTo(sentenceStart());
        }
        break;
    case Input_0 :
        if (input.shift)
        {
            goTo(sentenceEnd());
        }
        else
        {
            goTo(lineStart());
        }
        break;
    case Input_Minus :
        if (input.shift)
        {
            goTo(softLineStart());
        }
        else
        {
            goTo(softLineStart(cursorLine() - 1));
        }
        break;
    case Input_Equals :
        if (input.shift)
        {
            goTo(softLineStart(cursorLine() + 1));
        }
        break;
    case Input_W :
        if (!input.shift)
        {
            goTo(nextWordStart());
        }
        break;
    case Input_E :
        if (!input.shift)
        {
            goTo(wordEnd());
        }
        break;
    case Input_R :
        if (input.control)
        {
            redo();
        }
        else if (input.shift)
        {
            enterReplaceMode();
        }
        else
        {
            partial = bind(
                static_cast<void (AdvancedTextEditor::*)(const Input&)>(&AdvancedTextEditor::replace),
                this,
                placeholders::_1
            );
        }
        break;
    case Input_T :
        if (input.shift)
        {
            partial = bind(&AdvancedTextEditor::untilPrevious, this, placeholders::_1);
        }
        else
        {
            partial = bind(&AdvancedTextEditor::untilNext, this, placeholders::_1);
        }
        break;
    case Input_Y :
        if (input.shift)
        {
            copyRegion(input, cursorIndex(), lineEnd() - cursorIndex());
        }
        else
        {
            partial = bind(&AdvancedTextEditor::yank, this, placeholders::_1);
        }
        break;
    case Input_U :
        if (input.control)
        {
            scrollUp(visualLines() / 2);
        }
        if (!input.shift)
        {
            undo();
        }
        break;
    case Input_I :
        if (input.shift)
        {
            goTo(softLineStart());
            enterInsertMode();
        }
        else
        {
            enterInsertMode();
        }
        break;
    case Input_O :
        if (input.shift)
        {
            i32 indent = lineIndent();
            goTo(lineStart());
            add(cursorIndex(), "\n" + string(lineIndent() * indentSize, ' '));
            cursorLeft();
            indentLine(cursorLine(), indent);
            enterInsertMode();
        }
        else
        {
            i32 indent = lineIndent();
            goTo(lineEnd());
            add(cursorIndex(), "\n" + string(lineIndent() * indentSize, ' '));
            indentLine(cursorLine(), indent);
            enterInsertMode();
        }
        break;
    case Input_P :
        if (input.shift)
        {
            pasteClipboard();
        }
        else
        {
            cursorRight();
            pasteClipboard();
        }
        break;
    case Input_Left_Brace :
        if (input.shift)
        {
            goTo(paragraphStart());
        }
        break;
    case Input_Right_Brace :
        if (input.shift)
        {
            goTo(paragraphEnd());
        }
        break;
    case Input_A :
        if (input.shift)
        {
            goTo(lineEnd());
            enterInsertMode();
        }
        else
        {
            cursorRight();
            enterInsertMode();
        }
        break;
    case Input_S :
        if (input.shift)
        {
            i32 indent = lineIndent();
            goTo(lineStart());
            cutRegion(input, cursorIndex(), lineEnd() - cursorIndex());
            indentLine(cursorLine(), indent);
            enterInsertMode();
        }
        else
        {
            cutRegion(input, nextCharacter(), 1);
            enterInsertMode();
        }
        break;
    case Input_D :
        if (input.control)
        {
            scrollDown(visualLines() / 2);
        }
        else if (input.shift)
        {
            cutRegion(input, cursorIndex(), lineEnd() - cursorIndex());
        }
        else
        {
            partial = bind(&AdvancedTextEditor::remove, this, placeholders::_1);
        }
        break;
    case Input_F :
        if (input.control)
        {
            scrollDown(visualLines());
        }
        else if (input.shift)
        {
            partial = bind(&AdvancedTextEditor::findPrevious, this, placeholders::_1);
        }
        else
        {
            partial = bind(&AdvancedTextEditor::findNext, this, placeholders::_1);
        }
        break;
    case Input_G :
        if (input.shift)
        {
            goTo(documentEnd());
        }
        else
        {
            partial = bind(&AdvancedTextEditor::misc, this, placeholders::_1);
        }
        break;
    case Input_H :
        if (input.shift)
        {
            goTo(pageStart());
        }
        else
        {
            cursorLeft();
        }
        break;
    case Input_J :
        if (input.shift)
        {
            goTo(nextOccurrence("\n"));
            if (cursorIndex() != static_cast<i32>(state().text.size()) && characterAtCursor() == '\n')
            {
                BasicTextEditor::remove(nextCharacter(), 1);
            }
        }
        else
        {
            cursorDown();
        }
        break;
    case Input_K :
        if (!input.shift)
        {
            cursorUp();
        }
        break;
    case Input_L :
        if (input.shift)
        {
            goTo(pageEnd());
        }
        else
        {
            cursorRight();
        }
        break;
    case Input_Backslash :
        if (input.shift)
        {
            goTo(lineStart());
        }
        break;
    case Input_X :
        if (input.shift)
        {
            cutRegion(input, previousCharacter(), 1);
        }
        else
        {
            cutRegion(input, nextCharacter(), 1);
        }
        break;
    case Input_C :
        if (input.shift)
        {
            cutRegion(input, cursorIndex(), lineEnd() - cursorIndex());
            enterInsertMode();
        }
        else
        {
            partial = bind(&AdvancedTextEditor::change, this, placeholders::_1);
        }
        break;
    case Input_V :
        if (!input.shift)
        {
            enterVisualMode();
        }
        break;
    case Input_B :
        if (input.control)
        {
            scrollUp(visualLines());
        }
        else
        {
            goTo(wordStart());
        }
        break;
    case Input_N :
        if (!search.empty())
        {
            if (input.shift)
            {
                goTo(searchForwards ? previousOccurrence(search) : nextOccurrence(search));
            }
            else
            {
                goTo(searchForwards ? nextOccurrence(search) : previousOccurrence(search));
            }
        }
        break;
    case Input_M :
        if (input.shift)
        {
            goTo(pageMiddle());
        }
        break;
    case Input_Comma :
        if (input.shift)
        {
            partial = bind(&AdvancedTextEditor::unindent, this, placeholders::_1);
        }
        break;
    case Input_Period :
        if (input.shift)
        {
            partial = bind(&AdvancedTextEditor::indent, this, placeholders::_1);
        }
        break;
    case Input_Slash :
        if (input.shift)
        {
            enterSearchMode(true);
        }
        else
        {
            enterSearchMode();
        }
        break;
    case Input_Left_Mouse :
        startSelect(local);
        break;
    case Input_Backspace :
        cursorLeft();
        break;
    case Input_Return :
        goTo(softLineStart(cursorLine() + 1));
        break;
    case Input_Space :
        cursorRight();
        break;
    case Input_Left :
        if (input.shift)
        {
            goTo(wordStart());
        }
        else
        {
            cursorLeft();
        }
        break;
    case Input_Right :
        if (input.shift)
        {
            goTo(nextWordStart());
        }
        else
        {
            cursorRight();
        }
        break;
    case Input_Up :
        cursorUp();
        break;
    case Input_Down :
        cursorDown();
        break;
    case Input_Insert :
        enterInsertMode();
        break;
    case Input_Delete :
        BasicTextEditor::remove(nextCharacter(), 1);
        break;
    case Input_Home :
        goTo(pageStart());
        break;
    case Input_End :
        goTo(pageEnd());
        break;
    case Input_Page_Up :
        scrollUp(visualLines());
        break;
    case Input_Page_Down :
        scrollDown(visualLines());
        break;
    case Input_Num_Add :
        goTo(softLineStart(cursorLine() + 1));
        break;
    case Input_Num_Subtract :
        goTo(softLineStart(cursorLine() - 1));
        break;
    case Input_Num_Multiply :
        searchForwards = true;
        search = wordAtCursor();
        goTo(previousOccurrence(search));
        break;
    case Input_Num_Divide :
        enterSearchMode();
        break;
    case Input_Num_Return :
        goTo(softLineStart(cursorLine() + 1));
        break;
    }
}

void AdvancedTextEditor::insertModeInput(const Input& input, u32 repeats)
{
    if (input.type == Input_Escape)
    {
        enterCommandMode();
        return;
    }

    scope()->parent()->handle(input, repeats);
}

void AdvancedTextEditor::replaceModeInput(const Input& input)
{
    if (input.up)
    {
        return;
    }

    switch (input.type)
    {
    default :
    {
        optional<char> symbol = this->symbol(input);

        if (symbol)
        {
            BasicTextEditor::replace(cursorIndex(), *symbol);
            cursorRight();
        }
        break;
    }
    case Input_Escape :
        enterCommandMode();
        break;
    case Input_Backspace :
        cursorLeft();
        break;
    case Input_Left :
        if (input.shift)
        {
            goTo(wordStart());
        }
        else
        {
            cursorLeft();
        }
        break;
    case Input_Right :
        if (input.shift)
        {
            goTo(nextWordStart());
        }
        else
        {
            cursorRight();
        }
        break;
    case Input_Up :
        cursorUp();
        break;
    case Input_Down :
        cursorDown();
        break;
    case Input_Insert :
        enterInsertMode();
        break;
    case Input_Delete :
        BasicTextEditor::remove(nextCharacter(), 1);
        break;
    case Input_Home :
        goTo(lineStart());
        break;
    case Input_End :
        goTo(lineEnd());
        break;
    case Input_Page_Up :
        scrollUp(visualLines());
        break;
    case Input_Page_Down :
        scrollDown(visualLines());
        break;
    }
}

void AdvancedTextEditor::visualModeInput(const Input& input)
{
    Point local = pointer - position();

    if (input.up && input.type == Input_Left_Mouse)
    {
        endVisualModeSelection(input, local);
        return;
    }
    else if (input.type == Input_Mouse_Move)
    {
        moveSelect(local);
        return;
    }

    if (input.up)
    {
        return;
    }

    if (partial)
    {
        partial(input);
        return;
    }

    ivec2 old = state().cursor;

    switch (input.type)
    {
    default :
        break;
    case Input_3 :
        if (input.shift)
        {
            searchForwards = false;
            search = wordAtCursor();
            selectTo(previousOccurrence(search));
        }
        break;
    case Input_4 :
        if (input.shift)
        {
            selectTo(lineEnd());
        }
        break;
    case Input_5 :
        if (input.shift)
        {
            selectTo(matchingBracket());
        }
        break;
    case Input_6 :
        if (input.shift)
        {
            selectTo(softLineStart());
        }
        break;
    case Input_8 :
        if (input.shift)
        {
            searchForwards = true;
            search = wordAtCursor();
            selectTo(nextOccurrence(search));
        }
        break;
    case Input_9 :
        if (input.shift)
        {
            selectTo(sentenceStart());
        }
        break;
    case Input_0 :
        if (input.shift)
        {
            selectTo(sentenceEnd());
        }
        else
        {
            selectTo(lineStart());
        }
        break;
    case Input_Minus :
        if (input.shift)
        {
            selectTo(softLineStart());
        }
        else
        {
            selectTo(softLineStart(cursorLine() - 1));
        }
        break;
    case Input_Equals :
        if (input.shift)
        {
            selectTo(softLineStart(cursorLine() + 1));
        }
        break;
    case Input_W :
        if (!input.shift)
        {
            selectTo(nextWordStart());
        }
        break;
    case Input_E :
        if (!input.shift)
        {
            selectTo(wordEnd());
        }
        break;
    case Input_T :
        if (input.shift)
        {
            partial = bind(&AdvancedTextEditor::untilPrevious, this, placeholders::_1);
        }
        else
        {
            partial = bind(&AdvancedTextEditor::untilNext, this, placeholders::_1);
        }
        break;
    case Input_U :
        if (input.control)
        {
            scrollUp(visualLines() / 2);
        }
        else if (input.shift)
        {
            i32 start = selectionStart();
            i32 end = selectionEnd();
            enterCommandMode();
            for (i32 i = start; i < end; i++)
            {
                char c = characterAt(i);

                if (c >= 'a' && c <= 'z')
                {
                    BasicTextEditor::replace(i, c - ('a' - 'A'));
                }
            }
        }
        else
        {
            i32 start = selectionStart();
            i32 end = selectionEnd();
            enterCommandMode();
            for (i32 i = start; i < end; i++)
            {
                char c = characterAt(i);

                if (c >= 'A' && c <= 'Z')
                {
                    BasicTextEditor::replace(i, c + ('a' - 'A'));
                }
            }
        }
        break;
    case Input_Left_Brace :
        if (input.shift)
        {
            selectTo(paragraphStart());
        }
        break;
    case Input_Right_Brace :
        if (input.shift)
        {
            selectTo(paragraphEnd());
        }
        break;
    case Input_F :
        if (input.control)
        {
            scrollDown(visualLines());
        }
        else if (input.shift)
        {
            partial = bind(&AdvancedTextEditor::findPrevious, this, placeholders::_1);
        }
        else
        {
            partial = bind(&AdvancedTextEditor::findNext, this, placeholders::_1);
        }
        break;
    case Input_G :
        if (input.shift)
        {
            selectTo(documentEnd());
        }
        else
        {
            partial = bind(&AdvancedTextEditor::misc, this, placeholders::_1);
        }
        break;
    case Input_H :
        if (input.shift)
        {
            selectTo(pageStart());
        }
        else
        {
            selectLeft();
        }
        break;
    case Input_J :
        if (input.shift)
        {
            enterCommandMode();
            goTo(nextOccurrence("\n"));
            if (cursorIndex() != static_cast<i32>(state().text.size()) && characterAtCursor() == '\n')
            {
                BasicTextEditor::remove(nextCharacter(), 1);
            }
        }
        else
        {
            selectDown();
        }
        break;
    case Input_K :
        if (!input.shift)
        {
            selectUp();
        }
        break;
    case Input_L :
        if (input.shift)
        {
            selectTo(pageEnd());
        }
        else
        {
            selectRight();
        }
        break;
    case Input_Backslash :
        if (input.shift)
        {
            selectTo(lineStart());
        }
        break;
    case Input_B :
        if (input.control)
        {
            scrollUp(visualLines());
        }
        else
        {
            selectTo(wordStart());
        }
        break;
    case Input_N :
        if (!search.empty())
        {
            if (input.shift)
            {
                selectTo(searchForwards ? previousOccurrence(search) : nextOccurrence(search));
            }
            else
            {
                selectTo(searchForwards ? nextOccurrence(search) : previousOccurrence(search));
            }
        }
        break;
    case Input_M :
        if (input.shift)
        {
            selectTo(pageMiddle());
        }
        break;
    case Input_Slash :
        if (input.shift)
        {
            enterSearchMode(true);
        }
        else
        {
            enterSearchMode();
        }
        break;
    case Input_Left_Mouse :
        startSelect(local);
        break;
    case Input_Backspace :
        selectLeft();
        break;
    case Input_Return :
        selectTo(softLineStart(cursorLine() + 1));
        break;
    case Input_Space :
        selectRight();
        break;
    case Input_Left :
        if (input.shift)
        {
            selectTo(wordStart());
        }
        else
        {
            selectLeft();
        }
        break;
    case Input_Right :
        if (input.shift)
        {
            selectTo(nextWordStart());
        }
        else
        {
            selectRight();
        }
        break;
    case Input_Up :
        selectUp();
        break;
    case Input_Down :
        selectDown();
        break;
    case Input_Home :
        selectTo(pageStart());
        break;
    case Input_End :
        selectTo(pageEnd());
        break;
    case Input_Page_Up :
        scrollUp(visualLines());
        break;
    case Input_Page_Down :
        scrollDown(visualLines());
        break;
    case Input_Num_Add :
        selectTo(softLineStart(cursorLine() + 1));
        break;
    case Input_Num_Subtract :
        selectTo(softLineStart(cursorLine() - 1));
        break;
    case Input_Num_Multiply :
        searchForwards = true;
        search = wordAtCursor();
        selectTo(previousOccurrence(search));
        break;
    case Input_Num_Return :
        selectTo(softLineStart(cursorLine() + 1));
        break;
    case Input_Escape :
        enterCommandMode();
        break;
    case Input_Tab :
        enterCommandMode();
        break;
    case Input_V :
        if (!input.shift)
        {
            enterCommandMode();
        }
        break;
    case Input_R :
        if (input.shift)
        {
            cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
            enterReplaceMode();
        }
        else
        {
            partial = bind(
                static_cast<void (AdvancedTextEditor::*)(const Input&)>(&AdvancedTextEditor::replace),
                this,
                placeholders::_1
            );
        }
        break;
    case Input_D :
        if (input.shift)
        {
            cutRegion(input, lineStart(selectionLineStart()), lineEnd(selectionLineEnd()) - lineStart(selectionLineStart()));
        }
        else
        {
            cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
        }
        enterCommandMode();
        break;
    case Input_S :
        if (input.shift)
        {
            i32 indent = lineIndent(selectionLineStart());
            cutRegion(input, lineStart(selectionLineStart()), lineEnd(selectionLineEnd()) - lineStart(selectionLineStart()));
            indentLine(cursorLine(), indent);
        }
        else
        {
            cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
        }
        enterInsertMode();
        break;
    case Input_C :
        if (input.shift)
        {
            i32 indent = lineIndent(selectionLineStart());
            cutRegion(input, lineStart(selectionLineStart()), lineEnd(selectionLineEnd()) - lineStart(selectionLineStart()));
            indentLine(cursorLine(), indent);
        }
        else
        {
            cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
        }
        enterInsertMode();
        break;
    case Input_X :
        if (input.shift)
        {
            cutRegion(input, lineStart(selectionLineStart()), lineEnd(selectionLineEnd()) - lineStart(selectionLineStart()));
        }
        else
        {
            cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
        }
        enterCommandMode();
        break;
    case Input_Y :
        if (input.shift)
        {
            copyRegion(input, lineStart(selectionLineStart()), lineEnd(selectionLineEnd()) - lineStart(selectionLineStart()));
        }
        else
        {
            copyRegion(input, selectionStart(), selectionEnd() - selectionStart());
        }
        enterCommandMode();
        break;
    case Input_Comma :
        if (input.shift)
        {
            enterCommandMode();
            i32 start = selectionLineStart();
            i32 end = selectionLineEnd();
            for (i32 i = start; i <= end; i++)
            {
                unindentLine(i, 1);
            }
        }
        break;
    case Input_Period :
        if (input.shift)
        {
            enterCommandMode();
            i32 start = selectionLineStart();
            i32 end = selectionLineEnd();
            for (i32 i = start; i <= end; i++)
            {
                indentLine(i, 1);
            }
        }
        break;
    case Input_O :
        invertSelection();
        break;
    case Input_P :
    {
        string selection = state().text.substr(selectionStart(), selectionEnd() - selectionStart());
        BasicTextEditor::remove(selectionStart(), selectionEnd() - selectionStart());
        pasteClipboard();
        setClipboard(input, selection);
        enterCommandMode();
        break;
    }
    case Input_Delete :
        if (state().select != state().cursor)
        {
            cutRegion(input, selectionStart(), selectionEnd() - selectionStart());
        }
        else
        {
            cutRegion(input, nextCharacter(), 1);
        }
        enterCommandMode();
        break;
    }

    i32 oldCursorIndex = positionToIndex(old);
    i32 newCursorIndex = positionToIndex(state().cursor);
    i32 selectIndex = positionToIndex(state().select);

    if (mode == Text_Editor_Visual_Mode && input.type != Input_Left_Mouse)
    {
        if (oldCursorIndex < selectIndex && newCursorIndex >= selectIndex)
        {
            updateState(
                [&](TextEditorState& state) -> void
                {
                    state.select.x--;
                }
            );
        }
        else if (oldCursorIndex > selectIndex && newCursorIndex <= selectIndex)
        {
            updateState(
                [&](TextEditorState& state) -> void
                {
                    state.select.x++;
                }
            );
        }
    }
}

void AdvancedTextEditor::searchModeInput(const Input& input)
{
    if (input.up)
    {
        return;
    }

    switch (input.type)
    {
    default :
    {
        optional<char> symbol = this->symbol(input);

        if (symbol)
        {
            search += *symbol;
        }
        break;
    }
    case Input_Tab :
        search += string(indentSize, ' ');
        break;
    case Input_Backspace :
        if (!search.empty())
        {
            search = search.substr(0, static_cast<i32>(search.size()) - 1);
        }
        break;
    case Input_Escape :
        enterCommandMode();
        break;
    case Input_Return :
    case Input_Num_Return :
        enterCommandMode();
        goTo(searchForwards ? nextOccurrence(search) : previousOccurrence(search));
        break;
    }
}

void AdvancedTextEditor::replace(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }

    optional<char> symbol = this->symbol(input);

    if (!symbol)
    {
        return;
    }

    if (mode != Text_Editor_Visual_Mode)
    {
        BasicTextEditor::replace(cursorIndex(), *symbol);
    }
    else
    {
        for (i32 i = selectionStart(); i < selectionEnd(); i++)
        {
            BasicTextEditor::replace(i, *symbol);
        }
        enterCommandMode();
    }

    partial = nullptr;
}

void AdvancedTextEditor::yank(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }
    else if (!input.shift && input.type == Input_Y)
    {
        copyRegion(input, lineStart(), (lineEnd() + (cursorLine() + 1 != lines() ? 1 : 0)) - lineStart());
        partial = nullptr;
        return;
    }

    optional<i32> motion = this->endPoint(input);

    if (!motion)
    {
        return;
    }

    i32 a = cursorIndex();
    i32 b = *motion;

    if (a < b)
    {
        copyRegion(input, a, b - a);
    }
    else
    {
        copyRegion(input, b, a - b);
    }

    partial = nullptr;
}

void AdvancedTextEditor::untilNext(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }

    optional<char> symbol = this->symbol(input);

    if (!symbol)
    {
        return;
    }

    i32 index = nextOccurrence(string() + *symbol);

    if (index == cursorIndex())
    {
        return;
    }

    if (mode != Text_Editor_Visual_Mode)
    {
        goTo(index - 1);
    }
    else
    {
        selectTo(index);
    }

    partial = nullptr;
}

void AdvancedTextEditor::untilPrevious(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }

    optional<char> symbol = this->symbol(input);

    if (!symbol)
    {
        return;
    }

    i32 index = previousOccurrence(string() + *symbol);

    if (index == cursorIndex())
    {
        return;
    }

    if (mode != Text_Editor_Visual_Mode)
    {
        goTo(index + 1);
    }
    else
    {
        selectTo(index + 1);
    }

    partial = nullptr;
}

void AdvancedTextEditor::findNext(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }

    optional<char> symbol = this->symbol(input);

    if (!symbol)
    {
        return;
    }

    i32 index = nextOccurrence(string() + *symbol);

    if (index == cursorIndex())
    {
        return;
    }

    if (mode != Text_Editor_Visual_Mode)
    {
        goTo(index);
    }
    else
    {
        selectTo(index + 1);
    }

    partial = nullptr;
}

void AdvancedTextEditor::findPrevious(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }

    optional<char> symbol = this->symbol(input);

    if (!symbol)
    {
        return;
    }

    i32 index = previousOccurrence(string() + *symbol);

    if (index == cursorIndex())
    {
        return;
    }

    if (mode != Text_Editor_Visual_Mode)
    {
        goTo(index);
    }
    else
    {
        selectTo(index);
    }

    partial = nullptr;
}

void AdvancedTextEditor::remove(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }
    else if (!input.shift && input.type == Input_D)
    {
        cutRegion(input, lineStart(), (lineEnd() + (cursorLine() + 1 != lines() ? 1 : 0)) - lineStart());
        partial = nullptr;
        return;
    }

    optional<i32> motion = this->endPoint(input);

    if (!motion)
    {
        return;
    }

    i32 a = cursorIndex();
    i32 b = *motion;

    if (a < b)
    {
        cutRegion(input, a, b - a);
    }
    else
    {
        cutRegion(input, b, a - b);
    }

    partial = nullptr;
}

void AdvancedTextEditor::misc(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }

    if (input.type == Input_G)
    {
        if (!input.shift)
        {
            if (mode != Text_Editor_Visual_Mode)
            {
                goTo(documentStart());
            }
            else
            {
                selectTo(documentStart());
            }
        }
    }

    partial = nullptr;
}

void AdvancedTextEditor::change(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }
    else if (!input.shift && input.type == Input_C)
    {
        cutRegion(input, lineStart(), lineEnd() - lineStart());
        enterInsertMode();
        partial = nullptr;
        return;
    }

    optional<i32> motion = this->endPoint(input);

    if (!motion)
    {
        return;
    }

    i32 a = cursorIndex();
    i32 b = *motion;

    if (a < b)
    {
        cutRegion(input, a, b - a);
        enterInsertMode();
    }
    else
    {
        cutRegion(input, b, a - b);
        enterInsertMode();
    }

    partial = nullptr;
}

void AdvancedTextEditor::indent(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }
    else if (input.shift && input.type == Input_Period)
    {
        indentLine(cursorLine(), 1);
        partial = nullptr;
        return;
    }

    optional<i32> motion = this->endPoint(input);

    if (!motion)
    {
        return;
    }

    i32 a = cursorIndex();
    i32 b = *motion;

    if (a < b)
    {
        i32 firstLine = indexToPosition(a).y;
        i32 lastLine = indexToPosition(b).y;

        for (i32 i = firstLine; i <= lastLine; i++)
        {
            indentLine(i, 1);
        }
    }
    else
    {
        i32 firstLine = indexToPosition(b).y;
        i32 lastLine = indexToPosition(a).y;

        for (i32 i = firstLine; i <= lastLine; i++)
        {
            indentLine(i, 1);
        }
    }

    partial = nullptr;
}

void AdvancedTextEditor::unindent(const Input& input)
{
    if (input.type == Input_Escape)
    {
        partial = nullptr;
        return;
    }
    else if (input.shift && input.type == Input_Comma)
    {
        unindentLine(cursorLine(), 1);
        partial = nullptr;
        return;
    }

    optional<i32> motion = this->endPoint(input);

    if (!motion)
    {
        return;
    }

    i32 a = cursorIndex();
    i32 b = *motion;

    if (a < b)
    {
        i32 firstLine = indexToPosition(a).y;
        i32 lastLine = indexToPosition(b).y;

        for (i32 i = firstLine; i <= lastLine; i++)
        {
            unindentLine(i, 1);
        }
    }
    else
    {
        i32 firstLine = indexToPosition(b).y;
        i32 lastLine = indexToPosition(a).y;

        for (i32 i = firstLine; i <= lastLine; i++)
        {
            unindentLine(i, 1);
        }
    }

    partial = nullptr;
}

optional<i32> AdvancedTextEditor::endPoint(const Input& input)
{
    if (subPartial)
    {
        return subPartial(input);
    }

    Point local = pointer - position();

    switch (input.type)
    {
    default :
        break;
    case Input_Left_Mouse :
        return screenToIndex(local);
    case Input_3 :
        if (input.shift)
        {
            return previousOccurrence(search);
        }
        break;
    case Input_4 :
        if (input.shift)
        {
            return lineEnd();
        }
        break;
    case Input_5 :
        if (input.shift)
        {
            return matchingBracket() + 1;
        }
        break;
    case Input_6 :
        if (input.shift)
        {
            return softLineStart();
        }
        break;
    case Input_8 :
        if (input.shift)
        {
            return nextOccurrence(wordAtCursor()) + static_cast<i32>(wordAtCursor().size());
        }
        break;
    case Input_9 :
        if (input.shift)
        {
            return sentenceStart();
        }
        break;
    case Input_0 :
        if (input.shift)
        {
            return sentenceEnd() + 1;
        }
        else
        {
            return lineStart();
        }
    case Input_Minus :
        if (input.shift)
        {
            return softLineStart();
        }
        else
        {
            return lineStart(cursorLine() - 1);
        }
    case Input_Equals :
        if (input.shift)
        {
            return softLineStart(cursorLine() + 1);
        }
        break;
    case Input_A :
        if (!input.shift)
        {
            subPartial = [this](const Input& input) -> optional<i32>
            {
                switch (input.type)
                {
                default :
                    return {};
                case Input_W :
                    if (!input.shift)
                    {
                        goTo(wordStart());
                        return wordEnd();
                    }
                    else
                    {
                        return {};
                    }
                }
            };
        }
        break;
    case Input_W :
        return wordEnd();
    case Input_E :
        return wordEnd();
    case Input_T :
        if (input.shift)
        {
            subPartial = [this](const Input& input) -> optional<i32>
            {
                optional<char> symbol = this->symbol(input);

                if (!symbol)
                {
                    return {};
                }

                subPartial = nullptr;
                return previousOccurrence(string() + *symbol) + 1;
            };
        }
        else
        {
            subPartial = [this](const Input& input) -> optional<i32>
            {
                optional<char> symbol = this->symbol(input);

                if (!symbol)
                {
                    return {};
                }

                subPartial = nullptr;
                return (nextOccurrence(string() + *symbol) - 1) + 1;
            };
        }
        break;
    case Input_Left_Brace :
        return paragraphStart();
    case Input_Right_Brace :
        return paragraphEnd() + 1;
    case Input_F :
        if (input.shift)
        {
            subPartial = [this](const Input& input) -> optional<i32>
            {
                optional<char> symbol = this->symbol(input);

                if (!symbol)
                {
                    return {};
                }

                subPartial = nullptr;
                return previousOccurrence(string() + *symbol);
            };
        }
        else
        {
            subPartial = [this](const Input& input) -> optional<i32>
            {
                optional<char> symbol = this->symbol(input);

                if (!symbol)
                {
                    return {};
                }

                subPartial = nullptr;
                return nextOccurrence(string() + *symbol) + 1;
            };
        }
        break;
    case Input_G :
        if (input.shift)
        {
            return documentEnd();
        }
        else
        {
            subPartial = [this](const Input& input) -> optional<i32>
            {
                switch (input.type)
                {
                default :
                    return {};
                case Input_G :
                    if (!input.shift)
                    {
                        return documentStart();
                    }
                    else
                    {
                        return {};
                    }
                }
            };
        }
        break;
    case Input_H :
        if (input.shift)
        {
            return pageStart();
        }
        else
        {
            return previousCharacter();
        }
    case Input_J :
        return lineEnd(cursorLine() + 1);
    case Input_K :
        return lineStart(cursorLine() - 1);
    case Input_L :
        if (input.shift)
        {
            return pageEnd();
        }
        else
        {
            return nextCharacter() + 1;
        }
    case Input_Backslash :
        if (input.shift)
        {
            lineStart();
        }
        break;
    case Input_B :
        return wordStart();
    case Input_N :
        return !input.shift && searchForwards
            ? nextOccurrence(search) + 1
            : previousOccurrence(search);
    case Input_M :
        return pageMiddle();
    case Input_Left :
        return previousCharacter();
    case Input_Right :
        return nextCharacter() + 1;
    case Input_Up :
        return lineStart(cursorLine() - 1);
    case Input_Down :
        return lineEnd(cursorLine() + 1);
    case Input_Backspace :
        return previousCharacter();
    case Input_Space :
        return nextCharacter() + 1;
    case Input_Return :
        return softLineStart(cursorLine() + 1);
    case Input_Home :
        return lineStart();
    case Input_End :
        return lineEnd();
    case Input_Page_Up :
        return pageStart();
    case Input_Page_Down :
        return pageEnd();
    case Input_Num_Add :
        return softLineStart(cursorLine() + 1);
    case Input_Num_Subtract :
        return softLineStart(cursorLine() - 1);
    case Input_Num_Multiply :
        return nextOccurrence(wordAtCursor()) + static_cast<i32>(wordAtCursor().size());
    case Input_Num_Return :
        return softLineStart(cursorLine() + 1);
    }

    return {};
}

optional<char> AdvancedTextEditor::symbol(const Input& input) const
{
    if (input.type >= Input_A && input.type <= Input_Z)
    {
        if (input.shift)
        {
            return 'A' + (input.type - Input_A);
        }
        else
        {
            return 'a' + (input.type - Input_A);
        }
    }
    else if (input.type >= Input_0 && input.type <= Input_9)
    {
        if (input.shift)
        {
            switch (input.type)
            {
            default :
                return '\0';
            case Input_1 :
                return '!';
            case Input_2 :
                return '@';
            case Input_3 :
                return '#';
            case Input_4 :
                return '$';
            case Input_5 :
                return '%';
            case Input_6 :
                return '^';
            case Input_7 :
                return '&';
            case Input_8 :
                return '*';
            case Input_9 :
                return '(';
            case Input_0 :
                return ')';
            }
        }
        else
        {
            return '0' + (input.type - Input_0);
        }
    }
    else if (input.type == Input_Grave_Accent)
    {
        return !input.shift ? '`' : '~';
    }
    else if (input.type == Input_Minus)
    {
        return !input.shift ? '-' : '_';
    }
    else if (input.type == Input_Equals)
    {
        return !input.shift ? '=' : '+';
    }
    else if (input.type == Input_Left_Brace)
    {
        return !input.shift ? '[' : '{';
    }
    else if (input.type == Input_Right_Brace)
    {
        return !input.shift ? ']' : '}';
    }
    else if (input.type == Input_Backslash)
    {
        return !input.shift ? '\\' : '|';
    }
    else if (input.type == Input_Semicolon)
    {
        return !input.shift ? ';' : ':';
    }
    else if (input.type == Input_Quote)
    {
        return !input.shift ? '\'' : '"';
    }
    else if (input.type == Input_Comma)
    {
        return !input.shift ? ',' : '<';
    }
    else if (input.type == Input_Period)
    {
        return !input.shift ? '.' : '>';
    }
    else if (input.type == Input_Slash)
    {
        return !input.shift ? '/' : '?';
    }
    else if (input.type == Input_Space)
    {
        return ' ';
    }
    else if (input.type == Input_Return)
    {
        return '\n';
    }
    else if (input.type == Input_Num_Add)
    {
        return '+';
    }
    else if (input.type == Input_Num_Subtract)
    {
        return '-';
    }
    else if (input.type == Input_Num_Multiply)
    {
        return '*';
    }
    else if (input.type == Input_Num_Divide)
    {
        return '/';
    }
    else if (input.type == Input_Num_Period)
    {
        return '.';
    }
    else if (input.type >= Input_Num_0 && input.type <= Input_Num_9)
    {
        return '0' + (input.type - Input_Num_0);
    }
    else
    {
        return {};
    }
}

void AdvancedTextEditor::enterCommandMode()
{
    if (mode == Text_Editor_Visual_Mode)
    {
        updateState(
            [&](TextEditorState& state) -> void
            {
                state.selected = false;
            }
        );
    }
    else if (mode == Text_Editor_Insert_Mode)
    {
        cursorLeft();
    }

    mode = Text_Editor_Command_Mode;
}

void AdvancedTextEditor::enterInsertMode()
{
    mode = Text_Editor_Insert_Mode;
}

void AdvancedTextEditor::enterReplaceMode()
{
    mode = Text_Editor_Replace_Mode;
}

void AdvancedTextEditor::enterVisualMode()
{
    mode = Text_Editor_Visual_Mode;

    updateState(
        [&](TextEditorState& state) -> void
        {
            state.selected = true;
            state.select = state.cursor;
        }
    );

    selectTo(nextCharacter() + 1);
    invertSelection();
}

void AdvancedTextEditor::enterSearchMode(bool reverse)
{
    mode = Text_Editor_Search_Mode;

    searchForwards = !reverse;
    search = "";
}

void AdvancedTextEditor::invertSelection()
{
    updateState(
        [&](TextEditorState& state) -> void
        {
            swap(state.cursor.x, state.select.x);
            swap(state.cursor.y, state.select.y);
        }
    );
}

void AdvancedTextEditor::indentLine(i32 index, i32 indent)
{
    add(lineStart(index), string(indent * indentSize, ' '));
}

void AdvancedTextEditor::unindentLine(i32 index, i32 indent)
{
    BasicTextEditor::remove(lineStart(index), min(indent, lineIndent(index)) * indentSize);
}

i32 AdvancedTextEditor::paragraphStart() const
{
    for (i32 i = 0; i < static_cast<i32>(state().text.size()); i++)
    {
        char c = state().text[i];

        if (c == '(' && matchingBracket(i) >= cursorIndex())
        {
            return i;
        }
    }

    return cursorIndex();
}

i32 AdvancedTextEditor::paragraphEnd() const
{
    for (i32 i = static_cast<i32>(state().text.size()) - 1; i >= 0; i--)
    {
        char c = state().text[i];

        if (c == ')' && matchingBracket(i) < cursorIndex())
        {
            return i;
        }
    }

    return cursorIndex();
}

i32 AdvancedTextEditor::sentenceStart() const
{
    return previousOccurrence("(");
}

i32 AdvancedTextEditor::sentenceEnd() const
{
    return nextOccurrence(")");
}

i32 AdvancedTextEditor::matchingBracket(i32 index) const
{
    if (index < 0)
    {
        index = cursorIndex();
    }

    if (characterAtCursor() == '(')
    {
        i32 depth = 1;

        for (i32 i = index + 1; i < static_cast<i32>(state().text.size()); i++)
        {
            char c = state().text[i];

            if (c == '(')
            {
                depth++;
            }
            else if (c == ')')
            {
                depth--;
            }

            if (depth == 0)
            {
                return i;
            }
        }
    }
    else if (characterAtCursor() == ')')
    {
        i32 depth = 1;

        for (i32 i = index - 1; i >= 0; i--)
        {
            char c = state().text[i];

            if (c == ')')
            {
                depth++;
            }
            else if (c == '(')
            {
                depth--;
            }

            if (depth == 0)
            {
                return i;
            }
        }
    }

    return index;
}

i32 AdvancedTextEditor::previousOccurrence(const string& pattern) const
{
    const string& text = state().text;

    for (i32 i = cursorIndex(); i >= static_cast<i32>(pattern.size()); i--)
    {
        if (text.substr(i - static_cast<i32>(pattern.size()), static_cast<i32>(pattern.size())) == pattern)
        {
            return i - static_cast<i32>(pattern.size());
        }
    }

    for (i32 i = static_cast<i32>(text.size()); i >= static_cast<i32>(pattern.size()); i--)
    {
        if (text.substr(i - static_cast<i32>(pattern.size()), static_cast<i32>(pattern.size())) == pattern)
        {
            return i - static_cast<i32>(pattern.size());
        }
    }

    return cursorIndex();
}

i32 AdvancedTextEditor::nextOccurrence(const string& pattern) const
{
    const string& text = state().text;

    for (i32 i = cursorIndex() + 1; i <= static_cast<i32>(text.size()) - static_cast<i32>(pattern.size()); i++)
    {
        if (text.substr(i, static_cast<i32>(pattern.size())) == pattern)
        {
            return i;
        }
    }

    for (i32 i = 0; i <= static_cast<i32>(text.size()) - static_cast<i32>(pattern.size()); i++)
    {
        if (text.substr(i, static_cast<i32>(pattern.size())) == pattern)
        {
            return i;
        }
    }

    return cursorIndex();
}

vector<tuple<i32, i32>> AdvancedTextEditor::allSearchResults() const
{
    vector<tuple<i32, i32>> results;

    for (i32 i = 0; i <= static_cast<i32>(state().text.size()) - static_cast<i32>(search.size()); i++)
    {
        if (state().text.substr(i, static_cast<i32>(search.size())) == search)
        {
            results.push_back({ i, static_cast<i32>(search.size()) });
        }
    }

    return results;
}

void AdvancedTextEditor::endCommandModeSelection(const Input& input, const Point& local)
{
    endSelect(input, local);
    if (state().select.x != state().cursor.x || state().select.y != state().cursor.y)
    {
        // Special entrance to visual mode
        mode = Text_Editor_Visual_Mode;
    }
}

void AdvancedTextEditor::endVisualModeSelection(const Input& input, const Point& local)
{
    endSelect(input, local);
    if (state().select == state().cursor)
    {
        enterCommandMode();
    }
}

SizeProperties AdvancedTextEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
