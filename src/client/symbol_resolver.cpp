/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "symbol_resolver.hpp"
#include "tools.hpp"
#include "log.hpp"
#include "paths.hpp"

static constexpr chrono::milliseconds cacheTimeout(10 * 1000);

SymbolResolverKey::SymbolResolverKey(SymbolType type, u32 resolution)
    : type(type)
    , resolution(resolution)
{

}

bool SymbolResolverKey::operator==(const SymbolResolverKey& rhs) const
{
    return type == rhs.type && resolution == rhs.resolution;
}

bool SymbolResolverKey::operator!=(const SymbolResolverKey& rhs) const
{
    return !(*this == rhs);
}

bool SymbolResolverKey::operator>(const SymbolResolverKey& rhs) const
{
    if (type > rhs.type)
    {
        return true;
    }
    else if (type < rhs.type)
    {
        return false;
    }
    else
    {
        if (resolution < rhs.resolution)
        {
            return true;
        }
        else if (resolution > rhs.resolution)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool SymbolResolverKey::operator<(const SymbolResolverKey& rhs) const
{
    if (type < rhs.type)
    {
        return true;
    }
    else if (type > rhs.type)
    {
        return false;
    }
    else
    {
        if (resolution < rhs.resolution)
        {
            return true;
        }
        else if (resolution > rhs.resolution)
        {
            return false;
        }
        else
        {
            return false;
        }
    }
}

bool SymbolResolverKey::operator>=(const SymbolResolverKey& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool SymbolResolverKey::operator<=(const SymbolResolverKey& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering SymbolResolverKey::operator<=>(const SymbolResolverKey& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

SymbolResolver::SymbolResolver()
{

}

SymbolResolver::~SymbolResolver()
{
    for (const auto& [ key, image ] : cachedImages)
    {
        delete image.get();
    }

    for (const auto& [ type, symbol ] : cachedSymbols)
    {
        delete symbol.get();
    }
}

Image* SymbolResolver::resolve(SymbolType type, u32 resolution) const
{
    auto it = cachedImages.find(SymbolResolverKey(type, resolution));

    if (it != cachedImages.end())
    {
        return it->second.get()->copy();
    }

    auto it2 = cachedSymbols.find(type);

    lunasvg::Document* document;

    if (it2 != cachedSymbols.end())
    {
        document = it2->second.get();
    }
    else
    {
        document = loadSymbol(type);
        cachedSymbols.insert_or_assign(type, SymbolResolverCacheEntry(document, cacheTimeout));
    }

    Image* image = renderSymbol(document, resolution);
    cachedImages.insert_or_assign(SymbolResolverKey(type, resolution), SymbolResolverCacheEntry(image, cacheTimeout));

    clean();

    return image->copy();
}

lunasvg::Document* SymbolResolver::loadSymbol(SymbolType type) const
{
    string file = getFile(getSymbolPath(type));

    lunasvg::Document* document = lunasvg::Document::loadFromData(file).release();

    if (!document)
    {
        fatal("Failed to load SVG '" + getSymbolPath(type) + "'.");
    }

    return document;
}

Image* SymbolResolver::renderSymbol(lunasvg::Document* document, u32 resolution) const
{
    lunasvg::Bitmap bitmap = document->renderToBitmap(resolution, resolution);

    bitmap.convert(2, 1, 0, 3, true);

    const u8* data = bitmap.data();

    auto pixels = new u8[resolution * resolution * sizeof(u32)];

    memcpy(pixels, data, resolution * resolution * sizeof(u32));

    return new Image(resolution, resolution, sizeof(u32), pixels);
}

string SymbolResolver::getSymbolPath(SymbolType type) const
{
    switch (type)
    {
    case Symbol_Star :
        return symbolPath + "/star" + symbolExt;
    default :
        fatal("Encountered unknown body symbol type '" + to_string(type) + "'.");
    }
}

void SymbolResolver::clean() const
{
    for (auto it = cachedSymbols.begin(); it != cachedSymbols.end();)
    {
        if (it->second.expired())
        {
            delete it->second.get();
            cachedSymbols.erase(it++);
        }
        else
        {
            it++;
        }
    }

    for (auto it = cachedImages.begin(); it != cachedImages.end();)
    {
        if (it->second.expired())
        {
            delete it->second.get();
            cachedImages.erase(it++);
        }
        else
        {
            it++;
        }
    }
}

SymbolResolver* symbolResolver = nullptr;
