#pragma once

#include "types.hpp"
#include "interface_types.hpp"

struct InterfaceMarker
{
    InterfaceMarker();
    InterfaceMarker(
        Icon type,
        string name,
        vec3 position,
        vec3 color
    );

    Icon type;
    string name;
    vec3 position;
    vec3 color;
};
