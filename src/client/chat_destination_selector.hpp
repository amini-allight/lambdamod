/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "popup.hpp"
#include "chat_destination.hpp"

class ChatDestinationSelectorPopup : public Popup
{
public:
    ChatDestinationSelectorPopup(
        InterfaceWidget* parent,
        const optional<ChatDestination>& destination,
        const vector<tuple<string, ChatDestination>>& destinations,
        const function<void(size_t)>& onSet
    );

    void step() override;
    void draw(const DrawContext& ctx) const override;

private:
    friend class ChatDestinationSelector;

    const optional<ChatDestination>& destination;
    const vector<tuple<string, ChatDestination>>& destinations;
    function<void(size_t)> onSet;
    i32 scrollIndex;
    i32 activeIndex;

    size_t maxScrollIndex() const;

    void interact(const Input& input);
    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);

    SizeProperties sizeProperties() const override;
};

class ChatDestinationSelector : public InterfaceWidget
{
public:
    ChatDestinationSelector(
        InterfaceWidget* parent,
        const function<void(const optional<ChatDestination>&)>& onSet
    );
    ~ChatDestinationSelector();

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;
    void focus() override;
    void unfocus() override;

private:
    function<void(const optional<ChatDestination>&)> onSet;

    optional<ChatDestination> destination;
    vector<tuple<string, ChatDestination>> destinations;

    ChatDestinationSelectorPopup* popup;

    vector<tuple<string, ChatDestination>> allDestinations() const;

    void onSelect(size_t index);

    void interact(const Input& input);

    SizeProperties sizeProperties() const override;
};
