/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "vr_panel_input_scope.hpp"

class InterfaceWidget;

class VRMenuInputScope : public VRPanelInputScope
{
public:
    VRMenuInputScope(
        InterfaceWidget* widget,
        InputScope* parent,
        const string& name,
        InputScopeMode mode,
        const function<optional<Point>(const vec3&, const quaternion&)>& toLocal,
        const function<void(InputScope*)>& block
    );

    bool movePointer(PointerType type, const Point& pointer, bool consumed) const override;
    bool moveLeftHand(const vec3& position, const quaternion& rotation, bool consumed) const override;
    bool moveRightHand(const vec3& position, const quaternion& rotation, bool consumed) const override;

private:
    bool isTargeted(const Input& input) const override;
};

#define START_VR_MENU_SCOPE(_parent, _name) \
setScope<VRMenuInputScope>(this, _parent, _name, Input_Scope_Normal, bind(static_cast<optional<Point> (std::decay_t<decltype(*this)>::*)(const vec3&, const quaternion&) const>(&std::decay_t<decltype(*this)>::toLocal), this, placeholders::_1, placeholders::_2), [&](InputScope* parent) -> void {
#define START_BLOCKING_VR_MENU_SCOPE(_parent, _name) \
setScope<VRMenuInputScope>(this, _parent, _name, Input_Scope_Blocking, bind(static_cast<optional<Point> (std::decay_t<decltype(*this)>::*)(const vec3&, const quaternion&) const>(&std::decay_t<decltype(*this)>::toLocal), this, placeholders::_1, placeholders::_2), [&](InputScope* parent) -> void {
#endif
