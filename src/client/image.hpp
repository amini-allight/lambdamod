/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

struct Image
{
    Image(u32 width, u32 height, u32 sampleSize, u8* pixels, u32 pitch = 0);
    Image(const Image& image) = delete;
    Image(Image&& rhs) = delete;
    ~Image();

    Image& operator=(const Image& image) = delete;
    Image& operator=(Image&& rhs) = delete;

    size_t size() const;
    Image* copy() const;

    u32 width() const;
    u32 height() const;
    u32 sampleSize() const;
    u32 pitch() const;
    const u8* pixels() const;

private:
    u32 _width;
    u32 _height;
    u32 _sampleSize;
    u8* _pixels;
    u32 _pitch;
};

Image* loadPNG(const string& name);
Image* emptyImage(u32 sampleSize);
