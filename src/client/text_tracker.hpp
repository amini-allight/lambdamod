/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "text.hpp"

class TextTracker
{
public:
    explicit TextTracker(TextID id);
    template<typename T>
    TextTracker(TextID id, const T& data);

    TextID id() const;
    TextType type() const;
    template<typename T>
    const T& data() const
    {
        return get<T>(_data);
    }

    void start();
    bool matches(const TextResponse& response) const;
    bool timedOut() const;
    chrono::milliseconds elapsed() const;

    TextResponse response() const;

private:
    TextID _id;
    TextType _type;
    variant<
        TextTitlecardRequest,
        TextTimeoutRequest,
        TextUnaryRequest,
        TextBinaryRequest,
        TextOptionsRequest,
        TextChooseRequest,
        TextAssignValuesRequest,
        TextSpendPointsRequest,
        TextAssignPointsRequest,
        TextVoteRequest,
        TextChooseMajorRequest
    > _data;
    chrono::milliseconds startTime;
};

template<>
TextTracker::TextTracker(TextID id, const TextTitlecardRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextTimeoutRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextUnaryRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextBinaryRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextOptionsRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextChooseRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextAssignValuesRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextSpendPointsRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextAssignPointsRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextVoteRequest& request);

template<>
TextTracker::TextTracker(TextID id, const TextChooseMajorRequest& request);
