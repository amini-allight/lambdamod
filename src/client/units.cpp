/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "units.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "unit_conversion.hpp"

string lengthSuffix()
{
    return g_config.imperialUnits ? "ft" : "m";
}

string frequencySuffix()
{
    return "Hz";
}

string powerSuffix()
{
    return "W";
}

string timeSuffix()
{
    return "s";
}

string speedSuffix()
{
    return lengthSuffix() + "/" + timeSuffix();
}

string accelerationSuffix()
{
    return lengthSuffix() + "/" + timeSuffix() + "^2";
}

string rotationalSpeedSuffix()
{
    return angleSuffix() + "/" + timeSuffix();
}

string massSuffix()
{
    return g_config.imperialUnits ? "lb" : "kg";
}

string volumeSuffix()
{
    return lengthSuffix() + "^3";
}

string densitySuffix()
{
    return massSuffix() + "/" + volumeSuffix();
}

string angleSuffix()
{
    return g_config.degrees ? "deg" : "rad";
}

string luminousFluxSuffix()
{
    return "lm";
}

string illuminanceSuffix()
{
    return "lx";
}

string forceSuffix()
{
    return g_config.imperialUnits ? "lbf" : "N";
}

string torqueSuffix()
{
    return g_config.imperialUnits ? "lbf*ft" : "N*m";
}

string massRateSuffix()
{
    return massSuffix() + "/" + timeSuffix();
}

string bytesSuffix()
{
    return "B";
}

string pixelSuffix()
{
    return "px";
}

string percentSuffix()
{
    return "%";
}

f64 localizeLength(f64 metric)
{
    return g_config.imperialUnits ? metersToFeet(metric) : metric;
}

f64 delocalizeLength(f64 value)
{
    return g_config.imperialUnits ? feetToMeters(value) : value;
}

f64 localizeSpeed(f64 metric)
{
    return localizeLength(metric);
}

f64 delocalizeSpeed(f64 value)
{
    return delocalizeLength(value);
}

f64 localizeAcceleration(f64 metric)
{
    return localizeLength(metric);
}

f64 delocalizeAcceleration(f64 value)
{
    return delocalizeLength(value);
}

f64 localizeMass(f64 metric)
{
    return g_config.imperialUnits ? kilogramsToPounds(metric) : metric;
}

f64 delocalizeMass(f64 value)
{
    return g_config.imperialUnits ? poundsToKilograms(value) : value;
}

f64 localizeVolume(f64 metric)
{
    return pow(localizeLength(metric), 3);
}

f64 delocalizeVolume(f64 value)
{
    return pow(delocalizeLength(value), 3);
}

f64 localizeDensity(f64 metric)
{
    return localizeMass(metric) / pow(localizeLength(1), 3);
}

f64 delocalizeDensity(f64 value)
{
    return delocalizeMass((value * pow(localizeLength(1), 3)));
}

f64 localizeForce(f64 metric)
{
    return g_config.imperialUnits ? metric * 0.224808 : metric;
}

f64 delocalizeForce(f64 value)
{
    return g_config.imperialUnits ? value * 4.448222 : value;
}

f64 localizeAngle(f64 radians)
{
    return g_config.degrees ? degrees(radians) : radians;
}

f64 delocalizeAngle(f64 value)
{
    return g_config.degrees ? radians(value) : value;
}

vec2 localizeLength(const vec2& metric)
{
    return vec2(
        localizeLength(metric.x),
        localizeLength(metric.y)
    );
}

vec2 delocalizeLength(const vec2& value)
{
    return vec2(
        delocalizeLength(value.x),
        delocalizeLength(value.y)
    );
}

vec3 localizeLength(const vec3& metric)
{
    return vec3(
        localizeLength(metric.x),
        localizeLength(metric.y),
        localizeLength(metric.z)
    );
}

vec3 delocalizeLength(const vec3& value)
{
    return vec3(
        delocalizeLength(value.x),
        delocalizeLength(value.y),
        delocalizeLength(value.z)
    );
}

vec3 localizeSpeed(const vec3& metric)
{
    return vec3(
        localizeSpeed(metric.x),
        localizeSpeed(metric.y),
        localizeSpeed(metric.z)
    );
}

vec3 delocalizeSpeed(const vec3& value)
{
    return vec3(
        delocalizeSpeed(value.x),
        delocalizeSpeed(value.y),
        delocalizeSpeed(value.z)
    );
}

vec3 localizeAcceleration(const vec3& metric)
{
    return vec3(
        localizeAcceleration(metric.x),
        localizeAcceleration(metric.y),
        localizeAcceleration(metric.z)
    );
}

vec3 delocalizeAcceleration(const vec3& value)
{
    return vec3(
        delocalizeAcceleration(value.x),
        delocalizeAcceleration(value.y),
        delocalizeAcceleration(value.z)
    );
}

vec2 localizeAngle(const vec2& radians)
{
    return vec2(
        localizeAngle(radians.x),
        localizeAngle(radians.y)
    );
}

vec2 delocalizeAngle(const vec2& value)
{
    return vec2(
        delocalizeAngle(value.x),
        delocalizeAngle(value.y)
    );
}

vec3 localizeAngle(const vec3& radians)
{
    return vec3(
        localizeAngle(radians.x),
        localizeAngle(radians.y),
        localizeAngle(radians.z)
    );
}

vec3 delocalizeAngle(const vec3& value)
{
    return vec3(
        delocalizeAngle(value.x),
        delocalizeAngle(value.y),
        delocalizeAngle(value.z)
    );
}
