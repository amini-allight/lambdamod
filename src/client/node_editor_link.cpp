/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "node_editor_link.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "theme.hpp"
#include "node_editor.hpp"
#include "node_editor_node.hpp"
#include "bezier.hpp"

NodeEditorLink::NodeEditorLink(
    InterfaceWidget* parent,
    const NodeEditorNode* src,
    size_t srcSlotIndex,
    const NodeEditorNode* dst,
    size_t dstSlotIndex
)
    : InterfaceWidget(parent)
    , src(src)
    , _srcSlotIndex(srcSlotIndex)
    , dst(dst)
    , _dstSlotIndex(dstSlotIndex)
{

}

ScriptNodeID NodeEditorLink::srcID() const
{
    return src->id();
}

size_t NodeEditorLink::srcSlotIndex() const
{
    return _srcSlotIndex;
}

ScriptNodeID NodeEditorLink::dstID() const
{
    return dst->id();
}

size_t NodeEditorLink::dstSlotIndex() const
{
    return _dstSlotIndex;
}

bool NodeEditorLink::curveContains(const Point& pointer) const
{
    vec2 p0 = start().toVec2();
    vec2 p1 = vec2(end().x, start().y);
    vec2 p2 = vec2(start().x, end().y);
    vec2 p3 = end().toVec2();

    return cubicBezierProximity(p0, p1, p2, p3, pointer.toVec2(), scriptNodeLinkCurveResolution) < UIScale::scriptNodeLinkWidth() / 2;
}

void NodeEditorLink::step()
{
    i32 minX = min(start().x, end().x);
    i32 minY = min(start().y, end().y);
    i32 maxX = max(start().x, end().x);
    i32 maxY = max(start().y, end().y);

    Point point = Point(minX, minY);
    Size size = Size(
        max(maxX - minX, UIScale::scriptNodeLinkWidth()),
        max(maxY - minY, UIScale::scriptNodeLinkWidth())
    );

    move(point);
    resize(size);
}

void NodeEditorLink::draw(const DrawContext& ctx) const
{
    drawCurve(
        ctx,
        parent(),
        start(),
        Point(end().x, start().y),
        Point(start().x, end().y),
        end(),
        UIScale::scriptNodeLinkWidth(),
        focused() ? theme.accentColor : theme.inactiveTextColor
    );
}

Point NodeEditorLink::start() const
{
    return src->position() + get<1>(src->interactionPoints()).at(_srcSlotIndex);
}

Point NodeEditorLink::end() const
{
    return dst->position() + get<0>(dst->interactionPoints()).at(_dstSlotIndex);
}

NodeEditor* NodeEditorLink::editor() const
{
    return dynamic_cast<NodeEditor*>(parent());
}

SizeProperties NodeEditorLink::sizeProperties() const
{
    return {
        static_cast<f64>(size().w), static_cast<f64>(size().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
