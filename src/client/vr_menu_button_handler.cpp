/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_menu_button_handler.hpp"
#include "tools.hpp"

static constexpr chrono::milliseconds holdVRMenuButtonDelay(500);

VRMenuButtonHandler::VRMenuButtonHandler()
    : _vrMenu(false)
    , vrMenuButtons{{ false, chrono::milliseconds(0) }}
{

}

void VRMenuButtonHandler::step()
{
    if (vrMenuButtons[0].started && currentTime() - vrMenuButtons[0].startTime >= holdVRMenuButtonDelay &&
        vrMenuButtons[1].started && currentTime() - vrMenuButtons[1].startTime >= holdVRMenuButtonDelay
    )
    {
        openVRMenu();
        vrMenuButtons[0].started = false;
        vrMenuButtons[1].started = false;
    }
}

bool VRMenuButtonHandler::vrMenu() const
{
    return _vrMenu;
}

void VRMenuButtonHandler::openVRMenu()
{
    _vrMenu = true;
}

void VRMenuButtonHandler::closeVRMenu()
{
    _vrMenu = false;
}

void VRMenuButtonHandler::holdVRMenuButton(bool right, bool held)
{
    if (held && !vrMenuButtons[right].started)
    {
        vrMenuButtons[right].started = true;
        vrMenuButtons[right].startTime = currentTime();
    }
    else
    {
        vrMenuButtons[right].started = false;
    }
}
#endif
