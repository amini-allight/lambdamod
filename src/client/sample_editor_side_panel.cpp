/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_editor_side_panel.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "theme.hpp"
#include "units.hpp"
#include "sample_editor.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "dynamic_label.hpp"
#include "uint_edit.hpp"
#include "ufloat_edit.hpp"
#include "option_select.hpp"

SampleEditorSidePanel::SampleEditorSidePanel(InterfaceWidget* parent)
    : SidePanel(parent)
{
    listView = new ListView(
        this,
        [](size_t index) -> void {},
        theme.backgroundColor
    );
}

void SampleEditorSidePanel::step()
{
    InterfaceWidget::step();

    if (editor()->activeParts.size() != 1 && id != ActionPartID())
    {
        id = SamplePartID();
        clearChildren();
        update();
    }
    else if (editor()->activeParts.size() == 1 && id != *editor()->activeParts.begin())
    {
        id = *editor()->activeParts.begin();
        clearChildren();
        addCommonFields();
        
        switch (editor()->currentPart().type())
        {
        case Sample_Part_Tone :
            addToneFields();
            break;
        }

        update();
    }
}

void SampleEditorSidePanel::addCommonFields()
{
    new EditorEntry(listView, "sample-editor-side-panel-id", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&SampleEditorSidePanel::idSource, this)
        );
    });

    new EditorEntry(listView, "sample-editor-side-panel-start", timeSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SampleEditorSidePanel::startSource, this),
            bind(&SampleEditorSidePanel::onStart, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sample-editor-side-panel-index", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&SampleEditorSidePanel::indexSource, this),
            bind(&SampleEditorSidePanel::onIndex, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sample-editor-side-panel-length", timeSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SampleEditorSidePanel::lengthSource, this),
            bind(&SampleEditorSidePanel::onLength, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sample-editor-side-panel-volume", powerSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SampleEditorSidePanel::volumeSource, this),
            bind(&SampleEditorSidePanel::onVolume, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sample-editor-side-panel-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&SampleEditorSidePanel::typeOptionsSource, this),
            bind(&SampleEditorSidePanel::typeOptionTooltipsSource, this),
            bind(&SampleEditorSidePanel::typeSource, this),
            bind(&SampleEditorSidePanel::onType, this, placeholders::_1)
        );
    });
}

void SampleEditorSidePanel::addToneFields()
{
    new EditorEntry(listView, "sample-editor-side-panel-pitch", frequencySuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SampleEditorSidePanel::pitchSource, this),
            bind(&SampleEditorSidePanel::onPitch, this, placeholders::_1)
        );
    });
}

void SampleEditorSidePanel::clearChildren()
{
    listView->clearChildren();
}

string SampleEditorSidePanel::idSource() const
{
    return to_string(editor()->currentPart().id().value());
}

f64 SampleEditorSidePanel::startSource() const
{
    return editor()->currentPart().start() / 1000.0;
}

void SampleEditorSidePanel::onStart(f64 start)
{
    editor()->edit([this, start](Sample& sample) -> void {
        auto it = sample.parts().find(id);

        if (it == sample.parts().end())
        {
            return;
        }

        SamplePart part = it->second;

        part.setStart(start * 1000);

        sample.add(part);
    });
}

u64 SampleEditorSidePanel::indexSource() const
{
    return editor()->currentPart().index();
}

void SampleEditorSidePanel::onIndex(u64 index)
{
    editor()->edit([this, index](Sample& sample) -> void {
        auto it = sample.parts().find(id);

        if (it == sample.parts().end())
        {
            return;
        }

        SamplePart part = it->second;

        part.setIndex(index);

        sample.add(part);
    });
}

f64 SampleEditorSidePanel::lengthSource() const
{
    return editor()->currentPart().length() / 1000.0;
}

void SampleEditorSidePanel::onLength(f64 length)
{
    editor()->edit([this, length](Sample& sample) -> void {
        auto it = sample.parts().find(id);

        if (it == sample.parts().end())
        {
            return;
        }

        SamplePart part = it->second;

        part.setLength(length * 1000);

        sample.add(part);
    });
}

f64 SampleEditorSidePanel::volumeSource() const
{
    return editor()->currentPart().volume();
}

void SampleEditorSidePanel::onVolume(f64 volume)
{
    editor()->edit([this, volume](Sample& sample) -> void {
        auto it = sample.parts().find(id);

        if (it == sample.parts().end())
        {
            return;
        }

        SamplePart part = it->second;

        part.setVolume(volume);

        sample.add(part);
    });
}

vector<string> SampleEditorSidePanel::typeOptionsSource()
{
    // NOTE: Update this when adding new sample part types
    return {
        localize("sample-editor-side-panel-tone")
    };
}

vector<string> SampleEditorSidePanel::typeOptionTooltipsSource()
{
    // NOTE: Update this when adding new sample part types
    return {
        localize("sample-editor-side-panel-tone-tooltip")
    };
}

size_t SampleEditorSidePanel::typeSource()
{
    return static_cast<size_t>(editor()->currentPart().type());
}

void SampleEditorSidePanel::onType(size_t index)
{
    editor()->edit([this, index](Sample& sample) -> void {
        auto it = sample.parts().find(id);

        if (it == sample.parts().end())
        {
            return;
        }

        SamplePart part = it->second;
        part.setType(static_cast<SamplePartType>(index));

        sample.add(part);
    });
}

f64 SampleEditorSidePanel::pitchSource() const
{
    return editor()->currentPart().get<SamplePartTone>().pitch;
}

void SampleEditorSidePanel::onPitch(f64 pitch)
{
    editor()->edit([this, pitch](Sample& sample) -> void {
        auto it = sample.parts().find(id);

        if (it == sample.parts().end())
        {
            return;
        }

        SamplePart part = it->second;

        part.get<SamplePartTone>().pitch = pitch;

        sample.add(part);
    });
}

SampleEditor* SampleEditorSidePanel::editor() const
{
    return dynamic_cast<SampleEditor*>(parent());
}
