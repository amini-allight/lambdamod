/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "sound_device_interface.hpp"
#include "sound_effect.hpp"
#include "sound_entity.hpp"
#include "sound_oneshot.hpp"
#include "sound_voice_input.hpp"
#include "sound_voice_stream.hpp"
#include "sound_post_process_effect.hpp"
#include "sound_stereo_chunk.hpp"
#include "hrir.hpp"

class Controller;
class Entity;

class Sound
{
public:
    Sound(Controller* controller);
    Sound(const Sound& rhs) = delete;
    Sound(Sound&& rhs) = delete;
    ~Sound();

    Sound& operator=(const Sound& rhs) = delete;
    Sound& operator=(Sound&& rhs) = delete;

    void step(f64 multiplier);

    void setMicrophone(const vec3& position, const quaternion& rotation);
    void setSpeedOfSound(f64 speedOfSound);

    void add(EntityID parentID, const Entity* entity);
    void remove(EntityID parentID, EntityID id);
    void update(EntityID parentID, const Entity* entity);

    void addOneshot(SoundOneshotType type, const vec3& position = vec3());

    template<typename T, typename... Args>
    SoundPostProcessEffectID addPostProcessEffect(Args... args)
    {
        auto effect = new T(
            args...
        );

        SoundPostProcessEffectID id = nextPostProcessEffectID++;

        postProcessEffects.insert({ id, effect });

        return id;
    }
    void removePostProcessEffect(SoundPostProcessEffectID id);
    template<typename T, typename... Args>
    void updatePostProcessEffect(SoundPostProcessEffectID id, Args... args)
    {
        auto it = postProcessEffects.find(id);

        if (it == postProcessEffects.end())
        {
            return;
        }

        dynamic_cast<T*>(it->second)->update(args...);
    }
    void setPostProcessEffectActive(SoundPostProcessEffectID id, bool state);
    void setPostProcessEffectOrder(const vector<SoundPostProcessEffectID>& order);

    void pushVoice(
        UserID id,
        const vec3& position,
        const quaternion& rotation,
        const vec3& linearVelocity,
        const string& data
    );

private:
    Controller* controller;

    struct {
        SoundEffect* startup;
        SoundEffect* vrStartup;
        SoundEffect* ping;
        SoundEffect* brake;
        SoundEffect* chat;
        SoundEffect* error;
        SoundEffect* title;
        SoundEffect* attach;
        SoundEffect* detach;
        SoundEffect* focus;
        SoundEffect* unfocus;
        SoundEffect* positiveActivate;
        SoundEffect* negativeActivate;
    } samples;

    map<EntityID, SoundEntity> entities;

    SoundPostProcessEffectID nextPostProcessEffectID;
    map<SoundPostProcessEffectID, SoundPostProcessEffect*> postProcessEffects;
    vector<SoundPostProcessEffectID> postProcessOrder;

    vector<SoundOneshot> oneshots;

    SoundVoiceInput voiceInput;
    map<UserID, SoundVoiceStream> voiceStreams;

    f32 exposure;
    chrono::microseconds lastWriteTime;

    SoundStereoChunk outputChunk;

    void adjustExposure(const chrono::microseconds& elapsed, f32 amplitude);

private:
    friend class SoundSource;

    mat4 microphoneTransform;

    f64 speedOfSound;

    HRIR* hrir;

    vec3 earPosition(u32 ear) const;

    void output(const variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk);
};
