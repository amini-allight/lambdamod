/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "gamepad_pointer_manager.hpp"
#include "vr_capable_panel.hpp"

#ifdef LMOD_VR
class KnowledgeViewer : public VRCapablePanel
#else
class KnowledgeViewer : public InterfaceWidget
#endif
{
public:
    KnowledgeViewer(InterfaceView* view);

    void draw(const DrawContext& ctx) const override;
    bool shouldDraw() const override;

private:
    GamepadPointerManager pointerManager;

    i32 activeIndex;

    vector<string> splitLines(const string& text) const;

#ifdef LMOD_VR
    optional<Point> toLocal(const vec3& position, const quaternion& rotation) const;
#endif

    void interact(const Input& input);
    void dismiss(const Input& input);

    vector<string> itemsSource() const;
    void onSelect(const string& title);

    tuple<string, string, string> activeItemSource();

    PositionProperties positionProperties() const override;
    SizeProperties sizeProperties() const override;
};
