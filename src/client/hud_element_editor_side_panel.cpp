/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud_element_editor_side_panel.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "theme.hpp"
#include "tools.hpp"
#include "hud_element_editor.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "vec2_edit.hpp"
#include "line_edit.hpp"
#include "float_edit.hpp"
#include "ufloat_edit.hpp"
#include "percent_edit.hpp"
#include "option_select.hpp"
#include "color_picker.hpp"
#include "checkbox.hpp"
#include "units.hpp"

HUDElementEditorSidePanel::HUDElementEditorSidePanel(InterfaceWidget* parent)
    : SidePanel(parent)
    , type(HUD_Element_Line)
{
    listView = new ListView(
        this,
        [](size_t index) -> void {},
        theme.backgroundColor
    );
}

void HUDElementEditorSidePanel::step()
{
    if (editor()->name != name || editor()->currentElement().type() != type)
    {
        name = editor()->name;
        type = editor()->currentElement().type();

        clearChildren();
        addCommonFields();
        
        switch (editor()->currentElement().type())
        {
        case HUD_Element_Line :
            addLineFields();
            break;
        case HUD_Element_Text :
            addTextFields();
            break;
        case HUD_Element_Bar :
            addBarFields();
            break;
        case HUD_Element_Symbol :
            addSymbolFields();
            break;
        }

        update();
    }

    InterfaceWidget::step();
}

void HUDElementEditorSidePanel::addCommonFields()
{
    new EditorEntry(listView, "hud-element-editor-side-panel-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&HUDElementEditorSidePanel::typesSource, this),
            bind(&HUDElementEditorSidePanel::typeTooltipsSource, this),
            bind(&HUDElementEditorSidePanel::typeSource, this),
            bind(&HUDElementEditorSidePanel::onType, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-anchor-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&HUDElementEditorSidePanel::anchorTypesSource, this),
            bind(&HUDElementEditorSidePanel::anchorTypeTooltipsSource, this),
            bind(&HUDElementEditorSidePanel::anchorTypeSource, this),
            bind(&HUDElementEditorSidePanel::onAnchorType, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-position", [this](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&HUDElementEditorSidePanel::positionSource, this),
            bind(&HUDElementEditorSidePanel::onPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-color", [this](InterfaceWidget* parent) -> void {
        new ColorPicker(
            parent,
            bind(&HUDElementEditorSidePanel::colorSource, this),
            bind(&HUDElementEditorSidePanel::onColor, this, placeholders::_1)
        );
    });
}

void HUDElementEditorSidePanel::addLineFields()
{
    new EditorEntry(listView, "hud-element-editor-side-panel-length", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&HUDElementEditorSidePanel::lengthSource, this),
            bind(&HUDElementEditorSidePanel::onLength, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-rotation", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&HUDElementEditorSidePanel::rotationSource, this),
            bind(&HUDElementEditorSidePanel::onRotation, this, placeholders::_1)
        );
    });
}

void HUDElementEditorSidePanel::addTextFields()
{
    new EditorEntry(listView, "hud-element-editor-side-panel-text", [this](InterfaceWidget* parent) -> void {
        new LineEdit(
            parent,
            bind(&HUDElementEditorSidePanel::textSource, this),
            bind(&HUDElementEditorSidePanel::onText, this, placeholders::_1)
        );
    });
}

void HUDElementEditorSidePanel::addBarFields()
{
    new EditorEntry(listView, "hud-element-editor-side-panel-length", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&HUDElementEditorSidePanel::lengthSource, this),
            bind(&HUDElementEditorSidePanel::onLength, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-fraction", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&HUDElementEditorSidePanel::fractionSource, this),
            bind(&HUDElementEditorSidePanel::onFraction, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-right-to-left", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&HUDElementEditorSidePanel::rightToLeftSource, this),
            bind(&HUDElementEditorSidePanel::onRightToLeft, this, placeholders::_1)
        );
    });
}

void HUDElementEditorSidePanel::addSymbolFields()
{
    new EditorEntry(listView, "hud-element-editor-side-panel-symbol-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&HUDElementEditorSidePanel::symbolTypesSource, this),
            bind(&HUDElementEditorSidePanel::symbolTypeTooltipsSource, this),
            bind(&HUDElementEditorSidePanel::symbolTypeSource, this),
            bind(&HUDElementEditorSidePanel::onSymbolType, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "hud-element-editor-side-panel-size", [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&HUDElementEditorSidePanel::sizeSource, this),
            bind(&HUDElementEditorSidePanel::onSize, this, placeholders::_1)
        );
    });
}

void HUDElementEditorSidePanel::clearChildren()
{
    listView->clearChildren();
}

vector<string> HUDElementEditorSidePanel::typesSource()
{
    // NOTE: Update these when changing the HUD system
    return {
        localize("hud-element-editor-side-panel-line"),
        localize("hud-element-editor-side-panel-text"),
        localize("hud-element-editor-side-panel-bar"),
        localize("hud-element-editor-side-panel-symbol")
    };
}

vector<string> HUDElementEditorSidePanel::typeTooltipsSource()
{
    // NOTE: Update these when changing the HUD system
    return {
        localize("hud-element-editor-side-panel-line-tooltip"),
        localize("hud-element-editor-side-panel-text-tooltip"),
        localize("hud-element-editor-side-panel-bar-tooltip"),
        localize("hud-element-editor-side-panel-symbol-tooltip")
    };
}

size_t HUDElementEditorSidePanel::typeSource()
{
    return static_cast<size_t>(editor()->currentElement().type());
}

void HUDElementEditorSidePanel::onType(size_t index)
{
    HUDElement element = editor()->currentElement();
    element.setType(static_cast<HUDElementType>(index));

    editor()->updateEntity(element);
}

vector<string> HUDElementEditorSidePanel::anchorTypesSource()
{
    // NOTE: Update this when adding new HUD anchor types
    return {
        localize("hud-element-editor-side-panel-center"),
        localize("hud-element-editor-side-panel-left"),
        localize("hud-element-editor-side-panel-right"),
        localize("hud-element-editor-side-panel-up"),
        localize("hud-element-editor-side-panel-down"),
        localize("hud-element-editor-side-panel-up-left"),
        localize("hud-element-editor-side-panel-down-left"),
        localize("hud-element-editor-side-panel-up-right"),
        localize("hud-element-editor-side-panel-down-right")
    };
}

vector<string> HUDElementEditorSidePanel::anchorTypeTooltipsSource()
{
    // NOTE: Update this when adding new HUD anchor types
    return {
        localize("hud-element-editor-side-panel-center-tooltip"),
        localize("hud-element-editor-side-panel-left-tooltip"),
        localize("hud-element-editor-side-panel-right-tooltip"),
        localize("hud-element-editor-side-panel-up-tooltip"),
        localize("hud-element-editor-side-panel-down-tooltip"),
        localize("hud-element-editor-side-panel-up-left-tooltip"),
        localize("hud-element-editor-side-panel-down-left-tooltip"),
        localize("hud-element-editor-side-panel-up-right-tooltip"),
        localize("hud-element-editor-side-panel-down-right-tooltip")
    };
}

size_t HUDElementEditorSidePanel::anchorTypeSource()
{
    return static_cast<size_t>(editor()->currentElement().anchor());
}

void HUDElementEditorSidePanel::onAnchorType(size_t index)
{
    HUDElement element = editor()->currentElement();
    element.setAnchor(static_cast<HUDElementAnchor>(index));

    editor()->updateEntity(element);
}

vec2 HUDElementEditorSidePanel::positionSource()
{
    return editor()->currentElement().position();
}

void HUDElementEditorSidePanel::onPosition(vec2 value)
{
    HUDElement element = editor()->currentElement();
    element.setPosition(value);

    editor()->updateEntity(element);
}

vec3 HUDElementEditorSidePanel::colorSource()
{
    return editor()->currentElement().color();
}

void HUDElementEditorSidePanel::onColor(const vec3& color)
{
    HUDElement element = editor()->currentElement();
    element.setColor(color);

    editor()->updateEntity(element);
}

f64 HUDElementEditorSidePanel::lengthSource()
{
    switch (editor()->currentElement().type())
    {
    default :
        return 0;
    case HUD_Element_Line :
        return editor()->currentElement().get<HUDElementLine>().length;
    case HUD_Element_Bar :
        return editor()->currentElement().get<HUDElementBar>().length;
    }
}

void HUDElementEditorSidePanel::onLength(f64 value)
{
    HUDElement element = editor()->currentElement();

    switch (editor()->currentElement().type())
    {
    default :
        break;
    case HUD_Element_Line :
        element.get<HUDElementLine>().length = value;
        break;
    case HUD_Element_Bar :
        element.get<HUDElementBar>().length = value;
        break;
    }

    editor()->updateEntity(element);
}

f64 HUDElementEditorSidePanel::rotationSource()
{
    return localizeAngle(editor()->currentElement().get<HUDElementLine>().rotation);
}

void HUDElementEditorSidePanel::onRotation(f64 value)
{
    HUDElement element = editor()->currentElement();

    element.get<HUDElementLine>().rotation = delocalizeLength(value);

    editor()->updateEntity(element);
}

string HUDElementEditorSidePanel::textSource()
{
    return editor()->currentElement().get<HUDElementText>().text;
}

void HUDElementEditorSidePanel::onText(const string& text)
{
    HUDElement element = editor()->currentElement();

    element.get<HUDElementText>().text = text;

    editor()->updateEntity(element);
}

f64 HUDElementEditorSidePanel::fractionSource()
{
    return editor()->currentElement().get<HUDElementBar>().fraction;
}

void HUDElementEditorSidePanel::onFraction(f64 value)
{
    HUDElement element = editor()->currentElement();

    element.get<HUDElementBar>().fraction = value;

    editor()->updateEntity(element);
}

bool HUDElementEditorSidePanel::rightToLeftSource()
{
    return editor()->currentElement().get<HUDElementBar>().rightToLeft;
}

void HUDElementEditorSidePanel::onRightToLeft(bool state)
{
    HUDElement element = editor()->currentElement();

    element.get<HUDElementBar>().rightToLeft = state;

    editor()->updateEntity(element);
}

vector<string> HUDElementEditorSidePanel::symbolTypesSource()
{
    // NOTE: Update this when adding new symbol types
    return {
        localize("symbol-star")
    };
}

vector<string> HUDElementEditorSidePanel::symbolTypeTooltipsSource()
{
    // NOTE: Update this when adding new symbol types
    return {
        localize("symbol-star-tooltip")
    };
}

size_t HUDElementEditorSidePanel::symbolTypeSource()
{
    return static_cast<SymbolType>(editor()->currentElement().get<HUDElementSymbol>().type);
}

void HUDElementEditorSidePanel::onSymbolType(size_t index)
{
    HUDElement element = editor()->currentElement();

    element.get<HUDElementSymbol>().type = static_cast<SymbolType>(index);

    editor()->updateEntity(element);
}

f64 HUDElementEditorSidePanel::sizeSource()
{
    return editor()->currentElement().get<HUDElementSymbol>().size;
}

void HUDElementEditorSidePanel::onSize(f64 value)
{
    HUDElement element = editor()->currentElement();

    element.get<HUDElementSymbol>().size = value;

    editor()->updateEntity(element);
}

HUDElementEditor* HUDElementEditorSidePanel::editor() const
{
    return dynamic_cast<HUDElementEditor*>(parent());
}
