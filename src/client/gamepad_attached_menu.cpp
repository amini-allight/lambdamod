/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "gamepad_attached_menu.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "panel_text_button.hpp"
#include "gamepad_magnitude_tool.hpp"

GamepadAttachedMenu::GamepadAttachedMenu(InterfaceView* view)
    : GamepadMenu(view)
{
    auto column = new Column(this);

    new PanelTextButton(column, localize("gamepad-attached-menu-detach"), bind(&GamepadAttachedMenu::onDetach, this));

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new PanelTextButton(column, localize("gamepad-attached-menu-ping"), bind(&GamepadAttachedMenu::onPing, this));

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new PanelTextButton(column, localize("gamepad-attached-menu-brake"), bind(&GamepadAttachedMenu::onBrake, this));

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new GamepadMagnitudeTool(column);

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new PanelTextButton(column, localize("gamepad-attached-menu-knowledge"), bind(&GamepadAttachedMenu::onKnowledge, this));

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new PanelTextButton(column, localize("gamepad-attached-menu-disconnect"), bind(&GamepadAttachedMenu::onDisconnect, this));

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new PanelTextButton(column, localize("gamepad-attached-menu-quit"), bind(&GamepadAttachedMenu::onQuit, this));
}

bool GamepadAttachedMenu::shouldDraw() const
{
    return GamepadMenu::shouldDraw() && context()->attachedMode()->gamepadMenu();
}

void GamepadAttachedMenu::onDetach()
{
    context()->attachedMode()->detach();
}

void GamepadAttachedMenu::onPing()
{
    context()->attachedMode()->ping();
}

void GamepadAttachedMenu::onBrake()
{
    context()->attachedMode()->brake();
}

void GamepadAttachedMenu::onKnowledge()
{
    context()->attachedMode()->toggleKnowledgeViewer();
}

void GamepadAttachedMenu::onDisconnect()
{
    context()->controller()->disconnect();
}

void GamepadAttachedMenu::onQuit()
{
    context()->controller()->quit();
}
