/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

#include "node_editor.hpp"
#include "code_editor.hpp"

class LogicEditor : public InterfaceWidget, public TabContent
{
public:
    LogicEditor(InterfaceWidget* parent);

    bool saved() const;
    void set(const string& text);
    void notifyReload();
    void display(const string& message);

protected:
    NodeEditor* nodeEditor;
    CodeEditor* codeEditor;

    virtual ScriptNodeFormat nodeFormatSource() = 0;
    virtual void onNodeFormatSave(const ScriptNodeFormat& format) = 0;

    virtual string textSource() = 0;
    virtual void onTextSave(const string& text) = 0;

    SizeProperties sizeProperties() const override;
};
