/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_generators.hpp"
#include "render_types.hpp"
#include "render_constants.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "paths.hpp"
#include "symbol_resolver.hpp"
#include "text_resolver.hpp"
#include "mesh_cache.hpp"
#include "vertex_color_encoding.hpp"
#include "line_builder.hpp"
#include "polygon_triangulation.hpp"

// We do not invert normals on inverted hulls, the inverted normals are detected in the fragment shader and used as a signal to do special view-space inversion

static constexpr f32 lineSegments = 12;
static constexpr f32 ellipseSegments = 64;
static constexpr f32 circleBandSegments = 64;

static constexpr size_t triangleVertices = 3;
static constexpr size_t rectangleVertices = 4;
static constexpr size_t pentagonVertices = 5;
static constexpr size_t hexagonVertices = 6;
static constexpr size_t heptagonVertices = 7;
static constexpr size_t octagonVertices = 8;
static constexpr size_t cuboidFaces = 6;
static constexpr size_t cuboidCorners = 8;
static constexpr size_t cuboidEdges = 12;
static constexpr size_t sphereSegments = 64;
static constexpr size_t sphereRings = 32;
static constexpr size_t cylinderSegments = 64;
static constexpr size_t coneSegments = 64;

static constexpr f32 lightSphereRadius = 0.01;

static void invertLastTriangleWinding(vector<u32>& indices)
{
    swap(indices.at(indices.size() - 1), indices.at(indices.size() - 2));
}

static void doubleSideLastTriangle(vector<u32>& indices)
{
    u32 a = indices.at(indices.size() - 3);
    u32 b = indices.at(indices.size() - 2);
    u32 c = indices.at(indices.size() - 1);

    indices.push_back(a);
    indices.push_back(c);
    indices.push_back(b);
}

BodyRenderData generateLine(const mat4& transform, const BodyPartLine& line)
{
    vector<u32> indices;
    indices.reserve(3 * 4 * lineSegments);
    vector<LitColoredVertex> vertices;
    vertices.reserve(2 + (lineSegments * 2));

    vertices.push_back({ fvec3(0, +(line.length / 2), 0), forwardDir, litColor(line.endColor) });
    vertices.push_back({ fvec3(0, -(line.length / 2), 0), backDir, litColor(line.startColor) });

    for (size_t i = 0; i < lineSegments; i++)
    {
        bool last = i + 1 == lineSegments;

        fquaternion rot(forwardDir, (i / lineSegments) * tau);

        fvec3 start = rot.rotate(fvec3(0, +(line.length / 2), lineWidth / 2));
        fvec3 end = rot.rotate(fvec3(0, -(line.length / 2), lineWidth / 2));
        fvec3 normal = rot.rotate(upDir);

        vertices.push_back({ start, normal, litColor(line.endColor) });
        vertices.push_back({ end, normal, litColor(line.startColor) });

        u32 top = 2 + (i * 2) + 0;
        u32 nextTop = 2 + ((last ? 0 : (i + 1)) * 2) + 0;

        u32 bot = 2 + (i * 2) + 1;
        u32 nextBot = 2 + ((last ? 0 : (i + 1)) * 2) + 1;

        indices.push_back(top);
        indices.push_back(nextTop);
        indices.push_back(0);

        indices.push_back(bot);
        indices.push_back(nextTop);
        indices.push_back(top);

        indices.push_back(bot);
        indices.push_back(nextBot);
        indices.push_back(nextTop);

        indices.push_back(bot);
        indices.push_back(1);
        indices.push_back(nextBot);
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    return data;
}

static BodyRenderData generateEllipseMesh(const mat4& transform, const BodyPartPlane& plane)
{
    vector<u32> indices;
    indices.reserve((3 * ellipseSegments) + (3 * 2 * lineSegments * ellipseSegments));

    vector<LitColoredVertex> vertices;
    vertices.reserve(ellipseSegments + 1 + (lineSegments * ellipseSegments));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ fvec3(), upDir, litColor(plane.color) });

        for (size_t i = 0; i < ellipseSegments; i++)
        {
            fvec3 position(
                (plane.width / 2) * cos(tau * (i / static_cast<f32>(ellipseSegments))),
                (plane.height / 2) * sin(tau * (i / static_cast<f32>(ellipseSegments))),
                0
            );

            vertices.push_back({ position, upDir, unlitColor(baseBackgroundColor) });
        }

        // Background indices
        for (size_t i = 1; i <= ellipseSegments; i++)
        {
            indices.push_back(0);
            indices.push_back(i);
            indices.push_back((i == ellipseSegments ? 1 : i + 1));

            doubleSideLastTriangle(indices);
        }

        offset = ellipseSegments + 1;
    }
    else
    {
        offset = 0;
    }

    for (size_t i = 0; i < ellipseSegments; i++)
    {
        fvec3 spinDir = fvec3(1, 0, 0);
        fvec3 start = fvec3(0, lineWidth / 2, 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(upDir, (i / static_cast<f32>(ellipseSegments)) * tau);

            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            position += rotateToCorner.rotate(fvec3(0, plane.height / 2, 0));

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == ellipseSegments;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    if (!plane.empty)
    {
        data.occlusionPrimitives.push_back({
            transform,
            fvec3(plane.width, plane.height, 0),
            Body_Render_Occlusion_Primitive_Ellipse
        });
    }

    return data;
}

static BodyRenderData generateTriangleMesh(
    const mat4& transform,
    const BodyPartPlane& plane,
    const fvec3& a,
    const fvec3& b,
    const fvec3& c
)
{
    vector<u32> indices;
    indices.reserve(triangleVertices + (3 * 2 * lineSegments * triangleVertices));

    vector<LitColoredVertex> vertices;
    vertices.reserve(triangleVertices + (lineSegments * triangleVertices));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ a, upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ b, upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ c, upDir, unlitColor(baseBackgroundColor) });

        // Background indices
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);

        doubleSideLastTriangle(indices);

        offset = triangleVertices;
    }
    else
    {
        offset = 0;
    }

    fvec2 center = (a.toVec2() + b.toVec2() + c.toVec2()) / 3;

    for (size_t i = 0; i < triangleVertices; i++)
    {
        fvec3 spinDir = fvec3(1, 0, 0);
        fvec3 start = fvec3(0, lineWidth / 2, 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner;

            switch (i)
            {
            case 0 :
                rotateToCorner = fquaternion(upDir, -handedAngleBetween((a.toVec2() - center).normalize(), fvec2(0, 1)));
                break;
            case 1 :
                rotateToCorner = fquaternion(upDir, -handedAngleBetween((b.toVec2() - center).normalize(), fvec2(0, 1)));
                break;
            case 2 :
                rotateToCorner = fquaternion(upDir, -handedAngleBetween((c.toVec2() - center).normalize(), fvec2(0, 1)));
                break;
            }


            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            switch (i)
            {
            case 0 :
                position += a;
                break;
            case 1 :
                position += b;
                break;
            case 2 :
                position += c;
                break;
            }

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == triangleVertices;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    return data;
}

static BodyRenderData generateRectangleMesh(const mat4& transform, const BodyPartPlane& plane)
{
    vector<u32> indices;
    indices.reserve((3 * 2) + (3 * 2 * lineSegments * rectangleVertices));

    vector<LitColoredVertex> vertices;
    vertices.reserve(rectangleVertices + (lineSegments * rectangleVertices));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ fvec3(-(plane.width / 2), -(plane.height / 2), 0), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fvec3(+(plane.width / 2), -(plane.height / 2), 0), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fvec3(-(plane.width / 2), +(plane.height / 2), 0), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fvec3(+(plane.width / 2), +(plane.height / 2), 0), upDir, unlitColor(baseBackgroundColor) });

        // Background indices
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(3);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(2);

        doubleSideLastTriangle(indices);

        offset = rectangleVertices;
    }
    else
    {
        offset = 0;
    }

    for (size_t i = 0; i < rectangleVertices; i++)
    {
        fvec3 spinDir = fvec3(1, -1, 0).normalize();
        fvec3 start = fvec3(-(lineWidth / 2), -(lineWidth / 2), 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(upDir, (i / static_cast<f32>(rectangleVertices)) * tau);

            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            switch (i)
            {
            case 0 :
                position += fvec3(plane.width / 2, plane.height / 2, 0);
                break;
            case 1 :
                position += fvec3(-plane.width / 2, plane.height / 2, 0);
                break;
            case 2 :
                position += fvec3(-plane.width / 2, -plane.height / 2, 0);
                break;
            case 3 :
                position += fvec3(plane.width / 2, -plane.height / 2, 0);
                break;
            }

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == rectangleVertices;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    if (!plane.empty)
    {
        data.occlusionPrimitives.push_back({
            transform,
            fvec3(plane.width, plane.height, 0),
            Body_Render_Occlusion_Primitive_Rectangle
        });
    }

    return data;
}

static BodyRenderData generatePentagonMesh(const mat4& transform, const BodyPartPlane& plane)
{
    vector<u32> indices;
    indices.reserve((3 * pentagonVertices) + (3 * 2 * lineSegments * pentagonVertices));

    vector<LitColoredVertex> vertices;
    vertices.reserve(pentagonVertices + 1 + (lineSegments * pentagonVertices));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ fvec3(), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fvec3(0, plane.width / 2, 0), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(72)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(144)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(216)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(288)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });

        // Background indices
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(4);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(4);
        indices.push_back(5);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(5);
        indices.push_back(1);

        offset = pentagonVertices + 1;
    }
    else
    {
        offset = 0;
    }

    for (size_t i = 0; i < pentagonVertices; i++)
    {
        fvec3 spinDir = fvec3(1, 0, 0);
        fvec3 start = fvec3(0, lineWidth / 2, 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(upDir, (i / static_cast<f32>(pentagonVertices)) * tau);

            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            fvec3 offset = rotateToCorner.rotate(fvec3(0, plane.width / 2, 0));

            position += offset;

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == pentagonVertices;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    if (!plane.empty)
    {
        data.occlusionPrimitives.push_back({
            transform,
            fvec3(plane.width, 0, 0),
            Body_Render_Occlusion_Primitive_Pentagon
        });
    }

    return data;
}

static BodyRenderData generateHexagonMesh(const mat4& transform, const BodyPartPlane& plane)
{
    vector<u32> indices;
    indices.reserve((3 * hexagonVertices) + (3 * 2 * lineSegments * hexagonVertices));

    vector<LitColoredVertex> vertices;
    vertices.reserve(hexagonVertices + 1 + (lineSegments * hexagonVertices));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ fvec3(), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(30)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(90)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(150)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(210)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(270)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(330)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });

        // Background indices
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(4);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(4);
        indices.push_back(5);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(5);
        indices.push_back(6);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(6);
        indices.push_back(1);

        doubleSideLastTriangle(indices);

        offset = hexagonVertices + 1;
    }
    else
    {
        offset = 0;
    }

    for (size_t i = 0; i < hexagonVertices; i++)
    {
        fvec3 spinDir = fvec3(1, 0, 0);
        fvec3 start = fvec3(0, lineWidth / 2, 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(upDir, ((i + 0.5) / static_cast<f32>(hexagonVertices)) * tau);

            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            f32 toCorner = plane.width / 2;
            f32 y = (toCorner / sin((pi / 2) - radians(30))) * sin(pi / 2);

            fvec3 offset = rotateToCorner.rotate(fvec3(0, y, 0));

            position += offset;

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == hexagonVertices;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    if (!plane.empty)
    {
        data.occlusionPrimitives.push_back({
            transform,
            fvec3(plane.width, 0, 0),
            Body_Render_Occlusion_Primitive_Hexagon
        });
    }

    return data;
}

static BodyRenderData generateHeptagonMesh(const mat4& transform, const BodyPartPlane& plane)
{
    vector<u32> indices;
    indices.reserve((3 * heptagonVertices) + (3 * 2 * lineSegments * heptagonVertices));

    vector<LitColoredVertex> vertices;
    vertices.reserve(heptagonVertices + 1 + (lineSegments * heptagonVertices));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ fvec3(), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fvec3(0, plane.width / 2, 0), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(51.4285)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(102.8571)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(154.2857)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(205.7142)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(257.1428)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(308.5714)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });

        // Background indices
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(4);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(4);
        indices.push_back(5);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(5);
        indices.push_back(6);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(6);
        indices.push_back(7);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(7);
        indices.push_back(1);

        doubleSideLastTriangle(indices);

        offset = heptagonVertices + 1;
    }
    else
    {
        offset = 0;
    }

    for (size_t i = 0; i < heptagonVertices; i++)
    {
        fvec3 spinDir = fvec3(1, 0, 0);
        fvec3 start = fvec3(0, lineWidth / 2, 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(upDir, (i / static_cast<f32>(heptagonVertices)) * tau);

            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            fvec3 offset = rotateToCorner.rotate(fvec3(0, plane.width / 2, 0));

            position += offset;

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == heptagonVertices;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    if (!plane.empty)
    {
        data.occlusionPrimitives.push_back({
            transform,
            fvec3(plane.width, 0, 0),
            Body_Render_Occlusion_Primitive_Heptagon
        });
    }

    return data;
}

static BodyRenderData generateOctagonMesh(const mat4& transform, const BodyPartPlane& plane)
{
    vector<u32> indices;
    indices.reserve((3 * octagonVertices) + (3 * 2 * lineSegments * octagonVertices));

    vector<LitColoredVertex> vertices;
    vertices.reserve(octagonVertices + 1 + (lineSegments * octagonVertices));

    size_t offset;

    if (!plane.empty)
    {
        // Corners
        vertices.push_back({ fvec3(), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(22.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(67.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(112.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(157.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(202.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(247.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(292.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });
        vertices.push_back({ fquaternion(upDir, radians(337.5)).rotate(fvec3(0, plane.width / 2, 0)), upDir, unlitColor(baseBackgroundColor) });

        // Background indices
        indices.push_back(0);
        indices.push_back(1);
        indices.push_back(2);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(2);
        indices.push_back(3);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(3);
        indices.push_back(4);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(4);
        indices.push_back(5);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(5);
        indices.push_back(6);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(6);
        indices.push_back(7);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(7);
        indices.push_back(8);

        doubleSideLastTriangle(indices);

        indices.push_back(0);
        indices.push_back(8);
        indices.push_back(1);

        offset = octagonVertices + 1;
    }
    else
    {
        offset = 0;
    }

    for (size_t i = 0; i < octagonVertices; i++)
    {
        fvec3 spinDir = fvec3(1, 0, 0);
        fvec3 start = fvec3(0, lineWidth / 2, 0);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(spinDir, (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(upDir, ((i + 0.5) / static_cast<f32>(octagonVertices)) * tau);

            fvec3 position = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = position.normalize();

            f32 toCorner = plane.width / 2;
            f32 y = (toCorner / sin((pi / 2) - radians(22.5))) * sin(pi / 2);

            fvec3 offset = rotateToCorner.rotate(fvec3(0, y, 0));

            position += offset;

            vertices.push_back({ position, normal, litColor(plane.color) });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == octagonVertices;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(offset + b);
            indices.push_back(offset + nextA);
            indices.push_back(offset + nextB);

            indices.push_back(offset + b);
            indices.push_back(offset + a);
            indices.push_back(offset + nextA);
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    if (!plane.empty)
    {
        data.occlusionPrimitives.push_back({
            transform,
            fvec3(plane.width, 0, 0),
            Body_Render_Occlusion_Primitive_Octagon
        });
    }

    return data;
}

BodyRenderData generatePlane(const mat4& transform, const BodyPartPlane& plane)
{
    switch (plane.type)
    {
    default :
        fatal("Encountered unknown plane type '" + to_string(plane.type) + "'.");
    case Body_Plane_Ellipse :
        return generateEllipseMesh(transform, plane);
    case Body_Plane_Equilateral_Triangle :
    {
        BodyRenderData data = generateTriangleMesh(
            transform,
            plane,
            fvec3(0, plane.width / 2, 0),
            fquaternion(upDir, +(tau / 3)).rotate(fvec3(0, plane.width / 2, 0)),
            fquaternion(upDir, -(tau / 3)).rotate(fvec3(0, plane.width / 2, 0))
        );

        if (!plane.empty)
        {
            data.occlusionPrimitives.push_back({
                transform,
                fvec3(plane.width, 0, 0),
                Body_Render_Occlusion_Primitive_Equilateral_Triangle
            });
        }

        return data;
    }
    case Body_Plane_Isosceles_Triangle :
    {
        BodyRenderData data = generateTriangleMesh(
            transform,
            plane,
            fvec3(0, plane.height, 0),
            fvec3(+plane.width / 2, 0, 0),
            fvec3(-plane.width / 2, 0, 0)
        );

        if (!plane.empty)
        {
            data.occlusionPrimitives.push_back({
                transform,
                fvec3(plane.width, plane.height, 0),
                Body_Render_Occlusion_Primitive_Isosceles_Triangle
            });
        }

        return data;
    }
    case Body_Plane_Right_Angle_Triangle :
    {
        BodyRenderData data = generateTriangleMesh(
            transform,
            plane,
            fvec3(0, plane.height, 0),
            fvec3(plane.width, 0, 0),
            fvec3()
        );

        if (!plane.empty)
        {
            data.occlusionPrimitives.push_back({
                transform,
                fvec3(plane.width, plane.height, 0),
                Body_Render_Occlusion_Primitive_Right_Angle_Triangle
            });
        }

        return data;
    }
    case Body_Plane_Rectangle :
        return generateRectangleMesh(transform, plane);
    case Body_Plane_Pentagon :
        return generatePentagonMesh(transform, plane);
    case Body_Plane_Hexagon :
        return generateHexagonMesh(transform, plane);
    case Body_Plane_Heptagon :
        return generateHeptagonMesh(transform, plane);
    case Body_Plane_Octagon :
        return generateOctagonMesh(transform, plane);
    }
}

static BodyRenderData generateCuboidMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;
    indices.reserve((3 * 2 * cuboidFaces) + (3 * 2 * cuboidEdges * lineSegments));

    vector<LitColoredVertex> vertices;
    vertices.reserve(cuboidCorners);

    // Corners
    static constexpr fvec3 directions[] = {
        fvec3(+1, +1, +1),
        fvec3(+1, -1, +1),
        fvec3(-1, +1, +1),
        fvec3(-1, -1, +1),
        fvec3(+1, +1, -1),
        fvec3(+1, -1, -1),
        fvec3(-1, +1, -1),
        fvec3(-1, -1, -1)
    };

    for (size_t i = 0; i < cuboidCorners; i++)
    {
        fvec3 position = directions[i] * (solid.size / 2);
        fvec3 normal = directions[i];

        vertices.push_back({ position, normal, unlitColor(baseBackgroundColor) });
    }

    // Depth indices
    // +z
    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(1);

    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);

    // +x
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(5);

    indices.push_back(0);
    indices.push_back(5);
    indices.push_back(4);

    // -x
    indices.push_back(2);
    indices.push_back(7);
    indices.push_back(3);

    indices.push_back(2);
    indices.push_back(6);
    indices.push_back(7);

    // +y
    indices.push_back(0);
    indices.push_back(6);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(4);
    indices.push_back(6);

    // -y
    indices.push_back(1);
    indices.push_back(3);
    indices.push_back(7);

    indices.push_back(1);
    indices.push_back(7);
    indices.push_back(5);

    // -z
    indices.push_back(4);
    indices.push_back(5);
    indices.push_back(7);

    indices.push_back(4);
    indices.push_back(7);
    indices.push_back(6);

    auto createEdge = [&](const fmat4& transform, f32 length, const fvec4& color) -> void
    {
        fquaternion initialRotation(rightDir, radians(45));
        vector<fvec3> basis(lineSegments);

        for (u32 i = 0; i < lineSegments; i++)
        {
            fquaternion rotation(rightDir, tau * (i / static_cast<f32>(lineSegments)));

            basis.at(i) = rotation.rotate(initialRotation.rotate(forwardDir * (lineWidth / 2)));
        }

        u32 indexOffset = vertices.size();

        for (u32 i = 0; i < lineSegments; i++)
        {
            const vec3& p = basis.at(i);

            LitColoredVertex vertex;

            switch (i)
            {
            case 0 :
                vertex.position = fvec3(-p.y, p.y, p.y);
                break;
            case 1 :
            case 2 :
            case 3 :
            case 4 :
            case 5 :
                vertex.position = fvec3(-p.y, p.y, p.z);
                break;
            case 6 :
                vertex.position = fvec3(-p.y, p.y, p.y);
                break;
            case 7 :
            case 8 :
            case 9 :
            case 10 :
            case 11 :
                vertex.position = fvec3(-p.z, p.y, p.z);
                break;
            }

            vertex.position = transform.applyToPosition(fvec3(-length / 2, 0, 0) + vertex.position);
            vertex.normal = transform.applyToDirection(p.normalize());
            vertex.color = color;

            vertices.push_back(vertex);
        }

        for (u32 i = 0; i < lineSegments; i++)
        {
            const vec3& p = basis.at(i);

            LitColoredVertex vertex;

            switch (i)
            {
            case 0 :
                vertex.position = fvec3(p.y);
                break;
            case 1 :
            case 2 :
            case 3 :
            case 4 :
            case 5 :
                vertex.position = fvec3(p.y, p.y, p.z);
                break;
            case 6 :
                vertex.position = fvec3(p.y);
                break;
            case 7 :
            case 8 :
            case 9 :
            case 10 :
            case 11 :
                vertex.position = fvec3(p.z, p.y, p.z);
                break;
            }

            vertex.position = transform.applyToPosition(fvec3(+length / 2, 0, 0) + vertex.position);
            vertex.normal = transform.applyToDirection(p.normalize());
            vertex.color = color;

            vertices.push_back(vertex);
        }

        for (u32 i = 0; i < lineSegments; i++)
        {
            u32 a = indexOffset + i;
            u32 b = indexOffset + loopIndex<u32>(i + 1, lineSegments);
            u32 c = indexOffset + lineSegments + i;
            u32 d = indexOffset + lineSegments + loopIndex<u32>(i + 1, lineSegments);

            indices.push_back(a);
            indices.push_back(d);
            indices.push_back(c);

            indices.push_back(a);
            indices.push_back(b);
            indices.push_back(d);
        }
    };

    // Top
    createEdge(
        fmat4(fvec3(0, solid.size.y / 2, solid.size.z / 2), fquaternion(forwardDir, upDir), fvec3(1)),
        solid.size.x,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(solid.size.x / 2, 0, solid.size.z / 2), fquaternion(rightDir, upDir), fvec3(1)),
        solid.size.y,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(0, -solid.size.y / 2, solid.size.z / 2), fquaternion(backDir, upDir), fvec3(1)),
        solid.size.x,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(-solid.size.x / 2, 0, solid.size.z / 2), fquaternion(leftDir, upDir), fvec3(1)),
        solid.size.y,
        litColor(solid.color)
    );

    // Verticals
    createEdge(
        fmat4(fvec3(solid.size.x / 2, solid.size.y / 2, 0), fquaternion(rightDir, forwardDir), fvec3(1)),
        solid.size.z,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(solid.size.x / 2, -solid.size.y / 2, 0), fquaternion(backDir, rightDir), fvec3(1)),
        solid.size.z,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(-solid.size.x / 2, -solid.size.y / 2, 0), fquaternion(leftDir, backDir), fvec3(1)),
        solid.size.z,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(-solid.size.x / 2, solid.size.y / 2, 0), fquaternion(forwardDir, leftDir), fvec3(1)),
        solid.size.z,
        litColor(solid.color)
    );

    // Bottom
    createEdge(
        fmat4(fvec3(0, solid.size.y / 2, -solid.size.z / 2), fquaternion(forwardDir, downDir), fvec3(1)),
        solid.size.x,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(solid.size.x / 2, 0, -solid.size.z / 2), fquaternion(rightDir, downDir), fvec3(1)),
        solid.size.y,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(0, -solid.size.y / 2, -solid.size.z / 2), fquaternion(backDir, downDir), fvec3(1)),
        solid.size.x,
        litColor(solid.color)
    );

    createEdge(
        fmat4(fvec3(-solid.size.x / 2, 0, -solid.size.z / 2), fquaternion(leftDir, downDir), fvec3(1)),
        solid.size.y,
        litColor(solid.color)
    );

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Cuboid
    });

    return data;
}

static void generateSphereHull(
    f32 radius,
    const vec4& color,
    bool invert,
    vector<LitColoredVertex>& outVertices,
    vector<u32>& outIndices
)
{
    vector<LitColoredVertex> vertices;
    vector<u32> indices;

    u32 indexOffset = outVertices.size();

    vertices.push_back({ fvec3(0, 0, +radius), upDir, color });
    vertices.push_back({ fvec3(0, 0, -radius), downDir, color });

    for (u32 r = 1; r < sphereRings; r++)
    {
        fquaternion toRing(leftDir, (r / static_cast<f32>(sphereRings)) * pi);

        for (u32 s = 0; s < sphereSegments; s++)
        {
            fquaternion toSegment(downDir, (s / static_cast<f32>(sphereSegments)) * tau);

            fvec3 position = toSegment.rotate(toRing.rotate(vertices.front().position));
            fvec3 normal = position.normalize();

            vertices.push_back({ position, normal, color });
        }
    }

    for (u32 r = 0; r < sphereRings; r++)
    {
        u32 elapsed = 2 + max<i32>(static_cast<i32>(r) - 1, 0) * sphereSegments;

        for (u32 s = 0; s < sphereSegments; s++)
        {
            if (r == 0)
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);

                indices.push_back(a);
                indices.push_back(0);
                indices.push_back(b);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }
            }
            else if (r + 1 == sphereRings)
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);

                indices.push_back(b);
                indices.push_back(1);
                indices.push_back(a);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }
            }
            else
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);
                u32 c = a + sphereSegments;
                u32 d = b + sphereSegments;

                indices.push_back(b);
                indices.push_back(c);
                indices.push_back(a);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }

                indices.push_back(d);
                indices.push_back(c);
                indices.push_back(b);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }
            }
        }
    }

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    outVertices.insert(outVertices.end(), vertices.begin(), vertices.end());
    outIndices.insert(outIndices.end(), indices.begin(), indices.end());
}

static BodyRenderData generateSphereMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;
    indices.reserve(2 * 3 * 2 * sphereSegments * (sphereRings - 1));

    vector<LitColoredVertex> vertices;
    vertices.reserve(2 * (2 + (sphereSegments * (sphereRings - 1))));

    generateSphereHull(
        (solid.size.x / 2) + (lineWidth / 2),
        litColor(solid.color),
        true,
        vertices,
        indices
    );

    generateSphereHull(
        (solid.size.x / 2) - (lineWidth / 2),
        unlitColor(baseBackgroundColor),
        false,
        vertices,
        indices
    );

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Sphere
    });

    return data;
}

static void generateCircleBand(
    const vec3& position,
    const quaternion& rotation,
    f32 radius,
    const vec4& color,
    vector<LitColoredVertex>& outVertices,
    vector<u32>& outIndices
)
{
    vector<LitColoredVertex> vertices;
    vector<u32> indices;

    u32 indexOffset = outVertices.size();

    for (size_t i = 0; i < circleBandSegments; i++)
    {
        fvec3 start = rotation.forward() * (lineWidth / 2);

        for (size_t j = 0; j < lineSegments; j++)
        {
            fquaternion rotateToCylinder(rotation.right(), (j / static_cast<f32>(lineSegments)) * tau);
            fquaternion rotateToCorner(rotation.up(), (i / static_cast<f32>(circleBandSegments)) * tau);

            fvec3 vertexPosition = rotateToCorner.rotate(rotateToCylinder.rotate(start));
            fvec3 normal = vertexPosition.normalize();

            fvec3 offset = rotateToCorner.rotate(rotation.forward() * radius);

            vertexPosition += position;
            vertexPosition += offset;

            vertices.push_back({ vertexPosition, normal, color });
        }

        for (size_t j = 0; j < lineSegments; j++)
        {
            bool lastCorner = i + 1 == circleBandSegments;
            bool last = j + 1 == lineSegments;

            u32 a = (i * lineSegments) + j;
            u32 b = (i * lineSegments) + (last ? 0 : j + 1);

            u32 nextA = ((lastCorner ? 0 : (i + 1)) * lineSegments) + j;
            u32 nextB = ((lastCorner ? 0 : (i + 1)) * lineSegments) + (last ? 0 : j + 1);

            indices.push_back(b);
            indices.push_back(nextA);
            indices.push_back(nextB);

            indices.push_back(b);
            indices.push_back(a);
            indices.push_back(nextA);
        }
    }

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    outVertices.insert(outVertices.end(), vertices.begin(), vertices.end());
    outIndices.insert(outIndices.end(), indices.begin(), indices.end());
}

static void generateCylinderHull(
    f32 radius,
    f32 height,
    const vec4& color,
    bool invert,
    vector<LitColoredVertex>& outVertices,
    vector<u32>& outIndices,
    bool capped = true
)
{
    vector<LitColoredVertex> vertices;
    vector<u32> indices;

    u32 indexOffset = outVertices.size();

    for (size_t i = 0; i < cylinderSegments; i++)
    {
        fquaternion toSegment(downDir, (i / static_cast<f32>(cylinderSegments)) * tau);

        fvec3 position = toSegment.rotate(fvec3(radius, 0, +(height / 2)));
        fvec3 normal = toSegment.rotate(rightDir);

        vertices.push_back({ position, normal, color });
    }

    for (size_t i = 0; i < cylinderSegments; i++)
    {
        fquaternion toSegment(downDir, (i / static_cast<f32>(cylinderSegments)) * tau);

        fvec3 position = toSegment.rotate(fvec3(radius, 0, -(height / 2)));
        fvec3 normal = toSegment.rotate(rightDir);

        vertices.push_back({ position, normal, color });
    }

    for (size_t i = 0; i < cylinderSegments; i++)
    {
        u32 a = i;
        u32 b = loopIndex(i + 1, cylinderSegments);
        u32 c = cylinderSegments + i;
        u32 d = cylinderSegments + loopIndex(i + 1, cylinderSegments);

        indices.push_back(c);
        indices.push_back(a);
        indices.push_back(b);

        if (invert)
        {
            invertLastTriangleWinding(indices);
        }

        indices.push_back(c);
        indices.push_back(b);
        indices.push_back(d);

        if (invert)
        {
            invertLastTriangleWinding(indices);
        }
    }

    if (capped)
    {
        for (size_t i = 0; i < cylinderSegments; i++)
        {
            fquaternion toSegment(downDir, (i / static_cast<f32>(cylinderSegments)) * tau);

            fvec3 position = toSegment.rotate(fvec3(radius, 0, +(height / 2)));
            fvec3 normal = toSegment.rotate(upDir);

            vertices.push_back({ position, normal, color });
        }

        for (size_t i = 0; i < cylinderSegments; i++)
        {
            fquaternion toSegment(downDir, (i / static_cast<f32>(cylinderSegments)) * tau);

            fvec3 position = toSegment.rotate(fvec3(radius, 0, -(height / 2)));
            fvec3 normal = toSegment.rotate(downDir);

            vertices.push_back({ position, normal, color });
        }

        vertices.push_back({ fvec3(0, 0, +(height / 2)), upDir, color });
        vertices.push_back({ fvec3(0, 0, -(height / 2)), downDir, color });

        u32 topIndex = vertices.size() - 2;
        u32 bottomIndex = vertices.size() - 1;

        for (size_t i = 0; i < cylinderSegments; i++)
        {
            const u32 offset = cylinderSegments * 2;

            u32 a = offset + i;
            u32 b = offset + loopIndex<u32>(i + 1, cylinderSegments);

            indices.push_back(a);
            indices.push_back(topIndex);
            indices.push_back(b);

            if (invert)
            {
                invertLastTriangleWinding(indices);
            }
        }

        for (size_t i = 0; i < cylinderSegments; i++)
        {
            const u32 offset = cylinderSegments * 3;

            u32 a = offset + i;
            u32 b = offset + loopIndex<u32>(i + 1, cylinderSegments);

            indices.push_back(a);
            indices.push_back(b);
            indices.push_back(bottomIndex);

            if (invert)
            {
                invertLastTriangleWinding(indices);
            }
        }
    }

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    outVertices.insert(outVertices.end(), vertices.begin(), vertices.end());
    outIndices.insert(outIndices.end(), indices.begin(), indices.end());
}

static BodyRenderData generateCylinderMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;
    indices.reserve(2 * 3 * 4 * cylinderSegments);

    vector<LitColoredVertex> vertices;
    vertices.reserve(2 * (2 * cylinderSegments));

    generateCylinderHull(
        (solid.size.x / 2) + (lineWidth / 2),
        solid.size.z,
        litColor(solid.color),
        true,
        vertices,
        indices,
        false
    );

    generateCylinderHull(
        (solid.size.x / 2) - (lineWidth / 2),
        solid.size.z,
        unlitColor(baseBackgroundColor),
        false,
        vertices,
        indices
    );

    generateCircleBand(
        vec3(0, 0, +solid.size.z / 2),
        quaternion(),
        solid.size.x / 2,
        litColor(solid.color),
        vertices,
        indices
    );

    generateCircleBand(
        vec3(0, 0, -solid.size.z / 2),
        quaternion(),
        solid.size.x / 2,
        litColor(solid.color),
        vertices,
        indices
    );

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Cylinder
    });

    return data;
}

static void generateConeHull(
    f32 radius,
    f32 height,
    const vec4& color,
    bool invert,
    vector<LitColoredVertex>& outVertices,
    vector<u32>& outIndices,
    bool capped = true
)
{
    vector<LitColoredVertex> vertices;
    vector<u32> indices;

    u32 indexOffset = outVertices.size();

    vertices.push_back({ fvec3(0, 0, height), upDir, color });
    vertices.push_back({ fvec3(0, 0, 0), downDir, color });

    for (size_t i = 0; i < coneSegments; i++)
    {
        fquaternion toSegment(downDir, (i / static_cast<f32>(coneSegments)) * tau);

        fvec3 position = toSegment.rotate(fvec3(radius, 0, 0));
        fvec3 normal = fquaternion(backDir, atan2(height, radius) + (tau / 4)).rotate(leftDir);

        vertices.push_back({ position, normal, color });

        u32 a = 2 + i;
        u32 b = 2 + loopIndex<u32>(i + 1, coneSegments);

        indices.push_back(a);
        indices.push_back(0);
        indices.push_back(b);

        if (invert)
        {
            invertLastTriangleWinding(indices);
        }
    }

    if (capped)
    {
        for (size_t i = 0; i < coneSegments; i++)
        {
            fquaternion toSegment(downDir, (i / static_cast<f32>(coneSegments)) * tau);

            fvec3 position = toSegment.rotate(fvec3(radius, 0, 0));

            vertices.push_back({ position, downDir, color });

            u32 a = 2 + i;
            u32 b = 2 + loopIndex<u32>(i + 1, coneSegments);

            indices.push_back(a);
            indices.push_back(b);
            indices.push_back(1);

            if (invert)
            {
                invertLastTriangleWinding(indices);
            }
        }
    }

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    outVertices.insert(outVertices.end(), vertices.begin(), vertices.end());
    outIndices.insert(outIndices.end(), indices.begin(), indices.end());
}

static BodyRenderData generateConeMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;
    indices.reserve(2 * 3 * 2 * coneSegments);

    vector<LitColoredVertex> vertices;
    vertices.reserve(2 * (1 + coneSegments));

    generateConeHull(
        (solid.size.x / 2) + (lineWidth / 2),
        solid.size.z + ((lineWidth * (solid.size.z / (solid.size.x / 2))) / 2),
        litColor(solid.color),
        true,
        vertices,
        indices,
        false
    );

    generateConeHull(
        (solid.size.x / 2) - (lineWidth / 2),
        solid.size.z - ((lineWidth * (solid.size.z / (solid.size.x / 2))) / 2),
        unlitColor(baseBackgroundColor),
        false,
        vertices,
        indices
    );

    generateCircleBand(
        vec3(),
        quaternion(),
        solid.size.x / 2,
        litColor(solid.color),
        vertices,
        indices
    );

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Cone
    });

    return data;
}

static void generateCapsuleCap(
    const fmat4& transform,
    const vec4& color,
    bool invert,
    vector<LitColoredVertex>& outVertices,
    vector<u32>& outIndices
)
{
    vector<LitColoredVertex> vertices;
    vector<u32> indices;

    u32 indexOffset = outVertices.size();

    vertices.push_back({ fvec3(0, 0, 1), upDir, color });

    for (u32 r = 1; r < (sphereRings / 2) + 1; r++)
    {
        fquaternion toRing(leftDir, (r / static_cast<f32>(sphereRings)) * pi);

        for (u32 s = 0; s < sphereSegments; s++)
        {
            fquaternion toSegment(downDir, (s / static_cast<f32>(sphereSegments)) * tau);

            fvec3 position = toSegment.rotate(toRing.rotate(vertices.front().position));
            fvec3 normal = position.normalize();

            vertices.push_back({ position, normal, color });
        }
    }

    for (u32 r = 0; r < sphereRings / 2; r++)
    {
        u32 elapsed = 1 + max<i32>(static_cast<i32>(r) - 1, 0) * sphereSegments;

        for (u32 s = 0; s < sphereSegments; s++)
        {
            if (r == 0)
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);

                indices.push_back(a);
                indices.push_back(0);
                indices.push_back(b);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }
            }
            else
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);
                u32 c = a + sphereSegments;
                u32 d = b + sphereSegments;

                indices.push_back(b);
                indices.push_back(c);
                indices.push_back(a);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }

                indices.push_back(d);
                indices.push_back(c);
                indices.push_back(b);

                if (invert)
                {
                    invertLastTriangleWinding(indices);
                }
            }
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
    }

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    outVertices.insert(outVertices.end(), vertices.begin(), vertices.end());
    outIndices.insert(outIndices.end(), indices.begin(), indices.end());
}

static void generateCapsuleHull(
    f32 radius,
    f32 height,
    const vec4& color,
    bool invert,
    vector<LitColoredVertex>& outVertices,
    vector<u32>& outIndices
)
{
    vector<LitColoredVertex> vertices;
    vector<u32> indices;

    u32 indexOffset = outVertices.size();

    generateCapsuleCap(
        fmat4(fvec3(0, 0, +height / 2), fquaternion(), fvec3(radius)),
        color,
        invert,
        vertices,
        indices
    );

    generateCylinderHull(
        radius,
        height,
        color,
        invert,
        vertices,
        indices,
        false
    );

    generateCapsuleCap(
        fmat4(fvec3(0, 0, -height / 2), fquaternion(forwardDir, downDir), fvec3(radius)),
        color,
        invert,
        vertices,
        indices
    );

    for (u32& index : indices)
    {
        index += indexOffset;
    }

    outVertices.insert(outVertices.end(), vertices.begin(), vertices.end());
    outIndices.insert(outIndices.end(), indices.begin(), indices.end());
}

static BodyRenderData generateCapsuleMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;
    indices.reserve(2 * ((3 * 2 * sphereSegments * (sphereRings - 1)) + (3 * 2 * cylinderSegments)));

    vector<LitColoredVertex> vertices;
    vertices.reserve(2 * (2 + (sphereSegments * sphereRings)));

    generateCapsuleHull(
        (solid.size.x / 2) + (lineWidth / 2),
        solid.size.z + (lineWidth / 2),
        litColor(solid.color),
        true,
        vertices,
        indices
    );

    generateCapsuleHull(
        (solid.size.x / 2) - (lineWidth / 2),
        solid.size.z - (lineWidth / 2),
        unlitColor(baseBackgroundColor),
        false,
        vertices,
        indices
    );

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Capsule
    });

    return data;
}

static BodyRenderData generatePyramidMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;

    vector<LitColoredVertex> vertices;

    // Depth vertices
    vertices.push_back({ fvec3(0, 0, solid.size.z), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(+solid.size.x / 2, +solid.size.x / 2, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(+solid.size.x / 2, -solid.size.x / 2, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(-solid.size.x / 2, +solid.size.x / 2, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(-solid.size.x / 2, -solid.size.x / 2, 0), fvec3(), unlitColor(baseBackgroundColor) });

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.normal = vertex.position.normalize();
    }

    // Depth indices
    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(1);

    indices.push_back(0);
    indices.push_back(4);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(4);

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(3);

    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(4);

    indices.push_back(1);
    indices.push_back(4);
    indices.push_back(3);

    vector<BuildableLine> lines = {
        { vertices[0].position, vertices[1].position },
        { vertices[0].position, vertices[2].position },
        { vertices[0].position, vertices[3].position },
        { vertices[0].position, vertices[4].position },

        { vertices[1].position, vertices[2].position },
        { vertices[2].position, vertices[4].position },
        { vertices[4].position, vertices[3].position },
        { vertices[3].position, vertices[1].position }
    };

    optional<BuiltLines> builtLines = buildLines(
        lineSegments,
        lineWidth / 2,
        solid.color,
        lines
    );

    if (builtLines)
    {
        for (u32& index : builtLines->indices)
        {
            index += vertices.size();
        }

        vertices.insert(vertices.end(), builtLines->vertices.begin(), builtLines->vertices.end());
        indices.insert(indices.end(), builtLines->indices.begin(), builtLines->indices.end());
    }
    else
    {
        error("Failed to construct pyramid mesh lines.");
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Pyramid
    });

    return data;
}

static BodyRenderData generateTetrahedronMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;

    vector<LitColoredVertex> vertices;

    // Depth vertices
    fvec3 ringStart = fquaternion(leftDir, radians(120)).rotate(fvec3(0, 0, solid.size.x / 2));

    vertices.push_back({ fvec3(0, 0, solid.size.x / 2), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ ringStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, radians(+120)).rotate(ringStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, radians(-120)).rotate(ringStart), fvec3(), unlitColor(baseBackgroundColor) });

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.normal = vertex.position.normalize();
    }

    // Depth indices
    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(1);

    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(3);

    indices.push_back(1);
    indices.push_back(3);
    indices.push_back(2);

    vector<BuildableLine> lines = {
        { vertices[0].position, vertices[1].position },
        { vertices[0].position, vertices[2].position },
        { vertices[0].position, vertices[3].position },

        { vertices[1].position, vertices[2].position },
        { vertices[2].position, vertices[3].position },
        { vertices[3].position, vertices[1].position }
    };

    optional<BuiltLines> builtLines = buildLines(
        lineSegments,
        lineWidth / 2,
        solid.color,
        lines
    );

    if (builtLines)
    {
        for (u32& index : builtLines->indices)
        {
            index += vertices.size();
        }

        vertices.insert(vertices.end(), builtLines->vertices.begin(), builtLines->vertices.end());
        indices.insert(indices.end(), builtLines->indices.begin(), builtLines->indices.end());
    }
    else
    {
        error("Failed to construct tetrahedron mesh lines.");
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Tetrahedron
    });

    return data;
}

static BodyRenderData generateOctahedronMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;

    vector<LitColoredVertex> vertices;

    // Depth vertices
    vertices.push_back({ fvec3(+solid.size.x / 2, 0, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(-solid.size.x / 2, 0, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(0, +solid.size.x / 2, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(0, -solid.size.x / 2, 0), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(0, 0, +solid.size.x / 2), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(0, 0, -solid.size.x / 2), fvec3(), unlitColor(baseBackgroundColor) });

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.normal = vertex.position.normalize();
    }

    // Depth indices
    indices.push_back(0);
    indices.push_back(2);
    indices.push_back(4);

    indices.push_back(0);
    indices.push_back(4);
    indices.push_back(3);

    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(5);

    indices.push_back(0);
    indices.push_back(5);
    indices.push_back(2);

    indices.push_back(1);
    indices.push_back(4);
    indices.push_back(2);

    indices.push_back(1);
    indices.push_back(3);
    indices.push_back(4);

    indices.push_back(1);
    indices.push_back(5);
    indices.push_back(3);

    indices.push_back(1);
    indices.push_back(2);
    indices.push_back(5);

    vector<BuildableLine> lines = {
        { vertices[4].position, vertices[0].position },
        { vertices[4].position, vertices[1].position },
        { vertices[4].position, vertices[2].position },
        { vertices[4].position, vertices[3].position },

        { vertices[5].position, vertices[0].position },
        { vertices[5].position, vertices[1].position },
        { vertices[5].position, vertices[2].position },
        { vertices[5].position, vertices[3].position },

        { vertices[0].position, vertices[2].position },
        { vertices[2].position, vertices[1].position },
        { vertices[1].position, vertices[3].position },
        { vertices[3].position, vertices[0].position }
    };

    optional<BuiltLines> builtLines = buildLines(
        lineSegments,
        lineWidth / 2,
        solid.color,
        lines
    );

    if (builtLines)
    {
        for (u32& index : builtLines->indices)
        {
            index += vertices.size();
        }

        vertices.insert(vertices.end(), builtLines->vertices.begin(), builtLines->vertices.end());
        indices.insert(indices.end(), builtLines->indices.begin(), builtLines->indices.end());
    }
    else
    {
        error("Failed to construct octahedron mesh lines.");
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Octahedron
    });

    return data;
}

static void assemblePentagon(u32 a, u32 b, u32 c, u32 d, u32 e, vector<u32>& indices)
{
    indices.push_back(a);
    indices.push_back(b);
    indices.push_back(c);

    indices.push_back(a);
    indices.push_back(c);
    indices.push_back(d);

    indices.push_back(d);
    indices.push_back(e);
    indices.push_back(a);
}

static BodyRenderData generateDodecahedronMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;

    vector<LitColoredVertex> vertices;

    // Depth vertices
    fvec3 upperRingStart = static_cast<f32>(solid.size.x / 2) * fvec3(+dodecahedronRingStart);
    fvec3 upperMiddleRingStart = static_cast<f32>(solid.size.x / 2) * fvec3(+dodecahedronMiddleRingStart);
    fvec3 lowerMiddleRingStart = static_cast<f32>(solid.size.x / 2) * fvec3(-dodecahedronMiddleRingStart);
    fvec3 lowerRingStart = static_cast<f32>(solid.size.x / 2) * fvec3(-dodecahedronRingStart);

    vertices.push_back({ upperRingStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });

    vertices.push_back({ upperMiddleRingStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 1 * (tau / 5)).rotate(upperMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 2 * (tau / 5)).rotate(upperMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 3 * (tau / 5)).rotate(upperMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 4 * (tau / 5)).rotate(upperMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });

    vertices.push_back({ lowerMiddleRingStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 1 * (tau / 5)).rotate(lowerMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 2 * (tau / 5)).rotate(lowerMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 3 * (tau / 5)).rotate(lowerMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 4 * (tau / 5)).rotate(lowerMiddleRingStart), fvec3(), unlitColor(baseBackgroundColor) });

    vertices.push_back({ lowerRingStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.normal = vertex.position.normalize();
    }

    // Depth indices
    assemblePentagon(0, 1, 2, 3, 4, indices);

    assemblePentagon(5, 13, 6, 1, 0, indices);
    assemblePentagon(6, 14, 7, 2, 1, indices);
    assemblePentagon(7, 10, 8, 3, 2, indices);
    assemblePentagon(8, 11, 9, 4, 3, indices);
    assemblePentagon(9, 12, 5, 0, 4, indices);

    assemblePentagon(15, 16, 11, 8, 10, indices);
    assemblePentagon(16, 17, 12, 9, 11, indices);
    assemblePentagon(17, 18, 13, 5, 12, indices);
    assemblePentagon(18, 19, 14, 6, 13, indices);
    assemblePentagon(19, 15, 10, 7, 14, indices);

    assemblePentagon(19, 18, 17, 16, 15, indices);

    vector<BuildableLine> lines = {
        // top side
        { vertices[0].position, vertices[1].position },
        { vertices[1].position, vertices[2].position },
        { vertices[2].position, vertices[3].position },
        { vertices[3].position, vertices[4].position },
        { vertices[4].position, vertices[0].position },

        // bottom side
        { vertices[15].position, vertices[16].position },
        { vertices[16].position, vertices[17].position },
        { vertices[17].position, vertices[18].position },
        { vertices[18].position, vertices[19].position },
        { vertices[19].position, vertices[15].position },

        // top to upper-middle
        { vertices[0].position, vertices[5].position },
        { vertices[1].position, vertices[6].position },
        { vertices[2].position, vertices[7].position },
        { vertices[3].position, vertices[8].position },
        { vertices[4].position, vertices[9].position },

        // bottom to lower-middle
        { vertices[15].position, vertices[10].position },
        { vertices[16].position, vertices[11].position },
        { vertices[17].position, vertices[12].position },
        { vertices[18].position, vertices[13].position },
        { vertices[19].position, vertices[14].position },

        // middle ring
        { vertices[5].position, vertices[13].position },
        { vertices[13].position, vertices[6].position },
        { vertices[6].position, vertices[14].position },
        { vertices[14].position, vertices[7].position },
        { vertices[7].position, vertices[10].position },
        { vertices[10].position, vertices[8].position },
        { vertices[8].position, vertices[11].position },
        { vertices[11].position, vertices[9].position },
        { vertices[9].position, vertices[12].position },
        { vertices[12].position, vertices[5].position }
    };

    optional<BuiltLines> builtLines = buildLines(
        lineSegments,
        lineWidth / 2,
        solid.color,
        lines
    );

    if (builtLines)
    {
        for (u32& index : builtLines->indices)
        {
            index += vertices.size();
        }

        vertices.insert(vertices.end(), builtLines->vertices.begin(), builtLines->vertices.end());
        indices.insert(indices.end(), builtLines->indices.begin(), builtLines->indices.end());
    }
    else
    {
        error("Failed to construct dodecahedron mesh lines.");
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Dodecahedron
    });

    return data;
}

static BodyRenderData generateIcosahedronMesh(const mat4& transform, const BodyPartSolid& solid)
{
    vector<u32> indices;

    vector<LitColoredVertex> vertices;

    // Depth vertices
    fvec3 upperRingStart = static_cast<f32>(solid.size.x / 2) * fvec3(+icosahedronRingStart);
    fvec3 lowerRingStart = static_cast<f32>(solid.size.x / 2) * fvec3(-icosahedronRingStart);

    vertices.push_back({ fvec3(0, 0, +solid.size.x / 2), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fvec3(0, 0, -solid.size.x / 2), fvec3(), unlitColor(baseBackgroundColor) });

    vertices.push_back({ upperRingStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart), fvec3(), unlitColor(baseBackgroundColor) });

    vertices.push_back({ lowerRingStart, fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });
    vertices.push_back({ fquaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart), fvec3(), unlitColor(baseBackgroundColor) });

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.normal = vertex.position.normalize();
    }

    // Depth indices
    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(0);
        indices.push_back(2 + i);
        indices.push_back(2 + loopIndex<u32>(i + 1, 5));
    }

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(1);
        indices.push_back(7 + loopIndex<u32>(i + 1, 5));
        indices.push_back(7 + i);
    }

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(2 + i);
        indices.push_back(7 + loopIndex<u32>(3 + i, 5));
        indices.push_back(2 + loopIndex<u32>(i + 1, 5));
    }

    for (u32 i = 0; i < 5; i++)
    {
        indices.push_back(7 + i);
        indices.push_back(7 + loopIndex<u32>(i + 1, 5));
        indices.push_back(2 + loopIndex<u32>(3 + i, 5));
    }

    vector<BuildableLine> lines = {
        // top side
        { vertices[0].position, vertices[2].position },
        { vertices[0].position, vertices[3].position },
        { vertices[0].position, vertices[4].position },
        { vertices[0].position, vertices[5].position },
        { vertices[0].position, vertices[6].position },

        // bottom side
        { vertices[1].position, vertices[7].position },
        { vertices[1].position, vertices[8].position },
        { vertices[1].position, vertices[9].position },
        { vertices[1].position, vertices[10].position },
        { vertices[1].position, vertices[11].position },

        // top ring
        { vertices[2].position, vertices[3].position },
        { vertices[3].position, vertices[4].position },
        { vertices[4].position, vertices[5].position },
        { vertices[5].position, vertices[6].position },
        { vertices[6].position, vertices[2].position },

        // bottom ring
        { vertices[7].position, vertices[8].position },
        { vertices[8].position, vertices[9].position },
        { vertices[9].position, vertices[10].position },
        { vertices[10].position, vertices[11].position },
        { vertices[11].position, vertices[7].position },

        // top to bottom
        { vertices[2].position, vertices[10].position },
        { vertices[3].position, vertices[11].position },
        { vertices[4].position, vertices[7].position },
        { vertices[5].position, vertices[8].position },
        { vertices[6].position, vertices[9].position },

        // bottom to top
        { vertices[7].position, vertices[5].position },
        { vertices[8].position, vertices[6].position },
        { vertices[9].position, vertices[2].position },
        { vertices[10].position, vertices[3].position },
        { vertices[11].position, vertices[4].position }
    };

    optional<BuiltLines> builtLines = buildLines(
        lineSegments,
        lineWidth / 2,
        solid.color,
        lines
    );

    if (builtLines)
    {
        for (u32& index : builtLines->indices)
        {
            index += vertices.size();
        }

        vertices.insert(vertices.end(), builtLines->vertices.begin(), builtLines->vertices.end());
        indices.insert(indices.end(), builtLines->indices.begin(), builtLines->indices.end());
    }
    else
    {
        error("Failed to construct icosahedron mesh lines.");
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    data.occlusionPrimitives.push_back({
        transform,
        solid.size,
        Body_Render_Occlusion_Primitive_Pyramid
    });

    return data;
}

BodyRenderData generateSolid(const mat4& transform, const BodyPartSolid& solid)
{
    switch (solid.type)
    {
    default :
        fatal("Encountered unknown solid type '" + to_string(solid.type) + "'.");
    case Body_Solid_Cuboid :
        return generateCuboidMesh(transform, solid);
    case Body_Solid_Sphere :
        return generateSphereMesh(transform, solid);
    case Body_Solid_Cylinder :
        return generateCylinderMesh(transform, solid);
    case Body_Solid_Cone :
        return generateConeMesh(transform, solid);
    case Body_Solid_Capsule :
        return generateCapsuleMesh(transform, solid);
    case Body_Solid_Pyramid :
        return generatePyramidMesh(transform, solid);
    case Body_Solid_Tetrahedron :
        return generateTetrahedronMesh(transform, solid);
    case Body_Solid_Octahedron :
        return generateOctahedronMesh(transform, solid);
    case Body_Solid_Dodecahedron :
        return generateDodecahedronMesh(transform, solid);
    case Body_Solid_Icosahedron :
        return generateIcosahedronMesh(transform, solid);
    }
}

BodyRenderData generateText(const mat4& transform, const BodyPartText& text)
{
    fvec4 color = text.color.toVec4();

    Image* texture = textResolver->resolve(text);

    ivec2 tiles = ceil(vec2(texture->width(), texture->height()) / static_cast<f64>(textureTileSize));

    f64 width = text.height * (texture->width() / static_cast<f64>(texture->height()));
    f64 height = text.height;

    vector<u32> indices;
    vector<LitTexturedVertex> vertices;

    vertices.push_back({ fvec3(-(width / 2), -(height / 2), 0), fvec2(0, 0), upDir });
    vertices.push_back({ fvec3(+(width / 2), -(height / 2), 0), fvec2(1, 0), upDir });
    vertices.push_back({ fvec3(-(width / 2), +(height / 2), 0), fvec2(0, 1), upDir });
    vertices.push_back({ fvec3(+(width / 2), +(height / 2), 0), fvec2(1, 1), upDir });

    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(3);

    for (LitTexturedVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitTexturedVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitTexturedVertex));

    vector<f32> uniformData(4 + 2, 0);
    memcpy(uniformData.data(), &color, sizeof(fvec4));
    memcpy(uniformData.data() + 4, &tiles, sizeof(ivec2));

    BodyRenderData data;
    data.materials.push_back({ Body_Render_Material_Texture, BodyRenderMaterialData() });

    BodyRenderMaterialData& textureMaterial = get<1>(data.materials.front());
    textureMaterial.indices = indices;
    textureMaterial.vertexData = vertexData;
    textureMaterial.vertexCount = vertices.size();
    textureMaterial.uniformData = uniformData;
    textureMaterial.textures = { { texture->width(), texture->height(), vector<u8>(texture->size()) } };
    textureMaterial.position = transform.position();

    memcpy(textureMaterial.textures.front().data.data(), texture->pixels(), texture->size());

    delete texture;

    return data;
}

BodyRenderData generateSymbol(const mat4& transform, const BodyPartSymbol& symbol)
{
    fvec4 color = symbol.color.toVec4();

    u32 resolution = min<u32>(symbol.size * symbolPixelsPerUnit, maxSymbolPixels);

    Image* texture = symbolResolver->resolve(symbol.type, resolution);

    ivec2 tiles = ceil(vec2(texture->width(), texture->height()) / static_cast<f64>(textureTileSize));

    vector<u32> indices;
    vector<LitTexturedVertex> vertices;

    vertices.push_back({ fvec3(-(symbol.size / 2), -(symbol.size / 2), 0), fvec2(0, 0), upDir });
    vertices.push_back({ fvec3(+(symbol.size / 2), -(symbol.size / 2), 0), fvec2(1, 0), upDir });
    vertices.push_back({ fvec3(-(symbol.size / 2), +(symbol.size / 2), 0), fvec2(0, 1), upDir });
    vertices.push_back({ fvec3(+(symbol.size / 2), +(symbol.size / 2), 0), fvec2(1, 1), upDir });

    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(3);

    for (LitTexturedVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitTexturedVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitTexturedVertex));

    vector<f32> uniformData(4 + 2, 0);
    memcpy(uniformData.data(), &color, sizeof(fvec4));
    memcpy(uniformData.data() + 4, &tiles, sizeof(ivec2));

    BodyRenderData data;
    data.materials.push_back({ Body_Render_Material_Texture, BodyRenderMaterialData() });

    BodyRenderMaterialData& textureMaterial = get<1>(data.materials.front());
    textureMaterial.indices = indices;
    textureMaterial.vertexData = vertexData;
    textureMaterial.vertexCount = vertices.size();
    textureMaterial.uniformData = uniformData;
    textureMaterial.textures = { { texture->width(), texture->height(), vector<u8>(texture->size()) } };
    textureMaterial.position = transform.position();

    memcpy(textureMaterial.textures.front().data.data(), texture->pixels(), texture->size());

    delete texture;

    return data;
}

BodyRenderData generateCanvas(const mat4& transform, const BodyPartCanvas& canvas)
{
    fvec4 color = canvas.color.toVec4();

    ivec2 tiles = ceil(vec2(canvasResolution) / static_cast<f64>(textureTileSize));

    vector<u32> indices;
    vector<LitTexturedVertex> vertices;

    vertices.push_back({ fvec3(-(canvas.size / 2), -(canvas.size / 2), 0), fvec2(0, 0), upDir });
    vertices.push_back({ fvec3(+(canvas.size / 2), -(canvas.size / 2), 0), fvec2(1, 0), upDir });
    vertices.push_back({ fvec3(-(canvas.size / 2), +(canvas.size / 2), 0), fvec2(0, 1), upDir });
    vertices.push_back({ fvec3(+(canvas.size / 2), +(canvas.size / 2), 0), fvec2(1, 1), upDir });

    indices.push_back(0);
    indices.push_back(3);
    indices.push_back(2);

    indices.push_back(0);
    indices.push_back(1);
    indices.push_back(3);

    for (LitTexturedVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitTexturedVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitTexturedVertex));

    vector<f32> uniformData(4 + 2, 0);
    memcpy(uniformData.data(), &color, sizeof(fvec4));
    memcpy(uniformData.data() + 4, &tiles, sizeof(ivec2));

    BodyRenderData data;
    data.materials.push_back({ Body_Render_Material_Texture, BodyRenderMaterialData() });

    BodyRenderMaterialData& textureMaterial = get<1>(data.materials.front());
    textureMaterial.indices = indices;
    textureMaterial.vertexData = vertexData;
    textureMaterial.vertexCount = vertices.size();
    textureMaterial.uniformData = uniformData;
    textureMaterial.textures = { { canvasResolution, canvasResolution, vector<u8>(canvasResolution * canvasResolution * 4) } };
    textureMaterial.position = transform.position();

    vector<u32> pixels(canvasResolution * canvasResolution, 0);

    for (u32 y = 0; y < canvasResolution; y++)
    {
        for (u32 x = 0; x < canvasResolution; x++)
        {
            u32 index = y * canvasResolution + x;
            pixels[index] = canvas.get(vec2(x, y)) * 0xff000000;
        }
    }

    memcpy(textureMaterial.textures.front().data.data(), pixels.data(), pixels.size() * sizeof(u32));

    return data;
}

BodyRenderData generateStructure(const mat4& transform, const BodyPartStructure& structure)
{
    const StructureMesh& mesh = meshCache->ensure(structure.node);

    vector<LitColoredVertex> vertices;
    vertices.reserve(mesh.vertices.size());

    for (const auto& [ position, normal, foreground ] : mesh.vertices)
    {
        vertices.push_back({
            position,
            normal,
            foreground ? fvec4(structure.color.toVec4()) : unlitColor(baseBackgroundColor)
        });
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32));
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = mesh.indices;
    material.vertexData = vertexData;
    material.vertexCount = mesh.vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    return data;
}

BodyRenderData generateTerrain(const mat4& transform, const BodyPartTerrain& terrain)
{
    const TerrainMesh& mesh = meshCache->ensure(terrain.color, terrain.nodes);

    vector<LitColoredVertex> vertices;
    vertices.reserve(mesh.vertices.size());

    for (const TerrainVertex& vertex : mesh.vertices)
    {
        vertices.push_back({
            vertex.position,
            vertex.normal,
            vertex.color
        });
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32));
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = mesh.indices;
    material.vertexData = vertexData;
    material.vertexCount = mesh.vertices.size();

    data.materials.push_back({ Body_Render_Material_Terrain, material });

    return data;
}

BodyRenderData generateLight(const mat4& transform, const BodyPartLight& light)
{
    if (!light.enabled)
    {
        return BodyRenderData();
    }

    vector<u32> indices;
    vector<LitColoredVertex> vertices;

    vertices.push_back({ fvec3(0, 0, +1), upDir, litColor(light.color) });
    vertices.push_back({ fvec3(0, 0, -1), downDir, litColor(light.color) });

    for (u32 r = 1; r < sphereRings; r++)
    {
        fquaternion toRing(leftDir, (r / static_cast<f32>(sphereRings)) * pi);

        for (u32 s = 0; s < sphereSegments; s++)
        {
            fquaternion toSegment(downDir, (s / static_cast<f32>(sphereSegments)) * tau);

            fvec3 position = toSegment.rotate(toRing.rotate(vertices.front().position));
            fvec3 normal = position.normalize();

            vertices.push_back({ position, normal, litColor(light.color) });
        }
    }

    for (u32 r = 0; r < sphereRings; r++)
    {
        u32 elapsed = 2 + max<i32>(static_cast<i32>(r) - 1, 0) * sphereSegments;

        for (u32 s = 0; s < sphereSegments; s++)
        {
            if (r == 0)
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);

                indices.push_back(a);
                indices.push_back(0);
                indices.push_back(b);
            }
            else if (r + 1 == sphereRings)
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);

                indices.push_back(b);
                indices.push_back(1);
                indices.push_back(a);
            }
            else
            {
                u32 a = elapsed + s;
                u32 b = elapsed + loopIndex<u32>(s + 1, sphereSegments);
                u32 c = a + sphereSegments;
                u32 d = b + sphereSegments;

                indices.push_back(b);
                indices.push_back(c);
                indices.push_back(a);

                indices.push_back(d);
                indices.push_back(c);
                indices.push_back(b);
            }
        }
    }

    for (LitColoredVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position * lightSphereRadius);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitColoredVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitColoredVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();

    data.materials.push_back({ Body_Render_Material_Vertex, material });

    BodyRenderLight lightData;
    lightData.transform = transform;
    lightData.color = light.color.toVec4();
    lightData.angle = light.angle;

    data.lights.push_back(lightData);

    return data;
}

BodyRenderData generateArea(const mat4& transform, const BodyPartArea& area)
{
    if (!area.surface)
    {
        return BodyRenderData();
    }

    vector<LitTexturedVertex> vertices;
    vertices.reserve(area.points.size());

    for (const vec2& point : area.points)
    {
        vertices.push_back({ vec3(point, +area.height / 2), point, upDir });
    }

    vector<u32> endCapIndices = triangulatePolygon(area.points);

    vector<u32> indices;
    indices.reserve(endCapIndices.size());

    for (size_t i = 0; i < endCapIndices.size(); i += 3)
    {
        indices.push_back(endCapIndices[i + 0]);
        indices.push_back(endCapIndices[i + 1]);
        indices.push_back(endCapIndices[i + 2]);

        invertLastTriangleWinding(indices);
    }

    for (LitTexturedVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.normal = transform.applyToDirection(vertex.normal);
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitTexturedVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitTexturedVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();
    material.uniformData = vector<f32>((sizeof(fvec4) + sizeof(f32) + sizeof(i32)) / sizeof(f32));

    fvec4 color = area.color.toVec4();
    f32 animationSpeed = area.animationSpeed;
    i32 opaque = area.opaque;

    memcpy(material.uniformData.data(), &color, sizeof(color));
    memcpy(material.uniformData.data() + 4, &animationSpeed, sizeof(animationSpeed));
    memcpy(material.uniformData.data() + 5, &opaque, sizeof(opaque));

    data.materials.push_back({ Body_Render_Material_Area, material });

    return data;
}

BodyRenderData generateRope(const mat4& transform, const BodyPartRope& rope)
{
    vector<LitRopeVertex> vertices;
    vertices.reserve(rope.points.size());

    quaternion rotation;

    for (u32 i = 0; i < rope.points.size(); i++)
    {
        const vec3& point = rope.points[i];

        fvec3 scale;

        if (i == 0)
        {
            vec3 direction = (rope.points[i + 1] - point).normalize();

            rotation = quaternion(
                direction,
                pickUpDirection(direction)
            );
        }
        else if (i + 1 == rope.points.size())
        {
            vec3 direction = (point - rope.points[i - 1]).normalize();

            if (!roughly(abs(rotation.forward().dot(direction)), 1.0))
            {
                vec3 axis = rotation.forward().cross(direction).normalize();
                f64 angle = handedAngleBetween(rotation.forward(), direction, axis);

                rotation = quaternion(axis, angle) * rotation;
            }
        }
        else
        {
            vec3 direction = (rope.points[i + 1] - point).normalize();

            if (!roughly(abs(rotation.forward().dot(direction)), 1.0))
            {
                vec3 axis = rotation.forward().cross(direction).normalize();
                f64 angle = handedAngleBetween(rotation.forward(), direction, axis) / 2;

                vec3 scaleVector = (direction - rotation.forward()).normalize() * (1 / ((1 / sin(pi / 2)) * sin(pi - ((pi / 2) + abs(angle)))));

                rotation = quaternion(axis, angle) * rotation;

                scaleVector = rotation.inverse().rotate(scaleVector);

                scale = scaleVector;
            }
        }

        f64 t = i / static_cast<f64>(rope.segmentCount);

        vertices.push_back({
            point,
            rotation.toVec4(),
            scale,
            fvec4(rope.startColor.toVec4() * (1 - t) + rope.endColor.toVec4() * t)
        });
    }

    vector<u32> indices;
    indices.reserve(rope.segmentCount * 2);

    for (u32 i = 0; i < rope.segmentCount; i++)
    {
        indices.push_back(i);
        indices.push_back(i + 1);
    }

    for (LitRopeVertex& vertex : vertices)
    {
        vertex.position = transform.applyToPosition(vertex.position);
        vertex.rotation = transform.applyToRotation(quaternion(vertex.rotation)).toVec4();
    }

    vector<f32> vertexData((vertices.size() * sizeof(LitRopeVertex)) / sizeof(f32), 0);
    memcpy(vertexData.data(), vertices.data(), vertices.size() * sizeof(LitRopeVertex));

    BodyRenderData data;

    BodyRenderMaterialData material;
    material.indices = indices;
    material.vertexData = vertexData;
    material.vertexCount = vertices.size();
    material.uniformData = vector<f32>((sizeof(f32) + sizeof(i32)) / sizeof(f32));

    f32 radius = rope.radius;
    i32 thin = rope.thin;

    memcpy(material.uniformData.data(), &radius, sizeof(radius));
    memcpy(material.uniformData.data() + 1, &thin, sizeof(thin));

    data.materials.push_back({ Body_Render_Material_Rope, material });

    return data;
}
