/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_swapchain_elements.hpp"
#include "render_post_process_effect_component.hpp"

class RenderPostProcessEffect
{
public:
    RenderPostProcessEffect(
        const RenderContext* ctx,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    );
    RenderPostProcessEffect(const RenderPostProcessEffect& rhs) = delete;
    RenderPostProcessEffect(RenderPostProcessEffect&& rhs) = delete;
    virtual ~RenderPostProcessEffect() = 0;

    RenderPostProcessEffect& operator=(const RenderPostProcessEffect& rhs) = delete;
    RenderPostProcessEffect& operator=(RenderPostProcessEffect&& rhs) = delete;

    void show(bool state);
    bool shown() const;

    void work(const SwapchainElement* swapchainElement, f32 timer) const;

protected:
    bool _shown;

    const RenderContext* ctx;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;

    vector<RenderPostProcessEffectComponent*> components;

    void destroyComponents();

    virtual VmaBuffer createUniformBuffer() const = 0;
    virtual void fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const = 0;
    void setDescriptorSet(
        VkDescriptorSet descriptorSet,
        VkImageView colorImageView
    ) const;
    void setDescriptorSet(
        VkDescriptorSet descriptorSet,
        VkBuffer buffer,
        VkImageView colorImageView
    ) const;
    void setDescriptorSet(
        VkDescriptorSet descriptorSet,
        VkBuffer buffer,
        VkImageView colorImageView,
        VkImageView depthImageView
    ) const;
};
