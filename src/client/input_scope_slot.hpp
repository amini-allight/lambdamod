/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_types.hpp"

// This exists to allow the VR menu to submit an input (changeFocus) which runs alongside other inputs sharing the same key, but only if those other inputs originated from widgets lying below it in the widget tree
// This means that scopes parallel/above in the tree like the VR keyboard can still steal focus, preventing punch-through when using the keyboard
// But scopes below in the tree like the panel tab view's tab bar cannot steal focus, preventing the pointer from getting permanently stuck on them
enum InputScopeSlotInclusionMode : u8
{
    Input_Scope_Slot_Inclusion_None,
    Input_Scope_Slot_Inclusion_With_Above,
    Input_Scope_Slot_Inclusion_With_Below,
    Input_Scope_Slot_Inclusion_With_Both
};

class InputScopeSlot
{
public:
    explicit InputScopeSlot(
        const function<void(const Input&)>& behavior,
        bool replayable = false,
        InputScopeSlotInclusionMode inclusionMode = Input_Scope_Slot_Inclusion_None
    );

    function<void(const Input&)> behavior;
    bool replayable;
    InputScopeSlotInclusionMode inclusionMode;
};
