/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "gizmo_intersector.hpp"
#include "paths.hpp"
#include "tools.hpp"
#include "intersections.hpp"
#include "render_constants.hpp"
#include "edit_tool_axis.hpp"

optional<vec3> GizmoIntersectorTriangle::intersect(const vec3& cameraPosition, const vec3& selectionDirection) const
{
    vector<vec3> intersections = intersectTriangle(
        a,
        b,
        c,
        cameraPosition,
        selectionDirection,
        numeric_limits<f64>::max()
    );

    return intersections.empty() ? optional<vec3>() : intersections.front();
}

GizmoIntersector::GizmoIntersector()
{
    string arrow = getFile(meshPath + "/" + gizmoArrowFileName + meshExt);
    vector<GizmoIntersectorTriangle> arrowTriangles(arrow.size() / sizeof(GizmoIntersectorTriangle));
    memcpy(arrowTriangles.data(), arrow.data(), arrow.size());

    string ring = getFile(meshPath + "/" + gizmoRingFileName + meshExt);
    vector<GizmoIntersectorTriangle> ringTriangles(ring.size() / sizeof(GizmoIntersectorTriangle));
    memcpy(ringTriangles.data(), ring.data(), ring.size());

    string planes = getFile(meshPath + "/" + gizmoPlanesFileName + meshExt);
    vector<GizmoIntersectorTriangle> planesTriangles(planes.size() / sizeof(GizmoIntersectorTriangle));
    memcpy(planesTriangles.data(), planes.data(), planes.size());

    elements.push_back({
        Transform_Translate,
        Edit_Tool_Axis_Global_X,
        transformTriangles(gizmoElementTransforms[0], arrowTriangles)
    });

    elements.push_back({
        Transform_Translate,
        Edit_Tool_Axis_Global_Y,
        transformTriangles(gizmoElementTransforms[1], arrowTriangles)
    });

    elements.push_back({
        Transform_Translate,
        Edit_Tool_Axis_Global_Z,
        transformTriangles(gizmoElementTransforms[2], arrowTriangles)
    });

    elements.push_back({
        Transform_Rotate,
        Edit_Tool_Axis_Global_X,
        transformTriangles(gizmoElementTransforms[0], ringTriangles)
    });

    elements.push_back({
        Transform_Rotate,
        Edit_Tool_Axis_Global_Y,
        transformTriangles(gizmoElementTransforms[1], ringTriangles)
    });

    elements.push_back({
        Transform_Rotate,
        Edit_Tool_Axis_Global_Z,
        transformTriangles(gizmoElementTransforms[2], ringTriangles)
    });

    elements.push_back({
        Transform_Scale,
        Edit_Tool_Axis_Global_X,
        transformTriangles(gizmoElementTransforms[0], planesTriangles)
    });

    elements.push_back({
        Transform_Scale,
        Edit_Tool_Axis_Global_Y,
        transformTriangles(gizmoElementTransforms[1], planesTriangles)
    });

    elements.push_back({
        Transform_Scale,
        Edit_Tool_Axis_Global_Z,
        transformTriangles(gizmoElementTransforms[2], planesTriangles)
    });
}

GizmoIntersector::~GizmoIntersector()
{
    // Do nothing
}

optional<tuple<TransformMode, EditToolAxis>> GizmoIntersector::select(
    const vec3& gizmoPosition,
    const quaternion& gizmoRotation,
    const vec3& cameraPosition,
    const vec3& selectionDirection
)
{
    mat4 gizmoToWorld(gizmoPosition, gizmoRotation, vec3((gizmoPosition.distance(cameraPosition) / gizmoBaseDistance)));

    vec3 localCameraPosition = gizmoToWorld.applyToPosition(cameraPosition, false);
    vec3 localSelectionDirection = gizmoToWorld.applyToDirection(selectionDirection, false);

    optional<tuple<TransformMode, EditToolAxis>> bestMode;
    optional<vec3> bestIntersection;

    for (const GizmoIntersectorElement& element : elements)
    {
        for (const GizmoIntersectorTriangle& triangle : element.triangles)
        {
            optional<vec3> intersection = triangle.intersect(localCameraPosition, localSelectionDirection);

            if (intersection &&
                (!bestIntersection || localCameraPosition.distance(*intersection) < localCameraPosition.distance(*bestIntersection))
            )
            {
                bestMode = { element.mode, element.axis };
                bestIntersection = intersection;
            }
        }
    }

    return bestMode;
}

vector<GizmoIntersectorTriangle> GizmoIntersector::transformTriangles(
    const fmat4& transform,
    vector<GizmoIntersectorTriangle> triangles
) const
{
    for (GizmoIntersectorTriangle& triangle : triangles)
    {
        triangle.a = transform.applyToPosition(triangle.a);
        triangle.b = transform.applyToPosition(triangle.b);
        triangle.c = transform.applyToPosition(triangle.c);
    }

    return triangles;
}
