/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_area_bounds.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

RenderAreaBounds::RenderAreaBounds(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    const vector<vec2>& points,
    f64 height
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, rotation, vec3(1) };

    vertexCount = 2 * 3 * points.size();

    vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));

    vector<ColoredVertex> vertexData;
    vertexData.reserve(vertexCount);

    fvec3 color = theme.panelForegroundColor.toVec3();

    for (size_t i = 0; i < points.size(); i++)
    {
        const vec2& point = points[i];
        const vec2& nextPoint = points[loopIndex(i + 1, points.size())];

        vec3 top = vec3(point, +height / 2);
        vec3 bottom = vec3(point, -height / 2);

        vec3 nextTop = vec3(nextPoint, +height / 2);
        vec3 nextBottom = vec3(nextPoint, -height / 2);

        vertexData.push_back({ top, color });
        vertexData.push_back({ bottom, color });

        vertexData.push_back({ top, color });
        vertexData.push_back({ nextTop, color });

        vertexData.push_back({ bottom, color });
        vertexData.push_back({ nextBottom, color });
    }

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderAreaBounds::~RenderAreaBounds()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderAreaBounds::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderAreaBounds::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
