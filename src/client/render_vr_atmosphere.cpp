/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_atmosphere.hpp"

RenderVRAtmosphere::RenderVRAtmosphere(
    const RenderInterface* render,

    const vector<VRSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    const RenderVRPipelineStore* pipelineStore
)
    : RenderAtmosphere(
        render,
        descriptorSetLayout,
        pipelineStore->atmosphereLightning.pipelineLayout,
        pipelineStore->atmosphereLightning.pipeline,
        pipelineStore->atmosphereLine.pipelineLayout,
        pipelineStore->atmosphereLine.pipeline,
        pipelineStore->atmosphereSolid.pipelineLayout,
        pipelineStore->atmosphereSolid.pipeline,
        pipelineStore->atmosphereCloud.pipelineLayout,
        pipelineStore->atmosphereCloud.pipeline,
        pipelineStore->atmosphereStreamer.pipelineLayout,
        pipelineStore->atmosphereStreamer.pipeline,
        pipelineStore->atmosphereWarp.pipelineLayout,
        pipelineStore->atmosphereWarp.pipeline
    )
    , swapchainElements(swapchainElements)
{
    createComponents();
}

RenderVRAtmosphere::~RenderVRAtmosphere()
{
    destroyComponents();
}

void RenderVRAtmosphere::work(
    const VRSwapchainElement* swapchainElement,
    const AtmosphereParameters& atmosphere,
    vec3 cameraPosition[eyeCount],
    const vec3& up,
    const vec3& gravity,
    const vec3& flowVelocity,
    f64 density
)
{
    if (!atmosphere.enabled)
    {
        return;
    }

    update(
        swapchainElement,
        atmosphere,
        cameraPosition,
        up,
        gravity,
        flowVelocity,
        density
    );

    fillCommandBuffer(
        swapchainElement,
        atmosphere,
        gravity,
        density
    );
}

void RenderVRAtmosphere::refresh(const vector<VRSwapchainElement*>& swapchainElements)
{
    this->swapchainElements = swapchainElements;

    destroyComponents();

    createComponents();
}

void RenderVRAtmosphere::createComponents()
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        components.push_back(new RenderAtmosphereComponent(
            render,
            descriptorSetLayout,
            swapchainElements.at(i)->worldCamera(),
            lightningVertices
        ));
    }
}
#endif
