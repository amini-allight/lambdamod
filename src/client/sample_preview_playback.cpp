/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_preview_playback.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"

SamplePreviewPlayback::SamplePreviewPlayback(ViewerModeContext* ctx, EntityID id)
    : ctx(ctx)
    , id(id)
{

}

SamplePreviewPlayback::~SamplePreviewPlayback()
{
    close();
}

bool SamplePreviewPlayback::playing() const
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return false;
    }

    auto it = entity->samplePlaybacks().find(playbackID);

    if (it == entity->samplePlaybacks().end())
    {
        return false;
    }

    return it->second.playing;
}

u32 SamplePreviewPlayback::position() const
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return 0;
    }

    auto it = entity->samplePlaybacks().find(playbackID);

    if (it == entity->samplePlaybacks().end())
    {
        return 0;
    }

    auto it2 = entity->samples().find(name);

    if (it2 == entity->samples().end())
    {
        return 0;
    }

    if (it2->second.length() == 0)
    {
        return 0;
    }

    return it->second.elapsed % it2->second.length();
}

void SamplePreviewPlayback::open(const string& name)
{
    close();

    this->name = name;

    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    ctx->context()->controller()->edit([this, name, entity]() -> void {
        playbackID = entity->playSamplePreview(name);
        entity->pauseSamplePlayback(playbackID);
    });
}

void SamplePreviewPlayback::close()
{
    if (playbackID == SamplePlaybackID())
    {
        return;
    }

    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    ctx->context()->controller()->edit([this, entity]() -> void {
        entity->stopSample(playbackID);
    });

    playbackID = SamplePlaybackID();
}

void SamplePreviewPlayback::play()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    ctx->context()->controller()->edit([this, entity]() -> void {
        entity->resumeSamplePlayback(playbackID);
    });
}

void SamplePreviewPlayback::pause()
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    ctx->context()->controller()->edit([this, entity]() -> void {
        entity->pauseSamplePlayback(playbackID);
    });
}

void SamplePreviewPlayback::seek(u32 position)
{
    Entity* entity = activeEntity();

    if (!entity)
    {
        return;
    }

    ctx->context()->controller()->edit([this, entity, position]() -> void {
        entity->seekSamplePlayback(playbackID, position);
    });
}

Entity* SamplePreviewPlayback::activeEntity() const
{
    return ctx->context()->controller()->game().get(id);
}
