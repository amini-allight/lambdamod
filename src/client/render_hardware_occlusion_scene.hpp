/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "render_types.hpp"
#include "render_hardware_occlusion_entity.hpp"
#include "render_occlusion_scene_reference.hpp"
#include "render_context.hpp"

class RenderFlatEntity;
#ifdef LMOD_VR
class RenderVREntity;
#endif

class RenderHardwareOcclusionScene
{
public:
    RenderHardwareOcclusionScene(const RenderContext* ctx);
    RenderHardwareOcclusionScene(const RenderHardwareOcclusionScene& rhs) = delete;
    RenderHardwareOcclusionScene(RenderHardwareOcclusionScene&& rhs) = delete;
    ~RenderHardwareOcclusionScene();

    RenderHardwareOcclusionScene& operator=(const RenderHardwareOcclusionScene& rhs) = delete;
    RenderHardwareOcclusionScene& operator=(RenderHardwareOcclusionScene&& rhs) = delete;

    void update(
        VkCommandBuffer commandBuffer,
        const vec3& cameraPosition,
        const map<EntityID, RenderFlatEntity*>& entities
    );
#ifdef LMOD_VR
    void update(
        VkCommandBuffer commandBuffer,
        vec3 cameraPosition[eyeCount],
        const map<EntityID, RenderVREntity*>& entities
    );
#endif

    RenderOcclusionSceneReference get() const;

private:
    friend class RenderHardwareOcclusionEntity;

    const RenderContext* ctx;
    PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
    PFN_vkDestroyAccelerationStructureKHR vkDestroyAccelerationStructureKHR;
    PFN_vkGetAccelerationStructureBuildSizesKHR vkGetAccelerationStructureBuildSizesKHR;
    PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR;
    PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
    PFN_vkGetBufferDeviceAddressKHR vkGetBufferDeviceAddressKHR;
    VkPhysicalDeviceAccelerationStructurePropertiesKHR accelerationStructureProperties;
    VkPhysicalDeviceProperties2 physDeviceProperties;

private:
    VmaBuffer instanceBuffer[eyeCount];
    VmaMapping<VkAccelerationStructureInstanceKHR>* instanceBufferMapping[eyeCount];
    VkDeviceAddress instanceBufferAddress[eyeCount];

    VmaBuffer backingBuffer[eyeCount];
    VmaBuffer scratchBuffer[eyeCount];
    VkDeviceAddress scratchBufferAddress[eyeCount];
    VkAccelerationStructureKHR accelerationStructure[eyeCount];
    bool initialized[eyeCount];
    u32 lastInstanceCount;

    map<EntityID, RenderHardwareOcclusionEntity*> entities;

    void build(
        VkCommandBuffer commandBuffer,
        const vector<VkAccelerationStructureInstanceKHR>& entities,
        u32 eye = 0
    );

    void recordInstances(
        VkCommandBuffer commandBuffer,
        const vec3& cameraPosition,
        const map<EntityID, RenderFlatEntity*>& entities,
        vector<VkAccelerationStructureInstanceKHR>& instances
    );
#ifdef LMOD_VR
    void recordInstances(
        VkCommandBuffer commandBuffer,
        vec3 cameraPosition[eyeCount],
        const map<EntityID, RenderVREntity*>& entities,
        u32 eye,
        vector<VkAccelerationStructureInstanceKHR>& instances
    );
#endif

    tuple<VmaBuffer, VkDeviceAddress> createInstanceBuffer();
    VmaBuffer createAccelerationStructureBuffer(const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo) const;
    tuple<VmaBuffer, VkDeviceAddress> createScratchBuffer(const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo) const;
    tuple<VmaBuffer, VkDeviceAddress> createBufferAndAddress(
        size_t size,
        VkBufferUsageFlags bufferUsage,
        VmaMemoryUsage memoryUsage,
        VkDeviceSize alignment = 0
    ) const;
};
