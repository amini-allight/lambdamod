/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface.hpp"
#include "control_context.hpp"
#include "render_tools.hpp"
#include "flat_render.hpp"
#include "vr_render.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_screen_widgets.hpp"
#include "entity.hpp"
#include "rotate_tool.hpp"
#include "scale_tool.hpp"
#include "interface_panel_view.hpp"
#include "interface_window_view.hpp"

#define START_VIEW(_name) \
{ \
    auto view = new InterfaceView(this, _name); \
    views.insert({ _name, view });

#define START_PANEL_VIEW(_name) \
{ \
    auto view = new InterfacePanelView(this, _name); \
    views.insert({ _name, view });

#define START_WINDOW_VIEW(_name) \
{ \
    auto view = new InterfaceWindowView(this, _name); \
    views.insert({ _name, view });

#define END_VIEW(_size) \
    view->resize(_size); \
}

Interface::Interface(ControlContext* ctx)
    : ctx(ctx)
    , lastStepTime(currentTime())
{
    width = g_config.width;
    height = g_config.height;

    interfaceDrawInit(ctx->controller());

    createCommandBuffers();

    Size screenSize;

#ifdef LMOD_VR
    if (g_vr)
    {
        screenSize = Size(hudWidth, hudHeight);
    }
    else
    {
        screenSize = Size(width, height);
    }
#else
    screenSize = Size(width, height);
#endif

    createViews(screenSize);
}

Interface::~Interface()
{
    destroyViews();

    destroyCommandBuffers();

    logOneshots.clear();
    oneshots.clear();
    markers.clear();
    hoveredEntity = {};
    ctx = nullptr;

    height = g_config.height;
    width = g_config.width;

    interfaceDrawQuit();
}

ControlContext* Interface::context() const
{
    return ctx;
}

void Interface::movePointer(const Point& pointer)
{
    this->pointer = pointer;

    // Entity hovering
    vector<Entity*> targets = ctx->viewerMode()->entityMode()->entitiesUnderPoint(pointer);

    if (!targets.empty())
    {
        hoveredEntity = targets.front()->id();

        for (Entity* target : targets)
        {
            if (ctx->viewerMode()->entityMode()->selection().contains(target))
            {
                hoveredEntity = target->id();
                break;
            }
        }
    }
    else
    {
        hoveredEntity = EntityID();
    }
}

void Interface::focus()
{
    // Do nothing
}

void Interface::unfocus()
{
    for (const auto& [ name, view ] : views)
    {
        view->unfocus();
    }
}

void Interface::resize(u32 width, u32 height)
{
    this->width = width;
    this->height = height;

    destroyCommandBuffers();
    createCommandBuffers();

    for (const auto& [ name, view ] : views)
    {
        view->resize(Size(width, height));
    }
}

void Interface::step()
{
    // Used to move the gamepad pointer per-frame and timeout log oneshots
    _stepInterval = (currentTime() - lastStepTime).count() / 1000.0;

    for (const auto& [ name, view ] : views)
    {
        view->step();
    }

#ifdef LMOD_VR
    if (!g_vr)
    {
        dynamic_cast<InterfaceWindowView*>(getView("windows"))->clean();
    }
#else
    dynamic_cast<InterfaceWindowView*>(getView("windows"))->clean();
#endif

    for (size_t i = 0; i < oneshots.size();)
    {
        InterfaceOneshot& oneshot = oneshots[i];

        if (oneshot.duration <= _stepInterval)
        {
            oneshots.erase(oneshots.begin() + i);
        }
        else
        {
            oneshot.duration -= _stepInterval;
            i++;
        }
    }

    for (size_t i = 0; i < logOneshots.size();)
    {
        InterfaceLogOneshot& logOneshot = logOneshots[i];

        if (logOneshot.duration <= _stepInterval)
        {
            logOneshots.erase(logOneshots.begin() + i);
        }
        else
        {
            logOneshot.duration -= _stepInterval;
            i++;
        }
    }

    interfaceDrawPreStep();

    u32 currentImage = ctx->controller()->render()->currentImage();

    VkResult result;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(transferCommandBuffers[currentImage], &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    drawTextTitlecard(textTitlecardCommandBuffers[currentImage], transferCommandBuffers[currentImage]);
    drawText(textCommandBuffers[currentImage], transferCommandBuffers[currentImage]);

#ifdef LMOD_VR
    if (g_vr)
    {
        drawHUD(
            hudCommandBuffers[currentImage],
            transferCommandBuffers[currentImage]
        );

        drawKeyboard(
            keyboardCommandBuffers[currentImage],
            transferCommandBuffers[currentImage]
        );
    }
    else
    {
        drawScreen(
            screenCommandBuffers[currentImage],
            transferCommandBuffers[currentImage]
        );
    }
#else
    drawScreen(
        screenCommandBuffers[currentImage],
        transferCommandBuffers[currentImage]
    );
#endif

    result = vkEndCommandBuffer(transferCommandBuffers[currentImage]);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }

    interfaceDrawPostStep();

    lastStepTime = currentTime();
}

InterfaceView* Interface::getView(const string& name) const
{
    return views.contains(name) ? views.at(name) : nullptr;
}

void Interface::setMarker(
    u32 id,
    Icon type,
    const string& name,
    const vec3& position,
    const vec3& color
)
{
    markers.insert_or_assign(
        id,
        InterfaceMarker(type, name, position, color)
    );
}

void Interface::clearMarker(u32 id)
{
    markers.erase(id);
}

void Interface::ping(const User* user)
{
    addOneshot(
        Icon_Ping,
        user->name(),
        user->viewer().position(),
        user->color(),
        chrono::duration_cast<chrono::milliseconds>(pingOneshotDuration).count() / 1000.0
    );
    ctx->controller()->sound()->addOneshot(Sound_Oneshot_Ping, user->viewer().position());
}

void Interface::brake(const User* user)
{
    addOneshot(
        Icon_Brake,
        user->name(),
        user->viewer().position(),
        user->color(),
        chrono::duration_cast<chrono::milliseconds>(brakeOneshotDuration).count() / 1000.0
    );
    ctx->controller()->sound()->addOneshot(Sound_Oneshot_Brake, user->viewer().position());
}

void Interface::log(const Message& message)
{
    Console* console = this->console();

    if (!console)
    {
        return;
    }

    console->log(message);

    if (!console->shown())
    {
        addLogOneshot(message);
    }

#if defined(LMOD_VR)
    if (!g_vr && (!console->shown() || !ctx->controller()->window()->focused()))
#else
    if (!console->shown() || !ctx->controller()->window()->focused())
#endif
    {
        switch (message.type)
        {
        case Message_Direct_Chat_In :
        case Message_Team_Chat_In :
        case Message_Chat :
            ctx->controller()->sound()->addOneshot(Sound_Oneshot_Chat);
            break;
        case Message_Direct_Chat_Out :
        case Message_Team_Chat_Out :
        case Message_Script :
        case Message_Log :
        case Message_Warning :
            break;
        case Message_Error :
            ctx->controller()->sound()->addOneshot(Sound_Oneshot_Error);
            break;
        }
    }
}

Console* Interface::console() const
{
    return getWindow<Console>();
}

Help* Interface::help() const
{
    return getWindow<Help>();
}

DocumentManager* Interface::documentManager() const
{
    return getWindow<DocumentManager>();
}

Settings* Interface::settings() const
{
    return getWindow<Settings>();
}

SystemSettings* Interface::systemSettings() const
{
    return getWindow<SystemSettings>();
}

NewTextTool* Interface::newTextTool() const
{
    return getWindow<NewTextTool>();
}

UserControlsTool* Interface::userControlsTool() const
{
    return getWindow<UserControlsTool>();
}

OracleManager* Interface::oracleManager() const
{
    return getWindow<OracleManager>();
}

QuickAccessor* Interface::quickAccessor() const
{
    return getWindow<QuickAccessor>();
}

EntityEditor* Interface::editor(EntityID id) const
{
    for (InterfaceWindow* window : windows())
    {
        if (auto editor = dynamic_cast<EntityEditor*>(window); editor && editor->id() == id)
        {
            return editor;
        }
    }

    return nullptr;
}

vector<VkCommandBuffer> Interface::panelBuffers() const
{
    u32 currentImage = ctx->controller()->render()->currentImage();

#ifdef LMOD_VR
    if (g_vr)
    {
        return {
            transferCommandBuffers[currentImage],
            textTitlecardCommandBuffers[currentImage],
            textCommandBuffers[currentImage],
            hudCommandBuffers[currentImage],
            keyboardCommandBuffers[currentImage]
        };
    }
    else
    {
        return {
            transferCommandBuffers[currentImage],
            textTitlecardCommandBuffers[currentImage],
            textCommandBuffers[currentImage]
        };
    }
#else
    return {
        transferCommandBuffers[currentImage],
        textTitlecardCommandBuffers[currentImage],
        textCommandBuffers[currentImage]
    };
#endif
}

VkCommandBuffer Interface::uiBuffer() const
{
    return screenCommandBuffers[ctx->controller()->render()->currentImage()];
}

f64 Interface::stepInterval() const
{
    return _stepInterval;
}

void Interface::updateWindowFocus()
{
    ctx->input()->getScope("windows")->movePointer(Pointer_Normal, pointer, false);
}

void Interface::addOneshot(
    Icon type,
    const string& name,
    const vec3& position,
    const vec3& color,
    f64 duration
)
{
    oneshots.push_back(InterfaceOneshot(type, name, position, color, duration));
}

void Interface::addLogOneshot(const Message& message)
{
    logOneshots.push_back(InterfaceLogOneshot(
        message,
        chrono::duration_cast<chrono::milliseconds>(logOneshotDuration).count() / 1000.0
    ));
}

void Interface::drawTextTitlecard(
    VkCommandBuffer commandBuffer,
    VkCommandBuffer transferCommandBuffer
)
{
    VkResult result;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    VkClearValue clearValues[2]{};
    clearValues[0] = colorToClearValue(pureAlpha.toVec4());
    clearValues[1] = colorToClearValue(pureAlpha.toVec4());

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = ctx->controller()->render()->panelRenderPass();
    beginRenderPassInfo.framebuffer = ctx->controller()->render()->textTitlecardFramebuffer();
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { textTitlecardWidth, textTitlecardHeight }
    };
    beginRenderPassInfo.clearValueCount = 2;
    beginRenderPassInfo.pClearValues = clearValues;

    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    DrawContext drawCtx = {
        commandBuffer,
        transferCommandBuffer,
        { textTitlecardWidth, textTitlecardHeight },
        false
    };

    getView("text-titlecard")->draw(drawCtx);

    vkCmdEndRenderPass(commandBuffer);

    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }
}

void Interface::drawText(
    VkCommandBuffer commandBuffer,
    VkCommandBuffer transferCommandBuffer
)
{
    VkResult result;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    VkClearValue clearValues[2]{};
    clearValues[0] = colorToClearValue(pureAlpha.toVec4());
    clearValues[1] = colorToClearValue(pureAlpha.toVec4());

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = ctx->controller()->render()->panelRenderPass();
    beginRenderPassInfo.framebuffer = ctx->controller()->render()->textFramebuffer();
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { textWidth, textHeight }
    };
    beginRenderPassInfo.clearValueCount = 2;
    beginRenderPassInfo.pClearValues = clearValues;

    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    DrawContext drawCtx = {
        commandBuffer,
        transferCommandBuffer,
        { textWidth, textHeight },
        false
    };

    getView("text")->draw(drawCtx);

    for (const function<void(const DrawContext&)>& delayedCommand : drawCtx.delayedCommands)
    {
        delayedCommand(drawCtx);
    }

    drawCtx.delayedCommands.clear();

    vkCmdEndRenderPass(commandBuffer);

    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }
}

#ifdef LMOD_VR
void Interface::drawHUD(
    VkCommandBuffer commandBuffer,
    VkCommandBuffer transferCommandBuffer
)
{
    VkResult result;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    VkClearValue clearValues[2]{};
    clearValues[0] = colorToClearValue(pureAlpha.toVec4());
    clearValues[1] = colorToClearValue(pureAlpha.toVec4());

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = ctx->controller()->render()->panelRenderPass();
    beginRenderPassInfo.framebuffer = dynamic_cast<VRRender*>(ctx->controller()->render())->hudFramebuffer();
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { hudWidth, hudHeight }
    };
    beginRenderPassInfo.clearValueCount = 2;
    beginRenderPassInfo.pClearValues = clearValues;

    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    i32 width = hudWidth;
    i32 height = g_vr ? hudHeight : static_cast<i32>(hudWidth * (this->height / static_cast<f64>(this->width)));

    DrawContext drawCtx = {
        commandBuffer,
        transferCommandBuffer,
        { width, height },
        false
    };

    getView("under-screen")->draw(drawCtx);
    getView("over-screen")->draw(drawCtx);

    vkCmdEndRenderPass(commandBuffer);

    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }
}

void Interface::drawKeyboard(
    VkCommandBuffer commandBuffer,
    VkCommandBuffer transferCommandBuffer
)
{
    VkResult result;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    VkClearValue clearValues[2]{};
    clearValues[0] = colorToClearValue(pureAlpha.toVec4());
    clearValues[1] = colorToClearValue(pureAlpha.toVec4());

    VkRenderPassBeginInfo beginRenderPassInfo{};
    beginRenderPassInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginRenderPassInfo.renderPass = ctx->controller()->render()->panelRenderPass();
    beginRenderPassInfo.framebuffer = dynamic_cast<VRRender*>(ctx->controller()->render())->keyboardFramebuffer();
    beginRenderPassInfo.renderArea = {
        { 0, 0 },
        { vrKeyboardWidth, vrKeyboardHeight }
    };
    beginRenderPassInfo.clearValueCount = 2;
    beginRenderPassInfo.pClearValues = clearValues;

    vkCmdBeginRenderPass(commandBuffer, &beginRenderPassInfo, VK_SUBPASS_CONTENTS_INLINE);

    DrawContext drawCtx = {
        commandBuffer,
        transferCommandBuffer,
        { vrKeyboardWidth, vrKeyboardHeight },
        false
    };

    getView("vr-keyboard")->draw(drawCtx);

    vkCmdEndRenderPass(commandBuffer);

    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }
}
#endif

void Interface::drawScreen(
    VkCommandBuffer commandBuffer,
    VkCommandBuffer transferCommandBuffer
)
{
    VkResult result;

    VkCommandBufferInheritanceInfo inheritanceInfo{};
    inheritanceInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_INHERITANCE_INFO;
    inheritanceInfo.renderPass = dynamic_cast<FlatRender*>(ctx->controller()->render())->uiRenderPass();
    inheritanceInfo.subpass = 0;
    inheritanceInfo.framebuffer = nullptr;
    inheritanceInfo.occlusionQueryEnable = VK_FALSE;
    inheritanceInfo.queryFlags = 0;
    inheritanceInfo.pipelineStatistics = 0;

    VkCommandBufferBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    beginInfo.flags = VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT | VK_COMMAND_BUFFER_USAGE_RENDER_PASS_CONTINUE_BIT;
    beginInfo.pInheritanceInfo = &inheritanceInfo;

    result = vkBeginCommandBuffer(commandBuffer, &beginInfo);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to begin command buffer: " + vulkanError(result));
    }

    DrawContext drawCtx = {
        commandBuffer,
        transferCommandBuffer,
        { width, height },
        true
    };

    // Screen widgets
    if (ctx->controller()->ready())
    {
        drawScreenWidgets(drawCtx);
    }

    // Windows
    getView("under-screen")->draw(drawCtx);
    getView("windows")->draw(drawCtx);
    getView("over-screen")->draw(drawCtx);

    result = vkEndCommandBuffer(commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to end command buffer: " + vulkanError(result));
    }
}

struct EntityScreenWidgetDraw
{
    EntityScreenWidgetDraw()
        : ctx(nullptr)
        , entity(nullptr)
        , behavior(nullptr)
        , hovered(false)
    {

    }

    EntityScreenWidgetDraw(const ControlContext* ctx, const Entity* entity, const function<void()>& behavior, bool hovered)
        : ctx(ctx)
        , entity(entity)
        , behavior(behavior)
        , hovered(hovered)
    {

    }

    u64 score() const
    {
        // hovered
        if (hovered)
        {
            return 3;
        }

        // selected by self
        if (entity->selectingUserID() == ctx->controller()->self()->id())
        {
            return 2;
        }

        // selected by other
        if (entity->selectingUserID() != UserID())
        {
            return 1;
        }

        // unselected
        return 0;
    }

    const ControlContext* ctx;
    const Entity* entity;
    function<void()> behavior;
    bool hovered;
};

void Interface::drawScreenWidgets(const DrawContext& drawCtx)
{
    if (auto tool = dynamic_cast<const RotateTool*>(ctx->viewerMode()->editMode()->tool()))
    {
        drawDashedLine(
            drawCtx,
            nullptr,
            tool->pivotPoint(),
            tool->cursorPoint(),
            theme.panelForegroundColor
        );
    }
    else if (auto tool = dynamic_cast<const ScaleTool*>(ctx->viewerMode()->editMode()->tool()))
    {
        drawDashedLine(
            drawCtx,
            nullptr,
            tool->pivotPoint(),
            tool->cursorPoint(),
            theme.panelForegroundColor
        );
    }

    if (!ctx->attached() && ctx->viewerMode()->entityPointsShown())
    {
        switch (ctx->viewerMode()->mode())
        {
        case Viewer_Sub_Mode_Entity :
        {
            ctx->controller()->selfWorld()->traverse([&](const Entity* entity) -> void {
                drawEntityLinks(drawCtx, ctx, entity);
            });

            vector<EntityScreenWidgetDraw> draws;

            ctx->controller()->selfWorld()->traverse([&](const Entity* entity) -> void {
                bool hovered = hoveredEntity && entity->id() == *hoveredEntity;

                draws.push_back(EntityScreenWidgetDraw(ctx, entity, [&, entity, hovered]() -> void {
                    drawEntity(drawCtx, ctx, entity, hovered);
                }, hovered));
            });

            sort(
                draws.begin(),
                draws.end(),
                [&](const EntityScreenWidgetDraw& a, const EntityScreenWidgetDraw& b) -> bool {
                    return a.score() < b.score();
                }
            );

            for (const EntityScreenWidgetDraw& draw: draws)
            {
                draw.behavior();
            }
            break;
        }
        case Viewer_Sub_Mode_Body :
        case Viewer_Sub_Mode_Action :
        {
            const Entity* entity = ctx->viewerMode()->bodyMode()->activeEntity();

            if (entity)
            {
                drawBody(drawCtx, ctx, entity);
            }
            break;
        }
        }
    }

    if (ctx->viewerMode()->editMode()->selecting())
    {
        drawBoxSelect(
            drawCtx,
            ctx,
            ctx->viewerMode()->editMode()->selectStart(),
            ctx->viewerMode()->editMode()->selectEnd()
        );
    }

    if (ctx->hudShown() && g_config.frameCounter)
    {
        drawFrameCounter(drawCtx, ctx);
    }

    if (ctx->hudShown() && g_config.pingCounter)
    {
        drawPingCounter(drawCtx, ctx, ctx->controller()->roundTripTime());
    }

    if (ctx->hudShown() && !ctx->attached())
    {
        drawStatusLine(drawCtx, ctx);
    }

    for (const InterfaceOneshot& oneshot : oneshots)
    {
        drawOneshot(drawCtx, ctx, oneshot);
    }

    for (const auto& [ id, marker ] : markers)
    {
        drawMarker(drawCtx, ctx, marker);
    }

    drawLogOneshots(drawCtx, ctx, logOneshots);
}

void Interface::createCommandBuffers()
{
    u32 imageCount = ctx->controller()->render()->imageCount();

    const RenderContext* ctx = this->ctx->controller()->render()->context();

    VkResult result;

    VkCommandPoolCreateInfo commandPoolCreateInfo{};
    commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT | VK_COMMAND_POOL_CREATE_TRANSIENT_BIT;
    commandPoolCreateInfo.queueFamilyIndex = ctx->graphicsQueueIndex;

    result = vkCreateCommandPool(ctx->device, &commandPoolCreateInfo, nullptr, &commandPool);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface command pool: " + vulkanError(result));
    }

    VkCommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.commandPool = commandPool;
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = imageCount;

    transferCommandBuffers = vector<VkCommandBuffer>(imageCount);

    result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, transferCommandBuffers.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface command buffers: " + vulkanError(result));
    }

    textTitlecardCommandBuffers = vector<VkCommandBuffer>(imageCount);

    result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, textTitlecardCommandBuffers.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface command buffers: " + vulkanError(result));
    }

    textCommandBuffers = vector<VkCommandBuffer>(imageCount);

    result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, textCommandBuffers.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface command buffers: " + vulkanError(result));
    }

#ifdef LMOD_VR
    if (g_vr)
    {
        hudCommandBuffers = vector<VkCommandBuffer>(imageCount);

        result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, hudCommandBuffers.data());

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create interface command buffers: " + vulkanError(result));
        }

        keyboardCommandBuffers = vector<VkCommandBuffer>(imageCount);

        result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, keyboardCommandBuffers.data());

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create interface command buffers: " + vulkanError(result));
        }
    }
#endif

    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_SECONDARY;

    screenCommandBuffers = vector<VkCommandBuffer>(imageCount);

    result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, screenCommandBuffers.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface command buffers: " + vulkanError(result));
    }
}

void Interface::destroyCommandBuffers()
{
    VkDevice device = ctx->controller()->render()->context()->device;

    vkFreeCommandBuffers(device, commandPool, screenCommandBuffers.size(), screenCommandBuffers.data());
    screenCommandBuffers.clear();
#ifdef LMOD_VR
    if (g_vr)
    {
        vkFreeCommandBuffers(device, commandPool, keyboardCommandBuffers.size(), keyboardCommandBuffers.data());
        keyboardCommandBuffers.clear();
        vkFreeCommandBuffers(device, commandPool, hudCommandBuffers.size(), hudCommandBuffers.data());
        hudCommandBuffers.clear();
    }
#endif
    vkFreeCommandBuffers(device, commandPool, textCommandBuffers.size(), textCommandBuffers.data());
    textCommandBuffers.clear();
    vkFreeCommandBuffers(device, commandPool, textTitlecardCommandBuffers.size(), textTitlecardCommandBuffers.data());
    textTitlecardCommandBuffers.clear();
    vkFreeCommandBuffers(device, commandPool, transferCommandBuffers.size(), transferCommandBuffers.data());
    transferCommandBuffers.clear();

    vkDestroyCommandPool(device, commandPool, nullptr);
}

void Interface::createViews(const Size& screenSize)
{
    START_PANEL_VIEW("text-titlecard")
        new TextTitlecard(view);
    END_VIEW(Size(textTitlecardWidth, textTitlecardHeight))

    START_PANEL_VIEW("text")
        new TextPanel(view);
    END_VIEW(Size(textWidth, textHeight))

    START_VIEW("under-screen")
        new SystemHUD(view);

        new HUD(
            view,
            [this]() -> const map<string, HUDElement>&
            {
                return ctx->attachedMode()->hudElements();
            }
        );

        new SaveIndicator(view);

#ifdef LMOD_VR
        if (!g_vr)
        {
            new VirtualTouchGamepad(view);
        }
#else
        new VirtualTouchGamepad(view);
#endif

        new Splash(view);

        new GamepadAttachedMenu(view);
        new GamepadViewerMenu(view);

#ifdef LMOD_VR
        if (g_vr)
        {
            new VRAttachedMenu(view);
            new VRViewerMenu(view);
        }
#endif

        new KnowledgeViewer(view);
    END_VIEW(screenSize)

#ifdef LMOD_VR
    if (!g_vr)
    {
        START_WINDOW_VIEW("windows")
            new Console(view);
            new Help(view);
            new DocumentManager(view);
            new Settings(view);
            new SystemSettings(view);
            new NewTextTool(view);
            new UserControlsTool(view);
            new OracleManager(view);
            new QuickAccessor(view);
        END_VIEW(screenSize)
    }
#else
    START_WINDOW_VIEW("windows")
        new Console(view);
        new Help(view);
        new DocumentManager(view);
        new Settings(view);
        new SystemSettings(view);
        new NewTextTool(view);
        new UserControlsTool(view);
        new OracleManager(view);
        new QuickAccessor(view);
    END_VIEW(screenSize)
#endif

    START_VIEW("over-screen")
        new ConnectingMessage(view);
        new DisconnectedMessage(view);
        new LoadingMessage(view);

        new MainMenu(view);

        new StartupSplash(view);
    END_VIEW(screenSize)

#ifdef LMOD_VR
    if (g_vr)
    {
        START_PANEL_VIEW("vr-keyboard")
            new VRKeyboard(view);
        END_VIEW(Size(vrKeyboardWidth, vrKeyboardHeight))
    }
#endif
}

void Interface::destroyViews()
{
    for (const auto& [ name, view ] : views)
    {
        delete view;
    }
    views.clear();
}

vector<InterfaceWindow*> Interface::windows() const
{
    vector<InterfaceWindow*> windows;
    windows.reserve(getView("windows")->children().size());

    for (InterfaceWidget* widget : getView("windows")->children())
    {
        auto window = dynamic_cast<InterfaceWindow*>(widget);

        if (!window)
        {
            continue;
        }

        windows.push_back(window);
    }

    return windows;
}
