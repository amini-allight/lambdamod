/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_axis.hpp"
#include "render_tools.hpp"
#include "log.hpp"
#include "tools.hpp"

static constexpr f64 axisIndicatorLength = 1024;

RenderAxis::RenderAxis(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const vec3& direction,
    const Color& color
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, quaternion(direction, pickUpDirection(direction)), vec3(axisIndicatorLength) };

    vertices = createVertexBuffer(2 * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData = {
        { fvec3(0, -0.5f, 0), color.toVec3() },
        { fvec3(0, +0.5f, 0), color.toVec3() }
    };

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderAxis::~RenderAxis()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderAxis::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        2
    );
}

VmaBuffer RenderAxis::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
