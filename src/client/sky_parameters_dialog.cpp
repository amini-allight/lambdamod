/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sky_parameters_dialog.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"
#include "uint_edit.hpp"
#include "option_select.hpp"
#include "percent_edit.hpp"
#include "float_edit.hpp"
#include "ufloat_edit.hpp"
#include "checkbox.hpp"
#include "hdr_color_picker.hpp"
#include "spacer.hpp"
#include "text_button.hpp"
#include "vec2_edit.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(384, 512) * uiScale();
}

SkyParametersDialog::SkyParametersDialog(
    InterfaceView* view,
    const function<SkyParameters()>& parametersSource,
    const function<void(const SkyParameters&)>& onParameters
)
    : Dialog(view, dialogSize())
    , parametersSource(parametersSource)
    , onParameters(onParameters)
    , listView(new ListView(this))
{
    updateWidgets();
}

void SkyParametersDialog::step()
{
    if (needsWidgetUpdate())
    {
        updateWidgets();
    }

    InterfaceWidget::step();
}

bool SkyParametersDialog::needsWidgetUpdate() const
{
    if (!lastSkyParameters)
    {
        return true;
    }

    SkyParameters sky = parametersSource();

    if (lastSkyParameters->base.type != sky.base.type)
    {
        return true;
    }

    if (lastSkyParameters->layers.size() != sky.layers.size())
    {
        return true;
    }

    for (size_t i = 0; i < sky.layers.size(); i++)
    {
        if (lastSkyParameters->layers.at(i).type != sky.layers.at(i).type)
        {
            return true;
        }
    }

    return false;
}

void SkyParametersDialog::updateWidgets()
{
    listView->clearChildren();

    addSkyChildren();

    listView->update();

    lastSkyParameters = parametersSource();
}

void SkyParametersDialog::addSkyChildren()
{
    const SkyParameters& sky = parametersSource();

    new EditorEntry(listView, "sky-parameters-dialog-enabled", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&SkyParametersDialog::skyEnabledSource, this),
            bind(&SkyParametersDialog::onSkyEnabled, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-sky-seed", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&SkyParametersDialog::skySeedSource, this),
            bind(&SkyParametersDialog::onSkySeed, this, placeholders::_1)
        );
    });

    addSkyBaseChildren(sky.base);

    for (size_t i = 0; i < sky.layers.size(); i++)
    {
        const SkyLayer& layer = sky.layers.at(i);

        addSkyLayerChildren(i, layer);
    }

    addSkyLayerNewPrompt();
}

void SkyParametersDialog::addSkyBaseChildren(const SkyBase& base)
{
    new EditorEntry(listView, "sky-parameters-dialog-sky-base-type", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&SkyParametersDialog::skyBaseTypeOptionsSource, this),
            bind(&SkyParametersDialog::skyBaseTypeOptionTooltipsSource, this),
            bind(&SkyParametersDialog::skyBaseTypeSource, this),
            bind(&SkyParametersDialog::onSkyBaseType, this, placeholders::_1)
        );
    });

    switch (base.type)
    {
    case Sky_Base_Blank :
        addSkyBlankChildren(base.get<SkyBaseBlank>());
        break;
    case Sky_Base_Unidirectional :
        addSkyUnidirectionalChildren(base.get<SkyBaseUnidirectional>());
        break;
    case Sky_Base_Bidirectional :
        addSkyBidirectionalChildren(base.get<SkyBaseBidirectional>());
        break;
    case Sky_Base_Omnidirectional :
        addSkyOmnidirectionalChildren(base.get<SkyBaseOmnidirectional>());
        break;
    }
}

void SkyParametersDialog::addSkyBlankChildren(const SkyBaseBlank& base)
{
    // Do nothing
}

void SkyParametersDialog::addSkyUnidirectionalChildren(const SkyBaseUnidirectional& base)
{
    new EditorEntry(listView, "sky-parameters-dialog-horizon-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyBaseUnidirectionalHorizonColorSource, this),
            bind(&SkyParametersDialog::onSkyBaseUnidirectionalHorizonColor, this, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-zenith-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyBaseUnidirectionalZenithColorSource, this),
            bind(&SkyParametersDialog::onSkyBaseUnidirectionalZenithColor, this, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyBidirectionalChildren(const SkyBaseBidirectional& base)
{
    new EditorEntry(listView, "sky-parameters-dialog-horizon-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyBaseBidirectionalHorizonColorSource, this),
            bind(&SkyParametersDialog::onSkyBaseBidirectionalHorizonColor, this, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-lower-zenith-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyBaseBidirectionalLowerZenithColorSource, this),
            bind(&SkyParametersDialog::onSkyBaseBidirectionalLowerZenithColor, this, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-upper-zenith-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyBaseBidirectionalUpperZenithColorSource, this),
            bind(&SkyParametersDialog::onSkyBaseBidirectionalUpperZenithColor, this, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyOmnidirectionalChildren(const SkyBaseOmnidirectional& base)
{
    new EditorEntry(listView, "sky-parameters-dialog-omnidirectional-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyBaseOmnidirectionalColorSource, this),
            bind(&SkyParametersDialog::onSkyBaseOmnidirectionalColor, this, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerChildren(size_t index, const SkyLayer& layer)
{
    MAKE_SPACER(listView, 0, UIScale::marginSize() * 8);

    new EditorEntry(listView, "sky-parameters-dialog-sky-layer-type", [this, index](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&SkyParametersDialog::skyLayerTypeOptionsSource, this),
            bind(&SkyParametersDialog::skyLayerTypeOptionTooltipsSource, this),
            bind(&SkyParametersDialog::skyLayerTypeSource, this, index),
            bind(&SkyParametersDialog::onSkyLayerType, this, index, placeholders::_1)
        );
    });

    switch (layer.type)
    {
    case Sky_Layer_Stars :
        addSkyLayerStarsChildren(index, layer.get<SkyLayerStars>());
        break;
    case Sky_Layer_Circle :
        addSkyLayerCircleChildren(index, layer.get<SkyLayerCircle>());
        break;
    case Sky_Layer_Cloud :
        addSkyLayerCloudChildren(index, layer.get<SkyLayerCloud>());
        break;
    case Sky_Layer_Aurora :
        addSkyLayerAuroraChildren(index, layer.get<SkyLayerAurora>());
        break;
    case Sky_Layer_Comet :
        addSkyLayerCometChildren(index, layer.get<SkyLayerComet>());
        break;
    case Sky_Layer_Vortex :
        addSkyLayerVortexChildren(index, layer.get<SkyLayerVortex>());
        break;
    case Sky_Layer_Meteor_Shower :
        addSkyLayerMeteorShowerChildren(index, layer.get<SkyLayerMeteorShower>());
        break;
    }

    new EditorMultiEntry(
        listView,
        3,
        [this, index](InterfaceWidget* parent, size_t entryIndex) -> void
        {
            switch (entryIndex)
            {
            case 0 :
                new TextButton(
                    parent,
                    localize("sky-parameters-dialog-move-sky-layer-down"),
                    bind(&SkyParametersDialog::moveSkyLayerDown, this, index)
                );
                break;
            case 1 :
                new TextButton(
                    parent,
                    localize("sky-parameters-dialog-move-sky-layer-up"),
                    bind(&SkyParametersDialog::moveSkyLayerUp, this, index)
                );
                break;
            case 2 :
                new TextButton(
                    parent,
                    localize("sky-parameters-dialog-remove-sky-layer"),
                    bind(&SkyParametersDialog::removeSkyLayer, this, index)
                );
                break;
            }
        }
    );
}

void SkyParametersDialog::addSkyLayerStarsChildren(size_t index, const SkyLayerStars& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-stars-density", percentSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&SkyParametersDialog::skyStarsDensitySource, this, index),
            bind(&SkyParametersDialog::onSkyStarsDensity, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-stars-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyStarsColorSource, this, index),
            bind(&SkyParametersDialog::onSkyStarsColor, this, index, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerCircleChildren(size_t index, const SkyLayerCircle& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-position", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&SkyParametersDialog::skyCirclePositionSource, this, index),
            bind(&SkyParametersDialog::onSkyCirclePosition, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-size", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SkyParametersDialog::skyCircleSizeSource, this, index),
            bind(&SkyParametersDialog::onSkyCircleSize, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-circle-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyCircleColorSource, this, index),
            bind(&SkyParametersDialog::onSkyCircleColor, this, index, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-circle-phase", percentSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&SkyParametersDialog::skyCirclePhaseSource, this, index),
            bind(&SkyParametersDialog::onSkyCirclePhase, this, index, placeholders::_1)
        );
    });
}

void SkyParametersDialog::addSkyLayerCloudChildren(size_t index, const SkyLayerCloud& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-cloud-density", percentSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&SkyParametersDialog::skyCloudDensitySource, this, index),
            bind(&SkyParametersDialog::onSkyCloudDensity, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-position", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&SkyParametersDialog::skyCloudPositionSource, this, index),
            bind(&SkyParametersDialog::onSkyCloudPosition, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-size", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&SkyParametersDialog::skyCloudSizeSource, this, index),
            bind(&SkyParametersDialog::onSkyCloudSize, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-inner-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyCloudInnerColorSource, this, index),
            bind(&SkyParametersDialog::onSkyCloudInnerColor, this, index, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-outer-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyCloudOuterColorSource, this, index),
            bind(&SkyParametersDialog::onSkyCloudOuterColor, this, index, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerAuroraChildren(size_t index, const SkyLayerAurora& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-aurora-density", percentSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&SkyParametersDialog::skyAuroraDensitySource, this, index),
            bind(&SkyParametersDialog::onSkyAuroraDensity, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-lower-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyAuroraLowerColorSource, this, index),
            bind(&SkyParametersDialog::onSkyAuroraLowerColor, this, index, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-upper-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyAuroraUpperColorSource, this, index),
            bind(&SkyParametersDialog::onSkyAuroraUpperColor, this, index, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerCometChildren(size_t index, const SkyLayerComet& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-position", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&SkyParametersDialog::skyCometPositionSource, this, index),
            bind(&SkyParametersDialog::onSkyCometPosition, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-size", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SkyParametersDialog::skyCometSizeSource, this, index),
            bind(&SkyParametersDialog::onSkyCometSize, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-length", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SkyParametersDialog::skyCometLengthSource, this, index),
            bind(&SkyParametersDialog::onSkyCometLength, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-rotation", angleSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&SkyParametersDialog::skyCometRotationSource, this, index),
            bind(&SkyParametersDialog::onSkyCometRotation, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-head-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyCometHeadColorSource, this, index),
            bind(&SkyParametersDialog::onSkyCometHeadColor, this, index, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-tail-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyCometTailColorSource, this, index),
            bind(&SkyParametersDialog::onSkyCometTailColor, this, index, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerVortexChildren(size_t index, const SkyLayerVortex& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-vortex-position", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new Vec2Edit(
            parent,
            bind(&SkyParametersDialog::skyVortexPositionSource, this, index),
            bind(&SkyParametersDialog::onSkyVortexPosition, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-vortex-rotation-speed", rotationalSpeedSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&SkyParametersDialog::skyVortexRotationSpeedSource, this, index),
            bind(&SkyParametersDialog::onSkyVortexRotationSpeed, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-vortex-radius", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&SkyParametersDialog::skyVortexRadiusSource, this, index),
            bind(&SkyParametersDialog::onSkyVortexRadius, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-vortex-inner-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyVortexInnerColorSource, this, index),
            bind(&SkyParametersDialog::onSkyVortexInnerColor, this, index, placeholders::_1),
            true
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-vortex-outer-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyVortexOuterColorSource, this, index),
            bind(&SkyParametersDialog::onSkyVortexOuterColor, this, index, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerMeteorShowerChildren(size_t index, const SkyLayerMeteorShower& layer)
{
    new EditorEntry(listView, "sky-parameters-dialog-meteor-shower-density", percentSuffix(), [this, index](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&SkyParametersDialog::skyMeteorShowerDensitySource, this, index),
            bind(&SkyParametersDialog::onSkyMeteorShowerDensity, this, index, placeholders::_1)
        );
    });

    new EditorEntry(listView, "sky-parameters-dialog-meteor-shower-color", [this, index](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&SkyParametersDialog::skyMeteorShowerColorSource, this, index),
            bind(&SkyParametersDialog::onSkyMeteorShowerColor, this, index, placeholders::_1),
            true
        );
    });
}

void SkyParametersDialog::addSkyLayerNewPrompt()
{
    new EditorMultiEntry(
        listView,
        1,
        [this](InterfaceWidget* parent, size_t index) -> void
        {
            new TextButton(
                parent,
                localize("sky-parameters-dialog-add-sky-layer"),
                bind(&SkyParametersDialog::addSkyLayer, this)
            );
        }
    );
}

void SkyParametersDialog::addSkyLayer()
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.push_back(SkyLayer());

        onParameters(sky);
    });
}

void SkyParametersDialog::moveSkyLayerDown(size_t index)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        if (index + 1 != sky.layers.size())
        {
            SkyLayer layer = sky.layers.at(index);
            sky.layers.erase(sky.layers.begin() + index);
            // Normally we'd add 2 to move past the following element, but we just removed the element which means we can just add 1
            sky.layers.insert(sky.layers.begin() + (index + 1), layer);
        }

        onParameters(sky);
    });
}

void SkyParametersDialog::moveSkyLayerUp(size_t index)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        if (index != 0)
        {
            SkyLayer layer = sky.layers.at(index);
            sky.layers.erase(sky.layers.begin() + index);
            sky.layers.insert(sky.layers.begin() + (index - 1), layer);
        }

        onParameters(sky);
    });
}

void SkyParametersDialog::removeSkyLayer(size_t index)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.erase(sky.layers.begin() + index);

        onParameters(sky);
    });
}

bool SkyParametersDialog::skyEnabledSource()
{
    return parametersSource().enabled;
}

void SkyParametersDialog::onSkyEnabled(bool state)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.enabled = state;

        onParameters(sky);
    });
}

u64 SkyParametersDialog::skySeedSource()
{
    return parametersSource().seed;
}

void SkyParametersDialog::onSkySeed(u64 seed)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.seed = seed;

        onParameters(sky);
    });
}

vector<string> SkyParametersDialog::skyBaseTypeOptionsSource()
{
    // NOTE: Update this when adding new sky base types
    return {
        localize("sky-parameters-dialog-blank"),
        localize("sky-parameters-dialog-unidirectional"),
        localize("sky-parameters-dialog-bidirectional"),
        localize("sky-parameters-dialog-omnidirectional")
    };
}

vector<string> SkyParametersDialog::skyBaseTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new sky base types
    return {
        localize("sky-parameters-dialog-blank-tooltip"),
        localize("sky-parameters-dialog-unidirectional-tooltip"),
        localize("sky-parameters-dialog-bidirectional-tooltip"),
        localize("sky-parameters-dialog-omnidirectional-tooltip")
    };
}

size_t SkyParametersDialog::skyBaseTypeSource()
{
    return static_cast<size_t>(parametersSource().base.type);
}

void SkyParametersDialog::onSkyBaseType(size_t index)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.type = static_cast<SkyBaseType>(index);

        switch (sky.base.type)
        {
        case Sky_Base_Blank :
            sky.base.data = SkyBaseBlank();
            break;
        case Sky_Base_Unidirectional :
            sky.base.data = SkyBaseUnidirectional();
            break;
        case Sky_Base_Bidirectional :
            sky.base.data = SkyBaseBidirectional();
            break;
        case Sky_Base_Omnidirectional :
            sky.base.data = SkyBaseOmnidirectional();
            break;
        }

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyBaseUnidirectionalHorizonColorSource()
{
    return parametersSource().base.get<SkyBaseUnidirectional>().horizonColor;
}

void SkyParametersDialog::onSkyBaseUnidirectionalHorizonColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.get<SkyBaseUnidirectional>().horizonColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyBaseUnidirectionalZenithColorSource()
{
    return parametersSource().base.get<SkyBaseUnidirectional>().zenithColor;
}

void SkyParametersDialog::onSkyBaseUnidirectionalZenithColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.get<SkyBaseUnidirectional>().zenithColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyBaseBidirectionalHorizonColorSource()
{
    return parametersSource().base.get<SkyBaseBidirectional>().horizonColor;
}

void SkyParametersDialog::onSkyBaseBidirectionalHorizonColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.get<SkyBaseBidirectional>().horizonColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyBaseBidirectionalLowerZenithColorSource()
{
    return parametersSource().base.get<SkyBaseBidirectional>().lowerZenithColor;
}

void SkyParametersDialog::onSkyBaseBidirectionalLowerZenithColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.get<SkyBaseBidirectional>().lowerZenithColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyBaseBidirectionalUpperZenithColorSource()
{
    return parametersSource().base.get<SkyBaseBidirectional>().upperZenithColor;
}

void SkyParametersDialog::onSkyBaseBidirectionalUpperZenithColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.get<SkyBaseBidirectional>().upperZenithColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyBaseOmnidirectionalColorSource()
{
    return parametersSource().base.get<SkyBaseOmnidirectional>().color;
}

void SkyParametersDialog::onSkyBaseOmnidirectionalColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.base.get<SkyBaseOmnidirectional>().color = color;

        onParameters(sky);
    });
}

vector<string> SkyParametersDialog::skyLayerTypeOptionsSource()
{
    // NOTE: Update this when adding new sky layer types
    return {
        localize("sky-parameters-dialog-stars"),
        localize("sky-parameters-dialog-circle"),
        localize("sky-parameters-dialog-cloud"),
        localize("sky-parameters-dialog-aurora"),
        localize("sky-parameters-dialog-comet"),
        localize("sky-parameters-dialog-vortex"),
        localize("sky-parameters-dialog-meteor-shower")
    };
}

vector<string> SkyParametersDialog::skyLayerTypeOptionTooltipsSource()
{
    // NOTE: Update this when adding new sky layer types
    return {
        localize("sky-parameters-dialog-stars-tooltip"),
        localize("sky-parameters-dialog-circle-tooltip"),
        localize("sky-parameters-dialog-cloud-tooltip"),
        localize("sky-parameters-dialog-aurora-tooltip"),
        localize("sky-parameters-dialog-comet-tooltip"),
        localize("sky-parameters-dialog-vortex-tooltip"),
        localize("sky-parameters-dialog-meteor-shower-tooltip")
    };
}

size_t SkyParametersDialog::skyLayerTypeSource(size_t index)
{
    return static_cast<size_t>(parametersSource().layers.at(index).type);
}

void SkyParametersDialog::onSkyLayerType(size_t index, size_t typeIndex)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).setType(static_cast<SkyLayerType>(typeIndex));

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyStarsDensitySource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerStars>().density;
}

void SkyParametersDialog::onSkyStarsDensity(size_t index, f64 density)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerStars>().density = density;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyStarsColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerStars>().color;
}

void SkyParametersDialog::onSkyStarsColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerStars>().color = color;

        onParameters(sky);
    });
}

vec2 SkyParametersDialog::skyCirclePositionSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerCircle>().position);
}

void SkyParametersDialog::onSkyCirclePosition(size_t index, const vec2& position)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCircle>().position = delocalizeAngle(position);

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyCircleSizeSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerCircle>().size);
}

void SkyParametersDialog::onSkyCircleSize(size_t index, const f64& size)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCircle>().size = delocalizeAngle(size);

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyCircleColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerCircle>().color;
}

void SkyParametersDialog::onSkyCircleColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCircle>().color = color;

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyCirclePhaseSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerCircle>().phase;
}

void SkyParametersDialog::onSkyCirclePhase(size_t index, f64 phase)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCircle>().phase = phase;

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyCloudDensitySource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerCloud>().density;
}

void SkyParametersDialog::onSkyCloudDensity(size_t index, f64 density)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCloud>().density = density;

        onParameters(sky);
    });
}

vec2 SkyParametersDialog::skyCloudPositionSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerCloud>().position);
}

void SkyParametersDialog::onSkyCloudPosition(size_t index, const vec2& position)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCloud>().position = delocalizeAngle(position);

        onParameters(sky);
    });
}

vec2 SkyParametersDialog::skyCloudSizeSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerCloud>().size);
}

void SkyParametersDialog::onSkyCloudSize(size_t index, const vec2& size)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCloud>().size = delocalizeAngle(size);

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyCloudInnerColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerCloud>().innerColor;
}

void SkyParametersDialog::onSkyCloudInnerColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCloud>().innerColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyCloudOuterColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerCloud>().outerColor;
}

void SkyParametersDialog::onSkyCloudOuterColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerCloud>().outerColor = color;

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyAuroraDensitySource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerAurora>().density;
}

void SkyParametersDialog::onSkyAuroraDensity(size_t index, f64 density)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerAurora>().density = density;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyAuroraLowerColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerAurora>().lowerColor;
}

void SkyParametersDialog::onSkyAuroraLowerColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerAurora>().lowerColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyAuroraUpperColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerAurora>().upperColor;
}

void SkyParametersDialog::onSkyAuroraUpperColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerAurora>().upperColor = color;

        onParameters(sky);
    });
}

vec2 SkyParametersDialog::skyCometPositionSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerComet>().position);
}

void SkyParametersDialog::onSkyCometPosition(size_t index, const vec2& position)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerComet>().position = delocalizeAngle(position);

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyCometSizeSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerComet>().size);
}

void SkyParametersDialog::onSkyCometSize(size_t index, f64 size)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerComet>().size = delocalizeAngle(size);

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyCometLengthSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerComet>().length);
}

void SkyParametersDialog::onSkyCometLength(size_t index, f64 length)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerComet>().length = delocalizeAngle(length);

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyCometRotationSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerComet>().rotation);
}

void SkyParametersDialog::onSkyCometRotation(size_t index, f64 rotation)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerComet>().rotation = delocalizeAngle(rotation);

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyCometHeadColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerComet>().headColor;
}

void SkyParametersDialog::onSkyCometHeadColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerComet>().headColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyCometTailColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerComet>().tailColor;
}

void SkyParametersDialog::onSkyCometTailColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerComet>().tailColor = color;

        onParameters(sky);
    });
}

vec2 SkyParametersDialog::skyVortexPositionSource(size_t index)
{
    return localizeLength(parametersSource().layers.at(index).get<SkyLayerVortex>().position);
}

void SkyParametersDialog::onSkyVortexPosition(size_t index, const vec2& position)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerVortex>().position = delocalizeLength(position);

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyVortexRotationSpeedSource(size_t index)
{
    return localizeAngle(parametersSource().layers.at(index).get<SkyLayerVortex>().rotationSpeed);
}

void SkyParametersDialog::onSkyVortexRotationSpeed(size_t index, f64 rotationSpeed)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerVortex>().rotationSpeed = delocalizeAngle(rotationSpeed);

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyVortexRadiusSource(size_t index)
{
    return localizeLength(parametersSource().layers.at(index).get<SkyLayerVortex>().radius);
}

void SkyParametersDialog::onSkyVortexRadius(size_t index, f64 radius)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerVortex>().radius = delocalizeLength(radius);

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyVortexInnerColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerVortex>().innerColor;
}

void SkyParametersDialog::onSkyVortexInnerColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerVortex>().innerColor = color;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyVortexOuterColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerVortex>().outerColor;
}

void SkyParametersDialog::onSkyVortexOuterColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerVortex>().outerColor = color;

        onParameters(sky);
    });
}

f64 SkyParametersDialog::skyMeteorShowerDensitySource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerMeteorShower>().density;
}

void SkyParametersDialog::onSkyMeteorShowerDensity(size_t index, f64 density)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerMeteorShower>().density = density;

        onParameters(sky);
    });
}

HDRColor SkyParametersDialog::skyMeteorShowerColorSource(size_t index)
{
    return parametersSource().layers.at(index).get<SkyLayerMeteorShower>().color;
}

void SkyParametersDialog::onSkyMeteorShowerColor(size_t index, const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        SkyParameters sky = parametersSource();

        sky.layers.at(index).get<SkyLayerMeteorShower>().color = color;

        onParameters(sky);
    });
}
