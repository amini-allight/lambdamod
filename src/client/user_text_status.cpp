/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_text_status.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "row.hpp"
#include "read_only_item_list.hpp"
#include "label.hpp"
#include "spacer.hpp"
#include "close_button.hpp"

UserTextStatus::UserTextStatus(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto row = new Row(this);

    new ReadOnlyItemList(
        row,
        bind(&UserTextStatus::usersSource, this),
        bind(&UserTextStatus::onSelect, this, placeholders::_1)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    listView = new ListView(
        row,
        [](size_t index) -> void {},
        theme.backgroundColor
    );
}

void UserTextStatus::open()
{

}

void UserTextStatus::close()
{

}

void UserTextStatus::step()
{
    if (!userName.empty())
    {
        updateListView();
    }

    InterfaceWidget::step();
}

vector<string> UserTextStatus::usersSource()
{
    vector<string> users;

    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        users.push_back(user.name());
    }

    return users;
}

void UserTextStatus::onSelect(const string& name)
{
    userName = name;
}

void UserTextStatus::onRemoveKnowledge(const string& title)
{
    TextClearRequest clear;
    clear.type = Text_Clear_Specific_Knowledge;
    clear.title = title;

    TextRequestEvent event(TextRequest(TextID(), clear));

    event.userIDs.insert(context()->controller()->game().getUserByName(userName)->id());

    context()->controller()->send(event);
}

void UserTextStatus::updateListView()
{
    listView->clearChildren();

    const User* user = context()->controller()->game().getUserByName(userName);

    TextStyleOverride typeStyle;
    typeStyle.weight = Text_Bold;

    bool signal = false;

    if (user && context()->controller()->self()->admin())
    {
        signal = user->signal();
    }

    {
        auto row = new Row(listView);

        new Label(row, localize("user-text-status-signal"), typeStyle);

        new Label(row, signal ? localize("user-text-status-enabled") : localize("user-text-status-disabled"));

        MAKE_SPACER(row, UIScale::iconButtonSize().w, UIScale::iconButtonSize().h);
    }

    optional<TextTitlecardRequest> titlecard;

    if (user && context()->controller()->self()->admin())
    {
        titlecard = user->titlecard();
    }
    
    if (titlecard)
    {
        auto row = new Row(listView);

        new Label(row, localize("user-text-status-titlecard"), typeStyle);

        new Label(row, titlecard->title);

        MAKE_SPACER(row, UIScale::iconButtonSize().w, UIScale::iconButtonSize().h);
    }

    deque<TextRequest> texts;

    if (user && context()->controller()->self()->admin())
    {
        texts = user->texts();
    }

    for (const TextRequest& text : texts)
    {
        auto row = new Row(listView);

        string type;
        string content;
        
        switch (text.type())
        {
        case Text_Titlecard :
            type = "user-text-status-titlecard";
            content = text.data<TextTitlecardRequest>().title;
            break;
        case Text_Timeout :
            type = "user-text-status-timeout";
            content = text.data<TextTimeoutRequest>().title;
            break;
        case Text_Unary :
            type = "user-text-status-unary";
            content = text.data<TextUnaryRequest>().title;
            break;
        case Text_Binary :
            type = "user-text-status-binary";
            content = text.data<TextBinaryRequest>().title;
            break;
        case Text_Options :
            type = "user-text-status-options";
            content = text.data<TextOptionsRequest>().title;
            break;
        case Text_Choose :
            type = "user-text-status-choose";
            content = text.data<TextChooseRequest>().title;
            break;
        case Text_Assign_Values :
            type = "user-text-status-assign-values";
            content = text.data<TextAssignValuesRequest>().title;
            break;
        case Text_Spend_Points :
            type = "user-text-status-spend-points";
            content = text.data<TextSpendPointsRequest>().title;
            break;
        case Text_Assign_Points :
            type = "user-text-status-assign-points";
            content = text.data<TextAssignPointsRequest>().title;
            break;
        case Text_Vote :
            type = "user-text-status-vote";
            content = text.data<TextVoteRequest>().title;
            break;
        case Text_Choose_Major :
            type = "user-text-status-choose-major";
            content = text.data<TextChooseMajorRequest>().title;
            break;
        case Text_Signal :
        case Text_Knowledge :
        case Text_Clear :
            // Shouldn't happen
            continue;
        }

        new Label(row, localize(type), typeStyle);

        new Label(row, content);

        MAKE_SPACER(row, UIScale::iconButtonSize().w, UIScale::iconButtonSize().h);
    }

    vector<TextKnowledgeRequest> knowledges;

    if (user && context()->controller()->self()->admin())
    {
        knowledges = user->knowledge();
    }

    for (const TextKnowledgeRequest& knowledge : knowledges)
    {
        auto row = new Row(listView);

        new Label(row, localize("user-text-status-knowledge"), typeStyle);

        new Label(row, knowledge.title);

        new CloseButton(
            row,
            [this, knowledge]() -> void { onRemoveKnowledge(knowledge.title); }
        );
    }

    listView->update();
}

SizeProperties UserTextStatus::sizeProperties() const
{
    return sizePropertiesFillAll;
}
