/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "syntax_highlighting.hpp"

class LineEdit : public InterfaceWidget
{
public:
    LineEdit(
        InterfaceWidget* parent,
        const function<string()>& source,
        const function<void(const string&)>& onReturn,
        bool codeMode = false
    );

    void set(const string& text);
    const string& content() const;
    i32 cursorIndex() const;
    i32 scrollIndex() const;
    bool empty() const;
    void clear();

    void setPlaceholder(const string& text);

    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;
    void focus() override;
    void unfocus() override;

protected:
    function<string()> source;
    function<void(const string&)> onReturn;
    bool codeMode;

    string text;
    i32 _cursorIndex;
    i32 _selectIndex;
    i32 _scrollIndex;
    bool selected;
    string placeholder;
    bool selecting;
    chrono::milliseconds lastClickTime;

    vector<SyntaxHighlightChunk> syntaxHighlight;

    void add(i32 index, const string& s);
    void add(i32 index, char c);
    void remove(i32 start, i32 count);
    void cursorLeft();
    void cursorRight();
    void selectLeft();
    void selectRight();
    void goTo(i32 index);
    void selectTo(i32 index);
    void startSelect(const Point& pointer);
    void endSelect(const Input& input, const Point& pointer);
    void moveSelect(const Point& pointer);

    virtual i32 charWidth() const;
    virtual i32 charHeight() const;

    i32 visualColumns() const;

    string wordAtCursor() const;

    i32 softLineStart() const;
    i32 lineStart() const;
    i32 lineEnd() const;
    i32 lineIndent() const;

    i32 previousWordStart() const;
    i32 previousWordEnd() const;

    i32 wordStart() const;
    i32 wordEnd() const;

    i32 nextWordStart() const;
    i32 nextWordEnd() const;

    i32 previousCharacter() const;
    i32 nextCharacter() const;

    i32 selectionStart() const;
    i32 selectionEnd() const;

    virtual i32 screenToIndex(const Point& point) const;

    void setText(const string& text, bool after = false);
    void correct();

    void updateSyntaxHighlight();

    void selectAll(const Input& input);
    void cursorToPreviousWord(const Input& input);
    void cursorToNextWord(const Input& input);
    void selectLeft(const Input& input);
    void selectRight(const Input& input);
    void selectToPreviousWord(const Input& input);
    void selectToNextWord(const Input& input);
    void cursorLeft(const Input& input);
    void cursorRight(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);
    void removePreviousWord(const Input& input);
    void removeNextWord(const Input& input);
    void removeToStart(const Input& input);
    void removeToEnd(const Input& input);
    void removePreviousCharacter(const Input& input);
    void removeNextCharacter(const Input& input);
    void upcaseNextWord(const Input& input);
    void downcaseNextWord(const Input& input);
    void swapWords(const Input& input);
    void swapCharacters(const Input& input);
    void selectTo(const Input& input);
    void cursorTo(const Input& input);
    void endSelect(const Input& input);
    void moveSelect(const Input& input);
    void addText(const Input& input);
    void cut(const Input& input);
    void copy(const Input& input);
    void paste(const Input& input);
#if (defined(__linux__) || defined(__FreeBSD__))
    void pastePrimary(const Input& input);
#endif

    SizeProperties sizeProperties() const override;
};
