/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "client.hpp"
#include "log.hpp"
#include "message_boundaries.hpp"

#ifdef LMOD_FAKELAG
static constexpr chrono::milliseconds fakeLagDuration(100);
#endif

Client::Client(
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : connectCallback(connectCallback)
    , receiveCallback(receiveCallback)
    , disconnectCallback(disconnectCallback)
    , nextMessageSize(0)
    , connected(false)
#ifdef LMOD_FAKELAG
    , lagFaker(fakeLagDuration)
#endif
{

}

Client::~Client()
{

}

void Client::onConnect()
{
    log("Connected to server.");

    connectCallback();
}

void Client::onReceive(const string& data)
{
    for (const string& message : messageUnboundary(data, nextMessageSize, buffer))
    {
#ifdef LMOD_FAKELAG
        if (lagFaker.pushReceive(message))
        {
            continue;
        }
#endif

        receiveCallback(NetworkEvent(message));
    }
}

void Client::onDisconnect()
{
    log("Disconnected from server.");

    disconnectCallback();
}
