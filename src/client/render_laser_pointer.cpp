/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_laser_pointer.hpp"
#include "log.hpp"
#include "interface_constants.hpp"
#include "render_tools.hpp"

static constexpr i32 laserPointerLength = 2;

RenderLaserPointer::RenderLaserPointer(
    const RenderContext* ctx,

    vector<VRSwapchainElement*> swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderVRPipelineStore* pipelineStore,
    const vec3& position,
    const vec3& direction
)
    : RenderVRDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, quaternion(), vec3(1) };

    vertices = createVertexBuffer(2 * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData = {
        { fvec3(), theme.accentColor.toVec3() },
        { direction * laserPointerLength, theme.accentColor.toVec3() }
    };

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderLaserPointer::~RenderLaserPointer()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderLaserPointer::work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const
{
    RenderVRDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        2
    );
}

RenderDecorationType RenderLaserPointer::drawType() const
{
    return Render_Decoration_Over_Panels;
}

VmaBuffer RenderLaserPointer::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
#endif
