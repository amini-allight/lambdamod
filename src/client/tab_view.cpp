/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tab_view.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tab_pane.hpp"
#include "tab_content.hpp"
#include "control_context.hpp"

TabView::TabView(InterfaceWidget* parent, const vector<string>& tabNames, bool vertical)
    : InterfaceWidget(parent)
    , tabNames(tabNames)
    , vertical(vertical)
    , index(0)
{
    START_WIDGET_SCOPE("tab-view")
        WIDGET_SLOT("previous-tab", previousTab)
        WIDGET_SLOT("next-tab", nextTab)

        START_SCOPE("tab-view-bar", tabBarFocused())
            WIDGET_SLOT("interact", interact)
        END_SCOPE
    END_SCOPE

    for (size_t i = 0; i < tabNames.size(); i++)
    {
        new TabPane(this, i);
    }
}

void TabView::open(const string& tabName)
{
    size_t i = 0;
    for (const string& name : tabNames)
    {
        if (name == tabName)
        {
            index = i;
        }

        i++;
    }
}

InterfaceWidget* TabView::tab(const string& name) const
{
    size_t i = 0;
    for (const string& tabName : tabNames)
    {
        if (tabName == name)
        {
            return children()[i];
        }

        i++;
    }

    return nullptr;
}

void TabView::move(const Point& position)
{
    this->position(position);

    Point offset = vertical ? Point(UIScale::tabSize().w + UIScale::dividerWidth(), 0) : Point(0, UIScale::tabSize().h + UIScale::dividerWidth());

    for (InterfaceWidget* child : children())
    {
        child->move(position + offset);
    }
}

void TabView::resize(const Size& size)
{
    this->size(size);

    Size offset = vertical ? Size(UIScale::tabSize().w + UIScale::dividerWidth(), 0) : Size(0, UIScale::tabSize().h + UIScale::dividerWidth());

    for (InterfaceWidget* child : children())
    {
        child->resize(size - offset);
    }
}

void TabView::step()
{
    children()[index]->step();
}

void TabView::draw(const DrawContext& ctx) const
{
    size_t i = 0;
    for (const string& tabName : tabNames)
    {
        Point point = vertical ? Point(0, i * UIScale::tabSize().h) : Point(i * UIScale::tabSize().w, 0);

        if (i == index || (focused() && contains(point, UIScale::tabSize(), pointer)))
        {
            drawSolid(
                ctx,
                this,
                point,
                UIScale::tabSize(),
                theme.accentColor
            );
        }

        TextStyle style;
        style.weight = i == index ? Text_Bold : Text_Regular;
        style.color = i == index ? theme.textColor : theme.inactiveTextColor;
        style.alignment = Text_Center;

        drawText(
            ctx,
            this,
            point,
            UIScale::tabSize(),
            tabName,
            style
        );

        i++;
    }

    Point dividerPoint = vertical ? Point(UIScale::tabSize().w, 0) : Point(0, UIScale::tabSize().h);
    Size dividerSize = vertical ? Size(UIScale::dividerWidth(), size().h) : Size(size().w, UIScale::dividerWidth());

    drawSolid(
        ctx,
        this,
        dividerPoint,
        dividerSize,
        theme.dividerColor
    );

    InterfaceWidget::draw(ctx);
}

size_t TabView::activeIndex() const
{
    return index;
}

InterfaceWidget* TabView::activeTab() const
{
    return children().at(index);
}

void TabView::openTab()
{
    for (InterfaceWidget* subChild : children()[index]->children())
    {
        if (auto pane = dynamic_cast<TabContent*>(subChild))
        {
            pane->open();
        }
    }
}

void TabView::closeTab()
{
    children()[index]->clearFocus();

    for (InterfaceWidget* subChild : children()[index]->children())
    {
        if (auto pane = dynamic_cast<TabContent*>(subChild))
        {
            pane->close();
        }
    }
}

void TabView::previousTab(const Input& input)
{
    closeTab();
    if (index == 0)
    {
        index = tabNames.size();
    }
    index--;
    openTab();
}

void TabView::nextTab(const Input& input)
{
    closeTab();
    index++;
    if (index >= tabNames.size())
    {
        index = 0;
    }
    openTab();
}

void TabView::interact(const Input& input)
{
    Point local = pointer - position();

    closeTab();
    index = vertical ? local.y / UIScale::tabSize().h : local.x / UIScale::tabSize().w;
    if (index >= tabNames.size())
    {
        index = 0;
    }
    openTab();
}

bool TabView::tabBarFocused() const
{
    return vertical
        ? (pointer - position()).x < UIScale::tabSize().w
        : (pointer - position()).y < UIScale::tabSize().h;
}

SizeProperties TabView::sizeProperties() const
{
    return sizePropertiesFillAll;
}
