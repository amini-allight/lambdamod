/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "team_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "fuzzy_find.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "label.hpp"

#include "new_team_dialog.hpp"
#include "confirm_dialog.hpp"
#include "list_dialog.hpp"

TeamDetail::TeamDetail()
{

}

TeamDetail::TeamDetail(const Team& team)
    : name(team.name)
    , size(team.userIDs.size())
{

}

bool TeamDetail::operator==(const TeamDetail& rhs) const
{
    return name == rhs.name && size == rhs.size;
}

bool TeamDetail::operator!=(const TeamDetail& rhs) const
{
    return !(*this == rhs);
}

TeamSettings::TeamSettings(InterfaceWidget* parent)
    : RightClickableWidget(parent)
{
    START_WIDGET_SCOPE("team-settings")
        WIDGET_SLOT("add-new", add)
    END_SCOPE

    auto column = new Column(this);

    auto header = new Row(column);

    TextStyleOverride style;
    style.weight = Text_Bold;
    style.color = &theme.activeTextColor;
    style.alignment = Text_Center;

    new Label(header, localize("team-settings-name"), style);
    new Label(header, localize("team-settings-size"), style);
    MAKE_SPACER(header, UIScale::scrollbarWidth(), UIScale::labelHeight());

    searchBar = new LineEdit(column, nullptr, [](const string& text) -> void {});
    searchBar->setPlaceholder(localize("team-settings-type-to-search"));

    listView = new ListView(column);
}

void TeamSettings::step()
{
    RightClickableWidget::step();

    vector<TeamDetail> details = convert(context()->controller()->game().teams());
    string search = searchBar->content();

    if (details != this->details || search != this->search)
    {
        this->details = details;
        this->search = search;
        updateListView();
    }
}

vector<TeamDetail> TeamSettings::convert(const map<string, Team>& teams) const
{
    vector<TeamDetail> details;
    details.reserve(teams.size());

    for (const auto& [ name, team ] : teams)
    {
        details.push_back(TeamDetail(team));
    }

    return details;
}

void TeamSettings::openRightClickMenu(i32 index, const Point& pointer)
{
    if (index >= static_cast<i32>(details.size()))
    {
        return;
    }

    const TeamDetail& detail = details[index];

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("team-settings-remove"), bind(&TeamSettings::onRemove, this, detail.name, placeholders::_1) },
        { localize("team-settings-manage-users"), bind(&TeamSettings::onManageUsers, this, detail.name, placeholders::_1) },
    };

    rightClickMenu = new RightClickMenu(
        this,
        bind(&TeamSettings::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void TeamSettings::onRemove(const string& name, const Point& point)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("team-settings-are-you-sure-you-want-to-delete-team", name),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            TeamEvent event;
            event.type = Team_Event_Remove;
            event.name = name;

            context()->controller()->send(event);
        }
    );

    closeRightClickMenu();
}

void TeamSettings::onManageUsers(const string& name, const Point& point)
{
    context()->interface()->addWindow<ListDialog>(
        localize("team-settings-users"),
        bind(&TeamSettings::usersSource, this, name),
        bind(&TeamSettings::onAddUser, this, name, placeholders::_1),
        bind(&TeamSettings::onRemoveUser, this, name, placeholders::_1)
    );

    closeRightClickMenu();
}

vector<string> TeamSettings::usersSource(const string& name)
{
    auto it = context()->controller()->game().teams().find(name);

    if (it == context()->controller()->game().teams().end())
    {
        return {};
    }

    const Team& team = it->second;

    vector<string> users;
    users.reserve(team.userIDs.size());

    for (UserID userID : team.userIDs)
    {
        const User* user = context()->controller()->game().getUser(userID);

        if (!user)
        {
            continue;
        }

        users.push_back(user->name());
    }

    return users;
}

void TeamSettings::onAddUser(const string& name, const string& userName)
{
    auto it = context()->controller()->game().teams().find(name);

    if (it == context()->controller()->game().teams().end())
    {
        return;
    }

    const Team& team = it->second;

    set<UserID> userIDs = team.userIDs;

    const User* user = context()->controller()->game().getUserByName(userName);

    if (!user)
    {
        return;
    }

    userIDs.insert(user->id());

    TeamEvent event;
    event.type = Team_Event_Update;
    event.name = name;
    event.userIDs = userIDs;

    context()->controller()->send(event);
}

void TeamSettings::onRemoveUser(const string& name, const string& userName)
{
    auto it = context()->controller()->game().teams().find(name);

    if (it == context()->controller()->game().teams().end())
    {
        return;
    }

    const Team& team = it->second;

    set<UserID> userIDs = team.userIDs;

    const User* user = context()->controller()->game().getUserByName(userName);

    if (!user)
    {
        return;
    }

    userIDs.erase(user->id());

    TeamEvent event;
    event.type = Team_Event_Update;
    event.name = name;
    event.userIDs = userIDs;

    context()->controller()->send(event);
}

void TeamSettings::updateListView()
{
    listView->clearChildren();

    // If we sort the member variable version it will cause an update on every TeamSettings::step
    vector<TeamDetail> details = this->details;

    if (!search.empty())
    {
        sort(
            details.begin(),
            details.end(),
            [&](const TeamDetail& a, const TeamDetail& b) -> bool
            {
                return fuzzyFindCompare(a.name, b.name, search);
            }
        );
    }

    size_t i = 0;
    for (const TeamDetail& detail : details)
    {
        auto row = new Row(
            listView,
            {},
            [this, i](const Point& pointer) -> void
            {
                onRightClick(i, pointer);
            }
        );

        TextStyleOverride style;
        style.alignment = Text_Center;

        new Label(row, detail.name, style);
        new Label(row, to_string(detail.size), style);

        i++;
    }

    listView->update();
}

void TeamSettings::add(const Input& input)
{
    context()->interface()->addWindow<NewTeamDialog>();
}

SizeProperties TeamSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
