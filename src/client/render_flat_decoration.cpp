/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_decoration.hpp"
#include "global.hpp"
#include "render_tools.hpp"

RenderFlatDecoration::RenderFlatDecoration(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
)
    : RenderDecoration(ctx, descriptorSetLayout, pipelineLayout, pipeline)
{

}

RenderFlatDecoration::~RenderFlatDecoration()
{

}

void RenderFlatDecoration::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    fmat4 model = fmat4(mat4(transform.position() - cameraPosition, transform.rotation(), transform.scale()));

    memcpy(components.at(swapchainElement->imageIndex())->uniformMapping->data, &model, sizeof(fmat4));
}

void RenderFlatDecoration::refresh(const vector<FlatSwapchainElement*>& swapchainElements)
{
    destroyComponents();

    createComponents(swapchainElements);
}

void RenderFlatDecoration::fillCommandBuffer(
    const MainSwapchainElement* swapchainElement,
    VkDescriptorSet descriptorSet,
    VkBuffer vertices,
    u32 vertexCount
) const
{
    VkCommandBuffer commandBuffer = swapchainElement->commandBuffer();
    u32 width = swapchainElement->width();
    u32 height = swapchainElement->height();

    VkViewport viewport;

    if (g_vr)
    {
        viewport = {
            0, 0,
            static_cast<f32>(width), static_cast<f32>(height),
            0, 1
        };
    }
    else
    {
        viewport = {
            0, static_cast<f32>(height),
            static_cast<f32>(width), -static_cast<f32>(height),
            0, 1
        };
    }

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { static_cast<u32>(width), static_cast<u32>(height) }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &descriptorSet,
        0,
        nullptr
    );

    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, &vertices, &offset);

    vkCmdDraw(commandBuffer, vertexCount, 1, 0, 0);
}

void RenderFlatDecoration::createComponents(const vector<FlatSwapchainElement*>& swapchainElements)
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        components.push_back(new RenderDecorationComponent(
            ctx,
            descriptorSetLayout,
            swapchainElements.at(i)->camera(),
            createUniformBuffer()
        ));
    }
}
