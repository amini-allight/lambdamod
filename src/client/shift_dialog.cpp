/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "shift_dialog.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "option_select.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(
        128 * uiScale(),
        UIScale::optionSelectHeight()
    );
}

ShiftDialog::ShiftDialog(InterfaceView* view, const function<void(const string&)>& onDone)
    : Dialog(view, dialogSize())
    , onDone(onDone)
{
    auto column = new Column(this);

    new OptionSelect(
        column,
        bind(&ShiftDialog::worldsSource, this),
        nullptr,
        bind(&ShiftDialog::onSelect, this, placeholders::_1)
    );
}

vector<string> ShiftDialog::worldsSource()
{
    vector<string> worldNames;
    worldNames.reserve(context()->controller()->game().worlds().size());

    for (const auto& [ name, world ] : context()->controller()->game().worlds())
    {
        worldNames.push_back(name);
    }

    return worldNames;
}

void ShiftDialog::onSelect(size_t index)
{
    onDone(worldsSource().at(index));
}
