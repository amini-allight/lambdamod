/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_widget.hpp"
#include "interface_window.hpp"
#include "control_context.hpp"
#include "global.hpp"
#include "interface.hpp"

InterfaceWidget::InterfaceWidget(InterfaceWidget* parent)
    : _parent(parent)
    , _view(nullptr)
    , _focused(false)
{
    _parent->addChild(this);
}

InterfaceWidget::InterfaceWidget(InterfaceView* view)
    : _parent(nullptr)
    , _view(view)
    , _focused(false)
{
    _view->add(this);
}

InterfaceWidget::~InterfaceWidget()
{
    vector<InterfaceWidget*> children = _children;

    for (InterfaceWidget* child : children)
    {
        delete child;
    }

    while (!scopes.empty())
    {
        delete scopes.top();
        scopes.pop();
    }

    if (_parent)
    {
        _parent->removeChild(this);
    }
    else
    {
        _view->remove(this);
    }
}

static bool parentFocused(InterfaceWidget* widget)
{
    if (!widget->parent())
    {
        return true;
    }

    if (widget->parent()->scope())
    {
        return widget->parent()->focused();
    }
    else
    {
        return parentFocused(widget->parent());
    }
}

bool InterfaceWidget::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    this->pointer = pointer;

    if (!focused() && contains(pointer) && shouldDraw() && !consumed && parentFocused(this))
    {
        focus();
    }
    else if (focused() && (!contains(pointer) || !shouldDraw() || consumed || !parentFocused(this)))
    {
        unfocus();
    }

    return focused();
}

void InterfaceWidget::update()
{
    this->resize(size());
    this->move(position());
}

void InterfaceWidget::move(const Point& position)
{
    _position = position;

    for (InterfaceWidget* child : _children)
    {
        child->move(position);
    }
}

void InterfaceWidget::resize(const Size& size)
{
    _size = size;

    for (InterfaceWidget* child : _children)
    {
        child->resize(size);
    }
}

void InterfaceWidget::step()
{
    for (InterfaceWidget* child : _children)
    {
        if (!child->shouldDraw())
        {
            continue;
        }

        child->step();
    }
}

void InterfaceWidget::draw(const DrawContext& ctx) const
{
    for (InterfaceWidget* child : _children)
    {
        if (!child->shouldDraw())
        {
            continue;
        }

        child->draw(ctx);
    }
}

void InterfaceWidget::focus()
{
    _focused = true;
}

void InterfaceWidget::unfocus()
{
    _focused = false;
}

void InterfaceWidget::clearFocus()
{
    if (focused())
    {
        unfocus();
    }

    for (InterfaceWidget* child : children())
    {
        child->clearFocus();
    }
}

bool InterfaceWidget::shouldDraw() const
{
    return _parent ? _parent->shouldDraw() : true;
}

InterfaceWidget* InterfaceWidget::parent() const
{
    return _parent;
}

InterfaceWidget* InterfaceWidget::rootParent() const
{
    return _parent ? _parent->rootParent() : const_cast<InterfaceWidget*>(this);
}

InterfaceView* InterfaceWidget::view() const
{
    return _view ? _view : _parent->view();
}

ControlContext* InterfaceWidget::context() const
{
    return _parent ? _parent->context() : _view->interface()->context();
}

const vector<InterfaceWidget*> InterfaceWidget::children() const
{
    return _children;
}

void InterfaceWidget::clearChildren()
{
    vector<InterfaceWidget*> children = _children;

    for (InterfaceWidget* child : children)
    {
        delete child;
    }
}

bool InterfaceWidget::focused() const
{
    return _focused;
}

bool InterfaceWidget::contains(const Point& point) const
{
    return point.x >= _position.x &&
        point.y >= _position.y &&
        point.x < _position.x + _size.w &&
        point.y < _position.y + _size.h;
}

bool InterfaceWidget::contains(const Point& subPoint, const Size& subSize, const Point& point) const
{
    Point start = _position + subPoint;
    Point end = start + subSize;

    return point.x >= start.x &&
        point.y >= start.y &&
        point.x < end.x &&
        point.y < end.y;
}

const Point& InterfaceWidget::position() const
{
    return _position;
}

const Size& InterfaceWidget::size() const
{
    return _size;
}

InputScope* InterfaceWidget::scope() const
{
    return scopes.empty() ? nullptr : scopes.top();
}

void InterfaceWidget::addChild(InterfaceWidget* child)
{
    _children.push_back(child);
}

void InterfaceWidget::removeChild(InterfaceWidget* child)
{
    auto it = find_if(
        _children.begin(),
        _children.end(),
        [&](const InterfaceWidget* widget) -> bool
        {
            return widget == child;
        }
    );

    if (it == _children.end())
    {
        return;
    }

    _children.erase(it);
}

void InterfaceWidget::position(const Point& position)
{
    _position = position;
}

void InterfaceWidget::size(const Size& size)
{
    _size = size;
}

void InterfaceWidget::playFocusEffect() const
{
    if (!g_config.uiSounds)
    {
        return;
    }

    if (_focused)
    {
        return;
    }

    context()->controller()->sound()->addOneshot(Sound_Oneshot_Focus);
}

void InterfaceWidget::playUnfocusEffect() const
{
    if (!g_config.uiSounds)
    {
        return;
    }

    if (!_focused)
    {
        return;
    }

    context()->controller()->sound()->addOneshot(Sound_Oneshot_Unfocus);
}

void InterfaceWidget::playPositiveActivateEffect() const
{
    if (!g_config.uiSounds)
    {
        return;
    }

    context()->controller()->sound()->addOneshot(Sound_Oneshot_Positive_Activate);
}

void InterfaceWidget::playNegativeActivateEffect() const
{
    if (!g_config.uiSounds)
    {
        return;
    }

    context()->controller()->sound()->addOneshot(Sound_Oneshot_Negative_Activate);
}

void InterfaceWidget::createScope(
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<bool(const Input&)>& isActive,
    const function<void(InputScope*)>& block
)
{
    new InputScope(context(), parent, name, mode, isActive, block);
}

PositionProperties InterfaceWidget::positionProperties() const
{
    return { 0, 0, Positioning_Fraction, Positioning_Fraction };
}
