/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "control_context.hpp"
#include "global.hpp"

// Fix for Windows namespace pollution
#undef interface

ControlContext::ControlContext(Controller* controller)
    : _controller(controller)
    , _viewerMode(nullptr)
    , _attachedMode(nullptr)
    , _flatViewer(nullptr)
#ifdef LMOD_VR
    , _vrViewer(nullptr)
    , _vrMenuButtonHandler(nullptr)
#endif
    , _tether(nullptr)
    , _confinement(nullptr)
    , _tint(nullptr)
    , _fade(nullptr)
    , _wait(nullptr)
    , _text(nullptr)
    , _input(nullptr)
    , _userVisualizer(nullptr)
    , _splash(nullptr)
    , postProcessManager(nullptr)
    , _interface(nullptr)
    , _hud(true)
{
    _viewerMode = new ViewerModeContext(this);
    _attachedMode = new AttachedModeContext(this);

#ifdef LMOD_VR
    if (!g_vr)
    {
        _flatViewer = new FlatViewer(this);
    }
    else
    {
        _vrViewer = new VRViewer(this);
        _vrMenuButtonHandler = new VRMenuButtonHandler();
    }
#else
    _flatViewer = new FlatViewer(this);
#endif

    _tether = new TetherManager(this);
    _confinement = new ConfinementManager(this);
    _tint = new TintManager(this);
    _fade = new FadeManager(this);
    _wait = new WaitManager(this);

    _text = new TextManager(this);

    _input = new InputManager(this);
    // Necessary because this has a self-referential initialization that reads _input
    _input->init();

    _userVisualizer = new UserVisualizer(this);
    _splash = new SplashManager();

    postProcessManager = new PostProcessManager(this, _tint, _fade);

    _interface = new Interface(this);
}

ControlContext::~ControlContext()
{
    delete _splash;
    delete _userVisualizer;

    // Must be after user visualizer because user visualizer destroys markers on shutdown, but before attached mode & viewer mode, because some windows access the context via those
    delete _interface;

    delete postProcessManager;

    delete _input;

    delete _text;

    delete _wait;
    delete _fade;
    delete _confinement;
    delete _tether;

#ifdef LMOD_VR
    if (!g_vr)
    {
        delete _flatViewer;
    }
    else
    {
        delete _vrMenuButtonHandler;
        delete _vrViewer;
    }
#else
    delete _flatViewer;
#endif

    delete _attachedMode;
    delete _viewerMode;
}

void ControlContext::focus()
{
    _flatViewer->focus();
    _interface->focus();
}

void ControlContext::unfocus()
{
    _flatViewer->unfocus();
    _interface->unfocus();
}

void ControlContext::resize(u32 width, u32 height)
{
    _interface->resize(width, height);
    _flatViewer->resize(width, height);
}

void ControlContext::step()
{
    if (attached())
    {
        _attachedMode->step();
    }
    else
    {
        _viewerMode->step();
    }

#ifdef LMOD_VR
    if (!g_vr)
    {
        _flatViewer->step();
    }
    else
    {
        _vrViewer->step();
        _vrMenuButtonHandler->step();
    }
#else
    _flatViewer->step();
#endif

    _tether->step();
    _confinement->step();
    _fade->step();
    _wait->step();

    _text->step();
}

bool ControlContext::attached() const
{
    const User* self = _controller->self();

    return self ? self->attached() : false;
}

Controller* ControlContext::controller() const
{
    return _controller;
}

Interface* ControlContext::interface() const
{
    return _interface;
}

ViewerModeContext* ControlContext::viewerMode() const
{
    return _viewerMode;
}

AttachedModeContext* ControlContext::attachedMode() const
{
    return _attachedMode;
}

FlatViewer* ControlContext::flatViewer() const
{
    return _flatViewer;
}

#ifdef LMOD_VR
VRViewer* ControlContext::vrViewer() const
{
    return _vrViewer;
}

VRMenuButtonHandler* ControlContext::vrMenuButtonHandler() const
{
    return _vrMenuButtonHandler;
}
#endif

TetherManager* ControlContext::tether() const
{
    return _tether;
}

ConfinementManager* ControlContext::confinement() const
{
    return _confinement;
}

TintManager* ControlContext::tint() const
{
    return _tint;
}

FadeManager* ControlContext::fade() const
{
    return _fade;
}

WaitManager* ControlContext::wait() const
{
    return _wait;
}

TextManager* ControlContext::text() const
{
    return _text;
}

InputManager* ControlContext::input() const
{
    return _input;
}

UserVisualizer* ControlContext::userVisualizer() const
{
    return _userVisualizer;
}

SplashManager* ControlContext::splash() const
{
    return _splash;
}

mat4 ControlContext::viewerSpace() const
{
    if (!attached())
    {
        return mat4();
    }
    else
    {
        return _controller->host()->globalTransform();
    }
}

void ControlContext::save()
{
    _controller->send(NetworkEvent(SaveEvent()));
}

void ControlContext::moveViewer(const mat4& transform)
{
    const vec3& position = transform.position();
    const quaternion& rotation = transform.rotation();

    // Important to check !_controller->self() here, this is received during initial events before users have been initialized
    if (!_controller->self() || !_controller->self()->attached())
    {
#ifdef LMOD_VR
        if (!g_vr)
        {
            _flatViewer->move(position, rotation);
        }
        else
        {
            _vrViewer->moveRoom(position, rotation);
        }
#else
        _flatViewer->move(position, rotation);
#endif
    }
}

void ControlContext::unbrake()
{
    GameEvent event(Game_Event_Unbrake);

    _controller->send(event);
}

void ControlContext::lockAttachments()
{
    GameEvent event(Game_Event_Lock_Attachments);

    _controller->send(event);
}

void ControlContext::unlockAttachments()
{
    GameEvent event(Game_Event_Unlock_Attachments);

    _controller->send(event);
}

void ControlContext::setMagnitude(u32 magnitude)
{
    UserEvent event(User_Event_Magnitude);
    event.u = magnitude;

    _controller->send(event);
}

void ControlContext::setHUDShown(bool state)
{
    _hud = state;
}

bool ControlContext::hudShown() const
{
    return _hud;
}
