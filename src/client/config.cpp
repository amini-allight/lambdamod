/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "config.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "yaml_tools.hpp"

static VRMotionMode vrMotionModeFromString(const string& name)
{
    if (name == "head")
    {
        return VR_Motion_Head;
    }
    else if (name == "hand")
    {
        return VR_Motion_Hand;
    }
    else if (name == "hip")
    {
        return VR_Motion_Hip;
    }
    else
    {
        warning("Encountered unknown VR motion mode '" + name + "', assuming head.");
        return VR_Motion_Head;
    }
}

static string vrMotionModeToString(VRMotionMode mode)
{
    switch (mode)
    {
    case VR_Motion_Head : return "head";
    case VR_Motion_Hand : return "hand";
    case VR_Motion_Hip : return "hip";
    default :
        warning("Encountered unknown VR motion mode '" + to_string(mode) + "', assuming head.");
        return "head";
    }
}

static SoundSpatialMode soundSpatialModeFromString(const string& name)
{
    if (name == "headphones")
    {
        return Sound_Spatial_Mode_Headphones;
    }
    else if (name == "speakers")
    {
        return Sound_Spatial_Mode_Speakers;
    }
    else
    {
        warning("Encountered unknown sound spatial mode '" + name + "', assuming headphones.");
        return Sound_Spatial_Mode_Headphones;
    }
}

static string soundSpatialModeToString(SoundSpatialMode mode)
{
    switch (mode)
    {
    case Sound_Spatial_Mode_Headphones : return "headphones";
    case Sound_Spatial_Mode_Speakers : return "speakers";
    default :
        warning("Encountered unknown sound spatial mode '" + to_string(mode) + "', assuming headphones.");
        return "headphones";
    }
}

Config::Config()
{
    startupSplash = true;
    splash = true;
    frameCounter = false;
    frameLimit = 0;
    stepRate = 100;
    historyWindow = 10;
    pingCounter = false;
    antiAliasingLevel = 4;
    width = 1600;
    height = 900;
    fullscreen = false;
    outputMuted = false;
    outputVolume = 1;
    inputMuted = false;
    inputVolume = 1;
    vrMotionMode = VR_Motion_Head;
    vrSmoothTurning = false;
    vrSmoothTurnSpeed = radians(22.5);
    vrSnapTurnIncrement = radians(45);
    vrKeyboardRightHand = false;
    vrForced = false;
    vrRollCage = false;
    advancedTextEditor = false;
    nodeEditor = false;
    quaternionEditor = false;
    numpadAlternative = false;
    maxRecursionDepth = 4096;
    maxMemoryUsage = 128 * 1024 * 1024;
    maxLoopDuration = 0.02;
    invertMouseViewerX = false;
    invertMouseViewerY = false;
    mouseViewerSensitivity = { 1.0, 1.0 };
    invertGamepadViewerX = false;
    invertGamepadViewerY = false;
    gamepadViewerSensitivity = { 1.0, 1.0 };
    gamepadLeftDeadZone = { 0.1, 0.1 };
    gamepadRightDeadZone = { 0.1, 0.1 };
    vrLeftDeadZone = { 0.0, 0.0 };
    vrRightDeadZone = { 0.0, 0.0 };
    soundSpatialMode = Sound_Spatial_Mode_Headphones;
    soundMono = false;
    touchInput = true;
    flatMotionBlur = false;
    flatDepthOfField = false;
    flatBloom = true;
    flatChromaticAberration = false;
    vrMotionBlur = false;
    vrDepthOfField = false;
    vrBloom = true;
    vrChromaticAberration = false;
    rayTracing = false;
    imperialUnits = false;
    degrees = true;
    uiSounds = true;
    uiAutoScale = false;
    uiScale = 1;
    luminanceScale = 0.5;
}

template<>
Config YAMLNode::convert() const
{
    Config config;

    if (has("startupSplash"))
    {
        config.startupSplash = at("startupSplash").as<bool>();
    }

    if (has("splash"))
    {
        config.splash = at("splash").as<bool>();
    }

    if (has("frameCounter"))
    {
        config.frameCounter = at("frameCounter").as<bool>();
    }

    if (has("frameLimit"))
    {
        config.frameLimit = at("frameLimit").as<u64>();
    }

    if (has("stepRate"))
    {
        config.stepRate = at("stepRate").as<u64>();

        if (config.stepRate == 0)
        {
            warning("Invalid 'stepRate' config value: " + to_string(config.stepRate));
            config.stepRate = Config().stepRate;
        }
    }

    if (has("historyWindow"))
    {
        config.historyWindow = at("historyWindow").as<f64>();

        if (config.historyWindow <= 0)
        {
            warning("Invalid 'historyWindow' config value: " + to_string(config.historyWindow));
            config.historyWindow = Config().historyWindow;
        }
    }

    if (has("pingCounter"))
    {
        config.pingCounter = at("pingCounter").as<bool>();
    }

    if (has("antiAliasingLevel"))
    {
        config.antiAliasingLevel = at("antiAliasingLevel").as<u32>();

        if (config.antiAliasingLevel > 64 || log(config.antiAliasingLevel, 2) != floor(log(config.antiAliasingLevel, 2)))
        {
            warning("Invalid 'antiAliasingLevel' config value: " + to_string(config.antiAliasingLevel));
            config.antiAliasingLevel = Config().antiAliasingLevel;
        }
    }

    if (has("width"))
    {
        config.width = at("width").as<u32>();

        if (config.width == 0)
        {
            warning("Invalid 'width' config value: " + to_string(config.width));
            config.width = Config().width;
        }
    }

    if (has("height"))
    {
        config.height = at("height").as<u32>();

        if (config.height == 0)
        {
            warning("Invalid 'height' config value: " + to_string(config.height));
            config.height = Config().height;
        }
    }

    if (has("fullscreen"))
    {
        config.fullscreen = at("fullscreen").as<bool>();
    }

    if (has("outputMuted"))
    {
        config.outputMuted = at("outputMuted").as<bool>();
    }

    if (has("outputVolume"))
    {
        config.outputVolume = at("outputVolume").as<f64>();

        if (config.outputVolume < 0 || config.outputVolume > 1)
        {
            warning("Invalid 'outputVolume' config value: " + to_string(config.outputVolume));
            config.outputVolume = clamp<f64>(config.outputVolume, 0, 1);
        }
    }

    if (has("inputMuted"))
    {
        config.inputMuted = at("inputMuted").as<bool>();
    }

    if (has("inputVolume"))
    {
        config.inputVolume = at("inputVolume").as<f64>();

        if (config.inputVolume < 0 || config.inputVolume > 1)
        {
            warning("Invalid 'inputVolume' config value: " + to_string(config.inputVolume));
            config.inputVolume = clamp<f64>(config.inputVolume, 0, 1);
        }
    }

    if (has("vrMotionMode"))
    {
        config.vrMotionMode = vrMotionModeFromString(at("vrMotionMode").as<string>());
    }

    if (has("vrSmoothTurning"))
    {
        config.vrSmoothTurning = at("vrSmoothTurning").as<bool>();
    }

    if (has("vrSmoothTurnSpeed"))
    {
        config.vrSmoothTurnSpeed = radians(at("vrSmoothTurnSpeed").as<f64>());

        if (config.vrSmoothTurnSpeed == 0)
        {
            warning("Invalid 'vrSmoothTurnSpeed' config value: " + to_string(config.vrSmoothTurnSpeed));
            config.vrSmoothTurnSpeed = Config().vrSmoothTurnSpeed;
        }
    }

    if (has("vrSnapTurnIncrement"))
    {
        config.vrSnapTurnIncrement = radians(at("vrSnapTurnIncrement").as<f64>());

        if (config.vrSnapTurnIncrement == 0)
        {
            warning("Invalid 'vrSnapTurnIncrement' config value: " + to_string(config.vrSnapTurnIncrement));
            config.vrSnapTurnIncrement = Config().vrSnapTurnIncrement;
        }
    }

    if (has("vrKeyboardRightHand"))
    {
        config.vrKeyboardRightHand = at("vrKeyboardRightHand").as<bool>();
    }

    if (has("vrForced"))
    {
        config.vrForced = at("vrForced").as<bool>();
    }

    if (has("vrRollCage"))
    {
        config.vrRollCage = at("vrRollCage").as<bool>();
    }

    if (has("graphicsDevice"))
    {
        config.graphicsDevice = at("graphicsDevice").as<string>();
    }

    if (has("soundInputDevice"))
    {
        config.soundInputDevice = at("soundInputDevice").as<string>();
    }

    if (has("soundOutputDevice"))
    {
        config.soundOutputDevice = at("soundOutputDevice").as<string>();
    }

    if (has("gamepadDevice"))
    {
        config.gamepadDevice = at("gamepadDevice").as<string>();
    }

    if (has("touchDevice"))
    {
        config.touchDevice = at("touchDevice").as<string>();
    }

    if (has("caFilePath"))
    {
        config.caFilePath = at("caFilePath").as<string>();
    }

    if (has("advancedTextEditor"))
    {
        config.advancedTextEditor = at("advancedTextEditor").as<bool>();
    }

    if (has("nodeEditor"))
    {
        config.nodeEditor = at("nodeEditor").as<bool>();
    }

    if (has("quaternionEditor"))
    {
        config.quaternionEditor = at("quaternionEditor").as<bool>();
    }

    if (has("numpadAlternative"))
    {
        config.numpadAlternative = at("numpadAlternative").as<bool>();
    }

    if (has("maxRecursionDepth"))
    {
        config.maxRecursionDepth = at("maxRecursionDepth").as<u64>();

        if (config.maxRecursionDepth == 0)
        {
            warning("Invalid 'maxRecursionDepth' config value: " + to_string(config.maxRecursionDepth));
            config.maxRecursionDepth = Config().maxRecursionDepth;
        }
    }

    if (has("maxMemoryUsage"))
    {
        config.maxMemoryUsage = at("maxMemoryUsage").as<u64>();

        if (config.maxMemoryUsage == 0)
        {
            warning("Invalid 'maxMemoryUsage' config value: " + to_string(config.maxMemoryUsage));
            config.maxMemoryUsage = Config().maxMemoryUsage;
        }
    }

    if (has("maxLoopDuration"))
    {
        config.maxLoopDuration = at("maxLoopDuration").as<f64>();

        if (config.maxLoopDuration == 0)
        {
            warning("Invalid 'maxLoopDuration' config value: " + to_string(config.maxLoopDuration));
            config.maxLoopDuration = Config().maxLoopDuration;
        }
    }

    if (has("invertMouseViewerX"))
    {
        config.invertMouseViewerX = at("invertMouseViewerX").as<bool>();
    }

    if (has("invertMouseViewerY"))
    {
        config.invertMouseViewerY = at("invertMouseViewerY").as<bool>();
    }

    if (has("mouseViewerSensitivity"))
    {
        config.mouseViewerSensitivity = at("mouseViewerSensitivity").as<vec2>();

        if (config.mouseViewerSensitivity.x <= 0 || config.mouseViewerSensitivity.y <= 0)
        {
            stringstream ss;
            ss << config.mouseViewerSensitivity;
            warning("Invalid 'mouseViewerSensitivity' config value: " + ss.str());

            if (config.mouseViewerSensitivity.x <= 0)
            {
                config.mouseViewerSensitivity.x = Config().mouseViewerSensitivity.x;
            }

            if (config.mouseViewerSensitivity.y <= 0)
            {
                config.mouseViewerSensitivity.y = Config().mouseViewerSensitivity.y;
            }
        }
    }

    if (has("invertGamepadViewerX"))
    {
        config.invertGamepadViewerX = at("invertGamepadViewerX").as<bool>();
    }

    if (has("invertGamepadViewerY"))
    {
        config.invertGamepadViewerY = at("invertGamepadViewerY").as<bool>();
    }

    if (has("gamepadViewerSensitivity"))
    {
        config.gamepadViewerSensitivity = at("gamepadViewerSensitivity").as<vec2>();

        if (config.gamepadViewerSensitivity.x <= 0 || config.gamepadViewerSensitivity.y <= 0)
        {
            stringstream ss;
            ss << config.gamepadViewerSensitivity;
            warning("Invalid 'gamepadViewerSensitivity' config value: " + ss.str());

            if (config.gamepadViewerSensitivity.x <= 0)
            {
                config.gamepadViewerSensitivity.x = Config().gamepadViewerSensitivity.x;
            }

            if (config.gamepadViewerSensitivity.y <= 0)
            {
                config.gamepadViewerSensitivity.y = Config().gamepadViewerSensitivity.y;
            }
        }
    }

    if (has("gamepadLeftDeadZone"))
    {
        config.gamepadLeftDeadZone = at("gamepadLeftDeadZone").as<vec2>();

        if (
            config.gamepadLeftDeadZone.x < 0 ||
            config.gamepadLeftDeadZone.x > 1 ||
            config.gamepadLeftDeadZone.y < 0 ||
            config.gamepadLeftDeadZone.y > 1
        )
        {
            stringstream ss;
            ss << config.gamepadLeftDeadZone;
            warning("Invalid 'gamepadLeftDeadZone' config value: " + ss.str());

            config.gamepadLeftDeadZone.x = clamp<f64>(config.gamepadLeftDeadZone.x, 0, 1);
            config.gamepadLeftDeadZone.y = clamp<f64>(config.gamepadLeftDeadZone.y, 0, 1);
        }
    }

    if (has("gamepadRightDeadZone"))
    {
        config.gamepadRightDeadZone = at("gamepadRightDeadZone").as<vec2>();

        if (
            config.gamepadRightDeadZone.x < 0 ||
            config.gamepadRightDeadZone.x > 1 ||
            config.gamepadRightDeadZone.y < 0 ||
            config.gamepadRightDeadZone.y > 1
        )
        {
            stringstream ss;
            ss << config.gamepadRightDeadZone;
            warning("Invalid 'gamepadRightDeadZone' config value: " + ss.str());

            config.gamepadRightDeadZone.x = clamp<f64>(config.gamepadRightDeadZone.x, 0, 1);
            config.gamepadRightDeadZone.y = clamp<f64>(config.gamepadRightDeadZone.y, 0, 1);
        }
    }

    if (has("vrLeftDeadZone"))
    {
        config.vrLeftDeadZone = at("vrLeftDeadZone").as<vec2>();

        if (
            config.vrLeftDeadZone.x < 0 ||
            config.vrLeftDeadZone.x > 1 ||
            config.vrLeftDeadZone.y < 0 ||
            config.vrLeftDeadZone.y > 1
        )
        {
            stringstream ss;
            ss << config.vrLeftDeadZone;
            warning("Invalid 'vrLeftDeadZone' config value: " + ss.str());

            config.vrLeftDeadZone.x = clamp<f64>(config.vrLeftDeadZone.x, 0, 1);
            config.vrLeftDeadZone.y = clamp<f64>(config.vrLeftDeadZone.y, 0, 1);
        }
    }

    if (has("vrRightDeadZone"))
    {
        config.vrRightDeadZone = at("vrRightDeadZone").as<vec2>();

        if (
            config.vrRightDeadZone.x < 0 ||
            config.vrRightDeadZone.x > 1 ||
            config.vrRightDeadZone.y < 0 ||
            config.vrRightDeadZone.y > 1
        )
        {
            stringstream ss;
            ss << config.vrRightDeadZone;
            warning("Invalid 'vrRightDeadZone' config value: " + ss.str());

            config.vrRightDeadZone.x = clamp<f64>(config.vrRightDeadZone.x, 0, 1);
            config.vrRightDeadZone.y = clamp<f64>(config.vrRightDeadZone.y, 0, 1);
        }
    }

    if (has("soundSpatialMode"))
    {
        config.soundSpatialMode = soundSpatialModeFromString(at("soundSpatialMode").as<string>());
    }

    if (has("soundMono"))
    {
        config.soundMono = at("soundMono").as<bool>();
    }

    if (has("touchInput"))
    {
        config.touchInput = at("touchInput").as<bool>();
    }

    if (has("flatMotionBlur"))
    {
        config.flatMotionBlur = at("flatMotionBlur").as<bool>();
    }

    if (has("flatDepthOfField"))
    {
        config.flatDepthOfField = at("flatDepthOfField").as<bool>();
    }

    if (has("flatBloom"))
    {
        config.flatBloom = at("flatBloom").as<bool>();
    }

    if (has("flatChromaticAberration"))
    {
        config.flatChromaticAberration = at("flatChromaticAberration").as<bool>();
    }

    if (has("vrMotionBlur"))
    {
        config.vrMotionBlur = at("vrMotionBlur").as<bool>();
    }

    if (has("vrDepthOfField"))
    {
        config.vrDepthOfField = at("vrDepthOfField").as<bool>();
    }

    if (has("vrBloom"))
    {
        config.vrBloom = at("vrBloom").as<bool>();
    }

    if (has("vrChromaticAberration"))
    {
        config.vrChromaticAberration = at("vrChromaticAberration").as<bool>();
    }

    if (has("rayTracing"))
    {
        config.rayTracing = at("rayTracing").as<bool>();
    }

    if (has("imperialUnits"))
    {
        config.imperialUnits = at("imperialUnits").as<bool>();
    }

    if (has("degrees"))
    {
        config.degrees = at("degrees").as<bool>();
    }

    if (has("uiSounds"))
    {
        config.uiSounds = at("uiSounds").as<bool>();
    }

    if (has("uiAutoScale"))
    {
        config.uiAutoScale = at("uiAutoScale").as<bool>();
    }

    if (has("uiScale"))
    {
        config.uiScale = at("uiScale").as<f64>();

        if (config.uiScale <= minUIScale)
        {
            warning("Invalid 'uiScale' config value: " + to_string(config.uiScale));

            config.uiScale = max<f64>(config.uiScale, minUIScale);
        }
    }

    if (has("luminanceScale"))
    {
        config.luminanceScale = clamp<f64>(at("luminanceScale").as<f64>(), numeric_limits<f64>::min(), 1);
    }

    return config;
}

template<>
void YAMLSerializer::emit(Config v)
{
    startMapping();

    emitPair("startupSplash", v.startupSplash);
    emitPair("splash", v.splash);
    emitPair("frameCounter", v.frameCounter);
    emitPair("frameLimit", v.frameLimit);
    emitPair("stepRate", v.stepRate);
    emitPair("historyWindow", v.historyWindow);
    emitPair("pingCounter", v.pingCounter);
    emitPair("antiAliasingLevel", v.antiAliasingLevel);
    emitPair("width", v.width);
    emitPair("height", v.height);
    emitPair("fullscreen", v.fullscreen);
    emitPair("outputMuted", v.outputMuted);
    emitPair("outputVolume", v.outputVolume);
    emitPair("inputMuted", v.inputMuted);
    emitPair("inputVolume", v.inputVolume);
    emit("vrMotionMode");
    emit(vrMotionModeToString(v.vrMotionMode));
    emitPair("vrSmoothTurning", v.vrSmoothTurning);
    emitPair("vrSmoothTurnSpeed", degrees(v.vrSmoothTurnSpeed));
    emitPair("vrSnapTurnIncrement", degrees(v.vrSnapTurnIncrement));
    emitPair("vrKeyboardRightHand", v.vrKeyboardRightHand);
    emitPair("vrForced", v.vrForced);
    emitPair("vrRollCage", v.vrRollCage);
    emitPair("graphicsDevice", v.graphicsDevice);
    emitPair("soundInputDevice", v.soundInputDevice);
    emitPair("soundOutputDevice", v.soundOutputDevice);
    emitPair("gamepadDevice", v.gamepadDevice);
    emitPair("touchDevice", v.touchDevice);
    emitPair("caFilePath", v.caFilePath);
    emitPair("advancedTextEditor", v.advancedTextEditor);
    emitPair("nodeEditor", v.nodeEditor);
    emitPair("quaternionEditor", v.quaternionEditor);
    emitPair("numpadAlternative", v.numpadAlternative);
    emitPair("maxRecursionDepth", v.maxRecursionDepth);
    emitPair("maxMemoryUsage", v.maxMemoryUsage);
    emitPair("maxLoopDuration", v.maxLoopDuration);
    emitPair("invertMouseViewerX", v.invertMouseViewerX);
    emitPair("invertMouseViewerY", v.invertMouseViewerY);
    emitPair("mouseViewerSensitivity", v.mouseViewerSensitivity);
    emitPair("invertGamepadViewerX", v.invertGamepadViewerX);
    emitPair("invertGamepadViewerY", v.invertGamepadViewerY);
    emitPair("gamepadViewerSensitivity", v.gamepadViewerSensitivity);
    emitPair("gamepadLeftDeadZone", v.gamepadLeftDeadZone);
    emitPair("gamepadRightDeadZone", v.gamepadRightDeadZone);
    emitPair("vrLeftDeadZone", v.vrLeftDeadZone);
    emitPair("vrRightDeadZone", v.vrRightDeadZone);
    emitPair("soundSpatialMode", soundSpatialModeToString(v.soundSpatialMode));
    emitPair("soundMono", v.soundMono);
    emitPair("touchInput", v.touchInput);
    emitPair("flatMotionBlur", v.flatMotionBlur);
    emitPair("flatDepthOfField", v.flatDepthOfField);
    emitPair("flatBloom", v.flatBloom);
    emitPair("flatChromaticAberration", v.flatChromaticAberration);
    emitPair("vrMotionBlur", v.vrMotionBlur);
    emitPair("vrDepthOfField", v.vrDepthOfField);
    emitPair("vrBloom", v.vrBloom);
    emitPair("vrChromaticAberration", v.vrChromaticAberration);
    emitPair("rayTracing", v.rayTracing);
    emitPair("imperialUnits", v.imperialUnits);
    emitPair("degrees", v.degrees);
    emitPair("uiSounds", v.uiSounds);
    emitPair("uiAutoScale", v.uiAutoScale);
    emitPair("uiScale", v.uiScale);
    emitPair("luminanceScale", v.luminanceScale);

    endMapping();
}

static void ensureConfig(const string& path)
{
    if (fs::exists(path))
    {
        return;
    }

    if (!fs::exists(fs::path(path).parent_path()))
    {
        bool ok = fs::create_directories(fs::path(path).parent_path());

        if (!ok)
        {
            throw runtime_error("Failed to create config directories.");
        }
    }

    YAMLSerializer ctx(path);

    ctx.emit(Config());
}

Config loadConfig()
{
    string path = configPath() + clientConfigFileName;

    ensureConfig(path);

    YAMLParser ctx(path);

    return ctx.root().as<Config>();
}

void saveConfig(const Config& config)
{
    string path = configPath() + clientConfigFileName;

    YAMLSerializer ctx(path);

    ctx.emit(config);
}
