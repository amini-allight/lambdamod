/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "action.hpp"
#include "action_part.hpp"
#include "action_preview_playback.hpp"

class Column;
class ActionTimeline;
class ActionEditorSidePanel;

class ActionEditor : public InterfaceWidget
{
public:
    ActionEditor(InterfaceWidget* parent, EntityID id);

    void addActionPart(BodyPartID id);
    void removeActionPart(BodyPartID id);
    mat4 getTransform(BodyPartID id) const;
    void setTransform(BodyPartID id, const mat4& transform);
    void open(const string& name);

    void move(const Point& position) override;
    void resize(const Size& size) override;

private:
    friend class ActionEditorSidePanel;

    EntityID id;
    string name;
    set<ActionPartID> activeParts;

    Column* column;
    ActionTimeline* timeline;
    ActionEditorSidePanel* sidePanel;
    ActionPreviewPlayback previewPlayback;

    map<ActionPartID, ActionPart> lastParts;

    u32 playPositionSource();
    void onSeek(u32 position);
    void onSelect(const set<ActionPartID>& parts);

    bool playingSource();

    void onGoToStart();
    void onStop();
    void onPlayPause();
    void onGoToEnd();

    void edit(const function<void(Action&)>& behavior) const;

    const Action& currentAction() const;
    const ActionPart& currentPart() const;

    void toggleSidePanel(const Input& input);

    SizeProperties sizeProperties() const override;
};
