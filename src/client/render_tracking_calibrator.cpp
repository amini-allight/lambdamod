/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_tracking_calibrator.hpp"
#include "log.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr f64 trackerSize = 0.1;

RenderTrackingCalibrator::RenderTrackingCalibrator(
    const RenderContext* ctx,

    vector<VRSwapchainElement*> swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderVRPipelineStore* pipelineStore,
    const vec3& origin,
    const map<VRDevice, mat4>& transforms
)
    : RenderVRDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
    , transforms(transforms)
{
    transform = { origin, quaternion(), vec3(1) };

    vertices = createVertexBuffer(transforms.size() * 2 * 3 * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData;

    for (const auto& [ device, transform ] : transforms)
    {
        vertexData.push_back({
            transform.applyToPosition(fvec3()) - origin,
            theme.xAxisColor.toVec3()
        });

        vertexData.push_back({
            transform.applyToPosition(rightDir * trackerSize) - origin,
            theme.xAxisColor.toVec3()
        });

        vertexData.push_back({
            transform.applyToPosition(fvec3()) - origin,
            theme.yAxisColor.toVec3()
        });

        vertexData.push_back({
            transform.applyToPosition(forwardDir * trackerSize) - origin,
            theme.yAxisColor.toVec3()
        });

        vertexData.push_back({
            transform.applyToPosition(fvec3()) - origin,
            theme.zAxisColor.toVec3()
        });

        vertexData.push_back({
            transform.applyToPosition(upDir * trackerSize, 1) - origin,
            theme.zAxisColor.toVec3()
        });
    }

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderTrackingCalibrator::~RenderTrackingCalibrator()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderTrackingCalibrator::work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const
{
    RenderVRDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        transforms.size() * 3 * 2
    );
}

VmaBuffer RenderTrackingCalibrator::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
#endif
