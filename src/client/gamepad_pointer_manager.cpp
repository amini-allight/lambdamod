/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "gamepad_pointer_manager.hpp"
#include "global.hpp"
#include "constants.hpp"
#include "control_context.hpp"
#include "interface_widget.hpp"

GamepadPointerManager::GamepadPointerManager(InterfaceWidget* parent)
    : parent(parent)
    , scope(nullptr)
{

}

GamepadPointerManager::~GamepadPointerManager()
{
    delete scope;
}

void GamepadPointerManager::init()
{
    scope = new InputScope(
        parent->context(),
        parent->scope(),
        "gamepad-pointer-manager",
        Input_Scope_Normal,
        [](const Input& input) -> bool { return true; },
        [this](InputScope* parent) -> void {
            parent->bind(
                "move-gamepad-pointer",
                bind(&GamepadPointerManager::moveGamepadPointer, this, placeholders::_1)
            );
        }
    );
}

void GamepadPointerManager::step()
{
    if (stickMotion.length() != 0)
    {
        vec2 delta = stickMotion.toVec2() * gamepadPointerSpeed * parent->view()->interface()->stepInterval();
        gamepadPointer += delta;
        gamepadPointer = vec2(
            clamp(gamepadPointer.x, 0.0, static_cast<f64>(parent->size().w)),
            clamp(gamepadPointer.y, 0.0, static_cast<f64>(parent->size().h))
        );
        parent->movePointer(Pointer_Normal, parent->position() + Point(gamepadPointer), false);
    }
}

void GamepadPointerManager::moveGamepadPointer(const Input& input)
{
    stickMotion = input.position;
}
