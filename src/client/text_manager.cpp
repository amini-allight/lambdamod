/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_manager.hpp"
#include "control_context.hpp"
#include "global.hpp"

TextManager::TextManager(ControlContext* ctx)
    : ctx(ctx)
    , _signal(false)
{

}

void TextManager::step()
{
    if (_titlecard && _titlecard->timedOut())
    {
        _titlecard = {};
    }

    if (!requests.empty() && requests.front().timedOut())
    {
        requests.pop_front();

        if (!requests.empty())
        {
            requests.front().start();
            _response = requests.front().response();
        }
    }
}

void TextManager::request(const TextRequest& request)
{
    TextID frontID = !requests.empty() ? requests.front().id() : TextID();

    switch (request.type())
    {
    case Text_Signal :
        _signal = request.data<TextSignalRequest>().state;
        break;
    case Text_Titlecard :
        _titlecard = TextTracker(request.id(), request.data<TextTitlecardRequest>());
        _titlecard->start();
        ctx->controller()->sound()->addOneshot(Sound_Oneshot_Title);
        break;
    case Text_Timeout :
        requests.push_back(TextTracker(request.id(), request.data<TextTimeoutRequest>()));
        break;
    case Text_Unary :
        requests.push_back(TextTracker(request.id(), request.data<TextUnaryRequest>()));
        break;
    case Text_Binary :
        requests.push_back(TextTracker(request.id(), request.data<TextBinaryRequest>()));
        break;
    case Text_Options :
        requests.push_back(TextTracker(request.id(), request.data<TextOptionsRequest>()));
        break;
    case Text_Choose :
        requests.push_back(TextTracker(request.id(), request.data<TextChooseRequest>()));
        break;
    case Text_Assign_Values :
        requests.push_back(TextTracker(request.id(), request.data<TextAssignValuesRequest>()));
        break;
    case Text_Spend_Points :
        requests.push_back(TextTracker(request.id(), request.data<TextSpendPointsRequest>()));
        break;
    case Text_Assign_Points :
        requests.push_back(TextTracker(request.id(), request.data<TextAssignPointsRequest>()));
        break;
    case Text_Vote :
        requests.push_back(TextTracker(request.id(), request.data<TextVoteRequest>()));
        break;
    case Text_Choose_Major :
        requests.push_back(TextTracker(request.id(), request.data<TextChooseMajorRequest>()));
        break;
    case Text_Knowledge :
        _knowledge.push_back(request.data<TextKnowledgeRequest>());
        break;
    case Text_Clear :
        switch (request.data<TextClearRequest>().type)
        {
        case Text_Clear_All :
            _knowledge.clear();
            [[fallthrough]];
        case Text_Clear_All_But_Knowledge :
            _signal = false;
            _titlecard = {};
            requests.clear();
            break;
        case Text_Clear_Specific_Knowledge :
            for (auto it = _knowledge.begin(); it != _knowledge.end(); it++)
            {
                if (it->title == request.data<TextClearRequest>().title)
                {
                    _knowledge.erase(it);
                    break;
                }
            }
        }
        break;
    }

    if (!requests.empty() && (!frontID || requests.front().id() != frontID))
    {
        requests.front().start();
        _response = requests.front().response();
    }
}

void TextManager::respond()
{
    bool success = false;

    switch (requests.front().type())
    {
    case Text_Signal :
    case Text_Titlecard :
    case Text_Timeout :
    case Text_Knowledge :
    case Text_Clear :
        fatal("Cannot respond to text type '" + to_string(requests.front().type()) + "' that does not accept a response.");
    case Text_Unary :
        if (requestData<TextUnaryRequest>().valid(response<TextUnaryResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Binary :
        if (requestData<TextBinaryRequest>().valid(response<TextBinaryResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Options :
        if (requestData<TextOptionsRequest>().valid(response<TextOptionsResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Choose :
        if (requestData<TextChooseRequest>().valid(response<TextChooseResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Assign_Values :
        if (requestData<TextAssignValuesRequest>().valid(response<TextAssignValuesResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Spend_Points :
        if (requestData<TextSpendPointsRequest>().valid(response<TextSpendPointsResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Assign_Points :
        if (requestData<TextAssignPointsRequest>().valid(response<TextAssignPointsResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Vote :
        if (requestData<TextVoteRequest>().valid(response<TextVoteResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    case Text_Choose_Major :
        if (requestData<TextChooseMajorRequest>().valid(response<TextChooseMajorResponse>()))
        {
            TextResponseEvent event(_response);

            ctx->controller()->send(event);
            success = true;
        }
        break;
    }

    if (success)
    {
        requests.pop_front();

        if (!requests.empty())
        {
            requests.front().start();
            _response = requests.front().response();
        }
    }
}

bool TextManager::signal() const
{
    return _signal;
}

optional<TextTracker> TextManager::titlecard() const
{
    if (_titlecard)
    {
        return _titlecard;
    }
    else
    {
        return {};
    }
}

bool TextManager::hasRequest() const
{
    return !requests.empty();
}

TextID TextManager::requestID() const
{
    return requests.front().id();
}

TextType TextManager::requestType() const
{
    return requests.front().type();
}

u64 TextManager::requestElapsed() const
{
    return requests.front().elapsed().count();
}

bool TextManager::needsInteraction() const
{
    if (!hasRequest())
    {
        return false;
    }

    switch (requests.front().type())
    {
    default :
    case Text_Signal :
    case Text_Titlecard :
    case Text_Timeout :
        return false;
    case Text_Unary :
    case Text_Binary :
    case Text_Options :
    case Text_Choose :
    case Text_Assign_Values :
    case Text_Spend_Points :
    case Text_Assign_Points :
    case Text_Vote :
    case Text_Choose_Major :
        return true;
    case Text_Knowledge :
    case Text_Clear :
        return false;
    }
}

const vector<TextKnowledgeRequest>& TextManager::knowledge() const
{
    return _knowledge;
}
