/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tether_manager.hpp"
#include "control_context.hpp"
#include "global.hpp"
#include "vr_render.hpp"
#include "flat_render.hpp"

#include "render_vr_tether_indicator.hpp"
#include "render_flat_tether_indicator.hpp"

static constexpr f64 tetherOffset = -1;

TetherManager::TetherManager(ControlContext* ctx)
    : ctx(ctx)
{
    tethered = false;
    distance = 0;

#ifdef LMOD_VR
    if (g_vr)
    {
        tetherIndicatorID = dynamic_cast<VRRender*>(ctx->controller()->render())->addDecoration<RenderVRTetherIndicator>(vec3(), vec3(), 0);
    }
    else
    {
        tetherIndicatorID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<RenderFlatTetherIndicator>(vec3(), vec3(), 0);
    }
#else
    tetherIndicatorID = dynamic_cast<FlatRender*>(ctx->controller()->render())->addDecoration<RenderFlatTetherIndicator>(vec3(), vec3(), 0);
#endif

    ctx->controller()->render()->setDecorationShown(tetherIndicatorID, false);
}

void TetherManager::step()
{
    if (!tethered)
    {
        return;
    }

    const User* self = ctx->controller()->self();

    if (!self)
    {
        return;
    }

    vec3 position = center();

    vec3 selfPosition;

    if (self->attached())
    {
        selfPosition = ctx->controller()->host()->globalPosition();
    }
#ifdef LMOD_VR
    else if (g_vr)
    {
        selfPosition = ctx->vrViewer()->roomTransform().position();
    }
#endif
    else
    {
        selfPosition = ctx->flatViewer()->position() + ctx->controller()->selfWorld()->up(ctx->flatViewer()->position()) * tetherOffset;
    }

    f64 selfDistance = position.distance(selfPosition);

    f64 strength = min(selfDistance, 1.0);

#ifdef LMOD_VR
    if (g_vr)
    {
        dynamic_cast<VRRender*>(ctx->controller()->render())->updateDecoration<RenderVRTetherIndicator>(
            tetherIndicatorID,
            position,
            selfPosition,
            strength
        );
    }
    else
    {
        dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<RenderFlatTetherIndicator>(
            tetherIndicatorID,
            position,
            selfPosition,
            strength
        );
    }
#else
    dynamic_cast<FlatRender*>(ctx->controller()->render())->updateDecoration<RenderFlatTetherIndicator>(
        tetherIndicatorID,
        position,
        selfPosition,
        strength
    );
#endif
}

void TetherManager::tether(const set<UserID>& userIDs, f64 distance)
{
    tethered = true;
    this->userIDs = userIDs;
    this->distance = distance;

    ctx->controller()->render()->setDecorationShown(tetherIndicatorID, true);
}

void TetherManager::untether()
{
    tethered = false;
    userIDs.clear();
    distance = 0;

    ctx->controller()->render()->setDecorationShown(tetherIndicatorID, false);
}

vec3 TetherManager::center() const
{
    vec3 position;
    u32 count = 0;

    for (UserID userID : userIDs)
    {
        if (userID == ctx->controller()->selfID())
        {
            continue;
        }

        const User* user = ctx->controller()->game().getUser(userID);

        if (!user)
        {
            continue;
        }

        if (user->attached())
        {
            position += ctx->controller()->game().get(user->entityID())->globalPosition();
        }
        else
        {
            position += user->viewer().position();
        }

        count++;
    }

    if (count == 0)
    {
        return position;
    }
    else
    {
        return position / count;
    }
}
