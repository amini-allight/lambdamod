/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_atmosphere_types.hpp"
#include "render_interface.hpp"
#include "render_occlusion_scene_reference.hpp"
#include "atmosphere_parameters.hpp"

class RenderAtmosphereComponentEffect
{
public:
    RenderAtmosphereComponentEffect(
        const RenderInterface* render,
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    );
    RenderAtmosphereComponentEffect(const RenderAtmosphereComponentEffect& rhs) = delete;
    RenderAtmosphereComponentEffect(RenderAtmosphereComponentEffect&& rhs) = delete;
    ~RenderAtmosphereComponentEffect();

    RenderAtmosphereComponentEffect& operator=(const RenderAtmosphereComponentEffect& rhs) = delete;
    RenderAtmosphereComponentEffect& operator=(RenderAtmosphereComponentEffect&& rhs) = delete;

    void initParticles(
        const AtmosphereEffect& effect,
        mt19937_64& engine,
        const vec3& gravity,
        f64 density
    );
    void copyParticles(VkCommandBuffer commandBuffer, const RenderAtmosphereComponentEffect* lastEffect) const;

    void fillUniformBuffer(
        const AtmosphereEffect& effect,
        vec3 cameraPosition[eyeCount],
        const vec3& gravity,
        const vec3& flowVelocity,
        f64 density
    );
    void fillCommandBuffer(
        VkCommandBuffer commandBuffer,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    ) const;

    AtmosphereEffectType type;
    VmaBuffer uniform;
    VmaMapping<AtmosphereEffectGPU>* uniformMapping;
    VmaBuffer particles;
    VmaMapping<AtmosphereEffectParticleGPU>* particlesMapping;
    VmaBuffer rotations;
    VmaMapping<fvec4>* rotationsMapping;
    VmaBuffer clouds;
    VmaMapping<fvec4>* cloudsMapping;
    VkDescriptorSet descriptorSet;

private:
    const RenderInterface* render;
    mutable bool initialized;
    optional<vec3> lastViewerPosition;

    VmaBuffer createUniformBuffer(size_t size) const;
    VmaBuffer createStorageBuffer(size_t size) const;
    VkDescriptorSet createDescriptorSet(
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera,
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene
    ) const;

    mat4 cameraLocalTransform(const vec3& cameraPosition, const vec3& effectPosition) const;

    tuple<vec3, f64> randomRotation(mt19937_64& engine) const;
    tuple<vec3, f64> randomSubParticle(mt19937_64& engine) const;
};
