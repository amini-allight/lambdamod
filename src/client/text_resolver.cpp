/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_resolver.hpp"
#include "tools.hpp"
#include "log.hpp"
#include "theme.hpp"
#include "text_style.hpp"

static constexpr chrono::milliseconds cacheTimeout(10 * 1000);

TextResolverKey::TextResolverKey(BodyTextType type, const string& text, u32 resolution)
    : type(type)
    , text(text)
    , resolution(resolution)
{

}

bool TextResolverKey::operator==(const TextResolverKey& rhs) const
{
    return type == rhs.type && text == rhs.text && resolution == rhs.resolution;
}

bool TextResolverKey::operator!=(const TextResolverKey& rhs) const
{
    return !(*this == rhs);
}

bool TextResolverKey::operator>(const TextResolverKey& rhs) const
{
    if (type > rhs.type)
    {
        return true;
    }
    else if (type < rhs.type)
    {
        return false;
    }
    else
    {
        if (text > rhs.text)
        {
            return true;
        }
        else if (text < rhs.text)
        {
            return false;
        }
        else
        {
            if (resolution < rhs.resolution)
            {
                return true;
            }
            else if (resolution > rhs.resolution)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}

bool TextResolverKey::operator<(const TextResolverKey& rhs) const
{
    if (type < rhs.type)
    {
        return true;
    }
    else if (type > rhs.type)
    {
        return false;
    }
    else
    {
        if (text > rhs.text)
        {
            return true;
        }
        else if (text < rhs.text)
        {
            return false;
        }
        else
        {
            if (resolution < rhs.resolution)
            {
                return true;
            }
            else if (resolution > rhs.resolution)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }
}

bool TextResolverKey::operator>=(const TextResolverKey& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool TextResolverKey::operator<=(const TextResolverKey& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering TextResolverKey::operator<=>(const TextResolverKey& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

TextResolver::TextResolver()
{

}

TextResolver::~TextResolver()
{
    for (const auto& [ key, image ] : cachedImages)
    {
        delete image.get();
    }
}

Image* TextResolver::resolve(const BodyPartText& part) const
{
    TextStyle style = part.style();

    auto it = cachedImages.find(TextResolverKey(part.type, part.text, style.size));

    if (it != cachedImages.end())
    {
        return it->second.get()->copy();
    }

    Image* image = style.getFont()->render(part.text, white);

    cachedImages.insert_or_assign(TextResolverKey(part.type, part.text, style.size), TextResolverCacheEntry<Image*>(image, cacheTimeout));

    clean();

    return image->copy();
}

void TextResolver::clean() const
{
    for (auto it = cachedImages.begin(); it != cachedImages.end();)
    {
        if (it->second.expired())
        {
            delete it->second.get();
            cachedImages.erase(it++);
        }
        else
        {
            it++;
        }
    }
}

TextResolver* textResolver = nullptr;
