/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "documentation_generation.hpp"

static string createMarkdownTableLine(const vector<tuple<string, size_t>>& columns)
{
    string s;

    s += "|";

    for (const auto& [ value, width ] : columns)
    {
        size_t contentWidth = value.size() + 3;

        s += " " + value + " " + string(width - contentWidth, ' ') + "|";
    }

    s += "\n";

    return s;
}

static string createMarkdownTableSeparator(const vector<size_t>& widths)
{
    string s;

    s += "|";

    for (size_t width : widths)
    {
        s += string(width - 1, '-');
        s += "|";
    }

    s += "\n";

    return s;
}

static string toHumanReadable(string name)
{
    if (name.size() >= 3 && name.substr(0, 3) == "vr-")
    {
        name = "VR-" + name.substr(3, name.size() - 3);
    }
    else if (name.size() >= 3 && name.substr(name.size() - 3, 3) == "-vr")
    {
        name = name.substr(0, name.size() - 3) + "-VR";
    }

    for (size_t i = 0; i < name.size() - 3; i++)
    {
        if (name.substr(i, 4) == "-vr-")
        {
            name = name.substr(0, i) + "-VR-" + name.substr(i + 4, name.size() - (i + 4));
        }
    }

    bool capitalize = true;

    for (size_t i = 0; i < name.size(); i++)
    {
        char c = name[i];

        if (c == '-')
        {
            capitalize = true;
            name[i] = ' ';
        }
        else if (capitalize && c >= 'a' && c <= 'z')
        {
            capitalize = false;
            name[i] = toupper(c);
        }
        else
        {
            capitalize = false;
            name[i] = c;
        }
    }

    return name;
}

void documentConfigKeys(
    const string& name,
    const map<string, vector<InputScopeBinding>>& bindings
)
{
    ofstream file(name);

    string s;

    const size_t nameWidth = 32;

    for (const auto& [ name, bindings ] : bindings)
    {
        s += createMarkdownTableLine({ { name, nameWidth } });
    }

    s = createMarkdownTableSeparator({ nameWidth }) + s;
    s = createMarkdownTableLine({ { "Key", nameWidth } }) + s;

    file << s;
}

void documentBindings(
    const string& name,
    const map<string, vector<InputScopeBinding>>& bindings,
    const function<bool(const InputScopeBinding&)>& select
)
{
    ofstream file(name);

    string s;

    const size_t nameWidth = 32;
    const size_t bindingsWidth = 160;

    for (const auto& [ name, bindings ] : bindings)
    {
        vector<InputScopeBinding> filteredBindings;

        for (const InputScopeBinding& binding : bindings)
        {
            if (!select(binding))
            {
                continue;
            }

            filteredBindings.push_back(binding);
        }

        string bindingsSummary;

        if (bindings.empty())
        {
            bindingsSummary = "unbound";
        }
        else if (filteredBindings.empty())
        {
            continue;
        }
        else
        {
            for (size_t i = 0; i < filteredBindings.size(); i++)
            {
                const InputScopeBinding& binding = filteredBindings[i];

                bindingsSummary += binding.toString();

                if (i + 1 != filteredBindings.size())
                {
                    bindingsSummary += ", ";
                }
            }
        }

        s += createMarkdownTableLine({ { toHumanReadable(name), nameWidth }, { bindingsSummary, bindingsWidth } });
    }

    s = createMarkdownTableSeparator({ nameWidth, bindingsWidth }) + s;
    s = createMarkdownTableLine({ { "Input", nameWidth }, { "Bindings", bindingsWidth } }) + s;

    file << s;
}
