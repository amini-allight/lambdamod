/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

class TabView : public InterfaceWidget
{
public:
    TabView(InterfaceWidget* parent, const vector<string>& tabNames, bool vertical = false);

    void open(const string& tabName);

    InterfaceWidget* tab(const string& name) const;

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;

    size_t activeIndex() const;
    InterfaceWidget* activeTab() const;

protected:
    vector<string> tabNames;
    bool vertical;
    size_t index;

    void openTab();
    void closeTab();

    void previousTab(const Input& input);
    void nextTab(const Input& input);
    virtual void interact(const Input& input);

    virtual bool tabBarFocused() const;

    SizeProperties sizeProperties() const override;
};
