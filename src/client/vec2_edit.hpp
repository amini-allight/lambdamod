/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"

class FloatEdit;

class Vec2Edit : public InterfaceWidget
{
public:
    Vec2Edit(
        InterfaceWidget* parent,
        const function<vec2()>& source,
        const function<void(const vec2&)>& onSet
    );

    void set(const vec2& value);
    vec2 value() const;
    void clear();

private:
    FloatEdit* x;
    FloatEdit* y;

    SizeProperties sizeProperties() const override;
};
