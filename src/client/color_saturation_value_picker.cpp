/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "color_saturation_value_picker.hpp"
#include "tools.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

ColorSaturationValuePicker::ColorSaturationValuePicker(
    InterfaceWidget* parent,
    const function<f64()>& source,
    const function<void(const vec2&)>& onSet
)
    : InterfaceWidget(parent)
    , source(source)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("color-saturation-value-picker")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void ColorSaturationValuePicker::draw(const DrawContext& ctx) const
{
    drawGradient2D(
        ctx,
        this,
        black,
        Color(hsvToRGB(vec3(source(), 1, 1)))
    );
}

void ColorSaturationValuePicker::interact(const Input& input)
{
    Point local = pointer - position();

    f64 saturation = static_cast<f64>(local.x) / size().w;
    f64 value = 1 - (static_cast<f64>(local.y) / size().h);

    playPositiveActivateEffect();
    onSet({ saturation, value });
}

SizeProperties ColorSaturationValuePicker::sizeProperties() const
{
    return sizePropertiesFillAll;
}
