/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_panel_icon.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

TextPanelIcon::TextPanelIcon(InterfaceWidget* parent, ControlContext* ctx)
    : InterfaceWidget(parent)
    , ctx(ctx)
{

}

void TextPanelIcon::draw(const DrawContext& ctx) const
{
    Icon icon;

    if (this->ctx->text()->signal())
    {
        icon = Icon_Text;
    }
    else if (this->ctx->text()->hasRequest() && this->ctx->text()->requestType() == Text_Timeout)
    {
        icon = Icon_Timeout;
    }
    else
    {
        return;
    }

    drawImage(
        ctx,
        this,
        icon
    );
}

SizeProperties TextPanelIcon::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::panelTextPanelIconSize(this)), static_cast<f64>(UIScale::panelTextPanelIconSize(this)),
        Scaling_Fixed, Scaling_Fixed
    };
}
