/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "atmosphere_parameters_dialog.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"
#include "uint_edit.hpp"
#include "percent_edit.hpp"
#include "float_edit.hpp"
#include "hdr_color_picker.hpp"
#include "spacer.hpp"
#include "option_select.hpp"
#include "vec3_edit.hpp"
#include "ufloat_edit.hpp"
#include "checkbox.hpp"
#include "text_button.hpp"
#include "emissive_color_picker.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(384, 512) * uiScale();
}

AtmosphereParametersDialog::AtmosphereParametersDialog(
    InterfaceView* view,
    const function<AtmosphereParameters()>& parametersSource,
    const function<void(const AtmosphereParameters&)>& onParameters
)
    : Dialog(view, dialogSize())
    , parametersSource(parametersSource)
    , onParameters(onParameters)
    , listView(new ListView(this))
{
    updateWidgets();
}

void AtmosphereParametersDialog::step()
{
    if (needsWidgetUpdate())
    {
        updateWidgets();
    }

    InterfaceWidget::step();
}

bool AtmosphereParametersDialog::needsWidgetUpdate() const
{
    if (!lastAtmosphereParameters)
    {
        return true;
    }

    AtmosphereParameters atmosphere = parametersSource();

    if (lastAtmosphereParameters->effects.size() != atmosphere.effects.size())
    {
        return true;
    }

    for (size_t i = 0; i < atmosphere.effects.size(); i++)
    {
        if (atmosphere.effects.at(i).type != lastAtmosphereParameters->effects.at(i).type)
        {
            return true;
        }
    }

    return false;
}

void AtmosphereParametersDialog::updateWidgets()
{
    listView->clearChildren();

    addAtmosphereChildren();

    listView->update();

    lastAtmosphereParameters = parametersSource();
}

void AtmosphereParametersDialog::addAtmosphereChildren()
{
    const AtmosphereParameters& atmosphere = parametersSource();

    new EditorEntry(listView, "atmosphere-parameters-dialog-enabled", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereEnabledSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereEnabled, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "atmosphere-parameters-dialog-atmosphere-seed", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereSeedSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereSeed, this, placeholders::_1)
        );
    });

    addAtmosphereLightningChildren(atmosphere.lightning);

    for (size_t i = 0; i < atmosphere.effects.size(); i++)
    {
        const AtmosphereEffect& effect = atmosphere.effects.at(i);

        addAtmosphereEffectChildren(i, effect);
    }

    addAtmosphereEffectNewPrompt();
}

void AtmosphereParametersDialog::addAtmosphereLightningChildren(const AtmosphereLightning& lightning)
{
    new EditorEntry(listView, "atmosphere-parameters-dialog-lightning-frequency", frequencySuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereLightningFrequencySource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereLightningFrequency, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "atmosphere-parameters-dialog-lightning-min-distance", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereLightningMinDistanceSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereLightningMinDistance, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "atmosphere-parameters-dialog-lightning-max-distance", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereLightningMaxDistanceSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereLightningMaxDistance, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "atmosphere-parameters-dialog-lightning-lower-height", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereLightningLowerHeightSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereLightningLowerHeight, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "atmosphere-parameters-dialog-lightning-upper-height", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereLightningUpperHeightSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereLightningUpperHeight, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "atmosphere-parameters-dialog-lightning-color", [this](InterfaceWidget* parent) -> void {
        new HDRColorPicker(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereLightningColorSource, this),
            bind(&AtmosphereParametersDialog::onAtmosphereLightningColor, this, placeholders::_1),
            false
        );
    });
}

void AtmosphereParametersDialog::addAtmosphereEffectChildren(size_t index, const AtmosphereEffect& effect)
{
    MAKE_SPACER(listView, 0, UIScale::marginSize() * 8);

    new EditorEntry(listView, "atmosphere-parameters-dialog-effect-type", [this, index](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereEffectTypeOptionsSource, this),
            bind(&AtmosphereParametersDialog::atmosphereEffectTypeSource, this, index),
            bind(&AtmosphereParametersDialog::onAtmosphereEffectType, this, index, placeholders::_1)
        );
    });

    if (effect.type != Atmosphere_Effect_Streamer)
    {
        new EditorEntry(listView, "atmosphere-parameters-dialog-effect-density", percentSuffix(), [this, index](InterfaceWidget* parent) -> void {
            new PercentEdit(
                parent,
                bind(&AtmosphereParametersDialog::atmosphereEffectDensitySource, this, index),
                bind(&AtmosphereParametersDialog::onAtmosphereEffectDensity, this, index, placeholders::_1)
            );
        });

        new EditorEntry(listView, "atmosphere-parameters-dialog-effect-size", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&AtmosphereParametersDialog::atmosphereEffectSizeSource, this, index),
                bind(&AtmosphereParametersDialog::onAtmosphereEffectSize, this, index, placeholders::_1)
            );
        });
    }

    if (effect.type == Atmosphere_Effect_Line || effect.type == Atmosphere_Effect_Solid)
    {
        new EditorEntry(listView, "atmosphere-parameters-dialog-effect-radius", lengthSuffix(), [this, index](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&AtmosphereParametersDialog::atmosphereEffectRadiusSource, this, index),
                bind(&AtmosphereParametersDialog::onAtmosphereEffectRadius, this, index, placeholders::_1)
            );
        });

        new EditorEntry(listView, "atmosphere-parameters-dialog-effect-mass", massSuffix(), [this, index](InterfaceWidget* parent) -> void {
            new UFloatEdit(
                parent,
                bind(&AtmosphereParametersDialog::atmosphereEffectMassSource, this, index),
                bind(&AtmosphereParametersDialog::onAtmosphereEffectMass, this, index, placeholders::_1)
            );
        });
    }

    if (effect.type == Atmosphere_Effect_Warp)
    {
        new EditorEntry(listView, "atmosphere-parameters-dialog-effect-flow-velocity", speedSuffix(), [this, index](InterfaceWidget* parent) -> void {
            new Vec3Edit(
                parent,
                bind(&AtmosphereParametersDialog::atmosphereEffectFlowVelocitySource, this, index),
                bind(&AtmosphereParametersDialog::onAtmosphereEffectFlowVelocity, this, index, placeholders::_1)
            );
        });
    }

    new EditorEntry(listView, "atmosphere-parameters-dialog-effect-color", [this, index](InterfaceWidget* parent) -> void {
        new EmissiveColorPicker(
            parent,
            bind(&AtmosphereParametersDialog::atmosphereEffectColorSource, this, index),
            bind(&AtmosphereParametersDialog::onAtmosphereEffectColor, this, index, placeholders::_1),
            false
        );
    });

    new EditorMultiEntry(
        listView,
        3,
        [this, index](InterfaceWidget* parent, size_t entryIndex) -> void
        {
            switch (entryIndex)
            {
            case 0 :
                new TextButton(
                    parent,
                    localize("atmosphere-parameters-dialog-move-atmosphere-effect-down"),
                    bind(&AtmosphereParametersDialog::moveAtmosphereEffectDown, this, index)
                );
                break;
            case 1 :
                new TextButton(
                    parent,
                    localize("atmosphere-parameters-dialog-move-atmosphere-effect-up"),
                    bind(&AtmosphereParametersDialog::moveAtmosphereEffectUp, this, index)
                );
                break;
            case 2 :
                new TextButton(
                    parent,
                    localize("atmosphere-parameters-dialog-remove-atmosphere-effect"),
                    bind(&AtmosphereParametersDialog::removeAtmosphereEffect, this, index)
                );
                break;
            }
        }
    );
}

void AtmosphereParametersDialog::addAtmosphereEffectNewPrompt()
{
    new EditorMultiEntry(
        listView,
        1,
        [this](InterfaceWidget* parent, size_t index) -> void
        {
            new TextButton(
                parent,
                localize("atmosphere-parameters-dialog-add-atmosphere-effect"),
                bind(&AtmosphereParametersDialog::addAtmosphereEffect, this)
            );
        }
    );
}

void AtmosphereParametersDialog::addAtmosphereEffect()
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.push_back(AtmosphereEffect());

        onParameters(atmosphere);
    });
}

void AtmosphereParametersDialog::moveAtmosphereEffectDown(size_t index)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        if (index + 1 != atmosphere.effects.size())
        {
            AtmosphereEffect effect = atmosphere.effects.at(index);
            atmosphere.effects.erase(atmosphere.effects.begin() + index);
            // Normally we'd add 2 to move past the following element, but we just removed the element which means we can just add 1
            atmosphere.effects.insert(atmosphere.effects.begin() + (index + 1), effect);
        }

        onParameters(atmosphere);
    });
}

void AtmosphereParametersDialog::moveAtmosphereEffectUp(size_t index)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        if (index != 0)
        {
            AtmosphereEffect effect = atmosphere.effects.at(index);
            atmosphere.effects.erase(atmosphere.effects.begin() + index);
            atmosphere.effects.insert(atmosphere.effects.begin() + (index - 1), effect);
        }

        onParameters(atmosphere);
    });
}

void AtmosphereParametersDialog::removeAtmosphereEffect(size_t index)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.erase(atmosphere.effects.begin() + index);

        onParameters(atmosphere);
    });
}

bool AtmosphereParametersDialog::atmosphereEnabledSource()
{
    return parametersSource().enabled;
}

void AtmosphereParametersDialog::onAtmosphereEnabled(bool enabled)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.enabled = enabled;

        onParameters(atmosphere);
    });
}

u64 AtmosphereParametersDialog::atmosphereSeedSource()
{
    return parametersSource().seed;
}

void AtmosphereParametersDialog::onAtmosphereSeed(u64 seed)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.seed = seed;

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereLightningFrequencySource()
{
    return parametersSource().lightning.frequency;
}

void AtmosphereParametersDialog::onAtmosphereLightningFrequency(f64 frequency)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.lightning.frequency = frequency;

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereLightningMinDistanceSource()
{
    return localizeLength(parametersSource().lightning.minDistance);
}

void AtmosphereParametersDialog::onAtmosphereLightningMinDistance(f64 distance)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.lightning.minDistance = delocalizeLength(distance);

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereLightningMaxDistanceSource()
{
    return localizeLength(parametersSource().lightning.maxDistance);
}

void AtmosphereParametersDialog::onAtmosphereLightningMaxDistance(f64 distance)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.lightning.maxDistance = delocalizeLength(distance);

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereLightningLowerHeightSource()
{
    return localizeLength(parametersSource().lightning.lowerHeight);
}

void AtmosphereParametersDialog::onAtmosphereLightningLowerHeight(f64 height)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.lightning.lowerHeight = delocalizeLength(height);

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereLightningUpperHeightSource()
{
    return localizeLength(parametersSource().lightning.upperHeight);
}

void AtmosphereParametersDialog::onAtmosphereLightningUpperHeight(f64 height)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.lightning.upperHeight = delocalizeLength(height);

        onParameters(atmosphere);
    });
}

HDRColor AtmosphereParametersDialog::atmosphereLightningColorSource()
{
    return parametersSource().lightning.color;
}

void AtmosphereParametersDialog::onAtmosphereLightningColor(const HDRColor& color)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.lightning.color = color;

        onParameters(atmosphere);
    });
}

vector<string> AtmosphereParametersDialog::atmosphereEffectTypeOptionsSource()
{
    // NOTE: Update this when adding new atmosphere effect type
    return {
        localize("atmosphere-parameters-dialog-line"),
        localize("atmosphere-parameters-dialog-solid"),
        localize("atmosphere-parameters-dialog-cloud"),
        localize("atmosphere-parameters-dialog-streamer"),
        localize("atmosphere-parameters-dialog-warp")
    };
}

size_t AtmosphereParametersDialog::atmosphereEffectTypeSource(size_t index)
{
    return static_cast<size_t>(parametersSource().effects.at(index).type);
}

void AtmosphereParametersDialog::onAtmosphereEffectType(size_t index, size_t typeIndex)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).type = static_cast<AtmosphereEffectType>(typeIndex);

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereEffectDensitySource(size_t index)
{
    return parametersSource().effects.at(index).density;
}

void AtmosphereParametersDialog::onAtmosphereEffectDensity(size_t index, f64 density)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).density = density;

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereEffectSizeSource(size_t index)
{
    return localizeLength(parametersSource().effects.at(index).size);
}

void AtmosphereParametersDialog::onAtmosphereEffectSize(size_t index, f64 size)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).size = delocalizeLength(size);

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereEffectRadiusSource(size_t index)
{
    return localizeLength(parametersSource().effects.at(index).radius);
}

void AtmosphereParametersDialog::onAtmosphereEffectRadius(size_t index, f64 radius)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).radius = delocalizeLength(radius);

        onParameters(atmosphere);
    });
}

f64 AtmosphereParametersDialog::atmosphereEffectMassSource(size_t index)
{
    return localizeMass(parametersSource().effects.at(index).mass);
}

void AtmosphereParametersDialog::onAtmosphereEffectMass(size_t index, f64 mass)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).mass = delocalizeMass(mass);

        onParameters(atmosphere);
    });
}

vec3 AtmosphereParametersDialog::atmosphereEffectFlowVelocitySource(size_t index)
{
    return localizeSpeed(parametersSource().effects.at(index).flowVelocity);
}

void AtmosphereParametersDialog::onAtmosphereEffectFlowVelocity(size_t index, const vec3& velocity)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).flowVelocity = delocalizeSpeed(velocity);

        onParameters(atmosphere);
    });
}

EmissiveColor AtmosphereParametersDialog::atmosphereEffectColorSource(size_t index)
{
    return parametersSource().effects.at(index).color;
}

void AtmosphereParametersDialog::onAtmosphereEffectColor(size_t index, const EmissiveColor& color)
{
    context()->controller()->edit([&]() -> void {
        AtmosphereParameters atmosphere = parametersSource();

        atmosphere.effects.at(index).color = color;

        onParameters(atmosphere);
    });
}
