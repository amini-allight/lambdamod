/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "input_interface.hpp"
#include "vr_input_paths.hpp"

#include <openxr/openxr.h>

class Controller;
class VR;

class VRInput final : public InputInterface
{
public:
    VRInput(Controller* controller, VR* vr);
    ~VRInput() override;

    bool step() override;

    void haptic(const HapticEffect& effect) override;

    bool hasGamepad() const override;
    bool hasTouch() const override;

private:
    Controller* controller;
    VR* vr;

    EyeTrackingPaths eyeTracking;
    GenericTrackingPaths genericTracking;
    HTCFullBodyTrackingPaths htcFullBodyTracking;
    GoogleDaydreamPaths googleDaydream;
    HTCVivePaths htcVive;
    HTCViveProPaths htcVivePro;
    HTCViveCosmosPaths htcViveCosmos;
    HTCViveFocus3Paths htcViveFocus3;
    HTCHandPaths htcHand;
    MicrosoftMixedRealityMotionPaths microsoftMixedRealityMotion;
    MicrosoftXboxPaths microsoftXbox;
    MicrosoftHandPaths microsoftHand;
    HPMixedRealityPaths hpMixedReality;
    HuaweiPaths huawei;
    OculusGoPaths oculusGo;
    OculusTouchPaths oculusTouch;
    ValveIndexPaths valveIndex;
    MagicLeap2Paths magicLeap2;
    PicoNeo3Paths picoNeo3;
    Pico4Paths pico4;
    PicoG3Paths picoG3;
    QuestTouchProPaths questTouchPro;
    QuestTouchPlusPaths questTouchPlus;
    OppoPaths oppo;
    YVRPaths yvr;
    VarjoXR4Paths varjoXR4;

    XrActionSet actionSet;

    XrAction heartrateAction;

    XrAction systemClickAction;
    XrAction volumeUpClickAction;
    XrAction volumeDownClickAction;
    XrAction muteMicrophoneClickAction;

    XrAction eyesAction;

    XrAction leftHandAction;
    XrAction rightHandAction;
    XrAction hipsAction;
    XrAction leftFootAction;
    XrAction rightFootAction;
    XrAction chestAction;
    XrAction leftShoulderAction;
    XrAction rightShoulderAction;
    XrAction leftElbowAction;
    XrAction rightElbowAction;
    XrAction leftWristAction;
    XrAction rightWristAction;
    XrAction leftKneeAction;
    XrAction rightKneeAction;
    XrAction leftAnkleAction;
    XrAction rightAnkleAction;

    XrAction leftSystemTouchAction;
    XrAction leftSystemClickAction;
    XrAction leftMenuTouchAction;
    XrAction leftMenuClickAction;
    XrAction leftATouchAction;
    XrAction leftAClickAction;
    XrAction leftBTouchAction;
    XrAction leftBClickAction;
    XrAction leftBumperTouchAction;
    XrAction leftBumperClickAction;
    XrAction leftThumbrestTouchAction;
    XrAction leftThumbrestSqueezeAction;

    XrAction leftTriggerTouchAction;
    XrAction leftTriggerSqueezeAction;
    XrAction leftTriggerClickAction;

    XrAction leftTouchpadXAction;
    XrAction leftTouchpadYAction;
    XrAction leftTouchpadTouchAction;
    XrAction leftTouchpadSqueezeAction;
    XrAction leftTouchpadClickAction;

    XrAction leftStickXAction;
    XrAction leftStickYAction;
    XrAction leftStickTouchAction;
    XrAction leftStickClickAction;

    XrAction leftSqueezeTouchAction;
    XrAction leftSqueezeAction;
    XrAction leftSqueezeClickAction;

    XrAction leftHandHapticAction;

    XrAction rightSystemTouchAction;
    XrAction rightSystemClickAction;
    XrAction rightMenuTouchAction;
    XrAction rightMenuClickAction;
    XrAction rightATouchAction;
    XrAction rightAClickAction;
    XrAction rightBTouchAction;
    XrAction rightBClickAction;
    XrAction rightBumperTouchAction;
    XrAction rightBumperClickAction;
    XrAction rightThumbrestTouchAction;
    XrAction rightThumbrestSqueezeAction;

    XrAction rightTriggerTouchAction;
    XrAction rightTriggerSqueezeAction;
    XrAction rightTriggerClickAction;

    XrAction rightTouchpadXAction;
    XrAction rightTouchpadYAction;
    XrAction rightTouchpadTouchAction;
    XrAction rightTouchpadSqueezeAction;
    XrAction rightTouchpadClickAction;

    XrAction rightStickXAction;
    XrAction rightStickYAction;
    XrAction rightStickTouchAction;
    XrAction rightStickClickAction;

    XrAction rightSqueezeTouchAction;
    XrAction rightSqueezeAction;
    XrAction rightSqueezeClickAction;

    XrAction rightHandHapticAction;

    XrAction gamepadDPadLeftClickAction;
    XrAction gamepadDPadRightClickAction;
    XrAction gamepadDPadUpClickAction;
    XrAction gamepadDPadDownClickAction;

    XrAction gamepadAClickAction;
    XrAction gamepadBClickAction;
    XrAction gamepadXClickAction;
    XrAction gamepadYClickAction;

    XrAction gamepadBackClickAction;
    XrAction gamepadStartClickAction;

    XrAction gamepadLeftStickXAction;
    XrAction gamepadLeftStickYAction;
    XrAction gamepadLeftStickClickAction;
    XrAction gamepadLeftBumperClickAction;
    XrAction gamepadLeftTriggerSqueezeAction;

    XrAction gamepadRightStickXAction;
    XrAction gamepadRightStickYAction;
    XrAction gamepadRightStickClickAction;
    XrAction gamepadRightBumperClickAction;
    XrAction gamepadRightTriggerSqueezeAction;

    XrSpace eyesSpace;

    XrSpace headSpace;
    XrSpace leftHandSpace;
    XrSpace rightHandSpace;
    XrSpace hipsSpace;
    XrSpace leftFootSpace;
    XrSpace rightFootSpace;
    XrSpace chestSpace;
    XrSpace leftShoulderSpace;
    XrSpace rightShoulderSpace;
    XrSpace leftElbowSpace;
    XrSpace rightElbowSpace;
    XrSpace leftWristSpace;
    XrSpace rightWristSpace;
    XrSpace leftKneeSpace;
    XrSpace rightKneeSpace;
    XrSpace leftAnkleSpace;
    XrSpace rightAnkleSpace;

#if defined(XR_EXT_hand_tracking)
    XrHandTrackerEXT leftHandTracker;
    XrHandTrackerEXT rightHandTracker;
#endif

#if defined(XR_HTC_facial_tracking)
    XrFacialTrackerHTC htcFacialEyeTracker;
    XrFacialTrackerHTC htcFacialLipTracker;
#endif

#if defined(XR_HTC_body_tracking)
    XrBodyTrackerHTC htcBodyTracker;
#endif

#if defined(XR_FB_face_tracking)
    XrFaceTrackerFB metaFaceTracker;
#endif
#if defined(XR_FB_face_tracking2)
    XrFaceTracker2FB metaFaceTracker2;
#endif
#if defined(XR_FB_body_tracking)
    XrBodyTrackerFB metaBodyTracker;
#endif
#if defined(XR_BD_body_tracking)
    XrBodyTrackerBD bdBodyTracker;
#endif

    vec2 lastLeftTouchpad;
    vec2 lastLeftStick;
    vec2 lastRightTouchpad;
    vec2 lastRightStick;

    u32 width;
    u32 height;

    void updatePose(const XrAction& action, const XrSpace& space, const function<void(Input&)>& block);
    void updateSpacePose(const XrSpace& space, const function<void(Input&)>& block);
    void updateHandPose(
        const XrHandJointLocationEXT& location,
        const XrHandJointVelocityEXT& velocity,
        const function<void(Input&)>& block
    );
    void updateFacialPose(InputType type, const vec3& position);
    void updateFacialPose(InputType type, f64 intensity);
    void updateFacialPose(InputType type, const vec3& position, f64 intensity);
    void updateFacialPose(InputType type, InputDimensions dimensions);

    void whenChanged(const XrAction& action, const function<void(Input&)>& block);
    void whenChanged(const XrAction& action, const function<void(Input&, f64)>& block);

    void checkRoomBounds();
    void checkNormalInputs();
#if defined(XR_EXT_hand_tracking)
    void checkHandTracking();
#endif
#if defined(XR_HTC_facial_tracking)
    void checkHTCFaceTracking();
#endif
#if defined(XR_HTC_body_tracking)
    void checkHTCBodyTracking();
#endif
#if defined(XR_FB_face_tracking)
    void checkMetaFaceTracking();
#endif
#if defined(XR_FB_face_tracking2)
    void checkMetaFaceTracking2();
#endif
#if defined(XR_FB_body_tracking)
    void checkMetaBodyTracking();
#endif
#if defined(XR_BD_body_tracking)
    void checkBDBodyTracking();
#endif
    void checkResize();

    void createPaths();
    void createEyeTrackingPaths();
    void createGenericTrackingPaths();
    void createHTCFullBodyTrackingPaths();
    void createGoogleDaydreamPaths();
    void createHTCVivePaths();
    void createHTCViveProPaths();
    void createHTCViveCosmosPaths();
    void createHTCViveFocus3Paths();
    void createHTCHandPaths();
    void createMicrosoftMixedRealityMotionPaths();
    void createMicrosoftXboxPaths();
    void createMicrosoftHandPaths();
    void createHPMixedRealityPaths();
    void createHuaweiPaths();
    void createOculusGoPaths();
    void createOculusTouchPaths();
    void createValveIndexPaths();
    void createMagicLeap2Paths();
    void createPicoNeo3Paths();
    void createPico4Paths();
    void createPicoG3Paths();
    void createQuestTouchProPaths();
    void createQuestTouchPlusPaths();
    void createOppoPaths();
    void createYVRPaths();
    void createVarjoXR4Paths();

    void createActions();
    void createAction(const string& name, XrActionType type, XrAction* action);

    void createActionSpaces();

    void suggestBindings();
    void suggestEyeTrackingBindings();
    void suggestGenericTrackingBindings();
    void suggestHTCFullBodyTrackingBindings();
    void suggestGoogleDaydreamBindings();
    void suggestHTCViveBindings();
    void suggestHTCViveProBindings();
    void suggestHTCViveCosmosBindings();
    void suggestHTCViveFocus3Bindings();
    void suggestHTCHandBindings();
    void suggestMicrosoftMixedRealityMotionBindings();
    void suggestMicrosoftXboxBindings();
    void suggestMicrosoftHandBindings();
    void suggestHPMixedRealityBindings();
    void suggestHuaweiBindings();
    void suggestOculusGoBindings();
    void suggestOculusTouchBindings();
    void suggestValveIndexBindings();
    void suggestMagicLeap2Bindings();
    void suggestPicoNeo3Bindings();
    void suggestPico4Bindings();
    void suggestPicoG3Bindings();
    void suggestQuestTouchProBindings();
    void suggestQuestTouchPlusBindings();
    void suggestOppoBindings();
    void suggestYVRBindings();
    void suggestVarjoXR4Bindings();
};

#endif
