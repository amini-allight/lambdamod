/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_occlusion_mesh.hpp"
#include "body.hpp"

class RenderHardwareOcclusionScene;

class RenderHardwareOcclusionEntity
{
public:
    RenderHardwareOcclusionEntity(
        RenderHardwareOcclusionScene* scene,
        VkCommandBuffer commandBuffer,
        const RenderOcclusionMesh& mesh,
        const Body& body
    );
    RenderHardwareOcclusionEntity(const RenderHardwareOcclusionEntity& rhs) = delete;
    RenderHardwareOcclusionEntity(RenderHardwareOcclusionEntity&& rhs) = delete;
    ~RenderHardwareOcclusionEntity();

    RenderHardwareOcclusionEntity& operator=(const RenderHardwareOcclusionEntity& rhs) = delete;
    RenderHardwareOcclusionEntity& operator=(RenderHardwareOcclusionEntity&& rhs) = delete;

    VkDeviceAddress address() const;

    // Needs update includes needs reinit, check reinit first
    bool needsUpdate(const RenderOcclusionMesh& mesh, const Body& body) const;
    bool needsReinit(const RenderOcclusionMesh& mesh) const;
    void update(VkCommandBuffer commandBuffer, const RenderOcclusionMesh& mesh, const Body& body);

private:
    RenderHardwareOcclusionScene* scene;
    PFN_vkCreateAccelerationStructureKHR vkCreateAccelerationStructureKHR;
    PFN_vkDestroyAccelerationStructureKHR vkDestroyAccelerationStructureKHR;
    PFN_vkGetAccelerationStructureBuildSizesKHR vkGetAccelerationStructureBuildSizesKHR;
    PFN_vkGetAccelerationStructureDeviceAddressKHR vkGetAccelerationStructureDeviceAddressKHR;
    PFN_vkCmdBuildAccelerationStructuresKHR vkCmdBuildAccelerationStructuresKHR;
    PFN_vkGetBufferDeviceAddressKHR vkGetBufferDeviceAddressKHR;

    VmaBuffer vertexBuffer;
    VkDeviceAddress vertexBufferAddress;
    VmaMapping<fvec3>* vertexBufferMapping;
    VmaBuffer indexBuffer;
    VmaMapping<u32>* indexBufferMapping;
    VkDeviceAddress indexBufferAddress;

    VmaBuffer backingBuffer;
    VmaBuffer scratchBuffer;
    VkDeviceAddress scratchBufferAddress;
    VkAccelerationStructureKHR accelerationStructure;
    VkDeviceAddress accelerationStructureAddress;

    u32 vertexCount;
    u32 triangleCount;
    bool initialized;

    Body lastBody;

    void build(VkCommandBuffer commandBuffer, const RenderOcclusionMesh& mesh);

    tuple<VmaBuffer, VkDeviceAddress> createVertexBuffer(u32 vertexCount);
    tuple<VmaBuffer, VkDeviceAddress> createIndexBuffer(u32 triangleCount);
    tuple<VmaBuffer, VkDeviceAddress> createTransformBuffer();
    VmaBuffer createAccelerationStructureBuffer(const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo) const;
    tuple<VmaBuffer, VkDeviceAddress> createScratchBuffer(const VkAccelerationStructureBuildSizesInfoKHR& buildSizesInfo) const;
    tuple<VmaBuffer, VkDeviceAddress> createBufferAndAddress(
        size_t size,
        VkBufferUsageFlags bufferUsage,
        VmaMemoryUsage memoryUsage,
        VkDeviceSize alignment = 0
    ) const;
};
