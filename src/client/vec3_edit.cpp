/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "vec3_edit.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "row.hpp"
#include "float_edit.hpp"

Vec3Edit::Vec3Edit(
    InterfaceWidget* parent,
    const function<vec3()>& source,
    const function<void(const vec3&)>& onSet
)
    : InterfaceWidget(parent)
{
    auto row = new Row(this);

    x = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().x;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            vec3 v = source();

            v.x = value;

            onSet(v);
        }
    );
    x->setPlaceholder(localize("vec3-edit-x"));

    y = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().y;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            vec3 v = source();

            v.y = value;

            onSet(v);
        }
    );
    y->setPlaceholder(localize("vec3-edit-y"));

    z = new FloatEdit(
        row,
        source ? [=]() -> f64
        {
            return source().z;
        } : function<f64()>(nullptr),
        [=](f64 value) -> void
        {
            vec3 v = source();

            v.z = value;

            onSet(v);
        }
    );
    z->setPlaceholder(localize("vec3-edit-z"));
}

void Vec3Edit::set(const vec3& value)
{
    x->set(value.x);
    y->set(value.y);
    z->set(value.z);
}

vec3 Vec3Edit::value() const
{
    return {
        x->value(),
        y->value(),
        z->value()
    };
}

void Vec3Edit::clear()
{
    x->clear();
    y->clear();
    z->clear();
}

SizeProperties Vec3Edit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
