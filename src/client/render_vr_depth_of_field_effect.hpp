/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "render_vr_post_process_effect.hpp"

class RenderVRDepthOfFieldEffect final : public RenderVRPostProcessEffect
{
public:
    RenderVRDepthOfFieldEffect(
        const RenderContext* ctx,
        RenderVRPipelineStore* pipelineStore,
        vector<VRSwapchainElement*> swapchainElements,
        f64 range
    );
    ~RenderVRDepthOfFieldEffect();

    void work(const VRSwapchainElement* swapchainElement, f32 timer) const override;
    void refresh(vector<VRSwapchainElement*> swapchainElements) override;
    void update(f64 range);

private:
    f32 range;
    VmaBuffer storageBuffer;

    VmaBuffer createUniformBuffer() const override;
    void fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const override;

    void setDescriptorSet(
        VkDescriptorSet descriptorSet,
        VkBuffer uniformBuffer,
        VkImageView colorImageView,
        VkImageView depthImageView,
        VkBuffer storageBuffer
    ) const;

    VmaBuffer createStorageBuffer() const;
};
#endif
