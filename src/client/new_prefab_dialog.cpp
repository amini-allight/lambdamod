/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "new_prefab_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "row.hpp"
#include "label.hpp"
#include "list_view.hpp"
#include "text_button.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(256, 384) * uiScale();
}

NewPrefabDialog::NewPrefabDialog(InterfaceView* view)
    : Dialog(view, dialogSize())
{
    vector<string> prefabs = {
        localize("new-prefab-dialog-human")
    }; // NOTE: Update this when adding new prefabs

    auto column = new Column(this);

    new Label(column, localize("new-prefab-dialog-prefabs"));

    auto listView = new ListView(
        column,
        [](size_t index) -> void {},
        theme.outdentColor
    );

    for (const string& prefab : prefabs)
    {
        auto row = new Row(listView);

        new TextButton(
            row,
            prefab,
            [this, prefab]() -> void
            {
                context()->viewerMode()->editMode()->addNewPrefab(prefab);
                destroy();
            }
        );
    }
}
