/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_scope.hpp"

class InterfaceWidget;

class FlatPanelInputScope : public InputScope
{
public:
    FlatPanelInputScope(
        InterfaceWidget* widget,
        InputScope* parent,
        const string& name,
        InputScopeMode mode,
        const function<optional<Point>(const Point&)>& toLocal,
        const function<void(InputScope*)>& block
    );

    bool movePointer(PointerType type, const Point& pointer, bool consumed) const override;

private:
    InterfaceWidget* widget;
    function<optional<Point>(const Point&)> toLocal;

    bool isTargeted(const Input& input) const;
};

#define START_FLAT_PANEL_SCOPE(_parent, _name) \
setScope<FlatPanelInputScope>(this, _parent, _name, Input_Scope_Normal, bind(static_cast<optional<Point> (std::decay_t<decltype(*this)>::*)(const Point&) const>(&std::decay_t<decltype(*this)>::toLocal), this, placeholders::_1), [&](InputScope* parent) -> void {
