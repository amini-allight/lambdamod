/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "events.hpp"
#include "interface_window.hpp"
#include "interface_marker.hpp"
#include "interface_oneshot.hpp"
#include "interface_log_oneshot.hpp"
#include "interface_view.hpp"

#include "console.hpp"
#include "help.hpp"
#include "document_manager.hpp"
#include "settings.hpp"
#include "system_settings.hpp"
#include "new_text_tool.hpp"
#include "user_controls_tool.hpp"
#include "oracle_manager.hpp"
#include "quick_accessor.hpp"
#include "entity_editor.hpp"

#include "main_menu.hpp"
#include "startup_splash.hpp"
#include "gamepad_attached_menu.hpp"
#include "gamepad_viewer_menu.hpp"
#include "text_titlecard.hpp"
#include "text_panel.hpp"
#include "vr_keyboard.hpp"
#include "splash.hpp"
#include "system_hud.hpp"
#include "hud.hpp"
#include "vr_attached_menu.hpp"
#include "vr_viewer_menu.hpp"
#include "knowledge_viewer.hpp"
#include "connecting_message.hpp"
#include "disconnected_message.hpp"
#include "loading_message.hpp"
#include "save_indicator.hpp"
#include "virtual_touch_gamepad.hpp"

#include <vulkan/vulkan.h>

class Interface
{
public:
    Interface(ControlContext* context);
    Interface(const Interface& rhs) = delete;
    Interface(Interface&& rhs) = delete;
    ~Interface();

    Interface& operator=(const Interface& rhs) = delete;
    Interface& operator=(Interface&& rhs) = delete;

    ControlContext* context() const;

    void movePointer(const Point& pointer);
    void focus();
    void unfocus();
    void resize(u32 width, u32 height);

    void step();

    InterfaceView* getView(const string& name) const;

    void setMarker(
        u32 id,
        Icon type,
        const string& name,
        const vec3& position,
        const vec3& color
    );
    void clearMarker(u32 id);

    void ping(const User* user);
    void brake(const User* user);
    void log(const Message& message);

    template<typename T, typename... Args>
    void addWindow(Args... args)
    {
        auto window = new T(getView("windows"), args...);
        window->open(pointer);

        updateWindowFocus();
    }
    template<typename T>
    T* getWindow() const
    {
        InterfaceView* windows = getView("windows");

        if (!windows)
        {
            return nullptr;
        }

        for (InterfaceWidget* window : windows->children())
        {
            if (auto result = dynamic_cast<T*>(window))
            {
                return result;
            }
        }

        return nullptr;
    }
    Console* console() const;
    Help* help() const;
    DocumentManager* documentManager() const;
    Settings* settings() const;
    SystemSettings* systemSettings() const;
    NewTextTool* newTextTool() const;
    UserControlsTool* userControlsTool() const;
    OracleManager* oracleManager() const;
    QuickAccessor* quickAccessor() const;
    EntityEditor* editor(EntityID id) const;

    // API for use by render system
    vector<VkCommandBuffer> panelBuffers() const;
    VkCommandBuffer uiBuffer() const;

    f64 stepInterval() const;

    void updateWindowFocus();

private:
    ControlContext* ctx;

    Point pointer;

    i32 width;
    i32 height;

    optional<EntityID> hoveredEntity;

    map<u32, InterfaceMarker> markers;
    vector<InterfaceOneshot> oneshots;
    vector<InterfaceLogOneshot> logOneshots;

    map<string, InterfaceView*> views;

    VkCommandPool commandPool;
    vector<VkCommandBuffer> transferCommandBuffers;
    vector<VkCommandBuffer> textTitlecardCommandBuffers;
    vector<VkCommandBuffer> textCommandBuffers;
#ifdef LMOD_VR
    vector<VkCommandBuffer> hudCommandBuffers;
    vector<VkCommandBuffer> keyboardCommandBuffers;
#endif
    vector<VkCommandBuffer> screenCommandBuffers;

    f64 _stepInterval;
    chrono::milliseconds lastStepTime;

    void addOneshot(
        Icon type,
        const string& name,
        const vec3& position,
        const vec3& color,
        f64 duration
    );

    void addLogOneshot(const Message& message);

    void drawTextTitlecard(
        VkCommandBuffer commandBuffer,
        VkCommandBuffer transferCommandBuffer
    );
    void drawText(
        VkCommandBuffer commandBuffer,
        VkCommandBuffer transferCommandBuffer
    );
#ifdef LMOD_VR
    void drawHUD(
        VkCommandBuffer commandBuffer,
        VkCommandBuffer transferCommandBuffer
    );
    void drawKeyboard(
        VkCommandBuffer commandBuffer,
        VkCommandBuffer transferCommandBuffer
    );
#endif
    void drawScreen(
        VkCommandBuffer commandBuffer,
        VkCommandBuffer transferCommandBuffer
    );

    void drawScreenWidgets(const DrawContext& drawCtx);

    void createCommandBuffers();
    void destroyCommandBuffers();

    void createViews(const Size& screenSize);
    void destroyViews();

    vector<InterfaceWindow*> windows() const;
};
