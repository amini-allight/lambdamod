/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "standalone_color_picker_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "tools.hpp"
#include "control_context.hpp"

static Size dialogSize()
{
    return Size(192, 384) * uiScale() + UIScale::dialogBordersSize();
}

StandaloneColorPickerDialog::StandaloneColorPickerDialog(
    InterfaceView* view,
    const Color& color,
    const function<void(const Color&)>& onDone
)
    : Dialog(view, dialogSize())
    , color(color)
    , onDone(onDone)
{
    START_WIDGET_SCOPE("standalone-color-picker-dialog")
        START_SCOPE("saturation-value-picker", contains(saturationValuePosition(), saturationValueSize(), pointer))
            WIDGET_SLOT("interact", setSaturationValue)
        END_SCOPE
        START_SCOPE("hue-picker", contains(huePosition(), hueSize(), pointer))
            WIDGET_SLOT("interact", setHue)
        END_SCOPE
    END_SCOPE

    lineEdit = new LineEdit(
        this,
        bind(&StandaloneColorPickerDialog::hexSource, this),
        bind(&StandaloneColorPickerDialog::onSetHex, this, placeholders::_1)
    );
    lineEdit->setPlaceholder(localize("standalone-color-picker-dialog-hex-color"));

    button = new TextButton(
        this,
        localize("standalone-color-picker-dialog-done"),
        bind(&StandaloneColorPickerDialog::onSubmit, this)
    );
}

void StandaloneColorPickerDialog::move(const Point& position)
{
    this->position(position - (this->size() / 2));

    lineEdit->move(this->position() + Point(UIScale::marginSize() * 2, size().h - (32 + 32 + (UIScale::marginSize() * 3))));

    button->move(this->position() + Point(UIScale::marginSize() * 2, size().h - (32 + (UIScale::marginSize() * 2))));
}

void StandaloneColorPickerDialog::resize(const Size& size)
{
    this->size(size - (UIScale::marginSize() * 2));

    lineEdit->resize(Size(this->size().w - (UIScale::marginSize() * 4), 32));

    button->resize(Size(this->size().w - (UIScale::marginSize() * 4), 32));
}

void StandaloneColorPickerDialog::step()
{
    Dialog::step();
}

void StandaloneColorPickerDialog::draw(const DrawContext& ctx) const
{
    Dialog::draw(ctx);

    drawSolid(
        ctx,
        this,
        Point(UIScale::marginSize() * 2),
        Size(size().w - (UIScale::marginSize() * 4), 32),
        color
    );

    f64 hue = rgbToHSV(color.toVec3()).x;

    drawGradient2D(
        ctx,
        this,
        saturationValuePosition(),
        saturationValueSize(),
        black,
        Color(hsvToRGB(vec3(hue, 1, 1)))
    );

    static constexpr Color hues[]{
        Color(255, 0, 0),
        Color(255, 255, 0),
        Color(0, 255, 0),
        Color(0, 255, 255),
        Color(0, 0, 255),
        Color(255, 0, 255),
        Color(255, 0, 0)
    };

    for (i32 i = 0; i < 6; i++)
    {
        drawGradient1D(
            ctx,
            this,
            huePosition() + Point(0, i * (hueSize().h / 6)),
            Size(hueSize().w, hueSize().h / 6),
            hues[i],
            hues[i + 1]
        );
    }

    drawSolid(
        ctx,
        this,
        Point(huePosition().x - (UIScale::marginSize() / 2), huePosition().y - (UIScale::hueSliderSize().h / 2)) + Point(0, hueSize().h * hue),
        UIScale::hueSliderSize(),
        theme.outdentColor
    );
}

string StandaloneColorPickerDialog::hexSource()
{
    return color.toString(false);
}

void StandaloneColorPickerDialog::onSetHex(const string& text)
{
    color = Color(text);
    color.a = 255;
}

void StandaloneColorPickerDialog::onSubmit()
{
    onDone(color);
    destroy();
}

Point StandaloneColorPickerDialog::huePosition() const
{
    return Point(size().w - (UIScale::hueBarWidth() + (UIScale::marginSize() * 2)), 32 + (UIScale::marginSize() * 3));
}

Size StandaloneColorPickerDialog::hueSize() const
{
    return Size(UIScale::hueBarWidth(), size().h - (32 + 32 + 32 + (UIScale::marginSize() * 7)));
}

Point StandaloneColorPickerDialog::saturationValuePosition() const
{
    return Point(UIScale::marginSize() * 2, 32 + (UIScale::marginSize() * 3));
}

Size StandaloneColorPickerDialog::saturationValueSize() const
{
    return Size(size().w - ((UIScale::marginSize() * 5) + UIScale::hueBarWidth()), size().h - (32 + 32 + 32 + (UIScale::marginSize() * 7)));
}

void StandaloneColorPickerDialog::setHue(const Input& input)
{
    Point local = pointer - position();

    local -= huePosition();

    f64 hue = static_cast<f64>(local.y) / hueSize().h;

    vec3 hsv = rgbToHSV(color.toVec3());

    hsv.x = hue;

    color = Color(hsvToRGB(hsv));
}

void StandaloneColorPickerDialog::setSaturationValue(const Input& input)
{
    Point local = pointer - position();

    local -= saturationValuePosition();

    f64 saturation = static_cast<f64>(local.x) / saturationValueSize().w;
    f64 value = 1 - (static_cast<f64>(local.y) / saturationValueSize().h);

    vec3 hsv = rgbToHSV(color.toVec3());

    hsv.y = saturation;
    hsv.z = value;

    color = Color(hsvToRGB(hsv));
}
