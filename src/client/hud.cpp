/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "global.hpp"

HUD::HUD(InterfaceView* view, const function<const map<string, HUDElement>&(void)>& source)
    : InterfaceWidget(view)
    , source(source)
{

}

HUD::HUD(InterfaceWidget* parent, const function<const map<string, HUDElement>&(void)>& source)
    : InterfaceWidget(parent)
    , source(source)
{

}

void HUD::step()
{
    elements = source();
}

void HUD::draw(const DrawContext& ctx) const
{
    for (const auto& [ name, element ] : elements)
    {
        drawElement(ctx, element);
    }
}

void HUD::drawLine(
    const DrawContext& ctx,
    const HUDElement& element
) const
{
    const HUDElementLine& line = element.get<HUDElementLine>();

    Point start = vec2(-(line.length * size().w) / 2, 0).rotate(line.rotation);
    Point end = vec2(+(line.length * size().w) / 2, 0).rotate(line.rotation);

    Size lineSize(
        start.x < end.x ? end.x - start.x : start.x - end.x,
        start.y < end.y ? end.y - start.y : start.y - end.y
    );

    Point topLeft = anchorToTopLeft(
        element.anchor(),
        size().toVec2(),
        element.position(),
        lineSize.toVec2()
    );

    ::drawLine(
        ctx,
        this,
        topLeft + (lineSize / 2) + start,
        topLeft + (lineSize / 2) + end,
        Color(element.color())
    );
}

void HUD::drawText(
    const DrawContext& ctx,
    const HUDElement& element
) const
{
    const HUDElementText& text = element.get<HUDElementText>();

    TextStyle style;
    style.font = Text_Sans_Serif;
    style.weight = Text_Bold;
    style.size = UIScale::hudFontSize(this);
    style.color = Color(element.color());

    Size textSize(
        style.getFont()->width(text.text) + (UIScale::hudMarginSize(this) * 2),
        style.getFont()->height() + (UIScale::hudMarginSize(this) * 2)
    );

    Point topLeft = anchorToTopLeft(
        element.anchor(),
        size().toVec2(),
        element.position(),
        textSize.toVec2()
    );

    ::drawText(
        ctx,
        this,
        topLeft,
        textSize,
        text.text,
        style
    );
}

void HUD::drawBar(
    const DrawContext& ctx,
    const HUDElement& element
) const
{
    const HUDElementBar& bar = element.get<HUDElementBar>();

    Size barSize(
        bar.length * size().w,
        UIScale::hudBarHeight(this)
    );

    Point topLeft = anchorToTopLeft(
        element.anchor(),
        size().toVec2(),
        element.position(),
        barSize.toVec2()
    );

    drawBorder(
        ctx,
        this,
        topLeft,
        barSize,
        UIScale::hudMarginSize(this),
        Color(element.color())
    );

    Size fillSize = barSize - Point(UIScale::hudMarginSize(this) * 2);

    drawSolid(
        ctx,
        this,
        topLeft + Point(UIScale::hudMarginSize(this)),
        Size(fillSize.w * bar.fraction, fillSize.h),
        Color(element.color())
    );
}

void HUD::drawSymbol(
    const DrawContext& ctx,
    const HUDElement& element
) const
{
    const HUDElementSymbol& symbol = element.get<HUDElementSymbol>();

    Size symbolSize(
        symbol.size * size().w,
        symbol.size * size().w
    );

    Point topLeft = anchorToTopLeft(
        element.anchor(),
        size().toVec2(),
        element.position(),
        symbolSize.toVec2()
    );

    ::drawSymbol(
        ctx,
        this,
        topLeft,
        symbolSize,
        symbol.type,
        Color(element.color())
    );
}

void HUD::drawElement(
    const DrawContext& ctx,
    const HUDElement& element
) const
{
    switch (element.type())
    {
    case HUD_Element_Line :
        drawLine(ctx, element);
        break;
    case HUD_Element_Text :
        drawText(ctx, element);
        break;
    case HUD_Element_Bar :
        drawBar(ctx, element);
        break;
    case HUD_Element_Symbol :
        drawSymbol(ctx, element);
        break;
    }
}

SizeProperties HUD::sizeProperties() const
{
    return sizePropertiesFillAll;
}
