/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_pipeline_store_types.hpp"
#include "render_objects.hpp"
#include "paths.hpp"
#include "tools.hpp"

RenderStoredDescriptorSetLayout::RenderStoredDescriptorSetLayout(
    const RenderContext* ctx,
    const function<void(VkDevice, VkDescriptorSetLayout&)>& constructor
)
    : ctx(ctx)
{
    constructor(ctx->device, descriptorSetLayout);
}

RenderStoredDescriptorSetLayout::RenderStoredDescriptorSetLayout(
    const RenderContext* ctx,
    const function<void(u32, VkPhysicalDevice, VkDevice, VkDescriptorSetLayout&)>& constructor
)
    : ctx(ctx)
{
    constructor(ctx->apiVersion, ctx->physDevice, ctx->device, descriptorSetLayout);
}

RenderStoredDescriptorSetLayout::~RenderStoredDescriptorSetLayout()
{
    destroyDescriptorSetLayout(ctx->device, descriptorSetLayout);
}

RenderStoredDescriptorSetLayout::operator VkDescriptorSetLayout() const
{
    return descriptorSetLayout;
}

RenderStoredShader::RenderStoredShader(const RenderContext* ctx, const string& name)
    : ctx(ctx)
{
    createShader(ctx->device, getFile(shaderPath + name + shaderExt), shader);
}

RenderStoredShader::~RenderStoredShader()
{
    destroyShader(ctx->device, shader);
}

RenderStoredShader::operator VkShaderModule() const
{
    return shader;
}

RenderStoredPipeline::RenderStoredPipeline(const RenderContext* ctx, const tuple<VkPipeline, VkPipelineLayout>& objects)
    : pipeline(get<0>(objects))
    , pipelineLayout(get<1>(objects))
    , ctx(ctx)
{

}

RenderStoredPipeline::~RenderStoredPipeline()
{
    destroyPipeline(ctx->device, pipeline, pipelineLayout);
}
