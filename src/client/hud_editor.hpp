/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

#include "item_list.hpp"
#include "hud_element_editor.hpp"

class HUDEditor : public InterfaceWidget, public TabContent
{
public:
    HUDEditor(InterfaceWidget* parent, EntityID id);

private:
    EntityID id;

    ItemList* elementList;
    HUDElementEditor* editor;

    vector<string> itemSource();
    void onAdd(const string& name);
    void onRemove(const string& name);
    void onSelect(const string& name);
    void onRename(const string& oldName, const string& newName);
    void onClone(const string& oldName, const string& newName);
    void onSelect(size_t index);

    void undo(const Input& input);
    void redo(const Input& input);

    SizeProperties sizeProperties() const override;
};
