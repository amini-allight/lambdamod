/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if !defined(__EMSCRIPTEN__)
#pragma once

#include "types.hpp"
#include "client.hpp"

#include <openssl/bio.h>
#include <openssl/ssl.h>

class ClientNative : public Client
{
public:
    ClientNative(
        const string& caFile,
        const function<void()>& connectCallback,
        const function<void(const NetworkEvent&)>& receiveCallback,
        const function<void()>& disconnectCallback
    );
    ~ClientNative() override;

    void step() override;

    virtual void holePunch(const Address& localAddress, const Address& remoteAddress) = 0;
    void send(const NetworkEvent& event) override;

protected:
    SSL_CTX* ctx;
    SSL* ssl;

    bool verify;
};
#endif
