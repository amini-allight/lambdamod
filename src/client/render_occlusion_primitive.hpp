/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "render_constants.hpp"
#include "body_render_data.hpp"

struct RenderOcclusionMesh;

enum OcclusionPrimitiveType : i32
{
    Occlusion_Primitive_Ellipse,
    Occlusion_Primitive_Equilateral_Triangle,
    Occlusion_Primitive_Isosceles_Triangle,
    Occlusion_Primitive_Right_Angle_Triangle,
    Occlusion_Primitive_Rectangle,
    Occlusion_Primitive_Pentagon,
    Occlusion_Primitive_Hexagon,
    Occlusion_Primitive_Heptagon,
    Occlusion_Primitive_Octagon,
    Occlusion_Primitive_Cuboid,
    Occlusion_Primitive_Sphere,
    Occlusion_Primitive_Cylinder,
    Occlusion_Primitive_Cone,
    Occlusion_Primitive_Capsule,
    Occlusion_Primitive_Pyramid,
    Occlusion_Primitive_Tetrahedron,
    Occlusion_Primitive_Octahedron,
    Occlusion_Primitive_Dodecahedron,
    Occlusion_Primitive_Icosahedron
};

#pragma pack(1)
struct OcclusionPrimitiveGPU
{
    fmat4 transform[eyeCount];
    f32 width;
    f32 length;
    f32 height;
    OcclusionPrimitiveType type;
};

struct OcclusionSceneGPU
{
    OcclusionPrimitiveGPU primitives[maxPrimitiveCount];
    i32 primitiveCount;
};
#pragma pack()

class RenderOcclusionPrimitive
{
public:
    RenderOcclusionPrimitive(const mat4& entityTransform, const BodyRenderOcclusionPrimitive& primitive);

    void update(const mat4& entityTransform);

    OcclusionPrimitiveGPU uniformData(const vec3& cameraPosition) const;
    OcclusionPrimitiveGPU uniformData(vec3 cameraPosition[eyeCount]) const;

    void toMesh(RenderOcclusionMesh& out) const;

private:
    mat4 entityTransform;
    mat4 transform;
    OcclusionPrimitiveGPU _uniformData;

    void generateEllipse(f32 xRadius, f32 yRadius, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateRegularPolygon(u32 sides, f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateIsoscelesTriangle(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateRightAngleTriangle(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateRectangle(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateCuboid(f32 width, f32 length, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateSphere(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateCylinder(f32 radius, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateCone(f32 radius, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateCapsule(f32 radius, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generatePyramid(f32 width, f32 height, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateTetrahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateOctahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateDodecahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const;
    void generateIcosahedron(f32 radius, vector<fvec3>& vertices, vector<u32>& indices) const;
};
