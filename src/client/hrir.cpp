/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hrir.hpp"
#include "tools.hpp"
#include "convolution.hpp"

HRIR::HRIR()
{

}

void HRIR::apply(vector<f32>& lo, vector<f32>& hi, i32 ear, const vec3& dir) const
{
    array<f32, sampleCount> hrir = at(ear, dir);
    hi = convolve(hi, { hrir.begin(), hrir.end() });
}

array<f32, HRIR::sampleCount> HRIR::at(i32 ear, const vec3& dir) const
{
    // Coordinate system from hrir_data_documentation.pdf in the CIPIC HRTF database
    f64 azimuth = -handedAngleBetween(forwardDir.toVec2(), dir.toVec2().normalize());
    f64 elevation = handedAngleBetween(vec2(1, 0), vec2(dir.y, dir.z).normalize());

    if (elevation < -pi / 2)
    {
        elevation = tau + elevation;
    }

    azimuth = degrees(azimuth);
    elevation = degrees(elevation);

    static constexpr f64 azimuths[azimuthCount] = {
        -80,
        -65,
        -55,
        -45,
        -40,
        -35,
        -30,
        -25,
        -20,
        -15,
        -10,
        -5,
        0,
        +5,
        +10,
        +15,
        +20,
        +25,
        +30,
        +35,
        +40,
        +45,
        +55,
        +65,
        +80
    };

    auto at = [&](i32 azimuthIndex, i32 elevationIndex) -> tuple<f32, array<f32, sampleCount>>
    {
        azimuthIndex = clamp(azimuthIndex, 0, azimuthCount - 1);

        if (elevationIndex == elevationCount)
        {
            elevationIndex = 0;
        }
        else if (elevationIndex < 0)
        {
            elevationIndex = elevationCount - 1;
        }

        f64 azimuth = radians(azimuths[azimuthIndex]);
        f64 elevation = radians(-45 + elevationIndex * 5.625);

        vec3 sampleDir = quaternion(rightDir, elevation).rotate(quaternion(upDir, azimuth).rotate(forwardDir));

        return { angleBetween(sampleDir, dir), data.at(ear).at(azimuthIndex).at(elevationIndex) };
    };

    i32 closestAzimuthIndex = 0;
    f64 closestAzimuth = numeric_limits<f64>::max();

    for (i32 i = 0; i < azimuthCount; i++)
    {
        f64 option = azimuths[i];
        f64 distance = abs(azimuth - option);

        if (option < azimuth && distance < closestAzimuth)
        {
            closestAzimuthIndex = i;
            closestAzimuth = distance;
        }
    }

    i32 closestElevationIndex = elevationCount - 1;
    f64 closestElevation = numeric_limits<f64>::max();

    for (i32 i = 0; i < elevationCount; i++)
    {
        f64 option = -45 + i * 5.625;
        f64 distance = abs(elevation - option);

        if (option < elevation && distance < closestElevation)
        {
            closestElevationIndex = i;
            closestElevation = distance;
        }
    }

    i32 azimuthDirection = azimuth < closestAzimuth ? -1 : +1;
    i32 elevationDirection = elevation < closestElevation ? -1 : +1;

    auto [ aDistance, aSamples ] = at(closestAzimuthIndex, closestElevationIndex);
    auto [ bDistance, bSamples ] = at(closestAzimuthIndex, closestElevationIndex + elevationDirection);
    auto [ cDistance, cSamples ] = at(closestAzimuthIndex + azimuthDirection, closestElevationIndex);
    auto [ dDistance, dSamples ] = at(closestAzimuthIndex + azimuthDirection, closestElevationIndex + elevationDirection);

    if (roughly<f32>(aDistance, 0))
    {
        return aSamples;
    }
    else if (roughly<f32>(bDistance, 0))
    {
        return bSamples;
    }
    else if (roughly<f32>(cDistance, 0))
    {
        return cSamples;
    }
    else if (roughly<f32>(dDistance, 0))
    {
        return dSamples;
    }
    else
    {
        // Shepard inverse distance weighting
        array<f32, sampleCount> result{0};

        f32 b = 0;

        b += 1 / sq(aDistance);
        b += 1 / sq(bDistance);
        b += 1 / sq(cDistance);
        b += 1 / sq(dDistance);

        for (i32 i = 0; i < sampleCount; i++)
        {
            f32 t = 0;

            t += aSamples[i] / sq(aDistance);
            t += bSamples[i] / sq(bDistance);
            t += cSamples[i] / sq(cDistance);
            t += dSamples[i] / sq(dDistance);

            result[i] = t / b;
        }

        return result;
    }
}

HRIR* loadHRIR(const string& path)
{
    FILE* file = fopen(path.c_str(), "rb");

    if (!file)
    {
        fatal("Failed to load HRIR '" + path + "'.");
    }

    HRIR* hrir = new HRIR();

    size_t result = fread(
        &hrir->data,
        sizeof(f32),
        HRIR::earCount * HRIR::azimuthCount * HRIR::elevationCount * HRIR::sampleCount,
        file
    );

    if (result != HRIR::earCount * HRIR::azimuthCount * HRIR::elevationCount * HRIR::sampleCount)
    {
        fatal("Failed to load HRIR '" + path + "'.");
    }

    fclose(file);

    return hrir;
}
