/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_scale.hpp"
#include "interface_constants.hpp"
#include "interface_widget.hpp"
#include "interface_panel_view.hpp"
#include "global.hpp"

f64 uiScale(const InterfaceWidget* widget)
{
    // Detect VR mode or panel usage
    if (g_windowHeight == 0 || (widget && dynamic_cast<const InterfacePanelView*>(widget->view())))
    {
        return 1;
    }
    else if (g_config.uiAutoScale)
    {
        return (g_windowHeight / static_cast<f64>(baseWindowHeight)) * g_config.uiScale;
    }
    else
    {
        return g_config.uiScale;
    }
}
