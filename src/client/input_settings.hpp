/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class InputSettings : public InterfaceWidget, public TabContent
{
public:
    InputSettings(InterfaceWidget* parent);

private:
    bool touchInputSource();
    void onTouchInput(bool state);

    bool invertMouseViewerXSource();
    void onInvertMouseViewerX(bool state);

    bool invertMouseViewerYSource();
    void onInvertMouseViewerY(bool state);

    vec2 mouseViewerSensitivitySource();
    void onMouseViewerSensitivity(const vec2& sensitivity);

    bool invertGamepadViewerXSource();
    void onInvertGamepadViewerX(bool state);

    bool invertGamepadViewerYSource();
    void onInvertGamepadViewerY(bool state);

    vec2 gamepadViewerSensitivitySource();
    void onGamepadViewerSensitivity(const vec2& sensitivity);

    vec2 gamepadLeftDeadZoneSource();
    void onGamepadLeftDeadZone(const vec2& deadZone);

    vec2 gamepadRightDeadZoneSource();
    void onGamepadRightDeadZone(const vec2& deadZone);

    SizeProperties sizeProperties() const override;
};
