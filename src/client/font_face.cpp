/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "font_face.hpp"
#include "log.hpp"

#include <ft2build.h>
#include FT_FREETYPE_H

static FT_Library fontLibrary = nullptr;
static constexpr i32 fontDPI = 64;

void fontInit()
{
    FT_Error result = FT_Init_FreeType(&fontLibrary);

    if (result)
    {
        fatal("Failed to initialize freetype2: " + to_string(result));
    }
}

void fontQuit()
{
    FT_Done_FreeType(fontLibrary);
}

FontFace::FontFace(void* face, u32 size)
{
    this->face = face;

    _monoWidth = width("a");
    _height = (static_cast<FT_Face>(face)->size->metrics.ascender - static_cast<FT_Face>(face)->size->metrics.descender) / fontDPI;
    softHeight = static_cast<FT_Face>(face)->size->metrics.ascender / fontDPI;

    if (_height == 0)
    {
        fatal("Font reported height of 0.");
    }
}

FontFace::~FontFace()
{
    FT_Done_Face(static_cast<FT_Face>(face));
}

Image* FontFace::render(const string& text, const Color& color) const
{
    u32 pixel = 0;
    pixel |= color.r << 16;
    pixel |= color.g << 8;
    pixel |= color.b << 0;

    i32 w = width(text);
    i32 h = height();
    i32 sh = softHeight;

    if (w == 0 || h == 0)
    {
        return emptyImage(sizeof(u32));
    }

    auto pixels = new u8[w * h * sizeof(u32)];
    memset(pixels, 0, w * h * sizeof(u32));

    u32 offset = 0;
    FT_GlyphSlot slot = static_cast<FT_Face>(face)->glyph;

    for (char c : text)
    {
        FT_Error result = FT_Load_Char(static_cast<FT_Face>(face), c, FT_LOAD_RENDER);

        if (result)
        {
            error("TTF render failed.");
            delete[] pixels;
            return nullptr;
        }

        auto inPixels = static_cast<const u8*>(slot->bitmap.buffer);
        auto outPixels = reinterpret_cast<u32*>(pixels);

        for (u32 y = 0; y < slot->bitmap.rows; y++)
        {
            for (u32 x = 0; x < slot->bitmap.width; x++)
            {
                u8 alpha = inPixels[(y * slot->bitmap.width) + x] * (color.a / 255.0);

                i32 oy = (sh - slot->bitmap_top) + y;
                i32 ox = slot->bitmap_left + x + offset;

                outPixels[(oy * w) + ox] = (static_cast<u32>(alpha) << 24) | pixel;
            }
        }

        offset += slot->advance.x / fontDPI;
    }

    return new Image(w, h, sizeof(u32), pixels);
}

i32 FontFace::width(const string& text) const
{
    i32 w = 0;

    FT_GlyphSlot slot = static_cast<FT_Face>(face)->glyph;

    for (char c : text)
    {
        FT_Error result = FT_Load_Char(static_cast<FT_Face>(face), c, FT_LOAD_DEFAULT);

        if (result)
        {
            error("TTF size measurement failed.");
            return w;
        }

        w += slot->advance.x / fontDPI;
    }
    
    return w;
}

i32 FontFace::monoWidth() const
{
    return _monoWidth;
}

i32 FontFace::height() const
{
    return _height;
}

FontFace* loadTTF(const string& path, u32 size)
{
    FT_Face face;

    FT_Error result = FT_New_Face(fontLibrary, path.c_str(), 0, &face);

    if (result)
    {
        error("Failed to open TTF file '" + path + "'.");
        return nullptr;
    }

    result = FT_Set_Pixel_Sizes(face, 0, size);

    if (result)
    {
        error("Failed to set size of font '" + path + "'.");
        FT_Done_Face(face);
        return nullptr;
    }

    return new FontFace(face, size);
}
