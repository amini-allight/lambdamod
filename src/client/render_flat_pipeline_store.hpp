/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_pipeline_store.hpp"

class RenderFlatPipelineStore final : public RenderPipelineStore
{
public:
    RenderFlatPipelineStore(
        const RenderContext* ctx,
        VkRenderPass skyRenderPass,
        VkRenderPass mainRenderPass,
        VkRenderPass hdrRenderPass,
        VkRenderPass postProcessRenderPass,
        VkRenderPass overlayRenderPass,
        VkRenderPass independentDepthOverlayRenderPass,
        VkSampleCountFlagBits sampleCount
    );
    ~RenderFlatPipelineStore();

    RenderEntityPipelines get(BodyRenderMaterialType type) const override;

    RenderStoredDescriptorSetLayout objectDescriptorSetLayout;
    RenderStoredDescriptorSetLayout litObjectDescriptorSetLayout;
    RenderStoredDescriptorSetLayout texturedObjectDescriptorSetLayout;
    RenderStoredDescriptorSetLayout litTexturedObjectDescriptorSetLayout;
    RenderStoredDescriptorSetLayout materialedObjectDescriptorSetLayout;
    RenderStoredDescriptorSetLayout litMaterialedObjectDescriptorSetLayout;
    RenderStoredDescriptorSetLayout areaDescriptorSetLayout;
    RenderStoredDescriptorSetLayout litAreaDescriptorSetLayout;
    RenderStoredDescriptorSetLayout panelDescriptorSetLayout;
    RenderStoredDescriptorSetLayout postProcessDescriptorSetLayout;
    RenderStoredDescriptorSetLayout skyDescriptorSetLayout;
    RenderStoredDescriptorSetLayout atmosphereDescriptorSetLayout;
    RenderStoredDescriptorSetLayout hdrDescriptorSetLayout;
    RenderStoredDescriptorSetLayout depthOfFieldDescriptorSetLayout;
    RenderStoredDescriptorSetLayout motionBlurDescriptorSetLayout;

    RenderStoredShader coloredVertexShader;
    RenderStoredShader coloredFragmentShader;
    RenderStoredShader litColoredVertexShader;
    RenderStoredShader litColoredFragmentShader;
    RenderStoredShader texturedVertexShader;
    RenderStoredShader texturedFragmentShader;
    RenderStoredShader litTexturedVertexShader;
    RenderStoredShader litTexturedFragmentShader;
    RenderStoredShader areaVertexShader;
    RenderStoredShader areaFragmentShader;
    RenderStoredShader litAreaVertexShader;
    RenderStoredShader litAreaFragmentShader;
    RenderStoredShader terrainVertexShader;
    RenderStoredShader terrainFragmentShader;
    RenderStoredShader litTerrainVertexShader;
    RenderStoredShader litTerrainFragmentShader;
    RenderStoredShader ropeVertexShader;
    RenderStoredShader ropeGeometryShader;
    RenderStoredShader ropeFragmentShader;
    RenderStoredShader litRopeVertexShader;
    RenderStoredShader litRopeGeometryShader;
    RenderStoredShader litRopeFragmentShader;
    RenderStoredShader coloredOverlayVertexShader;
    RenderStoredShader coloredOverlayFragmentShader;
    RenderStoredShader orthographicGridVertexShader;
    RenderStoredShader orthographicGridFragmentShader;
    RenderStoredShader panelVertexShader;
    RenderStoredShader panelFragmentShader;
    RenderStoredShader tetherIndicatorVertexShader;
    RenderStoredShader tetherIndicatorFragmentShader;
    RenderStoredShader confinementIndicatorVertexShader;
    RenderStoredShader confinementIndicatorFragmentShader;
    RenderStoredShader waitingIndicatorVertexShader;
    RenderStoredShader waitingIndicatorFragmentShader;
    RenderStoredShader themeGradientVertexShader;
    RenderStoredShader themeGradientFragmentShader;
    RenderStoredShader screenShader;
    RenderStoredShader hdrShader;
    RenderStoredShader fadeShader;
    RenderStoredShader motionBlurShader;
    RenderStoredShader chromaticAberrationShader;
    RenderStoredShader depthOfFieldShader;
    RenderStoredShader tintShader;
    RenderStoredShader skyVertexShader;
    RenderStoredShader skyFragmentShader;
    RenderStoredShader skyBillboardVertexShader;
    RenderStoredShader skyBillboardGeometryShader;
    RenderStoredShader skyStarsFragmentShader;
    RenderStoredShader skyCircleFragmentShader;
    RenderStoredShader skyCloudFragmentShader;
    RenderStoredShader skyAuroraVertexShader;
    RenderStoredShader skyAuroraGeometryShader;
    RenderStoredShader skyAuroraFragmentShader;
    RenderStoredShader skyCometVertexShader;
    RenderStoredShader skyCometGeometryShader;
    RenderStoredShader skyCometFragmentShader;
    RenderStoredShader skyVortexVertexShader;
    RenderStoredShader skyVortexGeometryShader;
    RenderStoredShader skyVortexFragmentShader;
    RenderStoredShader skyEnvMapVertexShader;
    RenderStoredShader skyEnvMapFragmentShader;
    RenderStoredShader skyEnvMapBillboardVertexShader;
    RenderStoredShader skyEnvMapBillboardGeometryShader;
    RenderStoredShader skyEnvMapStarsFragmentShader;
    RenderStoredShader skyEnvMapCircleFragmentShader;
    RenderStoredShader skyEnvMapCloudFragmentShader;
    RenderStoredShader skyEnvMapAuroraVertexShader;
    RenderStoredShader skyEnvMapAuroraGeometryShader;
    RenderStoredShader skyEnvMapAuroraFragmentShader;
    RenderStoredShader skyEnvMapCometVertexShader;
    RenderStoredShader skyEnvMapCometGeometryShader;
    RenderStoredShader skyEnvMapCometFragmentShader;
    RenderStoredShader skyEnvMapVortexVertexShader;
    RenderStoredShader skyEnvMapVortexGeometryShader;
    RenderStoredShader skyEnvMapVortexFragmentShader;
    RenderStoredShader atmosphereLightningVertexShader;
    RenderStoredShader atmosphereLightningFragmentShader;
    RenderStoredShader atmosphereLineVertexShader;
    RenderStoredShader atmosphereLineGeometryShader;
    RenderStoredShader atmosphereLineFragmentShader;
    RenderStoredShader atmosphereSolidVertexShader;
    RenderStoredShader atmosphereSolidGeometryShader;
    RenderStoredShader atmosphereSolidFragmentShader;
    RenderStoredShader atmosphereCloudVertexShader;
    RenderStoredShader atmosphereCloudGeometryShader;
    RenderStoredShader atmosphereCloudFragmentShader;
    RenderStoredShader atmosphereStreamerVertexShader;
    RenderStoredShader atmosphereStreamerGeometryShader;
    RenderStoredShader atmosphereStreamerFragmentShader;
    RenderStoredShader atmosphereWarpVertexShader;
    RenderStoredShader atmosphereWarpGeometryShader;
    RenderStoredShader atmosphereWarpFragmentShader;

    RenderStoredPipeline panel;
    RenderStoredPipeline vertexColor;
    RenderStoredPipeline litVertexColor;
    RenderStoredPipeline texturedColor;
    RenderStoredPipeline litTexturedColor;
    RenderStoredPipeline area;
    RenderStoredPipeline litArea;
    RenderStoredPipeline terrain;
    RenderStoredPipeline litTerrain;
    RenderStoredPipeline rope;
    RenderStoredPipeline litRope;

    RenderStoredPipeline orthographicGrid;
    RenderStoredPipeline coloredLine;
    RenderStoredPipeline gizmo;
    RenderStoredPipeline tetherIndicator;
    RenderStoredPipeline confinementIndicator;
    RenderStoredPipeline waitingIndicator;
    RenderStoredPipeline perspectiveGrid;
    RenderStoredPipeline themeGradient;

    RenderStoredPipeline hdr;
    RenderStoredPipeline fade;
    RenderStoredPipeline motionBlur;
    RenderStoredPipeline chromaticAberration;
    RenderStoredPipeline depthOfField;
    RenderStoredPipeline tint;

    RenderStoredPipeline sky;
    RenderStoredPipeline skyStars;
    RenderStoredPipeline skyCircle;
    RenderStoredPipeline skyCloud;
    RenderStoredPipeline skyAurora;
    RenderStoredPipeline skyComet;
    RenderStoredPipeline skyVortex;

    RenderStoredPipeline skyEnvMap;
    RenderStoredPipeline skyEnvMapStars;
    RenderStoredPipeline skyEnvMapCircle;
    RenderStoredPipeline skyEnvMapCloud;
    RenderStoredPipeline skyEnvMapAurora;
    RenderStoredPipeline skyEnvMapComet;
    RenderStoredPipeline skyEnvMapVortex;

    RenderStoredPipeline atmosphereLightning;
    RenderStoredPipeline atmosphereLine;
    RenderStoredPipeline atmosphereSolid;
    RenderStoredPipeline atmosphereCloud;
    RenderStoredPipeline atmosphereStreamer;
    RenderStoredPipeline atmosphereWarp;
};
