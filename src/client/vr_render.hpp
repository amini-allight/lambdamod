/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "render_interface.hpp"
#include "render_context.hpp"
#include "render_deletion_task.hpp"
#include "render_vr_sky.hpp"
#include "render_vr_atmosphere.hpp"
#include "render_vr_pipeline_store.hpp"
#include "render_vr_entity.hpp"
#include "render_vr_decoration.hpp"
#include "render_vr_post_process_effect.hpp"
#include "render_light.hpp"

#include <openxr/openxr.h>

class Controller;

class VRRender final : public RenderInterface
{
public:
    VRRender(Controller* controller, RenderContext* ctx);
    ~VRRender() override;

    void waitIdle() const override;

    void step() override;

    tuple<u32, u32> reinit(u32 width, u32 height) override;

    void setCameraSpace(const quaternion& space) override;

    void add(EntityID parentID, const Entity* entity) override;
    void remove(EntityID parentID, EntityID id) override;
    void update(EntityID parentID, const Entity* entity) override;

    template<typename T, typename... Args>
    RenderDecorationID addDecoration(Args... args)
    {
        auto decoration = new T(
            ctx,
            swapchainElements,
            pipelineStore->objectDescriptorSetLayout,
            pipelineStore,
            args...
        );

        RenderDecorationID id = nextDecorationID++;

        decorations.insert({ id, decoration });

        return id;
    }
    void removeDecoration(RenderDecorationID id) override;
    template<typename T, typename... Args>
    void updateDecoration(RenderDecorationID id, Args... args)
    {
        auto it = decorations.find(id);

        if (it == decorations.end())
        {
            return;
        }

        RenderVRDecoration* previous = it->second;

        decorationsToDelete.push_back(RenderDeletionTask(previous));

        decorations.erase(it);

        auto decoration = new T(
            ctx,
            swapchainElements,
            pipelineStore->objectDescriptorSetLayout,
            pipelineStore,
            args...
        );

        decorations.insert({ id, decoration });
    }
    void setDecorationShown(RenderDecorationID id, bool state) override;

    template<typename T, typename... Args>
    RenderPostProcessEffectID addPostProcessEffect(Args... args)
    {
        auto effect = new T(
            ctx,
            pipelineStore,
            swapchainElements,
            args...
        );

        RenderPostProcessEffectID id = nextPostProcessEffectID++;

        postProcessEffects.insert({ id, effect });

        return id;
    }
    void removePostProcessEffect(RenderPostProcessEffectID id) override;
    template<typename T, typename... Args>
    void updatePostProcessEffect(RenderPostProcessEffectID id, Args... args)
    {
        auto it = postProcessEffects.find(id);

        if (it == postProcessEffects.end())
        {
            return;
        }

        dynamic_cast<T*>(it->second)->update(args...);
    }
    void setPostProcessEffectActive(RenderPostProcessEffectID id, bool state) override;
    void setPostProcessEffectOrder(const vector<RenderPostProcessEffectID>& order) override;

    RenderContext* context() const override;
    u32 imageCount() const override;
    u32 currentImage() const override;
    VkRenderPass panelRenderPass() const override;
    VkFramebuffer textTitlecardFramebuffer() const override;
    VkFramebuffer textFramebuffer() const override;
    VkFramebuffer hudFramebuffer() const;
    VkFramebuffer keyboardFramebuffer() const;

    f32 timer() const override;

    bool rayTracingSupported() const override;
    bool rayTracingEnabled() const override;

    f32 fogDistance() const override;

    mat4 worldSpaceEyeTransform() const;
    tuple<u32, u32> size() const;
    f64 angle() const;

    void showKeyboard(bool state);
    void setKeyboard(const vec3& position, const quaternion& rotation);

private:
    Controller* controller;

    RenderContext* ctx;

    VkRenderPass _panelRenderPass;
    VkRenderPass skyRenderPass;
    VkRenderPass mainRenderPass;
    VkRenderPass hdrRenderPass;
    VkRenderPass postProcessRenderPass;
    VkRenderPass overlayRenderPass;
    VkRenderPass independentDepthOverlayRenderPass;

    RenderVRPipelineStore* pipelineStore;

    XrSwapchain swapchain;
    vector<VRSwapchainElement*> swapchainElements;

    VmaBuffer hudVertexBuffer;
    u32 imageIndex;

    RenderVRPostProcessEffect* hdr;

    RenderVRSky* sky;
    RenderVRAtmosphere* atmosphere;

    RenderVRPostProcessEffect* steamvrColorAdjustment;

    map<EntityID, RenderVREntity*> entities;
    vector<RenderDeletionTask<RenderVREntity>> entitiesToDelete;

    RenderDecorationID nextDecorationID;
    map<RenderDecorationID, RenderVRDecoration*> decorations;
    vector<RenderDeletionTask<RenderVRDecoration>> decorationsToDelete;

    RenderPostProcessEffectID nextPostProcessEffectID;
    map<RenderPostProcessEffectID, RenderVRPostProcessEffect*> postProcessEffects;
    vector<RenderDeletionTask<RenderVRPostProcessEffect>> postProcessEffectsToDelete;
    vector<RenderPostProcessEffectID> postProcessOrder;

    u32 width;
    u32 height;
    f64 _angle;
    VkFormat format;
    VkSampleCountFlagBits sampleCount;

    vec3 cameraPosition[eyeCount];
    quaternion cameraSpace;

    // Used to tell the server where the camera now is
    vec3 worldSpaceEyePosition;
    quaternion worldSpaceEyeRotation;

    bool keyboard;
    vec3 keyboardPosition;
    quaternion keyboardRotation;

    f32 exposure;
    chrono::microseconds lastWriteTime;

    f32 _timer;
    bool _rayTracing;

    u32 pickAPIVersion() const;
    set<string> instanceExtensions() const;
    VkPhysicalDevice pickPhysicalDevice();
    set<string> deviceExtensions() const;
    u32 prepareSwapchain();

    void updateCameraUniforms(const vector<XrView>& eyeViews);
    void updateTextTitlecardUniforms(const vec3& position, const vec3& direction);
    void updateTextUniforms(const vec3& position, const vec3& direction);
    void updateHUDUniforms(const vec3& position);
    void updateKeyboardUniforms();

    void render(VRSwapchainElement* swapchainElement);
    void renderTextTitlecard(VkCommandBuffer commandBuffer);
    void renderText(VkCommandBuffer commandBuffer);
    void renderHUD(VkCommandBuffer commandBuffer);
    void renderKeyboard(VkCommandBuffer commandBuffer);
    void renderSky(const VRSwapchainElement* swapchainElement);

    void outputImage(const VRSwapchainElement* swapchainElement);

    void applySteamVRColorAdjustment(const VRSwapchainElement* swapchainElement);

    void advanceDeletionQueues();

    void createSwapchain();
    void destroySwapchain();
    void createSwapchainElements();
    void destroySwapchainElements();

    vec3 averageCameraPosition() const;
    fmat4 projectionMatrix(XrView eyeView) const;
    fmat4 worldViewMatrix(XrView eyeView) const;
    fmat4 roomViewMatrix(XrView eyeView) const;

    void computeLuminance(VRSwapchainElement* element) const;
    f32 getLuminance(const VRSwapchainElement* element) const;
    void adjustExposure(const chrono::microseconds& elapsed, f32 luminance);
};
#endif
