/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_scope_binding.hpp"
#include "input_scope_slot.hpp"

class ControlContext;

class InputHandlingResultEntry
{
public:
    InputHandlingResultEntry(const vector<string>& path, const InputScopeBinding& binding, const InputScopeSlot& slot, size_t depth);

    bool operator<(const InputHandlingResultEntry& rhs) const;
    bool operator>(const InputHandlingResultEntry& rhs) const;

    void commit(ControlContext* ctx, const Input& input) const;
    bool wantsInclusion(const InputHandlingResultEntry& previous) const;

private:
    vector<string> path;
    InputScopeBinding binding;
    InputScopeSlot slot;
    size_t depth;

    bool isAbove(const InputHandlingResultEntry& previous) const;
    bool isBelow(const InputHandlingResultEntry& previous) const;
};
