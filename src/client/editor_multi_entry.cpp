/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "editor_multi_entry.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "column.hpp"
#include "row.hpp"
#include "column.hpp"
#include "row.hpp"
#include "spacer.hpp"

EditorMultiEntry::EditorMultiEntry(InterfaceWidget* parent, size_t count, const function<void(InterfaceWidget*, size_t)>& body)
    : InterfaceWidget(parent)
{
    auto column = new Column(this);

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto row = new Row(column);
    MAKE_SPACER(row, UIScale::marginSize() * 2, 0);
    for (size_t i = 0; i < count; i++)
    {
        body(row, i);

        if (i + 1 != count)
        {
            MAKE_SPACER(row, UIScale::marginSize() * 4, 0);
        }
    }
    MAKE_SPACER(row, UIScale::marginSize() * 2, 0);

    MAKE_SPACER(column, 0, UIScale::marginSize());
}

SizeProperties EditorMultiEntry::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::labelHeight() + UIScale::marginSize() * 2),
        Scaling_Fill, Scaling_Fixed
    };
}
