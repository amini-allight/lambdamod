/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "popup.hpp"
#include "line_edit.hpp"
#include "row.hpp"

class AlphaColorPickerPopup : public Popup
{
public:
    AlphaColorPickerPopup(
        InterfaceWidget* parent,
        const Color& color,
        const function<void(const Color&)>& onSet
    );

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void draw(const DrawContext& ctx) const override;

private:
    const Color& color;
    function<void(const Color&)> onSet;

    f64 hueSource();
    void onHueSet(f64 hue);
    void onSaturationValueSet(const vec2& saturationValue);

    string textSource();
    void onTextReturn(const string& text);

    f64 alphaSource();
    void onAlpha(f64 alpha);

    SizeProperties sizeProperties() const override;
};

class AlphaColorPicker : public InterfaceWidget
{
public:
    AlphaColorPicker(
        InterfaceWidget* parent,
        const function<Color()>& source,
        const function<void(const Color&)>& onSet
    );
    ~AlphaColorPicker();

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;
    void focus() override;
    void unfocus() override;

private:
    Color color;

    AlphaColorPickerPopup* popup;

    function<Color()> source;
    function<void(const Color&)> _onSet;

    void onSet(const Color& color);

    void interact(const Input& input);

    SizeProperties sizeProperties() const override;
};
