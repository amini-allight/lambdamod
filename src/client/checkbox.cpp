/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "checkbox.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

Checkbox::Checkbox(
    InterfaceWidget* parent,
    const function<bool()>& source,
    const function<void(bool)>& onSet
)
    : InterfaceWidget(parent)
    , state(source ? source() : false)
    , source(source)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("checkbox")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void Checkbox::step()
{
    InterfaceWidget::step();

    if (source)
    {
        state = source();
    }
}

void Checkbox::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        Point(UIScale::marginSize()),
        UIScale::checkboxSize() - (UIScale::marginSize() * 2),
        theme.indentColor
    );

    drawBorder(
        ctx,
        this,
        Point(UIScale::marginSize()),
        UIScale::checkboxSize() - (UIScale::marginSize() * 2),
        UIScale::marginSize(),
        focused() && hovered() ? theme.accentColor : theme.outdentColor
    );

    if (state)
    {
        drawImage(
            ctx,
            this,
            Point(UIScale::marginSize() * 2),
            UIScale::checkboxSize() - (UIScale::marginSize() * 4),
            Icon_Checked
        );
    }
}

void Checkbox::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void Checkbox::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void Checkbox::set(bool state)
{
    this->state = state;
}

bool Checkbox::checked() const
{
    return state;
}

bool Checkbox::hovered() const
{
    return contains(Point(UIScale::marginSize()), UIScale::checkboxSize() - (UIScale::marginSize() * 2), pointer);
}

void Checkbox::interact(const Input& input)
{
    state ? playNegativeActivateEffect() : playPositiveActivateEffect();
    state = !state;
    onSet(state);
}

SizeProperties Checkbox::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::checkboxSize().w), static_cast<f64>(UIScale::checkboxSize().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
