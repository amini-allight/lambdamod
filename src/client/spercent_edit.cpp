/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "spercent_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "float_edit.hpp"
#include "tools.hpp"

SPercentEdit::SPercentEdit(
    InterfaceWidget* parent,
    const function<f64()>& source,
    const function<void(const f64&)>& onSet
)
    : InterfaceWidget(parent)
{
    f = new FloatEdit(
        this,
        source ? [source]() -> f64
        {
            return clamp<f64>(source(), -1, +1) * 100;
        } : function<f64()>(nullptr),
        [onSet](f64 x) -> void
        {
            onSet(x / 100);
        }
    );
}

void SPercentEdit::set(f64 value)
{
    f->set(clamp<f64>(value, -1, +1) * 100);
}

f64 SPercentEdit::value() const
{
    return f->value() / 100;
}

void SPercentEdit::clear()
{
    f->clear();
}

void SPercentEdit::setPlaceholder(const string& text)
{
    f->setPlaceholder(text);
}

SizeProperties SPercentEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
