/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_decoration_component.hpp"
#include "render_tools.hpp"
#include "render_descriptor_set_write_components.hpp"

RenderDecorationComponent::RenderDecorationComponent(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    const VmaBuffer& camera,
    const VmaBuffer& object
)
    : uniform(object)
    , ctx(ctx)
{
    uniformMapping = new VmaMapping<f32>(ctx->allocator, uniform);

    descriptorSet = createDescriptorSet(descriptorSetLayout, camera, object);
}

RenderDecorationComponent::~RenderDecorationComponent()
{
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSet);

    delete uniformMapping;

    destroyBuffer(ctx->allocator, uniform);
}

VkDescriptorSet RenderDecorationComponent::createDescriptorSet(
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera,
    VmaBuffer object
) const
{
    VkDescriptorSet descriptorSet = ::createDescriptorSet(
        ctx->device,
        ctx->descriptorPool,
        descriptorSetLayout
    );

    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, uniform.buffer, &objectWriteBuffer, &objectWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    return descriptorSet;
}
