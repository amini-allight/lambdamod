/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "gamepad_magnitude_tool.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "attached_mode_context.hpp"
#include "control_context.hpp"

GamepadMagnitudeTool::GamepadMagnitudeTool(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    START_WIDGET_SCOPE("gamepad-magnitude-tool")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void GamepadMagnitudeTool::draw(const DrawContext& ctx) const
{
    TextStyle style(this);
    style.weight = Text_Bold;

    drawText(
        ctx,
        this,
        Point(),
        Size(size().w / 2, UIScale::panelGamepadMagnitudeToolHeight(this)),
        localize("gamepad-magnitude-tool-magnitude"),
        style
    );

    style.alignment = Text_Center;

    for (size_t i = 0; i < 4; i++)
    {
        i32 x = (size().w / 2) + (size().w / 8) * i;

        Point point(x, 0);
        Size size(this->size().w / 8, UIScale::panelGamepadMagnitudeToolHeight(this));

        bool focused = contains(point, size, pointer);

        style.color = focused ? theme.accentColor : theme.panelForegroundColor;

        drawText(
            ctx,
            this,
            point,
            size,
            to_string(i),
            style
        );

        const User* self = context()->controller()->self();

        if (i == (self ? self->magnitude() : 0))
        {
            drawBorder(
                ctx,
                this,
                point,
                size,
                UIScale::panelBorderSize(this),
                focused ? theme.accentColor : theme.panelForegroundColor
            );
        }
    }
}

void GamepadMagnitudeTool::interact(const Input& input)
{
    Point local = pointer - position();

    if (local.x < size().w / 2)
    {
        return;
    }

    i32 x = local.x - (size().w / 2);

    i32 i = x / (size().w / 8);

    context()->setMagnitude(i);
}

SizeProperties GamepadMagnitudeTool::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::panelGamepadMagnitudeToolHeight(this)),
        Scaling_Fill, Scaling_Fixed
    };
}
