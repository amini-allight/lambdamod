/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "unit_field.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

UnitField::UnitField(
    InterfaceWidget* parent,
    const string& unit,
    const function<void(InterfaceWidget*)>& body
)
    : InterfaceWidget(parent)
    , unit(unit)
{
    body(this);
}

void UnitField::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        child->move(position);
    }
}

void UnitField::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->resize(size - Size(UIScale::unitIndicatorWidth(), 0));
    }
}

void UnitField::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

    drawSolid(
        ctx,
        this,
        Point(size().w - UIScale::unitIndicatorWidth(), 0),
        Size(UIScale::unitIndicatorWidth(), UIScale::lineEditHeight()),
        theme.softIndentColor
    );

    TextStyle style;
    style.alignment = Text_Center;
    style.color = theme.inactiveTextColor;

    drawText(
        ctx,
        this,
        Point(size().w - UIScale::unitIndicatorWidth(), 0),
        Size(UIScale::unitIndicatorWidth(), UIScale::lineEditHeight()),
        unit,
        style
    );
}

SizeProperties UnitField::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
