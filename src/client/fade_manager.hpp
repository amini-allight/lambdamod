/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

class ControlContext;

class FadeManager
{
public:
    FadeManager(ControlContext* ctx);

    void step();

    void fadeOut();
    void fadeIn();

    RenderPostProcessEffectID renderPostProcessID() const;
    SoundPostProcessEffectID soundPostProcessID() const;

private:
    ControlContext* ctx;

    bool fading;
    chrono::milliseconds startTime;

    RenderPostProcessEffectID _renderPostProcessID;
    SoundPostProcessEffectID _soundPostProcessID;
};
