/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "log.hpp"

#include <openxr/openxr.h>

inline string openxrError(XrResult result)
{
    switch (result)
    {
    default : return "unknown error " + to_string(result) + ".";
    case XR_TIMEOUT_EXPIRED : return "timeout expired.";
    case XR_SESSION_LOSS_PENDING : return "session loss pending.";
    case XR_EVENT_UNAVAILABLE : return "event unavailable.";
    case XR_SPACE_BOUNDS_UNAVAILABLE : return "space bounds unavailable.";
    case XR_SESSION_NOT_FOCUSED : return "session not focused.";
    case XR_FRAME_DISCARDED : return "frame discarded.";
    case XR_ERROR_VALIDATION_FAILURE : return "validation failure.";
    case XR_ERROR_RUNTIME_FAILURE : return "runtime failure.";
    case XR_ERROR_OUT_OF_MEMORY : return "out of memory.";
    case XR_ERROR_API_VERSION_UNSUPPORTED : return "API version unsupported.";
    case XR_ERROR_INITIALIZATION_FAILED : return "initialization failed.";
    case XR_ERROR_FUNCTION_UNSUPPORTED : return "function unsupported.";
    case XR_ERROR_FEATURE_UNSUPPORTED : return "feature unsupported.";
    case XR_ERROR_EXTENSION_NOT_PRESENT : return "extension not present.";
    case XR_ERROR_LIMIT_REACHED : return "limit reached.";
    case XR_ERROR_SIZE_INSUFFICIENT : return "size insufficient.";
    case XR_ERROR_HANDLE_INVALID : return "handle invalid.";
    case XR_ERROR_INSTANCE_LOST : return "instance lost.";
    case XR_ERROR_SESSION_RUNNING : return "session running.";
    case XR_ERROR_SESSION_NOT_RUNNING : return "session not running.";
    case XR_ERROR_SESSION_LOST : return "session lost.";
    case XR_ERROR_SYSTEM_INVALID : return "system invalid.";
    case XR_ERROR_PATH_INVALID : return "path invalid.";
    case XR_ERROR_PATH_COUNT_EXCEEDED : return "path count exceeded.";
    case XR_ERROR_PATH_FORMAT_INVALID : return "path format invalid.";
    case XR_ERROR_PATH_UNSUPPORTED : return "path unsupported.";
    case XR_ERROR_LAYER_INVALID : return "layer invalid.";
    case XR_ERROR_LAYER_LIMIT_EXCEEDED : return "layer limit exceeded.";
    case XR_ERROR_SWAPCHAIN_RECT_INVALID : return "swapchain rect invalid.";
    case XR_ERROR_SWAPCHAIN_FORMAT_UNSUPPORTED : return "swapchain format unsupported.";
    case XR_ERROR_ACTION_TYPE_MISMATCH : return "action type mismatch.";
    case XR_ERROR_SESSION_NOT_READY : return "session not ready.";
    case XR_ERROR_SESSION_NOT_STOPPING : return "session not stopping.";
    case XR_ERROR_TIME_INVALID : return "time invalid.";
    case XR_ERROR_REFERENCE_SPACE_UNSUPPORTED : return "reference space unsupported.";
    case XR_ERROR_FILE_ACCESS_ERROR : return "file access error.";
    case XR_ERROR_FILE_CONTENTS_INVALID : return "file contents invalid.";
    case XR_ERROR_FORM_FACTOR_UNSUPPORTED : return "form factor unsupported.";
    case XR_ERROR_FORM_FACTOR_UNAVAILABLE : return "form factor unavailable.";
    case XR_ERROR_API_LAYER_NOT_PRESENT : return "API layer not present.";
    case XR_ERROR_CALL_ORDER_INVALID : return "call order invalid.";
    case XR_ERROR_GRAPHICS_DEVICE_INVALID : return "graphics device invalid.";
    case XR_ERROR_POSE_INVALID : return "pose invalid.";
    case XR_ERROR_INDEX_OUT_OF_RANGE : return "index out of range.";
    case XR_ERROR_VIEW_CONFIGURATION_TYPE_UNSUPPORTED : return "view configure type unsupported.";
    case XR_ERROR_ENVIRONMENT_BLEND_MODE_UNSUPPORTED : return "environment blend mode unsupported.";
    case XR_ERROR_NAME_DUPLICATED : return "name duplicated.";
    case XR_ERROR_NAME_INVALID : return "name invalid.";
    case XR_ERROR_ACTIONSET_NOT_ATTACHED : return "actionset not attached.";
    case XR_ERROR_ACTIONSETS_ALREADY_ATTACHED : return "actionset already attached.";
    case XR_ERROR_LOCALIZED_NAME_DUPLICATED : return "localized name duplicated.";
    case XR_ERROR_LOCALIZED_NAME_INVALID : return "localized name invalid.";
    case XR_ERROR_GRAPHICS_REQUIREMENTS_CALL_MISSING : return "graphics requirements call missing.";
    case XR_ERROR_ANDROID_THREAD_SETTINGS_ID_INVALID_KHR : return "android thread settings id invalid.";
    case XR_ERROR_ANDROID_THREAD_SETTINGS_FAILURE_KHR : return "android thread settings failure.";
    case XR_ERROR_CREATE_SPATIAL_ANCHOR_FAILED_MSFT : return "create spatial anchor failed.";
    case XR_ERROR_SECONDARY_VIEW_CONFIGURATION_TYPE_NOT_ENABLED_MSFT : return "secondary view configuratin type not enabled.";
    case XR_ERROR_CONTROLLER_MODEL_KEY_INVALID_MSFT : return "controller model key invalid.";
    case XR_ERROR_DISPLAY_REFRESH_RATE_UNSUPPORTED_FB : return "display refresh rate unsupported.";
    case XR_ERROR_COLOR_SPACE_UNSUPPORTED_FB : return "color space unsupported.";
    }
}

#ifdef LMOD_XR_VALIDATION
inline XrBool32 handleError(
    XrDebugUtilsMessageSeverityFlagsEXT severity,
    XrDebugUtilsMessageTypeFlagsEXT type,
    const XrDebugUtilsMessengerCallbackDataEXT* callbackData,
    void* userData
)
{
    string sType;

    switch (type)
    {
    case XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT :
        sType = "general";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT :
        sType = "validation";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT :
        sType = "performance";
        break;
    case XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT :
        sType = "conformance";
        break;
    }

    switch (severity)
    {
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT :
        log("OpenXR " + sType + ": " + string(callbackData->message));
        break;
    default :
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT :
        log("OpenXR " + sType + ": " + string(callbackData->message));
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT :
        warning("OpenXR " + sType + ": " + string(callbackData->message));
        break;
    case XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT :
        error("OpenXR " + sType + ": " + string(callbackData->message));
        break;
    }

    return XR_FALSE;
}
#endif

template<size_t outSize>
void fillBuffer(char out[outSize], const char* in)
{
    out[outSize - 1] = 0;

    size_t inSize = strlen(in) + 1;

    memcpy(out, in, inSize < outSize - 1 ? inSize : outSize - 1);
}

inline XrPath makePath(XrInstance instance, const string& s)
{
    XrPath path;

    XrResult result = xrStringToPath(instance, s.c_str(), &path);

    if (result != XR_SUCCESS)
    {
        error("Failed to create path: " + openxrError(result));
    }

    return path;
}

template<typename T>
T getXRExtensionFunction(XrInstance instance, const char* name)
{
    T f;

    XrResult result = xrGetInstanceProcAddr(instance, name, reinterpret_cast<PFN_xrVoidFunction*>(&f));

    if (result != XR_SUCCESS)
    {
        fatal("Failed to get OpenXR extension function '" + std::string(name) + "': " + openxrError(result));
    }

    return f;
}

#define GET_XR_EXTENSION_FUNCTION(_instance, _name) getXRExtensionFunction<PFN_##_name>(_instance, #_name)
