/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "controller.hpp"

class AttachedModeContext
{
public:
    AttachedModeContext(ControlContext* context);

    void onAttach(
        const map<InputType, InputBinding>& bindings,
        const map<string, HUDElement>& hudElements,
        f64 fieldOfView,
        bool mouseLocked,
        f64 voiceVolume,
        const vec3& tintColor,
        f64 tintStrength,
        bool hot
    );
    void onDetach();

    ControlContext* context() const;

    void step();

    void detach();
    void ping();
    void brake();

#ifdef LMOD_VR
    f64 vrScale() const;
    void scalePlayerUp();
    void scalePlayerDown();
    void recenter();
    void orientRoom();
    void resetRoom();

    bool calibratingTracking() const;
    void enableTrackingCalibrator();
    void disableTrackingCalibrator();
    void setTrackingCalibration();
    void clearTrackingCalibration();
#endif

    bool knowledgeViewer() const;
    void toggleKnowledgeViewer();
    bool gamepadMenu() const;
    void toggleGamepadMenu();

    const map<InputType, InputBinding>& bindings() const;
    const map<string, HUDElement>& hudElements() const;
    bool mouseLocked() const;

private:
    ControlContext* _context;

#ifdef LMOD_VR
    bool _calibratingTracking;
#endif
    bool _knowledgeViewer;
    bool _gamepadMenu;

    map<InputType, InputBinding> _bindings;
    map<string, HUDElement> _hudElements;
    bool _mouseLocked;
};
