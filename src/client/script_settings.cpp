/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_settings.hpp"
#include "global.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "uint_edit.hpp"
#include "ufloat_edit.hpp"

ScriptSettings::ScriptSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "script-settings-max-recursion-depth", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&ScriptSettings::maxRecursionDepthSource, this),
            bind(&ScriptSettings::onMaxRecursionDepth, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "script-settings-max-memory-usage", bytesSuffix(), [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&ScriptSettings::maxMemoryUsageSource, this),
            bind(&ScriptSettings::onMaxMemoryUsage, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "script-settings-max-loop-duration", timeSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&ScriptSettings::maxLoopDurationSource, this),
            bind(&ScriptSettings::onMaxLoopDuration, this, placeholders::_1)
        );
    });
}

u64 ScriptSettings::maxRecursionDepthSource()
{
    return g_config.maxRecursionDepth;
}

void ScriptSettings::onMaxRecursionDepth(u64 depth)
{
    g_config.maxRecursionDepth = depth;

    saveConfig(g_config);
}

u64 ScriptSettings::maxMemoryUsageSource()
{
    return g_config.maxMemoryUsage;
}

void ScriptSettings::onMaxMemoryUsage(u64 usage)
{
    g_config.maxMemoryUsage = usage;

    saveConfig(g_config);
}

f64 ScriptSettings::maxLoopDurationSource()
{
    return g_config.maxLoopDuration;
}

void ScriptSettings::onMaxLoopDuration(f64 duration)
{
    g_config.maxLoopDuration = duration;

    saveConfig(g_config);
}

SizeProperties ScriptSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
