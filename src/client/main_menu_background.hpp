/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "render_types.hpp"

class Controller;

// This exists because this effect looks terrible without anti-aliasing but anti-aliasing the UI means we need two copies of every UI shader and a significant video memory cost

class MainMenuBackgroundFrame
{
public:
    MainMenuBackgroundFrame(
        VkDevice device,
        VmaAllocator allocator,
        VkDescriptorPool descriptorPool,
        VkRenderPass renderPass,
        VkDescriptorSetLayout descriptorSetLayout,
        VkSampler sampler,
        u32 width,
        u32 height,
        u32 vertexCount
    );
    MainMenuBackgroundFrame(const MainMenuBackgroundFrame& rhs) = delete;
    MainMenuBackgroundFrame(MainMenuBackgroundFrame&& rhs) = delete;
    ~MainMenuBackgroundFrame();

    MainMenuBackgroundFrame& operator=(const MainMenuBackgroundFrame& rhs) = delete;
    MainMenuBackgroundFrame& operator=(MainMenuBackgroundFrame&& rhs) = delete;

    void setVertices(const vector<fvec3>& vertices);

    VmaImage image;
    VkImageView imageView;
    VmaImage resolveImage;
    VkImageView resolveImageView;
    VkFramebuffer framebuffer;
    VmaBuffer vertices;
    VkDescriptorSet descriptorSet;

private:
    VkDevice device;
    VmaAllocator allocator;
    VkDescriptorPool descriptorPool;
};

class MainMenuBackground
{
public:
    MainMenuBackground(Controller* controller, const Size& size);
    MainMenuBackground(const MainMenuBackground& rhs) = delete;
    MainMenuBackground(MainMenuBackground&& rhs) = delete;
    ~MainMenuBackground();

    MainMenuBackground& operator=(const MainMenuBackground& rhs) = delete;
    MainMenuBackground& operator=(MainMenuBackground&& rhs) = delete;

    void resize(const Size& size);
    void step();
    void draw(const DrawContext& ctx) const;

private:
    Controller* controller;
    VkDevice device;
    VmaAllocator allocator;
    u32 thickLineOffset;
    u32 thickLineVertexCount;
    u32 thinLineOffset;
    u32 thinLineVertexCount;
    VkRenderPass renderPass;
    VkShaderModule vertexShader;
    VkShaderModule fragmentShader;
    VkPipelineLayout pipelineLayout;
    VkPipeline pipeline;
    VkShaderModule copyVertexShader;
    VkShaderModule copyFragmentShader;
    VkDescriptorSetLayout copyDescriptorSetLayout;
    VkPipelineLayout copyPipelineLayout;
    VkPipeline copyPipeline;
    vector<MainMenuBackgroundFrame*> frames;
    chrono::milliseconds startTime;

    void drawOffscreen(const DrawContext& ctx) const;

    vector<fvec3> generateVertices(f32 timer);

    void createFrames(u32 width, u32 height);
    void destroyFrames();
};
