/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_occlusion_primitive.hpp"
#include "render_occlusion_scene_reference.hpp"
#include "render_context.hpp"

class RenderFlatEntity;
#ifdef LMOD_VR
class RenderVREntity;
#endif

class RenderSoftwareOcclusionScene
{
public:
    RenderSoftwareOcclusionScene(const RenderContext* ctx);
    RenderSoftwareOcclusionScene(const RenderSoftwareOcclusionScene& rhs) = delete;
    RenderSoftwareOcclusionScene(RenderSoftwareOcclusionScene&& rhs) = delete;
    ~RenderSoftwareOcclusionScene();

    RenderSoftwareOcclusionScene& operator=(const RenderSoftwareOcclusionScene& rhs) = delete;
    RenderSoftwareOcclusionScene& operator=(RenderSoftwareOcclusionScene&& rhs) = delete;

    void update(const vec3& cameraPosition, const map<EntityID, RenderFlatEntity*>& entities);
#ifdef LMOD_VR
    void update(vec3 cameraPosition[eyeCount], const map<EntityID, RenderVREntity*>& entities);
#endif

    RenderOcclusionSceneReference get() const;

private:
    const RenderContext* ctx;
    VmaBuffer buffer;
    VmaMapping<OcclusionSceneGPU>* bufferMapping;

    void recordPrimitives(
        const vec3& cameraPosition,
        const map<EntityID, RenderFlatEntity*>& entities,
        vector<OcclusionPrimitiveGPU>& primitives
    );
#ifdef LMOD_VR
    void recordPrimitives(
        vec3 cameraPosition[eyeCount],
        const map<EntityID, RenderVREntity*>& entities,
        vector<OcclusionPrimitiveGPU>& primitives
    );
#endif

    void createPrimitiveBuffer();
    void destroyPrimitiveBuffer();
    void populatePrimitiveBuffer(const vector<OcclusionPrimitiveGPU>& primitives);
};
