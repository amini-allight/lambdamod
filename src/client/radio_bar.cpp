/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "radio_bar.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

RadioBar::RadioBar(
    InterfaceWidget* parent,
    const vector<string>& options,
    const function<size_t()>& source,
    const function<void(size_t)>& onSet
)
    : InterfaceWidget(parent)
    , index(source ? source() : 0)
    , options(options)
    , source(source)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("radio-bar")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void RadioBar::step()
{
    InterfaceWidget::step();

    if (source)
    {
        index = source();
    }
}

void RadioBar::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

    for (size_t i = 0; i < options.size(); i++)
    {
        const string& option = options[i];

        i32 w = size().w / options.size();
        i32 x = i * w;

        drawSolid(
            ctx,
            this,
            Point(x, 0),
            Size(w, size().h),
            i == index ? theme.accentColor : theme.outdentColor
        );

        TextStyle style;
        style.alignment = Text_Center;

        drawText(
            ctx,
            this,
            Point(x, 0),
            Size(w, size().h),
            option,
            style
        );
    }
}

void RadioBar::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void RadioBar::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

size_t RadioBar::activeIndex() const
{
    return index;
}

void RadioBar::interact(const Input& input)
{
    playPositiveActivateEffect();
    index = (pointer.x - position().x) / (size().w / options.size());
    onSet(index);
}

SizeProperties RadioBar::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::radioBarHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
