/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_swapchain_elements.hpp"
#include "global.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_descriptor_set_write_components.hpp"

SwapchainElement::SwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height
)
    : ctx(ctx)
    , _imageIndex(imageIndex)
    , _width(width)
    , _height(height)
{

}

SwapchainElement::~SwapchainElement()
{

}

u32 SwapchainElement::imageIndex() const
{
    return _imageIndex;
}

u32 SwapchainElement::width() const
{
    return _width;
}

u32 SwapchainElement::height() const
{
    return _height;
}

VmaImage SwapchainElement::createImage(
    u32 width,
    u32 height,
    VkFormat format,
    VkSampleCountFlagBits sampleCount,
    VkImageUsageFlags usage,
    u32 mipLevels,
    u32 layers
)
{
    VkResult result;

    VkImageCreateInfo imageCreateInfo{};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.extent = { width, height, 1 };
    imageCreateInfo.mipLevels = mipLevels;
    imageCreateInfo.arrayLayers = layers;
    imageCreateInfo.format = format;
    imageCreateInfo.samples = sampleCount;
    imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.usage = usage;

    VmaAllocationCreateInfo imageAllocateInfo{};
    imageAllocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    VmaImage image;

    result = vmaCreateImage(
        ctx->allocator,
        &imageCreateInfo,
        &imageAllocateInfo,
        &image.image,
        &image.allocation,
        nullptr
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create image: " + vulkanError(result));
    }

    return image;
}

VkImageView SwapchainElement::createImageView(
    VkFormat format,
    VkImage image,
    VkImageAspectFlags aspect,
    VkImageViewType type,
    u32 mipLevels,
    u32 layers
)
{
    VkResult result;

    VkImageViewCreateInfo imageViewCreateInfo{};
    imageViewCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    imageViewCreateInfo.image = image;
    imageViewCreateInfo.format = format;
    imageViewCreateInfo.viewType = type;
    imageViewCreateInfo.components.r = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.g = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.b = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.components.a = VK_COMPONENT_SWIZZLE_IDENTITY;
    imageViewCreateInfo.subresourceRange.aspectMask = aspect;
    imageViewCreateInfo.subresourceRange.baseMipLevel = 0;
    imageViewCreateInfo.subresourceRange.levelCount = mipLevels;
    imageViewCreateInfo.subresourceRange.baseArrayLayer = 0;
    imageViewCreateInfo.subresourceRange.layerCount = layers;

    VkImageView imageView;

    result = vkCreateImageView(ctx->device, &imageViewCreateInfo, nullptr, &imageView);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create image view: " + vulkanError(result));
    }

    return imageView;
}

VkFramebuffer SwapchainElement::createFramebuffer(
    u32 width,
    u32 height,
    VkRenderPass renderPass,
    const vector<VkImageView>& imageViews,
    u32 layers
)
{
    VkResult result;

    VkFramebufferCreateInfo framebufferCreateInfo{};
    framebufferCreateInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    framebufferCreateInfo.flags = 0;
    framebufferCreateInfo.renderPass = renderPass;
    framebufferCreateInfo.attachmentCount = imageViews.size();
    framebufferCreateInfo.pAttachments = imageViews.data();
    framebufferCreateInfo.width = width;
    framebufferCreateInfo.height = height;
    framebufferCreateInfo.layers = layers;

    VkFramebuffer framebuffer;

    result = vkCreateFramebuffer(ctx->device, &framebufferCreateInfo, nullptr, &framebuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create framebuffer: " + vulkanError(result));
    }

    return framebuffer;
}

VkCommandBuffer SwapchainElement::createCommandBuffer()
{
    VkResult result;

    VkCommandBufferAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    allocateInfo.commandPool = ctx->commandPool;
    allocateInfo.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    allocateInfo.commandBufferCount = 1;

    VkCommandBuffer commandBuffer;

    result = vkAllocateCommandBuffers(ctx->device, &allocateInfo, &commandBuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to allocate command buffer: " + vulkanError(result));
    }

    return commandBuffer;
}

VkDescriptorSet SwapchainElement::createDescriptorSet(
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera,
    VmaBuffer object,
    VkImageView imageView
)
{
    VkDescriptorSet descriptorSet = ::createDescriptorSet(
        ctx->device,
        ctx->descriptorPool,
        descriptorSetLayout
    );

    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, object.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorImageInfo textureWriteImage{};
    VkWriteDescriptorSet textureWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 2, imageView, ctx->filteredSampler, &textureWriteImage, &textureWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite,
        textureWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    return descriptorSet;
}

VkSemaphore SwapchainElement::createSemaphore()
{
    VkResult result;

    VkSemaphoreCreateInfo semaphoreCreateInfo{};
    semaphoreCreateInfo.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;

    VkSemaphore semaphore;

    result = vkCreateSemaphore(ctx->device, &semaphoreCreateInfo, nullptr, &semaphore);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create semaphore: " + vulkanError(result));
    }

    return semaphore;
}

VkFence SwapchainElement::createFence()
{
    VkResult result;

    VkFenceCreateInfo fenceCreateInfo{};
    fenceCreateInfo.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fenceCreateInfo.flags = VK_FENCE_CREATE_SIGNALED_BIT;

    VkFence fence;

    result = vkCreateFence(ctx->device, &fenceCreateInfo, nullptr, &fence);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create fence: " + vulkanError(result));
    }

    return fence;
}

MainSwapchainElement::MainSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height
)
    : SwapchainElement(ctx, imageIndex, width, height)
{

}

AuxSwapchainElement::AuxSwapchainElement(
    const MainSwapchainElement* parent,
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height,
    VkRenderPass renderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera
)
    : SwapchainElement(ctx, imageIndex, width, height)
    , parent(parent)
{
    image = createImage(width, height, panelFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    imageView = createImageView(panelFormat, image.image, VK_IMAGE_ASPECT_COLOR_BIT);
    framebuffer = createFramebuffer(width, height, renderPass, { imageView });
    uniform = createBuffer(ctx->allocator, sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    uniformMapping = new VmaMapping<f32>(ctx->allocator, uniform);
    descriptorSet = createDescriptorSet(descriptorSetLayout, camera, uniform, imageView);
}

AuxSwapchainElement::~AuxSwapchainElement()
{
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSet);
    delete uniformMapping;
    destroyBuffer(ctx->allocator, uniform);
    vkDestroyFramebuffer(ctx->device, framebuffer, nullptr);
    vkDestroyImageView(ctx->device, imageView, nullptr);
    vmaDestroyImage(ctx->allocator, image.image, image.allocation);
}

SkySwapchainElement::SkySwapchainElement(
    const MainSwapchainElement* parent,
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height,
    VkRenderPass renderPass
)
    : SwapchainElement(ctx, imageIndex, width, height)
    , parent(parent)
{
    VkResult result;

    VkImageCreateInfo imageCreateInfo{};
    imageCreateInfo.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    imageCreateInfo.flags = VK_IMAGE_CREATE_CUBE_COMPATIBLE_BIT;
    imageCreateInfo.imageType = VK_IMAGE_TYPE_2D;
    imageCreateInfo.extent = { width, height, 1 };
    imageCreateInfo.mipLevels = 1;
    imageCreateInfo.arrayLayers = cubemapImageCount;
    imageCreateInfo.format = colorFormat;
    imageCreateInfo.samples =  VK_SAMPLE_COUNT_1_BIT;
    imageCreateInfo.tiling = VK_IMAGE_TILING_OPTIMAL;
    imageCreateInfo.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    imageCreateInfo.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    imageCreateInfo.usage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT;

    VmaAllocationCreateInfo imageAllocateInfo{};
    imageAllocateInfo.usage = VMA_MEMORY_USAGE_GPU_ONLY;

    result = vmaCreateImage(
        ctx->allocator,
        &imageCreateInfo,
        &imageAllocateInfo,
        &image.image,
        &image.allocation,
        nullptr
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create image: " + vulkanError(result));
    }

    imageView = createImageView(colorFormat, image.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_CUBE, 1, cubemapImageCount);
    framebuffer = createFramebuffer(width, height, renderPass, { imageView });
}

SkySwapchainElement::~SkySwapchainElement()
{
    vkDestroyFramebuffer(ctx->device, framebuffer, nullptr);
    vkDestroyImageView(ctx->device, imageView, nullptr);
    vmaDestroyImage(ctx->allocator, image.image, image.allocation);
}

FlatSwapchainElement::FlatSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height
)
    : MainSwapchainElement(ctx, imageIndex, width, height)
{

}

FlatMonosampleSwapchainElement::FlatMonosampleSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height,
    VkRenderPass panelRenderPass,
    VkRenderPass skyRenderPass,
    VkRenderPass mainRenderPass,
    VkRenderPass hdrRenderPass,
    VkRenderPass postProcessRenderPass,
    VkRenderPass overlayRenderPass,
    VkRenderPass independentDepthOverlayRenderPass,
    VkRenderPass uiRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkFormat format,
    VkImage image
)
    : FlatSwapchainElement(ctx, imageIndex, width, height)
    , _outputImage(image)
    , _hardwareOcclusionScene(nullptr)
    , _softwareOcclusionScene(nullptr)
    , _luminanceReady(false)
{
    u32 luminanceSize = luminanceImageSize(width * g_config.luminanceScale, height * g_config.luminanceScale);

    _colorImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    depthImage = createImage(width, height, depthFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    _stencilImage = createImage(width, height, stencilFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    _sideImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    _luminanceImage = createImage(luminanceSize, luminanceSize, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, maxMipLevels(luminanceSize, luminanceSize));

    colorImageView = createImageView(colorFormat, _colorImage.image, VK_IMAGE_ASPECT_COLOR_BIT);
    _depthImageView = createImageView(depthFormat, depthImage.image, VK_IMAGE_ASPECT_DEPTH_BIT);
    _stencilImageView = createImageView(stencilFormat, _stencilImage.image, VK_IMAGE_ASPECT_COLOR_BIT);
    _sideImageView = createImageView(colorFormat, _sideImage.image, VK_IMAGE_ASPECT_COLOR_BIT);

    _mainFramebuffer = createFramebuffer(width, height, mainRenderPass, { colorImageView, _depthImageView, _stencilImageView });
    _hdrFramebuffer = createFramebuffer(width, height, hdrRenderPass, { colorImageView });
    _postProcessFramebuffer = createFramebuffer(width, height, postProcessRenderPass, { colorImageView });
    _overlayFramebuffer = createFramebuffer(width, height, overlayRenderPass, { colorImageView, _depthImageView });
    _independentDepthOverlayFramebuffer = createFramebuffer(width, height, independentDepthOverlayRenderPass, { colorImageView, _depthImageView });
    _uiFramebuffer = createFramebuffer(width, height, uiRenderPass, { colorImageView });

    _transferCommandBuffer = createCommandBuffer();
    _commandBuffer = createCommandBuffer();

    _start = createSemaphore();
    _end = createSemaphore();
    _fence = createFence();
    _lastFence = nullptr;

    _camera = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    _cameraMapping = new VmaMapping<fmat4>(ctx->allocator, _camera);
    _luminance = createBuffer(ctx->allocator, 2 * sizeof(fvec4), VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_TO_CPU);
    _luminanceMapping = new VmaMapping<fvec4>(ctx->allocator, _luminance);

    _textTitlecard = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textTitlecardWidth,
        textTitlecardHeight,
        panelRenderPass,
        descriptorSetLayout,
        _camera
    );

    _text = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textWidth,
        textHeight,
        panelRenderPass,
        descriptorSetLayout,
        _camera
    );

    _sky = new SkySwapchainElement(
        this,
        ctx,
        imageIndex,
        skyWidth,
        skyHeight,
        skyRenderPass
    );

    if (ctx->rayTracingEnabled())
    {
        _hardwareOcclusionScene = new RenderHardwareOcclusionScene(ctx);
    }
    else
    {
        _softwareOcclusionScene = new RenderSoftwareOcclusionScene(ctx);
    }
}

FlatMonosampleSwapchainElement::~FlatMonosampleSwapchainElement()
{
    if (_softwareOcclusionScene)
    {
        delete _softwareOcclusionScene;
    }
    else
    {
        delete _hardwareOcclusionScene;
    }

    delete _sky;
    delete _text;
    delete _textTitlecard;

    delete _luminanceMapping;
    destroyBuffer(ctx->allocator, _luminance);
    delete _cameraMapping;
    destroyBuffer(ctx->allocator, _camera);

    vkDestroyFence(ctx->device, _fence, nullptr);

    vkDestroySemaphore(ctx->device, _end, nullptr);
    vkDestroySemaphore(ctx->device, _start, nullptr);

    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_commandBuffer);
    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_transferCommandBuffer);

    vkDestroyFramebuffer(ctx->device, _uiFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _independentDepthOverlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _overlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _hdrFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _postProcessFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _mainFramebuffer, nullptr);

    vkDestroyImageView(ctx->device, _sideImageView, nullptr);
    vkDestroyImageView(ctx->device, _stencilImageView, nullptr);
    vkDestroyImageView(ctx->device, _depthImageView, nullptr);
    vkDestroyImageView(ctx->device, colorImageView, nullptr);

    vmaDestroyImage(ctx->allocator, _luminanceImage.image, _luminanceImage.allocation);
    vmaDestroyImage(ctx->allocator, _sideImage.image, _sideImage.allocation);
    vmaDestroyImage(ctx->allocator, _stencilImage.image, _stencilImage.allocation);
    vmaDestroyImage(ctx->allocator, depthImage.image, depthImage.allocation);
    vmaDestroyImage(ctx->allocator, _colorImage.image, _colorImage.allocation);
}

VkImage FlatMonosampleSwapchainElement::colorImage() const
{
    return _colorImage.image;
}

VkImage FlatMonosampleSwapchainElement::stencilImage() const
{
    return _stencilImage.image;
}

VkImage FlatMonosampleSwapchainElement::sideImage() const
{
    return _sideImage.image;
}

VkImage FlatMonosampleSwapchainElement::luminanceImage() const
{
    return _luminanceImage.image;
}

VkImage FlatMonosampleSwapchainElement::outputImage() const
{
    return _outputImage;
}

VkImageView FlatMonosampleSwapchainElement::depthImageView() const
{
    return _depthImageView;
}

VkImageView FlatMonosampleSwapchainElement::stencilImageView() const
{
    return _stencilImageView;
}

VkImageView FlatMonosampleSwapchainElement::sideImageView() const
{
    return _sideImageView;
}

VkFramebuffer FlatMonosampleSwapchainElement::mainFramebuffer() const
{
    return _mainFramebuffer;
}

VkFramebuffer FlatMonosampleSwapchainElement::hdrFramebuffer() const
{
    return _hdrFramebuffer;
}

VkFramebuffer FlatMonosampleSwapchainElement::postProcessFramebuffer() const
{
    return _postProcessFramebuffer;
}

VkFramebuffer FlatMonosampleSwapchainElement::overlayFramebuffer() const
{
    return _overlayFramebuffer;
}

VkFramebuffer FlatMonosampleSwapchainElement::independentDepthOverlayFramebuffer() const
{
    return _independentDepthOverlayFramebuffer;
}

VkFramebuffer FlatMonosampleSwapchainElement::uiFramebuffer() const
{
    return _uiFramebuffer;
}

VkCommandBuffer FlatMonosampleSwapchainElement::transferCommandBuffer() const
{
    return _transferCommandBuffer;
}

VkCommandBuffer FlatMonosampleSwapchainElement::commandBuffer() const
{
    return _commandBuffer;
}

const VkSemaphore& FlatMonosampleSwapchainElement::start() const
{
    return _start;
}

const VkSemaphore& FlatMonosampleSwapchainElement::end() const
{
    return _end;
}

const VkFence& FlatMonosampleSwapchainElement::fence() const
{
    return _fence;
}

VkFence& FlatMonosampleSwapchainElement::lastFence()
{
    return _lastFence;
}

VmaBuffer FlatMonosampleSwapchainElement::camera() const
{
    return _camera;
}

VmaMapping<fmat4>* FlatMonosampleSwapchainElement::cameraMapping() const
{
    return _cameraMapping;
}

VmaBuffer FlatMonosampleSwapchainElement::luminance() const
{
    return _luminance;
}

VmaMapping<fvec4>* FlatMonosampleSwapchainElement::luminanceMapping() const
{
    return _luminanceMapping;
}

AuxSwapchainElement* FlatMonosampleSwapchainElement::textTitlecard() const
{
    return _textTitlecard;
}

AuxSwapchainElement* FlatMonosampleSwapchainElement::text() const
{
    return _text;
}

SkySwapchainElement* FlatMonosampleSwapchainElement::sky() const
{
    return _sky;
}

RenderHardwareOcclusionScene* FlatMonosampleSwapchainElement::hardwareOcclusionScene() const
{
    return _hardwareOcclusionScene;
}

RenderSoftwareOcclusionScene* FlatMonosampleSwapchainElement::softwareOcclusionScene() const
{
    return _softwareOcclusionScene;
}

RenderOcclusionSceneReference FlatMonosampleSwapchainElement::occlusionScene() const
{
    if (_hardwareOcclusionScene)
    {
        return _hardwareOcclusionScene->get();
    }
    else
    {
        return _softwareOcclusionScene->get();
    }
}

void FlatMonosampleSwapchainElement::setLuminanceReady()
{
    _luminanceReady = true;
}

bool FlatMonosampleSwapchainElement::luminanceReady() const
{
    return _luminanceReady;
}

FlatMultisampleSwapchainElement::FlatMultisampleSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height,
    VkSampleCountFlagBits sampleCount,
    VkRenderPass panelRenderPass,
    VkRenderPass skyRenderPass,
    VkRenderPass mainRenderPass,
    VkRenderPass hdrRenderPass,
    VkRenderPass postProcessRenderPass,
    VkRenderPass overlayRenderPass,
    VkRenderPass independentDepthOverlayRenderPass,
    VkRenderPass uiRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkFormat format,
    VkImage image
)
    : FlatSwapchainElement(ctx, imageIndex, width, height)
    , _outputImage(image)
    , _hardwareOcclusionScene(nullptr)
    , _softwareOcclusionScene(nullptr)
    , _luminanceReady(false)
{
    u32 luminanceSize = luminanceImageSize(width * g_config.luminanceScale, height * g_config.luminanceScale);

    colorMultisampleImage = createImage(width, height, colorFormat, sampleCount, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
    depthMultisampleImage = createImage(width, height, depthFormat, sampleCount, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT);
    stencilMultisampleImage = createImage(width, height, stencilFormat, sampleCount, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT);
    _colorImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    depthImage = createImage(width, height, depthFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    _stencilImage = createImage(width, height, stencilFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT);
    _sideImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT);
    _luminanceImage = createImage(luminanceSize, luminanceSize, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, maxMipLevels(luminanceSize, luminanceSize));

    colorMultisampleImageView = createImageView(colorFormat, colorMultisampleImage.image, VK_IMAGE_ASPECT_COLOR_BIT);
    depthMultisampleImageView = createImageView(depthFormat, depthMultisampleImage.image, VK_IMAGE_ASPECT_DEPTH_BIT);
    stencilMultisampleImageView = createImageView(stencilFormat, stencilMultisampleImage.image, VK_IMAGE_ASPECT_COLOR_BIT);
    colorImageView = createImageView(colorFormat, _colorImage.image, VK_IMAGE_ASPECT_COLOR_BIT);
    _depthImageView = createImageView(depthFormat, depthImage.image, VK_IMAGE_ASPECT_DEPTH_BIT);
    _stencilImageView = createImageView(stencilFormat, _stencilImage.image, VK_IMAGE_ASPECT_COLOR_BIT);
    _sideImageView = createImageView(colorFormat, _sideImage.image, VK_IMAGE_ASPECT_COLOR_BIT);

    _mainFramebuffer = createFramebuffer(width, height, mainRenderPass, { colorMultisampleImageView, depthMultisampleImageView, stencilMultisampleImageView, colorImageView, _depthImageView, _stencilImageView });
    _hdrFramebuffer = createFramebuffer(width, height, hdrRenderPass, { colorMultisampleImageView, colorImageView });
    _postProcessFramebuffer = createFramebuffer(width, height, postProcessRenderPass, { colorMultisampleImageView, colorImageView });
    _overlayFramebuffer = createFramebuffer(width, height, overlayRenderPass, { colorMultisampleImageView, depthMultisampleImageView });
    _independentDepthOverlayFramebuffer = createFramebuffer(width, height, independentDepthOverlayRenderPass, { colorMultisampleImageView, depthMultisampleImageView, colorImageView });
    _uiFramebuffer = createFramebuffer(width, height, uiRenderPass, { colorImageView });

    _transferCommandBuffer = createCommandBuffer();
    _commandBuffer = createCommandBuffer();

    _start = createSemaphore();
    _end = createSemaphore();
    _fence = createFence();
    _lastFence = nullptr;

    _camera = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    _cameraMapping = new VmaMapping<fmat4>(ctx->allocator, _camera);
    _luminance = createBuffer(ctx->allocator, 2 * sizeof(fvec4), VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_TO_CPU);
    _luminanceMapping = new VmaMapping<fvec4>(ctx->allocator, _luminance);

    _textTitlecard = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textTitlecardWidth,
        textTitlecardHeight,
        panelRenderPass,
        descriptorSetLayout,
        _camera
    );

    _text = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textWidth,
        textHeight,
        panelRenderPass,
        descriptorSetLayout,
        _camera
    );

    _sky = new SkySwapchainElement(
        this,
        ctx,
        imageIndex,
        skyWidth,
        skyHeight,
        skyRenderPass
    );

    if (ctx->rayTracingEnabled())
    {
        _hardwareOcclusionScene = new RenderHardwareOcclusionScene(ctx);
    }
    else
    {
        _softwareOcclusionScene = new RenderSoftwareOcclusionScene(ctx);
    }
}

FlatMultisampleSwapchainElement::~FlatMultisampleSwapchainElement()
{
    if (_softwareOcclusionScene)
    {
        delete _softwareOcclusionScene;
    }
    else
    {
        delete _hardwareOcclusionScene;
    }

    delete _sky;
    delete _text;
    delete _textTitlecard;

    delete _luminanceMapping;
    destroyBuffer(ctx->allocator, _luminance);
    delete _cameraMapping;
    destroyBuffer(ctx->allocator, _camera);

    vkDestroyFence(ctx->device, _fence, nullptr);

    vkDestroySemaphore(ctx->device, _end, nullptr);
    vkDestroySemaphore(ctx->device, _start, nullptr);

    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_commandBuffer);
    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_transferCommandBuffer);

    vkDestroyFramebuffer(ctx->device, _uiFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _independentDepthOverlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _overlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _postProcessFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _hdrFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _mainFramebuffer, nullptr);

    vkDestroyImageView(ctx->device, _sideImageView, nullptr);
    vkDestroyImageView(ctx->device, _stencilImageView, nullptr);
    vkDestroyImageView(ctx->device, _depthImageView, nullptr);
    vkDestroyImageView(ctx->device, colorImageView, nullptr);
    vkDestroyImageView(ctx->device, stencilMultisampleImageView, nullptr);
    vkDestroyImageView(ctx->device, depthMultisampleImageView, nullptr);
    vkDestroyImageView(ctx->device, colorMultisampleImageView, nullptr);

    vmaDestroyImage(ctx->allocator, _luminanceImage.image, _luminanceImage.allocation);
    vmaDestroyImage(ctx->allocator, _sideImage.image, _sideImage.allocation);
    vmaDestroyImage(ctx->allocator, _stencilImage.image, _stencilImage.allocation);
    vmaDestroyImage(ctx->allocator, depthImage.image, depthImage.allocation);
    vmaDestroyImage(ctx->allocator, _colorImage.image, _colorImage.allocation);
    vmaDestroyImage(ctx->allocator, stencilMultisampleImage.image, stencilMultisampleImage.allocation);
    vmaDestroyImage(ctx->allocator, depthMultisampleImage.image, depthMultisampleImage.allocation);
    vmaDestroyImage(ctx->allocator, colorMultisampleImage.image, colorMultisampleImage.allocation);
}

VkImage FlatMultisampleSwapchainElement::colorImage() const
{
    return _colorImage.image;
}

VkImage FlatMultisampleSwapchainElement::stencilImage() const
{
    return _stencilImage.image;
}

VkImage FlatMultisampleSwapchainElement::sideImage() const
{
    return _sideImage.image;
}

VkImage FlatMultisampleSwapchainElement::luminanceImage() const
{
    return _luminanceImage.image;
}

VkImage FlatMultisampleSwapchainElement::outputImage() const
{
    return _outputImage;
}

VkImageView FlatMultisampleSwapchainElement::depthImageView() const
{
    return _depthImageView;
}

VkImageView FlatMultisampleSwapchainElement::stencilImageView() const
{
    return _stencilImageView;
}

VkImageView FlatMultisampleSwapchainElement::sideImageView() const
{
    return _sideImageView;
}

VkFramebuffer FlatMultisampleSwapchainElement::mainFramebuffer() const
{
    return _mainFramebuffer;
}

VkFramebuffer FlatMultisampleSwapchainElement::hdrFramebuffer() const
{
    return _hdrFramebuffer;
}

VkFramebuffer FlatMultisampleSwapchainElement::postProcessFramebuffer() const
{
    return _postProcessFramebuffer;
}

VkFramebuffer FlatMultisampleSwapchainElement::overlayFramebuffer() const
{
    return _overlayFramebuffer;
}

VkFramebuffer FlatMultisampleSwapchainElement::independentDepthOverlayFramebuffer() const
{
    return _independentDepthOverlayFramebuffer;
}

VkFramebuffer FlatMultisampleSwapchainElement::uiFramebuffer() const
{
    return _uiFramebuffer;
}

VkCommandBuffer FlatMultisampleSwapchainElement::transferCommandBuffer() const
{
    return _transferCommandBuffer;
}

VkCommandBuffer FlatMultisampleSwapchainElement::commandBuffer() const
{
    return _commandBuffer;
}

const VkSemaphore& FlatMultisampleSwapchainElement::start() const
{
    return _start;
}

const VkSemaphore& FlatMultisampleSwapchainElement::end() const
{
    return _end;
}

const VkFence& FlatMultisampleSwapchainElement::fence() const
{
    return _fence;
}

VkFence& FlatMultisampleSwapchainElement::lastFence()
{
    return _lastFence;
}

VmaBuffer FlatMultisampleSwapchainElement::camera() const
{
    return _camera;
}

VmaMapping<fmat4>* FlatMultisampleSwapchainElement::cameraMapping() const
{
    return _cameraMapping;
}

VmaBuffer FlatMultisampleSwapchainElement::luminance() const
{
    return _luminance;
}

VmaMapping<fvec4>* FlatMultisampleSwapchainElement::luminanceMapping() const
{
    return _luminanceMapping;
}

AuxSwapchainElement* FlatMultisampleSwapchainElement::textTitlecard() const
{
    return _textTitlecard;
}

AuxSwapchainElement* FlatMultisampleSwapchainElement::text() const
{
    return _text;
}

SkySwapchainElement* FlatMultisampleSwapchainElement::sky() const
{
    return _sky;
}

RenderHardwareOcclusionScene* FlatMultisampleSwapchainElement::hardwareOcclusionScene() const
{
    return _hardwareOcclusionScene;
}

RenderSoftwareOcclusionScene* FlatMultisampleSwapchainElement::softwareOcclusionScene() const
{
    return _softwareOcclusionScene;
}

RenderOcclusionSceneReference FlatMultisampleSwapchainElement::occlusionScene() const
{
    if (_hardwareOcclusionScene)
    {
        return _hardwareOcclusionScene->get();
    }
    else
    {
        return _softwareOcclusionScene->get();
    }
}

void FlatMultisampleSwapchainElement::setLuminanceReady()
{
    _luminanceReady = true;
}

bool FlatMultisampleSwapchainElement::luminanceReady() const
{
    return _luminanceReady;
}

VRSwapchainElement::VRSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height
)
    : MainSwapchainElement(ctx, imageIndex, width, height)
{

}

VRMonosampleSwapchainElement::VRMonosampleSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height,
    VkRenderPass panelRenderPass,
    VkRenderPass skyRenderPass,
    VkRenderPass mainRenderPass,
    VkRenderPass hdrRenderPass,
    VkRenderPass postProcessRenderPass,
    VkRenderPass overlayRenderPass,
    VkRenderPass independentDepthOverlayRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkFormat format,
    VkImage image
)
    : VRSwapchainElement(ctx, imageIndex, width, height)
    , _outputImage(image)
    , _hardwareOcclusionScene(nullptr)
    , _softwareOcclusionScene(nullptr)
    , _luminanceReady(false)
{
    u32 luminanceSize = luminanceImageSize(width * g_config.luminanceScale, height * g_config.luminanceScale);

    _colorImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT, 1, eyeCount);
    depthImage = createImage(width, height, depthFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, 1, eyeCount);
    _stencilImage = createImage(width, height, stencilFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT, 1, eyeCount);
    _sideImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, 1, eyeCount);
    _luminanceImage = createImage(luminanceSize, luminanceSize, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, maxMipLevels(luminanceSize, luminanceSize), eyeCount);

    colorImageView = createImageView(colorFormat, _colorImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    _depthImageView = createImageView(depthFormat, depthImage.image, VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    _stencilImageView = createImageView(stencilFormat, _stencilImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    _sideImageView = createImageView(colorFormat, _sideImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);

    _mainFramebuffer = createFramebuffer(width, height, mainRenderPass, { colorImageView, _depthImageView, _stencilImageView });
    _hdrFramebuffer = createFramebuffer(width, height, hdrRenderPass, { colorImageView });
    _postProcessFramebuffer = createFramebuffer(width, height, postProcessRenderPass, { colorImageView });
    _overlayFramebuffer = createFramebuffer(width, height, overlayRenderPass, { colorImageView, _depthImageView });
    _independentDepthOverlayFramebuffer = createFramebuffer(width, height, independentDepthOverlayRenderPass, { colorImageView, _depthImageView });

    _transferCommandBuffer = createCommandBuffer();
    _commandBuffer = createCommandBuffer();

    _worldCamera = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    _worldCameraMapping = new VmaMapping<fmat4>(ctx->allocator, _worldCamera);
    _roomCamera = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    _roomCameraMapping = new VmaMapping<fmat4>(ctx->allocator, _roomCamera);
    _luminance = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fvec4), VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_TO_CPU);
    _luminanceMapping = new VmaMapping<fvec4>(ctx->allocator, _luminance);

    _textTitlecard = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textTitlecardWidth,
        textTitlecardHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _text = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textWidth,
        textHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _hud = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        hudWidth,
        hudHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _keyboard = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        vrKeyboardWidth,
        vrKeyboardHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _sky = new SkySwapchainElement(
        this,
        ctx,
        imageIndex,
        skyWidth,
        skyHeight,
        skyRenderPass
    );

    if (ctx->rayTracingEnabled())
    {
        _hardwareOcclusionScene = new RenderHardwareOcclusionScene(ctx);
    }
    else
    {
        _softwareOcclusionScene = new RenderSoftwareOcclusionScene(ctx);
    }
}

VRMonosampleSwapchainElement::~VRMonosampleSwapchainElement()
{
    if (_softwareOcclusionScene)
    {
        delete _softwareOcclusionScene;
    }
    else
    {
        delete _hardwareOcclusionScene;
    }

    delete _sky;
    delete _keyboard;
    delete _hud;
    delete _text;
    delete _textTitlecard;

    delete _luminanceMapping;
    destroyBuffer(ctx->allocator, _luminance);
    delete _roomCameraMapping;
    destroyBuffer(ctx->allocator, _roomCamera);
    delete _worldCameraMapping;
    destroyBuffer(ctx->allocator, _worldCamera);

    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_commandBuffer);
    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_transferCommandBuffer);

    vkDestroyFramebuffer(ctx->device, _independentDepthOverlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _overlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _postProcessFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _hdrFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _mainFramebuffer, nullptr);

    vkDestroyImageView(ctx->device, _sideImageView, nullptr);
    vkDestroyImageView(ctx->device, _stencilImageView, nullptr);
    vkDestroyImageView(ctx->device, _depthImageView, nullptr);
    vkDestroyImageView(ctx->device, colorImageView, nullptr);

    vmaDestroyImage(ctx->allocator, _luminanceImage.image, _luminanceImage.allocation);
    vmaDestroyImage(ctx->allocator, _sideImage.image, _sideImage.allocation);
    vmaDestroyImage(ctx->allocator, _stencilImage.image, _stencilImage.allocation);
    vmaDestroyImage(ctx->allocator, depthImage.image, depthImage.allocation);
    vmaDestroyImage(ctx->allocator, _colorImage.image, _colorImage.allocation);
}

VkImage VRMonosampleSwapchainElement::colorImage() const
{
    return _colorImage.image;
}

VkImage VRMonosampleSwapchainElement::stencilImage() const
{
    return _stencilImage.image;
}

VkImage VRMonosampleSwapchainElement::sideImage() const
{
    return _sideImage.image;
}

VkImage VRMonosampleSwapchainElement::luminanceImage() const
{
    return _luminanceImage.image;
}

VkImage VRMonosampleSwapchainElement::outputImage() const
{
    return _outputImage;
}

VkImageView VRMonosampleSwapchainElement::depthImageView() const
{
    return _depthImageView;
}

VkImageView VRMonosampleSwapchainElement::stencilImageView() const
{
    return _stencilImageView;
}

VkImageView VRMonosampleSwapchainElement::sideImageView() const
{
    return _sideImageView;
}

VkFramebuffer VRMonosampleSwapchainElement::mainFramebuffer() const
{
    return _mainFramebuffer;
}

VkFramebuffer VRMonosampleSwapchainElement::hdrFramebuffer() const
{
    return _hdrFramebuffer;
}

VkFramebuffer VRMonosampleSwapchainElement::postProcessFramebuffer() const
{
    return _postProcessFramebuffer;
}

VkFramebuffer VRMonosampleSwapchainElement::overlayFramebuffer() const
{
    return _overlayFramebuffer;
}

VkFramebuffer VRMonosampleSwapchainElement::independentDepthOverlayFramebuffer() const
{
    return _independentDepthOverlayFramebuffer;
}

VkCommandBuffer VRMonosampleSwapchainElement::transferCommandBuffer() const
{
    return _transferCommandBuffer;
}

VkCommandBuffer VRMonosampleSwapchainElement::commandBuffer() const
{
    return _commandBuffer;
}

VmaBuffer VRMonosampleSwapchainElement::worldCamera() const
{
    return _worldCamera;
}

VmaMapping<fmat4>* VRMonosampleSwapchainElement::worldCameraMapping() const
{
    return _worldCameraMapping;
}

VmaBuffer VRMonosampleSwapchainElement::roomCamera() const
{
    return _roomCamera;
}

VmaMapping<fmat4>* VRMonosampleSwapchainElement::roomCameraMapping() const
{
    return _roomCameraMapping;
}

VmaBuffer VRMonosampleSwapchainElement::luminance() const
{
    return _luminance;
}

VmaMapping<fvec4>* VRMonosampleSwapchainElement::luminanceMapping() const
{
    return _luminanceMapping;
}

AuxSwapchainElement* VRMonosampleSwapchainElement::textTitlecard() const
{
    return _textTitlecard;
}

AuxSwapchainElement* VRMonosampleSwapchainElement::text() const
{
    return _text;
}

AuxSwapchainElement* VRMonosampleSwapchainElement::hud() const
{
    return _hud;
}

AuxSwapchainElement* VRMonosampleSwapchainElement::keyboard() const
{
    return _keyboard;
}

SkySwapchainElement* VRMonosampleSwapchainElement::sky() const
{
    return _sky;
}

RenderHardwareOcclusionScene* VRMonosampleSwapchainElement::hardwareOcclusionScene() const
{
    return _hardwareOcclusionScene;
}

RenderSoftwareOcclusionScene* VRMonosampleSwapchainElement::softwareOcclusionScene() const
{
    return _softwareOcclusionScene;
}

RenderOcclusionSceneReference VRMonosampleSwapchainElement::occlusionScene() const
{
    if (_hardwareOcclusionScene)
    {
        return _hardwareOcclusionScene->get();
    }
    else
    {
        return _softwareOcclusionScene->get();
    }
}

void VRMonosampleSwapchainElement::setLuminanceReady()
{
    _luminanceReady = true;
}

bool VRMonosampleSwapchainElement::luminanceReady() const
{
    return _luminanceReady;
}

VRMultisampleSwapchainElement::VRMultisampleSwapchainElement(
    const RenderContext* ctx,
    u32 imageIndex,
    u32 width,
    u32 height,
    VkSampleCountFlagBits sampleCount,
    VkRenderPass panelRenderPass,
    VkRenderPass skyRenderPass,
    VkRenderPass mainRenderPass,
    VkRenderPass hdrRenderPass,
    VkRenderPass postProcessRenderPass,
    VkRenderPass overlayRenderPass,
    VkRenderPass independentDepthOverlayRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkFormat format,
    VkImage image
)
    : VRSwapchainElement(ctx, imageIndex, width, height)
    , _outputImage(image)
    , _hardwareOcclusionScene(nullptr)
    , _softwareOcclusionScene(nullptr)
    , _luminanceReady(false)
{
    u32 luminanceSize = luminanceImageSize(width * g_config.luminanceScale, height * g_config.luminanceScale);

    colorMultisampleImage = createImage(width, height, colorFormat, sampleCount, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, 1, eyeCount);
    depthMultisampleImage = createImage(width, height, depthFormat, sampleCount, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT, 1, eyeCount);
    stencilMultisampleImage = createImage(width, height, stencilFormat, sampleCount, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT, 1, eyeCount);
    _colorImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT, 1, eyeCount);
    depthImage = createImage(width, height, depthFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, 1, eyeCount);
    _stencilImage = createImage(width, height, stencilFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT, 1, eyeCount);
    _sideImage = createImage(width, height, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_DST_BIT | VK_IMAGE_USAGE_SAMPLED_BIT, 1, eyeCount);
    _luminanceImage = createImage(luminanceSize, luminanceSize, colorFormat, VK_SAMPLE_COUNT_1_BIT, VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT, maxMipLevels(luminanceSize, luminanceSize), eyeCount);

    colorMultisampleImageView = createImageView(colorFormat, colorMultisampleImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    depthMultisampleImageView = createImageView(depthFormat, depthMultisampleImage.image, VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    stencilMultisampleImageView = createImageView(stencilFormat, stencilMultisampleImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    colorImageView = createImageView(colorFormat, _colorImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    _depthImageView = createImageView(depthFormat, depthImage.image, VK_IMAGE_ASPECT_DEPTH_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    _stencilImageView = createImageView(stencilFormat, _stencilImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);
    _sideImageView = createImageView(colorFormat, _sideImage.image, VK_IMAGE_ASPECT_COLOR_BIT, VK_IMAGE_VIEW_TYPE_2D_ARRAY, 1, eyeCount);

    _mainFramebuffer = createFramebuffer(width, height, mainRenderPass, { colorMultisampleImageView, depthMultisampleImageView, stencilMultisampleImageView, colorImageView, _depthImageView, _stencilImageView });
    _hdrFramebuffer = createFramebuffer(width, height, hdrRenderPass, { colorMultisampleImageView, colorImageView });
    _postProcessFramebuffer = createFramebuffer(width, height, postProcessRenderPass, { colorMultisampleImageView, colorImageView });
    _overlayFramebuffer = createFramebuffer(width, height, overlayRenderPass, { colorMultisampleImageView, depthMultisampleImageView });
    _independentDepthOverlayFramebuffer = createFramebuffer(width, height, independentDepthOverlayRenderPass, { colorMultisampleImageView, depthMultisampleImageView, colorImageView });

    _transferCommandBuffer = createCommandBuffer();
    _commandBuffer = createCommandBuffer();

    _worldCamera = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    _worldCameraMapping = new VmaMapping<fmat4>(ctx->allocator, _worldCamera);
    _roomCamera = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fmat4), VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT, VMA_MEMORY_USAGE_CPU_TO_GPU);
    _roomCameraMapping = new VmaMapping<fmat4>(ctx->allocator, _roomCamera);
    _luminance = createBuffer(ctx->allocator, eyeCount * 2 * sizeof(fvec4), VK_BUFFER_USAGE_TRANSFER_DST_BIT, VMA_MEMORY_USAGE_GPU_TO_CPU);
    _luminanceMapping = new VmaMapping<fvec4>(ctx->allocator, _luminance);

    _textTitlecard = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textTitlecardWidth,
        textTitlecardHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _text = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        textWidth,
        textHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _hud = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        hudWidth,
        hudHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _keyboard = new AuxSwapchainElement(
        this,
        ctx,
        imageIndex,
        vrKeyboardWidth,
        vrKeyboardHeight,
        panelRenderPass,
        descriptorSetLayout,
        _roomCamera
    );

    _sky = new SkySwapchainElement(
        this,
        ctx,
        imageIndex,
        skyWidth,
        skyHeight,
        skyRenderPass
    );

    if (ctx->rayTracingEnabled())
    {
        _hardwareOcclusionScene = new RenderHardwareOcclusionScene(ctx);
    }
    else
    {
        _softwareOcclusionScene = new RenderSoftwareOcclusionScene(ctx);
    }
}

VRMultisampleSwapchainElement::~VRMultisampleSwapchainElement()
{
    if (_softwareOcclusionScene)
    {
        delete _softwareOcclusionScene;
    }
    else
    {
        delete _hardwareOcclusionScene;
    }

    delete _sky;
    delete _keyboard;
    delete _hud;
    delete _text;
    delete _textTitlecard;

    delete _luminanceMapping;
    destroyBuffer(ctx->allocator, _luminance);
    delete _roomCameraMapping;
    destroyBuffer(ctx->allocator, _roomCamera);
    delete _worldCameraMapping;
    destroyBuffer(ctx->allocator, _worldCamera);

    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_commandBuffer);
    vkFreeCommandBuffers(ctx->device, ctx->commandPool, 1, &_transferCommandBuffer);

    vkDestroyFramebuffer(ctx->device, _independentDepthOverlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _overlayFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _postProcessFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _hdrFramebuffer, nullptr);
    vkDestroyFramebuffer(ctx->device, _mainFramebuffer, nullptr);

    vkDestroyImageView(ctx->device, _sideImageView, nullptr);
    vkDestroyImageView(ctx->device, _stencilImageView, nullptr);
    vkDestroyImageView(ctx->device, _depthImageView, nullptr);
    vkDestroyImageView(ctx->device, colorImageView, nullptr);
    vkDestroyImageView(ctx->device, stencilMultisampleImageView, nullptr);
    vkDestroyImageView(ctx->device, depthMultisampleImageView, nullptr);
    vkDestroyImageView(ctx->device, colorMultisampleImageView, nullptr);

    vmaDestroyImage(ctx->allocator, _luminanceImage.image, _luminanceImage.allocation);
    vmaDestroyImage(ctx->allocator, _sideImage.image, _sideImage.allocation);
    vmaDestroyImage(ctx->allocator, _stencilImage.image, _stencilImage.allocation);
    vmaDestroyImage(ctx->allocator, depthImage.image, depthImage.allocation);
    vmaDestroyImage(ctx->allocator, _colorImage.image, _colorImage.allocation);
    vmaDestroyImage(ctx->allocator, stencilMultisampleImage.image, stencilMultisampleImage.allocation);
    vmaDestroyImage(ctx->allocator, depthMultisampleImage.image, depthMultisampleImage.allocation);
    vmaDestroyImage(ctx->allocator, colorMultisampleImage.image, colorMultisampleImage.allocation);
}

VkImage VRMultisampleSwapchainElement::colorImage() const
{
    return _colorImage.image;
}

VkImage VRMultisampleSwapchainElement::stencilImage() const
{
    return _stencilImage.image;
}

VkImage VRMultisampleSwapchainElement::sideImage() const
{
    return _sideImage.image;
}

VkImage VRMultisampleSwapchainElement::luminanceImage() const
{
    return _luminanceImage.image;
}

VkImage VRMultisampleSwapchainElement::outputImage() const
{
    return _outputImage;
}

VkImageView VRMultisampleSwapchainElement::depthImageView() const
{
    return _depthImageView;
}

VkImageView VRMultisampleSwapchainElement::stencilImageView() const
{
    return _stencilImageView;
}

VkImageView VRMultisampleSwapchainElement::sideImageView() const
{
    return _sideImageView;
}

VkFramebuffer VRMultisampleSwapchainElement::mainFramebuffer() const
{
    return _mainFramebuffer;
}

VkFramebuffer VRMultisampleSwapchainElement::hdrFramebuffer() const
{
    return _hdrFramebuffer;
}

VkFramebuffer VRMultisampleSwapchainElement::postProcessFramebuffer() const
{
    return _postProcessFramebuffer;
}

VkFramebuffer VRMultisampleSwapchainElement::overlayFramebuffer() const
{
    return _overlayFramebuffer;
}

VkFramebuffer VRMultisampleSwapchainElement::independentDepthOverlayFramebuffer() const
{
    return _independentDepthOverlayFramebuffer;
}

VkCommandBuffer VRMultisampleSwapchainElement::transferCommandBuffer() const
{
    return _transferCommandBuffer;
}

VkCommandBuffer VRMultisampleSwapchainElement::commandBuffer() const
{
    return _commandBuffer;
}

VmaBuffer VRMultisampleSwapchainElement::worldCamera() const
{
    return _worldCamera;
}

VmaMapping<fmat4>* VRMultisampleSwapchainElement::worldCameraMapping() const
{
    return _worldCameraMapping;
}

VmaBuffer VRMultisampleSwapchainElement::roomCamera() const
{
    return _roomCamera;
}

VmaMapping<fmat4>* VRMultisampleSwapchainElement::roomCameraMapping() const
{
    return _roomCameraMapping;
}

VmaBuffer VRMultisampleSwapchainElement::luminance() const
{
    return _luminance;
}

VmaMapping<fvec4>* VRMultisampleSwapchainElement::luminanceMapping() const
{
    return _luminanceMapping;
}

AuxSwapchainElement* VRMultisampleSwapchainElement::textTitlecard() const
{
    return _textTitlecard;
}

AuxSwapchainElement* VRMultisampleSwapchainElement::text() const
{
    return _text;
}

AuxSwapchainElement* VRMultisampleSwapchainElement::hud() const
{
    return _hud;
}

AuxSwapchainElement* VRMultisampleSwapchainElement::keyboard() const
{
    return _keyboard;
}

SkySwapchainElement* VRMultisampleSwapchainElement::sky() const
{
    return _sky;
}

RenderHardwareOcclusionScene* VRMultisampleSwapchainElement::hardwareOcclusionScene() const
{
    return _hardwareOcclusionScene;
}

RenderSoftwareOcclusionScene* VRMultisampleSwapchainElement::softwareOcclusionScene() const
{
    return _softwareOcclusionScene;
}

RenderOcclusionSceneReference VRMultisampleSwapchainElement::occlusionScene() const
{
    if (_hardwareOcclusionScene)
    {
        return _hardwareOcclusionScene->get();
    }
    else
    {
        return _softwareOcclusionScene->get();
    }
}

void VRMultisampleSwapchainElement::setLuminanceReady()
{
    _luminanceReady = true;
}

bool VRMultisampleSwapchainElement::luminanceReady() const
{
    return _luminanceReady;
}
