/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "new_text_tool.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "tab_view.hpp"
#include "user_text_status.hpp"
#include "new_text_tool_pages.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(760 * uiScale(), UIScale::tabSize().h * 15);
}

NewTextTool::NewTextTool(InterfaceView* view)
    : InterfaceWindow(view, localize("new-text-tool-new-text-tool"))
{
    START_WIDGET_SCOPE("new-text-tool")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-new-text-tool", dismiss)
    END_SCOPE

    tabs = new TabView(this, {
        localize("new-text-tool-status"),
        localize("new-text-tool-signal"),
        localize("new-text-tool-titlecard"),
        localize("new-text-tool-timeout"),
        localize("new-text-tool-unary"),
        localize("new-text-tool-binary"),
        localize("new-text-tool-options"),
        localize("new-text-tool-choose"),
        localize("new-text-tool-assign"),
        localize("new-text-tool-point-buy"),
        localize("new-text-tool-point-assign"),
        localize("new-text-tool-vote"),
        localize("new-text-tool-choose-major"),
        localize("new-text-tool-knowledge"),
        localize("new-text-tool-clear")
    }, true);

    new UserTextStatus(tabs->tab(localize("new-text-tool-status")));
    new TextSignalTool(tabs->tab(localize("new-text-tool-signal")));
    new TextTitlecardTool(tabs->tab(localize("new-text-tool-titlecard")));
    new TextTimeoutTool(tabs->tab(localize("new-text-tool-timeout")));
    new TextUnaryTool(tabs->tab(localize("new-text-tool-unary")));
    new TextBinaryTool(tabs->tab(localize("new-text-tool-binary")));
    new TextOptionsTool(tabs->tab(localize("new-text-tool-options")));
    new TextChooseTool(tabs->tab(localize("new-text-tool-choose")));
    new TextAssignValuesTool(tabs->tab(localize("new-text-tool-assign")));
    new TextSpendPointsTool(tabs->tab(localize("new-text-tool-point-buy")));
    new TextAssignPointsTool(tabs->tab(localize("new-text-tool-point-assign")));
    new TextVoteTool(tabs->tab(localize("new-text-tool-vote")));
    new TextChooseMajorTool(tabs->tab(localize("new-text-tool-choose-major")));
    new TextKnowledgeTool(tabs->tab(localize("new-text-tool-knowledge")));
    new TextClearTool(tabs->tab(localize("new-text-tool-clear")));
}

void NewTextTool::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());

    for (InterfaceWidget* child : tabs->activeTab()->children())
    {
        dynamic_cast<TabContent*>(child)->open();
    }
}

void NewTextTool::open(const Point& point, const string& tabName)
{
    open(point);

    tabs->open(tabName);
}

void NewTextTool::dismiss(const Input& input)
{
    hide();
}
