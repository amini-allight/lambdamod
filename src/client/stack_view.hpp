/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "stack_pane.hpp"

class StackView : public InterfaceWidget
{
public:
    StackView(InterfaceWidget* parent);

    template<typename T, typename... Args>
    void add(const string& pageName, Args... args)
    {
        auto pane = new StackPane(this, pageName);
        pages.insert_or_assign(pageName, pane);
        new T(pane, args...);
        update();
    }
    void remove(const string& pageName);
    void open(const string& pageName);
    void close();

    InterfaceWidget* page(const string& name) const;

    void step() override;
    void draw(const DrawContext& ctx) const override;

    const string& activePageName() const;
    vector<string> pageNames() const;
    InterfaceWidget* activePage() const;

protected:
    string _activePageName;
    map<string, InterfaceWidget*> pages;

    SizeProperties sizeProperties() const override;
};
