/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "theme.hpp"
#include "yaml_tools.hpp"

static void tryLoadColor(const YAMLNode& node, Color& color, const string& name)
{
    if (node.has(name))
    {
        color = Color(node.at(name).as<string>());
    }
}

template<>
Theme YAMLNode::convert() const
{
    Theme theme;

    tryLoadColor(*this, theme.panelForegroundColor, "panelForegroundColor");
    tryLoadColor(*this, theme.worldBackgroundColor, "worldBackgroundColor");
    tryLoadColor(*this, theme.backgroundColor, "backgroundColor");
    tryLoadColor(*this, theme.altBackgroundColor, "altBackgroundColor");
    tryLoadColor(*this, theme.softIndentColor, "softIndentColor");
    tryLoadColor(*this, theme.indentColor, "indentColor");
    tryLoadColor(*this, theme.outdentColor, "outdentColor");
    tryLoadColor(*this, theme.accentColor, "accentColor");
    tryLoadColor(*this, theme.activeTextColor, "activeTextColor");
    tryLoadColor(*this, theme.textColor, "textColor");
    tryLoadColor(*this, theme.inactiveTextColor, "inactiveTextColor");
    tryLoadColor(*this, theme.warningColor, "warningColor");
    tryLoadColor(*this, theme.errorColor, "errorColor");
    tryLoadColor(*this, theme.dividerColor, "dividerColor");
    tryLoadColor(*this, theme.positiveColor, "positiveColor");
    tryLoadColor(*this, theme.negativeColor, "negativeColor");
    tryLoadColor(*this, theme.keywordColor, "keywordColor");
    tryLoadColor(*this, theme.stdlibColor, "stdlibColor");
    tryLoadColor(*this, theme.bracketColor, "bracketColor");
    tryLoadColor(*this, theme.quoteColor, "quoteColor");
    tryLoadColor(*this, theme.escapedColor, "escapedColor");
    tryLoadColor(*this, theme.stringColor, "stringColor");
    tryLoadColor(*this, theme.numberColor, "numberColor");
    tryLoadColor(*this, theme.cursorColor, "cursorColor");
    tryLoadColor(*this, theme.bracketHighlightColor, "bracketHighlightColor");
    tryLoadColor(*this, theme.xAxisColor, "xAxisColor");
    tryLoadColor(*this, theme.yAxisColor, "yAxisColor");
    tryLoadColor(*this, theme.zAxisColor, "zAxisColor");
    tryLoadColor(*this, theme.gridColor, "gridColor");
    tryLoadColor(*this, theme.gridSecondaryColor, "gridSecondaryColor");
    tryLoadColor(*this, theme.touchInterfaceColor, "touchInterfaceColor");
    tryLoadColor(*this, theme.touchInterfaceActiveColor, "touchInterfaceActiveColor");
    tryLoadColor(*this, theme.accentGradientXStartColor, "accentGradientXStartColor");
    tryLoadColor(*this, theme.accentGradientYStartColor, "accentGradientYStartColor");
    tryLoadColor(*this, theme.accentGradientXEndColor, "accentGradientXEndColor");
    tryLoadColor(*this, theme.accentGradientYEndColor, "accentGradientYEndColor");

    return theme;
}

template<>
void YAMLSerializer::emit(Theme v)
{
    startMapping();

    emitPair("panelForegroundColor", theme.panelForegroundColor.toString(true));
    emitPair("worldBackgroundColor", theme.worldBackgroundColor.toString(true));
    emitPair("backgroundColor", theme.backgroundColor.toString(true));
    emitPair("altBackgroundColor", theme.altBackgroundColor.toString(true));
    emitPair("softIndentColor", theme.softIndentColor.toString(true));
    emitPair("indentColor", theme.indentColor.toString(true));
    emitPair("outdentColor", theme.outdentColor.toString(true));
    emitPair("accentColor", theme.accentColor.toString(true));
    emitPair("activeTextColor", theme.activeTextColor.toString(true));
    emitPair("textColor", theme.textColor.toString(true));
    emitPair("inactiveTextColor", theme.inactiveTextColor.toString(true));
    emitPair("warningColor", theme.warningColor.toString(true));
    emitPair("errorColor", theme.errorColor.toString(true));
    emitPair("dividerColor", theme.dividerColor.toString(true));
    emitPair("positiveColor", theme.positiveColor.toString(true));
    emitPair("negativeColor", theme.negativeColor.toString(true));
    emitPair("keywordColor", theme.keywordColor.toString(true));
    emitPair("stdlibColor", theme.stdlibColor.toString(true));
    emitPair("bracketColor", theme.bracketColor.toString(true));
    emitPair("quoteColor", theme.quoteColor.toString(true));
    emitPair("escapedColor", theme.escapedColor.toString(true));
    emitPair("stringColor", theme.stringColor.toString(true));
    emitPair("numberColor", theme.numberColor.toString(true));
    emitPair("cursorColor", theme.cursorColor.toString(true));
    emitPair("bracketHighlightColor", theme.bracketHighlightColor.toString(true));
    emitPair("xAxisColor", theme.xAxisColor.toString(true));
    emitPair("yAxisColor", theme.yAxisColor.toString(true));
    emitPair("zAxisColor", theme.zAxisColor.toString(true));
    emitPair("gridColor", theme.gridColor.toString(true));
    emitPair("gridSecondaryColor", theme.gridSecondaryColor.toString(true));
    emitPair("touchInterfaceColor", theme.touchInterfaceColor.toString(true));
    emitPair("touchInterfaceActiveColor", theme.touchInterfaceActiveColor.toString(true));
    emitPair("accentGradientXStartColor", theme.accentGradientXStartColor.toString(true));
    emitPair("accentGradientYStartColor", theme.accentGradientYStartColor.toString(true));
    emitPair("accentGradientXEndColor", theme.accentGradientXEndColor.toString(true));
    emitPair("accentGradientYEndColor", theme.accentGradientYEndColor.toString(true));

    endMapping();
}

Theme theme;
