/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "node_editor.hpp"
#include "tools.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "node_editor_node.hpp"
#include "node_editor_link.hpp"
#include "node_editor_add_dialog.hpp"
#include "control_context.hpp"

static constexpr i32 viewStep = 32;
static constexpr i32 maxUndoHistory = 1024;

NodeEditorState::NodeEditorState()
{

}

NodeEditorState::NodeEditorState(const ScriptNodeFormat& nodes)
    : NodeEditorState()
{
    this->nodes = nodes;
}

NodeEditor::NodeEditor(
    InterfaceWidget* parent,
    const function<ScriptNodeFormat()>& source,
    const function<void(const ScriptNodeFormat&)>& onSave
)
    : InterfaceWidget(parent)
    , source(source)
    , onSave(onSave)
    , _saved(true)
    , transformMode(Node_Transform_None)
    , transformSubMode(Node_Transform_Sub_None)
    , selecting(false)
    , panning(false)
{
    START_WIDGET_SCOPE("node-editor")
        WIDGET_SLOT("remove-link", removeLink)
        WIDGET_SLOT("select-all", selectAll)
        WIDGET_SLOT("deselect-all", deselectAll)
        WIDGET_SLOT("invert-selection", invertSelection)
        WIDGET_SLOT("duplicate", duplicate)
        WIDGET_SLOT("cut", cut)
        WIDGET_SLOT("copy", copy)
        WIDGET_SLOT("paste", paste)
        WIDGET_SLOT("save", save)
        WIDGET_SLOT("reload", reload)
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("pan-left", panLeft)
        WIDGET_SLOT("pan-right", panRight)
        WIDGET_SLOT("add-new", addNew)
        WIDGET_SLOT("remove-selected", removeSelected)
        WIDGET_SLOT("start-select", startSelect)
        WIDGET_SLOT("start-pan", startPan)
        WIDGET_SLOT("translate", translate)

        START_SCOPE("targeting-link-connector", isTargetingLinkConnector())
            WIDGET_SLOT("interact", interact)
        END_SCOPE

        START_INDEPENDENT_SCOPE("selecting", selecting)
            WIDGET_SLOT("move-select", moveSelect)
            WIDGET_SLOT("end-replace-select", endReplaceSelect)
            WIDGET_SLOT("end-add-select", endAddSelect)
            WIDGET_SLOT("end-remove-select", endRemoveSelect)
        END_SCOPE

        START_INDEPENDENT_SCOPE("panning", panning)
            WIDGET_SLOT("pan", pan)
            WIDGET_SLOT("end-pan", endPan)
        END_SCOPE

        START_BLOCKING_SCOPE("tool", transformMode != Node_Transform_None)
            WIDGET_SLOT("toggle-x", toggleX)
            WIDGET_SLOT("toggle-y", toggleY)
            WIDGET_SLOT("use-tool", useTool)
            WIDGET_SLOT("cancel-tool", cancelTool)
            WIDGET_SLOT("end-tool", endTool)
        END_SCOPE
    END_SCOPE

    states.push_back(NodeEditorState());

    updateChildren();
}

bool NodeEditor::saved() const
{
    return _saved;
}

void NodeEditor::set(const ScriptNodeFormat& nodes)
{
    setState([&](NodeEditorState& state) -> void {
        state.nodes = nodes;
        state.view.x = 0;
        state.view.y = 0;
        state.selection.clear();
    });
    _saved = true;
}

void NodeEditor::notifyReload()
{
    vector<InputScopeBinding> bindings = context()->input()->getBindings("reload");

    string bindingsSummary;

    for (size_t i = 0; i < bindings.size(); i++)
    {
        bindingsSummary += bindings.at(i).toString();

        if (i + 1 != bindings.size())
        {
            bindingsSummary += ", ";
        }
    }

    statusMessage = localize("node-editor-source-has-changed", bindingsSummary);
}

void NodeEditor::display(const string& message)
{
    statusMessage = message;
}

void NodeEditor::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.indentColor
    );

    if (transformMode == Node_Transform_Link)
    {
        drawCurve(
            ctx,
            this,
            connectStartScreenPosition,
            Point(connectEndScreenPosition.x, connectStartScreenPosition.y),
            Point(connectStartScreenPosition.x, connectEndScreenPosition.y),
            connectEndScreenPosition,
            UIScale::scriptNodeLinkWidth(),
            theme.accentColor
        );
    }

    // Forces draw order links, unselected, selected
    for (const InterfaceWidget* child : children())
    {
        if (!dynamic_cast<const NodeEditorLink*>(child))
        {
            continue;
        }

        child->draw(ctx);
    }

    for (const InterfaceWidget* child : children())
    {
        auto node = dynamic_cast<const NodeEditorNode*>(child);

        if (!node || node->selected())
        {
            continue;
        }

        child->draw(ctx);
    }

    for (const InterfaceWidget* child : children())
    {
        auto node = dynamic_cast<const NodeEditorNode*>(child);

        if (!node || !node->selected())
        {
            continue;
        }

        child->draw(ctx);
    }

    if (transformMode == Node_Transform_Link)
    {
        drawRoundedSolid(
            ctx,
            this,
            (connectStartScreenPosition - position()) - Point(UIScale::scriptNodeLinkConnectorHeight() / 2),
            Size(UIScale::scriptNodeLinkConnectorHeight()),
            UIScale::scriptNodeLinkConnectorHeight() / 2,
            theme.accentColor
        );
    }

    if (transformSubMode == Node_Transform_Sub_X)
    {
        drawSolid(
            ctx,
            this,
            Point(0, size().h / 2),
            Size(size().w, 1),
            theme.xAxisColor
        );
    }
    else if (transformSubMode == Node_Transform_Sub_Y)
    {
        drawSolid(
            ctx,
            this,
            Point(size().w / 2, 0),
            Size(1, size().h),
            theme.yAxisColor
        );
    }

    bool box = sqrt(sq(selectEnd.x - selectStart.x) + sq(selectEnd.y - selectStart.y)) >= UIScale::minBoxSelectSize();

    if (selecting && box)
    {
        i32 minX = min(selectStart.x, selectEnd.x);
        i32 minY = min(selectStart.y, selectEnd.y);
        i32 maxX = max(selectStart.x, selectEnd.x);
        i32 maxY = max(selectStart.y, selectEnd.y);

        minX -= position().x;
        minY -= position().y;
        maxX -= position().x;
        maxY -= position().y;

        drawBorder(
            ctx,
            this,
            Point(minX, minY),
            Size(maxX - minX, maxY - minY),
            UIScale::marginSize(),
            theme.accentColor
        );
    }

    drawStatusLine(ctx);
}

const NodeEditorState& NodeEditor::state() const
{
    return states.back();
}

void NodeEditor::setState(const function<void(NodeEditorState&)>& transform, bool needsRegen)
{
    NodeEditorState state = this->state();

    transform(state);

    _saved = false;
    statusMessage = "";
    states.push_back(state);
    if (static_cast<i32>(states.size()) > maxUndoHistory)
    {
        states.pop_front();
    }
    redoStates.clear();

    if (needsRegen)
    {
        updateChildren();
    }
}

void NodeEditor::makeSelections(SelectMode mode)
{
    if (mode == Select_Replace)
    {
        states.back().selection.clear();
    }

    bool box = sqrt(sq(selectEnd.x - selectStart.x) + sq(selectEnd.y - selectStart.y)) >= UIScale::minBoxSelectSize();

    if (box)
    {
        i32 minX = min(selectStart.x, selectEnd.x);
        i32 minY = min(selectStart.y, selectEnd.y);
        i32 maxX = max(selectStart.x, selectEnd.x);
        i32 maxY = max(selectStart.y, selectEnd.y);

        for (const shared_ptr<ScriptNode>& node : state().nodes.allNodes())
        {
            Point screenPosition = node->screenPosition(position().toVec2(), state().view, size().toVec2());

            if (screenPosition.x >= minX && screenPosition.y >= minY && screenPosition.x < maxX && screenPosition.y < maxY)
            {
                if (mode == Select_Remove)
                {
                    states.back().selection.erase(node->id());
                }
                else
                {
                    states.back().selection.insert(node->id());
                }
            }
        }
    }
    else
    {
        for (const shared_ptr<ScriptNode>& node : state().nodes.allNodes())
        {
            Point screenPosition = node->screenPosition(position().toVec2(), state().view, size().toVec2());

            i32 minX = screenPosition.x;
            i32 minY = screenPosition.y;
            i32 maxX = screenPosition.x + node->size(context()->controller()->game().context()).x;
            i32 maxY = screenPosition.y + node->size(context()->controller()->game().context()).y;

            if (selectEnd.x >= minX && selectEnd.x < maxX && selectEnd.y >= minY && selectEnd.y < maxY)
            {
                if (mode == Select_Remove)
                {
                    states.back().selection.erase(node->id());
                }
                else
                {
                    states.back().selection.insert(node->id());
                }
                break;
            }
        }
    }
}

bool NodeEditor::isTargetingLinkConnector() const
{
    for (const InterfaceWidget* child : children())
    {
        auto node = dynamic_cast<const NodeEditorNode*>(child);

        if (!node)
        {
            continue;
        }

        Point local = pointer - node->position();

        auto [ inputs, outputs ] = node->interactionPoints();

        for (const Point& input : inputs)
        {
            if (input.toVec2().distance(local.toVec2()) <= UIScale::scriptNodeLinkConnectorHeight() / 2)
            {
                return true;
            }
        }

        for (const Point& output : outputs)
        {
            if (output.toVec2().distance(local.toVec2()) <= UIScale::scriptNodeLinkConnectorHeight() / 2)
            {
                return true;
            }
        }
    }

    return false;
}

void NodeEditor::startLink(ScriptNodeID id, bool output, size_t index, const Point& position)
{
    startTransform(Node_Transform_Link, pointer);
    connectStartNodeID = id;
    connectStartIsOutput = output;
    connectStartSlotIndex = index;
    connectStartScreenPosition = position;
    connectEndScreenPosition = position;
}

void NodeEditor::makeLink()
{
    for (const InterfaceWidget* child : children())
    {
        auto node = dynamic_cast<const NodeEditorNode*>(child);

        if (!node)
        {
            continue;
        }

        if (node->id() == connectStartNodeID)
        {
            continue;
        }

        Point local = pointer - node->position();

        auto [ inputs, outputs ] = node->interactionPoints();

        if (connectStartIsOutput)
        {
            size_t i = 0;
            for (const Point& input : inputs)
            {
                if (input.toVec2().distance(local.toVec2()) <= UIScale::scriptNodeLinkConnectorHeight() / 2)
                {
                    if (connectStartIsOutput)
                    {
                        makeLink(connectStartNodeID, connectStartSlotIndex, node->id(), i);
                    }
                    else
                    {
                        makeLink(node->id(), i, connectStartNodeID, connectStartSlotIndex);
                    }
                    return;
                }
                i++;
            }
        }
        else
        {
            size_t i = 0;
            for (const Point& output : outputs)
            {
                if (output.toVec2().distance(local.toVec2()) <= UIScale::scriptNodeLinkConnectorHeight() / 2)
                {
                    if (connectStartIsOutput)
                    {
                        makeLink(connectStartNodeID, connectStartSlotIndex, node->id(), i);
                    }
                    else
                    {
                        makeLink(node->id(), i, connectStartNodeID, connectStartSlotIndex);
                    }
                    return;
                }
                i++;
            }
        }
    }
}

void NodeEditor::makeLink(ScriptNodeID srcID, size_t srcSlotIndex, ScriptNodeID dstID, size_t dstSlotIndex)
{
    if (isCircular(srcID, dstID))
    {
        return;
    }

    setState([this, srcID, dstID, dstSlotIndex](NodeEditorState& state) -> void {
        shared_ptr<ScriptNode> dst = state.nodes.find(dstID);

        shared_ptr<ScriptNode> nextSrc = state.nodes.find(srcID);

        if (any_of(
            state.nodes.nodes().begin(),
            state.nodes.nodes().end(),
            [srcID](const shared_ptr<ScriptNode>& node) -> bool { return node->id() == srcID; }
        ))
        {
            state.nodes.remove(srcID);
        }

        shared_ptr<ScriptNode> previousSrc = dst->getInputNode(dstSlotIndex);

        if (previousSrc)
        {
            dst->clearInputNode(context()->controller()->game().context(), dstSlotIndex);

            if (!state.nodes.find(previousSrc->id()))
            {
                state.nodes.add(previousSrc);
            }
        }

        dst->setInputNode(dstSlotIndex, nextSrc);
    });
}

void NodeEditor::removeLink(ScriptNodeID srcID, size_t srcSlotIndex, ScriptNodeID dstID, size_t dstSlotIndex)
{
    setState([this, srcID, dstID, dstSlotIndex](NodeEditorState& state) -> void {
        shared_ptr<ScriptNode> src = state.nodes.find(srcID);
        shared_ptr<ScriptNode> dst = state.nodes.find(dstID);

        dst->clearInputNode(context()->controller()->game().context(), dstSlotIndex);

        if (!state.nodes.find(src->id()))
        {
            state.nodes.add(src);
        }
    });
}

void NodeEditor::transform(const Point& pointer)
{
    switch (transformMode)
    {
    default :
    case Node_Transform_None :
        return;
    case Node_Transform_Translate :
    {
        Point delta = pointer - initialPointer;

        if (transformSubMode == Node_Transform_Sub_X)
        {
            delta.y = 0;
        }
        else if (transformSubMode == Node_Transform_Sub_Y)
        {
            delta.x = 0;
        }

        translate(delta);
        break;
    }
    case Node_Transform_Link :
    {
        Point delta = pointer - initialPointer;

        if (transformSubMode == Node_Transform_Sub_X)
        {
            delta.y = 0;
        }
        else if (transformSubMode == Node_Transform_Sub_Y)
        {
            delta.x = 0;
        }

        connect(delta);
        break;
    }
    }
}

void NodeEditor::translate(const Point& delta)
{
    setState([this, delta](NodeEditorState& state) -> void {
        state.nodes.traverse([this, &state, delta](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
            if (state.selection.contains(node->id()))
            {
                node->setPosition(initialNodes.find(node->id())->position() + ivec2(delta.x, delta.y));
            }
        });
    });
}

void NodeEditor::connect(const Point& delta)
{
    connectEndScreenPosition = initialPointer + delta;
}

void NodeEditor::startTransform(NodeTransformMode mode, const Point& pointer)
{
    transformMode = mode;
    transformSubMode = Node_Transform_Sub_None;
    initialPointer = pointer;
    initialNodes = state().nodes;
}

void NodeEditor::endTransform()
{
    switch (transformMode)
    {
    case Node_Transform_None :
    case Node_Transform_Translate :
        break;
    case Node_Transform_Link :
        makeLink();
        break;
    }

    initialNodes = ScriptNodeFormat();
    transformMode = Node_Transform_None;
    transformSubMode = Node_Transform_Sub_None;
}

void NodeEditor::cancelTransform()
{
    translate(Point());
    endTransform();
}

void NodeEditor::updateChildren()
{
    clearChildren();

    map<ScriptNodeID, NodeEditorNode*> createdNodes;

    for (const shared_ptr<ScriptNode>& node : state().nodes.nodes())
    {
        addChild(node, &createdNodes);
    }
}

NodeEditorNode* NodeEditor::addChild(shared_ptr<ScriptNode> node, map<ScriptNodeID, NodeEditorNode*>* createdNodes)
{
    if (auto it = createdNodes->find(node->id()); it != createdNodes->end())
    {
        return it->second;
    }

    auto child = new NodeEditorNode(this, node->id());

    createdNodes->insert({ node->id(), child });

    vector<Point> inputPoints = get<0>(child->interactionPoints());

    size_t i = 0;
    for (const shared_ptr<ScriptNode>& input : node->inputNodes())
    {
        if (!input)
        {
            i++;
            continue;
        }

        NodeEditorNode* inputNode = addChild(input, createdNodes);

        new NodeEditorLink(this, inputNode, 0, child, i);
        i++;
    }

    return child;
}

void NodeEditor::drawStatusLine(const DrawContext& ctx) const
{
    TextStyle style;

    i32 height = style.getFont()->height();

    drawSolid(
        ctx,
        this,
        Point(0, size().h - height),
        Size(size().w, height),
        theme.indentColor
    );

    drawText(
        ctx,
        this,
        Point(0, size().h - height),
        Size(size().w, height),
        statusMessage + (_saved ? "" : " (" + localize("node-editor-unsaved") + ")"),
        style
    );
}

void NodeEditor::interact(const Input& input)
{
    for (const InterfaceWidget* child : children())
    {
        auto node = dynamic_cast<const NodeEditorNode*>(child);

        if (!node)
        {
            continue;
        }

        Point local = pointer - node->position();

        auto [ inputs, outputs ] = node->interactionPoints();

        size_t i = 0;
        for (const Point& input : inputs)
        {
            if (input.toVec2().distance(local.toVec2()) <= UIScale::scriptNodeLinkConnectorHeight() / 2)
            {
                startLink(node->id(), false, i, node->position() + input);
                return;
            }
            i++;
        }

        i = 0;
        for (const Point& output : outputs)
        {
            if (output.toVec2().distance(local.toVec2()) <= UIScale::scriptNodeLinkConnectorHeight() / 2)
            {
                startLink(node->id(), true, i, node->position() + output);
                return;
            }
            i++;
        }
    }
}

void NodeEditor::removeLink(const Input& input)
{
    for (const InterfaceWidget* child : children())
    {
        auto link = dynamic_cast<const NodeEditorLink*>(child);

        if (!link)
        {
            continue;
        }

        if (link->curveContains(pointer))
        {
            removeLink(link->srcID(), link->srcSlotIndex(), link->dstID(), link->dstSlotIndex());
            return;
        }
    }
}

void NodeEditor::selectAll(const Input& input)
{
    for (const shared_ptr<ScriptNode>& node : state().nodes.allNodes())
    {
        states.back().selection.insert(node->id());
    }
}

void NodeEditor::deselectAll(const Input& input)
{
    states.back().selection.clear();
}

void NodeEditor::invertSelection(const Input& input)
{
    std::set<ScriptNodeID> newSelection;

    for (const shared_ptr<ScriptNode>& node : state().nodes.allNodes())
    {
        if (state().selection.contains(node->id()))
        {
            continue;
        }

        newSelection.insert(node->id());
    }

    states.back().selection = newSelection;
}

void NodeEditor::duplicate(const Input& input)
{
    std::set<shared_ptr<ScriptNode>> buffer;

    auto findInBuffer = [&buffer](ScriptNodeID id) -> shared_ptr<ScriptNode>
    {
        shared_ptr<ScriptNode> result = nullptr;

        for (const shared_ptr<ScriptNode>& node : buffer)
        {
            if (node->id() == id)
            {
                result = node;
                break;
            }

            node->traverse(node, [&](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
                if (node->id() == id)
                {
                    result = node;
                }
            });
        }

        return result;
    };

    state().nodes.traverse([&](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
        if (!state().selection.contains(node->id()))
        {
            return;
        }

        if (parent)
        {
            if (shared_ptr<ScriptNode> copiedParent = findInBuffer(parent->id()); copiedParent)
            {
                copiedParent->setInputNode(index, node->copyWithoutChildren());
            }
            else
            {
                buffer.insert(node->copyWithoutChildren());
            }
        }
        else
        {
            buffer.insert(node->copyWithoutChildren());
        }
    });

    setState([&](NodeEditorState& state) -> void {
        state.selection.clear();

        for (const shared_ptr<ScriptNode>& node : buffer)
        {
            state.selection = join(state.selection, state.nodes.add(node));
        }
    });

    startTransform(Node_Transform_Translate, pointer);
}

void NodeEditor::cut(const Input& input)
{
    copyView = state().view;
    copyBuffer.clear();

    state().nodes.traverse([&](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
        if (!state().selection.contains(node->id()))
        {
            return;
        }

        if (parent)
        {
            if (shared_ptr<ScriptNode> copiedParent = findInCopyBuffer(parent->id()); copiedParent)
            {
                copiedParent->setInputNode(index, node->copyWithoutChildren());
            }
            else
            {
                copyBuffer.insert(node->copyWithoutChildren());
            }
        }
        else
        {
            copyBuffer.insert(node->copyWithoutChildren());
        }
    });

    setState([&](NodeEditorState& state) -> void {
        for (ScriptNodeID id : state.selection)
        {
            state.nodes.remove(id);
        }

        state.selection.clear();
    });
}

void NodeEditor::copy(const Input& input)
{
    copyView = state().view;
    copyBuffer.clear();

    state().nodes.traverse([&](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
        if (!state().selection.contains(node->id()))
        {
            return;
        }

        if (parent)
        {
            if (shared_ptr<ScriptNode> copiedParent = findInCopyBuffer(parent->id()); copiedParent)
            {
                copiedParent->setInputNode(index, node->copyWithoutChildren());
            }
            else
            {
                copyBuffer.insert(node->copyWithoutChildren());
            }
        }
        else
        {
            copyBuffer.insert(node->copyWithoutChildren());
        }
    });
}

void NodeEditor::paste(const Input& input)
{
    setState([&](NodeEditorState& state) -> void {
        state.selection.clear();

        for (const shared_ptr<ScriptNode>& node : copyBuffer)
        {
            shared_ptr<ScriptNode> copy = node->copyWithChildren();

            copy->setPosition(state.view + (copy->position() - copyView));

            state.selection = join(state.selection, state.nodes.add(copy));
        }
    });

    startTransform(Node_Transform_Translate, pointer);
}

void NodeEditor::save(const Input& input)
{
    _saved = true;
    onSave(state().nodes);
}

void NodeEditor::reload(const Input& input)
{
    if (source)
    {
        set(source());
    }
}

void NodeEditor::undo(const Input& input)
{
    if (static_cast<i32>(states.size()) == 1)
    {
        return;
    }

    _saved = false;
    statusMessage = "";
    redoStates.push_back(states.back());
    states.pop_back();
}

void NodeEditor::redo(const Input& input)
{
    if (redoStates.empty())
    {
        return;
    }

    _saved = false;
    statusMessage = "";
    states.push_back(redoStates.back());
    redoStates.pop_back();
}

void NodeEditor::scrollUp(const Input& input)
{
    states.back().view.y += viewStep;
}

void NodeEditor::scrollDown(const Input& input)
{
    states.back().view.y -= viewStep;
}

void NodeEditor::panLeft(const Input& input)
{
    states.back().view.x -= viewStep;
}

void NodeEditor::panRight(const Input& input)
{
    states.back().view.x += viewStep;
}

void NodeEditor::addNew(const Input& input)
{
    context()->interface()->addWindow<NodeEditorAddDialog>([this](ScriptNodeType type, const string& name) -> void {
        setState([this, type, name](NodeEditorState& state) -> void {
            Point screenOffset = pointer - (position() + size() / 2);

            ivec2 position = state.view + ivec2(screenOffset.x, screenOffset.y);

            state.selection = { state.nodes.addNew(context()->controller()->game().context(), type, name, position) };
        });

        startTransform(Node_Transform_Translate, pointer);
    });
}

void NodeEditor::removeSelected(const Input& input)
{
    setState([&](NodeEditorState& state) -> void {
        for (ScriptNodeID id : state.selection)
        {
            shared_ptr<ScriptNode> node = state.nodes.find(id);

            if (!node)
            {
                continue;
            }

            state.nodes.remove(id);

            std::set<ScriptNodeID> addedNodeIDs;

            for (const shared_ptr<ScriptNode>& input : node->inputNodes())
            {
                if (!input)
                {
                    continue;
                }

                if (!addedNodeIDs.contains(input->id()) && !state.nodes.find(input->id()))
                {
                    state.nodes.add(input);
                    addedNodeIDs.insert(input->id());
                }
            }
        }

        state.selection.clear();
    });
}

void NodeEditor::startSelect(const Input& input)
{
    selectStart = pointer;
    selectEnd = pointer;
    selecting = true;
}

void NodeEditor::startPan(const Input& input)
{
    panStart = pointer;
    panStartView = state().view;
    panning = true;
}

void NodeEditor::translate(const Input& input)
{
    startTransform(Node_Transform_Translate, pointer);
}

void NodeEditor::moveSelect(const Input& input)
{
    selectEnd = pointer;
}

void NodeEditor::endReplaceSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Replace);
}

void NodeEditor::endAddSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Add);
}

void NodeEditor::endRemoveSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Remove);
}

void NodeEditor::pan(const Input& input)
{
    vec2 delta = (pointer - panStart).toVec2();

    delta *= -1;

    states.back().view = panStartView + delta;
}

void NodeEditor::endPan(const Input& input)
{
    panning = false;
}

void NodeEditor::toggleX(const Input& input)
{
    transformSubMode = transformSubMode != Node_Transform_Sub_X
        ? Node_Transform_Sub_X
        : Node_Transform_Sub_None;
}

void NodeEditor::toggleY(const Input& input)
{
    transformSubMode = transformSubMode != Node_Transform_Sub_Y
        ? Node_Transform_Sub_Y
        : Node_Transform_Sub_None;
}

void NodeEditor::useTool(const Input& input)
{
    transform(pointer);
}

void NodeEditor::cancelTool(const Input& input)
{
    cancelTransform();
}

void NodeEditor::endTool(const Input& input)
{
    endTransform();
}

shared_ptr<ScriptNode> NodeEditor::findInCopyBuffer(ScriptNodeID id)
{
    shared_ptr<ScriptNode> result = nullptr;

    for (const shared_ptr<ScriptNode>& node : copyBuffer)
    {
        if (node->id() == id)
        {
            result = node;
            break;
        }

        node->traverse(node, [&](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
            if (node->id() == id)
            {
                result = node;
            }
        });
    }

    return result;
}

bool NodeEditor::isCircular(ScriptNodeID srcID, ScriptNodeID dstID) const
{
    shared_ptr<ScriptNode> src = state().nodes.find(srcID);
    shared_ptr<ScriptNode> dst = state().nodes.find(dstID);

    bool circular = false;

    src->traverse(src, [dst, &circular](shared_ptr<ScriptNode> parent, size_t index, shared_ptr<ScriptNode> node) -> void {
        if (node->id() == dst->id())
        {
            circular = true;
        }
    });

    return circular;
}

SizeProperties NodeEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
