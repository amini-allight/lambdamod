/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "vr_interface.hpp"
#include "vr_input.hpp"

#include <vulkan/vulkan.h>
#define XR_USE_GRAPHICS_API_VULKAN
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>

class VR final : public VRInterface
{
public:
    VR(Controller* controller);
    ~VR();

    void requestExit() override;

    bool step() override;

    void setRoom(const vec3& position, const quaternion& rotation) override;
    void resetRoom() override;
    vec3 roomToWorld(const vec3& v) const override;
    quaternion roomToWorld(const quaternion& q) const override;
    vec3 worldToRoom(const vec3& v) const override;
    quaternion worldToRoom(const quaternion& q) const override;

    bool running() const override;
    bool shouldRender() const override;

    InputInterface* input() const override;

private:
    friend class VRRender;
    friend class VRInput;

    void initSession(
        VkInstance instance,
        VkPhysicalDevice physDevice,
        VkDevice device,
        u32 queueFamilyIndex
    );
    void quitSession();

    XrInstance instance() const;
    XrSystemId systemID() const;
    XrSession session() const;
    XrSpace roomSpace() const;
    XrTime predictedDisplayTime() const;
    bool userPresence() const;
    bool eyeTracking() const;
    bool handTracking() const;
    bool htcFacialEyeTracking() const;
    bool htcFacialLipTracking() const;
    bool htcBodyTracking() const;
    u32 htcFullBodyTracking() const;
    bool htcViveCosmos() const;
    bool htcViveFocus3() const;
    bool htcHandTracking() const;
    bool microsoftHandTracking() const;
    bool hpMixedReality() const;
    bool huawei() const;
    bool magicLeap2() const;
    u32 pico() const;
    bool questTouchPro() const;
    bool questTouchPlus() const;
    bool oppo() const;
    bool yvr() const;
    bool varjoXR4() const;
    u32 metaFaceTracking() const;
    bool metaBodyTracking() const;
    bool bdBodyTracking() const;
    bool monado() const;
    bool steamvr() const;

private:
    Controller* controller;
    XrInstance _instance;
#ifdef LMOD_XR_VALIDATION
    XrDebugUtilsMessengerEXT debugMessenger;
#endif
    XrSystemId _systemID;
    XrSession _session;
    XrSpace _roomSpace;
    bool _running;
    bool exiting;

    bool _userPresence;
    bool _eyeTracking;
    bool _handTracking;
    bool _htcViveCosmos;
    bool _htcViveFocus3;
    bool _htcFaceTracking;
    bool _htcHandTracking;
    bool _htcBodyTracking;
    u32 _htcFullBodyTracking;
    bool _microsoftHandTracking;
    bool _hpMixedReality;
    bool _huawei;
    bool _magicLeap2;
    u32 _pico;
    bool _questTouchPro;
    bool _questTouchPlus;
    bool _oppo;
    bool _yvr;
    bool _varjoXR4;
    u32 _metaFaceTracking;
    bool _metaBodyTracking;
    bool _bdBodyTracking;
    bool _monado;
    bool _steamvr;

    string leftInteractionProfile;
    string rightInteractionProfile;

    // The above flag is if the extension is available, this one is if the needed controllers/etc. are available
    bool userPresenceAvailable;
    bool handTrackingAvailable;
    bool htcFacialEyeTrackingAvailable;
    bool htcFacialLipTrackingAvailable;
    bool htcBodyTrackingAvailable;
    bool metaFaceTrackingAvailable;
    bool metaBodyTrackingAvailable;
    bool bdBodyTrackingAvailable;

    // Tracking user presence in headset with XR_EXT_user_presence, separate from the XrSession state
    bool userPresent;

    XrFrameState nextFrameState;

    mat4 roomTransform;

    VRInput* _input;
};
#endif
