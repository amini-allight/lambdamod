/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_source.hpp"
#include "sound_constants.hpp"
#include "sound.hpp"
#include "controller.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "frequency_splitting.hpp"
#include "fourier_transform.hpp"
#include "resample.hpp"

// TODO: SOUND: What happens when this (mainly the delay and reverb buffers) runs backwards due to time multiplier

static constexpr size_t chunkLengthWithoutTail = SoundOverlappedChunk::length() - soundChunkOverlap;
static constexpr f64 retainedChunkFraction = soundChunkSamplesPerChannel / static_cast<f64>(chunkLengthWithoutTail);

SoundSource::SoundSource(
    Sound* sound,
    const vec3& position,
    const quaternion& rotation,
    bool directional,
    f64 angle,
    bool canBeSpatial
)
    : sound(sound)
    , position(position)
    , rotation(rotation)
    , directional(directional)
    , angle(angle)
    , canBeSpatial(canBeSpatial)
{
    for (u32 ear = 0; ear < earCount; ear++)
    {
        f64 distance = min(sound->earPosition(ear).distance(position), maxSoundDistance);
        f64 delay = distance / sound->speedOfSound;

        delayedSamples[ear].resize(soundChunkOverlap + ceil(delay * soundFrequency), 0);
        delayedSamplesWriteHead[ear] = soundChunkOverlap + ceil(delay * soundFrequency);

        lastDistance[ear] = distance;
    }
    lastDistanceUpdateTime = sound->controller->lastStepTime();
}

SoundSource::~SoundSource()
{

}

bool SoundSource::depleted() const
{
    return roughly(reverbEnergy, 0.0) && roughly(delayedEnergy[0], 0.0) && roughly(delayedEnergy[1], 0.0);
}

void SoundSource::output(f64 multiplier)
{
    variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> chunk = pull(multiplier);

    filter(chunk);

    sound->output(chunk);
}

void SoundSource::recordLastDistance()
{
    for (u32 ear = 0; ear < earCount; ear++)
    {
        lastDistance[ear] = min(sound->earPosition(ear).distance(position), maxSoundDistance);
    }
    lastDistanceUpdateTime = sound->controller->lastStepTime();
}

void SoundSource::filter(variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk)
{
    if (holds_alternative<SoundOverlappedChunk>(chunk) && canBeSpatial)
    {
        spatialize(chunk);
    }
    else
    {
        stabilizeVolume(chunk);
    }
}

void SoundSource::spatialize(variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk)
{
    vector<f32> mono(get<SoundOverlappedChunk>(chunk).length());
    memcpy(mono.data(), get<SoundOverlappedChunk>(chunk).samples(), mono.size() * sizeof(f32));

    vector<vector<f32>> channels(earCount, vector<f32>(mono.size(), 0));

    if (roughly<f64>(sound->speedOfSound, 0))
    {
        chunk = SoundOverlappedStereoChunk();
        return;
    }

    // Reverb
    pushReverb(mono);

    for (u32 ear = 0; ear < earCount; ear++)
    {
        vec3 earPosition = sound->earPosition(ear);
        vec3 toSound = (position - earPosition).normalize();

        // Delay & doppler
        pushSamples(ear, earPosition, mono);

        channels[ear] = pullSamples(ear);

        // Reverb
        peekReverb(channels[ear]);

        // Directional emission
        applyDirectionality(channels[ear], earPosition);

        // Split for processing because the split is quite compute-intensive
        auto [ lo, hi ] = splitFrequencies(
            channels[ear],
            soundTransitionBandStartFrequency,
            soundTransitionBandEndFrequency
        );

        // Distance falloff
        applyDistanceFalloff(lo, hi, earPosition);

        // Occlusion
        applyOcclusion(lo, hi, earPosition);

        // Panning
        vec3 localToSound = sound->microphoneTransform.applyToDirection(toSound, false);

        // Only apply panning if the sound source is not inside the head, otherwise we get strange results
        if (sound->microphoneTransform.position().distance(position) > interEarDistance / 2)
        {
            switch (g_config.soundSpatialMode)
            {
            case Sound_Spatial_Mode_Headphones :
            {
                applyHRTF(
                    lo,
                    hi,
                    ear,
                    localToSound
                );

                channels[ear] = joinFrequencies(lo, hi);
                break;
            }
            case Sound_Spatial_Mode_Speakers :
            {
                // Map to the range 0 - pi/2 where right = 0 and left = pi/2
                f64 v = abs(handedAngleBetween(rightDir, localToSound, upDir) / 2);

                f64 volume = ear == 0 ? sin(v) : cos(v);

                // Only apply to high frequencies
                for (f32& sample : hi)
                {
                    sample *= static_cast<f32>(volume);
                }

                channels[ear] = joinFrequencies(lo, hi);
                break;
            }
            }
        }
    }

    // Reverb
    popReverb();

    chunk = SoundOverlappedStereoChunk(
        SoundOverlappedChunk(channels.front().data()),
        SoundOverlappedChunk(channels.back().data())
    );
}

void SoundSource::stabilizeVolume(variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk)
{
    if (holds_alternative<SoundOverlappedChunk>(chunk))
    {
        get<SoundOverlappedChunk>(chunk).counterExpose(sound->exposure);
    }
    else
    {
        get<SoundOverlappedStereoChunk>(chunk).counterExpose(sound->exposure);
    }
}

void SoundSource::pushSamples(u32 ear, const vec3& earPosition, const vector<f32>& samples)
{
    f64 delta = min(position.distance(earPosition), maxSoundDistance) - lastDistance[ear];
    f64 elapsed = (sound->controller->lastStepTime() - lastDistanceUpdateTime).count() / 1'000'000.0;

    f64 velocity = delta / elapsed;
    f64 doppler = (sound->speedOfSound + velocity) / sound->speedOfSound;

    if (doppler >= 0)
    {
        vector<f32> centerAndTail = {
            samples.begin() + soundChunkOverlap,
            samples.end()
        };

        vector<f32> resampled = hermiteResample(centerAndTail, doppler);

        for (size_t i = 0; i < resampled.size() - soundChunkOverlap; i++)
        {
            delayedEnergy[ear] += abs(samples[i]);
        }

        delayedSamples[ear].erase(delayedSamples[ear].begin() + delayedSamplesWriteHead[ear], delayedSamples[ear].end());
        delayedSamples[ear].insert(delayedSamples[ear].end(), resampled.begin(), resampled.end());
        delayedSamplesWriteHead[ear] += ceil(resampled.size() * retainedChunkFraction);
    }
    else
    {
        // Shorten the delay buffer by more than 1 second per second when moving towards the source at supersonic velocity
        size_t removeSize = min<size_t>(abs(doppler) * soundChunkSamplesPerChannel, delayedSamples[ear].size());

        delayedSamples[ear].erase(delayedSamples[ear].begin(), delayedSamples[ear].begin() + removeSize);
        delayedSamplesWriteHead[ear] -= removeSize;
    }
}

vector<f32> SoundSource::pullSamples(u32 ear)
{
    // Prevent overread when moving towards the source at supersonic velocity
    size_t readSize = min<size_t>(SoundOverlappedChunk::length(), delayedSamples[ear].size());
    size_t removeSize = min<size_t>(soundChunkSamplesPerChannel, delayedSamples[ear].size());

    vector<f32> samples = {
        delayedSamples[ear].begin(),
        delayedSamples[ear].begin() + readSize
    };

    if (samples.size() < SoundOverlappedChunk::length())
    {
        samples.resize(SoundOverlappedChunk::length(), 0);
    }

    for (size_t i = soundChunkOverlap; i < samples.size() - soundChunkOverlap; i++)
    {
        delayedEnergy[ear] -= abs(samples[i]);
    }

    delayedSamples[ear].erase(
        delayedSamples[ear].begin(),
        delayedSamples[ear].begin() + removeSize
    );
    delayedSamplesWriteHead[ear] -= soundChunkSamplesPerChannel;

    return samples;
}

void SoundSource::pushReverb(const vector<f32>& chunk)
{
    if (!sound->controller->selfWorld())
    {
        return;
    }

    vector<ReverbEffect> effects = sound->controller->selfWorld()->soundReverb(position);

    for (const ReverbEffect& effect : effects)
    {
        // Strength already integrates one copy of distance, only apply the return distance here
        f64 amplitude = effect.strength * (1 / sq(effect.distance + 1));
        f64 delay = min(effect.distance, maxSoundDistance) / sound->speedOfSound;

        if (roughly(amplitude, 0.0))
        {
            continue;
        }

        size_t offset = soundChunkOverlap + delay * soundFrequency;

        if (offset + SoundChunk::length() > reverbSamples.size())
        {
            reverbSamples.resize(offset + SoundChunk::length(), 0);
        }

        for (size_t i = soundChunkOverlap; i < chunkLengthWithoutTail; i++)
        {
            reverbSamples[offset + i] += chunk[i] * amplitude;
            reverbEnergy += abs(chunk[i] * amplitude);
        }
    }
}

void SoundSource::peekReverb(vector<f32>& samples) const
{
    size_t readSize = min(SoundOverlappedChunk::length(), reverbSamples.size());

    for (size_t i = 0; i < readSize; i++)
    {
        samples[i] += reverbSamples[i];
    }
}

void SoundSource::popReverb()
{
    size_t removeSize = min(chunkLengthWithoutTail, reverbSamples.size());

    for (size_t i = 0; i < removeSize; i++)
    {
        reverbEnergy -= abs(reverbSamples[i]);
    }

    reverbSamples.erase(reverbSamples.begin(), reverbSamples.begin() + removeSize);
}

void SoundSource::applyDirectionality(vector<f32>& samples, const vec3& earPosition) const
{
    f64 volume = 1;

    if (directional)
    {
        f64 listenAngle = angleBetween(rotation.forward(), (earPosition - position).normalize());
        f64 angle = this->angle / 2;

        if (listenAngle > angle)
        {
            volume *= 1 - (listenAngle - angle) / (pi - angle);
        }
    }

    for (f32& sample : samples)
    {
        sample *= static_cast<f32>(volume);
    }
}

void SoundSource::applyDistanceFalloff(vector<f32>& lo, vector<f32>& hi, const vec3& earPosition) const
{
    f64 distance = position.distance(earPosition);
    f64 volume = 1 / sq(distance + 1);

    for (f32& sample : lo)
    {
        sample *= static_cast<f32>(volume);
    }

    for (f32& sample : hi)
    {
        sample *= (sound->speedOfSound / soundFalloffFactor) * static_cast<f32>(volume);
    }
}

void SoundSource::applyOcclusion(vector<f32>& lo, vector<f32>& hi, const vec3& earPosition) const
{
    if (!sound->controller->selfWorld())
    {
        return;
    }

    f32 occlusion = sound->controller->selfWorld()->soundOcclusion(earPosition, position);

    for (f32& sample : hi)
    {
        sample *= static_cast<f32>(1 - occlusion);
    }
}

void SoundSource::applyDopplerEffect(vector<f32>& samples, f64 doppler) const
{
    valarray<complex<f32>> frequencies = fft(samples);

    frequencies *= static_cast<f32>(doppler);

    samples = ifft(frequencies);
}

void SoundSource::applyHRTF(vector<f32>& lo, vector<f32>& hi, i32 ear, const vec3& dir) const
{
    sound->hrir->apply(lo, hi, ear, dir);
}
