/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_tether_indicator.hpp"
#include "render_constants.hpp"
#include "render_tools.hpp"
#include "tools.hpp"
#include "theme.hpp"

RenderVRTetherIndicator::RenderVRTetherIndicator(
    const RenderContext* ctx,

    vector<VRSwapchainElement*> swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderVRPipelineStore* pipelineStore,
    const vec3& start,
    const vec3& end,
    f64 strength
)
    : RenderVRDecoration(ctx, descriptorSetLayout, pipelineStore->tetherIndicator.pipelineLayout, pipelineStore->tetherIndicator.pipeline)
    , length(start.distance(end))
    , strength(strength)
{
    vec3 direction = (end - start).normalize();

    transform = { (end + start) / 2, quaternion(direction, pickUpDirection(direction)), vec3(1, start.distance(end), 1) };

    vertices = createVertexBuffer(2 * sizeof(fvec3));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<fvec3> vertexData = {
        fvec3(0, -0.5f, 0),
        fvec3(0, +0.5f, 0)
    };

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(fvec3));

    createComponents(swapchainElements);
}

RenderVRTetherIndicator::~RenderVRTetherIndicator()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderVRTetherIndicator::work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const
{
    RenderVRDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        2
    );
}

VmaBuffer RenderVRTetherIndicator::createUniformBuffer() const
{
    VmaBuffer uniform = RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount + sizeof(fvec4) * 2 + sizeof(f32) * 2);

    VmaMapping<fvec4> mapping(ctx->allocator, uniform);

    mapping.data[8] = theme.accentGradientYStartColor.toVec4();
    mapping.data[9] = theme.accentGradientYEndColor.toVec4();

    reinterpret_cast<f32*>(mapping.data)[40] = length;
    reinterpret_cast<f32*>(mapping.data)[41] = strength;

    return uniform;
}
#endif
