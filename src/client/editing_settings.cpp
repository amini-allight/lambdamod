/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "editing_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "ufloat_edit.hpp"
#include "vec3_edit.hpp"
#include "checkbox.hpp"

EditingSettings::EditingSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "editing-settings-viewer-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&EditingSettings::viewerPositionSource, this),
            bind(&EditingSettings::onViewerPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-hud-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&EditingSettings::hudShownSource, this),
            bind(&EditingSettings::onHUDShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-entity-points-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&EditingSettings::entityPointsShownSource, this),
            bind(&EditingSettings::onEntityPointsShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-cursor-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&EditingSettings::cursorShownSource, this),
            bind(&EditingSettings::onCursorShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-cursor-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&EditingSettings::cursorPositionSource, this),
            bind(&EditingSettings::onCursorPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-grid-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&EditingSettings::gridShownSource, this),
            bind(&EditingSettings::onGridShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-grid-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&EditingSettings::gridPositionSource, this),
            bind(&EditingSettings::onGridPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-grid-size", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&EditingSettings::gridSizeSource, this),
            bind(&EditingSettings::onGridSize, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-gizmo-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&EditingSettings::gizmoShownSource, this),
            bind(&EditingSettings::onGizmoShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-space-graph-visualizer-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&EditingSettings::spaceGraphVisualizerShownSource, this),
            bind(&EditingSettings::onSpaceGraphVisualizerShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-canvas-paint-brush-radius", pixelSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&EditingSettings::canvasPaintBrushRadiusSource, this),
            bind(&EditingSettings::onCanvasPaintBrushRadius, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-translation-step", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&EditingSettings::translationStepSource, this),
            bind(&EditingSettings::onTranslationStep, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-rotation-step", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&EditingSettings::rotationStepSource, this),
            bind(&EditingSettings::onRotationStep, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "editing-settings-scale-step", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&EditingSettings::scaleStepSource, this),
            bind(&EditingSettings::onScaleStep, this, placeholders::_1)
        );
    });
}

vec3 EditingSettings::viewerPositionSource()
{
#ifdef LMOD_VR
    if (g_vr)
    {
        return vec3();
    }
    else
    {
        return localizeLength(context()->flatViewer()->position());
    }
#else
    return localizeLength(context()->flatViewer()->position());
#endif
}

void EditingSettings::onViewerPosition(const vec3& v)
{
    context()->flatViewer()->move(delocalizeLength(v), context()->flatViewer()->rotation());
}

bool EditingSettings::hudShownSource()
{
    return context()->hudShown();
}

void EditingSettings::onHUDShown(bool state)
{
    context()->setHUDShown(state);
}

bool EditingSettings::entityPointsShownSource()
{
    return context()->viewerMode()->entityPointsShown();
}

void EditingSettings::onEntityPointsShown(bool state)
{
    context()->viewerMode()->setEntityPointsShown(state);
}

bool EditingSettings::cursorShownSource()
{
    return context()->viewerMode()->cursorShown();
}

void EditingSettings::onCursorShown(bool state)
{
    context()->viewerMode()->setCursorShown(state);
}

vec3 EditingSettings::cursorPositionSource()
{
    return localizeLength(context()->viewerMode()->cursorPosition());
}

void EditingSettings::onCursorPosition(const vec3& v)
{
    context()->viewerMode()->setCursorPosition(delocalizeLength(v));
}

bool EditingSettings::gridShownSource()
{
    return context()->viewerMode()->gridShown();
}

void EditingSettings::onGridShown(bool state)
{
    context()->viewerMode()->setGridShown(state);
}

vec3 EditingSettings::gridPositionSource()
{
    return localizeLength(context()->viewerMode()->gridPosition());
}

void EditingSettings::onGridPosition(const vec3& v)
{
    context()->viewerMode()->setGridPosition(delocalizeLength(v));
}

f64 EditingSettings::gridSizeSource()
{
    return localizeLength(context()->viewerMode()->gridSize());
}

void EditingSettings::onGridSize(f64 v)
{
    context()->viewerMode()->setGridSize(delocalizeLength(v));
}

bool EditingSettings::gizmoShownSource()
{
    return context()->viewerMode()->gizmoShown();
}

void EditingSettings::onGizmoShown(bool state)
{
    context()->viewerMode()->setGizmoShown(state);
}

bool EditingSettings::spaceGraphVisualizerShownSource() const
{
    return context()->viewerMode()->spaceGraphVisualizerShown();
}

void EditingSettings::onSpaceGraphVisualizerShown(bool state)
{
    context()->viewerMode()->setSpaceGraphVisualizerShown(state);
}

f64 EditingSettings::canvasPaintBrushRadiusSource()
{
    return context()->viewerMode()->canvasPaintBrushRadius();
}

void EditingSettings::onCanvasPaintBrushRadius(f64 v)
{
    context()->viewerMode()->setCanvasPaintBrushRadius(v);
}

f64 EditingSettings::translationStepSource()
{
    return localizeLength(context()->viewerMode()->translationStep());
}

void EditingSettings::onTranslationStep(f64 v)
{
    context()->viewerMode()->setTranslationStep(delocalizeLength(v));
}

f64 EditingSettings::rotationStepSource()
{
    return localizeAngle(context()->viewerMode()->rotationStep());
}

void EditingSettings::onRotationStep(f64 v)
{
    context()->viewerMode()->setRotationStep(delocalizeAngle(v));
}

f64 EditingSettings::scaleStepSource()
{
    return context()->viewerMode()->scaleStep();
}

void EditingSettings::onScaleStep(f64 v)
{
     context()->viewerMode()->setScaleStep(v);
}

SizeProperties EditingSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
