/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_chromatic_aberration_effect.hpp"
#include "render_tools.hpp"

RenderVRChromaticAberrationEffect::RenderVRChromaticAberrationEffect(
    const RenderContext* ctx,
    RenderVRPipelineStore* pipelineStore,
    vector<VRSwapchainElement*> swapchainElements,
    f64 strength
)
    : RenderVRPostProcessEffect(
        ctx,
        pipelineStore->chromaticAberration.pipelineLayout,
        pipelineStore->chromaticAberration.pipeline
    )
    , strength(strength)
{
    createComponents(swapchainElements, pipelineStore->postProcessDescriptorSetLayout);
}

RenderVRChromaticAberrationEffect::~RenderVRChromaticAberrationEffect()
{
    destroyComponents();
}

void RenderVRChromaticAberrationEffect::update(f64 strength)
{
    this->strength = strength;
}

VmaBuffer RenderVRChromaticAberrationEffect::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(f32) * 2,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderVRChromaticAberrationEffect::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    memcpy(mapping->data, &strength, sizeof(f32));
    memcpy(mapping->data + 1, &timer, sizeof(f32));
}
#endif
