/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "entity_mode_context.hpp"
#include "tools.hpp"
#include "localization.hpp"
#include "control_context.hpp"
#include "viewer_mode_context.hpp"
#include "confirm_dialog.hpp"
#include "entity_editor.hpp"
#include "interface_constants.hpp"
#include "apply_prefab.hpp"
#include "new_entity_dialog.hpp"
#include "prefabs.hpp"

EntityModeContext::EntityModeContext(ViewerModeContext* ctx)
    : EditModeContext(ctx)
    , copyBuffer(ctx)
{

}

EntityModeContext::~EntityModeContext()
{

}

void EntityModeContext::enter()
{
    EditModeContext::enter();
}

void EntityModeContext::exit()
{
    EditModeContext::exit();
}

void EntityModeContext::addNewPrefab(const string& name)
{
    Entity* entity;

    ctx->context()->controller()->edit([this, name, &entity]() -> void {
        World* world = ctx->context()->controller()->selfWorld();

        entity = new Entity(
            world,
            ctx->context()->controller()->game().nextEntityID()
        );
        entity->setName(name);
        entity->setOwnerID(ctx->context()->controller()->selfID());
        entity->setGlobalPosition(ctx->cursorPosition());
        prefabDefault(entity, mat4());

        entity->body() = Body();

        applyPrefabByName(entity, name, mat4());

        world->add(EntityID(), entity);
    });

    selectNew({ entity });
}

void EntityModeContext::addNew()
{
    ctx->context()->interface()->addWindow<NewEntityDialog>();
}

void EntityModeContext::addNew(const string& name)
{
    Entity* entity;

    ctx->context()->controller()->edit([this, name, &entity]() -> void {
        World* world = ctx->context()->controller()->selfWorld();

        entity = new Entity(
            world,
            ctx->context()->controller()->game().nextEntityID()
        );
        entity->setName(name);
        entity->setOwnerID(ctx->context()->controller()->selfID());
        entity->setGlobalPosition(ctx->cursorPosition());
        prefabDefault(entity, mat4());

        world->add(EntityID(), entity);
    });

    selectNew({ entity });
}

void EntityModeContext::removeSelected()
{
    string text;

    if (selectionRootsOnly().empty())
    {
        return;
    }

    size_t entityCount = 0;

    for (const Entity* entity : selectionRootsOnly())
    {
        entity->traverse([&](const Entity* entity) -> void {
            entityCount++;
        });
    }

    if (entityCount == 1)
    {
        text = localize("entity-mode-context-are-you-sure-you-want-to-delete-entity", to_string(entityCount));
    }
    else
    {
        text = localize("entity-mode-context-are-you-sure-you-want-to-delete-entities", to_string(entityCount));
    }

    ctx->context()->interface()->addWindow<ConfirmDialog>(
        text,
        [this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            ctx->context()->controller()->edit([this]() -> void {
                for (Entity* entity : selectionRootsOnly())
                {
                    ctx->context()->controller()->selfWorld()->remove(entity->id());
                }
            });

            deselectAll();
        }
    );
}

void EntityModeContext::addChildren(const Point& point)
{
    ctx->context()->controller()->edit([this, point]() -> void {
        World* world = ctx->context()->controller()->selfWorld();

        vector<Entity*> parentCandidates = entitiesUnderPoint(point);

        if (parentCandidates.empty())
        {
            return;
        }

        Entity* parent = parentCandidates.front();

        for (Entity* entity : selection())
        {
            world->setParent(entity, parent);
        }
    });
}

void EntityModeContext::removeParent()
{
    ctx->context()->controller()->edit([this]() -> void {
        World* world = ctx->context()->controller()->selfWorld();

        for (Entity* entity : selection())
        {
            world->clearParent(entity);
        }
    });
}

void EntityModeContext::hideSelected()
{
    for (const EntityID& id : selectionIDs())
    {
        _hiddenEntityIDs.insert(id);
    }

    deselectAll();
}

void EntityModeContext::hideUnselected()
{
    for (const EntityID& id : unselectionIDs())
    {
        _hiddenEntityIDs.insert(id);
    }
}

void EntityModeContext::unhideAll()
{
    _hiddenEntityIDs.clear();
}

const set<EntityID>& EntityModeContext::hiddenEntityIDs() const
{
    return _hiddenEntityIDs;
}

set<EntityID> EntityModeContext::selectionIDs() const
{
    set<EntityID> ids;

    ctx->context()->controller()->selfWorld()->traverse([&](const Entity* entity) -> void {
        if (entity->selectingUserID() != ctx->context()->controller()->selfID())
        {
            return;
        }

        ids.insert(entity->id());
    });

    return ids;
}

set<EntityID> EntityModeContext::unselectionIDs() const
{
    set<EntityID> ids;

    ctx->context()->controller()->selfWorld()->traverse([&](const Entity* entity) -> void {
        if (entity->selectingUserID() == ctx->context()->controller()->selfID())
        {
            return;
        }

        ids.insert(entity->id());
    });

    return ids;
}

set<Entity*> EntityModeContext::selection() const
{
    set<Entity*> selection;

    ctx->context()->controller()->selfWorld()->traverse([&](Entity* entity) -> void {
        if (entity->selectingUserID() != ctx->context()->controller()->selfID())
        {
            return;
        }

        selection.insert(entity);
    });

    return selection;
}

set<EntityID> EntityModeContext::selectionRootIDsOnly() const
{
    set<Entity*> selectionRoots = selection();

    set<EntityID> ids;

    for (Entity* entity : selectionRoots)
    {
        ids.insert(entity->id());
    }

    return ids;
}

set<Entity*> EntityModeContext::selectionRootsOnly() const
{
    set<Entity*> selection = this->selection();

    for (Entity* entity : this->selection())
    {
        bool found = false;

        for (Entity* otherEntity : selection)
        {
            if (entity->hasParent(otherEntity))
            {
                found = true;
                break;
            }
        }

        if (found)
        {
            selection.erase(entity);
        }
    }

    return selection;
}

Entity* EntityModeContext::selectionRootAtIndex(size_t index) const
{
    // Have to use selectionRootIDsOnly here not selectionRootsOnly, otherwise the order is inconsistent across invocations
    size_t i = 0;
    for (EntityID id : selectionRootIDsOnly())
    {
        if (i != index)
        {
            i++;
            continue;
        }

        return ctx->context()->controller()->game().get(id);
    }

    fatal("Entity root selection index out of range: " + to_string(index) + " > " + to_string(selectionRootIDsOnly().size()) + ".");
}

vec3 EntityModeContext::selectionCenter() const
{
    vec3 center;

    set<Entity*> entities = selectionRootsOnly();

    if (entities.empty())
    {
        return center;
    }

    for (Entity* entity : entities)
    {
        center += entity->globalPosition();
    }

    center /= entities.size();

    return center;
}

void EntityModeContext::select(Entity* entity)
{
    entity->setSelectingUserID(ctx->context()->controller()->selfID());

    GameEvent event(Game_Event_Select);
    event.u = entity->id().value();

    ctx->context()->controller()->send(event);
}

void EntityModeContext::select(const vector<Entity*>& entities)
{
    for (Entity* entity : entities)
    {
        select(entity);
    }
}

void EntityModeContext::selectAll()
{
    ctx->context()->controller()->selfWorld()->traverse([&](Entity* entity) -> void {
        if (_hiddenEntityIDs.contains(entity->id()))
        {
            return;
        }

        if (entity->selectingUserID() != UserID())
        {
            return;
        }

        select(entity);
    });
}

void EntityModeContext::deselect(Entity* entity)
{
    entity->setSelectingUserID(UserID());

    GameEvent event(Game_Event_Deselect);
    event.u = entity->id().value();

    ctx->context()->controller()->send(event);
}

void EntityModeContext::deselect(const vector<Entity*>& entities)
{
    for (Entity* entity : entities)
    {
        deselect(entity);
    }
}

void EntityModeContext::deselectAll()
{
    for (EntityID id : selectionIDs())
    {
        Entity* entity = ctx->context()->controller()->selfWorld()->get(id);

        if (entity)
        {
            deselect(entity);
        }
    }
}

bool EntityModeContext::hasSelection() const
{
    return !selectionIDs().empty();
}

void EntityModeContext::invertSelection()
{
    set<EntityID> previousSelectionIDs = selectionIDs();

    selectAll();

    for (EntityID id : previousSelectionIDs)
    {
        Entity* entity = ctx->context()->controller()->selfWorld()->get(id);

        if (entity)
        {
            deselect(entity);
        }
    }
}

void EntityModeContext::translate(const Point& point, bool soft)
{
    if (selectionRootsOnly().empty())
    {
        return;
    }

    EditModeContext::translate(point, soft);
}

void EntityModeContext::rotate(const Point& point, bool soft)
{
    if (selectionRootsOnly().empty())
    {
        return;
    }

    EditModeContext::rotate(point, soft);
}

void EntityModeContext::scale(const Point& point, bool soft)
{
    if (selectionRootsOnly().empty())
    {
        return;
    }

    EditModeContext::scale(point, soft);
}

void EntityModeContext::store()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->store();
        }
    });
}

void EntityModeContext::reset()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->reset();
        }
    });
}

void EntityModeContext::resetPosition()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->setGlobalPosition(vec3());
        }
    });
}

void EntityModeContext::resetRotation()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->setGlobalRotation(quaternion());
        }
    });
}

void EntityModeContext::resetScale()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->setGlobalScale(vec3(1));
        }
    });
}

void EntityModeContext::resetLinearVelocity()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->setLinearVelocity(vec3());
        }
    });
}

void EntityModeContext::resetAngularVelocity()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            entity->setAngularVelocity(vec3());
        }
    });
}

void EntityModeContext::selectionToCursor()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            vec3 cursor = ctx->cursorPosition();

            if (entity->parent())
            {
                cursor = entity->parent()->globalTransform().applyToPosition(cursor, false);
            }

            entity->setGlobalPosition(cursor);
        }
    });
}

void EntityModeContext::duplicate()
{
    set<EntityID> previousIDs = allIDs();

    vector<Entity> entities;

    for (const Entity* entity : selectionRootsOnly())
    {
        entities.push_back(*entity);
    }

    ctx->context()->controller()->edit([this, entities]() -> void {
        World* world = ctx->context()->controller()->selfWorld();

        for (const Entity& entity : entities)
        {
            auto newEntity = new Entity(world, ctx->context()->controller()->game().nextEntityID(), entity);

            world->add(EntityID(), newEntity);
        }
    });

    selectNew(previousIDs);
}

void EntityModeContext::cut()
{
    copy();

    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selectionRootsOnly())
        {
            ctx->context()->controller()->selfWorld()->remove(entity->id());
        }
    });

    deselectAll();
}

void EntityModeContext::copy()
{
    copyBuffer.copy(
        ctx->context()->flatViewer()->position(),
        ctx->context()->flatViewer()->rotation(),
        selectionRootsOnly()
    );
}

void EntityModeContext::paste()
{
    set<EntityID> previousIDs = allIDs();

    copyBuffer.paste(
        ctx->context()->flatViewer()->position(),
        ctx->context()->flatViewer()->rotation()
    );

    selectNew(previousIDs);
}

void EntityModeContext::extrude()
{
    // Do nothing
}

void EntityModeContext::split()
{
    // Do nothing
}

void EntityModeContext::join(const Point& point)
{
    ctx->context()->controller()->edit([this, point]() -> void {
        World* world = ctx->context()->controller()->selfWorld();

        vector<Entity*> dstCandidates = entitiesUnderPoint(point);

        if (dstCandidates.empty())
        {
            return;
        }

        Entity* dst = dstCandidates.front();

        for (Entity* src : selection())
        {
            for (const auto& [ id, part ] : src->body().parts())
            {
                auto newPart = new BodyPart(nullptr, dst->body().nextPartID(), *part);

                newPart->setRegionalTransform(dst->globalTransform().applyToTransform(
                    src->globalTransform().applyToTransform(newPart->regionalTransform()),
                    false
                ));

                dst->body().add(BodyPartID(), newPart);
            }

            world->remove(src->id());
        }
    });

    deselectAll();
}

void EntityModeContext::originTo3DCursor()
{
    ctx->context()->controller()->edit([this]() -> void {
        vector<Entity*> entities = toVector(selection());

        sort(
            entities.begin(),
            entities.end(),
            [](const Entity* a, const Entity* b) -> bool { return a->parentDepth() < b->parentDepth(); }
        );

        for (Entity* entity : entities)
        {
            vec3 globalOffset = entity->globalPosition() - ctx->cursorPosition();

            entity->setGlobalPosition(ctx->cursorPosition());

            vec3 regionalOffset = entity->globalTransform().applyToVector(globalOffset, false);

            for (const auto& [ id, part ] : entity->body().parts())
            {
                part->setRegionalPosition(part->regionalPosition() + regionalOffset);
            }
        }
    });
}

void EntityModeContext::originToBody()
{
    ctx->context()->controller()->edit([this]() -> void {
        vector<Entity*> entities = toVector(selection());

        sort(
            entities.begin(),
            entities.end(),
            [](const Entity* a, const Entity* b) -> bool { return a->parentDepth() < b->parentDepth(); }
        );

        for (Entity* entity : entities)
        {
            vec3 regionalCenter;
            size_t partCount = 0;

            entity->body().traverse([&](const BodyPart* part) -> void {
                regionalCenter += part->regionalPosition();
                partCount++;
            });

            regionalCenter /= partCount;

            vec3 globalCenter = entity->globalTransform().applyToPosition(regionalCenter);

            entity->setGlobalPosition(globalCenter);

            for (const auto& [ id, part ] : entity->body().parts())
            {
                part->setRegionalPosition(part->regionalPosition() - regionalCenter);
            }
        }
    });
}

void EntityModeContext::bodyToOrigin()
{
    ctx->context()->controller()->edit([this]() -> void {
        for (Entity* entity : selection())
        {
            vec3 regionalCenter;
            size_t partCount = 0;

            entity->body().traverse([&](const BodyPart* part) -> void {
                regionalCenter += part->regionalPosition();
                partCount++;
            });

            regionalCenter /= partCount;

            for (const auto& [ id, part ] : entity->body().parts())
            {
                part->setRegionalPosition(part->regionalPosition() - regionalCenter);
            }
        }
    });
}

void EntityModeContext::openEditor(EntityID id)
{
    if (ctx->context()->interface()->editor(id))
    {
        return;
    }

    ctx->context()->interface()->addWindow<EntityEditor>(id);
}

vector<Entity*> EntityModeContext::entitiesUnderPoint(const Point& point) const
{
    vector<Entity*> entities;

    World* world = ctx->context()->controller()->selfWorld();

    if (!world)
    {
        return {};
    }

    world->traverse([&](Entity* entity) -> void {
        if (_hiddenEntityIDs.contains(entity->id()))
        {
            return;
        }

        optional<Point> entityPoint = ctx->context()->flatViewer()->worldToScreen(entity->globalPosition());

        if (!entityPoint)
        {
            return;
        }

        Point start = *entityPoint - (UIScale::controlPointSize() / 2);
        Point end = *entityPoint + (UIScale::controlPointSize() / 2);

        if (start.x > point.x ||
            start.y > point.y ||
            end.x <= point.x ||
            end.y <= point.y
        )
        {
            return;
        }

        entities.push_back(entity);
    });

    sort(
        entities.begin(),
        entities.end(),
        [this](const Entity* a, const Entity* b) -> bool
        {
            return a->globalPosition().distance(ctx->context()->flatViewer()->position()) < b->globalPosition().distance(ctx->context()->flatViewer()->position());
        }
    );

    // Rotates the list so when single-selecting the selection will rotate through all possibilities
    for (size_t i = 0; i < entities.size(); i++)
    {
        if (selectionIDs().contains(entities.at(i)->id()))
        {
            vector<Entity*> before = { entities.begin(), entities.begin() + i + 1 };
            vector<Entity*> after = { entities.begin() + i + 1, entities.end() };
            entities = ::join(after, before);
            break;
        }
    }

    return entities;
}

vector<Entity*> EntityModeContext::entitiesInRegion(const Point& start, const Point& end) const
{
    vector<Entity*> entities;

    World* world = ctx->context()->controller()->selfWorld();

    if (!world)
    {
        return {};
    }

    world->traverse([&](Entity* entity) -> void {
        if (_hiddenEntityIDs.contains(entity->id()))
        {
            return;
        }

        optional<Point> entityPoint = ctx->context()->flatViewer()->worldToScreen(entity->globalPosition());

        if (!entityPoint)
        {
            return;
        }

        if (start.x > entityPoint->x ||
            start.y > entityPoint->y ||
            end.x <= entityPoint->x ||
            end.y <= entityPoint->y
        )
        {
            return;
        }

        entities.push_back(entity);
    });

    return entities;
}

SampleCopyBuffer& EntityModeContext::sampleCopyBuffer()
{
    return _sampleCopyBuffer;
}

ActionCopyBuffer& EntityModeContext::actionCopyBuffer()
{
    return _actionCopyBuffer;
}

set<EntityID> EntityModeContext::allIDs() const
{
    set<EntityID> allIDs;

    World* world = ctx->context()->controller()->selfWorld();

    if (!world)
    {
        return allIDs;
    }

    world->traverse([&](const Entity* entity) -> void {
        allIDs.insert(entity->id());
    });

    return allIDs;
}

void EntityModeContext::selectNew(const vector<Entity*>& newEntities)
{
    deselectAll();
    for (Entity* entity : newEntities)
    {
        select(entity);
    }
}

void EntityModeContext::selectNew(const set<EntityID>& previousIDs)
{
    World* world = ctx->context()->controller()->selfWorld();

    if (!world)
    {
        return;
    }

    deselectAll();
    world->traverse([&](Entity* entity) -> void {
        if (previousIDs.contains(entity->id()))
        {
            return;
        }

        select(entity);
    });
}

bool EntityModeContext::gizmoShown() const
{
    return ctx->gizmoShown() && ctx->context()->controller()->selfWorld() && !selection().empty();
}

bool EntityModeContext::gizmoSelectable(const Point& point) const
{
    return ctx->entityMode()->entitiesUnderPoint(point).empty();
}

void EntityModeContext::makeSelections(const Point& point, SelectMode mode)
{
    vector<Entity*> entities = entitiesUnderPoint(point);

    switch (mode)
    {
    case Select_Replace :
        deselectAll();

        if (!entities.empty())
        {
            select(entities.front());
        }
        break;
    case Select_Add :
        if (!entities.empty())
        {
            select(entities.front());
        }
        break;
    case Select_Remove :
        if (!entities.empty())
        {
            deselect(entities.front());
        }
        break;
    }
}

void EntityModeContext::makeSelections(const Point& start, const Point& end, SelectMode mode)
{
    vector<Entity*> entities = entitiesInRegion(start, end);

    switch (mode)
    {
    case Select_Replace :
        deselectAll();

        select(entities);
        break;
    case Select_Add :
        select(entities);
        break;
    case Select_Remove :
        deselect(entities);
        break;
    }
}

void EntityModeContext::transformSet(size_t i, const vec3& pivot, const mat4& initial, const mat4& transform)
{
    ctx->context()->controller()->edit([this, i, transform]() -> void {
        Entity* entity = selectionRootAtIndex(i);

        entity->setGlobalTransform(transform);
    }, !firstTransform);
    firstTransform = false;
}

mat4 EntityModeContext::transformGet(size_t i) const
{
    Entity* entity = selectionRootAtIndex(i);

    return entity->globalTransform();
}

size_t EntityModeContext::transformCount() const
{
    return selectionRootsOnly().size();
}

mat4 EntityModeContext::transformToolSpace() const
{
    Entity* closestSharedParent = nullptr;

    for (Entity* entity : selectionRootsOnly())
    {
        if (!entity->parent())
        {
            continue;
        }

        if (!closestSharedParent)
        {
            closestSharedParent = entity->parent();
        }
        else
        {
            while (closestSharedParent && !entity->hasParent(closestSharedParent))
            {
                closestSharedParent = closestSharedParent->parent();
            }

            if (!closestSharedParent)
            {
                break;
            }
        }
    }

    if (closestSharedParent)
    {
        return closestSharedParent->globalTransform();
    }
    else
    {
        return mat4();
    }
}
