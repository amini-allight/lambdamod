/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "chat_destination_selector.hpp"
#include "line_edit.hpp"
#include "chat_destination.hpp"

class ConsoleChatInput : public InterfaceWidget
{
public:
    ConsoleChatInput(InterfaceWidget* parent, const function<void(const string&, const optional<ChatDestination>&)>& onReturn);

    void move(const Point& position) override;
    void resize(const Size& size) override;
    bool shouldDraw() const override;

    void focus() override;
    void forceFocus();

private:
    ChatDestinationSelector* destinationSelector;
    LineEdit* edit;   
    function<void(const string&, const optional<ChatDestination>&)> onReturn;

    optional<ChatDestination> destination;

    void onDestination(const optional<ChatDestination>& destination);
    void onDone(const string& text);

    SizeProperties sizeProperties() const override;
};
