/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_fade_effect.hpp"
#include "render_tools.hpp"
#include "theme.hpp"

RenderFlatFadeEffect::RenderFlatFadeEffect(
    const RenderContext* ctx,
    RenderFlatPipelineStore* pipelineStore,
    const vector<FlatSwapchainElement*>& swapchainElements,
    f64 strength
)
    : RenderFlatPostProcessEffect(
        ctx,
        pipelineStore->fade.pipelineLayout,
        pipelineStore->fade.pipeline
    )
    , strength(strength)
{
    createComponents(swapchainElements, pipelineStore->postProcessDescriptorSetLayout);
}

RenderFlatFadeEffect::~RenderFlatFadeEffect()
{
    destroyComponents();
}

void RenderFlatFadeEffect::update(f64 strength)
{
    this->strength = strength;
}

VmaBuffer RenderFlatFadeEffect::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(fvec4) + sizeof(f32) * 2,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderFlatFadeEffect::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    fvec4 backgroundColor = theme.worldBackgroundColor.toVec4();

    memcpy(mapping->data, &backgroundColor, sizeof(backgroundColor));
    memcpy(mapping->data + 4, &strength, sizeof(strength));
    memcpy(mapping->data + 5, &timer, sizeof(timer));
}
