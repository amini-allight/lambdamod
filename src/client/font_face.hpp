/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "image.hpp"

void fontInit();
void fontQuit();

class FontFace
{
public:
    FontFace(void* face, u32 size);
    FontFace(const FontFace& rhs) = delete;
    FontFace(FontFace&& rhs) = delete;
    ~FontFace();

    FontFace& operator=(const FontFace& rhs) = delete;
    FontFace& operator=(FontFace&& rhs) = delete;

    Image* render(const string& text, const Color& color) const;
    i32 width(const string& text) const;
    // Only returns a meaningful value for monospace fonts
    i32 monoWidth() const;
    i32 height() const;

private:
    void* face;
    i32 _monoWidth;
    i32 _height;
    i32 softHeight;
};

FontFace* loadTTF(const string& path, u32 size);
