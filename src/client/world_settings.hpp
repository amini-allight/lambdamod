/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"
#include "sky_parameters.hpp"
#include "atmosphere_parameters.hpp"
#include "list_view.hpp"

class WorldSettings : public InterfaceWidget, public TabContent
{
public:
    WorldSettings(InterfaceWidget* parent);

private:
    f64 speedOfSoundSource();
    void onSpeedOfSound(f64 v);

    vec3 gravitySource();
    void onGravity(const vec3& v);

    vec3 flowVelocitySource();
    void onFlowVelocity(const vec3& v);

    f64 densitySource();
    void onDensity(f64 v);

    vec3 upSource();
    void onUp(const vec3& v);

    bool shownSource();
    void onShown(bool state);

    f64 fogDistanceSource();
    void onFogDistance(f64 distance);

    void onOpenSkyParameters();
    SkyParameters skyParametersSource();
    void onSkyParameters(const SkyParameters& parameters);

    void onOpenAtmosphereParameters();
    AtmosphereParameters atmosphereParametersSource();
    void onAtmosphereParameters(const AtmosphereParameters& parameters);

    SizeProperties sizeProperties() const override;
};
