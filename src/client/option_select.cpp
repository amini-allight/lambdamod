/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "option_select.hpp"
#include "control_context.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_window.hpp"
#include "interface_window_view.hpp"

OptionSelectPopup::OptionSelectPopup(
    InterfaceWidget* parent,
    const vector<string>& options,
    const vector<string>& optionTooltips,
    const function<void(size_t)>& onSet
)
    : Popup(parent)
    , options(options)
    , optionTooltips(optionTooltips)
    , onSet(onSet)
    , _scrollIndex(0)
    , activeIndex(0)
{
    START_WIDGET_SCOPE("option-select-popup")
        WIDGET_SLOT("interact", interact)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)

        START_INDEPENDENT_SCOPE("thumb-grabbed", thumbGrabbed)
            WIDGET_SLOT("drag", drag)
            WIDGET_SLOT("release", release)
        END_SCOPE
    END_SCOPE
}

void OptionSelectPopup::step()
{
    InterfaceWidget::step();

    Point local = pointer - position();

    if (local.x < size().w - UIScale::scrollbarWidth())
    {
        size_t index = _scrollIndex + (local.y / UIScale::optionSelectEntryHeight());

        if (index < options.size())
        {
            activeIndex = index;
        }

        if (focused() && activeIndex < static_cast<i32>(optionTooltips.size()))
        {
            dynamic_cast<InterfaceWindowView*>(view())->setTooltip(optionTooltips.at(activeIndex), pointer);
        }
    }
}

void OptionSelectPopup::draw(const DrawContext& ctx) const
{
    ctx.delayedCommands.push_back([this](const DrawContext& ctx) -> void {
        drawSolid(
            ctx,
            this,
            theme.indentColor
        );

        drawSolid(
            ctx,
            this,
            Point(size().w - UIScale::scrollbarWidth(), 0),
            Size(UIScale::scrollbarWidth(), size().h),
            theme.softIndentColor
        );

        if (needsScroll())
        {
            drawSolid(
                ctx,
                this,
                Point(size().w - UIScale::scrollbarWidth(), thumbY()),
                Size(UIScale::scrollbarWidth(), thumbHeight()),
                theme.outdentColor
            );
        }
    });

    i32 maxRows = static_cast<i32>(ceil(size().h / static_cast<f64>(UIScale::optionSelectEntryHeight())));

    for (i32 i = _scrollIndex; i < static_cast<i32>(options.size()) && i - _scrollIndex < maxRows; i++)
    {
        i32 y = (i - _scrollIndex) * UIScale::optionSelectEntryHeight();

        if (focused() && i == activeIndex)
        {
            ctx.delayedCommands.push_back([this, y](const DrawContext& ctx) -> void {
                drawSolid(
                    ctx,
                    this,
                    Point(0, y),
                    Size(size().w - UIScale::scrollbarWidth(), UIScale::optionSelectEntryHeight()),
                    theme.accentColor
                );
            });
        }

        ctx.delayedCommands.push_back([this, i, y](const DrawContext& ctx) -> void {
            drawText(
                ctx,
                this,
                Point(0, y),
                Size(size().w - UIScale::scrollbarWidth(), UIScale::optionSelectEntryHeight()),
                options[i],
                TextStyle()
            );
        });
    }
}

bool OptionSelectPopup::canClose() const
{
    return !focused() && !thumbGrabbed;
}

i32 OptionSelectPopup::viewHeight() const
{
    return size().h;
}

i32 OptionSelectPopup::scrollIndex() const
{
    return _scrollIndex;
}

void OptionSelectPopup::setScrollIndex(i32 index)
{
    _scrollIndex = index;
}

i32 OptionSelectPopup::childCount() const
{
    return options.size();
}

i32 OptionSelectPopup::childHeight() const
{
    return UIScale::optionSelectEntryHeight();
}

void OptionSelectPopup::interact(const Input& input)
{
    Point local = pointer - position();

    if (local.x < size().w - UIScale::scrollbarWidth())
    {
        size_t index = _scrollIndex + (local.y / UIScale::optionSelectEntryHeight());

        if (index < options.size())
        {
            playPositiveActivateEffect();
            onSet(index);
        }
    }
    else
    {
        if (local.y >= thumbY() && local.y < thumbY() + thumbHeight())
        {
            grabThumb(local);
        }
        else
        {
            setScrollIndexFromThumbY(local.y);
        }
    }
}

void OptionSelectPopup::scrollUp(const Input& input)
{
    if (_scrollIndex > 0)
    {
        _scrollIndex--;
    }
}

void OptionSelectPopup::scrollDown(const Input& input)
{
    if (_scrollIndex < maxScrollIndex())
    {
        _scrollIndex++;
    }
}

void OptionSelectPopup::goToStart(const Input& input)
{
    _scrollIndex = 0;
}

void OptionSelectPopup::goToEnd(const Input& input)
{
    _scrollIndex = maxScrollIndex();
}

void OptionSelectPopup::release(const Input& input)
{
    releaseThumb();
}

void OptionSelectPopup::drag(const Input& input)
{
    Point local = pointer - position();

    dragThumb(local);
}

SizeProperties OptionSelectPopup::sizeProperties() const
{
    return sizePropertiesFillAll;
}

OptionSelect::OptionSelect(
    InterfaceWidget* parent,
    const function<vector<string>()>& optionsSource,
    const function<size_t()>& source,
    const function<void(size_t)>& onSet
)
    : OptionSelect(
        parent,
        optionsSource,
        nullptr,
        source,
        onSet
    )
{

}

OptionSelect::OptionSelect(
    InterfaceWidget* parent,
    const function<vector<string>()>& optionsSource,
    const function<vector<string>()>& optionTooltipsSource,
    const function<size_t()>& source,
    const function<void(size_t)>& onSet
)
    : InterfaceWidget(parent)
    , options(optionsSource())
    , optionTooltips(optionTooltipsSource ? optionTooltipsSource() : vector<string>())
    , index(0)
    , optionsSource(optionsSource)
    , optionTooltipsSource(optionTooltipsSource)
    , source(source)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("option-select")
        WIDGET_SLOT("interact", interact)
    END_SCOPE

    popup = new OptionSelectPopup(
        this,
        options,
        optionTooltips,
        bind(&OptionSelect::onSelect, this, placeholders::_1)
    );
}

OptionSelect::~OptionSelect()
{
    delete popup;
}

void OptionSelect::move(const Point& position)
{
    this->position(position);

    popup->move(position + Point(0, size().h));
}

void OptionSelect::resize(const Size& size)
{
    this->size(size);

    popup->resize(Size(
        this->size().w,
        min<i32>(optionsSource().size() * UIScale::optionSelectEntryHeight(), UIScale::optionSelectPopupMaxHeight())
    ));
}

void OptionSelect::step()
{
    InterfaceWidget::step();

    if (source)
    {
        index = source();
    }

    if (optionsSource)
    {
        vector<string> newOptions = optionsSource();

        if (newOptions != options)
        {
            options = newOptions;
            index = 0;
            popup->_scrollIndex = 0;
            popup->activeIndex = 0;
        }
    }

    if (optionTooltipsSource)
    {
        optionTooltips = optionTooltipsSource();
    }

    if (!focused() && popup->canClose() && popup->opened())
    {
        popup->close();
    }
}

void OptionSelect::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        focused() ? theme.accentColor : theme.outdentColor
    );

    drawText(
        ctx,
        this,
        Point(0),
        Size(size().w - UIScale::iconButtonSize().w, size().h),
        index < options.size() ? options[index] : "",
        TextStyle()
    );

    drawImage(
        ctx,
        this,
        Point(size().w - UIScale::iconButtonSize().w, 0),
        UIScale::iconButtonSize(),
        Icon_Down
    );

    InterfaceWidget::draw(ctx);
}

void OptionSelect::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void OptionSelect::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void OptionSelect::onSelect(size_t index)
{
    popup->close();
    this->index = index;
    onSet(index);
}

void OptionSelect::interact(const Input& input)
{
    if (popup->opened())
    {
        popup->close();
        playNegativeActivateEffect();
    }
    else
    {
        popup->open();
        playPositiveActivateEffect();
    }

    if (popup->opened())
    {
        update();
    }
}

SizeProperties OptionSelect::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::optionSelectHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
