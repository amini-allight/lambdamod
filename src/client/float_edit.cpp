/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "float_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "centered_line_edit.hpp"
#include "tools.hpp"

FloatEdit::FloatEdit(
    InterfaceWidget* parent,
    const function<f64()>& source,
    const function<void(const f64&)>& onSet
)
    : InterfaceWidget(parent)
{
    f = new CenteredLineEdit(
        this,
        source ? [source]() -> string
        {
            return toPrettyString(source());
        } : function<string()>(nullptr),
        [onSet](const string& s) -> void
        {
            try
            {
                onSet(stod(s));
            }
            catch (const exception& e)
            {

            }
        }
    );
}

void FloatEdit::set(f64 value)
{
    f->set(toPrettyString(value));
}

f64 FloatEdit::value() const
{
    try
    {
        return stod(f->content());
    }
    catch (const exception& e)
    {
        return 0;
    }
}

void FloatEdit::clear()
{
    f->clear();
}

void FloatEdit::setPlaceholder(const string& text)
{
    f->setPlaceholder(text);
}

SizeProperties FloatEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
