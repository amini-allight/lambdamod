/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "context_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "tools.hpp"
#include "script_tools.hpp"
#include "row.hpp"
#include "spacer.hpp"
#include "user_select_dialog.hpp"
#include "user_closure.hpp"
#include "stack_view.hpp"
#include "context_logic_editor.hpp"

ContextSettings::ContextSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto row = new Row(this);

    definitionList = new ItemList(
        row,
        bind(&ContextSettings::itemSource, this),
        bind(&ContextSettings::onAdd, this, placeholders::_1),
        bind(&ContextSettings::onRemove, this, placeholders::_1),
        bind(&ContextSettings::onSelect, this, placeholders::_1),
        bind(&ContextSettings::onRename, this, placeholders::_1, placeholders::_2),
        bind(&ContextSettings::onClone, this, placeholders::_1, placeholders::_2),
        bind(&ContextSettings::onListRightClick, this, placeholders::_1)
    );
    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    stack = new StackView(row);
}

void ContextSettings::step()
{
    InterfaceWidget::step();

    vector<string> items = itemSource();

    if (
        definitionList->activeIndex() < 0 ||
        definitionList->activeIndex() >= static_cast<i32>(items.size()) ||
        items.at(definitionList->activeIndex()) != stack->activePageName()
    )
    {
        definitionList->clearSelection();
        for (size_t i = 0; i < items.size(); i++)
        {
            if (items.at(i) == stack->activePageName())
            {
                definitionList->select(i);
                break;
            }
        }
    }
}

void ContextSettings::openDefinition(const string& name)
{
    if (!stack->page(name))
    {
        stack->add<ContextLogicEditor>(
            name,
            bind(&ContextSettings::valueSource, this, name),
            bind(&ContextSettings::onValueSave, this, name, placeholders::_1)
        );
    }

    stack->open(name);
}

void ContextSettings::closeDefinition(const string& name)
{
    if (stack->page(name))
    {
        stack->remove(name);
    }

    if (stack->activePageName() == name)
    {
        stack->close();
    }
}

vector<string> ContextSettings::itemSource()
{
    const map<string, Value>& scope = context()->controller()->game().context()->scopes.at(globalScope);

    vector<string> names;
    names.reserve(scope.size());

    for (const auto& [ name, value ] : scope)
    {
        names.push_back(name);
    }

    return names;
}

void ContextSettings::onAdd(const string& name)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, name]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), name))
        {
            try
            {
                ctx->define(name, Value());

                openDefinition(name);
            }
            catch (const Value& e)
            {
                // Suppress errors caused by hitting the script memory ceiling
            }
        }
    });
}

void ContextSettings::onRemove(const string& name)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, name]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), name))
        {
            ctx->undefine(name);
        }

        closeDefinition(name);
    });
}

void ContextSettings::onSelect(const string& name)
{
    openDefinition(name);
}

void ContextSettings::onRename(const string& oldName, const string& newName)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, oldName, newName]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), oldName) && ctx->canWrite(context()->controller()->self(), newName))
        {
            try
            {
                ctx->define(
                    toValidName(newName),
                    ctx->scopes.at(globalScope).at(oldName)
                );
                ctx->undefine(oldName);

                closeDefinition(oldName);
                openDefinition(newName);
            }
            catch (const Value& e)
            {
                // Suppress errors caused by hitting the script memory ceiling
            }
        }
    });
}

void ContextSettings::onClone(const string& oldName, const string& newName)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, oldName, newName]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), oldName) && ctx->canWrite(context()->controller()->self(), newName))
        {
            try
            {
                ctx->define(
                    toValidName(newName),
                    ctx->scopes.at(globalScope).at(oldName)
                );

                openDefinition(newName);
            }
            catch (const Value& e)
            {
                // Suppress errors caused by hitting the script memory ceiling
            }
        }
    });
}

vector<tuple<string, function<void(const Point&)>>> ContextSettings::onListRightClick(size_t index)
{
    vector<tuple<string, function<void(const Point&)>>> entries;

    ScriptContext* ctx = context()->controller()->game().context();
    const map<string, Value>& scope = ctx->scopes.at(globalScope);

    string targetName;

    size_t i = 0;
    for (const auto& [ name, value ] : scope)
    {
        if (i == index)
        {
            targetName = name;
            break;
        }

        i++;
    }

    entries.push_back({
        localize("context-settings-set-owner"),
        bind(&ContextSettings::onSetOwner, this, targetName, placeholders::_1)
    });

    if (!ctx->isRestricted(targetName))
    {
        entries.push_back({
            localize("context-settings-restrict"),
            bind(&ContextSettings::onToggleRestriction, this, targetName, placeholders::_1)
        });
    }
    else
    {
        entries.push_back({
            localize("context-settings-unrestrict"),
            bind(&ContextSettings::onToggleRestriction, this, targetName, placeholders::_1)
        });
    }

    return entries;
}

void ContextSettings::onSetOwner(const string& name, const Point& point)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->interface()->addWindow<UserSelectDialog>(
        ctx->getOwner(name),
        bind(&ContextSettings::onSetOwnerDone, this, name, placeholders::_1)
    );
}

void ContextSettings::onSetOwnerDone(const string& name, UserID id)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, name, id]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), name))
        {
            ctx->controlDefine(name, id);
        }
    });
}

void ContextSettings::onToggleRestriction(const string& name, const Point& point)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, name]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), name))
        {
            ctx->restrictDefine(name, !ctx->isRestricted(name));
        }
    });
}

Value ContextSettings::valueSource(const string& name)
{
    const map<string, Value>& scope = context()->controller()->game().context()->scopes.at(globalScope);

    auto it = scope.find(name);

    if (it != scope.end())
    {
        return it->second;
    }
    else
    {
        return Value();
    }
}

void ContextSettings::onValueSave(const string& name, const Value& value)
{
    ScriptContext* ctx = context()->controller()->game().context();

    context()->controller()->edit([this, ctx, name, value]() -> void {
        UserClosure closure(ctx, context()->controller()->self());

        if (ctx->canWrite(context()->controller()->self(), name))
        {
            try
            {
                ctx->define(name, value);
            }
            catch (const Value& e)
            {
                // Suppress errors caused by hitting the script memory ceiling
            }
        }
    });
}

SizeProperties ContextSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
