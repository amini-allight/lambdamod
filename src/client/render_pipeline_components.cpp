/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_pipeline_components.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"

void createPipelineLayout(
    VkDevice device,
    VkDescriptorSetLayout descriptorSetLayout,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    VkPipelineLayoutCreateInfo pipelineLayoutCreateInfo{};
    pipelineLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipelineLayoutCreateInfo.setLayoutCount = 1;
    pipelineLayoutCreateInfo.pSetLayouts = &descriptorSetLayout;

    result = vkCreatePipelineLayout(device, &pipelineLayoutCreateInfo, nullptr, &pipelineLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create pipeline layout: " + vulkanError(result));
    }
}

VkPipelineVertexInputStateCreateInfo emptyVertexInputStage()
{
    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = 0;
    vertexInputStage.pVertexBindingDescriptions = nullptr;
    vertexInputStage.vertexAttributeDescriptionCount = 0;
    vertexInputStage.pVertexAttributeDescriptions = nullptr;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo basicVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo coloredVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3) * 2;
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription colorAttributeDescription{};
    colorAttributeDescription.binding = 0;
    colorAttributeDescription.location = 1;
    colorAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    colorAttributeDescription.offset = sizeof(fvec3);

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription,
        colorAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo litColoredVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3) + sizeof(fvec3) + sizeof(fvec4);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription normalAttributeDescription{};
    normalAttributeDescription.binding = 0;
    normalAttributeDescription.location = 1;
    normalAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    normalAttributeDescription.offset = sizeof(fvec3);

    static VkVertexInputAttributeDescription colorAttributeDescription{};
    colorAttributeDescription.binding = 0;
    colorAttributeDescription.location = 2;
    colorAttributeDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
    colorAttributeDescription.offset = sizeof(fvec3) + sizeof(fvec3);

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription,
        normalAttributeDescription,
        colorAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo texturedVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3) + sizeof(fvec2);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription uvAttributeDescription{};
    uvAttributeDescription.binding = 0;
    uvAttributeDescription.location = 1;
    uvAttributeDescription.format = VK_FORMAT_R32G32_SFLOAT;
    uvAttributeDescription.offset = sizeof(fvec3);

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription,
        uvAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo litTexturedVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3) + sizeof(fvec2) + sizeof(fvec3);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription uvAttributeDescription{};
    uvAttributeDescription.binding = 0;
    uvAttributeDescription.location = 1;
    uvAttributeDescription.format = VK_FORMAT_R32G32_SFLOAT;
    uvAttributeDescription.offset = sizeof(fvec3);

    static VkVertexInputAttributeDescription normalAttributeDescription{};
    normalAttributeDescription.binding = 0;
    normalAttributeDescription.location = 2;
    normalAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    normalAttributeDescription.offset = sizeof(fvec3) + sizeof(fvec2);

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription,
        uvAttributeDescription,
        normalAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo ropeVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3) + sizeof(fvec4) + sizeof(fvec3) + sizeof(fvec3);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription rotationAttributeDescription{};
    rotationAttributeDescription.binding = 0;
    rotationAttributeDescription.location = 1;
    rotationAttributeDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
    rotationAttributeDescription.offset = sizeof(fvec3);

    static VkVertexInputAttributeDescription scaleAttributeDescription{};
    scaleAttributeDescription.binding = 0;
    scaleAttributeDescription.location = 2;
    scaleAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    scaleAttributeDescription.offset = sizeof(fvec3) + sizeof(fvec4);

    static VkVertexInputAttributeDescription colorAttributeDescription{};
    colorAttributeDescription.binding = 0;
    colorAttributeDescription.location = 3;
    colorAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    colorAttributeDescription.offset = sizeof(fvec3) + sizeof(fvec4) + sizeof(fvec3);

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription,
        rotationAttributeDescription,
        scaleAttributeDescription,
        colorAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineVertexInputStateCreateInfo litRopeVertexInputStage()
{
    static VkVertexInputBindingDescription bindingDescription{};
    bindingDescription.binding = 0;
    bindingDescription.stride = sizeof(fvec3) + sizeof(fvec4) + sizeof(fvec3) + sizeof(fvec4);
    bindingDescription.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;

    static VkVertexInputBindingDescription bindingDescriptions[] = {
        bindingDescription
    };

    static VkVertexInputAttributeDescription positionAttributeDescription{};
    positionAttributeDescription.binding = 0;
    positionAttributeDescription.location = 0;
    positionAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    positionAttributeDescription.offset = 0;

    static VkVertexInputAttributeDescription rotationAttributeDescription{};
    rotationAttributeDescription.binding = 0;
    rotationAttributeDescription.location = 1;
    rotationAttributeDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
    rotationAttributeDescription.offset = sizeof(fvec3);

    static VkVertexInputAttributeDescription scaleAttributeDescription{};
    scaleAttributeDescription.binding = 0;
    scaleAttributeDescription.location = 2;
    scaleAttributeDescription.format = VK_FORMAT_R32G32B32_SFLOAT;
    scaleAttributeDescription.offset = sizeof(fvec3) + sizeof(fvec4);

    static VkVertexInputAttributeDescription colorAttributeDescription{};
    colorAttributeDescription.binding = 0;
    colorAttributeDescription.location = 3;
    colorAttributeDescription.format = VK_FORMAT_R32G32B32A32_SFLOAT;
    colorAttributeDescription.offset = sizeof(fvec3) + sizeof(fvec4) + sizeof(fvec3);

    static VkVertexInputAttributeDescription attributeDescriptions[] = {
        positionAttributeDescription,
        rotationAttributeDescription,
        scaleAttributeDescription,
        colorAttributeDescription
    };

    VkPipelineVertexInputStateCreateInfo vertexInputStage{};
    vertexInputStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertexInputStage.vertexBindingDescriptionCount = sizeof(bindingDescriptions) / sizeof(VkVertexInputBindingDescription);
    vertexInputStage.pVertexBindingDescriptions = bindingDescriptions;
    vertexInputStage.vertexAttributeDescriptionCount = sizeof(attributeDescriptions) / sizeof(VkVertexInputAttributeDescription);
    vertexInputStage.pVertexAttributeDescriptions = attributeDescriptions;

    return vertexInputStage;
}

VkPipelineInputAssemblyStateCreateInfo pointInputAssemblyStage()
{
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage{};
    inputAssemblyStage.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyStage.topology = VK_PRIMITIVE_TOPOLOGY_POINT_LIST;
    inputAssemblyStage.primitiveRestartEnable = false;

    return inputAssemblyStage;
}

VkPipelineInputAssemblyStateCreateInfo lineInputAssemblyStage()
{
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage{};
    inputAssemblyStage.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyStage.topology = VK_PRIMITIVE_TOPOLOGY_LINE_LIST;
    inputAssemblyStage.primitiveRestartEnable = false;

    return inputAssemblyStage;
}

VkPipelineInputAssemblyStateCreateInfo triangleInputAssemblyStage()
{
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage{};
    inputAssemblyStage.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    inputAssemblyStage.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    inputAssemblyStage.primitiveRestartEnable = false;

    return inputAssemblyStage;
}

VkPipelineShaderStageCreateInfo shaderStage(VkShaderStageFlagBits stage, VkShaderModule shader)
{
    VkPipelineShaderStageCreateInfo shaderStage{};
    shaderStage.sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    shaderStage.stage = stage;
    shaderStage.module = shader;
    shaderStage.pName = "main";

    return shaderStage;
}

VkPipelineViewportStateCreateInfo defaultViewportStage()
{
    static VkViewport viewport = {
        0, 0,
        shaderDefaultSize, shaderDefaultSize,
        0, 1
    };

    static VkRect2D scissor = {
        { 0, 0 },
        { shaderDefaultSize, shaderDefaultSize }
    };

    VkPipelineViewportStateCreateInfo viewportStage{};
    viewportStage.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewportStage.viewportCount = 1;
    viewportStage.pViewports = &viewport;
    viewportStage.scissorCount = 1;
    viewportStage.pScissors = &scissor;

    return viewportStage;
}

VkPipelineRasterizationStateCreateInfo pointRasterizationStage(bool culling)
{
    VkPipelineRasterizationStateCreateInfo rasterizationStage{};
    rasterizationStage.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationStage.depthClampEnable = false;
    rasterizationStage.rasterizerDiscardEnable = false;
    rasterizationStage.polygonMode = VK_POLYGON_MODE_POINT;
    rasterizationStage.lineWidth = 1;
    rasterizationStage.cullMode = culling ? VK_CULL_MODE_BACK_BIT : VK_CULL_MODE_NONE;
    rasterizationStage.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationStage.depthBiasEnable = false;
    rasterizationStage.depthBiasConstantFactor = 0;
    rasterizationStage.depthBiasClamp = 0;
    rasterizationStage.depthBiasSlopeFactor = 0;

    return rasterizationStage;
}

VkPipelineRasterizationStateCreateInfo lineRasterizationStage(f32 lineWidth, bool culling)
{
    VkPipelineRasterizationStateCreateInfo rasterizationStage{};
    rasterizationStage.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationStage.depthClampEnable = false;
    rasterizationStage.rasterizerDiscardEnable = false;
    rasterizationStage.polygonMode = VK_POLYGON_MODE_LINE;
    rasterizationStage.lineWidth = lineWidth;
    rasterizationStage.cullMode = culling ? VK_CULL_MODE_BACK_BIT : VK_CULL_MODE_NONE;
    rasterizationStage.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationStage.depthBiasEnable = false;
    rasterizationStage.depthBiasConstantFactor = 0;
    rasterizationStage.depthBiasClamp = 0;
    rasterizationStage.depthBiasSlopeFactor = 0;

    return rasterizationStage;
}

VkPipelineRasterizationStateCreateInfo triangleRasterizationStage(bool culling)
{
    VkPipelineRasterizationStateCreateInfo rasterizationStage{};
    rasterizationStage.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    rasterizationStage.depthClampEnable = false;
    rasterizationStage.rasterizerDiscardEnable = false;
    rasterizationStage.polygonMode = VK_POLYGON_MODE_FILL;
    rasterizationStage.lineWidth = 1;
    rasterizationStage.cullMode = culling ? VK_CULL_MODE_BACK_BIT : VK_CULL_MODE_NONE;
    rasterizationStage.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    rasterizationStage.depthBiasEnable = false;
    rasterizationStage.depthBiasConstantFactor = 0;
    rasterizationStage.depthBiasClamp = 0;
    rasterizationStage.depthBiasSlopeFactor = 0;

    return rasterizationStage;
}

VkPipelineMultisampleStateCreateInfo defaultMultisampleStage(VkSampleCountFlagBits sampleCount)
{
    VkPipelineMultisampleStateCreateInfo multisampleStage{};
    multisampleStage.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    multisampleStage.rasterizationSamples = sampleCount;

    return multisampleStage;
}

VkPipelineDepthStencilStateCreateInfo defaultDepthStencilStage()
{
    VkPipelineDepthStencilStateCreateInfo depthStencilStage{};
    depthStencilStage.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilStage.depthTestEnable = true;
    depthStencilStage.depthWriteEnable = true;
    depthStencilStage.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilStage.depthBoundsTestEnable = false;
    depthStencilStage.minDepthBounds = 0;
    depthStencilStage.maxDepthBounds = 1;
    depthStencilStage.stencilTestEnable = false;

    return depthStencilStage;
}

VkPipelineDepthStencilStateCreateInfo ignoreDepthStencilStage()
{
    VkPipelineDepthStencilStateCreateInfo depthStencilStage{};
    depthStencilStage.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilStage.depthTestEnable = false;
    depthStencilStage.depthWriteEnable = false;
    depthStencilStage.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilStage.depthBoundsTestEnable = false;
    depthStencilStage.minDepthBounds = 0;
    depthStencilStage.maxDepthBounds = 1;
    depthStencilStage.stencilTestEnable = false;

    return depthStencilStage;
}

VkPipelineDepthStencilStateCreateInfo readOnlyDepthStencilStage()
{
    VkPipelineDepthStencilStateCreateInfo depthStencilStage{};
    depthStencilStage.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilStage.depthTestEnable = true;
    depthStencilStage.depthWriteEnable = false;
    depthStencilStage.depthCompareOp = VK_COMPARE_OP_LESS;
    depthStencilStage.depthBoundsTestEnable = false;
    depthStencilStage.minDepthBounds = 0;
    depthStencilStage.maxDepthBounds = 1;
    depthStencilStage.stencilTestEnable = false;

    return depthStencilStage;
}

VkPipelineDepthStencilStateCreateInfo writeOnlyDepthStencilStage()
{
    VkPipelineDepthStencilStateCreateInfo depthStencilStage{};
    depthStencilStage.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depthStencilStage.depthTestEnable = false;
    depthStencilStage.depthWriteEnable = true;
    depthStencilStage.depthCompareOp = VK_COMPARE_OP_ALWAYS;
    depthStencilStage.depthBoundsTestEnable = false;
    depthStencilStage.minDepthBounds = 0;
    depthStencilStage.maxDepthBounds = 1;
    depthStencilStage.stencilTestEnable = false;

    return depthStencilStage;
}

VkPipelineColorBlendStateCreateInfo defaultColorBlendStage()
{
    static VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = true;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    VkPipelineColorBlendStateCreateInfo colorBlendStage{};
    colorBlendStage.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendStage.logicOpEnable = false;
    colorBlendStage.logicOp = VK_LOGIC_OP_COPY;
    colorBlendStage.attachmentCount = 1;
    colorBlendStage.pAttachments = &colorBlendAttachment;
    colorBlendStage.blendConstants[0] = 0;
    colorBlendStage.blendConstants[1] = 0;
    colorBlendStage.blendConstants[2] = 0;
    colorBlendStage.blendConstants[3] = 0;

    return colorBlendStage;
}

VkPipelineColorBlendStateCreateInfo stencilColorBlendStage()
{
    static VkPipelineColorBlendAttachmentState colorBlendAttachment{};
    colorBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    colorBlendAttachment.blendEnable = true;
    colorBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    colorBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    colorBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    colorBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    colorBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    static VkPipelineColorBlendAttachmentState stencilBlendAttachment{};
    stencilBlendAttachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT | VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT;
    stencilBlendAttachment.blendEnable = true;
    stencilBlendAttachment.srcColorBlendFactor = VK_BLEND_FACTOR_SRC_ALPHA;
    stencilBlendAttachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    stencilBlendAttachment.colorBlendOp = VK_BLEND_OP_ADD;
    stencilBlendAttachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    stencilBlendAttachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA;
    stencilBlendAttachment.alphaBlendOp = VK_BLEND_OP_ADD;

    static VkPipelineColorBlendAttachmentState colorBlendAttachments[] = {
        colorBlendAttachment,
        stencilBlendAttachment
    };

    VkPipelineColorBlendStateCreateInfo colorBlendStage{};
    colorBlendStage.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    colorBlendStage.logicOpEnable = false;
    colorBlendStage.logicOp = VK_LOGIC_OP_COPY;
    colorBlendStage.attachmentCount = sizeof(colorBlendAttachments) / sizeof(VkPipelineColorBlendAttachmentState);
    colorBlendStage.pAttachments = colorBlendAttachments;
    colorBlendStage.blendConstants[0] = 0;
    colorBlendStage.blendConstants[1] = 0;
    colorBlendStage.blendConstants[2] = 0;
    colorBlendStage.blendConstants[3] = 0;

    return colorBlendStage;
}

VkPipelineDynamicStateCreateInfo defaultDynamicState()
{
    static VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR
    };

    VkPipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = sizeof(dynamicStates) / sizeof(VkDynamicState);
    dynamicState.pDynamicStates = dynamicStates;

    return dynamicState;
}

VkPipelineDynamicStateCreateInfo lineWidthDynamicState()
{
    static VkDynamicState dynamicStates[] = {
        VK_DYNAMIC_STATE_VIEWPORT,
        VK_DYNAMIC_STATE_SCISSOR,
        VK_DYNAMIC_STATE_LINE_WIDTH
    };

    VkPipelineDynamicStateCreateInfo dynamicState{};
    dynamicState.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamicState.dynamicStateCount = sizeof(dynamicStates) / sizeof(VkDynamicState);
    dynamicState.pDynamicStates = dynamicStates;

    return dynamicState;
}
