/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"
#include "hud_element.hpp"

enum HUDTransformMode : u8
{
    HUD_Transform_None,
    HUD_Transform_Translate,
    HUD_Transform_Rotate,
    HUD_Transform_Scale
};

enum HUDTransformSubMode : u8
{
    HUD_Transform_Sub_None,
    HUD_Transform_Sub_X,
    HUD_Transform_Sub_Y
};

class HUD;

class HUDPreview : public InterfaceWidget
{
public:
    HUDPreview(
        InterfaceWidget* parent,
        EntityID id,
        const function<void(size_t)>& onSelect
    );

    void open(const string& name);

    void draw(const DrawContext& ctx) const override;

private:
    EntityID id;
    string name;

    function<void(size_t)> onSelect;

    HUD* hud;

    HUDTransformMode transformMode;
    HUDTransformSubMode transformSubMode;
    Point initialPointer;
    HUDElement initialState;

    tuple<Point, Size> elementExtent(const HUDElement& element) const;

    void transform(const Point& pointer);
    void translate(const vec2& position);
    void rotate(f64 rotation);
    void scale(f64 scale);
    void startTransform(HUDTransformMode mode, const Point& pointer);
    void endTransform();
    void cancelTransform();

    void updateEntity(const HUDElement& element);

    const HUDElement& currentElement() const;

    const map<string, HUDElement>& hudSource() const;

    void select(const Input& input);
    void translate(const Input& input);
    void rotate(const Input& input);
    void scale(const Input& input);
    void toggleX(const Input& input);
    void toggleY(const Input& input);
    void useTool(const Input& input);
    void cancelTool(const Input& input);
    void endTool(const Input& input);

    SizeProperties sizeProperties() const override;
};
