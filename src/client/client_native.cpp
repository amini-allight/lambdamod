/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if !defined(__EMSCRIPTEN__)
#include "client_native.hpp"
#include "log.hpp"
#include "ssl_tools.hpp"
#include "message_boundaries.hpp"

static constexpr size_t bufferSize = 1024;

ClientNative::ClientNative(
    const string& caFile,
    const function<void()>& connectCallback,
    const function<void(const NetworkEvent&)>& receiveCallback,
    const function<void()>& disconnectCallback
)
    : Client(connectCallback, receiveCallback, disconnectCallback)
    , ctx(nullptr)
    , ssl(nullptr)
    , verify(false)
{
    log("Initializing networking...");

    int result;

    initOpenSSL();

    const SSL_METHOD* method = TLS_client_method();

    if (!method)
    {
        fatal("Failed to get OpenSSL method: " + getOpenSSLError());
    }

    ctx = SSL_CTX_new(method);

    if (!ctx)
    {
        fatal("Failed to create OpenSSL context: " + getOpenSSLError());
    }

    SSL_CTX_set_min_proto_version(ctx, TLS1_3_VERSION);
    SSL_CTX_set_max_proto_version(ctx, 0);

    if (!caFile.empty())
    {
        BIO* certReader = BIO_new_mem_buf(caFile.data(), -1);

        if (!certReader)
        {
            fatal("Failed to create OpenSSL certificate authority reader: " + getOpenSSLError());
        }

        X509* cert = PEM_read_bio_X509(certReader, nullptr, nullptr, 0);
        BIO_free_all(certReader);

        if (!cert)
        {
            fatal("Failed to create OpenSSL certificate authority: " + getOpenSSLError());
        }

        X509_STORE* store = X509_STORE_new();

        if (!store)
        {
            fatal("Failed to create OpenSSL certificate store: " + getOpenSSLError());
        }

        result = X509_STORE_add_cert(store, cert);

        if (!result)
        {
            fatal("Failed to add certificate to OpenSSL certificate store: " + getOpenSSLError());
        }

        SSL_CTX_set_cert_store(ctx, store);

        verify = true;
    }

    ssl = SSL_new(ctx);

    if (!ssl)
    {
        fatal("Failed to create OpenSSL connection: " + getOpenSSLError());
    }
}

ClientNative::~ClientNative()
{
    if (connected)
    {
        onDisconnect();
    }

    if (ssl)
    {
        SSL_free(ssl);
    }

    if (ctx)
    {
        SSL_CTX_free(ctx);
    }

    log("Networking shut down.");
}

void ClientNative::step()
{
    if (!connected)
    {
        return;
    }

#ifdef LMOD_FAKELAG
    for (const string& message : lagFaker.pullReceive())
    {
        receiveCallback(NetworkEvent(message));
    }

    for (const string& message : lagFaker.pullSend())
    {
        if (!connected)
        {
            continue;
        }

        string data = messageBoundary(message);

        int result = SSL_write(ssl, data.data(), data.size());

        if (result < (int)data.size())
        {
            warning("Failed to write to socket.");
            onDisconnect();
            connected = false;
            return;
        }
    }
#endif

    u8 buffer[bufferSize];

    while (true)
    {
        int result = SSL_read(ssl, buffer, bufferSize);

        if (result <= 0)
        {
            if (!BIO_should_retry(SSL_get_rbio(ssl)))
            {
                warning("Failed to read from socket.");
                onDisconnect();
                connected = false;
            }
            return;
        }

        onReceive(string(reinterpret_cast<const char*>(buffer), result));
    }
}

void ClientNative::send(const NetworkEvent& event)
{
    if (!connected)
    {
        return;
    }

    string message = event.data();

#ifdef LMOD_FAKELAG
    if (lagFaker.pushSend(message))
    {
        return;
    }
#endif

    string data = messageBoundary(message);

    int result = SSL_write(ssl, data.data(), data.size());

    if (result < (int)data.size())
    {
        warning("Failed to write to socket.");
        onDisconnect();
        connected = false;
    }
}
#endif
