/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "script_node_format.hpp"

class NodeEditorNode;

enum NodeTransformMode : u8
{
    Node_Transform_None,
    Node_Transform_Translate,
    Node_Transform_Link
};

enum NodeTransformSubMode : u8
{
    Node_Transform_Sub_None,
    Node_Transform_Sub_X,
    Node_Transform_Sub_Y
};

struct NodeEditorState
{
    NodeEditorState();
    explicit NodeEditorState(const ScriptNodeFormat& nodes);

    ScriptNodeFormat nodes;
    ivec2 view;
    set<ScriptNodeID> selection;
};

class NodeEditor : public InterfaceWidget
{
public:
    NodeEditor(
        InterfaceWidget* parent,
        const function<ScriptNodeFormat()>& source,
        const function<void(const ScriptNodeFormat&)>& onSave
    );

    bool saved() const;
    void set(const ScriptNodeFormat& format);
    void notifyReload();
    void display(const string& message);

    void draw(const DrawContext& ctx) const override;

private:
    friend class NodeEditorNode;
    friend class NodeEditorLink;

    const NodeEditorState& state() const;
    void setState(const function<void(NodeEditorState&)>& transform, bool needsRegen = true);

private:
    function<ScriptNodeFormat()> source;
    function<void(const ScriptNodeFormat&)> onSave;

    bool _saved;
    string statusMessage;
    deque<NodeEditorState> states;
    deque<NodeEditorState> redoStates;
    ivec2 copyView;
    std::set<shared_ptr<ScriptNode>> copyBuffer;

    NodeTransformMode transformMode;
    NodeTransformSubMode transformSubMode;
    Point initialPointer;
    ScriptNodeFormat initialNodes;

    Point selectStart;
    Point selectEnd;
    bool selecting;

    Point panStart;
    vec2 panStartView;
    bool panning;

    ScriptNodeID connectStartNodeID;
    bool connectStartIsOutput;
    size_t connectStartSlotIndex;
    Point connectStartScreenPosition;
    Point connectEndScreenPosition;

    void makeSelections(SelectMode mode);

    bool isTargetingLinkConnector() const;
    void startLink(ScriptNodeID id, bool output, size_t index, const Point& screenPosition);
    void makeLink();
    void makeLink(ScriptNodeID srcID, size_t srcSlotIndex, ScriptNodeID dstID, size_t dstSlotIndex);
    void removeLink(ScriptNodeID srcID, size_t srcSlotIndex, ScriptNodeID dstID, size_t dstSlotIndex);

    void transform(const Point& pointer);
    void translate(const Point& delta);
    void connect(const Point& delta);
    void startTransform(NodeTransformMode mode, const Point& pointer);
    void endTransform();
    void cancelTransform();

    void updateChildren();
    NodeEditorNode* addChild(shared_ptr<ScriptNode> node, map<ScriptNodeID, NodeEditorNode*>* visitedNodes);

    void drawStatusLine(const DrawContext& ctx) const;

    void interact(const Input& input);
    void removeLink(const Input& input);
    void selectAll(const Input& input);
    void deselectAll(const Input& input);
    void invertSelection(const Input& input);
    void duplicate(const Input& input);
    void cut(const Input& input);
    void copy(const Input& input);
    void paste(const Input& input);
    void save(const Input& input);
    void reload(const Input& input);
    void undo(const Input& input);
    void redo(const Input& input);
    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void panLeft(const Input& input);
    void panRight(const Input& input);
    void addNew(const Input& input);
    void removeSelected(const Input& input);
    void startSelect(const Input& input);
    void startPan(const Input& input);
    void translate(const Input& input);
    void moveSelect(const Input& input);
    void endReplaceSelect(const Input& input);
    void endAddSelect(const Input& input);
    void endRemoveSelect(const Input& input);
    void pan(const Input& input);
    void endPan(const Input& input);
    void toggleX(const Input& input);
    void toggleY(const Input& input);
    void useTool(const Input& input);
    void cancelTool(const Input& input);
    void endTool(const Input& input);

    shared_ptr<ScriptNode> findInCopyBuffer(ScriptNodeID id);

    bool isCircular(ScriptNodeID srcID, ScriptNodeID dstID) const;

    SizeProperties sizeProperties() const override;
};
