/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "console.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "log.hpp"
#include "constants.hpp"
#include "column.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(568, 336) * uiScale();
}

Console::Console(InterfaceView* view)
    : InterfaceWindow(view, localize("console-console"))
{
    START_WIDGET_SCOPE("console")
        WIDGET_SLOT("close-dialog", dismiss)
    END_SCOPE

    auto column = new Column(this);

    output = new ConsoleOutput(
        column
    );
    output->push(Message(Message_Log, "", "console-welcome-to", { gameName, gameVersion }));

    _input = new ConsoleInput(
        column,
        bind(&Console::onChat, this, placeholders::_1, placeholders::_2),
        bind(&Console::onCommand, this, placeholders::_1)
    );

    resize(windowSize());
}

void Console::log(const Message& message)
{
    if (message.text.empty())
    {
        return;
    }

    output->push(message);
}

void Console::recordHistory(const string& history)
{
    _input->push(history);
}

void Console::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());

    _input->forceFocus();
}

void Console::onChat(const string& message, const optional<ChatDestination>& destination)
{
    CommandEvent event;

    if (!destination)
    {
        event.type = Command_Event_Chat;
    }
    else if (!destination->team)
    {
        event.type = Command_Event_Direct_Chat;
        event.destination = destination->name;
    }
    else
    {
        event.type = Command_Event_Team_Chat;
        event.destination = destination->name;
    }

    event.text = message;

    context()->controller()->send(event);
}

void Console::onCommand(const string& command)
{
    CommandEvent event;
    event.type = Command_Event_Command;
    event.text = command;

    context()->controller()->send(event);
}

void Console::dismiss(const Input& input)
{
    hide();
}
