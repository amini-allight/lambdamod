/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_scope_binding.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

static constexpr string anyTextIdentifier = "any-text";
static constexpr string releasePrefix = "release ";
static constexpr string controlPrefix = "ctrl+";
static constexpr string shiftPrefix = "shift+";
static constexpr string altPrefix = "alt+";
static constexpr string superPrefix = "super+";
static constexpr string inputIdentifierPrefix = "input-";

InputScopeBinding::InputScopeBinding(InputType type)
    : type(type)
    , up(false)
    , control(false)
    , shift(false)
    , alt(false)
    , super(false)
    , repeats(0)
    , anyText(false)
{

}

bool InputScopeBinding::operator==(const InputScopeBinding& rhs) const
{
    return type == rhs.type &&
        up == rhs.up &&
        control == rhs.control &&
        shift == rhs.shift &&
        alt == rhs.alt &&
        super == rhs.super &&
        repeats == rhs.repeats &&
        anyText == rhs.anyText;
}

bool InputScopeBinding::operator!=(const InputScopeBinding& rhs) const
{
    return !(*this == rhs);
}

bool InputScopeBinding::operator>(const InputScopeBinding& rhs) const
{
    return !(*this == rhs || *this < rhs);
}

bool InputScopeBinding::operator<(const InputScopeBinding& rhs) const
{
    // has more modifiers < has modifiers < anyText < normal inputs
    if (modifierCount() != 0 || rhs.modifierCount() != 0)
    {
        if (modifierCount() > rhs.modifierCount())
        {
            return true;
        }
        else if (modifierCount() < rhs.modifierCount())
        {
            return false;
        }
        else
        {
            if (control && !rhs.control)
            {
                return true;
            }
            else if (!control && rhs.control)
            {
                return false;
            }
            else
            {
                if (shift && !rhs.shift)
                {
                    return true;
                }
                else if (!shift && rhs.shift)
                {
                    return false;
                }
                else
                {
                    if (alt && !rhs.alt)
                    {
                        return true;
                    }
                    else if (!alt && rhs.alt)
                    {
                        return false;
                    }
                    else
                    {
                        if (super && !rhs.super)
                        {
                            return true;
                        }
                        else if (!super && rhs.super)
                        {
                            return false;
                        }
                        else
                        {
                            if (up && !rhs.up)
                            {
                                return true;
                            }
                            else if (!up && rhs.up)
                            {
                                return false;
                            }
                            else
                            {
                                if (type < rhs.type)
                                {
                                    return true;
                                }
                                else if (type > rhs.type)
                                {
                                    return false;
                                }
                                else
                                {
                                    return repeats > rhs.repeats;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    else
    {
        if (anyText && !rhs.anyText)
        {
            return true;
        }
        else if (!anyText && rhs.anyText)
        {
            return false;
        }
        else
        {
            if (up && !rhs.up)
            {
                return true;
            }
            else if (!up && rhs.up)
            {
                return false;
            }
            else
            {
                if (type < rhs.type)
                {
                    return true;
                }
                else if (type > rhs.type)
                {
                    return false;
                }
                else
                {
                    return repeats > rhs.repeats;
                }
            }
        }
    }
}

bool InputScopeBinding::operator>=(const InputScopeBinding& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool InputScopeBinding::operator<=(const InputScopeBinding& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering InputScopeBinding::operator<=>(const InputScopeBinding& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

bool InputScopeBinding::match(const Input& input, u32 repeats) const
{
    if (anyText)
    {
        return !input.text().empty();
    }

    if (this->repeats != 0 && this->repeats != repeats)
    {
        return false;
    }

    return input.type == type &&
        input.up == up &&
        (!control || input.control) &&
        (!shift || input.shift) &&
        (!alt || input.alt) &&
        (!super || input.super);
}

u32 InputScopeBinding::modifierCount() const
{
    return static_cast<u32>(control) + static_cast<u32>(shift) + static_cast<u32>(alt) + static_cast<u32>(super);
}

InputScopeBinding InputScopeBinding::fromString(const string& s)
{
    string stripped = removeLeftWhitespace(removeRightWhitespace(s));

    InputScopeBinding binding(Input_Null);

    if (stripped == anyTextIdentifier)
    {
        binding.anyText = true;
        return binding;
    }

    if (stripped.starts_with(releasePrefix))
    {
        binding.up = true;

        stripped = stripped.substr(releasePrefix.size(), stripped.size() - releasePrefix.size());
    }

    while (!stripped.empty())
    {
        string part;

        for (char c : stripped)
        {
            if (c == ' ')
            {
                stripped = stripped.substr(part.size() + 1, stripped.size() - (part.size() + 1));
                break;
            }

            part += c;

            if (c == '+')
            {
                stripped = stripped.substr(part.size(), stripped.size() - part.size());
                break;
            }
        }

        if (part == controlPrefix)
        {
            binding.control = true;
        }
        else if (part == shiftPrefix)
        {
            binding.shift = true;
        }
        else if (part == altPrefix)
        {
            binding.alt = true;
        }
        else if (part == superPrefix)
        {
            binding.super = true;
        }
        else if (smatch m; regex_match(part, m, regex("x[0-9]+")))
        {
            binding.repeats = toU64(part.substr(1, part.size() - 1));
        }
        else
        {
            try
            {
                binding.type = inputTypeFromString(inputIdentifierPrefix + part);
            }
            catch (const exception& e)
            {

            }
        }
    }

    return binding;
}

string InputScopeBinding::toString() const
{
    if (anyText)
    {
        return anyTextIdentifier;
    }

    string s;

    if (up)
    {
        s += releasePrefix;
    }

    if (control)
    {
        s += controlPrefix;
    }

    if (shift)
    {
        s += shiftPrefix;
    }

    if (alt)
    {
        s += altPrefix;
    }

    if (super)
    {
        s += superPrefix;
    }

    string type = inputTypeToString(this->type);
    s += type.substr(inputIdentifierPrefix.size(), type.size() - inputIdentifierPrefix.size());

    if (repeats != 0)
    {
        s += " x" + to_string(repeats);
    }

    return s;
}

InputScopeBinding InputScopeBindingTools::binding(InputType type)
{
    return InputScopeBinding(type);
}

InputScopeBinding InputScopeBindingTools::up(InputScopeBinding binding)
{
    binding.up = true;

    return binding;
}

InputScopeBinding InputScopeBindingTools::repeat(InputScopeBinding binding, u32 repeats)
{
    binding.repeats = repeats;

    return binding;
}

InputScopeBinding InputScopeBindingTools::control(InputScopeBinding binding)
{
    binding.control = true;

    return binding;
}

InputScopeBinding InputScopeBindingTools::shift(InputScopeBinding binding)
{
    binding.shift = true;

    return binding;
}

InputScopeBinding InputScopeBindingTools::alt(InputScopeBinding binding)
{
    binding.alt = true;

    return binding;
}

InputScopeBinding InputScopeBindingTools::super(InputScopeBinding binding)
{
    binding.super = true;

    return binding;
}

InputScopeBinding InputScopeBindingTools::anyText()
{
    InputScopeBinding binding(Input_Null);
    binding.anyText = true;

    return binding;
}

template<>
InputScopeBinding YAMLNode::convert() const
{
    InputScopeBinding binding(inputTypeFromString(at("type").as<string>()));

    binding.up = has("up") ? at("up").as<bool>() : false;
    binding.shift = has("shift") ? at("shift").as<bool>() : false;
    binding.control = has("control") ? at("control").as<bool>() : false;
    binding.alt = has("alt") ? at("alt").as<bool>() : false;
    binding.super = has("super") ? at("super").as<bool>() : false;
    binding.repeats = has("repeat") ? at("repeat").as<u32>() : 0;
    binding.anyText = has("anyText") ? at("anyText").as<bool>() : false;

    return binding;
}

template<>
void YAMLSerializer::emit(InputScopeBinding v)
{
    startMapping();

    emitPair("type", inputTypeToString(v.type));

    if (v.up)
    {
        emitPair("up", v.up);
    }

    if (v.shift)
    {
        emitPair("shift", v.shift);
    }

    if (v.control)
    {
        emitPair("control", v.control);
    }

    if (v.alt)
    {
        emitPair("alt", v.alt);
    }

    if (v.super)
    {
        emitPair("super", v.super);
    }

    if (v.repeats != 0)
    {
        emitPair("repeat", v.repeats);
    }

    if (v.anyText)
    {
        emitPair("anyText", v.anyText);
    }

    endMapping();
}
