/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_context.hpp"
#include "body_render_data.hpp"

class RenderEntityBodyTextureTile
{
public:
    RenderEntityBodyTextureTile(
        const RenderContext* ctx,
        const BodyRenderMaterialTextureData& texture,
        const uvec2& offset,
        const uvec2& extent
    );
    RenderEntityBodyTextureTile(const RenderEntityBodyTextureTile& rhs) = delete;
    RenderEntityBodyTextureTile(RenderEntityBodyTextureTile&& rhs) = delete;
    ~RenderEntityBodyTextureTile();

    RenderEntityBodyTextureTile& operator=(const RenderEntityBodyTextureTile& rhs) = delete;
    RenderEntityBodyTextureTile& operator=(RenderEntityBodyTextureTile&& rhs) = delete;

    void fill(const BodyRenderMaterialTextureData& texture);
    void setupLayout(VkCommandBuffer commandBuffer);

    VmaImage image;
    VkImageView imageView;
    VmaBuffer stagingBuffer;
    VmaMapping<u8>* stagingBufferMapping;

private:
    const RenderContext* ctx;
    uvec2 offset;
    uvec2 extent;
};
