/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "scale_tool.hpp"
#include "tools.hpp"
#include "localization.hpp"
#include "viewer_mode_context.hpp"

ScaleTool::ScaleTool(
    ViewerModeContext* ctx,
    const mat4& localSpace,
    const function<void(size_t, const vec3&, const mat4&, const mat4&)>& setGlobal,
    const function<mat4(size_t)>& getGlobal,
    const function<mat4(size_t)>& getGlobalWithPromotion,
    const function<size_t()>& count,
    bool soft,
    const Point& point
)
    : EditTool(ctx, localSpace, setGlobal, getGlobal, getGlobalWithPromotion, count, soft)
    , onScreenMultiplier(1)
    , onScreenCenter(getOnScreenCenter())
    , onScreenCursor(point)
    , onScreenStart(point)
{
}

ScaleTool::~ScaleTool()
{

}

string ScaleTool::name() const
{
    return localize("scale-tool-scale");
}

string ScaleTool::extent() const
{
    return "x " + toPrettyString(multiplier());
}

void ScaleTool::use(const Point& point)
{
    onScreenCursor = point;
    onScreenMultiplier = point.toVec2().distance(onScreenCenter.toVec2()) / onScreenStart.toVec2().distance(onScreenCenter.toVec2());

    update();
}

void ScaleTool::update()
{
    for (size_t i = 0; i < count(); i++)
    {
        setGlobal(i, globalPivot(i), initialGlobal(i), apply(globalPivot(i), initialGlobal(i)));
    }
}

void ScaleTool::cancel()
{
    for (size_t i = 0; i < count(); i++)
    {
        setGlobal(i, globalPivot(i), initialGlobal(i), initialGlobal(i));
    }
}

void ScaleTool::end()
{
    // Do nothing
}

void ScaleTool::togglePivotMode(const Point& point)
{
    EditTool::togglePivotMode(point);

    onScreenMultiplier = 1;
    onScreenCenter = getOnScreenCenter();
    onScreenCursor = point;
    onScreenStart = point;

    update();
}

Point ScaleTool::pivotPoint() const
{
    return onScreenCenter;
}

Point ScaleTool::cursorPoint() const
{
    return onScreenCursor;
}

optional<vec3> ScaleTool::globalAxis() const
{
    switch (toolAxis)
    {
    default :
    case Edit_Tool_Axis_None : return {};
    case Edit_Tool_Axis_Global_X : return rightDir;
    case Edit_Tool_Axis_Global_Y : return forwardDir;
    case Edit_Tool_Axis_Global_Z : return upDir;
    case Edit_Tool_Axis_Local_X : return localSpace.right();
    case Edit_Tool_Axis_Local_Y : return localSpace.forward();
    case Edit_Tool_Axis_Local_Z : return localSpace.up();
    case Edit_Tool_Axis_Self_X :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).right();
        }

        if (dir.length() == 0)
        {
            return rightDir;
        }
        else
        {
            return dir.normalize();
        }
    }
    case Edit_Tool_Axis_Self_Y :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).forward();
        }

        if (dir.length() == 0)
        {
            return forwardDir;
        }
        else
        {
            return dir.normalize();
        }
    }
    case Edit_Tool_Axis_Self_Z :
    {
        vec3 dir;

        for (size_t i = 0; i < count(); i++)
        {
            dir += initialGlobal(i).up();
        }

        if (dir.length() == 0)
        {
            return upDir;
        }
        else
        {
            return dir.normalize();
        }
    }
    }
}

f64 ScaleTool::multiplier() const
{
    f64 multiplier;

    if (numericInput)
    {
        multiplier = numericInputValue();
    }
    else
    {
        multiplier = onScreenMultiplier;
    }

    return snap(multiplier);
}

f64 ScaleTool::snap(f64 v) const
{
    if (!snapping() || roughly<f64>(ctx->scaleStep(), 0))
    {
        return v;
    }

    return floor(v / ctx->scaleStep()) * ctx->scaleStep();
}

mat4 ScaleTool::apply(const vec3& pivot, mat4 transform) const
{
    if (globalAxis())
    {
        // Don't touch this, it's very fragile
        vec3 axis = *globalAxis();

        mat4 scale(
            1, 0, 0, 0,
            0, multiplier(), 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1
        );

        mat4 space(pivot, quaternion(axis, pickSideDirection(axis)), vec3(1));

        transform = space.applyToTransform(transform, false);

        quaternion rotation = transform.rotation();

        transform = scale.applyToTransform(transform);

        transform.setRotation(rotation);

        transform = space.applyToTransform(transform, true);
    }
    else
    {
        transform.setPosition(pivot + (transform.position() - pivot) * multiplier());
        transform.setScale(transform.scale() * multiplier());
    }

    return transform;
}
