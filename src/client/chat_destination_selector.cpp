/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "chat_destination_selector.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

ChatDestinationSelectorPopup::ChatDestinationSelectorPopup(
    InterfaceWidget* parent,
    const optional<ChatDestination>& destination,
    const vector<tuple<string, ChatDestination>>& destinations,
    const function<void(size_t)>& onSet
)
    : Popup(parent)
    , destination(destination)
    , destinations(destinations)
    , onSet(onSet)
    , scrollIndex(0)
    , activeIndex(-1)
{
    START_WIDGET_SCOPE("chat-destination-selector-popup")
        WIDGET_SLOT("interact", interact)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)
    END_SCOPE
}

void ChatDestinationSelectorPopup::step()
{
    InterfaceWidget::step();

    Point local = pointer - position();

    activeIndex = scrollIndex + (local.y / UIScale::destinationSelectorRowHeight());
}

void ChatDestinationSelectorPopup::draw(const DrawContext& ctx) const
{
    ctx.delayedCommands.push_back([this](const DrawContext& ctx) -> void {
        drawSolid(ctx, this, theme.indentColor);
    });

    for (i32 i = scrollIndex; i < static_cast<i32>(destinations.size()); i++)
    {
        ctx.delayedCommands.push_back([this, i](const DrawContext& ctx) -> void {
            const string& text = get<0>(destinations[i]);
            const ChatDestination& destination = get<1>(destinations[i]);

            TextStyle style;

            if (i == 0 && !this->destination)
            {
                style.weight = Text_Bold;
            }
            else if (this->destination && destination == *this->destination)
            {
                style.weight = Text_Bold;
            }

            if (i == activeIndex)
            {
                drawSolid(
                    ctx,
                    this,
                    Point(0, i * UIScale::destinationSelectorRowHeight()),
                    Size(UIScale::destinationSelectorPopupWidth(), UIScale::destinationSelectorRowHeight()),
                    theme.accentColor
                );
            }

            drawText(
                ctx,
                this,
                Point(0, i * UIScale::destinationSelectorRowHeight()),
                Size(UIScale::destinationSelectorPopupWidth(), UIScale::destinationSelectorRowHeight()),
                text,
                style
            );
        });
    }
}

size_t ChatDestinationSelectorPopup::maxScrollIndex() const
{
    if (destinations.size() > static_cast<size_t>(UIScale::destinationSelectorRows()))
    {
        return destinations.size() - UIScale::destinationSelectorRows();
    }
    else
    {
        return 0;
    }
}

void ChatDestinationSelectorPopup::interact(const Input& input)
{
    Point local = pointer - position();

    size_t index = scrollIndex + (local.y / UIScale::destinationSelectorRowHeight());

    onSet(index);
}

void ChatDestinationSelectorPopup::scrollUp(const Input& input)
{
    if (scrollIndex != 0)
    {
        scrollIndex--;
    }
}

void ChatDestinationSelectorPopup::scrollDown(const Input& input)
{
    if (scrollIndex != static_cast<i32>(maxScrollIndex()))
    {
        scrollIndex++;
    }
}

void ChatDestinationSelectorPopup::goToStart(const Input& input)
{
    scrollIndex = 0;
}

void ChatDestinationSelectorPopup::goToEnd(const Input& input)
{
    scrollIndex = maxScrollIndex();
}

SizeProperties ChatDestinationSelectorPopup::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::destinationSelectorPopupWidth()), static_cast<f64>(UIScale::destinationSelectorPopupHeight()),
        Scaling_Fixed, Scaling_Fixed
    };
}

ChatDestinationSelector::ChatDestinationSelector(
    InterfaceWidget* parent,
    const function<void(const optional<ChatDestination>&)>& onSet
)
    : InterfaceWidget(parent)
    , onSet(onSet)
{
    START_WIDGET_SCOPE("chat-destination-selector")
        WIDGET_SLOT("interact", interact)
    END_SCOPE

    popup = new ChatDestinationSelectorPopup(
        this,
        destination,
        destinations,
        bind(&ChatDestinationSelector::onSelect, this, placeholders::_1)
    );
}

ChatDestinationSelector::~ChatDestinationSelector()
{
    delete popup;
}

void ChatDestinationSelector::move(const Point& position)
{
    this->position(position);

    popup->move(position + Point(0, size().h));
}

void ChatDestinationSelector::resize(const Size& size)
{
    this->size(size);

    popup->resize(Size(
        UIScale::destinationSelectorPopupWidth(),
        min<i32>(UIScale::destinationSelectorRows(), allDestinations().size()) * UIScale::destinationSelectorRowHeight()
    ));
}

void ChatDestinationSelector::step()
{
    InterfaceWidget::step();

    vector<tuple<string, ChatDestination>> newDestinations = allDestinations();

    if (newDestinations != destinations)
    {
        destination = {};
        destinations = newDestinations;
        popup->scrollIndex = 0;
        popup->activeIndex = 0;
    }

    if (!focused() && !popup->focused())
    {
        popup->close();
    }
}

void ChatDestinationSelector::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        focused() ? theme.accentColor : theme.outdentColor
    );

    drawColoredImage(
        ctx,
        this,
        Icon_Dot,
        destination ? theme.accentColor : theme.inactiveTextColor
    );

    InterfaceWidget::draw(ctx);
}

void ChatDestinationSelector::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void ChatDestinationSelector::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

vector<tuple<string, ChatDestination>> ChatDestinationSelector::allDestinations() const
{
    if (!context()->controller()->self())
    {
        return {};
    }

    vector<tuple<string, ChatDestination>> targets;
    targets.reserve(1 + context()->controller()->self()->teams().size() + context()->controller()->game().users().size());

    targets.push_back({ localize("chat-destination-selector-none"), ChatDestination() });

    for (const string& team : context()->controller()->self()->teams())
    {
        targets.push_back({ localize("chat-destination-selector-team", team), ChatDestination(true, team) });
    }

    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        targets.push_back({ localize("chat-destination-selector-user", user.name()), ChatDestination(false, user.name()) });
    }

    return targets;
}

void ChatDestinationSelector::onSelect(size_t index)
{
    if (index == 0)
    {
        destination = {};
    }
    else if (index < destinations.size())
    {
        destination = get<1>(destinations[index]);
    }

    onSet(destination);
    popup->close();
}

void ChatDestinationSelector::interact(const Input& input)
{
    if (popup->opened())
    {
        popup->close();
        playNegativeActivateEffect();
    }
    else
    {
        popup->open();
        playPositiveActivateEffect();
    }

    if (popup->opened())
    {
        update();
    }
}

SizeProperties ChatDestinationSelector::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::destinationSelectorSize().w), static_cast<f64>(UIScale::destinationSelectorSize().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
