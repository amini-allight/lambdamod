/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_types.hpp"
#include "render_tools.hpp"

VmaImage::VmaImage()
{
    image = nullptr;
    allocation = nullptr;
}

VmaImage::VmaImage(VkImage image, VmaAllocation allocation)
    : image(image)
    , allocation(allocation)
{

}

VmaBuffer::VmaBuffer()
{
    buffer = nullptr;
    allocation = nullptr;
}

VmaBuffer::VmaBuffer(VkBuffer buffer, VmaAllocation allocation)
    : buffer(buffer)
    , allocation(allocation)
{

}
