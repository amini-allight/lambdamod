/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "input_types.hpp"
#include "widget_input_scope.hpp"

enum PositioningMode : u8
{
    Positioning_Fixed,
    Positioning_Fraction,
    Positioning_Fixed_Inverted,
    Positioning_Fraction_Inverted
};

enum ScalingMode : u8
{
    Scaling_Fixed,
    Scaling_Fraction,
    Scaling_Fill
};

struct PositionProperties
{
    f64 x;
    f64 y;
    PositioningMode horizontal;
    PositioningMode vertical;
};

struct SizeProperties
{
    f64 w;
    f64 h;
    ScalingMode horizontal;
    ScalingMode vertical;
};

static constexpr SizeProperties sizePropertiesFillAll = {
    0, 0,
    Scaling_Fill, Scaling_Fill
};

class ControlContext;
class InterfaceView;

class InterfaceWidget
{
public:
    InterfaceWidget(InterfaceWidget* parent);
    InterfaceWidget(InterfaceView* view);
    InterfaceWidget(const InterfaceWidget& rhs) = delete;
    InterfaceWidget(InterfaceWidget&& rhs) = delete;
    virtual ~InterfaceWidget();

    InterfaceWidget& operator=(const InterfaceWidget& rhs) = delete;
    InterfaceWidget& operator=(InterfaceWidget&& rhs) = delete;

    virtual bool movePointer(PointerType type, const Point& pointer, bool consumed);
    void update();
    virtual void move(const Point& position);
    virtual void resize(const Size& size);
    virtual void step();
    virtual void draw(const DrawContext& ctx) const;

    virtual void focus();
    virtual void unfocus();
    virtual void clearFocus();

    virtual bool shouldDraw() const;

    InterfaceWidget* parent() const;
    InterfaceWidget* rootParent() const;
    InterfaceView* view() const;
    ControlContext* context() const;
    const vector<InterfaceWidget*> children() const;
    void clearChildren();
    bool focused() const;

    bool contains(const Point& point) const;
    bool contains(const Point& subPoint, const Size& subSize, const Point& point) const;
    const Point& position() const;
    const Size& size() const;

    InputScope* scope() const;

protected:
    Point pointer;

    void addChild(InterfaceWidget* child);
    void removeChild(InterfaceWidget* child);

    void position(const Point& position);
    void size(const Size& size);

    void playFocusEffect() const;
    void playUnfocusEffect() const;
    void playPositiveActivateEffect() const;
    void playNegativeActivateEffect() const;

    template<typename T, typename... Args>
    void setScope(Args... args)
    {
        scopes.push(new T(args...));
    }
    template<typename T, typename... Args>
    void replaceScope(Args... args)
    {
        delete scopes.top();
        scopes.pop();
        scopes.push(new T(args...));
    }
    // for creating sub-scopes
    void createScope(
        InputScope* parent,
        const string& name,
        InputScopeMode mode,
        const function<bool(const Input&)>& isActive,
        const function<void(InputScope*)>& block
    );

private:
    InterfaceWidget* _parent;
    InterfaceView* _view;
    vector<InterfaceWidget*> _children;
    bool _focused;
    Point _position;
    Size _size;
    stack<InputScope*> scopes;

    friend class InterfaceView;
    friend class Layout;
    virtual PositionProperties positionProperties() const;
    virtual SizeProperties sizeProperties() const = 0;
};
