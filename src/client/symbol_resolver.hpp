/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "tools.hpp"
#include "body_parts.hpp"
#include "image.hpp"

#include <lunasvg.h>

class SymbolResolverKey
{
public:
    SymbolResolverKey(SymbolType type, u32 resolution);

    bool operator==(const SymbolResolverKey& rhs) const;
    bool operator!=(const SymbolResolverKey& rhs) const;
    bool operator>(const SymbolResolverKey& rhs) const;
    bool operator<(const SymbolResolverKey& rhs) const;
    bool operator>=(const SymbolResolverKey& rhs) const;
    bool operator<=(const SymbolResolverKey& rhs) const;
    strong_ordering operator<=>(const SymbolResolverKey& rhs) const;

private:
    SymbolType type;
    u32 resolution;
};

template<typename T>
class SymbolResolverCacheEntry
{
public:
    SymbolResolverCacheEntry(const T& value, chrono::milliseconds timeout)
        : value(value)
        , lastAccessed(currentTime())
        , timeout(timeout)
    {

    }

    T get() const
    {
        lastAccessed = currentTime();
        return value;
    }

    bool expired() const
    {
        return currentTime() - lastAccessed > timeout;
    }

private:
    T value;
    mutable chrono::milliseconds lastAccessed;
    chrono::milliseconds timeout;
};

class SymbolResolver
{
public:
    SymbolResolver();
    SymbolResolver(const SymbolResolver& rhs) = delete;
    SymbolResolver(SymbolResolver&& rhs) = delete;
    ~SymbolResolver();

    SymbolResolver& operator=(const SymbolResolver& rhs) = delete;
    SymbolResolver& operator=(SymbolResolver&& rhs) = delete;

    Image* resolve(SymbolType type, u32 resolution) const;

private:
    mutable map<SymbolType, SymbolResolverCacheEntry<lunasvg::Document*>> cachedSymbols;
    mutable map<SymbolResolverKey, SymbolResolverCacheEntry<Image*>> cachedImages;

    lunasvg::Document* loadSymbol(SymbolType type) const;
    Image* renderSymbol(lunasvg::Document* document, u32 resolution) const;

    string getSymbolPath(SymbolType type) const;

    void clean() const;
};

extern SymbolResolver* symbolResolver;
