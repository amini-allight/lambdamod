/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "side_panel.hpp"

class ListView;
class SampleEditor;

class SampleEditorSidePanel : public SidePanel
{
public:
    SampleEditorSidePanel(InterfaceWidget* parent);

    void step() override;

private:
    ListView* listView;

    SamplePartID id;

    void addCommonFields();
    void addToneFields();
    void clearChildren();

    string idSource() const;

    f64 startSource() const;
    void onStart(f64 start);

    u64 indexSource() const;
    void onIndex(u64 index);

    f64 lengthSource() const;
    void onLength(f64 length);

    f64 volumeSource() const;
    void onVolume(f64 volume);

    vector<string> typeOptionsSource();
    vector<string> typeOptionTooltipsSource();
    size_t typeSource();
    void onType(size_t index);

    f64 pitchSource() const;
    void onPitch(f64 pitch);

    SampleEditor* editor() const;
};
