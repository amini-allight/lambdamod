/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound.hpp"
#include "controller.hpp"
#include "sound_constants.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "global.hpp"
#include "entity.hpp"

Sound::Sound(Controller* controller)
    : controller(controller)
    , voiceInput(controller)
    , exposure(1)
    , lastWriteTime(currentTime<chrono::microseconds>())
    , speedOfSound(defaultSpeedOfSound)
{
    log("Initializing sound system...");

    samples.startup = loadWAV(soundPath + "/startup" + soundExt);
    samples.ping = loadWAV(soundPath + "/ping" + soundExt);
    samples.brake = loadWAV(soundPath + "/brake" + soundExt);
    samples.chat = loadWAV(soundPath + "/chat" + soundExt);
    samples.error = loadWAV(soundPath + "/error" + soundExt);
    samples.title = loadWAV(soundPath + "/title" + soundExt);
    samples.attach = loadWAV(soundPath + "/attach" + soundExt);
    samples.detach = loadWAV(soundPath + "/detach" + soundExt);
    samples.focus = loadWAV(soundPath + "/focus" + soundExt);
    samples.unfocus = loadWAV(soundPath + "/unfocus" + soundExt);
    samples.positiveActivate = loadWAV(soundPath + "/positive-activate" + soundExt);
    samples.negativeActivate = loadWAV(soundPath + "/negative-activate" + soundExt);

    hrir = loadHRIR(resourcePath() + hrirPath);

    log("Sound system initialized.");
}

Sound::~Sound()
{
    log("Shutting down sound system...");

    delete hrir;

    for (const auto& [ id, effect ] : postProcessEffects)
    {
        delete effect;
    }

    delete samples.negativeActivate;
    delete samples.positiveActivate;
    delete samples.unfocus;
    delete samples.focus;
    delete samples.detach;
    delete samples.attach;
    delete samples.title;
    delete samples.error;
    delete samples.chat;
    delete samples.brake;
    delete samples.ping;
    delete samples.startup;

    log("Sound system shut down.");
}

void Sound::step(f64 multiplier)
{
    voiceInput.step();

    chrono::microseconds elapsed = currentTime<chrono::microseconds>() - lastWriteTime;

    f64 exactChunkCount = static_cast<f64>(elapsed.count()) / static_cast<f64>(soundChunkInterval.count());
    f64 chunkCount = ceil(exactChunkCount);

    lastWriteTime = lastWriteTime + chrono::microseconds(static_cast<u64>(elapsed.count() * (chunkCount / exactChunkCount)));

    f32 totalMaxAmplitude = 0;

    for (size_t i = 0; i < chunkCount; i++)
    {
        outputChunk = SoundStereoChunk();

        for (auto& [ id, entity ] : entities)
        {
            entity.output(multiplier);
        }

        for (auto it = voiceStreams.begin(); it != voiceStreams.end();)
        {
            if (it->second.expired())
            {
                voiceStreams.erase(it++);
            }
            else
            {
                it->second.output(1);
                it++;
            }
        }

        if (g_config.soundMono)
        {
            outputChunk = outputChunk.toMono().toStereo();
        }

        totalMaxAmplitude += outputChunk.maxAmplitude();

        for (SoundPostProcessEffectID id : postProcessOrder)
        {
            SoundPostProcessEffect* effect = postProcessEffects.at(id);

            if (!effect->active())
            {
                continue;
            }

            effect->apply(outputChunk);
        }

        for (size_t i = 0; i < oneshots.size();)
        {
            SoundOneshot& oneshot = oneshots[i];

            if (oneshot.completed())
            {
                oneshots.erase(oneshots.begin() + i);
            }
            else
            {
                oneshot.output(1);
                i++;
            }
        }

        outputChunk.expose(exposure);

        vector<i16> data = outputChunk.toRaw();
        u32 count = data.size();

        controller->soundDevice()->write(data.data(), count);
    }

    if (chunkCount != 0)
    {
        for (auto& [ id, entity ] : entities)
        {
            entity.recordLastDistance();
        }

        for (auto& [ id, voiceStream ] : voiceStreams)
        {
            voiceStream.recordLastDistance();
        }

        for (SoundOneshot& oneshot : oneshots)
        {
            oneshot.recordLastDistance();
        }

        adjustExposure(elapsed, totalMaxAmplitude / chunkCount);
    }
}

void Sound::setMicrophone(const vec3& position, const quaternion& rotation)
{
    microphoneTransform = mat4(position, rotation, vec3(1));
}

void Sound::setSpeedOfSound(f64 speedOfSound)
{
    this->speedOfSound = speedOfSound;
}

void Sound::add(EntityID parentID, const Entity* entity)
{
    if (parentID == EntityID())
    {
        auto it = entities.find(entity->id());

        if (it != entities.end())
        {
            return;
        }

        entities.insert({ entity->id(), SoundEntity(this, entity) });
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child.add(parentID, entity);
        }
    }
}

void Sound::remove(EntityID parentID, EntityID id)
{
    if (parentID == EntityID())
    {
        entities.erase(id);
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child.remove(parentID, id);
        }
    }
}

void Sound::update(EntityID parentID, const Entity* entity)
{
    if (parentID == EntityID())
    {
        auto it = entities.find(entity->id());

        if (it == entities.end())
        {
            add(parentID, entity);
            return;
        }

        SoundEntity& soundEntity = it->second;

        soundEntity.update(entity);
    }
    else
    {
        for (auto& [ childID, child ] : entities)
        {
            child.update(parentID, entity);
        }
    }
}

void Sound::addOneshot(SoundOneshotType type, const vec3& position)
{
    SoundEffect* effect = nullptr;

    switch (type)
    {
    case Sound_Oneshot_Startup :
        effect = samples.startup;
        break;
    case Sound_Oneshot_Ping :
        effect = samples.ping;
        break;
    case Sound_Oneshot_Brake :
        effect = samples.brake;
        break;
    case Sound_Oneshot_Chat :
        effect = samples.chat;
        break;
    case Sound_Oneshot_Error :
        effect = samples.error;
        break;
    case Sound_Oneshot_Title :
        effect = samples.title;
        break;
    case Sound_Oneshot_Attach :
        effect = samples.attach;
        break;
    case Sound_Oneshot_Detach :
        effect = samples.detach;
        break;
    case Sound_Oneshot_Focus :
        effect = samples.focus;
        break;
    case Sound_Oneshot_Unfocus :
        effect = samples.unfocus;
        break;
    case Sound_Oneshot_Positive_Activate :
        effect = samples.positiveActivate;
        break;
    case Sound_Oneshot_Negative_Activate :
        effect = samples.negativeActivate;
        break;
    }

    if (!effect)
    {
        return;
    }

    oneshots.push_back(SoundOneshot(this, effect, position));
}

void Sound::removePostProcessEffect(SoundPostProcessEffectID id)
{
    auto it = postProcessEffects.find(id);

    if (it == postProcessEffects.end())
    {
        return;
    }

    delete it->second;
    postProcessEffects.erase(it);
}

void Sound::setPostProcessEffectActive(SoundPostProcessEffectID id, bool state)
{
    auto it = postProcessEffects.find(id);

    if (it == postProcessEffects.end())
    {
        return;
    }

    it->second->setActive(state);
}

void Sound::setPostProcessEffectOrder(const vector<SoundPostProcessEffectID>& order)
{
    postProcessOrder = order;
}

void Sound::pushVoice(
    UserID id,
    const vec3& position,
    const quaternion& rotation,
    const vec3& linearVelocity,
    const string& data
)
{
    const User* user = controller->game().getUser(id);

    if (!user)
    {
        return;
    }

    const Entity* host = controller->game().get(user->entityID());

    auto it = voiceStreams.find(id);

    if (it != voiceStreams.end())
    {
        it->second.update(position, rotation, host ? host->voiceVolume() : unattachedVoiceVolume, host);
        it->second.push(data);
    }
    else
    {
        SoundVoiceStream stream(this, position, rotation, host ? host->voiceVolume() : unattachedVoiceVolume, host);
        stream.push(data);

        voiceStreams.insert({ id, stream });
    }
}

void Sound::adjustExposure(const chrono::microseconds& elapsed, f32 amplitude)
{
    if (amplitude < soundAutoExposureMinAmplitude || amplitude > soundAutoExposureMaxAmplitude)
    {
        return;
    }

    amplitude = clamp(amplitude, soundAutoExposureMinAmplitude, soundAutoExposureMaxAmplitude);

    f32 targetExposure = log(numeric_limits<f32>::epsilon(), numbers::e) / -amplitude;

    if (targetExposure < exposure)
    {
        exposure = targetExposure;
    }
    else
    {
        exposure = exposure + (1 - exp(-(elapsed.count() / static_cast<f32>(soundAutoExposureDelay.count())))) * (targetExposure - exposure);
    }
}

vec3 Sound::earPosition(u32 ear) const
{
    vec3 earOffset = microphoneTransform.right() * (interEarDistance / 2) * (ear == 0 ? -1 : +1);

    return microphoneTransform.position() + earOffset;
}

void Sound::output(const variant<SoundOverlappedChunk, SoundOverlappedStereoChunk>& chunk)
{
    if (holds_alternative<SoundOverlappedChunk>(chunk))
    {
        outputChunk += removeOverlap(get<SoundOverlappedChunk>(chunk)).toStereo();
    }
    else
    {
        outputChunk += removeOverlap(get<SoundOverlappedStereoChunk>(chunk));
    }
}
