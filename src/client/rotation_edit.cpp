/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "rotation_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "units.hpp"
#include "unit_field.hpp"

RotationEdit::RotationEdit(
    InterfaceWidget* parent,
    const function<quaternion()>& source,
    const function<void(const quaternion&)>& onSet
)
    : InterfaceWidget(parent)
{
    if (g_config.quaternionEditor)
    {
        quaternionEdit = new QuaternionEdit(parent, source, onSet);
    }
    else
    {
        new UnitField(this, angleSuffix(), [this, source, onSet](InterfaceWidget* parent) -> void {
            eulerEdit = new EulerEdit(parent, source, onSet);
        });
    }
}

void RotationEdit::set(const quaternion& value)
{
    if (g_config.quaternionEditor)
    {
        quaternionEdit->set(value);
    }
    else
    {
        eulerEdit->set(value);
    }
}

quaternion RotationEdit::value() const
{
    if (g_config.quaternionEditor)
    {
        return quaternionEdit->value();
    }
    else
    {
        return eulerEdit->value();
    }
}

void RotationEdit::clear()
{
    if (g_config.quaternionEditor)
    {
        quaternionEdit->clear();
    }
    else
    {
        eulerEdit->clear();
    }
}

SizeProperties RotationEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
