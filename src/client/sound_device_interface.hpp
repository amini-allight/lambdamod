/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "sound_constants.hpp"

#include <boost/lockfree/spsc_queue.hpp>

class SoundDeviceInterface
{
public:
    SoundDeviceInterface();
    SoundDeviceInterface(const SoundDeviceInterface& rhs) = delete;
    SoundDeviceInterface(SoundDeviceInterface&& rhs) = delete;
    virtual ~SoundDeviceInterface() = 0;

    SoundDeviceInterface& operator=(const SoundDeviceInterface& rhs) = delete;
    SoundDeviceInterface& operator=(SoundDeviceInterface&& rhs) = delete;

    virtual void write(const i16* data, u32 count);
    virtual void read(i16* data, u32* count);

    void setOutputMuted(bool state);
    void setOutputVolume(f64 volume);
    void setInputMuted(bool state);
    void setInputVolume(f64 volume);

    bool outputMuted() const;
    f64 outputVolume() const;
    bool inputMuted() const;
    f64 inputVolume() const;

protected:
    bool _outputMuted;
    f64 _outputVolume;
    bool _inputMuted;
    f64 _inputVolume;

    boost::lockfree::spsc_queue<i16, boost::lockfree::capacity<soundBufferSize * soundChannels>> outputBuffer;
    boost::lockfree::spsc_queue<i16, boost::lockfree::capacity<soundBufferSize * soundChannels>> inputBuffer;
};
