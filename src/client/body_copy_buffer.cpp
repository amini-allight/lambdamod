/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_copy_buffer.hpp"
#include "viewer_mode_context.hpp"
#include "control_context.hpp"

BodyCopyBuffer::BodyCopyBuffer()
{

}

void BodyCopyBuffer::copy(
    const vec3& cameraPosition,
    const quaternion& cameraRotation,
    const Entity* entity,
    const set<BodyPart*>& parts
)
{
    previousCameraPosition = entity->globalTransform().applyToPosition(cameraPosition, false);
    previousCameraRotation = entity->globalTransform().applyToRotation(cameraRotation, false);
    buffer.clear();

    for (BodyPart* part : parts)
    {
        buffer.push_back(*part);
    }
}

void BodyCopyBuffer::paste(const vec3& cameraPosition, const quaternion& cameraRotation, Entity* entity)
{
    vec3 entitySpaceCameraPosition = entity->globalTransform().applyToPosition(cameraPosition, false);
    quaternion entitySpaceCameraRotation = entity->globalTransform().applyToRotation(cameraRotation, false);

    for (const BodyPart& part : buffer)
    {
        auto newPart = new BodyPart(nullptr, entity->body().nextPartID(), part);

        vec3 cameraSpacePosition = previousCameraRotation.inverse().rotate(part.regionalPosition() - previousCameraPosition);
        quaternion cameraSpaceRotation = part.regionalRotation() / previousCameraRotation;

        newPart->setLocalPosition(entitySpaceCameraPosition + entitySpaceCameraRotation.rotate(cameraSpacePosition));
        newPart->setLocalRotation(entitySpaceCameraRotation * cameraSpaceRotation);

        entity->body().add(BodyPartID(), newPart);
    }
}
