/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "gamepad_viewer_menu.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "panel_text_button.hpp"

GamepadViewerMenu::GamepadViewerMenu(InterfaceView* view)
    : GamepadMenu(view)
{
    auto column = new Column(this);

    new PanelTextButton(column, localize("gamepad-viewer-menu-disconnect"), bind(&GamepadViewerMenu::onDisconnect, this));

    MAKE_SPACER(column, 0, UIScale::panelGamepadMenuItemSpacing(this));

    new PanelTextButton(column, localize("gamepad-viewer-menu-quit"), bind(&GamepadViewerMenu::onQuit, this));
}

bool GamepadViewerMenu::shouldDraw() const
{
    return GamepadMenu::shouldDraw() && context()->viewerMode()->gamepadMenu();
}

void GamepadViewerMenu::onDisconnect()
{
    context()->controller()->disconnect();
}

void GamepadViewerMenu::onQuit()
{
     context()->controller()->quit();   
}
