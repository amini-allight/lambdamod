/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_types.hpp"
#include "interface_widget.hpp"
#include "input_types.hpp"
#include "connection_details.hpp"
#include "gamepad_pointer_manager.hpp"
#include "vr_capable_panel.hpp"
#include "main_menu_background.hpp"

class Column;
class PanelLineEdit;

#ifdef LMOD_VR
class MainMenu : public VRCapablePanel
#else
class MainMenu : public InterfaceWidget
#endif
{
public:
    MainMenu(InterfaceView* view);
    ~MainMenu();

#ifdef LMOD_VR
    bool movePointer(PointerType type, const Point& pointer, bool consumed) override;
#endif
    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;

    bool shouldDraw() const override;

private:
    GamepadPointerManager pointerManager;
    InterfaceWidget* icon;
    InterfaceWidget* title;
    Column* column;
    MainMenuBackground* background;
#ifdef LMOD_VR
    mutable optional<chrono::milliseconds> firstClearDrawTime;
#endif
    InterfaceWidget* dialog;
    bool destroyDialog;

    struct {
        PanelLineEdit* host;
        PanelLineEdit* port;
        PanelLineEdit* serverPassword;
        PanelLineEdit* name;
        PanelLineEdit* password;
    } connect;

    struct {
        PanelLineEdit* host;
        PanelLineEdit* port;
        PanelLineEdit* serverPassword;
        PanelLineEdit* adminPassword;
        PanelLineEdit* savePath;
        PanelLineEdit* name;
        PanelLineEdit* password;
    } host;

    void onConnect();
    void onHost();
    void onQuit();

    ConnectionDetails loadConnectionDetails() const;

#ifdef LMOD_VR
    optional<Point> toLocal(const vec3& position, const quaternion& rotation) const;

    void changeFocus(const Input& input);
#endif

    SizeProperties sizeProperties() const override;
};
