/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_viewer.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "entity.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "theme.hpp"
#include "vr.hpp"
#include "vr_render.hpp"
#include "sound.hpp"

#include "render_vr_hardware_device.hpp"
#include "render_tracking_calibrator.hpp"
#include "render_laser_pointer.hpp"
#include "render_vr_grid.hpp"
#include "render_roll_cage.hpp"

using namespace std;

static constexpr f64 viewerSpeed = 1;
static constexpr f64 viewerSprintSpeed = 10;
static constexpr f64 viewerVerticalSpeed = 1;
static constexpr f64 turnDeadZone = 0.2;
static constexpr f64 snapThreshold = 0.25;
static constexpr f64 trackingCalibratorHeadOffset = 0.2;

VRViewer::VRViewer(ControlContext* ctx)
    : ctx(ctx)
    , lastStickMotionTime{ chrono::milliseconds(0) }
    , lastTouchpadMotionTime{ chrono::milliseconds(0) }
    , ascending(false)
    , descending(false)
    , sprinting(false)
{
    leftHandID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Hand, vec3(), quaternion());
    render()->setDecorationShown(leftHandID, false);

    rightHandID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Hand, vec3(), quaternion());
    render()->setDecorationShown(rightHandID, false);

    hipsID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Hips, vec3(), quaternion());
    render()->setDecorationShown(hipsID, false);

    leftFootID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Foot, vec3(), quaternion());
    render()->setDecorationShown(leftFootID, false);

    rightFootID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Foot, vec3(), quaternion());
    render()->setDecorationShown(rightFootID, false);

    chestID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Chest, vec3(), quaternion());
    render()->setDecorationShown(chestID, false);

    leftShoulderID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Shoulder, vec3(), quaternion());
    render()->setDecorationShown(leftShoulderID, false);

    rightShoulderID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Shoulder, vec3(), quaternion());
    render()->setDecorationShown(rightShoulderID, false);

    leftElbowID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Elbow, vec3(), quaternion());
    render()->setDecorationShown(leftElbowID, false);

    rightElbowID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Elbow, vec3(), quaternion());
    render()->setDecorationShown(rightElbowID, false);

    leftWristID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Wrist, vec3(), quaternion());
    render()->setDecorationShown(leftWristID, false);

    rightWristID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Wrist, vec3(), quaternion());
    render()->setDecorationShown(rightWristID, false);

    leftKneeID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Knee, vec3(), quaternion());
    render()->setDecorationShown(leftKneeID, false);

    rightKneeID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Knee, vec3(), quaternion());
    render()->setDecorationShown(rightKneeID, false);

    leftAnkleID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Left_Ankle, vec3(), quaternion());
    render()->setDecorationShown(leftAnkleID, false);

    rightAnkleID = render()->addDecoration<RenderVRHardwareDevice>(VR_Device_Right_Ankle, vec3(), quaternion());
    render()->setDecorationShown(rightAnkleID, false);

    leftLaserID = render()->addDecoration<RenderLaserPointer>(vec3(), forwardDir);
    render()->setDecorationShown(leftLaserID, false);

    rightLaserID = render()->addDecoration<RenderLaserPointer>(vec3(), forwardDir);
    render()->setDecorationShown(rightLaserID, false);

    trackingCalibratorID = render()->addDecoration<RenderTrackingCalibrator>(
        vec3(),
        map<VRDevice, mat4>({
            { VR_Device_Head, mat4() }
        }
    ));
    render()->setDecorationShown(trackingCalibratorID, false);

    gridID = render()->addDecoration<RenderVRGrid>(vec3(), quaternion());
    render()->setDecorationShown(gridID, false);

    rollCageID = render()->addDecoration<RenderRollCage>();
}

VRViewer::~VRViewer()
{
    render()->removeDecoration(rollCageID);
    render()->removeDecoration(gridID);
    render()->removeDecoration(trackingCalibratorID);
    render()->removeDecoration(rightLaserID);
    render()->removeDecoration(leftLaserID);
    render()->removeDecoration(rightAnkleID);
    render()->removeDecoration(leftAnkleID);
    render()->removeDecoration(rightKneeID);
    render()->removeDecoration(leftKneeID);
    render()->removeDecoration(rightWristID);
    render()->removeDecoration(leftWristID);
    render()->removeDecoration(rightElbowID);
    render()->removeDecoration(leftElbowID);
    render()->removeDecoration(rightShoulderID);
    render()->removeDecoration(leftShoulderID);
    render()->removeDecoration(chestID);
    render()->removeDecoration(rightFootID);
    render()->removeDecoration(leftFootID);
    render()->removeDecoration(hipsID);
    render()->removeDecoration(rightHandID);
    render()->removeDecoration(leftHandID);
}

void VRViewer::moveRoom(const vec3& position, const quaternion& rotation)
{
    roomPosition = position;
    roomRotation = rotation;

    updateRoom();
}

void VRViewer::step()
{
    f64 stepInterval = 1.0 / g_config.stepRate;

    render()->setDecorationShown(rollCageID, g_config.vrRollCage);

    ctx->controller()->render()->setCameraSpace(ctx->viewerSpace().rotation());
    ctx->controller()->sound()->setSpeedOfSound(ctx->controller()->selfWorld()->speedOfSound(headTransform().position()));

    // Movement
    vec3 leftDir;
    vec3 rightDir;

    switch (g_config.vrMotionMode)
    {
    case VR_Motion_Head :
        leftDir = transforms.head.forward();
        rightDir = transforms.head.forward();
        break;
    case VR_Motion_Hand :
        leftDir = transforms.hand[0].forward();
        rightDir = transforms.hand[1].forward();
        break;
    case VR_Motion_Hip :
        leftDir = transforms.hips.up();
        rightDir = transforms.hips.up();
        break;
    }

    leftDir = ctx->viewerSpace().up().cross(leftDir.cross(ctx->viewerSpace().up()).normalize()).normalize();
    rightDir = ctx->viewerSpace().up().cross(rightDir.cross(ctx->viewerSpace().up()).normalize()).normalize();

    f64 speed = sprinting ? viewerSprintSpeed : viewerSpeed;

    vec3 movement = (
        (leftDir * stickMotions[0].y) +
        (rightDir * stickMotions[1].y) +
        (leftDir * touchpadMotions[0].y) +
        (rightDir * touchpadMotions[1].y)
    ).normalize() * max(stickMotions[0].y, max(stickMotions[1].y, max(touchpadMotions[0].y, touchpadMotions[1].y))) * speed * stepInterval;

    movement += ctx->viewerSpace().up() * (ascending ? +1 : 0) * viewerVerticalSpeed * stepInterval;
    movement += ctx->viewerSpace().up() * (descending ? -1 : 0) * viewerVerticalSpeed * stepInterval;

    roomPosition += movement;

    // Updates
    updateRoom();

    mat4 eyeTransform = render()->worldSpaceEyeTransform();

    updateServerViewer(eyeTransform.position() + movement, eyeTransform.rotation());
}

void VRViewer::moveDevice(VRDevice device, const vec3& position, const quaternion& rotation)
{
    switch (device)
    {
    case VR_Device_Head :
        transforms.headValid = true;
        transforms.head = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Hand :
        transforms.handValid[0] = true;
        transforms.hand[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Hand :
        transforms.handValid[1] = true;
        transforms.hand[1] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Hips :
        transforms.hipsValid = true;
        transforms.hips = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Foot :
        transforms.footValid[0] = true;
        transforms.foot[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Foot :
        transforms.footValid[1] = true;
        transforms.foot[1] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Chest :
        transforms.chestValid = true;
        transforms.chest = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Shoulder :
        transforms.shoulderValid[0] = true;
        transforms.shoulder[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Shoulder :
        transforms.shoulderValid[0] = true;
        transforms.shoulder[1] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Elbow :
        transforms.elbowValid[0] = true;
        transforms.elbow[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Elbow :
        transforms.elbowValid[0] = true;
        transforms.elbow[1] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Wrist :
        transforms.wristValid[0] = true;
        transforms.wrist[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Wrist :
        transforms.wristValid[0] = true;
        transforms.wrist[1] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Knee :
        transforms.kneeValid[0] = true;
        transforms.knee[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Knee :
        transforms.kneeValid[1] = true;
        transforms.knee[1] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Left_Ankle :
        transforms.ankleValid[0] = true;
        transforms.ankle[0] = { position, rotation, vec3(1) };
        break;
    case VR_Device_Right_Ankle :
        transforms.ankleValid[0] = true;
        transforms.ankle[1] = { position, rotation, vec3(1) };
        break;
    }

    // Devices & attachment
    if (!ctx->attached())
    {
        switch (device)
        {
        case VR_Device_Head :
            break;
        case VR_Device_Left_Hand :
            render()->updateDecoration<RenderVRHardwareDevice>(leftHandID, device, position, rotation);
            break;
        case VR_Device_Right_Hand :
            render()->updateDecoration<RenderVRHardwareDevice>(rightHandID, device, position, rotation);
            break;
        case VR_Device_Hips :
            render()->updateDecoration<RenderVRHardwareDevice>(hipsID, device, position, rotation);
            break;
        case VR_Device_Left_Foot :
            render()->updateDecoration<RenderVRHardwareDevice>(leftFootID, device, position, rotation);
            break;
        case VR_Device_Right_Foot :
            render()->updateDecoration<RenderVRHardwareDevice>(rightFootID, device, position, rotation);
            break;
        case VR_Device_Chest :
            render()->updateDecoration<RenderVRHardwareDevice>(chestID, device, position, rotation);
            break;
        case VR_Device_Left_Shoulder :
            render()->updateDecoration<RenderVRHardwareDevice>(leftShoulderID, device, position, rotation);
            break;
        case VR_Device_Right_Shoulder :
            render()->updateDecoration<RenderVRHardwareDevice>(rightShoulderID, device, position, rotation);
            break;
        case VR_Device_Left_Elbow :
            render()->updateDecoration<RenderVRHardwareDevice>(leftElbowID, device, position, rotation);
            break;
        case VR_Device_Right_Elbow :
            render()->updateDecoration<RenderVRHardwareDevice>(rightElbowID, device, position, rotation);
            break;
        case VR_Device_Left_Wrist :
            render()->updateDecoration<RenderVRHardwareDevice>(leftWristID, device, position, rotation);
            break;
        case VR_Device_Right_Wrist :
            render()->updateDecoration<RenderVRHardwareDevice>(rightWristID, device, position, rotation);
            break;
        case VR_Device_Left_Knee :
            render()->updateDecoration<RenderVRHardwareDevice>(leftKneeID, device, position, rotation);
            break;
        case VR_Device_Right_Knee :
            render()->updateDecoration<RenderVRHardwareDevice>(rightKneeID, device, position, rotation);
            break;
        case VR_Device_Left_Ankle :
            render()->updateDecoration<RenderVRHardwareDevice>(leftAnkleID, device, position, rotation);
            break;
        case VR_Device_Right_Ankle :
            render()->updateDecoration<RenderVRHardwareDevice>(rightAnkleID, device, position, rotation);
            break;
        }

        render()->updateDecoration<RenderVRGrid>(gridID, roomPosition, roomRotation);
    }
    else
    {
        render()->setDecorationShown(leftHandID, false);
        render()->setDecorationShown(rightHandID, false);
        render()->setDecorationShown(hipsID, false);
        render()->setDecorationShown(leftFootID, false);
        render()->setDecorationShown(rightFootID, false);
        render()->setDecorationShown(chestID, false);
        render()->setDecorationShown(leftShoulderID, false);
        render()->setDecorationShown(rightShoulderID, false);
        render()->setDecorationShown(leftElbowID, false);
        render()->setDecorationShown(rightElbowID, false);
        render()->setDecorationShown(leftWristID, false);
        render()->setDecorationShown(rightWristID, false);
        render()->setDecorationShown(leftKneeID, false);
        render()->setDecorationShown(rightKneeID, false);
        render()->setDecorationShown(leftAnkleID, false);
        render()->setDecorationShown(rightAnkleID, false);

        render()->setDecorationShown(gridID, false);
    }

    // Tracking calibration
    if (ctx->attachedMode()->calibratingTracking())
    {
        map<VRDevice, mat4> mapping = this->mapping();

        for (auto& [ device, transform ] : mapping)
        {
            if (device == VR_Device_Head)
            {
                transform.setPosition(transform.position() + transform.forward() * trackingCalibratorHeadOffset);
            }
        }

        render()->updateDecoration<RenderTrackingCalibrator>(
            trackingCalibratorID,
            ctx->controller()->host()->globalPosition(),
            mapping
        );
    }
    else
    {
        render()->setDecorationShown(trackingCalibratorID, false);
    }

    // Lasers & keyboard
    if ((device == VR_Device_Left_Hand || device == VR_Device_Right_Hand) && (
        ctx->text()->needsInteraction() ||
        ctx->vrMenuButtonHandler()->vrMenu() ||
        ctx->attachedMode()->knowledgeViewer() ||
        ctx->controller()->standby()
    ))
    {
        render()->updateDecoration<RenderLaserPointer>(
            device == VR_Device_Left_Hand ? leftLaserID : rightLaserID,
            position,
            rotation.forward()
        );

        if (ctx->controller()->standby())
        {
            render()->showKeyboard(true);
            render()->setKeyboard(
                transforms.hand[g_config.vrKeyboardRightHand].position(),
                transforms.hand[g_config.vrKeyboardRightHand].rotation()
            );
            dynamic_cast<VRKeyboard*>(ctx->interface()->getView("vr-keyboard")->children().front())->moveKeyboard(
                transforms.hand[g_config.vrKeyboardRightHand].position(),
                transforms.hand[g_config.vrKeyboardRightHand].rotation()
            );
        }
    }

    // No lasers
    if (!ctx->text()->needsInteraction() &&
        !ctx->vrMenuButtonHandler()->vrMenu() &&
        !ctx->attachedMode()->knowledgeViewer() &&
        !ctx->controller()->standby()
    )
    {
        render()->setDecorationShown(leftLaserID, false);
        render()->setDecorationShown(rightLaserID, false);
    }

    // No keyboard
    if (!ctx->controller()->standby())
    {
        render()->showKeyboard(false);
    }

    ctx->controller()->send(UserEvent(
        User_Event_Move_Devices,
        ctx->controller()->selfID(),
        mapping()
    ));

    // Update local & remote IK pose
    if (const User* self = ctx->controller()->self(); self && self->anchored())
    {
        ctx->controller()->host()->world()->setPose(self->entityID(), self->anchorMapping(), mapping());

        ctx->controller()->send(PoseEvent(mapping()));
    }
}

void VRViewer::stickMotion(bool right, const vec3& position)
{
    if (lastStickMotionTime[right] == chrono::milliseconds(0))
    {
        lastStickMotionTime[right] = currentTime();
    }

    f64 elapsed = (currentTime() - lastStickMotionTime[right]).count() / 1000.0;

    // Turn
    if (!g_config.vrSmoothTurning)
    {
        if ((stickMotions[right].x <= +turnDeadZone && stickMotions[right].x >= -turnDeadZone) &&
            (position.x > +turnDeadZone || position.x < -turnDeadZone)
        )
        {
            f64 delta = position.x - stickMotions[right].x;

            quaternion snap;

            if (delta >= +snapThreshold)
            {
                snap = quaternion(upDir, -g_config.vrSnapTurnIncrement);
            }
            else if (delta <= -snapThreshold)
            {
                snap = quaternion(upDir, +g_config.vrSnapTurnIncrement);
            }

            vec3 viewerPosition = render()->worldSpaceEyeTransform().position();
            roomPosition = snap.rotate(roomPosition - viewerPosition) + viewerPosition;
            roomRotation = roomRotation * snap;

            updateRoom();
        }
    }
    else
    {
        if (position.x > +turnDeadZone || position.x < -turnDeadZone)
        {
            quaternion smooth(upDir, position.x * -1 * g_config.vrSmoothTurnSpeed * elapsed);

            vec3 viewerPosition = render()->worldSpaceEyeTransform().position();
            roomPosition = smooth.rotate(roomPosition - viewerPosition) + viewerPosition;
            roomRotation = roomRotation * smooth;

            updateRoom();
        }
    }

    // Move
    stickMotions[right] = position;
    lastStickMotionTime[right] = currentTime();
}

void VRViewer::touchpadMotion(bool right, bool state, const vec3& position)
{
    if (lastTouchpadMotionTime[right] == chrono::milliseconds(0))
    {
        lastTouchpadMotionTime[right] = chrono::milliseconds(0);
    }

    f64 elapsed = (currentTime() - lastTouchpadMotionTime[right]).count() / 1000.0;

    // Turn
    if (state)
    {
        if (!g_config.vrSmoothTurning)
        {
            if ((touchpadMotions[right].x <= +turnDeadZone && touchpadMotions[right].x >= -turnDeadZone) &&
                (position.x > +turnDeadZone || position.x < -turnDeadZone)
            )
            {
                f64 delta = position.x - touchpadMotions[right].x;

                quaternion snap;

                if (delta >= +snapThreshold)
                {
                    snap = quaternion(upDir, -g_config.vrSnapTurnIncrement);
                }
                else if (delta <= -snapThreshold)
                {
                    snap = quaternion(upDir, +g_config.vrSnapTurnIncrement);
                }

                vec3 viewerPosition = render()->worldSpaceEyeTransform().position();
                roomPosition = snap.rotate(roomPosition - viewerPosition) + viewerPosition;
                roomRotation = roomRotation * snap;

                updateRoom();
            }
        }
        else
        {
            if (position.x > +turnDeadZone || position.x < -turnDeadZone)
            {
                quaternion smooth(upDir, position.x * -1 * g_config.vrSmoothTurnSpeed * elapsed);

                vec3 viewerPosition = render()->worldSpaceEyeTransform().position();
                roomPosition = smooth.rotate(roomPosition - viewerPosition) + viewerPosition;
                roomRotation = roomRotation * smooth;

                updateRoom();
            }
        }
    }

    // Move
    if (state)
    {
        touchpadMotions[right] = position;
    }
    else
    {
        touchpadMotions[right] = vec3();
    }

    lastTouchpadMotionTime[right] = currentTime();
}

void VRViewer::ascend(bool state)
{
    ascending = state;
}

void VRViewer::descend(bool state)
{
    descending = state;
}

void VRViewer::sprint(bool state)
{
    sprinting = state;
}

void VRViewer::attach(bool right)
{
    vec3 position = transforms.hand[right].position();

    f64 minBoundingRadius = numeric_limits<f64>::max();
    Entity* bestEntity = nullptr;

    ctx->controller()->selfWorld()->traverse([&](Entity* entity) -> void {
        if (!entity->host())
        {
            return;
        }

        f64 boundingRadius = entity->boundingRadius();

        if (entity->globalPosition().distance(position) < boundingRadius && boundingRadius < minBoundingRadius)
        {
            minBoundingRadius = boundingRadius;
            bestEntity = entity;
        }
    });

    if (!bestEntity)
    {
        return;
    }

    GameEvent event(Game_Event_Attach);
    event.u = bestEntity->id().value();

    ctx->controller()->send(event);
}

mat4 VRViewer::roomTransform() const
{
    return { roomPosition, roomRotation, vec3(1) };
}

mat4 VRViewer::headTransform() const
{
    return transforms.head;
}

mat4 VRViewer::leftHandTransform() const
{
    return transforms.hand[0];
}

mat4 VRViewer::rightHandTransform() const
{
    return transforms.hand[1];
}

mat4 VRViewer::hipsTransform() const
{
    return transforms.hips;
}

mat4 VRViewer::leftFootTransform() const
{
    return transforms.foot[0];
}

mat4 VRViewer::rightFootTransform() const
{
    return transforms.foot[1];
}

mat4 VRViewer::chestTransform() const
{
    return transforms.chest;
}

mat4 VRViewer::leftShoulderTransform() const
{
    return transforms.shoulder[0];
}

mat4 VRViewer::rightShoulderTransform() const
{
    return transforms.shoulder[1];
}

mat4 VRViewer::leftElbowTransform() const
{
    return transforms.elbow[0];
}

mat4 VRViewer::rightElbowTransform() const
{
    return transforms.elbow[1];
}

mat4 VRViewer::leftWristTransform() const
{
    return transforms.wrist[0];
}

mat4 VRViewer::rightWristTransform() const
{
    return transforms.wrist[1];
}

mat4 VRViewer::leftKneeTransform() const
{
    return transforms.knee[0];
}

mat4 VRViewer::rightKneeTransform() const
{
    return transforms.knee[1];
}

mat4 VRViewer::leftAnkleTransform() const
{
    return transforms.ankle[0];
}

mat4 VRViewer::rightAnkleTransform() const
{
    return transforms.ankle[1];
}

map<VRDevice, mat4> VRViewer::mapping() const
{
    map<VRDevice, mat4> mapping;

    if (transforms.headValid)
    {
        mapping.insert({ VR_Device_Head, transforms.head });
    }

    for (i32 i = 0; i < handCount; i++)
    {
        if (transforms.handValid[i])
        {
            mapping.insert({ i ? VR_Device_Right_Hand : VR_Device_Left_Hand, transforms.hand[i] });
        }
    }

    if (transforms.hipsValid)
    {
        mapping.insert({ VR_Device_Hips, transforms.hips });
    }

    for (i32 i = 0; i < bodyPartCount; i++)
    {
        if (transforms.footValid[i])
        {
            mapping.insert({ i ? VR_Device_Right_Foot : VR_Device_Left_Foot, transforms.foot[i] });
        }
    }

    return mapping;
}

void VRViewer::updateRoom()
{
    ctx->controller()->vr()->setRoom(
        roomPosition,
        roomRotation
    );
    ctx->controller()->sound()->setMicrophone(
        render()->worldSpaceEyeTransform().position(),
        render()->worldSpaceEyeTransform().rotation()
    );

    updateServerRoom();
}

void VRViewer::updateServerRoom()
{
    ctx->controller()->send(UserEvent(
        User_Event_Move_Room,
        ctx->controller()->selfID(),
        { roomPosition, roomRotation, vec3(1) }
    ));
}

void VRViewer::updateServerViewer(const vec3& position, const quaternion& rotation)
{
    ctx->controller()->send(UserEvent(
        User_Event_Move_Viewer,
        ctx->controller()->selfID(),
        { position, rotation, vec3(1) }
    ));
}

VRRender* VRViewer::render() const
{
    return dynamic_cast<VRRender*>(ctx->controller()->render());
}
#endif
