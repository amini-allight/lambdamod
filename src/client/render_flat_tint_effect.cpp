/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_tint_effect.hpp"
#include "render_tools.hpp"

RenderFlatTintEffect::RenderFlatTintEffect(
    const RenderContext* ctx,
    RenderFlatPipelineStore* pipelineStore,
    const vector<FlatSwapchainElement*>& swapchainElements,
    const vec3& color,
    f64 strength
)
    : RenderFlatPostProcessEffect(
        ctx,
        pipelineStore->tint.pipelineLayout,
        pipelineStore->tint.pipeline
    )
    , color(color, 1)
    , strength(strength)
{
    createComponents(swapchainElements, pipelineStore->postProcessDescriptorSetLayout);
}

RenderFlatTintEffect::~RenderFlatTintEffect()
{
    destroyComponents();
}

void RenderFlatTintEffect::update(const vec3& color, f64 strength)
{
    this->color = vec4(color, 1);
    this->strength = strength;
}

VmaBuffer RenderFlatTintEffect::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(fvec4) + sizeof(f32) + sizeof(f32),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderFlatTintEffect::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    memcpy(mapping->data, &color, sizeof(fvec4));
    memcpy(mapping->data + 4, &strength, sizeof(f32));
    memcpy(mapping->data + 4 + 1, &timer, sizeof(f32));
}
