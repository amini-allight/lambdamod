/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_draw_objects.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_pipeline_components.hpp"
#include "render_descriptor_set_components.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "global.hpp"

VkDescriptorPool descriptorPool;

VkDescriptorSetLayout lineDescriptorSetLayout;
VkDescriptorSetLayout solidDescriptorSetLayout;
VkDescriptorSetLayout curveDescriptorSetLayout;
VkDescriptorSetLayout imageDescriptorSetLayout;

VkShaderModule lineVertexShader;
VkShaderModule lineFragmentShader;
VkShaderModule solidVertexShader;
VkShaderModule solidFragmentShader;
VkShaderModule roundedBorderVertexShader;
VkShaderModule roundedBorderFragmentShader;
VkShaderModule imageVertexShader;
VkShaderModule imageFragmentShader;
VkShaderModule coloredImageVertexShader;
VkShaderModule coloredImageFragmentShader;
VkShaderModule dashedLineVertexShader;
VkShaderModule dashedLineFragmentShader;
VkShaderModule gradient1DVertexShader;
VkShaderModule gradient1DFragmentShader;
VkShaderModule gradient2DVertexShader;
VkShaderModule gradient2DFragmentShader;
VkShaderModule curveVertexShader;
VkShaderModule curveGeometryShader;
VkShaderModule curveFragmentShader;
VkShaderModule roundedSolidVertexShader;
VkShaderModule roundedSolidFragmentShader;

GraphicsPipeline line;
GraphicsPipeline solid;
GraphicsPipeline roundedBorder;
GraphicsPipeline image;
GraphicsPipeline coloredImage;

GraphicsPipeline dashedLine;
GraphicsPipeline gradient1D;
GraphicsPipeline gradient2D;

GraphicsPipeline curve;
GraphicsPipeline roundedSolid;

VkSampler sampler;

static void createLineGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkRenderPass uiRenderPass,
    VkRenderPass panelRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    GraphicsPipeline& pipeline
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipeline.pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(VK_SAMPLE_COUNT_1_BIT);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipeline.pipelineLayout;
    pipelineCreateInfo.renderPass = uiRenderPass;
    pipelineCreateInfo.subpass = 0;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    if (!g_vr)
    {
        result = vkCreateGraphicsPipelines(
            device,
            pipelineCache,
            1,
            &pipelineCreateInfo,
            nullptr,
            &pipeline.uiPipeline
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create pipeline: " + vulkanError(result));
        }
    }

    multisampleStage.rasterizationSamples = VK_SAMPLE_COUNT_1_BIT;
    pipelineCreateInfo.renderPass = panelRenderPass;

    result = vkCreateGraphicsPipelines(
        device,
        pipelineCache,
        1,
        &pipelineCreateInfo,
        nullptr,
        &pipeline.panelPipeline
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create pipeline: " + vulkanError(result));
    }
}


static void createSolidGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkRenderPass uiRenderPass,
    VkRenderPass panelRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    GraphicsPipeline& pipeline
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipeline.pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(VK_SAMPLE_COUNT_1_BIT);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipeline.pipelineLayout;
    pipelineCreateInfo.renderPass = uiRenderPass;
    pipelineCreateInfo.subpass = 0;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    if (!g_vr)
    {
        result = vkCreateGraphicsPipelines(
            device,
            pipelineCache,
            1,
            &pipelineCreateInfo,
            nullptr,
            &pipeline.uiPipeline
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create pipeline: " + vulkanError(result));
        }
    }

    pipelineCreateInfo.renderPass = panelRenderPass;

    result = vkCreateGraphicsPipelines(
        device,
        pipelineCache,
        1,
        &pipelineCreateInfo,
        nullptr,
        &pipeline.panelPipeline
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create pipeline: " + vulkanError(result));
    }
}

static void createCurveGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkRenderPass uiRenderPass,
    VkRenderPass panelRenderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    GraphicsPipeline& pipeline
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipeline.pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = pointInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(VK_SAMPLE_COUNT_1_BIT);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipeline.pipelineLayout;
    pipelineCreateInfo.renderPass = uiRenderPass;
    pipelineCreateInfo.subpass = 0;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    if (!g_vr)
    {
        result = vkCreateGraphicsPipelines(
            device,
            pipelineCache,
            1,
            &pipelineCreateInfo,
            nullptr,
            &pipeline.uiPipeline
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create pipeline: " + vulkanError(result));
        }
    }

    pipelineCreateInfo.renderPass = panelRenderPass;

    result = vkCreateGraphicsPipelines(
        device,
        pipelineCache,
        1,
        &pipelineCreateInfo,
        nullptr,
        &pipeline.panelPipeline
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create pipeline: " + vulkanError(result));
    }
}

static void destroyGraphicsPipeline(VkDevice device, const GraphicsPipeline& pipeline)
{
    vkDestroyPipeline(device, pipeline.panelPipeline, nullptr);

    if (!g_vr)
    {
        vkDestroyPipeline(device, pipeline.uiPipeline, nullptr);
    }

    vkDestroyPipelineLayout(device, pipeline.pipelineLayout, nullptr);
}

void createDescriptorPool(VkDevice device)
{
    VkResult result;

    VkDescriptorPoolSize bufferPoolSize{};
    bufferPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    bufferPoolSize.descriptorCount = maxDescriptorCount;

    VkDescriptorPoolSize imagePoolSize{};
    imagePoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    imagePoolSize.descriptorCount = maxDescriptorCount;

    VkDescriptorPoolSize poolSizes[] = {
        bufferPoolSize,
        imagePoolSize
    };

    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo{};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptorPoolCreateInfo.maxSets = maxDescriptorCount;
    descriptorPoolCreateInfo.poolSizeCount = 2;
    descriptorPoolCreateInfo.pPoolSizes = poolSizes;

    result = vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, nullptr, &descriptorPool);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface descriptor pool: " + vulkanError(result));
    }
}

static VkDescriptorSetLayout createDescriptorSetLayout(VkDevice device, VkDescriptorSetLayoutBinding* bindings, u32 bindingCount)
{
    VkResult result;

    VkDescriptorSetLayoutCreateInfo descriptorSetLayoutCreateInfo{};
    descriptorSetLayoutCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    descriptorSetLayoutCreateInfo.flags = 0;
    descriptorSetLayoutCreateInfo.bindingCount = bindingCount;
    descriptorSetLayoutCreateInfo.pBindings = bindings;

    VkDescriptorSetLayout descriptorSetLayout;

    result = vkCreateDescriptorSetLayout(device, &descriptorSetLayoutCreateInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface descriptor set layout: " + vulkanError(result));
    }

    return descriptorSetLayout;
}

void createDescriptorSetLayouts(VkDevice device)
{
    {
        VkDescriptorSetLayoutBinding bindings[] = {
            uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
        };

        lineDescriptorSetLayout = createDescriptorSetLayout(
            device,
            bindings,
            sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding)
        );
    }

    {
        VkDescriptorSetLayoutBinding bindings[] = {
            uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
        };

        solidDescriptorSetLayout = createDescriptorSetLayout(
            device,
            bindings,
            sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding)
        );
    }

    {
        VkDescriptorSetLayoutBinding bindings[] = {
            uniformBufferLayoutBinding(0, VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
        };

        curveDescriptorSetLayout = createDescriptorSetLayout(
            device,
            bindings,
            sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding)
        );
    }

    {
        VkDescriptorSetLayoutBinding bindings[] = {
            uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
            combinedSamplerLayoutBinding(1, VK_SHADER_STAGE_FRAGMENT_BIT)
        };

        imageDescriptorSetLayout = createDescriptorSetLayout(
            device,
            bindings,
            sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding)
        );
    }
}

static VkShaderModule createShader(VkDevice device, const string& path)
{
    VkResult result;

    string source = getFile(path);

    VkShaderModuleCreateInfo shaderCreateInfo{};
    shaderCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderCreateInfo.codeSize = source.size();
    shaderCreateInfo.pCode = reinterpret_cast<const u32*>(source.data());

    VkShaderModule shader;

    result = vkCreateShaderModule(device, &shaderCreateInfo, nullptr, &shader);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface shader module: " + vulkanError(result));
    }

    return shader;
}

void createShaders(VkDevice device)
{
    lineVertexShader = createShader(device, shaderPath + "/interface_line_vertex" + shaderExt);
    lineFragmentShader = createShader(device, shaderPath + "/interface_line_fragment" + shaderExt);
    solidVertexShader = createShader(device, shaderPath + "/interface_solid_vertex" + shaderExt);
    solidFragmentShader = createShader(device, shaderPath + "/interface_solid_fragment" + shaderExt);
    roundedBorderVertexShader = createShader(device, shaderPath + "/interface_rounded_border_vertex" + shaderExt);
    roundedBorderFragmentShader = createShader(device, shaderPath + "/interface_rounded_border_fragment" + shaderExt);
    imageVertexShader = createShader(device, shaderPath + "/interface_image_vertex" + shaderExt);
    imageFragmentShader = createShader(device, shaderPath + "/interface_image_fragment" + shaderExt);
    coloredImageVertexShader = createShader(device, shaderPath + "/interface_colored_image_vertex" + shaderExt);
    coloredImageFragmentShader = createShader(device, shaderPath + "/interface_colored_image_fragment" + shaderExt);
    dashedLineVertexShader = createShader(device, shaderPath + "/interface_dashed_line_vertex" + shaderExt);
    dashedLineFragmentShader = createShader(device, shaderPath + "/interface_dashed_line_fragment" + shaderExt);
    gradient1DVertexShader = createShader(device, shaderPath + "/interface_gradient_1d_vertex" + shaderExt);
    gradient1DFragmentShader = createShader(device, shaderPath + "/interface_gradient_1d_fragment" + shaderExt);
    gradient2DVertexShader = createShader(device, shaderPath + "/interface_gradient_2d_vertex" + shaderExt);
    gradient2DFragmentShader = createShader(device, shaderPath + "/interface_gradient_2d_fragment" + shaderExt);
    curveVertexShader = createShader(device, shaderPath + "/interface_curve_vertex" + shaderExt);
    curveGeometryShader = createShader(device, shaderPath + "/interface_curve_geometry" + shaderExt);
    curveFragmentShader = createShader(device, shaderPath + "/interface_curve_fragment" + shaderExt);
    roundedSolidVertexShader = createShader(device, shaderPath + "/interface_rounded_solid_vertex" + shaderExt);
    roundedSolidFragmentShader = createShader(device, shaderPath + "/interface_rounded_solid_fragment" + shaderExt);
}

void createGraphicsPipelines(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkRenderPass uiRenderPass,
    VkRenderPass panelRenderPass
)
{
    createLineGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        lineDescriptorSetLayout,
        lineVertexShader,
        lineFragmentShader,
        line
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        solidDescriptorSetLayout,
        solidVertexShader,
        solidFragmentShader,
        solid
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        solidDescriptorSetLayout,
        roundedBorderVertexShader,
        roundedBorderFragmentShader,
        roundedBorder
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        imageDescriptorSetLayout,
        imageVertexShader,
        imageFragmentShader,
        image
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        imageDescriptorSetLayout,
        coloredImageVertexShader,
        coloredImageFragmentShader,
        coloredImage
    );
    createLineGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        lineDescriptorSetLayout,
        dashedLineVertexShader,
        dashedLineFragmentShader,
        dashedLine
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        solidDescriptorSetLayout,
        gradient1DVertexShader,
        gradient1DFragmentShader,
        gradient1D
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        solidDescriptorSetLayout,
        gradient2DVertexShader,
        gradient2DFragmentShader,
        gradient2D
    );
    createCurveGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        curveDescriptorSetLayout,
        curveVertexShader,
        curveGeometryShader,
        curveFragmentShader,
        curve
    );
    createSolidGraphicsPipeline(
        device,
        pipelineCache,
        uiRenderPass,
        panelRenderPass,
        solidDescriptorSetLayout,
        roundedSolidVertexShader,
        roundedSolidFragmentShader,
        roundedSolid
    );
}

void createSampler(VkDevice device)
{
    VkResult result;

    VkSamplerCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    createInfo.magFilter = VK_FILTER_LINEAR;
    createInfo.minFilter = VK_FILTER_LINEAR;
    createInfo.mipmapMode = VK_SAMPLER_MIPMAP_MODE_LINEAR;
    createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    createInfo.mipLodBias = 0;
    createInfo.anisotropyEnable = false;
    createInfo.maxAnisotropy = 16;
    createInfo.compareEnable = false;
    createInfo.compareOp = VK_COMPARE_OP_NEVER;
    createInfo.minLod = 0;
    createInfo.maxLod = VK_LOD_CLAMP_NONE;
    createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    createInfo.unnormalizedCoordinates = false;

    result = vkCreateSampler(device, &createInfo, nullptr, &sampler);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create interface sampler: " + vulkanError(result));
    }
}

void destroyGraphicsPipelines(VkDevice device)
{
    destroyGraphicsPipeline(device, roundedSolid);
    destroyGraphicsPipeline(device, curve);
    destroyGraphicsPipeline(device, gradient2D);
    destroyGraphicsPipeline(device, gradient1D);
    destroyGraphicsPipeline(device, dashedLine);
    destroyGraphicsPipeline(device, coloredImage);
    destroyGraphicsPipeline(device, image);
    destroyGraphicsPipeline(device, roundedBorder);
    destroyGraphicsPipeline(device, solid);
    destroyGraphicsPipeline(device, line);
}

void destroyShaders(VkDevice device)
{
    vkDestroyShaderModule(device, roundedSolidFragmentShader, nullptr);
    vkDestroyShaderModule(device, roundedSolidVertexShader, nullptr);

    vkDestroyShaderModule(device, curveFragmentShader, nullptr);
    vkDestroyShaderModule(device, curveGeometryShader, nullptr);
    vkDestroyShaderModule(device, curveVertexShader, nullptr);

    vkDestroyShaderModule(device, gradient2DFragmentShader, nullptr);
    vkDestroyShaderModule(device, gradient2DVertexShader, nullptr);

    vkDestroyShaderModule(device, gradient1DFragmentShader, nullptr);
    vkDestroyShaderModule(device, gradient1DVertexShader, nullptr);

    vkDestroyShaderModule(device, dashedLineFragmentShader, nullptr);
    vkDestroyShaderModule(device, dashedLineVertexShader, nullptr);

    vkDestroyShaderModule(device, coloredImageFragmentShader, nullptr);
    vkDestroyShaderModule(device, coloredImageVertexShader, nullptr);

    vkDestroyShaderModule(device, imageFragmentShader, nullptr);
    vkDestroyShaderModule(device, imageVertexShader, nullptr);

    vkDestroyShaderModule(device, roundedBorderFragmentShader, nullptr);
    vkDestroyShaderModule(device, roundedBorderVertexShader, nullptr);

    vkDestroyShaderModule(device, solidFragmentShader, nullptr);
    vkDestroyShaderModule(device, solidVertexShader, nullptr);

    vkDestroyShaderModule(device, lineFragmentShader, nullptr);
    vkDestroyShaderModule(device, lineVertexShader, nullptr);
}

void destroyDescriptorSetLayouts(VkDevice device)
{
    vkDestroyDescriptorSetLayout(device, imageDescriptorSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, curveDescriptorSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, solidDescriptorSetLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, lineDescriptorSetLayout, nullptr);
}

void destroyDescriptorPool(VkDevice device)
{
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
}

void destroySampler(VkDevice device)
{
    vkDestroySampler(device, sampler, nullptr);
}
