/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "entity_logic_editor.hpp"
#include "localization.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "tab_view.hpp"
#include "field_definition_closure.hpp"
#include "script_tools.hpp"

EntityLogicEditor::EntityLogicEditor(InterfaceWidget* parent, EntityID id)
    : LogicEditor(parent)
    , id(id)
{
    auto column = new Column(this);

    InterfaceWidget* textEditorContainer = column;

    if (g_config.nodeEditor)
    {
        auto tabs = new TabView(column, { localize("logic-editor-node"), localize("logic-editor-code") });

        nodeEditor = new NodeEditor(
            tabs->tab(localize("logic-editor-node")),
            bind(&EntityLogicEditor::nodeFormatSource, this),
            bind(&EntityLogicEditor::onNodeFormatSave, this, placeholders::_1)
        );
        nodeEditor->set(nodeFormatSource());

        textEditorContainer = tabs->tab(localize("logic-editor-code"));
    }

    codeEditor = new CodeEditor(
        textEditorContainer,
        bind(&EntityLogicEditor::textSource, this),
        bind(&EntityLogicEditor::onTextSave, this, placeholders::_1)
    );
    codeEditor->set(textSource());

    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    lastNodeEditorAssignment = entity->fields();
    lastCodeEditorAssignment = entity->fields();
}

void EntityLogicEditor::step()
{
    InterfaceWidget::step();

    Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    if (nodeEditor && entity->fields() != lastNodeEditorAssignment)
    {
        nodeEditor->notifyReload();
        lastNodeEditorAssignment = entity->fields();
    }

    if (entity->fields() != lastCodeEditorAssignment)
    {
        codeEditor->notifyReload();
        lastCodeEditorAssignment = entity->fields();
    }
}

ScriptNodeFormat EntityLogicEditor::nodeFormatSource()
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return ScriptNodeFormat();
    }

    return ScriptNodeFormat(context()->controller()->game().context(), entity->fieldsToString());
}

void EntityLogicEditor::onNodeFormatSave(const ScriptNodeFormat& format)
{
    context()->controller()->edit([this, format]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        FieldDefinitionClosure closure(
            context()->controller()->game().context(),
            [](const string& name, const Value& value) -> void {}
        );

        optional<vector<Value>> result = runSafely(
            context()->controller()->game().context(),
            format.code(),
            "setting entity fields",
            this->context()->controller()
        );

        if (!result)
        {
            return;
        }

        entity->fieldsFromString(context()->controller()->game().context(), format.code());

        lastNodeEditorAssignment = entity->fields();
    });
}

string EntityLogicEditor::textSource()
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return "";
    }

    return entity->fieldsToString();
}

void EntityLogicEditor::onTextSave(const string& text)
{
    context()->controller()->edit([this, text]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        FieldDefinitionClosure closure(
            context()->controller()->game().context(),
            [](const string& name, const Value& value) -> void {}
        );

        optional<vector<Value>> result = runSafely(
            context()->controller()->game().context(),
            text,
            "setting entity fields",
            this->context()->controller()
        );

        if (!result)
        {
            return;
        }

        entity->fieldsFromString(context()->controller()->game().context(), text);

        lastCodeEditorAssignment = entity->fields();
    });
}
