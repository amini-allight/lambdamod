/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "euler_edit.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

EulerEdit::EulerEdit(
    InterfaceWidget* parent,
    const function<quaternion()>& source,
    const function<void(const quaternion&)>& onSet
)
    : Vec3Edit(
        parent,
        [source]() -> vec3 { return localizeAngle(source().euler()); },
        [onSet](const vec3& value) -> void { onSet(quaternion(delocalizeAngle(value))); }
    )
{

}

void EulerEdit::set(const quaternion& value)
{
    Vec3Edit::set(localizeAngle(value.euler()));
}

quaternion EulerEdit::value() const
{
    return quaternion(delocalizeAngle(Vec3Edit::value()));
}
