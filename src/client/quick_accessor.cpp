/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "quick_accessor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"
#include "fuzzy_find.hpp"
#include "row.hpp"
#include "column.hpp"
#include "label.hpp"
#include "oracles.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(504, 376) * uiScale();
}

QuickAccessorOption::QuickAccessorOption(
    QuickAccessor* parent,
    ControlContext* ctx,
    const string& name,
    const string& search
)
    : parent(parent)
    , ctx(ctx)
    , _name(name)
    , search(search)
{

}

QuickAccessorOption::~QuickAccessorOption()
{
    // Do nothing
}

const string& QuickAccessorOption::name() const
{
    return _name;
}

bool QuickAccessorOption::compare(const QuickAccessorOption* rhs) const
{
    return fuzzyFindCompare(search, rhs->search, parent->buffer());
}

QuickAccessorOptionWindow::QuickAccessorOptionWindow(
    QuickAccessor* parent,
    ControlContext* ctx,
    const string& name,
    const function<void(const Point&)>& callback
)
    : QuickAccessorOption(parent, ctx, name, toLowercase(name))
    , callback(callback)
{

}

QuickAccessorOptionWindow::QuickAccessorOptionWindow(
    QuickAccessor* parent,
    ControlContext* ctx,
    const string& type,
    const string& name,
    const function<void(const Point&)>& callback
)
    : QuickAccessorOptionWindow(parent, ctx, name, callback)
{
    _type = type;
}

string QuickAccessorOptionWindow::type() const
{
    return _type ? *_type : localize("quick-accessor-window");
}

void QuickAccessorOptionWindow::pick(const Point& point)
{
    callback(point);
}

QuickAccessorOptionAction::QuickAccessorOptionAction(
    QuickAccessor* parent,
    ControlContext* ctx,
    const string& name,
    const function<void()>& callback
)
    : QuickAccessorOption(parent, ctx, name, toLowercase(name))
    , callback(callback)
{

}

string QuickAccessorOptionAction::type() const
{
    return localize("quick-accessor-action");
}

void QuickAccessorOptionAction::pick(const Point& point)
{
    callback();
}

QuickAccessorOptionOracle::QuickAccessorOptionOracle(QuickAccessor* parent, ControlContext* ctx, const string& name)
    : QuickAccessorOption(parent, ctx, name, toLowercase(name))
{

}

string QuickAccessorOptionOracle::type() const
{
    return localize("quick-accessor-oracle");
}

void QuickAccessorOptionOracle::pick(const Point& point)
{
    ctx->interface()->oracleManager()->open(point, name);
}

QuickAccessorOptionDocument::QuickAccessorOptionDocument(
    QuickAccessor* parent,
    ControlContext* ctx,
    const string& name
)
    : QuickAccessorOption(parent, ctx, name, toLowercase(name))
{

}

string QuickAccessorOptionDocument::type() const
{
    return localize("quick-accessor-document");
}

void QuickAccessorOptionDocument::pick(const Point& point)
{
    ctx->interface()->documentManager()->open(point, name());
}

QuickAccessorOptionEntity::QuickAccessorOptionEntity(
    QuickAccessor* parent,
    ControlContext* ctx,
    const string& name,
    EntityID id
)
    : QuickAccessorOption(parent, ctx, name, toLowercase(name))
    , id(id)
{

}

string QuickAccessorOptionEntity::type() const
{
    return localize("quick-accessor-entity");
}

void QuickAccessorOptionEntity::pick(const Point& point)
{
    ctx->viewerMode()->entityMode()->openEditor(id);
}

QuickAccessorLineEdit::QuickAccessorLineEdit(
    InterfaceWidget* parent,
    const function<string()>& source,
    const function<void(const string&)>& onReturn,
    bool codeMode
)
    : LineEdit(parent, source, onReturn, codeMode)
{

}

bool QuickAccessorLineEdit::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    return false; // Do nothing on purpose to prevent defocus, focus of this element is slaved to focus of the window
}

QuickAccessorListView::QuickAccessorListView(
    InterfaceWidget* parent,
    const function<void(size_t)>& onSelect,
    const optional<Color>& color
)
    : ListView(parent, onSelect, color)
{
    setScrollbarEnabled(false);
}

bool QuickAccessorListView::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    return false; // Do nothing on purpose to prevent defocus, focus of this element is slaved to focus of the window
}

QuickAccessor::QuickAccessor(InterfaceView* view)
    : InterfaceWindow(view, localize("quick-accessor-quick-accessor"))
    , shouldClose(false)
{
    START_WIDGET_SCOPE("quick-accessor")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-quick-accessor", dismiss)
    END_SCOPE

    auto column = new Column(this);

    lineEdit = new QuickAccessorLineEdit(
        column,
        nullptr,
        bind(&QuickAccessor::onReturn, this, placeholders::_1)
    );
    lineEdit->setPlaceholder(localize("quick-accessor-type-to-search"));

    listView = new QuickAccessorListView(
        column,
        bind(&QuickAccessor::onSelect, this, placeholders::_1)
    );

    resize(windowSize());
}

QuickAccessor::~QuickAccessor()
{
    clearOptions();
}

const string& QuickAccessor::buffer() const
{
    return lineEdit->content();
}

void QuickAccessor::open(const Point& point)
{
    lineEdit->clear();
    lastSearch = "";

    clearOptions();

    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-console"),
        [this](const Point& point) -> void {
            context()->interface()->console()->open(point);
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-help"),
        [this](const Point& point) -> void {
            context()->interface()->help()->open(point);
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-game-settings"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-game"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-world-settings"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-world"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-users"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-users"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-teams"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-teams"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-worlds"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-worlds"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-entities"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-entities"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-checkpoints"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-checkpoints"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-script-context"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-script-context"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-editing-settings"),
        [this](const Point& point) -> void {
            context()->interface()->settings()->open(point, localize("settings-editing"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-video-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-video"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-audio-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-audio"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-input-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-input"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-interface-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-interface"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-script-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-script"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-vr-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-vr"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-bindings-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-bindings"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-theme-settings"),
        [this](const Point& point) -> void {
            context()->interface()->systemSettings()->open(point, localize("system-settings-theme"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-tool-status"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-status"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-signal"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-signal"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-titlecard"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-titlecard"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-timeout"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-timeout"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-unary"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-unary"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-binary"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-binary"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-options"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-options"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-choose"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-choose"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-assign"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-assign"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-point-buy"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-point-buy"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-point-assign"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-point-assign"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-vote"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-vote"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-choose-major"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-choose-major"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-knowledge"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-knowledge"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-text"),
        localize("quick-accessor-text-clear"),
        [this](const Point& point) -> void {
            context()->interface()->newTextTool()->open(point, localize("new-text-tool-clear"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-user-controls"),
        localize("quick-accessor-user-controls-status"),
        [this](const Point& point) -> void {
            context()->interface()->userControlsTool()->open(point, localize("user-controls-tool-status"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-user-controls"),
        localize("quick-accessor-tether"),
        [this](const Point& point) -> void {
            context()->interface()->userControlsTool()->open(point, localize("user-controls-tool-tether"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-user-controls"),
        localize("quick-accessor-confine"),
        [this](const Point& point) -> void {
            context()->interface()->userControlsTool()->open(point, localize("user-controls-tool-confine"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-user-controls"),
        localize("quick-accessor-goto"),
        [this](const Point& point) -> void {
            context()->interface()->userControlsTool()->open(point, localize("user-controls-tool-goto"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-user-controls"),
        localize("quick-accessor-fade"),
        [this](const Point& point) -> void {
            context()->interface()->userControlsTool()->open(point, localize("user-controls-tool-fade"));
        }
    ));
    options.push_back(new QuickAccessorOptionWindow(
        this,
        context(),
        localize("quick-accessor-user-controls"),
        localize("quick-accessor-wait"),
        [this](const Point& point) -> void {
            context()->interface()->userControlsTool()->open(point, localize("user-controls-tool-wait"));
        }
    ));

    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-origin-to-3d-cursor"),
        [this]() -> void {
            if (context()->viewerMode()->mode() == Viewer_Sub_Mode_Entity)
            {
                context()->viewerMode()->entityMode()->originTo3DCursor();
            }
        }
    ));

    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-origin-to-body"),
        [this]() -> void {
            if (context()->viewerMode()->mode() == Viewer_Sub_Mode_Entity)
            {
                context()->viewerMode()->entityMode()->originToBody();
            }
        }
    ));

    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-body-to-origin"),
        [this]() -> void {
            if (context()->viewerMode()->mode() == Viewer_Sub_Mode_Entity)
            {
                context()->viewerMode()->entityMode()->bodyToOrigin();
            }
        }
    ));

    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-ping"),
        [this]() -> void { context()->attachedMode()->ping(); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-brake"),
        [this]() -> void { context()->attachedMode()->brake(); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-unbrake"),
        [this]() -> void { context()->unbrake(); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-lock-attachments"),
        [this]() -> void { context()->lockAttachments(); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-unlock-attachments"),
        [this]() -> void { context()->unlockAttachments(); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-magnitude-3"),
        [this]() -> void { context()->setMagnitude(3); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-magnitude-2"),
        [this]() -> void { context()->setMagnitude(2); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-magnitude-1"),
        [this]() -> void { context()->setMagnitude(1); }
    ));
    options.push_back(new QuickAccessorOptionAction(
        this,
        context(),
        localize("quick-accessor-magnitude-0"),
        [this]() -> void { context()->setMagnitude(0); }
    ));

    for (const Oracle& oracle : oracles)
    {
        options.push_back(new QuickAccessorOptionOracle(this, context(), oracle.name));
    }

    for (const auto& [ name, text ] : context()->controller()->documents())
    {
        options.push_back(new QuickAccessorOptionDocument(this, context(), name));
    }

    context()->controller()->selfWorld()->traverse([&](const Entity* entity) -> void {
        options.push_back(new QuickAccessorOptionEntity(this, context(), entity->name(), entity->id()));
    });

    sortedOptions = sort(options);

    updateListView();

    InterfaceWindow::open(
        point,
        windowSize()
    );

    shouldClose = false;
}

void QuickAccessor::close()
{
    InterfaceWindow::close();

    clearOptions();

    updateListView();

    shouldClose = false;
}

void QuickAccessor::step()
{
    InterfaceWindow::step();

    string search = buffer();

    if (search != lastSearch)
    {
        lastSearch = search;
        sortedOptions = sort(options);
        updateListView();
    }

    if (shouldClose)
    {
        close();
    }
}

void QuickAccessor::focus()
{
    InterfaceWidget::focus();

    lineEdit->focus();
    listView->unfocus();
}

void QuickAccessor::unfocus()
{
    InterfaceWidget::unfocus();

    lineEdit->unfocus();

    shouldClose = true;
}

void QuickAccessor::clearFocus()
{
    InterfaceWidget::clearFocus();

    shouldClose = true;
}

void QuickAccessor::onReturn(const string& text)
{
    sortedOptions.front()->pick(pointer);
    close();

    lineEdit->set("");
}

void QuickAccessor::onSelect(size_t index)
{
    sortedOptions.at(index)->pick(pointer);
    close();
}

vector<QuickAccessorOption*> QuickAccessor::sort(vector<QuickAccessorOption*> options) const
{
    std::sort(
        options.begin(),
        options.end(),
        [](const QuickAccessorOption* a, const QuickAccessorOption* b) -> bool
        {
            return a->compare(b);
        }
    );

    return options;
}

void QuickAccessor::clearOptions()
{
    for (QuickAccessorOption* option : options)
    {
        delete option;
    }

    options.clear();
    sortedOptions.clear();
}

void QuickAccessor::updateListView()
{
    listView->clearChildren();

    size_t i = 0;
    for (const QuickAccessorOption* option : sortedOptions)
    {
        bool first = i == 0;

        auto row = new Row(listView, first ? theme.accentColor : optional<Color>());

        TextStyleOverride typeStyle;
        typeStyle.weight = Text_Bold;
        typeStyle.alignment = Text_Right;
        typeStyle.color = first ? &theme.activeTextColor : &theme.textColor;

        new Label(
            row,
            option->type() + ":",
            typeStyle
        );

        TextStyleOverride valueStyle;
        valueStyle.color = first ? &theme.activeTextColor : &theme.textColor;

        new Label(
            row,
            option->name(),
            valueStyle
        );

        i++;
    }

    listView->update();
}

void QuickAccessor::dismiss(const Input& input)
{
    close();
}
