/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "render_types.hpp"
#include "text_style.hpp"
#include "symbols.hpp"

class Controller;

enum DrawSupportType : u8
{
    Draw_Support_Solid,
    Draw_Support_Rounded_Border,
    Draw_Support_Image,
    Draw_Support_Colored_Image,
    Draw_Support_Symbol,
    Draw_Support_Text,
    Draw_Support_Gradient_1D,
    Draw_Support_Gradient_2D,
    Draw_Support_Curve,
    Draw_Support_Rounded_Solid,
    Draw_Support_Line,
    Draw_Support_Dashed_Line
};

struct Image;

struct DrawSupportKey
{
    DrawSupportKey();

    Size screenSize;
    DrawSupportType type;
    Point pointA;
    Point pointB;
    Point pointC;
    Point pointD;
    Size size;
    Color colorA;
    Color colorB;
    f64 alpha;
    f64 rotation;
    i32 width;
    i32 radius;
    bool flag;
    Image* image;
    SymbolType symbol;
    string text;
    TextStyle style;

    bool operator==(const DrawSupportKey& rhs) const;
    bool operator!=(const DrawSupportKey& rhs) const;
};

DrawSupportKey solidSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Color color
);
DrawSupportKey imageSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Image* rawImage,
    f64 alpha,
    f64 rotation
);
DrawSupportKey coloredImageSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Image* rawImage,
    Color color,
    f64 rotation
);
DrawSupportKey textSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    const string& text,
    TextStyle style
);
DrawSupportKey gradient1DSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Color start,
    Color end
);
DrawSupportKey gradient2DSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    Color start,
    Color end
);
DrawSupportKey curveSupportKey(
    const Size& screenSize,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    Color color,
    f64 width
);
DrawSupportKey roundedSolidSupportKey(
    const Size& screenSize,
    const Point& point,
    const Size& size,
    f64 radius,
    Color color
);
DrawSupportKey lineSupportKey(
    const Size& screenSize,
    const Point& start,
    const Point& end,
    Color color
);
DrawSupportKey dashedLineSupportKey(
    const Size& screenSize,
    const Point& start,
    const Point& end,
    Color color
);

class DrawTexture
{
public:
    DrawTexture();
    DrawTexture(const DrawTexture& rhs) = delete;
    DrawTexture(DrawTexture&& rhs) = delete;
    ~DrawTexture();

    DrawTexture& operator=(const DrawTexture& rhs) = delete;
    DrawTexture& operator=(DrawTexture&& rhs) = delete;

    void mark();
    void clearMark();
    bool marked() const;

    bool matches(Image* image) const;
    bool matches(const string& text, const TextStyle& style) const;

    struct {
        bool isText;
        Image* image;
        string text;
        TextStyle style = TextStyle();
    } key;
    VmaImage image;
    VkImageView imageView;
    Size size;

private:
    bool _marked;
};

class DrawSupport
{
public:
    DrawSupport();
    DrawSupport(const DrawSupport& rhs) = delete;
    DrawSupport(DrawSupport&& rhs) = delete;
    ~DrawSupport();

    DrawSupport& operator=(const DrawSupport& rhs) = delete;
    DrawSupport& operator=(DrawSupport&& rhs) = delete;

    void mark();
    void clearMark();
    bool marked() const;

    DrawSupportKey key;
    VkDescriptorSet descriptorSet;
    VmaBuffer buffer;
    DrawTexture* texture;

private:
    bool _marked;
};

void interfaceDrawInit(Controller* controller);
void interfaceDrawPreStep();
void interfaceDrawPostStep();
void interfaceDrawQuit();

void drawSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Color color
);
void drawBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    i32 width,
    Color color
);
void drawRoundedBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    i32 width,
    i32 radius,
    Color color
);
void drawImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Icon icon,
    f64 alpha = 1,
    f64 rotation = 0
);
void drawColoredImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Icon icon,
    Color color,
    f64 rotation = 0
);
void drawSymbol(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    SymbolType symbol,
    Color color,
    f64 rotation = 0
);
void drawText(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    const string& text,
    const TextStyle& style
);
void drawGradient1D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Color start,
    Color end
);
void drawGradient2D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    Color start,
    Color end
);
void drawCurve(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    i32 width,
    Color color
);
void drawRoundedSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& point,
    const Size& size,
    i32 radius,
    Color color
);

void drawSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Color color
);
void drawBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    i32 width,
    Color color
);
void drawRoundedBorder(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    i32 width,
    i32 radius,
    Color color
);
void drawImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Icon icon,
    f64 alpha = 1,
    f64 rotation = 0
);
void drawColoredImage(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Icon icon,
    Color color,
    f64 rotation = 0
);
void drawSymbol(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    SymbolType symbol,
    Color color,
    f64 rotation = 0
);
void drawText(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const string& text,
    const TextStyle& style
);
void drawGradient1D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Color start,
    Color end
);
void drawGradient2D(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    Color start,
    Color end
);
void drawCurve(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& p0,
    const Point& p1,
    const Point& p2,
    const Point& p3,
    i32 width,
    Color color
);
void drawRoundedSolid(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    i32 radius,
    Color color
);

void drawLine(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& start,
    const Point& end,
    Color color
);
void drawDashedLine(
    const DrawContext& ctx,
    const InterfaceWidget* widget,
    const Point& start,
    const Point& end,
    Color color
);

void drawPointer(const DrawContext& ctx, const InterfaceWidget* widget, const optional<Point>& pointer);
