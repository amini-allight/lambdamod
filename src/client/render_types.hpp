/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "log.hpp"
#include "id_types.hpp"
#include "render_vertex_types.hpp"

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

struct VmaImage
{
    VmaImage();
    VmaImage(VkImage image, VmaAllocation allocation);

    VkImage image;
    VmaAllocation allocation;
};

struct VmaBuffer
{
    VmaBuffer();
    VmaBuffer(VkBuffer buffer, VmaAllocation allocation);

    VkBuffer buffer;
    VmaAllocation allocation;
};

string vulkanError(VkResult result);

template<typename T>
class VmaMapping
{
public:
    VmaMapping(VmaAllocator allocator, VmaAllocation allocation)
        : allocator(allocator)
        , allocation(allocation)
    {
        VkResult result;

        result = vmaMapMemory(allocator, allocation, reinterpret_cast<void**>(&data));

        if (result != VK_SUCCESS)
        {
            fatal("Failed to map memory: " + vulkanError(result));
        }
    }

    VmaMapping(VmaAllocator allocator, VmaImage image)
        : VmaMapping(allocator, image.allocation)
    {

    }

    VmaMapping(VmaAllocator allocator, VmaBuffer buffer)
        : VmaMapping(allocator, buffer.allocation)
    {

    }

    VmaMapping(const VmaMapping& rhs) = delete;
    VmaMapping(VmaMapping&& rhs) = delete;

    ~VmaMapping()
    {
        vmaUnmapMemory(allocator, allocation);
    }

    VmaMapping& operator=(const VmaMapping& rhs) = delete;
    VmaMapping& operator=(VmaMapping&& rhs) = delete;

    T* data;

private:
    VmaAllocator allocator;
    VmaAllocation allocation;
};
