/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_atmosphere_types.hpp"
#include "render_atmosphere_component_effect.hpp"

class RenderAtmosphereComponent
{
public:
    RenderAtmosphereComponent(
        const RenderInterface* render,
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera,
        VmaBuffer lightningVertices
    );
    RenderAtmosphereComponent(const RenderAtmosphereComponent& rhs) = delete;
    RenderAtmosphereComponent(RenderAtmosphereComponent&& rhs) = delete;
    ~RenderAtmosphereComponent();

    RenderAtmosphereComponent& operator=(const RenderAtmosphereComponent& rhs) = delete;
    RenderAtmosphereComponent& operator=(RenderAtmosphereComponent&& rhs) = delete;

    void update(
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene,
        const AtmosphereParameters& atmosphere,
        vec3 cameraPosition[eyeCount],
        const vec3& gravity,
        const vec3& flowVelocity,
        f64 density
    );

    VmaBuffer uniform;
    VmaMapping<AtmosphereLightningGPU>* uniformMapping;
    VmaBuffer particles;
    VmaMapping<AtmosphereLightningParticleGPU>* particlesMapping;
    VkDescriptorSet descriptorSet;
    vector<RenderAtmosphereComponentEffect*> effects;

private:
    const RenderInterface* render;
    VkDescriptorSetLayout descriptorSetLayout;
    VmaBuffer camera;

    void refreshEffects(
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene,
        size_t count
    );

    VmaBuffer createUniformBuffer(size_t size) const;
    VmaBuffer createStorageBuffer(size_t size) const;
    VkDescriptorSet createDescriptorSet(VmaBuffer camera, VmaBuffer vertices) const;

    void createEffects(
        VkImageView environmentImageView,
        RenderOcclusionSceneReference occlusionScene,
        size_t count
    );
    void destroyEffects();
};
