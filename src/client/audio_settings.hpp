/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class AudioSettings : public InterfaceWidget, public TabContent
{
public:
    AudioSettings(InterfaceWidget* parent);

private:
    bool outputMuteSource();
    void onOutputMute(bool state);

    f64 outputVolumeSource();
    void onOutputVolume(f64 volume);

    bool inputMuteSource();
    void onInputMute(bool state);

    f64 inputVolumeSource();
    void onInputVolume(f64 volume);

    vector<string> soundSpatialModeOptionsSource();
    vector<string> soundSpatialModeOptionTooltipsSource();
    size_t soundSpatialModeSource();
    void onSoundSpatialMode(size_t index);

    bool soundMonoSource();
    void onSoundMono(bool state);

    bool uiSoundsSource();
    void onUISounds(bool state);

    SizeProperties sizeProperties() const override;
};
