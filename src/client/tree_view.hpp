/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "layout.hpp"

class TreeView : public Layout
{
public:
    TreeView(
        InterfaceWidget* parent,
        const optional<Color>& color = {}
    );

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void draw(const DrawContext& ctx) const override;

private:
    optional<Color> color;
    size_t panIndex;
    size_t scrollIndex;

    size_t maxPanIndex() const;
    size_t maxScrollIndex() const;

    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void panLeft(const Input& input);
    void panRight(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);

    SizeProperties sizeProperties() const override;
};
