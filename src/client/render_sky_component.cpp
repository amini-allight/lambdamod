/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_sky_component.hpp"
#include "render_tools.hpp"
#include "render_descriptor_set_write_components.hpp"

RenderSkyComponent::RenderSkyComponent(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera
)
    : ctx(ctx)
    , descriptorSetLayout(descriptorSetLayout)
    , camera(camera)
{
    uniform = createUniformBuffer(sizeof(SkyBaseGPU));
    uniformMapping = new VmaMapping<f32>(ctx->allocator, uniform);
    descriptorSet = createDescriptorSet(camera);
    envMapDescriptorSet = createDescriptorSet(ctx->environmentMappingCameraUniformBuffer);
    // Layers are created upon first prepare
}

RenderSkyComponent::~RenderSkyComponent()
{
    destroyLayers();
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, envMapDescriptorSet);
    destroyDescriptorSet(ctx->device, ctx->descriptorPool, descriptorSet);
    delete uniformMapping;
    destroyBuffer(ctx->allocator, uniform);
}

void RenderSkyComponent::refreshLayers(size_t count)
{
    destroyLayers();
    createLayers(count);
}

VmaBuffer RenderSkyComponent::createUniformBuffer(size_t size) const
{
    return createBuffer(
        ctx->allocator,
        size,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VkDescriptorSet RenderSkyComponent::createDescriptorSet(VmaBuffer camera) const
{
    VkDescriptorSet descriptorSet = ::createDescriptorSet(
        ctx->device,
        ctx->descriptorPool,
        descriptorSetLayout
    );

    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, uniform.buffer, &parametersWriteBuffer, &parametersWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        parametersWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    return descriptorSet;
}

void RenderSkyComponent::createLayers(size_t count)
{
    for (size_t i = 0; i < count; i++)
    {
        layers.push_back(new RenderSkyComponentLayer(
            ctx,
            descriptorSetLayout,
            camera
        ));
    }
}

void RenderSkyComponent::destroyLayers()
{
    for (RenderSkyComponentLayer* layer : layers)
    {
        delete layer;
    }

    layers.clear();
}
