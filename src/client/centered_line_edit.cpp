/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "centered_line_edit.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

CenteredLineEdit::CenteredLineEdit(
    InterfaceWidget* parent,
    const function<string()>& source,
    const function<void(const string&)>& onReturn
)
    : LineEdit(parent, source, onReturn)
{

}

void CenteredLineEdit::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.indentColor
    );

    if (selected)
    {
        i32 start = indexToScreen(selectionStart()).x;
        i32 end = indexToScreen(selectionEnd()).x;

        start = clamp(start, 0, size().w);
        end = clamp(end, 0, size().w);

        drawSolid(
            ctx,
            this,
            Point(start, (size().h - charHeight()) / 2),
            Size(end - start, charHeight()),
            theme.accentColor
        );
    }

    TextStyle style;
    style.alignment = Text_Center;

    if (text.empty())
    {
        style.color = theme.inactiveTextColor;
    }

    drawText(
        ctx,
        this,
        text.empty() ? placeholder : text,
        style
    );

    if (focused())
    {
        drawSolid(
            ctx,
            this,
            indexToScreen(_cursorIndex),
            Size(2, charHeight()),
            theme.textColor
        );
    }
}

i32 CenteredLineEdit::screenToIndex(const Point& point) const
{
    f64 midpoint = text.size() / 2.0;

    f64 relative = (point.x - (size().w / 2)) / static_cast<f64>(charWidth());

    return midpoint + relative;
}

Point CenteredLineEdit::indexToScreen(i32 index) const
{
    f64 midpoint = text.size() / 2.0;

    f64 x = index - midpoint;

    return Point(
        (size().w / 2) + static_cast<i32>(x * charWidth()),
        (size().h - charHeight()) / 2
    );
}

SizeProperties CenteredLineEdit::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::lineEditHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
