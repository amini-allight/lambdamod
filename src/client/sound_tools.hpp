/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "sound_chunk.hpp"

i16 convertSampleToDiscrete(f32 continuous);
vector<i16> convertSamplesToDiscrete(const vector<f32>& input);
f32 convertSampleToContinuous(i16 discrete);
vector<f32> convertSamplesToContinuous(const vector<i16>& input);

vector<f32> reverseSamples(vector<f32> samples);
tuple<SoundOverlappedChunk, i64> readSampleSource(const vector<f32>& samples, i64 readHead, f64 multiplier, bool looping);

void writeWAV(const string& path, u32 frequency, u32 channels, const i16* samples, u32 samplesPerChannel);

template<typename T>
tuple<vector<T>, vector<T>> splitStereoSamples(const vector<T>& c)
{
    vector<T> l(c.size() / 2);
    vector<T> r(c.size() / 2);

    for (size_t i = 0; i < c.size(); i += 2)
    {
        l[i / 2] = c[i + 0];
        r[i / 2] = c[i + 1];
    }

    return { l, r };
}

template<typename T>
vector<T> joinStereoSamples(const vector<T>& l, const vector<T>& r)
{
    vector<T> c(l.size() * 2);

    for (size_t i = 0; i < l.size(); i++)
    {
        c[i * 2 + 0] = l[i];
        c[i * 2 + 1] = r[i];
    }

    return c;
}
