/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_select_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_window.hpp"
#include "interface_scale.hpp"
#include "column.hpp"
#include "option_select.hpp"
#include "control_context.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(
        128 * uiScale(),
        UIScale::optionSelectHeight()
    );
}

UserSelectDialog::UserSelectDialog(InterfaceView* view, UserID initial, const function<void(UserID)>& onDone)
    : Dialog(view, dialogSize())
    , initial(initial)
    , onDone(onDone)
{
    auto column = new Column(this);

    new OptionSelect(
        column,
        bind(&UserSelectDialog::usersSource, this),
        bind(&UserSelectDialog::source, this),
        bind(&UserSelectDialog::onSelect, this, placeholders::_1)
    );
}

vector<string> UserSelectDialog::usersSource()
{
    vector<string> users = { localize("user-select-dialog-no-user") };

    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        users.push_back(user.name());
    }

    return users;
}

size_t UserSelectDialog::source()
{
    size_t i = 1;
    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        if (id == initial)
        {
            return i;
        }

        i++;
    }

    return 0;
}

void UserSelectDialog::onSelect(size_t index)
{
    UserID id = index == 0 ? UserID() : context()->controller()->game().getUserByName(usersSource().at(index))->id();

    onDone(id);
    destroy();
}
