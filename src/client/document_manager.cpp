/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "document_manager.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "row.hpp"
#include "spacer.hpp"
#include "document_editor.hpp"
#include "confirm_dialog.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(
        UIScale::itemListWidth() + 568 * uiScale(),
        424 * uiScale()
    );
}

DocumentManager::DocumentManager(InterfaceView* view)
    : InterfaceWindow(view, localize("document-manager-document-manager"))
{
    START_WIDGET_SCOPE("document-manager")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-document-manager", dismiss)
    END_SCOPE

    auto row = new Row(this);

    list = new ItemList(
        row,
        bind(&DocumentManager::itemSource, this),
        bind(&DocumentManager::onAdd, this, placeholders::_1),
        bind(&DocumentManager::onRemove, this, placeholders::_1),
        bind(&DocumentManager::onSelect, this, placeholders::_1),
        bind(&DocumentManager::onRename, this, placeholders::_1, placeholders::_2),
        bind(&DocumentManager::onClone, this, placeholders::_1, placeholders::_2)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    stack = new StackView(row);

    resize(windowSize());
}

void DocumentManager::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());
}

void DocumentManager::open(const Point& point, const string& name)
{
    openDocument(name, textSource(name));

    open(point);
}

void DocumentManager::step()
{
    InterfaceWidget::step();

    vector<string> items = itemSource();

    if (
        list->activeIndex() < 0 ||
        list->activeIndex() >= static_cast<i32>(items.size()) ||
        items.at(list->activeIndex()) != stack->activePageName()
    )
    {
        list->clearSelection();
        for (size_t i = 0; i < items.size(); i++)
        {
            if (items.at(i) == stack->activePageName())
            {
                list->select(i);
                break;
            }
        }
    }
}

void DocumentManager::openDocument(const string& name, const string& text)
{
    if (!stack->page(name))
    {
        stack->add<DocumentEditor>(
            name,
            bind(&DocumentManager::textSource, this, name),
            bind(&DocumentManager::onSave, this, name, placeholders::_1)
        );
        dynamic_cast<DocumentEditor*>(stack->page(name))->set(text);
    }

    stack->open(name);
}

void DocumentManager::closeDocument(const string& name)
{
    if (stack->page(name))
    {
        stack->remove(name);
    }

    if (stack->activePageName() == name)
    {
        stack->close();
    }
}

vector<string> DocumentManager::itemSource() const
{
    vector<string> keys;
    keys.reserve(context()->controller()->documents().size());

    for (const auto& [ key, value ] : context()->controller()->documents())
    {
        keys.push_back(key);
    }

    return keys;
}

void DocumentManager::onAdd(const string& name)
{
    DocumentEvent event;
    event.type = Document_Event_Add;
    event.name = name;

    context()->controller()->send(event);

    openDocument(name, "");
}

void DocumentManager::onRemove(const string& name)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("document-manager-are-you-sure-you-want-to-delete-document", name),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            DocumentEvent event;
            event.type = Document_Event_Remove;
            event.name = name;

            context()->controller()->send(event);

            closeDocument(name);
        }
    );
}

void DocumentManager::onSelect(const string& name)
{
    openDocument(name, "");
}

void DocumentManager::onRename(const string& oldName, const string& newName)
{
    {
        DocumentEvent event;
        event.type = Document_Event_Add;
        event.name = toValidName(newName);

        context()->controller()->send(event);
    }

    {
        DocumentEvent event;
        event.type = Document_Event_Update;
        event.name = toValidName(newName);
        event.text = context()->controller()->documents().at(oldName);

        context()->controller()->send(event);
    }

    {
        DocumentEvent event;
        event.type = Document_Event_Remove;
        event.name = oldName;

        context()->controller()->send(event);
    }

    closeDocument(oldName);
    openDocument(newName, context()->controller()->documents().at(oldName));
}

void DocumentManager::onClone(const string& oldName, const string& newName)
{
    {
        DocumentEvent event;
        event.type = Document_Event_Add;
        event.name = toValidName(newName);

        context()->controller()->send(event);
    }

    {
        DocumentEvent event;
        event.type = Document_Event_Update;
        event.name = toValidName(newName);
        event.text = context()->controller()->documents().at(oldName);

        context()->controller()->send(event);
    }

    openDocument(newName, context()->controller()->documents().at(oldName));
}

string DocumentManager::textSource(const string& name) const
{
    auto it = context()->controller()->documents().find(name);

    if (it == context()->controller()->documents().end())
    {
        return "";
    }

    return it->second;
}

void DocumentManager::onSave(const string& name, const string& text)
{
    DocumentEvent event;
    event.type = Document_Event_Update;
    event.name = name;
    event.text = text;

    context()->controller()->send(event);
}

void DocumentManager::dismiss(const Input& input)
{
    hide();
}
