/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "edit_tool.hpp"

class RotateTool final : public EditTool
{
public:
    RotateTool(
        ViewerModeContext* ctx,
        const mat4& localSpace,
        const function<void(size_t, const vec3&, const mat4&, const mat4&)>& setGlobal,
        const function<mat4(size_t)>& getGlobal,
        const function<mat4(size_t)>& getGlobalWithPromotion,
        const function<size_t()>& count,
        bool soft,
        const Point& point
    );
    ~RotateTool() override;

    string name() const override;
    string extent() const override;

    void use(const Point& point) override;
    void update() override;

    void cancel() override;
    void end() override;

    void togglePivotMode(const Point& point) override;

    Point pivotPoint() const;
    Point cursorPoint() const;

private:
    vec3 cameraPosition;
    vec3 defaultAxis;
    f64 onScreenAngle;

    Point onScreenCenter;
    Point onScreenCursor;
    Point onScreenStart;

    optional<vec3> globalAxis() const override;

    f64 angle() const;

    f64 snap(f64 v) const;

    mat4 apply(const vec3& pivot, mat4 transform) const;

    vec3 correctAxisOrientation(const vec3& axis) const;
};
