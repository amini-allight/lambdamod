/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"

class NodeEditor;
class NodeEditorNode;

class NodeEditorLink : public InterfaceWidget
{
public:
    NodeEditorLink(
        InterfaceWidget* parent,
        const NodeEditorNode* src,
        size_t srcSlotIndex,
        const NodeEditorNode* dst,
        size_t dstSlotIndex
    );

    ScriptNodeID srcID() const;
    size_t srcSlotIndex() const;
    ScriptNodeID dstID() const;
    size_t dstSlotIndex() const;
    bool curveContains(const Point& pointer) const;

    void step() override;
    void draw(const DrawContext& ctx) const override;

private:
    const NodeEditorNode* src;
    size_t _srcSlotIndex;
    const NodeEditorNode* dst;
    size_t _dstSlotIndex;

    Point start() const;
    Point end() const;

    NodeEditor* editor() const;

    SizeProperties sizeProperties() const override;
};
