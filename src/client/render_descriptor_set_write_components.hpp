/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"

void uniformBufferDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkBuffer buffer,
    VkDescriptorBufferInfo* info,
    VkWriteDescriptorSet* write
);

void storageBufferDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkBuffer buffer,
    VkDescriptorBufferInfo* info,
    VkWriteDescriptorSet* write
);

void combinedSamplerDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkImageView imageView,
    VkSampler sampler,
    VkDescriptorImageInfo* info,
    VkWriteDescriptorSet* write
);

void combinedSamplerDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    const vector<VkImageView>& imageViews,
    VkSampler sampler,
    vector<VkDescriptorImageInfo>* info,
    VkWriteDescriptorSet* write
);

void sampledImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkImageView imageView,
    VkDescriptorImageInfo* info,
    VkWriteDescriptorSet* write
);

void sampledImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    const vector<VkImageView>& imageViews,
    vector<VkDescriptorImageInfo>* info,
    VkWriteDescriptorSet* write
);

void storageImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkImageView imageView,
    VkDescriptorImageInfo* info,
    VkWriteDescriptorSet* write
);

void storageImageDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    const vector<VkImageView>& imageViews,
    vector<VkDescriptorImageInfo>* info,
    VkWriteDescriptorSet* write
);

void accelerationStructureDescriptorSetWrite(
    VkDescriptorSet descriptorSet,
    u32 index,
    VkAccelerationStructureKHR* accelerationStructure,
    VkWriteDescriptorSetAccelerationStructureKHR* info,
    VkWriteDescriptorSet* write
);
