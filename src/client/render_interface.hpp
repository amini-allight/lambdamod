/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_light.hpp"

#include <vulkan/vulkan.h>
#include <vk_mem_alloc.h>

class Entity;
class RenderEntity;
class RenderContext;

class RenderInterface
{
public:
    RenderInterface();
    RenderInterface(const RenderInterface& rhs) = delete;
    RenderInterface(RenderInterface&& rhs) = delete;
    virtual ~RenderInterface() = 0;

    RenderInterface& operator=(const RenderInterface& rhs) = delete;
    RenderInterface& operator=(RenderInterface&& rhs) = delete;

    virtual void waitIdle() const = 0;

    virtual void step() = 0;

    virtual tuple<u32, u32> reinit(u32 width, u32 height) = 0;

    virtual void setCameraSpace(const quaternion& space) = 0;

    virtual void add(EntityID parentID, const Entity* entity) = 0;
    virtual void remove(EntityID parentID, EntityID id) = 0;
    virtual void update(EntityID parentID, const Entity* entity) = 0;

    virtual void removeDecoration(RenderDecorationID id) = 0;
    virtual void setDecorationShown(RenderDecorationID id, bool state) = 0;

    virtual void removePostProcessEffect(RenderPostProcessEffectID id) = 0;
    virtual void setPostProcessEffectActive(RenderPostProcessEffectID id, bool state) = 0;
    virtual void setPostProcessEffectOrder(const vector<RenderPostProcessEffectID>& order) = 0;

    virtual RenderContext* context() const = 0;
    virtual u32 imageCount() const = 0;
    virtual u32 currentImage() const = 0;
    virtual VkRenderPass panelRenderPass() const = 0;
    virtual VkFramebuffer textTitlecardFramebuffer() const = 0;
    virtual VkFramebuffer textFramebuffer() const = 0;

    virtual f32 timer() const = 0;

    virtual bool rayTracingSupported() const = 0;
    virtual bool rayTracingEnabled() const = 0;

    virtual f32 fogDistance() const = 0;

    void addLights(RenderEntity* entity);
    void removeLights(RenderEntity* entity);
    void updateLights(RenderEntity* entity);
    vector<RenderLight> getLights(const vec3& position) const;

private:
    deque<RenderLight> lights;
    map<RenderEntity*, tuple<size_t, size_t>> entityLightRanges;
};
