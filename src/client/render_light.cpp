#include "render_light.hpp"
#include "render_constants.hpp"
#include "constants.hpp"

RenderLight::RenderLight(const mat4& entityTransform, const BodyRenderLight& light)
    : entityTransform(entityTransform)
    , transform(light.transform)
    , intensity(light.color.toVec3().dot(vec3(1)))
{
    _uniformData.color = light.color;
    _uniformData.angle = light.angle;
}

void RenderLight::update(const mat4& entityTransform)
{
    this->entityTransform = entityTransform;
}

bool RenderLight::needed(const vec3& position) const
{
    return transform.position().distance(position) < maxDistance();
}

f64 RenderLight::maxDistance() const
{
    return sqrt(intensity / minLightIntensity);
}

LightGPU RenderLight::uniformData(const vec3& cameraPosition) const
{
    LightGPU data = _uniformData;

    mat4 transform = entityTransform * this->transform;
    transform.setPosition(transform.position() - cameraPosition);
    data.transform[0] = transform;

    return data;
}

LightGPU RenderLight::uniformData(vec3 cameraPosition[eyeCount]) const
{
    LightGPU data = _uniformData;

    for (u32 eye = 0; eye < eyeCount; eye++)
    {
        mat4 transform = entityTransform * this->transform;
        transform.setPosition(transform.position() - cameraPosition[eye]);
        data.transform[eye] = transform;
    }

    return data;
}
