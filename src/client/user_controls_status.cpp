/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_controls_status.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "row.hpp"
#include "read_only_item_list.hpp"
#include "editor_entry.hpp"
#include "dynamic_label.hpp"
#include "spacer.hpp"

UserControlsStatus::UserControlsStatus(InterfaceWidget* parent)
    : InterfaceWidget(parent)
    , initialized(false)
{
    auto row = new Row(this);

    new ReadOnlyItemList(
        row,
        bind(&UserControlsStatus::usersSource, this),
        bind(&UserControlsStatus::onSelect, this, placeholders::_1)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    listView = new ListView(
        row,
        [](size_t index) -> void {},
        theme.backgroundColor
    );
}

void UserControlsStatus::open()
{

}

void UserControlsStatus::close()
{

}

vector<string> UserControlsStatus::usersSource()
{
    vector<string> users;

    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        users.push_back(user.name());
    }

    return users;
}

void UserControlsStatus::onSelect(const string& name)
{
    userName = name;

    if (!initialized)
    {
        new EditorEntry(listView, "user-control-status-tether", [this](InterfaceWidget* parent) -> void {
            new DynamicLabel(
                parent,
                bind(&UserControlsStatus::tetherSource, this)
            );
        });

        new EditorEntry(listView, "user-control-status-confinement", [this](InterfaceWidget* parent) -> void {
            new DynamicLabel(
                parent,
                bind(&UserControlsStatus::confinementSource, this)
            );
        });

        new EditorEntry(listView, "user-control-status-fade", [this](InterfaceWidget* parent) -> void {
            new DynamicLabel(
                parent,
                bind(&UserControlsStatus::fadeSource, this)
            );
        });

        new EditorEntry(listView, "user-control-status-wait", [this](InterfaceWidget* parent) -> void {
            new DynamicLabel(
                parent,
                bind(&UserControlsStatus::waitSource, this)
            );
        });

        listView->update();

        initialized = true;
    }
}

string UserControlsStatus::tetherSource()
{
    const User* user = context()->controller()->game().getUserByName(userName);

    Tether tether;

    if (user && context()->controller()->self()->admin())
    {
        tether = user->tether();
    }

    if (!user || !tether.active)
    {
        return localize("user-control-status-disabled");
    }
    else
    {
        string users;

        size_t i = 0;
        for (UserID userID : tether.userIDs)
        {
            const User* user = context()->controller()->game().getUser(userID);

            if (!user)
            {
                continue;
            }

            users += user->name();

            if (i + 1 != tether.userIDs.size())
            {
                users += ", ";
            }

            i++;
        }

        return users + " " + toPrettyString(localizeLength(tether.distance)) + " " + lengthSuffix();
    }
}

string UserControlsStatus::confinementSource()
{
    const User* user = context()->controller()->game().getUserByName(userName);

    Confinement confinement;

    if (user && context()->controller()->self()->admin())
    {
        confinement = user->confinement();
    }

    if (!user || !confinement.active)
    {
        return localize("user-control-status-disabled");
    }
    else
    {
        stringstream ss;

        ss << localizeLength(confinement.position);
        ss << " ";
        ss << toPrettyString(localizeLength(confinement.distance));
        ss << " ";
        ss << lengthSuffix();

        return ss.str();
    }
}

string UserControlsStatus::fadeSource()
{
    const User* user = context()->controller()->game().getUserByName(userName);

    bool fade = false;

    if (user && context()->controller()->self()->admin())
    {
        fade = user->fade();
    }

    if (!user || !fade)
    {
        return localize("user-control-status-disabled");
    }
    else
    {
        return localize("user-control-status-enabled");
    }
}

string UserControlsStatus::waitSource()
{
    const User* user = context()->controller()->game().getUserByName(userName);

    bool wait = false;

    if (user && context()->controller()->self()->admin())
    {
        wait = user->wait();
    }

    if (!user || !wait)
    {
        return localize("user-control-status-disabled");
    }
    else
    {
        return localize("user-control-status-enabled");
    }
}

SizeProperties UserControlsStatus::sizeProperties() const
{
    return sizePropertiesFillAll;
}
