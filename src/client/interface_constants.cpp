/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_constants.hpp"
#include "interface_scale.hpp"
#include "text_style.hpp"
#include "hud.hpp"

static constexpr i32 smallFontSize = 12;
static constexpr i32 mediumFontSize = 18;
static constexpr i32 largeFontSize = 26;
static constexpr i32 vrMenuButtonFontSize = 34;
static constexpr i32 vrKeyboardFontSize = 52;
static constexpr i32 textTitlecardTitleFontSize = 60;
static constexpr i32 textTitlecardSubtitleFontSize = 40;

static constexpr i32 marginSize = 4;

static constexpr i32 headerSize = 24;
static constexpr Size iconButtonSize = { 32, 32 };
static constexpr i32 textButtonHeight = 32;
static constexpr i32 lineEditHeight = 32;
static constexpr Size checkboxSize = { 32, 32 };
static constexpr i32 radioBarHeight = 32;
static constexpr i32 colorPickerHeight = 32;
static constexpr i32 hueBarWidth = 16;
static constexpr Size hueSliderSize = { hueBarWidth + marginSize, 4 };
static constexpr i32 colorPickerGradientHeight = 128;
static constexpr i32 sliderHeight = 32;
static constexpr i32 sliderTrackHeight = 8;
static constexpr i32 sliderThumbSize = 16;
static constexpr i32 colorIndicatorBarHeight = 24;
static constexpr i32 consoleOutputRowHeight = 24;
static constexpr i32 menuWidth = 256;
static constexpr i32 menuRowHeight = 24;
static constexpr Size destinationSelectorSize = { 24, 24 };
static constexpr i32 destinationSelectorRowHeight = 24;
static constexpr i32 destinationSelectorRows = 4;
static constexpr i32 destinationSelectorPopupWidth = 128;
static constexpr i32 consoleInputHeight = 24;
static constexpr Size saveIndicatorSize = { 64, 64 };
static constexpr Size indicatorIconSize = { 32, 32 };
static constexpr i32 optionSelectHeight = 32;
static constexpr i32 optionSelectEntryHeight = 32;
static constexpr i32 optionSelectMaxVisibleEntryCount = 15;
static constexpr i32 itemListWidth = 192;
static constexpr i32 treeViewScrollIncrement = 32;
static constexpr i32 treeViewPanIncrement = 32;
static constexpr i32 treeViewNodeIndent = 32;
static constexpr i32 treeViewNodeHeight = 32;
static constexpr i32 helpColumnWidth = 256;
static constexpr i32 helpRowHeight = 32;
static constexpr i32 dividerWidth = 1;
static constexpr Size tabSize = { 96, 32 };
static constexpr i32 labelHeight = 32;
static constexpr Size spinnerSize = { 64, 64 };
static constexpr i32 scrollbarWidth = 16;
static constexpr i32 sidePanelWidth = 256;
static constexpr i32 scriptNodeLinkConnectorHeight = 16;
static constexpr i32 scriptNodeCapHeight = 32;
static constexpr i32 scriptNodeLinkWidth = marginSize * 2;
static constexpr i32 scriptNodeWidth = 192;
static constexpr i32 scriptNodeDefaultHorizontalSpacing = scriptNodeWidth * 2;
static constexpr i32 scriptNodeDefaultVerticalSpacing = 64;
static constexpr i32 scriptNodeBorderRadius = 8;
static constexpr i32 scriptNodeHeaderHeight = 32;
static constexpr i32 widgetToBottomSpacing = 8;
static constexpr Size controlPointSize = { 16, 16 };
static constexpr Size controlPointTextSize = { 512, 24 };
static constexpr Size entityCardSize = { 192, 80 };
static constexpr Size diagnosticLineSize = { 512, 24 };
static constexpr Size markerSize = { 32, 32 };
static constexpr Size markerLineSize = { 256, 24 };
static constexpr i32 oracleTableColumnWidth = 192;
static constexpr i32 oracleTableRowHeight = 32;
static constexpr i32 oracleListWidth = 192;
static constexpr i32 timelineTopHeaderHeight = 16;
static constexpr i32 timelineLeftHeaderWidth = 48;
static constexpr i32 timelineColumnWidth = 40;
static constexpr i32 timelineRowHeight = 20;
static constexpr i32 timelineEditorButtonSpacing = 16;
static constexpr i32 tooltipHeight = 24;
static constexpr i32 unitIndicatorWidth = 64;
static constexpr i32 textCursorWidth = 2;
static constexpr i32 minBoxSelectSize = 96;

static constexpr i32 panelBorderSize = 4;
static constexpr i32 panelScrollbarWidth = 32;
static constexpr i32 panelScrollbarTrackWidth = panelBorderSize;
static constexpr i32 panelScrollbarThumbWidth = panelScrollbarWidth / 2;
static constexpr Size panelIconButtonSize = { 64, 64 };
static constexpr i32 panelTextButtonHeight = 64;
static constexpr i32 panelOptionSelectHeight = 64;
static constexpr i32 panelOptionSelectEntryHeight = 32;
static constexpr Size panelTabSize = { 192, 64 };
static constexpr Size panelCheckboxSize = { 64, 64 };
static constexpr i32 panelLineEditHeight = 96;
static constexpr i32 panelLineEditLabelHeight = 32;
static constexpr i32 panelLabelHeight = 64;
static constexpr i32 panelBarTimerHeight = 4;
static constexpr i32 panelGamepadMagnitudeToolHeight = 64;
static constexpr i32 panelTextPanelMargin = 64;
static constexpr Size panelVRMenuButtonSize = { 256, 256 };

static constexpr i32 panelTextTitlecardTopPadding = 128;
static constexpr i32 panelTextTitlecardTitleHeight = 60;
static constexpr i32 panelTextTitlecardSubtitleHeight = 40;

static constexpr i32 panelGamepadMenuItemSpacing = 16;
static constexpr i32 panelTextPanelItemSpacing = 32;

static constexpr i32 panelTextPanelIconSize = 64;

static constexpr i32 panelChooseMajorBlockTitleHeight = 32;
static constexpr i32 panelChooseMajorBlockSubtitleHeight = 24;
static constexpr i32 panelChooseMajorBlockTextHeight = 24;

static constexpr i32 panelHUDTextWidth = 512;
static constexpr i32 panelItemListWidth = 256;
static constexpr i32 panelItemListRowHeight = 64;

static constexpr Size panelMainMenuColumnSize = { 800, 544 };
static constexpr Size panelMainMenuIconSize = { 140, 140 };
static constexpr Size panelMainMenuTitleSize = { 384, 120 };
static constexpr i32 panelMainMenuIconOffset = 16;

static constexpr i32 panelTextCursorWidth = 2;

static constexpr f64 hudFontSize = 0.02;
static constexpr i32 hudBarHeight = 24 + (marginSize * 2);

namespace UIScale
{

Point windowBordersOffset()
{
    return Point(marginSize(), headerSize());
}

Size windowBordersSize()
{
    return Size(marginSize() * 2, headerSize() + marginSize());
}

Point dialogBordersOffset()
{
    return Point(marginSize()) * uiScale();
}

Size dialogBordersSize()
{
    return Size(marginSize() * 2) * uiScale();
}

i32 smallFontSize(const InterfaceWidget* widget)
{
    return ::smallFontSize * uiScale(widget);
}

i32 mediumFontSize(const InterfaceWidget* widget)
{
    return ::mediumFontSize * uiScale(widget);
}

i32 largeFontSize(const InterfaceWidget* widget)
{
    return ::largeFontSize * uiScale(widget);
}

i32 vrMenuButtonFontSize(const InterfaceWidget* widget)
{
    return ::vrMenuButtonFontSize * uiScale(widget);
}

i32 vrKeyboardFontSize(const InterfaceWidget* widget)
{
    return ::vrKeyboardFontSize * uiScale(widget);
}

i32 textTitlecardTitleFontSize(const InterfaceWidget* widget)
{
    return ::textTitlecardTitleFontSize * uiScale(widget);
}

i32 textTitlecardSubtitleFontSize(const InterfaceWidget* widget)
{
    return ::textTitlecardSubtitleFontSize * uiScale(widget);
}

i32 headerSize()
{
    return ::headerSize * uiScale();
}

i32 marginSize(const InterfaceWidget* widget)
{
    return ::marginSize * uiScale(widget);
}

Size checkboxSize()
{
    return ::checkboxSize * uiScale();
}

Size iconButtonSize()
{
    return ::iconButtonSize * uiScale();
}

Size indicatorIconSize()
{
    return ::indicatorIconSize * uiScale();
}

Size tabSize()
{
    return ::tabSize * uiScale();
}

i32 labelHeight()
{
    return ::labelHeight * uiScale();
}

i32 radioBarHeight()
{
    return ::radioBarHeight * uiScale();
}

i32 textButtonHeight()
{
    return ::textButtonHeight * uiScale();
}

i32 lineEditHeight()
{
    return ::lineEditHeight * uiScale();
}

i32 scrollbarWidth()
{
    return ::scrollbarWidth * uiScale();
}

i32 optionSelectHeight()
{
    return ::optionSelectHeight * uiScale();
}

i32 optionSelectEntryHeight()
{
    return ::optionSelectEntryHeight * uiScale();
}

i32 optionSelectPopupMaxHeight()
{
    return ::optionSelectMaxVisibleEntryCount * optionSelectEntryHeight();
}

i32 colorPickerHeight()
{
    return ::colorPickerHeight * uiScale();
}

i32 colorPickerPopupHeight()
{
    return (::marginSize * (2 + 1) + colorPickerGradientHeight + ::lineEditHeight) * uiScale();
}

i32 alphaColorPickerPopupHeight()
{
    return (::marginSize * (2 + 2) + colorPickerGradientHeight + ::lineEditHeight * 2) * uiScale();
}

i32 emissiveColorPickerPopupHeight()
{
    return (::marginSize * (2 + 3) + colorPickerGradientHeight + ::lineEditHeight * 2 + ::checkboxSize.h) * uiScale();
}

i32 hdrColorPickerPopupHeight()
{
    return (::marginSize * (2 + 2) + colorPickerGradientHeight + ::lineEditHeight * 2) * uiScale();
}

i32 hueBarWidth()
{
    return ::hueBarWidth * uiScale();
}

Size hueSliderSize()
{
    return ::hueSliderSize * uiScale();
}

i32 sliderHeight()
{
    return ::sliderHeight * uiScale();
}

i32 sliderTrackHeight()
{
    return ::sliderTrackHeight * uiScale();
}

i32 sliderThumbSize()
{
    return ::sliderThumbSize * uiScale();
}

i32 colorIndicatorBarHeight()
{
    return ::colorIndicatorBarHeight * uiScale();
}

i32 consoleOutputRowHeight()
{
    return ::consoleOutputRowHeight * uiScale();
}

i32 menuWidth()
{
    return ::menuWidth * uiScale();
}

i32 menuRowHeight()
{
    return ::menuRowHeight * uiScale();
}

Size destinationSelectorSize()
{
    return ::destinationSelectorSize * uiScale();
}

i32 destinationSelectorRowHeight()
{
    return ::destinationSelectorRowHeight * uiScale();
}

i32 destinationSelectorRows()
{
    return ::destinationSelectorRows;
}

i32 destinationSelectorPopupWidth()
{
    return ::destinationSelectorPopupWidth * uiScale();
}

i32 destinationSelectorPopupHeight()
{
    return ::destinationSelectorRows * ::destinationSelectorRowHeight * uiScale();
}

i32 consoleInputHeight()
{
    return ::consoleInputHeight * uiScale();
}

Size saveIndicatorSize()
{
    return ::saveIndicatorSize * uiScale();
}

i32 itemListWidth()
{
    return ::itemListWidth * uiScale();
}

i32 treeViewPanIncrement()
{
    return ::treeViewPanIncrement * uiScale();
}

i32 treeViewScrollIncrement()
{
    return ::treeViewScrollIncrement * uiScale();
}

i32 treeViewNodeIndent()
{
    return ::treeViewNodeIndent * uiScale();
}

i32 treeViewNodeHeight()
{
    return ::treeViewNodeHeight * uiScale();
}

i32 virtualTouchControlRoundedCornerRadius()
{
    return marginSize() * 2;
}

i32 helpColumnWidth()
{
    return ::helpColumnWidth * uiScale();
}

i32 helpRowHeight()
{
    return ::helpRowHeight * uiScale();
}

i32 dividerWidth()
{
    return ::dividerWidth;
}

Size spinnerSize()
{
    return ::spinnerSize * uiScale();
}

i32 sidePanelWidth()
{
    return ::sidePanelWidth * uiScale();
}

i32 scriptNodeLinkConnectorHeight()
{
    return ::scriptNodeLinkConnectorHeight * uiScale();
}

i32 scriptNodeCapHeight()
{
    return ::scriptNodeCapHeight * uiScale();
}

i32 scriptNodeLinkWidth()
{
    return ::scriptNodeLinkWidth * uiScale();
}

i32 scriptNodeWidth()
{
    return ::scriptNodeWidth * uiScale();
}

i32 scriptNodeDefaultHorizontalSpacing()
{
    return ::scriptNodeDefaultHorizontalSpacing * uiScale();
}

i32 scriptNodeDefaultVerticalSpacing()
{
    return ::scriptNodeDefaultVerticalSpacing * uiScale();
}

i32 scriptNodeBorderRadius()
{
    return ::scriptNodeBorderRadius * uiScale();
}

i32 scriptNodeHeaderHeight()
{
    return ::scriptNodeHeaderHeight * uiScale();
}

i32 widgetToBottomSpacing()
{
    return ::widgetToBottomSpacing * uiScale();
}

Size controlPointSize()
{
    return ::controlPointSize * uiScale();
}

Size controlPointTextSize()
{
    return ::controlPointTextSize * uiScale();
}

Size entityCardSize()
{
    return ::entityCardSize * uiScale();
}

Size entityCardRowSize()
{
    Size withoutMargins = (::entityCardSize - ::marginSize * 2) * uiScale();

    return Size(withoutMargins.w, withoutMargins.h / 3);
}

Size diagnosticLineSize()
{
    return ::diagnosticLineSize * uiScale();
}

Size markerSize()
{
    return ::markerSize * uiScale();
}

Size markerLineSize()
{
    return ::markerLineSize * uiScale();
}

i32 lineNumberColumnWidth()
{
    return (4 * TextStyle().getFont()->monoWidth()) + (2 * marginSize());
}

i32 oracleTableColumnWidth()
{
    return ::oracleTableColumnWidth * uiScale();
}

i32 oracleTableRowHeight()
{
    return ::oracleTableRowHeight * uiScale();
}

i32 oracleListWidth()
{
    return ::oracleListWidth * uiScale();
}

i32 timelineTopHeaderHeight()
{
    return ::timelineTopHeaderHeight * uiScale();
}

i32 timelineLeftHeaderWidth()
{
    return ::timelineLeftHeaderWidth * uiScale();
}

i32 timelineColumnWidth()
{
    return ::timelineColumnWidth * uiScale();
}

i32 timelineRowHeight()
{
    return ::timelineRowHeight * uiScale();
}

Size actionTimelineMarkerSize()
{
    return Size(::timelineRowHeight * uiScale());
}

i32 timelineEditorButtonSpacing()
{
    return ::timelineEditorButtonSpacing * uiScale();
}

i32 tooltipHeight()
{
    return ::tooltipHeight * uiScale();
}

i32 unitIndicatorWidth()
{
    return ::unitIndicatorWidth * uiScale();
}

i32 textCursorWidth()
{
    return ::textCursorWidth * uiScale();
}

i32 minBoxSelectSize()
{
    return ::minBoxSelectSize * uiScale();
}

i32 panelBorderSize(const InterfaceWidget* widget)
{
    return ::panelBorderSize * uiScale(widget);
}

i32 panelScrollbarWidth(const InterfaceWidget* widget)
{
    return ::panelScrollbarWidth * uiScale(widget);
}

i32 panelScrollbarTrackWidth(const InterfaceWidget* widget)
{
    return ::panelScrollbarTrackWidth * uiScale(widget);
}

i32 panelScrollbarThumbWidth(const InterfaceWidget* widget)
{
    return ::panelScrollbarThumbWidth * uiScale(widget);
}

Size panelIconButtonSize(const InterfaceWidget* widget)
{
    return ::panelIconButtonSize * uiScale(widget);
}

i32 panelTextButtonHeight(const InterfaceWidget* widget)
{
    return ::panelTextButtonHeight * uiScale(widget);
}

i32 panelOptionSelectHeight(const InterfaceWidget* widget)
{
    return ::panelOptionSelectHeight * uiScale(widget);
}

i32 panelOptionSelectEntryHeight(const InterfaceWidget* widget)
{
    return ::panelOptionSelectEntryHeight * uiScale(widget);
}

i32 panelOptionSelectPopupMaxHeight(const InterfaceWidget* widget)
{
    return ::optionSelectMaxVisibleEntryCount * panelOptionSelectEntryHeight(widget);
}

Size panelTabSize(const InterfaceWidget* widget)
{
    return ::panelTabSize * uiScale(widget);
}

Size panelCheckboxSize(const InterfaceWidget* widget)
{
    return ::panelCheckboxSize * uiScale(widget);
}

i32 panelLineEditHeight(const InterfaceWidget* widget)
{
    return ::panelLineEditHeight * uiScale(widget);
}

i32 panelLineEditLabelHeight(const InterfaceWidget* widget)
{
    return ::panelLineEditLabelHeight * uiScale(widget);
}

i32 panelLabelHeight(const InterfaceWidget* widget)
{
    return ::panelLabelHeight * uiScale(widget);
}

i32 panelBarTimerHeight(const InterfaceWidget* widget)
{
    return ::panelBarTimerHeight * uiScale(widget);
}

i32 panelGamepadMagnitudeToolHeight(const InterfaceWidget* widget)
{
    return ::panelGamepadMagnitudeToolHeight * uiScale(widget);
}

i32 panelTextPanelMargin(const InterfaceWidget* widget)
{
    return ::panelTextPanelMargin * uiScale(widget);
}

Size panelVRMenuButtonSize(const InterfaceWidget* widget)
{
    return ::panelVRMenuButtonSize * uiScale(widget);
}

i32 panelGamepadMenuItemSpacing(const InterfaceWidget* widget)
{
    return ::panelGamepadMenuItemSpacing * uiScale(widget);
}

i32 panelTextPanelItemSpacing(const InterfaceWidget* widget)
{
    return ::panelTextPanelItemSpacing * uiScale(widget);
}

i32 panelTextPanelIconSize(const InterfaceWidget* widget)
{
    return ::panelTextPanelIconSize * uiScale(widget);
}

i32 panelChooseMajorBlockTitleHeight(const InterfaceWidget* widget)
{
    return ::panelChooseMajorBlockTitleHeight * uiScale(widget);
}

i32 panelChooseMajorBlockSubtitleHeight(const InterfaceWidget* widget)
{
    return ::panelChooseMajorBlockSubtitleHeight * uiScale(widget);
}

i32 panelChooseMajorBlockTextHeight(const InterfaceWidget* widget)
{
    return ::panelChooseMajorBlockTextHeight * uiScale(widget);
}

i32 panelChooseMajorBlockHeight(const InterfaceWidget* widget)
{
    return (marginSize(widget) * 4)
        + panelChooseMajorBlockTitleHeight(widget)
        + panelChooseMajorBlockSubtitleHeight(widget)
        + panelChooseMajorBlockTextHeight(widget);
}

i32 panelHUDTextWidth(const InterfaceWidget* widget)
{
    return ::panelHUDTextWidth * uiScale(widget);
}

i32 panelItemListWidth(const InterfaceWidget* widget)
{
    return ::panelItemListWidth * uiScale(widget);
}

i32 panelItemListRowHeight(const InterfaceWidget* widget)
{
    return ::panelItemListRowHeight * uiScale(widget);
}

i32 panelTextTitlecardTopPadding(const InterfaceWidget* widget)
{
    return ::panelTextTitlecardTopPadding * uiScale(widget);
}

i32 panelTextTitlecardTitleHeight(const InterfaceWidget* widget)
{
    return ::panelTextTitlecardTitleHeight * uiScale(widget);
}

i32 panelTextTitlecardSubtitleHeight(const InterfaceWidget* widget)
{
    return ::panelTextTitlecardSubtitleHeight * uiScale(widget);
}

Size panelMainMenuColumnSize(const InterfaceWidget* widget)
{
    return ::panelMainMenuColumnSize * uiScale(widget);
}

Size panelMainMenuIconSize(const InterfaceWidget* widget)
{
    return ::panelMainMenuIconSize * uiScale(widget);
}

Size panelMainMenuTitleSize(const InterfaceWidget* widget)
{
    return ::panelMainMenuTitleSize * uiScale(widget);
}

i32 panelMainMenuIconOffset(const InterfaceWidget* widget)
{
    return ::panelMainMenuIconOffset * uiScale(widget);
}

i32 panelTextCursorWidth(const InterfaceWidget* widget)
{
    return ::panelTextCursorWidth * uiScale(widget);
}

i32 hudFontSize(const HUD* hud)
{
    return ::hudFontSize * hud->size().h;
}

i32 hudBarHeight(const HUD* hud)
{
    return ::hudBarHeight * (hud->size().h / static_cast<f64>(baseWindowHeight));
}

i32 hudMarginSize(const HUD* hud)
{
    return ::marginSize * (hud->size().h / static_cast<f64>(baseWindowHeight));
}

}
