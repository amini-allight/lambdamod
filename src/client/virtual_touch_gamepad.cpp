/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "virtual_touch_gamepad.hpp"
#include "localization.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "virtual_touch_button.hpp"
#include "virtual_touch_thumbstick.hpp"
#include "virtual_touch_trigger.hpp"

VirtualTouchGamepad::VirtualTouchGamepad(InterfaceView* view)
    : Layout(view)
    , leftTrigger(nullptr)
    , leftShoulder(nullptr)
    , rightTrigger(nullptr)
    , rightShoulder(nullptr)
    , back(nullptr)
    , guide(nullptr)
    , start(nullptr)
    , dpadLeft(nullptr)
    , dpadRight(nullptr)
    , dpadUp(nullptr)
    , dpadDown(nullptr)
    , a(nullptr)
    , b(nullptr)
    , x(nullptr)
    , y(nullptr)
    , leftStick(nullptr)
    , rightStick(nullptr)
{
    START_FILTERED_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("under-screen"), "virtual-touch-gamepad", input.isTouch())

    END_SCOPE

    leftTrigger = new VirtualTouchTrigger(this, localize("virtual-touch-gamepad-left-trigger"), Input_Gamepad_Left_Trigger);
    leftShoulder = new VirtualTouchButton(this, localize("virtual-touch-gamepad-left-shoulder"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchShoulderButtonSize, Input_Gamepad_Left_Bumper);

    rightTrigger = new VirtualTouchTrigger(this, localize("virtual-touch-gamepad-right-trigger"), Input_Gamepad_Right_Trigger);
    rightShoulder = new VirtualTouchButton(this, localize("virtual-touch-gamepad-right-shoulder"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchShoulderButtonSize, Input_Gamepad_Right_Bumper);

    back = new VirtualTouchButton(this, localize("virtual-touch-gamepad-back"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchSmallButtonSize, Input_Gamepad_Back);
    guide = new VirtualTouchButton(this, localize("virtual-touch-gamepad-guide"), Virtual_Touch_Button_Circle, virtualTouchRoundButtonSize, Input_Gamepad_Guide);
    start = new VirtualTouchButton(this, localize("virtual-touch-gamepad-start"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchSmallButtonSize, Input_Gamepad_Start);

    dpadLeft = new VirtualTouchButton(this, localize("virtual-touch-gamepad-dpad-left"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchDpadButtonSize, Input_Gamepad_DPad_Left);
    dpadRight = new VirtualTouchButton(this, localize("virtual-touch-gamepad-dpad-right"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchDpadButtonSize, Input_Gamepad_DPad_Right);
    dpadUp = new VirtualTouchButton(this, localize("virtual-touch-gamepad-dpad-up"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchDpadButtonSize, Input_Gamepad_DPad_Up);
    dpadDown = new VirtualTouchButton(this, localize("virtual-touch-gamepad-dpad-down"), Virtual_Touch_Button_Rounded_Rectangle, virtualTouchDpadButtonSize, Input_Gamepad_DPad_Down);

    a = new VirtualTouchButton(this, localize("virtual-touch-gamepad-a"), Virtual_Touch_Button_Circle, virtualTouchRoundButtonSize, Input_Gamepad_A);
    b = new VirtualTouchButton(this, localize("virtual-touch-gamepad-b"), Virtual_Touch_Button_Circle, virtualTouchRoundButtonSize, Input_Gamepad_B);
    x = new VirtualTouchButton(this, localize("virtual-touch-gamepad-x"), Virtual_Touch_Button_Circle, virtualTouchRoundButtonSize, Input_Gamepad_X);
    y = new VirtualTouchButton(this, localize("virtual-touch-gamepad-y"), Virtual_Touch_Button_Circle, virtualTouchRoundButtonSize, Input_Gamepad_Y);

    leftStick = new VirtualTouchThumbstick(this, localize("virtual-touch-gamepad-left-stick"), Input_Gamepad_Left_Stick, Input_Gamepad_Left_Stick_Click);
    rightStick = new VirtualTouchThumbstick(this, localize("virtual-touch-gamepad-right-stick"), Input_Gamepad_Right_Stick, Input_Gamepad_Right_Stick_Click);

    update();
}

void VirtualTouchGamepad::move(const Point& position)
{
    this->position(position);

    vec2 aspect = { static_cast<f64>(size().w) / static_cast<f64>(size().h), 1 };

    moveChildByCenter(leftTrigger, virtualTouchLeftTriggerCenter);
    moveChildByCenter(leftShoulder, virtualTouchLeftShoulderCenter);

    moveChildByCenter(rightTrigger, virtualTouchRightTriggerCenter);
    moveChildByCenter(rightShoulder, virtualTouchRightShoulderCenter);

    moveChildByCenter(back, virtualTouchBackButtonCenter);
    moveChildByCenter(guide, virtualTouchGuideButtonCenter);
    moveChildByCenter(start, virtualTouchStartButtonCenter);

    moveChildByCenter(dpadLeft, virtualTouchDpadCenter + vec2(-virtualTouchDpadOffset, 0) / aspect);
    moveChildByCenter(dpadRight, virtualTouchDpadCenter + vec2(+virtualTouchDpadOffset, 0) / aspect);
    moveChildByCenter(dpadUp, virtualTouchDpadCenter + vec2(0, -virtualTouchDpadOffset) / aspect);
    moveChildByCenter(dpadDown, virtualTouchDpadCenter + vec2(0, +virtualTouchDpadOffset) / aspect);

    moveChildByCenter(a, virtualTouchRightStickCenter + (rotate(virtualTouchRightStickOffset, radians(virtualTouchButtonRotations[3])) / aspect) + virtualTouchButtonOffset);
    moveChildByCenter(b, virtualTouchRightStickCenter + (rotate(virtualTouchRightStickOffset, radians(virtualTouchButtonRotations[2])) / aspect) + virtualTouchButtonOffset);
    moveChildByCenter(x, virtualTouchRightStickCenter + (rotate(virtualTouchRightStickOffset, radians(virtualTouchButtonRotations[1])) / aspect) + virtualTouchButtonOffset);
    moveChildByCenter(y, virtualTouchRightStickCenter + (rotate(virtualTouchRightStickOffset, radians(virtualTouchButtonRotations[0])) / aspect) + virtualTouchButtonOffset);

    moveChildByCenter(leftStick, virtualTouchLeftStickCenter);
    moveChildByCenter(rightStick, virtualTouchRightStickCenter);
}

void VirtualTouchGamepad::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        SizeProperties props = sizePropertiesOf(child);

        child->resize(Size(props.w, props.h));
    }
}

bool VirtualTouchGamepad::shouldDraw() const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        return false;
    }
    else
    {
        return g_config.touchInput && context()->controller()->input()->hasTouch();
    }
#else
    return g_config.touchInput && context()->controller()->input()->hasTouch();
#endif
}

void VirtualTouchGamepad::moveChildByCenter(InterfaceWidget* widget, const vec2& center)
{
    widget->move(center * size().toVec2() - (widget->size().toVec2() / 2));
}

SizeProperties VirtualTouchGamepad::sizeProperties() const
{
    return sizePropertiesFillAll;
}
