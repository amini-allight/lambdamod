/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamic_label.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

DynamicLabel::DynamicLabel(InterfaceWidget* parent, const function<string()>& source, const TextStyleOverride& styleOverride)
    : InterfaceWidget(parent)
    , source(source)
    , styleOverride(styleOverride)
{

}

void DynamicLabel::step()
{
    InterfaceWidget::step();

    string text = source();

    if (text != this->text)
    {
        this->text = text;
    }
}

void DynamicLabel::draw(const DrawContext& ctx) const
{
    TextStyle style;
    style.color = theme.inactiveTextColor;

    style = styleOverride.apply(style);

    drawText(
        ctx,
        this,
        text,
        style
    );
}

SizeProperties DynamicLabel::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::labelHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
