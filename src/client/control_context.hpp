/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

#include "controller.hpp"
#include "viewer_mode_context.hpp"
#include "attached_mode_context.hpp"

#include "flat_viewer.hpp"
#include "vr_viewer.hpp"
#include "vr_menu_button_handler.hpp"

#include "tether_manager.hpp"
#include "confinement_manager.hpp"
#include "tint_manager.hpp"
#include "fade_manager.hpp"
#include "wait_manager.hpp"

#include "text_manager.hpp"
#include "input_manager.hpp"
#include "user_visualizer.hpp"
#include "splash_manager.hpp"

#include "post_process_manager.hpp"

// Fix for Windows namespace pollution
#undef interface

class ControlContext
{
public:
    ControlContext(Controller* controller);
    ControlContext(const ControlContext& rhs) = delete;
    ControlContext(ControlContext&& rhs) = delete;
    ~ControlContext();

    ControlContext& operator=(const ControlContext& rhs) = delete;
    ControlContext& operator=(ControlContext&& rhs) = delete;

    void focus();
    void unfocus();
    void resize(u32 width, u32 height);
    
    void step();

    bool attached() const;

    Controller* controller() const;
    ViewerModeContext* viewerMode() const;
    AttachedModeContext* attachedMode() const;

    FlatViewer* flatViewer() const;
#ifdef LMOD_VR
    VRViewer* vrViewer() const;
    VRMenuButtonHandler* vrMenuButtonHandler() const;
#endif

    TetherManager* tether() const;
    ConfinementManager* confinement() const;
    TintManager* tint() const;
    FadeManager* fade() const;
    WaitManager* wait() const;

    TextManager* text() const;

    InputManager* input() const;

    UserVisualizer* userVisualizer() const;

    SplashManager* splash() const;

    Interface* interface() const;

    mat4 viewerSpace() const;

    void save();

    void moveViewer(const mat4& transform);

    void unbrake();
    void lockAttachments();
    void unlockAttachments();
    void setMagnitude(u32 magnitude);

    void setHUDShown(bool state);
    bool hudShown() const;

private:
    Controller* _controller;
    ViewerModeContext* _viewerMode;
    AttachedModeContext* _attachedMode;

    FlatViewer* _flatViewer;
#ifdef LMOD_VR
    VRViewer* _vrViewer;
    VRMenuButtonHandler* _vrMenuButtonHandler;
#endif

    TetherManager* _tether;
    ConfinementManager* _confinement;
    TintManager* _tint;
    FadeManager* _fade;
    WaitManager* _wait;

    TextManager* _text;

    InputManager* _input;

    UserVisualizer* _userVisualizer;

    SplashManager* _splash;

    PostProcessManager* postProcessManager;

    Interface* _interface;

    bool _hud;
};
