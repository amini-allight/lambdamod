/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_atmosphere.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "lightning_builder.hpp"

RenderAtmosphere::RenderAtmosphere(
    const RenderInterface* render,
    VkDescriptorSetLayout descriptorSetLayout,
    VkPipelineLayout lightningPipelineLayout,
    VkPipeline lightningPipeline,
    VkPipelineLayout linePipelineLayout,
    VkPipeline linePipeline,
    VkPipelineLayout solidPipelineLayout,
    VkPipeline solidPipeline,
    VkPipelineLayout cloudPipelineLayout,
    VkPipeline cloudPipeline,
    VkPipelineLayout streamerPipelineLayout,
    VkPipeline streamerPipeline,
    VkPipelineLayout warpPipelineLayout,
    VkPipeline warpPipeline
)
    : render(render)
    , descriptorSetLayout(descriptorSetLayout)
    , lightningPipelineLayout(lightningPipelineLayout)
    , lightningPipeline(lightningPipeline)
    , linePipelineLayout(linePipelineLayout)
    , linePipeline(linePipeline)
    , solidPipelineLayout(solidPipelineLayout)
    , solidPipeline(solidPipeline)
    , cloudPipelineLayout(cloudPipelineLayout)
    , cloudPipeline(cloudPipeline)
    , streamerPipelineLayout(streamerPipelineLayout)
    , streamerPipeline(streamerPipeline)
    , warpPipelineLayout(warpPipelineLayout)
    , warpPipeline(warpPipeline)
    , lastComponentIndex(-1)
    , lastLightningGenerateTime(currentTime())
    , lightningCount(0)
{
    lightningParticlesStaging = vector<AtmosphereLightningParticleGPU>(atmosphereLightningMaxParticleCount);
    lightningParticlesStartTimes = vector<chrono::milliseconds>(atmosphereLightningMaxParticleCount);

    lightningVertices = createStorageBuffer(atmosphereLightningVarietyCount * atmosphereLightningVertexCount * sizeof(fvec4));
    lightningVerticesMapping = new VmaMapping<fvec4>(render->context()->allocator, lightningVertices);
}

RenderAtmosphere::~RenderAtmosphere()
{
    delete lightningVerticesMapping;
    destroyBuffer(render->context()->allocator, lightningVertices);
}

void RenderAtmosphere::destroyComponents()
{
    lastComponentIndex = -1;

    for (u32 i = 0; i < components.size(); i++)
    {
        delete components.at(i);
    }

    components.clear();
}

void RenderAtmosphere::update(
    const MainSwapchainElement* swapchainElement,
    const AtmosphereParameters& atmosphere,
    vec3 cameraPosition[eyeCount],
    const vec3& up,
    const vec3& gravity,
    const vec3& flowVelocity,
    f64 density
)
{
    // Delay update until occlusion scene is ready
    if (RenderOcclusionSceneReference occlusionScene = swapchainElement->occlusionScene();
        occlusionScene.hardwareAccelerated() &&
        !occlusionScene.hardwareScene(0)
    )
    {
        return;
    }

    if (!lastAtmosphere || *lastAtmosphere != atmosphere)
    {
        lastComponentIndex = -1;
        engine = mt19937_64(atmosphere.seed);

        updateLightningVertices(atmosphere);

        lastAtmosphere = atmosphere;
    }

    RenderAtmosphereComponent* component = components.at(swapchainElement->imageIndex());
    fmat4 worldTransform = mat4(quaternion(pickForwardDirection(up), up));

    updateLightning(component, atmosphere, worldTransform);

    fillUniformBuffer(
        component,
        atmosphere,
        cameraPosition,
        worldTransform,
        gravity,
        flowVelocity,
        density
    );

    component->update(
        swapchainElement->sky()->imageView,
        swapchainElement->occlusionScene(),
        atmosphere,
        cameraPosition,
        gravity,
        flowVelocity,
        density
    );
}

void RenderAtmosphere::fillCommandBuffer(
    const MainSwapchainElement* swapchainElement,
    const AtmosphereParameters& atmosphere,
    const vec3& gravity,
    f64 density
)
{
    const RenderAtmosphereComponent* component = components.at(swapchainElement->imageIndex());

    VkCommandBuffer commandBuffer = swapchainElement->commandBuffer();
    u32 width = swapchainElement->width();
    u32 height = swapchainElement->height();

    VkViewport viewport;

    if (g_vr)
    {
        viewport = {
            0, 0,
            static_cast<f32>(width), static_cast<f32>(height),
            0, 1
        };
    }
    else
    {
        viewport = {
            0, static_cast<f32>(height),
            static_cast<f32>(width), -static_cast<f32>(height),
            0, 1
        };
    }

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { width, height }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        lightningPipelineLayout,
        0,
        1,
        &component->descriptorSet,
        0,
        nullptr
    );

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, lightningPipeline);

    vkCmdDraw(commandBuffer, atmosphereLightningVertexCount, lightningCount, 0, 0);

    // This can't be RenderAtmosphereComponent::fillCommandBuffer because it needs to do inter-component copies
    for (size_t i = 0; i < component->effects.size(); i++)
    {
        RenderAtmosphereComponentEffect* effect = component->effects.at(i);

        if (lastComponentIndex != -1)
        {
            const RenderAtmosphereComponentEffect* lastEffect = components.at(lastComponentIndex)->effects.at(i);

            effect->copyParticles(swapchainElement->transferCommandBuffer(), lastEffect);
        }
        else
        {
            effect->initParticles(atmosphere.effects.at(i), engine, gravity, density);
        }

        VkPipelineLayout effectPipelineLayout;
        VkPipeline effectPipeline;

        switch (effect->type)
        {
        default :
            throw runtime_error("Encountered unknown atmosphere effect type + '" + to_string(effect->type) + "'.");
        case Atmosphere_Effect_Line :
            effectPipelineLayout = linePipelineLayout;
            effectPipeline = linePipeline;
            break;
        case Atmosphere_Effect_Solid :
            effectPipelineLayout = solidPipelineLayout;
            effectPipeline = solidPipeline;
            break;
        case Atmosphere_Effect_Cloud :
            effectPipelineLayout = cloudPipelineLayout;
            effectPipeline = cloudPipeline;
            break;
        case Atmosphere_Effect_Streamer :
            effectPipelineLayout = streamerPipelineLayout;
            effectPipeline = streamerPipeline;
            break;
        case Atmosphere_Effect_Warp :
            effectPipelineLayout = warpPipelineLayout;
            effectPipeline = warpPipeline;
            break;
        }

        effect->fillCommandBuffer(commandBuffer, effectPipelineLayout, effectPipeline);
    }

    lastComponentIndex = swapchainElement->imageIndex();
}

void RenderAtmosphere::updateLightningVertices(const AtmosphereParameters& atmosphere)
{
    size_t writeHead = 0;

    for (u32 i = 0; i < atmosphereLightningVarietyCount; i++)
    {
        vector<fvec4> vertices = buildLightning(
            engine,
            atmosphereLightningPointCount,
            atmosphere.lightning.lowerHeight,
            atmosphere.lightning.upperHeight,
            atmosphereLightningThickness
        );

        memcpy(lightningVerticesMapping->data + writeHead, vertices.data(), vertices.size() * sizeof(fvec4));
        writeHead += vertices.size();
    }
}

void RenderAtmosphere::updateLightning(
    RenderAtmosphereComponent* component,
    const AtmosphereParameters& atmosphere,
    const fmat4& worldTransform
)
{
    while (lightningCount != 0 && currentTime() - lightningParticlesStartTimes[lightningCount - 1] > atmosphereLightningDuration)
    {
        removeLightning();
    }

    if (currentTime() - lastLightningGenerateTime > atmosphereLightningGenerateInterval)
    {
        chrono::milliseconds currentGenerateTime = (currentTime() / atmosphereLightningGenerateInterval) * atmosphereLightningGenerateInterval;
        u32 generateAttemptCount = (currentGenerateTime - lastLightningGenerateTime) / atmosphereLightningGenerateInterval;
        lastLightningGenerateTime = currentGenerateTime;

        if (!roughly(atmosphere.lightning.frequency, 0.0))
        {
            uniform_real_distribution<f64> distribution(0, 1);
            f64 cutoff = 1.0 / ((1000.0 / atmosphereLightningGenerateInterval.count()) / atmosphere.lightning.frequency);

            for (u32 i = 0; i < generateAttemptCount; i++)
            {
                if (lightningCount < atmosphereLightningMaxParticleCount && distribution(engine) < cutoff)
                {
                    addLightning(atmosphere, worldTransform, distribution);

                    if (lightningCount == atmosphereLightningMaxParticleCount)
                    {
                        break;
                    }
                }
            }
        }
    }

    memcpy(
        component->particlesMapping->data,
        lightningParticlesStaging.data(),
        lightningParticlesStaging.size() * sizeof(AtmosphereLightningParticleGPU)
    );
}

void RenderAtmosphere::addLightning(
    const AtmosphereParameters& atmosphere,
    const fmat4& worldTransform,
    uniform_real_distribution<f64>& distribution
)
{
    AtmosphereLightningParticleGPU& particle = lightningParticlesStaging[lightningCount];
    lightningParticlesStartTimes[lightningCount] = currentTime();

    vec3 lightningDir = quaternion(upDir, distribution(engine) * tau).rotate(forwardDir);
    f64 lightningDistance = atmosphere.lightning.minDistance + distribution(engine) * (atmosphere.lightning.maxDistance - atmosphere.lightning.minDistance);

    particle.position = fvec4(worldTransform.applyToDirection(lightningDir) * lightningDistance, 0);
    particle.vertexOffset = static_cast<u32>(distribution(engine) * atmosphereLightningVarietyCount) * atmosphereLightningVertexCount;

    lightningCount++;
}

void RenderAtmosphere::removeLightning()
{
    for (size_t i = 0; i < lightningCount - 1; i++)
    {
        lightningParticlesStaging[i] = lightningParticlesStaging[i + 1];
        lightningParticlesStartTimes[i] = lightningParticlesStartTimes[i + 1];
    }

    lightningCount--;
}

void RenderAtmosphere::fillUniformBuffer(
    RenderAtmosphereComponent* component,
    const AtmosphereParameters& atmosphere,
    vec3 cameraPosition[eyeCount],
    const fmat4& worldTransform,
    const vec3& worldGravity,
    const vec3& worldFlowVelocity,
    f64 worldDensity
) const
{
    vec3 averageCameraPosition = (cameraPosition[0] + cameraPosition[1]) / 2;

    AtmosphereLightningGPU uniform;
    uniform.worldTransform = worldTransform;
    uniform.model[0] = cameraLocalTransform(cameraPosition[0], averageCameraPosition);
    uniform.model[1] = cameraLocalTransform(cameraPosition[1], averageCameraPosition);
    uniform.color = atmosphere.lightning.color.toVec4();
    uniform.fogDistance = render->fogDistance();

    memcpy(component->uniformMapping->data, &uniform, sizeof(uniform));
}

mat4 RenderAtmosphere::cameraLocalTransform(const vec3& cameraPosition, const vec3& effectPosition) const
{
    mat4 model;

    model.setPosition(effectPosition - cameraPosition);

    return model;
}

VmaBuffer RenderAtmosphere::createStorageBuffer(size_t size) const
{
    return createBuffer(
        render->context()->allocator,
        size,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}
