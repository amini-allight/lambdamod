/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "new_world_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "line_edit.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(128 * uiScale(), UIScale::lineEditHeight());
}

NewWorldDialog::NewWorldDialog(InterfaceView* view)
    : Dialog(view, dialogSize())
{
    auto input = new LineEdit(
        this,
        nullptr,
        bind(&NewWorldDialog::onReturn, this, placeholders::_1)
    );
    input->setPlaceholder(localize("new-world-dialog-world-name"));
}

void NewWorldDialog::onReturn(const string& text)
{
    context()->controller()->edit([this, text]() -> void {
        context()->controller()->game().addWorld(text);
    });

    destroy();
}
