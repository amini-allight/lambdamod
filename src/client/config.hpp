/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum VRMotionMode : u8
{
    VR_Motion_Head,
    VR_Motion_Hand,
    VR_Motion_Hip
};

enum SoundSpatialMode : u8
{
    Sound_Spatial_Mode_Headphones,
    Sound_Spatial_Mode_Speakers
};

struct Config
{
    Config();

    bool startupSplash;
    bool splash;
    bool frameCounter;
    u64 frameLimit;
    u64 stepRate;
    f64 historyWindow;
    bool pingCounter;
    u32 antiAliasingLevel;
    u32 width;
    u32 height;
    bool fullscreen;
    bool outputMuted;
    f64 outputVolume;
    bool inputMuted;
    f64 inputVolume;
    VRMotionMode vrMotionMode;
    bool vrSmoothTurning;
    f64 vrSmoothTurnSpeed;
    f64 vrSnapTurnIncrement;
    bool vrKeyboardRightHand;
    bool vrForced;
    bool vrRollCage;
    string graphicsDevice;
    string soundInputDevice;
    string soundOutputDevice;
    string gamepadDevice;
    string touchDevice;
    string caFilePath;
    bool advancedTextEditor;
    bool nodeEditor;
    bool quaternionEditor;
    bool numpadAlternative;
    u64 maxRecursionDepth;
    u64 maxMemoryUsage;
    f64 maxLoopDuration;
    bool invertMouseViewerX;
    bool invertMouseViewerY;
    vec2 mouseViewerSensitivity;
    bool invertGamepadViewerX;
    bool invertGamepadViewerY;
    vec2 gamepadViewerSensitivity;
    vec2 gamepadLeftDeadZone;
    vec2 gamepadRightDeadZone;
    vec2 vrLeftDeadZone;
    vec2 vrRightDeadZone;
    SoundSpatialMode soundSpatialMode;
    bool soundMono;
    bool touchInput;
    bool flatMotionBlur;
    bool flatDepthOfField;
    bool flatBloom;
    bool flatChromaticAberration;
    bool vrMotionBlur;
    bool vrDepthOfField;
    bool vrBloom;
    bool vrChromaticAberration;
    bool rayTracing;
    bool imperialUnits;
    bool degrees;
    bool uiSounds;
    bool uiAutoScale;
    f64 uiScale;
    f64 luminanceScale;
};

Config loadConfig();
void saveConfig(const Config& config);
