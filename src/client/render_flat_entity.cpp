/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_entity.hpp"
#include "render_tools.hpp"
#include "log.hpp"
#include "entity.hpp"
#include "render_generators.hpp"
#include "flat_render.hpp"

RenderFlatEntity::RenderFlatEntity(
    RenderInterface* render,

    const vector<FlatSwapchainElement*>& swapchainElements,

    const RenderPipelineStore* pipelineStore,
    const Entity* entity,
    const BodyPartMask& bodyPartMask,
    vector<RenderDeletionTask<RenderFlatEntity>>* entitiesToDelete
)
    : RenderEntity(
        render,

        pipelineStore,
        entity,
        bodyPartMask,

        render->rayTracingEnabled()
    )
    , swapchainElements(swapchainElements)
    , entitiesToDelete(entitiesToDelete)
{
    createComponents();
}

RenderFlatEntity::~RenderFlatEntity()
{
    for (const auto& [ childID, child ] : _children)
    {
        delete child;
    }

    destroyComponents();
}

void RenderFlatEntity::work(
    const FlatSwapchainElement* swapchainElement,
    const vec3& cameraPosition,
    deque<RenderTransparentDraw>* transparentDraws,
    const set<EntityID>& hiddenEntityIDs
) const
{
    if (!hiddenEntityIDs.contains(id))
    {
        // Gated internally
        body->setupTextureLayouts(swapchainElement->transferCommandBuffer());

        RenderEntityComponent* component = components.at(swapchainElement->imageIndex());

        // Gated internally
        component->bind(
            body->materials,
            swapchainElements.at(swapchainElement->imageIndex())->camera(),
            swapchainElements.at(swapchainElement->imageIndex())->sky()->imageView,
            swapchainElements.at(swapchainElement->imageIndex())->occlusionScene()
        );

        auto uniform = new EntityUniformGPU();
        uniform->model[0] = cameraLocalTransform(cameraPosition);

        vector<RenderLight> lights = render->getLights(transform.position());
        uniform->lightCount = lights.size();

        for (size_t i = 0; i < lights.size(); i++)
        {
            uniform->lights[i] = lights.at(i).uniformData(cameraPosition);
        }

        uniform->fogDistance = render->fogDistance();
        uniform->timer = render->timer();

        memcpy(component->objectMapping->data, uniform, sizeof(EntityUniformGPU));

        delete uniform;

        fillCommandBuffer(
            swapchainElement,
            components.at(swapchainElement->imageIndex())->materialDescriptorSets,
            transparentDraws
        );

        RenderEntity::work();
    }

    for (const auto& [ childID, child ] : _children)
    {
        child->work(swapchainElement, cameraPosition, transparentDraws, hiddenEntityIDs);
    }
}

void RenderFlatEntity::update(const Entity* entity, const BodyPartMask& bodyPartMask)
{
    transform = entity->globalTransform();
    visible = entity->visible();

    if (entity->body() != lastBody)
    {
        auto [ maskedEntityID, hiddenBodyPartIDs ] = bodyPartMask;

        BodyRenderData renderData = entity->body().toRender(id == maskedEntityID ? hiddenBodyPartIDs : set<BodyPartID>());

        if (!body->supports(renderData.materials))
        {
            bodiesToDelete.push_back({ body, 0, components.size() });

            body = new RenderEntityBody(render->context(), pipelineStore, renderData.materials);
        }
        else
        {
            body->fill(renderData.materials);
        }

        if (!components.front()->supports(body->materials))
        {
            for (RenderEntityComponent* component : components)
            {
                componentsToDelete.push_back({ component, 0, components.size() });
            }

            components.clear();

            createComponents();
        }
        else
        {
            for (RenderEntityComponent* component : components)
            {
                component->needsRebind = true;
            }
        }

        lastBody = entity->body();

        setBodySideEffects(renderData, render->rayTracingEnabled());
    }
    else
    {
        updateBodySideEffects(transform);
    }

    for (const auto& [ childID, child ] : _children)
    {
        child->update(entity->children().at(childID), bodyPartMask);
    }
}

const map<EntityID, RenderFlatEntity*>& RenderFlatEntity::children() const
{
    return _children;
}

void RenderFlatEntity::refresh(const vector<FlatSwapchainElement*>& swapchainElements)
{
    this->swapchainElements = swapchainElements;

    destroyComponents();

    createComponents();

    for (const auto& [ childID, child ] : _children)
    {
        child->refresh(swapchainElements);
    }
}

void RenderFlatEntity::add(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask)
{
    if (parentID == id)
    {
        auto it = _children.find(entity->id());

        if (it != _children.end())
        {
            return;
        }

        auto child = new RenderFlatEntity(
            render,

            swapchainElements,

            pipelineStore,
            entity,
            bodyPartMask,
            entitiesToDelete
        );

        render->addLights(child);

        _children.insert({
            entity->id(),
            child
        });
    }
    else
    {
        for (const auto& [ childID, child ] : _children)
        {
            child->add(parentID, entity, bodyPartMask);
        }
    }
}

void RenderFlatEntity::remove(EntityID parentID, EntityID id)
{
    if (parentID == this->id)
    {
        auto it = _children.find(id);

        if (it == _children.end())
        {
            return;
        }

        RenderFlatEntity* child = it->second;

        render->removeLights(child);

        _children.erase(it);
        entitiesToDelete->push_back(RenderDeletionTask(child));
    }
    else
    {
        for (const auto& [ childID, child ] : _children)
        {
            child->remove(parentID, id);
        }
    }
}

void RenderFlatEntity::update(EntityID parentID, const Entity* entity, const BodyPartMask& bodyPartMask)
{
    if (parentID == id)
    {
        auto [ maskedEntityID, hiddenBodyPartIDs ] = bodyPartMask;

        if (entity->body().empty(maskedEntityID == id ? hiddenBodyPartIDs : set<BodyPartID>()))
        {
            remove(parentID, entity->id());
            return;
        }

        auto it = _children.find(entity->id());

        if (it == _children.end())
        {
            add(parentID, entity, bodyPartMask);
            return;
        }

        RenderFlatEntity* child = it->second;

        child->update(entity, bodyPartMask);

        render->updateLights(child);
    }
    else
    {
        for (const auto& [ childID, child ] : _children)
        {
            child->update(parentID, entity, bodyPartMask);
        }
    }
}

void RenderFlatEntity::createComponents()
{
    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        components.push_back(new RenderEntityComponent(
            render->context(),
            body->materials,
            swapchainElements.at(i)->camera(),
            swapchainElements.at(i)->sky()->imageView,
            swapchainElements.at(i)->occlusionScene()
        ));
    }
}

void RenderFlatEntity::destroyComponents()
{
    for (u32 i = 0; i < components.size(); i++)
    {
        delete components.at(i);
    }

    components.clear();
}
