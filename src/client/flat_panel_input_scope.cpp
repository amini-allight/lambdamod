/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "flat_panel_input_scope.hpp"
#include "interface_widget.hpp"
#include "control_context.hpp"
#include "flat_render.hpp"

FlatPanelInputScope::FlatPanelInputScope(
    InterfaceWidget* widget,
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<optional<Point>(const Point&)>& toLocal,
    const function<void(InputScope*)>& block
)
    : InputScope(
        widget->context(),
        parent,
        name,
        mode,
        std::bind(&FlatPanelInputScope::isTargeted, this, placeholders::_1),
        block
    )
    , widget(widget)
    , toLocal(toLocal)
{

}

bool FlatPanelInputScope::movePointer(PointerType type, const Point& pointer, bool consumed) const
{
    optional<Point> point = toLocal(pointer);

    if (!point)
    {
        return false;
    }

    bool consumedLocally = widget->shouldDraw() && widget->movePointer(type, *point, consumed);
    bool consumedByChild = InputScope::movePointer(type, *point, consumed);

    return consumedLocally || consumedByChild;
}

bool FlatPanelInputScope::isTargeted(const Input& input) const
{
    auto render = dynamic_cast<const FlatRender*>(ctx->controller()->render());

    Point pointer(input.position.x * render->width(), input.position.y * render->height());

    return input.isFlat() && toLocal(pointer).has_value();
}
