/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_titlecard.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "control_context.hpp"

static constexpr f64 opacityPeak = 1.7 / 8.0;
static constexpr f64 minOpacity = 0.01;

TextTitlecard::TextTitlecard(InterfaceView* view)
    : InterfaceWidget(view)
{

}

void TextTitlecard::draw(const DrawContext& ctx) const
{
    if (context()->text()->titlecard())
    {
        f64 progress = context()->text()->titlecard()->elapsed().count() / static_cast<f64>(textTitlecardDuration.count());

        f64 opacity;

        if (progress <= opacityPeak)
        {
            opacity = progress / opacityPeak;
        }
        else
        {
            opacity = 1 - ((progress - opacityPeak) / (1 - opacityPeak));
        }

        opacity = sq(opacity);

        if (opacity <= minOpacity)
        {
            return;
        }

        TextStyle style(this);
        style.color.a *= opacity;
        style.font = Text_Sans_Serif;
        style.alignment = Text_Center;
        style.weight = Text_Extra_Bold;
        style.size = UIScale::textTitlecardTitleFontSize(this);

        drawText(
            ctx,
            this,
            Point(0, UIScale::panelTextTitlecardTopPadding(this)),
            Size(ctx.size.w, UIScale::panelTextTitlecardTitleHeight(this)),
            toTitle(context()->text()->titlecard()->data<TextTitlecardRequest>().title),
            style
        );

        style.size = UIScale::textTitlecardSubtitleFontSize(this);

        drawText(
            ctx,
            this,
            Point(0, UIScale::panelTextTitlecardTopPadding(this) + UIScale::panelTextTitlecardTitleHeight(this)),
            Size(ctx.size.w, UIScale::panelTextTitlecardSubtitleHeight(this)),
            context()->text()->titlecard()->data<TextTitlecardRequest>().subtitle,
            style
        );
    }
}

string TextTitlecard::toTitle(const string& title) const
{
    string s;

    for (size_t i = 0; i < title.size(); i++)
    {
        char c = title[i];

        s += c >= 'a' && c <= 'z' ? c - ('a' - 'A') : c;

        if (i + 1 != title.size())
        {
            s += ' ';
        }
    }

    return s;
}

SizeProperties TextTitlecard::sizeProperties() const
{
    return sizePropertiesFillAll;
}
