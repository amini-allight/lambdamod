/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "virtual_touch_thumbstick.hpp"
#include "global.hpp"
#include "tools.hpp"
#include "interface_constants.hpp"
#include "interface_scale.hpp"
#include "interface_draw.hpp"
#include "interface_view.hpp"
#include "control_context.hpp"
#include "flat_render.hpp"

VirtualTouchThumbstick::VirtualTouchThumbstick(
    InterfaceWidget* parent,
    const string& name,
    InputType output,
    InputType clickOutput
)
    : InterfaceWidget(parent)
    , name(name)
    , output(output)
    , clickOutput(clickOutput)
{
    START_WIDGET_SCOPE("virtual-touch-thumbstick")
        WIDGET_SLOT("touch", touch)
        WIDGET_SLOT("double-touch", doubleTouch)
    END_SCOPE
}

void VirtualTouchThumbstick::draw(const DrawContext& ctx) const
{
    drawRoundedBorder(
        ctx,
        this,
        UIScale::marginSize(),
        size().w / 2,
        theme.touchInterfaceColor
    );

    drawRoundedBorder(
        ctx,
        this,
        hatPosition(),
        hatSize(),
        UIScale::marginSize(),
        hatSize().w / 2,
        touchID ? theme.touchInterfaceActiveColor : theme.touchInterfaceColor
    );

    TextStyle style;
    style.alignment = Text_Center;
    style.size = UIScale::largeFontSize();
    style.weight = Text_Bold;
    style.font = Text_Sans_Serif;
    style.color = touchID ? theme.touchInterfaceActiveColor : theme.touchInterfaceColor;

    drawText(
        ctx,
        this,
        hatPosition(),
        hatSize(),
        name,
        style
    );
}

void VirtualTouchThumbstick::touch(const Input& input)
{
    if (input.up && touchID && *touchID == input.touch.id)
    {
        touchID = {};
        touchPosition = vec2();
    }
    else if (!input.up && !touchID)
    {
        touchID = input.touch.id;
    }
    else if (!input.up && touchID && *touchID == input.touch.id)
    {
        vec2 screenSize(
            dynamic_cast<FlatRender*>(context()->controller()->render())->width(),
            dynamic_cast<FlatRender*>(context()->controller()->render())->height()
        );
        vec2 offset = (input.position.toVec2() * screenSize - center().toVec2()) / hatMovementRange();

        offset.x = clamp<f64>(offset.x, -1, +1);
        offset.y = clamp<f64>(offset.y, -1, +1);

        touchPosition = offset;

        if (output == Input_Gamepad_Left_Stick)
        {
            offset.x = applyDeadZone(offset.x, g_config.gamepadLeftDeadZone.x);
            offset.y = applyDeadZone(offset.y, g_config.gamepadLeftDeadZone.y);
        }
        else if (output == Input_Gamepad_Right_Stick)
        {
            offset.x = applyDeadZone(offset.x, g_config.gamepadRightDeadZone.x);
            offset.y = applyDeadZone(offset.y, g_config.gamepadRightDeadZone.y);
        }

        Input virtualInput;
        virtualInput.type = output;
        virtualInput.position = vec3(touchPosition, 0);
        context()->input()->handle(virtualInput);
    }
}

void VirtualTouchThumbstick::doubleTouch(const Input& input)
{
    Input virtualInput;
    virtualInput.type = clickOutput;
    context()->input()->handle(virtualInput);
}

Point VirtualTouchThumbstick::center() const
{
    return position() + (size() / 2);
}

Point VirtualTouchThumbstick::hatCenter() const
{
    return (size().toVec2() / 2) + touchPosition.normalize() * hatMovementRange();
}

Point VirtualTouchThumbstick::hatPosition() const
{
    return hatCenter() - (hatSize() / 2);
}

Size VirtualTouchThumbstick::hatSize() const
{
    return size() * virtualTouchThumbstickHatSize;
}

f64 VirtualTouchThumbstick::hatMovementRange() const
{
    return (size().w / 2) - (UIScale::marginSize() + (hatSize().w / 2));
}

SizeProperties VirtualTouchThumbstick::sizeProperties() const
{
    return {
        virtualTouchThumbstickSize * parent()->size().h,
        virtualTouchThumbstickSize * parent()->size().h,
        Scaling_Fixed,
        Scaling_Fixed
    };
}
