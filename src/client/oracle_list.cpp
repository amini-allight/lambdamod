/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "oracle_list.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "row.hpp"
#include "column.hpp"
#include "label.hpp"
#include "spacer.hpp"
#include "text_button.hpp"
#include "localization.hpp"

OracleList::OracleList(
    InterfaceWidget* parent,
    const function<vector<string>()>& source,
    const function<void(const string&)>& onSelect,
    const function<void()>& onRoll
)
    : InterfaceWidget(parent)
    , activeIndex(0)
    , source(source)
    , _onSelect(onSelect)
{
    auto column = new Column(this);

    listView = new ListView(
        column,
        bind(&OracleList::onSelect, this, placeholders::_1)
    );

    MAKE_SPACER(column, 0, UIScale::dividerWidth(), theme.dividerColor);

    new TextButton(
        column,
        localize("oracle-list-roll"),
        onRoll
    );
}

void OracleList::select(i32 index)
{
    onSelect(index);
}

void OracleList::step()
{
    InterfaceWidget::step();

    vector<string> items = source();

    if (items != this->items)
    {
        this->items = items;
        updateListView();
    }
}

void OracleList::updateListView()
{
    listView->clearChildren();

    i32 i = 0;
    for (const string& item : items)
    {
        bool focused = i == activeIndex;

        auto row = new Row(listView, focused ? theme.accentColor : optional<Color>());

        TextStyleOverride style;
        style.weight = focused ? Text_Bold : Text_Regular;
        style.color = focused ? &theme.activeTextColor : &theme.textColor;

        new Label(
            row,
            item,
            style
        );
        i++;
    }

    listView->update();
}

void OracleList::onSelect(i32 index)
{
    if (index == activeIndex)
    {
        return;
    }

    activeIndex = index;
    updateListView();

    _onSelect(items[index]);
}

SizeProperties OracleList::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::oracleListWidth()), 0,
        Scaling_Fixed, Scaling_Fill
    };
}
