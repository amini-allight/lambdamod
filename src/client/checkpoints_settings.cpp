/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "checkpoints_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "row.hpp"
#include "label.hpp"
#include "spacer.hpp"
#include "fuzzy_find.hpp"

#include "new_checkpoint_dialog.hpp"
#include "confirm_dialog.hpp"

CheckpointDetail::CheckpointDetail()
{

}

bool CheckpointDetail::operator==(const CheckpointDetail& rhs) const
{
    return name == rhs.name;
}

bool CheckpointDetail::operator!=(const CheckpointDetail& rhs) const
{
    return !(*this == rhs);
}

CheckpointsSettings::CheckpointsSettings(InterfaceWidget* parent)
    : RightClickableWidget(parent)
{
    START_WIDGET_SCOPE("checkpoints-settings")
        WIDGET_SLOT("add-new", add)
    END_SCOPE

    auto column = new Column(this);

    auto header = new Row(column);

    TextStyleOverride style;
    style.weight = Text_Bold;
    style.color = &theme.activeTextColor;
    style.alignment = Text_Center;

    new Label(header, localize("checkpoints-settings-name"), style);
    MAKE_SPACER(header, UIScale::scrollbarWidth(), UIScale::labelHeight());

    searchBar = new LineEdit(column, nullptr, [](const string& text) -> void {});
    searchBar->setPlaceholder(localize("checkpoints-settings-type-to-search"));

    listView = new ListView(column);
}

void CheckpointsSettings::step()
{
    RightClickableWidget::step();

    vector<CheckpointDetail> details = convert(context()->controller()->checkpoints());
    string search = searchBar->content();

    if (details != this->details || search != this->search)
    {
        this->details = details;
        this->search = search;
        updateListView();
    }
}

vector<CheckpointDetail> CheckpointsSettings::CheckpointsSettings::convert(const set<string>& checkpoints) const
{
    vector<CheckpointDetail> details;
    details.reserve(checkpoints.size());

    for (const string& checkpoint : checkpoints)
    {
        CheckpointDetail detail;
        detail.name = checkpoint;

        details.push_back(detail);
    }

    return details;
}

void CheckpointsSettings::openRightClickMenu(i32 index, const Point& pointer)
{
    if (index >= static_cast<i32>(details.size()))
    {
        return;
    }

    const CheckpointDetail& detail = details[index];

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("checkpoints-settings-remove"), bind(&CheckpointsSettings::onRemove, this, detail.name, placeholders::_1) },
        { localize("checkpoints-settings-revert"), bind(&CheckpointsSettings::onRevert, this, detail.name, placeholders::_1) }
    };

    rightClickMenu = new RightClickMenu(
        this,
        bind(&CheckpointsSettings::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void CheckpointsSettings::onRemove(const string& name, const Point& point)
{
    context()->controller()->send(CheckpointEvent(Checkpoint_Event_Remove, name));

    closeRightClickMenu();
}

void CheckpointsSettings::onRevert(const string& name, const Point& point)
{
    context()->controller()->send(CheckpointEvent(Checkpoint_Event_Revert, name));

    closeRightClickMenu();
}

void CheckpointsSettings::updateListView()
{
    listView->clearChildren();

    // If we sort the member variable version it will cause an update on every CheckpointsSettings::step
    vector<CheckpointDetail> details = this->details;

    if (!search.empty())
    {
        sort(
            details.begin(),
            details.end(),
            [&](const CheckpointDetail& a, const CheckpointDetail& b) -> bool
            {
                return fuzzyFindCompare(a.name, b.name, search);
            }
        );
    }

    size_t i = 0;
    for (const CheckpointDetail& detail : details)
    {
        auto row = new Row(
            listView,
            {},
            [this, i](const Point& pointer) -> void
            {
                onRightClick(i, pointer);
            }
        );

        new Label(row, detail.name);

        i++;
    }

    listView->update();
}

void CheckpointsSettings::add(const Input& input)
{
    context()->interface()->addWindow<NewCheckpointDialog>();
}

SizeProperties CheckpointsSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
