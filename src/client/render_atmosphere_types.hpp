/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "render_constants.hpp"
#include "render_light.hpp"

#pragma pack(1)
struct AtmosphereLightningGPU
{
    fmat4 worldTransform;
    fmat4 model[eyeCount];
    fvec4 color;
    f32 fogDistance;
};

struct AtmosphereEffectGPU
{
    fmat4 model[eyeCount];
    fvec4 gravity;
    fvec4 flowVelocity;
    fvec4 color;
    f32 density;
    f32 size;
    f32 radius;
    f32 mass;
    LightGPU lights[maxLightCount];
    i32 lightCount;
    f32 fogDistance;
    f32 timer;
    f32 areaRadius;
    fvec4 viewerPositionDelta;
};

struct AtmosphereLightningParticleGPU
{
    fvec4 position;
    i32 vertexOffset;
    i32 pad[3];
};

struct AtmosphereEffectParticleGPU
{
    fvec4 position;
    fvec4 linearVelocity;
    f32 rotation;
    f32 timer;
    f32 pad[2];
};
#pragma pack()
