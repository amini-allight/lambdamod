/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

#include "line_edit.hpp"
#include "list_view.hpp"

class BindingsEditor : public InterfaceWidget, public TabContent
{
public:
    BindingsEditor(InterfaceWidget* parent, EntityID id);

    void step() override;

private:
    EntityID id;
    InputType inputType;

    map<InputType, InputBinding> bindings;
    LineEdit* lineEdit;
    ListView* listView;

    vector<string> inputTypesSource();
    void onSelect(size_t index);
    void onAdd(const string& text);
    void onRemove(InputType type);

    void updateListView();

    void undo(const Input& input);
    void redo(const Input& input);

    SizeProperties sizeProperties() const override;
};
