/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_bindings_loader.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "yaml_tools.hpp"

map<string, vector<InputScopeBinding>> loadInputBindings()
{
    string path = configPath() + bindingsFileName;

    if (!fs::exists(path))
    {
        return {};
    }

    YAMLParser ctx(path);

    return ctx.root().as<map<string, vector<InputScopeBinding>>>();
}

void saveInputBindings(const map<string, vector<InputScopeBinding>>& bindings)
{
    string path = configPath() + bindingsFileName;

    YAMLSerializer ctx(path);

    ctx.emit(bindings);
}
