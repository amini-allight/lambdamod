/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "connection_details.hpp"
#include "yaml_tools.hpp"

ConnectConnectionDetails::ConnectConnectionDetails()
{
    port = 7443;
}

template<>
ConnectConnectionDetails YAMLNode::convert() const
{
    ConnectConnectionDetails details;

    details.host = at("host").as<string>();
    details.port = at("port").as<u16>();
    details.serverPassword = at("serverPassword").as<string>();
    details.name = at("name").as<string>();
    details.password = at("password").as<string>();

    return details;
}

template<>
void YAMLSerializer::emit(ConnectConnectionDetails details)
{
    startMapping();

    emitPair("host", details.host);
    emitPair("port", details.port);
    emitPair("serverPassword", details.serverPassword);
    emitPair("name", details.name);
    emitPair("password", details.password);

    endMapping();
}

HostConnectionDetails::HostConnectionDetails()
{
    port = 7443;
}

template<>
HostConnectionDetails YAMLNode::convert() const
{
    HostConnectionDetails details;

    details.host = at("host").as<string>();
    details.port = at("port").as<u16>();
    details.serverPassword = at("serverPassword").as<string>();
    details.adminPassword = at("adminPassword").as<string>();
    details.savePath = at("savePath").as<string>();
    details.name = at("name").as<string>();
    details.password = at("password").as<string>();

    return details;
}

template<>
void YAMLSerializer::emit(HostConnectionDetails details)
{
    startMapping();

    emitPair("host", details.host);
    emitPair("port", details.port);
    emitPair("serverPassword", details.serverPassword);
    emitPair("adminPassword", details.adminPassword);
    emitPair("savePath", details.savePath);
    emitPair("name", details.name);
    emitPair("password", details.password);

    endMapping();
}

ConnectionDetails::ConnectionDetails()
{

}

template<>
ConnectionDetails YAMLNode::convert() const
{
    ConnectionDetails details;

    details.connect = at("connect").as<ConnectConnectionDetails>();
    details.host = at("host").as<HostConnectionDetails>();

    return details;
}

template<>
void YAMLSerializer::emit(ConnectionDetails details)
{
    startMapping();

    emitPair("connect", details.connect);
    emitPair("host", details.host);

    endMapping();
}
