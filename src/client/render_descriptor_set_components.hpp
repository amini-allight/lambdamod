/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"

VkDescriptorSetLayoutBinding uniformBufferLayoutBinding(u32 index, i32 stages, u32 count = 1);
VkDescriptorSetLayoutBinding storageBufferLayoutBinding(u32 index, i32 stages, u32 count = 1);
VkDescriptorSetLayoutBinding combinedSamplerLayoutBinding(u32 index, i32 stages, u32 count = 1);
VkDescriptorSetLayoutBinding sampledImageLayoutBinding(u32 index, i32 stages, u32 count = 1);
VkDescriptorSetLayoutBinding storageImageLayoutBinding(u32 index, i32 stages, u32 count = 1);
VkDescriptorSetLayoutBinding accelerationStructureLayoutBinding(u32 index, i32 stages, u32 count = 1);
