/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

#include <opus/opus.h>

inline string opusError(i32 status)
{
    switch (status)
    {
    default : return "unknown error.";
    case OPUS_OK : return "ok.";
    case OPUS_BAD_ARG : return "bad argument.";
    case OPUS_BUFFER_TOO_SMALL : return "buffer too small.";
    case OPUS_INTERNAL_ERROR : return "internal error.";
    case OPUS_INVALID_PACKET : return "invalid packet.";
    case OPUS_UNIMPLEMENTED : return "unimplemented.";
    case OPUS_INVALID_STATE : return "invalid state.";
    case OPUS_ALLOC_FAIL : return "allocation fail.";
    }
}
