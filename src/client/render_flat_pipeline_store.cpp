/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_pipeline_store.hpp"
#include "render_tools.hpp"
#include "render_objects.hpp"
#include "paths.hpp"
#include "tools.hpp"

RenderFlatPipelineStore::RenderFlatPipelineStore(
    const RenderContext* ctx,
    VkRenderPass skyRenderPass,
    VkRenderPass mainRenderPass,
    VkRenderPass hdrRenderPass,
    VkRenderPass postProcessRenderPass,
    VkRenderPass overlayRenderPass,
    VkRenderPass independentDepthOverlayRenderPass,
    VkSampleCountFlagBits sampleCount
)
    : RenderPipelineStore(ctx)

    , objectDescriptorSetLayout(ctx, createObjectDescriptorSetLayout)
    , litObjectDescriptorSetLayout(ctx, createLitObjectDescriptorSetLayout)
    , texturedObjectDescriptorSetLayout(ctx, createTexturedObjectDescriptorSetLayout)
    , litTexturedObjectDescriptorSetLayout(ctx, createLitTexturedObjectDescriptorSetLayout)
    , materialedObjectDescriptorSetLayout(ctx, createMaterialedObjectDescriptorSetLayout)
    , litMaterialedObjectDescriptorSetLayout(ctx, createLitMaterialedObjectDescriptorSetLayout)
    , areaDescriptorSetLayout(ctx, createAreaDescriptorSetLayout)
    , litAreaDescriptorSetLayout(ctx, createLitAreaDescriptorSetLayout)
    , panelDescriptorSetLayout(ctx, createTextDescriptorSetLayout)
    , postProcessDescriptorSetLayout(ctx, createPostProcessDescriptorSetLayout)
    , skyDescriptorSetLayout(ctx, createSkyDescriptorSetLayout)
    , atmosphereDescriptorSetLayout(ctx, createAtmosphereDescriptorSetLayout)
    , hdrDescriptorSetLayout(ctx, createHDRDescriptorSetLayout)
    , depthOfFieldDescriptorSetLayout(ctx, createDepthOfFieldDescriptorSetLayout)
    , motionBlurDescriptorSetLayout(ctx, createMotionBlurDescriptorSetLayout)

    , coloredVertexShader(ctx, "/colored_vertex")
    , coloredFragmentShader(ctx, "/colored_fragment")
    , litColoredVertexShader(ctx, "/lit_colored_vertex")
    , litColoredFragmentShader(ctx, selectRayTracing("/lit_colored_fragment"))
    , texturedVertexShader(ctx, "/textured_vertex")
    , texturedFragmentShader(ctx, "/textured_fragment")
    , litTexturedVertexShader(ctx, "/lit_textured_vertex")
    , litTexturedFragmentShader(ctx, selectRayTracing("/lit_textured_fragment"))
    , areaVertexShader(ctx, "/area_vertex")
    , areaFragmentShader(ctx, "/area_fragment")
    , litAreaVertexShader(ctx, "/lit_area_vertex")
    , litAreaFragmentShader(ctx, selectRayTracing("/lit_area_fragment"))
    , terrainVertexShader(ctx, "/terrain_vertex")
    , terrainFragmentShader(ctx, "/terrain_fragment")
    , litTerrainVertexShader(ctx, "/lit_terrain_vertex")
    , litTerrainFragmentShader(ctx, selectRayTracing("/lit_terrain_fragment"))
    , ropeVertexShader(ctx, "/rope_vertex")
    , ropeGeometryShader(ctx, "/rope_geometry")
    , ropeFragmentShader(ctx, "/rope_fragment")
    , litRopeVertexShader(ctx, "/lit_rope_vertex")
    , litRopeGeometryShader(ctx, "/lit_rope_geometry")
    , litRopeFragmentShader(ctx, selectRayTracing("/lit_rope_fragment"))
    , coloredOverlayVertexShader(ctx, "/colored_overlay_vertex")
    , coloredOverlayFragmentShader(ctx, "/colored_overlay_fragment")
    , orthographicGridVertexShader(ctx, "/orthographic_grid_vertex")
    , orthographicGridFragmentShader(ctx, "/orthographic_grid_fragment")
    , panelVertexShader(ctx, "/panel_vertex")
    , panelFragmentShader(ctx, "/panel_fragment")
    , tetherIndicatorVertexShader(ctx, "/tether_indicator_vertex")
    , tetherIndicatorFragmentShader(ctx, "/tether_indicator_fragment")
    , confinementIndicatorVertexShader(ctx, "/confinement_indicator_vertex")
    , confinementIndicatorFragmentShader(ctx, "/confinement_indicator_fragment")
    , waitingIndicatorVertexShader(ctx, "/flat_waiting_indicator_vertex")
    , waitingIndicatorFragmentShader(ctx, "/flat_waiting_indicator_fragment")
    , themeGradientVertexShader(ctx, "/theme_gradient_vertex")
    , themeGradientFragmentShader(ctx, "/theme_gradient_fragment")
    , screenShader(ctx, "/screen")
    , hdrShader(ctx, "/flat_hdr")
    , fadeShader(ctx, "/flat_fade")
    , motionBlurShader(ctx, "/flat_motion_blur")
    , chromaticAberrationShader(ctx, "/flat_chromatic_aberration")
    , depthOfFieldShader(ctx, "/flat_depth_of_field")
    , tintShader(ctx, "/flat_tint")
    , skyVertexShader(ctx, "/sky_vertex")
    , skyFragmentShader(ctx, "/sky_fragment")
    , skyBillboardVertexShader(ctx, "/sky_billboard_vertex")
    , skyBillboardGeometryShader(ctx, "/sky_billboard_geometry")
    , skyStarsFragmentShader(ctx, "/sky_stars_fragment")
    , skyCircleFragmentShader(ctx, "/sky_circle_fragment")
    , skyCloudFragmentShader(ctx, "/sky_cloud_fragment")
    , skyAuroraVertexShader(ctx, "/sky_aurora_vertex")
    , skyAuroraGeometryShader(ctx, "/sky_aurora_geometry")
    , skyAuroraFragmentShader(ctx, "/sky_aurora_fragment")
    , skyCometVertexShader(ctx, "/sky_comet_vertex")
    , skyCometGeometryShader(ctx, "/sky_comet_geometry")
    , skyCometFragmentShader(ctx, "/sky_comet_fragment")
    , skyVortexVertexShader(ctx, "/sky_vortex_vertex")
    , skyVortexGeometryShader(ctx, "/sky_vortex_geometry")
    , skyVortexFragmentShader(ctx, "/sky_vortex_fragment")
    , skyEnvMapVertexShader(ctx, "/sky_env_map_vertex")
    , skyEnvMapFragmentShader(ctx, "/sky_env_map_fragment")
    , skyEnvMapBillboardVertexShader(ctx, "/sky_env_map_billboard_vertex")
    , skyEnvMapBillboardGeometryShader(ctx, "/sky_env_map_billboard_geometry")
    , skyEnvMapStarsFragmentShader(ctx, "/sky_env_map_stars_fragment")
    , skyEnvMapCircleFragmentShader(ctx, "/sky_env_map_circle_fragment")
    , skyEnvMapCloudFragmentShader(ctx, "/sky_env_map_cloud_fragment")
    , skyEnvMapAuroraVertexShader(ctx, "/sky_env_map_aurora_vertex")
    , skyEnvMapAuroraGeometryShader(ctx, "/sky_env_map_aurora_geometry")
    , skyEnvMapAuroraFragmentShader(ctx, "/sky_env_map_aurora_fragment")
    , skyEnvMapCometVertexShader(ctx, "/sky_env_map_comet_vertex")
    , skyEnvMapCometGeometryShader(ctx, "/sky_env_map_comet_geometry")
    , skyEnvMapCometFragmentShader(ctx, "/sky_env_map_comet_fragment")
    , skyEnvMapVortexVertexShader(ctx, "/sky_env_map_vortex_vertex")
    , skyEnvMapVortexGeometryShader(ctx, "/sky_env_map_vortex_geometry")
    , skyEnvMapVortexFragmentShader(ctx, "/sky_env_map_vortex_fragment")
    , atmosphereLightningVertexShader(ctx, "/atmosphere_lightning_vertex")
    , atmosphereLightningFragmentShader(ctx, "/atmosphere_lightning_fragment")
    , atmosphereLineVertexShader(ctx, "/atmosphere_line_vertex")
    , atmosphereLineGeometryShader(ctx, "/atmosphere_line_geometry")
    , atmosphereLineFragmentShader(ctx, selectRayTracing("/atmosphere_line_fragment"))
    , atmosphereSolidVertexShader(ctx, "/atmosphere_solid_vertex")
    , atmosphereSolidGeometryShader(ctx, "/atmosphere_solid_geometry")
    , atmosphereSolidFragmentShader(ctx, selectRayTracing("/atmosphere_solid_fragment"))
    , atmosphereCloudVertexShader(ctx, "/atmosphere_cloud_vertex")
    , atmosphereCloudGeometryShader(ctx, "/atmosphere_cloud_geometry")
    , atmosphereCloudFragmentShader(ctx, selectRayTracing("/atmosphere_cloud_fragment"))
    , atmosphereStreamerVertexShader(ctx, "/atmosphere_streamer_vertex")
    , atmosphereStreamerGeometryShader(ctx, "/atmosphere_streamer_geometry")
    , atmosphereStreamerFragmentShader(ctx, selectRayTracing("/atmosphere_streamer_fragment"))
    , atmosphereWarpVertexShader(ctx, "/atmosphere_warp_vertex")
    , atmosphereWarpGeometryShader(ctx, "/atmosphere_warp_geometry")
    , atmosphereWarpFragmentShader(ctx, selectRayTracing("/atmosphere_warp_fragment"))

    , panel(ctx, STORED_PIPELINE(createTextGraphicsPipeline,
        panelDescriptorSetLayout,
        overlayRenderPass,
        0,
        panelVertexShader,
        panelFragmentShader,
        sampleCount
    ))
    , vertexColor(ctx, STORED_PIPELINE(createVertexColorGraphicsPipeline,
        objectDescriptorSetLayout,
        mainRenderPass,
        0,
        coloredVertexShader,
        coloredFragmentShader,
        sampleCount
    ))
    , litVertexColor(ctx, STORED_PIPELINE(createLitVertexColorGraphicsPipeline,
        litObjectDescriptorSetLayout,
        mainRenderPass,
        0,
        litColoredVertexShader,
        litColoredFragmentShader,
        sampleCount
    ))
    , texturedColor(ctx, STORED_PIPELINE(createTexturedColorGraphicsPipeline,
        texturedObjectDescriptorSetLayout,
        mainRenderPass,
        0,
        texturedVertexShader,
        texturedFragmentShader,
        sampleCount,
        false
    ))
    , litTexturedColor(ctx, STORED_PIPELINE(createLitTexturedColorGraphicsPipeline,
        litTexturedObjectDescriptorSetLayout,
        mainRenderPass,
        0,
        litTexturedVertexShader,
        litTexturedFragmentShader,
        sampleCount,
        false
    ))
    , area(ctx, STORED_PIPELINE(createAreaGraphicsPipeline,
        areaDescriptorSetLayout,
        mainRenderPass,
        0,
        areaVertexShader,
        areaFragmentShader,
        sampleCount
    ))
    , litArea(ctx, STORED_PIPELINE(createLitAreaGraphicsPipeline,
        litAreaDescriptorSetLayout,
        mainRenderPass,
        0,
        litAreaVertexShader,
        litAreaFragmentShader,
        sampleCount
    ))
    , terrain(ctx, STORED_PIPELINE(createLitVertexColorGraphicsPipeline,
        objectDescriptorSetLayout,
        mainRenderPass,
        0,
        terrainVertexShader,
        terrainFragmentShader,
        sampleCount
    ))
    , litTerrain(ctx, STORED_PIPELINE(createLitVertexColorGraphicsPipeline,
        litObjectDescriptorSetLayout,
        mainRenderPass,
        0,
        litTerrainVertexShader,
        litTerrainFragmentShader,
        sampleCount
    ))
    , rope(ctx, STORED_PIPELINE(createRopeGraphicsPipeline,
        materialedObjectDescriptorSetLayout,
        mainRenderPass,
        0,
        ropeVertexShader,
        ropeGeometryShader,
        ropeFragmentShader,
        sampleCount
    ))
    , litRope(ctx, STORED_PIPELINE(createLitRopeGraphicsPipeline,
        litMaterialedObjectDescriptorSetLayout,
        mainRenderPass,
        0,
        litRopeVertexShader,
        litRopeGeometryShader,
        litRopeFragmentShader,
        sampleCount
    ))
    , orthographicGrid(ctx, STORED_PIPELINE(createGridGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        orthographicGridVertexShader,
        orthographicGridFragmentShader,
        sampleCount
    ))
    , coloredLine(ctx, STORED_PIPELINE(createColoredLineGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        coloredOverlayVertexShader,
        coloredOverlayFragmentShader,
        sampleCount
    ))
    , gizmo(ctx, STORED_PIPELINE(createGizmoGraphicsPipeline,
        objectDescriptorSetLayout,
        independentDepthOverlayRenderPass,
        0,
        coloredOverlayVertexShader,
        coloredOverlayFragmentShader,
        sampleCount
    ))
    , tetherIndicator(ctx, STORED_PIPELINE(createTetherIndicatorGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        tetherIndicatorVertexShader,
        tetherIndicatorFragmentShader,
        sampleCount
    ))
    , confinementIndicator(ctx, STORED_PIPELINE(createConfinementIndicatorGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        confinementIndicatorVertexShader,
        confinementIndicatorFragmentShader,
        sampleCount
    ))
    , waitingIndicator(ctx, STORED_PIPELINE(createWaitingIndicatorGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        waitingIndicatorVertexShader,
        waitingIndicatorFragmentShader,
        sampleCount
    ))
    , perspectiveGrid(ctx, STORED_PIPELINE(createGridGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        coloredOverlayVertexShader,
        coloredOverlayFragmentShader,
        sampleCount
    ))
    , themeGradient(ctx, STORED_PIPELINE(createThemeGradientGraphicsPipeline,
        objectDescriptorSetLayout,
        overlayRenderPass,
        0,
        themeGradientVertexShader,
        themeGradientFragmentShader,
        sampleCount
    ))
    , hdr(ctx, STORED_PIPELINE(createHDRGraphicsPipeline,
        hdrDescriptorSetLayout,
        hdrRenderPass,
        0,
        screenShader,
        hdrShader,
        sampleCount
    ))
    , fade(ctx, STORED_PIPELINE(createPostProcessGraphicsPipeline,
        postProcessDescriptorSetLayout,
        postProcessRenderPass,
        0,
        screenShader,
        fadeShader,
        sampleCount
    ))
    , motionBlur(ctx, STORED_PIPELINE(createPostProcessGraphicsPipeline,
        motionBlurDescriptorSetLayout,
        postProcessRenderPass,
        0,
        screenShader,
        motionBlurShader,
        sampleCount
    ))
    , chromaticAberration(ctx, STORED_PIPELINE(createPostProcessGraphicsPipeline,
        postProcessDescriptorSetLayout,
        postProcessRenderPass,
        0,
        screenShader,
        chromaticAberrationShader,
        sampleCount
    ))
    , depthOfField(ctx, STORED_PIPELINE(createPostProcessGraphicsPipeline,
        depthOfFieldDescriptorSetLayout,
        postProcessRenderPass,
        0,
        screenShader,
        depthOfFieldShader,
        sampleCount
    ))
    , tint(ctx, STORED_PIPELINE(createPostProcessGraphicsPipeline,
        postProcessDescriptorSetLayout,
        postProcessRenderPass,
        0,
        screenShader,
        tintShader,
        sampleCount
    ))
    , sky(ctx, STORED_PIPELINE(createSkyGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyVertexShader,
        skyFragmentShader,
        sampleCount
    ))
    , skyStars(ctx, STORED_PIPELINE(createSkyBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyBillboardVertexShader,
        skyBillboardGeometryShader,
        skyStarsFragmentShader,
        sampleCount
    ))
    , skyCircle(ctx, STORED_PIPELINE(createSkyGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyVertexShader,
        skyCircleFragmentShader,
        sampleCount
    ))
    , skyCloud(ctx, STORED_PIPELINE(createSkyBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyBillboardVertexShader,
        skyBillboardGeometryShader,
        skyCloudFragmentShader,
        sampleCount
    ))
    , skyAurora(ctx, STORED_PIPELINE(createSkyAuroraGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyAuroraVertexShader,
        skyAuroraGeometryShader,
        skyAuroraFragmentShader,
        sampleCount
    ))
    , skyComet(ctx, STORED_PIPELINE(createSkyBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyCometVertexShader,
        skyCometGeometryShader,
        skyCometFragmentShader,
        sampleCount
    ))
    , skyVortex(ctx, STORED_PIPELINE(createSkyBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        mainRenderPass,
        0,
        skyVortexVertexShader,
        skyVortexGeometryShader,
        skyVortexFragmentShader,
        sampleCount
    ))
    , skyEnvMap(ctx, STORED_PIPELINE(createSkyEnvMapGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapVertexShader,
        skyEnvMapFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , skyEnvMapStars(ctx, STORED_PIPELINE(createSkyEnvMapBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapBillboardVertexShader,
        skyEnvMapBillboardGeometryShader,
        skyEnvMapStarsFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , skyEnvMapCircle(ctx, STORED_PIPELINE(createSkyEnvMapGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapVertexShader,
        skyEnvMapCircleFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , skyEnvMapCloud(ctx, STORED_PIPELINE(createSkyEnvMapBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapBillboardVertexShader,
        skyEnvMapBillboardGeometryShader,
        skyEnvMapCloudFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , skyEnvMapAurora(ctx, STORED_PIPELINE(createSkyEnvMapBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapAuroraVertexShader,
        skyEnvMapAuroraGeometryShader,
        skyEnvMapAuroraFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , skyEnvMapComet(ctx, STORED_PIPELINE(createSkyEnvMapBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapCometVertexShader,
        skyEnvMapCometGeometryShader,
        skyEnvMapCometFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , skyEnvMapVortex(ctx, STORED_PIPELINE(createSkyEnvMapBillboardGraphicsPipeline,
        skyDescriptorSetLayout,
        skyRenderPass,
        0,
        skyEnvMapVortexVertexShader,
        skyEnvMapVortexGeometryShader,
        skyEnvMapVortexFragmentShader,
        VK_SAMPLE_COUNT_1_BIT
    ))
    , atmosphereLightning(ctx, STORED_PIPELINE(createAtmosphereLightningGraphicsPipeline,
        atmosphereDescriptorSetLayout,
        mainRenderPass,
        0,
        atmosphereLightningVertexShader,
        atmosphereLightningFragmentShader,
        sampleCount
    ))
    , atmosphereLine(ctx, STORED_PIPELINE(createAtmosphereEffectGraphicsPipeline,
        atmosphereDescriptorSetLayout,
        mainRenderPass,
        0,
        atmosphereLineVertexShader,
        atmosphereLineGeometryShader,
        atmosphereLineFragmentShader,
        sampleCount
    ))
    , atmosphereSolid(ctx, STORED_PIPELINE(createAtmosphereEffectGraphicsPipeline,
        atmosphereDescriptorSetLayout,
        mainRenderPass,
        0,
        atmosphereSolidVertexShader,
        atmosphereSolidGeometryShader,
        atmosphereSolidFragmentShader,
        sampleCount
    ))
    , atmosphereCloud(ctx, STORED_PIPELINE(createAtmosphereEffectGraphicsPipeline,
        atmosphereDescriptorSetLayout,
        mainRenderPass,
        0,
        atmosphereCloudVertexShader,
        atmosphereCloudGeometryShader,
        atmosphereCloudFragmentShader,
        sampleCount
    ))
    , atmosphereStreamer(ctx, STORED_PIPELINE(createAtmosphereEffectGraphicsPipeline,
        atmosphereDescriptorSetLayout,
        mainRenderPass,
        0,
        atmosphereStreamerVertexShader,
        atmosphereStreamerGeometryShader,
        atmosphereStreamerFragmentShader,
        sampleCount
    ))
    , atmosphereWarp(ctx, STORED_PIPELINE(createAtmosphereEffectGraphicsPipeline,
        atmosphereDescriptorSetLayout,
        mainRenderPass,
        0,
        atmosphereWarpVertexShader,
        atmosphereWarpGeometryShader,
        atmosphereWarpFragmentShader,
        sampleCount
    ))
{

}

RenderFlatPipelineStore::~RenderFlatPipelineStore()
{

}

RenderEntityPipelines RenderFlatPipelineStore::get(BodyRenderMaterialType type) const
{
    switch (type)
    {
    default :
        fatal("Encountered unknown body render material type '" + to_string(type) + "'.");
    case Body_Render_Material_Vertex :
        return { litObjectDescriptorSetLayout, litVertexColor.pipelineLayout, litVertexColor.pipeline };
    case Body_Render_Material_Texture :
        return { litTexturedObjectDescriptorSetLayout, litTexturedColor.pipelineLayout, litTexturedColor.pipeline };
    case Body_Render_Material_Area :
        return { litAreaDescriptorSetLayout, litArea.pipelineLayout, litArea.pipeline };
    case Body_Render_Material_Terrain :
        return { litObjectDescriptorSetLayout, litTerrain.pipelineLayout, litTerrain.pipeline };
    case Body_Render_Material_Rope :
        return { litMaterialedObjectDescriptorSetLayout, litRope.pipelineLayout, litRope.pipeline };
    case Body_Render_Material_Shadeless_Vertex :
        return { objectDescriptorSetLayout, vertexColor.pipelineLayout, vertexColor.pipeline };
    case Body_Render_Material_Shadeless_Texture :
        return { texturedObjectDescriptorSetLayout, texturedColor.pipelineLayout, texturedColor.pipeline };
    case Body_Render_Material_Shadeless_Area :
        return { areaDescriptorSetLayout, area.pipelineLayout, area.pipeline };
    case Body_Render_Material_Shadeless_Terrain :
        return { objectDescriptorSetLayout, terrain.pipelineLayout, terrain.pipeline };
    case Body_Render_Material_Shadeless_Rope :
        return { materialedObjectDescriptorSetLayout, rope.pipelineLayout, rope.pipeline };
    }
}
