/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_context.hpp"
#include "body_render_data.hpp"
#include "render_entity_body_texture.hpp"
#include "render_pipeline_store.hpp"

// This is a container for material-specific entity rendering resources that are frame independent*, mesh data and textures
// It also stores references to descriptor set layouts, pipeline layouts and pipelines, but does not create or destroy them
// * = for details on what that means see RenderEntityBody, which contains several of these
class RenderEntityBodyMaterial
{
public:
    RenderEntityBodyMaterial(
        const RenderContext* ctx,
        const RenderPipelineStore* pipelineStore,
        BodyRenderMaterialType type,
        const BodyRenderMaterialData& material
    );
    RenderEntityBodyMaterial(const RenderEntityBodyMaterial& rhs) = delete;
    RenderEntityBodyMaterial(RenderEntityBodyMaterial&& rhs) = delete;
    ~RenderEntityBodyMaterial();

    RenderEntityBodyMaterial& operator=(const RenderEntityBodyMaterial& rhs) = delete;
    RenderEntityBodyMaterial& operator=(RenderEntityBodyMaterial&& rhs) = delete;

    bool supports(const BodyRenderMaterialData& material) const;
    void fill(const BodyRenderMaterialData& material);
    void setupTextureLayouts(VkCommandBuffer commandBuffer);

    bool shouldDraw() const;

    BodyRenderMaterialType type;
    // References taken from pipeline store
    RenderEntityPipelines pipelines;

    // Owned data
    VmaBuffer indices;
    VmaMapping<u32>* indicesMapping;
    size_t indexCount;
    VmaBuffer vertices;
    VmaMapping<f32>* verticesMapping;
    size_t vertexCount;
    VmaBuffer uniform;
    VmaMapping<f32>* uniformMapping;
    size_t uniformSize;
    vector<RenderEntityBodyTexture*> textures;
    // For transparent sorting, ignored on non-transparents
    vec3 position;

private:
    const RenderContext* ctx;
};
