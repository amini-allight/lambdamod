/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr.hpp"
#include "vr_tools.hpp"
#include "tools.hpp"

static bool addOptionalExtension(
    const vector<XrExtensionProperties>& instanceExtensions,
    const string& name,
    set<string>& extensionNames
)
{
    if (find_if(
        instanceExtensions.begin(),
        instanceExtensions.end(),
        [&](const XrExtensionProperties& properties) -> bool
        {
            return name == properties.extensionName;
        }
    ) != instanceExtensions.end())
    {
        extensionNames.insert(name);
        return true;
    }
    else
    {
        return false;
    }
}

static u32 addOptionalExtensionRevision(
    const vector<XrExtensionProperties>& instanceExtensions,
    const string& name,
    set<string>& extensionNames
)
{
    u32 revision = 0;

    if (find_if(
        instanceExtensions.begin(),
        instanceExtensions.end(),
        [&](const XrExtensionProperties& properties) -> bool
        {
            if (name == properties.extensionName)
            {
                revision = properties.extensionVersion;
                return true;
            }
            else
            {
                return false;
            }
        }
    ) != instanceExtensions.end())
    {
        extensionNames.insert(name);
        return revision;
    }
    else
    {
        return revision;
    }
}

VR::VR(Controller* controller)
    : VRInterface()
    , controller(controller)
    , _running(false)
    , exiting(false)
    , _userPresence(false)
    , _eyeTracking(false)
    , _handTracking(false)
    , _htcViveCosmos(false)
    , _htcViveFocus3(false)
    , _htcFaceTracking(false)
    , _htcHandTracking(false)
    , _htcBodyTracking(false)
    , _htcFullBodyTracking(0)
    , _microsoftHandTracking(false)
    , _hpMixedReality(false)
    , _huawei(false)
    , _magicLeap2(false)
    , _pico(0)
    , _questTouchPro(false)
    , _questTouchPlus(false)
    , _oppo(false)
    , _yvr(false)
    , _varjoXR4(false)
    , _metaFaceTracking(0)
    , _metaBodyTracking(false)
    , _bdBodyTracking(false)
    , _monado(false)
    , _steamvr(false)
    , userPresenceAvailable(false)
    , handTrackingAvailable(false)
    , htcFacialEyeTrackingAvailable(false)
    , htcFacialLipTrackingAvailable(false)
    , htcBodyTrackingAvailable(false)
    , metaFaceTrackingAvailable(false)
    , metaBodyTrackingAvailable(false)
    , bdBodyTrackingAvailable(false)
    , userPresent(false)
{
    log("Initializing VR...");

    XrResult result;

    u32 instanceExtensionCount;

    result = xrEnumerateInstanceExtensionProperties(nullptr, 0, &instanceExtensionCount, nullptr);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate instance extension properties: " + openxrError(result));
    }

    vector<XrExtensionProperties> instanceExtensions(
        instanceExtensionCount,
        { .type = XR_TYPE_EXTENSION_PROPERTIES }
    );

    result = xrEnumerateInstanceExtensionProperties(nullptr, instanceExtensionCount, &instanceExtensionCount, instanceExtensions.data());

    if (result != XR_SUCCESS)
    {
        fatal("Failed to enumerate instance extension properties: " + openxrError(result));
    }

    set<string> extensionNames = {
        "XR_KHR_vulkan_enable",
        "XR_KHR_vulkan_enable2"
    };

#ifdef LMOD_XR_VALIDATION
    extensionNames.insert("XR_EXT_debug_utils");
#endif

    _userPresence = addOptionalExtension(instanceExtensions, "XR_EXT_user_presence", extensionNames);
    _eyeTracking = addOptionalExtension(instanceExtensions, "XR_EXT_eye_gaze_interaction", extensionNames);
    _handTracking = addOptionalExtension(instanceExtensions, "XR_EXT_hand_tracking", extensionNames);
    _htcViveCosmos = addOptionalExtension(instanceExtensions, "XR_HTC_vive_cosmos_controller_interaction", extensionNames);
    _htcViveFocus3 = addOptionalExtension(instanceExtensions, "XR_HTC_vive_focus3_controller_interaction", extensionNames);
    _htcFaceTracking = addOptionalExtension(instanceExtensions, "XR_HTC_facial_tracking", extensionNames);
    _htcHandTracking = addOptionalExtension(instanceExtensions, "XR_HTC_hand_interaction", extensionNames);
    _htcBodyTracking = addOptionalExtension(instanceExtensions, "XR_HTC_body_tracking", extensionNames);
    _htcFullBodyTracking = addOptionalExtensionRevision(instanceExtensions, "XR_HTCX_vive_tracker_interaction", extensionNames);
    _microsoftHandTracking = addOptionalExtension(instanceExtensions, "XR_MSFT_hand_interaction", extensionNames);
    _hpMixedReality = addOptionalExtension(instanceExtensions, "XR_EXT_hp_mixed_reality_controller", extensionNames);
    _huawei = addOptionalExtension(instanceExtensions, "XR_HUAWEI_controller_interaction", extensionNames);
    _magicLeap2 = addOptionalExtension(instanceExtensions, "XR_ML_ml2_controller_interaction", extensionNames);
    _pico = addOptionalExtensionRevision(instanceExtensions, "XR_BD_controller_interaction", extensionNames);
    _questTouchPro = addOptionalExtension(instanceExtensions, "XR_FB_touch_controller_pro", extensionNames);
    _questTouchPlus = addOptionalExtension(instanceExtensions, "XR_META_touch_controller_plus", extensionNames);
    _oppo = addOptionalExtension(instanceExtensions, "XR_OPPO_controller_interaction", extensionNames);
    _yvr = addOptionalExtension(instanceExtensions, "XR_YVR_controller_interaction", extensionNames);
    _varjoXR4 = addOptionalExtension(instanceExtensions, "XR_VARJO_xr4_controller_interaction", extensionNames);

    if (addOptionalExtension(instanceExtensions, "XR_FB_face_tracking", extensionNames))
    {
        _metaFaceTracking = 1;
    }

    if (addOptionalExtension(instanceExtensions, "XR_FB_face_tracking2", extensionNames))
    {
        _metaFaceTracking = 2;
    }

    _metaBodyTracking = addOptionalExtension(instanceExtensions, "XR_FB_body_tracking", extensionNames);

    _bdBodyTracking = addOptionalExtension(instanceExtensions, "XR_BD_body_tracking", extensionNames);

    log("Using OpenXR instance extensions: " + join(extensionNames, ", "));

    vector<const char*> rawExtensionNames = toRawNames(extensionNames);

    // Using Vulkan version macro here because the OpenXR one produces 64 bit integers while the fields here are 32 bit
    XrInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.type = XR_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.createFlags = 0;
    fillBuffer<XR_MAX_APPLICATION_NAME_SIZE>(instanceCreateInfo.applicationInfo.applicationName, gameName);
    instanceCreateInfo.applicationInfo.applicationVersion = VK_MAKE_API_VERSION(version.major, version.minor, version.patch, 0);
    fillBuffer<XR_MAX_ENGINE_NAME_SIZE>(instanceCreateInfo.applicationInfo.engineName, gameName);
    instanceCreateInfo.applicationInfo.engineVersion = VK_MAKE_API_VERSION(version.major, version.minor, version.patch, 0);
    // Currently limited to OpenXR version 1.0 because SteamVR won't initialize otherwise
    instanceCreateInfo.applicationInfo.apiVersion = XR_API_VERSION_1_0;
#ifdef LMOD_XR_VALIDATION
    const char* layerNames[] = { "XR_APILAYER_LUNARG_core_validation" };
    instanceCreateInfo.enabledApiLayerCount = 1;
    instanceCreateInfo.enabledApiLayerNames = layerNames;
#else
    instanceCreateInfo.enabledApiLayerCount = 0;
    instanceCreateInfo.enabledApiLayerNames = nullptr;
#endif
    instanceCreateInfo.enabledExtensionCount = rawExtensionNames.size();
    instanceCreateInfo.enabledExtensionNames = rawExtensionNames.data();

    result = xrCreateInstance(&instanceCreateInfo, &_instance);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create OpenXR instance: " + openxrError(result));
    }

#ifdef LMOD_XR_VALIDATION
    XrDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
    debugMessengerCreateInfo.type = XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugMessengerCreateInfo.messageSeverities = XR_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageTypes = XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.userCallback = handleError;
    debugMessengerCreateInfo.userData = nullptr;

    result = GET_XR_EXTENSION_FUNCTION(_instance, xrCreateDebugUtilsMessengerEXT)(_instance, &debugMessengerCreateInfo, &debugMessenger);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create OpenXR debug messenger: " + openxrError(result));
    }
#endif

    XrInstanceProperties instanceProperties{};
    instanceProperties.type = XR_TYPE_INSTANCE_PROPERTIES;

    result = xrGetInstanceProperties(_instance, &instanceProperties);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to get OpenXR instance properties: " + openxrError(result));
    }

    log("OpenXR runtime: " + string(instanceProperties.runtimeName) + " " +
        to_string(XR_VERSION_MAJOR(instanceProperties.runtimeVersion)) + "." +
        to_string(XR_VERSION_MINOR(instanceProperties.runtimeVersion)) + "." +
        to_string(XR_VERSION_PATCH(instanceProperties.runtimeVersion))
    );

    XrSystemGetInfo systemGetInfo{};
    systemGetInfo.type = XR_TYPE_SYSTEM_GET_INFO;
    systemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

    result = xrGetSystem(_instance, &systemGetInfo, &_systemID);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to get OpenXR system: " + openxrError(result));
    }

    XrSystemProperties systemProperties{};
    systemProperties.type = XR_TYPE_SYSTEM_PROPERTIES;

    void** next = &systemProperties.next;

#if defined(XR_EXT_user_presence)
    XrSystemUserPresencePropertiesEXT userPresenceProperties{};
    userPresenceProperties.type = XR_TYPE_SYSTEM_USER_PRESENCE_PROPERTIES_EXT;

    if (_userPresence)
    {
        *next = &userPresenceProperties;
        next = &userPresenceProperties.next;
    }
#endif

#if defined(XR_EXT_hand_tracking)
    XrSystemHandTrackingPropertiesEXT handTrackingProperties{};
    handTrackingProperties.type = XR_TYPE_SYSTEM_HAND_TRACKING_PROPERTIES_EXT;

    if (_handTracking)
    {
        *next = &handTrackingProperties;
        next = &handTrackingProperties.next;
    }
#endif

#if defined(XR_HTC_facial_tracking)
    XrSystemFacialTrackingPropertiesHTC htcFaceTrackingProperties{};
    htcFaceTrackingProperties.type = XR_TYPE_SYSTEM_FACIAL_TRACKING_PROPERTIES_HTC;

    if (_htcFaceTracking)
    {
        *next = &htcFaceTrackingProperties;
        next = &htcFaceTrackingProperties.next;
    }
#endif

#if defined(XR_HTC_body_tracking)
    XrSystemBodyTrackingPropertiesHTC htcBodyTrackingProperties{};
    htcBodyTrackingProperties.type = XR_TYPE_SYSTEM_BODY_TRACKING_PROPERTIES_HTC;

    if (_htcBodyTracking)
    {
        *next = &htcBodyTrackingProperties;
        next = &htcBodyTrackingProperties.next;
    }
#endif

#if defined(XR_FB_face_tracking)
    XrSystemFaceTrackingPropertiesFB metaFaceTrackingProperties{};
    metaFaceTrackingProperties.type = XR_TYPE_SYSTEM_FACE_TRACKING_PROPERTIES_FB;

    if (_metaFaceTracking == 1)
    {
        *next = &metaFaceTrackingProperties;
        next = &metaFaceTrackingProperties.next;
    }
#endif

#if defined(XR_FB_face_tracking2)
    XrSystemFaceTrackingProperties2FB metaFaceTrackingProperties2{};
    metaFaceTrackingProperties2.type = XR_TYPE_SYSTEM_FACE_TRACKING_PROPERTIES2_FB;

    if (_metaFaceTracking == 2)
    {
        *next = &metaFaceTrackingProperties2;
        next = &metaFaceTrackingProperties2.next;
    }
#endif

#if defined(XR_FB_body_tracking)
    XrSystemBodyTrackingPropertiesFB metaBodyTrackingProperties{};
    metaBodyTrackingProperties.type = XR_TYPE_SYSTEM_BODY_TRACKING_PROPERTIES_FB;

    if (_metaBodyTracking)
    {
        *next = &metaBodyTrackingProperties;
        next = &metaBodyTrackingProperties.next;
    }
#endif

#if defined(XR_BD_body_tracking)
    XrSystemBodyTrackingPropertiesBD bdBodyTrackingProperties{};
    bdBodyTrackingProperties.type = XR_TYPE_SYSTEM_BODY_TRACKING_PROPERTIES_BD;

    if (_bdBodyTracking)
    {
        *next = &bdBodyTrackingProperties;
        next = &bdBodyTrackingProperties.next;
    }
#endif

    result = xrGetSystemProperties(_instance, _systemID, &systemProperties);

    if (result != XR_SUCCESS)
    {
        error("Failed to get OpenXR system properties: " + openxrError(result));
    }

#if defined(XR_EXT_user_presence)
    userPresenceAvailable = userPresenceProperties.supportsUserPresence;
#endif
#if defined(XR_EXT_hand_tracking)
    handTrackingAvailable = handTrackingProperties.supportsHandTracking;
#endif
#if defined(XR_HTC_facial_tracking)
    htcFacialEyeTrackingAvailable = htcFaceTrackingProperties.supportEyeFacialTracking;
    htcFacialLipTrackingAvailable = htcFaceTrackingProperties.supportLipFacialTracking;
#endif
#if defined(XR_FB_face_tracking)
    metaFaceTrackingAvailable |= metaFaceTrackingProperties.supportsFaceTracking;
#endif
#if defined(XR_FB_face_tracking2)
    metaFaceTrackingAvailable |= metaFaceTrackingProperties2.supportsVisualFaceTracking || metaFaceTrackingProperties2.supportsAudioFaceTracking;
#endif
#if defined(XR_FB_body_tracking)
    metaBodyTrackingAvailable = metaBodyTrackingProperties.supportsBodyTracking;
#endif
#if defined(XR_BD_body_tracking)
    bdBodyTrackingAvailable = bdBodyTrackingProperties.supportsBodyTracking;
#endif

    log("OpenXR system: " + string(systemProperties.systemName));
    log("Max resolution: " + to_string(systemProperties.graphicsProperties.maxSwapchainImageWidth) + " × " + to_string(systemProperties.graphicsProperties.maxSwapchainImageHeight));
    log("Max layers: " + to_string(systemProperties.graphicsProperties.maxLayerCount));
    log("Orientation tracking: " + string(systemProperties.trackingProperties.orientationTracking ? "yes" : "no"));
    log("Position tracking: " + string(systemProperties.trackingProperties.positionTracking ? "yes" : "no"));

#if defined(XR_EXT_hand_tracking)
    if (_handTracking)
    {
        log("Hand tracking: " + string(handTrackingProperties.supportsHandTracking ? "yes" : "no"));
    }
#endif

#if defined(XR_HTC_facial_tracking)
    if (_htcFaceTracking)
    {
        log("Facial eye tracking (HTC): " + string(htcFaceTrackingProperties.supportEyeFacialTracking ? "yes" : "no"));
        log("Facial lip tracking (HTC): " + string(htcFaceTrackingProperties.supportLipFacialTracking ? "yes" : "no"));
    }
#endif

#if defined(XR_HTC_body_tracking)
    if (_htcBodyTracking)
    {
        log("Body tracking (HTC): " + string(htcBodyTrackingProperties.supportsBodyTracking ? "yes" : "no"));
    }
#endif

#if defined(XR_FB_face_tracking)
    if (_metaFaceTracking == 1)
    {
        log("Face tracking (Meta): " + string(metaFaceTrackingProperties.supportsFaceTracking ? "yes" : "no"));
    }
#endif

#if defined(XR_FB_face_tracking2)
    if (_metaFaceTracking == 2)
    {
        log("Visual face tracking (Meta): " + string(metaFaceTrackingProperties2.supportsVisualFaceTracking ? "yes" : "no"));
        log("Audio face tracking (Meta): " + string(metaFaceTrackingProperties2.supportsAudioFaceTracking ? "yes" : "no"));
    }
#endif

#if defined(XR_FB_body_tracking)
    if (_metaBodyTracking)
    {
        log("Body tracking (Meta): " + string(metaBodyTrackingProperties.supportsBodyTracking ? "yes" : "no"));
    }
#endif

#if defined(XR_BD_body_tracking)
    if (_metaBodyTracking)
    {
        log("Body tracking (ByteDance): " + string(bdBodyTrackingProperties.supportsBodyTracking ? "yes" : "no"));
    }
#endif

    // Identify SteamVR to apply special SteamVR specific hacks
    if (string(instanceProperties.runtimeName).find("Monado") != string::npos)
    {
        log("Monado detected, applying Monado-specific hacks.");
        _monado = true;
    }
    // Identify SteamVR to apply special SteamVR specific hacks
    else if (string(instanceProperties.runtimeName).find("SteamVR/OpenXR") != string::npos)
    {
        log("SteamVR detected, applying SteamVR-specific hacks.");
        _steamvr = true;
    }

    log("VR initialized.");
}

VR::~VR()
{
    log("Shutting down VR...");

#ifdef LMOD_XR_VALIDATION
    GET_XR_EXTENSION_FUNCTION(_instance, xrDestroyDebugUtilsMessengerEXT)(debugMessenger);
#endif

    xrDestroyInstance(_instance);

    log("VR shut down.");
}

void VR::requestExit()
{
    if (exiting)
    {
        return;
    }

    XrResult result = xrRequestExitSession(_session);

    if (result != XR_SUCCESS)
    {
        error("Failed to request exit: " + openxrError(result));
    }

    exiting = true;
}

bool VR::step()
{
    XrResult result;

    bool quit = false;

    while (true)
    {
        XrEventDataBuffer eventData{};
        eventData.type = XR_TYPE_EVENT_DATA_BUFFER;

        result = xrPollEvent(_instance, &eventData);

        if (result == XR_EVENT_UNAVAILABLE)
        {
            break;
        }
        else if (result != XR_SUCCESS)
        {
            error("Failed to poll for OpenXR events: " + openxrError(result));
            break;
        }

        switch (eventData.type)
        {
        default :
            warning("Encountered unknown OpenXR event received.");
            break;
        case XR_TYPE_EVENT_DATA_INSTANCE_LOSS_PENDING :
            warning("OpenXR instance loss pending, shutting down.");
            quit = true;
            break;
        case XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED :
        {
            switch (reinterpret_cast<const XrEventDataSessionStateChanged*>(&eventData)->state)
            {
            default :
            case XR_SESSION_STATE_UNKNOWN :
            case XR_SESSION_STATE_MAX_ENUM :
                warning("OpenXR session state changed to unknown state.");
                break;
            case XR_SESSION_STATE_IDLE :
                _running = false;
                break;
            case XR_SESSION_STATE_READY :
            {
                XrSessionBeginInfo sessionBeginInfo{};
                sessionBeginInfo.type = XR_TYPE_SESSION_BEGIN_INFO;
                sessionBeginInfo.primaryViewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;

                result = xrBeginSession(_session, &sessionBeginInfo);

                if (result != XR_SUCCESS)
                {
                    error("Failed to begin OpenXR session: " + openxrError(result));
                }

                _running = true;
                break;
            }
            case XR_SESSION_STATE_SYNCHRONIZED :
            case XR_SESSION_STATE_VISIBLE :
            case XR_SESSION_STATE_FOCUSED :
                _running = true;
                break;
            case XR_SESSION_STATE_STOPPING :
                result = xrEndSession(_session);

                if (result != XR_SUCCESS)
                {
                    error("Failed to end OpenXR session: " + openxrError(result));
                }

                _running = false;
                break;
            case XR_SESSION_STATE_LOSS_PENDING :
                warning("OpenXR session loss pending, shutting down.");
                quit = true;
                break;
            case XR_SESSION_STATE_EXITING :
                warning("OpenXR session exiting, shutting down.");
                quit = true;
                break;
            }

            break;
        }
        case XR_TYPE_EVENT_DATA_REFERENCE_SPACE_CHANGE_PENDING :
            log("The OpenXR reference space is changing.");
            break;
        case XR_TYPE_EVENT_DATA_EVENTS_LOST :
            error("OpenXR event queue overflowed and " + to_string(reinterpret_cast<const XrEventDataEventsLost*>(&eventData)->lostEventCount) + " events were lost.");
            break;
        case XR_TYPE_EVENT_DATA_INTERACTION_PROFILE_CHANGED :
        {
            static constexpr const char* const handPathNames[] = {
                "/user/hand/left",
                "/user/hand/right"
            };

            for (size_t i = 0; i < handCount; i++)
            {
                XrPath handPath;

                result = xrStringToPath(_instance, handPathNames[i], &handPath);

                if (result != XR_SUCCESS)
                {
                    error("Failed to convert string to path: " + openxrError(result));
                    continue;
                }

                XrInteractionProfileState state{};
                state.type = XR_TYPE_INTERACTION_PROFILE_STATE;

                result = xrGetCurrentInteractionProfile(_session, handPath, &state);

                if (result != XR_SUCCESS)
                {
                    error("Failed to get current OpenXR interaction profile: " + openxrError(result));
                    continue;
                }

                char name[XR_MAX_PATH_LENGTH]{0};

                if (state.interactionProfile == XR_NULL_PATH)
                {
                    static constexpr const char* const null = "null";

                    memcpy(name, null, strlen(null));
                }
                else
                {
                    u32 size = XR_MAX_PATH_LENGTH;

                    result = xrPathToString(_instance, state.interactionProfile, size, &size, name);

                    if (result != XR_SUCCESS)
                    {
                        error("Failed to convert path to string: " + openxrError(result));
                        continue;
                    }
                }

                string newInteractionProfile = name;

                if (i == 0 && newInteractionProfile != leftInteractionProfile)
                {
                    log("The left hand OpenXR interaction profile is now: " + newInteractionProfile);
                    leftInteractionProfile = newInteractionProfile;
                }
                else if (i == 1 && newInteractionProfile != rightInteractionProfile)
                {
                    log("The right hand OpenXR interaction profile is now: " + newInteractionProfile);
                    rightInteractionProfile = newInteractionProfile;
                }
            }
            break;
        }
        case XR_TYPE_EVENT_DATA_VIVE_TRACKER_CONNECTED_HTCX :
        {
            char name[XR_MAX_PATH_LENGTH]{0};
            u32 size = XR_MAX_PATH_LENGTH;

            result = xrPathToString(_instance, reinterpret_cast<const XrEventDataViveTrackerConnectedHTCX*>(&eventData)->paths->rolePath, size, &size, name);

            if (result != XR_SUCCESS)
            {
                error("Failed to convert path to string: " + openxrError(result));
                break;
            }

            log("HTC Vive Tracker '" + string(name) + "' connected.");
            break;
        }
#if defined(XR_EXT_user_presence)
        case XR_TYPE_EVENT_DATA_USER_PRESENCE_CHANGED_EXT :
            userPresent = reinterpret_cast<const XrEventDataUserPresenceChangedEXT*>(&eventData)->isUserPresent;
            break;
#endif
        }
    }

    if (!quit)
    {
        XrFrameWaitInfo frameWaitInfo{};
        frameWaitInfo.type = XR_TYPE_FRAME_WAIT_INFO;

        XrFrameState frameState{};
        frameState.type = XR_TYPE_FRAME_STATE;

        result = xrWaitFrame(_session, &frameWaitInfo, &frameState);

        if (result != XR_SUCCESS)
        {
            error("Failed to wait for frame: " + openxrError(result));
        }

        nextFrameState = frameState;
    }

    return quit;
}

void VR::setRoom(const vec3& position, const quaternion& rotation)
{
    roomTransform = { position, rotation, vec3(1) };
}

void VR::resetRoom()
{
    roomTransform = mat4();
}

vec3 VR::roomToWorld(const vec3& v) const
{
    return roomTransform.applyToPosition(v);
}

quaternion VR::roomToWorld(const quaternion& q) const
{
    return roomTransform.applyToRotation(q);
}

vec3 VR::worldToRoom(const vec3& v) const
{
    return roomTransform.applyToPosition(v, false);
}

quaternion VR::worldToRoom(const quaternion& q) const
{
    return roomTransform.applyToRotation(q, false);
}

bool VR::running() const
{
    return _running && (!_userPresence || !userPresenceAvailable || userPresent);
}

bool VR::shouldRender() const
{
    return nextFrameState.shouldRender;
}

InputInterface* VR::input() const
{
    return _input;
}

void VR::initSession(
    VkInstance instance,
    VkPhysicalDevice physDevice,
    VkDevice device,
    u32 queueFamilyIndex
)
{
    XrResult result;

    XrGraphicsBindingVulkanKHR graphicsBinding{};
    graphicsBinding.type = XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR;
    graphicsBinding.instance = instance;
    graphicsBinding.physicalDevice = physDevice;
    graphicsBinding.device = device;
    graphicsBinding.queueFamilyIndex = queueFamilyIndex;
    graphicsBinding.queueIndex = 0;

    XrSessionCreateInfo sessionCreateInfo{};
    sessionCreateInfo.type = XR_TYPE_SESSION_CREATE_INFO;
    sessionCreateInfo.next = &graphicsBinding;
    sessionCreateInfo.createFlags = 0;
    sessionCreateInfo.systemId = _systemID;

    result = xrCreateSession(_instance, &sessionCreateInfo, &_session);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create OpenXR session: " + openxrError(result));
    }

    XrReferenceSpaceCreateInfo referenceSpaceCreateInfo{};
    referenceSpaceCreateInfo.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO;
    referenceSpaceCreateInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
    referenceSpaceCreateInfo.poseInReferenceSpace.position = { 0, 0, 0 };
    referenceSpaceCreateInfo.poseInReferenceSpace.orientation = { 0, 0, 0, 1 };

    result = xrCreateReferenceSpace(_session, &referenceSpaceCreateInfo, &_roomSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create reference space: " + openxrError(result));
    }

    _input = new VRInput(controller, this);

}

void VR::quitSession()
{
    delete _input;

    xrDestroySpace(_roomSpace);

    xrDestroySession(_session);
}

XrInstance VR::instance() const
{
    return _instance;
}

XrSystemId VR::systemID() const
{
    return _systemID;
}

XrSession VR::session() const
{
    return _session;
}

XrSpace VR::roomSpace() const
{
    return _roomSpace;
}

XrTime VR::predictedDisplayTime() const
{
    return nextFrameState.predictedDisplayTime;
}

bool VR::userPresence() const
{
    return _userPresence && userPresenceAvailable;
}

bool VR::eyeTracking() const
{
    return _eyeTracking;
}

bool VR::handTracking() const
{
    return _handTracking && handTrackingAvailable;
}

bool VR::htcFacialEyeTracking() const
{
    return _htcFaceTracking && htcFacialEyeTrackingAvailable;
}

bool VR::htcFacialLipTracking() const
{
    return _htcFaceTracking && htcFacialLipTrackingAvailable;
}

bool VR::htcBodyTracking() const
{
    return _htcBodyTracking && htcBodyTrackingAvailable;
}

u32 VR::htcFullBodyTracking() const
{
    return _htcFullBodyTracking;
}

bool VR::htcViveCosmos() const
{
    return _htcViveCosmos;
}

bool VR::htcViveFocus3() const
{
    return _htcViveFocus3;
}

bool VR::htcHandTracking() const
{
    return _htcHandTracking;
}

bool VR::microsoftHandTracking() const
{
    return _microsoftHandTracking;
}

bool VR::hpMixedReality() const
{
    return _hpMixedReality;
}

bool VR::huawei() const
{
    return _huawei;
}

bool VR::magicLeap2() const
{
    return _magicLeap2;
}

u32 VR::pico() const
{
    return _pico;
}

bool VR::questTouchPro() const
{
    return _questTouchPro;
}

bool VR::questTouchPlus() const
{
    return _questTouchPlus;
}

bool VR::oppo() const
{
    return _oppo;
}

bool VR::yvr() const
{
    return _yvr;
}

bool VR::varjoXR4() const
{
    return _varjoXR4;
}

u32 VR::metaFaceTracking() const
{
    return _metaFaceTracking && metaFaceTrackingAvailable;
}

bool VR::metaBodyTracking() const
{
    return _metaBodyTracking && metaBodyTrackingAvailable;
}

bool VR::bdBodyTracking() const
{
    return _bdBodyTracking && bdBodyTrackingAvailable;
}

bool VR::monado() const
{
    return _monado;
}

bool VR::steamvr() const
{
    return _steamvr;
}
#endif
