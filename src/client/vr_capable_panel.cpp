/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_capable_panel.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

VRCapablePanel::VRCapablePanel(InterfaceView* view)
    : InterfaceWidget(view)
{

}

bool VRCapablePanel::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    switch (type)
    {
    default :
        break;
    case Pointer_VR_Left :
        leftPointer = pointer;
        break;
    case Pointer_VR_Right :
        rightPointer = pointer;
        break;
    }

    return InterfaceWidget::movePointer(type, pointer, consumed);
}

void VRCapablePanel::clearPointer(PointerType type)
{
    switch (type)
    {
    default :
        break;
    case Pointer_VR_Left :
        leftPointer = optional<Point>();
        break;
    case Pointer_VR_Right :
        rightPointer = optional<Point>();
        break;
    }
}

optional<Point> VRCapablePanel::getPointer(PointerType type) const
{
    switch (type)
    {
    default :
        return {};
    case Pointer_VR_Left :
        return leftPointer;
    case Pointer_VR_Right :
        return rightPointer;
    }
}

bool VRCapablePanel::hasPointer(PointerType type) const
{
    switch (type)
    {
    default :
        return false;
    case Pointer_VR_Left :
        return leftPointer.has_value();
    case Pointer_VR_Right :
        return rightPointer.has_value();
    }
}
#endif
