/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "context_logic_editor.hpp"
#include "localization.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "script_tools.hpp"
#include "column.hpp"
#include "tab_view.hpp"

ContextLogicEditor::ContextLogicEditor(
    InterfaceWidget* parent,
    const function<Value()>& source,
    const function<void(const Value&)>& onSave
)
    : LogicEditor(parent)
    , source(source)
    , onSave(onSave)
{
    auto column = new Column(this);

    InterfaceWidget* textEditorContainer = column;

    if (g_config.nodeEditor)
    {
        auto tabs = new TabView(column, { localize("logic-editor-node"), localize("logic-editor-code") });

        nodeEditor = new NodeEditor(
            tabs->tab(localize("logic-editor-node")),
            bind(&ContextLogicEditor::nodeFormatSource, this),
            bind(&ContextLogicEditor::onNodeFormatSave, this, placeholders::_1)
        );
        nodeEditor->set(nodeFormatSource());

        textEditorContainer = tabs->tab(localize("logic-editor-code"));
    }

    codeEditor = new CodeEditor(
        textEditorContainer,
        bind(&ContextLogicEditor::textSource, this),
        bind(&ContextLogicEditor::onTextSave, this, placeholders::_1)
    );
    codeEditor->set(textSource());

    lastNodeEditorAssignment = source();
    lastCodeEditorAssignment = source();
}

void ContextLogicEditor::step()
{
    InterfaceWidget::step();

    if (nodeEditor && source() != lastNodeEditorAssignment)
    {
        nodeEditor->notifyReload();
        lastNodeEditorAssignment = source();
    }

    if (source() != lastCodeEditorAssignment)
    {
        codeEditor->notifyReload();
        lastCodeEditorAssignment = source();
    }
}

ScriptNodeFormat ContextLogicEditor::nodeFormatSource()
{
    return ScriptNodeFormat(context()->controller()->game().context(), vector<Value>({ source() }));
}

void ContextLogicEditor::onNodeFormatSave(const ScriptNodeFormat& format)
{
    optional<vector<Value>> results = runSafely(
        context()->controller()->game().context(),
        format.values(),
        "loading context value",
        this->context()->controller()
    );

    if (!results || results->empty())
    {
        return;
    }

    onSave(results->back());

    lastNodeEditorAssignment = results->back();
}

string ContextLogicEditor::textSource()
{
    return source().toPrettyString();
}

void ContextLogicEditor::onTextSave(const string& text)
{
    optional<vector<Value>> results = runSafely(
        context()->controller()->game().context(),
        text,
        "loading context value",
        this->context()->controller()
    );

    if (!results || results->empty())
    {
        return;
    }

    onSave(results->back());

    lastCodeEditorAssignment = results->back();
}
