/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_input.hpp"
#include "vr_tools.hpp"
#include "vr.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "controller.hpp"
#include "input_types.hpp"

static constexpr f32 metaFaceTrackingConfidenceThreshold = 0.5;
static constexpr f32 metaBodyTrackingConfidenceThreshold = 0.5;

VRInput::VRInput(Controller* controller, VR* vr)
    : InputInterface()
    , controller(controller)
    , vr(vr)
{
    XrResult result;

    XrActionSetCreateInfo actionSetCreateInfo{};
    actionSetCreateInfo.type = XR_TYPE_ACTION_SET_CREATE_INFO;
    fillBuffer<XR_MAX_ACTION_SET_NAME_SIZE>(actionSetCreateInfo.actionSetName, "attached");
    fillBuffer<XR_MAX_LOCALIZED_ACTION_SET_NAME_SIZE>(actionSetCreateInfo.localizedActionSetName, "attached");
    actionSetCreateInfo.priority = 0;

    result = xrCreateActionSet(vr->instance(), &actionSetCreateInfo, &actionSet);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create action set: " + openxrError(result));
    }

    createPaths();

    createActions();

    createActionSpaces();

    suggestBindings();

    XrSessionActionSetsAttachInfo attachInfo{};
    attachInfo.type = XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO;
    attachInfo.countActionSets = 1;
    attachInfo.actionSets = &actionSet;

    result = xrAttachSessionActionSets(vr->session(), &attachInfo);

    if (result != XR_SUCCESS)
    {
        error("Failed to attach session action sets: " + openxrError(result));
    }

    u32 configViewCount = 2;
    vector<XrViewConfigurationView> configViews(
        configViewCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    result = xrEnumerateViewConfigurationViews(
        vr->instance(),
        vr->systemID(),
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewCount,
        &configViewCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        error("Failed to enumerate view configuration views: " + openxrError(result));
    }

    width = configViews[0].recommendedImageRectWidth;
    height = configViews[0].recommendedImageRectHeight;

#if defined(XR_EXT_hand_tracking)
    if (vr->handTracking())
    {
        XrHandTrackerCreateInfoEXT handCreateInfo{};
        handCreateInfo.type = XR_TYPE_HAND_TRACKER_CREATE_INFO_EXT;
        handCreateInfo.handJointSet = XR_HAND_JOINT_SET_DEFAULT_EXT;

        handCreateInfo.hand = XR_HAND_LEFT_EXT;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateHandTrackerEXT)(
            vr->session(),
            &handCreateInfo,
            &leftHandTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize left hand tracker: " + openxrError(result));
        }

        handCreateInfo.hand = XR_HAND_RIGHT_EXT;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateHandTrackerEXT)(
            vr->session(),
            &handCreateInfo,
            &rightHandTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize right hand tracker: " + openxrError(result));
        }
    }
#endif

#if defined(XR_HTC_facial_tracking)
    if (vr->htcFacialEyeTracking())
    {
        XrFacialTrackerCreateInfoHTC facialEyeCreateInfo{};
        facialEyeCreateInfo.type = XR_TYPE_FACIAL_TRACKER_CREATE_INFO_HTC;
        facialEyeCreateInfo.facialTrackingType = XR_FACIAL_TRACKING_TYPE_EYE_DEFAULT_HTC;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateFacialTrackerHTC)(
            vr->session(),
            &facialEyeCreateInfo,
            &htcFacialEyeTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize HTC facial eye tracker: " + openxrError(result));
        }
    }

    if (vr->htcFacialLipTracking())
    {
        XrFacialTrackerCreateInfoHTC facialLipCreateInfo{};
        facialLipCreateInfo.type = XR_TYPE_FACIAL_TRACKER_CREATE_INFO_HTC;
        facialLipCreateInfo.facialTrackingType = XR_FACIAL_TRACKING_TYPE_LIP_DEFAULT_HTC;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateFacialTrackerHTC)(
            vr->session(),
            &facialLipCreateInfo,
            &htcFacialLipTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize HTC facial lip tracker: " + openxrError(result));
        }
    }
#endif

#if defined(XR_HTC_body_tracking)
    if (vr->htcBodyTracking())
    {
        XrBodyTrackerCreateInfoHTC bodyTrackerCreateInfo{};
        bodyTrackerCreateInfo.type = XR_TYPE_BODY_TRACKER_CREATE_INFO_HTC;
        bodyTrackerCreateInfo.bodyJointSet = XR_BODY_JOINT_SET_FULL_HTC;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateBodyTrackerHTC)(
            vr->session(),
            &bodyTrackerCreateInfo,
            &htcBodyTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize HTC body tracker: " + openxrError(result));
        }
    }
#endif

#if defined(XR_FB_face_tracking)
    if (vr->metaFaceTracking() == 1)
    {
        XrFaceTrackerCreateInfoFB faceTrackerCreateInfo{};
        faceTrackerCreateInfo.type = XR_TYPE_FACE_TRACKER_CREATE_INFO_FB;
        faceTrackerCreateInfo.faceExpressionSet = XR_FACE_EXPRESSION_SET_DEFAULT_FB;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateFaceTrackerFB)(
            vr->session(),
            &faceTrackerCreateInfo,
            &metaFaceTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize Meta face tracker: " + openxrError(result));
        }
    }
#endif

#if defined(XR_FB_face_tracking2)
    if (vr->metaFaceTracking() == 2)
    {
        XrFaceTrackerCreateInfo2FB faceTrackerCreateInfo{};
        faceTrackerCreateInfo.type = XR_TYPE_FACE_TRACKER_CREATE_INFO2_FB;
        faceTrackerCreateInfo.faceExpressionSet = XR_FACE_EXPRESSION_SET2_DEFAULT_FB;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateFaceTracker2FB)(
            vr->session(),
            &faceTrackerCreateInfo,
            &metaFaceTracker2
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize Meta face tracker 2: " + openxrError(result));
        }
    }
#endif

#if defined(XR_FB_body_tracking)
    if (vr->metaBodyTracking())
    {
        XrBodyTrackerCreateInfoFB bodyTrackerCreateInfo{};
        bodyTrackerCreateInfo.type = XR_TYPE_BODY_TRACKER_CREATE_INFO_FB;
        bodyTrackerCreateInfo.bodyJointSet = XR_BODY_JOINT_SET_DEFAULT_FB;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateBodyTrackerFB)(
            vr->session(),
            &bodyTrackerCreateInfo,
            &metaBodyTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize Meta body tracker: " + openxrError(result));
        }
    }
#endif

#if defined(XR_BD_body_tracking)
    if (vr->bdBodyTracking())
    {
        XrBodyTrackerCreateInfoBD bodyTrackerCreateInfo{};
        bodyTrackerCreateInfo.type = XR_TYPE_BODY_TRACKER_CREATE_INFO_BD;
        bodyTrackerCreateInfo.bodyJointSet = XR_BODY_JOINT_SET_FULL_BODY_JOINTS_BD;

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrCreateBodyTrackerBD)(
            vr->session(),
            &bodyTrackerCreateInfo,
            &bdBodyTracker
        );

        if (result != XR_SUCCESS)
        {
            fatal("Failed to initialize ByteDance body tracker: " + openxrError(result));
        }
    }
#endif
}

VRInput::~VRInput()
{
#if defined(XR_BD_body_tracking)
    if (vr->bdBodyTracking())
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyBodyTrackerBD)(bdBodyTracker);
    }
#endif

#if defined(XR_FB_body_tracking)
    if (vr->metaBodyTracking())
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyBodyTrackerFB)(metaBodyTracker);
    }
#endif

#if defined(XR_FB_face_tracking2)
    if (vr->metaFaceTracking() == 2)
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyFaceTracker2FB)(metaFaceTracker2);
    }
#endif

#if defined(XR_FB_face_tracking)
    if (vr->metaFaceTracking() == 1)
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyFaceTrackerFB)(metaFaceTracker);
    }
#endif

#if defined(XR_HTC_body_tracking)
    if (vr->htcBodyTracking())
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyBodyTrackerHTC)(htcBodyTracker);
    }
#endif

#if defined(XR_HTC_facial_tracking)
    if (vr->htcFacialLipTracking())
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyFacialTrackerHTC)(htcFacialLipTracker);
    }

    if (vr->htcFacialEyeTracking())
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyFacialTrackerHTC)(htcFacialEyeTracker);
    }
#endif

#if defined(XR_EXT_hand_tracking)
    if (vr->handTracking())
    {
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyHandTrackerEXT)(rightHandTracker);
        GET_XR_EXTENSION_FUNCTION(vr->instance(), xrDestroyHandTrackerEXT)(leftHandTracker);
    }
#endif

    xrDestroySpace(rightAnkleSpace);
    xrDestroySpace(leftAnkleSpace);
    xrDestroySpace(rightKneeSpace);
    xrDestroySpace(leftKneeSpace);
    xrDestroySpace(rightWristSpace);
    xrDestroySpace(leftWristSpace);
    xrDestroySpace(rightElbowSpace);
    xrDestroySpace(leftElbowSpace);
    xrDestroySpace(rightShoulderSpace);
    xrDestroySpace(leftShoulderSpace);
    xrDestroySpace(chestSpace);
    xrDestroySpace(rightFootSpace);
    xrDestroySpace(leftFootSpace);
    xrDestroySpace(hipsSpace);
    xrDestroySpace(rightHandSpace);
    xrDestroySpace(leftHandSpace);
    xrDestroySpace(headSpace);

    xrDestroySpace(eyesSpace);

    xrDestroyAction(gamepadRightTriggerSqueezeAction);
    xrDestroyAction(gamepadRightBumperClickAction);
    xrDestroyAction(gamepadRightStickClickAction);
    xrDestroyAction(gamepadRightStickYAction);
    xrDestroyAction(gamepadRightStickXAction);

    xrDestroyAction(gamepadLeftTriggerSqueezeAction);
    xrDestroyAction(gamepadLeftBumperClickAction);
    xrDestroyAction(gamepadLeftStickClickAction);
    xrDestroyAction(gamepadLeftStickYAction);
    xrDestroyAction(gamepadLeftStickXAction);

    xrDestroyAction(gamepadStartClickAction);
    xrDestroyAction(gamepadBackClickAction);

    xrDestroyAction(gamepadYClickAction);
    xrDestroyAction(gamepadXClickAction);
    xrDestroyAction(gamepadBClickAction);
    xrDestroyAction(gamepadAClickAction);

    xrDestroyAction(gamepadDPadDownClickAction);
    xrDestroyAction(gamepadDPadUpClickAction);
    xrDestroyAction(gamepadDPadRightClickAction);
    xrDestroyAction(gamepadDPadLeftClickAction);

    xrDestroyAction(rightHandHapticAction);

    xrDestroyAction(rightSqueezeClickAction);
    xrDestroyAction(rightSqueezeAction);
    xrDestroyAction(rightSqueezeTouchAction);

    xrDestroyAction(rightStickClickAction);
    xrDestroyAction(rightStickTouchAction);
    xrDestroyAction(rightStickYAction);
    xrDestroyAction(rightStickXAction);

    xrDestroyAction(rightTouchpadClickAction);
    xrDestroyAction(rightTouchpadSqueezeAction);
    xrDestroyAction(rightTouchpadTouchAction);
    xrDestroyAction(rightTouchpadYAction);
    xrDestroyAction(rightTouchpadXAction);

    xrDestroyAction(rightTriggerClickAction);
    xrDestroyAction(rightTriggerSqueezeAction);
    xrDestroyAction(rightTriggerTouchAction);

    xrDestroyAction(rightThumbrestSqueezeAction);
    xrDestroyAction(rightThumbrestTouchAction);
    xrDestroyAction(rightBumperClickAction);
    xrDestroyAction(rightBumperTouchAction);
    xrDestroyAction(rightMenuClickAction);
    xrDestroyAction(rightMenuTouchAction);
    xrDestroyAction(rightBClickAction);
    xrDestroyAction(rightBTouchAction);
    xrDestroyAction(rightAClickAction);
    xrDestroyAction(rightATouchAction);
    xrDestroyAction(rightSystemClickAction);
    xrDestroyAction(rightSystemTouchAction);

    xrDestroyAction(leftHandHapticAction);

    xrDestroyAction(leftSqueezeClickAction);
    xrDestroyAction(leftSqueezeAction);
    xrDestroyAction(leftSqueezeTouchAction);

    xrDestroyAction(leftStickClickAction);
    xrDestroyAction(leftStickTouchAction);
    xrDestroyAction(leftStickYAction);
    xrDestroyAction(leftStickXAction);

    xrDestroyAction(leftTouchpadClickAction);
    xrDestroyAction(leftTouchpadSqueezeAction);
    xrDestroyAction(leftTouchpadTouchAction);
    xrDestroyAction(leftTouchpadYAction);
    xrDestroyAction(leftTouchpadXAction);

    xrDestroyAction(leftTriggerClickAction);
    xrDestroyAction(leftTriggerSqueezeAction);
    xrDestroyAction(leftTriggerTouchAction);

    xrDestroyAction(leftThumbrestTouchAction);
    xrDestroyAction(leftThumbrestSqueezeAction);
    xrDestroyAction(leftBumperClickAction);
    xrDestroyAction(leftBumperTouchAction);
    xrDestroyAction(leftMenuClickAction);
    xrDestroyAction(leftMenuTouchAction);
    xrDestroyAction(leftBClickAction);
    xrDestroyAction(leftBTouchAction);
    xrDestroyAction(leftAClickAction);
    xrDestroyAction(leftATouchAction);
    xrDestroyAction(leftSystemClickAction);
    xrDestroyAction(leftSystemTouchAction);

    xrDestroyAction(rightAnkleAction);
    xrDestroyAction(leftAnkleAction);
    xrDestroyAction(rightKneeAction);
    xrDestroyAction(leftKneeAction);
    xrDestroyAction(rightWristAction);
    xrDestroyAction(leftWristAction);
    xrDestroyAction(rightElbowAction);
    xrDestroyAction(leftElbowAction);
    xrDestroyAction(rightShoulderAction);
    xrDestroyAction(leftShoulderAction);
    xrDestroyAction(chestAction);
    xrDestroyAction(rightFootAction);
    xrDestroyAction(leftFootAction);
    xrDestroyAction(hipsAction);
    xrDestroyAction(rightHandAction);
    xrDestroyAction(leftHandAction);

    xrDestroyAction(eyesAction);

    xrDestroyAction(volumeUpClickAction);
    xrDestroyAction(volumeDownClickAction);
    xrDestroyAction(muteMicrophoneClickAction);
    xrDestroyAction(systemClickAction);

    xrDestroyAction(heartrateAction);

    xrDestroyActionSet(actionSet);
}

bool VRInput::step()
{
    if (!vr->running())
    {
        return false;
    }

    XrResult result;

    XrActiveActionSet activeActionSet = {
        actionSet,
        XR_NULL_PATH
    };

    XrActionsSyncInfo syncInfo{};
    syncInfo.type = XR_TYPE_ACTIONS_SYNC_INFO;
    syncInfo.countActiveActionSets = 1;
    syncInfo.activeActionSets = &activeActionSet;

    result = xrSyncActions(vr->session(), &syncInfo);

    if (result == XR_SESSION_NOT_FOCUSED)
    {
        return false;
    }
    else if (result != XR_SUCCESS)
    {
        error("Failed to synchronize OpenXR actions: " + openxrError(result));
    }

    checkRoomBounds();
    checkNormalInputs();
#if defined(XR_EXT_hand_tracking)
    checkHandTracking();
#endif
#if defined(XR_HTC_facial_tracking)
    checkHTCFaceTracking();
#endif
#if defined(XR_HTC_body_tracking)
    checkHTCBodyTracking();
#endif
#if defined(XR_FB_face_tracking)
    checkMetaFaceTracking();
#endif
#if defined(XR_FB_face_tracking2)
    checkMetaFaceTracking2();
#endif
#if defined(XR_FB_body_tracking)
    checkMetaBodyTracking();
#endif
#if defined(XR_BD_body_tracking)
    checkBDBodyTracking();
#endif
    checkResize();

    return false;
}

void VRInput::haptic(const HapticEffect& effect)
{
    XrHapticActionInfo actionInfo{};
    actionInfo.type = XR_TYPE_HAPTIC_ACTION_INFO;

    switch (effect.target)
    {
    default :
    case Haptic_Target_Left_Hand :
        actionInfo.action = leftHandHapticAction;
        break;
    case Haptic_Target_Right_Hand :
        actionInfo.action = rightHandHapticAction;
        break;
    }

    XrHapticVibration feedback{};
    feedback.type = XR_TYPE_HAPTIC_VIBRATION;
    feedback.duration = effect.duration * 1'000'000;
    feedback.frequency = effect.frequency;
    feedback.amplitude = effect.amplitude;

    XrResult result = xrApplyHapticFeedback(
        vr->session(),
        &actionInfo,
        reinterpret_cast<const XrHapticBaseHeader*>(&feedback)
    );

    if (result != XR_SUCCESS)
    {
        error("Failed to apply haptic feedback: " + openxrError(result));
    }
}

bool VRInput::hasGamepad() const
{
    return false;
}

bool VRInput::hasTouch() const
{
    return false;
}

void VRInput::updatePose(const XrAction& action, const XrSpace& space, const function<void(Input&)>& block)
{
    XrResult result;

    XrActionStateGetInfo info{};
    info.type = XR_TYPE_ACTION_STATE_GET_INFO;
    info.action = action;

    XrActionStatePose state{};
    state.type = XR_TYPE_ACTION_STATE_POSE;

    result = xrGetActionStatePose(vr->session(), &info, &state);

    if (result != XR_SUCCESS)
    {
        error("Failed to get action state: " + openxrError(result));
    }

    XrSpaceVelocity velocity{};
    velocity.type = XR_TYPE_SPACE_VELOCITY;

    XrSpaceLocation location{};
    location.type = XR_TYPE_SPACE_LOCATION;
    location.next = &velocity;

    result = xrLocateSpace(space, vr->roomSpace(), vr->predictedDisplayTime(), &location);

    if (result != XR_SUCCESS)
    {
        error("Failed to get pose: " + openxrError(result));
    }

    Input input;

    bool valid = false;

    if (location.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT)
    {
        input.position = vr->roomToWorld({
            location.pose.position.x,
            location.pose.position.z * -1,
            location.pose.position.y
        });
        valid = true;
    }

    if (location.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT)
    {
        input.rotation = vr->roomToWorld({
            location.pose.orientation.w,
            location.pose.orientation.x,
            location.pose.orientation.z * -1,
            location.pose.orientation.y
        });
        valid = true;
    }

    if (velocity.velocityFlags & XR_SPACE_VELOCITY_LINEAR_VALID_BIT)
    {
        input.linearMotion = vr->roomToWorld({
            velocity.linearVelocity.x,
            velocity.linearVelocity.z * -1,
            velocity.linearVelocity.y
        });
        valid = true;
    }

    if (velocity.velocityFlags & XR_SPACE_VELOCITY_ANGULAR_VALID_BIT)
    {
        input.angularMotion = vr->roomToWorld({
            velocity.angularVelocity.x,
            velocity.angularVelocity.z * -1,
            velocity.angularVelocity.y
        });
        valid = true;
    }

    if (!valid)
    {
        return;
    }

    block(input);

    controller->onInput(input);
}

void VRInput::updateSpacePose(const XrSpace& space, const function<void(Input&)>& block)
{
    XrResult result;

    XrSpaceVelocity velocity{};
    velocity.type = XR_TYPE_SPACE_VELOCITY;

    XrSpaceLocation location{};
    location.type = XR_TYPE_SPACE_LOCATION;
    location.next = &velocity;

    result = xrLocateSpace(space, vr->roomSpace(), vr->predictedDisplayTime(), &location);

    if (result != XR_SUCCESS)
    {
        error("Failed to get pose: " + openxrError(result));
    }

    Input input;

    bool valid = false;

    if (location.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT)
    {
        input.position = vr->roomToWorld({
            location.pose.position.x,
            location.pose.position.z * -1,
            location.pose.position.y
        });
        valid = true;
    }

    if (location.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT)
    {
        input.rotation = vr->roomToWorld({
            location.pose.orientation.w,
            location.pose.orientation.x,
            location.pose.orientation.z * -1,
            location.pose.orientation.y
        });
        valid = true;
    }

    if (velocity.velocityFlags & XR_SPACE_VELOCITY_LINEAR_VALID_BIT)
    {
        input.linearMotion = vr->roomToWorld({
            velocity.linearVelocity.x,
            velocity.linearVelocity.z * -1,
            velocity.linearVelocity.y
        });
        valid = true;
    }

    if (velocity.velocityFlags & XR_SPACE_VELOCITY_ANGULAR_VALID_BIT)
    {
        input.angularMotion = vr->roomToWorld({
            velocity.angularVelocity.x,
            velocity.angularVelocity.z * -1,
            velocity.angularVelocity.y
        });
        valid = true;
    }

    if (!valid)
    {
        return;
    }

    block(input);

    controller->onInput(input);
}

void VRInput::updateHandPose(
    const XrHandJointLocationEXT& location,
    const XrHandJointVelocityEXT& velocity,
    const function<void(Input&)>& block
)
{
    Input input;

    bool valid = false;

    if (location.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT)
    {
        input.position = vr->roomToWorld({
            location.pose.position.x,
            location.pose.position.z * -1,
            location.pose.position.y
        });
        valid = true;
    }

    if (location.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT)
    {
        input.rotation = vr->roomToWorld({
            location.pose.orientation.w,
            location.pose.orientation.x,
            location.pose.orientation.z * -1,
            location.pose.orientation.y
        });
        valid = true;
    }

    if (velocity.velocityFlags & XR_SPACE_VELOCITY_LINEAR_VALID_BIT)
    {
        input.linearMotion = vr->roomToWorld({
            velocity.linearVelocity.x,
            velocity.linearVelocity.z * -1,
            velocity.linearVelocity.y
        });
        valid = true;
    }

    if (velocity.velocityFlags & XR_SPACE_VELOCITY_ANGULAR_VALID_BIT)
    {
        input.angularMotion = vr->roomToWorld({
            velocity.angularVelocity.x,
            velocity.angularVelocity.z * -1,
            velocity.angularVelocity.y
        });
        valid = true;
    }

    if (!valid)
    {
        return;
    }

    block(input);

    controller->onInput(input);
}

void VRInput::updateFacialPose(InputType type, const vec3& position)
{
    Input input;
    input.type = type;
    input.position = position;

    controller->onInput(input);
}

void VRInput::updateFacialPose(InputType type, f64 intensity)
{
    Input input;
    input.type = type;
    input.intensity = intensity;

    controller->onInput(input);
}

void VRInput::updateFacialPose(InputType type, const vec3& position, f64 intensity)
{
    Input input;
    input.type = type;
    input.position = position;
    input.intensity = intensity;

    controller->onInput(input);
}

void VRInput::updateFacialPose(InputType type, InputDimensions dimensions)
{
    Input input;
    input.type = type;
    input.dimensions = dimensions;

    controller->onInput(input);
}

void VRInput::whenChanged(const XrAction& action, const function<void(Input&)>& block)
{
    XrActionStateGetInfo info{};
    info.type = XR_TYPE_ACTION_STATE_GET_INFO;
    info.action = action;

    XrActionStateBoolean state{};
    state.type = XR_TYPE_ACTION_STATE_BOOLEAN;

    XrResult result = xrGetActionStateBoolean(vr->session(), &info, &state);

    if (result != XR_SUCCESS)
    {
        error("Failed to get action state: " + openxrError(result));
    }

    if (state.changedSinceLastSync)
    {
        Input input;
        input.up = !state.currentState;

        block(input);

        controller->onInput(input);
    }
}

void VRInput::whenChanged(const XrAction& action, const function<void(Input&, f64)>& block)
{
    XrActionStateGetInfo info{};
    info.type = XR_TYPE_ACTION_STATE_GET_INFO;
    info.action = action;

    XrActionStateFloat state{};
    state.type = XR_TYPE_ACTION_STATE_FLOAT;

    XrResult result = xrGetActionStateFloat(vr->session(), &info, &state);

    if (result != XR_SUCCESS)
    {
        error("Failed to get action state: " + openxrError(result));
    }

    if (state.changedSinceLastSync)
    {
        Input input;

        block(input, state.currentState);

        controller->onInput(input);
    }
}

void VRInput::checkRoomBounds()
{
    if (vr->monado())
    {
        return;
    }

    XrExtent2Df bounds{};

    XrResult result = xrGetReferenceSpaceBoundsRect(vr->session(), XR_REFERENCE_SPACE_TYPE_STAGE, &bounds);

    if (result == XR_SUCCESS)
    {
        controller->onBounds({ bounds.width, bounds.height });
    }
    else if (result != XR_SPACE_BOUNDS_UNAVAILABLE)
    {
        error("Failed to get OpenXR space bounds: " + openxrError(result));
    }
}

void VRInput::checkNormalInputs()
{
    whenChanged(heartrateAction, [&](Input& input, f64 rate) -> void
    {
        input.type = Input_VR_Heartrate;
        input.intensity = rate;
    });

    whenChanged(systemClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_System_Click;
    });

    whenChanged(volumeUpClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Volume_Up;
    });

    whenChanged(volumeDownClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Volume_Down;
    });

    whenChanged(muteMicrophoneClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Mute_Microphone;
    });

    updatePose(eyesAction, eyesSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Eyes;
    });

    updatePose(eyesAction, eyesSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Eyes;
    });

    updateSpacePose(headSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Head;
    });

    mat4 leftHandTransform;
    Velocities leftHandVelocities;

    updatePose(leftHandAction, leftHandSpace, [&](Input& input) -> void
    {
        quaternion adjust(input.rotation.right(), radians(-90));

        input.type = Input_VR_Left_Hand;
        input.rotation = adjust * input.rotation;

        leftHandTransform = { input.position, input.rotation, vec3(1) };
        leftHandVelocities = { input.linearMotion, input.angularMotion };
    });

    mat4 rightHandTransform;
    Velocities rightHandVelocities;

    updatePose(rightHandAction, rightHandSpace, [&](Input& input) -> void
    {
        quaternion adjust(input.rotation.right(), radians(-90));

        input.type = Input_VR_Right_Hand;
        input.rotation = adjust * input.rotation;

        rightHandTransform = { input.position, input.rotation, vec3(1) };
        rightHandVelocities = { input.linearMotion, input.angularMotion };
    });

    updatePose(hipsAction, hipsSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Hips;
    });

    updatePose(leftFootAction, leftFootSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Foot;
    });

    updatePose(rightFootAction, rightFootSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Foot;
    });

    updatePose(chestAction, chestSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Chest;
    });

    updatePose(leftShoulderAction, leftShoulderSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Shoulder;
    });

    updatePose(rightShoulderAction, rightShoulderSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Shoulder;
    });

    updatePose(leftElbowAction, leftElbowSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Elbow;
    });

    updatePose(rightElbowAction, rightElbowSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Elbow;
    });

    updatePose(leftWristAction, leftWristSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Wrist;
    });

    updatePose(rightWristAction, rightWristSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Wrist;
    });

    updatePose(leftKneeAction, leftKneeSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Knee;
    });

    updatePose(rightKneeAction, rightKneeSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Knee;
    });

    updatePose(leftAnkleAction, leftAnkleSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Ankle;
    });

    updatePose(rightAnkleAction, rightAnkleSpace, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Ankle;
    });

    whenChanged(leftSystemTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_System_Touch;
    });

    whenChanged(leftSystemClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_System;
    });

    whenChanged(leftATouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_A_Touch;
    });

    whenChanged(leftAClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_A;
    });

    whenChanged(leftBTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_B_Touch;
    });

    whenChanged(leftBClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_B;
    });

    whenChanged(leftMenuTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Menu_Touch;
    });

    whenChanged(leftMenuClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Menu;
    });

    whenChanged(leftBumperTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Bumper_Touch;
    });

    whenChanged(leftBumperClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Bumper;
    });

    whenChanged(leftThumbrestTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Thumbrest_Touch;
    });

    whenChanged(leftThumbrestSqueezeAction, [&](Input& input, f64 intensity) -> void
    {
        input.type = Input_VR_Left_Thumbrest;
        input.intensity = intensity;
    });

    whenChanged(leftTriggerTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Trigger_Touch;
    });

    whenChanged(leftTriggerSqueezeAction, [&](Input& input, f64 intensity) -> void
    {
        controller->onGrip(false, intensity, leftHandTransform, leftHandVelocities);

        input.type = Input_VR_Left_Trigger;
        input.intensity = intensity;
    });

    whenChanged(leftTriggerClickAction, [&](Input& input) -> void
    {
        controller->onGrip(false, 1, leftHandTransform, leftHandVelocities);

        input.type = Input_VR_Left_Trigger_Click;
    });

    whenChanged(leftTouchpadXAction, [&](Input& input, f64 x) -> void
    {
        input.type = Input_VR_Left_Touchpad;
        input.position = { x, lastLeftTouchpad.y, 0 };
        input.linearMotion = { x - lastLeftTouchpad.x, 0, 0 };

        lastLeftTouchpad.x = x;
    });

    whenChanged(leftTouchpadYAction, [&](Input& input, f64 y) -> void
    {
        input.type = Input_VR_Left_Touchpad;
        input.position = { lastLeftTouchpad.x, y, 0 };
        input.linearMotion = { 0, y - lastLeftTouchpad.y, 0 };

        lastLeftTouchpad.y = y;
    });

    whenChanged(leftTouchpadTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Touchpad_Touch;
        input.position = { lastLeftTouchpad.x, lastLeftTouchpad.y, 0 };
    });

    whenChanged(leftTouchpadSqueezeAction, [&](Input& input, f64 force) -> void
    {
        input.type = Input_VR_Left_Touchpad_Press;
        input.intensity = force;
        input.position = { lastLeftTouchpad.x, lastLeftTouchpad.y, 0 };
    });

    whenChanged(leftTouchpadClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Touchpad_Click;
        input.position = { lastLeftTouchpad.x, lastLeftTouchpad.y, 0 };
    });

    whenChanged(leftStickXAction, [&](Input& input, f64 x) -> void
    {
        x = applyDeadZone(x, g_config.vrLeftDeadZone.x);

        input.type = Input_VR_Left_Stick;
        input.position = { x, lastLeftStick.y, 0 };
        input.linearMotion = { x - lastLeftStick.x, 0, 0 };

        lastLeftStick.x = x;
    });

    whenChanged(leftStickYAction, [&](Input& input, f64 y) -> void
    {
        y = applyDeadZone(y, g_config.vrLeftDeadZone.y);

        input.type = Input_VR_Left_Stick;
        input.position = { lastLeftStick.x, y, 0 };
        input.linearMotion = { 0, y - lastLeftStick.y, 0 };

        lastLeftStick.y = y;
    });

    whenChanged(leftStickTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Stick_Touch;
        input.position = { lastLeftStick.x, lastLeftStick.y, 0 };
    });

    whenChanged(leftStickClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Stick_Click;
    });

    whenChanged(leftSqueezeTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Grip_Touch;
    });

    whenChanged(leftSqueezeAction, [&](Input& input, f64 force) -> void
    {
        controller->onGrip(false, force, leftHandTransform, leftHandVelocities);

        input.type = Input_VR_Left_Grip;
        input.intensity = force;
    });

    whenChanged(leftSqueezeClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Left_Grip_Click;
    });

    whenChanged(rightSystemTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_System_Touch;
    });

    whenChanged(rightSystemClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_System;
    });

    whenChanged(rightATouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_A_Touch;
    });

    whenChanged(rightAClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_A;
    });

    whenChanged(rightBTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_B_Touch;
    });

    whenChanged(rightBClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_B;
    });

    whenChanged(rightMenuTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Menu_Touch;
    });

    whenChanged(rightMenuClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Menu;
    });

    whenChanged(rightBumperTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Bumper_Touch;
    });

    whenChanged(rightBumperClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Bumper;
    });

    whenChanged(rightThumbrestTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Thumbrest_Touch;
    });

    whenChanged(rightThumbrestSqueezeAction, [&](Input& input, f64 intensity) -> void
    {
        input.type = Input_VR_Right_Thumbrest;
        input.intensity = intensity;
    });

    whenChanged(rightTriggerTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Trigger_Touch;
    });

    whenChanged(rightTriggerSqueezeAction, [&](Input& input, f64 intensity) -> void
    {
        controller->onGrip(true, intensity, rightHandTransform, rightHandVelocities);

        input.type = Input_VR_Right_Trigger;
        input.intensity = intensity;
    });

    whenChanged(rightTriggerClickAction, [&](Input& input) -> void
    {
        controller->onGrip(true, 1, rightHandTransform, rightHandVelocities);

        input.type = Input_VR_Right_Trigger_Click;
    });

    whenChanged(rightTouchpadXAction, [&](Input& input, f64 x) -> void
    {
        input.type = Input_VR_Right_Touchpad;
        input.position = { x, lastRightTouchpad.y, 0 };
        input.linearMotion = { x - lastRightTouchpad.x, 0, 0 };

        lastRightTouchpad.x = x;
    });

    whenChanged(rightTouchpadYAction, [&](Input& input, f64 y) -> void
    {
        input.type = Input_VR_Right_Touchpad;
        input.position = { lastRightTouchpad.x, y, 0 };
        input.linearMotion = { 0, y - lastRightTouchpad.y, 0 };

        lastRightTouchpad.y = y;
    });

    whenChanged(rightTouchpadTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Touchpad_Touch;
        input.position = { lastRightTouchpad.x, lastRightTouchpad.y, 0 };
    });

    whenChanged(rightTouchpadSqueezeAction, [&](Input& input, f64 force) -> void
    {
        input.type = Input_VR_Right_Touchpad_Press;
        input.intensity = force;
        input.position = { lastRightTouchpad.x, lastRightTouchpad.y, 0 };
    });

    whenChanged(rightTouchpadClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Touchpad_Click;
        input.position = { lastRightTouchpad.x, lastRightTouchpad.y, 0 };
    });

    whenChanged(rightStickXAction, [&](Input& input, f64 x) -> void
    {
        x = applyDeadZone(x, g_config.vrRightDeadZone.x);

        input.type = Input_VR_Right_Stick;
        input.position = { x, lastRightStick.y, 0 };
        input.linearMotion = { x - lastRightStick.x, 0, 0 };

        lastRightStick.x = x;
    });

    whenChanged(rightStickYAction, [&](Input& input, f64 y) -> void
    {
        y = applyDeadZone(y, g_config.vrRightDeadZone.y);

        input.type = Input_VR_Right_Stick;
        input.position = { lastRightStick.x, y, 0 };
        input.linearMotion = { 0, y - lastRightStick.y, 0 };

        lastRightStick.y = y;
    });

    whenChanged(rightStickTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Stick_Touch;
        input.position = { lastRightStick.x, lastRightStick.y, 0 };
    });

    whenChanged(rightStickClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Stick_Click;
    });

    whenChanged(rightSqueezeTouchAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Grip_Touch;
    });

    whenChanged(rightSqueezeAction, [&](Input& input, f64 force) -> void
    {
        controller->onGrip(true, force, rightHandTransform, rightHandVelocities);

        input.type = Input_VR_Right_Grip;
        input.intensity = force;
    });

    whenChanged(rightSqueezeClickAction, [&](Input& input) -> void
    {
        input.type = Input_VR_Right_Grip_Click;
    });

    whenChanged(gamepadDPadLeftClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_DPad_Left;
    });

    whenChanged(gamepadDPadRightClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_DPad_Right;
    });

    whenChanged(gamepadDPadUpClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_DPad_Up;
    });

    whenChanged(gamepadDPadDownClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_DPad_Down;
    });

    whenChanged(gamepadAClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_A;
    });

    whenChanged(gamepadBClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_B;
    });

    whenChanged(gamepadXClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_X;
    });

    whenChanged(gamepadYClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Y;
    });

    whenChanged(gamepadBackClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Back;
    });

    whenChanged(gamepadStartClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Start;
    });

    whenChanged(gamepadLeftStickXAction, [&](Input& input, f64 x) -> void
    {
        input.type = Input_Gamepad_Left_Stick;
        input.position = { x, lastLeftStick.y, 0 };
        input.linearMotion = { x - lastLeftStick.x, 0, 0 };

        lastLeftStick.x = x;
    });

    whenChanged(gamepadLeftStickYAction, [&](Input& input, f64 y) -> void
    {
        input.type = Input_Gamepad_Left_Stick;
        input.position = { lastLeftStick.x, y, 0 };
        input.linearMotion = { 0, y - lastLeftStick.y, 0 };

        lastLeftStick.y = y;
    });

    whenChanged(gamepadLeftStickClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Left_Stick_Click;
    });

    whenChanged(gamepadLeftBumperClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Left_Bumper;
    });

    whenChanged(gamepadLeftTriggerSqueezeAction, [&](Input& input, f64 value) -> void
    {
        input.type = Input_Gamepad_Left_Trigger;
        input.intensity = value;
    });

    whenChanged(gamepadRightStickXAction, [&](Input& input, f64 x) -> void
    {
        input.type = Input_Gamepad_Right_Stick;
        input.position = { x, lastRightTouchpad.y, 0 };
        input.linearMotion = { x - lastRightTouchpad.x, 0, 0 };

        lastRightTouchpad.x = x;
    });

    whenChanged(gamepadRightStickYAction, [&](Input& input, f64 y) -> void
    {
        input.type = Input_Gamepad_Right_Stick;
        input.position = { lastRightTouchpad.x, y, 0 };
        input.linearMotion = { 0, y - lastRightTouchpad.y, 0 };

        lastRightTouchpad.y = y;
    });

    whenChanged(gamepadRightStickClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Right_Stick_Click;
    });

    whenChanged(gamepadRightBumperClickAction, [&](Input& input) -> void
    {
        input.type = Input_Gamepad_Right_Bumper;
    });

    whenChanged(gamepadRightTriggerSqueezeAction, [&](Input& input, f64 value) -> void
    {
        input.type = Input_Gamepad_Right_Trigger;
        input.intensity = value;
    });
}

#if defined(XR_EXT_hand_tracking)
void VRInput::checkHandTracking()
{
    XrResult result;

    if (vr->handTracking())
    {
        XrHandJointsLocateInfoEXT handJointLocateInfo{};
        handJointLocateInfo.type = XR_TYPE_HAND_JOINTS_LOCATE_INFO_EXT;
        handJointLocateInfo.baseSpace = vr->roomSpace();
        handJointLocateInfo.time = vr->predictedDisplayTime();

        XrHandJointVelocitiesEXT handJointVelocities{};
        handJointVelocities.type = XR_TYPE_HAND_JOINT_VELOCITIES_EXT;
        handJointVelocities.jointCount = XR_HAND_JOINT_COUNT_EXT;
        handJointVelocities.jointVelocities = new XrHandJointVelocityEXT[XR_HAND_JOINT_COUNT_EXT];

        XrHandJointLocationsEXT handJointLocations{};
        handJointLocations.type = XR_TYPE_HAND_JOINT_LOCATIONS_EXT;
        handJointLocations.next = &handJointVelocities;
        handJointLocations.jointCount = XR_HAND_JOINT_COUNT_EXT;
        handJointLocations.jointLocations = new XrHandJointLocationEXT[XR_HAND_JOINT_COUNT_EXT];

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrLocateHandJointsEXT)(
            leftHandTracker,
            &handJointLocateInfo,
            &handJointLocations
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to locate left hand joints: " + openxrError(result));
        }

        if (handJointLocations.isActive)
        {
            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_THUMB_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_THUMB_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Left_Thumb;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_INDEX_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_INDEX_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Left_Index;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_MIDDLE_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_MIDDLE_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Left_Middle;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_RING_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_RING_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Left_Ring;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_LITTLE_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_LITTLE_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Left_Little;
                }
            );
        }

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrLocateHandJointsEXT)(
            rightHandTracker,
            &handJointLocateInfo,
            &handJointLocations
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to locate right hand joints: " + openxrError(result));
        }

        if (!handJointLocations.isActive)
        {
            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_THUMB_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_THUMB_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Right_Thumb;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_INDEX_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_INDEX_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Right_Index;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_MIDDLE_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_MIDDLE_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Right_Middle;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_RING_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_RING_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Right_Ring;
                }
            );

            updateHandPose(
                handJointLocations.jointLocations[XR_HAND_JOINT_LITTLE_TIP_EXT],
                handJointVelocities.jointVelocities[XR_HAND_JOINT_LITTLE_TIP_EXT],
                [&](Input& input) -> void
                {
                    input.type = Input_VR_Right_Little;
                }
            );
        }

        delete[] handJointLocations.jointLocations;
        delete[] handJointVelocities.jointVelocities;
    }
}
#endif

#if defined(XR_HTC_facial_tracking)
void VRInput::checkHTCFaceTracking()
{
    XrResult result;

    if (vr->htcFacialEyeTracking())
    {
        XrFacialExpressionsHTC facialExpressions{};
        facialExpressions.type = XR_TYPE_FACIAL_EXPRESSIONS_HTC;
        facialExpressions.expressionCount = XR_FACIAL_EXPRESSION_EYE_COUNT_HTC;
        facialExpressions.expressionWeightings = new float[XR_FACIAL_EXPRESSION_EYE_COUNT_HTC];

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrGetFacialExpressionsHTC)(htcFacialEyeTracker, &facialExpressions);

        if (result != XR_SUCCESS)
        {
            error("Failed to get HTC facial eye expressions: " + openxrError(result));
        }

        if (facialExpressions.isActive)
        {
            f64 leftEyebrow = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_WIDE_HTC] / 100;

            updateFacialPose(Input_VR_Left_Eyebrow, leftEyebrow);

            f64 leftBlink = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_BLINK_HTC] / 100;
            f64 leftSqueeze = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_SQUEEZE_HTC] / 100;

            f64 leftEyelid;

            if (leftSqueeze > 0)
            {
                leftEyelid = leftSqueeze * -1;
            }
            else
            {
                leftEyelid = 1 - leftBlink;
            }

            updateFacialPose(Input_VR_Left_Eyelid, leftEyelid);

            InputDimensions leftEye;

            leftEye[0] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_IN_HTC] / 100;
            leftEye[1] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_OUT_HTC] / 100;
            leftEye[2] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_UP_HTC] / 100;
            leftEye[3] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_DOWN_HTC] / 100;
            leftEye[4] = 0;
            leftEye[5] = 0;

            updateFacialPose(Input_VR_Left_Eye, leftEye);

            f64 rightEyebrow = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_WIDE_HTC] / 100;

            updateFacialPose(Input_VR_Right_Eyebrow, rightEyebrow);

            f64 rightBlink = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_BLINK_HTC] / 100;
            f64 rightSqueeze = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_SQUEEZE_HTC] / 100;

            f64 rightEyelid;

            if (rightSqueeze > 0)
            {
                rightEyelid = rightSqueeze * -1;
            }
            else
            {
                rightEyelid = 1 - rightBlink;
            }

            updateFacialPose(Input_VR_Right_Eyelid, rightEyelid);

            InputDimensions rightEye;

            rightEye[0] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_IN_HTC] / 100;
            rightEye[1] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_OUT_HTC] / 100;
            rightEye[2] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_UP_HTC] / 100;
            rightEye[3] = facialExpressions.expressionWeightings[XR_EYE_EXPRESSION_LEFT_DOWN_HTC] / 100;
            rightEye[4] = 0;
            rightEye[5] = 0;

            updateFacialPose(Input_VR_Right_Eye, rightEye);
        }

        delete[] facialExpressions.expressionWeightings;
    }

    if (vr->htcFacialLipTracking())
    {
        XrFacialExpressionsHTC facialExpressions{};
        facialExpressions.type = XR_TYPE_FACIAL_EXPRESSIONS_HTC;
        facialExpressions.expressionCount = XR_FACIAL_EXPRESSION_LIP_COUNT_HTC;
        facialExpressions.expressionWeightings = new float[XR_FACIAL_EXPRESSION_LIP_COUNT_HTC];

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrGetFacialExpressionsHTC)(htcFacialLipTracker, &facialExpressions);

        if (result != XR_SUCCESS)
        {
            error("Failed to get HTC facial lip expressions: " + openxrError(result));
        }

        if (facialExpressions.isActive)
        {
            // Jaw
            f64 jawRight = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_JAW_RIGHT_HTC] / 100;
            f64 jawLeft = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_JAW_LEFT_HTC] / 100;
            f64 jawForward = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_JAW_FORWARD_HTC] / 100;
            f64 jawOpen = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_JAW_OPEN_HTC] / 100;

            vec3 jawPosition = {
                jawRight > 0 ? jawRight : jawLeft * -1,
                jawForward,
                0
            };

            updateFacialPose(Input_VR_Jaw, jawPosition, jawOpen);

            // Mouth
            f64 pout = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_POUT_HTC] / 100;

            f64 upperLipLeft = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_UPPER_LEFT_HTC] / 100;
            f64 upperLipRight = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_UPPER_RIGHT_HTC] / 100;
            f64 upperOut = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_UPPER_OVERTURN_HTC] / 100;
            f64 upperIn = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_UPPER_INSIDE_HTC] / 100;

            f64 lowerLipLeft = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_LEFT_HTC] / 100;
            f64 lowerLipRight = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_RIGHT_HTC] / 100;
            f64 lowerOut = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_OVERTURN_HTC] / 100;
            f64 lowerIn = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_INSIDE_HTC] / 100;
            f64 lowerOverlay = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_OVERLAY_HTC] / 100;

            f64 leftCornerUp = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_SMILE_LEFT_HTC] / 100;
            f64 leftCornerDown = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_SAD_LEFT_HTC] / 100;

            f64 rightCornerUp = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_SMILE_RIGHT_HTC] / 100;
            f64 rightCornerDown = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_SAD_RIGHT_HTC] / 100;

            vec3 upperLip = {
                upperLipRight > 0 ? upperLipRight : upperLipLeft * -1,
                upperIn > 0 ? upperIn * -1 : pout > 0 ? 0.5 + (pout / 2) : upperOut / 2,
                0
            };

            updateFacialPose(Input_VR_Upper_Lip, upperLip);

            vec3 lowerLip = {
                lowerLipRight > 0 ? lowerLipRight : lowerLipLeft * -1,
                lowerIn > 0 ? lowerIn * -1 : pout > 0 ? 0.5 + (pout / 2) : lowerOut / 2,
                lowerOverlay
            };

            updateFacialPose(Input_VR_Lower_Lip, lowerLip);

            f64 leftUpperLip = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_UPPER_UPLEFT_HTC] / 100;
            f64 rightUpperLip = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_UPPER_UPRIGHT_HTC] / 100;
            f64 leftCorner = leftCornerUp > 0 ? leftCornerUp : leftCornerDown * -1;
            f64 rightCorner = rightCornerUp > 0 ? rightCornerUp : rightCornerDown * -1;
            f64 leftLowerLip = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_DOWNLEFT_HTC] / 100;
            f64 rightLowerLip = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_MOUTH_LOWER_DOWNRIGHT_HTC] / 100;

            updateFacialPose(Input_VR_Mouth, {
                leftUpperLip,
                rightUpperLip,
                leftCorner,
                rightCorner,
                leftLowerLip,
                rightLowerLip
            });

            // Cheeks
            f64 cheekLeft = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_CHEEK_PUFF_LEFT_HTC] / 100;
            f64 cheekRight = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_CHEEK_PUFF_RIGHT_HTC] / 100;
            f64 cheekIn = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_CHEEK_SUCK_HTC] / 100;

            f64 leftCheek = cheekIn > 0 ? cheekIn * -1 : cheekLeft;

            updateFacialPose(Input_VR_Left_Cheek, leftCheek);

            f64 rightCheek = cheekIn > 0 ? cheekIn * -1 : cheekRight;

            updateFacialPose(Input_VR_Right_Cheek, rightCheek);

            // Tongue
            f64 tongueOut0 = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_LONGSTEP1_HTC] / 100;
            f64 tongueOut1 = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_LONGSTEP2_HTC] / 100;
            f64 tongueLeft = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_LEFT_HTC] / 100;
            f64 tongueRight = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_RIGHT_HTC] / 100;
            f64 tongueUp = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_UP_HTC] / 100;
            f64 tongueDown = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_DOWN_HTC] / 100;

            vec3 tonguePosition = {
                tongueRight > 0 ? tongueRight : tongueLeft * -1,
                tongueOut1 > 0 ? 0.5 + (tongueOut1 / 2) : tongueOut0 / 2,
                tongueUp > 0 ? tongueUp : tongueDown * -1
            };

            f64 tongueRoll = facialExpressions.expressionWeightings[XR_LIP_EXPRESSION_TONGUE_ROLL_HTC] / 100;

            updateFacialPose(Input_VR_Tongue, tonguePosition, tongueRoll);
        }

        delete[] facialExpressions.expressionWeightings;
    }
}
#endif

#if defined(XR_HTC_body_tracking)
void VRInput::checkHTCBodyTracking()
{
    XrResult result;

    if (vr->htcBodyTracking())
    {
        XrBodyJointsLocateInfoHTC locateInfo{};
        locateInfo.type = XR_TYPE_BODY_JOINTS_LOCATE_INFO_HTC;
        locateInfo.baseSpace = vr->roomSpace();
        locateInfo.time = vr->predictedDisplayTime();

        vector<XrBodyJointLocationHTC> jointLocations(XR_BODY_JOINT_COUNT_HTC, XrBodyJointLocationHTC{});

        XrBodyJointLocationsHTC locations{};
        locations.type = XR_TYPE_BODY_JOINT_LOCATIONS_HTC;
        locations.jointLocationCount = jointLocations.size();
        locations.jointLocations = jointLocations.data();

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrLocateBodyJointsHTC)(
            htcBodyTracker,
            &locateInfo,
            &locations
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to get HTC body pose: " + openxrError(result));
        }

        if (locations.confidenceLevel != XR_BODY_JOINT_CONFIDENCE_NONE_HTC)
        {
            for (u32 i = 0; i < XR_BODY_JOINT_COUNT_HTC; i++)
            {
                const XrBodyJointLocationHTC& joint = jointLocations.at(i);

                if (!(joint.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) ||
                    !(joint.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT)
                )
                {
                    continue;
                }

                vec3 position = vr->roomToWorld({
                    joint.pose.position.x,
                    joint.pose.position.z * -1,
                    joint.pose.position.y
                });
                quaternion rotation = vr->roomToWorld({
                    joint.pose.orientation.w,
                    joint.pose.orientation.x,
                    joint.pose.orientation.z * -1,
                    joint.pose.orientation.y
                });

                Input input;
                input.position = position;
                input.rotation = rotation;

                switch (i)
                {
                default :
                    continue;
                case XR_BODY_JOINT_PELVIS_HTC :
                    input.type = Input_VR_Hips;
                    break;
                case XR_BODY_JOINT_LEFT_KNEE_HTC :
                    input.type = Input_VR_Left_Knee;
                    break;
                case XR_BODY_JOINT_LEFT_ANKLE_HTC :
                    input.type = Input_VR_Left_Ankle;
                    break;
                case XR_BODY_JOINT_LEFT_FEET_HTC :
                    input.type = Input_VR_Left_Foot;
                    break;
                case XR_BODY_JOINT_RIGHT_KNEE_HTC :
                    input.type = Input_VR_Right_Knee;
                    break;
                case XR_BODY_JOINT_RIGHT_ANKLE_HTC :
                    input.type = Input_VR_Right_Ankle;
                    break;
                case XR_BODY_JOINT_RIGHT_FEET_HTC :
                    input.type = Input_VR_Right_Foot;
                    break;
                case XR_BODY_JOINT_CHEST_HTC :
                    input.type = Input_VR_Chest;
                    break;
                case XR_BODY_JOINT_LEFT_ARM_HTC :
                    input.type = Input_VR_Left_Shoulder;
                    break;
                case XR_BODY_JOINT_LEFT_ELBOW_HTC :
                    input.type = Input_VR_Left_Elbow;
                    break;
                case XR_BODY_JOINT_RIGHT_ARM_HTC :
                    input.type = Input_VR_Right_Shoulder;
                    break;
                case XR_BODY_JOINT_RIGHT_ELBOW_HTC :
                    input.type = Input_VR_Right_Elbow;
                    break;
                }

                controller->onInput(input);
            }
        }
    }
}
#endif

#if defined(XR_FB_face_tracking)
void VRInput::checkMetaFaceTracking()
{
    XrResult result;

    if (vr->metaFaceTracking() == 1)
    {
        XrFaceExpressionInfoFB expressionInfo{};
        expressionInfo.type = XR_TYPE_FACE_EXPRESSION_INFO_FB;
        expressionInfo.time = vr->predictedDisplayTime();

        vector<f32> weights(XR_FACE_EXPRESSION_COUNT_FB, 0);
        vector<f32> confidences(XR_FACE_CONFIDENCE_COUNT_FB, 0);

        XrFaceExpressionWeightsFB expressionWeights{};
        expressionWeights.type = XR_TYPE_FACE_EXPRESSION_WEIGHTS_FB;
        expressionWeights.weightCount = weights.size();
        expressionWeights.weights = weights.data();
        expressionWeights.confidenceCount = confidences.size();
        expressionWeights.confidences = confidences.data();

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrGetFaceExpressionWeightsFB)(
            metaFaceTracker,
            &expressionInfo,
            &expressionWeights
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to get Meta face expressions: " + openxrError(result));
        }

        vector<bool> validity(XR_FACE_EXPRESSION_COUNT_FB, false);

        for (u32 i = 0; i < XR_FACE_EXPRESSION_COUNT_FB; i++)
        {
            f32 confidence;

            switch (i)
            {
            case XR_FACE_EXPRESSION_EYES_LOOK_DOWN_L_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_DOWN_R_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_LEFT_L_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_LEFT_R_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_RIGHT_L_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_RIGHT_R_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_UP_L_FB :
            case XR_FACE_EXPRESSION_EYES_LOOK_UP_R_FB :
                if (!expressionWeights.status.isEyeFollowingBlendshapesValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE_UPPER_FACE_FB);
                break;
            case XR_FACE_EXPRESSION_BROW_LOWERER_L_FB :
            case XR_FACE_EXPRESSION_BROW_LOWERER_R_FB :
            case XR_FACE_EXPRESSION_EYES_CLOSED_L_FB :
            case XR_FACE_EXPRESSION_EYES_CLOSED_R_FB :
            case XR_FACE_EXPRESSION_INNER_BROW_RAISER_L_FB :
            case XR_FACE_EXPRESSION_INNER_BROW_RAISER_R_FB :
            case XR_FACE_EXPRESSION_OUTER_BROW_RAISER_L_FB :
            case XR_FACE_EXPRESSION_OUTER_BROW_RAISER_R_FB :
            case XR_FACE_EXPRESSION_UPPER_LID_RAISER_L_FB :
            case XR_FACE_EXPRESSION_UPPER_LID_RAISER_R_FB :
            case XR_FACE_EXPRESSION_LID_TIGHTENER_L_FB :
            case XR_FACE_EXPRESSION_LID_TIGHTENER_R_FB :
                if (!expressionWeights.status.isValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE_UPPER_FACE_FB);
                break;
            case XR_FACE_EXPRESSION_JAW_DROP_FB :
            case XR_FACE_EXPRESSION_JAW_SIDEWAYS_LEFT_FB :
            case XR_FACE_EXPRESSION_JAW_SIDEWAYS_RIGHT_FB :
            case XR_FACE_EXPRESSION_JAW_THRUST_FB :
            case XR_FACE_EXPRESSION_LIP_CORNER_DEPRESSOR_L_FB :
            case XR_FACE_EXPRESSION_LIP_CORNER_DEPRESSOR_R_FB :
            case XR_FACE_EXPRESSION_LIP_CORNER_PULLER_L_FB :
            case XR_FACE_EXPRESSION_LIP_CORNER_PULLER_R_FB :
            case XR_FACE_EXPRESSION_LIP_FUNNELER_LB_FB :
            case XR_FACE_EXPRESSION_LIP_FUNNELER_LT_FB :
            case XR_FACE_EXPRESSION_LIP_FUNNELER_RB_FB :
            case XR_FACE_EXPRESSION_LIP_FUNNELER_RT_FB :
            case XR_FACE_EXPRESSION_LIP_PRESSOR_L_FB :
            case XR_FACE_EXPRESSION_LIP_PRESSOR_R_FB :
            case XR_FACE_EXPRESSION_LIP_PUCKER_L_FB :
            case XR_FACE_EXPRESSION_LIP_PUCKER_R_FB :
            case XR_FACE_EXPRESSION_LIP_STRETCHER_L_FB :
            case XR_FACE_EXPRESSION_LIP_STRETCHER_R_FB :
            case XR_FACE_EXPRESSION_LIP_SUCK_LB_FB :
            case XR_FACE_EXPRESSION_LIP_SUCK_LT_FB :
            case XR_FACE_EXPRESSION_LIP_SUCK_RB_FB :
            case XR_FACE_EXPRESSION_LIP_SUCK_RT_FB :
            case XR_FACE_EXPRESSION_LIP_TIGHTENER_L_FB :
            case XR_FACE_EXPRESSION_LIP_TIGHTENER_R_FB :
            case XR_FACE_EXPRESSION_LIPS_TOWARD_FB :
            case XR_FACE_EXPRESSION_LOWER_LIP_DEPRESSOR_L_FB :
            case XR_FACE_EXPRESSION_LOWER_LIP_DEPRESSOR_R_FB :
            case XR_FACE_EXPRESSION_MOUTH_LEFT_FB :
            case XR_FACE_EXPRESSION_MOUTH_RIGHT_FB :
            case XR_FACE_EXPRESSION_UPPER_LIP_RAISER_L_FB :
            case XR_FACE_EXPRESSION_UPPER_LIP_RAISER_R_FB :
            case XR_FACE_EXPRESSION_CHIN_RAISER_B_FB :
            case XR_FACE_EXPRESSION_CHIN_RAISER_T_FB :
                if (!expressionWeights.status.isValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE_LOWER_FACE_FB);
                break;
            case XR_FACE_EXPRESSION_DIMPLER_L_FB :
            case XR_FACE_EXPRESSION_DIMPLER_R_FB :
            case XR_FACE_EXPRESSION_NOSE_WRINKLER_L_FB :
            case XR_FACE_EXPRESSION_NOSE_WRINKLER_R_FB :
            case XR_FACE_EXPRESSION_CHEEK_PUFF_L_FB :
            case XR_FACE_EXPRESSION_CHEEK_PUFF_R_FB :
            case XR_FACE_EXPRESSION_CHEEK_RAISER_L_FB :
            case XR_FACE_EXPRESSION_CHEEK_RAISER_R_FB :
            case XR_FACE_EXPRESSION_CHEEK_SUCK_L_FB :
            case XR_FACE_EXPRESSION_CHEEK_SUCK_R_FB :
                if (!expressionWeights.status.isValid)
                {
                    continue;
                }

                confidence = max(
                    confidences.at(XR_FACE_CONFIDENCE_LOWER_FACE_FB),
                    confidences.at(XR_FACE_CONFIDENCE_UPPER_FACE_FB)
                );
                break;
            }

            validity.at(i) = confidence >= metaFaceTrackingConfidenceThreshold;
        }

        auto isValid = [&](const vector<XrFaceExpressionFB>& slots) -> bool
        {
            return all_of(
                slots.begin(),
                slots.end(),
                [&](XrFaceExpressionFB slot) -> bool
                {
                    return validity.at(slot);
                }
            );
        };

        if (isValid({
            XR_FACE_EXPRESSION_UPPER_LID_RAISER_L_FB
        }))
        {
            InputDimensions dimensions{};
            dimensions[2] = weights.at(XR_FACE_EXPRESSION_UPPER_LID_RAISER_L_FB);

            updateFacialPose(Input_VR_Left_Eye, dimensions);
        }

        if (isValid({
            XR_FACE_EXPRESSION_UPPER_LID_RAISER_R_FB
        }))
        {
            InputDimensions dimensions{};
            dimensions[2] = weights.at(XR_FACE_EXPRESSION_UPPER_LID_RAISER_R_FB);

            updateFacialPose(Input_VR_Right_Eye, dimensions);
        }

        if (isValid({
            XR_FACE_EXPRESSION_LID_TIGHTENER_L_FB,
            XR_FACE_EXPRESSION_EYES_CLOSED_L_FB
        }))
        {
            f64 leftSqueeze = weights.at(XR_FACE_EXPRESSION_LID_TIGHTENER_L_FB);
            f64 leftBlink = weights.at(XR_FACE_EXPRESSION_EYES_CLOSED_L_FB);

            f64 leftEyelid;

            if (leftSqueeze > 0)
            {
                leftEyelid = leftSqueeze * -1;
            }
            else
            {
                leftEyelid = 1 - leftBlink;
            }

            updateFacialPose(Input_VR_Left_Eyelid, leftEyelid);
        }

        if (isValid({
            XR_FACE_EXPRESSION_LID_TIGHTENER_R_FB,
            XR_FACE_EXPRESSION_EYES_CLOSED_R_FB
        }))
        {
            f64 rightSqueeze = weights.at(XR_FACE_EXPRESSION_LID_TIGHTENER_L_FB);
            f64 rightBlink = weights.at(XR_FACE_EXPRESSION_EYES_CLOSED_L_FB);

            f64 rightEyelid;

            if (rightSqueeze > 0)
            {
                rightEyelid = rightSqueeze * -1;
            }
            else
            {
                rightEyelid = 1 - rightBlink;
            }

            updateFacialPose(Input_VR_Right_Eyelid, rightEyelid);
        }

        if (isValid({
            XR_FACE_EXPRESSION_BROW_LOWERER_L_FB,
            XR_FACE_EXPRESSION_INNER_BROW_RAISER_L_FB,
            XR_FACE_EXPRESSION_OUTER_BROW_RAISER_L_FB
        }))
        {
            f64 eyebrowUp = max(
                weights.at(XR_FACE_EXPRESSION_INNER_BROW_RAISER_L_FB),
                weights.at(XR_FACE_EXPRESSION_OUTER_BROW_RAISER_L_FB)
            );
            f64 eyebrowDown = weights.at(XR_FACE_EXPRESSION_BROW_LOWERER_L_FB);

            f64 eyebrow = eyebrowDown > 0 ? eyebrowDown * -1 : eyebrowUp;

            updateFacialPose(Input_VR_Left_Eyebrow, eyebrow);
        }

        if (isValid({
            XR_FACE_EXPRESSION_BROW_LOWERER_R_FB,
            XR_FACE_EXPRESSION_INNER_BROW_RAISER_R_FB,
            XR_FACE_EXPRESSION_OUTER_BROW_RAISER_R_FB
        }))
        {
            f64 eyebrowUp = max(
                weights.at(XR_FACE_EXPRESSION_INNER_BROW_RAISER_R_FB),
                weights.at(XR_FACE_EXPRESSION_OUTER_BROW_RAISER_R_FB)
            );
            f64 eyebrowDown = weights.at(XR_FACE_EXPRESSION_BROW_LOWERER_R_FB);

            f64 eyebrow = eyebrowDown > 0 ? eyebrowDown * -1 : eyebrowUp;

            updateFacialPose(Input_VR_Right_Eyebrow, eyebrow);
        }

        if (isValid({
            XR_FACE_EXPRESSION_JAW_DROP_FB,
            XR_FACE_EXPRESSION_JAW_SIDEWAYS_LEFT_FB,
            XR_FACE_EXPRESSION_JAW_SIDEWAYS_RIGHT_FB,
            XR_FACE_EXPRESSION_JAW_THRUST_FB
        }))
        {
            f64 jawRight = weights.at(XR_FACE_EXPRESSION_JAW_SIDEWAYS_RIGHT_FB);
            f64 jawLeft = weights.at(XR_FACE_EXPRESSION_JAW_SIDEWAYS_LEFT_FB);
            f64 jawForward = weights.at(XR_FACE_EXPRESSION_JAW_THRUST_FB);
            f64 jawOpen = weights.at(XR_FACE_EXPRESSION_JAW_DROP_FB);

            vec3 jawPosition = {
                jawRight > 0 ? jawRight : jawLeft * -1,
                jawForward,
                0
            };

            updateFacialPose(Input_VR_Jaw, jawPosition, jawOpen);
        }

        if (isValid({
            XR_FACE_EXPRESSION_LIP_CORNER_DEPRESSOR_L_FB,
            XR_FACE_EXPRESSION_LIP_CORNER_DEPRESSOR_R_FB,
            XR_FACE_EXPRESSION_LIP_CORNER_PULLER_L_FB,
            XR_FACE_EXPRESSION_LIP_CORNER_PULLER_R_FB,
            XR_FACE_EXPRESSION_LIP_FUNNELER_LB_FB,
            XR_FACE_EXPRESSION_LIP_FUNNELER_LT_FB,
            XR_FACE_EXPRESSION_LIP_FUNNELER_RB_FB,
            XR_FACE_EXPRESSION_LIP_FUNNELER_RT_FB,
            XR_FACE_EXPRESSION_LIP_PRESSOR_L_FB,
            XR_FACE_EXPRESSION_LIP_PRESSOR_R_FB,
            XR_FACE_EXPRESSION_LIP_PUCKER_L_FB,
            XR_FACE_EXPRESSION_LIP_PUCKER_R_FB,
            XR_FACE_EXPRESSION_LIP_STRETCHER_L_FB,
            XR_FACE_EXPRESSION_LIP_STRETCHER_R_FB,
            XR_FACE_EXPRESSION_LIP_SUCK_LB_FB,
            XR_FACE_EXPRESSION_LIP_SUCK_LT_FB,
            XR_FACE_EXPRESSION_LIP_SUCK_RB_FB,
            XR_FACE_EXPRESSION_LIP_SUCK_RT_FB,
            XR_FACE_EXPRESSION_LIP_TIGHTENER_L_FB,
            XR_FACE_EXPRESSION_LIP_TIGHTENER_R_FB,
            XR_FACE_EXPRESSION_LIPS_TOWARD_FB,
            XR_FACE_EXPRESSION_LOWER_LIP_DEPRESSOR_L_FB,
            XR_FACE_EXPRESSION_LOWER_LIP_DEPRESSOR_R_FB,
            XR_FACE_EXPRESSION_MOUTH_LEFT_FB,
            XR_FACE_EXPRESSION_MOUTH_RIGHT_FB,
            XR_FACE_EXPRESSION_UPPER_LIP_RAISER_L_FB,
            XR_FACE_EXPRESSION_UPPER_LIP_RAISER_R_FB
        }))
        {
            f64 pout = max(
                weights.at(XR_FACE_EXPRESSION_LIP_PUCKER_L_FB),
                weights.at(XR_FACE_EXPRESSION_LIP_PUCKER_R_FB)
            );

            f64 mouthLeft = weights.at(XR_FACE_EXPRESSION_MOUTH_LEFT_FB);
            f64 mouthRight = weights.at(XR_FACE_EXPRESSION_MOUTH_RIGHT_FB);

            f64 upperOut = max(
                weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_LT_FB),
                weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_RT_FB)
            );
            f64 upperIn = max(
                weights.at(XR_FACE_EXPRESSION_LIP_SUCK_LT_FB),
                weights.at(XR_FACE_EXPRESSION_LIP_SUCK_RT_FB)
            );

            f64 lowerOut = max(
                weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_LB_FB),
                weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_RB_FB)
            );
            f64 lowerIn = max(
                weights.at(XR_FACE_EXPRESSION_LIP_SUCK_LB_FB),
                weights.at(XR_FACE_EXPRESSION_LIP_SUCK_RB_FB)
            );

            f64 leftCornerUp = weights.at(XR_FACE_EXPRESSION_LIP_CORNER_PULLER_L_FB);
            f64 leftCornerDown = weights.at(XR_FACE_EXPRESSION_LIP_CORNER_DEPRESSOR_L_FB);

            f64 rightCornerUp = weights.at(XR_FACE_EXPRESSION_LIP_CORNER_PULLER_R_FB);
            f64 rightCornerDown = weights.at(XR_FACE_EXPRESSION_LIP_CORNER_DEPRESSOR_R_FB);

            vec3 upperLip = {
                mouthRight > 0 ? mouthRight : mouthLeft * -1,
                upperIn > 0 ? upperIn * -1 : pout > 0 ? 0.5 + (pout / 2) : upperOut / 2,
                0
            };

            updateFacialPose(Input_VR_Upper_Lip, upperLip);

            vec3 lowerLip = {
                mouthRight > 0 ? mouthRight : mouthLeft * -1,
                lowerIn > 0 ? lowerIn * -1 : pout > 0 ? 0.5 + (pout / 2) : lowerOut / 2,
                0
            };

            updateFacialPose(Input_VR_Lower_Lip, lowerLip);

            f64 leftUpperLip = weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_LT_FB);
            f64 rightUpperLip = weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_RT_FB);
            f64 leftCorner = leftCornerUp > 0 ? leftCornerUp : leftCornerDown * -1;
            f64 rightCorner = rightCornerUp > 0 ? rightCornerUp : rightCornerDown * -1;
            f64 leftLowerLip = weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_LB_FB);
            f64 rightLowerLip = weights.at(XR_FACE_EXPRESSION_LIP_FUNNELER_RB_FB);

            updateFacialPose(Input_VR_Mouth, {
                leftUpperLip,
                rightUpperLip,
                leftCorner,
                rightCorner,
                leftLowerLip,
                rightLowerLip
            });
        }

        if (isValid({
            XR_FACE_EXPRESSION_CHEEK_PUFF_L_FB,
            XR_FACE_EXPRESSION_CHEEK_SUCK_L_FB
        }))
        {
            f64 cheekOut = weights.at(XR_FACE_EXPRESSION_CHEEK_PUFF_L_FB);
            f64 cheekIn = weights.at(XR_FACE_EXPRESSION_CHEEK_SUCK_L_FB);

            f64 cheek = cheekIn > 0 ? cheekIn * -1 : cheekOut;

            updateFacialPose(Input_VR_Left_Cheek, cheek);
        }

        if (isValid({
            XR_FACE_EXPRESSION_CHEEK_PUFF_R_FB,
            XR_FACE_EXPRESSION_CHEEK_SUCK_R_FB
        }))
        {
            f64 cheekOut = weights.at(XR_FACE_EXPRESSION_CHEEK_PUFF_R_FB);
            f64 cheekIn = weights.at(XR_FACE_EXPRESSION_CHEEK_SUCK_R_FB);

            f64 cheek = cheekIn > 0 ? cheekIn * -1 : cheekOut;

            updateFacialPose(Input_VR_Right_Cheek, cheek);
        }
    }
}
#endif

#if defined(XR_FB_face_tracking2)
void VRInput::checkMetaFaceTracking2()
{
    XrResult result;

    if (vr->metaFaceTracking() == 2)
    {
        XrFaceExpressionInfo2FB expressionInfo{};
        expressionInfo.type = XR_TYPE_FACE_EXPRESSION_INFO2_FB;
        expressionInfo.time = vr->predictedDisplayTime();

        vector<f32> weights(XR_FACE_EXPRESSION2_COUNT_FB, 0);
        vector<f32> confidences(XR_FACE_CONFIDENCE2_COUNT_FB, 0);

        XrFaceExpressionWeights2FB expressionWeights{};
        expressionWeights.type = XR_TYPE_FACE_EXPRESSION_WEIGHTS2_FB;
        expressionWeights.weightCount = weights.size();
        expressionWeights.weights = weights.data();
        expressionWeights.confidenceCount = confidences.size();
        expressionWeights.confidences = confidences.data();

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrGetFaceExpressionWeights2FB)(
            metaFaceTracker2,
            &expressionInfo,
            &expressionWeights
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to get Meta face expressions 2: " + openxrError(result));
        }

        vector<bool> validity(XR_FACE_EXPRESSION2_COUNT_FB, false);

        for (u32 i = 0; i < XR_FACE_EXPRESSION2_COUNT_FB; i++)
        {
            f32 confidence;

            switch (i)
            {
            case XR_FACE_EXPRESSION2_EYES_LOOK_DOWN_L_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_DOWN_R_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_LEFT_L_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_LEFT_R_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_RIGHT_L_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_RIGHT_R_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_UP_L_FB :
            case XR_FACE_EXPRESSION2_EYES_LOOK_UP_R_FB :
                if (!expressionWeights.isEyeFollowingBlendshapesValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE2_UPPER_FACE_FB);
                break;
            case XR_FACE_EXPRESSION2_BROW_LOWERER_L_FB :
            case XR_FACE_EXPRESSION2_BROW_LOWERER_R_FB :
            case XR_FACE_EXPRESSION2_EYES_CLOSED_L_FB :
            case XR_FACE_EXPRESSION2_EYES_CLOSED_R_FB :
            case XR_FACE_EXPRESSION2_INNER_BROW_RAISER_L_FB :
            case XR_FACE_EXPRESSION2_INNER_BROW_RAISER_R_FB :
            case XR_FACE_EXPRESSION2_OUTER_BROW_RAISER_L_FB :
            case XR_FACE_EXPRESSION2_OUTER_BROW_RAISER_R_FB :
            case XR_FACE_EXPRESSION2_UPPER_LID_RAISER_L_FB :
            case XR_FACE_EXPRESSION2_UPPER_LID_RAISER_R_FB :
            case XR_FACE_EXPRESSION2_LID_TIGHTENER_L_FB :
            case XR_FACE_EXPRESSION2_LID_TIGHTENER_R_FB :
                if (!expressionWeights.isValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE2_UPPER_FACE_FB);
                break;
            case XR_FACE_EXPRESSION2_JAW_DROP_FB :
            case XR_FACE_EXPRESSION2_JAW_SIDEWAYS_LEFT_FB :
            case XR_FACE_EXPRESSION2_JAW_SIDEWAYS_RIGHT_FB :
            case XR_FACE_EXPRESSION2_JAW_THRUST_FB :
            case XR_FACE_EXPRESSION2_LIP_CORNER_DEPRESSOR_L_FB :
            case XR_FACE_EXPRESSION2_LIP_CORNER_DEPRESSOR_R_FB :
            case XR_FACE_EXPRESSION2_LIP_CORNER_PULLER_L_FB :
            case XR_FACE_EXPRESSION2_LIP_CORNER_PULLER_R_FB :
            case XR_FACE_EXPRESSION2_LIP_FUNNELER_LB_FB :
            case XR_FACE_EXPRESSION2_LIP_FUNNELER_LT_FB :
            case XR_FACE_EXPRESSION2_LIP_FUNNELER_RB_FB :
            case XR_FACE_EXPRESSION2_LIP_FUNNELER_RT_FB :
            case XR_FACE_EXPRESSION2_LIP_PRESSOR_L_FB :
            case XR_FACE_EXPRESSION2_LIP_PRESSOR_R_FB :
            case XR_FACE_EXPRESSION2_LIP_PUCKER_L_FB :
            case XR_FACE_EXPRESSION2_LIP_PUCKER_R_FB :
            case XR_FACE_EXPRESSION2_LIP_STRETCHER_L_FB :
            case XR_FACE_EXPRESSION2_LIP_STRETCHER_R_FB :
            case XR_FACE_EXPRESSION2_LIP_SUCK_LB_FB :
            case XR_FACE_EXPRESSION2_LIP_SUCK_LT_FB :
            case XR_FACE_EXPRESSION2_LIP_SUCK_RB_FB :
            case XR_FACE_EXPRESSION2_LIP_SUCK_RT_FB :
            case XR_FACE_EXPRESSION2_LIP_TIGHTENER_L_FB :
            case XR_FACE_EXPRESSION2_LIP_TIGHTENER_R_FB :
            case XR_FACE_EXPRESSION2_LIPS_TOWARD_FB :
            case XR_FACE_EXPRESSION2_LOWER_LIP_DEPRESSOR_L_FB :
            case XR_FACE_EXPRESSION2_LOWER_LIP_DEPRESSOR_R_FB :
            case XR_FACE_EXPRESSION2_MOUTH_LEFT_FB :
            case XR_FACE_EXPRESSION2_MOUTH_RIGHT_FB :
            case XR_FACE_EXPRESSION2_UPPER_LIP_RAISER_L_FB :
            case XR_FACE_EXPRESSION2_UPPER_LIP_RAISER_R_FB :
            case XR_FACE_EXPRESSION2_CHIN_RAISER_B_FB :
            case XR_FACE_EXPRESSION2_CHIN_RAISER_T_FB :
                if (!expressionWeights.isValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE2_LOWER_FACE_FB);
                break;
            case XR_FACE_EXPRESSION2_DIMPLER_L_FB :
            case XR_FACE_EXPRESSION2_DIMPLER_R_FB :
            case XR_FACE_EXPRESSION2_NOSE_WRINKLER_L_FB :
            case XR_FACE_EXPRESSION2_NOSE_WRINKLER_R_FB :
            case XR_FACE_EXPRESSION2_CHEEK_PUFF_L_FB :
            case XR_FACE_EXPRESSION2_CHEEK_PUFF_R_FB :
            case XR_FACE_EXPRESSION2_CHEEK_RAISER_L_FB :
            case XR_FACE_EXPRESSION2_CHEEK_RAISER_R_FB :
            case XR_FACE_EXPRESSION2_CHEEK_SUCK_L_FB :
            case XR_FACE_EXPRESSION2_CHEEK_SUCK_R_FB :
                if (!expressionWeights.isValid)
                {
                    continue;
                }

                confidence = max(
                    confidences.at(XR_FACE_CONFIDENCE2_LOWER_FACE_FB),
                    confidences.at(XR_FACE_CONFIDENCE2_UPPER_FACE_FB)
                );
                break;
            case XR_FACE_EXPRESSION2_TONGUE_TIP_INTERDENTAL_FB :
            case XR_FACE_EXPRESSION2_TONGUE_TIP_ALVEOLAR_FB :
            case XR_FACE_EXPRESSION2_TONGUE_FRONT_DORSAL_PALATE_FB :
            case XR_FACE_EXPRESSION2_TONGUE_MID_DORSAL_PALATE_FB :
            case XR_FACE_EXPRESSION2_TONGUE_BACK_DORSAL_VELAR_FB :
            case XR_FACE_EXPRESSION2_TONGUE_OUT_FB :
            case XR_FACE_EXPRESSION2_TONGUE_RETREAT_FB :
                if (!expressionWeights.isValid)
                {
                    continue;
                }

                confidence = confidences.at(XR_FACE_CONFIDENCE2_LOWER_FACE_FB);
                break;
            }

            validity.at(i) = confidence >= metaFaceTrackingConfidenceThreshold;
        }

        auto isValid = [&](const vector<XrFaceExpression2FB>& slots) -> bool
        {
            return all_of(
                slots.begin(),
                slots.end(),
                [&](XrFaceExpression2FB slot) -> bool
                {
                    return validity.at(slot);
                }
            );
        };

        if (isValid({
            XR_FACE_EXPRESSION2_UPPER_LID_RAISER_L_FB
        }))
        {
            InputDimensions dimensions{};
            dimensions[2] = weights.at(XR_FACE_EXPRESSION2_UPPER_LID_RAISER_L_FB);

            updateFacialPose(Input_VR_Left_Eye, dimensions);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_UPPER_LID_RAISER_R_FB
        }))
        {
            InputDimensions dimensions{};
            dimensions[2] = weights.at(XR_FACE_EXPRESSION2_UPPER_LID_RAISER_R_FB);

            updateFacialPose(Input_VR_Right_Eye, dimensions);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_LID_TIGHTENER_L_FB,
            XR_FACE_EXPRESSION2_EYES_CLOSED_L_FB
        }))
        {
            f64 leftSqueeze = weights.at(XR_FACE_EXPRESSION2_LID_TIGHTENER_L_FB);
            f64 leftBlink = weights.at(XR_FACE_EXPRESSION2_EYES_CLOSED_L_FB);

            f64 leftEyelid;

            if (leftSqueeze > 0)
            {
                leftEyelid = leftSqueeze * -1;
            }
            else
            {
                leftEyelid = 1 - leftBlink;
            }

            updateFacialPose(Input_VR_Left_Eyelid, leftEyelid);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_LID_TIGHTENER_R_FB,
            XR_FACE_EXPRESSION2_EYES_CLOSED_R_FB
        }))
        {
            f64 rightSqueeze = weights.at(XR_FACE_EXPRESSION2_LID_TIGHTENER_L_FB);
            f64 rightBlink = weights.at(XR_FACE_EXPRESSION2_EYES_CLOSED_L_FB);

            f64 rightEyelid;

            if (rightSqueeze > 0)
            {
                rightEyelid = rightSqueeze * -1;
            }
            else
            {
                rightEyelid = 1 - rightBlink;
            }

            updateFacialPose(Input_VR_Right_Eyelid, rightEyelid);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_BROW_LOWERER_L_FB,
            XR_FACE_EXPRESSION2_INNER_BROW_RAISER_L_FB,
            XR_FACE_EXPRESSION2_OUTER_BROW_RAISER_L_FB
        }))
        {
            f64 eyebrowUp = max(
                weights.at(XR_FACE_EXPRESSION2_INNER_BROW_RAISER_L_FB),
                weights.at(XR_FACE_EXPRESSION2_OUTER_BROW_RAISER_L_FB)
            );
            f64 eyebrowDown = weights.at(XR_FACE_EXPRESSION2_BROW_LOWERER_L_FB);

            f64 eyebrow = eyebrowDown > 0 ? eyebrowDown * -1 : eyebrowUp;

            updateFacialPose(Input_VR_Left_Eyebrow, eyebrow);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_BROW_LOWERER_R_FB,
            XR_FACE_EXPRESSION2_INNER_BROW_RAISER_R_FB,
            XR_FACE_EXPRESSION2_OUTER_BROW_RAISER_R_FB
        }))
        {
            f64 eyebrowUp = max(
                weights.at(XR_FACE_EXPRESSION2_INNER_BROW_RAISER_R_FB),
                weights.at(XR_FACE_EXPRESSION2_OUTER_BROW_RAISER_R_FB)
            );
            f64 eyebrowDown = weights.at(XR_FACE_EXPRESSION2_BROW_LOWERER_R_FB);

            f64 eyebrow = eyebrowDown > 0 ? eyebrowDown * -1 : eyebrowUp;

            updateFacialPose(Input_VR_Right_Eyebrow, eyebrow);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_JAW_DROP_FB,
            XR_FACE_EXPRESSION2_JAW_SIDEWAYS_LEFT_FB,
            XR_FACE_EXPRESSION2_JAW_SIDEWAYS_RIGHT_FB,
            XR_FACE_EXPRESSION2_JAW_THRUST_FB
        }))
        {
            f64 jawRight = weights.at(XR_FACE_EXPRESSION2_JAW_SIDEWAYS_RIGHT_FB);
            f64 jawLeft = weights.at(XR_FACE_EXPRESSION2_JAW_SIDEWAYS_LEFT_FB);
            f64 jawForward = weights.at(XR_FACE_EXPRESSION2_JAW_THRUST_FB);
            f64 jawOpen = weights.at(XR_FACE_EXPRESSION2_JAW_DROP_FB);

            vec3 jawPosition = {
                jawRight > 0 ? jawRight : jawLeft * -1,
                jawForward,
                0
            };

            updateFacialPose(Input_VR_Jaw, jawPosition, jawOpen);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_LIP_CORNER_DEPRESSOR_L_FB,
            XR_FACE_EXPRESSION2_LIP_CORNER_DEPRESSOR_R_FB,
            XR_FACE_EXPRESSION2_LIP_CORNER_PULLER_L_FB,
            XR_FACE_EXPRESSION2_LIP_CORNER_PULLER_R_FB,
            XR_FACE_EXPRESSION2_LIP_FUNNELER_LB_FB,
            XR_FACE_EXPRESSION2_LIP_FUNNELER_LT_FB,
            XR_FACE_EXPRESSION2_LIP_FUNNELER_RB_FB,
            XR_FACE_EXPRESSION2_LIP_FUNNELER_RT_FB,
            XR_FACE_EXPRESSION2_LIP_PRESSOR_L_FB,
            XR_FACE_EXPRESSION2_LIP_PRESSOR_R_FB,
            XR_FACE_EXPRESSION2_LIP_PUCKER_L_FB,
            XR_FACE_EXPRESSION2_LIP_PUCKER_R_FB,
            XR_FACE_EXPRESSION2_LIP_STRETCHER_L_FB,
            XR_FACE_EXPRESSION2_LIP_STRETCHER_R_FB,
            XR_FACE_EXPRESSION2_LIP_SUCK_LB_FB,
            XR_FACE_EXPRESSION2_LIP_SUCK_LT_FB,
            XR_FACE_EXPRESSION2_LIP_SUCK_RB_FB,
            XR_FACE_EXPRESSION2_LIP_SUCK_RT_FB,
            XR_FACE_EXPRESSION2_LIP_TIGHTENER_L_FB,
            XR_FACE_EXPRESSION2_LIP_TIGHTENER_R_FB,
            XR_FACE_EXPRESSION2_LIPS_TOWARD_FB,
            XR_FACE_EXPRESSION2_LOWER_LIP_DEPRESSOR_L_FB,
            XR_FACE_EXPRESSION2_LOWER_LIP_DEPRESSOR_R_FB,
            XR_FACE_EXPRESSION2_MOUTH_LEFT_FB,
            XR_FACE_EXPRESSION2_MOUTH_RIGHT_FB,
            XR_FACE_EXPRESSION2_UPPER_LIP_RAISER_L_FB,
            XR_FACE_EXPRESSION2_UPPER_LIP_RAISER_R_FB
        }))
        {
            f64 pout = max(
                weights.at(XR_FACE_EXPRESSION2_LIP_PUCKER_L_FB),
                weights.at(XR_FACE_EXPRESSION2_LIP_PUCKER_R_FB)
            );

            f64 mouthLeft = weights.at(XR_FACE_EXPRESSION2_MOUTH_LEFT_FB);
            f64 mouthRight = weights.at(XR_FACE_EXPRESSION2_MOUTH_RIGHT_FB);

            f64 upperOut = max(
                weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_LT_FB),
                weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_RT_FB)
            );
            f64 upperIn = max(
                weights.at(XR_FACE_EXPRESSION2_LIP_SUCK_LT_FB),
                weights.at(XR_FACE_EXPRESSION2_LIP_SUCK_RT_FB)
            );

            f64 lowerOut = max(
                weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_LB_FB),
                weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_RB_FB)
            );
            f64 lowerIn = max(
                weights.at(XR_FACE_EXPRESSION2_LIP_SUCK_LB_FB),
                weights.at(XR_FACE_EXPRESSION2_LIP_SUCK_RB_FB)
            );

            f64 leftCornerUp = weights.at(XR_FACE_EXPRESSION2_LIP_CORNER_PULLER_L_FB);
            f64 leftCornerDown = weights.at(XR_FACE_EXPRESSION2_LIP_CORNER_DEPRESSOR_L_FB);

            f64 rightCornerUp = weights.at(XR_FACE_EXPRESSION2_LIP_CORNER_PULLER_R_FB);
            f64 rightCornerDown = weights.at(XR_FACE_EXPRESSION2_LIP_CORNER_DEPRESSOR_R_FB);

            vec3 upperLip = {
                mouthRight > 0 ? mouthRight : mouthLeft * -1,
                upperIn > 0 ? upperIn * -1 : pout > 0 ? 0.5 + (pout / 2) : upperOut / 2,
                0
            };

            updateFacialPose(Input_VR_Upper_Lip, upperLip);

            vec3 lowerLip = {
                mouthRight > 0 ? mouthRight : mouthLeft * -1,
                lowerIn > 0 ? lowerIn * -1 : pout > 0 ? 0.5 + (pout / 2) : lowerOut / 2,
                0
            };

            updateFacialPose(Input_VR_Lower_Lip, lowerLip);

            f64 leftUpperLip = weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_LT_FB);
            f64 rightUpperLip = weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_RT_FB);
            f64 leftCorner = leftCornerUp > 0 ? leftCornerUp : leftCornerDown * -1;
            f64 rightCorner = rightCornerUp > 0 ? rightCornerUp : rightCornerDown * -1;
            f64 leftLowerLip = weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_LB_FB);
            f64 rightLowerLip = weights.at(XR_FACE_EXPRESSION2_LIP_FUNNELER_RB_FB);

            updateFacialPose(Input_VR_Mouth, {
                leftUpperLip,
                rightUpperLip,
                leftCorner,
                rightCorner,
                leftLowerLip,
                rightLowerLip
            });
        }

        if (isValid({
            XR_FACE_EXPRESSION2_CHEEK_PUFF_L_FB,
            XR_FACE_EXPRESSION2_CHEEK_SUCK_L_FB
        }))
        {
            f64 cheekOut = weights.at(XR_FACE_EXPRESSION2_CHEEK_PUFF_L_FB);
            f64 cheekIn = weights.at(XR_FACE_EXPRESSION2_CHEEK_SUCK_L_FB);

            f64 cheek = cheekIn > 0 ? cheekIn * -1 : cheekOut;

            updateFacialPose(Input_VR_Left_Cheek, cheek);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_CHEEK_PUFF_R_FB,
            XR_FACE_EXPRESSION2_CHEEK_SUCK_R_FB
        }))
        {
            f64 cheekOut = weights.at(XR_FACE_EXPRESSION2_CHEEK_PUFF_R_FB);
            f64 cheekIn = weights.at(XR_FACE_EXPRESSION2_CHEEK_SUCK_R_FB);

            f64 cheek = cheekIn > 0 ? cheekIn * -1 : cheekOut;

            updateFacialPose(Input_VR_Right_Cheek, cheek);
        }

        if (isValid({
            XR_FACE_EXPRESSION2_TONGUE_TIP_INTERDENTAL_FB,
            XR_FACE_EXPRESSION2_TONGUE_TIP_ALVEOLAR_FB,
            XR_FACE_EXPRESSION2_TONGUE_FRONT_DORSAL_PALATE_FB,
            XR_FACE_EXPRESSION2_TONGUE_MID_DORSAL_PALATE_FB,
            XR_FACE_EXPRESSION2_TONGUE_BACK_DORSAL_VELAR_FB,
            XR_FACE_EXPRESSION2_TONGUE_OUT_FB,
            XR_FACE_EXPRESSION2_TONGUE_RETREAT_FB
        }))
        {
            f64 tipInterdental = weights.at(XR_FACE_EXPRESSION2_TONGUE_TIP_INTERDENTAL_FB);
            f64 tipAlveolar = weights.at(XR_FACE_EXPRESSION2_TONGUE_TIP_ALVEOLAR_FB);
            f64 frontDorsalPalate = weights.at(XR_FACE_EXPRESSION2_TONGUE_FRONT_DORSAL_PALATE_FB);
            f64 midDorsalPalate = weights.at(XR_FACE_EXPRESSION2_TONGUE_MID_DORSAL_PALATE_FB);
            f64 backDorsalVelar = weights.at(XR_FACE_EXPRESSION2_TONGUE_BACK_DORSAL_VELAR_FB);
            f64 tongueOut = weights.at(XR_FACE_EXPRESSION2_TONGUE_OUT_FB);
            f64 tongueRetreat = weights.at(XR_FACE_EXPRESSION2_TONGUE_RETREAT_FB);

            f64 tongueY = min<f64>(tipInterdental * 1 + tipAlveolar * 0.75 + frontDorsalPalate * 0.5 + midDorsalPalate * 0 + backDorsalVelar * -0.5 + tongueRetreat * -1, 1);
            f64 tongueZ = min<f64>(tipInterdental * 0.75 + tipAlveolar * 1 + frontDorsalPalate * 1 + midDorsalPalate * 1 + backDorsalVelar * 1 + tongueRetreat * -1, 1);

            vec3 tonguePosition = {
                0,
                tongueOut > 0 ? 0.5 + (tongueOut / 2) : tongueY / 2,
                tongueZ
            };

            updateFacialPose(Input_VR_Tongue, tonguePosition, 0);
        }
    }
}
#endif

#if defined(XR_FB_body_tracking)
void VRInput::checkMetaBodyTracking()
{
    XrResult result;

    if (vr->metaBodyTracking())
    {
        XrBodyJointsLocateInfoFB locateInfo{};
        locateInfo.type = XR_TYPE_BODY_JOINTS_LOCATE_INFO_FB;
        locateInfo.baseSpace = vr->roomSpace();
        locateInfo.time = vr->predictedDisplayTime();

        vector<XrBodyJointLocationFB> joints(XR_BODY_JOINT_COUNT_FB);

        XrBodyJointLocationsFB jointLocations{};
        jointLocations.type = XR_TYPE_BODY_JOINT_LOCATIONS_FB;
        jointLocations.jointCount = joints.size();
        jointLocations.jointLocations = joints.data();

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrLocateBodyJointsFB)(
            metaBodyTracker,
            &locateInfo,
            &jointLocations
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to get Meta body pose: " + openxrError(result));
        }

        if (jointLocations.isActive && jointLocations.confidence > metaBodyTrackingConfidenceThreshold)
        {
            for (u32 i = 0; i < XR_BODY_JOINT_COUNT_FB; i++)
            {
                const XrBodyJointLocationFB& joint = joints.at(i);

                if (!(joint.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) ||
                    !(joint.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT)
                )
                {
                    continue;
                }

                vec3 position = vr->roomToWorld({
                    joint.pose.position.x,
                    joint.pose.position.z * -1,
                    joint.pose.position.y
                });
                quaternion rotation = vr->roomToWorld({
                    joint.pose.orientation.w,
                    joint.pose.orientation.x,
                    joint.pose.orientation.z * -1,
                    joint.pose.orientation.y
                });

                Input input;
                input.position = position;
                input.rotation = rotation;

                switch (i)
                {
                case XR_BODY_JOINT_CHEST_FB :
                    input.type = Input_VR_Chest;
                    break;
                case XR_BODY_JOINT_HIPS_FB :
                    input.type = Input_VR_Hips;
                    break;
                case XR_BODY_JOINT_LEFT_SHOULDER_FB :
                    input.type = Input_VR_Left_Shoulder;
                    break;
                case XR_BODY_JOINT_RIGHT_SHOULDER_FB :
                    input.type = Input_VR_Right_Shoulder;
                    break;
                case XR_BODY_JOINT_LEFT_ARM_UPPER_FB :
                    input.type = Input_VR_Left_Elbow;
                    break;
                case XR_BODY_JOINT_RIGHT_ARM_UPPER_FB :
                    input.type = Input_VR_Right_Elbow;
                    break;
                case XR_BODY_JOINT_LEFT_HAND_WRIST_FB :
                    input.type = Input_VR_Left_Wrist;
                    break;
                case XR_BODY_JOINT_RIGHT_HAND_WRIST_FB :
                    input.type = Input_VR_Right_Wrist;
                    break;
                default :
                    continue;
                }

                controller->onInput(input);
            }
        }
    }
}
#endif

#if defined(XR_BD_body_tracking)
void VRInput::checkBDBodyTracking()
{
    XrResult result;

    if (vr->bdBodyTracking())
    {
        XrBodyJointsLocateInfoBD locateInfo{};
        locateInfo.type = XR_TYPE_BODY_JOINTS_LOCATE_INFO_BD;
        locateInfo.baseSpace = vr->roomSpace();
        locateInfo.time = vr->predictedDisplayTime();

        vector<XrBodyJointLocationBD> joints(XR_BODY_JOINT_COUNT_BD);

        XrBodyJointLocationsBD jointLocations{};
        jointLocations.type = XR_TYPE_BODY_JOINT_LOCATIONS_BD;
        jointLocations.jointCount = joints.size();
        jointLocations.jointLocations = joints.data();

        result = GET_XR_EXTENSION_FUNCTION(vr->instance(), xrLocateBodyJointsBD)(
            metaBodyTracker,
            &locateInfo,
            &jointLocations
        );

        if (result != XR_SUCCESS)
        {
            error("Failed to get ByteDance body pose: " + openxrError(result));
        }

        if (jointLocations.isActive && jointLocations.confidence > metaBodyTrackingConfidenceThreshold)
        {
            for (u32 i = 0; i < XR_BODY_JOINT_COUNT_BD; i++)
            {
                const XrBodyJointLocationBD& joint = joints.at(i);

                if (!(joint.locationFlags & XR_SPACE_LOCATION_POSITION_VALID_BIT) ||
                    !(joint.locationFlags & XR_SPACE_LOCATION_ORIENTATION_VALID_BIT)
                )
                {
                    continue;
                }

                vec3 position = vr->roomToWorld({
                    joint.pose.position.x,
                    joint.pose.position.z * -1,
                    joint.pose.position.y
                });
                quaternion rotation = vr->roomToWorld({
                    joint.pose.orientation.w,
                    joint.pose.orientation.x,
                    joint.pose.orientation.z * -1,
                    joint.pose.orientation.y
                });

                Input input;
                input.position = position;
                input.rotation = rotation;

                switch (i)
                {
                case XR_BODY_JOINT_PELVIS_BD :
                    input.type = Input_VR_Hips;
                    break;
                case XR_BODY_JOINT_LEFT_KNEE_BD :
                    input.type = Input_VR_Left_Knee;
                    break;
                case XR_BODY_JOINT_RIGHT_KNEE_BD :
                    input.type = Input_VR_Right_Knee;
                    break;
                case XR_BODY_JOINT_LEFT_ANKLE_BD :
                    input.type = Input_VR_Left_Ankle;
                    break;
                case XR_BODY_JOINT_RIGHT_ANKLE_BD :
                    input.type = Input_VR_Right_Ankle;
                    break;
                case XR_BODY_JOINT_SPINE3_BD :
                    input.type = Input_VR_Chest;
                    break;
                case XR_BODY_JOINT_LEFT_FOOT_BD :
                    input.type = Input_VR_Left_Foot;
                    break;
                case XR_BODY_JOINT_RIGHT_FOOT_BD :
                    input.type = Input_VR_Right_Foot;
                    break;
                case XR_BODY_JOINT_LEFT_SHOULDER_BD :
                    input.type = Input_VR_Left_Shoulder;
                    break;
                case XR_BODY_JOINT_RIGHT_SHOULDER_BD :
                    input.type = Input_VR_Right_Shoulder;
                    break;
                case XR_BODY_JOINT_LEFT_ELBOW_BD :
                    input.type = Input_VR_Left_Elbow;
                    break;
                case XR_BODY_JOINT_RIGHT_ELBOW_BD :
                    input.type = Input_VR_Right_Elbow;
                    break;
                case XR_BODY_JOINT_LEFT_WRIST_BD :
                    input.type = Input_VR_Left_Wrist;
                    break;
                case XR_BODY_JOINT_RIGHT_WRIST_BD :
                    input.type = Input_VR_Right_Wrist;
                    break;
                default :
                    continue;
                }

                controller->onInput(input);
            }
        }
    }
}
#endif

void VRInput::checkResize()
{
    u32 configViewCount = 2;
    vector<XrViewConfigurationView> configViews(
        configViewCount,
        { .type = XR_TYPE_VIEW_CONFIGURATION_VIEW }
    );

    XrResult result = xrEnumerateViewConfigurationViews(
        vr->instance(),
        vr->systemID(),
        XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
        configViewCount,
        &configViewCount,
        configViews.data()
    );

    if (result != XR_SUCCESS)
    {
        error("Failed to enumerate view configuration views: " + openxrError(result));
    }

    if (configViews[0].recommendedImageRectWidth != width || configViews[1].recommendedImageRectHeight != height)
    {
        width = configViews[0].recommendedImageRectWidth;
        height = configViews[0].recommendedImageRectHeight;

        controller->onResize(width, height);
    }
}

void VRInput::createPaths()
{
    if (vr->eyeTracking())
    {
        createEyeTrackingPaths();
    }
    createGenericTrackingPaths();
    if (vr->htcFullBodyTracking())
    {
        createHTCFullBodyTrackingPaths();
    }

    createGoogleDaydreamPaths();

    createHTCVivePaths();
    createHTCViveProPaths();
    if (vr->htcViveCosmos())
    {
        createHTCViveCosmosPaths();
    }
    if (vr->htcViveFocus3())
    {
        createHTCViveFocus3Paths();
    }
    if (vr->htcHandTracking())
    {
        createHTCHandPaths();
    }

    createMicrosoftMixedRealityMotionPaths();
    createMicrosoftXboxPaths();

    if (vr->microsoftHandTracking())
    {
        createMicrosoftHandPaths();
    }

    if (vr->hpMixedReality())
    {
        createHPMixedRealityPaths();
    }

    if (vr->huawei())
    {
        createHuaweiPaths();
    }

    createOculusGoPaths();
    createOculusTouchPaths();

    createValveIndexPaths();

    if (vr->magicLeap2())
    {
        createMagicLeap2Paths();
    }

    if (vr->pico())
    {
        createPicoNeo3Paths();
        createPico4Paths();
        createPicoG3Paths();
    }

    if (vr->questTouchPro())
    {
        createQuestTouchProPaths();
    }

    if (vr->questTouchPlus())
    {
        createQuestTouchPlusPaths();
    }

    if (vr->oppo())
    {
        createOppoPaths();
    }

    if (vr->yvr())
    {
        createYVRPaths();
    }

    if (vr->varjoXR4())
    {
        createVarjoXR4Paths();
    }
}

void VRInput::createEyeTrackingPaths()
{
    eyeTracking.eyesPath = makePath(vr->instance(), "/user/eyes_ext/input/gaze_ext/pose");

    eyeTracking.interactionProfile = makePath(vr->instance(), "/interaction_profiles/ext/eye_gaze_interaction");
}

void VRInput::createGenericTrackingPaths()
{
    genericTracking.leftHandPath = makePath(vr->instance(), "/user/hand/left/input/grip/pose");
    genericTracking.rightHandPath = makePath(vr->instance(), "/user/hand/right/input/grip/pose");

    genericTracking.interactionProfile = makePath(vr->instance(), "/interaction_profiles/khr/simple_controller");
}

void VRInput::createHTCFullBodyTrackingPaths()
{
    htcFullBodyTracking.waistPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/waist/input/grip/pose");
    htcFullBodyTracking.leftFootPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/left_foot/input/grip/pose");
    htcFullBodyTracking.rightFootPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/right_foot/input/grip/pose");
    htcFullBodyTracking.chestPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/chest/input/grip/pose");
    htcFullBodyTracking.leftShoulderPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/left_shoulder/input/grip/pose");
    htcFullBodyTracking.rightShoulderPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/right_shoulder/input/grip/pose");
    htcFullBodyTracking.leftElbowPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/left_elbow/input/grip/pose");
    htcFullBodyTracking.rightElbowPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/right_elbow/input/grip/pose");
    htcFullBodyTracking.leftWristPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/left_wrist/input/grip/pose");
    htcFullBodyTracking.rightWristPath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/right_wrist/input/grip/pose");
    htcFullBodyTracking.leftKneePath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/left_knee/input/grip/pose");
    htcFullBodyTracking.rightKneePath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/right_knee/input/grip/pose");
    htcFullBodyTracking.leftAnklePath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/left_ankle/input/grip/pose");
    htcFullBodyTracking.rightAnklePath = makePath(vr->instance(), "/user/vive_tracker_htcx/role/right_ankle/input/grip/pose");

    htcFullBodyTracking.interactionProfile = makePath(vr->instance(), "/interaction_profiles/htc/vive_tracker_htcx");
}

void VRInput::createGoogleDaydreamPaths()
{
    googleDaydream.selectClickPath = makePath(vr->instance(), "/user/hand/right/input/select/click");
    googleDaydream.touchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    googleDaydream.touchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    googleDaydream.touchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");
    googleDaydream.touchpadClickPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/click");

    googleDaydream.interactionProfile = makePath(vr->instance(), "/interaction_profiles/google/daydream_controller");
}

void VRInput::createHTCVivePaths()
{
    // Left
    htcVive.leftSystemClickPath = makePath(vr->instance(), "/user/hand/left/input/system/click");
    htcVive.leftSqueezeClickPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");
    htcVive.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    htcVive.leftTouchpadXPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/x");
    htcVive.leftTouchpadYPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/y");
    htcVive.leftTouchpadTouchPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/touch");
    htcVive.leftTouchpadClickPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/click");

    htcVive.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    htcVive.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    htcVive.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    htcVive.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    htcVive.rightSqueezeClickPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");
    htcVive.rightMenuClickPath = makePath(vr->instance(), "/user/hand/right/input/menu/click");

    htcVive.rightTouchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    htcVive.rightTouchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    htcVive.rightTouchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");
    htcVive.rightTouchpadClickPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/click");

    htcVive.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    htcVive.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    htcVive.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    htcVive.interactionProfile = makePath(vr->instance(), "/interaction_profiles/htc/vive_controller");
}

void VRInput::createHTCViveProPaths()
{
    // Head
    htcVivePro.systemClickPath = makePath(vr->instance(), "/user/head/input/system/click");
    htcVivePro.volumeUpClickPath = makePath(vr->instance(), "/user/head/input/volume_up/click");
    htcVivePro.volumeDownClickPath = makePath(vr->instance(), "/user/head/input/volume_down/click");
    htcVivePro.muteMicrophoneClickPath = makePath(vr->instance(), "/user/head/input/mute_mic/click");

    // Interaction Profile
    htcVivePro.interactionProfile = makePath(vr->instance(), "/interaction_profiles/htc/vive_pro");
}

void VRInput::createHTCViveCosmosPaths()
{
    // Left
    htcViveCosmos.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    htcViveCosmos.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");
    htcViveCosmos.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    htcViveCosmos.leftShoulderClickPath = makePath(vr->instance(), "/user/hand/left/input/shoulder/click");

    htcViveCosmos.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    htcViveCosmos.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    htcViveCosmos.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    htcViveCosmos.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    htcViveCosmos.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    htcViveCosmos.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    htcViveCosmos.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    htcViveCosmos.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    htcViveCosmos.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");
    htcViveCosmos.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    htcViveCosmos.rightShoulderClickPath = makePath(vr->instance(), "/user/hand/right/input/shoulder/click");

    htcViveCosmos.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    htcViveCosmos.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    htcViveCosmos.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    htcViveCosmos.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    htcViveCosmos.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    htcViveCosmos.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    htcViveCosmos.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    htcViveCosmos.interactionProfile = makePath(vr->instance(), "/interaction_profiles/htc/vive_cosmos_controller");
}

void VRInput::createHTCViveFocus3Paths()
{
    // Left
    htcViveFocus3.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    htcViveFocus3.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");
    htcViveFocus3.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    htcViveFocus3.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    htcViveFocus3.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    htcViveFocus3.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    htcViveFocus3.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    htcViveFocus3.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    htcViveFocus3.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    htcViveFocus3.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    htcViveFocus3.leftSqueezeClickPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");
    htcViveFocus3.leftSqueezeTouchPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/touch");

    htcViveFocus3.leftThumbrestTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbrest/touch");

    htcViveFocus3.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    htcViveFocus3.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    htcViveFocus3.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");
    htcViveFocus3.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");

    htcViveFocus3.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    htcViveFocus3.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    htcViveFocus3.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    htcViveFocus3.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    htcViveFocus3.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    htcViveFocus3.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    htcViveFocus3.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    htcViveFocus3.rightSqueezeClickPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");
    htcViveFocus3.rightSqueezeTouchPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/touch");

    htcViveFocus3.rightThumbrestTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbrest/touch");

    htcViveFocus3.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    htcViveFocus3.interactionProfile = makePath(vr->instance(), "/interaction_profiles/htc/vive_focus3_controller");
}

void VRInput::createHTCHandPaths()
{
    // Left
    htcHand.leftHandSelectPath = makePath(vr->instance(), "/user/hand/left/input/select/value");
    htcHand.leftHandSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    // Right
    htcHand.rightHandSelectPath = makePath(vr->instance(), "/user/hand/right/input/select/value");
    htcHand.rightHandSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    // Interaction Profile
    htcHand.interactionProfile = makePath(vr->instance(), "/interaction_profiles/htc/hand_interaction");
}

void VRInput::createMicrosoftMixedRealityMotionPaths()
{
    // Left
    microsoftMixedRealityMotion.leftSqueezeClickPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");
    microsoftMixedRealityMotion.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    microsoftMixedRealityMotion.leftTouchpadXPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/x");
    microsoftMixedRealityMotion.leftTouchpadYPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/y");
    microsoftMixedRealityMotion.leftTouchpadTouchPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/touch");
    microsoftMixedRealityMotion.leftTouchpadClickPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/click");

    microsoftMixedRealityMotion.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    microsoftMixedRealityMotion.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    microsoftMixedRealityMotion.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    microsoftMixedRealityMotion.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    microsoftMixedRealityMotion.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    microsoftMixedRealityMotion.rightSqueezeClickPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");
    microsoftMixedRealityMotion.rightMenuClickPath = makePath(vr->instance(), "/user/hand/right/input/menu/click");

    microsoftMixedRealityMotion.rightTouchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    microsoftMixedRealityMotion.rightTouchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    microsoftMixedRealityMotion.rightTouchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");
    microsoftMixedRealityMotion.rightTouchpadClickPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/click");

    microsoftMixedRealityMotion.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    microsoftMixedRealityMotion.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    microsoftMixedRealityMotion.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    microsoftMixedRealityMotion.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    microsoftMixedRealityMotion.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    microsoftMixedRealityMotion.interactionProfile = makePath(vr->instance(), "/interaction_profiles/microsoft/motion_controller");
}

void VRInput::createMicrosoftXboxPaths()
{
    microsoftXbox.dpadLeftClickPath = makePath(vr->instance(), "/user/gamepad/input/dpad_left/click");
    microsoftXbox.dpadRightClickPath = makePath(vr->instance(), "/user/gamepad/input/dpad_right/click");
    microsoftXbox.dpadUpClickPath = makePath(vr->instance(), "/user/gamepad/input/dpad_up/click");
    microsoftXbox.dpadDownClickPath = makePath(vr->instance(), "/user/gamepad/input/dpad_down/click");

    microsoftXbox.aClickPath = makePath(vr->instance(), "/user/gamepad/input/a/click");
    microsoftXbox.bClickPath = makePath(vr->instance(), "/user/gamepad/input/b/click");
    microsoftXbox.xClickPath = makePath(vr->instance(), "/user/gamepad/input/x/click");
    microsoftXbox.yClickPath = makePath(vr->instance(), "/user/gamepad/input/y/click");

    microsoftXbox.backClickPath = makePath(vr->instance(), "/user/gamepad/input/view/click");
    microsoftXbox.startClickPath = makePath(vr->instance(), "/user/gamepad/input/menu/click");

    // Left
    microsoftXbox.leftStickXPath = makePath(vr->instance(), "/user/gamepad/input/thumbstick_left/x");
    microsoftXbox.leftStickYPath = makePath(vr->instance(), "/user/gamepad/input/thumbstick_left/y");
    microsoftXbox.leftStickClickPath = makePath(vr->instance(), "/user/gamepad/input/thumbstick_left/click");
    microsoftXbox.leftShoulderClickPath = makePath(vr->instance(), "/user/gamepad/input/shoulder_left/click");
    microsoftXbox.leftTriggerSqueezePath = makePath(vr->instance(), "/user/gamepad/input/trigger_left/value");

    microsoftXbox.leftHapticPath = makePath(vr->instance(), "/user/gamepad/output/haptic_left");

    // Right
    microsoftXbox.rightStickXPath = makePath(vr->instance(), "/user/gamepad/input/thumbstick_right/x");
    microsoftXbox.rightStickYPath = makePath(vr->instance(), "/user/gamepad/input/thumbstick_right/y");
    microsoftXbox.rightStickClickPath = makePath(vr->instance(), "/user/gamepad/input/thumbstick_right/click");
    microsoftXbox.rightShoulderClickPath = makePath(vr->instance(), "/user/gamepad/input/shoulder_right/click");
    microsoftXbox.rightTriggerSqueezePath = makePath(vr->instance(), "/user/gamepad/input/trigger_right/value");

    microsoftXbox.rightHapticPath = makePath(vr->instance(), "/user/gamepad/output/haptic_right");

    microsoftXbox.interactionProfile = makePath(vr->instance(), "/interaction_profiles/microsoft/xbox_controller");
}

void VRInput::createMicrosoftHandPaths()
{
    // Left
    microsoftHand.leftHandSelectPath = makePath(vr->instance(), "/user/hand/left/input/select/value");
    microsoftHand.leftHandSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    // Right
    microsoftHand.rightHandSelectPath = makePath(vr->instance(), "/user/hand/right/input/select/value");
    microsoftHand.rightHandSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    // Interaction Profile
    microsoftHand.interactionProfile = makePath(vr->instance(), "/interaction_profiles/microsoft/hand_interaction");
}

void VRInput::createHPMixedRealityPaths()
{
    // Left
    hpMixedReality.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    hpMixedReality.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");
    hpMixedReality.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    hpMixedReality.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    hpMixedReality.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    hpMixedReality.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    hpMixedReality.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    hpMixedReality.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    hpMixedReality.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    hpMixedReality.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    hpMixedReality.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");
    hpMixedReality.rightMenuClickPath = makePath(vr->instance(), "/user/hand/right/input/menu/click");

    hpMixedReality.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    hpMixedReality.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    hpMixedReality.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    hpMixedReality.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    hpMixedReality.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    hpMixedReality.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    hpMixedReality.interactionProfile = makePath(vr->instance(), "/interaction_profiles/hp/mixed_reality_controller");
}

void VRInput::createHuaweiPaths()
{
    // Left
    huawei.leftHomeClickPath = makePath(vr->instance(), "/user/hand/left/input/home/click");
    huawei.leftBackClickPath = makePath(vr->instance(), "/user/hand/left/input/back/click");
    huawei.leftVolumeUpClickPath = makePath(vr->instance(), "/user/hand/left/input/volume_up/click");
    huawei.leftVolumeDownClickPath = makePath(vr->instance(), "/user/hand/left/input/volume_down/click");

    huawei.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    huawei.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    huawei.leftTouchpadXPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/x");
    huawei.leftTouchpadYPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/y");
    huawei.leftTouchpadTouchPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/click");
    huawei.leftTouchpadClickPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/touch");

    huawei.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    huawei.rightHomeClickPath = makePath(vr->instance(), "/user/hand/right/input/home/click");
    huawei.rightBackClickPath = makePath(vr->instance(), "/user/hand/right/input/back/click");
    huawei.rightVolumeUpClickPath = makePath(vr->instance(), "/user/hand/right/input/volume_up/click");
    huawei.rightVolumeDownClickPath = makePath(vr->instance(), "/user/hand/right/input/volume_down/click");

    huawei.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    huawei.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    huawei.rightTouchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    huawei.rightTouchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    huawei.rightTouchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/click");
    huawei.rightTouchpadClickPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");

    huawei.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    huawei.interactionProfile = makePath(vr->instance(), "/interaction_profiles/huawei/controller");
}

void VRInput::createOculusGoPaths()
{
    // Left
    oculusGo.leftSystemClickPath = makePath(vr->instance(), "/user/hand/left/input/system/click");
    oculusGo.leftBackClickPath = makePath(vr->instance(), "/user/hand/left/input/back/click");

    oculusGo.leftTouchpadXPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/x");
    oculusGo.leftTouchpadYPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/y");
    oculusGo.leftTouchpadTouchPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/touch");
    oculusGo.leftTouchpadClickPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/click");

    oculusGo.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    // Right
    oculusGo.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    oculusGo.rightBackClickPath = makePath(vr->instance(), "/user/hand/right/input/back/click");

    oculusGo.rightTouchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    oculusGo.rightTouchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    oculusGo.rightTouchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");
    oculusGo.rightTouchpadClickPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/click");

    oculusGo.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    // Interaction Profile
    oculusGo.interactionProfile = makePath(vr->instance(), "/interaction_profiles/oculus/go_controller");
}

void VRInput::createOculusTouchPaths()
{
    // Left
    oculusTouch.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    oculusTouch.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    oculusTouch.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    oculusTouch.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    oculusTouch.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    oculusTouch.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    oculusTouch.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    oculusTouch.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    oculusTouch.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    oculusTouch.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    oculusTouch.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    oculusTouch.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    oculusTouch.leftThumbrestTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbrest/touch");

    oculusTouch.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    oculusTouch.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    oculusTouch.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    oculusTouch.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    oculusTouch.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    oculusTouch.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    oculusTouch.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    oculusTouch.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    oculusTouch.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    oculusTouch.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    oculusTouch.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    oculusTouch.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    oculusTouch.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    oculusTouch.rightThumbrestTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbrest/touch");

    oculusTouch.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    oculusTouch.interactionProfile = makePath(vr->instance(), "/interaction_profiles/oculus/touch_controller");
}

void VRInput::createValveIndexPaths()
{
    // Left
    valveIndex.leftSystemTouchPath = makePath(vr->instance(), "/user/hand/right/input/system/touch");
    valveIndex.leftSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    valveIndex.leftATouchPath = makePath(vr->instance(), "/user/hand/left/input/a/touch");
    valveIndex.leftAClickPath = makePath(vr->instance(), "/user/hand/left/input/a/click");
    valveIndex.leftBTouchPath = makePath(vr->instance(), "/user/hand/left/input/b/touch");
    valveIndex.leftBClickPath = makePath(vr->instance(), "/user/hand/left/input/b/click");

    valveIndex.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    valveIndex.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    valveIndex.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    valveIndex.leftTouchpadXPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/x");
    valveIndex.leftTouchpadYPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/y");
    valveIndex.leftTouchpadTouchPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/touch");
    valveIndex.leftTouchpadSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trackpad/force");

    valveIndex.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    valveIndex.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    valveIndex.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    valveIndex.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    valveIndex.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/force");

    valveIndex.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    valveIndex.rightSystemTouchPath = makePath(vr->instance(), "/user/hand/right/input/system/touch");
    valveIndex.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    valveIndex.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    valveIndex.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    valveIndex.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    valveIndex.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    valveIndex.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    valveIndex.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    valveIndex.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    valveIndex.rightTouchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    valveIndex.rightTouchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    valveIndex.rightTouchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");
    valveIndex.rightTouchpadSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trackpad/force");

    valveIndex.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    valveIndex.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    valveIndex.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    valveIndex.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    valveIndex.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/force");

    valveIndex.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    valveIndex.interactionProfile = makePath(vr->instance(), "/interaction_profiles/valve/index_controller");
}

void VRInput::createMagicLeap2Paths()
{
    // Left
    magicLeap2.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    magicLeap2.leftHomeClickPath = makePath(vr->instance(), "/user/hand/left/input/home/click");

    magicLeap2.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    magicLeap2.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    magicLeap2.leftTouchpadXPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/x");
    magicLeap2.leftTouchpadYPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/y");
    magicLeap2.leftTouchpadTouchPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/touch");
    magicLeap2.leftTouchpadSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trackpad/force");
    magicLeap2.leftTouchpadClickPath = makePath(vr->instance(), "/user/hand/left/input/trackpad/click");

    magicLeap2.leftShoulderClickPath = makePath(vr->instance(), "/user/hand/left/input/shoulder/click");

    magicLeap2.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    magicLeap2.rightHomeClickPath = makePath(vr->instance(), "/user/hand/right/input/menu/click");
    magicLeap2.rightMenuClickPath = makePath(vr->instance(), "/user/hand/right/input/home/click");

    magicLeap2.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    magicLeap2.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    magicLeap2.rightTouchpadXPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/x");
    magicLeap2.rightTouchpadYPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/y");
    magicLeap2.rightTouchpadTouchPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/touch");
    magicLeap2.rightTouchpadSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trackpad/force");
    magicLeap2.rightTouchpadClickPath = makePath(vr->instance(), "/user/hand/right/input/trackpad/click");

    magicLeap2.rightShoulderClickPath = makePath(vr->instance(), "/user/hand/right/input/shoulder/click");

    magicLeap2.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    magicLeap2.interactionProfile = makePath(vr->instance(), "/interaction_profiles/ml/ml2_controller");
}

void VRInput::createPicoNeo3Paths()
{
    // Left
    picoNeo3.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    picoNeo3.leftSystemClickPath = makePath(vr->instance(), "/user/hand/left/input/system/click");

    picoNeo3.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    picoNeo3.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    picoNeo3.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    picoNeo3.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    picoNeo3.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    picoNeo3.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    picoNeo3.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    picoNeo3.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    picoNeo3.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    picoNeo3.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    picoNeo3.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    picoNeo3.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");
    picoNeo3.leftSqueezeClickPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");

    picoNeo3.leftHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    picoNeo3.rightMenuClickPath = makePath(vr->instance(), "/user/hand/right/input/menu/click");
    picoNeo3.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");

    picoNeo3.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    picoNeo3.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    picoNeo3.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    picoNeo3.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    picoNeo3.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    picoNeo3.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    picoNeo3.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    picoNeo3.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    picoNeo3.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    picoNeo3.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    picoNeo3.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    picoNeo3.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");
    picoNeo3.rightSqueezeClickPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");

    picoNeo3.rightHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    picoNeo3.interactionProfile = makePath(vr->instance(), "/interaction_profiles/bytedance/pico_neo3_controller");
}

void VRInput::createPico4Paths()
{
    // Left
    pico4.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    pico4.leftSystemClickPath = makePath(vr->instance(), "/user/hand/left/input/system/click");

    pico4.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    pico4.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    pico4.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    pico4.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    pico4.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    pico4.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");
    pico4.leftTriggerClickPath = makePath(vr->instance(), "/user/hand/left/input/trigger/click");

    pico4.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    pico4.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    pico4.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    pico4.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    pico4.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");
    pico4.leftSqueezeClickPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");

    pico4.leftHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    pico4.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");

    pico4.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    pico4.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    pico4.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    pico4.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    pico4.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    pico4.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    pico4.rightTriggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    pico4.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    pico4.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    pico4.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    pico4.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    pico4.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");
    pico4.rightSqueezeClickPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");

    pico4.rightHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    pico4.interactionProfile = makePath(vr->instance(), "/interaction_profiles/bytedance/pico4_controller");
}

void VRInput::createPicoG3Paths()
{
    picoG3.menuClickPath = makePath(vr->instance(), "/user/hand/right/input/menu/click");

    picoG3.triggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");
    picoG3.triggerClickPath = makePath(vr->instance(), "/user/hand/right/input/trigger/click");

    picoG3.stickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    picoG3.stickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    picoG3.stickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    picoG3.interactionProfile = makePath(vr->instance(), "/interaction_profiles/bytedance/pico_g3_controller");
}

void VRInput::createQuestTouchProPaths()
{
    // Left
    questTouchPro.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    questTouchPro.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    questTouchPro.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    questTouchPro.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    questTouchPro.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    questTouchPro.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    questTouchPro.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    questTouchPro.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    questTouchPro.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    questTouchPro.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    questTouchPro.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    questTouchPro.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    questTouchPro.leftThumbrestTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbrest/touch");
    questTouchPro.leftThumbrestSqueezePath = makePath(vr->instance(), "/user/hand/left/input/thumbrest/force");

    questTouchPro.leftHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    questTouchPro.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");

    questTouchPro.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    questTouchPro.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    questTouchPro.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    questTouchPro.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    questTouchPro.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    questTouchPro.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    questTouchPro.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    questTouchPro.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    questTouchPro.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    questTouchPro.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    questTouchPro.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    questTouchPro.rightThumbrestTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbrest/touch");
    questTouchPro.rightThumbrestSqueezePath = makePath(vr->instance(), "/user/hand/right/input/thumbrest/force");

    questTouchPro.rightHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    questTouchPro.interactionProfile = makePath(vr->instance(), "/interaction_profiles/facebook/touch_controller_pro");
}

void VRInput::createQuestTouchPlusPaths()
{
    // Left
    questTouchPlus.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    questTouchPlus.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    questTouchPlus.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    questTouchPlus.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    questTouchPlus.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    questTouchPlus.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    questTouchPlus.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    questTouchPlus.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    questTouchPlus.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    questTouchPlus.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    questTouchPlus.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    questTouchPlus.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    questTouchPlus.leftThumbrestTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbrest/touch");

    questTouchPlus.leftHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    questTouchPlus.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");

    questTouchPlus.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    questTouchPlus.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    questTouchPlus.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    questTouchPlus.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    questTouchPlus.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    questTouchPlus.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    questTouchPlus.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    questTouchPlus.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    questTouchPlus.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    questTouchPlus.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    questTouchPlus.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    questTouchPlus.rightThumbrestTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbrest/touch");

    questTouchPlus.rightHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    questTouchPlus.interactionProfile = makePath(vr->instance(), "/interaction_profiles/meta/touch_controller_plus");
}

void VRInput::createOppoPaths()
{
    oppo.heartratePath = makePath(vr->instance(), "/user/hand/left/input/heartrate_oppo/value");

    // Left
    oppo.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");

    oppo.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    oppo.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    oppo.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    oppo.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    oppo.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    oppo.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    oppo.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    oppo.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    oppo.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    oppo.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    oppo.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/value");

    oppo.leftHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    // Right
    oppo.rightHomeClickPath = makePath(vr->instance(), "/user/hand/right/input/home/click");

    oppo.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    oppo.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    oppo.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    oppo.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    oppo.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    oppo.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    oppo.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    oppo.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    oppo.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    oppo.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    oppo.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/value");

    oppo.rightHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    // Interaction Profile
    oppo.interactionProfile = makePath(vr->instance(), "/interaction_profiles/oppo/mr_controller_oppo");
}

void VRInput::createYVRPaths()
{
    yvr.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    yvr.leftXTouchPath = makePath(vr->instance(), "/user/hand/left/input/x/touch");
    yvr.leftXClickPath = makePath(vr->instance(), "/user/hand/left/input/x/click");
    yvr.leftYTouchPath = makePath(vr->instance(), "/user/hand/left/input/y/touch");
    yvr.leftYClickPath = makePath(vr->instance(), "/user/hand/left/input/y/click");

    yvr.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    yvr.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    yvr.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    yvr.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    yvr.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    yvr.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    yvr.leftSqueezePath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");

    yvr.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    yvr.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    yvr.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    yvr.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    yvr.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    yvr.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    yvr.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    yvr.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    yvr.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    yvr.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    yvr.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    yvr.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    yvr.rightSqueezePath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");

    yvr.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    yvr.interactionProfile = makePath(vr->instance(), "/interaction_profiles/yvr/touch_controller_yvr");
}

void VRInput::createVarjoXR4Paths()
{
    varjoXR4.leftMenuClickPath = makePath(vr->instance(), "/user/hand/left/input/menu/click");
    varjoXR4.leftATouchPath = makePath(vr->instance(), "/user/hand/left/input/a/touch");
    varjoXR4.leftAClickPath = makePath(vr->instance(), "/user/hand/left/input/a/click");
    varjoXR4.leftBTouchPath = makePath(vr->instance(), "/user/hand/left/input/b/touch");
    varjoXR4.leftBClickPath = makePath(vr->instance(), "/user/hand/left/input/b/click");

    varjoXR4.leftTriggerTouchPath = makePath(vr->instance(), "/user/hand/left/input/trigger/touch");
    varjoXR4.leftTriggerSqueezePath = makePath(vr->instance(), "/user/hand/left/input/trigger/value");

    varjoXR4.leftStickXPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/x");
    varjoXR4.leftStickYPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/y");
    varjoXR4.leftStickTouchPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/touch");
    varjoXR4.leftStickClickPath = makePath(vr->instance(), "/user/hand/left/input/thumbstick/click");

    varjoXR4.leftSqueezeTouchPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/touch");
    varjoXR4.leftSqueezeClickPath = makePath(vr->instance(), "/user/hand/left/input/squeeze/click");

    varjoXR4.leftHandHapticPath = makePath(vr->instance(), "/user/hand/left/output/haptic");

    varjoXR4.rightSystemClickPath = makePath(vr->instance(), "/user/hand/right/input/system/click");
    varjoXR4.rightATouchPath = makePath(vr->instance(), "/user/hand/right/input/a/touch");
    varjoXR4.rightAClickPath = makePath(vr->instance(), "/user/hand/right/input/a/click");
    varjoXR4.rightBTouchPath = makePath(vr->instance(), "/user/hand/right/input/b/touch");
    varjoXR4.rightBClickPath = makePath(vr->instance(), "/user/hand/right/input/b/click");

    varjoXR4.rightTriggerTouchPath = makePath(vr->instance(), "/user/hand/right/input/trigger/touch");
    varjoXR4.rightTriggerSqueezePath = makePath(vr->instance(), "/user/hand/right/input/trigger/value");

    varjoXR4.rightStickXPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/x");
    varjoXR4.rightStickYPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/y");
    varjoXR4.rightStickTouchPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/touch");
    varjoXR4.rightStickClickPath = makePath(vr->instance(), "/user/hand/right/input/thumbstick/click");

    varjoXR4.rightSqueezeTouchPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/touch");
    varjoXR4.rightSqueezeClickPath = makePath(vr->instance(), "/user/hand/right/input/squeeze/click");

    varjoXR4.rightHandHapticPath = makePath(vr->instance(), "/user/hand/right/output/haptic");

    varjoXR4.interactionProfile = makePath(vr->instance(), "/interaction_profiles/varjo/xr-4_controller");
}

void VRInput::createActions()
{
    createAction("heartrate", XR_ACTION_TYPE_FLOAT_INPUT, &heartrateAction);

    createAction("system-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &systemClickAction);
    createAction("volume-up-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &volumeUpClickAction);
    createAction("volume-down-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &volumeDownClickAction);
    createAction("mute-microphone-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &muteMicrophoneClickAction);

    createAction("eyes", XR_ACTION_TYPE_POSE_INPUT, &eyesAction);

    createAction("left-hand", XR_ACTION_TYPE_POSE_INPUT, &leftHandAction);
    createAction("right-hand", XR_ACTION_TYPE_POSE_INPUT, &rightHandAction);
    createAction("waist", XR_ACTION_TYPE_POSE_INPUT, &hipsAction);
    createAction("left-foot", XR_ACTION_TYPE_POSE_INPUT, &leftFootAction);
    createAction("right-foot", XR_ACTION_TYPE_POSE_INPUT, &rightFootAction);
    createAction("chest", XR_ACTION_TYPE_POSE_INPUT, &chestAction);
    createAction("left-shoulder", XR_ACTION_TYPE_POSE_INPUT, &leftShoulderAction);
    createAction("right-shoulder", XR_ACTION_TYPE_POSE_INPUT, &rightShoulderAction);
    createAction("left-elbow", XR_ACTION_TYPE_POSE_INPUT, &leftElbowAction);
    createAction("right-elbow", XR_ACTION_TYPE_POSE_INPUT, &rightElbowAction);
    createAction("left-wrist", XR_ACTION_TYPE_POSE_INPUT, &leftWristAction);
    createAction("right-wrist", XR_ACTION_TYPE_POSE_INPUT, &rightWristAction);
    createAction("left-knee", XR_ACTION_TYPE_POSE_INPUT, &leftKneeAction);
    createAction("right-knee", XR_ACTION_TYPE_POSE_INPUT, &rightKneeAction);
    createAction("left-ankle", XR_ACTION_TYPE_POSE_INPUT, &leftAnkleAction);
    createAction("right-ankle", XR_ACTION_TYPE_POSE_INPUT, &rightAnkleAction);

    createAction("left-system-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftSystemTouchAction);
    createAction("left-system-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftSystemClickAction);
    createAction("left-a-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftATouchAction);
    createAction("left-a-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftAClickAction);
    createAction("left-b-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftBTouchAction);
    createAction("left-b-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftBClickAction);
    createAction("left-menu-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftMenuTouchAction);
    createAction("left-menu-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftMenuClickAction);
    createAction("left-bumper-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftBumperTouchAction);
    createAction("left-bumper-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftBumperClickAction);
    createAction("left-thumbrest-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftThumbrestTouchAction);
    createAction("left-thumbrest-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &leftThumbrestSqueezeAction);

    createAction("left-trigger-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftTriggerTouchAction);
    createAction("left-trigger-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &leftTriggerSqueezeAction);
    createAction("left-trigger-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftTriggerClickAction);

    createAction("left-trackpad-x", XR_ACTION_TYPE_FLOAT_INPUT, &leftTouchpadXAction);
    createAction("left-trackpad-y", XR_ACTION_TYPE_FLOAT_INPUT, &leftTouchpadYAction);
    createAction("left-trackpad-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftTouchpadTouchAction);
    createAction("left-trackpad-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &leftTouchpadSqueezeAction);
    createAction("left-trackpad-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftTouchpadClickAction);

    createAction("left-stick-x", XR_ACTION_TYPE_FLOAT_INPUT, &leftStickXAction);
    createAction("left-stick-y", XR_ACTION_TYPE_FLOAT_INPUT, &leftStickYAction);
    createAction("left-stick-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftStickTouchAction);
    createAction("left-stick-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftStickClickAction);

    createAction("left-squeeze-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftSqueezeTouchAction);
    createAction("left-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &leftSqueezeAction);
    createAction("left-squeeze-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &leftSqueezeClickAction);

    createAction("left-hand-haptic", XR_ACTION_TYPE_VIBRATION_OUTPUT, &leftHandHapticAction);

    createAction("right-system-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightSystemTouchAction);
    createAction("right-system-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightSystemClickAction);
    createAction("right-a-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightATouchAction);
    createAction("right-a-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightAClickAction);
    createAction("right-b-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightBTouchAction);
    createAction("right-b-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightBClickAction);
    createAction("right-menu-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightMenuTouchAction);
    createAction("right-menu-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightMenuClickAction);
    createAction("right-bumper-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightBumperTouchAction);
    createAction("right-bumper-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightBumperClickAction);
    createAction("right-thumbrest-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightThumbrestTouchAction);
    createAction("right-thumbrest-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &rightThumbrestSqueezeAction);

    createAction("right-trigger-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightTriggerTouchAction);
    createAction("right-trigger-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &rightTriggerSqueezeAction);
    createAction("right-trigger-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightTriggerClickAction);

    createAction("right-trackpad-x", XR_ACTION_TYPE_FLOAT_INPUT, &rightTouchpadXAction);
    createAction("right-trackpad-y", XR_ACTION_TYPE_FLOAT_INPUT, &rightTouchpadYAction);
    createAction("right-trackpad-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightTouchpadTouchAction);
    createAction("right-trackpad-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &rightTouchpadSqueezeAction);
    createAction("right-trackpad-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightTouchpadClickAction);

    createAction("right-stick-x", XR_ACTION_TYPE_FLOAT_INPUT, &rightStickXAction);
    createAction("right-stick-y", XR_ACTION_TYPE_FLOAT_INPUT, &rightStickYAction);
    createAction("right-stick-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightStickTouchAction);
    createAction("right-stick-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightStickClickAction);

    createAction("right-squeeze-touch", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightSqueezeTouchAction);
    createAction("right-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &rightSqueezeAction);
    createAction("right-squeeze-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &rightSqueezeClickAction);

    createAction("right-hand-haptic", XR_ACTION_TYPE_VIBRATION_OUTPUT, &rightHandHapticAction);

    createAction("gamepad-dpad-left-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadDPadLeftClickAction);
    createAction("gamepad-dpad-right-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadDPadRightClickAction);
    createAction("gamepad-dpad-up-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadDPadUpClickAction);
    createAction("gamepad-dpad-down-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadDPadDownClickAction);

    createAction("gamepad-a-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadAClickAction);
    createAction("gamepad-b-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadBClickAction);
    createAction("gamepad-x-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadXClickAction);
    createAction("gamepad-y-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadYClickAction);

    createAction("gamepad-back-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadBackClickAction);
    createAction("gamepad-start-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadStartClickAction);

    createAction("gamepad-left-stick-x", XR_ACTION_TYPE_FLOAT_INPUT, &gamepadLeftStickXAction);
    createAction("gamepad-left-stick-y", XR_ACTION_TYPE_FLOAT_INPUT, &gamepadLeftStickYAction);
    createAction("gamepad-left-stick-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadLeftStickClickAction);
    createAction("gamepad-left-bumper-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadLeftBumperClickAction);
    createAction("gamepad-left-trigger-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &gamepadLeftTriggerSqueezeAction);

    createAction("gamepad-right-stick-x", XR_ACTION_TYPE_FLOAT_INPUT, &gamepadRightStickXAction);
    createAction("gamepad-right-stick-y", XR_ACTION_TYPE_FLOAT_INPUT, &gamepadRightStickYAction);
    createAction("gamepad-right-stick-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadRightStickClickAction);
    createAction("gamepad-right-bumper-click", XR_ACTION_TYPE_BOOLEAN_INPUT, &gamepadRightBumperClickAction);
    createAction("gamepad-right-trigger-squeeze", XR_ACTION_TYPE_FLOAT_INPUT, &gamepadRightTriggerSqueezeAction);
}

void VRInput::createAction(const string& name, XrActionType type, XrAction* action)
{
    XrResult result;

    XrActionCreateInfo actionCreateInfo{};
    actionCreateInfo.type = XR_TYPE_ACTION_CREATE_INFO;

    fillBuffer<XR_MAX_ACTION_NAME_SIZE>(actionCreateInfo.actionName, name.c_str());
    actionCreateInfo.actionType = type;
    fillBuffer<XR_MAX_LOCALIZED_ACTION_NAME_SIZE>(actionCreateInfo.localizedActionName, name.c_str());

    result = xrCreateAction(actionSet, &actionCreateInfo, action);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create action: " + openxrError(result));
    }
}

void VRInput::createActionSpaces()
{
    XrResult result;

    XrActionSpaceCreateInfo createInfo{};
    createInfo.type = XR_TYPE_ACTION_SPACE_CREATE_INFO;
    createInfo.poseInActionSpace.position = { 0, 0, 0 };
    createInfo.poseInActionSpace.orientation = { 0, 0, 0, 1 };

    createInfo.action = eyesAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &eyesSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    XrReferenceSpaceCreateInfo referenceSpaceCreateInfo{};
    referenceSpaceCreateInfo.type = XR_TYPE_REFERENCE_SPACE_CREATE_INFO;
    referenceSpaceCreateInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_VIEW;
    referenceSpaceCreateInfo.poseInReferenceSpace.position = { 0, 0, 0 };
    referenceSpaceCreateInfo.poseInReferenceSpace.orientation = { 0, 0, 0, 1 };

    result = xrCreateReferenceSpace(vr->session(), &referenceSpaceCreateInfo, &headSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftHandAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftHandSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightHandAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightHandSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = hipsAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &hipsSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftFootAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftFootSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightFootAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightFootSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = chestAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &chestSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftShoulderAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftShoulderSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightShoulderAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightShoulderSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftElbowAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftElbowSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightElbowAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightElbowSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftWristAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftWristSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightWristAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightWristSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftKneeAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftKneeSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightKneeAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightKneeSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = leftAnkleAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &leftAnkleSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }

    createInfo.action = rightAnkleAction;
    result = xrCreateActionSpace(vr->session(), &createInfo, &rightAnkleSpace);

    if (result != XR_SUCCESS)
    {
        fatal("Failed to create space: " + openxrError(result));
    }
}

void VRInput::suggestBindings()
{
    if (vr->eyeTracking())
    {
        suggestEyeTrackingBindings();
    }
    suggestGenericTrackingBindings();
    if (vr->htcFullBodyTracking())
    {
        suggestHTCFullBodyTrackingBindings();
    }

    suggestGoogleDaydreamBindings();

    suggestHTCViveBindings();
    suggestHTCViveProBindings();
    if (vr->htcViveCosmos())
    {
        suggestHTCViveCosmosBindings();
    }
    if (vr->htcViveFocus3())
    {
        suggestHTCViveFocus3Bindings();
    }
    if (vr->htcHandTracking())
    {
        suggestHTCHandBindings();
    }

    suggestMicrosoftMixedRealityMotionBindings();
    suggestMicrosoftXboxBindings();
    if (vr->microsoftHandTracking())
    {
        suggestMicrosoftHandBindings();
    }

    if (vr->hpMixedReality())
    {
        suggestHPMixedRealityBindings();
    }

    if (vr->huawei())
    {
        suggestHuaweiBindings();
    }

    suggestOculusGoBindings();
    suggestOculusTouchBindings();

    suggestValveIndexBindings();

    if (vr->magicLeap2())
    {
        suggestMagicLeap2Bindings();
    }

    if (vr->pico())
    {
        suggestPicoNeo3Bindings();
        suggestPico4Bindings();

        if (vr->pico() >= 2)
        {
            suggestPicoG3Bindings();
        }
    }

    if (vr->questTouchPro())
    {
        suggestQuestTouchProBindings();
    }

    if (vr->questTouchPlus())
    {
        suggestQuestTouchPlusBindings();
    }

    if (vr->oppo())
    {
        suggestOppoBindings();
    }

    if (vr->yvr())
    {
        suggestYVRBindings();
    }

    if (vr->varjoXR4())
    {
        suggestVarjoXR4Bindings();
    }
}

void VRInput::suggestEyeTrackingBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ eyesAction, eyeTracking.eyesPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = eyeTracking.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Eye Tracking bindings: " + openxrError(result));
    }
}

void VRInput::suggestGenericTrackingBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = genericTracking.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Generic Tracking bindings: " + openxrError(result));
    }
}

void VRInput::suggestHTCFullBodyTrackingBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ hipsAction, htcFullBodyTracking.waistPath });
    suggestedBindings.push_back({ leftFootAction, htcFullBodyTracking.leftFootPath });
    suggestedBindings.push_back({ rightFootAction, htcFullBodyTracking.rightFootPath });
    suggestedBindings.push_back({ chestAction, htcFullBodyTracking.chestPath });
    suggestedBindings.push_back({ leftShoulderAction, htcFullBodyTracking.leftShoulderPath });
    suggestedBindings.push_back({ rightShoulderAction, htcFullBodyTracking.rightShoulderPath });
    suggestedBindings.push_back({ leftElbowAction, htcFullBodyTracking.leftElbowPath });
    suggestedBindings.push_back({ rightElbowAction, htcFullBodyTracking.rightElbowPath });

    if (vr->htcFullBodyTracking() >= 3)
    {
        suggestedBindings.push_back({ leftWristAction, htcFullBodyTracking.leftWristPath });
        suggestedBindings.push_back({ rightWristAction, htcFullBodyTracking.rightWristPath });
        suggestedBindings.push_back({ leftKneeAction, htcFullBodyTracking.leftKneePath });
        suggestedBindings.push_back({ rightKneeAction, htcFullBodyTracking.rightKneePath });
        suggestedBindings.push_back({ leftAnkleAction, htcFullBodyTracking.leftAnklePath });
        suggestedBindings.push_back({ rightAnkleAction, htcFullBodyTracking.rightAnklePath });
    }

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = htcFullBodyTracking.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HTC Full Body Tracking bindings: " + openxrError(result));
    }
}

void VRInput::suggestGoogleDaydreamBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ rightBClickAction, googleDaydream.selectClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, googleDaydream.touchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, googleDaydream.touchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, googleDaydream.touchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadClickAction, googleDaydream.touchpadClickPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = googleDaydream.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Google Daydream bindings: " + openxrError(result));
    }
}

void VRInput::suggestHTCViveBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftSystemClickAction, htcVive.leftSystemClickPath });
    suggestedBindings.push_back({ leftAClickAction, htcVive.leftSqueezeClickPath });
    suggestedBindings.push_back({ leftBClickAction, htcVive.leftMenuClickPath });

    suggestedBindings.push_back({ leftTouchpadXAction, htcVive.leftTouchpadXPath });
    suggestedBindings.push_back({ leftTouchpadYAction, htcVive.leftTouchpadYPath });
    suggestedBindings.push_back({ leftTouchpadTouchAction, htcVive.leftTouchpadTouchPath });
    suggestedBindings.push_back({ leftTouchpadClickAction, htcVive.leftTouchpadClickPath });

    suggestedBindings.push_back({ leftTriggerSqueezeAction, htcVive.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, htcVive.leftTriggerClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, htcVive.leftHandHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, htcVive.rightSystemClickPath });
    suggestedBindings.push_back({ rightAClickAction, htcVive.rightSqueezeClickPath });
    suggestedBindings.push_back({ rightBClickAction, htcVive.rightMenuClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, htcVive.rightTouchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, htcVive.rightTouchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, htcVive.rightTouchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadClickAction, htcVive.rightTouchpadClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, htcVive.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, htcVive.rightTriggerClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, htcVive.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = htcVive.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HTC Vive bindings: " + openxrError(result));
    }
}

void VRInput::suggestHTCViveProBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ systemClickAction, htcVivePro.systemClickPath });
    suggestedBindings.push_back({ volumeUpClickAction, htcVivePro.volumeUpClickPath });
    suggestedBindings.push_back({ volumeDownClickAction, htcVivePro.volumeDownClickPath });
    suggestedBindings.push_back({ muteMicrophoneClickAction, htcVivePro.muteMicrophoneClickPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = htcVivePro.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HTC Vive Pro bindings: " + openxrError(result));
    }
}

void VRInput::suggestHTCViveCosmosBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftAClickAction, htcViveCosmos.leftXClickPath });
    suggestedBindings.push_back({ leftBClickAction, htcViveCosmos.leftYClickPath });
    suggestedBindings.push_back({ leftMenuClickAction, htcViveCosmos.leftMenuClickPath });
    suggestedBindings.push_back({ leftBumperClickAction, htcViveCosmos.leftShoulderClickPath });

    suggestedBindings.push_back({ leftStickXAction, htcViveCosmos.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, htcViveCosmos.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, htcViveCosmos.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, htcViveCosmos.leftStickClickPath });

    suggestedBindings.push_back({ leftTriggerSqueezeAction, htcViveCosmos.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, htcViveCosmos.leftTriggerClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, htcViveCosmos.leftHandHapticPath });

    suggestedBindings.push_back({ rightAClickAction, htcViveCosmos.rightAClickPath });
    suggestedBindings.push_back({ rightBClickAction, htcViveCosmos.rightBClickPath });
    suggestedBindings.push_back({ rightSystemClickAction, htcViveCosmos.rightSystemClickPath });
    suggestedBindings.push_back({ rightBumperClickAction, htcViveCosmos.rightShoulderClickPath });

    suggestedBindings.push_back({ rightStickXAction, htcViveCosmos.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, htcViveCosmos.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, htcViveCosmos.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, htcViveCosmos.rightStickClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, htcViveCosmos.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, htcViveCosmos.rightTriggerClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, htcViveCosmos.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = htcViveCosmos.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HTC Vive Cosmos bindings: " + openxrError(result));
    }
}

void VRInput::suggestHTCViveFocus3Bindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftAClickAction, htcViveFocus3.leftXClickPath });
    suggestedBindings.push_back({ leftBClickAction, htcViveFocus3.leftYClickPath });
    suggestedBindings.push_back({ leftMenuClickAction, htcViveFocus3.leftMenuClickPath });

    suggestedBindings.push_back({ leftStickXAction, htcViveFocus3.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, htcViveFocus3.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, htcViveFocus3.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, htcViveFocus3.leftStickClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, htcViveFocus3.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, htcViveFocus3.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, htcViveFocus3.leftTriggerClickPath });

    suggestedBindings.push_back({ leftSqueezeTouchAction, htcViveFocus3.leftSqueezeTouchPath });
    suggestedBindings.push_back({ leftSqueezeClickAction, htcViveFocus3.leftSqueezeClickPath });

    suggestedBindings.push_back({ leftThumbrestTouchAction, htcViveFocus3.leftThumbrestTouchPath });

    suggestedBindings.push_back({ leftHandHapticAction, htcViveFocus3.leftHandHapticPath });

    suggestedBindings.push_back({ rightAClickAction, htcViveFocus3.rightAClickPath });
    suggestedBindings.push_back({ rightBClickAction, htcViveFocus3.rightBClickPath });
    suggestedBindings.push_back({ rightSystemClickAction, htcViveFocus3.rightSystemClickPath });

    suggestedBindings.push_back({ rightStickXAction, htcViveFocus3.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, htcViveFocus3.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, htcViveFocus3.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, htcViveFocus3.rightStickClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, htcViveFocus3.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, htcViveFocus3.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, htcViveFocus3.rightTriggerClickPath });

    suggestedBindings.push_back({ rightSqueezeTouchAction, htcViveFocus3.rightSqueezeTouchPath });
    suggestedBindings.push_back({ rightSqueezeClickAction, htcViveFocus3.rightSqueezeClickPath });

    suggestedBindings.push_back({ rightThumbrestTouchAction, htcViveFocus3.rightThumbrestTouchPath });

    suggestedBindings.push_back({ rightHandHapticAction, htcViveFocus3.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = htcViveFocus3.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HTC Vive Focus 3 bindings: " + openxrError(result));
    }
}

void VRInput::suggestHTCHandBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftTriggerSqueezeAction, microsoftHand.leftHandSelectPath });
    suggestedBindings.push_back({ leftSqueezeAction, microsoftHand.leftHandSqueezePath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, microsoftHand.rightHandSelectPath });
    suggestedBindings.push_back({ rightSqueezeAction, microsoftHand.rightHandSqueezePath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = htcHand.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HTC hand bindings: " + openxrError(result));
    }
}

void VRInput::suggestMicrosoftMixedRealityMotionBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftAClickAction, microsoftMixedRealityMotion.leftSqueezeClickPath });
    suggestedBindings.push_back({ leftBClickAction, microsoftMixedRealityMotion.leftMenuClickPath });

    suggestedBindings.push_back({ leftTouchpadXAction, microsoftMixedRealityMotion.leftTouchpadXPath });
    suggestedBindings.push_back({ leftTouchpadYAction, microsoftMixedRealityMotion.leftTouchpadYPath });
    suggestedBindings.push_back({ leftTouchpadTouchAction, microsoftMixedRealityMotion.leftTouchpadTouchPath });
    suggestedBindings.push_back({ leftTouchpadClickAction, microsoftMixedRealityMotion.leftTouchpadClickPath });

    suggestedBindings.push_back({ leftStickXAction, microsoftMixedRealityMotion.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, microsoftMixedRealityMotion.leftStickYPath });
    suggestedBindings.push_back({ leftStickClickAction, microsoftMixedRealityMotion.leftStickClickPath });

    suggestedBindings.push_back({ leftTriggerSqueezeAction, microsoftMixedRealityMotion.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, microsoftMixedRealityMotion.leftHandHapticPath });

    suggestedBindings.push_back({ rightAClickAction, microsoftMixedRealityMotion.rightSqueezeClickPath });
    suggestedBindings.push_back({ rightBClickAction, microsoftMixedRealityMotion.rightMenuClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, microsoftMixedRealityMotion.rightTouchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, microsoftMixedRealityMotion.rightTouchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, microsoftMixedRealityMotion.rightTouchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadClickAction, microsoftMixedRealityMotion.rightTouchpadClickPath });

    suggestedBindings.push_back({ rightStickXAction, microsoftMixedRealityMotion.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, microsoftMixedRealityMotion.rightStickYPath });
    suggestedBindings.push_back({ rightStickClickAction, microsoftMixedRealityMotion.rightStickClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, microsoftMixedRealityMotion.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, microsoftMixedRealityMotion.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = microsoftMixedRealityMotion.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Microsoft Mixed Reality Motion bindings: " + openxrError(result));
    }
}

void VRInput::suggestMicrosoftXboxBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ gamepadDPadLeftClickAction, microsoftXbox.dpadLeftClickPath });
    suggestedBindings.push_back({ gamepadDPadRightClickAction, microsoftXbox.dpadRightClickPath });
    suggestedBindings.push_back({ gamepadDPadUpClickAction, microsoftXbox.dpadUpClickPath });
    suggestedBindings.push_back({ gamepadDPadDownClickAction, microsoftXbox.dpadDownClickPath });

    suggestedBindings.push_back({ gamepadAClickAction, microsoftXbox.aClickPath });
    suggestedBindings.push_back({ gamepadBClickAction, microsoftXbox.bClickPath });
    suggestedBindings.push_back({ gamepadXClickAction, microsoftXbox.xClickPath });
    suggestedBindings.push_back({ gamepadYClickAction, microsoftXbox.yClickPath });

    suggestedBindings.push_back({ gamepadBackClickAction, microsoftXbox.backClickPath });
    suggestedBindings.push_back({ gamepadStartClickAction, microsoftXbox.startClickPath });

    suggestedBindings.push_back({ gamepadLeftStickXAction, microsoftXbox.leftStickXPath });
    suggestedBindings.push_back({ gamepadLeftStickYAction, microsoftXbox.leftStickYPath });
    suggestedBindings.push_back({ gamepadLeftStickClickAction, microsoftXbox.leftStickClickPath });
    suggestedBindings.push_back({ gamepadLeftBumperClickAction, microsoftXbox.leftShoulderClickPath });
    suggestedBindings.push_back({ gamepadLeftTriggerSqueezeAction, microsoftXbox.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, microsoftXbox.leftHapticPath });

    suggestedBindings.push_back({ gamepadRightStickXAction, microsoftXbox.rightStickXPath });
    suggestedBindings.push_back({ gamepadRightStickYAction, microsoftXbox.rightStickYPath });
    suggestedBindings.push_back({ gamepadRightStickClickAction, microsoftXbox.rightStickClickPath });
    suggestedBindings.push_back({ gamepadRightBumperClickAction, microsoftXbox.rightShoulderClickPath });
    suggestedBindings.push_back({ gamepadRightTriggerSqueezeAction, microsoftXbox.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, microsoftXbox.rightHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = microsoftXbox.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Microsoft Xbox bindings: " + openxrError(result));
    }
}

void VRInput::suggestMicrosoftHandBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftTriggerSqueezeAction, microsoftHand.leftHandSelectPath });
    suggestedBindings.push_back({ leftSqueezeAction, microsoftHand.leftHandSqueezePath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, microsoftHand.rightHandSelectPath });
    suggestedBindings.push_back({ rightSqueezeAction, microsoftHand.rightHandSqueezePath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = microsoftHand.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Microsoft hand bindings: " + openxrError(result));
    }
}

void VRInput::suggestHPMixedRealityBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftAClickAction, hpMixedReality.leftXClickPath });
    suggestedBindings.push_back({ leftBClickAction, hpMixedReality.leftYClickPath });
    suggestedBindings.push_back({ leftMenuClickAction, hpMixedReality.leftMenuClickPath });

    suggestedBindings.push_back({ leftStickXAction, hpMixedReality.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, hpMixedReality.leftStickYPath });
    suggestedBindings.push_back({ leftStickClickAction, hpMixedReality.leftStickClickPath });

    suggestedBindings.push_back({ leftTriggerSqueezeAction, hpMixedReality.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftSqueezeAction, hpMixedReality.leftSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, hpMixedReality.leftHandHapticPath });

    suggestedBindings.push_back({ rightAClickAction, hpMixedReality.rightAClickPath });
    suggestedBindings.push_back({ rightBClickAction, hpMixedReality.rightBClickPath });
    suggestedBindings.push_back({ rightMenuClickAction, hpMixedReality.rightMenuClickPath });

    suggestedBindings.push_back({ rightStickXAction, hpMixedReality.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, hpMixedReality.rightStickYPath });
    suggestedBindings.push_back({ rightStickClickAction, hpMixedReality.rightStickClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, hpMixedReality.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightSqueezeAction, hpMixedReality.rightSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, hpMixedReality.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = hpMixedReality.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest HP Mixed Reality bindings: " + openxrError(result));
    }
}

void VRInput::suggestHuaweiBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftAClickAction, huawei.leftHomeClickPath });
    suggestedBindings.push_back({ leftBClickAction, huawei.leftBackClickPath });
    suggestedBindings.push_back({ volumeUpClickAction, huawei.leftVolumeUpClickPath });
    suggestedBindings.push_back({ volumeDownClickAction, huawei.leftVolumeDownClickPath });

    suggestedBindings.push_back({ leftTriggerSqueezeAction, huawei.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, huawei.leftTriggerClickPath });

    suggestedBindings.push_back({ leftTouchpadXAction, huawei.leftTouchpadXPath });
    suggestedBindings.push_back({ leftTouchpadYAction, huawei.leftTouchpadYPath });
    suggestedBindings.push_back({ leftTouchpadTouchAction, huawei.leftTouchpadTouchPath });
    suggestedBindings.push_back({ leftTouchpadClickAction, huawei.leftTouchpadClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, huawei.leftHandHapticPath });

    suggestedBindings.push_back({ rightAClickAction, huawei.rightHomeClickPath });
    suggestedBindings.push_back({ rightBClickAction, huawei.rightBackClickPath });
    suggestedBindings.push_back({ volumeUpClickAction, huawei.rightVolumeUpClickPath });
    suggestedBindings.push_back({ volumeDownClickAction, huawei.rightVolumeDownClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, huawei.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, huawei.rightTriggerClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, huawei.rightTouchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, huawei.rightTouchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, huawei.rightTouchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadClickAction, huawei.rightTouchpadClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, huawei.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = huawei.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Huawei bindings: " + openxrError(result));
    }
}

void VRInput::suggestOculusGoBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftSystemClickAction, oculusGo.leftSystemClickPath });
    suggestedBindings.push_back({ leftAClickAction, oculusGo.leftBackClickPath });

    suggestedBindings.push_back({ leftTouchpadXAction, oculusGo.leftTouchpadXPath });
    suggestedBindings.push_back({ leftTouchpadYAction, oculusGo.leftTouchpadYPath });
    suggestedBindings.push_back({ leftTouchpadTouchAction, oculusGo.leftTouchpadTouchPath });
    suggestedBindings.push_back({ leftTouchpadClickAction, oculusGo.leftTouchpadClickPath });

    suggestedBindings.push_back({ leftTriggerClickAction, oculusGo.leftTriggerClickPath });

    suggestedBindings.push_back({ rightSystemClickAction, oculusGo.rightSystemClickPath });
    suggestedBindings.push_back({ rightAClickAction, oculusGo.rightBackClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, oculusGo.rightTouchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, oculusGo.rightTouchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, oculusGo.rightTouchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadClickAction, oculusGo.rightTouchpadClickPath });

    suggestedBindings.push_back({ rightTriggerClickAction, oculusGo.rightTriggerClickPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = oculusGo.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Oculus Go bindings: " + openxrError(result));
    }
}

void VRInput::suggestOculusTouchBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftMenuClickAction, oculusTouch.leftMenuClickPath });
    suggestedBindings.push_back({ leftATouchAction, oculusTouch.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, oculusTouch.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, oculusTouch.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, oculusTouch.leftYClickPath });

    suggestedBindings.push_back({ leftStickXAction, oculusTouch.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, oculusTouch.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, oculusTouch.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, oculusTouch.leftStickClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, oculusTouch.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, oculusTouch.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftSqueezeAction, oculusTouch.leftSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, oculusTouch.leftHandHapticPath });

    suggestedBindings.push_back({ leftThumbrestTouchAction, oculusTouch.leftThumbrestTouchPath });

    suggestedBindings.push_back({ rightSystemClickAction, oculusTouch.rightSystemClickPath });
    suggestedBindings.push_back({ rightATouchAction, oculusTouch.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, oculusTouch.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, oculusTouch.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, oculusTouch.rightBClickPath });

    suggestedBindings.push_back({ rightStickXAction, oculusTouch.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, oculusTouch.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, oculusTouch.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, oculusTouch.rightStickClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, oculusTouch.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, oculusTouch.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightSqueezeAction, oculusTouch.rightSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, oculusTouch.rightHandHapticPath });

    suggestedBindings.push_back({ rightThumbrestTouchAction, oculusTouch.rightThumbrestTouchPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = oculusTouch.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Oculus Touch bindings: " + openxrError(result));
    }
}

void VRInput::suggestValveIndexBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftSystemTouchAction, valveIndex.leftSystemTouchPath });
    suggestedBindings.push_back({ leftSystemClickAction, valveIndex.leftSystemClickPath });
    suggestedBindings.push_back({ leftATouchAction, valveIndex.leftATouchPath });
    suggestedBindings.push_back({ leftAClickAction, valveIndex.leftAClickPath });
    suggestedBindings.push_back({ leftBTouchAction, valveIndex.leftBTouchPath });
    suggestedBindings.push_back({ leftBClickAction, valveIndex.leftBClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, valveIndex.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, valveIndex.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, valveIndex.leftTriggerClickPath });

    suggestedBindings.push_back({ leftTouchpadXAction, valveIndex.leftTouchpadXPath });
    suggestedBindings.push_back({ leftTouchpadYAction, valveIndex.leftTouchpadYPath });
    suggestedBindings.push_back({ leftTouchpadTouchAction, valveIndex.leftTouchpadTouchPath });
    suggestedBindings.push_back({ leftTouchpadSqueezeAction, valveIndex.leftTouchpadSqueezePath });

    suggestedBindings.push_back({ leftStickXAction, valveIndex.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, valveIndex.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, valveIndex.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, valveIndex.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeAction, valveIndex.leftSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, valveIndex.leftHandHapticPath });

    suggestedBindings.push_back({ rightSystemTouchAction, valveIndex.rightSystemTouchPath });
    suggestedBindings.push_back({ rightSystemClickAction, valveIndex.rightSystemClickPath });
    suggestedBindings.push_back({ rightATouchAction, valveIndex.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, valveIndex.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, valveIndex.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, valveIndex.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, valveIndex.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, valveIndex.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, valveIndex.rightTriggerClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, valveIndex.rightTouchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, valveIndex.rightTouchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, valveIndex.rightTouchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadSqueezeAction, valveIndex.rightTouchpadSqueezePath });

    suggestedBindings.push_back({ rightStickXAction, valveIndex.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, valveIndex.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, valveIndex.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, valveIndex.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeAction, valveIndex.rightSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, valveIndex.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = valveIndex.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Valve Index bindings: " + openxrError(result));
    }
}

void VRInput::suggestMagicLeap2Bindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftAClickAction, magicLeap2.leftMenuClickPath });
    suggestedBindings.push_back({ leftSystemClickAction, magicLeap2.leftHomeClickPath });

    suggestedBindings.push_back({ leftTriggerSqueezeAction, magicLeap2.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, magicLeap2.leftTriggerClickPath });

    suggestedBindings.push_back({ leftTouchpadXAction, magicLeap2.leftTouchpadXPath });
    suggestedBindings.push_back({ leftTouchpadYAction, magicLeap2.leftTouchpadYPath });
    suggestedBindings.push_back({ leftTouchpadTouchAction, magicLeap2.leftTouchpadTouchPath });
    suggestedBindings.push_back({ leftTouchpadSqueezeAction, magicLeap2.leftTouchpadSqueezePath });
    suggestedBindings.push_back({ leftTouchpadClickAction, magicLeap2.leftTouchpadClickPath });

    suggestedBindings.push_back({ leftBumperClickAction, magicLeap2.leftShoulderClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, magicLeap2.leftHandHapticPath });

    suggestedBindings.push_back({ rightAClickAction, magicLeap2.rightMenuClickPath });
    suggestedBindings.push_back({ rightSystemClickAction, magicLeap2.rightHomeClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, magicLeap2.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, magicLeap2.rightTriggerClickPath });

    suggestedBindings.push_back({ rightTouchpadXAction, magicLeap2.rightTouchpadXPath });
    suggestedBindings.push_back({ rightTouchpadYAction, magicLeap2.rightTouchpadYPath });
    suggestedBindings.push_back({ rightTouchpadTouchAction, magicLeap2.rightTouchpadTouchPath });
    suggestedBindings.push_back({ rightTouchpadSqueezeAction, magicLeap2.rightTouchpadSqueezePath });
    suggestedBindings.push_back({ rightTouchpadClickAction, magicLeap2.rightTouchpadClickPath });

    suggestedBindings.push_back({ rightBumperClickAction, magicLeap2.rightShoulderClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, magicLeap2.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = magicLeap2.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Magic Leap 2 bindings: " + openxrError(result));
    }
}

void VRInput::suggestPicoNeo3Bindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftMenuClickAction, picoNeo3.leftMenuClickPath });
    suggestedBindings.push_back({ leftSystemClickAction, picoNeo3.leftSystemClickPath });

    suggestedBindings.push_back({ leftATouchAction, picoNeo3.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, picoNeo3.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, picoNeo3.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, picoNeo3.leftYClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, picoNeo3.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, picoNeo3.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, picoNeo3.leftTriggerClickPath });

    suggestedBindings.push_back({ leftStickXAction, picoNeo3.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, picoNeo3.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, picoNeo3.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, picoNeo3.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeAction, picoNeo3.leftSqueezePath });
    suggestedBindings.push_back({ leftSqueezeClickAction, picoNeo3.leftSqueezeClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, picoNeo3.leftHapticPath });

    suggestedBindings.push_back({ rightMenuClickAction, picoNeo3.rightMenuClickPath });
    suggestedBindings.push_back({ rightSystemClickAction, picoNeo3.rightSystemClickPath });

    suggestedBindings.push_back({ rightATouchAction, picoNeo3.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, picoNeo3.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, picoNeo3.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, picoNeo3.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, picoNeo3.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, picoNeo3.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, picoNeo3.rightTriggerClickPath });

    suggestedBindings.push_back({ rightStickXAction, picoNeo3.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, picoNeo3.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, picoNeo3.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, picoNeo3.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeAction, picoNeo3.rightSqueezePath });
    suggestedBindings.push_back({ rightSqueezeClickAction, picoNeo3.rightSqueezeClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, picoNeo3.rightHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = picoNeo3.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Pico Neo3 bindings: " + openxrError(result));
    }
}

void VRInput::suggestPico4Bindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftATouchAction, pico4.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, pico4.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, pico4.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, pico4.leftYClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, pico4.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, pico4.leftTriggerSqueezePath });
    suggestedBindings.push_back({ leftTriggerClickAction, pico4.leftTriggerClickPath });

    suggestedBindings.push_back({ leftStickXAction, pico4.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, pico4.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, pico4.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, pico4.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeAction, pico4.leftSqueezePath });
    suggestedBindings.push_back({ leftSqueezeClickAction, pico4.leftSqueezeClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, pico4.leftHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, pico4.rightSystemClickPath });

    suggestedBindings.push_back({ rightATouchAction, pico4.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, pico4.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, pico4.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, pico4.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, pico4.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, pico4.rightTriggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, pico4.rightTriggerClickPath });

    suggestedBindings.push_back({ rightStickXAction, pico4.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, pico4.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, pico4.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, pico4.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeAction, pico4.rightSqueezePath });
    suggestedBindings.push_back({ rightSqueezeClickAction, pico4.rightSqueezeClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, pico4.rightHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = pico4.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Pico 4 bindings: " + openxrError(result));
    }
}

void VRInput::suggestPicoG3Bindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ rightMenuClickAction, picoG3.menuClickPath });

    suggestedBindings.push_back({ rightTriggerSqueezeAction, picoG3.triggerSqueezePath });
    suggestedBindings.push_back({ rightTriggerClickAction, picoG3.triggerClickPath });

    suggestedBindings.push_back({ rightStickXAction, picoG3.stickXPath });
    suggestedBindings.push_back({ rightStickYAction, picoG3.stickYPath });
    suggestedBindings.push_back({ rightStickClickAction, picoG3.stickClickPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = picoG3.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Pico G3 bindings: " + openxrError(result));
    }
}

void VRInput::suggestQuestTouchProBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftMenuClickAction, questTouchPro.leftMenuClickPath });

    suggestedBindings.push_back({ leftATouchAction, questTouchPro.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, questTouchPro.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, questTouchPro.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, questTouchPro.leftYClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, questTouchPro.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, questTouchPro.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftStickXAction, questTouchPro.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, questTouchPro.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, questTouchPro.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, questTouchPro.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeAction, questTouchPro.leftSqueezePath });

    suggestedBindings.push_back({ leftThumbrestTouchAction, questTouchPro.leftThumbrestTouchPath });
    suggestedBindings.push_back({ leftThumbrestSqueezeAction, questTouchPro.leftThumbrestSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, questTouchPro.leftHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, questTouchPro.rightSystemClickPath });

    suggestedBindings.push_back({ rightATouchAction, questTouchPro.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, questTouchPro.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, questTouchPro.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, questTouchPro.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, questTouchPro.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, questTouchPro.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightStickXAction, questTouchPro.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, questTouchPro.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, questTouchPro.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, questTouchPro.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeAction, questTouchPro.rightSqueezePath });

    suggestedBindings.push_back({ rightThumbrestTouchAction, questTouchPro.rightThumbrestTouchPath });
    suggestedBindings.push_back({ rightThumbrestSqueezeAction, questTouchPro.rightThumbrestSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, questTouchPro.rightHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = questTouchPro.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Meta Quest Touch Pro bindings: " + openxrError(result));
    }
}

void VRInput::suggestQuestTouchPlusBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftMenuClickAction, questTouchPlus.leftMenuClickPath });

    suggestedBindings.push_back({ leftATouchAction, questTouchPlus.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, questTouchPlus.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, questTouchPlus.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, questTouchPlus.leftYClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, questTouchPlus.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, questTouchPlus.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftStickXAction, questTouchPlus.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, questTouchPlus.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, questTouchPlus.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, questTouchPlus.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeAction, questTouchPlus.leftSqueezePath });

    suggestedBindings.push_back({ leftThumbrestTouchAction, questTouchPlus.leftThumbrestTouchPath });

    suggestedBindings.push_back({ leftHandHapticAction, questTouchPlus.leftHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, questTouchPlus.rightSystemClickPath });

    suggestedBindings.push_back({ rightATouchAction, questTouchPlus.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, questTouchPlus.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, questTouchPlus.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, questTouchPlus.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, questTouchPlus.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, questTouchPlus.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightStickXAction, questTouchPlus.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, questTouchPlus.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, questTouchPlus.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, questTouchPlus.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeAction, questTouchPlus.rightSqueezePath });

    suggestedBindings.push_back({ rightThumbrestTouchAction, questTouchPlus.rightThumbrestTouchPath });

    suggestedBindings.push_back({ rightHandHapticAction, questTouchPlus.rightHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = questTouchPlus.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Meta Quest Touch Plus bindings: " + openxrError(result));
    }
}

void VRInput::suggestOppoBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ heartrateAction, oppo.heartratePath });

    suggestedBindings.push_back({ leftMenuClickAction, oppo.leftMenuClickPath });

    suggestedBindings.push_back({ leftATouchAction, oppo.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, oppo.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, oppo.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, oppo.leftYClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, oppo.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, oppo.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftStickXAction, oppo.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, oppo.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, oppo.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, oppo.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeAction, oppo.leftSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, oppo.leftHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, oppo.rightHomeClickPath });

    suggestedBindings.push_back({ rightATouchAction, oppo.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, oppo.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, oppo.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, oppo.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, oppo.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, oppo.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightStickXAction, oppo.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, oppo.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, oppo.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, oppo.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeAction, oppo.rightSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, oppo.rightHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = oppo.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Oppo bindings: " + openxrError(result));
    }
}

void VRInput::suggestYVRBindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftMenuClickAction, yvr.leftMenuClickPath });
    suggestedBindings.push_back({ leftATouchAction, yvr.leftXTouchPath });
    suggestedBindings.push_back({ leftAClickAction, yvr.leftXClickPath });
    suggestedBindings.push_back({ leftBTouchAction, yvr.leftYTouchPath });
    suggestedBindings.push_back({ leftBClickAction, yvr.leftYClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, yvr.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, yvr.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftStickXAction, yvr.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, yvr.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, yvr.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, yvr.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeClickAction, yvr.leftSqueezePath });

    suggestedBindings.push_back({ leftHandHapticAction, yvr.leftHandHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, yvr.rightSystemClickPath });
    suggestedBindings.push_back({ rightATouchAction, yvr.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, yvr.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, yvr.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, yvr.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, yvr.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, yvr.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightStickXAction, yvr.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, yvr.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, yvr.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, yvr.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeClickAction, yvr.rightSqueezePath });

    suggestedBindings.push_back({ rightHandHapticAction, yvr.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = yvr.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest YVR bindings: " + openxrError(result));
    }
}

void VRInput::suggestVarjoXR4Bindings()
{
    vector<XrActionSuggestedBinding> suggestedBindings;

    suggestedBindings.push_back({ leftHandAction, genericTracking.leftHandPath });
    suggestedBindings.push_back({ rightHandAction, genericTracking.rightHandPath });

    suggestedBindings.push_back({ leftMenuClickAction, varjoXR4.leftMenuClickPath });
    suggestedBindings.push_back({ leftATouchAction, varjoXR4.leftATouchPath });
    suggestedBindings.push_back({ leftAClickAction, varjoXR4.leftAClickPath });
    suggestedBindings.push_back({ leftBTouchAction, varjoXR4.leftBTouchPath });
    suggestedBindings.push_back({ leftBClickAction, varjoXR4.leftBClickPath });

    suggestedBindings.push_back({ leftTriggerTouchAction, varjoXR4.leftTriggerTouchPath });
    suggestedBindings.push_back({ leftTriggerSqueezeAction, varjoXR4.leftTriggerSqueezePath });

    suggestedBindings.push_back({ leftStickXAction, varjoXR4.leftStickXPath });
    suggestedBindings.push_back({ leftStickYAction, varjoXR4.leftStickYPath });
    suggestedBindings.push_back({ leftStickTouchAction, varjoXR4.leftStickTouchPath });
    suggestedBindings.push_back({ leftStickClickAction, varjoXR4.leftStickClickPath });

    suggestedBindings.push_back({ leftSqueezeTouchAction, varjoXR4.leftSqueezeTouchPath });
    suggestedBindings.push_back({ leftSqueezeClickAction, varjoXR4.leftSqueezeClickPath });

    suggestedBindings.push_back({ leftHandHapticAction, varjoXR4.leftHandHapticPath });

    suggestedBindings.push_back({ rightSystemClickAction, varjoXR4.rightSystemClickPath });
    suggestedBindings.push_back({ rightATouchAction, varjoXR4.rightATouchPath });
    suggestedBindings.push_back({ rightAClickAction, varjoXR4.rightAClickPath });
    suggestedBindings.push_back({ rightBTouchAction, varjoXR4.rightBTouchPath });
    suggestedBindings.push_back({ rightBClickAction, varjoXR4.rightBClickPath });

    suggestedBindings.push_back({ rightTriggerTouchAction, varjoXR4.rightTriggerTouchPath });
    suggestedBindings.push_back({ rightTriggerSqueezeAction, varjoXR4.rightTriggerSqueezePath });

    suggestedBindings.push_back({ rightStickXAction, varjoXR4.rightStickXPath });
    suggestedBindings.push_back({ rightStickYAction, varjoXR4.rightStickYPath });
    suggestedBindings.push_back({ rightStickTouchAction, varjoXR4.rightStickTouchPath });
    suggestedBindings.push_back({ rightStickClickAction, varjoXR4.rightStickClickPath });

    suggestedBindings.push_back({ rightSqueezeTouchAction, varjoXR4.rightSqueezeTouchPath });
    suggestedBindings.push_back({ rightSqueezeClickAction, varjoXR4.rightSqueezeClickPath });

    suggestedBindings.push_back({ rightHandHapticAction, varjoXR4.rightHandHapticPath });

    XrInteractionProfileSuggestedBinding suggestedBinding{};
    suggestedBinding.type = XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING;
    suggestedBinding.interactionProfile = varjoXR4.interactionProfile;
    suggestedBinding.countSuggestedBindings = suggestedBindings.size();
    suggestedBinding.suggestedBindings = suggestedBindings.data();

    XrResult result = xrSuggestInteractionProfileBindings(vr->instance(), &suggestedBinding);

    if (result != XR_SUCCESS)
    {
        warning("Failed to suggest Varjo XR-4 bindings: " + openxrError(result));
    }
}
#endif
