/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_viewer.hpp"
#include "render_tools.hpp"
#include "theme.hpp"

static constexpr size_t vertexCount = (4 + 4 + 3) * 2;
static constexpr f64 cameraSideLength = 0.5;

RenderFlatViewer::RenderFlatViewer(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation,
    f64 angle,
    f64 aspect
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->themeGradient.pipelineLayout, pipelineStore->themeGradient.pipeline)
{
    transform = { position, rotation, vec3(1) };

    vertices = createVertexBuffer(vertexCount * sizeof(fvec3));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    f64 altAngle = angle / aspect;

    fvec3 a = fquaternion(rightDir, +altAngle / 2).rotate(fquaternion(upDir, +angle / 2).rotate(forwardDir * cameraSideLength));
    fvec3 b = fquaternion(rightDir, +altAngle / 2).rotate(fquaternion(upDir, -angle / 2).rotate(forwardDir * cameraSideLength));
    fvec3 c = fquaternion(rightDir, -altAngle / 2).rotate(fquaternion(upDir, -angle / 2).rotate(forwardDir * cameraSideLength));
    fvec3 d = fquaternion(rightDir, -altAngle / 2).rotate(fquaternion(upDir, +angle / 2).rotate(forwardDir * cameraSideLength));

    vector<fvec3> vertexData;
    vertexData.reserve(vertexCount);

    vertexData.push_back(fvec3());
    vertexData.push_back(a);

    vertexData.push_back(fvec3());
    vertexData.push_back(b);

    vertexData.push_back(fvec3());
    vertexData.push_back(c);

    vertexData.push_back(fvec3());
    vertexData.push_back(d);

    vertexData.push_back(a);
    vertexData.push_back(b);

    vertexData.push_back(b);
    vertexData.push_back(c);

    vertexData.push_back(c);
    vertexData.push_back(d);

    vertexData.push_back(d);
    vertexData.push_back(a);

    fvec3 ta(-0.05f, a.y, a.z + 0.01f);
    fvec3 tb(0, a.y, a.z + 0.06f);
    fvec3 tc(0.05f, a.y, a.z + 0.01f);

    vertexData.push_back(ta);
    vertexData.push_back(tb);

    vertexData.push_back(tb);
    vertexData.push_back(tc);

    vertexData.push_back(ta);
    vertexData.push_back(tc);

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(fvec3));

    createComponents(swapchainElements);
}

RenderFlatViewer::~RenderFlatViewer()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderFlatViewer::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderFlatViewer::createUniformBuffer() const
{
    VmaBuffer buffer = RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount + sizeof(fvec4) * 4);

    VmaMapping<fvec4> mapping(ctx->allocator, buffer);

    mapping.data[8] = theme.accentGradientXStartColor.toVec4();
    mapping.data[9] = theme.accentGradientXEndColor.toVec4();
    mapping.data[10] = theme.accentGradientYStartColor.toVec4();
    mapping.data[11] = theme.accentGradientYEndColor.toVec4();

    return buffer;
}
