/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

string lengthSuffix();
string frequencySuffix();
string powerSuffix();
string timeSuffix();
string speedSuffix();
string accelerationSuffix();
string rotationalSpeedSuffix();
string massSuffix();
string volumeSuffix();
string densitySuffix();
string angleSuffix();
string luminousFluxSuffix();
string illuminanceSuffix();
string forceSuffix();
string torqueSuffix();
string massRateSuffix();
string bytesSuffix();
string pixelSuffix();
string percentSuffix();

f64 localizeLength(f64 metric);
f64 delocalizeLength(f64 value);
f64 localizeSpeed(f64 metric);
f64 delocalizeSpeed(f64 value);
f64 localizeAcceleration(f64 metric);
f64 delocalizeAcceleration(f64 value);
f64 localizeMass(f64 metric);
f64 delocalizeMass(f64 value);
f64 localizeVolume(f64 metric);
f64 delocalizeVolume(f64 value);
f64 localizeDensity(f64 metric);
f64 delocalizeDensity(f64 value);
f64 localizeForce(f64 metric);
f64 delocalizeForce(f64 value);
f64 localizeAngle(f64 radians);
f64 delocalizeAngle(f64 value);

vec2 localizeLength(const vec2& metric);
vec2 delocalizeLength(const vec2& value);
vec3 localizeLength(const vec3& metric);
vec3 delocalizeLength(const vec3& value);
vec3 localizeSpeed(const vec3& metric);
vec3 delocalizeSpeed(const vec3& value);
vec3 localizeAcceleration(const vec3& metric);
vec3 delocalizeAcceleration(const vec3& value);

vec2 localizeAngle(const vec2& radians);
vec2 delocalizeAngle(const vec2& value);
vec3 localizeAngle(const vec3& radians);
vec3 delocalizeAngle(const vec3& value);
