/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class ThemeSettings : public InterfaceWidget, public TabContent
{
public:
    ThemeSettings(InterfaceWidget* parent);

private:
    Color panelForegroundColorSource();
    void onPanelForegroundColor(const Color& color);
    Color worldBackgroundColorSource();
    void onWorldBackgroundColor(const Color& color);
    Color backgroundColorSource();
    void onBackgroundColor(const Color& color);
    Color altBackgroundColorSource();
    void onAltBackgroundColor(const Color& color);
    Color softIndentColorSource();
    void onSoftIndentColor(const Color& color);
    Color indentColorSource();
    void onIndentColor(const Color& color);
    Color outdentColorSource();
    void onOutdentColor(const Color& color);
    Color accentColorSource();
    void onAccentColor(const Color& color);
    Color activeTextColorSource();
    void onActiveTextColor(const Color& color);
    Color textColorSource();
    void onTextColor(const Color& color);
    Color inactiveTextColorSource();
    void onInactiveTextColor(const Color& color);
    Color warningColorSource();
    void onWarningColor(const Color& color);
    Color errorColorSource();
    void onErrorColor(const Color& color);
    Color dividerColorSource();
    void onDividerColor(const Color& color);
    Color positiveColorSource();
    void onPositiveColor(const Color& color);
    Color negativeColorSource();
    void onNegativeColor(const Color& color);
    Color keywordColorSource();
    void onKeywordColor(const Color& color);
    Color stdlibColorSource();
    void onStdlibColor(const Color& color);
    Color bracketColorSource();
    void onBracketColor(const Color& color);
    Color quoteColorSource();
    void onQuoteColor(const Color& color);
    Color escapedColorSource();
    void onEscapedColor(const Color& color);
    Color stringColorSource();
    void onStringColor(const Color& color);
    Color numberColorSource();
    void onNumberColor(const Color& color);
    Color cursorColorSource();
    void onCursorColor(const Color& color);
    Color bracketHighlightColorSource();
    void onBracketHighlightColor(const Color& color);
    Color xAxisColorSource();
    void onXAxisColor(const Color& color);
    Color yAxisColorSource();
    void onYAxisColor(const Color& color);
    Color zAxisColorSource();
    void onZAxisColor(const Color& color);
    Color gridColorSource();
    void onGridColor(const Color& color);
    Color gridSecondaryColorSource();
    void onGridSecondaryColor(const Color& color);
    Color touchInterfaceColorSource();
    void onTouchInterfaceColor(const Color& color);
    Color touchInterfaceActiveColorSource();
    void onTouchInterfaceActiveColor(const Color& color);
    Color accentGradientXStartColorSource();
    void onAccentGradientXStartColor(const Color& color);
    Color accentGradientXEndColorSource();
    void onAccentGradientXEndColor(const Color& color);
    Color accentGradientYStartColorSource();
    void onAccentGradientYStartColor(const Color& color);
    Color accentGradientYEndColorSource();
    void onAccentGradientYEndColor(const Color& color);

    SizeProperties sizeProperties() const override;
};
