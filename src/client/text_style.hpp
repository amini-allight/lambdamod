/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_types.hpp"
#include "font_face.hpp"

class InterfaceWidget;

enum TextFont
{
    Text_Monospace,
    Text_Sans_Serif
};

enum TextAlignment
{
    Text_Left,
    Text_Center,
    Text_Right
};

enum TextWeight
{
    Text_Regular,
    Text_Semi_Bold,
    Text_Bold,
    Text_Extra_Bold
};

struct TextStyle
{
    TextStyle();
    TextStyle(const InterfaceWidget* widget);
    TextStyle(
        Color color,
        TextFont font,
        TextAlignment alignment,
        TextWeight weight,
        u32 size
    );

    FontFace* getFont() const;

    Color color;
    TextFont font;
    TextAlignment alignment;
    TextWeight weight;
    u32 size;

    bool operator==(const TextStyle& rhs) const;
    bool operator!=(const TextStyle& rhs) const;
};

struct TextStyleOverride
{
    TextStyleOverride();
    TextStyleOverride(
        optional<Color*> color,
        optional<TextFont> font,
        optional<TextAlignment> alignment,
        optional<TextWeight> weight,
        optional<u32> size
    );

    optional<Color*> color;
    optional<TextFont> font;
    optional<TextAlignment> alignment;
    optional<TextWeight> weight;
    optional<u32> size;

    TextStyle apply(TextStyle style) const;

    bool operator==(const TextStyleOverride& rhs) const;
    bool operator!=(const TextStyleOverride& rhs) const;
};
