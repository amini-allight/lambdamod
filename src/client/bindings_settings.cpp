/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "bindings_settings.hpp"
#include "input_bindings_loader.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "line_edit.hpp"

BindingsSettings::BindingsSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    for (const string& name : context()->input()->allBindings())
    {
        new EditorEntry(listView, name, [this, name](InterfaceWidget* parent) -> void {
            new LineEdit(
                parent,
                bind(&BindingsSettings::bindingSource, this, name),
                bind(&BindingsSettings::onBinding, this, name, placeholders::_1)
            );
        }, false);
    }
}

string BindingsSettings::bindingSource(const string& name)
{
    return toString(context()->input()->getBindings(name));
}

void BindingsSettings::onBinding(const string& name, const string& value)
{
    context()->input()->setBindings(name, fromString(value));

    map<string, vector<InputScopeBinding>> bindings;

    for (const string& name : context()->input()->allBindings())
    {
        bindings.insert({ name, context()->input()->getBindings(name) });
    }

    saveInputBindings(bindings);
}

string BindingsSettings::toString(const vector<InputScopeBinding>& bindings) const
{
    string value;

    for (size_t i = 0; i < bindings.size(); i++)
    {
        value += bindings.at(i).toString();

        if (i + 1 != bindings.size())
        {
            value += ", ";
        }
    }

    return value;
}

vector<InputScopeBinding> BindingsSettings::fromString(const string& value) const
{
    vector<string> split;

    string part;

    for (char c : value)
    {
        if (c == ',')
        {
            if (!part.empty())
            {
                split.push_back(part);
                part = "";
            }
        }
        else
        {
            part += c;
        }
    }

    if (!part.empty())
    {
        split.push_back(part);
    }

    vector<InputScopeBinding> bindings;
    bindings.reserve(split.size());

    for (const string& part : split)
    {
        bindings.push_back(InputScopeBinding::fromString(part));
    }

    return bindings;
}

SizeProperties BindingsSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
