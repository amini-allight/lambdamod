/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "console_input.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "console_input_scope.hpp"

ConsoleInput::ConsoleInput(
    InterfaceWidget* parent,
    const function<void(const string&, const optional<ChatDestination>&)>& onChat,
    const function<void(const string&)>& onCommand
)
    : InterfaceWidget(parent)
    , _chatMode(false)
{
    START_CONSOLE_INPUT_SCOPE("console-input")
        WIDGET_SLOT("toggle-console-mode", toggleConsoleMode)
    END_SCOPE

    chatInput = new ConsoleChatInput(this, onChat);
    commandInput = new ConsoleCommandInput(this, onCommand);
}

void ConsoleInput::push(const string& history)
{
    commandInput->push(history);
}

void ConsoleInput::forceFocus()
{
    focus();

    if (_chatMode)
    {
        chatInput->forceFocus();
    }
    else
    {
        commandInput->forceFocus();
    }
}

bool ConsoleInput::chatMode() const
{
    return _chatMode;
}

void ConsoleInput::toggleConsoleMode(const Input& input)
{
    _chatMode = !_chatMode;

    if (_chatMode)
    {
        chatInput->forceFocus();
        commandInput->clearFocus();
    }
    else
    {
        chatInput->clearFocus();
        commandInput->forceFocus();
    }
}

SizeProperties ConsoleInput::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::consoleInputHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
