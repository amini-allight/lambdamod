/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_timeline.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "viewer_mode_context.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "tools.hpp"

ActionTimeline::ActionTimeline(
    InterfaceWidget* parent,
    EntityID id,
    const function<u32()>& playPositionSource,
    const function<void()>& onPlayPause,
    const function<void(u32)>& onSeek,
    const function<void(const set<ActionPartID>&)>& onSelect
)
    : InterfaceWidget(parent)
    , id(id)
    , scrollIndex(0)
    , panIndex(0)
    , playPositionSource(playPositionSource)
    , onPlayPause(onPlayPause)
    , onSeek(onSeek)
    , onSelect(onSelect)
    , transformMode(Action_Transform_None)
    , transformSubMode(Action_Transform_Sub_None)
    , selecting(false)
    , panStartScrollIndex(0)
    , panStartPanIndex(0)
    , panning(false)
{
    START_WIDGET_SCOPE("action-timeline")
        WIDGET_SLOT("select-all", selectAll)
        WIDGET_SLOT("deselect-all", deselectAll)
        WIDGET_SLOT("invert-selection", invertSelection)
        WIDGET_SLOT("duplicate", duplicate)
        WIDGET_SLOT("cut", cut)
        WIDGET_SLOT("copy", copy)
        WIDGET_SLOT("paste", paste)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("pan-left", panLeft)
        WIDGET_SLOT("pan-right", panRight)
        WIDGET_SLOT("add-new", addNew)
        WIDGET_SLOT("remove-selected", removeSelected)
        WIDGET_SLOT("start-select", startSelect)
        WIDGET_SLOT("start-pan", startPan)
        WIDGET_SLOT("translate", translate)
        WIDGET_SLOT("toggle-play", togglePlay)

        START_INDEPENDENT_SCOPE("selecting", selecting)
            WIDGET_SLOT("move-select", moveSelect)
            WIDGET_SLOT("end-replace-select", endReplaceSelect)
            WIDGET_SLOT("end-add-select", endAddSelect)
            WIDGET_SLOT("end-remove-select", endRemoveSelect)
        END_SCOPE

        START_INDEPENDENT_SCOPE("panning", panning)
            WIDGET_SLOT("pan", pan)
            WIDGET_SLOT("end-pan", endPan)
        END_SCOPE

        START_BLOCKING_SCOPE("tool", transformMode != Action_Transform_None)
            WIDGET_SLOT("toggle-x", toggleX)
            WIDGET_SLOT("toggle-y", toggleY)
            WIDGET_SLOT("use-tool", useTool)
            WIDGET_SLOT("cancel-tool", cancelTool)
            WIDGET_SLOT("end-tool", endTool)
        END_SCOPE
    END_SCOPE
}

void ActionTimeline::open(const string& name)
{
    this->name = name;
}

void ActionTimeline::draw(const DrawContext& ctx) const
{
    drawSolid(ctx, this, theme.softIndentColor);

    // ID lines
    for (size_t i = 0; i < static_cast<size_t>(((size().h - UIScale::timelineTopHeaderHeight()) / UIScale::timelineRowHeight()) + 1); i++)
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth(), size().h - ((i + 1) * UIScale::timelineRowHeight())),
            Size(size().w - UIScale::timelineLeftHeaderWidth(), 1),
            theme.indentColor
        );
    }

    // Time lines
    for (size_t i = 0; i < static_cast<size_t>(((size().w - UIScale::timelineLeftHeaderWidth()) / UIScale::timelineColumnWidth()) + 1); i++)
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth() + (i * UIScale::timelineColumnWidth()), UIScale::timelineTopHeaderHeight()),
            Size(1, size().h - UIScale::timelineTopHeaderHeight()),
            theme.indentColor
        );
    }

    // Parts
    for (const auto& [ id, part ] : currentAction().parts())
    {
        drawImage(
            ctx,
            this,
            screenPosition(part),
            UIScale::actionTimelineMarkerSize(),
            Icon_Frame
        );

        if (activeParts.contains(id))
        {
            drawBorder(
                ctx,
                this,
                screenPosition(part) - Point(UIScale::marginSize()),
                UIScale::actionTimelineMarkerSize() + Size(UIScale::marginSize() * 2),
                UIScale::marginSize(),
                theme.accentColor
            );
        }
    }

    // Current play position
    drawSolid(
        ctx,
        this,
        Point(UIScale::timelineLeftHeaderWidth() + ((playPositionSource() - (panIndex * millisecondsPerColumn())) / timelineMillisecondsPerPixel), UIScale::timelineTopHeaderHeight()),
        Size(1, size().h - UIScale::timelineTopHeaderHeight()),
        theme.accentColor
    );

    // ID column
    drawSolid(
        ctx,
        this,
        Point(0, 0),
        Size(UIScale::timelineLeftHeaderWidth(), size().h),
        theme.backgroundColor
    );

    for (size_t i = 0; i < static_cast<size_t>(((size().h - UIScale::timelineTopHeaderHeight()) / UIScale::timelineRowHeight()) + 1); i++)
    {
        drawText(
            ctx,
            this,
            Point(0, size().h - ((i + 1) * UIScale::timelineRowHeight())),
            Size(UIScale::timelineLeftHeaderWidth(), UIScale::timelineRowHeight()),
            to_string(scrollIndex + i),
            TextStyle()
        );
    }

    // Time row
    drawSolid(
        ctx,
        this,
        Point(0, 0),
        Size(size().w, UIScale::timelineTopHeaderHeight()),
        theme.backgroundColor
    );

    for (size_t i = 0; i < static_cast<size_t>(((size().w - UIScale::timelineLeftHeaderWidth()) / UIScale::timelineColumnWidth()) + 1); i++)
    {
        drawText(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth() + (i * UIScale::timelineColumnWidth()), 0),
            Size(UIScale::timelineColumnWidth(), UIScale::timelineTopHeaderHeight()),
            toFixedPrecision(((panIndex + i) * millisecondsPerColumn()) / 1000.0, 1),
            TextStyle()
        );
    }

    if (transformSubMode == Action_Transform_Sub_X)
    {
        drawSolid(
            ctx,
            this,
            Point(UIScale::timelineLeftHeaderWidth(), size().h / 2),
            Size(size().w - UIScale::timelineLeftHeaderWidth(), 1),
            theme.xAxisColor
        );
    }
    else if (transformSubMode == Action_Transform_Sub_Y)
    {
        drawSolid(
            ctx,
            this,
            Point(size().w / 2, UIScale::timelineTopHeaderHeight()),
            Size(1, size().h - UIScale::timelineTopHeaderHeight()),
            theme.yAxisColor
        );
    }

    bool box = sqrt(sq(selectEnd.x - selectStart.x) + sq(selectEnd.y - selectStart.y)) >= UIScale::minBoxSelectSize();

    if (selecting && box)
    {
        i32 minX = min(selectStart.x, selectEnd.x);
        i32 minY = min(selectStart.y, selectEnd.y);
        i32 maxX = max(selectStart.x, selectEnd.x);
        i32 maxY = max(selectStart.y, selectEnd.y);

        minX -= position().x;
        minY -= position().y;
        maxX -= position().x;
        maxY -= position().y;

        drawBorder(
            ctx,
            this,
            Point(minX, minY),
            Size(maxX - minX, maxY - minY),
            UIScale::marginSize(),
            theme.accentColor
        );
    }
}

void ActionTimeline::makeSelections(SelectMode mode)
{
    if (mode == Select_Replace)
    {
        activeParts.clear();
    }

    bool box = sqrt(sq(selectEnd.x - selectStart.x) + sq(selectEnd.y - selectStart.y)) >= UIScale::minBoxSelectSize();

    if (box)
    {
        i32 minX = selectStart.x < selectEnd.x ? selectStart.x : selectEnd.x;
        i32 minY = selectStart.y < selectEnd.y ? selectStart.y : selectEnd.y;
        i32 maxX = selectStart.x > selectEnd.x ? selectStart.x : selectEnd.x;
        i32 maxY = selectStart.y > selectEnd.y ? selectStart.y : selectEnd.y;

        minX -= position().x;
        minY -= position().y;
        maxX -= position().x;
        maxY -= position().y;

        for (const auto& [ id, part ] : currentAction().parts())
        {
            Point screenPos = screenPosition(part);

            if (screenPos.x >= minX && screenPos.y >= minY && screenPos.x < maxX && screenPos.y < maxY)
            {
                if (mode == Select_Remove)
                {
                    activeParts.erase(id);
                }
                else
                {
                    activeParts.insert(id);
                }
            }
        }
    }
    else
    {
        optional<ActionPartID> id = partUnderCursor(selectEnd);

        if (!id)
        {
            onSeek((panIndex * millisecondsPerColumn()) + ((selectEnd.x - (position().x + UIScale::timelineLeftHeaderWidth())) * timelineMillisecondsPerPixel));
        }
        else
        {
            if (mode == Select_Remove)
            {
                activeParts.erase(*id);
            }
            else
            {
                activeParts.insert(*id);
            }
        }
    }

    onSelect(activeParts);
}

void ActionTimeline::add(const Point& pointer)
{
    optional<tuple<u32, BodyPartID>> position = positionUnderCursor(pointer);

    if (!position)
    {
        return;
    }

    u32 end;
    BodyPartID targetID;

    tie(end, targetID) = *position;

    ActionPartID id;

    edit([end, targetID, &id](Action& action) -> void {
        ActionPart part(action.add());
        part.setEnd(end);
        part.setTargetID(targetID);

        action.add(part);

        id = part.id();
    });

    activeParts = { id };
}

void ActionTimeline::remove(const Point& pointer)
{
    edit([this](Action& action) -> void {
        for (ActionPartID partID : activeParts)
        {
            action.remove(partID);
        }

        activeParts.clear();
    });
}

void ActionTimeline::transform(const Point& pointer)
{
    switch (transformMode)
    {
    default :
    case Action_Transform_None :
        return;
    case Action_Transform_Translate :
    {
        Point delta = pointer - initialPointer;

        if (transformSubMode == Action_Transform_Sub_X)
        {
            delta.y = 0;
        }
        else if (transformSubMode == Action_Transform_Sub_Y)
        {
            delta.x = 0;
        }

        translate(delta.x * timelineMillisecondsPerPixel, -delta.y / UIScale::timelineRowHeight());
        break;
    }
    }
}

void ActionTimeline::translate(i64 endDelta, i64 targetIDDelta)
{
    edit([this, endDelta, targetIDDelta](Action& action) -> void {
        for (ActionPartID partID : activeParts)
        {
            auto it = action.parts().find(partID);

            if (it == action.parts().end())
            {
                continue;
            }

            ActionPart part = it->second;

            const ActionPart& initial = initialParts.at(partID);

            i64 end = max<i64>(initial.end() + endDelta, 0);
            i64 targetID = max<i64>(static_cast<i64>(initial.targetID().value()) + targetIDDelta, 0);

            part.setEnd(end);
            part.setTargetID(BodyPartID(targetID));

            action.add(part);
        }
    });
}

void ActionTimeline::startTransform(ActionTransformMode mode, const Point& pointer)
{
    const Entity* entity = context()->viewerMode()->context()->controller()->game().get(id);

    if (!entity)
    {
        return;
    }

    auto it = entity->actions().find(name);

    if (it == entity->actions().end())
    {
        return;
    }

    const Action& action = it->second;

    transformMode = mode;
    transformSubMode = Action_Transform_Sub_None;
    initialPointer = pointer;

    for (ActionPartID partID : activeParts)
    {
        auto it = action.parts().find(partID);

        if (it == action.parts().end())
        {
            continue;
        }

        initialParts.insert({ it->first, it->second });
    }
}

void ActionTimeline::endTransform()
{
    initialParts.clear();
    transformMode = Action_Transform_None;
    transformSubMode = Action_Transform_Sub_None;
}

void ActionTimeline::cancelTransform()
{
    translate(0, 0);
    endTransform();
}

void ActionTimeline::edit(const function<void(Action&)>& behavior) const
{
    context()->controller()->edit([this, behavior]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        auto it = entity->actions().find(name);

        if (it == entity->actions().end())
        {
            return;
        }

        Action action = it->second;

        behavior(action);

        entity->addAction(name, action);
    });
}

optional<tuple<u32, BodyPartID>> ActionTimeline::positionUnderCursor(const Point& pointer) const
{
    Point local = pointer - (position() + Point(UIScale::timelineLeftHeaderWidth(), UIScale::timelineTopHeaderHeight()));
    i32 height = size().h - UIScale::timelineTopHeaderHeight();

    if (local.x < 0 || local.y < 0)
    {
        return {};
    }

    u32 end = (panIndex * millisecondsPerColumn()) + (local.x * timelineMillisecondsPerPixel);
    BodyPartID targetID = BodyPartID(scrollIndex + ((height - local.y) / UIScale::timelineRowHeight()));

    return tuple<u32, BodyPartID>(end, targetID);
}

optional<ActionPartID> ActionTimeline::partUnderCursor(const Point& pointer) const
{
    for (const auto& [ id, part ] : currentAction().parts())
    {
        if (contains(screenPosition(part), UIScale::actionTimelineMarkerSize(), pointer))
        {
            return id;
        }
    }

    return {};
}

Point ActionTimeline::screenPosition(const ActionPart& part) const
{
    i32 height = size().h - UIScale::timelineTopHeaderHeight();

    i32 x = part.end() / timelineMillisecondsPerPixel;
    x -= panIndex * UIScale::timelineColumnWidth();
    x -= UIScale::actionTimelineMarkerSize().w / 2;

    i32 y = part.targetID().value() * UIScale::timelineRowHeight();
    y += UIScale::timelineRowHeight(); // To line up with the lines
    y -= scrollIndex * UIScale::timelineRowHeight();

    y = height - y;

    return Point(UIScale::timelineLeftHeaderWidth(), UIScale::timelineTopHeaderHeight()) + Point(x, y);
}

const Action& ActionTimeline::currentAction() const
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        static const Action empty;
        return empty;
    }

    auto it = entity->actions().find(name);

    if (it == entity->actions().end())
    {
        static const Action empty;
        return empty;
    }

    return it->second;
}

const ActionPart& ActionTimeline::currentPart() const
{
    const Action& action = currentAction();

    if (activeParts.size() != 1)
    {
        static const ActionPart empty;
        return empty;
    }

    auto it = action.parts().find(*activeParts.begin());

    if (it == action.parts().end())
    {
        static const ActionPart empty;
        return empty;
    }

    return it->second;
}

void ActionTimeline::selectAll(const Input& input)
{
    const Action& action = currentAction();

    for (const auto& [ id, part ] : action.parts())
    {
        activeParts.insert(id);
    }
}

void ActionTimeline::deselectAll(const Input& input)
{
    activeParts.clear();
}

void ActionTimeline::invertSelection(const Input& input)
{
    const Action& action = currentAction();

    set<ActionPartID> newActiveParts;

    for (const auto& [ id, part ] : action.parts())
    {
        if (activeParts.contains(id))
        {
            continue;
        }

        newActiveParts.insert(id);
    }

    activeParts = newActiveParts;
}

void ActionTimeline::duplicate(const Input& input)
{
    const Action& action = currentAction();

    vector<ActionPart> parts;

    for (ActionPartID partID : activeParts)
    {
        auto it = action.parts().find(partID);

        if (it == action.parts().end())
        {
            continue;
        }

        parts.push_back(it->second);
    }

    activeParts.clear();

    edit([this, parts](Action& action) -> void {
        for (ActionPart part : parts)
        {
            part = ActionPart(action.add(), part);
            action.add(part);

            activeParts.insert(part.id());
        }
    });
}

void ActionTimeline::cut(const Input& input)
{
    copy(input);

    edit([this](Action& action) -> void {
        for (ActionPartID partID : activeParts)
        {
            action.remove(partID);
        }

        activeParts.clear();
    });
}

void ActionTimeline::copy(const Input& input)
{
    const Action& action = currentAction();

    vector<ActionPart> parts;

    for (ActionPartID partID : activeParts)
    {
        auto it = action.parts().find(partID);

        if (it == action.parts().end())
        {
            continue;
        }

        parts.push_back(it->second);
    }

    context()->viewerMode()->entityMode()->actionCopyBuffer().set(parts);
}

void ActionTimeline::paste(const Input& input)
{
    edit([this](Action& action) -> void {
        for (ActionPart part : context()->viewerMode()->entityMode()->actionCopyBuffer().get())
        {
            part = ActionPart(action.add(), part);
            action.add(part);

            activeParts.insert(part.id());
        }
    });
}

void ActionTimeline::scrollUp(const Input& input)
{
    scrollIndex++;
}

void ActionTimeline::scrollDown(const Input& input)
{
    if (scrollIndex != 0)
    {
        scrollIndex--;
    }
}

void ActionTimeline::panLeft(const Input& input)
{
    if (panIndex != 0)
    {
        panIndex--;
    }
}

void ActionTimeline::panRight(const Input& input)
{
    panIndex++;
}

void ActionTimeline::addNew(const Input& input)
{
    add(pointer);
}

void ActionTimeline::removeSelected(const Input& input)
{
    remove(pointer);
}

void ActionTimeline::startSelect(const Input& input)
{
    selectStart = pointer;
    selectEnd = pointer;
    selecting = true;
}

void ActionTimeline::startPan(const Input& input)
{
    panStart = pointer;
    panStartScrollIndex = scrollIndex;
    panStartPanIndex = panIndex;
    panning = true;
}

void ActionTimeline::translate(const Input& input)
{
    startTransform(Action_Transform_Translate, pointer);
}

void ActionTimeline::togglePlay(const Input& input)
{
    onPlayPause();
}

void ActionTimeline::moveSelect(const Input& input)
{
    selectEnd = pointer;
}

void ActionTimeline::endReplaceSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Replace);
}

void ActionTimeline::endAddSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Add);
}

void ActionTimeline::endRemoveSelect(const Input& input)
{
    selectEnd = pointer;
    selecting = false;

    makeSelections(Select_Remove);
}

void ActionTimeline::pan(const Input& input)
{
    vec2 delta = (pointer - panStart).toVec2();

    f64 panDelta = (-delta.x / (1.0 / timelineMillisecondsPerPixel)) / millisecondsPerColumn();
    f64 scrollDelta = delta.y / UIScale::timelineRowHeight();

    panIndex = max(panStartPanIndex + panDelta, 0.0);
    scrollIndex = max(panStartScrollIndex + scrollDelta, 0.0);
}

void ActionTimeline::endPan(const Input& input)
{
    panning = false;
}

void ActionTimeline::toggleX(const Input& input)
{
    transformSubMode = transformSubMode != Action_Transform_Sub_X
        ? Action_Transform_Sub_X
        : Action_Transform_Sub_None;
}

void ActionTimeline::toggleY(const Input& input)
{
    transformSubMode = transformSubMode != Action_Transform_Sub_Y
        ? Action_Transform_Sub_Y
        : Action_Transform_Sub_None;
}

void ActionTimeline::useTool(const Input& input)
{
    transform(pointer);
}

void ActionTimeline::cancelTool(const Input& input)
{
    cancelTransform();
}

void ActionTimeline::endTool(const Input& input)
{
    endTransform();
}

i32 ActionTimeline::millisecondsPerColumn() const
{
    return UIScale::timelineColumnWidth() * timelineMillisecondsPerPixel;
}

SizeProperties ActionTimeline::sizeProperties() const
{
    return sizePropertiesFillAll;
}
