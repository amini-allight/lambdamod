/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_controls_tool_pages.hpp"
#include "units.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "row.hpp"
#include "column.hpp"
#include "label.hpp"
#include "ufloat_edit.hpp"
#include "vec3_edit.hpp"
#include "checkbox.hpp"
#include "text_button.hpp"
#include "spacer.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"

#include "list_dialog.hpp"

UserControlTool::UserControlTool(
    InterfaceWidget* parent,
    ControlContext* ctx
)
    : InterfaceWidget(parent)
    , ctx(ctx)
{

}

void UserControlTool::open()
{
    users.clear();

    for (const auto& [ id, user ] : ctx->controller()->game().users())
    {
        users.insert(user.name());
    }

    reset();
}

void UserControlTool::close()
{

}

void UserControlTool::onManageUsers()
{
    ctx->interface()->addWindow<ListDialog>(
        localize("user-controls-tool-pages-users"),
        bind(&UserControlTool::usersSource, this),
        bind(&UserControlTool::onAddUser, this, placeholders::_1),
        bind(&UserControlTool::onRemoveUser, this, placeholders::_1)
    );
}

vector<string> UserControlTool::usersSource()
{
    return { users.begin(), users.end() };
}

void UserControlTool::onAddUser(const string& name)
{
    users.insert(name);
}

void UserControlTool::onRemoveUser(const string& name)
{
    users.erase(name);
}

UserTetherTool::UserTetherTool(
    InterfaceWidget* parent,
    ControlContext* ctx
)
    : UserControlTool(parent, ctx)
{
    auto column = new Column(this);

    new EditorEntry(column, "user-controls-tool-pages-enabled", [this](InterfaceWidget* parent) -> void {
        enabled = new Checkbox(
            parent,
            nullptr,
            [](bool state) -> void {}
        );
    });

    new EditorEntry(column, "user-controls-tool-pages-tether-distance", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        distance = new UFloatEdit(
            parent,
            nullptr,
            [](f64 value) -> void {}
        );
        distance->setPlaceholder(localize("user-controls-tool-pages-tether-distance"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-manage-users"),
            bind(&UserTetherTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-submit"),
            bind(&UserTetherTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void UserTetherTool::reset()
{
    enabled->set(false);
    distance->clear();
}

void UserTetherTool::onDone()
{
    try
    {
        TetherEvent event;
        event.type = enabled->checked() ? Tether_Event_Tether : Tether_Event_Untether;
        event.distance = delocalizeLength(distance->value());

        for (const string& name : users)
        {
            const User* user = ctx->controller()->game().getUserByName(name);

            if (!user)
            {
                continue;
            }

            event.userIDs.insert(user->id());
        }

        ctx->controller()->send(event);
    }
    catch (const exception& e)
    {

    }
}

SizeProperties UserTetherTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

UserConfineTool::UserConfineTool(
    InterfaceWidget* parent,
    ControlContext* ctx
)
    : UserControlTool(parent, ctx)
{
    auto column = new Column(this);

    new EditorEntry(column, "user-controls-tool-pages-enabled", [this](InterfaceWidget* parent) -> void {
        enabled = new Checkbox(
            parent,
            nullptr,
            [](bool state) -> void {}
        );
    });

    new EditorEntry(column, "user-controls-tool-pages-confine-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        position = new Vec3Edit(
            parent,
            nullptr,
            [](const vec3& v) -> void {}
        );
    });

    new EditorEntry(column, "user-controls-tool-pages-confine-distance", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        distance = new UFloatEdit(
            parent,
            nullptr,
            [](f64 value) -> void {}
        );
        distance->setPlaceholder(localize("user-controls-tool-pages-confine-distance"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-manage-users"),
            bind(&UserConfineTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-submit"),
            bind(&UserConfineTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void UserConfineTool::reset()
{
    enabled->set(false);
    position->clear();
    distance->clear();
}

void UserConfineTool::onDone()
{
    try
    {
        ConfinementEvent event;
        event.type = enabled->checked() ? Confinement_Event_Confine : Confinement_Event_Unconfine;
        event.position = delocalizeLength(position->value());
        event.distance = delocalizeLength(distance->value());

        for (const string& name : users)
        {
            const User* user = ctx->controller()->game().getUserByName(name);

            if (!user)
            {
                continue;
            }

            event.userIDs.insert(user->id());
        }

        ctx->controller()->send(event);
    }
    catch (const exception& e)
    {

    }
}

SizeProperties UserConfineTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

UserGotoTool::UserGotoTool(
    InterfaceWidget* parent,
    ControlContext* ctx
)
    : UserControlTool(parent, ctx)
{
    auto column = new Column(this);

    new EditorEntry(column, "user-controls-tool-pages-goto-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        position = new Vec3Edit(
            parent,
            nullptr,
            [](const vec3& v) -> void {}
        );
    });

    new EditorEntry(column, "user-controls-tool-pages-goto-radius", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        radius = new UFloatEdit(
            parent,
            nullptr,
            [](f64 value) -> void {}
        );
        radius->setPlaceholder(localize("user-controls-tool-pages-goto-radius"));
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-manage-users"),
            bind(&UserGotoTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-submit"),
            bind(&UserGotoTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void UserGotoTool::reset()
{
    position->clear();
    radius->clear();
}

void UserGotoTool::onDone()
{
    try
    {
        GotoEvent event;
        event.position = delocalizeLength(position->value());
        event.radius = delocalizeLength(radius->value());

        for (const string& name : users)
        {
            const User* user = ctx->controller()->game().getUserByName(name);

            if (!user)
            {
                continue;
            }

            event.userIDs.insert(user->id());
        }

        ctx->controller()->send(event);
    }
    catch (const exception& e)
    {

    }
}

SizeProperties UserGotoTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

UserFadeTool::UserFadeTool(
    InterfaceWidget* parent,
    ControlContext* ctx
)
    : UserControlTool(parent, ctx)
{
    auto column = new Column(this);

    new EditorEntry(column, "user-controls-tool-pages-enabled", [this](InterfaceWidget* parent) -> void {
        enabled = new Checkbox(
            parent,
            nullptr,
            [](bool state) -> void {}
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-manage-users"),
            bind(&UserFadeTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-submit"),
            bind(&UserFadeTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void UserFadeTool::reset()
{
    enabled->set(false);
}

void UserFadeTool::onDone()
{
    FadeEvent event;
    event.type = enabled->checked() ? Fade_Event_Fade : Fade_Event_Unfade;

    for (const string& name : users)
    {
        const User* user = ctx->controller()->game().getUserByName(name);

        if (!user)
        {
            continue;
        }

        event.userIDs.insert(user->id());
    }

    ctx->controller()->send(event);
}

SizeProperties UserFadeTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}

UserWaitTool::UserWaitTool(
    InterfaceWidget* parent,
    ControlContext* ctx
)
    : UserControlTool(parent, ctx)
{
    auto column = new Column(this);

    new EditorEntry(column, "user-controls-tool-pages-enabled", [this](InterfaceWidget* parent) -> void {
        enabled = new Checkbox(
            parent,
            nullptr,
            [](bool state) -> void {}
        );
    });

    MAKE_SPACER(column, 0, 0);

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-manage-users"),
            bind(&UserWaitTool::onManageUsers, this)
        );
    });

    new EditorMultiEntry(column, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new TextButton(
            parent,
            localize("user-controls-tool-pages-submit"),
            bind(&UserWaitTool::onDone, this)
        );
    });

    MAKE_SPACER(column, 0, UIScale::widgetToBottomSpacing());
}

void UserWaitTool::reset()
{
    enabled->set(false);
}

void UserWaitTool::onDone()
{
    WaitEvent event;
    event.type = enabled->checked() ? Wait_Event_Wait : Wait_Event_Unwait;

    for (const string& name : users)
    {
        const User* user = ctx->controller()->game().getUserByName(name);

        if (!user)
        {
            continue;
        }

        event.userIDs.insert(user->id());
    }

    ctx->controller()->send(event);
}

SizeProperties UserWaitTool::sizeProperties() const
{
    return sizePropertiesFillAll;
}
