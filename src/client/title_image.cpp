/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "title_image.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

TitleImage::TitleImage(InterfaceWidget* parent, Icon icon, const function<Size(const InterfaceWidget*)>& desiredSizeSource)
    : InterfaceWidget(parent)
    , icon(icon)
    , desiredSizeSource(desiredSizeSource)
{

}

void TitleImage::draw(const DrawContext& ctx) const
{
    drawImage(
        ctx,
        this,
        Point(),
        desiredSizeSource(this),
        icon
    );
}

SizeProperties TitleImage::sizeProperties() const
{
    return {
        static_cast<f64>(desiredSizeSource(this).w), static_cast<f64>(desiredSizeSource(this).h),
        Scaling_Fixed, Scaling_Fixed
    };
}
