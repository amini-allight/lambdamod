/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_window.hpp"

#include "line_edit.hpp"
#include "list_view.hpp"

class QuickAccessor;

class QuickAccessorOption
{
public:
    QuickAccessorOption(
        QuickAccessor* parent,
        ControlContext* ctx,
        const string& name,
        const string& search
    );
    QuickAccessorOption(const QuickAccessorOption& rhs) = delete;
    QuickAccessorOption(QuickAccessorOption&& rhs) = delete;
    virtual ~QuickAccessorOption();

    QuickAccessorOption& operator=(const QuickAccessorOption& rhs) = delete;
    QuickAccessorOption& operator=(QuickAccessorOption&& rhs) = delete;

    virtual string type() const = 0;
    const string& name() const;

    virtual void pick(const Point& point) = 0;

    bool compare(const QuickAccessorOption* rhs) const;

protected:
    QuickAccessor* parent;
    ControlContext* ctx;

private:
    string _name;
    string search;
};

class QuickAccessorOptionWindow final : public QuickAccessorOption
{
public:
    QuickAccessorOptionWindow(
        QuickAccessor* parent,
        ControlContext* ctx,
        const string& name,
        const function<void(const Point&)>& callback
    );
    QuickAccessorOptionWindow(
        QuickAccessor* parent,
        ControlContext* ctx,
        const string& type,
        const string& name,
        const function<void(const Point&)>& callback
    );

    string type() const override;

    void pick(const Point& point) override;

private:
    optional<string> _type;
    function<void(const Point&)> callback;
};

class QuickAccessorOptionAction final : public QuickAccessorOption
{
public:
    QuickAccessorOptionAction(
        QuickAccessor* parent,
        ControlContext* ctx,
        const string& name,
        const function<void()>& callback
    );

    string type() const override;

    void pick(const Point& point) override;

private:
    function<void()> callback;
};

class QuickAccessorOptionOracle final : public QuickAccessorOption
{
public:
    QuickAccessorOptionOracle(QuickAccessor* parent, ControlContext* ctx, const string& name);

    string type() const override;

    void pick(const Point& point) override;

private:
    string name;
};

class QuickAccessorOptionDocument final : public QuickAccessorOption
{
public:
    QuickAccessorOptionDocument(
        QuickAccessor* parent,
        ControlContext* ctx,
        const string& name
    );

    string type() const override;

    void pick(const Point& point) override;
};

class QuickAccessorOptionEntity final : public QuickAccessorOption
{
public:
    QuickAccessorOptionEntity(
        QuickAccessor* parent,
        ControlContext* ctx,
        const string& name,
        EntityID id
    );

    string type() const override;

    void pick(const Point& point) override;

private:
    EntityID id;
};

class QuickAccessorLineEdit : public LineEdit
{
public:
    QuickAccessorLineEdit(
        InterfaceWidget* parent,
        const function<string()>& source,
        const function<void(const string&)>& onReturn,
        bool codeMode = false
    );

    bool movePointer(PointerType type, const Point& pointer, bool consumed) override;
};

class QuickAccessorListView : public ListView
{
public:
    QuickAccessorListView(
        InterfaceWidget* parent,
        const function<void(size_t)>& onSelect = nullptr,
        const optional<Color>& color = {}
    );

    bool movePointer(PointerType type, const Point& pointer, bool consumed) override;
};

class QuickAccessor : public InterfaceWindow
{
public:
    QuickAccessor(InterfaceView* view);
    ~QuickAccessor();

    const string& buffer() const;

    void open(const Point& point) override;
    void close() override;

    void step() override;

    void focus() override;
    void unfocus() override;
    void clearFocus() override;

private:
    LineEdit* lineEdit;
    ListView* listView;

    vector<QuickAccessorOption*> options;
    vector<QuickAccessorOption*> sortedOptions;
    string lastSearch;
    bool shouldClose;

    void onReturn(const string& text);
    void onSelect(size_t index);

    vector<QuickAccessorOption*> sort(vector<QuickAccessorOption*> options) const;
    void clearOptions();
    void updateListView();

    void dismiss(const Input& input) override;
};
