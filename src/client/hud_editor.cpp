/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"

#include "confirm_dialog.hpp"

HUDEditor::HUDEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
{
    START_WIDGET_SCOPE("hud-editor")
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
    END_SCOPE

    auto column = new Column(this);

    auto row = new Row(column);

    elementList = new ItemList(
        row,
        bind(&HUDEditor::itemSource, this),
        bind(&HUDEditor::onAdd, this, placeholders::_1),
        bind(&HUDEditor::onRemove, this, placeholders::_1),
        bind(static_cast<void (HUDEditor::*)(const string&)>(&HUDEditor::onSelect), this, placeholders::_1),
        bind(&HUDEditor::onRename, this, placeholders::_1, placeholders::_2),
        bind(&HUDEditor::onClone, this, placeholders::_1, placeholders::_2)
    );

    MAKE_SPACER(row, UIScale::dividerWidth(), 0, theme.dividerColor);

    editor = new HUDElementEditor(
        row,
        id,
        bind(static_cast<void (HUDEditor::*)(size_t)>(&HUDEditor::onSelect), this, placeholders::_1)
    );
}

vector<string> HUDEditor::itemSource()
{
    const Entity* entity = context()->controller()->game().get(id);

    if (!entity)
    {
        return {};
    }

    vector<string> names;
    names.reserve(entity->hudElements().size());

    for (const auto& [ name, element ] : entity->hudElements())
    {
        names.push_back(name);
    }

    return names;
}

void HUDEditor::onAdd(const string& name)
{
    context()->controller()->edit([this, name]() -> void {
        Entity* entity = context()->controller()->selfWorld()->get(id);

        if (!entity)
        {
            return;
        }

        entity->addHUDElement(name, HUDElement());
    });
}

void HUDEditor::onRemove(const string& name)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("hud-editor-are-you-sure-you-want-to-delete-hud-element", name),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            context()->controller()->edit([this, name]() -> void {
                Entity* entity = context()->controller()->selfWorld()->get(id);

                if (!entity)
                {
                    return;
                }

                entity->removeHUDElement(name);
            });
        }
    );
}

void HUDEditor::onSelect(const string& name)
{
    editor->open(name);
}

void HUDEditor::onRename(const string& oldName, const string& newName)
{
    context()->controller()->edit([this, oldName, newName]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addHUDElement(newName, entity->hudElements().at(oldName));
        entity->removeHUDElement(oldName);
    });
}

void HUDEditor::onClone(const string& oldName, const string& newName)
{
    context()->controller()->edit([this, oldName, newName]() -> void {
        Entity* entity = context()->controller()->game().get(id);

        if (!entity)
        {
            return;
        }

        entity->addHUDElement(newName, entity->hudElements().at(oldName));
    });
}

void HUDEditor::onSelect(size_t index)
{
    elementList->select(index);
}

void HUDEditor::undo(const Input& input)
{
    context()->viewerMode()->undo();
}

void HUDEditor::redo(const Input& input)
{
    context()->viewerMode()->redo();
}

SizeProperties HUDEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
