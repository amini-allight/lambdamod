/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_context.hpp"
#include "sky_parameters.hpp"

class RenderSkyComponentLayer
{
public:
    RenderSkyComponentLayer(
        const RenderContext* ctx,
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera
    );
    RenderSkyComponentLayer(const RenderSkyComponentLayer& rhs) = delete;
    RenderSkyComponentLayer(RenderSkyComponentLayer&& rhs) = delete;
    ~RenderSkyComponentLayer();

    RenderSkyComponentLayer& operator=(const RenderSkyComponentLayer& rhs) = delete;
    RenderSkyComponentLayer& operator=(RenderSkyComponentLayer&& rhs) = delete;

    void fillUniformBuffer(
        const vec3& up,
        const fmat4& worldTransform,
        mt19937_64& engine,
        uniform_real_distribution<f32>& distribution,
        const SkyLayer& layer
    );
    void fillCommandBuffer(
        VkCommandBuffer commandBuffer,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    ) const;
    void fillEnvMapCommandBuffer(
        VkCommandBuffer commandBuffer,
        VkPipelineLayout pipelineLayout,
        VkPipeline pipeline
    ) const;

    SkyLayerType type;
    u32 billboardCount;
    VmaBuffer uniform;
    VmaMapping<f32>* uniformMapping;
    VkDescriptorSet descriptorSet;
    VkDescriptorSet envMapDescriptorSet;

private:
    const RenderContext* ctx;
    VkDescriptorSetLayout descriptorSetLayout;

    VmaBuffer createUniformBuffer(size_t size) const;
    VkDescriptorSet createDescriptorSet(VmaBuffer camera) const;
};
