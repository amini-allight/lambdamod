/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "controller.hpp"
#include "disconnection_reason.hpp"
#include "constants.hpp"
#include "global.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "localization.hpp"
#include "cryptography.hpp"
#include "yaml_tools.hpp"
#include "network_tools.hpp"

#include "control_context.hpp"

#include "platform/client_posix.hpp"
#include "platform/client_windows.hpp"
#include "platform/client_web.hpp"

#include "platform/window_windows.hpp"
#include "platform/window_wayland.hpp"
#include "platform/window_x11.hpp"
#include "platform/window_macos.hpp"
#include "platform/window_android.hpp"
#include "platform/window_ios.hpp"
#include "platform/window_web.hpp"

#include "platform/sound_device_windows.hpp"
#include "platform/sound_device_pipewire.hpp"
#include "platform/sound_device_pulseaudio.hpp"
#include "platform/sound_device_macos.hpp"
#include "platform/sound_device_android.hpp"
#include "platform/sound_device_ios.hpp"
#include "platform/sound_device_web.hpp"

#include "flat_render.hpp"

#include "vr_render.hpp"
#include "vr_input.hpp"

// Fix for Windows namespace pollution
#undef interface

Controller::Controller()
#ifdef LMOD_VR
    : _vr(nullptr)
    , _window(nullptr)
#else
    : _window(nullptr)
#endif
    , renderContext(nullptr)
    , _render(nullptr)
    , _soundDevice(nullptr)
    , _sound(nullptr)
    , _context(nullptr)
    , _serverSaving(false)
    , stepIndex(0)
    , inReplayMode(false)
    , _quit(false)
    , shouldReset(false)
    , _lastStepTime(0)
    , _connectionState(Connection_State_Standby)
    , pingSentTime(0)
    , client(nullptr)
    , voiceEncoder(new VoiceEncoder())
    , serverRunner(nullptr)
{
#ifdef LMOD_VR
    if (g_vr)
    {
#if defined(__EMSCRIPTEN__)
        _vr = new VRWeb(this);
#else
        _vr = new VR(this);
#endif
    }
    else
    {
#endif
#if defined(__ANDROID__)
        _window = new WindowAndroid(this);
#elif defined(__APPLE__) && defined(TARGET_OS_IPHONE)
        _window = new WindowIOS(this);
#elif defined(WIN32)
        _window = new WindowWindows(this);
#elif (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_WAYLAND)
        _window = new WindowWayland(this);
#elif (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_X11)
        _window = new WindowX11(this);
#elif defined(__APPLE__) && defined(TARGET_OS_MAC)
        _window = new WindowMacOS(this);
#elif defined(__EMSCRIPTEN__)
        _window = new WindowWeb(this);
#endif
#ifdef LMOD_VR
    }
#endif

    renderContext = new RenderContext();

#ifdef LMOD_VR
    if (g_vr)
    {
        _render = new VRRender(this, renderContext);
    }
    else
    {
        _render = new FlatRender(this, renderContext);
    }
#else
    _render = new FlatRender(this, renderContext);
#endif

#if defined(__ANDROID__)
    _soundDevice = new SoundDeviceAndroid();
#elif defined(__APPLE__) && defined(TARGET_OS_IPHONE)
    _soundDevice = new SoundDeviceIOS();
#elif defined(WIN32)
    _soundDevice = new SoundDeviceWindows();
#elif (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_PIPEWIRE)
    _soundDevice = new SoundDevicePipeWire();
#elif (defined(__linux__) || defined(__FreeBSD__)) && defined(LMOD_PULSEAUDIO)
    _soundDevice = new SoundDevicePulseAudio();
#elif defined(__APPLE__) && defined(TARGET_OS_MAC)
    _soundDevice = new SoundDeviceMacOS();
#elif defined(__EMSCRIPTEN__)
    _soundDevice = new SoundDeviceWeb();
#endif

    _sound = new Sound(this);

    _context = new ControlContext(this);

    auto flatRender = dynamic_cast<FlatRender*>(_render);

#ifdef LMOD_VR
    if (!g_vr)
    {
        send(UserEvent(
            User_Event_Angle,
            selfID(),
            defaultCameraAngle
        ));

        send(UserEvent(
            User_Event_Aspect,
            selfID(),
            flatRender->width() / static_cast<f64>(flatRender->height())
        ));
    }
#else
    send(UserEvent(
        User_Event_Angle,
        selfID(),
        defaultCameraAngle
    ));

    send(UserEvent(
        User_Event_Aspect,
        selfID(),
        flatRender->width() / static_cast<f64>(flatRender->height())
    ));
#endif
}

Controller::~Controller()
{
    _render->waitIdle();

    if (!standby())
    {
        LeaveEvent event;
        client->send(event);
    }

    clear();

    delete _sound;
    delete _soundDevice;

    delete _render;
    delete renderContext;

#ifdef LMOD_VR
    if (g_vr)
    {
        delete _vr;
    }
    else
    {
        delete _window;
    }
#else
    delete _window;
#endif
}

bool Controller::run()
{
    bool quit = _quit;

    if (shouldReset)
    {
        reset();
        shouldReset = false;
        return quit;
    }

    quit |= stepSystemInputs();

    chrono::microseconds stepInterval(static_cast<u64>(1'000'000.0 / g_config.stepRate));

    if (_lastStepTime + stepInterval <= currentTime<chrono::microseconds>())
    {
        if (_lastStepTime == chrono::microseconds(0))
        {
            _lastStepTime = currentTime<chrono::microseconds>();
        }
        else
        {
            _lastStepTime += stepInterval;
        }

        if (ready())
        {
            _context->step();

            Compare compare(now, bind(&Controller::stepSimulation, this));
            afterCompare(compare);
        }

        if (connection.lastRemoteIndex)
        {
            StepEvent event;
            event.localIndex = connection.lastRemoteIndex;
            event.remoteIndex = stepIndex;

            send(event);
        }

        connection.lastRemoteIndex = {};
    }

    quit |= stepSystemOutputs();

    return quit;
}

void Controller::onVoice(const vector<i16>& samples)
{
    if (!client)
    {
        return;
    }

    client->send(VoiceEvent(
        voiceEncoder->encode(string(
            reinterpret_cast<const char*>(samples.data()),
            samples.size() * sizeof(i16)
        ))
    ));
}

void Controller::onInput(const Input& input)
{
    _context->input()->handle(input);
}

void Controller::onGrip(bool right, f64 strength, const mat4& transform, const Velocities& velocities)
{
    GripEvent event;
    event.right = right;
    event.strength = strength;
    event.position = transform.position();
    event.rotation = transform.rotation();
    event.linearVelocity = velocities.linearVelocity;
    event.angularVelocity = velocities.angularVelocity;

    send(event);
}

void Controller::onBounds(const vec2& bounds)
{
    send(UserEvent(User_Event_Bounds, selfID(), bounds));
}

void Controller::onResize(u32 width, u32 height)
{
    auto flatRender = dynamic_cast<FlatRender*>(_render);

    if (width == flatRender->width() && height == flatRender->height())
    {
        return;
    }

    tie(width, height) = _render->reinit(width, height);
    _context->resize(width, height);

    send(UserEvent(
        User_Event_Aspect,
        selfID(),
        static_cast<f64>(width) / static_cast<f64>(height)
    ));
}

void Controller::onFocus()
{
    _context->focus();
}

void Controller::onUnfocus()
{
    _context->unfocus();
}

#ifdef LMOD_VR
VRInterface* Controller::vr() const
{
    return _vr;
}
#endif

WindowInterface* Controller::window() const
{
    return _window;
}

SoundDeviceInterface* Controller::soundDevice() const
{
    return _soundDevice;
}

RenderInterface* Controller::render() const
{
    return _render;
}

Sound* Controller::sound() const
{
    return _sound;
}

InputInterface* Controller::input() const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        return _vr->input();
    }
    else
    {
        return _window->input();
    }
#else
    return _window->input();
#endif
}

ControlContext* Controller::context() const
{
    return _context;
}

UserID Controller::selfID() const
{
    return _selfID;
}

const User* Controller::self() const
{
    return now.state.getUser(_selfID);
}

User* Controller::self()
{
    return now.state.getUser(_selfID);
}

Entity* Controller::host() const
{
    const User* self = this->self();

    if (!self)
    {
        return nullptr;
    }

    return now.state.get(self->entityID());
}

World* Controller::selfWorld() const
{
    const User* self = this->self();

    if (!self)
    {
        return nullptr;
    }

    return now.state.getWorld(self->world());
}

const string& Controller::splashMessage() const
{
    return _splashMessage;
}

const string& Controller::voiceChannel() const
{
    return _voiceChannel;
}

const map<string, string>& Controller::documents() const
{
    return _documents;
}

const set<string>& Controller::checkpoints() const
{
    return _checkpoints;
}

const string& Controller::serverJoinURL() const
{
    return _serverJoinURL;
}

bool Controller::serverSaving() const
{
    return _serverSaving;
}

GameState& Controller::game()
{
    return now.state;
}

void Controller::edit(const function<void()>& behavior, bool mergeUndo)
{
    Compare compare(now, [this, behavior]() -> vector<NetworkEvent> {
        behavior();
        // This is only performed by StepFrame::step otherwise, and this is the only source of non-step entity modification
        now.state.updateDynamics();

        return {};
    });
    afterCompare(compare);

    for (NetworkEvent event : compare.deltaEvents())
    {
        switch (event.type())
        {
        default :
            break;
        case Network_Event_Entity :
            event.payload<EntityEvent>().mergeUndo = mergeUndo;
            break;
        case Network_Event_Entity_Body :
            event.payload<EntityBodyEvent>().mergeUndo = mergeUndo;
            break;
        case Network_Event_Entity_Sample :
            event.payload<EntitySampleEvent>().mergeUndo = mergeUndo;
            break;
        case Network_Event_Entity_Action :
            event.payload<EntityActionEvent>().mergeUndo = mergeUndo;
            break;
        }

        send(event);
    }
}

void Controller::pushEditInput(const function<void(const Input&)>& handler, const Input& input)
{
    replayableEditInputs.push_back({ handler, input });
}

void Controller::sendAttachedInput(const string& hook, const Input& input)
{
    now.push(hook, input);

    InputEvent event;
    event.hook = hook;
    event.input = input;

    send(event);
}

void Controller::connect(
    const Address& address,
    const string& serverPassword,
    const string& name,
    const string& password
)
{
    saveConnectionDetails(address, serverPassword, name, password);

    _connectionState = Connection_State_Connecting;

    createClient(serverPassword, name, password);

    client->connect(address);
}

void Controller::host(
    const Address& address,
    const string& serverPassword,
    const string& adminPassword,
    const string& savePath,
    const string& name,
    const string& password
)
{
    string connectHost;

    if (isWildcardIPv4Address(address.host))
    {
        connectHost = "127.0.0.1";
    }
    else if (isWildcardIPv6Address(address.host))
    {
        connectHost = "::1";
    }
    else
    {
        connectHost = address.host;
    }

    saveConnectionDetails(address, serverPassword, adminPassword, savePath, connectHost, name, password);

    serverRunner = new ServerRunner(address.host, address.port, serverPassword, adminPassword, savePath);

    _connectionState = Connection_State_Connecting;

    createClient(serverPassword, name, password);

    client->connect({ connectHost, address.port });
}

#if !defined(__EMSCRIPTEN__)
void Controller::holePunch(
    const Address& clientAddress,
    const Address& serverAddress,
    const string& serverPassword,
    const string& name,
    const string& password
)
{
    _connectionState = Connection_State_Connecting;

    createClient(serverPassword, name, password);

    dynamic_cast<ClientNative*>(client)->holePunch(clientAddress, serverAddress);
}
#endif

void Controller::disconnect()
{
    if (!client)
    {
        return;
    }

    LeaveEvent event;
    client->send(event);
}

void Controller::send(const NetworkEvent& event)
{
    if (!client)
    {
        return;
    }

    if (inReplayMode)
    {
        return;
    }

    client->send(event);
}

void Controller::markForReset()
{
    shouldReset = true;
}

ConnectionState Controller::connectionState() const
{
    return _connectionState;
}

chrono::microseconds Controller::lastStepTime() const
{
    return _lastStepTime;
}

bool Controller::connected() const
{
    return _connectionState == Connection_State_Connected;
}

bool Controller::standby() const
{
    return _connectionState == Connection_State_Standby;
}

const string& Controller::disconnectionReason() const
{
    return _disconnectionReason;
}

u64 Controller::roundTripTime() const
{
    return connection.roundTripTime;
}

bool Controller::ready() const
{
    return _selfID;
}

void Controller::quit()
{
#ifdef LMOD_VR
    if (g_vr)
    {
        _vr->requestExit();
    }
    else
    {
        _quit = true;
    }
#else
    _quit = true;
#endif
}

void Controller::handleNetworkEvents()
{
    for (size_t i = 0; i < systemEvents.size(); i++)
    {
        const NetworkEvent& event = systemEvents[i];

        if (event.isSystem())
        {
            handleNetworkEvent(event, ready());
        }
        // Redirects any extra normal events after the welcome into the normal stream
        else if (ready())
        {
            connection.eventBatcher.push(event);
        }
        // Feeds any normal events before welcome directly into state
        else
        {
            now.push(event);
        }
    }

    systemEvents.clear();

    optional<NetworkEventBatch> batch = connection.eventBatcher.pull();

    if (!batch)
    {
        return;
    }

    handleNetworkEventBatch(*batch);
}

void Controller::handleNetworkEventBatch(const NetworkEventBatch& batch)
{
    connection.lastRemoteIndex = batch.remoteIndex;

    optional<u64> localIndex = batch.localIndex;

    if (batch.events.empty())
    {
        return;
    }
    else if (!localIndex || !history.has(*localIndex))
    {
        now.push(batch.events);
        return;
    }

    u64 targetIndex = stepIndex;
    stepIndex = *localIndex;

    now = history.get(stepIndex);

    now.push(batch.events);

    enterReplay();

    {
        Compare compare(now, bind(&Controller::stepSimulation, this), targetIndex - stepIndex);
        afterCompare(compare, [batch](const NetworkEvent& event) -> bool {
            return find(batch.events.begin(), batch.events.end(), event) != batch.events.end();
        });
    }

    exitReplay();
}

vector<NetworkEvent> Controller::stepSimulation()
{
    StepFrame fullNow = now;
    fullNow.push(replayableEditInputs);
    replayableEditInputs.clear();

    history.set(stepIndex, fullNow);

    vector<NetworkEvent> sideEffects = now.step(self() ? self()->entityID() : EntityID());

    stepIndex++;

    if (history.has(stepIndex))
    {
        now.makeReplayOf(history.get(stepIndex));
    }

    return sideEffects;
}

bool Controller::stepSystemInputs()
{
    if (client && currentTime() - pingSentTime >= pingInterval)
    {
        pingSentTime = currentTime();
        client->send(PingEvent(true));
    }

    if (client)
    {
        client->step();
    }

    handleNetworkEvents();

    if (serverRunner)
    {
        serverRunner->step();
    }

    return input()->step();
}

bool Controller::stepSystemOutputs()
{
    bool quit = false;

#ifdef LMOD_VR
    if (g_vr)
    {
        quit |= _vr->step();
    }
    else
    {
        _window->step();
    }
#else
    _window->step();
#endif
    _render->step();
    _sound->step(now.state.timeMultiplier() * (1 - now.state.brakeProgress()));

    return quit;
}

void Controller::enterReplay()
{
    // Blocks sending of messages to the server
    inReplayMode = true;
}

void Controller::exitReplay()
{
    inReplayMode = false;
}

void Controller::afterCompare(const Compare& compare, const function<bool(const NetworkEvent&)>& hot)
{
    for (const NetworkEvent& event : compare.sideEffectEvents())
    {
        handleSideEffect(event, hot(event));
    }

    for (const EntityUpdateSideEffect& sideEffect : compare.sideEffects())
    {
        handleSideEffect(sideEffect);
    }
}

void Controller::createClient(const string& serverPassword, const string& name, const string& password)
{
#if defined(__unix__) || defined(__APPLE__)
    client = new ClientPOSIX(
        g_config.caFilePath.empty() ? "" : getFile(g_config.caFilePath),
        bind(&Controller::onConnect, this, serverPassword, name, password),
        bind(&Controller::onReceive, this, placeholders::_1),
        bind(&Controller::onDisconnect, this)
    );
#elif defined(WIN32)
    client = new ClientWindows(
        g_config.caFilePath.empty() ? "" : getFile(g_config.caFilePath),
        bind(&Controller::onConnect, this, serverPassword, name, password),
        bind(&Controller::onReceive, this, placeholders::_1),
        bind(&Controller::onDisconnect, this)
    );
#elif defined(__EMSCRIPTEN__)
    client = new ClientWeb(
        g_config.caFilePath.empty() ? "" : getFile(g_config.caFilePath),
        bind(&Controller::onConnect, this, serverPassword, name, password),
        bind(&Controller::onReceive, this, placeholders::_1),
        bind(&Controller::onDisconnect, this)
    );
#endif
}

void Controller::onConnect(const string& serverPassword, const string& name, const string& password)
{
    _connectionState = Connection_State_Connected;

    JoinEvent event;
    event.serverPassword = serverPassword;
    event.name = name;
    event.password = sha256(password);
    event.version = version;
    event.vr = g_vr;

    client->send(event);
}

void Controller::onReceive(const NetworkEvent& event)
{
    if (ready() && !event.isSystem())
    {
        connection.eventBatcher.push(event);
    }
    else
    {
        systemEvents.push_back(event);
    }
}

void Controller::onDisconnect()
{
    _connectionState = Connection_State_Disconnected;
}

void Controller::handleNetworkEvent(const NetworkEvent& event, bool hot)
{
    switch (event.type())
    {
    default :
        fatal("Encountered unknown system event type " + to_string(event.type()) + " received.");
    case Network_Event_Welcome :
        handleWelcomeEvent(event.payload<WelcomeEvent>(), hot);
        break;
    case Network_Event_Kick :
        handleKickEvent(event.payload<KickEvent>(), hot);
        break;
    case Network_Event_Checkpoint :
        handleCheckpointEvent(event.payload<CheckpointEvent>(), hot);
        break;
    case Network_Event_Document :
        handleDocumentEvent(event.payload<DocumentEvent>(), hot);
        break;
    case Network_Event_Attach :
        handleAttachEvent(event.payload<AttachEvent>(), hot);
        break;
    case Network_Event_Detach :
        handleDetachEvent(event.payload<DetachEvent>(), hot);
        break;
    case Network_Event_Move :
        handleMoveEvent(event.payload<MoveEvent>(), hot);
        break;
    case Network_Event_Haptic :
        handleHapticEvent(event.payload<HapticEvent>(), hot); 
        break;
    case Network_Event_Log :
        handleLogEvent(event.payload<LogEvent>(), hot);
        break;
    case Network_Event_History :
        handleHistoryEvent(event.payload<HistoryEvent>(), hot);
        break;
    case Network_Event_Text_Request :
        handleTextRequestEvent(event.payload<TextRequestEvent>(), hot);
        break;
    case Network_Event_Tether :
        handleTetherEvent(event.payload<TetherEvent>(), hot);
        break;
    case Network_Event_Confinement :
        handleConfinementEvent(event.payload<ConfinementEvent>(), hot);
        break;
    case Network_Event_Fade :
        handleFadeEvent(event.payload<FadeEvent>(), hot);
        break;
    case Network_Event_Wait :
        handleWaitEvent(event.payload<WaitEvent>(), hot);
        break;
    case Network_Event_Voice :
        handleVoiceEvent(event.payload<VoiceEvent>(), hot);
        break;
    case Network_Event_Voice_Channel :
        handleVoiceChannelEvent(event.payload<VoiceChannelEvent>(), hot);
        break;
    case Network_Event_Ping :
        handlePingEvent(event.payload<PingEvent>(), hot);
        break;
    case Network_Event_URL :
        handleURLEvent(event.payload<URLEvent>());
        break;
    case Network_Event_Save :
        handleSaveEvent(event.payload<SaveEvent>());
        break;
    }
}

void Controller::handleWelcomeEvent(const WelcomeEvent& event, bool hot)
{
    _selfID = event.id;
    _splashMessage = event.splashMessage;
    connection.lastRemoteIndex = event.remoteIndex;

    // Handle side effects resulting from the initial state sent by the server
    Compare compare(now, bind(&Controller::stepSimulation, this));
    afterCompare(compare, [](const NetworkEvent& event) -> bool { return false; });
}

void Controller::handleKickEvent(const KickEvent& event, bool hot)
{
    if (!_disconnectionReason.empty())
    {
        return;
    }

    auto setDisconnectionReason = [this](const string& text) -> void {
        error("Disconnected: " + text);
        _disconnectionReason = text;
    };

    switch (event.reason)
    {
    case Disconnection_Reason_Incorrect_Password :
        setDisconnectionReason(localize("controller-login-failed-incorrect-password"));
        break;
    case Disconnection_Reason_User_Already_Logged_In :
        setDisconnectionReason(localize("controller-login-failed-already-logged-in"));
        break;
    case Disconnection_Reason_Kicked :
        setDisconnectionReason(localize("controller-you-were-kicked-from-the-server"));
        break;
    case Disconnection_Reason_Invalid_Username :
        setDisconnectionReason(localize("controller-login-failed-invalid-username"));
        break;
    case Disconnection_Reason_Client_Too_New :
        setDisconnectionReason(localize("controller-login-failed-client-too-new"));
        break;
    case Disconnection_Reason_Client_Too_Old :
        setDisconnectionReason(localize("controller-login-failed-client-too-old"));
        break;
    case Disconnection_Reason_Maximum_Latency_Exceeded :
        setDisconnectionReason(localize("controller-timed-out"));
    }

    disconnect();
}

void Controller::handlePingEvent(const PingEvent& event, bool hot)
{
    if (event.ping)
    {
        client->send(PingEvent(false));
    }
    else
    {
        connection.roundTripTime = (currentTime() - pingSentTime).count();
    }
}

void Controller::handleURLEvent(const URLEvent& event)
{
    _serverJoinURL = event.url;
}

void Controller::handleSaveEvent(const SaveEvent& event)
{
    _serverSaving = !event.stop && serverRunner;
}

void Controller::handleCheckpointEvent(const CheckpointEvent& event, bool hot)
{
    switch (event.type)
    {
    case Checkpoint_Event_Add :
        handleCheckpointAdd(event);
        break;
    case Checkpoint_Event_Remove :
        handleCheckpointRemove(event);
        break;
    case Checkpoint_Event_Revert :
        break;
    }
}

void Controller::handleDocumentEvent(const DocumentEvent& event, bool hot)
{
    switch (event.type)
    {
    case Document_Event_Add :
        handleDocumentAdd(event);
        break;
    case Document_Event_Remove :
        handleDocumentRemove(event);
        break;
    case Document_Event_Update :
        handleDocumentUpdate(event);
        break;
    }
}

void Controller::handleAttachEvent(const AttachEvent& event, bool hot)
{
    _context->attachedMode()->onAttach(
        event.bindings,
        event.hudElements,
        event.fieldOfView,
        event.mouseLocked,
        event.voiceVolume,
        event.tintColor,
        event.tintStrength,
        hot
    );
}

void Controller::handleDetachEvent(const DetachEvent& event, bool hot)
{
    _context->attachedMode()->onDetach();
}

void Controller::handleMoveEvent(const MoveEvent& event, bool hot)
{
    _context->moveViewer(event.transform);
}

void Controller::handleHapticEvent(const HapticEvent& event, bool hot)
{
    if (!hot)
    {
        return;
    }

    input()->haptic(event.effect);
}

void Controller::handleLogEvent(const LogEvent& event, bool hot)
{
    if (hot)
    {
        _context->interface()->log(event.message);
    }
    else
    {
        Console* console = _context->interface()->getWindow<Console>();

        if (!console)
        {
            return;
        }

        console->log(event.message);
    }
}

void Controller::handleHistoryEvent(const HistoryEvent& event, bool hot)
{
    Console* console = _context->interface()->getWindow<Console>();

    if (!console)
    {
        return;
    }

    console->recordHistory(event.command);
}

void Controller::handleTextRequestEvent(const TextRequestEvent& event, bool hot)
{
    _context->text()->request(event.request);
}

void Controller::handleTetherEvent(const TetherEvent& event, bool hot)
{
    switch (event.type)
    {
    case Tether_Event_Tether :
        _context->tether()->tether(event.userIDs, event.distance);
        break;
    case Tether_Event_Untether :
        _context->tether()->untether();
        break;
    }
}

void Controller::handleConfinementEvent(const ConfinementEvent& event, bool hot)
{
    switch (event.type)
    {
    case Confinement_Event_Confine :
        _context->confinement()->start(event.position, event.distance);
        break;
    case Confinement_Event_Unconfine :
        _context->confinement()->end();
        break;
    }
}

void Controller::handleFadeEvent(const FadeEvent& event, bool hot)
{
    switch (event.type)
    {
    case Fade_Event_Fade :
        _context->fade()->fadeOut();
        break;
    case Fade_Event_Unfade :
        _context->fade()->fadeIn();
        break;
    }
}

void Controller::handleWaitEvent(const WaitEvent& event, bool hot)
{
    switch (event.type)
    {
    case Wait_Event_Wait :
        _context->wait()->start();
        break;
    case Wait_Event_Unwait :
        _context->wait()->end();
        break;
    }
}

void Controller::handleVoiceEvent(const VoiceEvent& event, bool hot)
{
    if (!ready())
    {
        return;
    }

    if (event.id == selfID())
    {
        return;
    }

    auto it = voiceDecoders.find(event.id);

    VoiceDecoder* decoder;

    if (it != voiceDecoders.end())
    {
        decoder = it->second;
    }
    else
    {
        decoder = new VoiceDecoder();
        voiceDecoders.insert({ event.id, decoder });
    }

    _sound->pushVoice(
        event.id,
        event.position,
        event.rotation,
        event.velocity,
        decoder->decode(event.data)
    );
}

void Controller::handleVoiceChannelEvent(const VoiceChannelEvent& event, bool hot)
{
    _voiceChannel = event.name;
}

void Controller::handleCheckpointAdd(const CheckpointEvent& event)
{
    _checkpoints.insert(event.name);
}

void Controller::handleCheckpointRemove(const CheckpointEvent& event)
{
    _checkpoints.erase(event.name);
}

void Controller::handleDocumentAdd(const DocumentEvent& event)
{
    auto it = _documents.find(event.name);

    if (it != _documents.end())
    {
        return;
    }

    _documents.insert({ event.name, "" });
}

void Controller::handleDocumentRemove(const DocumentEvent& event)
{
    auto it = _documents.find(event.name);

    if (it == _documents.end())
    {
        return;
    }

    _documents.erase(it);
}

void Controller::handleDocumentUpdate(const DocumentEvent& event)
{
    auto it = _documents.find(event.name);

    if (it == _documents.end())
    {
        return;
    }

    it->second = event.text;
}

void Controller::handleSideEffect(const EntityUpdateSideEffect& effect)
{
    Entity* entity = now.state.get(effect.id);

    switch (effect.type)
    {
    case Entity_Update_Display_Add :
        _render->add(effect.parentID, entity);
        _sound->add(effect.parentID, entity);
        break;
    case Entity_Update_Display_Remove :
        _render->remove(effect.parentID, effect.id);
        _sound->remove(effect.parentID, effect.id);
        break;
    case Entity_Update_Display_Update_Render :
        _render->update(effect.parentID, entity);
        break;
    case Entity_Update_Display_Update_Sound :
        _sound->update(effect.parentID, entity);
        break;
    case Entity_Update_Attachment_Update :
    {
        Entity* host = this->host();

        if (host && host->id() == effect.id)
        {
            _context->attachedMode()->onAttach(
                host->bindings(),
                host->hudElements(),
                host->fieldOfView(),
                host->mouseLocked(),
                host->voiceVolume(),
                host->tintColor(),
                host->tintStrength(),
                false
            );
        }
        break;
    }
    }
}

void Controller::handleSideEffect(const NetworkEvent& event, bool hot)
{
    switch (event.type())
    {
    default :
        break;
    case Network_Event_User :
    {
        auto userEvent = event.payload<UserEvent>();

        switch (userEvent.type)
        {
        default :
            break;
        case User_Event_Add :
            _context->userVisualizer()->add(userEvent.id);
            break;
        case User_Event_Remove :
            _context->userVisualizer()->remove(userEvent.id);
            break;
        case User_Event_Shift :
            _context->userVisualizer()->setWorld(userEvent.id, userEvent.s);
            break;
        case User_Event_Move_Viewer :
            _context->userVisualizer()->moveViewer(userEvent.id, userEvent.transform);
            break;
        case User_Event_Move_Room :
            _context->userVisualizer()->moveRoom(userEvent.id, userEvent.transform);
            break;
        case User_Event_Move_Devices :
            for (const auto& [ device, transform ] : userEvent.mapping)
            {
                _context->userVisualizer()->moveDevice(userEvent.id, device, transform);
            }
            break;
        case User_Event_Angle :
            _context->userVisualizer()->setViewerAngle(userEvent.id, userEvent.f);
            break;
        case User_Event_Aspect :
            _context->userVisualizer()->setViewerAspect(userEvent.id, userEvent.f);
            break;
        case User_Event_Bounds :
            _context->userVisualizer()->setRoomBounds(userEvent.id, userEvent.bounds);
            break;
        case User_Event_Join :
            _context->userVisualizer()->join(userEvent.id);
            break;
        case User_Event_Leave :
            _context->userVisualizer()->leave(userEvent.id);
            break;
        }
        break;
    }
    case Network_Event_Game :
    {
        auto gameEvent = event.payload<GameEvent>();

        switch (gameEvent.type)
        {
        default :
            break;
        case Game_Event_Ping :
            if (hot)
            {
                User* user = now.state.getUser(UserID(gameEvent.u));

                _context->interface()->ping(user);
            }
            break;
        case Game_Event_Brake :
            if (hot)
            {
                User* user = now.state.getUser(UserID(gameEvent.u));

                _context->interface()->brake(user);
            }
            break;
        }
        break;
    }
    }
}

void Controller::saveConnectionDetails(
    const Address& address,
    const string& serverPassword,
    const string& name,
    const string& password
) const
{
    ConnectionDetails details;

    string path = configPath() + connectionFileName;

    if (fs::exists(path))
    {
        try
        {
            YAMLParser ctx(path);

            details = ctx.root().as<ConnectionDetails>();
        }
        catch (const exception& e)
        {

        }
    }

    details.connect.host = address.host;
    details.connect.port = address.port;
    details.connect.serverPassword = serverPassword;
    details.connect.name = name;
    details.connect.password = password;

    YAMLSerializer ctx(path);

    ctx.emit(details);
}

void Controller::saveConnectionDetails(
    const Address& address,
    const string& serverPassword,
    const string& adminPassword,
    const string& savePath,
    const string& connectHost,
    const string& name,
    const string& password
) const
{
    ConnectionDetails details;

    string path = configPath() + connectionFileName;

    if (fs::exists(path))
    {
        try
        {
            YAMLParser ctx(path);

            details = ctx.root().as<ConnectionDetails>();
        }
        catch (const exception& e)
        {

        }
    }

    details.host.host = address.host;
    details.host.port = address.port;
    details.host.serverPassword = serverPassword;
    details.host.adminPassword = adminPassword;
    details.host.savePath = savePath;
    details.host.name = name;
    details.host.password = password;

    details.connect.host = connectHost;
    details.connect.port = address.port;
    details.connect.serverPassword = serverPassword;
    details.connect.name = name;
    details.connect.password = password;

    YAMLSerializer ctx(path);

    ctx.emit(details);
}

void Controller::clear()
{
    if (serverRunner)
    {
        delete serverRunner;
        serverRunner = nullptr;
    }

    for (const auto& [ id, decoder ] : voiceDecoders)
    {
        delete decoder;
    }
    voiceDecoders.clear();

    if (client)
    {
        delete client;
        client = nullptr;
    }

    pingSentTime = chrono::milliseconds(0);
    _disconnectionReason = "";
    _connectionState = Connection_State_Standby;

    systemEvents.clear();
    connection = Connection();

    _lastStepTime = chrono::milliseconds(0);

    replayableEditInputs.clear();
    inReplayMode = false;
    history = StepFrameHistory();
    now = StepFrame();
    stepIndex = 0;

    _serverSaving = false;
    _serverJoinURL = "";
    _checkpoints.clear();
    _documents.clear();
    _voiceChannel = "";
    _splashMessage = "";
    _selfID = UserID();

    delete _context;
}

void Controller::reset()
{
    _render->waitIdle();

#ifdef LMOD_VR
    if (g_vr)
    {
        _vr->resetRoom();
    }
#endif

    clear();

    _context = new ControlContext(this);
}
