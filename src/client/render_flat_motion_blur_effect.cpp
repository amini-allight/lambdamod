/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_flat_motion_blur_effect.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_descriptor_set_write_components.hpp"

RenderFlatMotionBlurEffect::RenderFlatMotionBlurEffect(
    const RenderContext* ctx,
    RenderFlatPipelineStore* pipelineStore,
    const vector<FlatSwapchainElement*>& swapchainElements,
    f64 strength
)
    : RenderFlatPostProcessEffect(
        ctx,
        pipelineStore->motionBlur.pipelineLayout,
        pipelineStore->motionBlur.pipeline
    )
    , strength(strength)
{
    storageImage = new RenderMotionBlurStorageImage(
        ctx,
        swapchainElements.front()->width(),
        swapchainElements.front()->height(),
        1
    );

    for (size_t i = 0; i < swapchainElements.size(); i++)
    {
        auto component = new RenderPostProcessEffectComponent(ctx, pipelineStore->motionBlurDescriptorSetLayout, createUniformBuffer());

        setDescriptorSet(
            component->descriptorSet,
            component->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->depthImageView(),
            storageImage->imageView
        );

        components.push_back(component);
    }
}

RenderFlatMotionBlurEffect::~RenderFlatMotionBlurEffect()
{
    destroyComponents();

    delete storageImage;
}

void RenderFlatMotionBlurEffect::work(const FlatSwapchainElement* swapchainElement, f32 timer) const
{
    // Gated internally
    storageImage->setupLayout(swapchainElement->transferCommandBuffer());

    storageImage->barrier(swapchainElement->transferCommandBuffer());

    RenderFlatPostProcessEffect::work(swapchainElement, timer);
}

void RenderFlatMotionBlurEffect::refresh(const vector<FlatSwapchainElement*>& swapchainElements)
{
    delete storageImage;

    storageImage = new RenderMotionBlurStorageImage(
        ctx,
        swapchainElements.front()->width(),
        swapchainElements.front()->height(),
        1
    );

    for (u32 i = 0; i < swapchainElements.size(); i++)
    {
        setDescriptorSet(
            components.at(i)->descriptorSet,
            components.at(i)->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->depthImageView(),
            storageImage->imageView
        );
    }
}

void RenderFlatMotionBlurEffect::update(f64 strength)
{
    this->strength = strength;
}

VmaBuffer RenderFlatMotionBlurEffect::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(f32) * 2,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderFlatMotionBlurEffect::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    memcpy(mapping->data, &strength, sizeof(f32));
    memcpy(mapping->data + 1, &timer, sizeof(f32));
}

void RenderFlatMotionBlurEffect::setDescriptorSet(
    VkDescriptorSet descriptorSet,
    VkBuffer uniformBuffer,
    VkImageView colorImageView,
    VkImageView depthImageView,
    VkImageView storageImageView
) const
{
    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, uniformBuffer, &parametersWriteBuffer, &parametersWrite);

    VkDescriptorImageInfo colorImageWriteImage{};
    VkWriteDescriptorSet colorImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, colorImageView, ctx->unfilteredSampler, &colorImageWriteImage, &colorImageWrite);

    VkDescriptorImageInfo depthImageWriteImage{};
    VkWriteDescriptorSet depthImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 2, depthImageView, ctx->unfilteredSampler, &depthImageWriteImage, &depthImageWrite);

    VkDescriptorImageInfo storageImageWriteImage{};
    VkWriteDescriptorSet storageImageWrite{};

    storageImageDescriptorSetWrite(descriptorSet, 3, storageImageView, &storageImageWriteImage, &storageImageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        parametersWrite,
        colorImageWrite,
        depthImageWrite,
        storageImageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}
