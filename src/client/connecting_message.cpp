/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "connecting_message.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "control_context.hpp"

ConnectingMessage::ConnectingMessage(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
    , elapsed(0)
    , lastTime(currentTime())
{

}

void ConnectingMessage::step()
{
    InterfaceWidget::step();

    elapsed += (currentTime() - lastTime).count() / 1000.0;
    lastTime = currentTime();
}

void ConnectingMessage::draw(const DrawContext& ctx) const
{
    drawSolid(ctx, this, theme.worldBackgroundColor);

    drawImage(
        ctx,
        this,
        ((size() / 2) - (UIScale::spinnerSize() / 2)).toPoint(),
        UIScale::spinnerSize(),
        Icon_Spinner,
        1,
        tau * ((elapsed * 1000) / spinnerRevolutionInterval.count())
    );

    TextStyle style;
    style.alignment = Text_Center;
    style.weight = Text_Bold;
    style.size = UIScale::mediumFontSize();
    style.color = theme.panelForegroundColor;

    drawText(
        ctx,
        this,
        Point(0, (size().h / 2) + (UIScale::spinnerSize().h / 2) + UIScale::labelHeight()),
        Size(size().w, UIScale::labelHeight()),
        localize("connecting-message-connecting"),
        style
    );
}

bool ConnectingMessage::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && context()->controller()->connectionState() == Connection_State_Connecting;
}

SizeProperties ConnectingMessage::sizeProperties() const
{
    return sizePropertiesFillAll;
}
