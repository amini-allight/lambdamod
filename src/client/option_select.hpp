/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "popup.hpp"
#include "scrollable.hpp"

class OptionSelectPopup : public Popup, public Scrollable
{
public:
    OptionSelectPopup(
        InterfaceWidget* parent,
        const vector<string>& options,
        const vector<string>& optionTooltips,
        const function<void(size_t)>& onSet
    );

    void step() override;
    void draw(const DrawContext& ctx) const override;

    bool canClose() const;

protected:
    friend class OptionSelect;

    const vector<string>& options;
    const vector<string>& optionTooltips;
    function<void(size_t)> onSet;
    i32 _scrollIndex;
    i32 activeIndex;

    i32 viewHeight() const override;

    i32 scrollIndex() const override;
    void setScrollIndex(i32 index) override;

    i32 childCount() const override;
    i32 childHeight() const override;

    void interact(const Input& input);
    void scrollUp(const Input& input);
    void scrollDown(const Input& input);
    void goToStart(const Input& input);
    void goToEnd(const Input& input);
    void release(const Input& input);
    void drag(const Input& input);

    SizeProperties sizeProperties() const override;
};

class OptionSelect : public InterfaceWidget
{
public:
    OptionSelect(
        InterfaceWidget* parent,
        const function<vector<string>()>& optionsSource,
        const function<size_t()>& source,
        const function<void(size_t)>& onSet
    );
    OptionSelect(
        InterfaceWidget* parent,
        const function<vector<string>()>& optionsSource,
        const function<vector<string>()>& optionTooltipsSource,
        const function<size_t()>& source,
        const function<void(size_t)>& onSet
    );
    ~OptionSelect();

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;
    void focus() override;
    void unfocus() override;

protected:
    vector<string> options;
    vector<string> optionTooltips;
    size_t index;

    OptionSelectPopup* popup;

    function<vector<string>()> optionsSource;
    function<vector<string>()> optionTooltipsSource;
    function<size_t()> source;
    function<void(size_t)> onSet;

    void onSelect(size_t index);

    void interact(const Input& input);

    SizeProperties sizeProperties() const override;
};
