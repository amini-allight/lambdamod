/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "user.hpp"
#include "fuzzy_find.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "label.hpp"
#include "indicator_icon.hpp"
#include "standalone_color_picker_dialog.hpp"

#include "confirm_dialog.hpp"
#include "list_dialog.hpp"

UserDetail::UserDetail()
{
    active = false;
    admin = false;
    vr = false;
    attached = false;
    magnitude = 0;
    advantage = 0;
    doom = false;
    ping = 0;
}

UserDetail::UserDetail(UserID id, const User& user)
    : UserDetail()
{
    this->id = id;
    name = user.name();
    color = user.color();
    active = user.active();
    admin = user.admin();
    vr = user.vr();
    attached = user.attached();
    magnitude = user.magnitude();
    advantage = user.advantage();
    doom = user.doom();
    ping = user.ping();
}

bool UserDetail::operator==(const UserDetail& rhs) const
{
    return id == rhs.id &&
        name == rhs.name &&
        color == rhs.color &&
        active == rhs.active &&
        admin == rhs.admin &&
        vr == rhs.vr &&
        attached == rhs.attached &&
        magnitude == rhs.magnitude &&
        advantage == rhs.advantage &&
        doom == rhs.doom &&
        ping == rhs.ping;
}

bool UserDetail::operator!=(const UserDetail& rhs) const
{
    return !(*this == rhs);
}

UserSettings::UserSettings(InterfaceWidget* parent)
    : RightClickableWidget(parent)
{
    auto column = new Column(this);

    auto header = new Row(column);

    TextStyleOverride style;
    style.weight = Text_Bold;
    style.color = &theme.activeTextColor;
    style.alignment = Text_Center;

    new Label(header, localize("user-settings-name"), style);
    new Label(header, localize("user-settings-active"), style);
    new Label(header, localize("user-settings-admin"), style);
    new Label(header, localize("user-settings-mode"), style);
    new Label(header, localize("user-settings-attached"), style);
    new Label(header, localize("user-settings-magnitude"), style);
    new Label(header, localize("user-settings-advantage"), style);
    new Label(header, localize("user-settings-doom"), style);
    new Label(header, localize("user-settings-ping"), style);
    MAKE_SPACER(header, UIScale::scrollbarWidth(), UIScale::labelHeight());

    searchBar = new LineEdit(column, nullptr, [](const string& text) -> void {});
    searchBar->setPlaceholder(localize("user-settings-type-to-search"));

    listView = new ListView(column);
}

void UserSettings::step()
{
    RightClickableWidget::step();

    vector<UserDetail> details = convert(context()->controller()->game().users());
    string search = searchBar->content();

    if (details != this->details || search != this->search)
    {
        this->details = details;
        this->search = search;
        updateListView();
    }
}

vector<UserDetail> UserSettings::convert(const map<UserID, User>& users) const
{
    vector<UserDetail> details;
    details.reserve(users.size());

    for (const auto& [ id, user ] : users)
    {
        details.push_back(UserDetail(id, user));
    }

    return details;
}

void UserSettings::openRightClickMenu(i32 index, const Point& pointer)
{
    if (index >= static_cast<i32>(details.size()))
    {
        return;
    }

    const UserDetail& detail = details[index];

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("user-settings-remove"), bind(&UserSettings::onRemove, this, detail.id, detail.name, placeholders::_1) },
        { localize("user-settings-kick"), bind(&UserSettings::onKick, this, detail.id, placeholders::_1) },
        { localize("user-settings-set-color"), bind(&UserSettings::onSetColor, this, detail.id, placeholders::_1) },
        { localize("user-settings-manage-teams"), bind(&UserSettings::onManageTeams, this, detail.id, placeholders::_1) },
        { localize("user-settings-teleport-to-user"), bind(&UserSettings::onTeleportToUser, this, detail.id, placeholders::_1) },
        { localize("user-settings-teleport-user-to-me"), bind(&UserSettings::onTeleportUserToMe, this, detail.id, placeholders::_1) },
        { localize("user-settings-increase-advantage"), bind(&UserSettings::onAdvantageUp, this, detail.id, placeholders::_1) },
        { localize("user-settings-decrease-advantage"), bind(&UserSettings::onAdvantageDown, this, detail.id, placeholders::_1) },
        { localize("user-settings-flag-doom"), bind(&UserSettings::onDoomFlag, this, detail.id, placeholders::_1) },
        { localize("user-settings-clear-doom"), bind(&UserSettings::onDoomClear, this, detail.id, placeholders::_1) }
    };

    rightClickMenu = new RightClickMenu(
        this,
        bind(&UserSettings::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void UserSettings::onRemove(UserID id, const string& name, const Point& point)
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("user-settings-are-you-sure-you-want-to-delete-user", name),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            UserEvent event;
            event.type = User_Event_Remove;
            event.id = id;

            context()->controller()->send(event);
        }
    );

    closeRightClickMenu();
}

void UserSettings::onKick(UserID id, const Point& point)
{
    UserEvent event;
    event.type = User_Event_Kick;
    event.id = id;

    context()->controller()->send(event);

    closeRightClickMenu();
}

void UserSettings::onSetColor(UserID id, const Point& point)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    context()->interface()->addWindow<StandaloneColorPickerDialog>(
        Color(user->color()),
        bind(&UserSettings::onColor, this, id, placeholders::_1)
    );

    closeRightClickMenu();
}

void UserSettings::onManageTeams(UserID id, const Point& point)
{
    context()->interface()->addWindow<ListDialog>(
        localize("user-settings-teams"),
        bind(&UserSettings::teamsSource, this, id),
        bind(&UserSettings::onAddTeam, this, id, placeholders::_1),
        bind(&UserSettings::onRemoveTeam, this, id, placeholders::_1)
    );

    closeRightClickMenu();
}

void UserSettings::onTeleportToUser(UserID id, const Point& point)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    {
        UserEvent event(User_Event_Shift);
        event.s = user->world();

        context()->controller()->send(event);
    }

    context()->moveViewer(user->viewer());

    closeRightClickMenu();
}

void UserSettings::onTeleportUserToMe(UserID id, const Point& point)
{
    {
        UserEvent event;
        event.type = User_Event_Shift_Forced;
        event.id = id;
        event.s = context()->controller()->self()->world();

        context()->controller()->send(event);
    }

    {
        UserEvent event;
        event.type = User_Event_Move_Forced;
        event.id = id;
        event.transform = { context()->flatViewer()->position(), context()->flatViewer()->rotation(), vec3(1) };

        context()->controller()->send(event);
    }

    closeRightClickMenu();
}

void UserSettings::onAdvantageUp(UserID id, const Point& point)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    UserEvent event;
    event.type = User_Event_Advantage;
    event.id = id;
    event.i = user->advantage() + 1;

    context()->controller()->send(event);

    closeRightClickMenu();
}

void UserSettings::onAdvantageDown(UserID id, const Point& point)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    UserEvent event;
    event.type = User_Event_Advantage;
    event.id = id;
    event.i = user->advantage() - 1;

    context()->controller()->send(event);

    closeRightClickMenu();
}

void UserSettings::onDoomFlag(UserID id, const Point& point)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    UserEvent event;
    event.type = User_Event_Doom;
    event.id = id;
    event.b = true;

    context()->controller()->send(event);

    closeRightClickMenu();
}

void UserSettings::onDoomClear(UserID id, const Point& point)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    UserEvent event;
    event.type = User_Event_Doom;
    event.id = id;
    event.b = false;

    context()->controller()->send(event);

    closeRightClickMenu();
}

vector<string> UserSettings::teamsSource(UserID id)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return {};
    }

    return { user->teams().begin(), user->teams().end() };
}

void UserSettings::onAddTeam(UserID id, const string& name)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    set<string> teams = user->teams();

    teams.insert(name);

    UserEvent event;
    event.type = User_Event_Teams;
    event.id = id;
    event.teams = teams;

    context()->controller()->send(event);
}

void UserSettings::onRemoveTeam(UserID id, const string& name)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    set<string> teams = user->teams();

    teams.erase(name);

    UserEvent event;
    event.type = User_Event_Teams;
    event.id = id;
    event.teams = teams;

    context()->controller()->send(event);
}

void UserSettings::onColor(UserID id, const Color& color)
{
    const User* user = context()->controller()->game().getUser(id);

    if (!user)
    {
        return;
    }

    UserEvent event;
    event.type = User_Event_Color;
    event.id = id;
    event.color = color.toVec3();

    context()->controller()->send(event);
}

void UserSettings::updateListView()
{
    listView->clearChildren();

    // If we sort the member variable version it will cause an update on every UserSettings::step
    vector<UserDetail> details = this->details;

    if (!search.empty())
    {
        sort(
            details.begin(),
            details.end(),
            [&](const UserDetail& a, const UserDetail& b) -> bool
            {
                return fuzzyFindCompare(a.name, b.name, search);
            }
        );
    }

    size_t i = 0;
    for (const UserDetail& detail : details)
    {
        auto row = new Row(
            listView,
            {},
            [this, i](const Point& pointer) -> void
            {
                onRightClick(i, pointer);
            }
        );

        new Label(row, detail.name);

        new IndicatorIcon(row, detail.active ? Icon_Online : Icon_Offline, detail.active ? theme.positiveColor : theme.negativeColor);
        new IndicatorIcon(row, detail.admin ? Icon_Admin : Icon_None);
        new IndicatorIcon(row, detail.vr ? Icon_VR : Icon_Flat);
        new IndicatorIcon(row, detail.attached ? Icon_Yes : Icon_No, detail.attached ? theme.positiveColor : theme.negativeColor);

        TextStyleOverride style;
        style.alignment = Text_Center;

        new Label(row, to_string(detail.magnitude), style);
        new Label(row, (detail.advantage > 0 ? "+" : "") + to_string(detail.advantage), style);
        new IndicatorIcon(row, detail.doom ? Icon_Yes : Icon_No, detail.attached ? theme.positiveColor : theme.negativeColor);
        new Label(row, detail.active ? to_string(detail.ping) : "", style);

        i++;
    }

    listView->update();
}

SizeProperties UserSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
