/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "side_panel.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

using namespace std;

SidePanel::SidePanel(InterfaceWidget* parent)
    : InterfaceWidget(parent)
    , _open(false)
{
    START_WIDGET_SCOPE("side-panel")

    END_SCOPE
}

void SidePanel::toggle()
{
    _open = !_open;
}

bool SidePanel::open() const
{
    return _open;
}

void SidePanel::draw(const DrawContext& ctx) const
{
    if (!_open)
    {
        return;
    }

    drawSolid(ctx, this, theme.altBackgroundColor);

    InterfaceWidget::draw(ctx);
}

bool SidePanel::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && _open;
}

SizeProperties SidePanel::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::sidePanelWidth()), 0,
        Scaling_Fixed, Scaling_Fill
    };
}
