/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_scope.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "control_context.hpp"

InputScope::InputScope(
    ControlContext* ctx,
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<bool(const Input&)>& isActive,
    const function<void(InputScope*)>& block
)
    : ctx(ctx)
    , _parent(parent)
    , name(name)
    , mode(mode)
    , isActive(isActive)
{
    if (_parent)
    {
        _parent->add(this);
    }

    block(this);
}

InputScope::~InputScope()
{
    vector<InputScope*> children = this->children;

    for (InputScope* child : children)
    {
        delete child;
    }

    if (_parent)
    {
        _parent->remove(this);
    }
}

InputScope* InputScope::parent() const
{
    return _parent;
}

vector<string> InputScope::path() const
{
    return join(_parent ? _parent->path() : vector<string>(), vector<string>({ name }));
}

void InputScope::showActivePath(size_t indent) const
{
    cout << string(indent * indentSize, ' ') << (shouldInvokeBindings(Input()) ? "*" : "") << name << endl;

    for (InputScope* child : children)
    {
        if (!child->shouldHandleInput(Input()))
        {
            continue;
        }

        child->showActivePath(indent + 1);
    }
}

bool InputScope::movePointer(PointerType type, const Point& position, bool consumed) const
{
    for (size_t i = children.size() - 1; i < children.size(); i--)
    {
        consumed |= children[i]->movePointer(type, position, consumed);
    }

    return consumed;
}

bool InputScope::moveLeftHand(const vec3& position, const quaternion& rotation, bool consumed) const
{
    for (size_t i = children.size() - 1; i < children.size(); i--)
    {
        if (consumed)
        {
            return consumed;
        }

        consumed |= children[i]->moveLeftHand(position, rotation, consumed);
    }

    return consumed;
}

bool InputScope::moveRightHand(const vec3& position, const quaternion& rotation, bool consumed) const
{
    for (size_t i = children.size() - 1; i < children.size(); i--)
    {
        if (consumed)
        {
            return consumed;
        }

        consumed |= children[i]->moveRightHand(position, rotation, consumed);
    }

    return consumed;
}

InputHandlingResult InputScope::handle(const Input& input, u32 repeats, size_t depth) const
{
    InputHandlingResult result(ctx, input);

    for (size_t i = children.size() - 1; i < children.size(); i--)
    {
        const InputScope* child = children[i];

        if (!child->shouldHandleInput(input))
        {
            continue;
        }

        result.merge(child->handle(input, repeats, depth + 1));
    }

    if (shouldInvokeBindings(input))
    {
        for (const auto& [ binding, slot ] : allBindings())
        {
            if (!binding.match(input, repeats))
            {
                continue;
            }

            result.add(path(), binding, slot, depth);
        }
    }

    result.setBlocking(mode == Input_Scope_Blocking);

    return result;
}

void InputScope::bind(const string& bindingName, const function<void(const Input&)>& behavior)
{
    if (bindings.contains(bindingName))
    {
        warning("Double binding of '" + bindingName + "' encountered in scope '" + name + "'.");
    }

    map<InputScopeBinding, InputScopeSlot> allBindings = this->allBindings();

    for (const InputScopeBinding& binding : ctx->input()->getBindings(bindingName))
    {
        if (allBindings.contains(binding))
        {
            warning("Double binding of '" + binding.toString() + "' encountered in scope '" + name + "'.");
        }
    }

    bindings.insert_or_assign(bindingName, InputScopeSlot(behavior));
}

void InputScope::replayableBind(const string& bindingName, const function<void(const Input&)>& behavior)
{
    if (bindings.contains(bindingName))
    {
        warning("Double binding of '" + bindingName + "' encountered in scope '" + name + "'.");
    }

    map<InputScopeBinding, InputScopeSlot> allBindings = this->allBindings();

    for (const InputScopeBinding& binding : ctx->input()->getBindings(bindingName))
    {
        if (allBindings.contains(binding))
        {
            warning("Double binding of '" + binding.toString() + "' encountered in scope '" + name + "'.");
        }
    }

    bindings.insert_or_assign(bindingName, InputScopeSlot(behavior, true));
}

void InputScope::inclusionBind(
    const string& bindingName,
    const function<void(const Input&)>& behavior,
    InputScopeSlotInclusionMode inclusionMode
)
{
    if (bindings.contains(bindingName))
    {
        warning("Double binding of '" + bindingName + "' encountered in scope '" + name + "'.");
    }

    map<InputScopeBinding, InputScopeSlot> allBindings = this->allBindings();

    for (const InputScopeBinding& binding : ctx->input()->getBindings(bindingName))
    {
        if (allBindings.contains(binding))
        {
            warning("Double binding of '" + binding.toString() + "' encountered in scope '" + name + "'.");
        }
    }

    bindings.insert_or_assign(bindingName, InputScopeSlot(behavior, false, inclusionMode));
}

InputScope* InputScope::get(const string& name)
{
    if (this->name == name)
    {
        return this;
    }

    for (InputScope* child : children)
    {
        InputScope* result = child->get(name);

        if (result)
        {
            return result;
        }
    }

    return nullptr;
}

void InputScope::add(InputScope* child)
{
    children.push_back(child);
}

void InputScope::remove(InputScope* child)
{
    children.erase(find(children.begin(), children.end(), child));
}

bool InputScope::shouldHandleInput(const Input& input) const
{
    if (mode == Input_Scope_Normal)
    {
        return dependentlyActive(input) || hasActiveChild(input);
    }
    else
    {
        return independentlyActive(input) || hasActiveChild(input);
    }
}

bool InputScope::shouldInvokeBindings(const Input& input) const
{
    if (mode == Input_Scope_Normal)
    {
        return dependentlyActive(input);
    }
    else
    {
        return independentlyActive(input);
    }
}

bool InputScope::independentlyActive(const Input& input) const
{
    return isActive(input);
}

bool InputScope::dependentlyActive(const Input& input) const
{
    return isActive(input) && (_parent ? _parent->dependentlyActive(input) : true);
}

bool InputScope::hasActiveChild(const Input& input) const
{
    return any_of(
        children.begin(),
        children.end(),
        [input](const InputScope* child) -> bool { return child->shouldHandleInput(input); }
    );
}

map<InputScopeBinding, InputScopeSlot> InputScope::allBindings() const
{
    map<InputScopeBinding, InputScopeSlot> allBindings;

    for (const auto& [ bindingName, slot ] : bindings)
    {
        for (const InputScopeBinding& binding : ctx->input()->getBindings(bindingName))
        {
            allBindings.insert_or_assign(binding, slot);
        }
    }

    return allBindings;
}
