/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "general_editor.hpp"
#include "localization.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "theme.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"
#include "line_edit.hpp"
#include "checkbox.hpp"
#include "vec3_edit.hpp"
#include "rotation_edit.hpp"
#include "uint_edit.hpp"
#include "ufloat_edit.hpp"
#include "float_edit.hpp"
#include "spercent_edit.hpp"
#include "percent_edit.hpp"
#include "dynamic_label.hpp"
#include "searchable_option_select.hpp"
#include "option_select.hpp"
#include "dynamic_text_button.hpp"
#include "radio_bar.hpp"
#include "tools.hpp"
#include "units.hpp"
#include "entity_display_name.hpp"
#include "body_part_display_name.hpp"

GeneralEditor::GeneralEditor(InterfaceWidget* parent, EntityID id)
    : InterfaceWidget(parent)
    , id(id)
    , localSpace(false)
{
    START_WIDGET_SCOPE("general-editor")
        WIDGET_SLOT("undo", undo)
        WIDGET_SLOT("redo", redo)
    END_SCOPE

    auto listView = new ListView(
        this,
        [](size_t index) -> void {},
        theme.backgroundColor
    );

    new EditorEntry(listView, "general-editor-id", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&GeneralEditor::idSource, this)
        );
    });

    new EditorEntry(listView, "general-editor-name", [this](InterfaceWidget* parent) -> void {
        new LineEdit(
            parent,
            bind(&GeneralEditor::nameSource, this),
            bind(&GeneralEditor::onName, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-parent", [this](InterfaceWidget* parent) -> void {
        new SearchableOptionSelect(
            parent,
            bind(&GeneralEditor::parentOptionsSource, this),
            bind(&GeneralEditor::parentSource, this),
            bind(&GeneralEditor::onParent, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-parent-part", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&GeneralEditor::parentPartOptionsSource, this),
            bind(&GeneralEditor::parentPartSource, this),
            bind(&GeneralEditor::onParentPart, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-parent-subpart", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&GeneralEditor::parentSubpartOptionsSource, this),
            bind(&GeneralEditor::parentSubpartSource, this),
            bind(&GeneralEditor::onParentSubpart, this, placeholders::_1)
        );
    });

    new EditorMultiEntry(listView, 1, [this](InterfaceWidget* parent, size_t index) -> void {
        new RadioBar(
            parent,
            { localize("general-editor-global"), localize("general-editor-local") },
            bind(&GeneralEditor::spaceSource, this),
            bind(&GeneralEditor::onSpace, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-position", lengthSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&GeneralEditor::positionSource, this),
            bind(&GeneralEditor::onPosition, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-rotation", [this](InterfaceWidget* parent) -> void {
        new RotationEdit(
            parent,
            bind(&GeneralEditor::rotationSource, this),
            bind(&GeneralEditor::onRotation, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-scale", [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&GeneralEditor::scaleSource, this),
            bind(&GeneralEditor::onScale, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-linear-velocity", speedSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&GeneralEditor::linearVelocitySource, this),
            bind(&GeneralEditor::onLinearVelocity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-angular-velocity", rotationalSpeedSuffix(), [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&GeneralEditor::angularVelocitySource, this),
            bind(&GeneralEditor::onAngularVelocity, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-owner", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&GeneralEditor::ownerOptionsSource, this),
            bind(&GeneralEditor::ownerSource, this),
            bind(&GeneralEditor::onOwner, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-shared", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::sharedSource, this),
            bind(&GeneralEditor::onShared, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-attached-user", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&GeneralEditor::attachedUserSource, this)
        );
    });

    new EditorEntry(listView, "general-editor-selecting-user", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&GeneralEditor::selectingUserSource, this)
        );
    });

    new EditorEntry(listView, "general-editor-mass", massSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&GeneralEditor::massSource, this),
            bind(&GeneralEditor::onMass, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-volume", volumeSuffix(), [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&GeneralEditor::volumeSource, this)
        );
    });

    new EditorEntry(listView, "general-editor-density", densitySuffix(), [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&GeneralEditor::densitySource, this)
        );
    });

    new EditorEntry(listView, "general-editor-drag", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&GeneralEditor::dragSource, this),
            bind(&GeneralEditor::onDrag, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-buoyancy", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&GeneralEditor::buoyancySource, this),
            bind(&GeneralEditor::onBuoyancy, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-lift", [this](InterfaceWidget* parent) -> void {
        new FloatEdit(
            parent,
            bind(&GeneralEditor::liftSource, this),
            bind(&GeneralEditor::onLift, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-magnetism", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new SPercentEdit(
            parent,
            bind(&GeneralEditor::magnetismSource, this),
            bind(&GeneralEditor::onMagnetism, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-friction", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&GeneralEditor::frictionSource, this),
            bind(&GeneralEditor::onFriction, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-restitution", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&GeneralEditor::restitutionSource, this),
            bind(&GeneralEditor::onRestitution, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-host", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::hostSource, this),
            bind(&GeneralEditor::onHost, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-shown", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::shownSource, this),
            bind(&GeneralEditor::onShown, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-physical", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::physicalSource, this),
            bind(&GeneralEditor::onPhysical, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-visible", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::visibleSource, this),
            bind(&GeneralEditor::onVisible, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-audible", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::audibleSource, this),
            bind(&GeneralEditor::onAudible, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-field-of-view", angleSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&GeneralEditor::fieldOfViewSource, this),
            bind(&GeneralEditor::onFieldOfView, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-mouse-locked", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GeneralEditor::mouseLockedSource, this),
            bind(&GeneralEditor::onMouseLocked, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-voice-volume", powerSuffix(), [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&GeneralEditor::voiceVolumeSource, this),
            bind(&GeneralEditor::onVoiceVolume, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-tint-color", [this](InterfaceWidget* parent) -> void {
        new Vec3Edit(
            parent,
            bind(&GeneralEditor::tintColorSource, this),
            bind(&GeneralEditor::onTintColor, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "general-editor-tint-strength", percentSuffix(), [this](InterfaceWidget* parent) -> void {
        new PercentEdit(
            parent,
            bind(&GeneralEditor::tintStrengthSource, this),
            bind(&GeneralEditor::onTintStrength, this, placeholders::_1)
        );
    });

    new EditorMultiEntry(listView, 2, [this](InterfaceWidget* parent, size_t index) -> void {
        switch (index)
        {
        case 0 :
            new DynamicTextButton(
                parent,
                bind(&GeneralEditor::playPauseTextSource, this),
                bind(&GeneralEditor::onPlayPause, this)
            );
            break;
        case 1 :
            new DynamicTextButton(
                parent,
                bind(&GeneralEditor::storeResetTextSource, this),
                bind(&GeneralEditor::onStoreReset, this)
            );
            break;
        }
    });

    listView->update();
}

string GeneralEditor::idSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "";
    }

    return to_string(entity->id().value());
}

string GeneralEditor::nameSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "";
    }

    return entity->name();
}

void GeneralEditor::onName(const string& name)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, name]() -> void {
        entity->setName(name);
    });
}

vector<string> GeneralEditor::parentOptionsSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return {};
    }

    vector<string> parentOptions = { localize("general-editor-none") };

    entity->world()->traverse([&](const Entity* entity) -> void {
        // skip self
        if (entity->id() == this->id)
        {
            return;
        }

        parentOptions.push_back(entityDisplayName(entity));
    });

    return parentOptions;
}

size_t GeneralEditor::parentSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent())
    {
        return 0;
    }

    size_t index = 0;

    size_t i = 1;
    entity->world()->partiallyTraverse([&](const Entity* candidate) -> bool {
        // skip self
        if (candidate == entity)
        {
            return false;
        }

        // found
        if (candidate == entity->parent())
        {
            index = i;
            return true;
        }

        i++;
        return false;
    });

    if (index == 0)
    {
        warning("Failed to find entity's parent.");
    }

    return index;
}

void GeneralEditor::onParent(size_t index)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    Entity* previousParent = entity->parent();
    Entity* nextParent = nullptr;

    if (index != 0)
    {
        size_t i = 1;
        entity->world()->partiallyTraverse([&](Entity* candidate) -> bool {
            // skip self
            if (candidate == entity)
            {
                return false;
            }

            // found
            if (i == index)
            {
                nextParent = candidate;
                return true;
            }

            i++;
            return false;
        });

        if (!nextParent)
        {
            warning("Failed to find entity's new parent.");
        }
    }

    if (previousParent == nextParent)
    {
        return;
    }

    context()->controller()->edit([entity, nextParent]() -> void {
        if (nextParent)
        {
            entity->world()->setParent(entity, nextParent);
        }
        else
        {
            entity->world()->clearParent(entity);
        }
    });
}

vector<string> GeneralEditor::parentPartOptionsSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent())
    {
        return {};
    }

    vector<string> partNames = { localize("general-editor-none") };

    entity->parent()->body().traverse([&](const BodyPart* part) -> void {
        partNames.push_back(bodyPartDisplayName(part));
    });

    return partNames;
}

size_t GeneralEditor::parentPartSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent() || !entity->parentPartID())
    {
        return 0;
    }

    size_t index = 0;

    size_t i = 1;
    entity->parent()->body().traverse([&](const BodyPart* part) -> void {
        if (part->id() == entity->parentPartID())
        {
            index = i;
        }

        i++;
    });

    if (index == 0)
    {
        warning("Failed to find entity's parent part.");
    }

    return index;
}

void GeneralEditor::onParentPart(size_t index)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent())
    {
        return;
    }

    BodyPartID nextParentPartID;

    if (index != 0)
    {
        size_t i = 1;
        entity->body().traverse([&](BodyPart* part) -> void {
            if (i == index)
            {
                nextParentPartID = part->id();
            }

            i++;
        });

        if (!nextParentPartID)
        {
            warning("Failed to find entity's new parent part.");
        }
    }

    context()->controller()->edit([index, entity, nextParentPartID]() -> void {
        if (index == 0)
        {
            entity->setParentPartID(BodyPartID());
        }
        else
        {
            entity->setParentPartID(nextParentPartID);
        }
    });
}

vector<string> GeneralEditor::parentSubpartOptionsSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent())
    {
        return {};
    }

    const BodyPart* part = entity->parent()->body().get(entity->parentPartID());

    if (!part)
    {
        return {};
    }

    vector<string> subPartNames = { localize("general-editor-none") };

    size_t subpartCount = part->subparts().size();

    for (size_t i = 0; i < subpartCount; i++)
    {
        subPartNames.push_back(to_string(i + 1));
    }

    return subPartNames;
}

size_t GeneralEditor::parentSubpartSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent() || !entity->parentPartID())
    {
        return 0;
    }

    return entity->parentPartSubID().value();
}

void GeneralEditor::onParentSubpart(size_t index)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity || !entity->parent() || !entity->parentPartID())
    {
        return;
    }

    context()->controller()->edit([index, entity]() -> void {
        entity->setParentPartSubID(BodyPartSubID(index));
    });
}

string GeneralEditor::playPauseTextSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return localize("general-editor-play");
    }

    return entity->active() ? localize("general-editor-pause") : localize("general-editor-play");
}

void GeneralEditor::onPlayPause()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity]() -> void {
        entity->setActive(!entity->active());
    });
}

string GeneralEditor::storeResetTextSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return localize("general-editor-reset");
    }

    return entity->active() ? localize("general-editor-store") : localize("general-editor-reset");
}

void GeneralEditor::onStoreReset()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity]() -> void {
        if (entity->active())
        {
            entity->store();
        }
        else
        {
            entity->reset();
        }
    });
}

size_t GeneralEditor::spaceSource()
{
    return localSpace ? 1 : 0;
}

void GeneralEditor::onSpace(size_t index)
{
    localSpace = index != 0;
}

vec3 GeneralEditor::positionSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return vec3();
    }

    return localizeLength(localSpace ? entity->localPosition() : entity->globalPosition());
}

void GeneralEditor::onPosition(const vec3& position)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([this, entity, position]() -> void {
        if (localSpace)
        {
            entity->setLocalPosition(delocalizeLength(position));
        }
        else
        {
            entity->setGlobalPosition(delocalizeLength(position));
        }
    });
}

quaternion GeneralEditor::rotationSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return quaternion();
    }

    return localSpace ? entity->localRotation() : entity->globalRotation();
}

void GeneralEditor::onRotation(const quaternion& rotation)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([this, entity, rotation]() -> void {
        if (localSpace)
        {
            entity->setLocalRotation(rotation);
        }
        else
        {
            entity->setGlobalRotation(rotation);
        }
    });
}

vec3 GeneralEditor::scaleSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return vec3(1);
    }

    return localSpace ? entity->localScale() : entity->globalScale();
}

void GeneralEditor::onScale(const vec3& scale)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([this, entity, scale]() -> void {
        if (localSpace)
        {
            entity->setLocalScale(scale);
        }
        else
        {
            entity->setGlobalScale(scale);
        }
    });
}

vec3 GeneralEditor::linearVelocitySource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return vec3();
    }

    return localizeSpeed(entity->linearVelocity());
}

void GeneralEditor::onLinearVelocity(const vec3& linV)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, linV]() -> void {
        entity->setLinearVelocity(delocalizeSpeed(linV));
    });
}

vec3 GeneralEditor::angularVelocitySource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return vec3();
    }

    return localizeAngle(quaternion(entity->angularVelocity().normalize(), entity->angularVelocity().length()).euler());
}

void GeneralEditor::onAngularVelocity(const vec3& angV)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, angV]() -> void {
        entity->setAngularVelocity(quaternion(delocalizeAngle(angV)).axis() * quaternion(delocalizeAngle(angV)).angle());
    });
}

vector<string> GeneralEditor::ownerOptionsSource()
{
    vector<string> options;
    options.reserve(context()->controller()->game().users().size() + 1);

    options.push_back(localize("general-editor-none"));

    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        options.push_back(user.name());
    }

    return options;
}

size_t GeneralEditor::ownerSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    size_t i = 1;
    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        if (id == entity->ownerID())
        {
            return i;
        }

        i++;
    }

    return 0;
}

void GeneralEditor::onOwner(size_t index)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    UserID ownerID;

    size_t i = 1;
    for (const auto& [ id, user ] : context()->controller()->game().users())
    {
        if (i == index)
        {
            ownerID = id;
            break;
        }

        i++;
    }

    context()->controller()->edit([entity, ownerID]() -> void {
        entity->setOwnerID(ownerID);
    });
}

bool GeneralEditor::sharedSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return entity->shared();
}

void GeneralEditor::onShared(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setShared(state);
    });
}

string GeneralEditor::attachedUserSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "[" + localize("general-editor-no-user") + "]";
    }

    const User* user = context()->controller()->game().getUser(entity->attachedUserID());

    if (!user)
    {
        return "[" + localize("general-editor-no-user") + "]";
    }

    return user->name();
}

string GeneralEditor::selectingUserSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "[" + localize("general-editor-no-user") + "]";
    }

    const User* user = context()->controller()->game().getUser(entity->selectingUserID());

    if (!user)
    {
        return "[" + localize("general-editor-no-user") + "]";
    }

    return user->name();
}

f64 GeneralEditor::massSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return localizeMass(entity->mass());
}

void GeneralEditor::onMass(f64 mass)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, mass]() -> void {
        entity->setMass(delocalizeMass(mass));
    });
}

string GeneralEditor::volumeSource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "";
    }

    return toPrettyString(localizeVolume(entity->volume()));
}

string GeneralEditor::densitySource()
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "";
    }

    f64 mass = entity->mass();
    f64 volume = entity->volume();

    if (roughly(mass, 0.0) || roughly(volume, 0.0))
    {
        return toPrettyString(0);
    }

    return toPrettyString(localizeDensity(mass / volume));
}

f64 GeneralEditor::dragSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->drag();
}

void GeneralEditor::onDrag(f64 drag)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, drag]() -> void {
        entity->setDrag(drag);
    });
}

f64 GeneralEditor::buoyancySource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->buoyancy();
}

void GeneralEditor::onBuoyancy(f64 buoyancy)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, buoyancy]() -> void {
        entity->setBuoyancy(buoyancy);
    });
}

f64 GeneralEditor::liftSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->lift();
}

void GeneralEditor::onLift(f64 lift)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, lift]() -> void {
        entity->setLift(lift);
    });
}

f64 GeneralEditor::magnetismSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->magnetism();
}

void GeneralEditor::onMagnetism(f64 magnetism)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, magnetism]() -> void {
        entity->setMagnetism(magnetism);
    });
}

f64 GeneralEditor::frictionSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->friction();
}

void GeneralEditor::onFriction(f64 friction)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, friction]() -> void {
        entity->setFriction(friction);
    });
}

f64 GeneralEditor::restitutionSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->restitution();
}

void GeneralEditor::onRestitution(f64 restitution)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, restitution]() -> void {
        entity->setRestitution(restitution);
    });
}

bool GeneralEditor::hostSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return entity->host();
}

void GeneralEditor::onHost(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setHost(state);
    });
}

bool GeneralEditor::shownSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return "";
    }

    return entity->shown();
}

void GeneralEditor::onShown(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setShown(state);
    });
}

bool GeneralEditor::physicalSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return entity->physical();
}

void GeneralEditor::onPhysical(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setPhysical(state);
    });
}

bool GeneralEditor::visibleSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return entity->visible();
}

void GeneralEditor::onVisible(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setVisible(state);
    });
}

bool GeneralEditor::audibleSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return entity->audible();
}

void GeneralEditor::onAudible(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setAudible(state);
    });
}

f64 GeneralEditor::fieldOfViewSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return localizeAngle(entity->fieldOfView());
}

void GeneralEditor::onFieldOfView(f64 fov)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, fov]() -> void {
        entity->setFieldOfView(delocalizeAngle(fov));
    });
}

bool GeneralEditor::mouseLockedSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return false;
    }

    return entity->mouseLocked();
}

void GeneralEditor::onMouseLocked(bool state)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, state]() -> void {
        entity->setMouseLocked(state);
    });
}

f64 GeneralEditor::voiceVolumeSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 1;
    }

    return entity->voiceVolume();
}

void GeneralEditor::onVoiceVolume(f64 volume)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, volume]() -> void {
        entity->setVoiceVolume(volume);
    });
}

vec3 GeneralEditor::tintColorSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return vec3(1);
    }

    return entity->tintColor();
}

void GeneralEditor::onTintColor(const vec3& color)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, color]() -> void {
        entity->setTintColor(color);
    });
}

f64 GeneralEditor::tintStrengthSource()
{
    const Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return 0;
    }

    return entity->tintStrength();
}

void GeneralEditor::onTintStrength(f64 strength)
{
    Entity* entity = context()->controller()->selfWorld()->get(id);

    if (!entity)
    {
        return;
    }

    context()->controller()->edit([entity, strength]() -> void {
        entity->setTintStrength(strength);
    });
}

void GeneralEditor::undo(const Input& input)
{
    context()->viewerMode()->undo();
}

void GeneralEditor::redo(const Input& input)
{
    context()->viewerMode()->redo();
}

SizeProperties GeneralEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
