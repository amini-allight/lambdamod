/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_manager.hpp"
#include "localization.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "confirm_dialog.hpp"
#include "flat_render.hpp"
#include "input_bindings_loader.hpp"
#include "interface_window_view.hpp"
#include "documentation_generation.hpp"
#include "window_manager_input_scope.hpp"

#define SLOT(_binding_name, _function_name) \
parent->bind(_binding_name, bind(&InputManager::_function_name, this, placeholders::_1));
#define REPLAYABLE_SLOT(_binding_name, _function_name) \
parent->replayableBind(_binding_name, bind(&InputManager::_function_name, this, placeholders::_1));
#define INCLUSION_SLOT(_binding_name, _function_name, _inclusion_mode) \
parent->inclusionBind(_binding_name, bind(&InputManager::_function_name, this, placeholders::_1), _inclusion_mode);

static constexpr f64 volumeIncrement = 0.1;

InputManager::InputManager(ControlContext* ctx)
    : ctx(ctx)
{
    applyBindings();
}

InputManager::~InputManager()
{
    destroyScopes();
}

void InputManager::init()
{
    createScopes();
}

void InputManager::handle(const Input& input)
{
    u32 repeats = monitorRepeats(input);

    if (input.isMouse())
    {
        globalScope->movePointer(Pointer_Normal, pointerPoint(input).toVec2(), false);

        dynamic_cast<WindowManagerInputScope*>(getScope("windows"))->sort();
        dynamic_cast<InterfaceWindowView*>(ctx->interface()->getView("windows"))->sort();
    }
    // Not all VR inputs from a hand encode that hand's spatial position (e.g. thumbstick uses the position field for the stick pose instead)
    else if (input.type == Input_VR_Left_Hand)
    {
        globalScope->moveLeftHand(input.position, input.rotation, false);
    }
    else if (input.type == Input_VR_Right_Hand)
    {
        globalScope->moveRightHand(input.position, input.rotation, false);
    }

    InputHandlingResult result = globalScope->handle(input, repeats);

    bool consumed = result.commit();

    if (consumed)
    {
        return;
    }

    if (!ctx->attached())
    {
        return;
    }

    auto it = ctx->attachedMode()->bindings().find(input.type);

    if (it == ctx->attachedMode()->bindings().end())
    {
        return;
    }

    ctx->controller()->sendAttachedInput(it->second.input, input);
}

vector<string> InputManager::allBindings() const
{
    vector<string> names;
    names.reserve(bindings.size());

    for (const auto& [ name, binding ] : bindings)
    {
        names.push_back(name);
    }

    return names;
}

InputScope* InputManager::getScope(const string& name) const
{
    return globalScope->get(name);
}

void InputManager::setBindings(const string& name, const vector<InputScopeBinding>& bindings)
{
    this->bindings.at(name) = bindings;
}

vector<InputScopeBinding> InputManager::getBindings(const string& name) const
{
    return bindings.at(name);
}

void InputManager::applyBindings()
{
    applyDefaultBindings();

    if (g_config.numpadAlternative)
    {
        applyNumpadAlternativeBindings();
    }

    applyCustomBindings();
}

void InputManager::applyDefaultBindings()
{
    using namespace InputScopeBindingTools;

    bindings.insert({ "quit", { control(binding(Input_Q)), alt(binding(Input_F4)) } });
    bindings.insert({ "increase-volume", { binding(Input_VR_Volume_Up) } });
    bindings.insert({ "decrease-volume", { binding(Input_VR_Volume_Down) } });
    bindings.insert({ "toggle-microphone", { binding(Input_VR_Mute_Microphone) } });

#ifdef LMOD_VR
    bindings.insert({ "confirm-tracking-calibration", { binding(Input_VR_Left_A), binding(Input_VR_Right_A) } });
    bindings.insert({ "cancel-tracking-calibration", { binding(Input_VR_Left_B), binding(Input_VR_Right_B) } });
#endif

    bindings.insert({ "dismiss-splash", { binding(Input_Return), binding(Input_Gamepad_B), binding(Input_VR_Left_B), binding(Input_VR_Right_B) } });

    bindings.insert({ "show-knowledge-viewer", { binding(Input_F4) } });
    bindings.insert({ "hide-knowledge-viewer", { binding(Input_Escape), binding(Input_Gamepad_B), binding(Input_VR_Left_B), binding(Input_VR_Right_B) } });

#ifdef LMOD_VR
    bindings.insert({ "start-show-vr-menu-left", { binding(Input_VR_Left_B) } });
    bindings.insert({ "start-show-vr-menu-right", { binding(Input_VR_Right_B) } });
    bindings.insert({ "end-show-vr-menu-left", { up(binding(Input_VR_Left_B)) } });
    bindings.insert({ "end-show-vr-menu-right", { up(binding(Input_VR_Right_B)) } });
    bindings.insert({ "hide-vr-menu", { binding(Input_VR_Left_B), binding(Input_VR_Right_B) } });
#endif

    bindings.insert({ "show-gamepad-menu", { binding(Input_Gamepad_Start) } });
    bindings.insert({ "hide-gamepad-menu", { binding(Input_Gamepad_Start), binding(Input_Gamepad_B) } });

    bindings.insert({ "toggle-console", { binding(Input_Grave_Accent) } });
    bindings.insert({ "toggle-help", { binding(Input_F1) } });
    bindings.insert({ "toggle-document-manager", { binding(Input_F2) } });
    bindings.insert({ "toggle-settings", { binding(Input_F3) } });
    bindings.insert({ "toggle-new-text-tool", { binding(Input_F4) } });
    bindings.insert({ "toggle-user-controls-tool", { binding(Input_F5) } });
    bindings.insert({ "toggle-oracle-manager", { binding(Input_F6) } });
    bindings.insert({ "toggle-system-settings", { binding(Input_F7) } });
    bindings.insert({ "toggle-quick-accessor", { binding(Input_F1) } });
    bindings.insert({ "open-editor", { repeat(binding(Input_Left_Mouse), 2) } });

    bindings.insert({ "set-magnitude-0", { control(binding(Input_Escape)) } });
    bindings.insert({ "set-magnitude-1", { control(binding(Input_F1)) } });
    bindings.insert({ "set-magnitude-2", { control(binding(Input_F2)) } });
    bindings.insert({ "set-magnitude-3", { control(binding(Input_F3)) } });

    bindings.insert({ "attach", { binding(Input_U), binding(Input_VR_Left_Trigger), binding(Input_VR_Right_Trigger), binding(Input_VR_Left_Trigger_Click), binding(Input_VR_Right_Trigger_Click), binding(Input_Gamepad_Left_Trigger), binding(Input_Gamepad_Right_Trigger) } });
    bindings.insert({ "detach", { binding(Input_Escape) } });

    bindings.insert({ "brake", { control(binding(Input_Space)) } });
    bindings.insert({ "unbrake", { control(binding(Input_Space)) } });

    bindings.insert({ "ping", { binding(Input_Tab) } });

    bindings.insert({ "move-interface-pointer", { binding(Input_Mouse_Move) } });

#ifdef LMOD_VR
    bindings.insert({ "move-head", { binding(Input_VR_Head) } });
    bindings.insert({ "move-left-hand", { binding(Input_VR_Left_Hand) } });
    bindings.insert({ "move-right-hand", { binding(Input_VR_Right_Hand) } });
    bindings.insert({ "move-hips", { binding(Input_VR_Hips) } });
    bindings.insert({ "move-left-foot", { binding(Input_VR_Left_Foot) } });
    bindings.insert({ "move-right-foot", { binding(Input_VR_Right_Foot) } });
    bindings.insert({ "move-chest", { binding(Input_VR_Chest) } });
    bindings.insert({ "move-left-shoulder", { binding(Input_VR_Left_Shoulder) } });
    bindings.insert({ "move-right-shoulder", { binding(Input_VR_Right_Shoulder) } });
    bindings.insert({ "move-left-elbow", { binding(Input_VR_Left_Elbow) } });
    bindings.insert({ "move-right-elbow", { binding(Input_VR_Right_Elbow) } });
    bindings.insert({ "move-left-wrist", { binding(Input_VR_Left_Wrist) } });
    bindings.insert({ "move-right-wrist", { binding(Input_VR_Right_Wrist) } });
    bindings.insert({ "move-left-knee", { binding(Input_VR_Left_Knee) } });
    bindings.insert({ "move-right-knee", { binding(Input_VR_Right_Knee) } });
    bindings.insert({ "move-left-ankle", { binding(Input_VR_Left_Ankle) } });
    bindings.insert({ "move-right-ankle", { binding(Input_VR_Right_Ankle) } });
    bindings.insert({ "vr-stick-motion", { binding(Input_VR_Left_Stick), binding(Input_VR_Right_Stick) } });
    bindings.insert({ "vr-touchpad-motion", { binding(Input_VR_Left_Touchpad), binding(Input_VR_Right_Touchpad) } });
#endif

    bindings.insert({ "reset-camera", { control(binding(Input_H)) } });
    bindings.insert({ "reset-cursor", { shift(binding(Input_C)) } });

    bindings.insert({ "viewer-start-forward", { binding(Input_W) } });
    bindings.insert({ "viewer-end-forward", { up(binding(Input_W)) } });
    bindings.insert({ "viewer-start-back", { binding(Input_S) } });
    bindings.insert({ "viewer-end-back", { up(binding(Input_S)) } });
    bindings.insert({ "viewer-start-left", { binding(Input_A) } });
    bindings.insert({ "viewer-end-left", { up(binding(Input_A)) } });
    bindings.insert({ "viewer-start-right", { binding(Input_D) } });
    bindings.insert({ "viewer-end-right", { up(binding(Input_D)) } });
    bindings.insert({ "viewer-start-up", { binding(Input_Space), binding(Input_VR_Left_A), binding(Input_VR_Right_A) } });
    bindings.insert({ "viewer-end-up", { up(binding(Input_Space)), up(binding(Input_VR_Left_A)), up(binding(Input_VR_Right_A)) } });
    bindings.insert({ "viewer-start-down", { binding(Input_Left_Control), binding(Input_VR_Left_B), binding(Input_VR_Right_B) } });
    bindings.insert({ "viewer-end-down", { up(binding(Input_Left_Control)), up(binding(Input_VR_Left_B)), up(binding(Input_VR_Right_B)) } });
    bindings.insert({ "viewer-start-shift", { shift(binding(Input_Middle_Mouse)) } });
    bindings.insert({ "viewer-end-shift", { up(binding(Input_Middle_Mouse)) } });
    bindings.insert({ "viewer-start-zoom", { control(binding(Input_Middle_Mouse)) } });
    bindings.insert({ "viewer-end-zoom", { up(binding(Input_Middle_Mouse)) } });
    bindings.insert({ "viewer-start-rotate", { binding(Input_Right_Mouse) } });
    bindings.insert({ "viewer-end-rotate", { up(binding(Input_Right_Mouse)) } });
    bindings.insert({ "viewer-start-sprint", { binding(Input_Left_Shift) } });
    bindings.insert({ "viewer-end-sprint", { up(binding(Input_Left_Shift)) } });
    bindings.insert({ "mouse-motion", { binding(Input_Mouse_Move) } });
    bindings.insert({ "move-stick-motion", { binding(Input_Gamepad_Left_Stick) } });
    bindings.insert({ "look-stick-motion", { binding(Input_Gamepad_Right_Stick) } });

    bindings.insert({ "ortho-look-up", { control(binding(Input_Num_7)) } });
    bindings.insert({ "ortho-look-down", { binding(Input_Num_7) } });
    bindings.insert({ "ortho-look-left", { binding(Input_Num_3) } });
    bindings.insert({ "ortho-look-right", { control(binding(Input_Num_3)) } });
    bindings.insert({ "ortho-look-forward", { control(binding(Input_Num_1)) } });
    bindings.insert({ "ortho-look-back", { binding(Input_Num_1) } });
    bindings.insert({ "ortho-zoom-in", { binding(Input_Mouse_Wheel_Up) } });
    bindings.insert({ "ortho-zoom-out", { binding(Input_Mouse_Wheel_Down) } });
    bindings.insert({ "ortho-invert", { binding(Input_Num_9) } });
    bindings.insert({ "ortho-rotate-up", { binding(Input_Num_8) } });
    bindings.insert({ "ortho-rotate-down", { binding(Input_Num_2) } });
    bindings.insert({ "ortho-rotate-left", { binding(Input_Num_4) } });
    bindings.insert({ "ortho-rotate-right", { binding(Input_Num_6) } });
    bindings.insert({ "toggle-orthographic", { binding(Input_Num_5) } });
    bindings.insert({ "camera-to-center-of-selection", { control(binding(Input_Num_Period)) } });
    bindings.insert({ "to-free-look", { binding(Input_Num_0) } });

    bindings.insert({ "start-snap", { binding(Input_Left_Control) } });
    bindings.insert({ "end-snap", { up(binding(Input_Left_Control)) } });

    bindings.insert({ "start-select", { binding(Input_Left_Mouse), control(binding(Input_Left_Mouse)), shift(binding(Input_Left_Mouse)) } });
    bindings.insert({ "end-add-select", { up(shift(binding(Input_Left_Mouse))) } });
    bindings.insert({ "end-remove-select", { up(control(binding(Input_Left_Mouse))) } });
    bindings.insert({ "end-replace-select", { up(binding(Input_Left_Mouse)) } });
    bindings.insert({ "move-select", { binding(Input_Mouse_Move) } });

    bindings.insert({ "select-linked", { control(binding(Input_L)) } });
    bindings.insert({ "select-all", { binding(Input_A) } });
    bindings.insert({ "deselect-all", { control(binding(Input_A)) } });
    bindings.insert({ "invert-selection", { control(binding(Input_I)) } });

    bindings.insert({ "toggle-edit-mode", { binding(Input_Tab) } });
    bindings.insert({ "move-cursor", { alt(binding(Input_Left_Mouse)) } });

    bindings.insert({ "toggle-play", { binding(Input_Space) } });

    bindings.insert({ "translate", { binding(Input_G) } });
    bindings.insert({ "rotate", { binding(Input_R) } });
    bindings.insert({ "scale", { binding(Input_S) } });
    bindings.insert({ "store", { control(binding(Input_S)) } });
    bindings.insert({ "reset", { control(binding(Input_R)) } });
    bindings.insert({ "reset-position", { alt(binding(Input_G)) } });
    bindings.insert({ "reset-rotation", { alt(binding(Input_R)) } });
    bindings.insert({ "reset-scale", { alt(binding(Input_S)) } });
    bindings.insert({ "reset-linear-velocity", { alt(binding(Input_L)) } });
    bindings.insert({ "reset-angular-velocity", { alt(binding(Input_A)) } });
    bindings.insert({ "add-keyframes", { binding(Input_I) } });
    bindings.insert({ "remove-keyframes", { alt(binding(Input_I)) } });
    bindings.insert({ "cancel-tool", { binding(Input_Escape), binding(Input_Right_Mouse) } });
    bindings.insert({ "end-tool", { binding(Input_Return), binding(Input_Left_Mouse) } });
    bindings.insert({ "use-tool", { binding(Input_Mouse_Move) } });
    bindings.insert({ "toggle-x", { binding(Input_X) } });
    bindings.insert({ "toggle-y", { binding(Input_Y) } });
    bindings.insert({ "toggle-z", { binding(Input_Z) } });
    bindings.insert({ "toggle-pivot-mode", { binding(Input_Comma) } });

    bindings.insert({ "cursor-to-selection", { shift(binding(Input_S)) } });
    bindings.insert({ "selection-to-cursor", { shift(control(binding(Input_S))) } });
    bindings.insert({ "duplicate", { shift(binding(Input_D)) } });
    bindings.insert({ "extrude", { binding(Input_E) } });
    bindings.insert({ "split", { binding(Input_P) } });
    bindings.insert({ "join", { control(binding(Input_J)) } });
    bindings.insert({ "origin-to-3d-cursor", {} });
    bindings.insert({ "origin-to-body", {} });
    bindings.insert({ "body-to-origin", {} });
    bindings.insert({ "add-children", { control(binding(Input_P)) } });
    bindings.insert({ "remove-parent", { alt(binding(Input_P)) } });
    bindings.insert({ "hide-selected", { binding(Input_H) } });
    bindings.insert({ "hide-unselected", { shift(binding(Input_H)) } });
    bindings.insert({ "unhide-all", { alt(binding(Input_H)) } });
    bindings.insert({ "save", { control(binding(Input_S)) } });
    bindings.insert({ "reload", { control(binding(Input_R)) } });
    bindings.insert({ "redo", { shift(control(binding(Input_Z))) } });
    bindings.insert({ "undo", { control(binding(Input_Z)) } });
    bindings.insert({ "cut", { control(binding(Input_X)), shift(binding(Input_Delete)) } });
    bindings.insert({ "copy", { control(binding(Input_C)), control(binding(Input_Insert)) } });
    bindings.insert({ "paste", { control(binding(Input_V)), shift(binding(Input_Insert)), control(binding(Input_Y)) } });
    bindings.insert({ "paste-primary", { binding(Input_Middle_Mouse) } });
    bindings.insert({ "add-new-prefab", { control(shift(binding(Input_A))) } });
    bindings.insert({ "add-new", { shift(binding(Input_A)) } });
    bindings.insert({ "remove-selected", { binding(Input_X), binding(Input_Delete) } });

    bindings.insert({ "add-numeric-input-0", { binding(Input_0), binding(Input_Num_0) } });
    bindings.insert({ "add-numeric-input-1", { binding(Input_1), binding(Input_Num_1) } });
    bindings.insert({ "add-numeric-input-2", { binding(Input_2), binding(Input_Num_2) } });
    bindings.insert({ "add-numeric-input-3", { binding(Input_3), binding(Input_Num_3) } });
    bindings.insert({ "add-numeric-input-4", { binding(Input_4), binding(Input_Num_4) } });
    bindings.insert({ "add-numeric-input-5", { binding(Input_5), binding(Input_Num_5) } });
    bindings.insert({ "add-numeric-input-6", { binding(Input_6), binding(Input_Num_6) } });
    bindings.insert({ "add-numeric-input-7", { binding(Input_7), binding(Input_Num_7) } });
    bindings.insert({ "add-numeric-input-8", { binding(Input_8), binding(Input_Num_8) } });
    bindings.insert({ "add-numeric-input-9", { binding(Input_9), binding(Input_Num_9) } });
    bindings.insert({ "remove-numeric-input", { binding(Input_Backspace) } });
    bindings.insert({ "toggle-numeric-input-sign", { binding(Input_Minus), binding(Input_Num_Subtract) } });
    bindings.insert({ "ensure-numeric-input-period", { binding(Input_Period), binding(Input_Num_Period) } });

    bindings.insert({ "toggle-hud", { binding(Input_F10) } });
    bindings.insert({ "toggle-fullscreen", { binding(Input_F11) } });

    bindings.insert({ "return-to-menu", { binding(Input_Left_Mouse), binding(Input_Space), binding(Input_Return), binding(Input_Gamepad_Right_Trigger), binding(Input_Gamepad_A), binding(Input_VR_Left_A), binding(Input_VR_Right_A), binding(Input_VR_Left_Trigger), binding(Input_VR_Right_Trigger), binding(Input_VR_Left_Trigger_Click), binding(Input_VR_Right_Trigger_Click) } });

    bindings.insert({ "interact", { binding(Input_Left_Mouse), binding(Input_Gamepad_Right_Trigger), binding(Input_Gamepad_A), binding(Input_VR_Left_A), binding(Input_VR_Right_A), binding(Input_VR_Left_Trigger), binding(Input_VR_Right_Trigger), binding(Input_VR_Left_Trigger_Click), binding(Input_VR_Right_Trigger_Click) } });
    bindings.insert({ "release", { up(binding(Input_Left_Mouse)), up(binding(Input_Gamepad_Right_Trigger)), up(binding(Input_Gamepad_A)), up(binding(Input_VR_Left_A)), up(binding(Input_VR_Right_A)), up(binding(Input_VR_Left_Trigger)), up(binding(Input_VR_Right_Trigger)), up(binding(Input_VR_Left_Trigger_Click)), up(binding(Input_VR_Right_Trigger_Click)) } });
    bindings.insert({ "drag", { binding(Input_Mouse_Move) } });
    bindings.insert({ "move-window", { control(binding(Input_Left_Mouse)) } });
    bindings.insert({ "move-gamepad-pointer", { binding(Input_Gamepad_Right_Stick) } });
    bindings.insert({ "previous-tab", { shift(control(binding(Input_Tab))) } });
    bindings.insert({ "next-tab", { control(binding(Input_Tab)) } });
    bindings.insert({ "close-dialog", { binding(Input_Escape) } });
    bindings.insert({ "scroll-up", { binding(Input_Up), control(binding(Input_P)), binding(Input_Mouse_Wheel_Up) } });
    bindings.insert({ "scroll-down", { binding(Input_Down), control(binding(Input_N)), binding(Input_Mouse_Wheel_Down) } });
    bindings.insert({ "pan-left", { binding(Input_Left), control(binding(Input_B)), binding(Input_Mouse_Wheel_Left) } });
    bindings.insert({ "pan-right", { binding(Input_Right), control(binding(Input_F)), binding(Input_Mouse_Wheel_Right) } });
    bindings.insert({ "go-to-start", { binding(Input_Home), control(binding(Input_A)) } });
    bindings.insert({ "go-to-end", { binding(Input_End), control(binding(Input_E)) } });
    bindings.insert({ "page-up", { binding(Input_Page_Up) } });
    bindings.insert({ "page-down", { binding(Input_Page_Down) } });
    bindings.insert({ "select-to", { shift(binding(Input_Left_Mouse)) } });
    bindings.insert({ "cursor-to", { binding(Input_Left_Mouse) } });
    bindings.insert({ "cursor-up", { binding(Input_Up), control(binding(Input_P)) } });
    bindings.insert({ "cursor-down", { binding(Input_Down), control(binding(Input_N)) } });
    bindings.insert({ "cursor-left", { binding(Input_Left), control(binding(Input_B)) } });
    bindings.insert({ "cursor-right", { binding(Input_Right), control(binding(Input_F)) } });
    bindings.insert({ "select-up", { shift(binding(Input_Up)) } });
    bindings.insert({ "select-down", { shift(binding(Input_Down)) } });
    bindings.insert({ "select-left", { shift(binding(Input_Left)) } });
    bindings.insert({ "select-right", { shift(binding(Input_Right)) } });
    bindings.insert({ "remove-previous-word", { control(binding(Input_Backspace)), control(binding(Input_W)), alt(binding(Input_Backspace)) } });
    bindings.insert({ "remove-next-word", { control(binding(Input_Delete)), alt(binding(Input_D)) } });
    bindings.insert({ "remove-previous-character", { binding(Input_Backspace), control(binding(Input_H)) } });
    bindings.insert({ "remove-next-character", { binding(Input_Delete), control(binding(Input_D)) } });
    bindings.insert({ "remove-to-start", { control(binding(Input_U)) } });
    bindings.insert({ "remove-to-end", { control(binding(Input_K)) } });
    bindings.insert({ "upcase-next-word", { alt(binding(Input_U)) } });
    bindings.insert({ "downcase-next-word", { alt(binding(Input_L)) } });
    bindings.insert({ "swap-words", { alt(binding(Input_T)) } });
    bindings.insert({ "swap-characters", { control(binding(Input_T)) } });
    bindings.insert({ "add-text", { anyText() } });
    bindings.insert({ "previous-history", { binding(Input_Up), alt(shift(binding(Input_Comma))), control(binding(Input_P)) } });
    bindings.insert({ "next-history", { binding(Input_Down), alt(shift(binding(Input_Period))), control(binding(Input_N)) } });
    bindings.insert({ "toggle-console-mode", { control(binding(Input_Tab)) } });
    bindings.insert({ "text-select-all", { shift(control(binding(Input_A))) } });
    bindings.insert({ "cursor-to-previous-word", { control(binding(Input_Left)), alt(binding(Input_B)) } });
    bindings.insert({ "cursor-to-next-word", { control(binding(Input_Right)), alt(binding(Input_F)) } });
    bindings.insert({ "select-to-previous-word", { shift(control(binding(Input_Left))) } });
    bindings.insert({ "select-to-next-word", { shift(control(binding(Input_Right))) } });
    bindings.insert({ "open-context-menu", { binding(Input_Right_Mouse) } });
    bindings.insert({ "close-context-menu", { binding(Input_Right_Mouse) } });
    bindings.insert({ "close-editor", { control(binding(Input_Escape)) } });
    bindings.insert({ "force-close-editor", { control(shift(binding(Input_Escape))) } });
    bindings.insert({ "document-go-to-start", { control(binding(Input_Home)) } });
    bindings.insert({ "document-go-to-end", { control(binding(Input_End)) } });
    bindings.insert({ "document-scroll-up", { binding(Input_Mouse_Wheel_Up) } });
    bindings.insert({ "document-scroll-down", { binding(Input_Mouse_Wheel_Down) } });
    bindings.insert({ "document-pan-left", { binding(Input_Mouse_Wheel_Left) } });
    bindings.insert({ "document-pan-right", { binding(Input_Mouse_Wheel_Right) } });
    bindings.insert({ "toggle-side-panel", { binding(Input_Tab) } });
    bindings.insert({ "start-pan", { binding(Input_Right_Mouse) } });
    bindings.insert({ "end-pan", { up(binding(Input_Right_Mouse)) } });
    bindings.insert({ "pan", { binding(Input_Mouse_Move) } });
    bindings.insert({ "start-special-edit", { binding(Input_Right_Mouse) } });
    bindings.insert({ "start-alt-special-edit", { control(binding(Input_Right_Mouse)) } });
    bindings.insert({ "end-special-edit", { up(binding(Input_Right_Mouse)) } });
    bindings.insert({ "move-special-edit", { binding(Input_Mouse_Move) } });
    bindings.insert({ "remove-link", { control(shift(binding(Input_Left_Mouse))) } });

    bindings.insert({ "touch", { binding(Input_Touch) } });
    bindings.insert({ "double-touch", { repeat(binding(Input_Touch), 2) } });

    if constexpr (false)
    {
        documentConfigKeys("config-keys.md", bindings);
        documentBindings("flat-bindings.md", bindings, [](const InputScopeBinding& binding) -> bool {
            return !isVR(binding.type) && !isGamepad(binding.type);
        });
        documentBindings("vr-bindings.md", bindings, [](const InputScopeBinding& binding) -> bool {
            return isVR(binding.type);
        });
        documentBindings("gamepad-bindings.md", bindings, [](const InputScopeBinding& binding) -> bool {
            return isGamepad(binding.type);
        });
    }
}

void InputManager::applyNumpadAlternativeBindings()
{
    using namespace InputScopeBindingTools;

    bindings.insert({ "ortho-look-forward", { binding(Input_1) } });
    bindings.insert({ "ortho-look-back", { control(binding(Input_1)) } });
    bindings.insert({ "ortho-rotate-down", { binding(Input_2) } });
    bindings.insert({ "ortho-look-left", { binding(Input_3) } });
    bindings.insert({ "ortho-look-right", { control(binding(Input_3)) } });

    bindings.insert({ "ortho-rotate-left", { binding(Input_4) } });
    bindings.insert({ "toggle-orthographic", { binding(Input_5) } });
    bindings.insert({ "ortho-rotate-right", { binding(Input_6) } });

    bindings.insert({ "ortho-look-down", { binding(Input_7) } });
    bindings.insert({ "ortho-look-up", { control(binding(Input_7)) } });
    bindings.insert({ "ortho-rotate-up", { binding(Input_8) } });
    bindings.insert({ "ortho-invert", { binding(Input_9) } });

    bindings.insert({ "to-free-look", { binding(Input_0) } });
}

void InputManager::applyCustomBindings()
{
    try
    {
        for (const auto& [ name, bindings ] : loadInputBindings())
        {
            this->bindings.insert_or_assign(name, bindings);
        }
    }
    catch (const exception& e)
    {
        error("Failed to load input bindings: " + string(e.what()));
    }
}

void InputManager::createScopes()
{
    START_GLOBAL_SCOPE
        SLOT("quit", quit)
        SLOT("increase-volume", increaseVolume)
        SLOT("decrease-volume", decreaseVolume)
        SLOT("toggle-microphone", toggleMicrophone)
        SLOT("toggle-fullscreen", toggleFullscreen)
        SLOT("move-interface-pointer", moveInterfacePointer)
#ifdef LMOD_VR
        SLOT("move-head", moveHead)
        SLOT("move-left-hand", moveLeftHand)
        SLOT("move-right-hand", moveRightHand)
        SLOT("move-hips", moveHips)
        SLOT("move-left-foot", moveLeftFoot)
        SLOT("move-right-foot", moveRightFoot)
        SLOT("move-chest", moveChest)
        SLOT("move-left-shoulder", moveLeftShoulder)
        SLOT("move-right-shoulder", moveRightShoulder)
        SLOT("move-left-elbow", moveLeftElbow)
        SLOT("move-right-elbow", moveRightElbow)
        SLOT("move-left-wrist", moveLeftWrist)
        SLOT("move-right-wrist", moveRightWrist)
        SLOT("move-left-knee", moveLeftKnee)
        SLOT("move-right-knee", moveRightKnee)
        SLOT("move-left-ankle", moveLeftAnkle)
        SLOT("move-right-ankle", moveRightAnkle)
#endif

        START_SCOPE("game", ctx->controller()->connectionState() == Connection_State_Connected)
            SLOT("toggle-hud", toggleHUD)
            SLOT("toggle-console", toggleConsole)
#ifdef LMOD_VR
            INCLUSION_SLOT("start-show-vr-menu-left", startShowVRMenuLeft, Input_Scope_Slot_Inclusion_With_Below)
            INCLUSION_SLOT("start-show-vr-menu-right", startShowVRMenuRight, Input_Scope_Slot_Inclusion_With_Below)
            INCLUSION_SLOT("end-show-vr-menu-left", endShowVRMenuLeft, Input_Scope_Slot_Inclusion_With_Below)
            INCLUSION_SLOT("end-show-vr-menu-right", endShowVRMenuRight, Input_Scope_Slot_Inclusion_With_Below)
#endif

            START_SCOPE("attached-mode", ctx->attached())
                SLOT("set-magnitude-0", setMagnitude0)
                SLOT("set-magnitude-1", setMagnitude1)
                SLOT("set-magnitude-2", setMagnitude2)
                SLOT("set-magnitude-3", setMagnitude3)
                SLOT("detach", detach)
                SLOT("brake", brake)
                SLOT("ping", ping)
                SLOT("toggle-help", toggleHelp)
                SLOT("show-knowledge-viewer", showKnowledgeViewer)

#ifdef LMOD_VR
                START_INDEPENDENT_SCOPE("calibrating-tracking", ctx->attachedMode()->calibratingTracking())
                    SLOT("confirm-tracking-calibration", confirmTrackingCalibration)
                    SLOT("cancel-tracking-calibration", cancelTrackingCalibration)
                END_SCOPE
#endif
            END_SCOPE

            START_SCOPE("unattached-mode", !ctx->attached())
                SLOT("unbrake", unbrake)
                SLOT("ortho-look-up", orthoLookUp)
                SLOT("ortho-look-down", orthoLookDown)
                SLOT("ortho-look-left", orthoLookLeft)
                SLOT("ortho-look-right", orthoLookRight)
                SLOT("ortho-look-forward", orthoLookForward)
                SLOT("ortho-look-back", orthoLookBack)
                SLOT("ortho-zoom-in", orthoZoomIn)
                SLOT("ortho-zoom-out", orthoZoomOut)
                SLOT("ortho-invert", orthoInvert)
                SLOT("ortho-rotate-up", orthoRotateUp)
                SLOT("ortho-rotate-down", orthoRotateDown)
                SLOT("ortho-rotate-left", orthoRotateLeft)
                SLOT("ortho-rotate-right", orthoRotateRight)
                SLOT("toggle-orthographic", toggleOrthographic)
                SLOT("camera-to-center-of-selection", cameraToCenterOfSelection)
                SLOT("to-free-look", toFreeLook)
                SLOT("toggle-edit-mode", toggleEditMode)
                SLOT("reset-camera", resetCamera)
                SLOT("toggle-document-manager", toggleDocumentManager)
                SLOT("toggle-settings", toggleSettings)
                SLOT("toggle-system-settings", toggleSystemSettings)
                SLOT("toggle-new-text-tool", toggleNewTextTool)
                SLOT("toggle-user-controls-tool", toggleUserControlsTool)
                SLOT("toggle-oracle-manager", toggleOracleManager)
                SLOT("toggle-quick-accessor", toggleQuickAccessor)
#ifdef LMOD_VR
                SLOT("vr-stick-motion", vrStickMotion)
                SLOT("vr-touchpad-motion", vrTouchpadMotion)
#endif

                START_SCOPE("edit-mode", ctx->viewerMode()->editing())
                    SLOT("save", save)
                    SLOT("undo", undo)
                    SLOT("redo", redo)
                    SLOT("cut", cut)
                    SLOT("copy", copy)
                    SLOT("paste", paste)
                    SLOT("duplicate", duplicate)
                    SLOT("add-children", addChildren)
                    SLOT("remove-parent", removeParent)
                    SLOT("hide-selected", hideSelected)
                    SLOT("hide-unselected", hideUnselected)
                    SLOT("unhide-all", unhideAll)
                    SLOT("selection-to-cursor", selectionToCursor)
                    SLOT("cursor-to-selection", cursorToSelection)
                    SLOT("reset-position", resetPosition)
                    SLOT("reset-rotation", resetRotation)
                    SLOT("reset-scale", resetScale)
                    SLOT("reset-linear-velocity", resetLinearVelocity)
                    SLOT("reset-angular-velocity", resetAngularVelocity)
                    SLOT("translate", translate)
                    SLOT("rotate", rotate)
                    SLOT("scale", scale)
                    SLOT("extrude", extrude)
                    SLOT("split", split)
                    SLOT("join", join)
                    SLOT("reset-cursor", resetCursor)
                    SLOT("select-all", selectAll)
                    SLOT("deselect-all", deselectAll)
                    SLOT("invert-selection", invertSelection)
                    SLOT("move-cursor", moveCursor)
                    SLOT("add-new-prefab", addNewPrefab)
                    SLOT("add-new", addNew)
                    SLOT("remove-selected", removeSelected)
                    SLOT("start-select", startSelect)
                    SLOT("start-special-edit", startSpecialEdit)
                    SLOT("start-alt-special-edit", startAltSpecialEdit)

                    START_SCOPE("perspective", ctx->flatViewer() && ctx->flatViewer()->mode() == Flat_Viewer_Perspective)
                        SLOT("viewer-start-rotate", viewerStartRotate)

                        START_INDEPENDENT_SCOPE("rotating", ctx->flatViewer() && ctx->flatViewer()->rotate())
                            SLOT("mouse-motion", mouseMotion)
                            SLOT("viewer-end-rotate", viewerEndRotate)
                        END_SCOPE
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("selecting", ctx->viewerMode()->editMode()->selecting())
                        SLOT("move-select", moveSelect)
                        SLOT("end-add-select", endAddSelect)
                        SLOT("end-remove-select", endRemoveSelect)
                        SLOT("end-replace-select", endReplaceSelect)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("editing", ctx->viewerMode()->editMode()->specialEditing())
                        SLOT("move-special-edit", moveSpecialEdit)
                        SLOT("end-special-edit", endSpecialEdit)
                    END_SCOPE

                    START_BLOCKING_SCOPE("tool-mode", ctx->viewerMode()->editMode()->toolActive())
                        SLOT("start-snap", startSnap)
                        SLOT("toggle-x", toggleX)
                        SLOT("toggle-y", toggleY)
                        SLOT("toggle-z", toggleZ)
                        SLOT("toggle-pivot-mode", togglePivotMode)
                        SLOT("add-numeric-input-0", addNumericInput0)
                        SLOT("add-numeric-input-1", addNumericInput1)
                        SLOT("add-numeric-input-2", addNumericInput2)
                        SLOT("add-numeric-input-3", addNumericInput3)
                        SLOT("add-numeric-input-4", addNumericInput4)
                        SLOT("add-numeric-input-5", addNumericInput5)
                        SLOT("add-numeric-input-6", addNumericInput6)
                        SLOT("add-numeric-input-7", addNumericInput7)
                        SLOT("add-numeric-input-8", addNumericInput8)
                        SLOT("add-numeric-input-9", addNumericInput9)
                        SLOT("remove-numeric-input", removeNumericInput)
                        SLOT("toggle-numeric-input-sign", toggleNumericInputSign)
                        SLOT("ensure-numeric-input-period", ensureNumericInputPeriod)
                        REPLAYABLE_SLOT("use-tool", useTool)
                        SLOT("cancel-tool", cancelTool)
                        SLOT("end-tool", endTool)

                        START_SCOPE("soft", ctx->viewerMode()->editMode()->tool() && ctx->viewerMode()->editMode()->tool()->soft())
                            SLOT("release", endTool)
                        END_SCOPE

                        START_INDEPENDENT_SCOPE("snapping", ctx->viewerMode()->editMode()->snap())
                            SLOT("end-snap", endSnap)
                        END_SCOPE
                    END_SCOPE

                    START_SCOPE("entity-mode", ctx->viewerMode()->mode() == Viewer_Sub_Mode_Entity)
                        SLOT("open-editor", openEditor)
                        SLOT("store", store)
                        SLOT("reset", reset)
                        SLOT("origin-to-3d-cursor", originTo3DCursor)
                        SLOT("origin-to-body", originToBody)
                        SLOT("body-to-origin", bodyToOrigin)
                    END_SCOPE

                    START_SCOPE("body-mode", ctx->viewerMode()->mode() == Viewer_Sub_Mode_Body)
                        SLOT("select-linked", selectLinked)
                    END_SCOPE

                    START_SCOPE("action-mode", ctx->viewerMode()->mode() == Viewer_Sub_Mode_Action)
                        SLOT("add-keyframes", addKeyframes)
                        SLOT("remove-keyframes", removeKeyframes)
                    END_SCOPE
                END_SCOPE

                START_SCOPE("viewer-mode", !ctx->viewerMode()->editing())
                    SLOT("viewer-start-forward", viewerStartForward)
                    SLOT("viewer-start-back", viewerStartBack)
                    SLOT("viewer-start-left", viewerStartLeft)
                    SLOT("viewer-start-right", viewerStartRight)
                    SLOT("viewer-start-up", viewerStartUp)
                    SLOT("viewer-start-down", viewerStartDown)
                    SLOT("viewer-start-shift", viewerStartShift)
                    SLOT("viewer-start-zoom", viewerStartZoom)
                    SLOT("viewer-start-rotate", viewerStartRotate)
                    SLOT("viewer-start-sprint", viewerStartSprint)
                    SLOT("move-stick-motion", moveStickMotion)
                    SLOT("look-stick-motion", lookStickMotion)
                    SLOT("attach", attach)

                    START_INDEPENDENT_SCOPE("moving-forward", ctx->flatViewer() && ctx->flatViewer()->forward())
                        SLOT("viewer-end-forward", viewerEndForward)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("moving-back", ctx->flatViewer() && ctx->flatViewer()->back())
                        SLOT("viewer-end-back", viewerEndBack)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("moving-left", ctx->flatViewer() && ctx->flatViewer()->left())
                        SLOT("viewer-end-left", viewerEndLeft)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("moving-right", ctx->flatViewer() && ctx->flatViewer()->right())
                        SLOT("viewer-end-right", viewerEndRight)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("moving-up", ctx->flatViewer() && ctx->flatViewer()->up())
                        SLOT("viewer-end-up", viewerEndUp)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("moving-down", ctx->flatViewer() && ctx->flatViewer()->down())
                        SLOT("viewer-end-down", viewerEndDown)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("shifting", ctx->flatViewer() && ctx->flatViewer()->shift())
                        SLOT("mouse-motion", mouseMotion)
                        SLOT("viewer-end-shift", viewerEndShift)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("zooming", ctx->flatViewer() && ctx->flatViewer()->zoom())
                        SLOT("mouse-motion", mouseMotion)
                        SLOT("viewer-end-zoom", viewerEndZoom)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("rotating", ctx->flatViewer() && ctx->flatViewer()->rotate())
                        SLOT("mouse-motion", mouseMotion)
                        SLOT("viewer-end-rotate", viewerEndRotate)
                    END_SCOPE

                    START_INDEPENDENT_SCOPE("sprinting", ctx->flatViewer() && ctx->flatViewer()->sprint())
                        SLOT("viewer-end-sprint", viewerEndSprint)
                    END_SCOPE
                END_SCOPE
            END_SCOPE

            START_SCOPE("text", true)

            END_SCOPE

            START_SCOPE("under-screen", true)

            END_SCOPE

            START_WINDOW_MANAGER_SCOPE("windows", true)

            END_SCOPE

            START_SCOPE("over-screen", true)

            END_SCOPE
        END_SCOPE

        START_SCOPE("disconnected", ctx->controller()->connectionState() == Connection_State_Disconnected)

        END_SCOPE

        START_SCOPE("menu", ctx->controller()->connectionState() == Connection_State_Standby)

        END_SCOPE
    END_SCOPE
}

void InputManager::destroyScopes()
{
    delete globalScope;
}

u32 InputManager::monitorRepeats(const Input& input)
{
    u32 count = 0;

    for (size_t i = 0; i < repeats.size();)
    {
        InputRepeat& repeat = repeats[i];

        if (repeat.expired())
        {
            repeats.erase(repeats.begin() + i);
        }
        else if (repeat.match(input))
        {
            repeat.increment();
            count = repeat.count();
            i++;
        }
        else
        {
            i++;
        }
    }

    if (count == 0)
    {
        repeats.push_back(InputRepeat(input));
        count = 1;
    }

    return count;
}

Point InputManager::pointerPoint(const Input& input) const
{
    return Point(
        input.position.x * dynamic_cast<FlatRender*>(ctx->controller()->render())->width(),
        input.position.y * dynamic_cast<FlatRender*>(ctx->controller()->render())->height()
    );
}

void InputManager::quit(const Input& input) const
{
    if (ctx->controller()->connected())
    {
        ctx->interface()->addWindow<ConfirmDialog>(
            localize("game-settings-are-you-sure-you-want-to-quit"),
            [=, this](bool confirmed) -> void
            {
                if (!confirmed)
                {
                    return;
                }

                ctx->controller()->quit();
            }
        );
    }
    else
    {
        ctx->controller()->quit();
    }
}

void InputManager::increaseVolume(const Input& input) const
{
    ctx->controller()->soundDevice()->setOutputVolume(min(ctx->controller()->soundDevice()->outputVolume() + volumeIncrement, 1.0));
}

void InputManager::decreaseVolume(const Input& input) const
{
    ctx->controller()->soundDevice()->setOutputVolume(min(ctx->controller()->soundDevice()->outputVolume() - volumeIncrement, 0.0));
}

void InputManager::toggleMicrophone(const Input& input) const
{
    ctx->controller()->soundDevice()->setInputMuted(!ctx->controller()->soundDevice()->inputMuted());
}

#ifdef LMOD_VR
void InputManager::confirmTrackingCalibration(const Input& input) const
{
    ctx->attachedMode()->setTrackingCalibration();
}

void InputManager::cancelTrackingCalibration(const Input& input) const
{
    ctx->attachedMode()->clearTrackingCalibration();
}
#endif

void InputManager::showKnowledgeViewer(const Input& input) const
{
    ctx->attachedMode()->toggleKnowledgeViewer();
}

#ifdef LMOD_VR
void InputManager::startShowVRMenuLeft(const Input& input) const
{
    ctx->vrMenuButtonHandler()->holdVRMenuButton(false, true);
}

void InputManager::startShowVRMenuRight(const Input& input) const
{
    ctx->vrMenuButtonHandler()->holdVRMenuButton(true, true);
}

void InputManager::endShowVRMenuLeft(const Input& input) const
{
    ctx->vrMenuButtonHandler()->holdVRMenuButton(false, false);
}

void InputManager::endShowVRMenuRight(const Input& input) const
{
    ctx->vrMenuButtonHandler()->holdVRMenuButton(true, false);
}
#endif

void InputManager::showGamepadMenu(const Input& input) const
{
    if (ctx->attached())
    {
        ctx->attachedMode()->toggleGamepadMenu();
    }
    else
    {
        ctx->viewerMode()->toggleGamepadMenu();
    }
}

void InputManager::toggleConsole(const Input& input) const
{
    ctx->interface()->console()->toggle(pointerPoint(input));
}

void InputManager::toggleHelp(const Input& input) const
{
    ctx->interface()->help()->toggle(pointerPoint(input));
}

void InputManager::toggleDocumentManager(const Input& input) const
{
    ctx->interface()->documentManager()->toggle(pointerPoint(input));
}

void InputManager::toggleSettings(const Input& input) const
{
    ctx->interface()->settings()->toggle(pointerPoint(input));
}

void InputManager::toggleSystemSettings(const Input& input) const
{
    ctx->interface()->systemSettings()->toggle(pointerPoint(input));
}

void InputManager::toggleNewTextTool(const Input& input) const
{
    ctx->interface()->newTextTool()->toggle(pointerPoint(input));
}

void InputManager::toggleUserControlsTool(const Input& input) const
{
    ctx->interface()->userControlsTool()->toggle(pointerPoint(input));
}

void InputManager::toggleOracleManager(const Input& input) const
{
    ctx->interface()->oracleManager()->toggle(pointerPoint(input));
}

void InputManager::toggleQuickAccessor(const Input& input) const
{
    ctx->interface()->quickAccessor()->toggle(pointerPoint(input));
}

void InputManager::openEditor(const Input& input) const
{
    vector<Entity*> targets = ctx->viewerMode()->entityMode()->entitiesUnderPoint(pointerPoint(input));

    for (const Entity* target : targets)
    {
        if (target->selectingUserID() != ctx->controller()->selfID())
        {
            continue;
        }

        ctx->viewerMode()->entityMode()->openEditor(target->id());
        return;
    }
}

void InputManager::setMagnitude0(const Input& input) const
{
    ctx->setMagnitude(0);
}

void InputManager::setMagnitude1(const Input& input) const
{
    ctx->setMagnitude(1);
}

void InputManager::setMagnitude2(const Input& input) const
{
    ctx->setMagnitude(2);
}

void InputManager::setMagnitude3(const Input& input) const
{
    ctx->setMagnitude(3);
}

void InputManager::attach(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->attach(input.isRight());
    }
    else if (input.isGamepad())
#else
    if (input.isGamepad())
#endif
    {
        Point center(
            0.5 * dynamic_cast<FlatRender*>(ctx->controller()->render())->width(),
            0.5 * dynamic_cast<FlatRender*>(ctx->controller()->render())->height()
        );

        vector<Entity*> targets = ctx->viewerMode()->entityMode()->entitiesUnderPoint(center);

        Entity* best = targets.front();

        for (Entity* target : targets)
        {
            if (ctx->viewerMode()->entityMode()->selection().contains(target))
            {
                best = target;
                break;
            }
        }

        ctx->viewerMode()->attach(best->id());
    }
    else
    {
        vector<Entity*> targets = ctx->viewerMode()->entityMode()->entitiesUnderPoint(pointerPoint(input));

        Entity* best = targets.front();

        for (Entity* target : targets)
        {
            if (ctx->viewerMode()->entityMode()->selection().contains(target))
            {
                best = target;
                break;
            }
        }

        ctx->viewerMode()->attach(best->id());
    }
}

void InputManager::detach(const Input& input) const
{
    ctx->attachedMode()->detach();
}

void InputManager::brake(const Input& input) const
{
    ctx->attachedMode()->brake();
}

void InputManager::unbrake(const Input& input) const
{
    ctx->unbrake();
}

void InputManager::ping(const Input& input) const
{
    ctx->attachedMode()->ping();
}

void InputManager::moveInterfacePointer(const Input& input) const
{
    ctx->interface()->movePointer(pointerPoint(input));
}

#ifdef LMOD_VR
void InputManager::moveHead(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Head, input.position, input.rotation);
}

void InputManager::moveLeftHand(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Hand, input.position, input.rotation);
}

void InputManager::moveRightHand(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Hand, input.position, input.rotation);
}

void InputManager::moveHips(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Hips, input.position, input.rotation);
}

void InputManager::moveLeftFoot(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Foot, input.position, input.rotation);
}

void InputManager::moveRightFoot(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Foot, input.position, input.rotation);
}

void InputManager::moveChest(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Chest, input.position, input.rotation);
}

void InputManager::moveLeftShoulder(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Shoulder, input.position, input.rotation);
}

void InputManager::moveRightShoulder(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Shoulder, input.position, input.rotation);
}

void InputManager::moveLeftElbow(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Elbow, input.position, input.rotation);
}

void InputManager::moveRightElbow(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Elbow, input.position, input.rotation);
}

void InputManager::moveLeftWrist(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Wrist, input.position, input.rotation);
}

void InputManager::moveRightWrist(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Wrist, input.position, input.rotation);
}

void InputManager::moveLeftKnee(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Knee, input.position, input.rotation);
}

void InputManager::moveRightKnee(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Knee, input.position, input.rotation);
}

void InputManager::moveLeftAnkle(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Left_Ankle, input.position, input.rotation);
}

void InputManager::moveRightAnkle(const Input& input) const
{
    ctx->vrViewer()->moveDevice(VR_Device_Right_Ankle, input.position, input.rotation);
}

void InputManager::vrStickMotion(const Input& input) const
{
    ctx->vrViewer()->stickMotion(
        input.isRight(),
        input.position
    );
}

void InputManager::vrTouchpadMotion(const Input& input) const
{
    ctx->vrViewer()->touchpadMotion(
        input.isRight(),
        input.type == Input_VR_Left_Touchpad_Press || input.type == Input_VR_Right_Touchpad_Press
            ? input.intensity
            : !input.up,
        input.position
    );
}
#endif

void InputManager::resetCamera(const Input& input) const
{
#ifdef LMOD_VR
    if (!g_vr)
    {
        ctx->flatViewer()->move(ctx->viewerMode()->cursorPosition(), quaternion());
    }
    else
    {
        ctx->vrViewer()->moveRoom(ctx->viewerMode()->cursorPosition(), quaternion());
    }
#else
    ctx->flatViewer()->move(ctx->viewerMode()->cursorPosition(), quaternion());
#endif
}

void InputManager::resetCursor(const Input& input) const
{
    ctx->viewerMode()->setCursorPosition(vec3());
}

void InputManager::viewerStartForward(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->forward(true);
}

void InputManager::viewerEndForward(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->forward(false);
}

void InputManager::viewerStartBack(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->back(true);
}

void InputManager::viewerEndBack(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->back(false);
}

void InputManager::viewerStartLeft(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->left(true);
}

void InputManager::viewerEndLeft(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->left(false);
}

void InputManager::viewerStartRight(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->right(true);
}

void InputManager::viewerEndRight(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->right(false);
}

void InputManager::viewerStartUp(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->ascend(true);
    }
    else
    {
        ctx->flatViewer()->up(true);
    }
#else
    ctx->flatViewer()->up(true);
#endif
}

void InputManager::viewerEndUp(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->ascend(false);
    }
    else
    {
        ctx->flatViewer()->up(false);
    }
#else
    ctx->flatViewer()->up(false);
#endif
}

void InputManager::viewerStartDown(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->descend(true);
    }
    else
    {
        ctx->flatViewer()->down(true);
    }
#else
    ctx->flatViewer()->down(true);
#endif
}

void InputManager::viewerEndDown(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->descend(false);
    }
    else
    {
        ctx->flatViewer()->down(false);
    }
#else
    ctx->flatViewer()->down(false);
#endif
}

void InputManager::viewerStartShift(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->shift(true);
}

void InputManager::viewerEndShift(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->shift(false);
}

void InputManager::viewerStartZoom(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->zoom(true);
}

void InputManager::viewerEndZoom(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->zoom(false);
}

void InputManager::viewerStartRotate(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->rotate(true);
}

void InputManager::viewerEndRotate(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->rotate(false);
}

void InputManager::viewerStartSprint(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->sprint(true);
    }
    else
    {
        ctx->flatViewer()->sprint(true);
    }
#else
    ctx->flatViewer()->sprint(true);
#endif
}

void InputManager::viewerEndSprint(const Input& input) const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        ctx->vrViewer()->sprint(false);
    }
    else
    {
        ctx->flatViewer()->sprint(false);
    }
#else
    ctx->flatViewer()->sprint(false);
#endif
}

void InputManager::mouseMotion(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->motion(input.linearMotion);
}

void InputManager::moveStickMotion(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->moveStickMotion(input.position);
}

void InputManager::lookStickMotion(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->lookStickMotion(input.position);
}

void InputManager::orthoLookUp(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoLook(
        ctx->viewerMode()->editMode()->selectionCenter(),
        upDir
    );
}

void InputManager::orthoLookDown(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoLook(
        ctx->viewerMode()->editMode()->selectionCenter(),
        downDir
    );
}

void InputManager::orthoLookLeft(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoLook(
        ctx->viewerMode()->editMode()->selectionCenter(),
        -rightDir
    );
}

void InputManager::orthoLookRight(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoLook(
        ctx->viewerMode()->editMode()->selectionCenter(),
        rightDir
    );
}

void InputManager::orthoLookForward(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoLook(
        ctx->viewerMode()->editMode()->selectionCenter(),
        forwardDir
    );
}

void InputManager::orthoLookBack(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoLook(
        ctx->viewerMode()->editMode()->selectionCenter(),
        backDir
    );
}

void InputManager::orthoZoomIn(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->zoomIn();
}

void InputManager::orthoZoomOut(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->zoomOut();
}

void InputManager::orthoInvert(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->invert();
}

void InputManager::orthoRotateUp(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->rotateUp();
}

void InputManager::orthoRotateDown(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->rotateDown();
}

void InputManager::orthoRotateLeft(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->rotateLeft();
}

void InputManager::orthoRotateRight(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->rotateRight();
}

void InputManager::toggleOrthographic(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->orthoToggle();
}

void InputManager::cameraToCenterOfSelection(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->perspLook(ctx->viewerMode()->editMode()->selectionCenter());
}

void InputManager::toFreeLook(const Input& input) const
{
    if (!ctx->flatViewer())
    {
        return;
    }

    ctx->flatViewer()->freeLook();
}

void InputManager::startSnap(const Input& input) const
{
    ctx->viewerMode()->editMode()->startSnap();
}

void InputManager::endSnap(const Input& input) const
{
    ctx->viewerMode()->editMode()->endSnap();
}

void InputManager::startSelect(const Input& input) const
{
    ctx->viewerMode()->editMode()->startSelect(pointerPoint(input));
}

void InputManager::endAddSelect(const Input& input) const
{
    ctx->viewerMode()->editMode()->endSelect(pointerPoint(input), Select_Add);
}

void InputManager::endRemoveSelect(const Input& input) const
{
    ctx->viewerMode()->editMode()->endSelect(pointerPoint(input), Select_Remove);
}

void InputManager::endReplaceSelect(const Input& input) const
{
    ctx->viewerMode()->editMode()->endSelect(pointerPoint(input), Select_Replace);
}

void InputManager::moveSelect(const Input& input) const
{
    ctx->viewerMode()->editMode()->moveSelect(pointerPoint(input));
}

void InputManager::startSpecialEdit(const Input& input) const
{
    ctx->viewerMode()->editMode()->startSpecialEdit(pointerPoint(input));
}

void InputManager::startAltSpecialEdit(const Input& input) const
{
    ctx->viewerMode()->editMode()->startAltSpecialEdit(pointerPoint(input));
}

void InputManager::endSpecialEdit(const Input& input) const
{
    ctx->viewerMode()->editMode()->endSpecialEdit(pointerPoint(input));
}

void InputManager::moveSpecialEdit(const Input& input) const
{
    ctx->viewerMode()->editMode()->moveSpecialEdit(pointerPoint(input));
}

void InputManager::selectLinked(const Input& input) const
{
    ctx->viewerMode()->bodyMode()->selectLinked();
}

void InputManager::selectAll(const Input& input) const
{
    ctx->viewerMode()->editMode()->selectAll();
}

void InputManager::deselectAll(const Input& input) const
{
    ctx->viewerMode()->editMode()->deselectAll();
}

void InputManager::invertSelection(const Input& input) const
{
    ctx->viewerMode()->editMode()->invertSelection();
}

void InputManager::toggleEditMode(const Input& input) const
{
    ctx->viewerMode()->toggleEditMode();
}

void InputManager::moveCursor(const Input& input) const
{
    ctx->viewerMode()->moveCursor(pointerPoint(input));
}

void InputManager::translate(const Input& input) const
{
    ctx->viewerMode()->editMode()->translate(pointerPoint(input));
}

void InputManager::rotate(const Input& input) const
{
    ctx->viewerMode()->editMode()->rotate(pointerPoint(input));
}

void InputManager::scale(const Input& input) const
{
    ctx->viewerMode()->editMode()->scale(pointerPoint(input));
}

void InputManager::store(const Input& input) const
{
    ctx->viewerMode()->entityMode()->store();
}

void InputManager::reset(const Input& input) const
{
    ctx->viewerMode()->entityMode()->reset();
}

void InputManager::originTo3DCursor(const Input& input) const
{
    ctx->viewerMode()->entityMode()->originTo3DCursor();
}

void InputManager::originToBody(const Input& input) const
{
    ctx->viewerMode()->entityMode()->originToBody();
}

void InputManager::bodyToOrigin(const Input& input) const
{
    ctx->viewerMode()->entityMode()->bodyToOrigin();
}

void InputManager::resetPosition(const Input& input) const
{
    ctx->viewerMode()->editMode()->resetPosition();
}

void InputManager::resetRotation(const Input& input) const
{
    ctx->viewerMode()->editMode()->resetRotation();
}

void InputManager::resetScale(const Input& input) const
{
    ctx->viewerMode()->editMode()->resetScale();
}

void InputManager::resetLinearVelocity(const Input& input) const
{
    ctx->viewerMode()->editMode()->resetLinearVelocity();
}

void InputManager::resetAngularVelocity(const Input& input) const
{
    ctx->viewerMode()->editMode()->resetAngularVelocity();
}

void InputManager::addKeyframes(const Input& input) const
{
    ctx->viewerMode()->actionMode()->addKeyframes();
}

void InputManager::removeKeyframes(const Input& input) const
{
    ctx->viewerMode()->actionMode()->removeKeyframes();
}

void InputManager::cancelTool(const Input& input) const
{
    ctx->viewerMode()->editMode()->cancelTool();
}

void InputManager::endTool(const Input& input) const
{
    ctx->viewerMode()->editMode()->endTool();
}

void InputManager::useTool(const Input& input) const
{
    ctx->viewerMode()->editMode()->useTool(pointerPoint(input));
}

void InputManager::toggleX(const Input& input) const
{
    ctx->viewerMode()->editMode()->toggleX();
}

void InputManager::toggleY(const Input& input) const
{
    ctx->viewerMode()->editMode()->toggleY();
}

void InputManager::toggleZ(const Input& input) const
{
    ctx->viewerMode()->editMode()->toggleZ();
}

void InputManager::togglePivotMode(const Input& input) const
{
    ctx->viewerMode()->editMode()->togglePivotMode(pointerPoint(input));
}

void InputManager::cursorToSelection(const Input& input) const
{
    ctx->viewerMode()->setCursorPosition(ctx->viewerMode()->editMode()->selectionCenter());
}

void InputManager::selectionToCursor(const Input& input) const
{
    ctx->viewerMode()->editMode()->selectionToCursor();
}

void InputManager::duplicate(const Input& input) const
{
    ctx->viewerMode()->editMode()->duplicate();
}

void InputManager::extrude(const Input& input) const
{
    ctx->viewerMode()->editMode()->extrude();
}

void InputManager::split(const Input& input) const
{
    ctx->viewerMode()->editMode()->split();
}

void InputManager::join(const Input& input) const
{
    ctx->viewerMode()->editMode()->join(pointerPoint(input));
}

void InputManager::addChildren(const Input& input) const
{
    ctx->viewerMode()->editMode()->addChildren(pointerPoint(input));
}

void InputManager::removeParent(const Input& input) const
{
    ctx->viewerMode()->editMode()->removeParent();
}

void InputManager::hideSelected(const Input& input) const
{
    ctx->viewerMode()->editMode()->hideSelected();
}

void InputManager::hideUnselected(const Input& input) const
{
    ctx->viewerMode()->editMode()->hideUnselected();
}

void InputManager::unhideAll(const Input& input) const
{
    ctx->viewerMode()->editMode()->unhideAll();
}

void InputManager::save(const Input& input) const
{
    ctx->save();
}

void InputManager::redo(const Input& input) const
{
    ctx->viewerMode()->redo();
}

void InputManager::undo(const Input& input) const
{
    ctx->viewerMode()->undo();
}

void InputManager::cut(const Input& input) const
{
    ctx->viewerMode()->editMode()->cut();
}

void InputManager::copy(const Input& input) const
{
    ctx->viewerMode()->editMode()->copy();
}

void InputManager::paste(const Input& input) const
{
    ctx->viewerMode()->editMode()->paste();
}

void InputManager::addNewPrefab(const Input& input) const
{
    ctx->viewerMode()->editMode()->addNewPrefab();
}

void InputManager::addNew(const Input& input) const
{
    ctx->viewerMode()->editMode()->addNew();
}

void InputManager::removeSelected(const Input& input) const
{
    ctx->viewerMode()->editMode()->removeSelected();
}

void InputManager::addNumericInput0(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('0');
}

void InputManager::addNumericInput1(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('1');
}

void InputManager::addNumericInput2(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('2');
}

void InputManager::addNumericInput3(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('3');
}

void InputManager::addNumericInput4(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('4');
}

void InputManager::addNumericInput5(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('5');
}

void InputManager::addNumericInput6(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('6');
}

void InputManager::addNumericInput7(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('7');
}

void InputManager::addNumericInput8(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('8');
}

void InputManager::addNumericInput9(const Input& input) const
{
    ctx->viewerMode()->editMode()->addToolNumericInput('9');
}

void InputManager::removeNumericInput(const Input& input) const
{
    ctx->viewerMode()->editMode()->removeToolNumericInput();
}

void InputManager::toggleNumericInputSign(const Input& input) const
{
    ctx->viewerMode()->editMode()->toggleToolNumericInputSign();
}

void InputManager::ensureNumericInputPeriod(const Input& input) const
{
    ctx->viewerMode()->editMode()->ensureToolNumericInputPeriod();
}

void InputManager::toggleHUD(const Input& input) const
{
    ctx->setHUDShown(!ctx->hudShown());
}

void InputManager::toggleFullscreen(const Input& input) const
{
    ctx->controller()->window()->setFullscreen(!ctx->controller()->window()->fullscreen());
}
