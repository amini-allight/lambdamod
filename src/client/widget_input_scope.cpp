/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "widget_input_scope.hpp"
#include "interface_widget.hpp"
#include "control_context.hpp"

static InputScope* pickParentScope(InterfaceWidget* widget)
{
    if (widget->scope())
    {
        return widget->scope();
    }
    else if (widget->parent())
    {
        return pickParentScope(widget->parent());
    }
    else
    {
        return widget->context()->input()->getScope("windows");
    }
}

WidgetInputScope::WidgetInputScope(
    InterfaceWidget* widget,
    InputScope* parent,
    const string& name,
    InputScopeMode mode,
    const function<bool(const Input&)>& isActive,
    const function<void(InputScope*)>& block
)
    : InputScope(
        widget->context(),
        parent,
        name,
        mode,
        [widget, isActive](const Input& input) -> bool { return widget->focused() && isActive(input); },
        block
    )
    , _widget(widget)
{

}

WidgetInputScope::WidgetInputScope(
    InterfaceWidget* widget,
    const string& name,
    InputScopeMode mode,
    const function<bool(const Input&)>& isActive,
    const function<void(InputScope*)>& block
)
    : InputScope(
        widget->context(),
        pickParentScope(widget),
        name,
        mode,
        [widget, isActive](const Input& input) -> bool { return widget->focused() && isActive(input); },
        block
    )
    , _widget(widget)
{

}

bool WidgetInputScope::movePointer(PointerType type, const Point& point, bool consumed) const
{
    // Allow both this widget and one of its children to consume because a chain of parent-child widgets can all be focused simultaneously
    bool consumedLocally = _widget->movePointer(type, point, consumed);

    bool consumedByChild = InputScope::movePointer(type, point, consumed);

    return consumedLocally || consumedByChild;
}

InterfaceWidget* WidgetInputScope::widget() const
{
    return _widget;
}
