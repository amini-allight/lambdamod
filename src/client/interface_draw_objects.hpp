/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

#include <vulkan/vulkan.h>

// This file is basically the second half of interface_draw, it's only ever included in interface_draw.cpp, hence the externs

extern VkDescriptorPool descriptorPool;

extern VkDescriptorSetLayout lineDescriptorSetLayout;
extern VkDescriptorSetLayout solidDescriptorSetLayout;
extern VkDescriptorSetLayout curveDescriptorSetLayout;
extern VkDescriptorSetLayout imageDescriptorSetLayout;

extern VkShaderModule lineVertexShader;
extern VkShaderModule lineFragmentShader;
extern VkShaderModule solidVertexShader;
extern VkShaderModule solidFragmentShader;
extern VkShaderModule roundedBorderVertexShader;
extern VkShaderModule roundedBorderFragmentShader;
extern VkShaderModule imageVertexShader;
extern VkShaderModule imageFragmentShader;
extern VkShaderModule coloredImageVertexShader;
extern VkShaderModule coloredImageFragmentShader;
extern VkShaderModule alphaImageVertexShader;
extern VkShaderModule alphaImageFragmentShader;
extern VkShaderModule dashedLineVertexShader;
extern VkShaderModule dashedLineFragmentShader;
extern VkShaderModule gradient1DVertexShader;
extern VkShaderModule gradient1DFragmentShader;
extern VkShaderModule gradient2DVertexShader;
extern VkShaderModule gradient2DFragmentShader;
extern VkShaderModule curveVertexShader;
extern VkShaderModule curveGeometryShader;
extern VkShaderModule curveFragmentShader;
extern VkShaderModule roundedSolidVertexShader;
extern VkShaderModule roundedSolidFragmentShader;

struct GraphicsPipeline
{
    VkPipelineLayout pipelineLayout;
    VkPipeline uiPipeline;
    VkPipeline panelPipeline;
};

extern GraphicsPipeline line;
extern GraphicsPipeline solid;
extern GraphicsPipeline roundedBorder;
extern GraphicsPipeline image;
extern GraphicsPipeline coloredImage;
extern GraphicsPipeline alphaImage;

extern GraphicsPipeline dashedLine;
extern GraphicsPipeline gradient1D;
extern GraphicsPipeline gradient2D;

extern GraphicsPipeline curve;
extern GraphicsPipeline roundedSolid;

extern VkSampler sampler;

void createDescriptorPool(VkDevice device);
void createDescriptorSetLayouts(VkDevice device);
void createShaders(VkDevice device);
void createGraphicsPipelines(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkRenderPass uiRenderPass,
    VkRenderPass panelRenderPass
);
void createSampler(VkDevice device);

void destroyGraphicsPipelines(VkDevice device);
void destroyShaders(VkDevice device);
void destroyDescriptorSetLayouts(VkDevice device);
void destroyDescriptorPool(VkDevice device);
void destroySampler(VkDevice device);
