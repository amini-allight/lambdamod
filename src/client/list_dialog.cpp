/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "list_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "column.hpp"
#include "row.hpp"
#include "icon_button.hpp"
#include "label.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(256, 384) * uiScale();
}

ListDialog::ListDialog(
    InterfaceView* view,
    const string& name,
    const function<vector<string>()>& source,
    const function<void(const string&)>& onAdd,
    const function<void(const string&)>& onRemove
)
    : Dialog(view, dialogSize())
    , source(source)
    , onAdd(onAdd)
    , onRemove(onRemove)
    , rightClickMenu(nullptr)
{
    auto column = new Column(this);

    new Label(
        column,
        name
    );

    listView = new ListView(
        column,
        [](size_t index) -> void {},
        theme.altBackgroundColor
    );

    auto row = new Row(column);

    lineEdit = new LineEdit(
        row,
        nullptr,
        [this](const string& text) -> void
        {
            this->onAdd(text);
            lineEdit->clear();
        }
    );
    lineEdit->setPlaceholder(localize("list-dialog-add-new"));

    new IconButton(
        row,
        Icon_Add,
        [this]() -> void
        {
            this->onAdd(lineEdit->content());
            lineEdit->clear();
        }
    );
}

ListDialog::~ListDialog()
{
    if (rightClickMenu)
    {
        delete rightClickMenu;
        rightClickMenu = nullptr;
    }
}

void ListDialog::move(const Point& point)
{
    if (rightClickMenu)
    {
        rightClickMenu->move(point + (rightClickMenu->position() - this->position()));
    }

    Dialog::move(point);
}

void ListDialog::step()
{
    Dialog::step();

    vector<string> items = source();

    if (items != this->items)
    {
        this->items = items;
        updateListView();
    }
}

void ListDialog::onRightClick(size_t index, const Point& pointer)
{
    openRightClickMenu(index, pointer);
}

void ListDialog::openRightClickMenu(size_t index, const Point& pointer)
{
    if (index >= items.size())
    {
        return;
    }

    const string& item = items[index];

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("list-dialog-remove"), [this, item](const Point& point) -> void { onRemove(item); closeRightClickMenu(); } }
    };

    rightClickMenu = new RightClickMenu(
        this,
        bind(&ListDialog::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void ListDialog::closeRightClickMenu()
{
    delete rightClickMenu;
    rightClickMenu = nullptr;
}

void ListDialog::updateListView()
{
    listView->clearChildren();

    vector<string> items = this->items;

    sort(items.begin(), items.end());

    size_t i = 0;
    for (const string& item : items)
    {
        auto row = new Row(
            listView,
            {},
            [this, i](const Point& pointer) -> void
            {
                onRightClick(i, pointer);
            }
        );

        new Label(row, item);

        i++;
    }

    listView->update();
}
