/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "knowledge_viewer_view.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "column.hpp"
#include "row.hpp"
#include "spacer.hpp"
#include "panel_dynamic_label.hpp"
#include "panel_label.hpp"
#include "panel_list_view.hpp"

KnowledgeViewerView::KnowledgeViewerView(
    InterfaceWidget* parent,
    const function<tuple<string, string, string>()>& source
)
    : InterfaceWidget(parent)
{
    auto column = new Column(this);

    TextStyleOverride titleStyleOverride;
    titleStyleOverride.font = Text_Monospace;
    titleStyleOverride.weight = Text_Bold;

    new PanelDynamicLabel(
        column,
        bind(&KnowledgeViewerView::titleSource, this),
        titleStyleOverride
    );

    TextStyleOverride subtitleStyleOverride;
    subtitleStyleOverride.font = Text_Monospace;
    subtitleStyleOverride.weight = Text_Regular;
    subtitleStyleOverride.size = UIScale::smallFontSize(this);

    new PanelDynamicLabel(
        column,
        bind(&KnowledgeViewerView::subtitleSource, this),
        subtitleStyleOverride
    );

    listView = new PanelListView(
        column,
        [](size_t index) -> void {}
    );
}

void KnowledgeViewerView::step()
{
    InterfaceWidget::step();

    auto [ title, subtitle, newText ] = source();

    if (newText != text)
    {
        text = newText;
        updateListView();
    }
}

string KnowledgeViewerView::titleSource() const
{
    return get<0>(source());
}

string KnowledgeViewerView::subtitleSource() const
{
    return get<1>(source());
}

TextStyleOverride KnowledgeViewerView::lineStyleOverride() const
{
    TextStyleOverride lineStyleOverride;
    lineStyleOverride.font = Text_Monospace;
    lineStyleOverride.weight = Text_Regular;

    return lineStyleOverride;
}

void KnowledgeViewerView::updateListView()
{
    listView->clearChildren();

    for (const string& line : splitLines(text))
    {
        auto row = new Row(listView);

        MAKE_SPACER(row, UIScale::marginSize(this) * 4, 0);

        new PanelLabel(
            row,
            line,
            lineStyleOverride()
        );

        MAKE_SPACER(row, UIScale::marginSize(this) * 4, 0);
    }
}

vector<string> KnowledgeViewerView::splitLines(const string& text) const
{
    vector<string> lines;

    string remaining = text;

    while (!remaining.empty())
    {
        string chunk;

        for (size_t i = 0; i < remaining.size(); i++)
        {
            chunk += remaining[i];

            i32 w = lineStyleOverride().apply(TextStyle(this)).getFont()->width(chunk);

            if (w == size().w)
            {
                break;
            }
            else if (w > size().w)
            {
                chunk = chunk.substr(0, chunk.size() - 1);
                break;
            }
        }

        remaining = remaining.substr(chunk.size(), remaining.size() - chunk.size());

        lines.push_back(chunk);
    }

    return lines;
}

SizeProperties KnowledgeViewerView::sizeProperties() const
{
    return sizePropertiesFillAll;
}
