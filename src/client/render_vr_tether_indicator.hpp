/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "render_vr_decoration.hpp"

class RenderVRTetherIndicator final : public RenderVRDecoration
{
public:
    RenderVRTetherIndicator(
        const RenderContext* ctx,

        vector<VRSwapchainElement*> swapchainElements,

        VkDescriptorSetLayout descriptorSetLayout,
        RenderVRPipelineStore* pipelineStore,
        const vec3& start,
        const vec3& end,
        f64 strength
    );
    ~RenderVRTetherIndicator();

    void work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const override;

private:
    fvec3 start;
    fvec3 end;
    f32 length;
    f32 strength;
    VmaBuffer vertices;

    VmaBuffer createUniformBuffer() const override;
};
#endif
