/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "log.hpp"

extern map<string, string> localization;

bool hasLocalization(const string& key);

template<typename... Args>
string localize(const string& key, Args... args)
{
    auto it = localization.find(key);

    if (it == localization.end())
    {
        error("Failed to localize '" + key + "': key not found.");
        return "#" + key;
    }

    const string& format = it->second;

    vector<string> parts;

    bool subMode = false;
    string part;

    for (size_t i = 0; i < format.size(); i++)
    {
        char c = format[i];

        if (c == '%')
        {
            if (subMode)
            {
                parts.push_back(part);
                part = "";

                subMode = false;
            }
            else
            {
                parts.push_back(part);
                part = string() + c;

                subMode = true;
            }
        }
        else if (isdigit(c))
        {
            part += c;
        }
        else
        {
            if (subMode)
            {
                parts.push_back(part);
                part = string() + c;

                subMode = false;
            }
            else
            {
                part += c;
            }
        }
    }

    if (!part.empty())
    {
        parts.push_back(part);
    }

    vector<string> inputs = { args... };

    for (size_t i = 0; i < inputs.size(); i++)
    {
        string name = "%" + to_string(i + 1);
        const string& input = inputs[i];

        for (size_t j = 0; j < parts.size(); j++)
        {
            if (parts[j] == name)
            {
                parts[j] = input;
                break;
            }
        }
    }

    string s;

    for (const string& part : parts)
    {
        s += part;
    }

    return s;
}
