/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_grid.hpp"
#include "constants.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr f64 gridRadius = 64;
static constexpr size_t lineCount = 8;
static constexpr size_t circleSegments = 64;
static constexpr size_t circleCount = 8;
static constexpr size_t vertexCount = (2 * lineCount) + (circleSegments * 2 * circleCount) + 4;

RenderVRGrid::RenderVRGrid(
    const RenderContext* ctx,

    vector<VRSwapchainElement*> swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderVRPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation
)
    : RenderVRDecoration(ctx, descriptorSetLayout, pipelineStore->grid.pipelineLayout, pipelineStore->grid.pipeline)
{
    transform = { position, rotation, vec3(1) };

    vertices = createVertexBuffer(vertexCount * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData;
    vertexData.reserve(vertexCount);

    for (size_t i = 0; i < lineCount; i++)
    {
        fquaternion rotation(upDir, 2 * pi * (i / static_cast<f64>(lineCount)));

        vertexData.push_back({
            rotation.rotate(fvec3(0, +gridRadius, 0)),
            theme.gridColor.toVec3()
        });

        vertexData.push_back({
            rotation.rotate(fvec3(0, -gridRadius, 0)),
            theme.gridColor.toVec3()
        });
    }

    for (size_t i = 0; i < circleCount; i++)
    {
        f64 radius = gridRadius * ((i + 1) / static_cast<f64>(circleCount));

        for (size_t j = 0; j < circleSegments; j++)
        {
            fvec3 point(0, radius, 0);

            vertexData.push_back({
                fquaternion(upDir, 2 * pi * (j / static_cast<f64>(circleSegments))).rotate(point),
                theme.gridSecondaryColor.toVec3()
            });

            vertexData.push_back({
                fquaternion(upDir, 2 * pi * ((j + 1) / static_cast<f64>(circleSegments))).rotate(point),
                theme.gridSecondaryColor.toVec3()
            });
        }
    }

    vertexData.push_back({
        forwardDir * 2,
        theme.gridColor.toVec3()
    });
    vertexData.push_back({
        (forwardDir * 1.5) - (rightDir / 2),
        theme.gridColor.toVec3()
    });

    vertexData.push_back({
        forwardDir * 2,
        theme.gridColor.toVec3()
    });
    vertexData.push_back({
        (forwardDir * 1.5) + (rightDir / 2),
        theme.gridColor.toVec3()
    });

    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderVRGrid::~RenderVRGrid()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderVRGrid::work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const
{
    RenderVRDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderVRGrid::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
#endif
