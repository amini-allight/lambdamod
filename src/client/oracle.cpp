/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "oracle.hpp"

bool OracleColumn::operator==(const OracleColumn& rhs) const
{
    return title == rhs.title &&
        options == rhs.options;
}

bool OracleColumn::operator!=(const OracleColumn& rhs) const
{
    return !(*this == rhs);
}

bool Oracle::operator==(const Oracle& rhs) const
{
    return name == rhs.name &&
        columns == rhs.columns;
}

bool Oracle::operator!=(const Oracle& rhs) const
{
    return !(*this == rhs);
}
