/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "dialog.hpp"
#include "list_view.hpp"
#include "sky_parameters.hpp"

class SkyParametersDialog : public Dialog
{
public:
    SkyParametersDialog(
        InterfaceView* view,
        const function<SkyParameters()>& parametersSource,
        const function<void(const SkyParameters&)>& onParameters
    );

    void step() override;

private:
    function<SkyParameters()> parametersSource;
    function<void(const SkyParameters&)> onParameters;
    ListView* listView;
    optional<SkyParameters> lastSkyParameters;

    bool needsWidgetUpdate() const;
    void updateWidgets();

    void addSkyChildren();
    void addSkyBaseChildren(const SkyBase& base);
    void addSkyBlankChildren(const SkyBaseBlank& base);
    void addSkyUnidirectionalChildren(const SkyBaseUnidirectional& base);
    void addSkyBidirectionalChildren(const SkyBaseBidirectional& base);
    void addSkyOmnidirectionalChildren(const SkyBaseOmnidirectional& base);
    void addSkyLayerChildren(size_t index, const SkyLayer& layer);
    void addSkyLayerStarsChildren(size_t index, const SkyLayerStars& layer);
    void addSkyLayerCircleChildren(size_t index, const SkyLayerCircle& layer);
    void addSkyLayerCloudChildren(size_t index, const SkyLayerCloud& layer);
    void addSkyLayerAuroraChildren(size_t index, const SkyLayerAurora& layer);
    void addSkyLayerCometChildren(size_t index, const SkyLayerComet& layer);
    void addSkyLayerVortexChildren(size_t index, const SkyLayerVortex& layer);
    void addSkyLayerMeteorShowerChildren(size_t index, const SkyLayerMeteorShower& layer);
    void addSkyLayerNewPrompt();

    void addSkyLayer();
    void moveSkyLayerDown(size_t index);
    void moveSkyLayerUp(size_t index);
    void removeSkyLayer(size_t index);

    bool skyEnabledSource();
    void onSkyEnabled(bool state);

    u64 skySeedSource();
    void onSkySeed(u64 seed);

    vector<string> skyBaseTypeOptionsSource();
    vector<string> skyBaseTypeOptionTooltipsSource();
    size_t skyBaseTypeSource();
    void onSkyBaseType(size_t index);

    HDRColor skyBaseUnidirectionalHorizonColorSource();
    void onSkyBaseUnidirectionalHorizonColor(const HDRColor& color);
    HDRColor skyBaseUnidirectionalZenithColorSource();
    void onSkyBaseUnidirectionalZenithColor(const HDRColor& color);

    HDRColor skyBaseBidirectionalHorizonColorSource();
    void onSkyBaseBidirectionalHorizonColor(const HDRColor& color);
    HDRColor skyBaseBidirectionalLowerZenithColorSource();
    void onSkyBaseBidirectionalLowerZenithColor(const HDRColor& color);
    HDRColor skyBaseBidirectionalUpperZenithColorSource();
    void onSkyBaseBidirectionalUpperZenithColor(const HDRColor& color);

    HDRColor skyBaseOmnidirectionalColorSource();
    void onSkyBaseOmnidirectionalColor(const HDRColor& color);

    vector<string> skyLayerTypeOptionsSource();
    vector<string> skyLayerTypeOptionTooltipsSource();
    size_t skyLayerTypeSource(size_t index);
    void onSkyLayerType(size_t index, size_t typeIndex);

    f64 skyStarsDensitySource(size_t index);
    void onSkyStarsDensity(size_t index, f64 density);
    HDRColor skyStarsColorSource(size_t index);
    void onSkyStarsColor(size_t index, const HDRColor& color);

    vec2 skyCirclePositionSource(size_t index);
    void onSkyCirclePosition(size_t index, const vec2& position);
    f64 skyCircleSizeSource(size_t index);
    void onSkyCircleSize(size_t index, const f64& size);
    HDRColor skyCircleColorSource(size_t index);
    void onSkyCircleColor(size_t index, const HDRColor& color);
    f64 skyCirclePhaseSource(size_t index);
    void onSkyCirclePhase(size_t index, f64 phase);

    f64 skyCloudDensitySource(size_t index);
    void onSkyCloudDensity(size_t index, f64 density);
    vec2 skyCloudPositionSource(size_t index);
    void onSkyCloudPosition(size_t index, const vec2& position);
    vec2 skyCloudSizeSource(size_t index);
    void onSkyCloudSize(size_t index, const vec2& size);
    HDRColor skyCloudInnerColorSource(size_t index);
    void onSkyCloudInnerColor(size_t index, const HDRColor& color);
    HDRColor skyCloudOuterColorSource(size_t index);
    void onSkyCloudOuterColor(size_t index, const HDRColor& color);

    f64 skyAuroraDensitySource(size_t index);
    void onSkyAuroraDensity(size_t index, f64 density);
    HDRColor skyAuroraLowerColorSource(size_t index);
    void onSkyAuroraLowerColor(size_t index, const HDRColor& color);
    HDRColor skyAuroraUpperColorSource(size_t index);
    void onSkyAuroraUpperColor(size_t index, const HDRColor& color);

    vec2 skyCometPositionSource(size_t index);
    void onSkyCometPosition(size_t index, const vec2& position);
    f64 skyCometSizeSource(size_t index);
    void onSkyCometSize(size_t index, f64 size);
    f64 skyCometLengthSource(size_t index);
    void onSkyCometLength(size_t index, f64 length);
    f64 skyCometRotationSource(size_t index);
    void onSkyCometRotation(size_t index, f64 rotation);
    HDRColor skyCometHeadColorSource(size_t index);
    void onSkyCometHeadColor(size_t index, const HDRColor& color);
    HDRColor skyCometTailColorSource(size_t index);
    void onSkyCometTailColor(size_t index, const HDRColor& color);

    vec2 skyVortexPositionSource(size_t index);
    void onSkyVortexPosition(size_t index, const vec2& position);
    f64 skyVortexRotationSpeedSource(size_t index);
    void onSkyVortexRotationSpeed(size_t index, f64 rotationSpeed);
    f64 skyVortexRadiusSource(size_t index);
    void onSkyVortexRadius(size_t index, f64 radius);
    HDRColor skyVortexInnerColorSource(size_t index);
    void onSkyVortexInnerColor(size_t index, const HDRColor& color);
    HDRColor skyVortexOuterColorSource(size_t index);
    void onSkyVortexOuterColor(size_t index, const HDRColor& color);

    f64 skyMeteorShowerDensitySource(size_t index);
    void onSkyMeteorShowerDensity(size_t index, f64 density);
    HDRColor skyMeteorShowerColorSource(size_t index);
    void onSkyMeteorShowerColor(size_t index, const HDRColor& color);
};
