/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "render_types.hpp"
#include "render_context.hpp"
#include "render_hardware_occlusion_scene.hpp"
#include "render_software_occlusion_scene.hpp"

class AuxSwapchainElement;
class SkySwapchainElement;

class SwapchainElement
{
public:
    SwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height
    );
    SwapchainElement(const SwapchainElement& rhs) = delete;
    SwapchainElement(SwapchainElement&& rhs) = delete;
    virtual ~SwapchainElement();

    SwapchainElement& operator=(const SwapchainElement& rhs) = delete;
    SwapchainElement& operator=(SwapchainElement&& rhs) = delete;

    u32 imageIndex() const;
    u32 width() const;
    u32 height() const;

protected:
    const RenderContext* ctx;
    u32 _imageIndex;
    u32 _width;
    u32 _height;

    VmaImage createImage(
        u32 width,
        u32 height,
        VkFormat format,
        VkSampleCountFlagBits sampleCount,
        VkImageUsageFlags usage,
        u32 mipLevels = 1,
        u32 layers = 1
    );
    VkImageView createImageView(
        VkFormat format,
        VkImage image,
        VkImageAspectFlags aspect,
        VkImageViewType type = VK_IMAGE_VIEW_TYPE_2D,
        u32 mipLevels = 1,
        u32 layers = 1
    );
    VkFramebuffer createFramebuffer(
        u32 width,
        u32 height,
        VkRenderPass renderPass,
        const vector<VkImageView>& imageViews,
        u32 layers = 1
    );

    VkCommandBuffer createCommandBuffer();
    VkDescriptorSet createDescriptorSet(
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera,
        VmaBuffer object,
        VkImageView imageView
    );
    VkSemaphore createSemaphore();
    VkFence createFence();
};

class MainSwapchainElement : public SwapchainElement
{
public:
    MainSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height
    );

    virtual VkImage colorImage() const = 0;
    virtual VkImage stencilImage() const = 0;
    virtual VkImage sideImage() const = 0;
    virtual VkImage luminanceImage() const = 0;
    virtual VkImage outputImage() const = 0;

    virtual VkImageView depthImageView() const = 0;
    virtual VkImageView stencilImageView() const = 0;
    virtual VkImageView sideImageView() const = 0;

    virtual VkFramebuffer mainFramebuffer() const = 0;
    virtual VkFramebuffer hdrFramebuffer() const = 0;
    virtual VkFramebuffer postProcessFramebuffer() const = 0;
    virtual VkFramebuffer overlayFramebuffer() const = 0;
    virtual VkFramebuffer independentDepthOverlayFramebuffer() const = 0;

    virtual VkCommandBuffer transferCommandBuffer() const = 0;
    virtual VkCommandBuffer commandBuffer() const = 0;

    virtual VmaBuffer luminance() const = 0;
    virtual VmaMapping<fvec4>* luminanceMapping() const = 0;

    virtual AuxSwapchainElement* textTitlecard() const = 0;
    virtual AuxSwapchainElement* text() const = 0;
    virtual SkySwapchainElement* sky() const = 0;

    virtual RenderHardwareOcclusionScene* hardwareOcclusionScene() const = 0;
    virtual RenderSoftwareOcclusionScene* softwareOcclusionScene() const = 0;

    virtual RenderOcclusionSceneReference occlusionScene() const = 0;

    virtual void setLuminanceReady() = 0;
    virtual bool luminanceReady() const = 0;
};

class AuxSwapchainElement final : public SwapchainElement
{
public:
    AuxSwapchainElement(
        const MainSwapchainElement* parent,
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height,
        VkRenderPass renderPass,
        VkDescriptorSetLayout descriptorSetLayout,
        VmaBuffer camera
    );
    ~AuxSwapchainElement() override;

    const MainSwapchainElement* parent;
    VmaImage image;
    VkImageView imageView;
    VkFramebuffer framebuffer;
    VmaBuffer uniform;
    VmaMapping<f32>* uniformMapping;
    VkDescriptorSet descriptorSet;
};

class SkySwapchainElement final : public SwapchainElement
{
public:
    SkySwapchainElement(
        const MainSwapchainElement* parent,
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height,
        VkRenderPass renderPass
    );
    ~SkySwapchainElement() override;

    const MainSwapchainElement* parent;
    VmaImage image;
    VkImageView imageView;
    VkFramebuffer framebuffer;
};

class FlatSwapchainElement : public MainSwapchainElement
{
public:
    FlatSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height
    );

    virtual VkFramebuffer uiFramebuffer() const = 0;

    virtual const VkSemaphore& start() const = 0;
    virtual const VkSemaphore& end() const = 0;
    virtual const VkFence& fence() const = 0;
    virtual VkFence& lastFence() = 0;

    virtual VmaBuffer camera() const = 0;
    virtual VmaMapping<fmat4>* cameraMapping() const = 0;
};

class FlatMonosampleSwapchainElement final : public FlatSwapchainElement
{
public:
    FlatMonosampleSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height,
        VkRenderPass panelRenderPass,
        VkRenderPass skyRenderPass,
        VkRenderPass mainRenderPass,
        VkRenderPass hdrRenderPass,
        VkRenderPass postProcessRenderPass,
        VkRenderPass overlayRenderPass,
        VkRenderPass independentDepthOverlayRenderPass,
        VkRenderPass uiRenderPass,
        VkDescriptorSetLayout descriptorSetLayout,
        VkFormat format,
        VkImage image
    );
    ~FlatMonosampleSwapchainElement() override;

    VkImage colorImage() const override;
    VkImage stencilImage() const override;
    VkImage sideImage() const override;
    VkImage luminanceImage() const override;
    VkImage outputImage() const override;

    VkImageView depthImageView() const override;
    VkImageView stencilImageView() const override;
    VkImageView sideImageView() const override;

    VkFramebuffer mainFramebuffer() const override;
    VkFramebuffer hdrFramebuffer() const override;
    VkFramebuffer postProcessFramebuffer() const override;
    VkFramebuffer overlayFramebuffer() const override;
    VkFramebuffer independentDepthOverlayFramebuffer() const override;
    VkFramebuffer uiFramebuffer() const override;

    VkCommandBuffer transferCommandBuffer() const override;
    VkCommandBuffer commandBuffer() const override;

    const VkSemaphore& start() const override;
    const VkSemaphore& end() const override;
    const VkFence& fence() const override;
    VkFence& lastFence() override;

    VmaBuffer camera() const override;
    VmaMapping<fmat4>* cameraMapping() const override;
    VmaBuffer luminance() const override;
    VmaMapping<fvec4>* luminanceMapping() const override;

    AuxSwapchainElement* textTitlecard() const override;
    AuxSwapchainElement* text() const override;
    SkySwapchainElement* sky() const override;

    RenderHardwareOcclusionScene* hardwareOcclusionScene() const override;
    RenderSoftwareOcclusionScene* softwareOcclusionScene() const override;

    RenderOcclusionSceneReference occlusionScene() const override;

    void setLuminanceReady() override;
    bool luminanceReady() const override;

private:
    VkImage _outputImage;

    VmaImage _colorImage;
    VmaImage depthImage;
    VmaImage _stencilImage;
    VmaImage _sideImage;
    VmaImage _luminanceImage;

    VkImageView colorImageView;
    VkImageView _depthImageView;
    VkImageView _stencilImageView;
    VkImageView _sideImageView;

    VkFramebuffer _mainFramebuffer;
    VkFramebuffer _hdrFramebuffer;
    VkFramebuffer _postProcessFramebuffer;
    VkFramebuffer _overlayFramebuffer;
    VkFramebuffer _independentDepthOverlayFramebuffer;
    VkFramebuffer _uiFramebuffer;

    VkCommandBuffer _transferCommandBuffer;
    VkCommandBuffer _commandBuffer;

    VkSemaphore _start;
    VkSemaphore _end;
    VkFence _fence;
    VkFence _lastFence;

    VmaBuffer _camera;
    VmaMapping<fmat4>* _cameraMapping;
    VmaBuffer _luminance;
    VmaMapping<fvec4>* _luminanceMapping;

    AuxSwapchainElement* _textTitlecard;
    AuxSwapchainElement* _text;
    SkySwapchainElement* _sky;

    RenderHardwareOcclusionScene* _hardwareOcclusionScene;
    RenderSoftwareOcclusionScene* _softwareOcclusionScene;

    bool _luminanceReady;
};

class FlatMultisampleSwapchainElement final : public FlatSwapchainElement
{
public:
    FlatMultisampleSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height,
        VkSampleCountFlagBits sampleCount,
        VkRenderPass panelRenderPass,
        VkRenderPass skyRenderPass,
        VkRenderPass mainRenderPass,
        VkRenderPass hdrRenderPass,
        VkRenderPass postProcessRenderPass,
        VkRenderPass overlayRenderPass,
        VkRenderPass independentDepthOverlayRenderPass,
        VkRenderPass uiRenderPass,
        VkDescriptorSetLayout descriptorSetLayout,
        VkFormat format,
        VkImage image
    );
    ~FlatMultisampleSwapchainElement() override;

    VkImage colorImage() const override;
    VkImage stencilImage() const override;
    VkImage sideImage() const override;
    VkImage luminanceImage() const override;
    VkImage outputImage() const override;

    VkImageView depthImageView() const override;
    VkImageView stencilImageView() const override;
    VkImageView sideImageView() const override;

    VkFramebuffer mainFramebuffer() const override;
    VkFramebuffer hdrFramebuffer() const override;
    VkFramebuffer postProcessFramebuffer() const override;
    VkFramebuffer overlayFramebuffer() const override;
    VkFramebuffer independentDepthOverlayFramebuffer() const override;
    VkFramebuffer uiFramebuffer() const override;

    VkCommandBuffer transferCommandBuffer() const override;
    VkCommandBuffer commandBuffer() const override;

    const VkSemaphore& start() const override;
    const VkSemaphore& end() const override;
    const VkFence& fence() const override;
    VkFence& lastFence() override;

    VmaBuffer camera() const override;
    VmaMapping<fmat4>* cameraMapping() const override;
    VmaBuffer luminance() const override;
    VmaMapping<fvec4>* luminanceMapping() const override;

    AuxSwapchainElement* textTitlecard() const override;
    AuxSwapchainElement* text() const override;
    SkySwapchainElement* sky() const override;

    RenderHardwareOcclusionScene* hardwareOcclusionScene() const override;
    RenderSoftwareOcclusionScene* softwareOcclusionScene() const override;

    RenderOcclusionSceneReference occlusionScene() const override;

    void setLuminanceReady() override;
    bool luminanceReady() const override;

private:
    VkImage _outputImage;

    VmaImage colorMultisampleImage;
    VmaImage depthMultisampleImage;
    VmaImage stencilMultisampleImage;
    VmaImage _colorImage;
    VmaImage depthImage;
    VmaImage _stencilImage;
    VmaImage _sideImage;
    VmaImage _luminanceImage;

    VkImageView colorMultisampleImageView;
    VkImageView depthMultisampleImageView;
    VkImageView stencilMultisampleImageView;
    VkImageView colorImageView;
    VkImageView _depthImageView;
    VkImageView _stencilImageView;
    VkImageView _sideImageView;

    VkFramebuffer _mainFramebuffer;
    VkFramebuffer _hdrFramebuffer;
    VkFramebuffer _postProcessFramebuffer;
    VkFramebuffer _overlayFramebuffer;
    VkFramebuffer _independentDepthOverlayFramebuffer;
    VkFramebuffer _uiFramebuffer;

    VkCommandBuffer _transferCommandBuffer;
    VkCommandBuffer _commandBuffer;

    VkSemaphore _start;
    VkSemaphore _end;
    VkFence _fence;
    VkFence _lastFence;

    VmaBuffer _camera;
    VmaMapping<fmat4>* _cameraMapping;
    VmaBuffer _luminance;
    VmaMapping<fvec4>* _luminanceMapping;

    AuxSwapchainElement* _textTitlecard;
    AuxSwapchainElement* _text;
    SkySwapchainElement* _sky;

    RenderHardwareOcclusionScene* _hardwareOcclusionScene;
    RenderSoftwareOcclusionScene* _softwareOcclusionScene;

    bool _luminanceReady;
};

class VRSwapchainElement : public MainSwapchainElement
{
public:
    VRSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height
    );

    virtual VmaBuffer worldCamera() const = 0;
    virtual VmaMapping<fmat4>* worldCameraMapping() const = 0;
    virtual VmaBuffer roomCamera() const = 0;
    virtual VmaMapping<fmat4>* roomCameraMapping() const = 0;

    virtual AuxSwapchainElement* hud() const = 0;
    virtual AuxSwapchainElement* keyboard() const = 0;

protected:
    u32 _eye;
};

class VRMonosampleSwapchainElement final : public VRSwapchainElement
{
public:
    VRMonosampleSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height,
        VkRenderPass panelRenderPass,
        VkRenderPass skyRenderPass,
        VkRenderPass mainRenderPass,
        VkRenderPass hdrRenderPass,
        VkRenderPass postProcessRenderPass,
        VkRenderPass overlayRenderPass,
        VkRenderPass independentDepthOverlayRenderPass,
        VkDescriptorSetLayout descriptorSetLayout,
        VkFormat format,
        VkImage image
    );
    ~VRMonosampleSwapchainElement() override;

    VkImage colorImage() const override;
    VkImage stencilImage() const override;
    VkImage sideImage() const override;
    VkImage luminanceImage() const override;
    VkImage outputImage() const override;

    VkImageView depthImageView() const override;
    VkImageView stencilImageView() const override;
    VkImageView sideImageView() const override;

    VkFramebuffer mainFramebuffer() const override;
    VkFramebuffer hdrFramebuffer() const override;
    VkFramebuffer postProcessFramebuffer() const override;
    VkFramebuffer overlayFramebuffer() const override;
    VkFramebuffer independentDepthOverlayFramebuffer() const override;

    VkCommandBuffer transferCommandBuffer() const override;
    VkCommandBuffer commandBuffer() const override;

    VmaBuffer worldCamera() const override;
    VmaMapping<fmat4>* worldCameraMapping() const override;
    VmaBuffer roomCamera() const override;
    VmaMapping<fmat4>* roomCameraMapping() const override;
    VmaBuffer luminance() const override;
    VmaMapping<fvec4>* luminanceMapping() const override;

    AuxSwapchainElement* textTitlecard() const override;
    AuxSwapchainElement* text() const override;
    AuxSwapchainElement* hud() const override;
    AuxSwapchainElement* keyboard() const override;
    SkySwapchainElement* sky() const override;

    RenderHardwareOcclusionScene* hardwareOcclusionScene() const override;
    RenderSoftwareOcclusionScene* softwareOcclusionScene() const override;

    RenderOcclusionSceneReference occlusionScene() const override;

    void setLuminanceReady() override;
    bool luminanceReady() const override;

private:
    VkImage _outputImage;

    VmaImage _colorImage;
    VmaImage depthImage;
    VmaImage _stencilImage;
    VmaImage _sideImage;
    VmaImage _luminanceImage;

    VkImageView colorImageView;
    VkImageView _depthImageView;
    VkImageView _stencilImageView;
    VkImageView _sideImageView;

    VkFramebuffer _mainFramebuffer;
    VkFramebuffer _hdrFramebuffer;
    VkFramebuffer _postProcessFramebuffer;
    VkFramebuffer _overlayFramebuffer;
    VkFramebuffer _independentDepthOverlayFramebuffer;

    VkCommandBuffer _transferCommandBuffer;
    VkCommandBuffer _commandBuffer;

    VmaBuffer _worldCamera;
    VmaMapping<fmat4>* _worldCameraMapping;
    VmaBuffer _roomCamera;
    VmaMapping<fmat4>* _roomCameraMapping;
    VmaBuffer _luminance;
    VmaMapping<fvec4>* _luminanceMapping;

    AuxSwapchainElement* _textTitlecard;
    AuxSwapchainElement* _text;
    AuxSwapchainElement* _hud;
    AuxSwapchainElement* _keyboard;
    SkySwapchainElement* _sky;

    RenderHardwareOcclusionScene* _hardwareOcclusionScene;
    RenderSoftwareOcclusionScene* _softwareOcclusionScene;

    bool _luminanceReady;
};

class VRMultisampleSwapchainElement final : public VRSwapchainElement
{
public:
    VRMultisampleSwapchainElement(
        const RenderContext* ctx,
        u32 imageIndex,
        u32 width,
        u32 height,
        VkSampleCountFlagBits sampleCount,
        VkRenderPass panelRenderPass,
        VkRenderPass skyRenderPass,
        VkRenderPass mainRenderPass,
        VkRenderPass hdrRenderPass,
        VkRenderPass postProcessRenderPass,
        VkRenderPass overlayRenderPass,
        VkRenderPass independentDepthOverlayRenderPass,
        VkDescriptorSetLayout descriptorSetLayout,
        VkFormat format,
        VkImage image
    );
    ~VRMultisampleSwapchainElement() override;

    VkImage colorImage() const override;
    VkImage stencilImage() const override;
    VkImage sideImage() const override;
    VkImage luminanceImage() const override;
    VkImage outputImage() const override;

    VkImageView depthImageView() const override;
    VkImageView stencilImageView() const override;
    VkImageView sideImageView() const override;

    VkFramebuffer mainFramebuffer() const override;
    VkFramebuffer hdrFramebuffer() const override;
    VkFramebuffer postProcessFramebuffer() const override;
    VkFramebuffer overlayFramebuffer() const override;
    VkFramebuffer independentDepthOverlayFramebuffer() const override;

    VkCommandBuffer transferCommandBuffer() const override;
    VkCommandBuffer commandBuffer() const override;

    VmaBuffer worldCamera() const override;
    VmaMapping<fmat4>* worldCameraMapping() const override;
    VmaBuffer roomCamera() const override;
    VmaMapping<fmat4>* roomCameraMapping() const override;
    VmaBuffer luminance() const override;
    VmaMapping<fvec4>* luminanceMapping() const override;

    AuxSwapchainElement* textTitlecard() const override;
    AuxSwapchainElement* text() const override;
    AuxSwapchainElement* hud() const override;
    AuxSwapchainElement* keyboard() const override;
    SkySwapchainElement* sky() const override;

    RenderHardwareOcclusionScene* hardwareOcclusionScene() const override;
    RenderSoftwareOcclusionScene* softwareOcclusionScene() const override;

    RenderOcclusionSceneReference occlusionScene() const override;

    void setLuminanceReady() override;
    bool luminanceReady() const override;

private:
    VkImage _outputImage;

    VmaImage colorMultisampleImage;
    VmaImage depthMultisampleImage;
    VmaImage stencilMultisampleImage;
    VmaImage _colorImage;
    VmaImage depthImage;
    VmaImage _stencilImage;
    VmaImage _sideImage;
    VmaImage _luminanceImage;

    VkImageView colorMultisampleImageView;
    VkImageView depthMultisampleImageView;
    VkImageView stencilMultisampleImageView;
    VkImageView colorImageView;
    VkImageView _depthImageView;
    VkImageView _stencilImageView;
    VkImageView _sideImageView;

    VkFramebuffer _mainFramebuffer;
    VkFramebuffer _hdrFramebuffer;
    VkFramebuffer _postProcessFramebuffer;
    VkFramebuffer _overlayFramebuffer;
    VkFramebuffer _independentDepthOverlayFramebuffer;

    VkCommandBuffer _transferCommandBuffer;
    VkCommandBuffer _commandBuffer;

    VmaBuffer _worldCamera;
    VmaMapping<fmat4>* _worldCameraMapping;
    VmaBuffer _roomCamera;
    VmaMapping<fmat4>* _roomCameraMapping;
    VmaBuffer _luminance;
    VmaMapping<fvec4>* _luminanceMapping;

    AuxSwapchainElement* _textTitlecard;
    AuxSwapchainElement* _text;
    AuxSwapchainElement* _hud;
    AuxSwapchainElement* _keyboard;
    SkySwapchainElement* _sky;

    RenderHardwareOcclusionScene* _hardwareOcclusionScene;
    RenderSoftwareOcclusionScene* _softwareOcclusionScene;

    bool _luminanceReady;
};
