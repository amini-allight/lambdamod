/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "item_list.hpp"
#include "tools.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "row.hpp"
#include "column.hpp"
#include "close_button.hpp"
#include "label.hpp"
#include "control_context.hpp"
#include "rename_dialog.hpp"
#include "clone_dialog.hpp"

ItemList::ItemList(
    InterfaceWidget* parent,
    const function<vector<string>()>& source,
    const function<void(const string&)>& onAdd,
    const function<void(const string&)>& onRemove,
    const function<void(const string&)>& onSelect,
    const function<void(const string&, const string&)>& onRename,
    const function<void(const string&, const string&)>& onClone,
    const function<vector<tuple<string, function<void(const Point&)>>>(i32)>& onRightClick
)
    : RightClickableWidget(parent)
    , _activeIndex(-1)
    , source(source)
    , _onAdd(onAdd)
    , onRemove(onRemove)
    , _onSelect(onSelect)
    , _onRightClick(onRightClick)
    , _onRename(onRename)
    , _onClone(onClone)
{
    auto column = new Column(this);

    auto row = new Row(column);

    lineEdit = new LineEdit(
        row,
        nullptr,
        bind(&ItemList::onAdd, this, placeholders::_1)
    );
    lineEdit->setPlaceholder(localize("item-list-add-new"));

    new IconButton(
        row,
        Icon_Add,
        bind(&ItemList::onAdd, this, "")
    );

    listView = new ListView(
        column,
        bind(&ItemList::onSelect, this, placeholders::_1)
    );
}

void ItemList::select(i32 index)
{
    onSelect(index);
}

void ItemList::clearSelection()
{
    _activeIndex = -1;
}

i32 ItemList::activeIndex() const
{
    return _activeIndex;
}

void ItemList::step()
{
    RightClickableWidget::step();

    vector<string> items = source();

    if (items != this->items)
    {
        this->items = items;
        updateListView();
    }
}

void ItemList::updateListView()
{
    listView->clearChildren();

    i32 i = 0;
    for (const string& item : items)
    {
        bool focused = i == _activeIndex;

        auto row = new Row(
            listView,
            focused ? theme.accentColor : optional<Color>(),
            bind(&ItemList::onRightClick, this, i, placeholders::_1)
        );

        TextStyleOverride style;
        style.weight = focused ? Text_Bold : Text_Regular;
        style.color = focused ? &theme.activeTextColor : &theme.textColor;

        new Label(
            row,
            item,
            style
        );

        new CloseButton(
            row,
            [this, item]() -> void { onRemove(item); }
        );
        i++;
    }

    listView->update();
}

void ItemList::onSelect(i32 index)
{
    if (index == _activeIndex)
    {
        return;
    }

    _activeIndex = index;
    updateListView();

    _onSelect(source().at(index));
}

void ItemList::openRightClickMenu(i32 index, const Point& pointer)
{
    if (index >= static_cast<i32>(items.size()))
    {
        return;
    }

    const string& name = items[index];

    vector<tuple<string, function<void(const Point&)>>> entries = {
        { localize("item-list-rename"), bind(&ItemList::onRename, this, name, placeholders::_1) },
        { localize("item-list-clone"), bind(&ItemList::onClone, this, name, placeholders::_1) }
    };

    if (_onRightClick)
    {
        for (const auto& it : _onRightClick(index))
        {
            string name;
            function<void(const Point&)> behavior;
            tie(name, behavior) = it;

            entries.push_back({ name, [this, behavior](const Point& point) -> void {
                behavior(point);

                closeRightClickMenu();
            }});
        }
    }

    rightClickMenu = new RightClickMenu(
        this,
        bind(&ItemList::closeRightClickMenu, this),
        entries
    );
    rightClickMenu->open(pointer);
}

void ItemList::onAdd(const string& name)
{
    size_t beforeCount = source().size();

    _onAdd(lineEdit->content());
    lineEdit->clear();

    size_t afterCount = source().size();

    if (beforeCount < afterCount)
    {
        onSelect(afterCount - 1);
    }
}

void ItemList::onRename(const string& name, const Point& pointer)
{
    context()->interface()->addWindow<RenameDialog>(bind(_onRename, name, placeholders::_1));

    closeRightClickMenu();
}

void ItemList::onClone(const string& name, const Point& pointer)
{
    context()->interface()->addWindow<CloneDialog>(bind(_onClone, name, placeholders::_1));

    closeRightClickMenu();
}

SizeProperties ItemList::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::itemListWidth()), 0,
        Scaling_Fixed, Scaling_Fill
    };
}
