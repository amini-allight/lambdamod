#include "render_post_process_effect.hpp"
#include "render_descriptor_set_write_components.hpp"
#include "render_decoration_component.hpp"

RenderPostProcessEffect::RenderPostProcessEffect(
    const RenderContext* ctx,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
)
    : _shown(true)
    , ctx(ctx)
    , pipelineLayout(pipelineLayout)
    , pipeline(pipeline)
{

}

RenderPostProcessEffect::~RenderPostProcessEffect()
{

}

void RenderPostProcessEffect::show(bool state)
{
    _shown = state;
}

bool RenderPostProcessEffect::shown() const
{
    return _shown;
}

void RenderPostProcessEffect::work(const SwapchainElement* swapchainElement, f32 timer) const
{
    // Skip for post process effects which don't use uniform buffers (none currently, previously copy)
    if (components.empty())
    {
        return;
    }

    fillUniformBuffer(components.at(swapchainElement->imageIndex())->uniformMapping, timer);
}

void RenderPostProcessEffect::destroyComponents()
{
    for (const RenderPostProcessEffectComponent* component : components)
    {
        delete component;
    }
}

void RenderPostProcessEffect::setDescriptorSet(
    VkDescriptorSet descriptorSet,
    VkImageView colorImageView
) const
{
    VkDescriptorImageInfo colorImageWriteImage{};
    VkWriteDescriptorSet colorImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 0, colorImageView, ctx->unfilteredSampler, &colorImageWriteImage, &colorImageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        colorImageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

void RenderPostProcessEffect::setDescriptorSet(
    VkDescriptorSet descriptorSet,
    VkBuffer buffer,
    VkImageView colorImageView
) const
{
    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer, &parametersWriteBuffer, &parametersWrite);

    VkDescriptorImageInfo colorImageWriteImage{};
    VkWriteDescriptorSet colorImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, colorImageView, ctx->unfilteredSampler, &colorImageWriteImage, &colorImageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        parametersWrite,
        colorImageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

void RenderPostProcessEffect::setDescriptorSet(
    VkDescriptorSet descriptorSet,
    VkBuffer buffer,
    VkImageView colorImageView,
    VkImageView depthImageView
) const
{
    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, buffer, &parametersWriteBuffer, &parametersWrite);

    VkDescriptorImageInfo colorImageWriteImage{};
    VkWriteDescriptorSet colorImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, colorImageView, ctx->unfilteredSampler, &colorImageWriteImage, &colorImageWrite);

    VkDescriptorImageInfo depthImageWriteImage{};
    VkWriteDescriptorSet depthImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 2, depthImageView, ctx->unfilteredSampler, &depthImageWriteImage, &depthImageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        parametersWrite,
        colorImageWrite,
        depthImageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}
