/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_marker.hpp"
#include "theme.hpp"
#include "render_tools.hpp"

static constexpr f64 anchorSize = 0.1; // m

RenderMarker::RenderMarker(
    const RenderContext* ctx,

    const vector<FlatSwapchainElement*>& swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderFlatPipelineStore* pipelineStore,
    const vec3& position,
    const quaternion& rotation
)
    : RenderFlatDecoration(ctx, descriptorSetLayout, pipelineStore->coloredLine.pipelineLayout, pipelineStore->coloredLine.pipeline)
{
    transform = { position, rotation, vec3(1) };

    vertices = createVertexBuffer(6 * sizeof(ColoredVertex));

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    vector<ColoredVertex> vertexData = {
        { fvec3(), theme.xAxisColor.toVec3() },
        { rightDir * anchorSize, theme.xAxisColor.toVec3() },
        { fvec3(), theme.yAxisColor.toVec3() },
        { forwardDir * anchorSize, theme.yAxisColor.toVec3() },
        { fvec3(), theme.zAxisColor.toVec3() },
        { upDir * anchorSize, theme.zAxisColor.toVec3() }
    };
    memcpy(mapping.data, vertexData.data(), vertexData.size() * sizeof(ColoredVertex));

    createComponents(swapchainElements);
}

RenderMarker::~RenderMarker()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderMarker::work(const FlatSwapchainElement* swapchainElement, const vec3& cameraPosition) const
{
    RenderFlatDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        6
    );
}

VmaBuffer RenderMarker::createUniformBuffer() const
{
    return RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount);
}
