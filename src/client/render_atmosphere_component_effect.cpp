/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_atmosphere_component_effect.hpp"
#include "render_tools.hpp"
#include "render_atmosphere.hpp"
#include "render_descriptor_set_write_components.hpp"

static f64 areaRadius(const AtmosphereEffect& effect)
{
    return atmosphereEffectMinAreaRadius * effect.density + atmosphereEffectMaxAreaRadius * (1 - effect.density);
}

RenderAtmosphereComponentEffect::RenderAtmosphereComponentEffect(
    const RenderInterface* render,
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
)
    : type(Atmosphere_Effect_Line)
    , render(render)
    , initialized(false)
{
    uniform = createUniformBuffer(sizeof(AtmosphereEffectGPU));
    uniformMapping = new VmaMapping<AtmosphereEffectGPU>(render->context()->allocator, uniform);

    particles = createStorageBuffer(atmosphereEffectParticleCount * sizeof(AtmosphereEffectParticleGPU));
    particlesMapping = new VmaMapping<AtmosphereEffectParticleGPU>(render->context()->allocator, particles);

    rotations = createStorageBuffer(atmosphereEffectRotationCount * sizeof(fvec4));
    rotationsMapping = new VmaMapping<fvec4>(render->context()->allocator, rotations);

    clouds = createStorageBuffer(atmosphereEffectCloudVarietyCount * atmosphereEffectCloudSubParticleCount * sizeof(fvec4));
    cloudsMapping = new VmaMapping<fvec4>(render->context()->allocator, clouds);

    descriptorSet = createDescriptorSet(descriptorSetLayout, camera, environmentImageView, occlusionScene);
}

RenderAtmosphereComponentEffect::~RenderAtmosphereComponentEffect()
{
    destroyDescriptorSet(render->context()->device, render->context()->descriptorPool, descriptorSet);

    delete cloudsMapping;
    destroyBuffer(render->context()->allocator, clouds);

    delete rotationsMapping;
    destroyBuffer(render->context()->allocator, rotations);

    delete particlesMapping;
    destroyBuffer(render->context()->allocator, particles);

    delete uniformMapping;
    destroyBuffer(render->context()->allocator, uniform);
}

void RenderAtmosphereComponentEffect::initParticles(
    const AtmosphereEffect& effect,
    mt19937_64& engine,
    const vec3& gravity,
    f64 density
)
{
    uniform_real_distribution<f64> distribution(-1, +1);

    u32 count = 0;
    while (count != atmosphereEffectParticleCount)
    {
        vec3 position;

        if (type == Atmosphere_Effect_Streamer)
        {
            vec2 flatPosition(
                distribution(engine),
                distribution(engine)
            );

            if (roughly(flatPosition.length(), 0.0) || flatPosition.length() > 1)
            {
                continue;
            }

            position = vec3(
                flatPosition * atmosphereEffectStreamerDisplayRadius,
                distribution(engine) * atmosphereEffectStreamerDisplayRadius
            );
        }
        else
        {
            position = vec3(
                distribution(engine),
                distribution(engine),
                distribution(engine)
            );

            if (roughly(position.length(), 0.0) || position.length() > 1)
            {
                continue;
            }

            position *= areaRadius(effect);
        }

        vec3 linearVelocity;

        if (!roughly(density, 0.0) || !roughly(effect.radius, 0.0))
        {
            linearVelocity = gravity.normalize()
                * sqrt((gravity.length() * effect.mass) / (0.5 * density * particleDragCoefficient * pi * sq(effect.radius)));
        }

        AtmosphereEffectParticleGPU& particle = particlesMapping->data[count];
        particle.position = fvec4(position, 0);
        particle.linearVelocity = fvec4(linearVelocity, 0);
        particle.rotation = distribution(engine) * pi;
        particle.timer = render->timer();

        count++;
    }

    for (u32 i = 0; i < atmosphereEffectRotationCount; i++)
    {
        auto [ axis, angle ] = randomRotation(engine);

        // Supplied as axis-angle so GPU can apply time interval before rotating
        rotationsMapping->data[i] = fvec4(axis, angle);
    }

    for (u32 i = 0; i < atmosphereEffectCloudVarietyCount * atmosphereEffectCloudSubParticleCount; i++)
    {
        auto [ offset, rotation ] = randomSubParticle(engine);

        cloudsMapping->data[i] = fvec4(offset * atmosphereEffectCloudRadius, rotation);
    }

    initialized = true;
}

void RenderAtmosphereComponentEffect::copyParticles(VkCommandBuffer commandBuffer, const RenderAtmosphereComponentEffect* lastEffect) const
{
    {
        barrierBuffer(
            commandBuffer,
            lastEffect->particles.buffer,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_ACCESS_SHADER_WRITE_BIT,
            VK_ACCESS_TRANSFER_READ_BIT
        );

        VkBufferCopy region{};
        region.srcOffset = 0;
        region.dstOffset = 0;
        region.size = atmosphereEffectParticleCount * sizeof(AtmosphereEffectParticleGPU);

        vkCmdCopyBuffer(
            commandBuffer,
            lastEffect->particles.buffer,
            particles.buffer,
            1,
            &region
        );

        barrierBuffer(
            commandBuffer,
            particles.buffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            VK_ACCESS_TRANSFER_WRITE_BIT,
            VK_ACCESS_SHADER_READ_BIT
        );
    }

    if (!initialized)
    {
        barrierBuffer(
            commandBuffer,
            lastEffect->rotations.buffer,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_ACCESS_SHADER_WRITE_BIT,
            VK_ACCESS_TRANSFER_READ_BIT
        );

        VkBufferCopy region{};
        region.srcOffset = 0;
        region.dstOffset = 0;
        region.size = atmosphereEffectRotationCount * sizeof(fvec4);

        vkCmdCopyBuffer(
            commandBuffer,
            lastEffect->particles.buffer,
            particles.buffer,
            1,
            &region
        );

        barrierBuffer(
            commandBuffer,
            rotations.buffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            VK_ACCESS_TRANSFER_WRITE_BIT,
            VK_ACCESS_SHADER_READ_BIT
        );
    }

    if (!initialized)
    {
        barrierBuffer(
            commandBuffer,
            lastEffect->clouds.buffer,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_ACCESS_SHADER_WRITE_BIT,
            VK_ACCESS_TRANSFER_READ_BIT
        );

        VkBufferCopy region{};
        region.srcOffset = 0;
        region.dstOffset = 0;
        region.size = atmosphereEffectCloudVarietyCount * atmosphereEffectCloudSubParticleCount * sizeof(fvec4);

        vkCmdCopyBuffer(
            commandBuffer,
            lastEffect->clouds.buffer,
            clouds.buffer,
            1,
            &region
        );

        barrierBuffer(
            commandBuffer,
            clouds.buffer,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_VERTEX_SHADER_BIT,
            VK_ACCESS_TRANSFER_WRITE_BIT,
            VK_ACCESS_SHADER_READ_BIT
        );
    }

    initialized = true;
}

void RenderAtmosphereComponentEffect::fillUniformBuffer(
    const AtmosphereEffect& effect,
    vec3 cameraPosition[eyeCount],
    const vec3& gravity,
    const vec3& flowVelocity,
    f64 density
)
{
    type = effect.type;

    vec3 averageCameraPosition = (cameraPosition[0] + cameraPosition[1]) / 2;

    auto uniform = new AtmosphereEffectGPU();

    uniform->model[0] = cameraLocalTransform(cameraPosition[0], averageCameraPosition);
    uniform->model[1] = cameraLocalTransform(cameraPosition[1], averageCameraPosition);
    uniform->gravity = fvec4(gravity, 0);
    uniform->flowVelocity = fvec4(type == Atmosphere_Effect_Warp ? effect.flowVelocity : flowVelocity, 0);
    uniform->color = effect.color.toVec4();
    uniform->density = density;
    uniform->size = effect.size;
    uniform->radius = effect.radius;
    uniform->mass = effect.mass;

    vector<RenderLight> lights = render->getLights(averageCameraPosition);
    uniform->lightCount = lights.size();

    for (size_t i = 0; i < lights.size(); i++)
    {
        uniform->lights[i] = lights.at(i).uniformData(cameraPosition);
    }

    uniform->fogDistance = render->fogDistance();
    uniform->timer = render->timer();

    if (type == Atmosphere_Effect_Streamer)
    {
        uniform->areaRadius = atmosphereEffectStreamerDisplayRadius;
    }
    else
    {
        uniform->areaRadius = areaRadius(effect);
    }

    vec3 viewerPosition = (cameraPosition[0] + cameraPosition[1]) / 2;
    uniform->viewerPositionDelta = lastViewerPosition
        ? fvec4(viewerPosition - *lastViewerPosition, 0)
        : fvec4();
    lastViewerPosition = viewerPosition;

    memcpy(uniformMapping->data, uniform, sizeof(AtmosphereEffectGPU));

    delete uniform;
}

void RenderAtmosphereComponentEffect::fillCommandBuffer(
    VkCommandBuffer commandBuffer,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
) const
{
    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &descriptorSet,
        0,
        nullptr
    );

    u32 instanceCount = type == Atmosphere_Effect_Streamer
        ? atmosphereEffectStreamerCount
        : atmosphereEffectParticleCount;

    vkCmdDraw(commandBuffer, 1, instanceCount, 0, 0);
}

VmaBuffer RenderAtmosphereComponentEffect::createUniformBuffer(size_t size) const
{
    return createBuffer(
        render->context()->allocator,
        size,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VmaBuffer RenderAtmosphereComponentEffect::createStorageBuffer(size_t size) const
{
    return createBuffer(
        render->context()->allocator,
        size,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT | VK_BUFFER_USAGE_TRANSFER_SRC_BIT | VK_BUFFER_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VkDescriptorSet RenderAtmosphereComponentEffect::createDescriptorSet(
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera,
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene
) const
{
    VkDescriptorSet descriptorSet = ::createDescriptorSet(
        render->context()->device,
        render->context()->descriptorPool,
        descriptorSetLayout
    );

    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, uniform.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo particlesWriteBuffer{};
    VkWriteDescriptorSet particlesWrite{};

    storageBufferDescriptorSetWrite(descriptorSet, 2, particles.buffer, &particlesWriteBuffer, &particlesWrite);

    VkDescriptorBufferInfo rotationsWriteBuffer{};
    VkWriteDescriptorSet rotationsWrite{};

    storageBufferDescriptorSetWrite(descriptorSet, 3, rotations.buffer, &rotationsWriteBuffer, &rotationsWrite);

    VkDescriptorBufferInfo cloudsWriteBuffer{};
    VkWriteDescriptorSet cloudsWrite{};

    storageBufferDescriptorSetWrite(descriptorSet, 4, clouds.buffer, &cloudsWriteBuffer, &cloudsWrite);

    VkDescriptorImageInfo environmentWriteImage{};
    VkWriteDescriptorSet environmentWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 5, environmentImageView, render->context()->unfilteredSampler, &environmentWriteImage, &environmentWrite);

    VkAccelerationStructureKHR accelerationStructures[eyeCount];
    VkWriteDescriptorSetAccelerationStructureKHR occlusionSceneAccelerationStructure{};
    VkDescriptorBufferInfo occlusionSceneBufferInfo{};
    VkWriteDescriptorSet occlusionSceneWrite{};

    if (occlusionScene.hardwareAccelerated())
    {
        accelerationStructures[0] = occlusionScene.hardwareScene(0);
        accelerationStructures[1] = occlusionScene.hardwareScene(1);

        occlusionSceneAccelerationStructure.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET_ACCELERATION_STRUCTURE_KHR;
        occlusionSceneAccelerationStructure.accelerationStructureCount = eyeCount;
        occlusionSceneAccelerationStructure.pAccelerationStructures = accelerationStructures;

        occlusionSceneWrite.sType = VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET;
        occlusionSceneWrite.pNext = &occlusionSceneAccelerationStructure;
        occlusionSceneWrite.dstSet = descriptorSet;
        occlusionSceneWrite.dstBinding = 6;
        occlusionSceneWrite.dstArrayElement = 0;
        occlusionSceneWrite.descriptorCount = eyeCount;
        occlusionSceneWrite.descriptorType = VK_DESCRIPTOR_TYPE_ACCELERATION_STRUCTURE_KHR;
    }
    else
    {
        uniformBufferDescriptorSetWrite(descriptorSet, 6, occlusionScene.softwareScene(), &occlusionSceneBufferInfo, &occlusionSceneWrite);
    }

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite,
        particlesWrite,
        rotationsWrite,
        cloudsWrite,
        environmentWrite,
        occlusionSceneWrite
    };

    vkUpdateDescriptorSets(
        render->context()->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    return descriptorSet;
}

mat4 RenderAtmosphereComponentEffect::cameraLocalTransform(const vec3& cameraPosition, const vec3& effectPosition) const
{
    mat4 model;

    model.setPosition(effectPosition - cameraPosition);

    return model;
}

tuple<vec3, f64> RenderAtmosphereComponentEffect::randomRotation(mt19937_64& engine) const
{
    uniform_real_distribution<f64> distribution(-1, +1);

    vec3 axis;

    while (roughly(axis.length(), 0.0) || axis.length() > 1)
    {
        axis = {
            distribution(engine),
            distribution(engine),
            distribution(engine)
        };
    }

    axis = axis.normalize();
    f64 angle = distribution(engine) * pi;

    return { axis, angle };
}

tuple<vec3, f64> RenderAtmosphereComponentEffect::randomSubParticle(mt19937_64& engine) const
{
    uniform_real_distribution<f64> distribution(-1, +1);

    vec3 offset;

    while (roughly(offset.length(), 0.0) || offset.length() > 1)
    {
        offset = {
            distribution(engine),
            distribution(engine),
            distribution(engine)
        };
    }

    f64 rotation = distribution(engine) * pi;

    return { offset, rotation };
}
