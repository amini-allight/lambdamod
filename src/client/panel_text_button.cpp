/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_text_button.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "control_context.hpp"

PanelTextButton::PanelTextButton(
    InterfaceWidget* parent,
    const string& text,
    const function<void()>& onActivate
)
    : TextButton(parent, text, onActivate)
    , text(text)
    , lastButtonTime(0)
{
    START_WIDGET_SCOPE("panel-text-button")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

void PanelTextButton::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        UIScale::panelBorderSize(this),
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    TextStyle style(this);
    style.alignment = Text_Center;
    style.size = UIScale::largeFontSize(this);
    style.weight = Text_Bold;
    style.color = focused() ? theme.accentColor : theme.panelForegroundColor;

    drawText(
        ctx,
        this,
        text,
        style
    );
}

void PanelTextButton::interact(const Input& input)
{
    if (currentTime() - lastButtonTime < minButtonInterval)
    {
        return;
    }

    playPositiveActivateEffect();
    onActivate();

    lastButtonTime = currentTime();
}

SizeProperties PanelTextButton::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::panelTextButtonHeight(this)),
        Scaling_Fill, Scaling_Fixed
    };
}
