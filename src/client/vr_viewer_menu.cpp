/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "vr_viewer_menu.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "vr_render.hpp"
#include "panel_transforms.hpp"
#include "column.hpp"
#include "fixed_row.hpp"
#include "spacer.hpp"
#include "vr_menu_button.hpp"

VRViewerMenu::VRViewerMenu(InterfaceView* view)
    : VRMenu(view)
{
    auto column = new Column(this);

    MAKE_SPACER(column, 0, 0);

    auto row = new FixedRow(column, UIScale::panelVRMenuButtonSize(this).h);

    new VRMenuButton(
        row,
        localize("vr-viewer-menu-disconnect"),
        bind(&VRViewerMenu::onDisconnect, this)
    );

    new VRMenuButton(
        row,
        localize("vr-viewer-menu-quit"),
        bind(&VRViewerMenu::onQuit, this)
    );

    MAKE_SPACER(column, 0, 0);
}

bool VRViewerMenu::shouldDraw() const
{
    return VRMenu::shouldDraw() && !context()->attached() && context()->vrMenuButtonHandler()->vrMenu();
}

void VRViewerMenu::draw(const DrawContext& ctx) const
{
    VRMenu::draw(ctx);

    drawPointer(ctx, nullptr, leftPointer);
    drawPointer(ctx, nullptr, rightPointer);
}

void VRViewerMenu::onDisconnect()
{
    context()->vrMenuButtonHandler()->closeVRMenu();
    context()->controller()->disconnect();
}

void VRViewerMenu::onQuit()
{
    context()->vrMenuButtonHandler()->closeVRMenu();
    context()->controller()->quit();
}
#endif
