/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "color_indicator_bar.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"

ColorIndicatorBar::ColorIndicatorBar(
    InterfaceWidget* parent,
    const function<Color()>& colorSource,
    const function<string()>& textSource
)
    : InterfaceWidget(parent)
    , colorSource(colorSource)
    , textSource(textSource)
{

}

void ColorIndicatorBar::draw(const DrawContext& ctx) const
{
    drawSolid(ctx, this, colorSource());

    TextStyle style;
    style.alignment = Text_Center;
    style.weight = Text_Bold;

    drawText(ctx, this, textSource(), style);
}

SizeProperties ColorIndicatorBar::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::colorIndicatorBarHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
