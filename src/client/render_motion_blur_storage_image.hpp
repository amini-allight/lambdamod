#pragma once

#include "types.hpp"
#include "render_types.hpp"
#include "render_context.hpp"

class RenderMotionBlurStorageImage
{
public:
    RenderMotionBlurStorageImage(const RenderContext* ctx, u32 width, u32 height, u32 layers);
    RenderMotionBlurStorageImage(const RenderMotionBlurStorageImage& rhs) = delete;
    RenderMotionBlurStorageImage(RenderMotionBlurStorageImage&& rhs) = delete;
    ~RenderMotionBlurStorageImage();

    RenderMotionBlurStorageImage& operator=(const RenderMotionBlurStorageImage& rhs) = delete;
    RenderMotionBlurStorageImage& operator=(RenderMotionBlurStorageImage&& rhs) = delete;

    void setupLayout(VkCommandBuffer commandBuffer);
    void barrier(VkCommandBuffer commandBuffer);

    VkImageView imageView;

private:
    const RenderContext* ctx;
    VmaImage image;
    u32 layers;
    bool layoutReady;
};
