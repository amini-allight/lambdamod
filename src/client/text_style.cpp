/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text_style.hpp"
#include "theme.hpp"
#include "interface_constants.hpp"
#include "log.hpp"
#include "font_store.hpp"

TextStyle::TextStyle()
{
    color = theme.textColor;
    font = Text_Monospace;
    alignment = Text_Left;
    weight = Text_Regular;
    size = UIScale::smallFontSize();
}

TextStyle::TextStyle(const InterfaceWidget* widget)
{
    color = theme.panelForegroundColor;
    font = Text_Sans_Serif;
    alignment = Text_Left;
    weight = Text_Semi_Bold;
    size = UIScale::largeFontSize(widget);
}

TextStyle::TextStyle(
    Color color,
    TextFont font,
    TextAlignment alignment,
    TextWeight weight,
    u32 size
)
    : color(color)
    , font(font)
    , alignment(alignment)
    , weight(weight)
    , size(size)
{

}

FontFace* TextStyle::getFont() const
{
    return fontStore->get(font, weight, size);
}

bool TextStyle::operator==(const TextStyle& rhs) const
{
    return color == rhs.color &&
        font == rhs.font &&
        alignment == rhs.alignment &&
        weight == rhs.weight &&
        size == rhs.size;
}

bool TextStyle::operator!=(const TextStyle& rhs) const
{
    return !(*this == rhs);
}

TextStyleOverride::TextStyleOverride()
{

}

TextStyleOverride::TextStyleOverride(
    optional<Color*> color,
    optional<TextFont> font,
    optional<TextAlignment> alignment,
    optional<TextWeight> weight,
    optional<u32> size
)
    : color(color)
    , font(font)
    , alignment(alignment)
    , weight(weight)
    , size(size)
{

}

TextStyle TextStyleOverride::apply(TextStyle style) const
{
    if (color)
    {
        style.color = **color;
    }

    if (font)
    {
        style.font = *font;
    }

    if (alignment)
    {
        style.alignment = *alignment;
    }

    if (weight)
    {
        style.weight = *weight;
    }

    if (size)
    {
        style.size = *size;
    }

    return style;
}

bool TextStyleOverride::operator==(const TextStyleOverride& rhs) const
{
    return color == rhs.color &&
        font == rhs.font &&
        alignment == rhs.alignment &&
        weight == rhs.weight &&
        size == rhs.size;
}

bool TextStyleOverride::operator!=(const TextStyleOverride& rhs) const
{
    return !(*this == rhs);
}
