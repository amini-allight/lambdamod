/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class ListView;

class UserControlsStatus : public InterfaceWidget, public TabContent
{
public:
    UserControlsStatus(InterfaceWidget* parent);

    void open() override;
    void close() override;

private:
    ListView* listView;

    string userName;
    bool initialized;

    vector<string> usersSource();
    void onSelect(const string& name);

    string tetherSource();
    string confinementSource();
    string fadeSource();
    string waitSource();

    SizeProperties sizeProperties() const override;
};
