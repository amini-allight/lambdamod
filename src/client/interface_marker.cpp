#include "interface_marker.hpp"

InterfaceMarker::InterfaceMarker()
{
    type = Icon_Ping;
}

InterfaceMarker::InterfaceMarker(
    Icon type,
    string name,
    vec3 position,
    vec3 color
)
    : InterfaceMarker()
{
    this->type = type;
    this->name = name;
    this->position = position;
    this->color = color;
}
