/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "color_picker.hpp"
#include "control_context.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_window.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "line_edit.hpp"
#include "color_saturation_value_picker.hpp"
#include "color_hue_picker.hpp"
#include "tools.hpp"

ColorPickerPopup::ColorPickerPopup(
    InterfaceWidget* parent,
    const vec3& color,
    const function<void(const vec3&)>& onSet
)
    : Popup(parent)
    , color(color)
    , onSet(onSet)
{
    auto column = new Column(this);

    auto row = new Row(column);

    new ColorSaturationValuePicker(
        row,
        bind(&ColorPickerPopup::hueSource, this),
        bind(&ColorPickerPopup::onSaturationValueSet, this, placeholders::_1)
    );

    MAKE_SPACER(row, UIScale::marginSize(), 0);

    new ColorHuePicker(
        row,
        bind(&ColorPickerPopup::hueSource, this),
        bind(&ColorPickerPopup::onHueSet, this, placeholders::_1)
    );

    MAKE_SPACER(column, 0, UIScale::marginSize());

    auto text = new LineEdit(
        column,
        bind(&ColorPickerPopup::textSource, this),
        bind(&ColorPickerPopup::onTextReturn, this, placeholders::_1)
    );
    text->setPlaceholder(localize("color-picker-hex-color"));
}

void ColorPickerPopup::move(const Point& position)
{
    this->position(position);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + UIScale::marginSize());
    }
}

void ColorPickerPopup::resize(const Size& size)
{
    this->size(size);

    for (InterfaceWidget* child : children())
    {
        child->move(this->position() + UIScale::marginSize());
        child->resize(this->size() - (UIScale::marginSize() * 2));
    }
}

void ColorPickerPopup::draw(const DrawContext& ctx) const
{
    ctx.delayedCommands.push_back([this](const DrawContext& ctx) -> void {
        drawSolid(
            ctx,
            this,
            theme.indentColor
        );
    });

    for (InterfaceWidget* child : children())
    {
        ctx.delayedCommands.push_back([child](const DrawContext& ctx) -> void {
            child->draw(ctx);
        });
    }
}

f64 ColorPickerPopup::hueSource()
{
    return rgbToHSV(color).x;
}

void ColorPickerPopup::onHueSet(f64 hue)
{
    vec3 hsv = rgbToHSV(color);

    hsv.x = hue;

    if (roughly<f64>(hsv.y, 0))
    {
        hsv.y = 1;
    }

    if (roughly<f64>(hsv.z, 0))
    {
        hsv.z = 1;
    }

    onSet(hsvToRGB(hsv));
}

void ColorPickerPopup::onSaturationValueSet(const vec2& saturationValue)
{
    vec3 hsv = rgbToHSV(color);

    hsv.y = saturationValue.x;
    hsv.z = saturationValue.y;

    onSet(hsvToRGB(hsv));
}

string ColorPickerPopup::textSource()
{
    return Color(color).toString(false);
}

void ColorPickerPopup::onTextReturn(const string& text)
{
    Color color(text);
    color.a = 255;

    onSet(color.toVec3());
}

SizeProperties ColorPickerPopup::sizeProperties() const
{
    return sizePropertiesFillAll;
}

ColorPicker::ColorPicker(
    InterfaceWidget* parent,
    const function<vec3()>& source,
    const function<void(const vec3&)>& onSet
)
    : InterfaceWidget(parent)
    , color(source())
    , source(source)
    , _onSet(onSet)
{
    START_WIDGET_SCOPE("color-picker")
        WIDGET_SLOT("interact", interact)
    END_SCOPE

    popup = new ColorPickerPopup(
        this,
        color,
        bind(&ColorPicker::onSet, this, placeholders::_1)
    );
}

ColorPicker::ColorPicker(
    InterfaceWidget* parent,
    const function<Color()>& source,
    const function<void(const Color&)>& onSet
)
    : ColorPicker(
        parent,
        [source]() -> vec3 {
            return source().toVec3();
        },
        [onSet](const vec3& color) -> void {
            onSet(Color(color));
        }
    )
{

}

ColorPicker::~ColorPicker()
{
    delete popup;
}

void ColorPicker::move(const Point& position)
{
    this->position(position);

    popup->move(position + Point(0, UIScale::colorPickerHeight()));
}

void ColorPicker::resize(const Size& size)
{
    this->size(size);

    popup->resize(Size(this->size().w, UIScale::colorPickerPopupHeight()));
}

void ColorPicker::step()
{
    InterfaceWidget::step();

    vec3 newColor = source();

    if (newColor != color)
    {
        color = newColor;
    }

    if (!focused() && !popup->focused() && popup->opened())
    {
        popup->close();
    }
}

void ColorPicker::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        Color(color)
    );

    drawBorder(
        ctx,
        this,
        UIScale::marginSize(),
        focused() ? theme.accentColor : theme.outdentColor
    );

    InterfaceWidget::draw(ctx);
}

void ColorPicker::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void ColorPicker::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

void ColorPicker::onSet(const vec3& color)
{
    this->color = color;

    _onSet(color);
}

void ColorPicker::interact(const Input& input)
{
    if (popup->opened())
    {
        popup->close();
        playNegativeActivateEffect();
    }
    else
    {
        popup->open();
        playPositiveActivateEffect();
    }

    if (popup->opened())
    {
        update();
    }
}

SizeProperties ColorPicker::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::colorPickerHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
