/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class VRSettings : public InterfaceWidget, public TabContent
{
public:
    VRSettings(InterfaceWidget* parent);

private:
    vector<string> vrMotionModeOptionsSource();
    vector<string> vrMotionModeOptionTooltipsSource();
    size_t vrMotionModeSource();
    void onVRMotionMode(size_t index);

    bool vrSmoothTurningSource();
    void onVRSmoothTurning(bool state);

    f64 vrSmoothTurnSpeedSource();
    void onVRSmoothTurnSpeed(f64 speed);

    f64 vrSnapTurnIncrementSource();
    void onVRSnapTurnIncrement(f64 increment);

    bool vrKeyboardRightHandSource();
    void onVRKeyboardRightHand(bool state);

    bool vrRollCageSource();
    void onVRRollCage(bool state);

    vec2 vrLeftDeadZoneSource();
    void onVRLeftDeadZone(const vec2& zone);

    vec2 vrRightDeadZoneSource();
    void onVRRightDeadZone(const vec2& zone);

    SizeProperties sizeProperties() const override;
};
