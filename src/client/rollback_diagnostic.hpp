/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_DEBUG
#pragma once

#include "types.hpp"
#include "step_frame.hpp"

class Controller;

#pragma pack(1)
struct RollbackDiagnosticEntry
{
    i64 index;
    f64 value;
    char text[16];
};
#pragma pack()

class RollbackDiagnostic
{
public:
    RollbackDiagnostic(Controller* controller);
    RollbackDiagnostic(const RollbackDiagnostic& rhs) = delete;
    RollbackDiagnostic(RollbackDiagnostic&& rhs) = delete;
    ~RollbackDiagnostic();

    RollbackDiagnostic& operator=(const RollbackDiagnostic& rhs) = delete;
    RollbackDiagnostic& operator=(RollbackDiagnostic&& rhs) = delete;

    void record(i64 index, const StepFrame& frame, const string& text);

    f64 value(const StepFrame& frame) const;

private:
    Controller* controller;
    vector<RollbackDiagnosticEntry> entries;
};
#endif
