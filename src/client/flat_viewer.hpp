/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_types.hpp"

class ControlContext;
class FlatRender;

enum FlatViewerMode : u8
{
    Flat_Viewer_Free,
    Flat_Viewer_Perspective,
    Flat_Viewer_Orthographic
};

class FlatViewer
{
public:
    FlatViewer(ControlContext* ctx);
    FlatViewer(const FlatViewer& rhs) = delete;
    FlatViewer(FlatViewer&& rhs) = delete;
    ~FlatViewer();

    FlatViewer& operator=(const FlatViewer& rhs) = delete;
    FlatViewer& operator=(FlatViewer&& rhs) = delete;

    void focus();
    void unfocus();
    void resize(f64 width, f64 height);

    optional<Point> worldToScreen(const vec3& world) const;
    vec3 screenToWorld(const Point& screen) const;

    void move(const vec3& position, const quaternion& rotation);

    void step();
    void motion(const vec3& motion);

    FlatViewerMode mode() const;

    vec3 position() const;
    quaternion rotation() const;

    void shift(bool state);
    bool shift() const;
    void zoom(bool state);
    bool zoom() const;
    void rotate(bool state);
    bool rotate() const;
    void sprint(bool state);
    bool sprint() const;
    void forward(bool state);
    bool forward() const;
    void back(bool state);
    bool back() const;
    void left(bool state);
    bool left() const;
    void right(bool state);
    bool right() const;
    void up(bool state);
    bool up() const;
    void down(bool state);
    bool down() const;

    void moveStickMotion(const vec3& position);
    void lookStickMotion(const vec3& position);

    void freeLook();
    void perspLook(const vec3& pos);
    void orthoLook(const vec3& pos, const vec3& dir);
    void orthoToggle();
    void rotateUp();
    void rotateDown();
    void rotateLeft();
    void rotateRight();
    void invert();
    void zoomIn();
    void zoomOut();

    void updatePerspectiveGrid();

private:
    ControlContext* ctx;

    f64 width;
    f64 height;

    mat4 projection;
    mat4 view;

    FlatViewerMode _mode;
    struct {
        vec3 position;
        f64 x;
        f64 z;
    } fly;
    struct {
        vec3 position;
        f64 x;
        f64 z;
        f64 zoom;
    } perspective;
    struct {
        vec3 position;
        vec3 direction;
        vec3 upDirection;
        f64 zoom;
    } orthographic;
    vec3 lastPosition;
    quaternion lastRotation;
    bool shifting;
    bool zooming;
    bool rotating;
    bool sprinting;
    bool movements[6];
    vec3 _moveStickMotion;
    vec3 _lookStickMotion;

    RenderDecorationID perspectiveGridID;
    RenderDecorationID orthographicGridID;

    void updateViewer();
    void updateMatrices();

    f64 perspectiveDistance() const;
    f64 orthographicDistance() const;

    FlatRender* render() const;
};
