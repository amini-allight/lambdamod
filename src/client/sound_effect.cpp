/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_effect.hpp"
#include "sound_tools.hpp"
#include "log.hpp"

static constexpr size_t wavHeaderSize = 44;

SoundEffect::SoundEffect(u8* data, size_t size, u32 channels)
{
    vector<i16> discrete(size / sizeof(i16));
    memcpy(discrete.data(), data, size);

    vector<f32> continuous = convertSamplesToContinuous(discrete);

    _channels = vector<vector<f32>>(channels, vector<f32>(continuous.size() / channels));

    for (size_t i = 0; i < _channels.size(); i++)
    {
        vector<f32>& channel = _channels.at(i);

        for (size_t j = 0; j < channel.size(); j++)
        {
            channel[j] = continuous[j * channels + i];
        }
    }
}

SoundEffect::~SoundEffect()
{

}

const vector<vector<f32>>& SoundEffect::channels() const
{
    return _channels;
}

size_t SoundEffect::length() const
{
    return _channels.front().size();
}

SoundEffect* loadWAV(const string& path)
{
    FILE* file = fopen(path.c_str(), "rb");

    if (!file)
    {
        error("Failed to open WAV file '" + path + "'.");
        return nullptr;
    }

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, SEEK_SET);

    if (size <= wavHeaderSize)
    {
        error("Encountered undersized WAV file '" + path + "'.");
        fclose(file);
        return nullptr;
    }

    size -= wavHeaderSize;

    fseek(file, 22, SEEK_SET);
    i16 channels;
    size_t result = fread(&channels, sizeof(i16), 1, file);
    if (result != 1)
    {
        error("Failed to read from WAV file '" + path + "'.");
        fclose(file);
        return nullptr;
    }
    fseek(file, 0, SEEK_SET);

    u8* data = new u8[size];

    fseek(file, wavHeaderSize, SEEK_SET);
    result = fread(data, sizeof(u8), size, file);

    if (result != size)
    {
        error("Failed to read from WAV file '" + path + "'.");
        fclose(file);
        return nullptr;
    }

    fclose(file);

    if (channels == 0)
    {
        fatal("Encountered WAV file '" + path + "' with invalid channel count '0'.");
    }

    return new SoundEffect(data, size, channels);
}
