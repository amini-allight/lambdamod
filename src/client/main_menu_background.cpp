/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "main_menu_background.hpp"
#include "theme.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "paths.hpp"
#include "tools.hpp"
#include "controller.hpp"
#include "render_tools.hpp"
#include "render_objects.hpp"
#include "render_pipeline_components.hpp"
#include "render_descriptor_set_components.hpp"
#include "render_descriptor_set_write_components.hpp"
#include "flat_render.hpp"

static constexpr VkSampleCountFlagBits sampleCount = VK_SAMPLE_COUNT_4_BIT;
static constexpr fvec3 menuPatternOffset = fvec3(0.5, 0, 0);
static constexpr f32 squareRotationPeriod = 120;
static constexpr f32 dashedCircleRotationPeriod = 240;
static constexpr f32 squareSize = 0.25;
static constexpr u32 circleSteps = 128;
static constexpr u32 dashedCircleSteps = 512;
static constexpr f32 outerCircleRadius = 0.5;
static constexpr f32 innerCircleRadius = 0.2;
static constexpr f32 thickLineWidth = 3;
static constexpr f32 thinLineWidth = 1.5;

MainMenuBackgroundFrame::MainMenuBackgroundFrame(
    VkDevice device,
    VmaAllocator allocator,
    VkDescriptorPool descriptorPool,
    VkRenderPass renderPass,
    VkDescriptorSetLayout descriptorSetLayout,
    VkSampler sampler,
    u32 width,
    u32 height,
    u32 vertexCount
)
    : device(device)
    , allocator(allocator)
    , descriptorPool(descriptorPool)
{
    image = createImage(
        allocator,
        { width, height, 1 },
        textureFormat,
        sampleCount,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY,
        false
    );

    imageView = createImageView(
        device,
        image.image,
        textureFormat,
        VK_IMAGE_ASPECT_COLOR_BIT
    );

    resolveImage = createImage(
        allocator,
        { width, height, 1 },
        textureFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT | VK_IMAGE_USAGE_SAMPLED_BIT,
        VMA_MEMORY_USAGE_GPU_ONLY,
        false
    );

    resolveImageView = createImageView(
        device,
        resolveImage.image,
        textureFormat,
        VK_IMAGE_ASPECT_COLOR_BIT
    );

    VkImageView imageViews[] = {
        imageView,
        resolveImageView
    };

    VkFramebufferCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
    createInfo.flags = 0;
    createInfo.renderPass = renderPass;
    createInfo.attachmentCount = sizeof(imageViews) / sizeof(VkImageView);
    createInfo.pAttachments = imageViews;
    createInfo.width = width;
    createInfo.height = height;
    createInfo.layers = 1;

    VkResult result = vkCreateFramebuffer(device, &createInfo, nullptr, &framebuffer);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create framebuffer: " + vulkanError(result));
    }

    vertices = createBuffer(
        allocator,
        vertexCount * sizeof(fvec3),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VkDescriptorSetAllocateInfo allocateInfo{};
    allocateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO;
    allocateInfo.descriptorPool = descriptorPool;
    allocateInfo.descriptorSetCount = 1;
    allocateInfo.pSetLayouts = &descriptorSetLayout;

    result = vkAllocateDescriptorSets(
        device,
        &allocateInfo,
        &descriptorSet
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to allocate descriptor set: " + vulkanError(result));
    }

    VkDescriptorImageInfo descriptorImageInfo{};
    VkWriteDescriptorSet descriptorWrite{};

    combinedSamplerDescriptorSetWrite(
        descriptorSet,
        0,
        resolveImageView,
        sampler,
        &descriptorImageInfo,
        &descriptorWrite
    );

    VkWriteDescriptorSet descriptorWrites[] = {
        descriptorWrite
    };

    vkUpdateDescriptorSets(
        device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

MainMenuBackgroundFrame::~MainMenuBackgroundFrame()
{
    vkFreeDescriptorSets(device, descriptorPool, 1, &descriptorSet);
    destroyBuffer(allocator, vertices);
    vkDestroyFramebuffer(device, framebuffer, nullptr);
    destroyImageView(device, resolveImageView);
    destroyImage(allocator, resolveImage);
    destroyImageView(device, imageView);
    destroyImage(allocator, image);
}

void MainMenuBackgroundFrame::setVertices(const vector<fvec3>& vertices)
{
    VmaMapping<fvec3> mapping(allocator, this->vertices);

    memcpy(mapping.data, vertices.data(), vertices.size() * sizeof(fvec3));
}

MainMenuBackground::MainMenuBackground(Controller* controller, const Size& size)
    : controller(controller)
    , device(controller->render()->context()->device)
    , allocator(controller->render()->context()->allocator)
    , thickLineOffset(0)
    , thickLineVertexCount(0)
    , thinLineOffset(0)
    , thinLineVertexCount(0)
    , startTime(currentTime())
{
    VkResult result;

    {
        auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
            0,
            textureFormat,
            sampleCount,
            VK_ATTACHMENT_LOAD_OP_CLEAR,
            VK_ATTACHMENT_STORE_OP_DONT_CARE,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
        );

        auto [ colorResolveAttachment, colorResolveAttachmentRef ] = renderPassAttachment(
            1,
            textureFormat,
            VK_SAMPLE_COUNT_1_BIT,
            VK_ATTACHMENT_LOAD_OP_DONT_CARE,
            VK_ATTACHMENT_STORE_OP_STORE,
            VK_IMAGE_LAYOUT_UNDEFINED,
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
        );

        auto [ beforeDependency, afterDependency ] = renderPassDependencies(
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_ACCESS_NONE,
            VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
            VK_ACCESS_NONE,
            VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
            VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            VK_ACCESS_SHADER_READ_BIT
        );

        VkAttachmentDescription2 attachments[] = {
            colorAttachment,
            colorResolveAttachment
        };

        VkSubpassDescription2 subpass = renderPassSubpass();
        subpass.colorAttachmentCount = 1;
        subpass.pColorAttachments = &colorAttachmentRef;
        subpass.pResolveAttachments = &colorResolveAttachmentRef;

        VkSubpassDependency2 dependencies[] = {
            beforeDependency,
            afterDependency
        };

        VkRenderPassCreateInfo2 createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
        createInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
        createInfo.pAttachments = attachments;
        createInfo.subpassCount = 1;
        createInfo.pSubpasses = &subpass;
        createInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
        createInfo.pDependencies = dependencies;

        auto f = VK_API_VERSION_MAJOR(controller->render()->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(controller->render()->context()->apiVersion) > 1
            ? vkCreateRenderPass2
            : GET_VK_EXTENSION_FUNCTION(controller->render()->context()->instance, vkCreateRenderPass2KHR);

        result = f(
            device,
            &createInfo,
            nullptr,
            &renderPass
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create render pass: " + vulkanError(result));
        }
    }

    createShader(device, getFile(shaderPath + "/menu_pattern_vertex" + shaderExt), vertexShader);

    createShader(device, getFile(shaderPath + "/menu_pattern_fragment" + shaderExt), fragmentShader);

    {
        VkPushConstantRange pushConstantRange{};
        pushConstantRange.stageFlags = VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT;
        pushConstantRange.offset = 0;
        pushConstantRange.size = sizeof(f32) * 5;

        VkPipelineLayoutCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
        createInfo.pushConstantRangeCount = 1;
        createInfo.pPushConstantRanges = &pushConstantRange;

        result = vkCreatePipelineLayout(
            device,
            &createInfo,
            nullptr,
            &pipelineLayout
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create pipeline layout: " + vulkanError(result));
        }
    }

    {
        VkPipelineVertexInputStateCreateInfo vertexInputStage = basicVertexInputStage();
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
        VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
        VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
        VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
        VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
        VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
        VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
        VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
        VkPipelineDynamicStateCreateInfo dynamicState = lineWidthDynamicState();

        VkPipelineShaderStageCreateInfo shaderStages[] = {
            vertexShaderStage,
            fragmentShaderStage
        };

        VkGraphicsPipelineCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
        createInfo.pStages = shaderStages;
        createInfo.pVertexInputState = &vertexInputStage;
        createInfo.pInputAssemblyState = &inputAssemblyStage;
        createInfo.pTessellationState = nullptr;
        createInfo.pViewportState = &viewportStage;
        createInfo.pRasterizationState = &rasterizationStage;
        createInfo.pMultisampleState = &multisampleStage;
        createInfo.pDepthStencilState = &depthStencilStage;
        createInfo.pColorBlendState = &colorBlendStage;
        createInfo.pDynamicState = &dynamicState;
        createInfo.layout = pipelineLayout;
        createInfo.renderPass = renderPass;
        createInfo.subpass = 0;
        createInfo.basePipelineHandle = nullptr;
        createInfo.basePipelineIndex = -1;

        result = vkCreateGraphicsPipelines(
            device,
            controller->render()->context()->pipelineCache,
            1,
            &createInfo,
            nullptr,
            &pipeline
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create pipeline: " + vulkanError(result));
        }
    }

    createShader(device, getFile(shaderPath + "/screen" + shaderExt), copyVertexShader);

    createShader(device, getFile(shaderPath + "/copy" + shaderExt), copyFragmentShader);

    {
        VkDescriptorSetLayoutBinding binding = combinedSamplerLayoutBinding(0, VK_SHADER_STAGE_FRAGMENT_BIT);

        VkDescriptorSetLayoutCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
        createInfo.bindingCount = 1;
        createInfo.pBindings = &binding;

        result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &copyDescriptorSetLayout);

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create descriptor set layout: " + vulkanError(result));
        }
    }

    createPipelineLayout(
        device,
        copyDescriptorSetLayout,
        copyPipelineLayout
    );

    {
        VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
        VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
        VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, copyVertexShader);
        VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
        VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
        VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(VK_SAMPLE_COUNT_1_BIT);
        VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
        VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, copyFragmentShader);
        VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
        VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

        VkPipelineShaderStageCreateInfo shaderStages[] = {
            vertexShaderStage,
            fragmentShaderStage
        };

        auto flatRender = dynamic_cast<const FlatRender*>(controller->render());

        VkGraphicsPipelineCreateInfo createInfo{};
        createInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
        createInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
        createInfo.pStages = shaderStages;
        createInfo.pVertexInputState = &vertexInputStage;
        createInfo.pInputAssemblyState = &inputAssemblyStage;
        createInfo.pTessellationState = nullptr;
        createInfo.pViewportState = &viewportStage;
        createInfo.pRasterizationState = &rasterizationStage;
        createInfo.pMultisampleState = &multisampleStage;
        createInfo.pDepthStencilState = &depthStencilStage;
        createInfo.pColorBlendState = &colorBlendStage;
        createInfo.pDynamicState = &dynamicState;
        createInfo.layout = copyPipelineLayout;
        createInfo.renderPass = flatRender ? flatRender->uiRenderPass() : controller->render()->panelRenderPass();
        createInfo.subpass = 0;
        createInfo.basePipelineHandle = nullptr;
        createInfo.basePipelineIndex = -1;

        result = vkCreateGraphicsPipelines(
            device,
            controller->render()->context()->pipelineCache,
            1,
            &createInfo,
            nullptr,
            &copyPipeline
        );

        if (result != VK_SUCCESS)
        {
            fatal("Failed to create pipeline: " + vulkanError(result));
        }
    }

    createFrames(size.w, size.h);
}

MainMenuBackground::~MainMenuBackground()
{
    destroyFrames();

    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
    vkDestroyShaderModule(device, fragmentShader, nullptr);
    vkDestroyShaderModule(device, vertexShader, nullptr);
    vkDestroyPipeline(device, copyPipeline, nullptr);
    vkDestroyPipelineLayout(device, copyPipelineLayout, nullptr);
    vkDestroyDescriptorSetLayout(device, copyDescriptorSetLayout, nullptr);
    vkDestroyShaderModule(device, copyFragmentShader, nullptr);
    vkDestroyShaderModule(device, copyVertexShader, nullptr);
    vkDestroyRenderPass(device, renderPass, nullptr);
}

void MainMenuBackground::resize(const Size& size)
{
    destroyFrames();

    createFrames(size.w, size.h);
}

void MainMenuBackground::step()
{
    vector<fvec3> vertices = generateVertices((currentTime() - startTime).count() / 1000.0f);

    frames.at(controller->render()->currentImage())->setVertices(vertices);
}

void MainMenuBackground::draw(const DrawContext& ctx) const
{
    drawOffscreen(ctx);

    VkViewport viewport;

    if (g_vr)
    {
        viewport = {
            0, 0,
            static_cast<f32>(ctx.size.w), static_cast<f32>(ctx.size.h),
            0, 1
        };
    }
    else
    {
        viewport = {
            0, static_cast<f32>(ctx.size.h),
            static_cast<f32>(ctx.size.w), -static_cast<f32>(ctx.size.h),
            0, 1
        };
    }

    vkCmdSetViewport(ctx.commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { static_cast<u32>(ctx.size.w), static_cast<u32>(ctx.size.h) }
    };

    vkCmdSetScissor(ctx.commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(ctx.commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, copyPipeline);

    vkCmdBindDescriptorSets(
        ctx.commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        copyPipelineLayout,
        0,
        1,
        &frames.at(controller->render()->currentImage())->descriptorSet,
        0,
        nullptr
    );

    vkCmdDraw(ctx.commandBuffer, 6, 1, 0, 0);
}

void MainMenuBackground::drawOffscreen(const DrawContext& ctx) const
{
    VkClearValue clearValue{};
    clearValue.color.float32[0] = 0;
    clearValue.color.float32[1] = 0;
    clearValue.color.float32[2] = 0;
    clearValue.color.float32[3] = 0;

    VkRenderPassBeginInfo beginInfo{};
    beginInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    beginInfo.renderPass = renderPass;
    beginInfo.framebuffer = frames.at(controller->render()->currentImage())->framebuffer;
    beginInfo.renderArea = {
        { 0, 0 },
        { static_cast<u32>(ctx.size.w), static_cast<u32>(ctx.size.h) }
    };
    beginInfo.clearValueCount = 1;
    beginInfo.pClearValues = &clearValue;

    vkCmdBeginRenderPass(ctx.transferCommandBuffer, &beginInfo, VK_SUBPASS_CONTENTS_INLINE);

    VkViewport viewport = {
        0, static_cast<f32>(ctx.size.h),
        static_cast<f32>(ctx.size.w), -static_cast<f32>(ctx.size.h),
        0, 1
    };

    vkCmdSetViewport(ctx.transferCommandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { static_cast<u32>(ctx.size.w), static_cast<u32>(ctx.size.h) }
    };

    vkCmdSetScissor(ctx.transferCommandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(ctx.transferCommandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    f32 pushConstants[] = {
        static_cast<f32>(theme.panelForegroundColor.toVec4().x),
        static_cast<f32>(theme.panelForegroundColor.toVec4().y),
        static_cast<f32>(theme.panelForegroundColor.toVec4().z),
        static_cast<f32>(theme.panelForegroundColor.toVec4().w),
        static_cast<f32>(ctx.size.w) / static_cast<f32>(ctx.size.h)
    };

    vkCmdPushConstants(
        ctx.transferCommandBuffer,
        pipelineLayout,
        VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT,
        0,
        sizeof(pushConstants),
        pushConstants
    );

    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers(ctx.transferCommandBuffer, 0, 1, &frames.at(controller->render()->currentImage())->vertices.buffer, &offset);

    vkCmdSetLineWidth(ctx.transferCommandBuffer, thickLineWidth);
    vkCmdDraw(ctx.transferCommandBuffer, thickLineVertexCount, 1, thickLineOffset, 0);

    vkCmdSetLineWidth(ctx.transferCommandBuffer, thinLineWidth);
    vkCmdDraw(ctx.transferCommandBuffer, thinLineVertexCount, 1, thinLineOffset, 0);

    vkCmdEndRenderPass(ctx.transferCommandBuffer);
}

vector<fvec3> MainMenuBackground::generateVertices(f32 timer)
{
    vector<fvec3> vertices;

    auto addSquare = [&](f32 rotation, f32 size) -> void {
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(1, 1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(1, -1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(1, -1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(-1, -1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(-1, -1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(-1, 1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(-1, 1, 0)));
        vertices.push_back(fquaternion(upDir, rotation).rotate(size * fvec3(1, 1, 0)));
    };

    auto addCircle = [&](f32 radius) -> void {
        for (u32 i = 0; i < circleSteps; i++)
        {
            f32 rotation = (i / static_cast<f32>(circleSteps)) * tau;
            f32 nextRotation = ((i + 1) / static_cast<f32>(circleSteps)) * tau;

            vertices.push_back(fquaternion(upDir, rotation).rotate(radius * fvec3(1, 0, 0)));
            vertices.push_back(fquaternion(upDir, nextRotation).rotate(radius * fvec3(1, 0, 0)));
        }
    };

    auto addDashedCircle = [&](f32 rotation, f32 radius) -> void {
        for (u32 i = 0; i < dashedCircleSteps; i++)
        {
            if (i % 2 != 0)
            {
                continue;
            }

            f32 currRotation = (i / static_cast<f32>(dashedCircleSteps)) * tau;
            f32 nextRotation = ((i + 1) / static_cast<f32>(dashedCircleSteps)) * tau;

            vertices.push_back(fquaternion(upDir, rotation).rotate(fquaternion(upDir, currRotation).rotate(radius * fvec3(1, 0, 0))));
            vertices.push_back(fquaternion(upDir, rotation).rotate(fquaternion(upDir, nextRotation).rotate(radius * fvec3(1, 0, 0))));
        }
    };

    // Thick
    thickLineOffset = vertices.size();

    // Circles
    addCircle(innerCircleRadius);
    addCircle(outerCircleRadius);

    thickLineVertexCount = vertices.size() - thickLineOffset;

    // Thin
    thinLineOffset = vertices.size();

    // Cross lines
    vertices.push_back(fvec3(-2, -2, 0));
    vertices.push_back(fvec3(+2, +2, 0));

    vertices.push_back(fvec3(-2, +2, 0));
    vertices.push_back(fvec3(+2, -2, 0));

    // Circles
    addCircle(innerCircleRadius * 0.9);
    addCircle(outerCircleRadius * 1.02);
    addDashedCircle((timer / dashedCircleRotationPeriod) * -tau, outerCircleRadius * 0.98);

    // Square
    addSquare(radians(45) + (timer / squareRotationPeriod) * tau, squareSize);
    addSquare((timer / squareRotationPeriod) * tau, squareSize);

    thinLineVertexCount = vertices.size() - thinLineOffset;

    for (fvec3& vertex : vertices)
    {
        vertex += menuPatternOffset;
    }

    return vertices;
}

void MainMenuBackground::createFrames(u32 width, u32 height)
{
    vector<fvec3> vertices = generateVertices(0);

    vertices.reserve(controller->render()->imageCount());

    for (u32 i = 0; i < controller->render()->imageCount(); i++)
    {
        frames.push_back(new MainMenuBackgroundFrame(
            device,
            allocator,
            controller->render()->context()->descriptorPool,
            renderPass,
            copyDescriptorSetLayout,
            controller->render()->context()->unfilteredSampler,
            width,
            height,
            vertices.size()
        ));
    }
}

void MainMenuBackground::destroyFrames()
{
    for (MainMenuBackgroundFrame* frame : frames)
    {
        delete frame;
    }

    frames.clear();
}
