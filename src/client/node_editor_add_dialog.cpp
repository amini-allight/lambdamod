/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "node_editor_add_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "column.hpp"
#include "line_edit.hpp"
#include "text_button.hpp"

static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(
        256 * uiScale(),
        (UIScale::textButtonHeight() * 9) + (UIScale::lineEditHeight() * 1)
    );
}

NodeEditorAddDialog::NodeEditorAddDialog(InterfaceView* view, const function<void(ScriptNodeType, const string&)>& onDone)
    : Dialog(view, dialogSize())
{
    auto column = new Column(this);

    auto lineEdit = new LineEdit(
        column,
        nullptr,
        [this, onDone](const string& text) -> void
        {
            onDone(Script_Node_Execution, text);
            destroy();
        }
    );
    lineEdit->setPlaceholder(localize("node-editor-add-dialog-enter-a-function-name"));

    new TextButton(
        column,
        localize("node-editor-add-dialog-output"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Output, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-void"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Void, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-symbol"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Symbol, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-integer"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Integer, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-float"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Float, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-string"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_String, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-list"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_List, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-lambda"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Lambda, "");
            destroy();
        }
    );

    new TextButton(
        column,
        localize("node-editor-add-dialog-handle"),
        [this, onDone]() -> void
        {
            onDone(Script_Node_Handle, "");
            destroy();
        }
    );
}
