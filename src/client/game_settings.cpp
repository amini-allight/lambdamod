/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "game_settings.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "editor_multi_entry.hpp"
#include "ufloat_edit.hpp"
#include "uint_edit.hpp"
#include "checkbox.hpp"
#include "option_select.hpp"
#include "text_button.hpp"
#include "confirm_dialog.hpp"

GameSettings::GameSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "game-settings-time-multiplier", [this](InterfaceWidget* parent) -> void {
        new UFloatEdit(
            parent,
            bind(&GameSettings::timeMultiplierSource, this),
            bind(&GameSettings::onTimeMultiplier, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "game-settings-magnitude-limit", [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&GameSettings::magnitudeLimitSource, this),
            bind(&GameSettings::onMagnitudeLimit, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "game-settings-attachments-locked", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&GameSettings::attachmentsLockSource, this),
            bind(&GameSettings::onAttachmentsLock, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "game-settings-splash-message", [this](InterfaceWidget* parent) -> void {
        new LineEdit(
            parent,
            bind(&GameSettings::splashMessageSource, this),
            bind(&GameSettings::onSplashMessage, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "game-settings-voice-channel", [this](InterfaceWidget* parent) -> void {
        new OptionSelect(
            parent,
            bind(&GameSettings::voiceChannelsSource, this),
            bind(&GameSettings::voiceChannelSource, this),
            bind(&GameSettings::onVoiceChannel, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "game-settings-server-join-url", [this](InterfaceWidget* parent) -> void {
        new LineEdit(
            parent,
            bind(&GameSettings::serverJoinURLSource, this),
            [](const string& text) -> void {}
        );
    });

    new EditorMultiEntry(
        listView,
        2,
        [this](InterfaceWidget* parent, size_t index) -> void
        {
            switch (index)
            {
            case 0 :
                new TextButton(
                    parent,
                    localize("game-settings-disconnect"),
                    bind(&GameSettings::onDisconnect, this)
                );
                break;
            case 1 :
                new TextButton(
                    parent,
                    localize("game-settings-quit"),
                    bind(&GameSettings::onQuit, this)
                );
                break;
            }
        }
    );
}

f64 GameSettings::timeMultiplierSource()
{
    return context()->controller()->game().timeMultiplier();
}

void GameSettings::onTimeMultiplier(f64 multiplier)
{
    context()->controller()->edit([this, multiplier]() -> void {
        context()->controller()->game().setTimeMultiplier(multiplier);
    });
}

u32 GameSettings::magnitudeLimitSource()
{
    return context()->controller()->game().magnitudeLimit();
}

void GameSettings::onMagnitudeLimit(u32 magnitude)
{
    context()->controller()->edit([this, magnitude]() -> void {
        context()->controller()->game().magnitudeLimit(magnitude);
    });
}

bool GameSettings::attachmentsLockSource()
{
    return context()->controller()->game().attachmentsLocked();
}

void GameSettings::onAttachmentsLock(bool state)
{
    context()->controller()->edit([this, state]() -> void {
        if (state)
        {
            context()->controller()->game().lockAttachments();
        }
        else
        {
            context()->controller()->game().unlockAttachments();
        }
    });
}

string GameSettings::splashMessageSource()
{
    return context()->controller()->game().splashMessage();
}

void GameSettings::onSplashMessage(const string& text)
{
    context()->controller()->edit([this, text]() -> void {
        context()->controller()->game().splashMessage(text);
    });
}

vector<string> GameSettings::voiceChannelsSource()
{
    const User* self = context()->controller()->self();

    if (!self)
    {
        return {};
    }

    vector<string> channels;
    channels.reserve(1 + self->teams().size());

    channels.push_back(localize("game-settings-global"));

    for (const string& team : self->teams())
    {
        channels.push_back(localize("game-settings-team", team));
    }

    return channels;
}

size_t GameSettings::voiceChannelSource()
{
    const User* self = context()->controller()->self();

    if (!self)
    {
        return 0;
    }

    string channelName = context()->controller()->voiceChannel();

    if (channelName.empty())
    {
        return 0;
    }

    size_t i = 1;
    for (const string& team : self->teams())
    {
        if (team == channelName)
        {
            return i;
        }

        i++;
    }

    return 0;
}

void GameSettings::onVoiceChannel(size_t index)
{
    const User* self = context()->controller()->self();

    if (!self)
    {
        return;
    }

    VoiceChannelEvent event;
    event.name = "";

    if (index > 0)
    {
        size_t i = 1;
        for (const string& team : self->teams())
        {
            if (i == index)
            {
                event.name = team;
                break;
            }

            i++;
        }
    }

    context()->controller()->send(event);
}

string GameSettings::serverJoinURLSource()
{
    string url = context()->controller()->serverJoinURL();

    if (url.empty())
    {
        return localize("game-settings-no-url-available");
    }
    else
    {
        return url;
    }
}

void GameSettings::GameSettings::onDisconnect()
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("game-settings-are-you-sure-you-want-to-disconnect"),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            context()->controller()->disconnect();
            dynamic_cast<InterfaceWindow*>(rootParent())->hide();
        }
    );
}

void GameSettings::onQuit()
{
    context()->interface()->addWindow<ConfirmDialog>(
        localize("game-settings-are-you-sure-you-want-to-quit"),
        [=, this](bool confirmed) -> void
        {
            if (!confirmed)
            {
                return;
            }

            context()->controller()->quit();
        }
    );
}

SizeProperties GameSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
