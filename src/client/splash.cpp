/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "splash.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "vr_panel_input_scope.hpp"

static constexpr i32 splashLineMaxLength = 115;
#ifdef LMOD_VR
static constexpr chrono::seconds minDrawTime(1);
#endif

Splash::Splash(InterfaceView* view)
#ifdef LMOD_VR
    : VRCapablePanel(view)
#else
    : InterfaceWidget(view)
#endif
{
#ifdef LMOD_VR
    if (g_vr)
    {
        START_VR_PANEL_SCOPE(context()->input()->getScope("under-screen"), "splash")
            WIDGET_SLOT("dismiss-splash", dismiss)
        END_SCOPE
    }
    else
    {
        START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("under-screen"), "splash")
            WIDGET_SLOT("dismiss-splash", dismiss)
        END_SCOPE
    }
#else
    START_TOPLEVEL_WIDGET_SCOPE(context()->input()->getScope("under-screen"), "splash")
        WIDGET_SLOT("dismiss-splash", dismiss)
    END_SCOPE
#endif
}

void Splash::draw(const DrawContext& ctx) const
{
#ifdef LMOD_VR
    if (!firstDrawTime)
    {
        firstDrawTime = currentTime();
    }
#endif

    vector<string> flatSplash = {
        localize("splash-tips-intro"),
        localize("splash-tip-1"),
        localize("splash-tip-2"),
        localize("splash-tip-3"),
        localize("splash-tip-4"),
        localize("splash-tip-5"),
        localize("splash-tip-6")
    };
#ifdef LMOD_VR
    vector<string> vrSplash = {
        localize("splash-tips-intro"),
        localize("splash-tip-1"),
        localize("splash-tip-2"),
        localize("splash-tip-3"),
        localize("splash-tip-4"),
        localize("splash-tip-5"),
        localize("splash-tip-6")
    };
#endif

    string title;
    vector<string> lines;

    switch (context()->splash()->splashMode())
    {
    case Splash_None :
        return;
    case Splash_System :
        title = string(gameName) + " " + gameVersion;

#ifdef LMOD_VR
        if (g_vr)
        {
            lines = vrSplash;
        }
        else
        {
            lines = flatSplash;
        }
#else
        lines = flatSplash;
#endif
        lines.push_back(terminator());
        break;
    case Splash_Server :
        title = localize("splash-server-welcome-message");
        lines = serverSplash();
        break;
    }

    i32 fontSize = g_vr ? UIScale::largeFontSize(this) : UIScale::smallFontSize(this);

    TextStyle titleStyle;
    titleStyle.color = theme.panelForegroundColor;
    titleStyle.weight = Text_Bold;
    titleStyle.size = fontSize;

    Size rowSize = Size(ctx.size.w * 0.6, titleStyle.getFont()->height() * 2);

    Point point(ctx.size.w * 0.2, (ctx.size.h - (rowSize.h * (1 + lines.size()))) / 2);

    drawText(
        ctx,
        this,
        point,
        rowSize,
        title,
        titleStyle
    );

    point.y += rowSize.h;

    for (const string& line : lines)
    {
        TextStyle textStyle;
        textStyle.color = theme.panelForegroundColor;
        textStyle.size = fontSize;

        drawText(
            ctx,
            this,
            point,
            rowSize,
            line,
            textStyle
        );

        point.y += rowSize.h;
    }
}

bool Splash::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && context()->controller()->connected() && context()->splash()->splashMode() != Splash_None;
}

vector<string> Splash::serverSplash() const
{
    size_t chunkCount = context()->controller()->splashMessage().size() / splashLineMaxLength;

    if (chunkCount * splashLineMaxLength < context()->controller()->splashMessage().size())
    {
        chunkCount += 1;
    }

    vector<string> splash;
    splash.reserve(chunkCount + 1);

    for (size_t i = 0; i < chunkCount; i++)
    {
        size_t start = i * splashLineMaxLength;
        size_t end = i + 1 == chunkCount
            ? context()->controller()->splashMessage().size()
            : (i + 1) * splashLineMaxLength;

        splash.push_back(context()->controller()->splashMessage().substr(start, end - start));
    }

    splash.push_back(terminator());

    return splash;
}

string Splash::terminator() const
{
#ifdef LMOD_VR
    if (g_vr)
    {
        return localize("splash-vr-dismiss");
    }
    else if (context()->controller()->input()->hasGamepad())
    {
        return localize("splash-gamepad-dismiss");
    }
    else
    {
        return localize("splash-keyboard-dismiss");
    }
#else
    if (context()->controller()->input()->hasGamepad())
    {
        return localize("splash-gamepad-dismiss");
    }
    else
    {
        return localize("splash-keyboard-dismiss");
    }
#endif
}

void Splash::dismiss(const Input& input)
{
    context()->splash()->dismissSplash();
}

#ifdef LMOD_VR
optional<Point> Splash::toLocal(const vec3& position, const quaternion& rotation) const
{
    // This prevents input gobbling
    return shouldDraw() && firstDrawTime && currentTime() - *firstDrawTime > minDrawTime ? Point() : optional<Point>();
}
#endif

SizeProperties Splash::sizeProperties() const
{
    return sizePropertiesFillAll;
}
