/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "attached_mode_context.hpp"
#include "control_context.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "flat_render.hpp"

AttachedModeContext::AttachedModeContext(ControlContext* context)
    : _context(context)
#ifdef LMOD_VR
    , _calibratingTracking(false)
#endif
    , _knowledgeViewer(false)
    , _gamepadMenu(false)
    , _mouseLocked(false)
{

}

void AttachedModeContext::onAttach(
    const map<InputType, InputBinding>& bindings,
    const map<string, HUDElement>& hudElements,
    f64 fieldOfView,
    bool mouseLocked,
    f64 voiceVolume,
    const vec3& tintColor,
    f64 tintStrength,
    bool hot
)
{
#ifdef LMOD_VR
    if (g_vr)
    {
        _context->vrMenuButtonHandler()->closeVRMenu();
    }
#endif
    _context->userVisualizer()->setShown(false);

    _context->viewerMode()->exit();

#ifdef LMOD_VR
    if (!g_vr)
    {
        dynamic_cast<FlatRender*>(_context->controller()->render())->setCameraAngle(fieldOfView);

        UserEvent event(User_Event_Angle);
        event.f = fieldOfView;

        _context->controller()->send(event);
    }
#else
    dynamic_cast<FlatRender*>(_context->controller()->render())->setCameraAngle(fieldOfView);

    UserEvent event(User_Event_Angle);
    event.f = fieldOfView;

    _context->controller()->send(event);
#endif

    if (mouseLocked && !_mouseLocked)
    {
        _context->controller()->window()->lockCursor(true);
    }
    else if (!mouseLocked && _mouseLocked)
    {
        _context->controller()->window()->lockCursor(false);
    }

    _bindings = bindings;
    _hudElements = hudElements;
    _mouseLocked = mouseLocked;

    context()->tint()->set(tintColor, tintStrength);

    if (hot)
    {
        _context->controller()->sound()->addOneshot(Sound_Oneshot_Attach);
    }
}

void AttachedModeContext::onDetach()
{
    _context->controller()->sound()->addOneshot(Sound_Oneshot_Detach);

#ifdef LMOD_VR
    if (!g_vr)
    {
        dynamic_cast<FlatRender*>(_context->controller()->render())->setCameraAngle(defaultCameraAngle);

        UserEvent event(User_Event_Angle);
        event.f = defaultCameraAngle;

        _context->controller()->send(event);
    }
#else
    dynamic_cast<FlatRender*>(_context->controller()->render())->setCameraAngle(defaultCameraAngle);

    UserEvent event(User_Event_Angle);
    event.f = defaultCameraAngle;

    _context->controller()->send(event);
#endif

    if (_mouseLocked)
    {
        _context->controller()->window()->lockCursor(false);
    }

    _mouseLocked = false;
    _hudElements.clear();
    _bindings.clear();

    context()->tint()->set(vec3(0), 0);

    _context->viewerMode()->enter();

    _context->userVisualizer()->setShown(true);
}

ControlContext* AttachedModeContext::context() const
{
    return _context;
}

void AttachedModeContext::step()
{
    const Entity* host = _context->controller()->host();

    if (!host)
    {
        return;
    }

    mat4 entityTransform = host->globalTransform();

#ifdef LMOD_VR
    const BodyPart* anchor = host->body().get(g_vr ? Body_Anchor_Room : Body_Anchor_Viewer);
#else
    const BodyPart* anchor = host->body().get(Body_Anchor_Viewer);
#endif

    if (anchor)
    {
        mat4 anchorTransform = anchor->regionalTransform();

        anchorTransform = entityTransform.applyToTransform(anchorTransform);

#ifdef LMOD_VR
        if (!g_vr)
        {
            _context->flatViewer()->move(anchorTransform.position(), anchorTransform.rotation());
        }
        else
        {
            _context->vrViewer()->moveRoom(anchorTransform.position(), anchorTransform.rotation());
        }
#else
        _context->flatViewer()->move(anchorTransform.position(), anchorTransform.rotation());
#endif
    }
}

void AttachedModeContext::detach()
{
    GameEvent event(Game_Event_Detach);

    _context->controller()->send(event);
}

void AttachedModeContext::ping()
{
    GameEvent event(Game_Event_Ping);

    _context->controller()->send(event);
}

void AttachedModeContext::brake()
{
    GameEvent event(Game_Event_Brake);

    _context->controller()->send(event);
}

#ifdef LMOD_VR
f64 AttachedModeContext::vrScale() const
{
    const User* self = _context->controller()->self();

    if (!self)
    {
        return 1;
    }

    return self->roomOffset().scale().x;
}

void AttachedModeContext::scalePlayerUp()
{
    const User* self = _context->controller()->self();

    UserEvent event;
    event.type = User_Event_Room_Offset;
    event.id = _context->controller()->selfID();
    event.transform = self->roomOffset();
    event.transform.setScale(self->roomOffset().scale() + 0.01);

    _context->controller()->send(NetworkEvent(event));
}

void AttachedModeContext::scalePlayerDown()
{
    const User* self = _context->controller()->self();

    if (self->roomOffset().scale().x <= 0.01)
    {
        return;
    }

    UserEvent event;
    event.type = User_Event_Room_Offset;
    event.id = _context->controller()->selfID();
    event.transform = self->roomOffset();
    event.transform.setScale(self->roomOffset().scale() - 0.01);

    _context->controller()->send(NetworkEvent(event));
}

void AttachedModeContext::recenter()
{
    const User* self = _context->controller()->self();

    if (!self)
    {
        return;
    }

    const Entity* host = _context->controller()->host();

    if (!host)
    {
        return;
    }

    const BodyPart* viewer = host->body().get(Body_Anchor_Viewer);

    if (!viewer)
    {
        return;
    }

    const BodyPart* originalViewer = host->body().get(Body_Anchor_Viewer);

    if (!originalViewer)
    {
        return;
    }

    quaternion originalViewerRotation = host->globalRotation() * originalViewer->regionalRotation();

    vec3 originalViewerDirection = vec3(_context->controller()->vr()->worldToRoom(originalViewerRotation).forward().toVec2(), 0).normalize();

    vec3 headDirection = vec3(_context->controller()->vr()->worldToRoom(_context->vrViewer()->headTransform().rotation()).forward().toVec2(), 0).normalize();

    vec3 position = viewer->regionalPosition() - _context->vrViewer()->headTransform().position();
    quaternion rotation = quaternion(originalViewerDirection, upDir) / quaternion(headDirection, upDir);

    UserEvent event;
    event.type = User_Event_Room_Offset;
    event.id = _context->controller()->selfID();
    event.transform = self->roomOffset();
    event.transform.setPosition(position);
    event.transform.setRotation(rotation);

    _context->controller()->send(NetworkEvent(event));
}

void AttachedModeContext::orientRoom()
{
    const User* self = _context->controller()->self();

    UserEvent event;
    event.type = User_Event_Room_Offset;
    event.id = _context->controller()->selfID();
    event.transform = self->roomOffset();
    event.transform.setRotation(_context->vrViewer()->headTransform().rotation().inverse());

    _context->controller()->send(NetworkEvent(event));
}

void AttachedModeContext::resetRoom()
{
    UserEvent event;
    event.type = User_Event_Room_Offset;
    event.id = _context->controller()->selfID();
    event.transform = mat4();

    _context->controller()->send(NetworkEvent(event));
}

bool AttachedModeContext::calibratingTracking() const
{
    return _calibratingTracking;
}

void AttachedModeContext::enableTrackingCalibrator()
{
    _calibratingTracking = true;
}

void AttachedModeContext::disableTrackingCalibrator()
{
    _calibratingTracking = false;
    _context->vrMenuButtonHandler()->openVRMenu();
}

void AttachedModeContext::setTrackingCalibration()
{
    // NOTE: Update this when adding new body anchor types
    static constexpr BodyAnchorType types[] = {
        Body_Anchor_Head,
        Body_Anchor_Hips,
        Body_Anchor_Left_Hand,
        Body_Anchor_Right_Hand,
        Body_Anchor_Left_Foot,
        Body_Anchor_Right_Foot,
        Body_Anchor_Chest,
        Body_Anchor_Left_Shoulder,
        Body_Anchor_Right_Shoulder,
        Body_Anchor_Left_Elbow,
        Body_Anchor_Right_Elbow,
        Body_Anchor_Left_Wrist,
        Body_Anchor_Right_Wrist,
        Body_Anchor_Left_Knee,
        Body_Anchor_Right_Knee,
        Body_Anchor_Left_Ankle,
        Body_Anchor_Right_Ankle
    };

    const Entity* host = _context->controller()->host();

    map<VRDevice, mat4> rawMapping = _context->vrViewer()->mapping();
    map<VRDevice, mat4> mapping;

    for (BodyAnchorType type : types)
    {
        const BodyPart* anchor = host->body().get(type);

        if (!anchor)
        {
            continue;
        }

        VRDevice device;

        switch (type)
        {
        default :
            continue;
        case Body_Anchor_Head :
            device = VR_Device_Head;
            break;
        case Body_Anchor_Left_Hand :
            device = VR_Device_Left_Hand;
            break;
        case Body_Anchor_Right_Hand :
            device = VR_Device_Right_Hand;
            break;
        case Body_Anchor_Hips :
            device = VR_Device_Hips;
            break;
        case Body_Anchor_Left_Foot :
            device = VR_Device_Left_Foot;
            break;
        case Body_Anchor_Right_Foot :
            device = VR_Device_Right_Foot;
            break;
        case Body_Anchor_Chest :
            device = VR_Device_Chest;
            break;
        case Body_Anchor_Left_Shoulder :
            device = VR_Device_Left_Shoulder;
            break;
        case Body_Anchor_Right_Shoulder :
            device = VR_Device_Right_Shoulder;
            break;
        case Body_Anchor_Left_Elbow :
            device = VR_Device_Left_Elbow;
            break;
        case Body_Anchor_Right_Elbow :
            device = VR_Device_Right_Elbow;
            break;
        case Body_Anchor_Left_Wrist :
            device = VR_Device_Left_Wrist;
            break;
        case Body_Anchor_Right_Wrist :
            device = VR_Device_Right_Wrist;
            break;
        case Body_Anchor_Left_Knee :
            device = VR_Device_Left_Knee;
            break;
        case Body_Anchor_Right_Knee :
            device = VR_Device_Right_Knee;
            break;
        case Body_Anchor_Left_Ankle :
            device = VR_Device_Left_Ankle;
            break;
        case Body_Anchor_Right_Ankle :
            device = VR_Device_Right_Ankle;
            break;
        }

        auto it = rawMapping.find(device);

        if (it != rawMapping.end())
        {
            const mat4& raw = it->second;

            mapping.insert({
                device,
                {
                    raw.rotation().inverse().rotate(anchor->regionalPosition() - raw.position()),
                    (anchor->regionalRotation() / raw.rotation()).normalize(),
                    vec3(1)
                }
            });
        }
    }

    UserEvent event;
    event.type = User_Event_Anchor;
    event.id = _context->controller()->selfID();
    event.mapping = mapping;

    _context->controller()->send(event);

    disableTrackingCalibrator();
}

void AttachedModeContext::clearTrackingCalibration()
{
    _context->controller()->send(UserEvent(User_Event_Unanchor));
}
#endif

bool AttachedModeContext::knowledgeViewer() const
{
    return _knowledgeViewer;
}

void AttachedModeContext::toggleKnowledgeViewer()
{
    _knowledgeViewer = !_knowledgeViewer;
}

bool AttachedModeContext::gamepadMenu() const
{
    return _gamepadMenu;
}

void AttachedModeContext::toggleGamepadMenu()
{
    _gamepadMenu = !_gamepadMenu;
}

const map<InputType, InputBinding>& AttachedModeContext::bindings() const
{
    return _bindings;
}

const map<string, HUDElement>& AttachedModeContext::hudElements() const
{
    return _hudElements;
}

bool AttachedModeContext::mouseLocked() const
{
    return _mouseLocked;
}
