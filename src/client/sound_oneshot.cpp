/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_oneshot.hpp"
#include "sound_tools.hpp"

SoundOneshot::SoundOneshot(Sound* sound, SoundEffect* soundEffect, const vec3& position)
    : SoundSource(sound, position, quaternion(), false, tau, false)
    , soundEffect(soundEffect)
    , readHead(0)
{

}

bool SoundOneshot::completed() const
{
    return readHead >= static_cast<i64>(soundEffect->length());
}

variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> SoundOneshot::pull(f64 multiplier)
{
    if (soundEffect->channels().size() == 1)
    {
        auto [ chunk, advance ] = readSampleSource(soundEffect->channels().front(), readHead, multiplier, false);

        readHead += advance;

        return chunk;
    }
    else
    {
        auto [ left, leftAdvance ] = readSampleSource(soundEffect->channels().front(), readHead, multiplier, false);
        auto [ right, rightAdvance ] = readSampleSource(soundEffect->channels().back(), readHead, multiplier, false);

        readHead += leftAdvance;

        return SoundOverlappedStereoChunk(left, right);
    }
}
