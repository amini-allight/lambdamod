/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user_controls_tool.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "tab_view.hpp"
#include "user_controls_status.hpp"
#include "user_controls_tool_pages.hpp"

static Size windowSize()
{
    return UIScale::windowBordersSize() + Size(UIScale::tabSize().w * 6, 504 * uiScale());
}

UserControlsTool::UserControlsTool(InterfaceView* view)
    : InterfaceWindow(view, localize("user-controls-tool-user-controls-tool"))
{
    START_WIDGET_SCOPE("user-controls-tool")
        WIDGET_SLOT("close-dialog", dismiss)
        WIDGET_SLOT("toggle-user-controls-tool", dismiss)
    END_SCOPE

    tabs = new TabView(this, {
        localize("user-controls-tool-status"),
        localize("user-controls-tool-tether"),
        localize("user-controls-tool-confine"),
        localize("user-controls-tool-goto"),
        localize("user-controls-tool-fade"),
        localize("user-controls-tool-wait")
    });

    new UserControlsStatus(
        tabs->tab(localize("user-controls-tool-status"))
    );

    new UserTetherTool(
        tabs->tab(localize("user-controls-tool-tether")),
        context()
    );

    new UserConfineTool(
        tabs->tab(localize("user-controls-tool-confine")),
        context()
    );

    new UserGotoTool(
        tabs->tab(localize("user-controls-tool-goto")),
        context()
    );

    new UserFadeTool(
        tabs->tab(localize("user-controls-tool-fade")),
        context()
    );

    new UserWaitTool(
        tabs->tab(localize("user-controls-tool-wait")),
        context()
    );
}

void UserControlsTool::open(const Point& point)
{
    InterfaceWindow::open(point, windowSize());

    for (InterfaceWidget* child : tabs->activeTab()->children())
    {
        dynamic_cast<TabContent*>(child)->open();
    }
}

void UserControlsTool::open(const Point& point, const string& tabName)
{
    open(point);

    tabs->open(tabName);
}

void UserControlsTool::dismiss(const Input& input)
{
    hide();
}
