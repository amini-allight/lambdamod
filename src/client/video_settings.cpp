/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "video_settings.hpp"
#include "localization.hpp"
#include "units.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "flat_render.hpp"
#include "list_view.hpp"
#include "editor_entry.hpp"
#include "uint_edit.hpp"
#include "checkbox.hpp"
#include "dynamic_label.hpp"

VideoSettings::VideoSettings(InterfaceWidget* parent)
    : InterfaceWidget(parent)
{
    auto listView = new ListView(this);

    new EditorEntry(listView, "video-settings-window-width", pixelSuffix(), [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&VideoSettings::windowWidthSource, this),
            bind(&VideoSettings::onWindowWidth, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "video-settings-window-height", pixelSuffix(), [this](InterfaceWidget* parent) -> void {
        new UIntEdit(
            parent,
            bind(&VideoSettings::windowHeightSource, this),
            bind(&VideoSettings::onWindowHeight, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "video-settings-window-fullscreen", [this](InterfaceWidget* parent) -> void {
        new Checkbox(
            parent,
            bind(&VideoSettings::windowFullscreenSource, this),
            bind(&VideoSettings::onWindowFullscreen, this, placeholders::_1)
        );
    });

    new EditorEntry(listView, "video-settings-ray-tracing", [this](InterfaceWidget* parent) -> void {
        new DynamicLabel(
            parent,
            bind(&VideoSettings::rayTracingSource, this)
        );
    });
}

u32 VideoSettings::windowWidthSource()
{
    return dynamic_cast<FlatRender*>(context()->controller()->render())->width();
}

void VideoSettings::onWindowWidth(u32 width)
{
    context()->controller()->window()->setSize(width, dynamic_cast<FlatRender*>(context()->controller()->render())->height());
}

u32 VideoSettings::windowHeightSource()
{
    return dynamic_cast<FlatRender*>(context()->controller()->render())->height();
}

void VideoSettings::onWindowHeight(u32 height)
{
    context()->controller()->window()->setSize(dynamic_cast<FlatRender*>(context()->controller()->render())->width(), height);
}

bool VideoSettings::windowFullscreenSource()
{
    return context()->controller()->window()->fullscreen();
}

void VideoSettings::onWindowFullscreen(bool state)
{
    context()->controller()->window()->setFullscreen(state);
}

string VideoSettings::rayTracingSource()
{
    bool rayTracingSupported = context()->controller()->render()->rayTracingSupported();
    bool rayTracingEnabled = context()->controller()->render()->rayTracingEnabled();

    if (rayTracingSupported && rayTracingEnabled)
    {
        return localize("video-settings-ray-tracing-active");
    }
    else if (rayTracingSupported && !rayTracingEnabled)
    {
        return localize("video-settings-ray-tracing-available-but-inactive");
    }
    else
    {
        return localize("video-settings-ray-tracing-unavailable");
    }
}

SizeProperties VideoSettings::sizeProperties() const
{
    return sizePropertiesFillAll;
}
