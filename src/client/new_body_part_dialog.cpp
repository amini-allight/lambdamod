/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "new_body_part_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_scale.hpp"
#include "control_context.hpp"
#include "column.hpp"
#include "text_button.hpp"

static constexpr size_t bodyPartTypeCount = 13;
static Size dialogSize()
{
    return UIScale::dialogBordersSize() + Size(
        128 * uiScale(),
        UIScale::textButtonHeight() * bodyPartTypeCount
    );
}

NewBodyPartDialog::NewBodyPartDialog(InterfaceView* view)
    : Dialog(view, dialogSize())
{
    auto column = new Column(this);

    new TextButton(
        column,
        localize("new-body-part-dialog-line"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Line);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-plane"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Plane);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-anchor"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Anchor);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-solid"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Solid);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-text"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Text);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-symbol"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Symbol);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-canvas"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Canvas);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-structure"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Structure);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-terrain"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Terrain);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-light"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Light);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-force"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Force);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-area"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Area);
            destroy();
        }
    );

    new TextButton(
        column,
        localize("new-body-part-dialog-rope"),
        [this]() -> void
        {
            context()->viewerMode()->bodyMode()->addNew(Body_Part_Rope);
            destroy();
        }
    );
}
