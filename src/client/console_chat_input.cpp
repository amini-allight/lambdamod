/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "console_chat_input.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "console_input.hpp"

ConsoleChatInput::ConsoleChatInput(
    InterfaceWidget* parent,
    const function<void(const string&, const optional<ChatDestination>&)>& onReturn
)
    : InterfaceWidget(parent)
    , onReturn(onReturn)
{
    START_WIDGET_SCOPE("console-chat-input")

    END_SCOPE

    destinationSelector = new ChatDestinationSelector(this, bind(&ConsoleChatInput::onDestination, this, placeholders::_1));
    edit = new LineEdit(this, nullptr, bind(&ConsoleChatInput::onDone, this, placeholders::_1));
    edit->setPlaceholder(localize("console-chat-input-switch-to-command-mode"));
}

void ConsoleChatInput::move(const Point& position)
{
    this->position(position);

    destinationSelector->move(position);
    edit->move(position + Point(UIScale::destinationSelectorSize().w, 0));
}

void ConsoleChatInput::resize(const Size& size)
{
    this->size(size);

    destinationSelector->move(position());
    destinationSelector->resize(UIScale::destinationSelectorSize());
    edit->move(position() + Point(UIScale::destinationSelectorSize().w, 0));
    edit->resize(Size(size.w - UIScale::destinationSelectorSize().w, size.h));
}

bool ConsoleChatInput::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && dynamic_cast<ConsoleInput*>(parent())->chatMode();
}

void ConsoleChatInput::focus()
{
    InterfaceWidget::focus();

    if (edit->contains(pointer))
    {
        edit->focus();
    }
}

void ConsoleChatInput::forceFocus()
{
    focus();
    edit->focus();
}

void ConsoleChatInput::onDestination(const optional<ChatDestination>& destination)
{
    this->destination = destination;
}

void ConsoleChatInput::onDone(const string& text)
{
    onReturn(text, destination);
    edit->clear();
}

SizeProperties ConsoleChatInput::sizeProperties() const
{
    return sizePropertiesFillAll;
}
