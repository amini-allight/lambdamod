/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_types.hpp"

struct InputScopeBinding
{
    explicit InputScopeBinding(InputType type);

    bool operator==(const InputScopeBinding& rhs) const;
    bool operator!=(const InputScopeBinding& rhs) const;
    bool operator>(const InputScopeBinding& rhs) const;
    bool operator<(const InputScopeBinding& rhs) const;
    bool operator>=(const InputScopeBinding& rhs) const;
    bool operator<=(const InputScopeBinding& rhs) const;
    strong_ordering operator<=>(const InputScopeBinding& rhs) const;

    bool match(const Input& input, u32 repeats) const;
    u32 modifierCount() const;

    static InputScopeBinding fromString(const string& s);
    string toString() const;

    InputType type;
    bool up;
    bool control;
    bool shift;
    bool alt;
    bool super;
    u32 repeats;
    bool anyText;
};

namespace InputScopeBindingTools
{
InputScopeBinding binding(InputType type);
InputScopeBinding up(InputScopeBinding binding);
InputScopeBinding repeat(InputScopeBinding binding, u32 repeats);
InputScopeBinding control(InputScopeBinding binding);
InputScopeBinding shift(InputScopeBinding binding);
InputScopeBinding alt(InputScopeBinding binding);
InputScopeBinding super(InputScopeBinding binding);
InputScopeBinding anyText();
};
