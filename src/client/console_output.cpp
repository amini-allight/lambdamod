/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "console_output.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

ConsoleOutput::ConsoleOutput(InterfaceWidget* parent)
    : InterfaceWidget(parent)
    , scrollIndex(0)
{
    START_WIDGET_SCOPE("console-output")
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)
        WIDGET_SLOT("page-up", pageUp)
        WIDGET_SLOT("page-down", pageDown)
    END_SCOPE
}

void ConsoleOutput::push(Message message)
{
    string text = message.toString();

    vector<SyntaxHighlightChunk> syntaxHighlight;

    if (message.type == Message_Script)
    {
        syntaxHighlight = calculateSyntaxHighlight(text);
    }

    if (static_cast<i32>(text.size()) > lineWidth())
    {
        size_t chunkCount = text.size() / lineWidth();

        if (chunkCount * lineWidth() < text.size())
        {
            chunkCount += 1;
        }

        for (size_t i = 0; i < chunkCount; i++)
        {
            i32 start = i * lineWidth();
            i32 end = i + 1 == chunkCount
                ? text.size()
                : (i + 1) * lineWidth();

            i32 size = end - start;

            vector<SyntaxHighlightChunk> syntaxHighlightSubset;

            for (SyntaxHighlightChunk chunk : syntaxHighlight)
            {
                if (chunk.start + chunk.size <= start)
                {
                    continue;
                }
                else if (chunk.start >= start + size)
                {
                    continue;
                }

                i32 newStart = max(chunk.start - start, 0);
                i32 newEnd = min((chunk.start - start) + chunk.size, end);

                i32 newSize = newEnd - newStart;

                chunk.start = newStart;
                chunk.size = newSize;

                syntaxHighlightSubset.push_back(chunk);
            }

            Message chunk = message;
            chunk.text = text.substr(start, size);

            this->syntaxHighlight.insert({ messages.size(), syntaxHighlightSubset });
            messages.push_back(chunk);
        }
    }
    else
    {
        this->syntaxHighlight.insert({ messages.size(), syntaxHighlight });
        message.text = text;
        messages.push_back(message);
    }

    scrollIndex = 0;
}

void ConsoleOutput::draw(const DrawContext& ctx) const
{
    i32 y = size().h - UIScale::consoleOutputRowHeight();
    size_t start = messages.size() - (scrollIndex + 1);
    for (size_t i = start; i < messages.size() && y >= 0; i--)
    {
        TextStyle style;
        style.color = messageColor(messages[i]);

        if (messages[i].type == Message_Script)
        {
            for (const SyntaxHighlightChunk& chunk : syntaxHighlight.at(i))
            {
                TextStyle style;
                style.color = syntaxHighlightColor(chunk.type);

                drawText(
                    ctx,
                    this,
                    Point(chunk.start * charWidth(), y),
                    Size(size().w, UIScale::consoleOutputRowHeight()),
                    messages[i].text.substr(chunk.start, chunk.size),
                    style
                );
            }
        }
        else
        {
            drawText(
                ctx,
                this,
                Point(0, y),
                Size(size().w, UIScale::consoleOutputRowHeight()),
                messages[i].text, // toString is used in Console::log for line splitting
                style
            );
        }

        y -= UIScale::consoleOutputRowHeight();
    }
}

i32 ConsoleOutput::charWidth() const
{
    return TextStyle().getFont()->monoWidth();
}

i32 ConsoleOutput::lineWidth() const
{
    return (size().w - (UIScale::marginSize() * 2)) / charWidth();
}

size_t ConsoleOutput::pageHeight() const
{
    return size().h / UIScale::consoleOutputRowHeight();
}

void ConsoleOutput::scrollUp(const Input& input)
{
    if (scrollIndex < messages.size() - pageHeight())
    {
        scrollIndex++;
    }
}

void ConsoleOutput::scrollDown(const Input& input)
{
    if (scrollIndex > 0)
    {
        scrollIndex--;
    }
}

void ConsoleOutput::goToStart(const Input& input)
{
    scrollIndex = messages.size() - pageHeight();
}

void ConsoleOutput::goToEnd(const Input& input)
{
    scrollIndex = 0;
}

void ConsoleOutput::pageUp(const Input& input)
{
    scrollIndex += pageHeight();

    if (scrollIndex > messages.size() - pageHeight())
    {
        scrollIndex = messages.size() - pageHeight();
    }
}

void ConsoleOutput::pageDown(const Input& input)
{
    if (scrollIndex < pageHeight())
    {
        scrollIndex = 0;
        return;
    }

    scrollIndex -= pageHeight();
}

SizeProperties ConsoleOutput::sizeProperties() const
{
    return sizePropertiesFillAll;
}
