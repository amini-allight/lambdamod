/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "confirm_dialog.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "column.hpp"
#include "row.hpp"
#include "label.hpp"
#include "text_button.hpp"

static Size dialogSize(const string& text)
{
    return Size(
        TextStyle().getFont()->width(" " + text + " "),
        UIScale::labelHeight() + UIScale::textButtonHeight()
    ) + UIScale::dialogBordersSize();
}

ConfirmDialog::ConfirmDialog(InterfaceView* view, const string& text, const function<void(bool)>& onDone)
    : Dialog(view, dialogSize(text))
    , onDone(onDone)
{
    auto column = new Column(this);

    TextStyleOverride style;
    style.alignment = Text_Center;

    new Label(
        column,
        text,
        style
    );

    auto row = new Row(column);

    new TextButton(
        row,
        localize("confirm-dialog-no"),
        [this]() -> void
        {
            this->onDone(false);
            destroy();
        }
    );
    new TextButton(
        row,
        localize("confirm-dialog-yes"),
        [this]() -> void
        {
            this->onDone(true);
            destroy();
        }
    );
}

void ConfirmDialog::destroy()
{
    if (!shouldDestroy())
    {
        onDone(false);
    }

    Dialog::destroy();
}
