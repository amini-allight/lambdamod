/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"
#include "body_part.hpp"
#include "body_mode_context.hpp"

class ListView;

enum BodySpaceType : u8
{
    Body_Space_Global,
    Body_Space_Regional,
    Body_Space_Local,
    Body_Space_Part
};

enum BodyEditorMode : u8
{
    Body_Editor_None,
    Body_Editor_Line,
    Body_Editor_Plane,
    Body_Editor_Anchor,
    Body_Editor_Multi,
    Body_Editor_Sub_Part
};

class BodyEditor : public InterfaceWidget, public TabContent
{
public:
    BodyEditor(InterfaceWidget* parent, EntityID id);

    void step() override;

private:
    EntityID id;

    set<BodyPartSelectionID> lastSelection;
    BodyPart lastActivePart;
    BodySpaceType spaceType;
    InterfaceWidget* content;

    bool needsModeUpdate() const;
    void updateMode();

    void addNoPartChildren();
    void addOtherChildren();
    void addLineChildren();
    void addPlaneChildren();
    void addAnchorChildren();
    void addSolidChildren();
    void addTextChildren();
    void addSymbolChildren();
    void addCanvasChildren();
    void addStructureChildren();
    void addTerrainChildren();
    void addLightChildren();
    void addForceChildren();
    void addAreaChildren();
    void addRopeChildren();
    void addPlaneDimensionChildren(u32 count);
    void addSolidDimensionChildren(u32 count);
    void addBallConstraintChildren();
    void addConeConstraintChildren();
    void addHingeConstraintChildren();
    void clearChildren();
    void addDefaultChildren();
    void addStructureSubpartChildren();
    void addStructureSubpartBlockChildren();
    void addStructureSubpartRectangleChildren();
    void addStructureSubpartCircleChildren();
    void addStructureSubpartLineChildren();
    void addStructureSubpartWallChildren();
    void addTerrainSubpartChildren();

    vec3 otherPositionSource();
    void onOtherPosition(const vec3& position);

    // General
    string idSource();
    vector<string> parentOptionsSource();
    size_t parentSource();
    void onParent(size_t index);
    vector<string> parentSubpartOptionsSource();
    size_t parentSubpartSource();
    void onParentSubpart(size_t index);
    size_t spaceSource();
    void onSpace(size_t index);
    vec3 positionSource();
    void onPosition(const vec3& position);
    quaternion rotationSource();
    void onRotation(const quaternion& rotation);
    vec3 scaleSource();
    void onScale(const vec3& scale);
    f64 massSource();
    void onMass(f64 mass);
    f64 maxForceSource();
    void onMaxForce(f64 force);
    f64 maxVelocitySource();
    void onMaxVelocity(f64 velocity);
    vector<string> typeOptionsSource();
    vector<string> typeOptionTooltipsSource();
    size_t typeSource();
    void onType(size_t index);
    vector<string> constraintTypeOptionsSource();
    vector<string> constraintTypeOptionTooltipsSource();
    size_t constraintTypeSource();
    void onConstraintType(size_t index);
    vec3 ballConstraintMinimumSource();
    void onBallConstraintMinimum(const vec3& min);
    vec3 ballConstraintMaximumSource();
    void onBallConstraintMaximum(const vec3& max);
    vec2 coneConstraintSpanSource();
    void onConeConstraintSpan(const vec2& span);
    f64 coneConstraintTwistMinimumSource();
    void onConeConstraintTwistMinimum(const f64& min);
    f64 coneConstraintTwistMaximumSource();
    void onConeConstraintTwistMaximum(const f64& max);
    f64 hingeConstraintMinimumSource();
    void onHingeConstraintMinimum(const f64& min);
    f64 hingeConstraintMaximumSource();
    void onHingeConstraintMaximum(const f64& max);

    // Line
    f64 lineDiameterSource();
    void onLineDiameter(f64 diameter);
    f64 lineLengthSource();
    void onLineLength(f64 length);
    EmissiveColor lineStartColorSource();
    void onLineStartColor(const EmissiveColor& color);
    EmissiveColor lineEndColorSource();
    void onLineEndColor(const EmissiveColor& color);
    bool lineConstraintAtEndSource();
    void onLineConstraintAtEnd(bool state);

    // Plane
    f64 planeThicknessSource();
    void onPlaneThickness(f64 thickness);
    f64 planeWidthSource();
    void onPlaneWidth(f64 width);
    f64 planeHeightSource();
    void onPlaneHeight(f64 height);
    vector<string> planeTypeOptionsSource();
    vector<string> planeTypeOptionTooltipsSource();
    size_t planeTypeSource();
    void onPlaneType(size_t index);
    EmissiveColor planeColorSource();
    void onPlaneColor(const EmissiveColor& color);
    vector<string> planeConstraintSubpartOptionsSource();
    size_t planeConstraintSubpartSource();
    void onPlaneConstraintSubpart(size_t index);
    bool planeEmptySource();
    void onPlaneEmpty(bool state);

    // Anchor
    vector<string> anchorTypeOptionsSource();
    size_t anchorTypeSource();
    void onAnchorType(size_t index);
    string anchorNameSource();
    void onAnchorName(const string& name);
    f64 anchorMassLimitSource();
    void onAnchorMassLimit(f64 limit);
    f64 anchorPullDistanceSource();
    void onAnchorPullDistance(f64 distance);

    // Solid
    f64 solidWidthSource();
    void onSolidWidth(f64 width);
    f64 solidLengthSource();
    void onSolidLength(f64 length);
    f64 solidHeightSource();
    void onSolidHeight(f64 height);
    vector<string> solidTypeOptionsSource();
    vector<string> solidTypeOptionTooltipsSource();
    size_t solidTypeSource();
    void onSolidType(size_t index);
    EmissiveColor solidColorSource();
    void onSolidColor(const EmissiveColor& color);
    vector<string> solidConstraintSubpartOptionsSource();
    size_t solidConstraintSubpartSource();
    void onSolidConstraintSubpart(size_t index);

    // Text
    vector<string> textTypeOptionsSource();
    vector<string> textTypeOptionTooltipsSource();
    size_t textTypeSource();
    void onTextType(size_t index);
    string textTextSource();
    void onTextText(const string& text);
    f64 textHeightSource();
    void onTextHeight(f64 height);
    EmissiveColor textColorSource();
    void onTextColor(const EmissiveColor& color);

    // Symbol
    vector<string> symbolTypeOptionsSource();
    vector<string> symbolTypeOptionTooltipsSource();
    size_t symbolTypeSource();
    void onSymbolType(size_t index);
    f64 symbolSizeSource();
    void onSymbolSize(f64 size);
    EmissiveColor symbolColorSource();
    void onSymbolColor(const EmissiveColor& color);

    // Canvas
    f64 canvasSizeSource();
    void onCanvasSize(f64 size);
    EmissiveColor canvasColorSource();
    void onCanvasColor(const EmissiveColor& color);

    // Structure
    bool structurePositiveSource();
    void onStructurePositive(bool state);
    void onOpenStructureTypes();
    StructureTypeID structureNextTypeIDSource();
    void onStructureNextTypeID(StructureTypeID id);
    vector<StructureType> structureTypesSource();
    void onStructureTypes(const vector<StructureType>& types);
    EmissiveColor structureColorSource();
    void onStructureColor(const EmissiveColor& color);

    string structureSubpartIDSource(size_t index);
    string structureSubpartTypeSource(size_t index);
    vector<string> structureSubpartTypeIDOptionsSource(size_t index);
    size_t structureSubpartTypeIDSource(size_t index);
    void onStructureSubpartTypeID(size_t index, size_t typeIndex);
    bool structureSubpartPositiveSource(size_t index);
    void onStructureSubpartPositive(size_t index, bool positive);
    bool structureSubpartInvertedSource(size_t index);
    void onStructureSubpartInverted(size_t index, bool inverted);
    vec2 structureSubpartPositionASource(size_t index);
    void onStructureSubpartPositionA(size_t index, const vec2& positionA);
    vec2 structureSubpartPositionBSource(size_t index);
    void onStructureSubpartPositionB(size_t index, const vec2& positionB);
    f64 structureSubpartRadiusSource(size_t index);
    void onStructureSubpartRadius(size_t index, f64 radius);
    f64 structureSubpartLengthSource(size_t index);
    void onStructureSubpartLength(size_t index, f64 length);

    // Terrain
    EmissiveColor terrainColorSource();
    void onTerrainColor(const EmissiveColor& color);

    string terrainSubpartIDSource(size_t index);
    optional<EmissiveColor> terrainSubpartColorSource(size_t index);
    void onTerrainSubpartColor(size_t index, const optional<EmissiveColor>& color);
    f64 terrainSubpartRadiusSource(size_t index);
    void onTerrainSubpartRadius(size_t index, f64 radius);
    bool terrainSubpartOneSidedSource(size_t index);
    void onTerrainSubpartOneSided(size_t index, bool state);

    // Light
    bool lightEnabledSource();
    void onLightEnabled(bool state);
    HDRColor lightColorSource();
    void onLightColor(const HDRColor& color);
    f64 lightAngleSource();
    void onLightAngle(f64 angle);

    // Force
    bool forceEnabledSource();
    void onForceEnabled(bool state);
    vector<string> forceTypeOptionsSource();
    vector<string> forceTypeOptionTooltipsSource();
    size_t forceTypeSource();
    void onForceType(size_t index);
    f64 forceFanIntensitySource();
    void onForceFanIntensity(f64 intensity);
    f64 forceIntensitySource();
    void onForceIntensity(f64 intensity);
    f64 forceRadiusSource();
    void onForceRadius(f64 radius);
    f64 forceMassFlowRateSource();
    void onForceMassFlowRate(f64 rate);
    bool forceReactionSource();
    void onForceReaction(bool state);

    // Area
    f64 areaHeightSource();
    void onAreaHeight(f64 height);
    f64 areaEnabledSource();
    void onAreaEnabled(bool state);
    optional<f64> areaSpeedOfSoundSource();
    void onAreaSpeedOfSound(const optional<f64>& speedOfSound);
    optional<vec3> areaGravitySource();
    void onAreaGravity(const optional<vec3>& gravity);
    optional<vec3> areaFlowVelocitySource();
    void onAreaFlowVelocity(const optional<vec3>& flowVelocity);
    optional<f64> areaDensitySource();
    void onAreaDensity(const optional<f64>& density);
    optional<vec3> areaUpSource();
    void onAreaUp(const optional<vec3>& up);
    optional<f64> areaFogDistanceSource();
    void onAreaFogDistance(const optional<f64>& distance);
    void onAreaOpenSkyParameters(
        const function<SkyParameters()>& valueSource,
        const function<void(const SkyParameters&)>& onValue
    );
    optional<SkyParameters> areaSkyParametersSource();
    void onAreaSkyParameters(const optional<SkyParameters>& parameters);
    void onAreaOpenAtmosphereParameters(
        const function<AtmosphereParameters()>& valueSource,
        const function<void(const AtmosphereParameters&)>& onValue
    );
    optional<AtmosphereParameters> areaAtmosphereParametersSource();
    void onAreaAtmosphereParameters(const optional<AtmosphereParameters>& parameters);
    bool areaSurfaceSource();
    void onAreaSurface(bool state);
    EmissiveColor areaColorSource();
    void onAreaColor(const EmissiveColor& color);
    bool areaOpaqueSource();
    void onAreaOpaque(bool state);
    f64 areaAnimationSpeedSource();
    void onAreaAnimationSpeed(f64 speed);

    // Rope
    u32 ropeSegmentCountSource();
    void onRopeSegmentCount(u32 count);
    f64 ropeLengthSource();
    void onRopeLength(f64 length);
    f64 ropeRadiusSource();
    void onRopeRadius(f64 radius);
    bool ropeThinSource();
    void onRopeThin(bool state);
    EmissiveColor ropeStartColorSource();
    void onRopeStartColor(const EmissiveColor& color);
    EmissiveColor ropeEndColorSource();
    void onRopeEndColor(const EmissiveColor& color);
    vector<string> ropeConstraintSubpartOptionsSource();
    size_t ropeConstraintSubpartSource();
    void onRopeConstraintSubpart(size_t index);

    Entity* activeEntity() const;
    BodyPart* activePart() const;

    vec3 selectionCenter() const;

    mat4 toSpaceMatrix() const;
    mat4 fromSpaceMatrix() const;

    void undo(const Input& input);
    void redo(const Input& input);

    SizeProperties sizeProperties() const override;
};
