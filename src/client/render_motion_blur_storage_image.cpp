#include "render_motion_blur_storage_image.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"

RenderMotionBlurStorageImage::RenderMotionBlurStorageImage(const RenderContext* ctx, u32 width, u32 height, u32 layers)
    : ctx(ctx)
    , layers(layers)
    , layoutReady(false)
{
    image = createImage(
        ctx->allocator,
        { width, height, 1 },
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_USAGE_STORAGE_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        false,
        1,
        layers
    );

    imageView = createImageView(
        ctx->device,
        image.image,
        colorFormat,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_VIEW_TYPE_2D_ARRAY,
        1,
        layers
    );
}

RenderMotionBlurStorageImage::~RenderMotionBlurStorageImage()
{
    destroyImageView(ctx->device, imageView);
    destroyImage(ctx->allocator, image);
}

void RenderMotionBlurStorageImage::setupLayout(VkCommandBuffer commandBuffer)
{
    if (layoutReady)
    {
        return;
    }

    transitionLayout(
        commandBuffer,
        image.image,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_GENERAL,
        layers
    );

    VkClearColorValue color{};

    VkImageSubresourceRange range{};
    range.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    range.baseMipLevel = 0;
    range.levelCount = 1;
    range.baseArrayLayer = 0;
    range.layerCount = layers;

    vkCmdClearColorImage(
        commandBuffer,
        image.image,
        VK_IMAGE_LAYOUT_GENERAL,
        &color,
        1,
        &range
    );

    layoutReady = true;
}

void RenderMotionBlurStorageImage::barrier(VkCommandBuffer commandBuffer)
{
    transitionLayout(
        commandBuffer,
        image.image,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_IMAGE_LAYOUT_GENERAL,
        VK_IMAGE_LAYOUT_GENERAL,
        layers
    );
}
