/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "list_view.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

ListView::ListView(
    InterfaceWidget* parent,
    const function<void(i32)>& onSelect,
    const optional<Color>& color
)
    : Layout(parent)
    , onSelect(onSelect)
    , color(color)
    , scrollbarEnabled(true)
    , _activeIndex(0)
{
    START_WIDGET_SCOPE("list-view")
        WIDGET_SLOT("interact", interact)
        WIDGET_SLOT("scroll-up", scrollUp)
        WIDGET_SLOT("scroll-down", scrollDown)
        WIDGET_SLOT("go-to-start", goToStart)
        WIDGET_SLOT("go-to-end", goToEnd)

        START_INDEPENDENT_SCOPE("thumb-grabbed", thumbGrabbed)
            WIDGET_SLOT("drag", drag)
            WIDGET_SLOT("release", release)
        END_SCOPE
    END_SCOPE
}

void ListView::move(const Point& position)
{
    this->position(position);

    Point p = position;
    p.y -= elapsedHeight();

    for (InterfaceWidget* child : children())
    {
        child->move(p);

        p.y += child->size().h;
    }
}

void ListView::resize(const Size& size)
{
    this->size(size);

    Size available = size;

    if (scrollbarEnabled)
    {
        available.w -= UIScale::scrollbarWidth();
    }

    for (InterfaceWidget* child : children())
    {
        i32 w = 0;

        switch (sizePropertiesOf(child).horizontal)
        {
        case Scaling_Fixed :
            w = sizePropertiesOf(child).w;
            break;
        case Scaling_Fraction :
            w = sizePropertiesOf(child).w * available.w;
            break;
        case Scaling_Fill :
            w = available.w;
            break;
        }

        i32 h = 0;

        switch (sizePropertiesOf(child).vertical)
        {
        case Scaling_Fixed :
            h = sizePropertiesOf(child).h;
            break;
        case Scaling_Fraction :
            h = sizePropertiesOf(child).h * available.h;
            break;
        case Scaling_Fill :
            h = available.h;
            break;
        }

        child->resize({ w, h });
    }
}

void ListView::draw(const DrawContext& ctx) const
{
    if (color)
    {
        drawSolid(
            ctx,
            this,
            *color
        );
    }

    if (scrollbarEnabled)
    {
        drawSolid(
            ctx,
            this,
            Point(size().w - UIScale::scrollbarWidth(), 0),
            Size(UIScale::scrollbarWidth(), size().h),
            theme.softIndentColor
        );

        if (needsScroll())
        {
            drawSolid(
                ctx,
                this,
                Point(size().w - UIScale::scrollbarWidth(), thumbY()),
                Size(UIScale::scrollbarWidth(), thumbHeight()),
                theme.outdentColor
            );
        }
    }

    InterfaceWidget::draw(ctx);
}

void ListView::setScrollbarEnabled(bool state)
{
    scrollbarEnabled = state;

    update();
}

i32 ListView::activeIndex() const
{
    return _activeIndex;
}

i32 ListView::viewHeight() const
{
    return size().h;
}

i32 ListView::scrollIndex() const
{
    return _activeIndex;
}

void ListView::setScrollIndex(i32 index)
{
    _activeIndex = index;
    update();
}

i32 ListView::childCount() const
{
    return children().size();
}

i32 ListView::childHeight() const
{
    return children().empty() ? 0 : children().front()->size().h;
}

void ListView::interact(const Input& input)
{
    Point local = pointer - position();

    if (local.x < size().w - (scrollbarEnabled ? UIScale::scrollbarWidth() : 0))
    {
        i32 y = 0;
        for (i32 i = _activeIndex; i < static_cast<i32>(children().size()); i++)
        {
            InterfaceWidget* child = children()[i];

            y += child->size().h;

            if (y > local.y && onSelect)
            {
                onSelect(i);
                break;
            }
        }
    }
    else
    {
        if (local.y >= thumbY() && local.y < thumbY() + thumbHeight())
        {
            grabThumb(local);
        }
        else
        {
            setScrollIndexFromThumbY(local.y);
        }
    }
}

void ListView::scrollUp(const Input& input)
{
    if (_activeIndex > 0)
    {
        _activeIndex--;
        update();
    }
}

void ListView::scrollDown(const Input& input)
{
    if (_activeIndex < maxScrollIndex())
    {
        _activeIndex++;
        update();
    }
}

void ListView::goToStart(const Input& input)
{
    _activeIndex = 0;
    update();
}

void ListView::goToEnd(const Input& input)
{
    _activeIndex = maxScrollIndex();
    update();
}

void ListView::release(const Input& input)
{
    releaseThumb();
}

void ListView::drag(const Input& input)
{
    Point local = pointer - position();

    dragThumb(local);
}

SizeProperties ListView::sizeProperties() const
{
    return sizePropertiesFillAll;
}
