/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "interface_window_view.hpp"
#include "tools.hpp"
#include "theme.hpp"
#include "global.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "interface.hpp"
#include "flat_render.hpp"

// Fix for Windows namespace pollution
#undef interface

InterfaceWindowView::InterfaceWindowView(Interface* interface, const string& name)
    : InterfaceView(interface, name)
    , _size(
        dynamic_cast<FlatRender*>(interface->context()->controller()->render())->width(),
        dynamic_cast<FlatRender*>(interface->context()->controller()->render())->height()
    )
{

}

InterfaceWindowView::~InterfaceWindowView()
{
    vector<InterfaceWidget*> children = _children;

    for (InterfaceWidget* child : children)
    {
        delete child;
    }
}

void InterfaceWindowView::step()
{
    if (!tooltipActive)
    {
        tooltip = "";
    }
    else
    {
        tooltipActive = false;
    }

    for (InterfaceWidget* child : _children)
    {
        if (!dynamic_cast<InterfaceWindow*>(child)->shouldDraw())
        {
            continue;
        }

        child->step();
    }
}

void InterfaceWindowView::draw(const DrawContext& ctx)
{
    for (InterfaceWidget* child : _children)
    {
        if (!dynamic_cast<InterfaceWindow*>(child)->shouldDraw())
        {
            continue;
        }

        child->draw(ctx);

        for (const function<void(const DrawContext&)>& delayedCommand : ctx.delayedCommands)
        {
            delayedCommand(ctx);
        }
        ctx.delayedCommands.clear();
    }

    if (tooltipShown())
    {
        drawTooltip(ctx);
    }
}

void InterfaceWindowView::resize(const Size& size)
{
    Size oldSize = _size;
    _size = size;

    for (InterfaceWidget* child : _children)
    {
        if (g_config.uiAutoScale)
        {
            Point newChildPosition = (child->position().toVec2() / oldSize.toVec2()) * size.toVec2();
            Size newChildSize = (child->size().toVec2() / oldSize.toVec2()) * size.toVec2();

            child->resize(newChildSize);
            child->move(newChildPosition + (newChildSize / 2));
        }
        else
        {
            Point newChildPosition = (child->position().toVec2() / oldSize.toVec2()) * size.toVec2();
            Size childSize = child->size();

            child->resize(childSize);
            child->move(newChildPosition + (childSize / 2));
        }
    }
}

const Size& InterfaceWindowView::size() const
{
    return _size;
}

void InterfaceWindowView::sort()
{
    InterfaceWindow* focusedWindow = nullptr;

    for (size_t i = 0; i < _children.size(); i++)
    {
        auto window = dynamic_cast<InterfaceWindow*>(_children[i]);

        if (!window->focused())
        {
            continue;
        }

        focusedWindow = window;
        _children.erase(_children.begin() + i);
        break;
    }

    if (!focusedWindow)
    {
        return;
    }

    _children.push_back(focusedWindow);
}

void InterfaceWindowView::unfocus()
{
    for (InterfaceWidget* child : _children)
    {
        child->clearFocus();
    }
}

void InterfaceWindowView::clean()
{
    bool destroyedLast = false;

    size_t i = 0;
    while (true)
    {
        vector<InterfaceWidget*> children = _children;

        for (; i < children.size(); i++)
        {
            InterfaceWidget* child = children[i];

            if (!dynamic_cast<InterfaceWindow*>(child)->shouldDestroy())
            {
                continue;
            }

            delete child;
            break;
        }

        if (i + 1 == children.size())
        {
            destroyedLast = true;
            break;
        }
        else if (i == children.size())
        {
            break;
        }
    }

    if (destroyedLast)
    {
        interface()->updateWindowFocus();
    }
}

void InterfaceWindowView::setTooltip(const string& tooltip, const Point& position)
{
    tooltipActive = true;

    if (tooltip != this->tooltip)
    {
        tooltipStartTime = currentTime();
    }

    this->tooltip = tooltip;

    if (!tooltipReady())
    {
        tooltipPosition = position;
    }
}

bool InterfaceWindowView::tooltipShown() const
{
    return tooltipActive && tooltipReady();
}

bool InterfaceWindowView::tooltipReady() const
{
    return currentTime() - tooltipStartTime >= tooltipDelay;
}

void InterfaceWindowView::drawTooltip(const DrawContext& ctx)
{
    TextStyle style;
    style.alignment = Text_Center;

    i32 width = style.getFont()->width(tooltip) + UIScale::marginSize() * 2;

    Point point = tooltipPosition + Point(0, cursorHeight);
    Size size(width, UIScale::tooltipHeight());

    i32 minX = point.x;
    i32 maxX = point.x + size.w;
    i32 minY = point.y;
    i32 maxY = point.y + size.h;

    if (minX < 0)
    {
        point.x += abs(minX);
    }

    if (maxX > ctx.size.w)
    {
        point.x -= maxX - ctx.size.w;
    }

    if (minY < 0)
    {
        point.y += abs(minY);
    }

    if (maxY > ctx.size.h)
    {
        point.y -= maxY - ctx.size.h;
    }

    drawSolid(
        ctx,
        nullptr,
        point,
        size,
        theme.outdentColor
    );

    drawText(
        ctx,
        nullptr,
        point,
        size,
        tooltip,
        style
    );
}
