/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "save_indicator.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "tools.hpp"
#include "control_context.hpp"

static constexpr f64 cycleDuration = 1;

SaveIndicator::SaveIndicator(InterfaceView* view)
    : InterfaceWidget(view)
    , saving(false)
    , startTime(0)
{

}

void SaveIndicator::step()
{
    InterfaceWidget::step();

    if (context()->controller()->serverSaving() && !saving)
    {
        saving = true;
        startTime = currentTime();
    }
    else if (!context()->controller()->serverSaving() && saving)
    {
        saving = false;
        startTime = currentTime();
    }
}

void SaveIndicator::draw(const DrawContext& ctx) const
{
    f64 elapsed = (currentTime() - startTime).count() / 1000.0;

    f64 alpha;

    if (saving)
    {
        f64 progress = elapsed - floor(elapsed / cycleDuration) * cycleDuration;

        if (progress <= 0.5)
        {
            alpha = progress / 0.5;
        }
        else
        {
            alpha = 1 - ((progress - 0.5) / 0.5);
        }
    }
    else
    {
        f64 progress = min(elapsed / (cycleDuration / 2), 1.0);

        alpha = 1 - progress;
    }

    drawImage(
        ctx,
        this,
        Icon_Saving,
        alpha
    );
}

PositionProperties SaveIndicator::positionProperties() const
{
    return {
        static_cast<f64>(UIScale::marginSize()), static_cast<f64>(UIScale::marginSize()),
        Positioning_Fixed_Inverted, Positioning_Fixed
    };
}

SizeProperties SaveIndicator::sizeProperties() const
{
    return {
        static_cast<f64>(UIScale::saveIndicatorSize().w), static_cast<f64>(UIScale::saveIndicatorSize().h),
        Scaling_Fixed, Scaling_Fixed
    };
}
