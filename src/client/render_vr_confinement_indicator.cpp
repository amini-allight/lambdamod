/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_confinement_indicator.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "theme.hpp"

static const string meshFilePath = meshPath + "/confinement-indicator" + meshExt;

RenderVRConfinementIndicator::RenderVRConfinementIndicator(
    const RenderContext* ctx,

    vector<VRSwapchainElement*> swapchainElements,

    VkDescriptorSetLayout descriptorSetLayout,
    RenderVRPipelineStore* pipelineStore,
    const vec3& position,
    f64 radius,
    f64 strength
)
    : RenderVRDecoration(ctx, descriptorSetLayout, pipelineStore->confinementIndicator.pipelineLayout, pipelineStore->confinementIndicator.pipeline)
    , strength(strength)
{
    transform = { position, quaternion(), vec3(radius) };

    string mesh = getFile(meshFilePath);

    vertexCount = mesh.size() / sizeof(fvec3);
    vertices = createVertexBuffer(mesh.size());

    VmaMapping<f32> mapping(ctx->allocator, vertices);

    memcpy(mapping.data, mesh.data(), mesh.size());

    createComponents(swapchainElements);
}

RenderVRConfinementIndicator::~RenderVRConfinementIndicator()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, vertices);
}

void RenderVRConfinementIndicator::work(const VRSwapchainElement* swapchainElement, vec3 cameraPosition[eyeCount]) const
{
    RenderVRDecoration::work(swapchainElement, cameraPosition);

    fillCommandBuffer(
        swapchainElement,
        components.at(swapchainElement->imageIndex())->descriptorSet,
        vertices.buffer,
        vertexCount
    );
}

VmaBuffer RenderVRConfinementIndicator::createUniformBuffer() const
{
    VmaBuffer uniform = RenderDecoration::createUniformBuffer(sizeof(fmat4) * eyeCount + sizeof(fvec4) * 4 + sizeof(f32));

    VmaMapping<fvec4> mapping(ctx->allocator, uniform);

    mapping.data[8] = theme.accentGradientXStartColor.toVec4();
    mapping.data[9] = theme.accentGradientXEndColor.toVec4();
    mapping.data[10] = theme.accentGradientYStartColor.toVec4();
    mapping.data[11] = theme.accentGradientYEndColor.toVec4();

    reinterpret_cast<f32*>(mapping.data)[48] = strength;

    return uniform;
}
#endif
