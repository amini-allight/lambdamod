/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

class HRIR
{
public:
    HRIR();

    void apply(vector<f32>& lo, vector<f32>& hi, i32 ear, const vec3& dir) const;

    static constexpr i32 earCount = 2;
    static constexpr i32 azimuthCount = 25;
    static constexpr i32 elevationCount = 50;
    static constexpr i32 sampleCount = 218;

    array<array<array<array<f32, sampleCount>, elevationCount>, azimuthCount>, earCount> data;

    array<f32, sampleCount> at(i32 ear, const vec3& dir) const;
};

HRIR* loadHRIR(const string& path);
