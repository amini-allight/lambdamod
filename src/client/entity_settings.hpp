/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "right_clickable_widget.hpp"
#include "tab_content.hpp"

#include "line_edit.hpp"
#include "tree_view.hpp"

class Entity;

struct EntityDetail
{
    EntityDetail();

    bool operator==(const EntityDetail& rhs) const;
    bool operator!=(const EntityDetail& rhs) const;

    EntityID id;
    string name;
    vector<EntityDetail> children;
};

class EntitySettings : public RightClickableWidget, public TabContent
{
public:
    EntitySettings(InterfaceWidget* parent);

    void step() override;

private:
    vector<EntityDetail> details;
    string search;
    LineEdit* searchBar;
    TreeView* treeView;

    vector<EntityDetail> convert(const map<EntityID, Entity*>& entities) const;

    void openRightClickMenu(i32 index, const Point& pointer) override;

    void onOpen(EntityID id, const Point& point);
    void onRemove(EntityID id, const Point& point);
    void onShift(EntityID id, const Point& point);
    void onCursorToEntity(EntityID id, const Point& point);
    void onEntityToCursor(EntityID id, const Point& point);

    void updateTreeView();
    void createNodes(InterfaceWidget* parent, const vector<EntityDetail>& details, const map<string, bool>& expanded);

    SizeProperties sizeProperties() const override;
};
