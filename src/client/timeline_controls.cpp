/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "timeline_controls.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "row.hpp"
#include "column.hpp"
#include "spacer.hpp"
#include "icon_button.hpp"
#include "dynamic_icon_button.hpp"

TimelineControls::TimelineControls(
    InterfaceWidget* parent,
    const function<bool()>& playing,
    const function<void()>& onGoToStart,
    const function<void()>& onPlayPause,
    const function<void()>& onStop,
    const function<void()>& onGoToEnd
)
    : InterfaceWidget(parent)
    , playing(playing)
    , onPlayPause(onPlayPause)
{
    START_WIDGET_SCOPE("timeline-controls")
        WIDGET_SLOT("toggle-play", togglePlay)
    END_SCOPE

    auto column = new Column(this);

    MAKE_SPACER(column, 0, UIScale::marginSize());
    auto row = new Row(column);
    MAKE_SPACER(column, 0, UIScale::marginSize());

    MAKE_SPACER(row, 0, UIScale::iconButtonSize().h);
    new IconButton(row, Icon_Go_To_Start, onGoToStart);
    MAKE_SPACER(row, UIScale::timelineEditorButtonSpacing(), UIScale::iconButtonSize().h);
    new IconButton(row, Icon_Stop, onStop);
    MAKE_SPACER(row, UIScale::timelineEditorButtonSpacing(), UIScale::iconButtonSize().h);
    new DynamicIconButton(
        row,
        bind(&TimelineControls::playPauseIconSource, this),
        onPlayPause
    );
    MAKE_SPACER(row, UIScale::timelineEditorButtonSpacing(), UIScale::iconButtonSize().h);
    new IconButton(row, Icon_Go_To_End, onGoToEnd);
    MAKE_SPACER(row, 0, UIScale::iconButtonSize().h);
}

Icon TimelineControls::playPauseIconSource()
{
    return playing() ? Icon_Pause : Icon_Play;
}

void TimelineControls::togglePlay(const Input& input)
{
    onPlayPause();
}

SizeProperties TimelineControls::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::marginSize() * 2 + UIScale::iconButtonSize().h),
        Scaling_Fill, Scaling_Fixed
    };
}
