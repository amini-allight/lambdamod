/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_decoration.hpp"
#include "render_tools.hpp"
#include "render_descriptor_set_write_components.hpp"

RenderDecoration::RenderDecoration(
    const RenderContext* ctx,
    VkDescriptorSetLayout descriptorSetLayout,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline
)
    : _shown(true)
    , ctx(ctx)
    , descriptorSetLayout(descriptorSetLayout)
    , pipelineLayout(pipelineLayout)
    , pipeline(pipeline)
{

}

RenderDecoration::~RenderDecoration()
{

}

void RenderDecoration::show(bool state)
{
    _shown = state;
}

bool RenderDecoration::shown() const
{
    return _shown;
}

RenderDecorationType RenderDecoration::drawType() const
{
    return Render_Decoration_Default;
}

VmaBuffer RenderDecoration::createVertexBuffer(size_t size) const
{
    return createBuffer(
        ctx->allocator,
        size,
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VmaBuffer RenderDecoration::createUniformBuffer(size_t size) const
{
    return createBuffer(
        ctx->allocator,
        size,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderDecoration::destroyComponents()
{
    for (const RenderDecorationComponent* component : components)
    {
        delete component;
    }

    components.clear();
}
