/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.
LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_atmosphere_component.hpp"
#include "render_tools.hpp"
#include "render_atmosphere.hpp"
#include "render_descriptor_set_write_components.hpp"

RenderAtmosphereComponent::RenderAtmosphereComponent(
    const RenderInterface* render,
    VkDescriptorSetLayout descriptorSetLayout,
    VmaBuffer camera,
    VmaBuffer lightningVertices
)
    : render(render)
    , descriptorSetLayout(descriptorSetLayout)
    , camera(camera)
{
    uniform = createUniformBuffer(sizeof(AtmosphereLightningGPU));
    uniformMapping = new VmaMapping<AtmosphereLightningGPU>(render->context()->allocator, uniform);
    particles = createStorageBuffer(atmosphereLightningMaxParticleCount * sizeof(AtmosphereLightningParticleGPU));
    particlesMapping = new VmaMapping<AtmosphereLightningParticleGPU>(render->context()->allocator, particles);
    descriptorSet = createDescriptorSet(camera, lightningVertices);
    // Effects are created upon first prepare
}

RenderAtmosphereComponent::~RenderAtmosphereComponent()
{
    destroyEffects();
    destroyDescriptorSet(render->context()->device, render->context()->descriptorPool, descriptorSet);
    delete particlesMapping;
    destroyBuffer(render->context()->allocator, particles);
    delete uniformMapping;
    destroyBuffer(render->context()->allocator, uniform);
}

void RenderAtmosphereComponent::update(
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene,
    const AtmosphereParameters& atmosphere,
    vec3 cameraPosition[eyeCount],
    const vec3& gravity,
    const vec3& flowVelocity,
    f64 density
)
{
    if (atmosphere.effects.size() != effects.size())
    {
        refreshEffects(environmentImageView, occlusionScene, atmosphere.effects.size());
    }

    for (size_t i = 0; i < atmosphere.effects.size(); i++)
    {
        const AtmosphereEffect& inEffect = atmosphere.effects.at(i);
        RenderAtmosphereComponentEffect* outEffect = effects.at(i);

        outEffect->fillUniformBuffer(
            inEffect,
            cameraPosition,
            gravity,
            flowVelocity,
            density
        );
    }
}

void RenderAtmosphereComponent::refreshEffects(
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene,
    size_t count
)
{
    destroyEffects();
    createEffects(environmentImageView, occlusionScene, count);
}

VmaBuffer RenderAtmosphereComponent::createUniformBuffer(size_t size) const
{
    return createBuffer(
        render->context()->allocator,
        size,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VmaBuffer RenderAtmosphereComponent::createStorageBuffer(size_t size) const
{
    return createBuffer(
        render->context()->allocator,
        size,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

VkDescriptorSet RenderAtmosphereComponent::createDescriptorSet(VmaBuffer camera, VmaBuffer vertices) const
{
    VkDescriptorSet descriptorSet = ::createDescriptorSet(
        render->context()->device,
        render->context()->descriptorPool,
        descriptorSetLayout
    );

    VkDescriptorBufferInfo cameraWriteBuffer{};
    VkWriteDescriptorSet cameraWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, camera.buffer, &cameraWriteBuffer, &cameraWrite);

    VkDescriptorBufferInfo objectWriteBuffer{};
    VkWriteDescriptorSet objectWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 1, uniform.buffer, &objectWriteBuffer, &objectWrite);

    VkDescriptorBufferInfo particlesWriteBuffer{};
    VkWriteDescriptorSet particlesWrite{};

    storageBufferDescriptorSetWrite(descriptorSet, 2, particles.buffer, &particlesWriteBuffer, &particlesWrite);

    VkDescriptorBufferInfo verticesWriteBuffer{};
    VkWriteDescriptorSet verticesWrite{};

    storageBufferDescriptorSetWrite(descriptorSet, 3, vertices.buffer, &verticesWriteBuffer, &verticesWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        cameraWrite,
        objectWrite,
        particlesWrite,
        verticesWrite
    };

    vkUpdateDescriptorSets(
        render->context()->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );

    return descriptorSet;
}

void RenderAtmosphereComponent::createEffects(
    VkImageView environmentImageView,
    RenderOcclusionSceneReference occlusionScene,
    size_t count
)
{
    for (size_t i = 0; i < count; i++)
    {
        effects.push_back(new RenderAtmosphereComponentEffect(
            render,
            descriptorSetLayout,
            camera,
            environmentImageView,
            occlusionScene
        ));
    }
}

void RenderAtmosphereComponent::destroyEffects()
{
    for (RenderAtmosphereComponentEffect* effect : effects)
    {
        delete effect;
    }

    effects.clear();
}
