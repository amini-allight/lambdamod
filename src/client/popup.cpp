/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "popup.hpp"
#include "interface_window.hpp"
#include "control_context.hpp"

Popup::Popup(InterfaceWidget* parent)
    : InterfaceWidget(parent)
    , _open(false)
{
    setScope<WidgetInputScope>(
        this,
        context()->input()->getScope("game"),
        "popup",
        Input_Scope_Blocking,
        [](const Input& input) -> bool { return true; },
        [&](InputScope* parent) -> void {}
    );
}

Popup::~Popup()
{
    if (auto window = dynamic_cast<InterfaceWindow*>(rootParent()); window && _open)
    {
        window->decrementPopupCount();
    }
}

void Popup::open()
{
    if (auto window = dynamic_cast<InterfaceWindow*>(rootParent()); window && !_open)
    {
        window->incrementPopupCount();
    }

    _open = true;
}

void Popup::close()
{
    if (auto window = dynamic_cast<InterfaceWindow*>(rootParent()); window && _open)
    {
        window->decrementPopupCount();
    }

    _open = false;
}

bool Popup::opened() const
{
    return _open;
}

bool Popup::movePointer(PointerType type, const Point& pointer, bool consumed)
{
    this->pointer = pointer;

    if (!focused() && contains(pointer) && shouldDraw() && !consumed)
    {
        focus();
    }
    else if (focused() && (!contains(pointer) || !shouldDraw() || consumed))
    {
        unfocus();
    }

    return focused();
}

bool Popup::shouldDraw() const
{
    return InterfaceWidget::shouldDraw() && _open;
}
