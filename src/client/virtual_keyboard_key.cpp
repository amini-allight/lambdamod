/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "virtual_keyboard_key.hpp"
#include "theme.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"
#include "virtual_keyboard.hpp"

VirtualKeyboardKey::VirtualKeyboardKey(
    InterfaceWidget* parent,
    const function<string()>& nameSource,
    InputType input,
    f64 width
)
    : InterfaceWidget(parent)
    , nameSource(nameSource)
    , input(input)
    , _width(width)
{
    START_WIDGET_SCOPE("virtual-keyboard-key")
        WIDGET_SLOT("interact", interact)
    END_SCOPE
}

f64 VirtualKeyboardKey::width() const
{
    return _width;
}

void VirtualKeyboardKey::draw(const DrawContext& ctx) const
{
    Color color = focused() ? theme.accentColor : theme.panelForegroundColor;

    drawSolid(
        ctx,
        this,
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        UIScale::marginSize(this),
        color
    );

    TextStyle style(this);
    style.font = Text_Sans_Serif;
    style.size = UIScale::vrKeyboardFontSize(this);
    style.weight = Text_Extra_Bold;
    style.alignment = Text_Center;
    style.color = color;

    drawText(
        ctx,
        this,
        nameSource(),
        style
    );
}

void VirtualKeyboardKey::interact(const Input& input)
{
    dynamic_cast<VirtualKeyboard*>(parent())->pressKey(this->input);
}

SizeProperties VirtualKeyboardKey::sizeProperties() const
{
    // Unused, resized by custom layout above
    return sizePropertiesFillAll;
}
