/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "input_handling_result_entry.hpp"

class ControlContext;

class InputHandlingResult
{
public:
    InputHandlingResult(ControlContext* ctx, const Input& input);

    void setBlocking(bool state);

    void merge(const InputHandlingResult& child);
    void add(const vector<string>& path, const InputScopeBinding& binding, const InputScopeSlot& slot, size_t depth);

    bool commit() const;

private:
    ControlContext* ctx;
    Input input;
    bool blocking;
    vector<InputHandlingResultEntry> entries;
};
