/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "voice_encoder.hpp"
#include "sound_constants.hpp"
#include "log.hpp"
#include "voice_tools.hpp"

#include <opus/opus.h>

VoiceEncoder::VoiceEncoder()
    : encoder(nullptr)
{
    i32 status;

    encoder = opus_encoder_create(voiceFrequency, voiceChannels, OPUS_APPLICATION_VOIP, &status);

    if (status != OPUS_OK)
    {
        fatal("Failed to initialize voice encoder: " + opusError(status));
        return;
    }

    status = opus_encoder_ctl(static_cast<OpusEncoder*>(encoder), OPUS_SET_BITRATE(voiceBitrate));

    if (status != OPUS_OK)
    {
        error("Failed to set voice encoder bitrate: " + opusError(status));
    }
}

VoiceEncoder::~VoiceEncoder()
{
    if (encoder)
    {
        opus_encoder_destroy(static_cast<OpusEncoder*>(encoder));
    }
}

string VoiceEncoder::encode(const string& raw)
{
    string encoded(raw.size() * 2, '\0');

    i32 bytesEncoded = opus_encode(
        static_cast<OpusEncoder*>(encoder),

        reinterpret_cast<const i16*>(raw.data()),
        raw.size() / (sizeof(i16) * voiceChannels),

        reinterpret_cast<u8*>(encoded.data()),
        encoded.size()
    );

    if (bytesEncoded < 0)
    {
        error("Failed to encode voice: " + opusError(bytesEncoded));
        return "";
    }

    return string(encoded.data(), bytesEncoded);
}
