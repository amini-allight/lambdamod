/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "panel_option_select.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "interface_window.hpp"

// Empty placeholder to fulfill the requirements of instantiating OptionSelectPopup
static constexpr vector<string> optionTooltips;

PanelOptionSelectPopup::PanelOptionSelectPopup(
    InterfaceWidget* parent,
    const vector<string>& options,
    const function<void(size_t)>& onSet
)
    : OptionSelectPopup(parent, options, ::optionTooltips, onSet)
{

}

void PanelOptionSelectPopup::draw(const DrawContext& ctx) const
{
    Point contentPosition = Point(UIScale::panelBorderSize(this));
    Size contentSize = size() - UIScale::panelBorderSize(this) * 2;

    ctx.delayedCommands.push_back([this, contentPosition, contentSize](const DrawContext& ctx) -> void {
        drawSolid(
            ctx,
            this,
            theme.worldBackgroundColor
        );

        drawBorder(
            ctx,
            this,
            UIScale::panelBorderSize(this),
            focused() ? theme.accentColor : theme.panelForegroundColor
        );

        if (needsScroll())
        {
            i32 scrollbarLeft = contentSize.w - UIScale::panelScrollbarWidth(this);

            drawSolid(
                ctx,
                this,
                contentPosition + Point(
                    scrollbarLeft + (UIScale::panelScrollbarWidth(this) - UIScale::panelScrollbarTrackWidth(this)) / 2,
                    0
                ),
                Size(UIScale::panelScrollbarTrackWidth(this), contentSize.h),
                theme.panelForegroundColor
            );

            drawSolid(
                ctx,
                this,
                contentPosition + Point(
                    scrollbarLeft + (UIScale::panelScrollbarWidth(this) - UIScale::panelScrollbarThumbWidth(this)) / 2,
                    thumbY()
                ),
                Size(UIScale::panelScrollbarThumbWidth(this), thumbHeight()),
                theme.panelForegroundColor
            );
        }
    });

    i32 maxRows = static_cast<i32>(ceil(size().h / static_cast<f64>(UIScale::optionSelectEntryHeight())));

    for (i32 i = _scrollIndex; i < static_cast<i32>(options.size()) && i - _scrollIndex < maxRows; i++)
    {
        i32 y = (i - _scrollIndex) * UIScale::optionSelectEntryHeight();

        ctx.delayedCommands.push_back([this, contentPosition, contentSize, i, y](const DrawContext& ctx) -> void {
            TextStyle style(this);
            style.size = UIScale::largeFontSize(this);
            style.color = focused() && i == activeIndex ? theme.accentColor : theme.panelForegroundColor;

            drawText(
                ctx,
                this,
                contentPosition + Point(0, y),
                Size(contentSize.w - UIScale::panelScrollbarWidth(this), UIScale::optionSelectEntryHeight()),
                options[i],
                style
            );
        });
    }
}

i32 PanelOptionSelectPopup::viewHeight() const
{
    return size().h - UIScale::panelBorderSize(this) * 2;
}

i32 PanelOptionSelectPopup::childHeight() const
{
    return UIScale::optionSelectEntryHeight();
}

PanelOptionSelect::PanelOptionSelect(
    InterfaceWidget* parent,
    const function<vector<string>()>& optionsSource,
    const function<size_t()>& source,
    const function<void(size_t)>& onSet
)
    : OptionSelect(parent, optionsSource, source, onSet)
{
    delete popup;
    popup = new PanelOptionSelectPopup(
        nullptr,
        options,
        bind(&PanelOptionSelect::onSelect, this, placeholders::_1)
    );
}

void PanelOptionSelect::draw(const DrawContext& ctx) const
{
    drawSolid(
        ctx,
        this,
        theme.worldBackgroundColor
    );

    drawBorder(
        ctx,
        this,
        UIScale::panelBorderSize(this),
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    TextStyle style(this);
    style.size = UIScale::largeFontSize(this);
    style.color = focused() ? theme.accentColor : theme.panelForegroundColor;

    drawText(
        ctx,
        this,
        Point(UIScale::panelBorderSize(this)),
        Size(
            size().w - (UIScale::panelIconButtonSize(this).w + (UIScale::panelBorderSize(this) * 2)),
            size().h - (UIScale::panelBorderSize(this) * 2)
        ),
        index < options.size() ? options[index] : "",
        style
    );

    drawColoredImage(
        ctx,
        this,
        Point(size().w - UIScale::panelIconButtonSize(this).w, 0),
        UIScale::panelIconButtonSize(this),
        Icon_Down,
        focused() ? theme.accentColor : theme.panelForegroundColor
    );

    if (popup->opened())
    {
        popup->draw(ctx);
    }
}

SizeProperties PanelOptionSelect::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::panelOptionSelectHeight(this)),
        Scaling_Fill, Scaling_Fixed
    };
}
