/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "slider.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "control_context.hpp"

Slider::Slider(
    InterfaceWidget* parent,
    const function<f64()>& source,
    const function<void(f64)>& onSet
)
    : InterfaceWidget(parent)
    , _value(source ? source() : 0)
    , source(source)
    , onSet(onSet)
    , grabbed(false)
    , grabValueStart(_value)
{
    START_WIDGET_SCOPE("slider")
        WIDGET_SLOT("interact", interact)

        START_INDEPENDENT_SCOPE("grabbed", grabbed)
            WIDGET_SLOT("drag", drag)
            WIDGET_SLOT("release", release)
        END_SCOPE
    END_SCOPE
}

void Slider::step()
{
    InterfaceWidget::step();

    if (source && !focused())
    {
        _value = source();
    }
}

void Slider::draw(const DrawContext& ctx) const
{
    InterfaceWidget::draw(ctx);

    i32 trackWidth = size().w - UIScale::sliderThumbSize();

    drawSolid(
        ctx,
        this,
        Point(UIScale::sliderThumbSize() / 2, (UIScale::sliderHeight() - UIScale::sliderTrackHeight()) / 2),
        Size(trackWidth, UIScale::sliderTrackHeight()),
        theme.indentColor
    );

    drawSolid(
        ctx,
        this,
        Point(_value * trackWidth, (UIScale::sliderHeight() - UIScale::sliderThumbSize()) / 2),
        Size(UIScale::sliderThumbSize()),
        grabbed ? theme.accentColor : theme.outdentColor
    );
}

void Slider::focus()
{
    playFocusEffect();
    InterfaceWidget::focus();
}

void Slider::unfocus()
{
    playUnfocusEffect();
    InterfaceWidget::unfocus();
}

f64 Slider::value() const
{
    return _value;
}

void Slider::interact(const Input& input)
{
    playPositiveActivateEffect();
    grabbed = true;
    grabPointerStart = pointer;
    grabValueStart = _value;
}

void Slider::drag(const Input& input)
{
    i32 trackWidth = size().w - UIScale::sliderThumbSize();

    if (trackWidth == 0)
    {
        return;
    }

    _value = grabValueStart + ((pointer.x - grabPointerStart.x) / static_cast<f64>(trackWidth));
    _value = clamp(_value, 0.0, 1.0);
    onSet(_value);
}

void Slider::release(const Input& input)
{
    playNegativeActivateEffect();
    grabbed = false;
}

SizeProperties Slider::sizeProperties() const
{
    return {
        0, static_cast<f64>(UIScale::sliderHeight()),
        Scaling_Fill, Scaling_Fixed
    };
}
