/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_objects.hpp"
#include "render_tools.hpp"
#include "render_constants.hpp"
#include "render_context.hpp"
#include "render_pipeline_components.hpp"
#include "render_descriptor_set_components.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "global.hpp"
#include "image.hpp"
#include "perlin_noise.hpp"

#ifdef LMOD_VK_VALIDATION
void createInstance(VkInstance& instance, VkDebugUtilsMessengerEXT& debugMessenger, u32 apiVersion, const set<string>& extensionNames)
#else
void createInstance(VkInstance& instance, u32 apiVersion, const set<string>& extensionNames)
#endif
{
    VkResult result;

    VkApplicationInfo appInfo{};
    appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    appInfo.pApplicationName = gameName;
    appInfo.applicationVersion = VK_MAKE_API_VERSION(0, version.major, version.minor, version.patch);
    appInfo.pEngineName = gameName;
    appInfo.engineVersion = VK_MAKE_API_VERSION(0, version.major, version.minor, version.patch);
    appInfo.apiVersion = apiVersion;

#ifdef LMOD_VK_VALIDATION
    set<string> allExtensionNames = { "VK_EXT_debug_utils" };
#else
    set<string> allExtensionNames = {};
#endif

    allExtensionNames.insert(extensionNames.begin(), extensionNames.end());

    log("Using Vulkan instance extensions: " + join(extensionNames, ", "));

    vector<const char*> rawExtensionNames = toRawNames(allExtensionNames);

    VkInstanceCreateInfo instanceCreateInfo{};
    instanceCreateInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreateInfo.pApplicationInfo = &appInfo;
    instanceCreateInfo.enabledExtensionCount = rawExtensionNames.size();
    instanceCreateInfo.ppEnabledExtensionNames = rawExtensionNames.data();
#ifdef LMOD_VK_VALIDATION
    const char* layerNames[] = { "VK_LAYER_KHRONOS_validation" };
    instanceCreateInfo.enabledLayerCount = 1;
    instanceCreateInfo.ppEnabledLayerNames = layerNames;
#else
    instanceCreateInfo.enabledLayerCount = 0;
    instanceCreateInfo.ppEnabledLayerNames = nullptr;
#endif

    result = vkCreateInstance(&instanceCreateInfo, nullptr, &instance);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan instance: " + vulkanError(result));
    }

#ifdef LMOD_VK_VALIDATION
    VkDebugUtilsMessengerCreateInfoEXT debugMessengerCreateInfo{};
    debugMessengerCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debugMessengerCreateInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_INFO_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debugMessengerCreateInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debugMessengerCreateInfo.pfnUserCallback = handleError;
    debugMessengerCreateInfo.pUserData = nullptr;

    result = GET_VK_EXTENSION_FUNCTION(instance, vkCreateDebugUtilsMessengerEXT)(
        instance,
        &debugMessengerCreateInfo,
        nullptr,
        &debugMessenger
    );

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan debug messenger: " + vulkanError(result));
    }
#endif
}

#ifdef LMOD_VK_VALIDATION
void destroyInstance(VkInstance instance, VkDebugUtilsMessengerEXT debugMessenger)
#else
void destroyInstance(VkInstance instance)
#endif
{
#ifdef LMOD_VK_VALIDATION
    GET_VK_EXTENSION_FUNCTION(instance, vkDestroyDebugUtilsMessengerEXT)(instance, debugMessenger, nullptr);
#endif

    vkDestroyInstance(instance, nullptr);
}

void createSpecificDevice(
    u32 apiVersion,
    VkInstance instance,
    VkPhysicalDevice physDevice,
    const set<string>& extensionNames,
    i32& graphicsQueueIndex,
    VkQueue& graphicsQueue,
    VkDevice& device
)
{
    VkResult result;

    u32 queueFamilyCount;

    vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, nullptr);

    vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);

    vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, queueFamilies.data());

    for (size_t i = 0; i < queueFamilies.size(); i++)
    {
        const VkQueueFamilyProperties& queueFamily = queueFamilies[i];

        if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            graphicsQueueIndex = i;
            break;
        }
    }

    f32 priority = 1;

    VkDeviceQueueCreateInfo graphicsQueueCreateInfo{};
    graphicsQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    graphicsQueueCreateInfo.queueFamilyIndex = graphicsQueueIndex;
    graphicsQueueCreateInfo.queueCount = 1;
    graphicsQueueCreateInfo.pQueuePriorities = &priority;

    VkDeviceQueueCreateInfo queueCreateInfos[] = {
        graphicsQueueCreateInfo
    };

    log("Using Vulkan device extensions: " + join(extensionNames, ", "));

    vector<const char*> rawExtensionNames = toRawNames(extensionNames);

    VkPhysicalDeviceBufferDeviceAddressFeatures bufferDeviceAddressFeatures{};
    bufferDeviceAddressFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
    bufferDeviceAddressFeatures.bufferDeviceAddress = true;

    VkPhysicalDeviceAccelerationStructureFeaturesKHR accelerationStructureFeatures{};
    accelerationStructureFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    accelerationStructureFeatures.pNext = &bufferDeviceAddressFeatures;
    accelerationStructureFeatures.accelerationStructure = true;

    VkPhysicalDeviceRayQueryFeaturesKHR rayQueryFeatures{};
    rayQueryFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
    rayQueryFeatures.pNext = &accelerationStructureFeatures;
    rayQueryFeatures.rayQuery = true;

    VkPhysicalDeviceMultiviewFeatures multiviewFeatures{};
    multiviewFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
    multiviewFeatures.multiview = true;
    multiviewFeatures.multiviewGeometryShader = true;

    VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexingFeatures{};
    descriptorIndexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
    descriptorIndexingFeatures.pNext = &multiviewFeatures;
    descriptorIndexingFeatures.descriptorBindingPartiallyBound = true;

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        multiviewFeatures.pNext = &rayQueryFeatures;
    }

    VkPhysicalDeviceFeatures2 features{};
    features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    features.pNext = &descriptorIndexingFeatures;
    features.features.wideLines = true;
    features.features.samplerAnisotropy = true;
    features.features.fillModeNonSolid = true;
    features.features.geometryShader = true;
    features.features.vertexPipelineStoresAndAtomics = true;
    features.features.fragmentStoresAndAtomics = true;
    features.features.independentBlend = true;

    VkDeviceCreateInfo deviceCreateInfo{};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pNext = &features;
    deviceCreateInfo.queueCreateInfoCount = 1;
    deviceCreateInfo.pQueueCreateInfos = queueCreateInfos;
    deviceCreateInfo.enabledExtensionCount = rawExtensionNames.size();
    deviceCreateInfo.ppEnabledExtensionNames = rawExtensionNames.data();
#ifdef LMOD_VK_VALIDATION
    static constexpr const char* const layerNames[] = { "VK_LAYER_KHRONOS_validation" };
    deviceCreateInfo.enabledLayerCount = 1;
    deviceCreateInfo.ppEnabledLayerNames = layerNames;
#else
    deviceCreateInfo.enabledLayerCount = 0;
    deviceCreateInfo.ppEnabledLayerNames = nullptr;
#endif

    result = vkCreateDevice(physDevice, &deviceCreateInfo, nullptr, &device);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan device: " + vulkanError(result));
    }

    vkGetDeviceQueue(device, graphicsQueueIndex, 0, &graphicsQueue);
}

void createSpecificPresentDevice(
    u32 apiVersion,
    VkInstance instance,
    VkPhysicalDevice physDevice,
    VkSurfaceKHR surface,
    const set<string>& extensionNames,
    i32& graphicsQueueIndex,
    VkQueue& graphicsQueue,
    i32& presentQueueIndex,
    VkQueue& presentQueue,
    VkDevice& device
)
{
    VkResult result;

    u32 queueFamilyCount;

    vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, nullptr);

    vector<VkQueueFamilyProperties> queueFamilies(queueFamilyCount);

    vkGetPhysicalDeviceQueueFamilyProperties(physDevice, &queueFamilyCount, queueFamilies.data());

    for (size_t i = 0; i < queueFamilies.size(); i++)
    {
        const VkQueueFamilyProperties& queueFamily = queueFamilies[i];

        if (queueFamily.queueFlags & VK_QUEUE_GRAPHICS_BIT)
        {
            graphicsQueueIndex = i;
        }

        VkBool32 present = false;

        result = vkGetPhysicalDeviceSurfaceSupportKHR(physDevice, i, surface, &present);

        if (result != VK_SUCCESS)
        {
            error("Failed to get Vulkan physical device surface support: " + vulkanError(result));
            continue;
        }

        if (present)
        {
            presentQueueIndex = i;
        }
    }

    f32 priority = 1;

    VkDeviceQueueCreateInfo graphicsQueueCreateInfo{};
    graphicsQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    graphicsQueueCreateInfo.queueFamilyIndex = graphicsQueueIndex;
    graphicsQueueCreateInfo.queueCount = 1;
    graphicsQueueCreateInfo.pQueuePriorities = &priority;

    VkDeviceQueueCreateInfo presentQueueCreateInfo{};
    presentQueueCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    presentQueueCreateInfo.queueFamilyIndex = presentQueueIndex;
    presentQueueCreateInfo.queueCount = 1;
    presentQueueCreateInfo.pQueuePriorities = &priority;

    VkDeviceQueueCreateInfo queueCreateInfos[] = {
        graphicsQueueCreateInfo,
        presentQueueCreateInfo
    };

    log("Using Vulkan device extensions: " + join(extensionNames, ", "));

    vector<const char*> rawExtensionNames = toRawNames(extensionNames);

    VkPhysicalDeviceBufferDeviceAddressFeatures bufferDeviceAddressFeatures{};
    bufferDeviceAddressFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_BUFFER_DEVICE_ADDRESS_FEATURES;
    bufferDeviceAddressFeatures.bufferDeviceAddress = true;

    VkPhysicalDeviceAccelerationStructureFeaturesKHR accelerationStructureFeatures{};
    accelerationStructureFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_ACCELERATION_STRUCTURE_FEATURES_KHR;
    accelerationStructureFeatures.pNext = &bufferDeviceAddressFeatures;
    accelerationStructureFeatures.accelerationStructure = true;

    VkPhysicalDeviceRayQueryFeaturesKHR rayQueryFeatures{};
    rayQueryFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_RAY_QUERY_FEATURES_KHR;
    rayQueryFeatures.pNext = &accelerationStructureFeatures;
    rayQueryFeatures.rayQuery = true;

    VkPhysicalDeviceMultiviewFeatures multiviewFeatures{};
    multiviewFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_MULTIVIEW_FEATURES;
    multiviewFeatures.multiview = true;
    multiviewFeatures.multiviewGeometryShader = true;

    VkPhysicalDeviceDescriptorIndexingFeatures descriptorIndexingFeatures{};
    descriptorIndexingFeatures.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_DESCRIPTOR_INDEXING_FEATURES;
    descriptorIndexingFeatures.pNext = &multiviewFeatures;
    descriptorIndexingFeatures.descriptorBindingPartiallyBound = true;

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        multiviewFeatures.pNext = &rayQueryFeatures;
    }

    VkPhysicalDeviceFeatures2 features{};
    features.sType = VK_STRUCTURE_TYPE_PHYSICAL_DEVICE_FEATURES_2;
    features.pNext = &descriptorIndexingFeatures;
    features.features.wideLines = true;
    features.features.samplerAnisotropy = true;
    features.features.fillModeNonSolid = true;
    features.features.geometryShader = true;
    features.features.vertexPipelineStoresAndAtomics = true;
    features.features.fragmentStoresAndAtomics = true;
    features.features.independentBlend = true;

    VkDeviceCreateInfo deviceCreateInfo{};
    deviceCreateInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceCreateInfo.pNext = &features;
    deviceCreateInfo.queueCreateInfoCount = 2;
    deviceCreateInfo.pQueueCreateInfos = queueCreateInfos;
    deviceCreateInfo.enabledExtensionCount = rawExtensionNames.size();
    deviceCreateInfo.ppEnabledExtensionNames = rawExtensionNames.data();
#ifdef LMOD_VK_VALIDATION
    static constexpr const char* const layerNames[] = { "VK_LAYER_KHRONOS_validation" };
    deviceCreateInfo.enabledLayerCount = 1;
    deviceCreateInfo.ppEnabledLayerNames = layerNames;
#else
    deviceCreateInfo.enabledLayerCount = 0;
    deviceCreateInfo.ppEnabledLayerNames = nullptr;
#endif

    result = vkCreateDevice(physDevice, &deviceCreateInfo, nullptr, &device);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create Vulkan device: " + vulkanError(result));
    }

    vkGetDeviceQueue(device, graphicsQueueIndex, 0, &graphicsQueue);
    vkGetDeviceQueue(device, presentQueueIndex, 0, &presentQueue);
}

void destroyDevice(VkDevice device)
{
    vkDestroyDevice(device, nullptr);
}

void createAllocator(u32 apiVersion, VkInstance instance, VkPhysicalDevice physDevice, VkDevice device, VmaAllocator& allocator)
{
    VkResult result;

    VmaAllocatorCreateInfo allocatorCreateInfo{};
    if (rayTracingEnabled(apiVersion, physDevice))
    {
        allocatorCreateInfo.flags = VMA_ALLOCATOR_CREATE_BUFFER_DEVICE_ADDRESS_BIT;
    }
    allocatorCreateInfo.physicalDevice = physDevice;
    allocatorCreateInfo.device = device;
    allocatorCreateInfo.instance = instance;

    result = vmaCreateAllocator(&allocatorCreateInfo, &allocator);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create allocator: " + vulkanError(result));
        return;
    }
}

void destroyAllocator(VmaAllocator allocator)
{
    vmaDestroyAllocator(allocator);
}

void createPipelineCache(VkDevice device, VkPipelineCache& pipelineCache)
{
    VkResult result;

    if (!fs::exists(cachePath()))
    {
        bool ok = fs::create_directories(cachePath());

        if (!ok)
        {
            error("Failed to create cache directories.");
            return;
        }
    }

    string path = cachePath() + pipelineCacheFileName;

    string data;

    if (fs::exists(path))
    {
        data = getFile(path);
    }

    VkPipelineCacheCreateInfo pipelineCacheCreateInfo{};
    pipelineCacheCreateInfo.sType = VK_STRUCTURE_TYPE_PIPELINE_CACHE_CREATE_INFO;
    pipelineCacheCreateInfo.initialDataSize = data.size();
    pipelineCacheCreateInfo.pInitialData = data.data();

    result = vkCreatePipelineCache(device, &pipelineCacheCreateInfo, nullptr, &pipelineCache);

    if (result != VK_SUCCESS)
    {
        error("Failed to create pipeline cache: " + vulkanError(result));
        return;
    }
}

void destroyPipelineCache(VkDevice device, VkPipelineCache pipelineCache)
{
    VkResult result;

    size_t size;
    result = vkGetPipelineCacheData(device, pipelineCache, &size, nullptr);

    if (result != VK_SUCCESS)
    {
        error("Failed to get pipeline cache data: " + vulkanError(result));
        vkDestroyPipelineCache(device, pipelineCache, nullptr);
        return;
    }

    string data(size, '\0');
    result = vkGetPipelineCacheData(device, pipelineCache, &size, data.data());

    if (result != VK_SUCCESS)
    {
        error("Failed to get pipeline cache data: " + vulkanError(result));
        vkDestroyPipelineCache(device, pipelineCache, nullptr);
        return;
    }

    setFile(cachePath() + pipelineCacheFileName, data);

    vkDestroyPipelineCache(device, pipelineCache, nullptr);
}

tuple<VkFormat, u32, u32> createSwapchain(
    VkDevice device,
    u32 width,
    u32 height,
    i32 graphicsQueueIndex,
    i32 presentQueueIndex,
    VkPhysicalDevice physDevice,
    VkSurfaceKHR surface,
    VkSwapchainKHR& swapchain
)
{
    VkResult result;

    VkSurfaceCapabilitiesKHR capabilities;
    result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physDevice, surface, &capabilities);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to query physical device surface capabilities: " + vulkanError(result));
    }

    width = clamp(width, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
    height = clamp(height, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);

    u32 formatCount;
    result = vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice, surface, &formatCount, nullptr);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to query physical device surface formats: " + vulkanError(result));
    }

    vector<VkSurfaceFormatKHR> formats(formatCount);
    result = vkGetPhysicalDeviceSurfaceFormatsKHR(physDevice, surface, &formatCount, formats.data());

    if (result != VK_SUCCESS || formats.empty())
    {
        fatal("Failed to query physical device surface formats: " + vulkanError(result));
    }

    VkSurfaceFormatKHR chosenFormat = formats.front();
    f32 bestScore = numeric_limits<f32>::lowest();

    for (const VkSurfaceFormatKHR& format : formats)
    {
        f32 score;

        switch (format.format)
        {
        default :
            score = 0;
            break;
        case VK_FORMAT_R8G8B8A8_UNORM :
        case VK_FORMAT_B8G8R8A8_UNORM :
            score = 1;
            break;
        case VK_FORMAT_R16G16B16A16_SFLOAT :
            score = 2;
            break;
        }

        if (score > bestScore)
        {
            chosenFormat = format;
            bestScore = score;
        }
    }

    u32 queueIndices[] = { static_cast<u32>(graphicsQueueIndex), static_cast<u32>(presentQueueIndex) };

    VkSwapchainCreateInfoKHR swapchainCreateInfo{};
    swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreateInfo.surface = surface;
    swapchainCreateInfo.minImageCount = capabilities.minImageCount;
    swapchainCreateInfo.imageFormat = chosenFormat.format;
    swapchainCreateInfo.imageColorSpace = chosenFormat.colorSpace;
    swapchainCreateInfo.imageExtent = { static_cast<u32>(width), static_cast<u32>(height) };
    swapchainCreateInfo.imageArrayLayers = 1;
    swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    if (graphicsQueueIndex != presentQueueIndex)
    {
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_CONCURRENT;
        swapchainCreateInfo.queueFamilyIndexCount = 2;
        swapchainCreateInfo.pQueueFamilyIndices = queueIndices;
    }
    else
    {
        swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
        swapchainCreateInfo.queueFamilyIndexCount = 0;
        swapchainCreateInfo.pQueueFamilyIndices = nullptr;

    }
    swapchainCreateInfo.preTransform = capabilities.currentTransform;
    swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreateInfo.presentMode =
        g_config.frameLimit == 0
            ? VK_PRESENT_MODE_MAILBOX_KHR
            : VK_PRESENT_MODE_IMMEDIATE_KHR;
    swapchainCreateInfo.clipped = true;
    swapchainCreateInfo.oldSwapchain = nullptr;

    result = vkCreateSwapchainKHR(device, &swapchainCreateInfo, nullptr, &swapchain);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create swapchain: " + vulkanError(result));
    }

    return { chosenFormat.format, width, height };
}

void destroySwapchain(
    VkDevice device,
    VkSwapchainKHR swapchain
)
{
    vkDestroySwapchainKHR(device, swapchain, nullptr);
}

size_t getSwapchainImageCount(
    VkDevice device,
    VkSwapchainKHR swapchain
)
{
    u32 imageCount;
    VkResult result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to get swapchain images: " + vulkanError(result));
    }

    return imageCount;
}

vector<VkImage> getSwapchainImages(
    VkDevice device,
    VkSwapchainKHR swapchain
)
{
    VkResult result;

    u32 imageCount;
    result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, nullptr);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to get swapchain images: " + vulkanError(result));
    }

    vector<VkImage> images(imageCount);
    result = vkGetSwapchainImagesKHR(device, swapchain, &imageCount, images.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to get swapchain images: " + vulkanError(result));
    }

    return images;
}

void createCommandPool(VkDevice device, i32 graphicsQueueIndex, VkCommandPool& commandPool)
{
    VkResult result;

    VkCommandPoolCreateInfo commandPoolCreateInfo{};
    commandPoolCreateInfo.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    commandPoolCreateInfo.flags = VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    commandPoolCreateInfo.queueFamilyIndex = graphicsQueueIndex;

    result = vkCreateCommandPool(device, &commandPoolCreateInfo, nullptr, &commandPool);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create command pool: " + vulkanError(result));
    }
}

void destroyCommandPool(VkDevice device, VkCommandPool commandPool)
{
    vkDestroyCommandPool(device, commandPool, nullptr);
}

void createObjectDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createLitObjectDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    vector<VkDescriptorSetLayoutBinding> bindings = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        bindings.push_back(accelerationStructureLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT, eyeCount));
    }
    else
    {
        bindings.push_back(uniformBufferLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT));
    }

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = bindings.size();
    createInfo.pBindings = bindings.data();

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createTexturedObjectDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        uniformBufferLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT, maxTextureTileCount)
    };

    vector<VkDescriptorBindingFlags> bindingFlags = {
        0,
        0,
        0,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT
    };

    VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsCreateInfo{};
    bindingFlagsCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
    bindingFlagsCreateInfo.bindingCount = bindingFlags.size();
    bindingFlagsCreateInfo.pBindingFlags = bindingFlags.data();

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.pNext = &bindingFlagsCreateInfo;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createLitTexturedObjectDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    vector<VkDescriptorSetLayoutBinding> bindings = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        uniformBufferLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT, maxTextureTileCount),
        combinedSamplerLayoutBinding(4, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    vector<VkDescriptorBindingFlags> bindingFlags = {
        0,
        0,
        0,
        VK_DESCRIPTOR_BINDING_PARTIALLY_BOUND_BIT,
        0
    };

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        bindings.push_back(accelerationStructureLayoutBinding(5, VK_SHADER_STAGE_FRAGMENT_BIT, eyeCount));
        bindingFlags.push_back(0);
    }
    else
    {
        bindings.push_back(uniformBufferLayoutBinding(5, VK_SHADER_STAGE_FRAGMENT_BIT));
        bindingFlags.push_back(0);
    }

    VkDescriptorSetLayoutBindingFlagsCreateInfo bindingFlagsCreateInfo{};
    bindingFlagsCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_BINDING_FLAGS_CREATE_INFO;
    bindingFlagsCreateInfo.bindingCount = bindingFlags.size();
    bindingFlagsCreateInfo.pBindingFlags = bindingFlags.data();

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.pNext = &bindingFlagsCreateInfo;
    createInfo.bindingCount = bindings.size();
    createInfo.pBindings = bindings.data();

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createMaterialedObjectDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    vector<VkDescriptorSetLayoutBinding> bindings = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        uniformBufferLayoutBinding(2, VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = bindings.size();
    createInfo.pBindings = bindings.data();

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createLitMaterialedObjectDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    vector<VkDescriptorSetLayoutBinding> bindings = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        uniformBufferLayoutBinding(2, VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        bindings.push_back(accelerationStructureLayoutBinding(4, VK_SHADER_STAGE_FRAGMENT_BIT, eyeCount));
    }
    else
    {
        bindings.push_back(uniformBufferLayoutBinding(4, VK_SHADER_STAGE_FRAGMENT_BIT));
    }

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = bindings.size();
    createInfo.pBindings = bindings.data();

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createAreaDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        uniformBufferLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createLitAreaDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    vector<VkDescriptorSetLayoutBinding> bindings = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        uniformBufferLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(4, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        bindings.push_back(accelerationStructureLayoutBinding(5, VK_SHADER_STAGE_FRAGMENT_BIT, eyeCount));
    }
    else
    {
        bindings.push_back(uniformBufferLayoutBinding(5, VK_SHADER_STAGE_FRAGMENT_BIT));
    }

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = bindings.size();
    createInfo.pBindings = bindings.data();

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createTextDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT),
        combinedSamplerLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createHDRDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(1, VK_SHADER_STAGE_FRAGMENT_BIT),
        storageImageLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createPostProcessDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(1, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createDepthOfFieldDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(1, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT),
        storageBufferLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_COMPUTE_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createMotionBlurDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(1, VK_SHADER_STAGE_FRAGMENT_BIT),
        combinedSamplerLayoutBinding(2, VK_SHADER_STAGE_FRAGMENT_BIT),
        storageImageLayoutBinding(3, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createSkyDescriptorSetLayout(
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    VkDescriptorSetLayoutBinding bindings[] = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_FRAGMENT_BIT | VK_SHADER_STAGE_GEOMETRY_BIT)
    };

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = sizeof(bindings) / sizeof(VkDescriptorSetLayoutBinding);
    createInfo.pBindings = bindings;

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void createAtmosphereDescriptorSetLayout(
    u32 apiVersion,
    VkPhysicalDevice physDevice,
    VkDevice device,
    VkDescriptorSetLayout& descriptorSetLayout
)
{
    VkResult result;

    vector<VkDescriptorSetLayoutBinding> bindings = {
        uniformBufferLayoutBinding(0, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT),
        uniformBufferLayoutBinding(1, VK_SHADER_STAGE_VERTEX_BIT | VK_SHADER_STAGE_GEOMETRY_BIT | VK_SHADER_STAGE_FRAGMENT_BIT),
        storageBufferLayoutBinding(2, VK_SHADER_STAGE_VERTEX_BIT),
        storageBufferLayoutBinding(3, VK_SHADER_STAGE_VERTEX_BIT),
        storageBufferLayoutBinding(4, VK_SHADER_STAGE_GEOMETRY_BIT),
        combinedSamplerLayoutBinding(5, VK_SHADER_STAGE_FRAGMENT_BIT)
    };

    if (rayTracingEnabled(apiVersion, physDevice))
    {
        bindings.push_back(accelerationStructureLayoutBinding(6, VK_SHADER_STAGE_FRAGMENT_BIT, eyeCount));
    }
    else
    {
        bindings.push_back(uniformBufferLayoutBinding(6, VK_SHADER_STAGE_FRAGMENT_BIT));
    }

    VkDescriptorSetLayoutCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO;
    createInfo.bindingCount = bindings.size();
    createInfo.pBindings = bindings.data();

    result = vkCreateDescriptorSetLayout(device, &createInfo, nullptr, &descriptorSetLayout);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor set layout: " + vulkanError(result));
    }
}

void destroyDescriptorSetLayout(VkDevice device, VkDescriptorSetLayout descriptorSetLayout)
{
    vkDestroyDescriptorSetLayout(device, descriptorSetLayout, nullptr);
}

void createShader(VkDevice device, const string& source, VkShaderModule& shader)
{
    VkResult result;

    VkShaderModuleCreateInfo shaderCreateInfo{};
    shaderCreateInfo.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shaderCreateInfo.codeSize = source.size();
    shaderCreateInfo.pCode = reinterpret_cast<const u32*>(source.data());

    result = vkCreateShaderModule(device, &shaderCreateInfo, nullptr, &shader);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create shader: " + vulkanError(result));
    }
}

void destroyShader(VkDevice device, VkShaderModule shader)
{
    vkDestroyShaderModule(device, shader, nullptr);
}

void createHUDGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = texturedVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create HUD pipeline: " + vulkanError(result));
    }
}

void createTextGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create text pipeline: " + vulkanError(result));
    }
}

void createVertexColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = coloredVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(true);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create vertex color pipeline: " + vulkanError(result));
    }
}

void createLitVertexColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = litColoredVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(true);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create lit vertex color pipeline: " + vulkanError(result));
    }
}

void createTexturedColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    bool culling,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = texturedVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(culling);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create textured color pipeline: " + vulkanError(result));
    }
}

void createLitTexturedColorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    bool culling,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = litTexturedVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(culling);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create lit textured color pipeline: " + vulkanError(result));
    }
}

void createAreaGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = texturedVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create area pipeline: " + vulkanError(result));
    }
}

void createLitAreaGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = litTexturedVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create lit area pipeline: " + vulkanError(result));
    }
}

void createRopeGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = ropeVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(true);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create lit rope pipeline: " + vulkanError(result));
    }
}

void createLitRopeGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = litRopeVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(true);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create lit rope pipeline: " + vulkanError(result));
    }
}

void createColoredLineGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = coloredVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create colored line pipeline: " + vulkanError(result));
        return;
    }
}

void createGizmoGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = coloredVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage(true);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create colored triangle pipeline: " + vulkanError(result));
        return;
    }
}

void createTetherIndicatorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = basicVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create tether indicator pipeline: " + vulkanError(result));
    }
}

void createConfinementIndicatorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = basicVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create confinement indicator pipeline: " + vulkanError(result));
    }
}

void createWaitingIndicatorGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = basicVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create waiting indicator pipeline: " + vulkanError(result));
    }
}

void createGridGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = coloredVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage(1);
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create grid pipeline: " + vulkanError(result));
    }
}

void createThemeGradientGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = basicVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = lineInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = lineRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create theme gradient pipeline: " + vulkanError(result));
    }
}

void createHDRGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create HDR pipeline: " + vulkanError(result));
    }
}

void createPostProcessGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = ignoreDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create post process pipeline: " + vulkanError(result));
    }
}

void createSkyGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = readOnlyDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sky pipeline: " + vulkanError(result));
    }
}

void createSkyBillboardGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = pointInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = readOnlyDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sky billboard pipeline: " + vulkanError(result));
    }
}

void createSkyAuroraGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = pointInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sky aurora pipeline: " + vulkanError(result));
    }
}

void createSkyEnvMapGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = readOnlyDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sky pipeline: " + vulkanError(result));
    }
}

void createSkyEnvMapBillboardGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = pointInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = readOnlyDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = defaultColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sky billboard pipeline: " + vulkanError(result));
    }
}

void createAtmosphereLightningGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = triangleInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create atmosphere lightning pipeline: " + vulkanError(result));
    }
}

void createAtmosphereEffectGraphicsPipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkRenderPass renderPass,
    u32 subpass,
    VkShaderModule vertexShader,
    VkShaderModule geometryShader,
    VkShaderModule fragmentShader,
    VkSampleCountFlagBits sampleCount,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkPipelineVertexInputStateCreateInfo vertexInputStage = emptyVertexInputStage();
    VkPipelineInputAssemblyStateCreateInfo inputAssemblyStage = pointInputAssemblyStage();
    VkPipelineShaderStageCreateInfo vertexShaderStage = shaderStage(VK_SHADER_STAGE_VERTEX_BIT, vertexShader);
    VkPipelineShaderStageCreateInfo geometryShaderStage = shaderStage(VK_SHADER_STAGE_GEOMETRY_BIT, geometryShader);
    VkPipelineViewportStateCreateInfo viewportStage = defaultViewportStage();
    VkPipelineRasterizationStateCreateInfo rasterizationStage = triangleRasterizationStage();
    VkPipelineMultisampleStateCreateInfo multisampleStage = defaultMultisampleStage(sampleCount);
    VkPipelineDepthStencilStateCreateInfo depthStencilStage = defaultDepthStencilStage();
    VkPipelineShaderStageCreateInfo fragmentShaderStage = shaderStage(VK_SHADER_STAGE_FRAGMENT_BIT, fragmentShader);
    VkPipelineColorBlendStateCreateInfo colorBlendStage = stencilColorBlendStage();
    VkPipelineDynamicStateCreateInfo dynamicState = defaultDynamicState();

    VkPipelineShaderStageCreateInfo shaderStages[] = {
        vertexShaderStage,
        geometryShaderStage,
        fragmentShaderStage
    };

    VkGraphicsPipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stageCount = sizeof(shaderStages) / sizeof(VkPipelineShaderStageCreateInfo);
    pipelineCreateInfo.pStages = shaderStages;
    pipelineCreateInfo.pVertexInputState = &vertexInputStage;
    pipelineCreateInfo.pInputAssemblyState = &inputAssemblyStage;
    pipelineCreateInfo.pTessellationState = nullptr;
    pipelineCreateInfo.pViewportState = &viewportStage;
    pipelineCreateInfo.pRasterizationState = &rasterizationStage;
    pipelineCreateInfo.pMultisampleState = &multisampleStage;
    pipelineCreateInfo.pDepthStencilState = &depthStencilStage;
    pipelineCreateInfo.pColorBlendState = &colorBlendStage;
    pipelineCreateInfo.pDynamicState = &dynamicState;
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.renderPass = renderPass;
    pipelineCreateInfo.subpass = subpass;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateGraphicsPipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create atmosphere solid pipeline: " + vulkanError(result));
    }
}

void createComputePipeline(
    VkDevice device,
    VkPipelineCache pipelineCache,
    VkDescriptorSetLayout descriptorSetLayout,
    VkShaderModule computeShader,
    VkPipeline& pipeline,
    VkPipelineLayout& pipelineLayout
)
{
    VkResult result;

    createPipelineLayout(device, descriptorSetLayout, pipelineLayout);

    VkComputePipelineCreateInfo pipelineCreateInfo{};
    pipelineCreateInfo.sType = VK_STRUCTURE_TYPE_COMPUTE_PIPELINE_CREATE_INFO;
    pipelineCreateInfo.stage = shaderStage(VK_SHADER_STAGE_COMPUTE_BIT, computeShader);
    pipelineCreateInfo.layout = pipelineLayout;
    pipelineCreateInfo.basePipelineHandle = nullptr;
    pipelineCreateInfo.basePipelineIndex = -1;

    result = vkCreateComputePipelines(device, pipelineCache, 1, &pipelineCreateInfo, nullptr, &pipeline);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create compute pipeline: " + vulkanError(result));
    }
}

void destroyPipeline(VkDevice device, VkPipeline pipeline, VkPipelineLayout pipelineLayout)
{
    vkDestroyPipeline(device, pipeline, nullptr);
    vkDestroyPipelineLayout(device, pipelineLayout, nullptr);
}

tuple<VkAttachmentDescription2, VkAttachmentReference2> renderPassAttachment(
    u32 index,
    VkFormat format,
    VkSampleCountFlagBits samples,
    VkAttachmentLoadOp loadOp,
    VkAttachmentStoreOp storeOp,
    VkImageLayout before,
    VkImageLayout during,
    VkImageLayout after
)
{
    VkAttachmentDescription2 attachment{};
    attachment.sType = VK_STRUCTURE_TYPE_ATTACHMENT_DESCRIPTION_2;
    attachment.format = format;
    attachment.samples = samples;
    attachment.loadOp = loadOp;
    attachment.storeOp = storeOp;
    attachment.stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachment.stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachment.initialLayout = before;
    attachment.finalLayout = after;

    VkAttachmentReference2 attachmentRef{};
    attachmentRef.sType = VK_STRUCTURE_TYPE_ATTACHMENT_REFERENCE_2;
    attachmentRef.attachment = index;
    attachmentRef.layout = during;

    return { attachment, attachmentRef };
}

VkSubpassDescription2 renderPassSubpass()
{
    VkSubpassDescription2 subpass{};

    subpass.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_2;
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;

    return subpass;
}

tuple<VkSubpassDependency2, VkSubpassDependency2> renderPassDependencies(
    VkPipelineStageFlags before,
    VkAccessFlags beforeAccess,
    VkPipelineStageFlags entrance,
    VkAccessFlags entranceAccess,
    VkPipelineStageFlags exit,
    VkAccessFlags exitAccess,
    VkPipelineStageFlags after,
    VkAccessFlags afterAccess
)
{
    VkSubpassDependency2 previousToSubpass{};
    previousToSubpass.sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;
    previousToSubpass.srcSubpass = VK_SUBPASS_EXTERNAL;
    previousToSubpass.dstSubpass = 0;
    previousToSubpass.srcStageMask = before;
    previousToSubpass.srcAccessMask = beforeAccess;
    previousToSubpass.dstStageMask = entrance;
    previousToSubpass.dstAccessMask = entranceAccess;

    VkSubpassDependency2 subpassToNext{};
    subpassToNext.sType = VK_STRUCTURE_TYPE_SUBPASS_DEPENDENCY_2;
    subpassToNext.srcSubpass = 0;
    subpassToNext.dstSubpass = VK_SUBPASS_EXTERNAL;
    subpassToNext.srcStageMask = exit;
    subpassToNext.srcAccessMask = exitAccess;
    subpassToNext.dstStageMask = after;
    subpassToNext.dstAccessMask = afterAccess;

    return { previousToSubpass, subpassToNext };
}

void createPanelRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        panelFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create panel render pass: " + vulkanError(result));
    }
}

void createSkyRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        VK_FORMAT_R32G32B32A32_SFLOAT,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.viewMask = 0b00111111;

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    u32 correlatedViewMask = 0b00111111;

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
    renderPassCreateInfo.correlatedViewMaskCount = 1;
    renderPassCreateInfo.pCorrelatedViewMasks = &correlatedViewMask;

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sky render pass: " + vulkanError(result));
    }
}

void createUIRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create UI render pass: " + vulkanError(result));
    }
}

void createMonosampleMainRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );
    const auto [ depthAttachment, depthAttachmentRef ] = renderPassAttachment(
        1,
        depthFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );
    const auto [ stencilAttachment, stencilAttachmentRef ] = renderPassAttachment(
        2,
        stencilFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_GENERAL
    );

    VkAttachmentReference2 colorAttachments[] = {
        colorAttachmentRef,
        stencilAttachmentRef
    };

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = sizeof(colorAttachments) / sizeof(VkAttachmentReference2);
    subpass.pColorAttachments = colorAttachments;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment,
        depthAttachment,
        stencilAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create main render pass: " + vulkanError(result));
    }
}

void createMonosampleHDRRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create HDR render pass: " + vulkanError(result));
    }
}

void createMonosamplePostProcessRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create post process render pass: " + vulkanError(result));
    }
}

void createMonosampleOverlayRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ depthAttachment, depthAttachmentRef ] = renderPassAttachment(
        1,
        depthFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment,
        depthAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create overlay render pass: " + vulkanError(result));
    }
}

void createMonosampleIndependentDepthOverlayRenderPass(RenderInterface* render, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ depthAttachment, depthAttachmentRef ] = renderPassAttachment(
        1,
        depthFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorAttachment,
        depthAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create overlay render pass: " + vulkanError(result));
    }
}

void createMultisampleMainRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorMultisampleAttachment, colorMultisampleAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ depthMultisampleAttachment, depthMultisampleAttachmentRef ] = renderPassAttachment(
        1,
        depthFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    );
    const auto [ stencilMultisampleAttachment, stencilMultisampleAttachmentRef ] = renderPassAttachment(
        2,
        stencilFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        3,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );
    const auto [ depthAttachment, depthAttachmentRef ] = renderPassAttachment(
        4,
        depthFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL
    );
    const auto [ stencilAttachment, stencilAttachmentRef ] = renderPassAttachment(
        5,
        stencilFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_GENERAL
    );

    VkSubpassDescriptionDepthStencilResolve depthStencilResolve{};
    depthStencilResolve.sType = VK_STRUCTURE_TYPE_SUBPASS_DESCRIPTION_DEPTH_STENCIL_RESOLVE;
    depthStencilResolve.depthResolveMode = VK_RESOLVE_MODE_MAX_BIT;
    depthStencilResolve.stencilResolveMode = VK_RESOLVE_MODE_SAMPLE_ZERO_BIT;
    depthStencilResolve.pDepthStencilResolveAttachment = &depthAttachmentRef;

    VkAttachmentReference2 colorAttachments[] = {
        colorMultisampleAttachmentRef,
        stencilMultisampleAttachmentRef
    };
    VkAttachmentReference2 resolveAttachments[] = {
        colorAttachmentRef,
        stencilAttachmentRef
    };

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.pNext = &depthStencilResolve;
    subpass.colorAttachmentCount = sizeof(colorAttachments) / sizeof(VkAttachmentReference2);
    subpass.pColorAttachments = colorAttachments;
    subpass.pResolveAttachments = resolveAttachments;
    subpass.pDepthStencilAttachment = &depthMultisampleAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorMultisampleAttachment,
        depthMultisampleAttachment,
        stencilMultisampleAttachment,
        colorAttachment,
        depthAttachment,
        stencilAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create main render pass: " + vulkanError(result));
    }
}

void createMultisampleHDRRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorMultisampleAttachment, colorMultisampleAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        1,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorMultisampleAttachmentRef;
    subpass.pResolveAttachments = &colorAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorMultisampleAttachment,
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create HDR render pass: " + vulkanError(result));
    }
}

void createMultisamplePostProcessRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorMultisampleAttachment, colorMultisampleAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        1,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorMultisampleAttachmentRef;
    subpass.pResolveAttachments = &colorAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_WRITE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_SHADER_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_ACCESS_TRANSFER_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorMultisampleAttachment,
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create post process render pass: " + vulkanError(result));
    }
}

void createMultisampleOverlayRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorMultisampleAttachment, colorMultisampleAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ depthMultisampleAttachment, depthMultisampleAttachmentRef ] = renderPassAttachment(
        1,
        depthFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorMultisampleAttachmentRef;
    subpass.pDepthStencilAttachment = &depthMultisampleAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorMultisampleAttachment,
        depthMultisampleAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create overlay render pass: " + vulkanError(result));
    }
}

void createMultisampleIndependentDepthOverlayRenderPass(RenderInterface* render, VkSampleCountFlagBits sampleCount, VkRenderPass& renderPass)
{
    VkResult result;

    const auto [ colorMultisampleAttachment, colorMultisampleAttachmentRef ] = renderPassAttachment(
        0,
        colorFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_LOAD,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );
    const auto [ depthMultisampleAttachment, depthMultisampleAttachmentRef ] = renderPassAttachment(
        1,
        depthFormat,
        sampleCount,
        VK_ATTACHMENT_LOAD_OP_CLEAR,
        VK_ATTACHMENT_STORE_OP_DONT_CARE,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
    );
    const auto [ colorAttachment, colorAttachmentRef ] = renderPassAttachment(
        2,
        colorFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        VK_ATTACHMENT_STORE_OP_STORE,
        VK_IMAGE_LAYOUT_UNDEFINED,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
        VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
    );

    VkSubpassDescription2 subpass = renderPassSubpass();
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &colorMultisampleAttachmentRef;
    subpass.pResolveAttachments = &colorAttachmentRef;
    subpass.pDepthStencilAttachment = &depthMultisampleAttachmentRef;
#ifdef LMOD_VR
    if (g_vr)
    {
        subpass.viewMask = renderPassVRViewMask;
    }
#endif

    const auto [ previousToSubpass, subpassToNext ] = renderPassDependencies(
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
        VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
        VK_ACCESS_COLOR_ATTACHMENT_READ_BIT
    );

    VkAttachmentDescription2 attachments[] = {
        colorMultisampleAttachment,
        depthMultisampleAttachment,
        colorAttachment
    };

    VkSubpassDescription2 subpasses[] = {
        subpass
    };

    VkSubpassDependency2 dependencies[] = {
        previousToSubpass,
        subpassToNext
    };

    VkRenderPassCreateInfo2 renderPassCreateInfo{};
    renderPassCreateInfo.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO_2;
    renderPassCreateInfo.attachmentCount = sizeof(attachments) / sizeof(VkAttachmentDescription2);
    renderPassCreateInfo.pAttachments = attachments;
    renderPassCreateInfo.subpassCount = sizeof(subpasses) / sizeof(VkSubpassDescription2);
    renderPassCreateInfo.pSubpasses = subpasses;
    renderPassCreateInfo.dependencyCount = sizeof(dependencies) / sizeof(VkSubpassDependency2);
    renderPassCreateInfo.pDependencies = dependencies;
#ifdef LMOD_VR
    if (g_vr)
    {
        renderPassCreateInfo.correlatedViewMaskCount = 1;
        renderPassCreateInfo.pCorrelatedViewMasks = &renderPassVRViewMask;
    }
#endif

    auto f = VK_API_VERSION_MAJOR(render->context()->apiVersion) > 1 || VK_API_VERSION_MINOR(render->context()->apiVersion) > 1
        ? vkCreateRenderPass2
        : GET_VK_EXTENSION_FUNCTION(render->context()->instance, vkCreateRenderPass2KHR);

    result = f(render->context()->device, &renderPassCreateInfo, nullptr, &renderPass);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create independent depth overlay render pass: " + vulkanError(result));
    }
}

void destroyRenderPass(VkDevice device, VkRenderPass renderPass)
{
    vkDestroyRenderPass(device, renderPass, nullptr);
}

void createSampler(VkDevice device, bool filtered, VkSampler& sampler)
{
    VkResult result;

    VkSamplerCreateInfo createInfo{};
    createInfo.sType = VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO;
    createInfo.magFilter = filtered ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;
    createInfo.minFilter = filtered ? VK_FILTER_LINEAR : VK_FILTER_NEAREST;
    createInfo.mipmapMode = filtered ? VK_SAMPLER_MIPMAP_MODE_LINEAR : VK_SAMPLER_MIPMAP_MODE_NEAREST;
    createInfo.addressModeU = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    createInfo.addressModeV = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    createInfo.addressModeW = VK_SAMPLER_ADDRESS_MODE_REPEAT;
    createInfo.mipLodBias = 0;
    createInfo.anisotropyEnable = true;
    createInfo.maxAnisotropy = 16;
    createInfo.compareEnable = false;
    createInfo.compareOp = VK_COMPARE_OP_NEVER;
    createInfo.minLod = 0;
    createInfo.maxLod = VK_LOD_CLAMP_NONE;
    createInfo.borderColor = VK_BORDER_COLOR_INT_OPAQUE_BLACK;
    createInfo.unnormalizedCoordinates = false;

    result = vkCreateSampler(device, &createInfo, nullptr, &sampler);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create sampler: " + vulkanError(result));
    }
}

void destroySampler(VkDevice device, VkSampler sampler)
{
    vkDestroySampler(device, sampler, nullptr);
}

void createReferenceTexture(
    VkDevice device,
    VkQueue queue,
    VmaAllocator allocator,
    VkCommandPool commandPool,
    const string& name,
    VmaImage& texture
)
{
    runOneshotCommands(device, queue, commandPool, [&](VkCommandBuffer commandBuffer) -> void {
        Image* source = loadPNG(texturePath + "/" + name + textureExt);

        texture = createImage(
            allocator,
            { source->width(), source->height(), 1 },
            textureFormat,
            VK_SAMPLE_COUNT_1_BIT,
            VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
            VMA_MEMORY_USAGE_CPU_TO_GPU,
            true,
            maxMipLevels(source->width(), source->height())
        );

        preinitializeImage(
            device,
            allocator,
            texture,
            source->width(),
            source->height(),
            source->pitch(),
            reinterpret_cast<const u32*>(source->pixels())
        );

        delete source;

        transitionLayout(
            commandBuffer,
            texture.image,
            VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            VK_IMAGE_LAYOUT_PREINITIALIZED,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL
        );

        computeMipmaps(
            commandBuffer,
            texture.image,
            VK_PIPELINE_STAGE_TRANSFER_BIT,
            VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
            VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            source->width(),
            source->height()
        );
    });
}

void createCurvedPanelBuffer(VmaAllocator allocator, VmaBuffer& hudVertexBuffer)
{
    hudVertexBuffer = createBuffer(
        allocator,
        6 * 24 * sizeof(TexturedVertex),
        VK_BUFFER_USAGE_VERTEX_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<TexturedVertex> mapping(allocator, hudVertexBuffer);

    for (u32 i = 1; i <= 24; i++)
    {
        f32 previous = (i - 1) / 24.0f;
        f32 current = i / 24.0f;

        fquaternion previousRot(backDir, (pi * 0.2f) + (pi * 0.6f * previous));
        fquaternion currentRot(backDir, (pi * 0.2f) + (pi * 0.6f * current));

        fvec3 top = { -1, +1, 0 };
        fvec3 bottom = { -1, -1, 0 };

        fvec3 previousTop = previousRot.rotate(top);
        fvec3 previousBottom = previousRot.rotate(bottom);
        fvec3 currentTop = currentRot.rotate(top);
        fvec3 currentBottom = currentRot.rotate(bottom);

        vector<TexturedVertex> segmentVertices = {
            { previousBottom, fvec2(previous, 0) },
            { currentTop, fvec2(current, 1) },
            { previousTop, fvec2(previous, 1) },
            { previousBottom, fvec2(previous, 0) },
            { currentBottom, fvec2(current, 0) },
            { currentTop, fvec2(current, 1) }
        };

        for (size_t j = 0; j < segmentVertices.size(); j++)
        {
            mapping.data[(i - 1) * 6 + j] = segmentVertices.at(j);
        }
    }
}

void createEnvironmentMappingCameraUniformBuffer(VmaAllocator allocator, VmaBuffer& environmentMapCameraUniformBuffer)
{
    environmentMapCameraUniformBuffer = createBuffer(
        allocator,
        7 * sizeof(fmat4),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<fmat4> mapping(allocator, environmentMapCameraUniformBuffer);

    mapping.data[0] = createFlatPerspectiveMatrix<f32>(radians(90), 1, defaultNearDistance, defaultFarDistance);

    // +x
    mapping.data[1] = createViewMatrix<f32>(fvec3(), fquaternion(forwardDir, rightDir));

    // -x
    mapping.data[2] = createViewMatrix<f32>(fvec3(), fquaternion(backDir, rightDir));

    // +y
    mapping.data[3] = createViewMatrix<f32>(fvec3(), fquaternion(rightDir, downDir));

    // -y
    mapping.data[4] = createViewMatrix<f32>(fvec3(), fquaternion(leftDir, upDir));

    // +z
    mapping.data[5] = createViewMatrix<f32>(fvec3(), fquaternion(upDir, rightDir));

    // -z
    mapping.data[6] = createViewMatrix<f32>(fvec3(), fquaternion(downDir, rightDir));
}

void createAreaSurfaceNoiseImage(VkDevice device, VmaAllocator allocator, VkCommandBuffer commandBuffer, VmaImage& image, VkImageView& imageView)
{
    PerlinNoise perlin(areaSurfaceNoiseTextureSeed, areaSurfaceNoiseTextureFrequency, areaSurfaceNoiseTextureDepth);

    vector<u32> pixels(areaSurfaceNoiseTextureSize * areaSurfaceNoiseTextureSize);

    for (u32 y = 0; y < areaSurfaceNoiseTextureSize; y++)
    {
        for (u32 x = 0; x < areaSurfaceNoiseTextureSize; x++)
        {
            u32 index = y * areaSurfaceNoiseTextureSize + x;

            u32 pixel = perlin.at(vec2(x, y)) * 255;

            pixels[index] = 0xff000000 | (pixel << 16);
        }
    }

    image = createImage(
        allocator,
        { areaSurfaceNoiseTextureSize, areaSurfaceNoiseTextureSize, 1 },
        textureFormat,
        VK_SAMPLE_COUNT_1_BIT,
        VK_IMAGE_USAGE_SAMPLED_BIT | VK_IMAGE_USAGE_TRANSFER_SRC_BIT | VK_IMAGE_USAGE_TRANSFER_DST_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU,
        true,
        maxMipLevels(areaSurfaceNoiseTextureSize, areaSurfaceNoiseTextureSize)
    );

    imageView = createImageView(
        device,
        image.image,
        textureFormat,
        VK_IMAGE_ASPECT_COLOR_BIT,
        VK_IMAGE_VIEW_TYPE_2D,
        maxMipLevels(areaSurfaceNoiseTextureSize, areaSurfaceNoiseTextureSize)
    );

    VmaMapping<u32> mapping(allocator, image);

    preinitializeImage(
        device,
        allocator,
        image,
        areaSurfaceNoiseTextureSize,
        areaSurfaceNoiseTextureSize,
        areaSurfaceNoiseTextureSize * sizeof(u32),
        pixels.data()
    );

    computeMipmaps(
        commandBuffer,
        image.image,
        VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_IMAGE_LAYOUT_PREINITIALIZED,
        VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        areaSurfaceNoiseTextureSize,
        areaSurfaceNoiseTextureSize
    );
}

void createDescriptorPool(VkDevice device, size_t imageCount, VkDescriptorPool& descriptorPool)
{
    VkResult result;

    VkDescriptorPoolSize uniformBufferPoolSize{};
    uniformBufferPoolSize.type = VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER;
    uniformBufferPoolSize.descriptorCount = maxRenderCount * imageCount * 2;

    VkDescriptorPoolSize imageSamplerPoolSize{};
    imageSamplerPoolSize.type = VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER;
    imageSamplerPoolSize.descriptorCount = maxRenderCount * imageCount * 1;

    VkDescriptorPoolSize poolSizes[] = {
        uniformBufferPoolSize,
        imageSamplerPoolSize
    };

    VkDescriptorPoolCreateInfo descriptorPoolCreateInfo{};
    descriptorPoolCreateInfo.sType = VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO;
    descriptorPoolCreateInfo.flags = VK_DESCRIPTOR_POOL_CREATE_FREE_DESCRIPTOR_SET_BIT;
    descriptorPoolCreateInfo.maxSets = maxRenderCount * imageCount;
    descriptorPoolCreateInfo.poolSizeCount = 2;
    descriptorPoolCreateInfo.pPoolSizes = poolSizes;

    result = vkCreateDescriptorPool(device, &descriptorPoolCreateInfo, nullptr, &descriptorPool);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to create descriptor pool: " + vulkanError(result));
    }
}

void destroyDescriptorPool(VkDevice device, VkDescriptorPool descriptorPool)
{
    vkDestroyDescriptorPool(device, descriptorPool, nullptr);
}

set<string> rayTracingDeviceExtensions(u32 apiVersion, VkPhysicalDevice physDevice)
{
    VkResult result;

    // Not supported at all under 1.1
    if (VK_API_VERSION_MAJOR(apiVersion) == 1 && VK_API_VERSION_MINOR(apiVersion) < 1)
    {
        return {};
    }

    u32 extensionCount;
    result = vkEnumerateDeviceExtensionProperties(physDevice, nullptr, &extensionCount, nullptr);

    if (result != VK_SUCCESS)
    {
        fatal("Failed to enumerate device extensions: " + vulkanError(result));
    }

    vector<VkExtensionProperties> extensions(extensionCount);
    result = vkEnumerateDeviceExtensionProperties(physDevice, nullptr, &extensionCount, extensions.data());

    if (result != VK_SUCCESS)
    {
        fatal("Failed to enumerate device extensions: " + vulkanError(result));
    }

    set<string> deviceExtensions = {
        "VK_KHR_deferred_host_operations",
        "VK_KHR_acceleration_structure",
        "VK_KHR_ray_query"
    };

    // Compatibility with 1.1
    if (VK_API_VERSION_MAJOR(apiVersion) == 1 && VK_API_VERSION_MINOR(apiVersion) < 2)
    {
        deviceExtensions.insert("VK_EXT_descriptor_indexing");
        deviceExtensions.insert("VK_KHR_shader_float_controls");
        deviceExtensions.insert("VK_KHR_spirv_1_4");
    }

    if (all_of(
        deviceExtensions.begin(),
        deviceExtensions.end(),
        [&](const string& extension) -> bool
        {
            return find_if(
                extensions.begin(),
                extensions.end(),
                [&](const VkExtensionProperties& properties) -> bool
                {
                    return strcmp(properties.extensionName, extension.c_str()) == 0;
                }
            ) != extensions.end();
        }
    ))
    {
        return deviceExtensions;
    }
    else
    {
        return {};
    }
}

bool rayTracingSupported(u32 apiVersion, VkPhysicalDevice physDevice)
{
    return !rayTracingDeviceExtensions(apiVersion, physDevice).empty();
}

bool rayTracingEnabled(u32 apiVersion, VkPhysicalDevice physDevice)
{
    return g_config.rayTracing && rayTracingSupported(apiVersion, physDevice);
}
