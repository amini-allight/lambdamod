/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "logic_editor.hpp"
#include "localization.hpp"
#include "interface_constants.hpp"
#include "interface_draw.hpp"
#include "global.hpp"
#include "control_context.hpp"
#include "entity.hpp"
#include "column.hpp"
#include "tab_view.hpp"

LogicEditor::LogicEditor(InterfaceWidget* parent)
    : InterfaceWidget(parent)
    , nodeEditor(nullptr)
{

}

bool LogicEditor::saved() const
{
    return (!nodeEditor || nodeEditor->saved()) && codeEditor->saved();
}

void LogicEditor::set(const string& text)
{
    if (nodeEditor)
    {
        nodeEditor->set(ScriptNodeFormat(context()->controller()->game().context(), text));
    }

    codeEditor->set(text);
}

void LogicEditor::notifyReload()
{
    if (nodeEditor)
    {
        nodeEditor->notifyReload();
    }

    codeEditor->notifyReload();
}

void LogicEditor::display(const string& message)
{
    if (nodeEditor)
    {
        nodeEditor->display(message);
    }

    codeEditor->display(message);
}

SizeProperties LogicEditor::sizeProperties() const
{
    return sizePropertiesFillAll;
}
