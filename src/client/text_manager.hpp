/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "text_tracker.hpp"

class ControlContext;

class TextManager
{
public:
    TextManager(ControlContext* ctx);

    void step();

    void request(const TextRequest& request);
    void respond();

    bool signal() const;
    optional<TextTracker> titlecard() const;

    bool hasRequest() const;
    TextID requestID() const;
    TextType requestType() const;
    template<typename T>
    const T& requestData() const
    {
        return requests.front().data<T>();
    }
    u64 requestElapsed() const;
    bool needsInteraction() const;

    const vector<TextKnowledgeRequest>& knowledge() const;

    template<typename T>
    T& response()
    {
        return _response.data<T>();
    }
    template<typename T>
    const T& response() const
    {
        return _response.data<T>();
    }

private:
    ControlContext* ctx;

    bool _signal;
    optional<TextTracker> _titlecard;
    deque<TextTracker> requests;
    vector<TextKnowledgeRequest> _knowledge;
    TextResponse _response;
};
