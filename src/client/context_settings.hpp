/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "interface_widget.hpp"
#include "script_types.hpp"

#include "item_list.hpp"
#include "stack_view.hpp"

class ContextSettings : public InterfaceWidget
{
public:
    ContextSettings(InterfaceWidget* parent);

    void step() override;

private:
    ItemList* definitionList;
    StackView* stack;

    void openDefinition(const string& name);
    void closeDefinition(const string& name);

    vector<string> itemSource();
    void onAdd(const string& name);
    void onRemove(const string& name);
    void onSelect(const string& name);
    void onRename(const string& oldName, const string& newName);
    void onClone(const string& oldName, const string& newName);
    vector<tuple<string, function<void(const Point&)>>> onListRightClick(size_t index);

    void onSetOwner(const string& name, const Point& point);
    void onSetOwnerDone(const string& name, UserID id);
    void onToggleRestriction(const string& name, const Point& point);

    Value valueSource(const string& name);
    void onValueSave(const string& name, const Value& value);

    SizeProperties sizeProperties() const override;
};
