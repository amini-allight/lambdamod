/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_voice_stream.hpp"
#include "sound.hpp"
#include "sound_tools.hpp"
#include "sound_constants.hpp"
#include "tools.hpp"

static constexpr chrono::seconds voiceStreamTimeout(5);
static constexpr chrono::seconds maxVoiceDelay(5);

SoundVoiceStream::SoundVoiceStream(
    Sound* sound,
    const vec3& position,
    const quaternion& rotation,
    f64 volume,
    bool canBeSpatial
)
    : SoundSource(sound, position, rotation, false, tau, canBeSpatial)
    , stream(soundChunkOverlap, 0)
    , lastSeenTime(currentTime())
    , draining(false)
    , volume(volume)
{

}

void SoundVoiceStream::update(const vec3& position, const quaternion& rotation, f64 volume, bool canBeSpatial)
{
    this->position = position;
    this->rotation = rotation;
    this->volume = volume;
    this->canBeSpatial = canBeSpatial;
}

void SoundVoiceStream::push(const string& data)
{
    lastSeenTime = currentTime();

    if (draining)
    {
        if (stream.empty())
        {
            draining = false;
        }
        else
        {
            return;
        }
    }

    // NOTE: Assumes voiceChannels = 1
    vector<i16> discrete(data.size() / sizeof(i16));
    memcpy(discrete.data(), data.data(), data.size());
    vector<f32> continuous = convertSamplesToContinuous(discrete);

    stream.insert(stream.end(), continuous.begin(), continuous.end());

    if (chrono::seconds(static_cast<u64>(stream.size() / static_cast<f64>(voiceFrequency))) > maxVoiceDelay)
    {
        draining = true;
    }
}

bool SoundVoiceStream::expired() const
{
    return depleted() && currentTime() > lastSeenTime + voiceStreamTimeout;
}

variant<SoundOverlappedChunk, SoundOverlappedStereoChunk> SoundVoiceStream::pull(f64 multiplier)
{
    if (stream.empty())
    {
        return SoundOverlappedChunk();
    }

    auto [ chunk, advance ] = readSampleSource(stream, soundChunkOverlap, 1, false);

    stream.erase(stream.begin(), stream.begin() + advance);

    return chunk;
}
