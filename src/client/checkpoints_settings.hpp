/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "right_clickable_widget.hpp"
#include "tab_content.hpp"

#include "line_edit.hpp"
#include "list_view.hpp"

class Checkpoint;

struct CheckpointDetail
{
    CheckpointDetail();

    bool operator==(const CheckpointDetail& rhs) const;
    bool operator!=(const CheckpointDetail& rhs) const;

    string name;
};

class CheckpointsSettings : public RightClickableWidget, public TabContent
{
public:
    CheckpointsSettings(InterfaceWidget* parent);

    void step() override;

private:
    vector<CheckpointDetail> details;
    string search;
    LineEdit* searchBar;
    ListView* listView;

    vector<CheckpointDetail> convert(const set<string>& checkpoints) const;

    void openRightClickMenu(i32 index, const Point& pointer) override;

    void onRemove(const string& name, const Point& point);
    void onRevert(const string& name, const Point& point);

    void updateListView();

    void add(const Input& input);

    SizeProperties sizeProperties() const override;
};
