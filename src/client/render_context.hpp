/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "render_types.hpp"

class RenderContext
{
public:
    RenderContext();
    RenderContext(const RenderContext& rhs) = delete;
    RenderContext(RenderContext&& rhs) = delete;
    ~RenderContext();

    RenderContext& operator=(const RenderContext& rhs) = delete;
    RenderContext& operator=(RenderContext&& rhs) = delete;

    bool initialized() const;

    void init(
        const function<u32()>& pickAPIVersion,
        const function<set<string>()>& instanceExtensions,
        const function<VkPhysicalDevice()>& pickPhysicalDevice,
        const function<set<string>()>& deviceExtensions,
        const function<u32()>& prepareSwapchain
    );
    void init(
        const function<u32()>& pickAPIVersion,
        const function<set<string>()>& instanceExtensions,
        const function<VkPhysicalDevice()>& pickPhysicalDevice,
        const function<VkSurfaceKHR()>& prepareSurface,
        const function<set<string>()>& deviceExtensions,
        const function<u32()>& prepareSwapchain
    );

    set<string> rayTracingDeviceExtensions() const;
    bool rayTracingSupported() const;
    bool rayTracingEnabled() const;

    u32 apiVersion;
    VkInstance instance;
#ifdef LMOD_VK_VALIDATION
    VkDebugUtilsMessengerEXT debugMessenger;
#endif
    VkPhysicalDevice physDevice;
    VkDevice device;
    i32 graphicsQueueIndex;
    VkQueue graphicsQueue;
    i32 presentQueueIndex;
    VkQueue presentQueue;
    VmaAllocator allocator;
    VkPipelineCache pipelineCache;
    VkCommandPool commandPool;
    VkDescriptorPool descriptorPool;
    VkSampler filteredSampler;
    VkSampler unfilteredSampler;
    VmaBuffer environmentMappingCameraUniformBuffer;
    VmaImage areaSurfaceNoiseImage;
    VkImageView areaSurfaceNoiseImageView;

private:
    bool _initialized;
};
