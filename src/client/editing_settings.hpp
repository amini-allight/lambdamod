/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "interface_widget.hpp"
#include "tab_content.hpp"

class EditingSettings : public InterfaceWidget, public TabContent
{
public:
    EditingSettings(InterfaceWidget* parent);

private:
    vec3 viewerPositionSource();
    void onViewerPosition(const vec3& v);

    bool hudShownSource();
    void onHUDShown(bool state);

    bool entityPointsShownSource();
    void onEntityPointsShown(bool state);

    bool cursorShownSource();
    void onCursorShown(bool state);

    vec3 cursorPositionSource();
    void onCursorPosition(const vec3& v);

    bool gridShownSource();
    void onGridShown(bool state);

    vec3 gridPositionSource();
    void onGridPosition(const vec3& v);

    f64 gridSizeSource();
    void onGridSize(f64 v);

    bool gizmoShownSource();
    void onGizmoShown(bool state);

    bool spaceGraphVisualizerShownSource() const;
    void onSpaceGraphVisualizerShown(bool state);

    f64 canvasPaintBrushRadiusSource();
    void onCanvasPaintBrushRadius(f64 v);

    f64 translationStepSource();
    void onTranslationStep(f64 v);

    f64 rotationStepSource();
    void onRotationStep(f64 v);

    f64 scaleStepSource();
    void onScaleStep(f64 v);

    SizeProperties sizeProperties() const override;
};
