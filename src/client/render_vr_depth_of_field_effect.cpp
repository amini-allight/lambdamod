/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_depth_of_field_effect.hpp"
#include "render_tools.hpp"
#include "constants.hpp"
#include "render_descriptor_set_write_components.hpp"

RenderVRDepthOfFieldEffect::RenderVRDepthOfFieldEffect(
    const RenderContext* ctx,
    RenderVRPipelineStore* pipelineStore,
    vector<VRSwapchainElement*> swapchainElements,
    f64 range
)
    : RenderVRPostProcessEffect(
        ctx,
        pipelineStore->depthOfField.pipelineLayout,
        pipelineStore->depthOfField.pipeline
    )
    , range(range)
{
    storageBuffer = createStorageBuffer();

    for (size_t i = 0; i < swapchainElements.size(); i++)
    {
        auto component = new RenderPostProcessEffectComponent(ctx, pipelineStore->depthOfFieldDescriptorSetLayout, createUniformBuffer());

        setDescriptorSet(
            component->descriptorSet,
            component->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->depthImageView(),
            storageBuffer.buffer
        );

        components.push_back(component);
    }
}

RenderVRDepthOfFieldEffect::~RenderVRDepthOfFieldEffect()
{
    destroyComponents();

    destroyBuffer(ctx->allocator, storageBuffer);
}

void RenderVRDepthOfFieldEffect::work(const VRSwapchainElement* swapchainElement, f32 timer) const
{
    barrierBuffer(
        swapchainElement->transferCommandBuffer(),
        storageBuffer.buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_COMPUTE_SHADER_BIT,
        VK_ACCESS_SHADER_WRITE_BIT,
        VK_ACCESS_TRANSFER_READ_BIT
    );

    VkBufferCopy region{};
    region.srcOffset = 4 * sizeof(f32);
    region.dstOffset = 0;
    region.size = 4 * sizeof(f32);

    vkCmdCopyBuffer(
        swapchainElement->transferCommandBuffer(),
        storageBuffer.buffer,
        storageBuffer.buffer,
        1,
        &region
    );

    barrierBuffer(
        swapchainElement->transferCommandBuffer(),
        storageBuffer.buffer,
        VK_PIPELINE_STAGE_TRANSFER_BIT,
        VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT,
        VK_ACCESS_TRANSFER_WRITE_BIT,
        VK_ACCESS_SHADER_READ_BIT
    );

    RenderVRPostProcessEffect::work(swapchainElement, timer);
}

void RenderVRDepthOfFieldEffect::refresh(vector<VRSwapchainElement*> swapchainElements)
{
    for (size_t i = 0; i < swapchainElements.size(); i++)
    {
        setDescriptorSet(
            components.at(i)->descriptorSet,
            components.at(i)->uniform.buffer,
            swapchainElements.at(i)->sideImageView(),
            swapchainElements.at(i)->depthImageView(),
            storageBuffer.buffer
        );
    }
}

void RenderVRDepthOfFieldEffect::update(f64 range)
{
    this->range = range;
}

VmaBuffer RenderVRDepthOfFieldEffect::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(f32) * 2,
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderVRDepthOfFieldEffect::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    mapping->data[0] = range;
    mapping->data[1] = timer;
}

void RenderVRDepthOfFieldEffect::setDescriptorSet(
    VkDescriptorSet descriptorSet,
    VkBuffer uniformBuffer,
    VkImageView colorImageView,
    VkImageView depthImageView,
    VkBuffer storageBuffer
) const
{
    VkDescriptorBufferInfo parametersWriteBuffer{};
    VkWriteDescriptorSet parametersWrite{};

    uniformBufferDescriptorSetWrite(descriptorSet, 0, uniformBuffer, &parametersWriteBuffer, &parametersWrite);

    VkDescriptorImageInfo colorImageWriteImage{};
    VkWriteDescriptorSet colorImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 1, colorImageView, ctx->unfilteredSampler, &colorImageWriteImage, &colorImageWrite);

    VkDescriptorImageInfo depthImageWriteImage{};
    VkWriteDescriptorSet depthImageWrite{};

    combinedSamplerDescriptorSetWrite(descriptorSet, 2, depthImageView, ctx->unfilteredSampler, &depthImageWriteImage, &depthImageWrite);

    VkDescriptorBufferInfo storageWriteBuffer{};
    VkWriteDescriptorSet storageWrite{};

    storageBufferDescriptorSetWrite(descriptorSet, 3, storageBuffer, &storageWriteBuffer, &storageWrite);

    VkWriteDescriptorSet descriptorWrites[] = {
        parametersWrite,
        colorImageWrite,
        depthImageWrite,
        storageWrite
    };

    vkUpdateDescriptorSets(
        ctx->device,
        sizeof(descriptorWrites) / sizeof(VkWriteDescriptorSet),
        descriptorWrites,
        0,
        nullptr
    );
}

VmaBuffer RenderVRDepthOfFieldEffect::createStorageBuffer() const
{
    VmaBuffer buffer = createBuffer(
        ctx->allocator,
        sizeof(f32) * 4 * eyeCount,
        VK_BUFFER_USAGE_STORAGE_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );

    VmaMapping<f32> mapping(ctx->allocator, buffer);

    mapping.data[0] = defaultFarDistance;
    mapping.data[1] = defaultFarDistance;
    mapping.data[2] = 0;
    mapping.data[3] = 0;
    mapping.data[4] = defaultFarDistance;
    mapping.data[5] = defaultFarDistance;
    mapping.data[6] = 0;
    mapping.data[7] = 0;

    return buffer;
}
#endif
