/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#include "render_vr_steamvr_color_adjustment.hpp"
#include "render_tools.hpp"

RenderVRSteamVRColorAdjustment::RenderVRSteamVRColorAdjustment(
    const RenderContext* ctx,
    RenderVRPipelineStore* pipelineStore,
    vector<VRSwapchainElement*> swapchainElements
)
    : RenderVRPostProcessEffect(
        ctx,
        pipelineStore->steamvrColorAdjustment.pipelineLayout,
        pipelineStore->steamvrColorAdjustment.pipeline
    )
{
    createComponents(swapchainElements, pipelineStore->postProcessDescriptorSetLayout);
}

RenderVRSteamVRColorAdjustment::~RenderVRSteamVRColorAdjustment()
{
    destroyComponents();
}

VmaBuffer RenderVRSteamVRColorAdjustment::createUniformBuffer() const
{
    return createBuffer(
        ctx->allocator,
        sizeof(f32),
        VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT,
        VMA_MEMORY_USAGE_CPU_TO_GPU
    );
}

void RenderVRSteamVRColorAdjustment::fillUniformBuffer(VmaMapping<f32>* mapping, f32 timer) const
{
    mapping->data[0] = timer;
}
#endif
