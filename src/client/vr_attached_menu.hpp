/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_VR
#pragma once

#include "types.hpp"
#include "vr_menu.hpp"

class VRAttachedMenu : public VRMenu
{
public:
    VRAttachedMenu(InterfaceView* view);

    bool shouldDraw() const override;
    void step() override;
    void draw(const DrawContext& ctx) const override;

private:
    bool orienting;
    chrono::milliseconds orientStartTime;

    void onDisconnect();
    void onQuit();
    void onDetach();
    void onMagnitude(u32 magnitude);
    void onPing();
    void onBrake();
    void onToggleAnchor();
    void onScaleUp();
    void onScaleDown();
    void onRecenter();
    void onOrientRoom();
    void onResetRoom();
    void onKnowledge();
};
#endif
