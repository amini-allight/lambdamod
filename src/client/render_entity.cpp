/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "render_entity.hpp"
#include "render_tools.hpp"
#include "render_generators.hpp"
#include "tools.hpp"
#include "global.hpp"
#include "entity.hpp"

RenderEntity::RenderEntity(
    RenderInterface* render,

    const RenderPipelineStore* pipelineStore,
    const Entity* entity,
    const BodyPartMask& bodyPartMask,

    bool rayTracing
)
    : render(render)

    , pipelineStore(pipelineStore)
    , id(entity->id())
    , transform(entity->globalTransform())
    , visible(entity->visible())
    , lastBody(entity->body())
{
    auto [ maskedEntityID, hiddenBodyPartIDs ] = bodyPartMask;

    BodyRenderData renderData = entity->body().toRender(id == maskedEntityID ? hiddenBodyPartIDs : set<BodyPartID>());

    body = new RenderEntityBody(
        render->context(),
        pipelineStore,
        renderData.materials
    );

    setBodySideEffects(renderData, rayTracing);
}

RenderEntity::~RenderEntity()
{
    for (const auto& [ body, count, target ] : bodiesToDelete)
    {
        delete body;
    }

    for (const auto& [ component, count, target ] : componentsToDelete)
    {
        delete component;
    }

    delete body;
}

void RenderEntity::work() const
{
    for (size_t i = 0; i < bodiesToDelete.size();)
    {
        auto& [ body, count, target ] = bodiesToDelete.at(i);
        count++;

        if (count == target)
        {
            delete body;
            bodiesToDelete.erase(bodiesToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }

    for (size_t i = 0; i < componentsToDelete.size();)
    {
        auto& [ component, count, target ] = componentsToDelete.at(i);
        count++;

        if (count == target)
        {
            delete component;
            componentsToDelete.erase(componentsToDelete.begin() + i);
        }
        else
        {
            i++;
        }
    }
}

const vector<RenderLight>& RenderEntity::lights() const
{
    static constexpr vector<RenderLight> emptyLights;

    return visible ? _lights : emptyLights;
}

const vector<RenderOcclusionPrimitive>& RenderEntity::occlusionPrimitives() const
{
    return _occlusionPrimitives;
}

const RenderOcclusionMesh& RenderEntity::occlusionMesh() const
{
    return _occlusionMesh;
}

const Body& RenderEntity::occlusionMeshSource() const
{
    return lastBody;
}

mat4 RenderEntity::cameraLocalTransform(const vec3& cameraPosition) const
{
    mat4 transform = this->transform;

    transform.setPosition(transform.position() - cameraPosition);

    return transform;
}

void RenderEntity::setBodySideEffects(const BodyRenderData& renderData, bool rayTracing)
{
    _lights.clear();

    for (const BodyRenderLight& light : renderData.lights)
    {
        _lights.push_back(RenderLight(transform, light));
    }

    _occlusionPrimitives.clear();

    for (const BodyRenderOcclusionPrimitive& occlusionPrimitive : renderData.occlusionPrimitives)
    {
        _occlusionPrimitives.push_back(RenderOcclusionPrimitive(transform, occlusionPrimitive));
    }

    if (rayTracing)
    {
        _occlusionMesh.vertices.clear();
        _occlusionMesh.indices.clear();

        for (const RenderOcclusionPrimitive& occlusionPrimitive : _occlusionPrimitives)
        {
            occlusionPrimitive.toMesh(_occlusionMesh);
        }
    }
}

void RenderEntity::updateBodySideEffects(const mat4& transform)
{
    for (RenderLight& light : _lights)
    {
        light.update(transform);
    }

    for (RenderOcclusionPrimitive& occlusionPrimitive : _occlusionPrimitives)
    {
        occlusionPrimitive.update(transform);
    }
}

void RenderEntity::fillCommandBuffer(
    const MainSwapchainElement* swapchainElement,
    const vector<tuple<BodyRenderMaterialType, VkDescriptorSet>>& descriptorSets,
    deque<RenderTransparentDraw>* transparentDraws
) const
{
    for (size_t i = 0; i < descriptorSets.size(); i++)
    {
        const auto& [ type, material ] = body->materials.at(i);

        if (isMaterialTransparent(type))
        {
            transparentDraws->push_back(RenderTransparentDraw(
                bind(
                    static_cast<void (RenderEntity::*)(const MainSwapchainElement*, VkDescriptorSet, const RenderEntityBodyMaterial*, VkPipelineLayout, VkPipeline) const>(&RenderEntity::fillCommandBuffer),
                    this,
                    swapchainElement,
                    get<1>(descriptorSets.at(i)),
                    material,
                    material->pipelines.pipelineLayout,
                    material->pipelines.pipeline
                ),
                transform.applyToPosition(material->position)
            ));
        }
        else
        {
            fillCommandBuffer(
                swapchainElement,
                get<1>(descriptorSets.at(i)),
                material,
                material->pipelines.pipelineLayout,
                material->pipelines.pipeline
            );
        }
    }
}

void RenderEntity::fillCommandBuffer(
    const MainSwapchainElement* swapchainElement,
    VkDescriptorSet descriptorSet,
    const RenderEntityBodyMaterial* material,
    VkPipelineLayout pipelineLayout,
    VkPipeline pipeline

) const
{
    if (!visible || !material->shouldDraw())
    {
        return;
    }

    VkCommandBuffer commandBuffer = swapchainElement->commandBuffer();
    u32 width = swapchainElement->width();
    u32 height = swapchainElement->height();

    VkViewport viewport;

    if (g_vr)
    {
        viewport = {
            0, 0,
            static_cast<f32>(width), static_cast<f32>(height),
            0, 1
        };
    }
    else
    {
        viewport = {
            0, static_cast<f32>(height),
            static_cast<f32>(width), -static_cast<f32>(height),
            0, 1
        };
    }

    vkCmdSetViewport(commandBuffer, 0, 1, &viewport);

    VkRect2D scissor = {
        { 0, 0 },
        { static_cast<u32>(width), static_cast<u32>(height) }
    };

    vkCmdSetScissor(commandBuffer, 0, 1, &scissor);

    vkCmdBindPipeline(commandBuffer, VK_PIPELINE_BIND_POINT_GRAPHICS, pipeline);

    vkCmdBindDescriptorSets(
        commandBuffer,
        VK_PIPELINE_BIND_POINT_GRAPHICS,
        pipelineLayout,
        0,
        1,
        &descriptorSet,
        0,
        nullptr
    );

    vkCmdBindIndexBuffer(commandBuffer, material->indices.buffer, 0, VK_INDEX_TYPE_UINT32);

    VkDeviceSize offset = 0;
    vkCmdBindVertexBuffers(commandBuffer, 0, 1, &material->vertices.buffer, &offset);

    vkCmdDrawIndexed(commandBuffer, material->indexCount, 1, 0, 0, 0);
}
