/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "popup.hpp"

class ColorPickerPopup : public Popup
{
public:
    ColorPickerPopup(
        InterfaceWidget* parent,
        const vec3& color,
        const function<void(const vec3&)>& onSet
    );

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void draw(const DrawContext& ctx) const override;

private:
    const vec3& color;
    function<void(const vec3&)> onSet;

    f64 hueSource();
    void onHueSet(f64 hue);
    void onSaturationValueSet(const vec2& saturationValue);

    string textSource();
    void onTextReturn(const string& text);

    SizeProperties sizeProperties() const override;
};

class ColorPicker : public InterfaceWidget
{
public:
    ColorPicker(
        InterfaceWidget* parent,
        const function<vec3()>& source,
        const function<void(const vec3&)>& onSet
    );
    ColorPicker(
        InterfaceWidget* parent,
        const function<Color()>& source,
        const function<void(const Color&)>& onSet
    );
    ~ColorPicker();

    void move(const Point& position) override;
    void resize(const Size& size) override;
    void step() override;
    void draw(const DrawContext& ctx) const override;
    void focus() override;
    void unfocus() override;

private:
    vec3 color;

    ColorPickerPopup* popup;

    function<vec3()> source;
    function<void(const vec3&)> _onSet;

    void onSet(const vec3& color);

    void interact(const Input& input);

    SizeProperties sizeProperties() const override;
};
