/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "space_graph_node.hpp"

class SpaceGraph
{
public:
    SpaceGraph();
    SpaceGraph(const map<EntityID, Entity*>& entities);
    SpaceGraph(const SpaceGraph& rhs) = delete;
    SpaceGraph(SpaceGraph&& rhs) = delete;
    ~SpaceGraph();

    SpaceGraph& operator=(const SpaceGraph& rhs) = delete;
    SpaceGraph& operator=(SpaceGraph&& rhs) = delete;

    SpaceGraphNode* root();
    const SpaceGraphNode* root() const;

    void traverse(const function<void(const uvec3&, SpaceGraphNode*)>& behavior);
    void traverseAscending(const vec3& position, const function<void(const uvec3&, SpaceGraphNode*)>& behavior);
    void traverse(const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const;
    void traverseAscending(const vec3& position, const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const;

    void update(const map<EntityID, Entity*>& entities);

private:
    SpaceGraphNode* _root;

    void summarizeEntities(const map<EntityID, Entity*>& in, vector<SpaceGraphGenerationEntity>& out) const;

    void create(const map<EntityID, Entity*>& entities);
    void destroy();

    vector<SpaceGraphGenerationEntity> selectWithin(
        const vector<SpaceGraphGenerationEntity>& entities,
        const vec3& innerExtent,
        const vec3& outerExtent
    ) const;
};
