/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "entity.hpp"
#include "dynamics.hpp"
#include "area_effect_cache.hpp"
#include "world_update.hpp"
#include "sky_parameters.hpp"
#include "atmosphere_parameters.hpp"

class User;
#if defined(LMOD_SERVER)
class UndoStack;
#endif

class World
{
public:
    explicit World(const string& name);
    World(const string& name, const World& rhs);
    World(const World& rhs);
    World(World&& rhs) noexcept;
    ~World();

    World& operator=(const World& rhs);
    World& operator=(World&& rhs) noexcept;

    void enable();
    void disable();

    void step(ScriptContext* context, f64 stepInterval);

    void compare(const World& previous, WorldUpdate& update) const;
    void updateDynamics();

#if defined(LMOD_SERVER)
    void push(ScriptContext* context, User* user, const NetworkEvent& event);
#elif defined(LMOD_CLIENT)
    void push(ScriptContext* context, const NetworkEvent& event);
#endif
    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> initial() const;
    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> terminal() const;

    void handleCollision(ScriptContext* context, Entity* a, Entity* b, const vec3& position);
    void clearSessionUserReferences(UserID userID);
    void clearUserReferences(UserID userID);
    void setPose(EntityID id, const map<VRDevice, mat4>& anchorMapping, const map<VRDevice, mat4>& pose);

    const string& name() const;

    void add(EntityID parentID, Entity* entity);
    void remove(EntityID id);
    Entity* get(EntityID id) const;
    const map<EntityID, Entity*>& entities() const;
    void traverse(const function<void(Entity*)>& behavior);
    void traverse(const function<void(const Entity*)>& behavior) const;
    bool partiallyTraverse(const function<bool(Entity*)>& behavior);
    bool partiallyTraverse(const function<bool(const Entity*)>& behavior) const;

    bool shown() const;
    f64 speedOfSound() const;
    const vec3& gravity() const;
    const vec3& flowVelocity() const;
    f64 density() const;
    const vec3& up() const;
    f64 fogDistance() const;
    const SkyParameters& sky() const;
    const AtmosphereParameters& atmosphere() const;

    f64 speedOfSound(const vec3& position) const;
    const vec3& gravity(const vec3& position) const;
    const vec3& flowVelocity(const vec3& position) const;
    f64 density(const vec3& position) const;
    const vec3& up(const vec3& position) const;
    f64 fogDistance(const vec3& position) const;
    const SkyParameters& sky(const vec3& position) const;
    const AtmosphereParameters& atmosphere(const vec3& position) const;

    void setShown(bool state);
    void setSpeedOfSound(f64 speed);
    void setGravity(const vec3& force);
    void setFlowVelocity(const vec3& velocity);
    void setDensity(f64 density);
    void setUp(const vec3& up);
    void setFogDistance(f64 distance);
    void setSky(const SkyParameters& sky);
    void setAtmosphere(const AtmosphereParameters& atmosphere);

    vector<Entity*> find(const string& pattern) const;
    vector<Entity*> near(const vec3& position, f64 distance) const;
    vector<tuple<Entity*, vec3>> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    vector<Entity*> within(const vec3& position) const;
    vector<Entity*> view(const vec3& position, const vec3& direction, f64 angle, f64 distance) const;

    f64 soundOcclusion(const vec3& a, const vec3& b) const;
    vector<ReverbEffect> soundReverb(const vec3& position) const;

    const SpaceGraph& spaceGraph() const;

#if defined(LMOD_SERVER)
    void recordAdd(UserID userID, const Entity* after);
    void recordRemove(UserID userID, const Entity* before);
    void recordModify(UserID userID, const Entity* before, const Entity* after, bool merge);
#endif

    void setParent(
        Entity* child,
        Entity* parent,
        BodyPartID partID = BodyPartID(),
        BodyPartSubID subID = BodyPartSubID()
    );
    void clearParent(Entity* child);
    bool isCircular(Entity* child, Entity* parent) const;

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    string _name;

    map<EntityID, Entity*> _entities;

    bool _shown;
    f64 _speedOfSound;
    vec3 _gravity;
    vec3 _flowVelocity;
    f64 _density;
    vec3 _up;
    f64 _fogDistance;
    SkyParameters _sky;
    AtmosphereParameters _atmosphere;

    mutable Dynamics* dynamics;
    AreaEffectCache areaEffectCache;

#if defined(LMOD_SERVER)
    map<UserID, UndoStack> undoStacks;

    UndoStack* ensureUndoStack(UserID id);

    void handleWorldEvent(User* user, const WorldEvent& event);
    void handleWorldSkyEvent(User* user, const WorldSkyEvent& event);
    void handleWorldAtmosphereEvent(User* user, const WorldAtmosphereEvent& event);
    void handleEntityAdd(User* user, const EntityEvent& event);
    void handleEntityRemove(User* user, const EntityEvent& event);
    void pushEntityEvent(ScriptContext* context, User* user, const NetworkEvent& event);
#elif defined(LMOD_CLIENT)
    void handleWorldEvent(const WorldEvent& event);
    void handleWorldSkyEvent(const WorldSkyEvent& event);
    void handleWorldAtmosphereEvent(const WorldAtmosphereEvent& event);
    void handleEntityAdd(const EntityEvent& event);
    void handleEntityRemove(const EntityEvent& event);
    void pushEntityEvent(ScriptContext* context, const NetworkEvent& event);
#endif

    void copy(const World& rhs);
};
