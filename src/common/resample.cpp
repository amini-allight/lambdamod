/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "resample.hpp"

vector<f32> linearResample(const vector<f32>& input, f32 scale)
{
    vector<f32> output(ceil(input.size() * scale), 0);

    for (size_t i = 0; i < output.size(); i++)
    {
        f32 elapsed = i / static_cast<f32>(output.size());

        size_t aIndex = elapsed * input.size();
        size_t bIndex = aIndex + 1;

        if (bIndex >= input.size())
        {
            output[i] = input[aIndex];
        }
        else
        {
            f32 t = (elapsed - aIndex / static_cast<f32>(input.size())) / (1.0f / input.size());

            output[i] = input[aIndex] * (1 - t) + input[bIndex] * t;
        }
    }

    return output;
}

vector<f32> hermiteResample(const vector<f32>& input, f32 scale)
{
    vector<f32> output(ceil(input.size() * scale), 0);

    for (size_t i = 0; i < output.size(); i++)
    {
        f32 elapsed = i / static_cast<f32>(output.size());

        i64 bIndex = elapsed * input.size();
        i64 aIndex = bIndex - 1;
        i64 cIndex = bIndex + 1;
        i64 dIndex = bIndex + 2;

        f32 a;
        f32 b = input[bIndex];
        f32 c;
        f32 d;

        if (aIndex >= 0 && aIndex < static_cast<i64>(input.size()))
        {
            a = input[aIndex];
        }
        else
        {
            output[i] = b;
            continue;
        }

        if (cIndex >= 0 && cIndex < static_cast<i64>(input.size()))
        {
            c = input[cIndex];
        }
        else
        {
            output[i] = b;
            continue;
        }

        if (dIndex >= 0 && dIndex < static_cast<i64>(input.size()))
        {
            d = input[dIndex];
        }
        else
        {
            output[i] = b;
            continue;
        }

        f32 t = (elapsed * input.size()) - bIndex;

        f32 c1 = (c - a) * 0.5f;
        f32 c2 = a - (b * 2.5f) + (c + c) - (d * 0.5f);
        f32 c3 = ((b - c) * 1.5f) + ((d - a) * 0.5f);
        output[i] = b + (((c3 * t + c2) * t + c1) * t);
    }

    return output;
}
