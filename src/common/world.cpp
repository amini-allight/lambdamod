/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "world.hpp"
#include "log.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"
#include "error_messages.hpp"

#if defined(LMOD_SERVER)
#include "server/undo_stack.hpp"
#endif

World::World(const string& name)
    : _name(name)
    , _shown(false)
    , _speedOfSound(defaultSpeedOfSound)
    , _gravity(defaultGravity)
    , _density(defaultDensity)
    , _up(upDir)
    , _fogDistance(0)
    , dynamics(nullptr)
{

}

World::World(const string& name, const World& rhs)
{
    *this = World(rhs);

    _name = name;
}

World::World(const World& rhs)
{
    copy(rhs);
    _entities.clear();

    for (const auto& [ id, entity ] : rhs._entities)
    {
        _entities.insert({ id, new Entity(*entity) });
    }

    for (auto& [ id, entity ] : _entities)
    {
        entity->_world = this;
    }

    dynamics = nullptr;
}

World::World(World&& rhs) noexcept
{
    copy(rhs);
    rhs._entities.clear();

    for (auto& [ id, entity ] : _entities)
    {
        entity->_world = this;
    }

    dynamics = nullptr;
}

World::~World()
{
    disable();

    for (const auto& [ id, entity ] : _entities)
    {
        delete entity;
    }
}

World& World::operator=(const World& rhs)
{
    if (&rhs != this)
    {
        disable();

        for (const auto& [ id, entity ] : _entities)
        {
            delete entity;
        }

        copy(rhs);
        _entities.clear();

        for (const auto& [ id, entity ] : rhs._entities)
        {
            _entities.insert({ id, new Entity(*entity) });
        }

        for (auto& [ id, entity ] : _entities)
        {
            entity->_world = this;
        }
    }

    return *this;
}

World& World::operator=(World&& rhs) noexcept
{
    if (&rhs != this)
    {
        disable();

        for (const auto& [ id, entity ] : _entities)
        {
            delete entity;
        }

        copy(rhs);
        rhs._entities.clear();

        for (auto& [ id, entity ] : _entities)
        {
            entity->_world = this;
        }
    }

    return *this;
}

void World::enable()
{
    if (!dynamics)
    {
        dynamics = new Dynamics(this);
    }
}

void World::disable()
{
    if (dynamics)
    {
        delete dynamics;
        dynamics = nullptr;
    }
}

void World::step(ScriptContext* context, f64 stepInterval)
{
    enable();
    dynamics->step(context, stepInterval);

    for (auto& [ id, entity ] : _entities)
    {
        entity->step(context, stepInterval);
    }

    areaEffectCache.updateWorld(this);
}

void World::compare(const World& previous, WorldUpdate& update) const
{
    for (const auto& [ id, entity ] : previous._entities)
    {
        auto it = _entities.find(id);

        if (it == _entities.end())
        {
            update.removeChild(entity);
        }
    }

    for (const auto& [ id, entity ] : _entities)
    {
        auto it = previous._entities.find(id);

        if (it == previous._entities.end())
        {
            update.addChild(entity);
        }
        else
        {
            const Entity* previousEntity = it->second;

            EntityUpdate entityUpdate(entity->world(), entity);

            entity->compare(*previousEntity, entityUpdate);

            update.updateChild(previousEntity, entity, entityUpdate);
        }
    }

    update.notify<bool, WorldEvent>(_shown, previous._shown, [&](WorldEvent& event){
        event.type = _shown ? World_Event_Show : World_Event_Hide;
    });

    update.notify<f64, WorldEvent>(_speedOfSound, previous._speedOfSound, [&](WorldEvent& event){
        event.type = World_Event_Speed_Of_Sound;
        event.data = { _speedOfSound };
    });

    update.notify<vec3, WorldEvent>(_gravity, previous._gravity, [&](WorldEvent& event){
        event.type = World_Event_Gravity;
        event.data = { _gravity.x, _gravity.y, _gravity.z };
    });

    update.notify<vec3, WorldEvent>(_flowVelocity, previous._flowVelocity, [&](WorldEvent& event){
        event.type = World_Event_Flow_Velocity;
        event.data = { _flowVelocity.x, _flowVelocity.y, _flowVelocity.z };
    });

    update.notify<f64, WorldEvent>(_density, previous._density, [&](WorldEvent& event){
        event.type = World_Event_Density;
        event.data = { _density };
    });

    update.notify<vec3, WorldEvent>(_up, previous._up, [&](WorldEvent& event){
        event.type = World_Event_Up;
        event.data = { _up.x, _up.y, _up.z };
    });

    update.notify<f64, WorldEvent>(_fogDistance, previous._fogDistance, [&](WorldEvent& event){
        event.type = World_Event_Fog_Distance;
        event.data = { _fogDistance };
    });

    update.notify<SkyParameters, WorldSkyEvent>(_sky, previous._sky, [&](WorldSkyEvent& event){
        event.sky = _sky;
    });

    update.notify<AtmosphereParameters, WorldAtmosphereEvent>(_atmosphere, previous._atmosphere, [&](WorldAtmosphereEvent& event){
        event.atmosphere = _atmosphere;
    });
}

void World::updateDynamics()
{
    if (!dynamics)
    {
        return;
    }

    dynamics->update();
}

#if defined(LMOD_SERVER)
void World::push(ScriptContext* context, User* user, const NetworkEvent& event)
{
    switch (event.type())
    {
    default :
        error("Non-world/entity event " + event.name() + " was pushed to world.");
        break;
    case Network_Event_World :
        handleWorldEvent(user, event.payload<WorldEvent>());
        break;
    case Network_Event_World_Sky :
        handleWorldSkyEvent(user, event.payload<WorldSkyEvent>());
        break;
    case Network_Event_World_Atmosphere :
        handleWorldAtmosphereEvent(user, event.payload<WorldAtmosphereEvent>());
        break;
    case Network_Event_Entity :
        if (event.payload<EntityEvent>().id == EntityID())
        {
            switch (event.payload<EntityEvent>().type)
            {
            case Entity_Event_Add :
                handleEntityAdd(user, event.payload<EntityEvent>());
                return;
            case Entity_Event_Remove :
                handleEntityRemove(user, event.payload<EntityEvent>());
                return;
            default :
                break;
            }
        }

        [[fallthrough]];
    case Network_Event_Entity_Body :
    case Network_Event_Entity_Sample :
    case Network_Event_Entity_Sample_Playback :
    case Network_Event_Entity_Action :
    case Network_Event_Entity_Action_Playback :
    case Network_Event_Entity_Input_Binding :
    case Network_Event_Entity_HUD :
    case Network_Event_Entity_Script :
        pushEntityEvent(context, user, event);
        break;
    }
}
#elif defined(LMOD_CLIENT)
void World::push(ScriptContext* context, const NetworkEvent& event)
{
    switch (event.type())
    {
    default :
        error("Non-world/entity event " + event.name() + " was pushed to world.");
        break;
    case Network_Event_World :
        handleWorldEvent(event.payload<WorldEvent>());
        break;
    case Network_Event_World_Sky :
        handleWorldSkyEvent(event.payload<WorldSkyEvent>());
        break;
    case Network_Event_World_Atmosphere :
        handleWorldAtmosphereEvent(event.payload<WorldAtmosphereEvent>());
        break;
    case Network_Event_Entity :
        if (event.payload<EntityEvent>().id == EntityID())
        {
            switch (event.payload<EntityEvent>().type)
            {
            case Entity_Event_Add :
                handleEntityAdd(event.payload<EntityEvent>());
                return;
            case Entity_Event_Remove :
                handleEntityRemove(event.payload<EntityEvent>());
                return;
            default :
                break;
            }
        }

        [[fallthrough]];
    case Network_Event_Entity_Body :
    case Network_Event_Entity_Sample :
    case Network_Event_Entity_Sample_Playback :
    case Network_Event_Entity_Action :
    case Network_Event_Entity_Action_Playback :
    case Network_Event_Entity_Input_Binding :
    case Network_Event_Entity_HUD :
    case Network_Event_Entity_Script :
        pushEntityEvent(context, event);
        break;
    }
}
#endif

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> World::initial() const
{
    vector<NetworkEvent> events;
    vector<EntityUpdateSideEffect> sideEffects;

    events.push_back(NetworkEvent(WorldEvent(World_Event_Add, _name)));
    events.push_back(NetworkEvent(WorldEvent(_shown ? World_Event_Show : World_Event_Hide, _name)));
    events.push_back(NetworkEvent(WorldEvent(World_Event_Speed_Of_Sound, _name, { _speedOfSound })));
    events.push_back(NetworkEvent(WorldEvent(World_Event_Gravity, _name, { _gravity.x, _gravity.y, _gravity.z })));
    events.push_back(NetworkEvent(WorldEvent(World_Event_Flow_Velocity, _name, { _flowVelocity.x, _flowVelocity.y, _flowVelocity.z })));
    events.push_back(NetworkEvent(WorldEvent(World_Event_Density, _name, { _density })));
    events.push_back(NetworkEvent(WorldEvent(World_Event_Up, _name, { _up.x, _up.y, _up.z })));
    events.push_back(NetworkEvent(WorldEvent(World_Event_Fog_Distance, _name, { _fogDistance })));
    events.push_back(NetworkEvent(WorldSkyEvent(_name, _sky)));
    events.push_back(NetworkEvent(WorldAtmosphereEvent(_name, _atmosphere)));

    for (const auto& [ id, entity ] : _entities)
    {
        auto [ entityEvents, entitySideEffects ] = entity->initial();

        events.insert(
            events.end(),
            entityEvents.begin(),
            entityEvents.end()
        );

        sideEffects.insert(
            sideEffects.end(),
            entitySideEffects.begin(),
            entitySideEffects.end()
        );
    }

    return { events, sideEffects };
}

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> World::terminal() const
{
    WorldEvent event(World_Event_Remove, _name);

    return {
        { event },
        {}
    };
}

void World::handleCollision(ScriptContext* context, Entity* a, Entity* b, const vec3& position)
{
    a->collide(context, b->id(), position);
    b->collide(context, a->id(), position);
}

void World::clearSessionUserReferences(UserID userID)
{
    traverse([&](Entity* entity) -> void {
        if (entity->selectingUserID() == userID)
        {
            entity->setSelectingUserID(UserID());
        }
    });
}

void World::clearUserReferences(UserID userID)
{
    traverse([&](Entity* entity) -> void {
        if (entity->ownerID() == userID)
        {
            entity->setOwnerID(UserID());
        }

        if (entity->attachedUserID() == userID)
        {
            entity->setAttachedUserID(UserID());
        }
    });
}

void World::setPose(EntityID id, const map<VRDevice, mat4>& anchorMapping, const map<VRDevice, mat4>& pose)
{
    auto it = _entities.find(id);

    if (it == _entities.end())
    {
        return;
    }

    Entity* entity = it->second;

    map<VRDevice, mat4> mappedPose;

    for (const auto& [ device, transform ] : pose)
    {
        mat4 mapped = {
            transform.position() + transform.rotation().rotate(anchorMapping.at(device).position()),
            transform.rotation() * anchorMapping.at(device).rotation(),
            vec3(1)
        };

        mappedPose.insert({
            device,
            mapped
        });
    }

    enable();
    dynamics->updatePose(entity, mappedPose);
}

const string& World::name() const
{
    return _name;
}

void World::add(EntityID parentID, Entity* entity)
{
    if (parentID == EntityID())
    {
        _entities.insert({ entity->id(), entity });
        return;
    }

    for (const auto& [ id, item ] : _entities)
    {
        item->add(parentID, entity);
    }
}

void World::remove(EntityID id)
{
    const Entity* entity = get(id);

    if (!entity)
    {
        return;
    }

    if (!entity->canDelete())
    {
        return;
    }

    auto it = _entities.find(id);

    if (it != _entities.end())
    {
        delete it->second;
        _entities.erase(it);
        return;
    }

    for (const auto& [ entityID, entity ] : _entities)
    {
        entity->remove(id);
    }
}

Entity* World::get(EntityID id) const
{
    for (const auto& [ entityID, entity ] : _entities)
    {
        if (entityID == id)
        {
            return entity;
        }

        Entity* child = entity->get(id);

        if (child)
        {
            return child;
        }
    }

    return nullptr;
}

const map<EntityID, Entity*>& World::entities() const
{
    return _entities;
}

void World::traverse(const function<void(Entity*)>& behavior)
{
    for (auto& [ id, entity ] : _entities)
    {
        entity->traverse(behavior);
    }
}

void World::traverse(const function<void(const Entity*)>& behavior) const
{
    for (const auto& [ id, entity ] : _entities)
    {
        static_cast<const Entity*>(entity)->traverse(behavior);
    }
}

bool World::partiallyTraverse(const function<bool(Entity*)>& behavior)
{
    for (auto& [ id, entity ] : _entities)
    {
        if (entity->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool World::partiallyTraverse(const function<bool(const Entity*)>& behavior) const
{
    for (const auto& [ id, entity ] : _entities)
    {
        if (static_cast<const Entity*>(entity)->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool World::shown() const
{
    return _shown;
}

f64 World::speedOfSound() const
{
    return _speedOfSound;
}

const vec3& World::gravity() const
{
    return _gravity;
}

const vec3& World::flowVelocity() const
{
    return _flowVelocity;
}

f64 World::density() const
{
    return _density;
}

const vec3& World::up() const
{
    return _up;
}

f64 World::fogDistance() const
{
    return _fogDistance;
}

const SkyParameters& World::sky() const
{
    return _sky;
}

const AtmosphereParameters& World::atmosphere() const
{
    return _atmosphere;
}

f64 World::speedOfSound(const vec3& position) const
{
    return areaEffectCache.at(position).speedOfSound;
}

const vec3& World::gravity(const vec3& position) const
{
    return areaEffectCache.at(position).gravity;
}

const vec3& World::flowVelocity(const vec3& position) const
{
    return areaEffectCache.at(position).flowVelocity;
}

f64 World::density(const vec3& position) const
{
    return areaEffectCache.at(position).density;
}

const vec3& World::up(const vec3& position) const
{
    return areaEffectCache.at(position).up;
}

f64 World::fogDistance(const vec3& position) const
{
    return areaEffectCache.at(position).fogDistance;
}

const SkyParameters& World::sky(const vec3& position) const
{
    return areaEffectCache.at(position).sky;
}

const AtmosphereParameters& World::atmosphere(const vec3& position) const
{
    return areaEffectCache.at(position).atmosphere;
}

void World::setShown(bool state)
{
    _shown = state;
}

void World::setSpeedOfSound(f64 speed)
{
    _speedOfSound = max(speed, 0.0);
}

void World::setGravity(const vec3& force)
{
    _gravity = force;
}

void World::setFlowVelocity(const vec3& velocity)
{
    _flowVelocity = velocity;
}

void World::setDensity(f64 density)
{
    _density = max(density, 0.0);
}

void World::setUp(const vec3& up)
{
    _up = up.normalize();
}

void World::setFogDistance(f64 distance)
{
    _fogDistance = max(distance, 0.0);
}

void World::setSky(const SkyParameters& sky)
{
    _sky = sky;

    _sky.verify();
}

void World::setAtmosphere(const AtmosphereParameters& atmosphere)
{
    _atmosphere = atmosphere;

    _atmosphere.verify();
}

vector<Entity*> World::find(const string& pattern) const
{
    vector<Entity*> result;

    for (auto& [ id, entity ] : entities())
    {
        if (entity->name().find(pattern) == string::npos)
        {
            continue;
        }

        result.push_back(entity);
    }

    return result;
}

vector<Entity*> World::near(const vec3& position, f64 distance) const
{
    vector<Entity*> result;

    for (auto& [ id, entity ] : entities())
    {
        f64 entityDistance = entity->globalPosition().distance(position);

        if (entityDistance > distance)
        {
            continue;
        }

        result.push_back(entity);
    }

    sort(
        result.begin(),
        result.end(),
        [&](const Entity* a, const Entity* b) -> bool
        {
            return position.distance(a->globalPosition()) < position.distance(b->globalPosition());
        }
    );

    return result;
}

vector<tuple<Entity*, vec3>> World::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vector<tuple<Entity*, vec3>> result;

    traverse([&](const Entity* entity) -> void {
        vector<vec3> intersections = entity->intersect(
            position,
            direction,
            distance
        );

        if (intersections.empty())
        {
            return;
        }

        vec3 intersection = intersections.front();

        f64 hitDistance = intersection.distance(position);

        if (hitDistance > distance)
        {
            return;
        }

        result.push_back({ const_cast<Entity*>(entity), intersection });
    });

    sort(
        result.begin(),
        result.end(),
        [&](const tuple<Entity*, vec3>& a, const tuple<Entity*, vec3>& b) -> bool
        {
            return position.distance(std::get<1>(a)) < position.distance(std::get<1>(b));
        }
    );

    return result;
}

vector<Entity*> World::within(const vec3& position) const
{
    vector<Entity*> result;

    traverse([&](const Entity* entity) -> void {
        if (entity->within(position))
        {
            result.push_back(const_cast<Entity*>(entity));
        }
    });

    return result;
}

vector<Entity*> World::view(const vec3& position, const vec3& direction, f64 angle, f64 distance) const
{
    vector<Entity*> result;

    for (auto& [ id, entity ] : entities())
    {
        if (entity->globalPosition().distance(position) > distance)
        {
            continue;
        }

        if (angleBetween((entity->globalPosition() - position).normalize(), direction) > angle / 2)
        {
            continue;
        }

        result.push_back(entity);
    }

    return result;
}

f64 World::soundOcclusion(const vec3& a, const vec3& b) const
{
    return dynamics->soundOcclusion(a, b);
}

vector<ReverbEffect> World::soundReverb(const vec3& position) const
{
    return dynamics->soundReverb(position);
}

const SpaceGraph& World::spaceGraph() const
{
    return dynamics->spaceGraph();
}

#if defined(LMOD_SERVER)
void World::recordAdd(UserID userID, const Entity* after)
{
    ensureUndoStack(userID)->add(after);
}

void World::recordRemove(UserID userID, const Entity* before)
{
    ensureUndoStack(userID)->remove(before);
}

void World::recordModify(UserID userID, const Entity* before, const Entity* after, bool merge)
{
    ensureUndoStack(userID)->modify(before, after, merge);
}
#endif

void World::setParent(
    Entity* child,
    Entity* parent,
    BodyPartID partID,
    BodyPartSubID subID
)
{
    if (isCircular(child, parent))
    {
        return;
    }

    auto newEntity = new Entity(parent, child->id(), *child);
    newEntity->setParentPartID(partID);
    newEntity->setParentPartSubID(subID);
    newEntity->setGlobalTransform(child->globalTransform());
    newEntity->setLinearVelocity(vec3());
    newEntity->setAngularVelocity(vec3());

    remove(child->id());
    add(parent->id(), newEntity);
}

void World::clearParent(Entity* child)
{
    if (!child->parent())
    {
        return;
    }

    auto newEntity = new Entity(this, child->id(), *child);
    newEntity->setParentPartID(BodyPartID());
    newEntity->setParentPartSubID(BodyPartSubID());
    newEntity->setGlobalTransform(child->globalTransform());

    remove(child->id());
    add(EntityID(), newEntity);
}

bool World::isCircular(Entity* child, Entity* parent) const
{
    return !child->hasParent(parent) && child->rootParent() == parent->rootParent();
}

#if defined(LMOD_SERVER)
UndoStack* World::ensureUndoStack(UserID id)
{
    auto it = undoStacks.find(id);

    if (it != undoStacks.end())
    {
        return &it->second;
    }
    else
    {
        undoStacks.insert({ id, UndoStack(id) });

        return &undoStacks.at(id);
    }
}

void World::handleWorldEvent(User* user, const WorldEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("modify-world"));
        return;
    }

    switch (event.type)
    {
    default :
        break;
    case World_Event_Show :
        setShown(true);
        break;
    case World_Event_Hide :
        setShown(false);
        break;
    case World_Event_Speed_Of_Sound :
        if (event.data.size() != 1)
        {
            return;
        }

        setSpeedOfSound(event.data.front());
        break;
    case World_Event_Gravity :
        if (event.data.size() != 3)
        {
            return;
        }

        setGravity({ event.data[0], event.data[1], event.data[2] });
        break;
    case World_Event_Flow_Velocity :
        if (event.data.size() != 3)
        {
            return;
        }

        setFlowVelocity({ event.data[0], event.data[1], event.data[2] });
        break;
    case World_Event_Density :
        if (event.data.size() != 1)
        {
            return;
        }

        setDensity(event.data.front());
        break;
    case World_Event_Up :
        if (event.data.size() != 3)
        {
            return;
        }

        setUp({ event.data[0], event.data[1], event.data[2] });
        break;
    case World_Event_Fog_Distance :
        if (event.data.size() != 1)
        {
            return;
        }

        setFogDistance(event.data.front());
        break;
    case World_Event_Undo :
        ensureUndoStack(user->id())->undo(this);
        break;
    case World_Event_Redo :
        ensureUndoStack(user->id())->redo(this);
        break;
    }
}

void World::handleWorldSkyEvent(User* user, const WorldSkyEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("modify-world"));
        return;
    }

    setSky(event.sky);
}

void World::handleWorldAtmosphereEvent(User* user, const WorldAtmosphereEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("modify-world"));
        return;
    }

    setAtmosphere(event.atmosphere);
}

void World::handleEntityAdd(User* user, const EntityEvent& event)
{
    auto it = _entities.find(EntityID(event.u));

    if (it != _entities.end())
    {
        user->pushMessage(entityFoundError(EntityID(event.u)));
        return;
    }


    auto entity = new Entity(this, EntityID(event.u));

    entity->setName(event.s);
    entity->setOwnerID(user->id());

    recordAdd(user->id(), entity);

    add(EntityID(), entity);
}

void World::handleEntityRemove(User* user, const EntityEvent& event)
{
    auto it = _entities.find(EntityID(event.u));

    if (it == _entities.end())
    {
        user->pushMessage(entityNotFoundError(EntityID(event.u)));
        return;
    }

    Entity* entity = it->second;

    if (!entity->allowed(user))
    {
        user->error(permissionDeniedError("remove-entity"));
        return;
    }

    recordRemove(user->id(), entity);

    remove(entity->id());
}

void World::pushEntityEvent(ScriptContext* context, User* user, const NetworkEvent& event)
{
    for (const auto& [ id, entity ] : _entities)
    {
        entity->push(user, context, event);
    }
}
#elif defined(LMOD_CLIENT)
void World::handleWorldEvent(const WorldEvent& event)
{
    switch (event.type)
    {
    default :
        break;
    case World_Event_Show :
        setShown(true);
        break;
    case World_Event_Hide :
        setShown(false);
        break;
    case World_Event_Speed_Of_Sound :
        if (event.data.size() != 1)
        {
            return;
        }

        setSpeedOfSound(event.data.front());
        break;
    case World_Event_Gravity :
        if (event.data.size() != 3)
        {
            return;
        }

        setGravity({ event.data[0], event.data[1], event.data[2] });
        break;
    case World_Event_Flow_Velocity :
        if (event.data.size() != 3)
        {
            return;
        }

        setFlowVelocity({ event.data[0], event.data[1], event.data[2] });
        break;
    case World_Event_Density :
        if (event.data.size() != 1)
        {
            return;
        }

        setDensity(event.data.front());
        break;
    case World_Event_Up :
        if (event.data.size() != 3)
        {
            return;
        }

        setUp({ event.data[0], event.data[1], event.data[2] });
        break;
    case World_Event_Fog_Distance :
        if (event.data.size() != 1)
        {
            return;
        }

        setFogDistance(event.data.front());
        break;
    }
}

void World::handleWorldSkyEvent(const WorldSkyEvent& event)
{
    setSky(event.sky);
}

void World::handleWorldAtmosphereEvent(const WorldAtmosphereEvent& event)
{
    setAtmosphere(event.atmosphere);
}

void World::handleEntityAdd(const EntityEvent& event)
{
    auto it = _entities.find(EntityID(event.u));

    if (it != _entities.end())
    {
        return;
    }

    auto entity = new Entity(this, EntityID(event.u));

    entity->setName(event.s);

    add(EntityID(), entity);
}

void World::handleEntityRemove(const EntityEvent& event)
{
    auto it = _entities.find(EntityID(event.u));

    if (it == _entities.end())
    {
        return;
    }

    Entity* entity = it->second;

    remove(entity->id());
}

void World::pushEntityEvent(ScriptContext* context, const NetworkEvent& event)
{
    for (const auto& [ id, entity ] : _entities)
    {
        entity->push(context, event);
    }
}
#endif

void World::copy(const World& rhs)
{
    _name = rhs._name;

    _entities = rhs._entities;

    _shown = rhs._shown;
    _speedOfSound = rhs._speedOfSound;
    _gravity = rhs._gravity;
    _flowVelocity = rhs._flowVelocity;
    _density = rhs._density;
    _up = rhs._up;
    _fogDistance = rhs._fogDistance;
    _sky = rhs._sky;
    _atmosphere = rhs._atmosphere;

    // Don't handle dynamics

#if defined(LMOD_SERVER)
    undoStacks = rhs.undoStacks;
#endif
}

template<>
World* YAMLNode::convert() const
{
    auto world = new World(at("name").as<string>());

    for (const auto& [ id, node ] : at("entities").as<map<EntityID, YAMLNode>>())
    {
        world->_entities.insert({ id, Entity::load(world, node) });
    }
    world->_shown = at("shown").as<bool>();
    world->_speedOfSound = at("speedOfSound").as<f64>();
    world->_gravity = at("gravity").as<vec3>();
    world->_flowVelocity = at("flowVelocity").as<vec3>();
    world->_density = at("density").as<f64>();
    world->_up = at("up").as<vec3>();
    world->_fogDistance = at("fogDistance").as<f64>();
    world->_sky = at("sky").as<SkyParameters>();
    world->_atmosphere = at("atmosphere").as<AtmosphereParameters>();

    return world;
}

template<>
void YAMLSerializer::emit(World* v)
{
    startMapping();

    emitPair("name", v->name());

    emitPair("shown", v->_shown);
    emitPair("entities", v->_entities);
    emitPair("speedOfSound", v->_speedOfSound);
    emitPair("gravity", v->_gravity);
    emitPair("flowVelocity", v->_flowVelocity);
    emitPair("density", v->_density);
    emitPair("up", v->_up);
    emitPair("fogDistance", v->_fogDistance);
    emitPair("sky", v->_sky);
    emitPair("atmosphere", v->_atmosphere);

    endMapping();
}
