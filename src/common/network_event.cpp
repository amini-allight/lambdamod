/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "network_event.hpp"

NetworkEvent::NetworkEvent(const string& data)
{
    _type = *reinterpret_cast<const NetworkEventType*>(data.data());

    msgpack::object_handle oh = msgpack::unpack(
        data.data() + sizeof(NetworkEventType),
        data.size() - sizeof(NetworkEventType)
    );

    switch (_type)
    {
    case Network_Event_Ping :
    {
        PingEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Join :
    {
        JoinEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Leave :
    {
        LeaveEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Welcome :
    {
        WelcomeEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Kick :
    {
        KickEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_User :
    {
        UserEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Team :
    {
        TeamEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Checkpoint :
    {
        CheckpointEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Document :
    {
        DocumentEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Game :
    {
        GameEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_World :
    {
        WorldEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_World_Sky :
    {
        WorldSkyEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_World_Atmosphere :
    {
        WorldAtmosphereEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity :
    {
        EntityEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Body :
    {
        EntityBodyEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Sample :
    {
        EntitySampleEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Sample_Playback :
    {
        EntitySamplePlaybackEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Action :
    {
        EntityActionEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Action_Playback :
    {
        EntityActionPlaybackEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Input_Binding :
    {
        EntityInputBindingEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_HUD :
    {
        EntityHUDEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Entity_Script :
    {
        EntityScriptEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Script :
    {
        ScriptEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Attach :
    {
        AttachEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Detach :
    {
        DetachEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Move :
    {
        MoveEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Haptic :
    {
        HapticEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Input :
    {
        InputEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Grip :
    {
        GripEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Command :
    {
        CommandEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Pose :
    {
        PoseEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Log :
    {
        LogEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_History :
    {
        HistoryEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Text_Request :
    {
        TextRequestEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Text_Response :
    {
        TextResponseEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Tether :
    {
        TetherEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Confinement :
    {
        ConfinementEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Goto :
    {
        GotoEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Fade :
    {
        FadeEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Wait :
    {
        WaitEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Voice :
    {
        VoiceEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Voice_Channel :
    {
        VoiceChannelEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Step :
    {
        StepEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_URL :
    {
        URLEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    case Network_Event_Save :
    {
        SaveEvent event;
        oh.get().convert(event);
        _payload = event;
        break;
    }
    }
}

string NetworkEvent::data() const
{
    stringstream ss;

    switch (_type)
    {
    case Network_Event_Ping :
        msgpack::pack(ss, get<PingEvent>(_payload));
        break;
    case Network_Event_Join :
        msgpack::pack(ss, get<JoinEvent>(_payload));
        break;
    case Network_Event_Leave :
        msgpack::pack(ss, get<LeaveEvent>(_payload));
        break;
    case Network_Event_Welcome :
        msgpack::pack(ss, get<WelcomeEvent>(_payload));
        break;
    case Network_Event_Kick :
        msgpack::pack(ss, get<KickEvent>(_payload));
        break;
    case Network_Event_User :
        msgpack::pack(ss, get<UserEvent>(_payload));
        break;
    case Network_Event_Team :
        msgpack::pack(ss, get<TeamEvent>(_payload));
        break;
    case Network_Event_Checkpoint :
        msgpack::pack(ss, get<CheckpointEvent>(_payload));
        break;
    case Network_Event_Document :
        msgpack::pack(ss, get<DocumentEvent>(_payload));
        break;
    case Network_Event_Game :
        msgpack::pack(ss, get<GameEvent>(_payload));
        break;
    case Network_Event_World :
        msgpack::pack(ss, get<WorldEvent>(_payload));
        break;
    case Network_Event_World_Sky :
        msgpack::pack(ss, get<WorldSkyEvent>(_payload));
        break;
    case Network_Event_World_Atmosphere :
        msgpack::pack(ss, get<WorldAtmosphereEvent>(_payload));
        break;
    case Network_Event_Entity :
        msgpack::pack(ss, get<EntityEvent>(_payload));
        break;
    case Network_Event_Entity_Body :
        msgpack::pack(ss, get<EntityBodyEvent>(_payload));
        break;
    case Network_Event_Entity_Sample :
        msgpack::pack(ss, get<EntitySampleEvent>(_payload));
        break;
    case Network_Event_Entity_Sample_Playback :
        msgpack::pack(ss, get<EntitySamplePlaybackEvent>(_payload));
        break;
    case Network_Event_Entity_Action :
        msgpack::pack(ss, get<EntityActionEvent>(_payload));
        break;
    case Network_Event_Entity_Action_Playback :
        msgpack::pack(ss, get<EntityActionPlaybackEvent>(_payload));
        break;
    case Network_Event_Entity_Input_Binding :
        msgpack::pack(ss, get<EntityInputBindingEvent>(_payload));
        break;
    case Network_Event_Entity_HUD :
        msgpack::pack(ss, get<EntityHUDEvent>(_payload));
        break;
    case Network_Event_Entity_Script :
        msgpack::pack(ss, get<EntityScriptEvent>(_payload));
        break;
    case Network_Event_Script :
        msgpack::pack(ss, get<ScriptEvent>(_payload));
        break;
    case Network_Event_Attach :
        msgpack::pack(ss, get<AttachEvent>(_payload));
        break;
    case Network_Event_Detach :
        msgpack::pack(ss, get<DetachEvent>(_payload));
        break;
    case Network_Event_Move :
        msgpack::pack(ss, get<MoveEvent>(_payload));
        break;
    case Network_Event_Haptic :
        msgpack::pack(ss, get<HapticEvent>(_payload));
        break;
    case Network_Event_Input :
        msgpack::pack(ss, get<InputEvent>(_payload));
        break;
    case Network_Event_Grip :
        msgpack::pack(ss, get<GripEvent>(_payload));
        break;
    case Network_Event_Command :
        msgpack::pack(ss, get<CommandEvent>(_payload));
        break;
    case Network_Event_Pose :
        msgpack::pack(ss, get<PoseEvent>(_payload));
        break;
    case Network_Event_Log :
        msgpack::pack(ss, get<LogEvent>(_payload));
        break;
    case Network_Event_History :
        msgpack::pack(ss, get<HistoryEvent>(_payload));
        break;
    case Network_Event_Text_Request :
        msgpack::pack(ss, get<TextRequestEvent>(_payload));
        break;
    case Network_Event_Text_Response :
        msgpack::pack(ss, get<TextResponseEvent>(_payload));
        break;
    case Network_Event_Tether :
        msgpack::pack(ss, get<TetherEvent>(_payload));
        break;
    case Network_Event_Confinement :
        msgpack::pack(ss, get<ConfinementEvent>(_payload));
        break;
    case Network_Event_Goto :
        msgpack::pack(ss, get<GotoEvent>(_payload));
        break;
    case Network_Event_Fade :
        msgpack::pack(ss, get<FadeEvent>(_payload));
        break;
    case Network_Event_Wait :
        msgpack::pack(ss, get<WaitEvent>(_payload));
        break;
    case Network_Event_Voice :
        msgpack::pack(ss, get<VoiceEvent>(_payload));
        break;
    case Network_Event_Voice_Channel :
        msgpack::pack(ss, get<VoiceChannelEvent>(_payload));
        break;
    case Network_Event_Step :
        msgpack::pack(ss, get<StepEvent>(_payload));
        break;
    case Network_Event_URL :
        msgpack::pack(ss, get<URLEvent>(_payload));
        break;
    case Network_Event_Save :
        msgpack::pack(ss, get<SaveEvent>(_payload));
        break;
    }

    return string(reinterpret_cast<const char*>(&_type), sizeof(NetworkEventType)) + ss.str();
}

NetworkEventType NetworkEvent::type() const
{
    return _type;
}

#if defined(LMOD_SERVER)
bool NetworkEvent::isSystem() const
{
    switch (_type)
    {
    default : fatal("Encountered unknown network event type '" + to_string(_type) + "'.");
    case Network_Event_Ping : return true;
    case Network_Event_Join : return true;
    case Network_Event_Leave : return true;
    case Network_Event_Welcome : return false;
    case Network_Event_Kick : return false;
    case Network_Event_User : return false;
    case Network_Event_Team : return false;
    case Network_Event_Checkpoint : return true;
    case Network_Event_Document : return false;
    case Network_Event_Game : return false;
    case Network_Event_World : return false;
    case Network_Event_World_Sky : return false;
    case Network_Event_World_Atmosphere : return false;
    case Network_Event_Entity : return false;
    case Network_Event_Entity_Body : return false;
    case Network_Event_Entity_Sample : return false;
    case Network_Event_Entity_Sample_Playback : return false;
    case Network_Event_Entity_Action : return false;
    case Network_Event_Entity_Action_Playback : return false;
    case Network_Event_Entity_Input_Binding : return false;
    case Network_Event_Entity_HUD : return false;
    case Network_Event_Entity_Script : return false;
    case Network_Event_Script : return false;
    case Network_Event_Attach : return false;
    case Network_Event_Detach : return false;
    case Network_Event_Move : return false;
    case Network_Event_Haptic : return false;
    case Network_Event_Input : return false;
    case Network_Event_Grip : return false;
    case Network_Event_Command : return false;
    case Network_Event_Pose : return false;
    case Network_Event_Log : return false;
    case Network_Event_History : return false;
    case Network_Event_Text_Request : return false;
    case Network_Event_Text_Response : return false;
    case Network_Event_Tether : return false;
    case Network_Event_Confinement : return false;
    case Network_Event_Goto : return false;
    case Network_Event_Fade : return false;
    case Network_Event_Wait : return false;
    case Network_Event_Voice : return true;
    case Network_Event_Voice_Channel : return false;
    case Network_Event_Step : return false;
    case Network_Event_URL : return false;
    case Network_Event_Save : return true;
    }
}
#elif defined(LMOD_CLIENT)
bool NetworkEvent::isSystem() const
{
    switch (_type)
    {
    default : fatal("Encountered unknown network event type '" + to_string(_type) + "'.");
    case Network_Event_Ping : return true;
    case Network_Event_Join : return false;
    case Network_Event_Leave : return false;
    case Network_Event_Welcome : return true;
    case Network_Event_Kick : return true;
    case Network_Event_User : return false;
    case Network_Event_Team : return false;
    case Network_Event_Checkpoint : return true;
    case Network_Event_Document : return true;
    case Network_Event_Game : return false;
    case Network_Event_World : return false;
    case Network_Event_World_Sky : return false;
    case Network_Event_World_Atmosphere : return false;
    case Network_Event_Entity : return false;
    case Network_Event_Entity_Body : return false;
    case Network_Event_Entity_Sample : return false;
    case Network_Event_Entity_Sample_Playback : return false;
    case Network_Event_Entity_Action : return false;
    case Network_Event_Entity_Action_Playback : return false;
    case Network_Event_Entity_Input_Binding : return false;
    case Network_Event_Entity_HUD : return false;
    case Network_Event_Entity_Script : return false;
    case Network_Event_Script : return false;
    case Network_Event_Attach : return true;
    case Network_Event_Detach : return true;
    case Network_Event_Move : return true;
    case Network_Event_Haptic : return true;
    case Network_Event_Input : return false;
    case Network_Event_Grip : return false;
    case Network_Event_Command : return false;
    case Network_Event_Pose : return false;
    case Network_Event_Log : return true;
    case Network_Event_History : return true;
    case Network_Event_Text_Request : return true;
    case Network_Event_Text_Response : return false;
    case Network_Event_Tether : return true;
    case Network_Event_Confinement : return true;
    case Network_Event_Goto : return false;
    case Network_Event_Fade : return true;
    case Network_Event_Wait : return true;
    case Network_Event_Voice : return true;
    case Network_Event_Voice_Channel : return true;
    case Network_Event_Step : return false;
    case Network_Event_URL : return true;
    case Network_Event_Save : return true;
    }
}
#endif

string NetworkEvent::name() const
{
    string s;

    switch (type())
    {
    case Network_Event_Ping :
        s += "ping";
        break;
    case Network_Event_Join :
        s += "join";
        break;
    case Network_Event_Leave :
        s += "leave";
        break;
    case Network_Event_Welcome :
        s += "welcome";
        break;
    case Network_Event_Kick :
        s += "kick";
        break;
    case Network_Event_User :
        s += "user/";
        switch (payload<UserEvent>().type)
        {
        case User_Event_Add :
            s += "add";
            break;
        case User_Event_Remove :
            s += "remove";
            break;
        case User_Event_Kick :
            s += "kick";
            break;
        case User_Event_Shift_Forced :
            s += "shift-forced";
            break;
        case User_Event_Move_Forced :
            s += "move-force";
            break;
        case User_Event_Shift :
            s += "shift";
            break;
        case User_Event_Move_Viewer :
            s += "move-viewer";
            break;
        case User_Event_Move_Room :
            s += "move-room";
            break;
        case User_Event_Move_Devices :
            s += "move-devices";
            break;
        case User_Event_Admin :
            s += "admin";
            break;
        case User_Event_Angle :
            s += "angle";
            break;
        case User_Event_Aspect :
            s += "aspect";
            break;
        case User_Event_Bounds :
            s += "bounds";
            break;
        case User_Event_Color :
            s += "color";
            break;
        case User_Event_Teams :
            s += "teams";
            break;
        case User_Event_Join :
            s += "join";
            break;
        case User_Event_Leave :
            s += "leave";
            break;
        case User_Event_Attach :
            s += "attach";
            break;
        case User_Event_Detach :
            s += "detach";
            break;
        case User_Event_Anchor :
            s += "anchor";
            break;
        case User_Event_Unanchor :
            s += "unanchor";
            break;
        case User_Event_Room_Offset :
            s += "room-offset";
            break;
        case User_Event_Ping :
            s += "ping";
            break;
        case User_Event_Magnitude :
            s += "magnitude";
            break;
        case User_Event_Advantage :
            s += "advantage";
            break;
        case User_Event_Doom :
            s += "doom";
            break;
        case User_Event_Tether :
            s += "tether";
            break;
        case User_Event_Confinement :
            s += "confinement";
            break;
        case User_Event_Fade :
            s += "fade";
            break;
        case User_Event_Wait :
            s += "wait";
            break;
        case User_Event_Signal :
            s += "signal";
            break;
        case User_Event_Titlecard :
            s += "titlecard";
            break;
        case User_Event_Texts :
            s += "texts";
            break;
        case User_Event_Knowledge :
            s += "knowledge";
            break;
        }
        break;
    case Network_Event_Team :
        s += "team/";
        switch (payload<TeamEvent>().type)
        {
        case Team_Event_Add :
            s += "add";
            break;
        case Team_Event_Remove :
            s += "remove";
            break;
        case Team_Event_Update :
            s += "update";
            break;
        }
        break;
    case Network_Event_Checkpoint :
        s += "checkpoint/";
        switch (payload<CheckpointEvent>().type)
        {
        case Checkpoint_Event_Add :
            s += "add";
            break;
        case Checkpoint_Event_Remove :
            s += "remove";
            break;
        case Checkpoint_Event_Revert :
            s += "revert";
            break;
        }
        break;
    case Network_Event_Document :
        s += "document/";
        switch (payload<DocumentEvent>().type)
        {
        case Document_Event_Add :
            s += "add";
            break;
        case Document_Event_Remove :
            s += "remove";
            break;
        case Document_Event_Update :
            s += "update";
            break;
        }
        break;
    case Network_Event_Game :
        s += "game/";
        switch (payload<GameEvent>().type)
        {
        case Game_Event_Attach :
            s += "attach";
            break;
        case Game_Event_Detach :
            s += "detach";
            break;
        case Game_Event_Select :
            s += "select";
            break;
        case Game_Event_Deselect :
            s += "deselect";
            break;
        case Game_Event_Splash_Message :
            s += "splash-message";
            break;
        case Game_Event_Time_Multiplier :
            s += "time-multiplier";
            break;
        case Game_Event_Lock_Attachments :
            s += "lock-attachments";
            break;
        case Game_Event_Unlock_Attachments :
            s += "unlock-attachments";
            break;
        case Game_Event_Ping :
            s += "ping";
            break;
        case Game_Event_Brake :
            s += "brake";
            break;
        case Game_Event_Unbrake :
            s += "unbrake";
            break;
        case Game_Event_Magnitude_Limit :
            s += "magnitude-limit";
            break;
        case Game_Event_Next_Entity_ID :
            s += "next-entity-id";
            break;
        case Game_Event_Next_Timer_ID :
            s += "next-timer-id";
            break;
        case Game_Event_Next_Repeater_ID :
            s += "next-repeater-id";
            break;
        }
        break;
    case Network_Event_World :
        s += "world/";
        switch (payload<WorldEvent>().type)
        {
        case World_Event_Add :
            s += "add";
            break;
        case World_Event_Remove :
            s += "remove";
            break;
        case World_Event_Show :
            s += "show";
            break;
        case World_Event_Hide :
            s += "hide";
            break;
        case World_Event_Speed_Of_Sound :
            s += "speed-of-sound";
            break;
        case World_Event_Gravity :
            s += "gravity";
            break;
        case World_Event_Flow_Velocity :
            s += "flow-velocity";
            break;
        case World_Event_Density :
            s += "density";
            break;
        case World_Event_Up :
            s += "up";
            break;
        case World_Event_Fog_Distance :
            s += "fog-distance";
            break;
        case World_Event_Undo :
            s += "undo";
            break;
        case World_Event_Redo :
            s += "redo";
            break;
        }
        break;
    case Network_Event_World_Sky :
        s += "world-sky";
        break;
    case Network_Event_World_Atmosphere :
        s += "world-atmosphere";
        break;
    case Network_Event_Entity :
        s += "entity/";
        switch (payload<EntityEvent>().type)
        {
        case Entity_Event_Add :
            s += "add";
            break;
        case Entity_Event_Remove :
            s += "remove";
            break;
        case Entity_Event_Parent_Part_ID :
            s += "parent-part-id";
            break;
        case Entity_Event_Parent_Part_Sub_ID :
            s += "parent-part-sub-id";
            break;
        case Entity_Event_Name :
            s += "name";
            break;
        case Entity_Event_Activate :
            s += "activate";
            break;
        case Entity_Event_Deactivate :
            s += "deactivate";
            break;
        case Entity_Event_Transform :
            s += "transform";
            break;
        case Entity_Event_Linear_Velocity :
            s += "linear-velocity";
            break;
        case Entity_Event_Angular_Velocity :
            s += "angular-velocity";
            break;
        case Entity_Event_Owner_ID :
            s += "owner-id";
            break;
        case Entity_Event_Attached_User_ID :
            s += "attached-user-id";
            break;
        case Entity_Event_Selecting_User_ID :
            s += "selecting-user-id";
            break;
        case Entity_Event_Mass :
            s += "mass";
            break;
        case Entity_Event_Drag :
            s += "drag";
            break;
        case Entity_Event_Buoyancy :
            s += "buoyancy";
            break;
        case Entity_Event_Lift :
            s += "lift";
            break;
        case Entity_Event_Magnetism :
            s += "magnetism";
            break;
        case Entity_Event_Friction :
            s += "friction";
            break;
        case Entity_Event_Restitution :
            s += "restitution";
            break;
        case Entity_Event_Start :
            s += "start";
            break;
        case Entity_Event_Stop :
            s += "stop";
            break;
        case Entity_Event_Transfer :
            s += "transfer";
            break;
        case Entity_Event_Share :
            s += "share";
            break;
        case Entity_Event_Unshare :
            s += "unshare";
            break;
        case Entity_Event_Take :
            s += "take";
            break;
        case Entity_Event_Discard :
            s += "discard";
            break;
        case Entity_Event_Hostify :
            s += "hostify";
            break;
        case Entity_Event_Unhostify :
            s += "unhostify";
            break;
        case Entity_Event_Show :
            s += "show";
            break;
        case Entity_Event_Hide :
            s += "hide";
            break;
        case Entity_Event_Physicalize :
            s += "physicalize";
            break;
        case Entity_Event_Etherealize :
            s += "etherealize";
            break;
        case Entity_Event_Visualize :
            s += "visualize";
            break;
        case Entity_Event_Unvisualize :
            s += "unvisualize";
            break;
        case Entity_Event_Audialize :
            s += "audialize";
            break;
        case Entity_Event_Unaudialize :
            s += "unaudialize";
            break;
        case Entity_Event_Field_Of_View :
            s += "field-of-view";
            break;
        case Entity_Event_Lock_Mouse :
            s += "lock-mouse";
            break;
        case Entity_Event_Unlock_Mouse :
            s += "unlock-mouse";
            break;
        case Entity_Event_Voice_Volume :
            s += "voice-volume";
            break;
        case Entity_Event_Tint_Color :
            s += "tint-color";
            break;
        case Entity_Event_Tint_Strength :
            s += "tint-strength";
            break;
        }
        break;
    case Network_Event_Entity_Body :
        s += "entity-body/";
        switch (payload<EntityBodyEvent>().type)
        {
        case Entity_Body_Event_Next_Part_ID :
            s += "next-part-id";
            break;
        case Entity_Body_Event_Add_Part :
            s += "add-part";
            break;
        case Entity_Body_Event_Remove_Part :
            s += "remove-part";
            break;
        case Entity_Body_Event_Update_Part :
            s += "update-part";
            break;
        }
        if (payload<EntityBodyEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_Sample :
        s += "entity-sample/";
        switch (payload<EntitySampleEvent>().type)
        {
        case Entity_Sample_Event_Add :
            s += "add";
            break;
        case Entity_Sample_Event_Remove :
            s += "remove";
            break;
        case Entity_Sample_Event_Next_Part_ID :
            s += "next-part-id";
            break;
        case Entity_Sample_Event_Add_Part :
            s += "add-part";
            break;
        case Entity_Sample_Event_Remove_Part :
            s += "remove-part";
            break;
        case Entity_Sample_Event_Update_Part :
            s += "update-part";
            break;
        }
        if (payload<EntitySampleEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_Sample_Playback :
        s += "entity-sample-playback/";
        switch (payload<EntitySamplePlaybackEvent>().type)
        {
        case Entity_Sample_Playback_Event_Add :
            s += "add";
            break;
        case Entity_Sample_Playback_Event_Remove :
            s += "remove";
            break;
        case Entity_Sample_Playback_Event_Resume :
            s += "resume";
            break;
        case Entity_Sample_Playback_Event_Pause :
            s += "pause";
            break;
        case Entity_Sample_Playback_Event_Seek :
            s += "seek";
            break;
        }
        if (payload<EntitySamplePlaybackEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_Action :
        s += "entity-action/";
        switch (payload<EntityActionEvent>().type)
        {
        case Entity_Action_Event_Add :
            s += "add";
            break;
        case Entity_Action_Event_Remove :
            s += "remove";
            break;
        case Entity_Action_Event_Next_Part_ID :
            s += "next-part-id";
            break;
        case Entity_Action_Event_Add_Part :
            s += "add-part";
            break;
        case Entity_Action_Event_Remove_Part :
            s += "remove-part";
            break;
        case Entity_Action_Event_Update_Part :
            s += "update-part";
            break;
        }
        if (payload<EntityActionEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_Action_Playback :
        s += "entity-action-playback/";
        switch (payload<EntityActionPlaybackEvent>().type)
        {
        case Entity_Action_Playback_Event_Add :
            s += "add";
            break;
        case Entity_Action_Playback_Event_Remove :
            s += "remove";
            break;
        case Entity_Action_Playback_Event_Resume :
            s += "resume";
            break;
        case Entity_Action_Playback_Event_Pause :
            s += "pause";
            break;
        case Entity_Action_Playback_Event_Seek :
            s += "seek";
            break;
        }
        if (payload<EntityActionPlaybackEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_Input_Binding :
        s += "entity-input-binding/";
        switch (payload<EntityInputBindingEvent>().type)
        {
        case Entity_Input_Binding_Event_Add :
            s += "add";
            break;
        case Entity_Input_Binding_Event_Remove :
            s += "remove";
            break;
        case Entity_Input_Binding_Event_Update :
            s += "update";
            break;
        }
        if (payload<EntityInputBindingEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_HUD :
        s += "entity-hud/";
        switch (payload<EntityHUDEvent>().type)
        {
        case Entity_HUD_Event_Add :
            s += "add";
            break;
        case Entity_HUD_Event_Remove :
            s += "remove";
            break;
        case Entity_HUD_Event_Update :
            s += "update";
            break;
        }
        if (payload<EntityHUDEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Entity_Script :
        s += "entity-script/";
        switch (payload<EntityScriptEvent>().type)
        {
        case Entity_Script_Event_Add :
            s += "add";
            break;
        case Entity_Script_Event_Remove :
            s += "remove";
            break;
        case Entity_Script_Event_Update :
            s += "update";
            break;
        }
        if (payload<EntityScriptEvent>().transformed)
        {
            s += " (transformed)";
        }
        break;
    case Network_Event_Script :
        s += "script/";
        switch (payload<ScriptEvent>().type)
        {
        case Script_Event_Set :
            s += "set";
            break;
        case Script_Event_Clear :
            s += "clear";
            break;
        }
        break;
    case Network_Event_Attach :
        s += "attach";
        break;
    case Network_Event_Detach :
        s += "detach";
        break;
    case Network_Event_Move :
        s += "move";
        break;
    case Network_Event_Haptic :
        s += "haptic";
        break;
    case Network_Event_Input :
        s += "input";
        break;
    case Network_Event_Grip :
        s += "grip";
        break;
    case Network_Event_Command :
        s += "command/";
        switch (payload<CommandEvent>().type)
        {
        case Command_Event_Command :
            s += "command";
            break;
        case Command_Event_Chat :
            s += "chat";
            break;
        case Command_Event_Direct_Chat :
            s += "direct-chat";
            break;
        case Command_Event_Team_Chat :
            s += "team-chat";
            break;
        }
        break;
    case Network_Event_Pose :
        s += "pose";
        break;
    case Network_Event_Log :
        s += "log";
        break;
    case Network_Event_History :
        s += "history";
        break;
    case Network_Event_Text_Request :
        s += "text-request";
        break;
    case Network_Event_Text_Response :
        s += "text-response";
        break;
    case Network_Event_Tether :
        s += "tether/";
        switch (payload<TetherEvent>().type)
        {
        case Tether_Event_Tether :
            s += "tether";
            break;
        case Tether_Event_Untether :
            s += "untether";
            break;
        }
        break;
    case Network_Event_Confinement :
        s += "confinement/";
        switch (payload<ConfinementEvent>().type)
        {
        case Confinement_Event_Confine :
            s += "confine";
            break;
        case Confinement_Event_Unconfine :
            s += "unconfine";
            break;
        }
        break;
    case Network_Event_Goto :
        s += "goto";
        break;
    case Network_Event_Fade :
        s += "fade/";
        switch (payload<FadeEvent>().type)
        {
        case Fade_Event_Fade :
            s += "fade";
            break;
        case Fade_Event_Unfade :
            s += "unfade";
            break;
        }
        break;
    case Network_Event_Wait :
        s += "wait/";
        switch (payload<WaitEvent>().type)
        {
        case Wait_Event_Wait :
            s += "wait";
            break;
        case Wait_Event_Unwait :
            s += "unwait";
            break;
        }
        break;
    case Network_Event_Voice :
        s += "voice";
        break;
    case Network_Event_Voice_Channel :
        s += "voice-channel";
        break;
    case Network_Event_Step :
        s += "step";
        break;
    case Network_Event_URL :
        s += "url";
        break;
    case Network_Event_Save :
        s += "save";
        break;
    }

    return s;
}

bool NetworkEvent::operator==(const NetworkEvent& rhs) const
{
    return data() == rhs.data();
}

bool NetworkEvent::operator!=(const NetworkEvent& rhs) const
{
    return !(*this == rhs);
}

ostream& operator<<(ostream& out, const NetworkEvent& event)
{
    out << event.name();

    return out;
}

template<>
NetworkEvent::NetworkEvent(const PingEvent& payload)
{
    _type = Network_Event_Ping;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const JoinEvent& payload)
{
    _type = Network_Event_Join;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const LeaveEvent& payload)
{
    _type = Network_Event_Leave;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const WelcomeEvent& payload)
{
    _type = Network_Event_Welcome;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const KickEvent& payload)
{
    _type = Network_Event_Kick;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const UserEvent& payload)
{
    _type = Network_Event_User;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const TeamEvent& payload)
{
    _type = Network_Event_Team;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const CheckpointEvent& payload)
{
    _type = Network_Event_Checkpoint;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const DocumentEvent& payload)
{
    _type = Network_Event_Document;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const GameEvent& payload)
{
    _type = Network_Event_Game;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const WorldEvent& payload)
{
    _type = Network_Event_World;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const WorldSkyEvent& payload)
{
    _type = Network_Event_World_Sky;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const WorldAtmosphereEvent& payload)
{
    _type = Network_Event_World_Atmosphere;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityEvent& payload)
{
    _type = Network_Event_Entity;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityBodyEvent& payload)
{
    _type = Network_Event_Entity_Body;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntitySampleEvent& payload)
{
    _type = Network_Event_Entity_Sample;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntitySamplePlaybackEvent& payload)
{
    _type = Network_Event_Entity_Sample_Playback;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityActionEvent& payload)
{
    _type = Network_Event_Entity_Action;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityActionPlaybackEvent& payload)
{
    _type = Network_Event_Entity_Action_Playback;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityInputBindingEvent& payload)
{
    _type = Network_Event_Entity_Input_Binding;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityHUDEvent& payload)
{
    _type = Network_Event_Entity_HUD;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const EntityScriptEvent& payload)
{
    _type = Network_Event_Entity_Script;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const ScriptEvent& payload)
{
    _type = Network_Event_Script;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const AttachEvent& payload)
{
    _type = Network_Event_Attach;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const DetachEvent& payload)
{
    _type = Network_Event_Detach;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const MoveEvent& payload)
{
    _type = Network_Event_Move;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const HapticEvent& payload)
{
    _type = Network_Event_Haptic;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const InputEvent& payload)
{
    _type = Network_Event_Input;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const GripEvent& payload)
{
    _type = Network_Event_Grip;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const CommandEvent& payload)
{
    _type = Network_Event_Command;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const PoseEvent& payload)
{
    _type = Network_Event_Pose;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const LogEvent& payload)
{
    _type = Network_Event_Log;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const HistoryEvent& payload)
{
    _type = Network_Event_History;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const TextRequestEvent& payload)
{
    _type = Network_Event_Text_Request;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const TextResponseEvent& payload)
{
    _type = Network_Event_Text_Response;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const TetherEvent& payload)
{
    _type = Network_Event_Tether;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const ConfinementEvent& payload)
{
    _type = Network_Event_Confinement;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const GotoEvent& payload)
{
    _type = Network_Event_Goto;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const FadeEvent& payload)
{
    _type = Network_Event_Fade;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const WaitEvent& payload)
{
    _type = Network_Event_Wait;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const VoiceEvent& payload)
{
    _type = Network_Event_Voice;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const VoiceChannelEvent& payload)
{
    _type = Network_Event_Voice_Channel;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const StepEvent& payload)
{
    _type = Network_Event_Step;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const URLEvent& payload)
{
    _type = Network_Event_URL;
    _payload = payload;
}

template<>
NetworkEvent::NetworkEvent(const SaveEvent& payload)
{
    _type = Network_Event_Save;
    _payload = payload;
}
