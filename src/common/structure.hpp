/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

struct StructureType
{
    StructureType();
    StructureType(StructureTypeID id);

    void verify();

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            thickness
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        thickness = get<1>(payload);
    }

    bool operator==(const StructureType& rhs) const;
    bool operator!=(const StructureType& rhs) const;

    typedef msgpack::type::tuple<StructureTypeID, f64> layout;

    StructureTypeID id;
    f64 thickness;
};

enum StructureNodeType : u8
{
    Structure_Node_Block,
    Structure_Node_Rectangle,
    Structure_Node_Circle,
    Structure_Node_Line,
    Structure_Node_Wall
};

StructureNodeType structureNodeTypeFromString(const string& name);
string structureNodeTypeToString(StructureNodeType type);

struct StructureNode
{
    StructureNode();
    StructureNode(StructureNodeID id);

    void verify(const set<StructureTypeID>& typeIDs);

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            typeID,
            type,
            faceIndex,
            positive,
            inverted,
            positionA,
            positionB,
            radius,
            length,
            children
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        typeID = get<1>(payload);
        type = static_cast<StructureNodeType>(get<2>(payload));
        faceIndex = get<3>(payload);
        positive = get<4>(payload);
        inverted = get<5>(payload);
        positionA = get<6>(payload);
        positionB = get<7>(payload);
        radius = get<8>(payload);
        length = get<9>(payload);
        children = get<10>(payload);
    }

    bool operator==(const StructureNode& rhs) const;
    bool operator!=(const StructureNode& rhs) const;

    StructureNode* getNodeByIndex(size_t* currentIndex, size_t targetIndex);
    const StructureNode* getNodeByIndex(size_t* currentIndex, size_t targetIndex) const;

    typedef msgpack::type::tuple<StructureNodeID, StructureTypeID, u8, u32, bool, bool, vec2, vec2, f64, f64, vector<StructureNode>> layout;

    StructureNodeID id;
    StructureTypeID typeID;
    StructureNodeType type;
    u32 faceIndex;
    bool positive;
    bool inverted;
    vec2 positionA;
    vec2 positionB;
    f64 radius;
    f64 length;
    vector<StructureNode> children;
};
