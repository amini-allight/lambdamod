/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

inline string messageBoundary(const string& msg)
{
    u64 size = msg.size();

    return string(reinterpret_cast<const char*>(&size), sizeof(u64)) + msg;
}

inline vector<string> messageUnboundary(const string& data, u64& nextMessageSize, string& buffer)
{
    buffer += data;

    if (nextMessageSize == 0)
    {
        if (buffer.size() >= sizeof(u64))
        {
            nextMessageSize = *reinterpret_cast<u64*>(buffer.data());
            buffer = buffer.substr(sizeof(u64), buffer.size() - sizeof(u64));
        }
        else
        {
            return {};
        }
    }

    vector<string> messages;

    while (nextMessageSize != 0 && buffer.size() >= nextMessageSize)
    {
        messages.push_back(buffer.substr(0, nextMessageSize));
        buffer = buffer.substr(nextMessageSize, buffer.size() - nextMessageSize);

        if (buffer.size() >= sizeof(u64))
        {
            nextMessageSize = *reinterpret_cast<u64*>(buffer.data());
            buffer = buffer.substr(sizeof(u64), buffer.size() - sizeof(u64));
        }
        else
        {
            nextMessageSize = 0;
        }
    }

    return messages;
}
