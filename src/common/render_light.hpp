#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "body_render_data.hpp"

#pragma pack(1)
struct LightGPU
{
    fmat4 transform[eyeCount];
    fvec4 color;
    f32 angle;
    f32 pad[3];
};
#pragma pack()

class RenderLight
{
public:
    RenderLight(const mat4& entityTransform, const BodyRenderLight& light);

    void update(const mat4& entityTransform);

    bool needed(const vec3& position) const;
    f64 maxDistance() const;
    LightGPU uniformData(const vec3& cameraPosition) const;
    LightGPU uniformData(vec3 cameraPosition[eyeCount]) const;

private:
    mat4 entityTransform;
    mat4 transform;
    f64 intensity;
    LightGPU _uniformData;
};
