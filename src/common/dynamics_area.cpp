/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_area.hpp"
#include "dynamics_component.hpp"
#include "world.hpp"

DynamicsArea::DynamicsArea(const BodyPart* part)
    : transform(part->regionalTransform())
    , area(part->get<BodyPartArea>())
{

}

DynamicsArea DynamicsArea::toGlobal(DynamicsComponent* self) const
{
    DynamicsArea ret = *this;

    ret.transform = self->globalTransform().applyToTransform(ret.transform);

    return ret;
}

optional<f64> DynamicsArea::immersion(const vec3& position) const
{
    vec3 localPosition = transform.applyToPosition(position, false);

    return area.immersion(localPosition);
}

optional<vec3> DynamicsArea::gravity() const
{
    return area.gravity ? transform.applyToVector(*area.gravity) : optional<vec3>();
}

optional<vec3> DynamicsArea::flowVelocity() const
{
    return area.flowVelocity ? transform.applyToVector(*area.flowVelocity) : optional<vec3>();
}

optional<f64> DynamicsArea::density() const
{
    return area.density;
}

tuple<vec3, vec3, f64> getEnvironment(const World* world, const vector<DynamicsArea>& areas, const vec3& position)
{
    optional<vec3> gravity;
    f64 closestGravity = numeric_limits<f64>::max();
    optional<vec3> flowVelocity;
    f64 closestFlowVelocity = numeric_limits<f64>::max();
    optional<f64> density;
    f64 closestDensity = numeric_limits<f64>::max();

    for (const DynamicsArea& area : areas)
    {
        optional<f64> oImmersion = area.immersion(position);

        if (!oImmersion)
        {
            continue;
        }

        optional<vec3> areaGravity = area.gravity();
        optional<vec3> areaFlowVelocity = area.flowVelocity();
        optional<f64> areaDensity = area.density();

        f64 immersion = *oImmersion;

        if (immersion < closestGravity && areaGravity)
        {
            gravity = areaGravity;
            closestGravity = immersion;
        }

        if (immersion < closestFlowVelocity && areaFlowVelocity)
        {
            flowVelocity = areaFlowVelocity;
            closestFlowVelocity = immersion;
        }

        if (immersion < closestDensity && areaDensity)
        {
            density = areaDensity;
            closestDensity = immersion;
        }
    }

    if (!gravity)
    {
        gravity = world->gravity();
    }

    if (!flowVelocity)
    {
        flowVelocity = world->flowVelocity();
    }

    if (!density)
    {
        density = world->density();
    }

    return { *gravity, *flowVelocity, *density };
}
