/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"

static constexpr u32 soundFrequency = 48000;
static constexpr u32 soundChannels = earCount;
static constexpr u32 soundChunkSamplesPerChannel = 2048;
static constexpr u32 soundChunkOverlap = 256;
static constexpr chrono::microseconds soundChunkInterval(static_cast<u64>((soundChunkSamplesPerChannel / static_cast<f64>(soundFrequency)) * 1'000'000));

static constexpr u32 voiceFrequency = 48000;
static constexpr u32 voiceChannels = 1;
static constexpr u32 voiceBitrate = 64000;
static constexpr u32 voiceChunkSamplesPerChannel = 480;
static constexpr chrono::microseconds voiceChunkInterval(static_cast<u64>((voiceChunkSamplesPerChannel / static_cast<f64>(voiceFrequency)) * 1'000'000));

static constexpr chrono::microseconds soundAutoExposureDelay(5'000'000);
static constexpr f32 soundAutoExposureMinAmplitude = 0.0000000001;
static constexpr f32 soundAutoExposureMaxAmplitude = 100;

static constexpr u32 soundBufferSize = 1024 * 1024;

static constexpr f64 soundOcclusionMinThickness = 0.01;

static constexpr f64 soundOcclusionAngularDiameterCoefficient = pi / 2;
static constexpr f64 soundTransitionBandStartFrequency = 200;
static constexpr f64 soundTransitionBandEndFrequency = 5000;
static constexpr f32 soundFalloffFactor = defaultSpeedOfSound / 2;

static constexpr f64 maxSoundDistance = 500'000; // Prevent the creation of arbitrarily long sound delay buffers
