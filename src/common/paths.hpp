/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

#define STR(x) #x
#define XSTR(x) STR(x)

inline string resourcePath()
{
    return fs::exists("res/lambdamod-resource-marker")
        ? "res"
        : XSTR(LMOD_INSTALL_PREFIX) + string("/share/lambdamod/res");
}

#undef STR
#undef XSTR

static const string fontPath = resourcePath() + "/fonts";
static const string iconPath = resourcePath() + "/icons";
static const string shaderPath = resourcePath() + "/shaders";
static const string texturePath = resourcePath() + "/textures";
static const string soundPath = resourcePath() + "/sounds";
static const string meshPath = resourcePath() + "/meshes";
static const string scriptPath = resourcePath() + "/scripts";
static const string oraclePath = resourcePath() + "/oracles";
static const string symbolPath = resourcePath() + "/symbols";

static constexpr const char* const fontExt = ".ttf";
static constexpr const char* const iconExt = ".png";
static constexpr const char* const shaderExt = ".spv";
static constexpr const char* const textureExt = ".png";
static constexpr const char* const soundExt = ".wav";
static constexpr const char* const meshExt = ".geo";
static constexpr const char* const symbolExt = ".svg";
static constexpr const char* const backupExt = ".bak";

static constexpr const char* const clientConfigFileName = "client.yml";
static constexpr const char* const serverConfigFileName = "server.yml";
static constexpr const char* const connectionFileName = "client-connection.yml";
static constexpr const char* const themeFileName = "client-theme.yml";
static constexpr const char* const localizationFileName = "client-localization.yml";
static constexpr const char* const bindingsFileName = "client-bindings.yml";

static constexpr const char* const pipelineCacheFileName = "pipelines.cache";

static constexpr const char* const hrirPath = "/hrir.dat";
