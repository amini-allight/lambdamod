/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "vr_device.hpp"
#include "log.hpp"
#include "yaml_tools.hpp"

string vrDeviceToString(VRDevice device)
{
    switch (device)
    {
    case VR_Device_Head :
        return "vr-device-head";
    case VR_Device_Left_Hand :
        return "vr-device-left-hand";
    case VR_Device_Right_Hand :
        return "vr-device-right-hand";
    case VR_Device_Hips :
        return "vr-device-hips";
    case VR_Device_Left_Foot :
        return "vr-device-left-foot";
    case VR_Device_Right_Foot :
        return "vr-device-right-foot";
    case VR_Device_Chest :
        return "vr-device-chest";
    case VR_Device_Left_Shoulder :
        return "vr-device-left-shoulder";
    case VR_Device_Right_Shoulder :
        return "vr-device-right-shoulder";
    case VR_Device_Left_Elbow :
        return "vr-device-left-elbow";
    case VR_Device_Right_Elbow :
        return "vr-device-right-elbow";
    case VR_Device_Left_Wrist :
        return "vr-device-left-wrist";
    case VR_Device_Right_Wrist :
        return "vr-device-right-wrist";
    case VR_Device_Left_Knee :
        return "vr-device-left-knee";
    case VR_Device_Right_Knee :
        return "vr-device-right-knee";
    case VR_Device_Left_Ankle :
        return "vr-device-left-ankle";
    case VR_Device_Right_Ankle :
        return "vr-device-right-ankle";
    default :
        fatal("Encountered unknown VR device '" + to_string(device) + "'.");
    }
}

VRDevice vrDeviceFromString(const string& s)
{
    if (s == "vr-device-head")
    {
        return VR_Device_Head;
    }
    else if (s == "vr-device-left-hand")
    {
        return VR_Device_Left_Hand;
    }
    else if (s == "vr-device-right-hand")
    {
        return VR_Device_Right_Hand;
    }
    else if (s == "vr-device-hips")
    {
        return VR_Device_Hips;
    }
    else if (s == "vr-device-left-foot")
    {
        return VR_Device_Left_Foot;
    }
    else if (s == "vr-device-right-foot")
    {
        return VR_Device_Right_Foot;
    }
    else if (s == "vr-device-chest")
    {
        return VR_Device_Chest;
    }
    else if (s == "vr-device-left-shoulder")
    {
        return VR_Device_Left_Shoulder;
    }
    else if (s == "vr-device-right-shoulder")
    {
        return VR_Device_Right_Shoulder;
    }
    else if (s == "vr-device-left-elbow")
    {
        return VR_Device_Left_Elbow;
    }
    else if (s == "vr-device-right-elbow")
    {
        return VR_Device_Right_Elbow;
    }
    else if (s == "vr-device-left-wrist")
    {
        return VR_Device_Left_Wrist;
    }
    else if (s == "vr-device-right-wrist")
    {
        return VR_Device_Right_Wrist;
    }
    else if (s == "vr-device-left-knee")
    {
        return VR_Device_Left_Knee;
    }
    else if (s == "vr-device-right-knee")
    {
        return VR_Device_Right_Knee;
    }
    else if (s == "vr-device-left-ankle")
    {
        return VR_Device_Left_Ankle;
    }
    else if (s == "vr-device-right-ankle")
    {
        return VR_Device_Right_Ankle;
    }
    else
    {
        fatal("Encountered unknown VR device '" + s + "'.");
    }
}

template<>
VRDevice YAMLNode::convert() const
{
    return vrDeviceFromString(_scalar);
}

template<>
void YAMLSerializer::emit(VRDevice v)
{
    emit(vrDeviceToString(v));
}
