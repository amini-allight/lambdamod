/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script.hpp"

Value symbol(const string& s);

Value symbolListValue(const vector<string>& l);
Value symbolListValue(const set<string>& l);

template<typename T>
Value toValue(const T& v);

template<>
Value toValue(const Value& v);

template<>
Value toValue(const Lambda& lambda);

template<>
Value toValue(const bool& b);

template<>
Value toValue(const string& s);

template<>
Value toValue(const f64& f);

template<>
Value toValue(const i32& i);

template<>
Value toValue(const u32& u);

template<>
Value toValue(const i64& i);

template<>
Value toValue(const u64& u);

template<>
Value toValue(const vec2& v);

template<>
Value toValue(const vec3& v);

template<>
Value toValue(const quaternion& q);

template<typename T, int N>
Value toValue(const IDType<T, N>& id);

Value toValue(const char* s);

template<typename T>
Value toValue(const set<T>& l)
{
    Value v;
    v.type = Value_List;

    for (const T& i : l)
    {
        v.l.push_back(toValue(i));
    }

    return v;
}

template<typename T>
Value toValue(const vector<T>& l)
{
    Value v;
    v.type = Value_List;

    for (const T& i : l)
    {
        v.l.push_back(toValue(i));
    }

    return v;
}

template<typename KeyT, typename ValT>
Value toValue(const map<KeyT, ValT>& m)
{
    Value result;
    result.type = Value_List;

    for (const auto& [ k, v ] : m)
    {
        result.l.push_back(composeList(toValue(k), toValue(v)));
    }

    return result;
}

template<typename T>
Value toValue(const optional<T>& o)
{
    if (o)
    {
        return toValue(*o);
    }
    else
    {
        return Value();
    }
}

template<typename T>
Value quote(const T& x)
{
    return quote(toValue(x));
}

template<>
Value quote(const Value& value);

template<typename T>
T fromValue(const Value& value);

template<>
vec2 fromValue(const Value& value);

template<>
vec3 fromValue(const Value& value);

template<>
quaternion fromValue(const Value& value);

template<typename T, int N>
IDType<T, N> fromValue(const Value& value)
{
    return IDType<T, N>(value.i);
}

bool isVec2(const Value& value);
bool isVec3(const Value& value);
bool isVec4(const Value& value);
bool isQuaternion(const Value& value);

bool isListOf(const Value& value, ValueType type);
bool isTableOf(const Value& value, ValueType keyType, ValueType valueType);

bool isTable(const Value& value);

template<typename T>
Value composeList(const T& i)
{
    Value v;
    v.type = Value_List;
    v.l = { toValue(i) };

    return v;
}

template<typename T, typename... Args>
Value composeList(const T& first, Args... args)
{
    Value v;
    v.type = Value_List;
    v.l = { toValue(first) };

    Value rest = composeList(args...);

    v.l.insert(
        v.l.end(),
        rest.l.begin(),
        rest.l.end()
    );

    return v;
}

Value listValue();

optional<vector<Value>> runSafely(ScriptContext* context, const string& s, const string& action, User* user);
optional<vector<Value>> runSafely(ScriptContext* context, const vector<Value>& values, const string& action, User* user);
optional<Value> runSafely(ScriptContext* context, const Lambda& lambda, const vector<Value>& args, const string& action, User* user);

optional<vector<Value>> runSafely(ScriptContext* context, const string& s, const string& action);
optional<vector<Value>> runSafely(ScriptContext* context, const vector<Value>& values, const string& action);
optional<Value> runSafely(ScriptContext* context, const Lambda& lambda, const vector<Value>& args, const string& action);

#if defined(LMOD_CLIENT)
class Controller;

optional<vector<Value>> runSafely(ScriptContext* context, const string& s, const string& action, Controller* controller);
optional<vector<Value>> runSafely(ScriptContext* context, const vector<Value>& values, const string& action, Controller* controller);
optional<Value> runSafely(ScriptContext* context, const Lambda& lambda, const vector<Value>& args, const string& action, Controller* controller);
#endif

chrono::milliseconds maxLoopDuration();
