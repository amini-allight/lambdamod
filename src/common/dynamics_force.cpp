/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_force.hpp"
#include "dynamics_component.hpp"
#include "tools.hpp"
#include "entity.hpp"

DynamicsForce::DynamicsForce(const BodyPart* part)
    : _regionalTransform(part->regionalTransform())
    , _force(part->get<BodyPartForce>())
{

}

const mat4& DynamicsForce::regionalTransform() const
{
    return _regionalTransform;
}

const BodyPartForce& DynamicsForce::force() const
{
    return _force;
}

optional<vec3> DynamicsForce::reactionPosition() const
{
    return _force.reaction ? _regionalTransform.position() : optional<vec3>();
}

vec3 DynamicsForce::forceOnComponent(const DynamicsComponent* self, const DynamicsComponent* other, f64 mediumDensity) const
{
    mat4 selfTransform = self->globalTransform().applyToTransform(_regionalTransform);
    mat4 otherTransform = other->globalTransform();

    vec3 forceDirection = selfTransform.forward();
    quaternion forceSpace(pickForwardDirection(forceDirection), forceDirection);
    vec3 relativeOtherPosition = forceSpace.inverse().rotate(selfTransform.applyToPosition(otherTransform.position(), false));

    vec3 forceVector;

    switch (_force.type)
    {
    case Body_Force_Linear :
        if (relativeOtherPosition.toVec2().length() > _force.radius)
        {
            return vec3();
        }

        forceVector = forceDirection * _force.intensity;
        forceVector /= sq(1 + abs(relativeOtherPosition.z));
        break;
    case Body_Force_Omnidirectional :
        if (relativeOtherPosition.length() < _force.radius)
        {
            return vec3();
        }

        forceDirection = (otherTransform.position() - selfTransform.position()).normalize();

        forceVector = forceDirection * _force.intensity;
        forceVector /= sq(1 + max(relativeOtherPosition.length() - _force.radius, 0.0));
        break;
    case Body_Force_Vortex :
    {
        forceDirection = forceSpace.up().cross((otherTransform.position() - selfTransform.position()).normalize()).normalize();

        f64 flowSpeed = _force.intensity / mediumDensity;
        f64 drag = self->entity->drag();
        f64 crossSectionalArea = self->entity->linearCrossSectionalArea(forceDirection);

        forceVector = forceDirection * 0.5 * mediumDensity * sq(flowSpeed) * drag * crossSectionalArea;
        forceVector /= sq(1 + relativeOtherPosition.toVec2().length());
        break;
    }
    case Body_Force_Fan :
    {
        if (relativeOtherPosition.toVec2().length() > _force.radius)
        {
            return vec3();
        }

        f64 thrust = cbrt(_force.intensity * pi * (sq(_force.radius * 2) / 2) * mediumDensity);
        f64 flowSpeed = sqrt(thrust / (0.5 * mediumDensity * pi * sq(_force.radius)));
        f64 drag = self->entity->drag();
        f64 crossSectionalArea = self->entity->linearCrossSectionalArea(forceDirection);

        forceVector = forceDirection * 0.5 * mediumDensity * sq(flowSpeed) * drag * crossSectionalArea;
        forceVector /= sq(1 + abs(relativeOtherPosition.z));
        break;
    }
    case Body_Force_Rocket :
    {
        if (relativeOtherPosition.toVec2().length() > _force.radius || relativeOtherPosition.z < 0)
        {
            return vec3();
        }

        f64 flowSpeed = _force.intensity / _force.massFlowRate;
        f64 flowVolume = flowSpeed * pi * sq(_force.radius);
        f64 density = _force.massFlowRate / flowVolume;
        f64 drag = self->entity->drag();
        f64 crossSectionalArea = self->entity->linearCrossSectionalArea(forceDirection);

        forceVector = forceDirection * 0.5 * density * sq(flowSpeed) * drag * crossSectionalArea;
        forceVector /= sq(1 + abs(relativeOtherPosition.z));
        break;
    }
    case Body_Force_Gravity :
        if (relativeOtherPosition.length() < _force.radius)
        {
            return vec3();
        }

        forceDirection = (otherTransform.position() - selfTransform.position()).normalize();

        forceVector = forceDirection * other->mass() * -_force.intensity;
        forceVector /= sq(1 + max(relativeOtherPosition.length() - _force.radius, 0.0));
        break;
    case Body_Force_Magnetism :
    {
        vec3 direction = (otherTransform.position() - selfTransform.position()).normalize();
        f64 distance = (otherTransform.position() - selfTransform.position()).length();

        vec3 moment = forceDirection * -_force.intensity * other->entity->magnetism();

        forceVector = (3 * moment.dot(direction) * direction - moment) / pow(distance, 3);
        break;
    }
    }

    return forceVector;
}
