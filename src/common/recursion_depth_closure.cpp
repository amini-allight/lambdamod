/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "recursion_depth_closure.hpp"

#if defined(LMOD_SERVER)
#include "server/global.hpp"
#elif defined(LMOD_CLIENT)
#include "client/global.hpp"
#endif

RecursionDepthClosure::RecursionDepthClosure(ScriptContext* context, const string& name)
    : context(context)
{
    context->recursionDepth++;

    if (context->recursionDepth > g_config.maxRecursionDepth)
    {
        throw runtime_error("Maximum recursion depth exceeded while executing '" + name + "'.");
    }
}

RecursionDepthClosure::~RecursionDepthClosure()
{
    context->recursionDepth--;
}
