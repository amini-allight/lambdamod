/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum DisconnectionReason : u32
{
    Disconnection_Reason_Incorrect_Password,
    Disconnection_Reason_User_Already_Logged_In,
    Disconnection_Reason_Kicked,
    Disconnection_Reason_Invalid_Username,
    Disconnection_Reason_Client_Too_New,
    Disconnection_Reason_Client_Too_Old,
    Disconnection_Reason_Maximum_Latency_Exceeded
};
