/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_part.hpp"

class World;
class DynamicsComponent;

class DynamicsArea
{
public:
    DynamicsArea(const BodyPart* part);

    DynamicsArea toGlobal(DynamicsComponent* self) const;
    optional<f64> immersion(const vec3& position) const;

    optional<vec3> gravity() const;
    optional<vec3> flowVelocity() const;
    optional<f64> density() const;

private:
    mat4 transform;
    BodyPartArea area;
};

tuple<vec3, vec3, f64> getEnvironment(const World* world, const vector<DynamicsArea>& areas, const vec3& position);
