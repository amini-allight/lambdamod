/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "pass_filters.hpp"
#include "tools.hpp"
#include "sound_constants.hpp"
#include "convolution.hpp"

#define ANGFREQ(x) toNormalizedAngularFrequency(x, soundFrequency)

static constexpr u32 hammingWindowSize(f32 passFrequency, f32 stopFrequency)
{
    return (8 * pi) / ANGFREQ(abs(passFrequency - stopFrequency));
}

static constexpr f32 hammingWindow(u32 n, f32 passFrequency, f32 stopFrequency)
{
    u32 m = hammingWindowSize(passFrequency, stopFrequency);

    return 0.54 - 0.46 * cos((2 * n * pi) / m);
}

static constexpr f32 loPassFilter(u32 n, f32 passFrequency, f32 stopFrequency)
{
    u32 m = hammingWindowSize(passFrequency, stopFrequency);
    f32 transitionCenterFrequency = ANGFREQ((passFrequency + stopFrequency) / 2);

    return (transitionCenterFrequency / pi) * sinc((transitionCenterFrequency * (n - static_cast<f32>(m / 2))) / pi);
}

static constexpr f32 hiPassFilter(u32 n, f32 passFrequency, f32 stopFrequency)
{
    u32 m = hammingWindowSize(passFrequency, stopFrequency);
    f32 transitionCenterFrequency = ANGFREQ((passFrequency + stopFrequency) / 2);

    return sinc(n - static_cast<f32>(m / 2)) - (transitionCenterFrequency / pi) * sinc((transitionCenterFrequency * (n - static_cast<f32>(m / 2))) / pi);
}

vector<f32> loPassFilter(vector<f32> samples, f32 passFrequency, f32 stopFrequency)
{
    if (roughly(passFrequency - stopFrequency, 0.0f))
    {
        fatal("Pass and stop frequencies passed to lo-pass filter must not be the same: " + to_string(passFrequency) + " == " + to_string(stopFrequency) + ".");
    }

    u32 m = hammingWindowSize(passFrequency, stopFrequency);

    vector<f32> coefficients(m);

    for (u32 n = 0; n < m; n++)
    {
        coefficients[n] = hammingWindow(n, passFrequency, stopFrequency) * loPassFilter(n, passFrequency, stopFrequency);
    }

    return convolve(samples, coefficients);
}

vector<f32> hiPassFilter(vector<f32> samples, f32 passFrequency, f32 stopFrequency)
{
    if (roughly(passFrequency - stopFrequency, 0.0f))
    {
        fatal("Pass and stop frequencies passed to hi-pass filter must not be the same: " + to_string(passFrequency) + " == " + to_string(stopFrequency) + ".");
    }

    u32 m = hammingWindowSize(passFrequency, stopFrequency);

    vector<f32> coefficients(m);

    for (u32 n = 0; n < m; n++)
    {
        coefficients[n] = hammingWindow(n, passFrequency, stopFrequency) * hiPassFilter(n, passFrequency, stopFrequency);
    }

    return convolve(samples, coefficients);
}
