/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

class tribool
{
public:
    tribool()
        : v(-1)
    {

    }

    tribool(bool b)
        : v(b)
    {

    }

    bool undefined() const
    {
        return v == -1;
    }

    bool defined() const
    {
        return v != -1;
    }

    bool truthy() const
    {
        return v == true;
    }

    bool falsey() const
    {
        return v == false;
    }

    tribool operator~() const
    {
        return !*this;
    }

    tribool operator&(tribool rhs) const
    {
        return *this && rhs;
    }

    tribool operator|(tribool rhs) const
    {
        return *this || rhs;
    }

    tribool operator^(tribool rhs) const
    {
        if (undefined() && rhs.undefined())
        {
            return tribool();
        }
        else if (!undefined() && rhs.undefined())
        {
            return tribool();
        }
        else if (undefined() && !rhs.undefined())
        {
            return tribool();
        }
        else
        {
            return v ^ rhs.v;
        }
    }

    tribool operator!() const
    {
        if (undefined())
        {
            return tribool();
        }
        else
        {
            return !v;
        }
    }

    tribool operator&&(tribool rhs) const
    {
        if (undefined() && rhs.undefined())
        {
            return tribool();
        }
        else if (!undefined() && rhs.undefined())
        {
            if (v)
            {
                return tribool();
            }
            else
            {
                return false;
            }
        }
        else if (undefined() && !rhs.undefined())
        {
            if (rhs.v)
            {
                return tribool();
            }
            else
            {
                return false;
            }
        }
        else
        {
            return v && rhs.v;
        }
    }

    tribool operator||(tribool rhs) const
    {
        if (undefined() && rhs.undefined())
        {
            return tribool();
        }
        else if (!undefined() && rhs.undefined())
        {
            if (v)
            {
                return true;
            }
            else
            {
                return tribool();
            }
        }
        else if (undefined() && !rhs.undefined())
        {
            if (rhs.v)
            {
                return true;
            }
            else
            {
                return tribool();
            }
        }
        else
        {
            return v || rhs.v;
        }
    }

    tribool operator==(tribool rhs) const
    {
        if (undefined() || rhs.undefined())
        {
            return tribool();
        }

        return v == rhs.v;
    }

    tribool operator!=(tribool rhs) const
    {
        if (undefined() || rhs.undefined())
        {
            return tribool();
        }

        return v != rhs.v;
    }

private:
    i8 v;
};
