/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_context.hpp"
#include "tools.hpp"
#include "paths.hpp"
#include "user.hpp"
#include "script.hpp"
#include "script_tools.hpp"
#include "script_keywords.hpp"
#include "script_stdlib.hpp"
#include "script_gamelib.hpp"
#include "script_contentlib.hpp"
#include "script_constants.hpp"

#if defined(LMOD_SERVER)
#include "server/global.hpp"
#elif defined(LMOD_CLIENT)
#include "client/global.hpp"
#endif

ScriptContext::ScriptContext()
{
    game = nullptr;

    builtins.push_back({});
    scopes.push_back({});

    builtins.back() = join(join(join(contentlib, gamelib), stdlib), keywords);

    for (const auto& [ name, func ] : builtins.back())
    {
        Value value;
        value.type = Value_Lambda;
        value.lambda.type = Lambda_Builtin;
        value.lambda.builtinName = name;

        define(name, value);
    }

    for (const auto& [ name, constant ] : constants)
    {
        define(name, constant);
    }

    builtins.push_back({});
    scopes.push_back({});

    recursionDepth = 0;
    _memorySize = 0;

    for (const fs::directory_entry& entry : fs::directory_iterator(scriptPath))
    {
        run(getFile(entry.path().string()));
    }
}

ScriptContext::ScriptContext(GameState* game)
    : ScriptContext()
{
    this->game = game;
}

ScriptContext::ScriptContext(GameState* game, const ScriptContext* rhs)
{
    // Do not invoke the base constructor here, it triggers a file read on every frame which is very slow

    this->game = game;

    copy(rhs);
}

ScriptContext::~ScriptContext()
{
    scopes.pop_back();
    builtins.pop_back();

    scopes.pop_back();
    builtins.pop_back();
}

vector<NetworkEvent> ScriptContext::compare(const ScriptContext& previous) const
{
#if defined(LMOD_SERVER)
    return {};
#else
    vector<NetworkEvent> events;

    const map<string, Value>& previousGlobalScope = previous.scopes.at(globalScope);
    const map<string, Value>& currentGlobalScope = scopes.at(globalScope);

    for (const auto& [ name, value ] : previousGlobalScope)
    {
        if (!currentGlobalScope.contains(name))
        {
            events.push_back(ScriptEvent(Script_Event_Clear, name));
        }
    }

    for (const auto& [ name, value ] : currentGlobalScope)
    {
        const ScriptValuePermissions& permissions = this->permissions.at(name);

        auto it = previousGlobalScope.find(name);

        if (it == previousGlobalScope.end() || value != it->second || permissions != previous.permissions.at(name))
        {
            events.push_back(ScriptEvent(Script_Event_Set, name, value, permissions));
        }
    }

    return events;
#endif
}

vector<NetworkEvent> ScriptContext::compareUser(const ScriptContext& previous, const User* user) const
{
    vector<NetworkEvent> events;

    const map<string, Value>& previousGlobalScope = previous.scopes.at(globalScope);
    const map<string, Value>& currentGlobalScope = scopes.at(globalScope);

    for (const auto& [ name, value ] : previousGlobalScope)
    {
        if (previous.canRead(user, name) && !currentGlobalScope.contains(name))
        {
            events.push_back(ScriptEvent(Script_Event_Clear, name));
        }
    }

    for (const auto& [ name, value ] : currentGlobalScope)
    {
        if (!canRead(user, name))
        {
            continue;
        }

        const ScriptValuePermissions& permissions = this->permissions.at(name);

        auto it = previousGlobalScope.find(name);

        if (it == previousGlobalScope.end() ||
            (!previous.canRead(user, name) && canRead(user, name)) ||
            value != it->second ||
            permissions != previous.permissions.at(name)
        )
        {
            events.push_back(ScriptEvent(Script_Event_Set, name, value, permissions));
        }
    }

    return events;
}

#if defined(LMOD_SERVER)
void ScriptContext::push(User* user, const ScriptEvent& event)
{
    switch (event.type)
    {
    case Script_Event_Set :
        if (canWrite(user, event.name))
        {
            define(event.name, event.value, event.permissions);
        }
        break;
    case Script_Event_Clear :
        if (canWrite(user, event.name))
        {
            undefine(event.name);
        }
        break;
    }
}
#elif defined(LMOD_CLIENT)
void ScriptContext::push(const ScriptEvent& event)
{
    switch (event.type)
    {
    case Script_Event_Set :
        define(event.name, event.value, event.permissions);
        break;
    case Script_Event_Clear :
        undefine(event.name);
        break;
    }
}
#endif

vector<NetworkEvent> ScriptContext::initial() const
{
    return {};
}

vector<NetworkEvent> ScriptContext::initialUser(const User* user) const
{
    vector<NetworkEvent> events;

    for (const auto& [ name, value ] : scopes.at(globalScope))
    {
        if (canRead(user, name))
        {
            events.push_back(ScriptEvent(Script_Event_Set, name, value, permissions.at(name)));
        }
    }

    return events;
}

void ScriptContext::clearUserReferences(UserID id)
{
    for (auto& [ name, permissions ] : permissions)
    {
        if (permissions.id == id)
        {
            permissions.id = UserID();
        }
    }
}

vector<Value> ScriptContext::run(const string& s)
{
    vector<Value> out;

    for (const Value& value : parse(s))
    {
        out.push_back(evaluate(this, value));
    }

    return out;
}

Value ScriptContext::run(const Value& value)
{
    return evaluate(this, value);
}

Value ScriptContext::run(const Lambda& l, const vector<Value>& args)
{
    Value result = evaluate(this, Value(join({ toValue(l) }, args)));

    return result;
}

Value ScriptContext::bind(const Value& value)
{
    return ::bind(this, value);
}

void ScriptContext::push()
{
    builtins.push_back({});
    scopes.push_back({});
}

void ScriptContext::pop()
{
    releaseMemory(memorySizeOf(scopes.back()));

    scopes.pop_back();
    builtins.pop_back();
}

void ScriptContext::define(const string& name, const Value& value, const optional<ScriptValuePermissions>& permissions)
{
    auto it = scopes.back().find(name);

    if (it != scopes.back().end())
    {
        releaseMemory(it->second.memorySize());
        it->second = value;
        acquireMemory(value.memorySize());
    }
    else
    {
        scopes.back().insert({ name, value });
        acquireMemory(sizeof(string) + name.size() + value.memorySize());
    }

    if (scopes.size() == globalScope + 1)
    {
        this->permissions.insert_or_assign(name, permissions ? *permissions : ScriptValuePermissions(currentUser()));
    }
}

void ScriptContext::undefine(const string& name)
{
    auto it = scopes.back().find(name);

    if (it == scopes.back().end())
    {
        return;
    }

    const Value& value = it->second;

    if (scopes.size() == globalScope + 1)
    {
        permissions.erase(name);
    }

    releaseMemory(sizeof(string) + name.size() + value.memorySize());
    scopes.back().erase(it);
}

void ScriptContext::restrictDefine(const string& name, bool state)
{
    permissions.at(name).restricted = state;
}

bool ScriptContext::isRestricted(const string& name) const
{
    return permissions.at(name).restricted;
}

void ScriptContext::controlDefine(const string& name, UserID id)
{
    permissions.at(name).id = id;
}

UserID ScriptContext::getOwner(const string& name) const
{
    return permissions.at(name).id;
}

bool ScriptContext::has(const string& name) const
{
    return scopes.back().contains(name);
}

bool ScriptContext::hasAnywhere(const string& name) const
{
    for (const map<string, Value>& scope : scopes)
    {
        if (scope.contains(name))
        {
            return true;
        }
    }

    return false;
}

void ScriptContext::set(const string& name, const Value& value)
{
    // Avoids modifications to the builtin base scope with i > builtinScope
    for (size_t i = scopes.size() - 1; i > builtinScope; i--)
    {
        map<string, Value>& scope = scopes[i];

        auto it = scope.find(name);

        if (it != scope.end())
        {
            releaseMemory(it->second.memorySize());

            it->second = value;

            acquireMemory(value.memorySize());
            return;
        }
    }
}

optional<Value> ScriptContext::get(const string& name) const
{
    for (size_t i = scopes.size() - 1; i < scopes.size(); i--)
    {
        auto it = scopes[i].find(name);

        if (it != scopes[i].end())
        {
            return it->second;
        }
    }

    return {};
}

optional<Value> ScriptContext::getLocal(const string& name) const
{
    // Avoids binding things in either of the two base scopes with i > globalScope
    for (size_t i = scopes.size() - 1; i > globalScope; i--)
    {
        const map<string, Value>& scope = scopes[i];

        auto it = scope.find(name);

        if (it != scope.end())
        {
            return it->second;
        }
    }

    return {};
}

void ScriptContext::addBuiltin(
    const string& name,
    const vector<string>& argNames,
    const ScriptFunction& builtin,
    ScriptBuiltinAuth auth
)
{
    builtins.back().insert_or_assign(name, ScriptBuiltin(name, argNames, builtin, auth));
}

void ScriptContext::addBuiltin(
    const string& name,
    bool variadic,
    const ScriptFunction& builtin,
    ScriptBuiltinAuth auth
)
{
    builtins.back().insert_or_assign(name, ScriptBuiltin(name, variadic, builtin, auth));
}

const ScriptBuiltin* ScriptContext::getBuiltin(const string& name) const
{
    for (size_t i = builtins.size() - 1; i < builtins.size(); i--)
    {
        auto it = builtins[i].find(name);

        if (it != builtins[i].end())
        {
            return &it->second;
        }
    }

    return nullptr;
}

User* ScriptContext::currentUser() const
{
    return !user.empty() ? user.top() : nullptr;
}

void ScriptContext::trace(const string& name)
{
    _traceback.push_back(name);
}

void ScriptContext::untrace()
{
    _traceback.pop_back();
}

void ScriptContext::clearTraceback()
{
    _traceback.clear();
}

const deque<string>& ScriptContext::traceback() const
{
    return _traceback;
}

size_t ScriptContext::memorySize() const
{
    return _memorySize;
}

bool ScriptContext::canRead(const User* user, const string& name) const
{
    if (scopes.size() - 1 != globalScope)
    {
        return true;
    }

    auto it = permissions.find(name);

    if (it == permissions.end())
    {
        return true;
    }

    const ScriptValuePermissions& permissions = it->second;

    return permissions.canRead(user);
}

bool ScriptContext::canWrite(const User* user, const string& name, bool set) const
{
    if (!set && scopes.size() - 1 != globalScope)
    {
        return true;
    }

    auto it = permissions.find(name);

    if (it == permissions.end())
    {
        return true;
    }

    const ScriptValuePermissions& permissions = it->second;

    return permissions.canWrite(user);
}

void ScriptContext::acquireMemory(size_t size)
{
    if (scopes.size() <= 1)
    {
        return;
    }

    _memorySize += size;

    if (_memorySize > g_config.maxMemoryUsage)
    {
        throw toValue("Maximum script memory usage exceeded.");
    }
}

void ScriptContext::releaseMemory(size_t size)
{
    _memorySize -= size;
}

size_t ScriptContext::memorySizeOf(const map<string, Value>& scope) const
{
    size_t size = 0;

    for (const auto& [ name, value ] : scope)
    {
        size += sizeof(string) + name.size() + value.memorySize();
    }

    return size;
}

void ScriptContext::copy(const ScriptContext* rhs)
{
    builtins = rhs->builtins;
    scopes = rhs->scopes;
    permissions = rhs->permissions;

    recursionDepth = 0;
    _memorySize = rhs->_memorySize;
}

template<>
ScriptContext* YAMLNode::convert() const
{
    ScriptContext* context = new ScriptContext();

    for (const auto& [ name, text ] : at("globals").as<map<string, string>>())
    {
        Value value = runSafely(
            context,
            text,
            "loading context value"
        )->front();

        context->define(name, value);
    }

    context->permissions = at("permissions").as<map<string, ScriptValuePermissions>>();

    return context;
}

template<>
void YAMLSerializer::emit(ScriptContext* v)
{
    startMapping();

    emit("globals");
    startMapping();

    for (const auto& [ name, value ] : v->scopes.at(globalScope))
    {
        emitPair(name, value.toString());
    }

    endMapping();

    emitPair("permissions", v->permissions);

    endMapping();
}
