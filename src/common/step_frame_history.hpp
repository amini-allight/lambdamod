/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#if defined(LMOD_SERVER)
#include "server/step_frame.hpp"
#else
#include "client/step_frame.hpp"
#endif

struct StepFrameHistoryEntry
{
    StepFrameHistoryEntry(const StepFrame& frame, const optional<chrono::milliseconds>& creationTime = {});

    bool expired() const;

    StepFrame frame;
    chrono::milliseconds creationTime;
};

class StepFrameHistory
{
public:
    StepFrameHistory();
    StepFrameHistory(const map<u64, StepFrameHistoryEntry>& entries);

    bool has(u64 index) const;
    void set(u64 index, const StepFrame& frame);
    const StepFrame& get(u64 index) const;
    void clear();
    void remove(u64 index);
    bool empty() const;
    const map<u64, StepFrameHistoryEntry>& entries() const;

private:
    map<u64, StepFrameHistoryEntry> _entries;

    void clean();
};
