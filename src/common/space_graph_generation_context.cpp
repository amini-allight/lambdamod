/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "space_graph_generation_context.hpp"
#include "tools.hpp"

static optional<vec3> intersectEdgeWithBounds(
    const vec3& linePosition,
    const vec3& lineDirection,
    f64 distance,
    const vec3& planePosition,
    const quaternion& planeRotation,
    const vec2& planeBounds
)
{
    vec3 planeNormal = planeRotation.up();

    f64 t = (planePosition - linePosition).dot(planeNormal);
    f64 b = lineDirection.dot(planeNormal);

    if (roughly(b, 0.0))
    {
        return {};
    }

    f64 d = t / b;

    if (d < 0 || d > distance)
    {
        return {};
    }

    vec3 intersection = linePosition + lineDirection * d;

    vec2 local = planeRotation.inverse().rotate(intersection - planePosition).toVec2();

    if (local.x < -(planeBounds.x / 2) || local.x >= +(planeBounds.x / 2) ||
        local.y < -(planeBounds.y / 2) || local.y >= +(planeBounds.y / 2)
    )
    {
        return {};
    }

    return intersection;
};

static bool within(const vec3& p, const vec3& min, const vec3& max)
{
    return
        p.x >= min.x &&
        p.y >= min.y &&
        p.z >= min.z &&
        p.x < max.x &&
        p.y < max.y &&
        p.z < max.z;
}

SpaceGraphGenerationEntity::SpaceGraphGenerationEntity(const Entity* entity)
    : id(entity->id())
    , transform(entity->globalTransform())
{
    tie(minExtent, maxExtent) = entity->body().boundingBox();
}

bool SpaceGraphGenerationEntity::operator==(const SpaceGraphGenerationEntity& rhs) const
{
    return id == rhs.id;
}

bool SpaceGraphGenerationEntity::operator!=(const SpaceGraphGenerationEntity& rhs) const
{
    return !(*this == rhs);
}

bool SpaceGraphGenerationEntity::operator>(const SpaceGraphGenerationEntity& rhs) const
{
    return id > rhs.id;
}

bool SpaceGraphGenerationEntity::operator<(const SpaceGraphGenerationEntity& rhs) const
{
    return id < rhs.id;
}

bool SpaceGraphGenerationEntity::operator>=(const SpaceGraphGenerationEntity& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool SpaceGraphGenerationEntity::operator<=(const SpaceGraphGenerationEntity& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering SpaceGraphGenerationEntity::operator<=>(const SpaceGraphGenerationEntity& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

pair<vec3, vec3> SpaceGraphGenerationEntity::localExtent(const vec3& nodeInner, const vec3& nodeOuter) const
{
    // Describe the containing graph node
    vec3 nodeCenter = (nodeInner + nodeOuter) / 2;
    vec3 nodeMin(
        min(nodeInner.x, nodeOuter.x),
        min(nodeInner.y, nodeOuter.y),
        min(nodeInner.z, nodeOuter.z)
    );
    vec3 nodeMax(
        max(nodeInner.x, nodeOuter.x),
        max(nodeInner.y, nodeOuter.y),
        max(nodeInner.z, nodeOuter.z)
    );

    tuple<vec3, quaternion, vec2> planes[] = {
        { vec3(nodeMin.x, nodeCenter.y, nodeCenter.z), quaternion(forwardDir, rightDir), vec2() },
        { vec3(nodeMax.x, nodeCenter.y, nodeCenter.z), quaternion(forwardDir, leftDir), vec2() },
        { vec3(nodeCenter.x, nodeMin.y, nodeCenter.z), quaternion(upDir, forwardDir), vec2() },
        { vec3(nodeCenter.x, nodeMax.y, nodeCenter.z), quaternion(upDir, backDir), vec2() },
        { vec3(nodeCenter.x, nodeCenter.y, nodeMin.z), quaternion(forwardDir, upDir), vec2() },
        { vec3(nodeCenter.x, nodeCenter.y, nodeMax.z), quaternion(forwardDir, downDir), vec2() }
    };

    // Describe the bounding box
    vec3 corners[] = {
        // back left top
        vec3(minExtent.x, minExtent.y, maxExtent.z),
        // back right top
        vec3(maxExtent.x, minExtent.y, maxExtent.z),
        // front left top
        vec3(minExtent.x, maxExtent.y, maxExtent.z),
        // front right top
        vec3(maxExtent.x, maxExtent.y, maxExtent.z),
        // back left bottom
        vec3(minExtent.x, minExtent.y, minExtent.z),
        // back right bottom
        vec3(maxExtent.x, minExtent.y, minExtent.z),
        // front left bottom
        vec3(minExtent.x, maxExtent.y, minExtent.z),
        // front right bottom
        vec3(maxExtent.x, maxExtent.y, minExtent.z)
    };

    for (vec3& corner : corners)
    {
        corner = transform.applyToPosition(corner);
    }

    pair<vec3, vec3> edges[] = {
        { corners[0], corners[1] },
        { corners[0], corners[2] },
        { corners[1], corners[3] },
        { corners[2], corners[3] },

        { corners[0], corners[4] },
        { corners[1], corners[5] },
        { corners[2], corners[6] },
        { corners[3], corners[7] },

        { corners[4], corners[5] },
        { corners[4], corners[6] },
        { corners[5], corners[7] },
        { corners[6], corners[7] },
    };

    // Clip to reduce the size of the bounding box
    vec3 localMinExtent(numeric_limits<f64>::max());
    vec3 localMaxExtent(numeric_limits<f64>::lowest());

    for (const vec3& corner : corners)
    {
        if (!within(corner, nodeMin, nodeMax))
        {
            continue;
        }

        localMinExtent.x = min(localMinExtent.x, corner.x);
        localMinExtent.y = min(localMinExtent.y, corner.y);
        localMinExtent.z = min(localMinExtent.z, corner.z);

        localMaxExtent.x = max(localMaxExtent.x, corner.x);
        localMaxExtent.y = max(localMaxExtent.y, corner.y);
        localMaxExtent.z = max(localMaxExtent.z, corner.z);
    }

    for (auto [ a, b ] : edges)
    {
        optional<vec3> adjustedA;
        optional<vec3> adjustedB;

        for (const auto& [ origin, rotation, bounds ] : planes)
        {
            if (!within(a, nodeMin, nodeMax))
            {
                optional<vec3> newA = intersectEdgeWithBounds(a, (b - a).normalize(), a.distance(b), origin, rotation, bounds);

                if (newA && (!adjustedA || newA->distance(a) < adjustedA->distance(a)))
                {
                    adjustedA = newA;
                }
            }

            if (!within(b, nodeMin, nodeMax))
            {
                optional<vec3> newB = intersectEdgeWithBounds(b, (a - b).normalize(), b.distance(a), origin, rotation, bounds);

                if (newB && (!adjustedB || newB->distance(b) < adjustedB->distance(b)))
                {
                    adjustedB = newB;
                }
            }
        }

        if (adjustedA)
        {
            a = *adjustedA;
        }

        if (adjustedB)
        {
            b = *adjustedB;
        }

        localMinExtent.x = min(localMinExtent.x, a.x);
        localMinExtent.y = min(localMinExtent.y, a.y);
        localMinExtent.z = min(localMinExtent.z, a.z);

        localMaxExtent.x = max(localMaxExtent.x, a.x);
        localMaxExtent.y = max(localMaxExtent.y, a.y);
        localMaxExtent.z = max(localMaxExtent.z, a.z);

        localMinExtent.x = min(localMinExtent.x, b.x);
        localMinExtent.y = min(localMinExtent.y, b.y);
        localMinExtent.z = min(localMinExtent.z, b.z);

        localMaxExtent.x = max(localMaxExtent.x, b.x);
        localMaxExtent.y = max(localMaxExtent.y, b.y);
        localMaxExtent.z = max(localMaxExtent.z, b.z);
    }

    return { localMinExtent, localMaxExtent };
}

bool SpaceGraphGenerationEntity::overlaps(const vec3& nodeInner, const vec3& nodeOuter) const
{
    vec3 nodeMin(
        min(nodeInner.x, nodeOuter.x),
        min(nodeInner.y, nodeOuter.y),
        min(nodeInner.z, nodeOuter.z)
    );
    vec3 nodeMax(
        max(nodeInner.x, nodeOuter.x),
        max(nodeInner.y, nodeOuter.y),
        max(nodeInner.z, nodeOuter.z)
    );

    if (within(transform.position(), nodeMin, nodeMax))
    {
        return true;
    }

    auto [ minExtent, maxExtent ] = localExtent(nodeInner, nodeOuter);

    if (
        minExtent.x >= nodeMax.x ||
        minExtent.y >= nodeMax.y ||
        minExtent.z >= nodeMax.z ||
        maxExtent.x <= nodeMin.x ||
        maxExtent.y <= nodeMin.y ||
        maxExtent.z <= nodeMin.z
    )
    {
        return false;
    }

    f64 maxProtrusion = 0;

    if (minExtent.x < nodeMax.x && minExtent.x > nodeMin.x)
    {
        maxProtrusion = max(maxProtrusion, minExtent.x - nodeMin.x);
    }

    if (minExtent.y < nodeMax.y && minExtent.y > nodeMin.y)
    {
        maxProtrusion = max(maxProtrusion, minExtent.y - nodeMin.y);
    }

    if (minExtent.z < nodeMax.z && minExtent.z > nodeMin.z)
    {
        maxProtrusion = max(maxProtrusion, minExtent.z - nodeMin.z);
    }

    if (maxExtent.x > nodeMin.x && maxExtent.x < nodeMax.x)
    {
        maxProtrusion = max(maxProtrusion, nodeMax.x - maxExtent.x);
    }

    if (maxExtent.y > nodeMin.y && maxExtent.y < nodeMax.y)
    {
        maxProtrusion = max(maxProtrusion, nodeMax.y - maxExtent.y);
    }

    if (maxExtent.z > nodeMin.z && maxExtent.z < nodeMax.z)
    {
        maxProtrusion = max(maxProtrusion, nodeMax.z - maxExtent.z);
    }

    return !roughly(maxProtrusion, 0.0);
}

SpaceGraphGenerationContext::SpaceGraphGenerationContext(
    const vector<SpaceGraphGenerationEntity>& entities,
    const vec3& innerExtent,
    const vec3& outerExtent,
    u32 depth
)
    : entities(entities)
    , innerExtent(innerExtent)
    , outerExtent(outerExtent)
    , depth(depth)
{

}

f64 pickCenter(SpaceGraphGenerationContext& ctx, const function<f64(const vec3&)>& pick)
{
    sort(
        ctx.entities.begin(),
        ctx.entities.end(),
        [pick](const SpaceGraphGenerationEntity& a, const SpaceGraphGenerationEntity& b) -> bool
        {
            return pick(a.transform.position()) < pick(b.transform.position());
        }
    );

    f64 minEdge = min(pick(ctx.innerExtent), pick(ctx.outerExtent));
    f64 maxEdge = max(pick(ctx.innerExtent), pick(ctx.outerExtent));

    size_t gapIndex = 0;
    f64 gap = 0;

    for (size_t i = 0; i < ctx.entities.size(); i++)
    {
        const SpaceGraphGenerationEntity& entity = ctx.entities.at(i);

        auto [ minExtent, maxExtent ] = entity.localExtent(ctx.innerExtent, ctx.outerExtent);

        if (f64 entityGap = max<f64>(pick(minExtent) - minEdge, 0); entityGap > gap)
        {
            gapIndex = i;
            gap = entityGap;
        }

        minEdge = max(minEdge, pick(maxExtent));
    }

    if (f64 entityGap = max<f64>(maxEdge - pick(ctx.entities.back().localExtent(ctx.innerExtent, ctx.outerExtent).second), 0); entityGap > gap)
    {
        gapIndex = ctx.entities.size();
    }

    if (gapIndex == ctx.entities.size())
    {
        return pick(ctx.entities.back().localExtent(ctx.innerExtent, ctx.outerExtent).second);
    }
    else
    {
        return pick(ctx.entities.at(gapIndex).localExtent(ctx.innerExtent, ctx.outerExtent).first);
    }
}
