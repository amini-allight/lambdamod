/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "ppm.hpp"

void writePPM(const string& path, u32 width, u32 height, const u8* pixels)
{
    ofstream file(path);

    file << "P3\n";
    file << width << " " << height << "\n";
    file << 255 << "\n";

    for (u32 i = 0; i < width * height; i++)
    {
        file << int(pixels[(i * 4) + 0]);
        file << " ";
        file << int(pixels[(i * 4) + 1]);
        file << " ";
        file << int(pixels[(i * 4) + 2]);
        file << "\n";
    }
}

void writePPM(const string& path, u32 width, u32 height, const vector<fvec4>& pixels)
{
    ofstream file(path);

    file << "P3\n";
    file << width << " " << height << "\n";
    file << 255 << "\n";

    for (u32 i = 0; i < width * height; i++)
    {
        file << int(pixels.at(i).x * 255);
        file << " ";
        file << int(pixels.at(i).y * 255);
        file << " ";
        file << int(pixels.at(i).z * 255);
        file << "\n";
    }
}
