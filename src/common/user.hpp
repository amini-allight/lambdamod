/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "script_types.hpp"
#include "network_event.hpp"
#include "grip_state.hpp"
#include "tether.hpp"
#include "confinement.hpp"

#if defined(LMOD_SERVER)
#include "server/text_tracker.hpp"
#endif

class Entity;
class ScriptContext;

class User
{
public:
    User(UserID id, const string& name, const string& world);
    User(UserID id, const string& name, const string& world, const string& password);

#if defined(LMOD_SERVER)
    void step(ScriptContext* context, f64 stepInterval);
#endif

    vector<NetworkEvent> compare(const User& previous) const;
    vector<NetworkEvent> compareSelf(const User& previous, const Entity* host) const;

    vector<NetworkEvent> initial() const;
    vector<NetworkEvent> initialSelf() const;

    UserID id() const;
    const string& name() const;
    const string& salt() const;
    const string& password() const;

    void activate(bool vr);
    void deactivate();
    bool active() const;
    bool vr() const;

    void attach(EntityID id);
    void detach();
    bool attached() const;
    EntityID entityID() const;

    void setAdmin(bool state);
    bool admin() const;

    void setTeams(const set<string>& teams);
    const set<string>& teams() const;

    void setMagnitude(u32 magnitude);
    u32 magnitude() const;

    void setAdvantage(i32 advantage);
    i32 advantage() const;

    void setDoom(bool state);
    bool doom() const;

    GripState& gripStates(size_t index);
    const GripState& gripStates(size_t index) const;

    void shift(const string& world);
    const string& world() const;

    void setVoiceChannel(const string& name);
    const string& voiceChannel() const;

    void setColor(const vec3& color);
    const vec3& color() const;

    void setViewer(const mat4& transform, bool forced);
    const mat4& viewer() const;

    void setRoom(const mat4& transform, bool forced);
    const mat4& room() const;

    void setRoomOffset(const mat4& transform);
    const mat4& roomOffset() const;

    void setHead(const mat4& transform);
    const optional<mat4>& head() const;

    void setLeftHand(const mat4& transform);
    const optional<mat4>& leftHand() const;

    void setRightHand(const mat4& transform);
    const optional<mat4>& rightHand() const;

    void setHips(const mat4& transform);
    const optional<mat4>& hips() const;

    void setLeftFoot(const mat4& transform);
    const optional<mat4>& leftFoot() const;

    void setRightFoot(const mat4& transform);
    const optional<mat4>& rightFoot() const;

    void setChest(const mat4& transform);
    const optional<mat4>& chest() const;

    void setLeftShoulder(const mat4& transform);
    const optional<mat4>& leftShoulder() const;

    void setRightShoulder(const mat4& transform);
    const optional<mat4>& rightShoulder() const;

    void setLeftElbow(const mat4& transform);
    const optional<mat4>& leftElbow() const;

    void setRightElbow(const mat4& transform);
    const optional<mat4>& rightElbow() const;

    void setLeftWrist(const mat4& transform);
    const optional<mat4>& leftWrist() const;

    void setRightWrist(const mat4& transform);
    const optional<mat4>& rightWrist() const;

    void setLeftKnee(const mat4& transform);
    const optional<mat4>& leftKnee() const;

    void setRightKnee(const mat4& transform);
    const optional<mat4>& rightKnee() const;

    void setLeftAnkle(const mat4& transform);
    const optional<mat4>& leftAnkle() const;

    void setRightAnkle(const mat4& transform);
    const optional<mat4>& rightAnkle() const;

    void anchor(const map<VRDevice, mat4>& mapping);
    void unanchor();
    bool anchored() const;
    const map<VRDevice, mat4>& anchorMapping() const;

    void setPing(u32 ping);
    u32 ping() const;

    void setAngle(f64 angle);
    f64 angle() const;

    void setAspect(f64 aspect);
    f64 aspect() const;

    void setBounds(const vec2& bounds);
    const vec2& bounds() const;

#if defined(LMOD_SERVER)
    void storeCommand(const string& text);
    string loadCommand();

    void recordHistory(const string& text);

    void logScript(const string& message);
    void log(const string& message);
    void error(const string& message);
    void chat(const User* user, const string& message);
    void directChatIn(const User* user, const string& message);
    void directChatOut(const User* user, const string& message);
    void teamChatIn(const User* user, const string& name, const string& message);
    void teamChatOut(const string& name, const string& message);
    void pushMessage(const Message& message);

    void addPing(UserID id);

    void addHaptic(const HapticEffect& effect);

    void addDocument(const string& name);
    void removeDocument(const string& name);
    void updateDocument(const string& name, const string& text);

    void requestText(const TextRequest& request, UserID userID, const optional<Lambda>& callback = {});
    void respondToText(ScriptContext* context, const TextResponse& response);

    void tether(f64 distance, const set<UserID>& userIDs);
    void untether();
    const Tether& tether() const;

    void confine(const vec3& position, f64 distance);
    void unconfine();
    const Confinement& confinement() const;

    void setFade(bool state);
    bool fade() const;

    void setWait(bool state);
    bool wait() const;

    void kick();
    void resetKick();
    bool shouldKick() const;
#elif defined(LMOD_CLIENT)
    void setSignal(bool signal);
    bool signal() const;

    void setTitlecard(const optional<TextTitlecardRequest>& titlecard);
    const optional<TextTitlecardRequest>& titlecard() const;

    void setTexts(const deque<TextRequest>& texts);
    const deque<TextRequest>& texts() const;

    void setKnowledge(const vector<TextKnowledgeRequest>& knowledge);
    const vector<TextKnowledgeRequest>& knowledge() const;

    void setTether(const Tether& tether);
    const Tether& tether() const;

    void setConfinement(const Confinement& confinement);
    const Confinement& confinement() const;

    void setFade(bool state);
    bool fade() const;

    void setWait(bool state);
    bool wait() const;
#endif

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    UserID _id;
    string _name;
    string _salt;
    string _password;
    bool _active;
    bool _vr;
    EntityID _entityID;
    bool _admin;
    set<string> _teams;
    u32 _magnitude;
    i32 _advantage;
    bool _doom;
    GripState _gripStates[handCount];
    string _world;
    string _voiceChannel;
    vec3 _color;

    mat4 _viewer;
    mutable bool viewerForcedUpdate;
    mat4 _room;
    mutable bool roomForcedUpdate;
    mat4 _roomOffset;
    optional<mat4> _head;
    optional<mat4> _leftHand;
    optional<mat4> _rightHand;
    optional<mat4> _hips;
    optional<mat4> _leftFoot;
    optional<mat4> _rightFoot;
    optional<mat4> _chest;
    optional<mat4> _leftShoulder;
    optional<mat4> _rightShoulder;
    optional<mat4> _leftElbow;
    optional<mat4> _rightElbow;
    optional<mat4> _leftWrist;
    optional<mat4> _rightWrist;
    optional<mat4> _leftKnee;
    optional<mat4> _rightKnee;
    optional<mat4> _leftAnkle;
    optional<mat4> _rightAnkle;

    bool _anchored;
    map<VRDevice, mat4> _anchorMapping;

    u32 _ping;
    f64 _angle;
    f64 _aspect;
    vec2 _bounds;

#if defined(LMOD_SERVER)
    string partialCommand;
    vector<string> commandHistory;

    vector<Message> messages;
    vector<UserID> pings;
    vector<HapticEffect> hapticEffects;
    vector<TextRequest> allTextRequests; // This exists because reversing titlecard/requests/knowledge state into a series of requests is very hard
    map<string, string> documents;

    bool signal;
    optional<TextTracker> titlecard;
    deque<TextTracker> requests;
    vector<TextKnowledgeRequest> knowledge;
    Tether _tether;
    Confinement _confinement;
    bool _fade;
    bool _wait;

    bool _shouldKick;

    void respondToUnary(ScriptContext* context, const TextTracker& tracker, const TextUnaryResponse& response);
    void respondToBinary(ScriptContext* context, const TextTracker& tracker, const TextBinaryResponse& response);
    void respondToOptions(ScriptContext* context, const TextTracker& tracker, const TextOptionsResponse& response);
    void respondToChoose(ScriptContext* context, const TextTracker& tracker, const TextChooseResponse& response);
    void respondToAssignValues(ScriptContext* context, const TextTracker& tracker, const TextAssignValuesResponse& response);
    void respondToSpendPoints(ScriptContext* context, const TextTracker& tracker, const TextSpendPointsResponse& response);
    void respondToAssignPoints(ScriptContext* context, const TextTracker& tracker, const TextAssignPointsResponse& response);
    void respondToVote(ScriptContext* context, const TextTracker& tracker, const TextVoteResponse& response);
    void respondToChooseMajor(ScriptContext* context, const TextTracker& tracker, const TextChooseMajorResponse& response);

    void callResponseHook(ScriptContext* context, const TextTracker& tracker);
    void callResponseHook(ScriptContext* context, const TextTracker& tracker, const Value& value);
#elif defined(LMOD_CLIENT)
    bool _signal;
    optional<TextTitlecardRequest> _titlecard;
    deque<TextRequest> _texts;
    vector<TextKnowledgeRequest> _knowledge;

    Tether _tether;
    Confinement _confinement;
    bool _fade;
    bool _wait;
#endif
};

void userMove(User* user, Entity* host, const mat4& transform);
void userGoto(const vector<tuple<User*, Entity*>>& users, const vec3& position, f64 radius, const vec3& up);
