/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "yaml_tools.hpp"
#include "network_event.hpp"
#include "user.hpp"
#include "body.hpp"
#include "sample.hpp"
#include "sample_playback.hpp"
#include "action.hpp"
#include "action_playback.hpp"
#include "input_types.hpp"
#include "hud_element.hpp"
#include "entity_update.hpp"
#include "bounding_box.hpp"

#include "script_context.hpp"

class World;

struct EntityMetaState
{
    EntityMetaState();

    void compare(const EntityMetaState& previous, EntityUpdate& update) const;

    void push(const NetworkEvent& event);
    vector<NetworkEvent> initial(const string& world, EntityID id) const;

    BodyPartID parentPartID() const;
    BodyPartSubID parentPartSubID() const;

    const string& name() const;
    bool active() const;

    const mat4& localTransform() const;
    const vec3& linearVelocity() const;
    const vec3& angularVelocity() const;

    UserID ownerID() const;
    bool shared() const;
    UserID attachedUserID() const;
    UserID selectingUserID() const;

    const map<string, Sample>& samples() const;
    const map<string, Action>& actions() const;

    void setParentPartID(BodyPartID id);
    void setParentPartSubID(BodyPartSubID id);

    void setName(const string& name);
    void setActive(bool active);

    void setLocalTransform(const mat4& transform);
    void setLinearVelocity(const vec3& linV);
    void setAngularVelocity(const vec3& angV);

    void setOwnerID(UserID id);
    void setShared(bool state);
    void setAttachedUserID(UserID id);
    void setSelectingUserID(UserID id);

    void setSamples(const map<string, Sample>& samples);
    void setActions(const map<string, Action>& actions);

private:
    BodyPartID _parentPartID;
    BodyPartSubID _parentPartSubID;

    string _name;
    bool _active;

    mat4 _transform;
    vec3 _linearVelocity;
    vec3 _angularVelocity;

    UserID _ownerID;
    bool _shared;
    UserID _attachedUserID;
    UserID _selectingUserID;

    map<string, Sample> _samples;
    map<string, Action> _actions;
};

struct EntityBaseState
{
    EntityBaseState();

    void compare(const EntityBaseState& previous, EntityUpdate& update, bool transformed = false) const;

    void push(ScriptContext* context, const NetworkEvent& event, bool transformed = false);
    vector<NetworkEvent> initial(const string& world, EntityID id, bool transformed = false) const;

    const map<string, Value>& fields() const;
    Body& body();
    const Body& body() const;

    f64 mass() const;
    f64 drag() const;
    f64 buoyancy() const;
    f64 lift() const;
    f64 magnetism() const;
    f64 friction() const;
    f64 restitution() const;

    bool host() const;
    bool shown() const;
    bool physical() const;
    bool visible() const;
    bool audible() const;

    const map<InputType, InputBinding>& bindings() const;
    const map<string, HUDElement>& hudElements() const;
    f64 fieldOfView() const;
    bool mouseLocked() const;
    f64 voiceVolume() const;
    vec3 tintColor() const;
    f64 tintStrength() const;

    void setFields(const map<string, Value>& fields);
    void setBody(const Body& body);

    void setMass(f64 mass);
    void setDrag(f64 drag);
    void setBuoyancy(f64 buoyancy);
    void setLift(f64 lift);
    void setMagnetism(f64 magnetism);
    void setFriction(f64 friction);
    void setRestitution(f64 restitution);

    void setHost(bool state);
    void setShown(bool state);
    void setPhysical(bool state);
    void setVisible(bool state);
    void setAudible(bool state);

    void setBindings(const map<InputType, InputBinding>& bindings);
    void setHUDElements(const map<string, HUDElement>& elements);
    void setFieldOfView(f64 angle);
    void setMouseLocked(bool state);
    void setVoiceVolume(f64 volume);
    void setTintColor(const vec3& color);
    void setTintStrength(f64 strength);

    string fieldsToString() const;
    void fieldsFromString(ScriptContext* context, const string& text);

private:
    map<string, Value> _fields;
    Body _body;

    f64 _mass;
    f64 _drag;
    f64 _buoyancy;
    f64 _lift;
    f64 _magnetism;
    f64 _friction;
    f64 _restitution;

    bool _host;
    bool _shown;
    bool _physical;
    bool _visible;
    bool _audible;

    map<InputType, InputBinding> _bindings;
    map<string, HUDElement> _hudElements;
    f64 _fieldOfView;
    bool _mouseLocked;
    f64 _voiceVolume;
    vec3 _tintColor;
    f64 _tintStrength;
};

struct EntityTransformedState : public EntityBaseState
{
    EntityTransformedState();
    EntityTransformedState(const EntityBaseState& state);

    void compare(const EntityTransformedState& previous, EntityUpdate& update) const;

    void push(ScriptContext* context, const NetworkEvent& event);
    vector<NetworkEvent> initial(const string& world, EntityID id) const;

    SamplePlaybackID nextSamplePlaybackID() const;
    const map<SamplePlaybackID, SamplePlayback>& samplePlaybacks() const;
    ActionPlaybackID nextActionPlaybackID() const;
    const map<ActionPlaybackID, ActionPlayback>& actionPlaybacks() const;

    void setNextSamplePlaybackID(SamplePlaybackID id);
    void setSamplePlaybacks(const map<SamplePlaybackID, SamplePlayback>& playbacks);
    void setNextActionPlaybackID(ActionPlaybackID id);
    void setActionPlaybacks(const map<ActionPlaybackID, ActionPlayback>& playbacks);

private:
    SamplePlaybackID _nextSamplePlaybackID;
    map<SamplePlaybackID, SamplePlayback> _samplePlaybacks;
    ActionPlaybackID _nextActionPlaybackID;
    map<ActionPlaybackID, ActionPlayback> _actionPlaybacks;
};

class Entity
{
public:
    Entity(World* world, EntityID id);
    Entity(Entity* parent, EntityID id);
    Entity(World* world, EntityID id, const Entity& rhs);
    Entity(Entity* parent, EntityID id, const Entity& rhs);
    Entity(const Entity& rhs);
    Entity(Entity&& rhs) noexcept;
    ~Entity();

    Entity& operator=(const Entity& rhs);
    Entity& operator=(Entity&& rhs) noexcept;

    static Entity* load(World* world, const YAMLNode& node);
    static Entity* load(Entity* parent, const YAMLNode& node);

    bool allowed(User* user) const;

    void step(ScriptContext* context, f64 stepInterval);

    void input(
        ScriptContext* context,
        const string& input,
        bool up,
        f64 intensity,
        const vec3& position,
        const quaternion& rotation,
        const vec3& linV,
        const vec3& angV,
        const InputDimensions& dimensions,
        const InputTouch& touch
    );
    void collide(ScriptContext* context, EntityID otherID, const vec3& position);

    void compare(const Entity& previous, EntityUpdate& update) const;

#if defined(LMOD_SERVER)
    void push(User* user, ScriptContext* context, const NetworkEvent& event);
#elif defined(LMOD_CLIENT)
    void push(ScriptContext* context, const NetworkEvent& event);
#endif
    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> initial() const;
    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> terminal() const;

    void store();
    void reset();
    bool canDelete() const;

    World* world() const;
    Entity* parent() const;
    const map<EntityID, Entity*>& children() const;
    Entity* rootParent();
    bool hasParent(Entity* possible) const;
    size_t parentDepth() const;
    vec3 parentGlobalPosition() const;

    void add(EntityID parentID, Entity* entity);
    void remove(EntityID id);
    Entity* get(EntityID id) const;
    const map<EntityID, Entity*>& entities() const;
    void traverse(const function<void(Entity*)>& behavior);
    void traverse(const function<void(const Entity*)>& behavior) const;
    bool partiallyTraverse(const function<bool(Entity*)>& behavior);
    bool partiallyTraverse(const function<bool(const Entity*)>& behavior) const;

    const EntityBaseState& base() const;
    const EntityBaseState& transformed() const;

    EntityID id() const;
    BodyPartID parentPartID() const;
    BodyPartSubID parentPartSubID() const;
    const string& name() const;
    bool active() const;

    UserID ownerID() const;
    bool shared() const;
    UserID attachedUserID() const;
    UserID selectingUserID() const;

    vec3 localPosition() const;
    quaternion localRotation() const;
    vec3 localScale() const;
    mat4 localTransform() const;

    vec3 globalPosition() const;
    quaternion globalRotation() const;
    vec3 globalScale() const;
    mat4 globalTransform() const;

    const vec3& linearVelocity() const;
    const vec3& angularVelocity() const;

    const map<string, Value>& fields() const;
    Body& body();
    const Body& body() const;
    const map<string, Sample>& samples() const;
    const map<string, Action>& actions() const;
    const map<SamplePlaybackID, SamplePlayback>& samplePlaybacks() const;
    const map<ActionPlaybackID, ActionPlayback>& actionPlaybacks() const;

    f64 mass() const;
    f64 drag() const;
    f64 buoyancy() const;
    f64 lift() const;
    f64 magnetism() const;
    f64 friction() const;
    f64 restitution() const;

    bool host() const;
    bool shown() const;
    bool physical() const;
    bool visible() const;
    bool audible() const;

    const map<InputType, InputBinding>& bindings() const;
    const map<string, HUDElement>& hudElements() const;
    f64 fieldOfView() const;
    bool mouseLocked() const;
    f64 voiceVolume() const;
    vec3 tintColor() const;
    f64 tintStrength() const;

    void setParentPartID(BodyPartID partID);
    void setParentPartSubID(BodyPartSubID partSubID);
    void setName(const string& name);
    void setActive(bool state);

    void setOwnerID(UserID id);
    void setShared(bool state);
    void setAttachedUserID(UserID id);
    void setSelectingUserID(UserID id);

    void setLocalPosition(const vec3& position);
    void setLocalRotation(const quaternion& rotation);
    void setLocalScale(const vec3& scale);
    void setLocalTransform(const mat4& transform);

    void setGlobalPosition(const vec3& position);
    void setGlobalRotation(const quaternion& rotation);
    void setGlobalScale(const vec3& scale);
    void setGlobalTransform(const mat4& transform);

    void setLinearVelocity(const vec3& linV);
    void setAngularVelocity(const vec3& angV);

    bool has(const string& name) const;
    Value exec(ScriptContext* context, const string& name, const vector<Value>& arguments);
    const Value& read(const string& name) const;
    void write(const string& name, const Value& value);
    void remove(const string& name);

    void addSample(const string& name, const Sample& sample);
    void removeSample(const string& name);

    void addAction(const string& name, const Action& action);
    void removeAction(const string& name);

    SamplePlaybackID playSample(
        const string& name,
        f64 volume,
        bool directional,
        f64 angle,
        const string& anchorName,
        bool loop
    );
    SamplePlaybackID playSamplePreview(const string& name);
    void stopSample(SamplePlaybackID id);
    void resumeSamplePlayback(SamplePlaybackID id);
    void pauseSamplePlayback(SamplePlaybackID id);
    void seekSamplePlayback(SamplePlaybackID id, u64 position);
    ActionPlaybackID playAction(const string& name, bool loop);
    ActionPlaybackID playActionPreview(const string& name);
    void stopAction(ActionPlaybackID id);
    void resumeActionPlayback(ActionPlaybackID id);
    void pauseActionPlayback(ActionPlaybackID id);
    void seekActionPlayback(ActionPlaybackID id, u64 position);

    void setMass(f64 v);
    void setDrag(f64 v);
    void setBuoyancy(f64 v);
    void setLift(f64 v);
    void setMagnetism(f64 v);
    void setFriction(f64 v);
    void setRestitution(f64 v);

    void setHost(bool state);
    void setShown(bool state);
    void setPhysical(bool state);
    void setVisible(bool state);
    void setAudible(bool state);

    void addBinding(InputType input, const InputBinding& binding);
    void removeBinding(InputType input);

    void addHUDElement(const string& name, const HUDElement& element);
    void removeHUDElement(const string& name);

    void setFieldOfView(f64 fov);
    void setMouseLocked(bool state);
    void setVoiceVolume(f64 volume);
    void setTintColor(const vec3& color);
    void setTintStrength(f64 strength);

    f64 boundingRadius() const;
    BoundingBox boundingBox() const;
    f64 volume() const;
    f64 buoyancyVolume() const;
    f64 angularCrossSectionalArea() const;
    f64 linearCrossSectionalArea(const vec3& globalDirection) const;
    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    optional<mat4> anchor(const string& name) const;

    string fieldsToString() const;
    void fieldsFromString(ScriptContext* context, const string& text);

private:
    friend class World;

    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    World* _world;
    Entity* _parent;

    EntityID _id;
    EntityMetaState meta;
    EntityBaseState _base;
    EntityTransformedState _transformed;

    map<EntityID, Entity*> _children;

    Value callHook(ScriptContext* context, const string& name, const vector<Value>& arguments) const;

    void copy(const Entity& rhs);
};
