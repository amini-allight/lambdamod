/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_part.hpp"
#include "sound_constants.hpp"
#include "sound_generators.hpp"
#include "log.hpp"

string samplePartTypeToString(SamplePartType type)
{
    switch (type)
    {
    case Sample_Part_Tone :
        return "sample-part-tone";
    default :
        fatal("Encountered unknown sample part type '" + to_string(type) + "'.");
    }
}

SamplePartType samplePartTypeFromString(const string& s)
{
    if (s == "sample-part-tone")
    {
        return Sample_Part_Tone;
    }
    else
    {
        fatal("Encountered unknown sample part type '" + s + "'.");
    }
}

template<>
SamplePartType YAMLNode::convert() const
{
    return samplePartTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(SamplePartType v)
{
    emit(samplePartTypeToString(v));
}

SamplePart::SamplePart()
{
    _id = SamplePartID(1);

    _index = 0;
    _start = 0;
    _length = 1000;
    _volume = 1;

    _type = Sample_Part_Tone;
    data = SamplePartTone();
}

SamplePart::SamplePart(SamplePartID id)
    : SamplePart()
{
    _id = id;
}

SamplePart::SamplePart(SamplePartID id, const SamplePart& other)
    : SamplePart(other)
{
    _id = id;
}

void SamplePart::verify()
{
    _volume = max(_volume, 0.0);

    if (auto tone = get_if<SamplePartTone>(&data); _type == Sample_Part_Tone)
    {
        tone->verify();
    }
    else
    {
        _type = Sample_Part_Tone;
        data = SamplePartTone();
    }
}

SampleSoundData SamplePart::toSound() const
{
    SampleSoundData data;

    switch (_type)
    {
    case Sample_Part_Tone :
        data.samples = generateTone(sampleCount(), get<SamplePartTone>());
        break;
    }

    for (f32& sample : data.samples)
    {
        sample *= _volume;
    }

    return data;
}

u32 SamplePart::sampleOffset() const
{
    return ceil(soundFrequency * (_start / 1000.0));
}

u32 SamplePart::sampleCount() const
{
    return ceil(soundFrequency * (_length / 1000.0));
}

bool SamplePart::operator==(const SamplePart& rhs) const
{
    if (_id != rhs._id)
    {
        return false;
    }

    if (_type != rhs._type)
    {
        return false;
    }

    if (_index != rhs._index)
    {
        return false;
    }

    if (_start != rhs._start)
    {
        return false;
    }

    if (_length != rhs._length)
    {
        return false;
    }

    if (_volume != rhs._volume)
    {
        return false;
    }

    switch (_type)
    {
    default :
        fatal("Encountered unknown sample part type '" + to_string(_type) + "'.");
    case Sample_Part_Tone :
        return get<SamplePartTone>() == rhs.get<SamplePartTone>();
    }
}

bool SamplePart::operator!=(const SamplePart& rhs) const
{
    return !(*this == rhs);
}

SamplePartID SamplePart::id() const
{
    return _id;
}

u32 SamplePart::index() const
{
    return _index;
}

u32 SamplePart::start() const
{
    return _start;
}

u32 SamplePart::length() const
{
    return _length;
}

f64 SamplePart::volume() const
{
    return _volume;
}

SamplePartType SamplePart::type() const
{
    return _type;
}

void SamplePart::setIndex(u32 index)
{
    _index = index;
}

void SamplePart::setStart(u32 start)
{
    _start = start;
}

void SamplePart::setLength(u32 length)
{
    _length = length;
}

void SamplePart::setVolume(f64 volume)
{
    _volume = volume;
}

void SamplePart::setType(SamplePartType type)
{
    _type = type;

    switch (_type)
    {
    case Sample_Part_Tone :
        data = SamplePartTone();
        break;
    }
}

string SamplePart::dataToString() const
{
    stringstream ss;

    switch (_type)
    {
    case Sample_Part_Tone :
        msgpack::pack(ss, get<SamplePartTone>());
        break;
    }

    return ss.str();
}

void SamplePart::dataFromString(const string& s)
{
    switch (_type)
    {
    case Sample_Part_Tone :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SamplePartTone part;
        oh.get().convert(part);

        data = part;
        break;
    }
    }
}

template<>
SamplePart YAMLNode::convert() const
{
    SamplePart part;

    part._id = at("id").as<SamplePartID>();
    part._index = at("index").as<u32>();
    part._start = at("start").as<u32>();
    part._length = at("length").as<u32>();
    part._volume = at("volume").as<f64>();
    part._type = samplePartTypeFromString(at("type").as<string>());

    switch (part._type)
    {
    case Sample_Part_Tone :
        part.data = at("data").as<SamplePartTone>();
        break;
    }

    return part;
}

template<>
void YAMLSerializer::emit(SamplePart v)
{
    startMapping();

    emitPair("id", v._id);
    emitPair("index", v._index);
    emitPair("start", v._start);
    emitPair("length", v._length);
    emitPair("volume", v._volume);
    emitPair("type", samplePartTypeToString(v._type));

    switch (v._type)
    {
    case Sample_Part_Tone :
        emitPair("data", v.get<SamplePartTone>());
        break;
    }

    endMapping();
}
