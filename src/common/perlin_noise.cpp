/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "perlin_noise.hpp"
#include "tools.hpp"

PerlinNoise::PerlinNoise(u64 seed, f64 frequency, u32 depth)
    : frequency(frequency)
    , depth(depth)
{
    for (u32 i = 0; i < size; i++)
    {
        permutationTable[i] = i;
    }

    mt19937_64 engine(seed);
    shuffle(permutationTable.begin(), permutationTable.end(), engine);

    for (u32 i = 0; i < size; i++)
    {
        permutationTable[size + i] = permutationTable[i];
    }
}

f64 PerlinNoise::at(const vec2& p) const
{
    vec2 a = p * frequency;
    f64 amp = 1.0;
    f64 fin = 0;
    f64 div = 0.0;

    for (size_t i = 0; i < depth; i++)
    {
        div += size * amp;
        fin += noise2D(a) * amp;
        amp /= 2;
        a *= 2;
    }

    return fin / div;
}

f64 PerlinNoise::noise2D(const vec2& position) const
{
    ivec2 iposition = position;
    vec2 fraction = position - vec2(iposition);

    i32 s = noise2D(iposition);
    i32 t = noise2D(iposition + ivec2(1, 0));
    i32 u = noise2D(iposition + ivec2(0, 1));
    i32 v = noise2D(iposition + ivec2(1, 1));

    f64 lo = serp(s, t, fraction.x);
    f64 hi = serp(u, v, fraction.x);

    return serp(lo, hi, fraction.y);
}

u8 PerlinNoise::noise2D(const ivec2& position) const
{
    i32 tmp = permutationTable[position.y % static_cast<i32>(size * frequency)];

    return permutationTable[(tmp + position.x) % static_cast<i32>(size * frequency)];
}

f64 PerlinNoise::lerp(f64 a, f64 b, f64 t) const
{
    return a * (1 - t) + b * t;
}

f64 PerlinNoise::serp(f64 a, f64 b, f64 t) const
{
    // Smoothed
    return lerp(a, b, sq(t) * (3 - 2 * t));
}
