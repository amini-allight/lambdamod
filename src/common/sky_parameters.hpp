/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "hdr_color.hpp"

enum SkyBaseType : i32
{
    Sky_Base_Blank,
    Sky_Base_Unidirectional,
    Sky_Base_Bidirectional,
    Sky_Base_Omnidirectional
};

string skyBaseTypeToString(SkyBaseType type);
SkyBaseType skyBaseTypeFromString(const string& s);

struct SkyBaseBlank
{
    SkyBaseBlank();

    void verify();

    bool operator==(const SkyBaseBlank& rhs) const;
    bool operator!=(const SkyBaseBlank& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {};

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);
    }

    typedef msgpack::type::tuple<> layout;
};

struct SkyBaseUnidirectional
{
    SkyBaseUnidirectional();

    void verify();

    bool operator==(const SkyBaseUnidirectional& rhs) const;
    bool operator!=(const SkyBaseUnidirectional& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            horizonColor,
            zenithColor
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        horizonColor = get<0>(payload);
        zenithColor = get<1>(payload);
    }

    typedef msgpack::type::tuple<HDRColor, HDRColor> layout;

    HDRColor horizonColor;
    HDRColor zenithColor;
};

struct SkyBaseBidirectional
{
    SkyBaseBidirectional();

    void verify();

    bool operator==(const SkyBaseBidirectional& rhs) const;
    bool operator!=(const SkyBaseBidirectional& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            horizonColor,
            lowerZenithColor,
            upperZenithColor
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        horizonColor = get<0>(payload);
        lowerZenithColor = get<1>(payload);
        upperZenithColor = get<2>(payload);
    }

    typedef msgpack::type::tuple<HDRColor, HDRColor, HDRColor> layout;

    HDRColor horizonColor;
    HDRColor lowerZenithColor;
    HDRColor upperZenithColor;
};

struct SkyBaseOmnidirectional
{
    SkyBaseOmnidirectional();

    void verify();

    bool operator==(const SkyBaseOmnidirectional& rhs) const;
    bool operator!=(const SkyBaseOmnidirectional& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        color = get<0>(payload);
    }

    typedef msgpack::type::tuple<HDRColor> layout;

    HDRColor color;
};

struct SkyBase
{
    SkyBase();

    void verify();

    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }

    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }

    bool operator==(const SkyBase& rhs) const;
    bool operator!=(const SkyBase& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<SkyBaseType>(std::get<0>(payload));
        dataFromString(std::get<1>(payload));
    }

    string dataToString() const;
    void dataFromString(const string& s);

    typedef msgpack::type::tuple<u8, string> layout;

    SkyBaseType type;
    variant<SkyBaseBlank, SkyBaseUnidirectional, SkyBaseBidirectional, SkyBaseOmnidirectional> data;
};

enum SkyLayerType : i32
{
    Sky_Layer_Stars,
    Sky_Layer_Circle,
    Sky_Layer_Cloud,
    Sky_Layer_Aurora,
    Sky_Layer_Comet,
    Sky_Layer_Vortex,
    Sky_Layer_Meteor_Shower
};

string skyLayerTypeToString(SkyLayerType type);
SkyLayerType skyLayerTypeFromString(const string& s);

struct SkyLayerStars
{
    SkyLayerStars();

    void verify();

    bool operator==(const SkyLayerStars& rhs) const;
    bool operator!=(const SkyLayerStars& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            density,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        density = get<0>(payload);
        color = get<1>(payload);
    }

    typedef msgpack::type::tuple<f64, HDRColor> layout;

    f64 density;
    HDRColor color;
};

struct SkyLayerCircle
{
    SkyLayerCircle();

    void verify();

    bool operator==(const SkyLayerCircle& rhs) const;
    bool operator!=(const SkyLayerCircle& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            position,
            size,
            color,
            phase
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        position = get<0>(payload);
        size = get<1>(payload);
        color = get<2>(payload);
        phase = get<3>(payload);
    }

    typedef msgpack::type::tuple<vec2, f64, HDRColor, f64> layout;

    vec2 position;
    f64 size;
    HDRColor color;
    f64 phase;
};

struct SkyLayerCloud
{
    SkyLayerCloud();

    void verify();

    bool operator==(const SkyLayerCloud& rhs) const;
    bool operator!=(const SkyLayerCloud& rhs) const;
    
    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            density,
            position,
            size,
            innerColor,
            outerColor
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        density = get<0>(payload);
        position = get<1>(payload);
        size = get<2>(payload);
        innerColor = get<3>(payload);
        outerColor = get<4>(payload);
    }

    typedef msgpack::type::tuple<f64, vec2, vec2, HDRColor, HDRColor> layout;

    f64 density;
    vec2 position;
    vec2 size;
    HDRColor innerColor;
    HDRColor outerColor;
};

struct SkyLayerAurora
{
    SkyLayerAurora();

    void verify();

    bool operator==(const SkyLayerAurora& rhs) const;
    bool operator!=(const SkyLayerAurora& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            density,
            lowerColor,
            upperColor
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        density = get<0>(payload);
        lowerColor = get<1>(payload);
        upperColor = get<2>(payload);
    }

    typedef msgpack::type::tuple<f64, HDRColor, HDRColor> layout;

    f64 density;
    HDRColor lowerColor;
    HDRColor upperColor;
};

struct SkyLayerComet
{
    SkyLayerComet();

    void verify();

    bool operator==(const SkyLayerComet& rhs) const;
    bool operator!=(const SkyLayerComet& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            position,
            size,
            length,
            rotation,
            headColor,
            tailColor
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        position = get<0>(payload);
        size = get<1>(payload);
        length = get<2>(payload);
        rotation = get<3>(payload);
        headColor = get<4>(payload);
        tailColor = get<5>(payload);
    }

    typedef msgpack::type::tuple<vec2, f64, f64, f64, HDRColor, HDRColor> layout;

    vec2 position;
    f64 size;
    f64 length;
    f64 rotation;
    HDRColor headColor;
    HDRColor tailColor;
};

struct SkyLayerVortex
{
    SkyLayerVortex();

    void verify();

    bool operator==(const SkyLayerVortex& rhs) const;
    bool operator!=(const SkyLayerVortex& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            position,
            rotationSpeed,
            radius,
            innerColor,
            outerColor
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        position = get<0>(payload);
        rotationSpeed = get<1>(payload);
        radius = get<2>(payload);
        innerColor = get<3>(payload);
        outerColor = get<4>(payload);
    }

    typedef msgpack::type::tuple<vec2, f64, f64, HDRColor, HDRColor> layout;

    vec2 position;
    f64 rotationSpeed;
    f64 radius;
    HDRColor innerColor;
    HDRColor outerColor;
};

struct SkyLayerMeteorShower
{
    SkyLayerMeteorShower();

    void verify();

    bool operator==(const SkyLayerMeteorShower& rhs) const;
    bool operator!=(const SkyLayerMeteorShower& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            density,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        density = get<0>(payload);
        color = get<1>(payload);
    }

    typedef msgpack::type::tuple<f64, HDRColor> layout;

    f64 density;
    HDRColor color;
};

struct SkyLayer
{
    SkyLayer();
    SkyLayer(const SkyLayerStars& data);
    SkyLayer(const SkyLayerCircle& data);
    SkyLayer(const SkyLayerCloud& data);
    SkyLayer(const SkyLayerAurora& data);
    SkyLayer(const SkyLayerComet& data);
    SkyLayer(const SkyLayerVortex& data);
    SkyLayer(const SkyLayerMeteorShower& data);

    void verify();

    void setType(SkyLayerType type);

    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }

    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }

    bool operator==(const SkyLayer& rhs) const;
    bool operator!=(const SkyLayer& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<SkyLayerType>(std::get<0>(payload));
        dataFromString(std::get<1>(payload));
    }

    string dataToString() const;
    void dataFromString(const string& s);

    typedef msgpack::type::tuple<u8, string> layout;

    SkyLayerType type;
    variant<SkyLayerStars, SkyLayerCircle, SkyLayerCloud, SkyLayerAurora, SkyLayerComet, SkyLayerVortex, SkyLayerMeteorShower> data;
};

struct SkyParameters
{
    SkyParameters();

    void verify();

    bool operator==(const SkyParameters& rhs) const;
    bool operator!=(const SkyParameters& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            enabled,
            seed,
            base,
            layers
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        enabled = get<0>(payload);
        seed = get<1>(payload);
        base = get<2>(payload);
        layers = get<3>(payload);
    }

    typedef msgpack::type::tuple<bool, u64, SkyBase, vector<SkyLayer>> layout;

    bool enabled;
    u64 seed;
    SkyBase base;
    vector<SkyLayer> layers;
};
