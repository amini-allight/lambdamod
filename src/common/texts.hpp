/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

struct TextUnaryResponse
{
    TextUnaryResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {};

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);
    }

    typedef msgpack::type::tuple<> layout;
};

struct TextBinaryResponse
{
    TextBinaryResponse()
    {
        choice = 0;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            choice
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        choice = get<0>(payload);
    }

    u64 choice;

    typedef msgpack::type::tuple<u64> layout;
};

struct TextOptionsResponse
{
    TextOptionsResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            choice
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        choice = get<0>(payload);
    }

    string choice;

    typedef msgpack::type::tuple<string> layout;
};

struct TextChooseResponse
{
    TextChooseResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            choices
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        choices = get<0>(payload);
    }

    set<string> choices;

    typedef msgpack::type::tuple<set<string>> layout;
};

struct TextAssignValuesResponse
{
    TextAssignValuesResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            assignment
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        assignment = get<0>(payload);
    }

    map<string, string> assignment;

    typedef msgpack::type::tuple<map<string, string>> layout;
};

struct TextSpendPointsResponse
{
    TextSpendPointsResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            purchases
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        purchases = get<0>(payload);
    }

    set<string> purchases;

    typedef msgpack::type::tuple<set<string>> layout;
};

struct TextAssignPointsResponse
{
    TextAssignPointsResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            assignment
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        assignment = get<0>(payload);
    }

    map<string, u64> assignment;

    typedef msgpack::type::tuple<map<string, u64>> layout;
};

struct TextVoteResponse
{
    TextVoteResponse()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            votes
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        votes = get<0>(payload);
    }

    set<u64> votes;

    typedef msgpack::type::tuple<set<u64>> layout;
};

struct TextChooseMajorResponse
{
    TextChooseMajorResponse()
    {
        choice = 0;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            choice
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        choice = get<0>(payload);
    }

    u64 choice;

    typedef msgpack::type::tuple<u64> layout;
};

struct TextSignalRequest
{
    TextSignalRequest()
    {
        state = false;
    }

    bool operator==(const TextSignalRequest& rhs) const;
    bool operator!=(const TextSignalRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            state
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        state = get<0>(payload);
    }

    bool valid() const;

    bool state;

    typedef msgpack::type::tuple<bool> layout;
};

struct TextTitlecardRequest
{
    TextTitlecardRequest()
    {

    }

    bool operator==(const TextTitlecardRequest& rhs) const;
    bool operator!=(const TextTitlecardRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            subtitle
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        subtitle = get<1>(payload);
    }

    bool valid() const;

    string title;
    string subtitle;

    typedef msgpack::type::tuple<string, string> layout;
};

struct TextTimeoutRequest
{
    TextTimeoutRequest()
    {
        timeout = 0;
    }

    bool operator==(const TextTimeoutRequest& rhs) const;
    bool operator!=(const TextTimeoutRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            text,
            timeout
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        text = get<1>(payload);
        timeout = get<2>(payload);
    }

    bool valid() const;

    string title;
    string text;
    f64 timeout;

    typedef msgpack::type::tuple<string, string, f64> layout;
};

struct TextUnaryRequest
{
    TextUnaryRequest()
    {

    }

    bool operator==(const TextUnaryRequest& rhs) const;
    bool operator!=(const TextUnaryRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            text
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        text = get<1>(payload);
    }

    bool valid() const;
    bool valid(const TextUnaryResponse& response) const;

    string title;
    string text;

    typedef msgpack::type::tuple<string, string> layout;
};

struct TextBinaryRequest
{
    TextBinaryRequest()
    {

    }

    bool operator==(const TextBinaryRequest& rhs) const;
    bool operator!=(const TextBinaryRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            text
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        text = get<1>(payload);
    }

    bool valid() const;
    bool valid(const TextBinaryResponse& response) const;

    string title;
    string text;

    typedef msgpack::type::tuple<string, string> layout;
};

struct TextOptionsRequest
{
    TextOptionsRequest()
    {

    }

    bool operator==(const TextOptionsRequest& rhs) const;
    bool operator!=(const TextOptionsRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            text,
            options
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        text = get<1>(payload);
        options = get<2>(payload);
    }

    bool valid() const;
    bool valid(const TextOptionsResponse& response) const;

    string title;
    string text;
    set<string> options;

    typedef msgpack::type::tuple<string, string, set<string>> layout;
};

struct TextChooseRequest
{
    TextChooseRequest()
    {
        count = 0;
    }

    bool operator==(const TextChooseRequest& rhs) const;
    bool operator!=(const TextChooseRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            options,
            count
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        options = get<1>(payload);
        count = get<2>(payload);
    }

    bool valid() const;
    bool valid(const TextChooseResponse& response) const;

    string title;
    set<string> options;
    u64 count;

    typedef msgpack::type::tuple<string, set<string>, u64> layout;
};

struct TextAssignValuesRequest
{
    TextAssignValuesRequest()
    {

    }

    bool operator==(const TextAssignValuesRequest& rhs) const;
    bool operator!=(const TextAssignValuesRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            keys,
            values
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        keys = get<1>(payload);
        values = get<2>(payload);
    }

    bool valid() const;
    bool valid(const TextAssignValuesResponse& response) const;

    string title;
    set<string> keys;
    set<string> values;

    typedef msgpack::type::tuple<string, set<string>, set<string>> layout;
};

struct TextSpendPointsRequest
{
    TextSpendPointsRequest()
    {
        points = 0;
    }

    bool operator==(const TextSpendPointsRequest& rhs) const;
    bool operator!=(const TextSpendPointsRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            prices,
            points
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        prices = get<1>(payload);
        points = get<2>(payload);
    }

    bool valid() const;
    bool valid(const TextSpendPointsResponse& response) const;

    string title;
    map<string, u64> prices;
    u64 points;

    typedef msgpack::type::tuple<string, map<string, u64>, u64> layout;
};

struct TextAssignPointsRequest
{
    TextAssignPointsRequest()
    {
        points = 0;
    }

    bool operator==(const TextAssignPointsRequest& rhs) const;
    bool operator!=(const TextAssignPointsRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            keys,
            points
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        keys = get<1>(payload);
        points = get<2>(payload);
    }

    bool valid() const;
    bool valid(const TextAssignPointsResponse& response) const;

    string title;
    set<string> keys;
    u64 points;

    typedef msgpack::type::tuple<string, set<string>, u64> layout;
};

struct TextVoteRequest
{
    TextVoteRequest()
    {
        votes = 0;
    }

    bool operator==(const TextVoteRequest& rhs) const;
    bool operator!=(const TextVoteRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            options,
            votes
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        options = get<1>(payload);
        votes = get<2>(payload);
    }

    bool valid() const;
    bool valid(const TextVoteResponse& response) const;

    string title;
    vector<string> options;
    u64 votes;

    typedef msgpack::type::tuple<string, vector<string>, u64> layout;
};

struct TextChooseMajorRequest
{
    TextChooseMajorRequest()
    {

    }

    bool operator==(const TextChooseMajorRequest& rhs) const;
    bool operator!=(const TextChooseMajorRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            options
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        options = get<1>(payload);
    }

    bool valid() const;
    bool valid(const TextChooseMajorResponse& response) const;

    string title;
    vector<tuple<string, string, string>> options;

    typedef msgpack::type::tuple<string, vector<tuple<string, string, string>>> layout;
};

struct TextKnowledgeRequest
{
    TextKnowledgeRequest()
    {

    }

    bool operator==(const TextKnowledgeRequest& rhs) const;
    bool operator!=(const TextKnowledgeRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            title,
            subtitle,
            text
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        title = get<0>(payload);
        subtitle = get<1>(payload);
        text = get<2>(payload);
    }

    bool valid() const;

    string title;
    string subtitle;
    string text;

    typedef msgpack::type::tuple<string, string, string> layout;
};

enum TextClearType : u8
{
    Text_Clear_All_But_Knowledge,
    Text_Clear_All,
    Text_Clear_Specific_Knowledge
};

struct TextClearRequest
{
    TextClearRequest()
    {
        type = Text_Clear_All_But_Knowledge;
    }

    bool operator==(const TextClearRequest& rhs) const;
    bool operator!=(const TextClearRequest& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            title
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<TextClearType>(get<0>(payload));
        title = get<1>(payload);
    }

    bool valid() const;

    TextClearType type;
    string title;

    typedef msgpack::type::tuple<u8, string> layout;
};
