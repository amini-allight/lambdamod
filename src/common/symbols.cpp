/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "symbols.hpp"
#include "log.hpp"
#include "yaml_tools.hpp"

string symbolTypeToString(SymbolType type)
{
    switch (type)
    {
    case Symbol_Star :
        return "symbol-star";
    default :
        fatal("Encountered unknown symbol type '" + to_string(type) + "'.");
    }
}

SymbolType symbolTypeFromString(const string& s)
{
    if (s == "symbol-star")
    {
        return Symbol_Star;
    }
    else
    {
        throw runtime_error("Encountered unknown symbol type '" + s + "'.");
    }
}

template<>
SymbolType YAMLNode::convert() const
{
    return symbolTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(SymbolType v)
{
    emit(symbolTypeToString(v));
}
