/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_CLIENT
#include "body_render_data.hpp"
#include "log.hpp"

bool isMaterialTransparent(BodyRenderMaterialType type)
{
    switch (type)
    {
    default :
        fatal("Encountered unknown body render material type '" + to_string(type) + "' while checking transparency.");
    case Body_Render_Material_Vertex :
    case Body_Render_Material_Shadeless_Vertex :
    case Body_Render_Material_Area :
    case Body_Render_Material_Shadeless_Area :
    case Body_Render_Material_Terrain :
    case Body_Render_Material_Shadeless_Terrain :
    case Body_Render_Material_Rope :
    case Body_Render_Material_Shadeless_Rope :
        return false;
    case Body_Render_Material_Texture :
    case Body_Render_Material_Shadeless_Texture :
        return true;
    }
}

bool canMaterialMerge(BodyRenderMaterialType type)
{
    switch (type)
    {
    default :
        fatal("Encountered unknown body render material type '" + to_string(type) + "' while checking merge behavior.");
    case Body_Render_Material_Vertex :
    case Body_Render_Material_Shadeless_Vertex :
    case Body_Render_Material_Terrain :
    case Body_Render_Material_Shadeless_Terrain :
    case Body_Render_Material_Rope :
    case Body_Render_Material_Shadeless_Rope :
        return true;
    case Body_Render_Material_Area :
    case Body_Render_Material_Shadeless_Area :
    case Body_Render_Material_Texture :
    case Body_Render_Material_Shadeless_Texture :
        return false;
    }
}

void BodyRenderMaterialData::add(const BodyRenderMaterialData& other)
{
    size_t previousIndicesSize = indices.size();
    indices.insert(
        indices.end(),
        other.indices.begin(),
        other.indices.end()
    );
    for (size_t i = previousIndicesSize; i < indices.size(); i++)
    {
        indices[i] += vertexCount;
    }
    
    vertexData.insert(
        vertexData.end(),
        other.vertexData.begin(),
        other.vertexData.end()
    );

    vertexCount += other.vertexCount;
}

void BodyRenderData::add(const BodyRenderData& other)
{
    for (const auto& [ otherType, otherMaterial ] : other.materials)
    {
        bool found = false;

        if (canMaterialMerge(otherType))
        {
            for (auto& [ type, material ] : materials)
            {
                if (type != otherType)
                {
                    continue;
                }

                material.add(otherMaterial);
                found = true;
            }
        }

        if (!found)
        {
            materials.push_back({ otherType, otherMaterial });
        }
    }

    lights.insert(lights.end(), other.lights.begin(), other.lights.end());

    occlusionPrimitives.insert(occlusionPrimitives.end(), other.occlusionPrimitives.begin(), other.occlusionPrimitives.end());
}
#endif
