/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "texts.hpp"
#include "log.hpp"

enum TextType : u8
{
    Text_Signal,
    Text_Titlecard,
    Text_Timeout,
    Text_Unary,
    Text_Binary,
    Text_Options,
    Text_Choose,
    Text_Assign_Values,
    Text_Spend_Points,
    Text_Assign_Points,
    Text_Vote,
    Text_Choose_Major,
    Text_Knowledge,
    Text_Clear
};

string textTypeToString(TextType type);
TextType textTypeFromString(const string& s);

class TextResponse
{
public:
    TextResponse();
    template<typename T>
    TextResponse(TextID id, const T& data);

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            _id,
            _type,
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        _id = get<0>(payload);
        _type = static_cast<TextType>(get<1>(payload));
        dataFromString(get<2>(payload));
    }

    TextID id() const;
    TextType type() const;
    template<typename T>
    T& data()
    {
        return get<T>(_data);
    }
    template<typename T>
    const T& data() const
    {
        return get<T>(_data);
    }

private:
    TextID _id;
    TextType _type;
    variant<
        TextUnaryResponse,
        TextBinaryResponse,
        TextOptionsResponse,
        TextChooseResponse,
        TextAssignValuesResponse,
        TextSpendPointsResponse,
        TextAssignPointsResponse,
        TextVoteResponse,
        TextChooseMajorResponse
    > _data;

    typedef msgpack::type::tuple<TextID, u8, string> layout;

    string dataToString() const
    {
        stringstream ss;

        switch (_type)
        {
        default :
            fatal("Encountered unknown text response type '" + to_string(_type) + "'.");
        case Text_Unary :
            msgpack::pack(ss, get<TextUnaryResponse>(_data));
            break;
        case Text_Binary :
            msgpack::pack(ss, get<TextBinaryResponse>(_data));
            break;
        case Text_Options :
            msgpack::pack(ss, get<TextOptionsResponse>(_data));
            break;
        case Text_Choose :
            msgpack::pack(ss, get<TextChooseResponse>(_data));
            break;
        case Text_Assign_Values :
            msgpack::pack(ss, get<TextAssignValuesResponse>(_data));
            break;
        case Text_Spend_Points :
            msgpack::pack(ss, get<TextSpendPointsResponse>(_data));
            break;
        case Text_Assign_Points :
            msgpack::pack(ss, get<TextAssignPointsResponse>(_data));
            break;
        case Text_Vote :
            msgpack::pack(ss, get<TextVoteResponse>(_data));
            break;
        case Text_Choose_Major :
            msgpack::pack(ss, get<TextChooseMajorResponse>(_data));
            break;
        }

        return ss.str();
    }

    void dataFromString(const string& s)
    {
        switch (_type)
        {
        default :
            fatal("Encountered unknown text response type '" + to_string(_type) + "'.");
        case Text_Unary :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextUnaryResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Binary :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextBinaryResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Options :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextOptionsResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Choose :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextChooseResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Assign_Values :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextAssignValuesResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Spend_Points :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextSpendPointsResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Assign_Points :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextAssignPointsResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Vote :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextVoteResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Choose_Major :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextChooseMajorResponse data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        }
    }
};

template<>
TextResponse::TextResponse(TextID id, const TextUnaryResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextBinaryResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextOptionsResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextChooseResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextSpendPointsResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextAssignPointsResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextVoteResponse& data);

template<>
TextResponse::TextResponse(TextID id, const TextChooseMajorResponse& data);

class TextRequest
{
public:
    TextRequest();
    template<typename T>
    TextRequest(TextID id, const T& data);

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            _id,
            _type,
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        _id = get<0>(payload);
        _type = static_cast<TextType>(get<1>(payload));
        dataFromString(get<2>(payload));
    }

    TextID id() const;
    TextType type() const;
    template<typename T>
    T& data()
    {
        return get<T>(_data);
    }
    template<typename T>
    const T& data() const
    {
        return get<T>(_data);
    }

    bool valid() const;

private:
    TextID _id;
    TextType _type;
    variant<
        TextSignalRequest,
        TextTitlecardRequest,
        TextTimeoutRequest,
        TextUnaryRequest,
        TextBinaryRequest,
        TextOptionsRequest,
        TextChooseRequest,
        TextAssignValuesRequest,
        TextSpendPointsRequest,
        TextAssignPointsRequest,
        TextVoteRequest,
        TextChooseMajorRequest,
        TextKnowledgeRequest,
        TextClearRequest
    > _data;

    typedef msgpack::type::tuple<TextID, u8, string> layout;

    string dataToString() const
    {
        stringstream ss;

        switch (_type)
        {
        default :
            fatal("Encountered unknown text request type '" + to_string(_type) + "'.");
        case Text_Signal :
            msgpack::pack(ss, get<TextSignalRequest>(_data));
            break;
        case Text_Titlecard :
            msgpack::pack(ss, get<TextTitlecardRequest>(_data));
            break;
        case Text_Timeout :
            msgpack::pack(ss, get<TextTimeoutRequest>(_data));
            break;
        case Text_Unary :
            msgpack::pack(ss, get<TextUnaryRequest>(_data));
            break;
        case Text_Binary :
            msgpack::pack(ss, get<TextBinaryRequest>(_data));
            break;
        case Text_Options :
            msgpack::pack(ss, get<TextOptionsRequest>(_data));
            break;
        case Text_Choose :
            msgpack::pack(ss, get<TextChooseRequest>(_data));
            break;
        case Text_Assign_Values :
            msgpack::pack(ss, get<TextAssignValuesRequest>(_data));
            break;
        case Text_Spend_Points :
            msgpack::pack(ss, get<TextSpendPointsRequest>(_data));
            break;
        case Text_Assign_Points :
            msgpack::pack(ss, get<TextAssignPointsRequest>(_data));
            break;
        case Text_Vote :
            msgpack::pack(ss, get<TextVoteRequest>(_data));
            break;
        case Text_Choose_Major :
            msgpack::pack(ss, get<TextChooseMajorRequest>(_data));
            break;
        case Text_Knowledge :
            msgpack::pack(ss, get<TextKnowledgeRequest>(_data));
            break;
        case Text_Clear :
            msgpack::pack(ss, get<TextClearRequest>(_data));
            break;
        }

        return ss.str();
    }

    void dataFromString(const string& s)
    {
        switch (_type)
        {
        default :
            fatal("Encountered unknown text request type '" + to_string(_type) + "'.");
        case Text_Signal :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextSignalRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Titlecard :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextTitlecardRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Timeout :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextTimeoutRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Unary :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextUnaryRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Binary :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextBinaryRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Options :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextOptionsRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Choose :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextChooseRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Assign_Values :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextAssignValuesRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Spend_Points :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextSpendPointsRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Assign_Points :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextAssignPointsRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Vote :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextVoteRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Choose_Major :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextChooseMajorRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Knowledge :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextKnowledgeRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        case Text_Clear :
        {
            msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

            TextClearRequest data;
            oh.get().convert(data);

            _data = data;
            break;
        }
        }
    }
};

template<>
TextRequest::TextRequest(TextID id, const TextSignalRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextTitlecardRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextTimeoutRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextUnaryRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextBinaryRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextOptionsRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextChooseRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextAssignValuesRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextSpendPointsRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextAssignPointsRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextVoteRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextChooseMajorRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextKnowledgeRequest& data);

template<>
TextRequest::TextRequest(TextID id, const TextClearRequest& data);
