/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "body_parts.hpp"
#include "sky_parameters.hpp"
#include "atmosphere_parameters.hpp"

class World;

struct WorldParameters
{
    f64 speedOfSound;
    vec3 gravity;
    vec3 flowVelocity;
    f64 density;
    vec3 up;
    f64 fogDistance;
    SkyParameters sky;
    AtmosphereParameters atmosphere;
};

struct WorldArea
{
    mat4 toAreaSpace;
    bool physical;
    bool visible;
    bool audible;
    BodyPartArea area;
};

class AreaEffectCache
{
public:
    AreaEffectCache();

    void updateWorld(const World* world);
    const WorldParameters& at(const vec3& position) const;

private:
    WorldParameters worldParameters;
    vector<WorldArea> worldAreas;

    mutable vec3 lastPosition;
    mutable WorldParameters lastResult;

    WorldParameters applyAreaEffects(const vec3& position) const;
};
