/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

class EntityUpdate;

class ActionPlayback
{
public:
    ActionPlayback();

    void verify();

    void compare(const ActionPlayback& previous, EntityUpdate& update) const;

    void resume();
    void pause();
    void seek(u64 position);

    bool operator==(const ActionPlayback& rhs) const;
    bool operator!=(const ActionPlayback& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            name,
            loop,
            elapsed,
            playing
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        name = get<0>(payload);
        loop = get<1>(payload);
        elapsed = get<2>(payload);
        playing = get<3>(payload);
    }

    typedef msgpack::type::tuple<string, bool, u64, bool> layout;

    string name;
    bool loop;
    u64 elapsed;
    bool playing;

private:
    mutable bool forcedSeek;
};
