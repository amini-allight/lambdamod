/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "mesh_cache.hpp"

static constexpr chrono::milliseconds cacheTimeout(10 * 1000);

MeshCache::MeshCache()
{

}

MeshCache::~MeshCache()
{
    for (const auto& [ key, entry ] : cachedTerrains)
    {
        delete entry;
    }

    for (const auto& [ key, entry ] : cachedStructures)
    {
        delete entry;
    }
}

const StructureMesh& MeshCache::ensure(const StructureNode& node) const
{
    return ensureEntry(node)->get();
}

const TerrainMesh& MeshCache::ensure(const EmissiveColor& color, const vector<TerrainNode>& nodes) const
{
    return ensureEntry(color, nodes)->get();
}

vector<vec3> MeshCache::intersect(
    const StructureNode& node,
    const vec3& position,
    const vec3& direction,
    f64 distance
) const
{
    return ensureEntry(node)->intersect(position, direction, distance);
}

vector<vec3> MeshCache::intersect(
    const EmissiveColor& color,
    const vector<TerrainNode>& nodes,
    const vec3& position,
    const vec3& direction,
    f64 distance
) const
{
    return ensureEntry(color, nodes)->intersect(position, direction, distance);
}

const MeshCacheEntry<StructureMesh>* MeshCache::ensureEntry(const StructureNode& node) const
{
    MeshCacheKey<StructureNode> key(node);

    auto it = find_if(
        cachedStructures.begin(),
        cachedStructures.end(),
        [key](const pair<MeshCacheKey<StructureNode>, MeshCacheEntry<StructureMesh>*>& pair) -> bool { return key == pair.first; }
    );

    const MeshCacheEntry<StructureMesh>* entry = nullptr;

    if (it != cachedStructures.end())
    {
        entry = it->second;
    }
    else
    {
        cachedStructures.push_back({ key, new MeshCacheEntry<StructureMesh>(buildStructure(node), cacheTimeout) });
        entry = cachedStructures.back().second;
    }

    clean();

    return entry;
}

const MeshCacheEntry<TerrainMesh>* MeshCache::ensureEntry(const EmissiveColor& color, const vector<TerrainNode>& nodes) const
{
    MeshCacheKey<EmissiveColor, vector<TerrainNode>> key(color, nodes);

    auto it = find_if(
        cachedTerrains.begin(),
        cachedTerrains.end(),
        [key](const pair<MeshCacheKey<EmissiveColor, vector<TerrainNode>>, MeshCacheEntry<TerrainMesh>*>& pair) -> bool { return key == pair.first; }
    );

    const MeshCacheEntry<TerrainMesh>* entry = nullptr;

    if (it != cachedTerrains.end())
    {
        entry = it->second;
    }
    else
    {
        cachedTerrains.push_back({ key, new MeshCacheEntry<TerrainMesh>(buildTerrain(color, nodes), cacheTimeout) });
        entry = cachedTerrains.back().second;
    }

    clean();

    return entry;
}

void MeshCache::clean() const
{
    for (auto it = cachedStructures.begin(); it != cachedStructures.end();)
    {
        if (it->second->expired())
        {
            delete it->second;
            cachedStructures.erase(it++);
        }
        else
        {
            it++;
        }
    }

    for (auto it = cachedTerrains.begin(); it != cachedTerrains.end();)
    {
        if (it->second->expired())
        {
            delete it->second;
            cachedTerrains.erase(it++);
        }
        else
        {
            it++;
        }
    }
}

MeshCache* meshCache = nullptr;
