/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum VRDevice : u8
{
    VR_Device_Head,
    VR_Device_Left_Hand,
    VR_Device_Right_Hand,
    VR_Device_Hips,
    VR_Device_Left_Foot,
    VR_Device_Right_Foot,
    VR_Device_Chest,
    VR_Device_Left_Shoulder,
    VR_Device_Right_Shoulder,
    VR_Device_Left_Elbow,
    VR_Device_Right_Elbow,
    VR_Device_Left_Wrist,
    VR_Device_Right_Wrist,
    VR_Device_Left_Knee,
    VR_Device_Right_Knee,
    VR_Device_Left_Ankle,
    VR_Device_Right_Ankle
};

string vrDeviceToString(VRDevice device);
VRDevice vrDeviceFromString(const string& s);
