/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "reverb_effect.hpp"
#include "tools.hpp"

ReverbEffect::ReverbEffect()
    : distance(0)
    , strength(0)
{

}

ReverbEffect::ReverbEffect(const SpaceGraphNode* node, const vec3& position)
{
    vec3 nearest;
    vec2 size;

    vec3 offsets(
        position.x < node->minExtent().x ? node->minExtent().x - position.x : position.x - node->maxExtent().x,
        position.y < node->minExtent().y ? node->minExtent().y - position.y : position.y - node->maxExtent().y,
        position.z < node->minExtent().z ? node->minExtent().z - position.z : position.z - node->maxExtent().z
    );

    if (offsets.x > offsets.y && offsets.x > offsets.z)
    {
        size = vec2(node->maxExtent().y - node->minExtent().y, node->maxExtent().z - node->minExtent().z);

        nearest = vec3(
            position.x < node->minExtent().x ? node->minExtent().x : node->maxExtent().x,
            (node->minExtent().y + node->maxExtent().y) / 2,
            (node->minExtent().z + node->maxExtent().z) / 2
        );
    }
    else if (offsets.y > offsets.x && offsets.y > offsets.z)
    {
        size = vec2(node->maxExtent().x - node->minExtent().x, node->maxExtent().z - node->minExtent().z);

        nearest = vec3(
            (node->minExtent().x + node->maxExtent().x) / 2,
            position.y < node->minExtent().y ? node->minExtent().y : node->maxExtent().y,
            (node->minExtent().z + node->maxExtent().z) / 2
        );
    }
    else
    {
        size = vec2(node->maxExtent().x - node->minExtent().x, node->maxExtent().y - node->minExtent().y);

        nearest = vec3(
            (node->minExtent().x + node->maxExtent().x) / 2,
            (node->minExtent().y + node->maxExtent().y) / 2,
            position.z < node->minExtent().z ? node->minExtent().z : node->maxExtent().z
        );
    }

    this->position = nearest;

    f64 distance = position.distance(nearest);

    vec2 angularSize(
        2 * atan2(size.x, 2 * distance),
        2 * atan2(size.y, 2 * distance)
    );

    f64 fractionOfTotalPower = (angularSize.x * angularSize.y) / sq(tau);

    this->distance = distance;
    this->strength = fractionOfTotalPower;
}

static vector<ReverbEffect> findReverbSurfaces(const SpaceGraphNode* node, const vec3& position, const vec3& direction)
{
    if (node->isLeaf())
    {
        return { ReverbEffect(node, position) };
    }

    vector<ReverbEffect> effects;

    for (u32 z = direction.z >= 0 ? 0 : 1; z < spaceGraphDivisionCount; direction.z >= 0 ? z++ : z--)
    {
        for (u32 y = direction.y >= 0 ? 0 : 1; y < spaceGraphDivisionCount; direction.y >= 0 ? y++ : y--)
        {
            for (u32 x = direction.x >= 0 ? 0 : 1; x < spaceGraphDivisionCount; direction.x >= 0 ? x++ : x--)
            {
                uvec3 index(x, y, z);

                const SpaceGraphNode* child = node->at(index);

                if (!child)
                {
                    continue;
                }

                vector<ReverbEffect> newEffects = findReverbSurfaces(child, position, direction);

                if (newEffects.empty())
                {
                    continue;
                }

                effects = join(effects, newEffects);
            }
        }
    }

    return effects;
}

vector<ReverbEffect> findReverbSurfaces(const SpaceGraphNode* node, const uvec3& index, const vec3& position)
{
    vector<ReverbEffect> effects;

    vec3 direction;

    uvec3 a = index;
    direction = a.x == 0 ? rightDir : leftDir;
    a.x = a.x == 0 ? 1 : 0;

    if (node->at(a))
    {
        effects = join(effects, findReverbSurfaces(node->at(a), position, direction));
    }

    uvec3 b = index;
    direction = b.y == 0 ? forwardDir : backDir;
    b.y = b.y == 0 ? 1 : 0;

    if (node->at(b))
    {
        effects = join(effects, findReverbSurfaces(node->at(b), position, direction));
    }

    uvec3 c = index;
    direction = c.z == 0 ? upDir : downDir;
    c.z = c.z == 0 ? 1 : 0;

    if (node->at(c))
    {
        effects = join(effects, findReverbSurfaces(node->at(c), position, direction));
    }

    return effects;
}
