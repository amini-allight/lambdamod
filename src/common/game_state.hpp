/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "user.hpp"
#include "team.hpp"
#include "world.hpp"
#include "timer.hpp"
#include "repeater.hpp"
#include "script_context.hpp"

class GameState
{
public:
    GameState();
    GameState(const GameState& rhs);
    GameState(GameState&& rhs) noexcept;
    ~GameState();

    GameState& operator=(const GameState& rhs);
    GameState& operator=(GameState&& rhs) noexcept;

    tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> compare(const GameState& previous) const;
    vector<NetworkEvent> compareUser(const GameState& previous, const User* user) const;
    void updateDynamics();

#if defined(LMOD_SERVER)
    void push(UserID userID, const NetworkEvent& event);
#elif defined(LMOD_CLIENT)
    bool push(const NetworkEvent& event);
#endif

    void step();
    vector<NetworkEvent> initial() const;
    vector<NetworkEvent> initialUser(const User* user) const;

#if defined(LMOD_SERVER)
    void executeCommand(User* user, const string& command);
#endif
    void clearSessionUserReferences(User* user);
    void clearUserReferences(User* user);
    void setPose(
        EntityID id,
        const map<VRDevice, mat4>& anchorMapping,
        const map<VRDevice, mat4>& pose
    );
    void pushInput(EntityID id, const string& hook, const Input& input);

    void enable();
    void disable();

    void addUser(const User& user);
    void removeUser(User* user);
#if defined(LMOD_SERVER)
    void kickUser(User* kicker, User* user);
#endif
    UserID nextUserID();
    const map<UserID, User>& users() const;
    User* getUser(UserID id);
    const User* getUser(UserID id) const;
    User* getUserByName(const string& name);
    const User* getUserByName(const string& name) const;

    void addTeam(const string& name);
    void removeTeam(const string& name);
    void updateTeam(const string& name, const set<UserID>& userIDs);
    const map<string, Team>& teams() const;
    Team* getTeam(const string& name);
    void updateUserTeams(User* user, const set<string>& teams);

    void splashMessage(const string& text);
    const string& splashMessage() const;

    void setTimeMultiplier(f64 multiplier);
    void setTimeMultiplier(const User* user, f64 multiplier);
    f64 timeMultiplier() const;

    void lockAttachments(const User* user);
    void unlockAttachments(const User* user);
    void lockAttachments();
    void unlockAttachments();
    bool attachmentsLocked() const;

    void brake(const User* user);
    void unbrake(const User* user);
    bool braking() const;
    UserID brakingID() const;
    f64 brakeProgress() const;

#if defined(LMOD_SERVER)
    void ping(User* user);
    void attach(User* user, Entity* entity);
    void detach(User* user);
    void magnitude(User* user, u32 value);

    void setMagnitudeLimit(const User* user, u32 limit);
#endif
    void magnitudeLimit(u32 limit);
    u32 magnitudeLimit() const;

    u32 magnitude() const;

    void addWorld(const string& name);
    void addWorld(const string& name, const World& world);
    void removeWorld(const string& name);
    World* getWorld(const string& name) const;
    const map<string, World*>& worlds() const;
    void traverse(const function<void(Entity*)>& behavior);
    void traverse(const function<void(const Entity*)>& behavior) const;
    bool partiallyTraverse(const function<bool(Entity*)>& behavior);
    bool partiallyTraverse(const function<bool(const Entity*)>& behavior) const;

    TimerID addTimer(f64 delay, const Lambda& callback, User* user);
    void removeTimer(TimerID id);
    const map<TimerID, Timer>& timers() const;

    RepeaterID addRepeater(const Lambda& callback, User* user);
    void removeRepeater(RepeaterID id);
    const map<RepeaterID, Repeater>& repeaters() const;

    TextID nextTextID();

    ScriptContext* context();

    EntityID peekNextEntityID();
    EntityID nextEntityID();
    Entity* get(EntityID id) const;

    f64 stepInterval() const;

#if defined(LMOD_SERVER)
    void sendChatMessage(User* user, const string& message);
    void sendDirectChatMessage(User* user, User* targetUser, const string& message);
    void sendTeamChatMessage(User* user, Team* team, const string& message);

    void scriptPrint(const string& text);
    void log(const string& text, const vector<string>& parameters);
    void adminLog(const string& text, const vector<string>& parameters);
#elif defined(LMOD_CLIENT)
    void log(const string& text, const vector<string>& parameters);
#endif

    bool canDeleteWorld(const string& name) const;

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    UserID _nextUserID;
    map<UserID, User> _users;
    map<string, Team> _teams;

    string _splashMessage;
    f64 _timeMultiplier;
    bool _attachmentsLocked;
    bool _braking;
    UserID _brakingID;
    f64 brakeElapsed;
    u32 _magnitudeLimit;

    EntityID _nextEntityID;
    map<string, World*> _worlds;

    TimerID nextTimerID;
    map<TimerID, Timer> _timers;

    RepeaterID nextRepeaterID;
    map<RepeaterID, Repeater> _repeaters;

    TextID _nextTextID;

    ScriptContext* _context;

#if defined(LMOD_SERVER)
    void handleUserEvent(User* user, const UserEvent& event);
    void handleTeamEvent(User* user, const TeamEvent& event);
    void handleDocumentEvent(User* user, const DocumentEvent& event);
    void handleGameEvent(User* user, const GameEvent& event);
    void handleWorldEvent(User* user, const WorldEvent& event);
    void handleInputEvent(User* user, const InputEvent& event);
    void handleGripEvent(User* user, const GripEvent& event);
    void handleCommandEvent(User* user, const CommandEvent& event);
    void handlePoseEvent(User* user, const PoseEvent& event);
    void handleTextRequestEvent(User* user, const TextRequestEvent& event);
    void handleTextResponseEvent(User* user, const TextResponseEvent& event);
    void handleTetherEvent(User* user, const TetherEvent& event);
    void handleConfinementEvent(User* user, const ConfinementEvent& event);
    void handleGotoEvent(User* user, const GotoEvent& event);
    void handleFadeEvent(User* user, const FadeEvent& event);
    void handleWaitEvent(User* user, const WaitEvent& event);
    void handleVoiceChannelEvent(User* user, const VoiceChannelEvent& event);

    void handleUserRemove(User* user, const UserEvent& event);
    void handleUserKick(User* user, const UserEvent& event);
    void handleUserShiftForced(User* user, const UserEvent& event);
    void handleUserMoveForced(User* user, const UserEvent& event);
    void handleUserShift(User* user, const UserEvent& event);
    void handleUserMoveViewer(User* user, const UserEvent& event);
    void handleUserMoveRoom(User* user, const UserEvent& event);
    void handleUserMoveDevices(User* user, const UserEvent& event);
    void handleUserAngle(User* user, const UserEvent& event);
    void handleUserAspect(User* user, const UserEvent& event);
    void handleUserBounds(User* user, const UserEvent& event);
    void handleUserColor(User* user, const UserEvent& event);
    void handleUserTeams(User* user, const UserEvent& event);
    void handleUserAnchor(User* user, const UserEvent& event);
    void handleUserUnanchor(User* user, const UserEvent& event);
    void handleUserRoomOffset(User* user, const UserEvent& event);
    void handleUserMagnitude(User* user, const UserEvent& event);
    void handleUserAdvantage(User* user, const UserEvent& event);
    void handleUserDoom(User* user, const UserEvent& event);

    void handleDocumentAdd(User* user, const DocumentEvent& event);
    void handleDocumentRemove(User* user, const DocumentEvent& event);
    void handleDocumentUpdate(User* user, const DocumentEvent& event);

    void handleTeamAdd(User* user, const TeamEvent& event);
    void handleTeamRemove(User* user, const TeamEvent& event);
    void handleTeamUpdate(User* user, const TeamEvent& event);

    void handleAttach(User* user, const GameEvent& event);
    void handleDetach(User* user, const GameEvent& event);
    void handleSelect(User* user, const GameEvent& event);
    void handleDeselect(User* user, const GameEvent& event);
    void handleSplashMessage(User* user, const GameEvent& event);
    void handleTimeMultiplier(User* user, const GameEvent& event);
    void handleLockAttachments(User* user, const GameEvent& event);
    void handleUnlockAttachments(User* user, const GameEvent& event);
    void handlePing(User* user, const GameEvent& event);
    void handleBrake(User* user, const GameEvent& event);
    void handleUnbrake(User* user, const GameEvent& event);
    void handleMagnitudeLimit(User* user, const GameEvent& event);
    void handleNextEntityID(User* user, const GameEvent& event);
    void handleNextTimerID(User* user, const GameEvent& event);
    void handleNextRepeaterID(User* user, const GameEvent& event);

    void handleWorldAdd(User* user, const WorldEvent& event);
    void handleWorldRemove(User* user, const WorldEvent& event);
    void pushWorldEvent(User* user, const NetworkEvent& event);
#elif defined(LMOD_CLIENT)
    bool handleUserEvent(const UserEvent& event);
    void handleTeamEvent(const TeamEvent& event);
    bool handleGameEvent(const GameEvent& event);
    void handleWorldEvent(const WorldEvent& event);

    bool handleUserAdd(const UserEvent& event);
    bool handleUserRemove(const UserEvent& event);
    bool handleUserShift(const UserEvent& event);
    bool handleUserMoveViewer(const UserEvent& event);
    bool handleUserMoveRoom(const UserEvent& event);
    bool handleUserMoveDevices(const UserEvent& event);
    void handleUserAdmin(const UserEvent& event);
    bool handleUserAngle(const UserEvent& event);
    bool handleUserAspect(const UserEvent& event);
    bool handleUserBounds(const UserEvent& event);
    void handleUserColor(const UserEvent& event);
    void handleUserTeams(const UserEvent& event);
    bool handleUserJoin(const UserEvent& event);
    bool handleUserLeave(const UserEvent& event);
    void handleUserAttach(const UserEvent& event);
    void handleUserDetach(const UserEvent& event);
    void handleUserAnchor(const UserEvent& event);
    void handleUserUnanchor(const UserEvent& event);
    void handleUserRoomOffset(const UserEvent& event);
    void handleUserPing(const UserEvent& event);
    void handleUserMagnitude(const UserEvent& event);
    void handleUserAdvantage(const UserEvent& event);
    void handleUserDoom(const UserEvent& event);
    void handleUserTether(const UserEvent& event);
    void handleUserConfinement(const UserEvent& event);
    void handleUserFade(const UserEvent& event);
    void handleUserWait(const UserEvent& event);
    void handleUserSignal(const UserEvent& event);
    void handleUserTitlecard(const UserEvent& event);
    void handleUserTexts(const UserEvent& event);
    void handleUserKnowledge(const UserEvent& event);

    void handleTeamAdd(const TeamEvent& event);
    void handleTeamRemove(const TeamEvent& event);
    void handleTeamUpdate(const TeamEvent& event);

    void handleSplashMessage(const GameEvent& event);
    void handleTimeMultiplier(const GameEvent& event);
    void handleLockAttachments(const GameEvent& event);
    void handleUnlockAttachments(const GameEvent& event);
    bool handlePing(const GameEvent& event);
    bool handleBrake(const GameEvent& event);
    void handleUnbrake(const GameEvent& event);
    void handleMagnitudeLimit(const GameEvent& event);
    void handleNextEntityID(const GameEvent& event);
    void handleNextTimerID(const GameEvent& event);
    void handleNextRepeaterID(const GameEvent& event);

    void handleWorldAdd(const WorldEvent& event);
    void handleWorldRemove(const WorldEvent& event);
    void pushWorldEvent(const NetworkEvent& event);
#endif

    void copy(const GameState& rhs);

    void vrDrop(const User* user);
#if defined(LMOD_SERVER)
    void checkMagnitude(u32 oldMagnitude);
#endif
};
