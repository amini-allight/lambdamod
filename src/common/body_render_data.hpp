/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_CLIENT
#pragma once

#include "types.hpp"

// Alpha-capable materials like Body_Render_Material_Texture should always follow all other materials
enum BodyRenderMaterialType : u8
{
    Body_Render_Material_Vertex,
    Body_Render_Material_Texture,
    Body_Render_Material_Area,
    Body_Render_Material_Terrain,
    Body_Render_Material_Rope,
    Body_Render_Material_Shadeless_Vertex,
    Body_Render_Material_Shadeless_Texture,
    Body_Render_Material_Shadeless_Area,
    Body_Render_Material_Shadeless_Terrain,
    Body_Render_Material_Shadeless_Rope
};

bool isMaterialTransparent(BodyRenderMaterialType type);
bool canMaterialMerge(BodyRenderMaterialType type);

struct BodyRenderMaterialTextureData
{
    u32 width;
    u32 height;
    vector<u8> data;
};

struct BodyRenderMaterialData
{
    vector<u32> indices;
    vector<f32> vertexData;
    size_t vertexCount;
    vector<f32> uniformData;
    vector<BodyRenderMaterialTextureData> textures;
    // For transparent sorting, ignored on non-transparents
    vec3 position;

    void add(const BodyRenderMaterialData& other);
};

struct BodyRenderLight
{
    fmat4 transform;
    fvec4 color;
    f32 angle;
};

enum BodyRenderOcclusionPrimitiveType : u8
{
    Body_Render_Occlusion_Primitive_Ellipse,
    Body_Render_Occlusion_Primitive_Equilateral_Triangle,
    Body_Render_Occlusion_Primitive_Isosceles_Triangle,
    Body_Render_Occlusion_Primitive_Right_Angle_Triangle,
    Body_Render_Occlusion_Primitive_Rectangle,
    Body_Render_Occlusion_Primitive_Pentagon,
    Body_Render_Occlusion_Primitive_Hexagon,
    Body_Render_Occlusion_Primitive_Heptagon,
    Body_Render_Occlusion_Primitive_Octagon,
    Body_Render_Occlusion_Primitive_Cuboid,
    Body_Render_Occlusion_Primitive_Sphere,
    Body_Render_Occlusion_Primitive_Cylinder,
    Body_Render_Occlusion_Primitive_Cone,
    Body_Render_Occlusion_Primitive_Capsule,
    Body_Render_Occlusion_Primitive_Pyramid,
    Body_Render_Occlusion_Primitive_Tetrahedron,
    Body_Render_Occlusion_Primitive_Octahedron,
    Body_Render_Occlusion_Primitive_Dodecahedron,
    Body_Render_Occlusion_Primitive_Icosahedron
};

struct BodyRenderOcclusionPrimitive
{
    fmat4 transform;
    fvec3 dimensions;
    BodyRenderOcclusionPrimitiveType type;
};

struct BodyRenderData
{
    vector<tuple<BodyRenderMaterialType, BodyRenderMaterialData>> materials;
    vector<BodyRenderLight> lights;
    vector<BodyRenderOcclusionPrimitive> occlusionPrimitives;

    void add(const BodyRenderData& other);
};
#endif
