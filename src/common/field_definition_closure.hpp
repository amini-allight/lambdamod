/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script_context.hpp"
#include "scope_closure.hpp"

class Entity;

class FieldDefinitionClosure
{
public:
    FieldDefinitionClosure(ScriptContext* context, const function<void(const string&, const Value&)>& set);
    FieldDefinitionClosure(const FieldDefinitionClosure& rhs) = delete;
    FieldDefinitionClosure(FieldDefinitionClosure&& rhs) = delete;
    ~FieldDefinitionClosure();

    FieldDefinitionClosure& operator=(const FieldDefinitionClosure& rhs) = delete;
    FieldDefinitionClosure& operator=(FieldDefinitionClosure&& rhs) = delete;

private:
    ScopeClosure closure;
};
