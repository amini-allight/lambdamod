/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "space_graph_generation_context.hpp"

class SpaceGraphNode
{
public:
    SpaceGraphNode(SpaceGraphGenerationContext ctx);
    SpaceGraphNode(const SpaceGraphNode& rhs) = delete;
    SpaceGraphNode(SpaceGraphNode&& rhs) = delete;
    ~SpaceGraphNode();

    SpaceGraphNode& operator=(const SpaceGraphNode& rhs) = delete;
    SpaceGraphNode& operator=(SpaceGraphNode&& rhs) = delete;

    vec3 minExtent() const;
    vec3 maxExtent() const;
    vec3 size() const;
    const vec3& innerExtent() const;
    const vec3& outerExtent() const;
    u32 depth() const;
    vec3 center() const;
    f64 boundingRadius() const;

    bool isLeaf() const;
    bool isInfinite() const;

    SpaceGraphNode* at(const uvec3& index);
    const SpaceGraphNode* at(const uvec3& index) const;

    void traverse(const function<void(const uvec3&, SpaceGraphNode*)>& behavior);
    void traverseAscending(const vec3& position, const function<void(const uvec3&, SpaceGraphNode*)>& behavior);
    void traverse(const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const;
    void traverseAscending(const vec3& position, const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const;

    void print(size_t indent = 0);

private:
    vec3 _innerExtent;
    vec3 _outerExtent;
    u32 _depth;
    vec3 _center;
    SpaceGraphNode* children[spaceGraphDivisionCount][spaceGraphDivisionCount][spaceGraphDivisionCount];

    vector<SpaceGraphGenerationEntity> selectWithin(
        const vector<SpaceGraphGenerationEntity>& entities,
        const vec3& innerExtent,
        const vec3& outerExtent
    ) const;
};
