/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "vr_device.hpp"
#include "message.hpp"
#include "input_types.hpp"
#include "hud_element.hpp"
#include "body_part.hpp"
#include "sample_part.hpp"
#include "sample_playback.hpp"
#include "action_part.hpp"
#include "action_playback.hpp"
#include "script_types.hpp"
#include "text.hpp"
#include "script_value_permissions.hpp"
#include "sky_parameters.hpp"
#include "atmosphere_parameters.hpp"

struct PingEvent
{
    PingEvent()
    {
        ping = true;
    }

    PingEvent(bool ping)
    {
        this->ping = ping;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            ping
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        ping = get<0>(payload);
    }

    typedef msgpack::type::tuple<bool> layout;

    bool ping;
};

struct JoinEvent
{
    JoinEvent()
    {
        vr = false;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            serverPassword,
            name,
            password,
            version,
            vr
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        serverPassword = get<0>(payload);
        name = get<1>(payload);
        password = get<2>(payload);
        version = get<3>(payload);
        vr = get<4>(payload);
    }

    typedef msgpack::type::tuple<string, string, string, Version, bool> layout;

    string serverPassword;
    string name;
    string password;
    Version version;
    bool vr;
};

struct LeaveEvent
{
    LeaveEvent()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {};

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);
    }

    typedef msgpack::type::tuple<> layout;
};

struct WelcomeEvent
{
    WelcomeEvent()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            splashMessage,
            remoteIndex
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        splashMessage = get<1>(payload);
        remoteIndex = get<2>(payload);
    }

    typedef msgpack::type::tuple<UserID, string, u64> layout;

    UserID id;
    string splashMessage;
    u64 remoteIndex;
};

struct KickEvent
{
    KickEvent()
    {
        reason = 0;
    }

    KickEvent(u32 reason)
        : KickEvent()
    {
        this->reason = reason;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            reason
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        reason = get<0>(payload);
    }

    typedef msgpack::type::tuple<u32> layout;

    u32 reason;
};

enum UserEventType : u8
{
    User_Event_Add,
    User_Event_Remove,
    User_Event_Kick,
    User_Event_Shift_Forced,
    User_Event_Move_Forced,
    User_Event_Shift,
    User_Event_Move_Viewer,
    User_Event_Move_Room,
    User_Event_Move_Devices,
    User_Event_Admin,
    User_Event_Angle,
    User_Event_Aspect,
    User_Event_Bounds,
    User_Event_Color,
    User_Event_Teams,
    User_Event_Join,
    User_Event_Leave,
    User_Event_Attach,
    User_Event_Detach,
    User_Event_Anchor,
    User_Event_Unanchor,
    User_Event_Room_Offset,
    User_Event_Ping,
    User_Event_Magnitude,
    User_Event_Advantage,
    User_Event_Doom,
    User_Event_Tether,
    User_Event_Confinement,
    User_Event_Fade,
    User_Event_Wait,
    User_Event_Signal,
    User_Event_Titlecard,
    User_Event_Texts,
    User_Event_Knowledge
};

struct UserEvent
{
    UserEvent()
    {
        type = User_Event_Add;
        u = 0;
        i = 0;
        b = false;
        f = 0;
    }

    UserEvent(UserEventType type)
        : UserEvent()
    {
        this->type = type;
    }

    UserEvent(UserEventType type, UserID id)
        : UserEvent()
    {
        this->type = type;
        this->id = id;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const string& s
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->s = s;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const string& s,
        const vec3& color,
        bool b
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->s = s;
        this->color = color;
        this->b = b;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const mat4& transform
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->transform = transform;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const vec3& color
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->color = color;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        u64 u
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->u = u;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        i64 i
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->i = i;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        bool b
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->b = b;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        f64 f
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->f = f;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const set<string>& teams
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->teams = teams;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const map<VRDevice, mat4>& mapping
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->mapping = mapping;
    }

    UserEvent(
        UserEventType type,
        UserID id,
        const vec2& bounds
    )
        : UserEvent()
    {
        this->type = type;
        this->id = id;
        this->bounds = bounds;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        map<u8, mat4> mapping;

        for (const auto& [ device, transform ] : this->mapping)
        {
            mapping.insert({ static_cast<u8>(device), transform });
        }

        layout payload = {
            static_cast<u8>(type),
            id,
            color,
            transform,
            u,
            i,
            b,
            f,
            s,
            teams,
            mapping,
            bounds,
            userIDs,
            textRequests
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<UserEventType>(get<0>(payload));
        id = get<1>(payload);
        color = get<2>(payload);
        transform = get<3>(payload);
        u = get<4>(payload);
        i = get<5>(payload);
        b = get<6>(payload);
        f = get<7>(payload);
        s = get<8>(payload);
        teams = get<9>(payload);

        for (const auto& [ device, transform ] : get<10>(payload))
        {
            this->mapping.insert({ static_cast<VRDevice>(device), transform });
        }

        bounds = get<11>(payload);
        userIDs = get<12>(payload);
        textRequests = get<13>(payload);
    }

    typedef msgpack::type::tuple<u8, UserID, vec3, mat4, u64, i64, bool, f64, string, set<string>, map<u8, mat4>, vec2, set<UserID>, vector<TextRequest>> layout;

    UserEventType type;
    UserID id;
    vec3 color;
    mat4 transform;
    u64 u;
    i64 i;
    bool b;
    f64 f;
    string s;
    set<string> teams;
    map<VRDevice, mat4> mapping;
    vec2 bounds;
    set<UserID> userIDs;
    vector<TextRequest> textRequests;
};

enum TeamEventType : u8
{
    Team_Event_Add,
    Team_Event_Remove,
    Team_Event_Update
};

struct TeamEvent
{
    TeamEvent()
    {
        type = Team_Event_Add;
    }

    TeamEvent(TeamEventType type, const string& name)
    {
        this->type = type;
        this->name = name;
    }

    TeamEvent(TeamEventType type, const string& name, const set<UserID>& userIDs)
    {
        this->type = type;
        this->name = name;
        this->userIDs = userIDs;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            name,
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<TeamEventType>(get<0>(payload));
        name = get<1>(payload);
        userIDs = get<2>(payload);
    }

    typedef msgpack::type::tuple<u8, string, set<UserID>> layout;

    TeamEventType type;
    string name;
    set<UserID> userIDs;
};

enum CheckpointEventType : u8
{
    Checkpoint_Event_Add,
    Checkpoint_Event_Remove,
    Checkpoint_Event_Revert
};

struct CheckpointEvent
{
    CheckpointEvent()
    {
        type = Checkpoint_Event_Add;
    }

    CheckpointEvent(CheckpointEventType type, const string& name)
        : type(type)
        , name(name)
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            name
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<CheckpointEventType>(get<0>(payload));
        name = get<1>(payload);
    }

    typedef msgpack::type::tuple<u8, string> layout;

    CheckpointEventType type;
    string name;
};

enum DocumentEventType : u8
{
    Document_Event_Add,
    Document_Event_Remove,
    Document_Event_Update
};

struct DocumentEvent
{
    DocumentEvent()
    {
        type = Document_Event_Add;
    }

    DocumentEvent(DocumentEventType type, const string& name)
    {
        this->type = type;
        this->name = name;
    }

    DocumentEvent(DocumentEventType type, const string& name, const string& text)
    {
        this->type = type;
        this->name = name;
        this->text = text;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            name,
            text
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<DocumentEventType>(get<0>(payload));
        name = get<1>(payload);
        text = get<2>(payload);
    }

    typedef msgpack::type::tuple<u8, string, string> layout;

    DocumentEventType type;
    string name;
    string text;
};

enum GameEventType : u8
{
    Game_Event_Attach,
    Game_Event_Detach,
    Game_Event_Select,
    Game_Event_Deselect,
    Game_Event_Splash_Message,
    Game_Event_Time_Multiplier,
    Game_Event_Lock_Attachments,
    Game_Event_Unlock_Attachments,
    Game_Event_Ping,
    Game_Event_Brake,
    Game_Event_Unbrake,
    Game_Event_Magnitude_Limit,
    Game_Event_Next_Entity_ID,
    Game_Event_Next_Timer_ID,
    Game_Event_Next_Repeater_ID
};

struct GameEvent
{
    GameEvent()
    {
        type = Game_Event_Attach;
        i = 0;
        u = 0;
        f = 0;
        b = false;
    }

    GameEvent(GameEventType type)
        : GameEvent()
    {
        this->type = type;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            s,
            i,
            u,
            f,
            b
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<GameEventType>(get<0>(payload));
        s = get<1>(payload);
        i = get<2>(payload);
        u = get<3>(payload);
        f = get<4>(payload);
        b = get<5>(payload);
    }

    typedef msgpack::type::tuple<u8, string, i64, u64, f64, bool> layout;

    GameEventType type;
    string s;
    i64 i;
    u64 u;
    f64 f;
    bool b;
};

enum WorldEventType : u8
{
    World_Event_Add,
    World_Event_Remove,
    World_Event_Show,
    World_Event_Hide,
    World_Event_Speed_Of_Sound,
    World_Event_Gravity,
    World_Event_Flow_Velocity,
    World_Event_Density,
    World_Event_Up,
    World_Event_Fog_Distance,
    World_Event_Undo,
    World_Event_Redo
};

struct WorldEvent
{
    WorldEvent()
    {
        type = World_Event_Add;
    }

    WorldEvent(WorldEventType type)
        : WorldEvent()
    {
        this->type = type;
    }

    WorldEvent(WorldEventType type, const string& name)
        : WorldEvent()
    {
        this->type = type;
        this->name = name;
    }

    WorldEvent(WorldEventType type, const string& name, const vector<f64>& data)
        : WorldEvent()
    {
        this->type = type;
        this->name = name;
        this->data = data;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            name,
            data
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<WorldEventType>(get<0>(payload));
        name = get<1>(payload);
        data = get<2>(payload);
    }

    typedef msgpack::type::tuple<u8, string, vector<f64>> layout;

    WorldEventType type;
    string name;
    vector<f64> data;
};

struct WorldSkyEvent
{
    WorldSkyEvent()
    {

    }

    WorldSkyEvent(const string& name, const SkyParameters& sky)
        : name(name)
        , sky(sky)
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            name,
            sky
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        name = get<0>(payload);
        sky = get<1>(payload);
    }

    typedef msgpack::type::tuple<string, SkyParameters> layout;

    string name;
    SkyParameters sky;
};

struct WorldAtmosphereEvent
{
    WorldAtmosphereEvent()
    {

    }

    WorldAtmosphereEvent(const string& name, const AtmosphereParameters& atmosphere)
        : name(name)
        , atmosphere(atmosphere)
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            name,
            atmosphere
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        name = get<0>(payload);
        atmosphere = get<1>(payload);
    }

    typedef msgpack::type::tuple<string, AtmosphereParameters> layout;

    string name;
    AtmosphereParameters atmosphere;
};

enum EntityEventType : u8
{
    Entity_Event_Add,
    Entity_Event_Remove,

    Entity_Event_Parent_Part_ID,
    Entity_Event_Parent_Part_Sub_ID,

    Entity_Event_Name,

    Entity_Event_Activate,
    Entity_Event_Deactivate,

    Entity_Event_Transform,
    Entity_Event_Linear_Velocity,
    Entity_Event_Angular_Velocity,

    Entity_Event_Owner_ID,
    Entity_Event_Attached_User_ID,
    Entity_Event_Selecting_User_ID,

    Entity_Event_Mass,
    Entity_Event_Drag,
    Entity_Event_Buoyancy,
    Entity_Event_Lift,
    Entity_Event_Magnetism,
    Entity_Event_Friction,
    Entity_Event_Restitution,

    Entity_Event_Start,
    Entity_Event_Stop,
    Entity_Event_Transfer,
    Entity_Event_Share,
    Entity_Event_Unshare,
    Entity_Event_Take,
    Entity_Event_Discard,
    Entity_Event_Hostify,
    Entity_Event_Unhostify,
    Entity_Event_Show,
    Entity_Event_Hide,
    Entity_Event_Physicalize,
    Entity_Event_Etherealize,
    Entity_Event_Visualize,
    Entity_Event_Unvisualize,
    Entity_Event_Audialize,
    Entity_Event_Unaudialize,

    Entity_Event_Field_Of_View,

    Entity_Event_Lock_Mouse,
    Entity_Event_Unlock_Mouse,

    Entity_Event_Voice_Volume,
    Entity_Event_Tint_Color,
    Entity_Event_Tint_Strength
};

struct EntityEvent
{
    EntityEvent()
    {
        type = Entity_Event_Add;
        f = 0;
        i = 0;
        u = 0;
        transformed = false;
        mergeUndo = false;
    }

    EntityEvent(EntityEventType type, const string& world, EntityID id)
        : EntityEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            s,
            f,
            i,
            u,
            v3,
            m4,
            transformed,
            mergeUndo
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        s = get<3>(payload);
        f = get<4>(payload);
        i = get<5>(payload);
        u = get<6>(payload);
        v3 = get<7>(payload);
        m4 = get<8>(payload);
        transformed = get<9>(payload);
        mergeUndo = get<10>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, string, f64, i64, u64, vec3, mat4, bool, bool> layout;

    EntityEventType type;
    string world;
    EntityID id;
    string s;
    f64 f;
    i64 i;
    u64 u;
    vec3 v3;
    mat4 m4;
    bool transformed;
    bool mergeUndo;
};

enum EntityBodyEventType : u8
{
    Entity_Body_Event_Next_Part_ID,
    Entity_Body_Event_Add_Part,
    Entity_Body_Event_Remove_Part,
    Entity_Body_Event_Update_Part
};

struct EntityBodyEvent
{
    EntityBodyEvent()
    {
        type = Entity_Body_Event_Next_Part_ID;
        transformed = false;
        mergeUndo = false;
    }

    EntityBodyEvent(EntityBodyEventType type, EntityID id)
        : EntityBodyEvent()
    {
        this->type = type;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            parentID,
            partID,
            part,
            transformed,
            mergeUndo
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityBodyEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        parentID = get<3>(payload);
        partID = get<4>(payload);
        part = get<5>(payload);
        transformed = get<6>(payload);
        mergeUndo = get<7>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, BodyPartID, BodyPartID, BodyPart, bool, bool> layout;

    EntityBodyEventType type;
    string world;
    EntityID id;
    BodyPartID parentID;
    BodyPartID partID;
    BodyPart part;
    bool transformed;
    bool mergeUndo;
};

enum EntitySampleEventType : u8
{
    Entity_Sample_Event_Add,
    Entity_Sample_Event_Remove,
    Entity_Sample_Event_Next_Part_ID,
    Entity_Sample_Event_Add_Part,
    Entity_Sample_Event_Remove_Part,
    Entity_Sample_Event_Update_Part
};

struct EntitySampleEvent
{
    EntitySampleEvent()
    {
        type = Entity_Sample_Event_Add;
        transformed = false;
        mergeUndo = false;
    }

    EntitySampleEvent(EntitySampleEventType type, const string& world, EntityID id)
        : EntitySampleEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            name,
            partID,
            part,
            transformed,
            mergeUndo
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntitySampleEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        name = get<3>(payload);
        partID = get<4>(payload);
        part = get<5>(payload);
        transformed = get<6>(payload);
        mergeUndo = get<7>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, string, SamplePartID, SamplePart, bool, bool> layout;

    EntitySampleEventType type;
    string world;
    EntityID id;
    string name;
    SamplePartID partID;
    SamplePart part;
    bool transformed;
    bool mergeUndo;
};

enum EntitySamplePlaybackEventType : u8
{
    Entity_Sample_Playback_Event_Add,
    Entity_Sample_Playback_Event_Remove,
    Entity_Sample_Playback_Event_Resume,
    Entity_Sample_Playback_Event_Pause,
    Entity_Sample_Playback_Event_Seek
};

struct EntitySamplePlaybackEvent
{
    EntitySamplePlaybackEvent()
    {
        type = Entity_Sample_Playback_Event_Add;
        position = 0;
        transformed = true;
    }

    EntitySamplePlaybackEvent(EntitySamplePlaybackEventType type, const string& world, EntityID id)
        : EntitySamplePlaybackEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            playbackID,
            playback,
            position,
            transformed
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntitySamplePlaybackEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        playbackID = get<3>(payload);
        playback = get<4>(payload);
        position = get<5>(payload);
        transformed = get<6>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, SamplePlaybackID, SamplePlayback, u64, bool> layout;

    EntitySamplePlaybackEventType type;
    string world;
    EntityID id;
    SamplePlaybackID playbackID;
    SamplePlayback playback;
    u64 position;
    bool transformed;
};

enum EntityActionEventType : u8
{
    Entity_Action_Event_Add,
    Entity_Action_Event_Remove,
    Entity_Action_Event_Next_Part_ID,
    Entity_Action_Event_Add_Part,
    Entity_Action_Event_Remove_Part,
    Entity_Action_Event_Update_Part
};

struct EntityActionEvent
{
    EntityActionEvent()
    {
        type = Entity_Action_Event_Add;
        transformed = false;
        mergeUndo = false;
    }

    EntityActionEvent(EntityActionEventType type, const string& world, EntityID id)
        : EntityActionEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            name,
            partID,
            part,
            transformed,
            mergeUndo
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityActionEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        name = get<3>(payload);
        partID = get<4>(payload);
        part = get<5>(payload);
        transformed = get<6>(payload);
        mergeUndo = get<7>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, string, ActionPartID, ActionPart, bool, bool> layout;

    EntityActionEventType type;
    string world;
    EntityID id;
    string name;
    ActionPartID partID;
    ActionPart part;
    bool transformed;
    bool mergeUndo;
};

enum EntityActionPlaybackEventType : u8
{
    Entity_Action_Playback_Event_Add,
    Entity_Action_Playback_Event_Remove,
    Entity_Action_Playback_Event_Resume,
    Entity_Action_Playback_Event_Pause,
    Entity_Action_Playback_Event_Seek
};

struct EntityActionPlaybackEvent
{
    EntityActionPlaybackEvent()
    {
        type = Entity_Action_Playback_Event_Add;
        position = 0;
        transformed = true;
    }

    EntityActionPlaybackEvent(EntityActionPlaybackEventType type, const string& world, EntityID id)
        : EntityActionPlaybackEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            playbackID,
            playback,
            position,
            transformed
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityActionPlaybackEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        playbackID = get<3>(payload);
        playback = get<4>(payload);
        position = get<5>(payload);
        transformed = get<6>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, ActionPlaybackID, ActionPlayback, u64, bool> layout;

    EntityActionPlaybackEventType type;
    string world;
    EntityID id;
    ActionPlaybackID playbackID;
    ActionPlayback playback;
    u64 position;
    bool transformed;
};

enum EntityInputBindingEventType : u8
{
    Entity_Input_Binding_Event_Add,
    Entity_Input_Binding_Event_Remove,
    Entity_Input_Binding_Event_Update
};

struct EntityInputBindingEvent
{
    EntityInputBindingEvent()
    {
        type = Entity_Input_Binding_Event_Add;
        input = Input_Null;
        transformed = false;
    }

    EntityInputBindingEvent(EntityInputBindingEventType type, const string& world, EntityID id)
        : EntityInputBindingEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            static_cast<u32>(input),
            binding,
            transformed
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityInputBindingEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        input = static_cast<InputType>(get<3>(payload));
        binding = get<4>(payload);
        transformed = get<5>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, u32, InputBinding, bool> layout;

    EntityInputBindingEventType type;
    string world;
    EntityID id;
    InputType input;
    InputBinding binding;
    bool transformed;
};

enum EntityHUDEventType : u8
{
    Entity_HUD_Event_Add,
    Entity_HUD_Event_Remove,
    Entity_HUD_Event_Update
};

struct EntityHUDEvent
{
    EntityHUDEvent()
    {
        type = Entity_HUD_Event_Add;
        transformed = false;
    }

    EntityHUDEvent(EntityHUDEventType type, const string& world, EntityID id)
        : EntityHUDEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            name,
            element,
            transformed
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityHUDEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        name = get<3>(payload);
        element = get<4>(payload);
        transformed = get<5>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, string, HUDElement, bool> layout;

    EntityHUDEventType type;
    string world;
    EntityID id;
    string name;
    HUDElement element;
    bool transformed;
};

enum EntityScriptEventType : u8
{
    Entity_Script_Event_Add,
    Entity_Script_Event_Remove,
    Entity_Script_Event_Update
};

struct EntityScriptEvent
{
    EntityScriptEvent()
    {
        type = Entity_Script_Event_Add;
        transformed = false;
    }

    EntityScriptEvent(EntityScriptEventType type, const string& world, EntityID id)
        : EntityScriptEvent()
    {
        this->type = type;
        this->world = world;
        this->id = id;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            world,
            id,
            name,
            value,
            transformed
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<EntityScriptEventType>(get<0>(payload));
        world = get<1>(payload);
        id = get<2>(payload);
        name = get<3>(payload);
        value = get<4>(payload);
        transformed = get<5>(payload);
    }

    typedef msgpack::type::tuple<u8, string, EntityID, string, Value, bool> layout;

    EntityScriptEventType type;
    string world;
    EntityID id;
    string name;
    Value value;
    bool transformed;
};

enum ScriptEventType : u8
{
    Script_Event_Set,
    Script_Event_Clear
};

struct ScriptEvent
{
    ScriptEvent()
    {
        type = Script_Event_Set;
    }

    ScriptEvent(ScriptEventType type, const string& name)
        : ScriptEvent()
    {
        this->type = type;
        this->name = name;
    }

    ScriptEvent(ScriptEventType type, const string& name, const Value& value, const ScriptValuePermissions& permissions)
        : ScriptEvent()
    {
        this->type = type;
        this->name = name;
        this->value = value;
        this->permissions = permissions;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            name,
            value,
            permissions
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<ScriptEventType>(get<0>(payload));
        name = get<1>(payload);
        value = get<2>(payload);
        permissions = get<3>(payload);
    }

    typedef msgpack::type::tuple<u8, string, Value, ScriptValuePermissions> layout;

    ScriptEventType type;
    string name;
    Value value;
    ScriptValuePermissions permissions;
};

struct AttachEvent
{
    AttachEvent()
    {
        fieldOfView = defaultCameraAngle;
        mouseLocked = false;
        voiceVolume = defaultVoiceVolume;
        tintStrength = 0;
    }

    AttachEvent(
        const map<InputType, InputBinding>& bindings,
        const map<string, HUDElement>& hudElements,
        f64 fieldOfView,
        bool mouseLocked,
        f64 voiceVolume,
        const vec3& tintColor,
        f64 tintStrength
    )
        : AttachEvent()
    {
        this->bindings = bindings;
        this->hudElements = hudElements;
        this->fieldOfView = fieldOfView;
        this->mouseLocked = mouseLocked;
        this->voiceVolume = voiceVolume;
        this->tintColor = tintColor;
        this->tintStrength = tintStrength;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        map<u32, InputBinding> bindings;

        for (const auto& [ input, binding ] : this->bindings)
        {
            bindings.insert({ static_cast<u32>(input), binding });
        }

        layout payload = {
            bindings,
            hudElements,
            fieldOfView,
            mouseLocked,
            voiceVolume,
            tintColor,
            tintStrength
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        for (const auto& [ input, binding ] : get<0>(payload))
        {
            bindings.insert({ static_cast<InputType>(input), binding });
        }

        hudElements = get<1>(payload);
        fieldOfView = get<2>(payload);
        mouseLocked = get<3>(payload);
        voiceVolume = get<4>(payload);
        tintColor = get<5>(payload);
        tintStrength = get<6>(payload);
    }

    typedef msgpack::type::tuple<map<u32, InputBinding>, map<string, HUDElement>, f64, bool, f64, vec3, f64> layout;

    map<InputType, InputBinding> bindings;
    map<string, HUDElement> hudElements;
    f64 fieldOfView;
    bool mouseLocked;
    f64 voiceVolume;
    vec3 tintColor;
    f64 tintStrength;
};

struct DetachEvent
{
    DetachEvent()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {};

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);
    }

    typedef msgpack::type::tuple<> layout;
};

enum MoveEventType : u8
{
    Move_Event_Viewer,
    Move_Event_Room
};

struct MoveEvent
{
    MoveEvent()
    {
        type = Move_Event_Viewer;
    }

    MoveEvent(MoveEventType type, const mat4& transform)
    {
        this->type = type;
        this->transform = transform;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            transform
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<MoveEventType>(get<0>(payload));
        transform = get<1>(payload);
    }

    typedef msgpack::type::tuple<u8, mat4> layout;

    MoveEventType type;
    mat4 transform;
};

struct HapticEvent
{
    HapticEvent()
    {

    }

    HapticEvent(const HapticEffect& effect)
        : HapticEvent()
    {
        this->effect = effect;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            effect
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        effect = get<0>(payload);
    }

    typedef msgpack::type::tuple<HapticEffect> layout;

    HapticEffect effect;
};

struct HistoryEvent
{
    HistoryEvent()
    {

    }

    HistoryEvent(const string& command)
        : HistoryEvent()
    {
        this->command = command;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            command
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        command = get<0>(payload);
    }

    typedef msgpack::type::tuple<string> layout;

    string command;
};

struct InputEvent
{
    InputEvent()
    {

    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            hook,
            input
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        hook = get<0>(payload);
        input = get<1>(payload);
    }

    typedef msgpack::type::tuple<string, Input> layout;

    string hook;
    Input input;
};

struct GripEvent
{
    GripEvent()
    {
        right = false;
        strength = 0;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            right,
            strength,
            position,
            rotation,
            linearVelocity,
            angularVelocity
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        right = get<0>(payload);
        strength = get<1>(payload);
        position = get<2>(payload);
        rotation = get<3>(payload);
        linearVelocity = get<4>(payload);
        angularVelocity = get<5>(payload);
    }

    typedef msgpack::type::tuple<bool, f64, vec3, quaternion, vec3, vec3> layout;

    bool right;
    f64 strength;
    vec3 position;
    quaternion rotation;
    vec3 linearVelocity;
    vec3 angularVelocity;
};

enum CommandEventType : u8
{
    Command_Event_Command,
    Command_Event_Chat,
    Command_Event_Direct_Chat,
    Command_Event_Team_Chat
};

struct CommandEvent
{
    CommandEvent()
    {
        type = Command_Event_Command;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            destination,
            text
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<CommandEventType>(get<0>(payload));
        destination = get<1>(payload);
        text = get<2>(payload);
    }

    typedef msgpack::type::tuple<u8, string, string> layout;

    CommandEventType type;
    string destination;
    string text;
};

struct PoseEvent
{
    PoseEvent()
    {

    }

    PoseEvent(const map<VRDevice, mat4>& pose)
        : PoseEvent()
    {
        this->pose = pose;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        map<u8, mat4> pose;

        for (const auto& [ device, transform ] : this->pose)
        {
            pose.insert({ static_cast<u8>(device), transform });
        }

        layout payload = {
            pose
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        map<u8, mat4> pose = get<0>(payload);

        for (const auto& [ device, transform ] : pose)
        {
            this->pose.insert({ static_cast<VRDevice>(device), transform });
        }
    }

    typedef msgpack::type::tuple<map<u8, mat4>> layout;

    map<VRDevice, mat4> pose;
};

struct LogEvent
{
    LogEvent()
    {

    }

    LogEvent(const Message& message)
        : LogEvent()
    {
        this->message = message;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            message
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        message = get<0>(payload);
    }

    typedef msgpack::type::tuple<Message> layout;

    Message message;
};

struct TextRequestEvent
{
    TextRequestEvent()
    {

    }

    TextRequestEvent(const TextRequest& request, const set<UserID>& userIDs = {})
        : TextRequestEvent()
    {
        this->request = request;
        this->userIDs = userIDs;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            request,
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        request = get<0>(payload);
        userIDs = get<1>(payload);
    }

    typedef msgpack::type::tuple<TextRequest, set<UserID>> layout;

    TextRequest request;
    set<UserID> userIDs;
};

struct TextResponseEvent
{
    TextResponseEvent()
    {

    }

    TextResponseEvent(TextResponse response)
        : TextResponseEvent()
    {
        this->response = response;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            response
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        response = get<0>(payload);
    }

    typedef msgpack::type::tuple<TextResponse> layout;

    TextResponse response;
};

enum TetherEventType : u8
{
    Tether_Event_Tether,
    Tether_Event_Untether
};

struct TetherEvent
{
    TetherEvent()
    {
        type = Tether_Event_Tether;
        distance = 0;
    }

    TetherEvent(TetherEventType type)
        : TetherEvent()
    {
        this->type = type;
    }

    TetherEvent(TetherEventType type, f64 distance, const set<UserID>& userIDs)
        : TetherEvent()
    {
        this->type = type;
        this->distance = distance;
        this->userIDs = userIDs;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            distance,
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<TetherEventType>(get<0>(payload));
        distance = get<1>(payload);
        userIDs = get<2>(payload);
    }

    typedef msgpack::type::tuple<u8, f64, set<UserID>> layout;

    TetherEventType type;
    f64 distance;
    set<UserID> userIDs;
};

enum ConfinementEventType : u8
{
    Confinement_Event_Confine,
    Confinement_Event_Unconfine
};

struct ConfinementEvent
{
    ConfinementEvent()
    {
        type = Confinement_Event_Confine;
        distance = 0;
    }

    ConfinementEvent(ConfinementEventType type)
        : ConfinementEvent()
    {
        this->type = type;
    }

    ConfinementEvent(ConfinementEventType type, const vec3& position, f64 distance, const set<UserID>& userIDs)
        : ConfinementEvent()
    {
        this->type = type;
        this->position = position;
        this->distance = distance;
        this->userIDs = userIDs;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            position,
            distance,
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<ConfinementEventType>(get<0>(payload));
        position = get<1>(payload);
        distance = get<2>(payload);
        userIDs = get<3>(payload);
    }

    typedef msgpack::type::tuple<u8, vec3, f64, set<UserID>> layout;

    ConfinementEventType type;
    vec3 position;
    f64 distance;
    set<UserID> userIDs;
};

enum FadeEventType : u8
{
    Fade_Event_Fade,
    Fade_Event_Unfade
};

struct FadeEvent
{
    FadeEvent()
    {
        type = Fade_Event_Fade;
    }

    FadeEvent(FadeEventType type)
        : FadeEvent()
    {
        this->type = type;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<FadeEventType>(get<0>(payload));
        userIDs = get<1>(payload);
    }

    typedef msgpack::type::tuple<u8, set<UserID>> layout;

    FadeEventType type;
    set<UserID> userIDs;
};

enum WaitEventType : u8
{
    Wait_Event_Wait,
    Wait_Event_Unwait
};

struct WaitEvent
{
    WaitEvent()
    {
        type = Wait_Event_Wait;
    }

    WaitEvent(WaitEventType type)
        : WaitEvent()
    {
        this->type = type;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<WaitEventType>(get<0>(payload));
        userIDs = get<1>(payload);
    }

    typedef msgpack::type::tuple<u8, set<UserID>> layout;

    WaitEventType type;
    set<UserID> userIDs;
};

struct GotoEvent
{
    GotoEvent()
    {
        radius = 1;
    }

    GotoEvent(const vec3& position, f64 radius, const vec3& up, const set<UserID>& userIDs)
        : GotoEvent()
    {
        this->position = position;
        this->radius = radius;
        this->up = up;
        this->userIDs = userIDs;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            position,
            radius,
            up,
            userIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        position = get<0>(payload);
        radius = get<1>(payload);
        up = get<2>(payload);
        userIDs = get<3>(payload);
    }

    typedef msgpack::type::tuple<vec3, f64, vec3, set<UserID>> layout;

    vec3 position;
    f64 radius;
    set<UserID> userIDs;
    vec3 up;
};

struct VoiceEvent
{
    VoiceEvent()
    {

    }

    VoiceEvent(const string& data)
        : VoiceEvent()
    {
        this->data = data;
    }

    VoiceEvent(UserID id, const string& world, const vec3& position, const quaternion& rotation, const vec3& velocity, const string& data)
        : VoiceEvent()
    {
        this->id = id;
        this->world = world;
        this->position = position;
        this->rotation = rotation;
        this->velocity = velocity;
        this->data = data;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            world,
            position,
            rotation,
            velocity,
            data
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        world = get<1>(payload);
        position = get<2>(payload);
        rotation = get<3>(payload);
        velocity = get<4>(payload);
        data = get<5>(payload);
    }

    typedef msgpack::type::tuple<UserID, string, vec3, quaternion, vec3, string> layout;

    UserID id;
    string world;
    vec3 position;
    quaternion rotation;
    vec3 velocity;
    string data;
};

struct VoiceChannelEvent
{
    VoiceChannelEvent()
    {

    }

    VoiceChannelEvent(const string& name)
        : VoiceChannelEvent()
    {
        this->name = name;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            name
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        name = get<0>(payload);
    }

    typedef msgpack::type::tuple<string> layout;

    string name;
};

struct StepEvent
{
    StepEvent()
    {
        remoteIndex = 0;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            localIndex,
            remoteIndex
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        localIndex = get<0>(payload);
        remoteIndex = get<1>(payload);
    }

    typedef msgpack::type::tuple<optional<u64>, u64> layout;

    // Fields are named from the perspective of the receiver
    optional<u64> localIndex;
    u64 remoteIndex;
};

struct URLEvent
{
    URLEvent()
    {

    }

    URLEvent(const string& url)
        : URLEvent()
    {
        this->url = url;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            url
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        url = get<0>(payload);
    }

    typedef msgpack::type::tuple<string> layout;

    string url;
};

struct SaveEvent
{
    SaveEvent()
    {
        stop = false;
    }

    SaveEvent(bool stop)
        : SaveEvent()
    {
        this->stop = stop;
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            stop
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        stop = get<0>(payload);
    }

    typedef msgpack::type::tuple<bool> layout;

    bool stop;
};
