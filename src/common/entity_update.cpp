/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "entity_update.hpp"
#include "world.hpp"
#include "entity.hpp"

EntityUpdateSideEffect::EntityUpdateSideEffect()
    : type(Entity_Update_Display_Add)
{

}

EntityUpdateSideEffect::EntityUpdateSideEffect(EntityID parentID, EntityID id, EntityUpdateSideEffectType type)
    : parentID(parentID)
    , id(id)
    , type(type)
{

}

EntityUpdate::EntityUpdate(const World* world, const Entity* entity)
    : entity(entity)
    , worldName(world->name())
    , entityID(entity->id())
    , _attachment(false)
{

}

void EntityUpdate::addChild(Entity* child)
{
    auto [ childEvents, childSideEffects ] = child->initial();

    push(childEvents);
    push(childSideEffects);
}

void EntityUpdate::removeChild(Entity* child)
{
    auto [ childEvents, childSideEffects ] = child->terminal();

    push(childEvents);
    push(childSideEffects);
}

void EntityUpdate::updateChild(const Entity* previousChild, Entity* child, const EntityUpdate& update)
{
    if (child->attachedUserID() && attachment())
    {
        attachmentUpdate(child->id());
    }

    push(update.events());
    push(update.sideEffects());
}

bool EntityUpdate::attachment() const
{
    return _attachment;
}

void EntityUpdate::push(const NetworkEvent& event)
{
    _events.push_back(event);
}

void EntityUpdate::push(const vector<NetworkEvent>& events)
{
    _events.insert(
        _events.end(),
        events.begin(),
        events.end()
    );
}

void EntityUpdate::push(const EntityUpdateSideEffect& sideEffect)
{
    _sideEffects.push_back(sideEffect);
}

void EntityUpdate::push(const vector<EntityUpdateSideEffect>& sideEffects)
{
    _sideEffects.insert(
        _sideEffects.end(),
        sideEffects.begin(),
        sideEffects.end()
    );
}

void EntityUpdate::flag(u8 flags)
{
    if (flags & Entity_Update_Flag_Render)
    {
        _sideEffects.push_back({ parentID(), entityID, Entity_Update_Display_Update_Render });
    }

    if (flags & Entity_Update_Flag_Sound)
    {
        _sideEffects.push_back({ parentID(), entityID, Entity_Update_Display_Update_Sound });
    }

    if (flags & Entity_Update_Flag_Attachment)
    {
        _attachment = true;
    }
}

const vector<NetworkEvent>& EntityUpdate::events() const
{
    return _events;
}

const vector<EntityUpdateSideEffect>& EntityUpdate::sideEffects() const
{
    return _sideEffects;
}

void EntityUpdate::attachmentUpdate(EntityID id)
{
    _sideEffects.push_back({ entityID, id, Entity_Update_Attachment_Update });
}

EntityID EntityUpdate::parentID() const
{
    return entity->parent() ? entity->parent()->id() : EntityID();
}
