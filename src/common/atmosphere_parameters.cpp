/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "atmosphere_parameters.hpp"
#include "log.hpp"
#include "constants.hpp"
#include "yaml_tools.hpp"

string atmosphereEffectTypeToString(AtmosphereEffectType type)
{
    switch (type)
    {
    case Atmosphere_Effect_Line :
        return "atmosphere-effect-line";
    case Atmosphere_Effect_Solid :
        return "atmosphere-effect-solid";
    case Atmosphere_Effect_Cloud :
        return "atmosphere-effect-cloud";
    case Atmosphere_Effect_Streamer :
        return "atmosphere-effect-streamer";
    case Atmosphere_Effect_Warp :
        return "atmosphere-effect-warp";
    default :
        fatal("Encountered unknown atmosphere effect type '" + to_string(type) + "'.");
    }
}

AtmosphereEffectType atmosphereEffectTypeFromString(const string& s)
{
    if (s == "atmosphere-effect-line")
    {
        return Atmosphere_Effect_Line;
    }
    else if (s == "atmosphere-effect-solid")
    {
        return Atmosphere_Effect_Solid;
    }
    else if (s == "atmosphere-effect-cloud")
    {
        return Atmosphere_Effect_Cloud;
    }
    else if (s == "atmosphere-effect-streamer")
    {
        return Atmosphere_Effect_Streamer;
    }
    else if (s == "atmosphere-effect-warp")
    {
        return Atmosphere_Effect_Warp;
    }
    else
    {
        fatal("Encountered unknown atmosphere effect type '" + s + "'.");
    }
}

template<>
AtmosphereEffectType YAMLNode::convert() const
{
    return atmosphereEffectTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(AtmosphereEffectType v)
{
    emit(atmosphereEffectTypeToString(v));
}

AtmosphereEffect::AtmosphereEffect()
    : type(Atmosphere_Effect_Line)
    , density(0.5)
    , size(0.01)
    , radius(0.002)
    , mass(0.00034)
    , flowVelocity(forwardDir)
{

}

void AtmosphereEffect::verify()
{
    // NOTE: Update this when adding new atmosphere effect types
    type = min(type, Atmosphere_Effect_Warp);
    density = clamp(density, 0.0, 1.0);
    size = max(size, 0.0);
    radius = max(radius, 0.0);
    mass = max(mass, 0.0);
    color.verify();
}

bool AtmosphereEffect::operator==(const AtmosphereEffect& rhs) const
{
    return type == rhs.type &&
        density == rhs.density &&
        size == rhs.size &&
        radius == rhs.radius &&
        mass == rhs.mass &&
        flowVelocity == rhs.flowVelocity &&
        color == rhs.color;
}

bool AtmosphereEffect::operator!=(const AtmosphereEffect& rhs) const
{
    return !(*this == rhs);
}

template<>
AtmosphereEffect YAMLNode::convert() const
{
    AtmosphereEffect effect;

    effect.type = atmosphereEffectTypeFromString(at("type").as<string>());
    effect.density = at("density").as<f64>();
    effect.size = at("size").as<f64>();
    effect.radius = at("radius").as<f64>();
    effect.mass = at("mass").as<f64>();
    effect.flowVelocity = at("flowVelocity").as<vec3>();
    effect.color = at("color").as<EmissiveColor>();

    return effect;
}

template<>
void YAMLSerializer::emit(AtmosphereEffect v)
{
    startMapping();

    emitPair("type", atmosphereEffectTypeToString(v.type));
    emitPair("density", v.density);
    emitPair("size", v.size);
    emitPair("radius", v.radius);
    emitPair("mass", v.mass);
    emitPair("flowVelocity", v.flowVelocity);
    emitPair("color", v.color);

    endMapping();
}

AtmosphereLightning::AtmosphereLightning()
    : frequency(0)
    , minDistance(25)
    , maxDistance(100)
    , lowerHeight(0)
    , upperHeight(100)
    , color(defaultAtmosphereLightningColor, defaultAtmosphereLightningBrightness)
{

}

void AtmosphereLightning::verify()
{
    frequency = max(frequency, 0.0);
    minDistance = max(minDistance, 0.0);
    maxDistance = max(maxDistance, minDistance);
    upperHeight = max(upperHeight, 0.0);
    lowerHeight = min(lowerHeight, 0.0);
    color.verify();
}

bool AtmosphereLightning::operator==(const AtmosphereLightning& rhs) const
{
    return frequency == rhs.frequency &&
        minDistance == rhs.minDistance &&
        maxDistance == rhs.maxDistance &&
        lowerHeight == rhs.lowerHeight &&
        upperHeight == rhs.upperHeight &&
        color == rhs.color;
}

bool AtmosphereLightning::operator!=(const AtmosphereLightning& rhs) const
{
    return !(*this == rhs);
}

template<>
AtmosphereLightning YAMLNode::convert() const
{
    AtmosphereLightning lightning;

    lightning.frequency = at("frequency").as<f64>();
    lightning.minDistance = at("minDistance").as<f64>();
    lightning.maxDistance = at("maxDistance").as<f64>();
    lightning.lowerHeight = at("lowerHeight").as<f64>();
    lightning.upperHeight = at("upperHeight").as<f64>();
    lightning.color = at("color").as<HDRColor>();

    return lightning;
}

template<>
void YAMLSerializer::emit(AtmosphereLightning v)
{
    startMapping();

    emitPair("frequency", v.frequency);
    emitPair("minDistance", v.minDistance);
    emitPair("maxDistance", v.maxDistance);
    emitPair("lowerHeight", v.lowerHeight);
    emitPair("upperHeight", v.upperHeight);
    emitPair("color", v.color);

    endMapping();
}

AtmosphereParameters::AtmosphereParameters()
{
    enabled = true;
    seed = 0;
}

void AtmosphereParameters::verify()
{
    for (AtmosphereEffect& effect : effects)
    {
        effect.verify();
    }
    lightning.verify();
}

bool AtmosphereParameters::operator==(const AtmosphereParameters& rhs) const
{
    return enabled == rhs.enabled &&
        seed == rhs.seed &&
        effects == rhs.effects &&
        lightning == rhs.lightning;
}

bool AtmosphereParameters::operator!=(const AtmosphereParameters& rhs) const
{
    return !(*this == rhs);
}

template<>
AtmosphereParameters YAMLNode::convert() const
{
    AtmosphereParameters parameters;

    parameters.enabled = at("enabled").as<bool>();
    parameters.seed = at("seed").as<u64>();
    parameters.effects = at("effects").as<vector<AtmosphereEffect>>();
    parameters.lightning = at("lightning").as<AtmosphereLightning>();

    return parameters;
}

template<>
void YAMLSerializer::emit(AtmosphereParameters v)
{
    startMapping();

    emitPair("enabled", v.enabled);
    emitPair("seed", v.seed);
    emitPair("effects", v.effects);
    emitPair("lightning", v.lightning);

    endMapping();
}
