/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_parts.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

ActionPartIK::ActionPartIK()
{

}

void ActionPartIK::verify()
{
    transform = verifyTransform(transform);
}

bool ActionPartIK::operator==(const ActionPartIK& rhs) const
{
    return transform == rhs.transform;
}

bool ActionPartIK::operator!=(const ActionPartIK& rhs) const
{
    return !(*this == rhs);
}

template<>
ActionPartIK YAMLNode::convert() const
{
    ActionPartIK part;

    part.transform = at("transform").as<mat4>();

    return part;
}

template<>
void YAMLSerializer::emit(ActionPartIK v)
{
    startMapping();

    emitPair("transform", v.transform);

    endMapping();
}
