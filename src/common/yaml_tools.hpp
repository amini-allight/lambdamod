/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

#include <yaml.h>

// TODO: SAVE: Add resiliency to slightly incorrect save files?

enum YAMLNodeType : u8
{
    YAML_Node_Scalar,
    YAML_Node_Sequence,
    YAML_Node_Mapping
};

template<typename T>
struct isVector : false_type {};

template<typename T>
struct isVector<vector<T>> : true_type {};

template<typename T>
struct isSet : false_type {};

template<typename T>
struct isSet<set<T>> : true_type {};

template<typename T>
struct isDeque : false_type {};

template<typename T>
struct isDeque<deque<T>> : true_type {};

template<typename T>
struct isMap : false_type {};

template<typename KeyT, typename ValT>
struct isMap<map<KeyT, ValT>> : true_type {};

template<typename T>
struct isOptional : false_type {};

template<typename T>
struct isOptional<optional<T>> : true_type {};

template<typename T>
struct isIDType : false_type {};

template<typename T, int N>
struct isIDType<IDType<T, N>> : true_type {};

class YAMLNode
{
public:
    YAMLNode();
    YAMLNode(size_t i);
    YAMLNode(const string& s);
    YAMLNode(const string& path, yaml_parser_t& parser, yaml_event_t& event);

    YAMLNodeType type() const;
    bool isNull() const;
    const string& scalar() const;
    const vector<YAMLNode>& sequence() const;
    const map<YAMLNode, YAMLNode>& mapping() const;

    template<typename T>
    T convert() const;

    template<typename T>
    vector<T> convertVector() const
    {
        vector<T> v;

        for (const YAMLNode& i : _sequence)
        {
            v.push_back(i.as<T>());
        }

        return v;
    }

    template<typename T>
    set<T> convertSet() const
    {
        set<T> v;

        for (const YAMLNode& i : _sequence)
        {
            v.insert(i.as<T>());
        }

        return v;
    }

    template<typename T>
    deque<T> convertDeque() const
    {
        deque<T> v;

        for (const YAMLNode& i : _sequence)
        {
            v.push_back(i.as<T>());
        }

        return v;
    }

    template<typename KeyT, typename ValT>
    map<KeyT, ValT> convertMap() const
    {
        map<KeyT, ValT> m;

        for (const auto& [ k, v ] : _mapping)
        {
            m.insert({ k.template as<KeyT>(), v.template as<ValT>() });
        }

        return m;
    }

    template<typename T>
    optional<T> convertOptional() const
    {
        if (isNull())
        {
            return {};
        }
        else
        {
            return as<T>();
        }
    }

    template<typename T, int N>
    IDType<T, N> convertIDType() const
    {
        return IDType<T, N>(as<T>());
    }

    template<typename T>
    T as() const
    {
        if constexpr (isVector<T>::value)
        {
            return convertVector<typename T::value_type>();
        }
        else if constexpr (isSet<T>::value)
        {
            return convertSet<typename T::value_type>();
        }
        else if constexpr (isDeque<T>::value)
        {
            return convertDeque<typename T::value_type>();
        }
        else if constexpr (isMap<T>::value)
        {
            return convertMap<typename T::key_type, typename T::mapped_type>();
        }
        else if constexpr (isOptional<T>::value)
        {
            return convertOptional<typename T::value_type>();
        }
        else if constexpr (isIDType<T>::value)
        {
            return convertIDType<typename T::value_type, T::type_index>();
        }
        else
        {
            return convert<T>();
        }
    }

    bool has(size_t i) const;
    bool has(const string& s) const;

    const YAMLNode& at(size_t i) const;
    const YAMLNode& at(const string& s) const;

    const YAMLNode& operator[](size_t i) const;
    const YAMLNode& operator[](const string& s) const;

    bool operator==(const YAMLNode& rhs) const;
    bool operator!=(const YAMLNode& rhs) const;
    bool operator>=(const YAMLNode& rhs) const;
    bool operator<=(const YAMLNode& rhs) const;
    bool operator>(const YAMLNode& rhs) const;
    bool operator<(const YAMLNode& rhs) const;
    strong_ordering operator<=>(const YAMLNode& rhs) const;

private:
    string path;
    YAMLNodeType _type;
    bool null;
    string _scalar;
    vector<YAMLNode> _sequence;
    map<YAMLNode, YAMLNode> _mapping;

    void check(int status);
};

template<>
bool YAMLNode::convert() const;

template<>
string YAMLNode::convert() const;

template<>
f64 YAMLNode::convert() const;

template<>
u8 YAMLNode::convert() const;

template<>
u16 YAMLNode::convert() const;

template<>
u32 YAMLNode::convert() const;

template<>
u64 YAMLNode::convert() const;

template<>
i8 YAMLNode::convert() const;

template<>
i16 YAMLNode::convert() const;

template<>
i32 YAMLNode::convert() const;

template<>
i64 YAMLNode::convert() const;

template<>
vec2 YAMLNode::convert() const;

template<>
vec3 YAMLNode::convert() const;

template<>
vec4 YAMLNode::convert() const;

template<>
quaternion YAMLNode::convert() const;

template<>
mat4 YAMLNode::convert() const;

class YAMLParser
{
public:
    YAMLParser(const string& path);

    const YAMLNode& root() const;

private:
    string path;
    FILE* file;
    yaml_parser_t parser;
    yaml_event_t event;

    YAMLNode _root;

    void check(int status);
};

class YAMLSerializer
{
public:
    YAMLSerializer(const string& path);
    YAMLSerializer(const YAMLSerializer& rhs) = delete;
    YAMLSerializer(YAMLSerializer&& rhs) = delete;
    ~YAMLSerializer();

    YAMLSerializer& operator=(const YAMLSerializer& rhs) = delete;
    YAMLSerializer& operator=(YAMLSerializer&& rhs) = delete;

    void startSequence();
    void endSequence();

    void startMapping();
    void endMapping();

    void emitNull();

    template<typename T>
    void emit(T v);

    template<typename T>
    void emit(vector<T> v)
    {
        startSequence();

        for (T i : v)
        {
            emit(i);
        }

        endSequence();
    }

    template<typename T>
    void emit(set<T> v)
    {
        startSequence();

        for (T i : v)
        {
            emit(i);
        }

        endSequence();
    }

    template<typename T>
    void emit(deque<T> v)
    {
        startSequence();

        for (T i : v)
        {
            emit(i);
        }

        endSequence();
    }

    template<typename KeyT, typename ValT>
    void emit(map<KeyT, ValT> m)
    {
        startMapping();

        for (const auto& [ k, v ] : m)
        {
            emit(k);
            emit(v);
        }

        endMapping();
    }

    template<typename T>
    void emit(const optional<T>& v)
    {
        if (v)
        {
            emit(*v);
        }
        else
        {
            emitNull();
        }
    }

    template<typename T, int N>
    void emit(const IDType<T, N>& id)
    {
        emit(id.value());
    }

    template<typename KeyT, typename ValT>
    void emitPair(KeyT k, ValT v)
    {
        emit(k);
        emit(v);
    }

private:
    string path;
    FILE* file;
    yaml_emitter_t emitter;
    yaml_event_t event;

    void check(int status);
};

template<>
void YAMLSerializer::emit(bool v);

template<>
void YAMLSerializer::emit(string v);

template<>
void YAMLSerializer::emit(const char* v);

template<>
void YAMLSerializer::emit(f64 v);

template<>
void YAMLSerializer::emit(u8 v);

template<>
void YAMLSerializer::emit(u16 v);

template<>
void YAMLSerializer::emit(u32 v);

template<>
void YAMLSerializer::emit(u64 v);

template<>
void YAMLSerializer::emit(i8 v);

template<>
void YAMLSerializer::emit(i16 v);

template<>
void YAMLSerializer::emit(i32 v);

template<>
void YAMLSerializer::emit(i64 v);

template<>
void YAMLSerializer::emit(vec2 v);

template<>
void YAMLSerializer::emit(vec3 v);

template<>
void YAMLSerializer::emit(vec4 v);

template<>
void YAMLSerializer::emit(quaternion v);

template<>
void YAMLSerializer::emit(mat4 v);

bool boolFromYAML(const string& s);
