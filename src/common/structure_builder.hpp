/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "structure.hpp"

struct StructureVertex
{
    StructureVertex();
    StructureVertex(const fvec3& position, const fvec3& normal, bool foreground);

    bool operator==(const StructureVertex& rhs) const;
    bool operator!=(const StructureVertex& rhs) const;

    fvec3 position;
    fvec3 normal;
    bool foreground;
};

struct StructureMesh
{
    StructureMesh();

    bool operator==(const StructureMesh& rhs) const;
    bool operator!=(const StructureMesh& rhs) const;

    vector<StructureVertex> vertices;
    vector<u32> indices;
};

StructureMesh buildStructure(const StructureNode& node);
