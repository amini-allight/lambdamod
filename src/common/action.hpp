/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "action_part.hpp"
#include "yaml_tools.hpp"

class EntityUpdate;
class NetworkEvent;

class Action
{
public:
    Action();

    void verify();

    void compare(const string& name, const Action& previous, EntityUpdate& update) const;

    void push(const NetworkEvent& event);
    vector<NetworkEvent> initial(const string& world, EntityID id, const string& name) const;

    ActionPartID add();
    void add(const ActionPart& part);
    void remove(ActionPartID id);

    const map<ActionPartID, ActionPart>& parts() const;

    u32 length() const;

    bool operator==(const Action& rhs) const;
    bool operator!=(const Action& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            nextPartID,
            _parts
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        nextPartID = get<0>(payload);
        _parts = get<1>(payload);
    }

    typedef msgpack::type::tuple<ActionPartID, map<ActionPartID, ActionPart>> layout;

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    ActionPartID nextPartID;
    map<ActionPartID, ActionPart> _parts;
};
