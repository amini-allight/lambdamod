/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "timer.hpp"
#include "yaml_tools.hpp"
#include "script_tools.hpp"
#include "game_state.hpp"
#include "scope_closure.hpp"

Timer::Timer()
    : delay(0)
{

}

Timer::Timer(f64 delay, const Lambda& lambda, UserID userID)
    : Timer()
{
    this->delay = delay;
    this->lambda = lambda;
    this->userID = userID;
}

bool Timer::step(GameState* game, f64 stepInterval)
{
    delay -= stepInterval;

    if (delay <= 0)
    {
        User* user = game->getUser(userID);

        if (user)
        {
            ScopeClosure closure(game->context());

            runSafely(game->context(), lambda, {}, "running timer hook", user);
        }

        return true;
    }
    else
    {
        return false;
    }
}

template<>
Timer YAMLNode::convert() const
{
    Timer timer;

    ScriptContext context;

    timer.delay = at("delay").as<f64>();
    timer.lambda = runSafely(
        &context,
        at("lambda").as<string>(),
        "loading timer lambda"
    )->front().lambda;
    timer.userID = at("userID").as<UserID>();

    return timer;
}

template<>
void YAMLSerializer::emit(Timer v)
{
    startMapping();

    emitPair("delay", v.delay);
    emitPair("lambda", v.lambda.toString());
    emitPair("userID", v.userID);

    endMapping();
}
