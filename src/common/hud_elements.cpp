/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud_elements.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

HUDElementLine::HUDElementLine()
{
    length = 0.5;
    rotation = 0;
}

void HUDElementLine::verify()
{
    length = max(length, 0.0);
}

bool HUDElementLine::operator==(const HUDElementLine& rhs) const
{
    return length == rhs.length &&
        rotation == rhs.rotation;
}

bool HUDElementLine::operator!=(const HUDElementLine& rhs) const
{
    return !(*this == rhs);
}

template<>
HUDElementLine YAMLNode::convert() const
{
    HUDElementLine line;

    line.length = at("length").as<f64>();
    line.rotation = at("rotation").as<f64>();

    return line;
}

template<>
void YAMLSerializer::emit(HUDElementLine v)
{
    startMapping();

    emitPair("length", v.length);
    emitPair("rotation", v.rotation);

    endMapping();
}

HUDElementText::HUDElementText()
{
    text = "Text";
}

void HUDElementText::verify()
{
    text = sanitize(text);
}

bool HUDElementText::operator==(const HUDElementText& rhs) const
{
    return text == rhs.text;
}

bool HUDElementText::operator!=(const HUDElementText& rhs) const
{
    return !(*this == rhs);
}

template<>
HUDElementText YAMLNode::convert() const
{
    HUDElementText text;

    text.text = at("text").as<string>();

    return text;
}

template<>
void YAMLSerializer::emit(HUDElementText v)
{
    startMapping();

    emitPair("text", v.text);

    endMapping();
}

HUDElementBar::HUDElementBar()
{
    length = 0.5;
    fraction = 0;
    rightToLeft = false;
}

void HUDElementBar::verify()
{
    length = max(length, 0.0);
    fraction = clamp(fraction, 0.0, 1.0);
}

bool HUDElementBar::operator==(const HUDElementBar& rhs) const
{
    return length == rhs.length &&
        fraction == rhs.fraction &&
        rightToLeft == rhs.rightToLeft;
}

bool HUDElementBar::operator!=(const HUDElementBar& rhs) const
{
    return !(*this == rhs);
}

template<>
HUDElementBar YAMLNode::convert() const
{
    HUDElementBar bar;

    bar.length = at("length").as<f64>();
    bar.fraction = at("fraction").as<f64>();
    bar.rightToLeft = at("rightToLeft").as<bool>();

    return bar;
}

template<>
void YAMLSerializer::emit(HUDElementBar v)
{
    startMapping();

    emitPair("length", v.length);
    emitPair("fraction", v.fraction);
    emitPair("rightToLeft", v.rightToLeft);

    endMapping();
}

HUDElementSymbol::HUDElementSymbol()
{
    type = Symbol_Star;
    size = 0.1;
}

void HUDElementSymbol::verify()
{
    type = min(type, Symbol_Star); // NOTE: Update this when adding new symbols
    size = clamp(size, 0.0, 1.0);
}

bool HUDElementSymbol::operator==(const HUDElementSymbol& rhs) const
{
    return type == rhs.type &&
        size == rhs.size;
}

bool HUDElementSymbol::operator!=(const HUDElementSymbol& rhs) const
{
    return !(*this == rhs);
}

template<>
HUDElementSymbol YAMLNode::convert() const
{
    HUDElementSymbol symbol;

    symbol.type = symbolTypeFromString(at("type").as<string>());
    symbol.size = at("size").as<f64>();

    return symbol;
}

template<>
void YAMLSerializer::emit(HUDElementSymbol v)
{
    startMapping();

    emitPair("type", symbolTypeToString(v.type));
    emitPair("size", v.size);

    endMapping();
}
