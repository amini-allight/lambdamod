/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_builtin_tools.hpp"
#include "world.hpp"
#include "entity.hpp"
#include "script_tools.hpp"
#include "tools.hpp"
#include "user.hpp"
#include "game_state.hpp"

void checkArgumentCount(const string& name, size_t actual, size_t exact)
{
    if (actual != exact)
    {
        throw runtime_error("Expected " + to_string(exact) + (exact != 1 ? " arguments to '" : " argument to '") + name + "', got " + to_string(actual) + ".");
    }
}

void checkArgumentCount(const string& name, size_t actual, size_t bound, bool upper)
{
    if (!upper && actual < bound)
    {
        throw runtime_error("Expected at least " + to_string(bound) + (bound != 1 ? " arguments to '" : " argument to '") + name + "', got " + to_string(actual) + ".");
    }

    if (upper && actual > bound)
    {
        throw runtime_error("Expected no more than " + to_string(bound) + (bound != 1 ? " arguments to '" : " argument to '") + name + "', got " + to_string(actual) + ".");
    }
}

void checkArgumentCount(const string& name, size_t actual, size_t lower, size_t upper)
{
    if (actual < lower || actual > upper)
    {
        throw runtime_error("Expected at least " + to_string(lower) + " and no more than " + to_string(upper) + " arguments to '" + name + "', got " + to_string(actual) + ".");
    }
}

void checkArgumentCount(const string& name, size_t actual, const vector<size_t>& options)
{
    if (find(options.begin(), options.end(), actual) == options.end())
    {
        string s;

        for (size_t i = 0; i < options.size(); i++)
        {
            size_t option = options[i];

            s += to_string(option);

            if (i + 2 == options.size())
            {
                s += " to ";
            }
            else if (i + 1 != options.size())
            {
                s += ", ";
            }
        }

        throw runtime_error("Expected " + s + " arguments to '" + name + "', got " + to_string(actual) + ".");
    }
}

void checkUserAuth(const string& name, const ScriptContext* context)
{
    if (!context->currentUser())
    {
        throw toValue("Function '" + name + "' must be called with a user's authorization.");
    }
}

void checkUserAdmin(const string& name, const ScriptContext* context)
{
    if (!context->currentUser()->admin())
    {
        throw toValue("Permission to execute '" + name + "' denied.");
    }
}

void checkUserAllowed(const string& name, const ScriptContext* context, const Entity& entity)
{
    if (!entity.allowed(context->currentUser()))
    {
        throw toValue("Permission to execute '" + name + "' denied.");
    }
}

void checkArgumentIsString(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_String)
    {
        throw toValue("Expected string as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsSymbol(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_Symbol)
    {
        throw toValue("Expected symbol as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsNumber(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_Integer && arg.type != Value_Float)
    {
        throw toValue("Expected integer or float as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsInteger(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_Integer)
    {
        throw toValue("Expected integer as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsFloat(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_Float)
    {
        throw toValue("Expected float as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsVec2(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (!isVec2(arg))
    {
        throw toValue("Expected length 2 list of integers or floats as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsVec3(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (!isVec3(arg))
    {
        throw toValue("Expected length 3 list of integers or floats as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsQuaternion(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (!isQuaternion(arg))
    {
        throw toValue("Expected length 4 list of integers or floats as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsTable(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (!isTable(arg))
    {
        throw toValue("Expected list of length 2 lists as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsList(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_List)
    {
        throw toValue("Expected list as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsLambda(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_Lambda)
    {
        throw toValue("Expected lambda as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentIsHandle(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (arg.type != Value_Handle)
    {
        throw toValue("Expected handle as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentHandleValid(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (!arg.id)
    {
        throw toValue("Expected non-nullptr handle as argument " + to_string(index + 1) + " to '" + name + "', got '" + arg.toString() + "'.");
    }
}

void checkArgumentNameValid(const string& name, const vector<Value>& args, size_t index)
{
    const Value& arg = args[index];

    if (!isValidName(arg.s))
    {
        throw toValue("Expected valid name as argument 1 to '" + name + "', got '" + arg.s + "'. Names must contain at least one character and only contain numbers, lowercase letters and hyphens. Names must not contain more than one hyphen in a row or use a hypen as the first or last character. Names must not be valid numbers.");
    }
}

void checkArgumentIntegerRange(const string& name, const vector<Value>& args, size_t index, i64 lower, i64 upper)
{
    const Value& arg = args[index];

    if (arg.number<i64>() < lower || arg.number<i64>() > upper)
    {
        throw toValue("Expected integer or float between " + to_string(lower) + " and " + to_string(upper) + " inclusive as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentIntegerLess(const string& name, const vector<Value>& args, size_t index, i64 upper)
{
    const Value& arg = args[index];

    if (arg.number<i64>() >= upper)
    {
        throw toValue("Expected integer or float less than " + to_string(upper) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentIntegerLessEqual(const string& name, const vector<Value>& args, size_t index, i64 upper)
{
    const Value& arg = args[index];

    if (arg.number<i64>() > upper)
    {
        throw toValue("Expected integer or float less than or equal to " + to_string(upper) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentIntegerGreater(const string& name, const vector<Value>& args, size_t index, i64 lower)
{
    const Value& arg = args[index];

    if (arg.number<i64>() <= lower)
    {
        throw toValue("Expected integer or float greater than " + to_string(lower) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentIntegerGreaterEqual(const string& name, const vector<Value>& args, size_t index, i64 lower)
{
    const Value& arg = args[index];

    if (arg.number<i64>() <= lower)
    {
        throw toValue("Expected integer or float greater than or equal to " + to_string(lower) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentFloatRange(const string& name, const vector<Value>& args, size_t index, f64 lower, f64 upper)
{
    const Value& arg = args[index];

    if (arg.number<f64>() < lower || arg.number<f64>() > upper)
    {
        throw toValue("Expected integer or float as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentFloatLess(const string& name, const vector<Value>& args, size_t index, f64 upper)
{
    const Value& arg = args[index];

    if (arg.number<f64>() >= upper)
    {
        throw toValue("Expected integer or float less than " + toPrettyString(upper) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentFloatLessEqual(const string& name, const vector<Value>& args, size_t index, f64 upper)
{
    const Value& arg = args[index];

    if (arg.number<f64>() >= upper)
    {
        throw toValue("Expected integer or float less than or equal to " + toPrettyString(upper) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentFloatGreater(const string& name, const vector<Value>& args, size_t index, f64 lower)
{
    const Value& arg = args[index];

    if (arg.number<f64>() <= lower)
    {
        throw toValue("Expected integer or float greater than " + toPrettyString(lower) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkArgumentFloatGreaterEqual(const string& name, const vector<Value>& args, size_t index, f64 lower)
{
    const Value& arg = args[index];

    if (arg.number<f64>() <= lower)
    {
        throw toValue("Expected integer or float greater than or equal to " + toPrettyString(lower) + " as argument " + to_string(index + 1) + " to '" + name + "', out of range.");
    }
}

void checkReturnIsNumber(const string& name, size_t index, const Value& value)
{
    if (value.type != Value_Integer && value.type != Value_Float)
    {
        throw toValue("Expected integer or float as return value from argument " + to_string(index + 1) + " lambda in '" + name + "', got '" + value.toString() + "'.");
    }
}

void checkReturnIsInteger(const string& name, size_t index, const Value& value)
{
    if (value.type != Value_Integer)
    {
        throw toValue("Expected integer as return value from argument " + to_string(index + 1) + " lambda in '" + name + "', got '" + value.toString() + "'.");
    }
}

void checkWorldExists(const string& name, const World* world)
{
    if (!world)
    {
        throw toValue("Target world in '" + name + "' not found.");
    }
}

void checkEntityExists(const string& name, const Entity* entity)
{
    if (!entity)
    {
        throw toValue("Target entity in '" + name + "' not found.");
    }
}

void checkUserExists(const string& name, const User* user)
{
    if (!user)
    {
        throw toValue("Target user in '" + name + "' not found.");
    }
}

vector<User*> parseUserArgument(const ScriptContext* context, const string& name, const vector<Value>& args, size_t index)
{
    const Value& target = args[index];

    if (target.type == Value_Symbol)
    {
        User* user = context->game->getUserByName(target.s);

        if (!user)
        {
            throw toValue("Target user '" + target.s + "' in '" + name + "' not found.");
        }

        return { user };
    }
    else if (isListOf(target, Value_Symbol))
    {
        vector<User*> users;

        for (const Value& item : target.l)
        {
            User* user = context->game->getUserByName(item.s);

            if (!user)
            {
                throw toValue("Target user '" + target.s + "' in '" + name + "' not found.");
            }

            users.push_back(user);
        }

        return users;
    }
    else
    {
        throw toValue("Expected symbol or list of symbols as argument " + to_string(index) + " to '" + name + "', got '" + target.toString() + "'.");
    }
}
