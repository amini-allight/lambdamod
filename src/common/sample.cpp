/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample.hpp"
#include "sound_constants.hpp"
#include "entity_update.hpp"

static void mix(vector<f32>& dst, u32 offset, const vector<f32>& src)
{
    for (u32 i = 0; i < src.size(); i++)
    {
        dst[offset + i] += src[i];
    }
}

Sample::Sample()
    : nextPartID(1)
{

}

void Sample::verify()
{
    for (auto& [ id, part ] : _parts)
    {
        part.verify();
    }
}

void Sample::compare(const string& name, const Sample& previous, EntityUpdate& update) const
{
    update.notify<SamplePartID, EntitySampleEvent>(nextPartID, previous.nextPartID, [&](EntitySampleEvent& event) -> void {
        event.type = Entity_Sample_Event_Next_Part_ID;
        event.name = name;
        event.partID = nextPartID;
    });

    update.notifyMap<SamplePartID, SamplePart, EntitySampleEvent>(
        _parts,
        previous._parts,
        [&](EntitySampleEvent& event, const SamplePartID& id, const SamplePart& part) -> void
        {
            event.type = Entity_Sample_Event_Add_Part;
            event.name = name;
            event.partID = id;
            event.part = part;
        },
        [&](EntitySampleEvent& event, const SamplePartID& id) -> void
        {
            event.type = Entity_Sample_Event_Remove_Part;
            event.name = name;
            event.partID = id;
        },
        [&](EntitySampleEvent& event, const SamplePartID& id, const SamplePart& part) -> void
        {
            event.type = Entity_Sample_Event_Update_Part;
            event.name = name;
            event.partID = id;
            event.part = part;
        }
    );
}

void Sample::push(const NetworkEvent& event)
{
    auto subEvent = event.payload<EntitySampleEvent>();

    switch (subEvent.type)
    {
    default :
        break;
    case Entity_Sample_Event_Next_Part_ID :
        nextPartID = subEvent.partID;
        break;
    case Entity_Sample_Event_Add_Part :
        if (subEvent.partID > nextPartID)
        {
            nextPartID = subEvent.partID;
        }

        _parts.insert_or_assign(subEvent.partID, subEvent.part);
        break;
    case Entity_Sample_Event_Remove_Part :
        _parts.erase(subEvent.partID);
        break;
    case Entity_Sample_Event_Update_Part :
    {
        auto it = _parts.find(subEvent.partID);

        if (it != _parts.end())
        {
            it->second = subEvent.part;
        }
        break;
    }
    }
}

vector<NetworkEvent> Sample::initial(const string& world, EntityID id, const string& name) const
{
    vector<NetworkEvent> events;

    {
        EntitySampleEvent event;
        event.type = Entity_Sample_Event_Add;
        event.world = world;
        event.id = id;
        event.name = name;

        events.push_back(event);
    }

    {
        EntitySampleEvent event;
        event.type = Entity_Sample_Event_Next_Part_ID;
        event.world = world;
        event.id = id;
        event.name = name;
        event.partID = nextPartID;

        events.push_back(event);
    }

    for (const auto& [ partID, part ] : _parts)
    {
        EntitySampleEvent event;
        event.type = Entity_Sample_Event_Add_Part;
        event.world = world;
        event.id = id;
        event.name = name;
        event.partID = partID;
        event.part = part;

        events.push_back(event);
    }

    return events;
}

SampleSoundData Sample::toSound() const
{
    SampleSoundData data;
    data.samples = vector<f32>(sampleCount(), 0);

    for (const auto& [ id, part ] : _parts)
    {
        mix(data.samples, (part.start() / 1000.0) * soundFrequency, part.toSound().samples);
    }

    return data;
}

SamplePartID Sample::add()
{
    SamplePartID id = nextPartID++;

    SamplePart part(id);

    add(part);

    return id;
}

void Sample::add(const SamplePart& part)
{
    if (part.id() > nextPartID)
    {
        nextPartID = part.id();
    }

    _parts.insert_or_assign(part.id(), part);
}

void Sample::remove(SamplePartID id)
{
    _parts.erase(id);
}

const map<SamplePartID, SamplePart>& Sample::parts() const
{
    return _parts;
}

u32 Sample::length() const
{
    u32 length = 0;

    for (const auto& [ id, part ] : _parts)
    {
        if (part.start() + part.length() > length)
        {
            length = part.start() + part.length();
        }
    }

    return length;
}

u32 Sample::sampleCount() const
{
    return ceil(soundFrequency * (length() / 1000.0));
}

bool Sample::operator==(const Sample& rhs) const
{
    return nextPartID == rhs.nextPartID &&
        _parts == rhs._parts;
}

bool Sample::operator!=(const Sample& rhs) const
{
    return !(*this == rhs);
}

template<>
Sample YAMLNode::convert() const
{
    Sample sample;

    sample.nextPartID = at("nextPartID").as<SamplePartID>();
    sample._parts = at("parts").as<map<SamplePartID, SamplePart>>();

    return sample;
}

template<>
void YAMLSerializer::emit(Sample v)
{
    startMapping();

    emitPair("nextPartID", v.nextPartID);
    emitPair("parts", v._parts);

    endMapping();
}
