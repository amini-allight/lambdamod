/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "vr_grip_interactions.hpp"
#include "world.hpp"

static constexpr f64 pullDuration = 1;

void vrPull(
    User* user,
    Entity* host,
    const BodyPart* anchor,
    GripState& state,
    Entity* target
)
{
    vec3 srcPos = target->globalPosition();
    vec3 srcLinV = target->linearVelocity();

    srcPos += srcPos + (srcLinV * pullDuration);

    vec3 dstPos = host->globalTransform().applyToPosition(anchor->regionalPosition());
    vec3 dstLinV = host->linearVelocity();

    dstPos += dstPos + (dstLinV * pullDuration);

    vec3 linV = (dstPos - srcPos) / pullDuration;

    linV += -host->world()->gravity(host->globalPosition()) * pullDuration;

    target->setLinearVelocity(srcLinV + linV);
}

void vrGrip(
    User* user,
    Entity* host,
    const BodyPart* anchor,
    GripState& state,
    Entity* target
)
{
    vec3 worldHandPosition = host->globalTransform().applyToPosition(anchor->regionalPosition());

    const BodyPart* gripAnchor = nullptr;
    f64 gripAnchorDistance = numeric_limits<f64>::max();

    target->body().traverse([&](const BodyPart* part) -> void {
        if (part->type() != Body_Part_Anchor ||
            part->get<BodyPartAnchor>().type != Body_Anchor_Grip
        )
        {
            return;
        }

        vec3 worldGripPosition = target->globalTransform().applyToPosition(part->regionalPosition());

        f64 distance = worldGripPosition.distance(worldHandPosition);

        if (distance < gripAnchorDistance)
        {
            gripAnchor = part;
            gripAnchorDistance = distance;
        }
    });

    state.grip(target->id());

    host->world()->setParent(target, host, anchor->id());
    target = host->get(state.entityID());

    if (!gripAnchor)
    {
        target->setGlobalTransform(target->globalTransform());
    }
    else
    {
        target->setLocalTransform(gripAnchor->regionalTransform());
    }
}

void vrRelease(
    User* user,
    Entity* host,
    const BodyPart* anchor,
    GripState& state
)
{
    Velocities velocities = state.exitVelocity();

    if (Entity* target = host->world()->get(state.entityID()))
    {
        host->world()->clearParent(target);

        target = host->world()->get(state.entityID());
        target->setLinearVelocity(velocities.linearVelocity);
        target->setAngularVelocity(velocities.angularVelocity);
    }

    state.release();
}
