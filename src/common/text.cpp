/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "text.hpp"
#include "yaml_tools.hpp"

string textTypeToString(TextType type)
{
    switch (type)
    {
    case Text_Signal :
        return "text-signal";
    case Text_Titlecard :
        return "text-titlecard";
    case Text_Timeout :
        return "text-timeout";
    case Text_Unary :
        return "text-unary";
    case Text_Binary :
        return "text-binary";
    case Text_Options :
        return "text-options";
    case Text_Choose :
        return "text-choose";
    case Text_Assign_Values :
        return "text-assign-values";
    case Text_Spend_Points :
        return "text-spend-points";
    case Text_Assign_Points :
        return "text-assign-points";
    case Text_Vote :
        return "text-vote";
    case Text_Choose_Major :
        return "text-choose-major";
    case Text_Knowledge :
        return "text-knowledge";
    case Text_Clear :
        return "text-clear";
    default :
        fatal("Encountered unknown text type '" + to_string(type) + "'.");
    }
}

TextType textTypeFromString(const string& s)
{
    if (s == "text-signal")
    {
        return Text_Signal;
    }
    else if (s == "text-titlecard")
    {
        return Text_Titlecard;
    }
    else if (s == "text-timeout")
    {
        return Text_Timeout;
    }
    else if (s == "text-unary")
    {
        return Text_Unary;
    }
    else if (s == "text-binary")
    {
        return Text_Binary;
    }
    else if (s == "text-options")
    {
        return Text_Options;
    }
    else if (s == "text-choose")
    {
        return Text_Choose;
    }
    else if (s == "text-assign-values")
    {
        return Text_Assign_Values;
    }
    else if (s == "text-spend-points")
    {
        return Text_Spend_Points;
    }
    else if (s == "text-assign-points")
    {
        return Text_Assign_Points;
    }
    else if (s == "text-vote")
    {
        return Text_Vote;
    }
    else if (s == "text-choose-major")
    {
        return Text_Choose_Major;
    }
    else if (s == "text-knowledge")
    {
        return Text_Knowledge;
    }
    else if (s == "text-clear")
    {
        return Text_Clear;
    }
    else
    {
        fatal("Encountered unknown text type '" + s + "'.");
    }
}

template<>
TextType YAMLNode::convert() const
{
    return textTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(TextType v)
{
    emit(textTypeToString(v));
}

TextResponse::TextResponse()
{
    _type = Text_Unary;
    _data = TextUnaryResponse();
}

template<>
TextResponse::TextResponse(TextID id, const TextUnaryResponse& data)
{
    _id = id;
    _type = Text_Unary;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextBinaryResponse& data)
{
    _id = id;
    _type = Text_Binary;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextOptionsResponse& data)
{
    _id = id;
    _type = Text_Options;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextChooseResponse& data)
{
    _id = id;
    _type = Text_Choose;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextAssignValuesResponse& data)
{
    _id = id;
    _type = Text_Assign_Values;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextSpendPointsResponse& data)
{
    _id = id;
    _type = Text_Spend_Points;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextAssignPointsResponse& data)
{
    _id = id;
    _type = Text_Assign_Points;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextVoteResponse& data)
{
    _id = id;
    _type = Text_Vote;
    _data = data;
}

template<>
TextResponse::TextResponse(TextID id, const TextChooseMajorResponse& data)
{
    _id = id;
    _type = Text_Choose_Major;
    _data = data;
}

TextID TextResponse::id() const
{
    return _id;
}

TextType TextResponse::type() const
{
    return _type;
}

TextRequest::TextRequest()
{
    _type = Text_Signal;
    _data = TextSignalRequest();
}

template<>
TextRequest::TextRequest(TextID id, const TextSignalRequest& data)
{
    _id = id;
    _type = Text_Signal;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextTitlecardRequest& data)
{
    _id = id;
    _type = Text_Titlecard;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextTimeoutRequest& data)
{
    _id = id;
    _type = Text_Timeout;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextUnaryRequest& data)
{
    _id = id;
    _type = Text_Unary;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextBinaryRequest& data)
{
    _id = id;
    _type = Text_Binary;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextOptionsRequest& data)
{
    _id = id;
    _type = Text_Options;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextChooseRequest& data)
{
    _id = id;
    _type = Text_Choose;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextAssignValuesRequest& data)
{
    _id = id;
    _type = Text_Assign_Values;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextSpendPointsRequest& data)
{
    _id = id;
    _type = Text_Spend_Points;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextAssignPointsRequest& data)
{
    _id = id;
    _type = Text_Assign_Points;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextVoteRequest& data)
{
    _id = id;
    _type = Text_Vote;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextChooseMajorRequest& data)
{
    _id = id;
    _type = Text_Choose_Major;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextKnowledgeRequest& data)
{
    _id = id;
    _type = Text_Knowledge;
    _data = data;
}

template<>
TextRequest::TextRequest(TextID id, const TextClearRequest& data)
{
    _id = id;
    _type = Text_Clear;
    _data = data;
}

TextID TextRequest::id() const
{
    return _id;
}

TextType TextRequest::type() const
{
    return _type;
}

bool TextRequest::valid() const
{
    switch (_type)
    {
    default :
        return false;
    case Text_Signal :
        return get<TextSignalRequest>(_data).valid();
    case Text_Titlecard :
        return get<TextTitlecardRequest>(_data).valid();
    case Text_Timeout :
        return get<TextTimeoutRequest>(_data).valid();
    case Text_Unary :
        return get<TextUnaryRequest>(_data).valid();
    case Text_Binary :
        return get<TextBinaryRequest>(_data).valid();
    case Text_Options :
        return get<TextOptionsRequest>(_data).valid();
    case Text_Choose :
        return get<TextChooseRequest>(_data).valid();
    case Text_Assign_Values :
        return get<TextAssignValuesRequest>(_data).valid();
    case Text_Spend_Points :
        return get<TextSpendPointsRequest>(_data).valid();
    case Text_Assign_Points :
        return get<TextAssignPointsRequest>(_data).valid();
    case Text_Vote :
        return get<TextVoteRequest>(_data).valid();
    case Text_Choose_Major :
        return get<TextChooseMajorRequest>(_data).valid();
    case Text_Knowledge :
        return get<TextKnowledgeRequest>(_data).valid();
    case Text_Clear :
        return get<TextClearRequest>(_data).valid();
    }
}
