/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_tools.hpp"
#include "log.hpp"
#include "user_closure.hpp"
#include "user.hpp"

#if defined(LMOD_SERVER)
#include "server/global.hpp"
#elif defined(LMOD_CLIENT)
#include "client/global.hpp"
#include "client/controller.hpp"
#include "client/control_context.hpp"
#endif

Value symbol(const string& s)
{
    Value value;
    value.type = Value_Symbol;
    value.s = s;

    return value;
}

Value symbolListValue(const vector<string>& l)
{
    Value v;
    v.type = Value_List;

    for (const string& i : l)
    {
        v.l.push_back(symbol(i));
    }

    return v;
}

Value symbolListValue(const set<string>& l)
{
    Value v;
    v.type = Value_List;

    for (const string& i : l)
    {
        v.l.push_back(symbol(i));
    }

    return v;
}

template<>
Value toValue(const Value& v)
{
    return v;
}

template<>
Value toValue(const Lambda& lambda)
{
    Value v;
    v.type = Value_Lambda;
    v.lambda = lambda;

    return v;
}

template<>
Value toValue(const bool& b)
{
    Value v;
    v.type = Value_Integer;
    v.i = b;

    return v;
}

template<>
Value toValue(const string& s)
{
    Value v;
    v.type = Value_String;
    v.s = s;

    return v;
}

template<>
Value toValue(const f64& f)
{
    Value v;
    v.type = Value_Float;
    v.f = f;

    return v;
}

template<>
Value toValue(const i32& i)
{
    Value v;
    v.type = Value_Integer;
    v.i = i;

    return v;
}

template<>
Value toValue(const u32& u)
{
    Value v;
    v.type = Value_Integer;
    v.i = u;

    return v;
}

template<>
Value toValue(const i64& i)
{
    Value v;
    v.type = Value_Integer;
    v.i = i;

    return v;
}

template<>
Value toValue(const u64& u)
{
    Value v;
    v.type = Value_Integer;
    v.i = u;

    return v;
}

template<>
Value toValue(const vec2& v)
{
    Value ret;
    ret.type = Value_List;

    Value x;
    x.type = Value_Float;
    x.f = v.x;

    Value y;
    y.type = Value_Float;
    y.f = v.y;

    ret.l = { x, y };

    return ret;
}

template<>
Value toValue(const vec3& v)
{
    Value ret;
    ret.type = Value_List;

    Value x;
    x.type = Value_Float;
    x.f = v.x;

    Value y;
    y.type = Value_Float;
    y.f = v.y;

    Value z;
    z.type = Value_Float;
    z.f = v.z;

    ret.l = { x, y, z };

    return ret;
}

template<>
Value toValue(const vec4& v)
{
    Value ret;
    ret.type = Value_List;

    Value x;
    x.type = Value_Float;
    x.f = v.x;

    Value y;
    y.type = Value_Float;
    y.f = v.y;

    Value z;
    z.type = Value_Float;
    z.f = v.z;

    Value w;
    w.type = Value_Float;
    w.f = v.w;

    ret.l = { x, y, z, w };

    return ret;
}

template<>
Value toValue(const quaternion& q)
{
    Value ret;
    ret.type = Value_List;

    Value w;
    w.type = Value_Float;
    w.f = q.w;

    Value x;
    x.type = Value_Float;
    x.f = q.x;

    Value y;
    y.type = Value_Float;
    y.f = q.y;

    Value z;
    z.type = Value_Float;
    z.f = q.z;

    ret.l = { w, x, y, z };

    return ret;
}

template<>
Value toValue(const UserID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const NetID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const EntityID& id)
{
    Value v;
    v.type = Value_Handle;
    v.id = id;

    return v;
}

template<>
Value toValue(const BodyPartID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const BodyPartSubID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const SamplePartID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const ActionPartID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const SamplePlaybackID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const ActionPlaybackID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const TimerID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

template<>
Value toValue(const TextID& id)
{
    Value v;
    v.type = Value_Integer;
    v.i = id.value();

    return v;
}

Value toValue(const char* s)
{
    Value value;
    value.type = Value_String;
    value.s = s;

    return value;
}

template<>
Value quote(const Value& value)
{
    if (value.type != Value_List && value.type != Value_Symbol)
    {
        return value;
    }

    Value quote;
    quote.type = Value_Symbol;
    quote.s = "quote";

    Value result;
    result.type = Value_List;
    result.l = {
        quote,
        value
    };

    return result;
}

template<>
vec2 fromValue(const Value& value)
{
    return {
        value.l[0].type == Value_Float ? value.l[0].f : value.l[0].i,
        value.l[1].type == Value_Float ? value.l[1].f : value.l[1].i
    };
}

template<>
vec3 fromValue(const Value& value)
{
    return {
        value.l[0].type == Value_Float ? value.l[0].f : value.l[0].i,
        value.l[1].type == Value_Float ? value.l[1].f : value.l[1].i,
        value.l[2].type == Value_Float ? value.l[2].f : value.l[2].i
    };
}

template<>
vec4 fromValue(const Value& value)
{
    return {
        value.l[0].type == Value_Float ? value.l[0].f : value.l[0].i,
        value.l[1].type == Value_Float ? value.l[1].f : value.l[1].i,
        value.l[2].type == Value_Float ? value.l[2].f : value.l[2].i,
        value.l[3].type == Value_Float ? value.l[3].f : value.l[3].i
    };
}

template<>
quaternion fromValue(const Value& value)
{
    return {
        value.l[0].type == Value_Float ? value.l[0].f : value.l[0].i,
        value.l[1].type == Value_Float ? value.l[1].f : value.l[1].i,
        value.l[2].type == Value_Float ? value.l[2].f : value.l[2].i,
        value.l[3].type == Value_Float ? value.l[3].f : value.l[3].i
    };
}

bool isVec2(const Value& value)
{
    return value.type == Value_List &&
        value.l.size() == 2 &&
        (value.l[0].type == Value_Integer || value.l[0].type == Value_Float) &&
        (value.l[1].type == Value_Integer || value.l[1].type == Value_Float);
}

bool isVec3(const Value& value)
{
    return value.type == Value_List &&
        value.l.size() == 3 &&
        (value.l[0].type == Value_Integer || value.l[0].type == Value_Float) &&
        (value.l[1].type == Value_Integer || value.l[1].type == Value_Float) &&
        (value.l[2].type == Value_Integer || value.l[2].type == Value_Float);
}

bool isVec4(const Value& value)
{
    return value.type == Value_List &&
        value.l.size() == 4 &&
        (value.l[0].type == Value_Integer || value.l[0].type == Value_Float) &&
        (value.l[1].type == Value_Integer || value.l[1].type == Value_Float) &&
        (value.l[2].type == Value_Integer || value.l[2].type == Value_Float) &&
        (value.l[3].type == Value_Integer || value.l[3].type == Value_Float);
}

bool isQuaternion(const Value& value)
{
    return value.type == Value_List &&
        value.l.size() == 4 &&
        (value.l[0].type == Value_Integer || value.l[0].type == Value_Float) &&
        (value.l[1].type == Value_Integer || value.l[1].type == Value_Float) &&
        (value.l[2].type == Value_Integer || value.l[2].type == Value_Float) &&
        (value.l[3].type == Value_Integer || value.l[3].type == Value_Float);
}

bool isListOf(const Value& value, ValueType type)
{
    return value.type == Value_List && all_of(
        value.l.begin(),
        value.l.end(),
        [&](const Value& item) -> bool { return item.type == type; }
    );
}

bool isTableOf(const Value& value, ValueType keyType, ValueType valueType)
{
    return value.type == Value_List && all_of(
        value.l.begin(),
        value.l.end(),
        [&](const Value& item) -> bool
        {
            return item.type == Value_List &&
                item.l.size() == 2 &&
                item.l[0].type == keyType &&
                item.l[1].type == valueType;
        }
    );
}

bool isTable(const Value& value)
{
    if (value.type != Value_List)
    {
        return false;
    }

    for (const Value& item : value.l)
    {
        if (item.type != Value_List || item.l.size() != 2)
        {
            return false;
        }
    }

    return true;
}

static void reportError(ScriptContext* context, const string& action, const Value& e, User* user)
{
#if defined(LMOD_SERVER)
    auto error = [&](const string& s) -> void
    {
        if (user)
        {
            user->error(s);
        }
        else
        {
            ::error(s);
        }
    };
#endif

    error("Traceback (most recent call last):");

    for (const string& name : context->traceback())
    {
        error("  " + name);
    }

    error("Script error " + action + ": " + e.toString());

    context->clearTraceback();
}

static void reportError(ScriptContext* context, const string& action, const exception& e, User* user)
{
#if defined(LMOD_SERVER)
    auto error = [&](const string& s) -> void
    {
        if (user)
        {
            user->error(s);
        }
        else
        {
            ::error(s);
        }
    };
#endif

    error("Traceback (most recent call last):");

    for (const string& name : context->traceback())
    {
        error("  " + name);
    }

    error("Script error " + action + ": " + e.what());

    context->clearTraceback();
}

#if defined(LMOD_CLIENT)
static void reportError(ScriptContext* context, const string& action, const Value& e, Controller* controller)
{
    auto error = [&](const string& s) -> void
    {
        if (controller)
        {
            controller->context()->interface()->log(Message(Message_Error, "", s));
        }
        else
        {
            ::error(s);
        }
    };

    error("Traceback (most recent call last):");

    for (const string& name : context->traceback())
    {
        error("  " + name);
    }

    error("Script error " + action + ": " + e.toString());

    context->clearTraceback();
}

static void reportError(ScriptContext* context, const string& action, const exception& e, Controller* controller)
{
    auto error = [&](const string& s) -> void
    {
        if (controller)
        {
            controller->context()->interface()->log(Message(Message_Error, "", s));
        }
        else
        {
            ::error(s);
        }
    };

    error("Traceback (most recent call last):");

    for (const string& name : context->traceback())
    {
        error("  " + name);
    }

    error("Script error " + action + ": " + e.what());

    context->clearTraceback();
}
#endif

Value listValue()
{
    Value v;
    v.type = Value_List;

    return v;
}

optional<vector<Value>> runSafely(ScriptContext* context, const string& s, const string& action, User* user)
{
    try
    {
        UserClosure closure(context, user);

        return context->run(s);
    }
    catch (const Value& e)
    {
        reportError(context, action, e, user);
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, user);
        return {};
    }
}

optional<vector<Value>> runSafely(ScriptContext* context, const vector<Value>& values, const string& action, User* user)
{
    try
    {
        UserClosure closure(context, user);

        vector<Value> result;
        result.reserve(values.size());

        for (const Value& value : values)
        {
            result.push_back(context->run(value));
        }

        return result;
    }
    catch (const Value& e)
    {
        reportError(context, action, e, user);
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, user);
        return {};
    }
}

optional<Value> runSafely(ScriptContext* context, const Lambda& lambda, const vector<Value>& args, const string& action, User* user)
{
    try
    {
        UserClosure closure(context, user);

        return context->run(lambda, args);
    }
    catch (const Value& e)
    {
        reportError(context, action, e, user);
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, user);
        return {};
    }
}

optional<vector<Value>> runSafely(ScriptContext* context, const string& s, const string& action)
{
    try
    {
        return context->run(s);
    }
    catch (const Value& e)
    {
        reportError(context, action, e, static_cast<User*>(nullptr));
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, static_cast<User*>(nullptr));
        return {};
    }
}

optional<vector<Value>> runSafely(ScriptContext* context, const vector<Value>& values, const string& action)
{
    try
    {
        vector<Value> result;
        result.reserve(values.size());

        for (const Value& value : values)
        {
            result.push_back(context->run(value));
        }

        return result;
    }
    catch (const Value& e)
    {
        reportError(context, action, e, static_cast<User*>(nullptr));
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, static_cast<User*>(nullptr));
        return {};
    }
}

optional<Value> runSafely(ScriptContext* context, const Lambda& lambda, const vector<Value>& args, const string& action)
{
    try
    {
        return context->run(lambda, args);
    }
    catch (const Value& e)
    {
        reportError(context, action, e, static_cast<User*>(nullptr));
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, static_cast<User*>(nullptr));
        return {};
    }
}

#if defined(LMOD_CLIENT)
optional<vector<Value>> runSafely(ScriptContext* context, const string& s, const string& action, Controller* controller)
{
    try
    {
        return context->run(s);
    }
    catch (const Value& e)
    {
        reportError(context, action, e, controller);
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, controller);
        return {};
    }
}

optional<vector<Value>> runSafely(ScriptContext* context, const vector<Value>& values, const string& action, Controller* controller)
{
    try
    {
        vector<Value> result;
        result.reserve(values.size());

        for (const Value& value : values)
        {
            result.push_back(context->run(value));
        }

        return result;
    }
    catch (const Value& e)
    {
        reportError(context, action, e, controller);
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, controller);
        return {};
    }
}

optional<Value> runSafely(ScriptContext* context, const Lambda& lambda, const vector<Value>& args, const string& action, Controller* controller)
{
    try
    {
        return context->run(lambda, args);
    }
    catch (const Value& e)
    {
        reportError(context, action, e, controller);
        return {};
    }
    catch (const exception& e)
    {
        reportError(context, action, e, controller);
        return {};
    }
}
#endif

chrono::milliseconds maxLoopDuration()
{
    return chrono::milliseconds(static_cast<u64>(g_config.maxLoopDuration * 1000));
}
