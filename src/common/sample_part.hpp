/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "yaml_tools.hpp"
#include "sample_sound_data.hpp"
#include "sample_parts.hpp"

enum SamplePartType : u8
{
    Sample_Part_Tone
};

string samplePartTypeToString(SamplePartType type);
SamplePartType samplePartTypeFromString(const string& s);

class SamplePart
{
public:
    SamplePart();
    explicit SamplePart(SamplePartID id);
    SamplePart(SamplePartID id, const SamplePart& other);

    // We still use verify rather than doing it per-function because we receive *Part types whole from the network and we need to validate that their _type and data fields match
    void verify();

    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }

    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }

    SampleSoundData toSound() const;

    u32 sampleOffset() const;
    u32 sampleCount() const;

    bool operator==(const SamplePart& rhs) const;
    bool operator!=(const SamplePart& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            _id,
            _index,
            _start,
            _length,
            _volume,
            static_cast<u8>(_type),
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        _id = std::get<0>(payload);
        _index = std::get<1>(payload);
        _start = std::get<2>(payload);
        _length = std::get<3>(payload);
        _volume = std::get<4>(payload);
        _type = static_cast<SamplePartType>(std::get<5>(payload));
        dataFromString(std::get<6>(payload));
    }

    SamplePartID id() const;
    u32 index() const;
    u32 start() const;
    u32 length() const;
    f64 volume() const;
    SamplePartType type() const;

    void setIndex(u32 index);
    void setStart(u32 start);
    void setLength(u32 length);
    void setVolume(f64 volume);

    // Special setter with side effect
    void setType(SamplePartType type);

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    SamplePartID _id;
    u32 _index;
    u32 _start;
    u32 _length;
    f64 _volume;
    SamplePartType _type;
    variant<SamplePartTone> data;

    typedef msgpack::type::tuple<SamplePartID, u32, u32, u32, f64, u8, string> layout;

    string dataToString() const;
    void dataFromString(const string& s);
};
