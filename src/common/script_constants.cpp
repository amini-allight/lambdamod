/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_constants.hpp"
#include "script_tools.hpp"

const map<string, Value> constants = {
    { "void", Value() },
    { "nullptr", toValue(EntityID()) },
    { "no-user", toValue(0) },
    { "true", toValue(1) },
    { "false", toValue(0) },
    { "pi", toValue(3.141592) },
    { "tau", toValue(6.283184) },
    { "phi", toValue(1.618033) },
    { "euler", toValue(2.718281) },
    { "forward", toValue(vec3(0, 1, 0)) },
    { "back", toValue(vec3(0, -1, 0)) },
    { "left", toValue(vec3(-1, 0, 0)) },
    { "right", toValue(vec3(1, 0, 0)) },
    { "up", toValue(vec3(0, 0, 1)) },
    { "down", toValue(vec3(0, 0, -1)) },
    { "type-void", toValue(0) },
    { "type-symbol", toValue(1) },
    { "type-integer", toValue(2) },
    { "type-float", toValue(3) },
    { "type-string", toValue(4) },
    { "type-list", toValue(5) },
    { "type-lambda", toValue(6) },
    { "type-handle", toValue(7) }
};
