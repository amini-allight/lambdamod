/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "network_tools.hpp"

#if defined(__unix__) || defined(__APPLE__)
#include <arpa/inet.h>
#include <sys/socket.h>
#elif defined(WIN32)
#include <ws2tcpip.h>
#endif

bool isWildcardIPv4Address(const string& address)
{
    u8 output[128]{0};

    int success = inet_pton(AF_INET, address.c_str(), output);

    if (!success)
    {
        return false;
    }

    bool wildcard = true;

    for (int i = 0; i < 128; i++)
    {
        if (output[i] != 0)
        {
            wildcard = false;
            break;
        }
    }

    return wildcard;
}

bool isWildcardIPv6Address(const string& address)
{
    u8 output[128]{0};

    int success = inet_pton(AF_INET6, address.c_str(), output);

    if (!success)
    {
        return false;
    }

    bool wildcard = true;

    for (int i = 0; i < 128; i++)
    {
        if (output[i] != 0)
        {
            wildcard = false;
            break;
        }
    }

    return wildcard;
}
