/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "bezier.hpp"
#include "tools.hpp"

vec2 cubicBezierPosition(const vec2& p0, const vec2& p1, const vec2& p2, const vec2& p3, f64 t)
{
    return pow(1 - t, 3)*p0 + 3*sq(1 - t)*t*p1 + 3*(1 - t)*sq(t)*p2 + pow(t, 3)*p3;
}

vec2 cubicBezierDirection(const vec2& p0, const vec2& p1, const vec2& p2, const vec2& p3, f64 t)
{
    return 3*sq(1 - t)*(p1 - p0) + 6*(1 - t)*t*(p2 - p1) + 3*sq(t)*(p3 - p2);
}

f64 cubicBezierProximity(const vec2& p0, const vec2& p1, const vec2& p2, const vec2& p3, const vec2& point, size_t curveResolution)
{
    f64 closest = numeric_limits<f64>::max();

    for (size_t i = 0; i < curveResolution; i++)
    {
        f64 t = i / static_cast<f64>(curveResolution);

        vec2 p = cubicBezierPosition(p0, p1, p2, p3, t);

        closest = min(closest, p.distance(point));
    }

    return closest;
}
