/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

class PerlinNoise
{
public:
    PerlinNoise(u64 seed, f64 frequency, u32 depth);

    f64 at(const vec2& p) const;

private:
    static constexpr u32 size = 256;
    f64 frequency;
    u32 depth;
    array<u8, size * 2> permutationTable;

    f64 noise2D(const vec2& position) const;
    u8 noise2D(const ivec2& position) const;

    f64 lerp(f64 a, f64 b, f64 t) const;
    f64 serp(f64 a, f64 b, f64 t) const;
};
