/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "terrain.hpp"

struct TerrainVertex
{
    TerrainVertex();
    TerrainVertex(const fvec3& position, const fvec3& normal, const fvec4& color);

    bool operator==(const TerrainVertex& rhs) const;
    bool operator!=(const TerrainVertex& rhs) const;

    fvec3 position;
    fvec3 normal;
    fvec4 color;
};

struct TerrainMesh
{
    TerrainMesh();
    TerrainMesh(const vector<TerrainVertex>& vertices, const vector<u32>& indices);

    void add(const TerrainMesh& rhs);

    bool operator==(const TerrainMesh& rhs) const;
    bool operator!=(const TerrainMesh& rhs) const;

    vector<TerrainVertex> vertices;
    vector<u32> indices;
};

TerrainMesh buildTerrain(const EmissiveColor& color, const vector<TerrainNode>& nodes);
