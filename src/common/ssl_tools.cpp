/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "ssl_tools.hpp"

#include <openssl/err.h>
#include <openssl/bio.h>
#include <openssl/ssl.h>

static bool initialized = false;

void initOpenSSL()
{
    if (initialized)
    {
        return;
    }

    SSL_load_error_strings();
    ERR_load_crypto_strings();
    SSL_library_init();

    initialized = true;
}

string getOpenSSLError()
{
    BIO* bio = BIO_new(BIO_s_mem());

    ERR_print_errors(bio);

    string s(BIO_pending(bio), '\0');
    BIO_read(bio, s.data(), s.size());

    BIO_free_all(bio);

    return s;
}
