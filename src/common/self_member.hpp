/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

// Allows a class to contain a copy of its own type that behaves like a normal member
template<typename T>
class SelfMember
{
public:
    SelfMember()
        : value(nullptr)
    {

    }

    explicit SelfMember(const T& value)
        : value(new T(value))
    {

    }

    SelfMember(const SelfMember& rhs)
        : value(nullptr)
    {
        if (rhs.value)
        {
            value = new T(*rhs.value);
        }
    }

    SelfMember(SelfMember&& rhs) noexcept
        : value(nullptr)
    {
        swap(value, rhs.value);
    }

    ~SelfMember()
    {
        if (value)
        {
            delete value;
        }
    }

    SelfMember& operator=(const SelfMember& rhs)
    {
        if (&rhs != this)
        {
            if (value)
            {
                delete value;
                value = nullptr;
            }

            if (rhs.value)
            {
                value = new T(*rhs.value);
            }
        }

        return *this;
    }

    SelfMember& operator=(SelfMember&& rhs) noexcept
    {
        if (&rhs != this)
        {
            swap(value, rhs.value);
        }

        return *this;
    }

    bool operator==(const SelfMember& rhs) const
    {
        if ((value && !rhs.value) || (!value && rhs.value))
        {
            return false;
        }

        return *value == *rhs.value;
    }

    bool operator!=(const SelfMember& rhs) const
    {
        return !(*this == rhs);
    }

    bool operator>(const SelfMember& rhs) const
    {
        if (value && !rhs.value)
        {
            return true;
        }
        else if (!value && rhs.value)
        {
            return false;
        }

        return value > rhs.value;
    }

    bool operator<(const SelfMember& rhs) const
    {
        if (value && !rhs.value)
        {
            return false;
        }
        else if (!value && rhs.value)
        {
            return true;
        }

        return value < rhs.value;
    }

    bool operator>=(const SelfMember& rhs) const
    {
        return *this > rhs || *this == rhs;
    }

    bool operator<=(const SelfMember& rhs) const
    {
        return *this < rhs || *this == rhs;
    }

    strong_ordering operator<=>(const SelfMember& rhs) const
    {
        if (*this == rhs)
        {
            return strong_ordering::equal;
        }
        else if (*this > rhs)
        {
            return strong_ordering::greater;
        }
        else
        {
            return strong_ordering::less;
        }
    }

    operator bool() const
    {
        return value;
    }

    T& operator*()
    {
        return *value;
    }

    const T& operator*() const
    {
        return *value;
    }

    T* operator->()
    {
        return value;
    }

    const T* operator->() const
    {
        return value;
    }

private:
    T* value;
};
