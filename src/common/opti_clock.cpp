/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "opti_clock.hpp"
#include "tools.hpp"

OptiClock::OptiClock(const string& name)
    : name(name)
    , lastTime(currentTime())
{

}

OptiClock::~OptiClock()
{
    done();

    for (size_t i = 0; i < durations.size(); i++)
    {
        auto [ name, duration ] = durations[i];

        cout << name << ": " << duration.count() << " ms";

        if (i + 1 == durations.size())
        {
            cout << endl;
        }
        else
        {
            cout << " ";
        }
    }
}

void OptiClock::punch(const string& name)
{
    done();

    this->name = name;
    lastTime = currentTime();
}

void OptiClock::done()
{
    durations.push_back({ name, currentTime() - lastTime });
}
