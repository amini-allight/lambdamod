/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "message.hpp"

// TODO: LOCALIZATION: Server-side scripting errors

inline Message userNotFoundError(UserID id)
{
    return Message(Message_Error, "", "error-user-not-found", { to_string(id.value()) });
}

inline Message entityFoundError(EntityID id)
{
    return Message(Message_Error, "", "error-entity-found", { to_string(id.value()) });
}

inline Message entityNotFoundError(EntityID id)
{
    return Message(Message_Error, "", "error-entity-not-found", { to_string(id.value()) });
}

inline Message checkpointFoundError(const string& name)
{
    return Message(Message_Error, "", "error-checkpoint-found", { name });
}

inline Message checkpointNotFoundError(const string& name)
{
    return Message(Message_Error, "", "error-checkpoint-not-found", { name });
}

inline Message worldFoundError(const string& name)
{
    return Message(Message_Error, "", "error-world-found", { name });
}

inline Message worldNotFoundError(const string& name)
{
    return Message(Message_Error, "", "error-world-not-found", { name });
}

inline Message teamNotFoundError(const string& name)
{
    return Message(Message_Error, "", "error-team-not-found", { name });
}

inline Message invalidNameError(const string& name)
{
    return Message(Message_Error, "", "error-invalid-name", { name });
}

inline Message userLoggedInError(const string& name)
{
    return Message(Message_Error, "", "error-user-logged-in", { name });
}

inline Message userNotLoggedInError(const string& name)
{
    return Message(Message_Error, "", "error-user-not-logged-in", { name });
}

inline string permissionDeniedError(const string& action)
{
    return "error-permission-denied-" + action;
}

inline Message worldDeletionBlockedError(const string& name)
{
    return Message(Message_Error, "", "error-world-deletion-blocked", { name });
}

inline string alreadyHasAttachedUserError()
{
    return "error-already-has-attached-user";
}

inline string alreadyAttachedError()
{
    return "error-already-attached";
}

inline string shiftWhileAttachedError()
{
    return "error-shift-while-attached";
}

inline string notAttachedError()
{
    return "error-not-attached";
}

inline string shiftWhileTetheredError()
{
    return "error-shift-while-tethered";
}

inline string shiftWhileConfinedError()
{
    return "error-shift-while-confined";
}

inline string alreadyAnchoredError()
{
    return "error-already-anchored";
}

inline string notAnchoredError()
{
    return "error-not-anchored";
}

inline string alreadyBrakingError()
{
    return "error-already-braking";
}

inline string notBrakingError()
{
    return "error-not-braking";
}

inline string notInVRError()
{
    return "error-not-in-vr";
}

inline string differentWorldsError()
{
    return "error-different-worlds";
}

inline string entityNotActiveError()
{
    return "error-entity-not-active";
}

inline string entityNotHostError()
{
    return "error-entity-not-host";
}

inline string attachmentsLockedError()
{
    return "error-attachments-locked";
}
