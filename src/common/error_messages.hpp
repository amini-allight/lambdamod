/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

// TODO: LOCALIZATION: Server-side strings

template<typename T, int N>
string foundError(const string& name, IDType<T, N> id)
{
    return name + " " + to_string(id.value()) + " already exists.";
}

inline string foundError(const string& name, const string& key)
{
    return name + " " + key + " already exists.";
}

template<typename T, int N>
string notFoundError(const string& name, IDType<T, N> id)
{
    return name + " " + to_string(id.value()) + " not found.";
}

inline string notFoundError(const string& name, const string& key)
{
    return name + " " + key + " not found.";
}

inline string userNotFoundError(UserID id)
{
    return notFoundError("User", id);
}

inline string entityFoundError(EntityID id)
{
    return foundError("Entity", id);
}

inline string entityNotFoundError(EntityID id)
{
    return notFoundError("Entity", id);
}

inline string invalidUsernameError(const string& name)
{
    return "Invalid username '" + name + "': Usernames must be 1 - 32 characters and only contain numbers, lowercase letters and hyphens. Usernames must not contain more than one hyphen in a row or use a hypen as the first or last character. Names must not be valid numbers.";
}

inline string invalidNameError(const string& name)
{
    return "Invalid name '" + name + "': Names must contain at least one character and only contain numbers, lowercase letters and hyphens. Names must not contain more than one hyphen in a row or use a hypen as the first or last character. Names must not be valid numbers.";
}

inline string userAlreadyExistsError(const string& name)
{
    return "User " + name + " already exists.";
}

inline string userAlreadyLoggedInError(const string& name)
{
    return "User " + name + " is already logged in.";
}

inline string userLoggedInError(const string& name)
{
    return "User " + name + " is logged in.";
}

inline string userNotLoggedInError(const string& name)
{
    return "User " + name + " is not logged in.";
}

inline string permissionDeniedError(const string& action)
{
    return "Permission to " + action + " denied.";
}

inline string worldDeletionBlockedError(const string& name)
{
    return "Deletion blocked, world " + name + " is in use by a user.";
}

inline string notHostError()
{
    return "That entity is not marked as a host.";
}

inline string alreadyHasSelectingUserError()
{
    return "That entity is already selected by another user.";
}

inline string alreadyHasAttachedUserError()
{
    return "That entity already has a user attached.";
}

inline string alreadyAttachedError()
{
    return "You are already attached to an entity.";
}

inline string attachedError(const string& action)
{
    return "Cannot " + action + ", you are attached to an entity.";
}

inline string notAttachedError()
{
    return "You are not attached to an entity.";
}

inline string tetheredError(const string& action)
{
    return "Cannot " + action + ", you are tethered to other users.";
}

inline string confinedError(const string& action)
{
    return "Cannot " + action + ", you are confined.";
}

inline string alreadyAnchoredError()
{
    return "You are already anchored to the entity.";
}

inline string notAnchoredError()
{
    return "You are not anchored to the entity.";
}

inline string alreadyBrakingError()
{
    return "The brake is already engaged.";
}

inline string notBrakingError()
{
    return "The brake is not engaged.";
}

inline string notInVRError()
{
    return "You must be in VR mode to do that.";
}

inline string differentWorldsError()
{
    return "Target users must all be in the same world.";
}

inline string entityNotActiveError()
{
    return "Cannot attach, target entity is not active.";
}

inline string entityNotHostError()
{
    return "Cannot attach, target entity is not marked as a host.";
}

inline string attachmentsLockedError()
{
    return "Attachments are currently locked.";
}
