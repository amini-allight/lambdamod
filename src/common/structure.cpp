/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "structure.hpp"
#include "yaml_tools.hpp"
#include "log.hpp"
#include "tools.hpp"

StructureType::StructureType()
    : thickness(0.2)
{

}

StructureType::StructureType(StructureTypeID id)
    : StructureType()
{
    this->id = id;
}

void StructureType::verify()
{
    thickness = max(thickness, minStructureWallThickness);
}

bool StructureType::operator==(const StructureType& rhs) const
{
    return id == rhs.id && thickness == rhs.thickness;
}

bool StructureType::operator!=(const StructureType& rhs) const
{
    return !(*this == rhs);
}

template<>
StructureType YAMLNode::convert() const
{
    StructureType type;

    type.id = at("id").as<StructureTypeID>();
    type.thickness = at("thickness").as<f64>();

    return type;
}

template<>
void YAMLSerializer::emit(StructureType type)
{
    startMapping();

    emitPair("id", type.id);
    emitPair("thickness", type.thickness);

    endMapping();
}

StructureNodeType structureNodeTypeFromString(const string& name)
{
    if (name == "block")
    {
        return Structure_Node_Block;
    }
    else if (name == "rectangle")
    {
        return Structure_Node_Rectangle;
    }
    else if (name == "circle")
    {
        return Structure_Node_Circle;
    }
    else if (name == "line")
    {
        return Structure_Node_Line;
    }
    else if (name == "wall")
    {
        return Structure_Node_Wall;
    }
    else
    {
        warning("Encountered unknown structure node type '" + name + "', assuming block.");
        return Structure_Node_Block;
    }
}

string structureNodeTypeToString(StructureNodeType type)
{
    switch (type)
    {
    case Structure_Node_Block :
        return "block";
    case Structure_Node_Rectangle :
        return "rectangle";
    case Structure_Node_Circle :
        return "circle";
    case Structure_Node_Line :
        return "line";
    case Structure_Node_Wall :
        return "wall";
    default :
        warning("Encountered unknown structure node type '" + to_string(type) + "', assuming block.");
        return "block";
    }
}

StructureNode::StructureNode()
    : type(Structure_Node_Block)
    , faceIndex(0)
    , positive(true)
    , inverted(false)
    , positionA(-1, -1)
    , positionB(+1, +1)
    , radius(1)
    , length(1)
{

}

StructureNode::StructureNode(StructureNodeID id)
    : StructureNode()
{
    this->id = id;
}

void StructureNode::verify(const set<StructureTypeID>& typeIDs)
{
    // NOTE: Update this when adding new structure node types
    typeID = typeIDs.contains(typeID) ? typeID : StructureTypeID();
    type = min(type, Structure_Node_Wall);
    faceIndex = min<u32>(faceIndex, 5);
    positionA = verifyPosition(positionA);
    positionB = verifyPosition(positionB);
    radius = max(radius, numeric_limits<f64>::min());
    length = max(length, numeric_limits<f64>::min());

    for (StructureNode& child : children)
    {
        child.verify(typeIDs);
    }
}

bool StructureNode::operator==(const StructureNode& rhs) const
{
    return id == rhs.id
        && typeID == rhs.typeID
        && type == rhs.type
        && faceIndex == rhs.faceIndex
        && positive == rhs.positive
        && inverted == rhs.inverted
        && positionA == rhs.positionA
        && positionB == rhs.positionB
        && radius == rhs.radius
        && length == rhs.length
        && children == rhs.children;
}

bool StructureNode::operator!=(const StructureNode& rhs) const
{
    return !(*this == rhs);
}

StructureNode* StructureNode::getNodeByIndex(size_t* currentIndex, size_t targetIndex)
{
    if (*currentIndex == targetIndex)
    {
        return this;
    }

    (*currentIndex)++;

    for (StructureNode& node : children)
    {
        if (StructureNode* result = node.getNodeByIndex(currentIndex, targetIndex))
        {
            return result;
        }
    }

    return nullptr;
}

const StructureNode* StructureNode::getNodeByIndex(size_t* currentIndex, size_t targetIndex) const
{
    if (*currentIndex == targetIndex)
    {
        return this;
    }

    (*currentIndex)++;

    for (const StructureNode& node : children)
    {
        if (const StructureNode* result = node.getNodeByIndex(currentIndex, targetIndex))
        {
            return result;
        }
    }

    return nullptr;
}

template<>
StructureNode YAMLNode::convert() const
{
    StructureNode node;

    node.id = at("id").as<StructureNodeID>();
    node.typeID = at("typeID").as<StructureTypeID>();
    node.type = structureNodeTypeFromString(at("type").as<string>());
    node.faceIndex = at("faceIndex").as<u32>();
    node.positive = at("positive").as<bool>();
    node.inverted = at("inverted").as<bool>();
    node.positionA = at("positionA").as<vec2>();
    node.positionB = at("positionB").as<vec2>();
    node.radius = at("radius").as<f64>();
    node.length = at("length").as<f64>();
    node.children = at("children").as<vector<StructureNode>>();

    return node;
}

template<>
void YAMLSerializer::emit(StructureNode node)
{
    startMapping();

    emitPair("id", node.id);
    emitPair("typeID", node.typeID);
    emitPair("type", structureNodeTypeToString(node.type));
    emitPair("faceIndex", node.faceIndex);
    emitPair("positive", node.positive);
    emitPair("inverted", node.inverted);
    emitPair("positionA", node.positionA);
    emitPair("positionB", node.positionB);
    emitPair("radius", node.radius);
    emitPair("length", node.length);
    emitPair("children", node.children);

    endMapping();
}
