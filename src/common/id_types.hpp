/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

template<typename T, int N>
class IDType
{
public:
    constexpr IDType()
    {
        id = 0;
    }

    explicit constexpr IDType(T value)
        : id(value)
    {

    }

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            id
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
    }

    constexpr T value() const
    {
        return id;
    }

    constexpr IDType operator++(int)
    {
        IDType previous = *this;

        id++;

        return previous;
    }

    constexpr IDType operator--(int)
    {
        IDType previous = *this;

        id--;

        return previous;
    }

    constexpr IDType& operator++()
    {
        id++;

        return *this;
    }

    constexpr IDType& operator--()
    {
        id--;

        return *this;
    }

    constexpr bool operator!() const
    {
        return !id;
    }

    constexpr operator bool() const
    {
        return id;
    }

    constexpr bool operator==(const IDType& rhs) const
    {
        return id == rhs.id;
    }

    constexpr bool operator!=(const IDType& rhs) const
    {
        return id != rhs.id;
    }

    constexpr bool operator>(const IDType& rhs) const
    {
        return id > rhs.id;
    }

    constexpr bool operator<(const IDType& rhs) const
    {
        return id < rhs.id;
    }

    constexpr bool operator>=(const IDType& rhs) const
    {
        return id >= rhs.id;
    }

    constexpr bool operator<=(const IDType& rhs) const
    {
        return id <= rhs.id;
    }

    constexpr strong_ordering operator<=>(const IDType& rhs) const
    {
        return id <=> rhs.id;
    }

    typedef msgpack::type::tuple<T> layout;

    typedef T value_type;
    static constexpr const int type_index = N;

private:
    T id;
};

template<typename T, int N>
ostream& operator<<(ostream& out, IDType<T, N> id)
{
    out << id.value();

    return out;
}

typedef IDType<u32, 0> UserID;
typedef IDType<u32, 1> NetID;
typedef IDType<u64, 2> EntityID;
typedef IDType<u32, 3> BodyPartID;
typedef IDType<u32, 4> BodyPartSubID;
typedef IDType<u32, 5> SamplePartID;
typedef IDType<u32, 6> ActionPartID;
typedef IDType<u32, 7> SamplePlaybackID;
typedef IDType<u32, 8> ActionPlaybackID;
typedef IDType<u32, 9> TimerID;
typedef IDType<u32, 10> RepeaterID;
typedef IDType<u32, 11> TextID;
typedef IDType<u64, 12> RenderDecorationID;
typedef IDType<u64, 13> RenderPostProcessEffectID;
typedef IDType<u64, 14> SoundPostProcessEffectID;
typedef IDType<u32, 15> ScriptNodeID;
typedef IDType<u32, 16> StructureNodeID;
typedef IDType<u32, 17> StructureTypeID;
typedef IDType<u32, 18> TerrainNodeID;
