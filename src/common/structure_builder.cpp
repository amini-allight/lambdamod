/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "structure_builder.hpp"

StructureVertex::StructureVertex()
    : normal(forwardDir)
    , foreground(false)
{

}

StructureVertex::StructureVertex(const fvec3& position, const fvec3& normal, bool foreground)
    : position(position)
    , normal(normal)
    , foreground(foreground)
{

}

bool StructureVertex::operator==(const StructureVertex& rhs) const
{
    return position == rhs.position && normal == rhs.normal && foreground == rhs.foreground;
}

bool StructureVertex::operator!=(const StructureVertex& rhs) const
{
    return !(*this == rhs);
}

StructureMesh::StructureMesh()
{

}

bool StructureMesh::operator==(const StructureMesh& rhs) const
{
    return vertices == rhs.vertices && indices == rhs.indices;
}

bool StructureMesh::operator!=(const StructureMesh& rhs) const
{
    return !(*this == rhs);
}

StructureMesh buildStructure(const StructureNode& node)
{
    return StructureMesh();
}
