/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "field_definition_closure.hpp"
#include "scope_closure.hpp"
#include "script_builtin_tools.hpp"

FieldDefinitionClosure::FieldDefinitionClosure(ScriptContext* context, const function<void(const string&, const Value&)>& set)
    : closure(context)
{
    context->addBuiltin(
        "field",
        false,
        [&](const string& functionName, ScriptContext* context, const vector<Value>& args) -> Value
        {
            CHECK_ARGUMENT_COUNT(2);
            CHECK_ARG_SYMBOL(0);

            Value name = args[0];
            Value value = context->run(args[1]);

            set(name.s, value);

            return Value();
        }
    );

    Value builtin;
    builtin.type = Value_Lambda;
    builtin.lambda.type = Lambda_Builtin;
    builtin.lambda.builtinName = "field";

    context->define("field", builtin); 
}

FieldDefinitionClosure::~FieldDefinitionClosure()
{

}
