/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "prefabs.hpp"

void prefabDefault(Entity* entity, const mat4& transform)
{
    auto part = new BodyPart(nullptr, entity->body().nextPartID());

    part->setType(Body_Part_Solid);

    entity->body().add(BodyPartID(), part);
}

void prefabTestCross(Entity* entity, const mat4& transform)
{
    entity->body().add(BodyPartID(), new BodyPart(nullptr, entity->body().nextPartID()));
    entity->body().add(BodyPartID(), new BodyPart(nullptr, entity->body().nextPartID()));
    entity->body().add(BodyPartID(), new BodyPart(nullptr, entity->body().nextPartID()));

    BodyPart* xPart = entity->body().parts().at(BodyPartID(1));
    xPart->setType(Body_Part_Line);

    auto& xLine = xPart->get<BodyPartLine>();
    xLine.startColor = EmissiveColor(vec3(1, 1, 0), 1, false);
    xLine.endColor = EmissiveColor(vec3(1, 0, 0), 1, false);

    xPart->setLocalRotation(quaternion(downDir, tau / 4));

    BodyPart* yPart = entity->body().parts().at(BodyPartID(2));
    yPart->setType(Body_Part_Line);

    auto& yLine = yPart->get<BodyPartLine>();
    yLine.startColor = EmissiveColor(vec3(0, 1, 1), 1, false);
    yLine.endColor = EmissiveColor(vec3(0, 1, 0), 1, false);

    yPart->setLocalRotation(quaternion());

    BodyPart* zPart = entity->body().parts().at(BodyPartID(3));
    zPart->setType(Body_Part_Line);

    auto& zLine = zPart->get<BodyPartLine>();
    zLine.startColor = EmissiveColor(vec3(1, 0, 1), 1, false);
    zLine.endColor = EmissiveColor(vec3(0, 0, 1), 1, false);

    zPart->setLocalRotation(quaternion(rightDir, tau / 4));
}

void prefabHuman(Entity* entity, const mat4& transform)
{
    // TODO: PREFABS: Not implemented
}
