/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_contentlib.hpp"
#include "script_tools.hpp"
#include "script_builtin_tools.hpp"
#include "ai.hpp"

static random_device device;
static mt19937 engine(device());

static Value builtin_ai_pathfind(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_LAMBDA(2);
    CHECK_ARG_LAMBDA(3);

    Value vStart = args[0];
    Value vEnd = args[1];
    Value vGenerator = args[2];
    Value vHeuristic = args[3];

    vec3 start = fromValue<vec3>(vStart);
    vec3 end = fromValue<vec3>(vEnd);
    auto generator = [&](const vec3& point) -> vector<vec3>
    {
        Value result = context->run(composeList(vGenerator, toValue(point)));

        if (result.type != Value_List)
        {
            throw toValue("Expected list of length 3 lists of integers or floats as return value from argument 3 lambda in '" + functionName + "', got something else.");
        }

        vector<vec3> points;

        for (const Value& item : result.l)
        {
            if (!isVec3(item))
            {
                throw toValue("Expected list of length 3 lists of integers or floats as return value from argument 3 lambda in '" + functionName + "', got something else.");
            }

            points.push_back(fromValue<vec3>(item));
        }

        return points;
    };
    auto heuristic = [&](const vec3& a, const vec3& b) -> f64
    {
        Value result = context->run(composeList(vHeuristic, toValue(a), toValue(b)));

        if (result.type != Value_Integer && result.type != Value_Float)
        {
            throw toValue("Expected integer or float as return value from argument 3 lambda in '" + functionName + "', got something else.");
        }

        return result.type == Value_Integer ? result.i : result.f;
    };

    vector<vec3> path = pathfind(start, end, generator, heuristic);

    Value result;
    result.type = Value_List;

    for (const vec3& point : path)
    {
        result.l.push_back(toValue(point));
    }

    return result;
}

static Value builtin_ai_decide(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_LAMBDA(2);
    CHECK_ARG_LAMBDA(3);
    CHECK_ARG_LAMBDA(4);

    Value vDepth = args[0];
    Value vState = args[1];
    Value vGenerator = args[2];
    Value vPredictor = args[3];
    Value vHeuristic = args[4];

    u32 depth = vDepth.i;
    auto generator = [&](const Value& state, bool maximizing) -> vector<Value>
    {
        Value result = context->run(composeList(vGenerator, state, toValue(static_cast<i64>(maximizing))));

        if (result.type != Value_List)
        {
            throw toValue("Expected list as return value from argument 3 lambda in '" + functionName + "', got something else.");
        }

        return result.l;
    };
    auto predictor = [&](const Value& state, const Value& option) -> Value
    {
        return context->run(composeList(vPredictor, state, option));
    };
    auto heuristic = [&](const Value& state) -> f64
    {
        Value result = context->run(composeList(vHeuristic, state));

        if (result.type != Value_Integer && result.type != Value_Float)
        {
            throw toValue("Expected integer or float as return value from argument 5 lambda in '" + functionName + "', got something else.");
        }

        return result.type == Value_Integer ? result.i : result.f;
    };

    return decide<Value, Value>(depth, vState, generator, predictor, heuristic);
}

static Value builtin_dice(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value count = args[0];
    Value sides = args[1];

    uniform_int_distribution<i64> dist(1, sides.i);

    Value total;
    total.type = Value_Integer;
    total.i = 0;

    Value values;
    values.type = Value_List;

    for (i64 i = 0; i < count.i; i++)
    {
        Value value;
        value.type = Value_Integer;
        value.i = dist(engine);

        total.i += value.i;

        values.l.push_back(value);
    }

    Value result;
    result.type = Value_List;
    result.l = { total, values };

    return result;
}

static Value builtin_zdice(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value count = args[0];
    Value sides = args[1];

    uniform_int_distribution<i64> dist(0, sides.i - 1);

    Value total;
    total.type = Value_Integer;
    total.i = 0;

    Value values;
    values.type = Value_List;

    for (i64 i = 0; i < count.i; i++)
    {
        Value value;
        value.type = Value_Integer;
        value.i = dist(engine);

        total.i += value.i;

        values.l.push_back(value);
    }

    Value result;
    result.type = Value_List;
    result.l = { total, values };

    return result;
}

const map<string, ScriptBuiltin> contentlib = {
    makeBuiltin("ai-pathfind", { "start", "end", "generator", "heuristic" }, builtin_ai_pathfind),
    makeBuiltin("ai-decide", { "depth", "state", "generator", "predictor", "heuristic" }, builtin_ai_decide),

    makeBuiltin("dice", { "count", "sides" }, builtin_dice),
    makeBuiltin("zdice", { "count", "sides" }, builtin_zdice)
};
