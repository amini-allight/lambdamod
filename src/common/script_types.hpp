/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

class User;
struct Value;

struct Token
{
    Token(const string& text, bool isString = false);

    string text;
    bool isString;
};

enum LambdaType : u8
{
    Lambda_Normal,
    Lambda_Syntax,
    Lambda_Builtin
};

struct Lambda
{
    Lambda();

    string toString() const;

    size_t memorySize() const;

    bool operator==(const Lambda& rhs) const;
    bool operator!=(const Lambda& rhs) const;
    bool operator>(const Lambda& rhs) const;
    bool operator<(const Lambda& rhs) const;
    bool operator>=(const Lambda& rhs) const;
    bool operator<=(const Lambda& rhs) const;
    strong_ordering operator<=>(const Lambda& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const;
    void msgpack_unpack(const msgpack::object& obj);

    typedef msgpack::type::tuple<u8, vector<string>, vector<Value>, vector<Value>, string> layout;

    LambdaType type;
    vector<string> argNames;
    vector<Value> body;
    vector<Value> argLayout;
    string builtinName;
};

enum ValueType : u8
{
    Value_Void,
    Value_Symbol,
    Value_Integer,
    Value_Float,
    Value_String,
    Value_List,
    Value_Lambda,
    Value_Handle
};

struct Value
{
    Value();
    explicit Value(const Token& value);
    explicit Value(const vector<Value>& children);

    string toString() const;
    string toPrettyString(u32 indent = 0) const;

    size_t memorySize() const;

    operator bool() const;
    bool operator==(const Value& rhs) const;
    bool operator!=(const Value& rhs) const;
    bool operator>(const Value& rhs) const;
    bool operator<(const Value& rhs) const;
    bool operator>=(const Value& rhs) const;
    bool operator<=(const Value& rhs) const;
    strong_ordering operator<=>(const Value& rhs) const;

    template<typename T>
    T number() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const;
    void msgpack_unpack(const msgpack::object& obj);

    typedef msgpack::type::tuple<u8, i64, f64, string, vector<Value>, Lambda, EntityID> layout;

    ValueType type;
    i64 i;
    f64 f;
    string s;
    vector<Value> l;
    Lambda lambda;
    EntityID id;

private:
    Value number(const string& s) const;
};

template<typename T>
void Lambda::msgpack_pack(msgpack::packer<T>& packer) const
{
    layout payload = {
        static_cast<u8>(type),
        argNames,
        body,
        argLayout,
        builtinName
    };

    packer.pack(payload);
}

template<typename T>
void Value::msgpack_pack(msgpack::packer<T>& packer) const
{
    layout payload = {
        static_cast<u8>(type),
        i,
        f,
        s,
        l,
        lambda,
        id
    };

    packer.pack(payload);
}
