/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "yaml_tools.hpp"
#include "hud_elements.hpp"

enum HUDElementType : u8
{
    HUD_Element_Line,
    HUD_Element_Text,
    HUD_Element_Bar,
    HUD_Element_Symbol
};

HUDElementType hudElementTypeFromString(const string& s);
string hudElementTypeToString(HUDElementType type);

enum HUDElementAnchor : u8
{
    HUD_Element_Anchor_Center,

    HUD_Element_Anchor_Left,
    HUD_Element_Anchor_Right,
    HUD_Element_Anchor_Up,
    HUD_Element_Anchor_Down,

    HUD_Element_Anchor_Up_Left,
    HUD_Element_Anchor_Down_Left,
    HUD_Element_Anchor_Up_Right,
    HUD_Element_Anchor_Down_Right
};

HUDElementAnchor hudElementAnchorFromString(const string& s);
string hudElementAnchorToString(HUDElementAnchor anchor);

vec2 anchorToTopLeft(HUDElementAnchor anchor, const vec2& viewportSize, const vec2& position, const vec2& size);

class HUDElement
{
public:
    HUDElement();

    void verify();

    bool operator==(const HUDElement& rhs) const;
    bool operator!=(const HUDElement& rhs) const;

    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }

    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            _anchor,
            _position,
            _color,
            _type,
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        _anchor = static_cast<HUDElementAnchor>(std::get<0>(payload));
        _position = std::get<1>(payload);
        _color = std::get<2>(payload);
        _type = static_cast<HUDElementType>(std::get<3>(payload));
        dataFromString(std::get<4>(payload));
    }

    HUDElementAnchor anchor() const;
    const vec2& position() const;
    const vec3& color() const;
    HUDElementType type() const;

    void setAnchor(HUDElementAnchor anchor);
    void setPosition(const vec2& position);
    void setColor(const vec3& color);

    // Special setter with side effect
    void setType(HUDElementType type);

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    HUDElementAnchor _anchor;
    vec2 _position;
    vec3 _color;
    HUDElementType _type;
    variant<HUDElementLine, HUDElementText, HUDElementBar, HUDElementSymbol> data;

    typedef msgpack::type::tuple<u8, vec2, vec3, u8, string> layout;

    string dataToString() const;
    void dataFromString(const string& s);
};
