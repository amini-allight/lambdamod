/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script_context.hpp"

class World;
class Entity;

void checkArgumentCount(const string& name, size_t actual, size_t exact);
void checkArgumentCount(const string& name, size_t actual, size_t bound, bool upper);
void checkArgumentCount(const string& name, size_t actual, size_t lower, size_t upper);
void checkArgumentCount(const string& name, size_t actual, const vector<size_t>& options);

void checkUserAuth(const string& name, const ScriptContext* context);
void checkUserAdmin(const string& name, const ScriptContext* context);
void checkUserAllowed(const string& name, const ScriptContext* context, const Entity& entity);

void checkArgumentIsString(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsSymbol(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsNumber(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsInteger(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsFloat(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsVec2(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsVec3(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsQuaternion(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsTable(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsList(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsLambda(const string& name, const vector<Value>& args, size_t index);
void checkArgumentIsHandle(const string& name, const vector<Value>& args, size_t index);

void checkArgumentHandleValid(const string& name, const vector<Value>& args, size_t index);
void checkArgumentNameValid(const string& name, const vector<Value>& args, size_t index);

void checkArgumentIntegerRange(const string& name, const vector<Value>& args, size_t index, i64 lower, i64 upper);
void checkArgumentIntegerLess(const string& name, const vector<Value>& args, size_t index, i64 upper);
void checkArgumentIntegerLessEqual(const string& name, const vector<Value>& args, size_t index, i64 upper);
void checkArgumentIntegerGreater(const string& name, const vector<Value>& args, size_t index, i64 lower);
void checkArgumentIntegerGreaterEqual(const string& name, const vector<Value>& args, size_t index, i64 lower);

void checkArgumentFloatRange(const string& name, const vector<Value>& args, size_t index, f64 lower, f64 upper);
void checkArgumentFloatLess(const string& name, const vector<Value>& args, size_t index, f64 upper);
void checkArgumentFloatLessEqual(const string& name, const vector<Value>& args, size_t index, f64 upper);
void checkArgumentFloatGreater(const string& name, const vector<Value>& args, size_t index, f64 lower);
void checkArgumentFloatGreaterEqual(const string& name, const vector<Value>& args, size_t index, f64 lower);

void checkReturnIsNumber(const string& name, size_t index, const Value& value);
void checkReturnIsInteger(const string& name, size_t index, const Value& value);

void checkWorldExists(const string& name, const World* world);
void checkEntityExists(const string& name, const Entity* entity);
void checkUserExists(const string& name, const User* user);

vector<User*> parseUserArgument(const ScriptContext* context, const string& name, const vector<Value>& args, size_t index);

#define CHECK_ARGUMENT_COUNT(...) checkArgumentCount(functionName, args.size(), __VA_ARGS__)
#define CHECK_USER_AUTH checkUserAuth(functionName, context)
#define CHECK_USER_ADMIN checkUserAdmin(functionName, context)
#define CHECK_USER_ALLOWED(_entity) checkUserAllowed(functionName, context, _entity)

#define CHECK_ARG_STRING(_index) checkArgumentIsString(functionName, args, _index)
#define CHECK_ARG_SYMBOL(_index) checkArgumentIsSymbol(functionName, args, _index)
#define CHECK_ARG_NUMBER(_index) checkArgumentIsNumber(functionName, args, _index)
#define CHECK_ARG_INTEGER(_index) checkArgumentIsInteger(functionName, args, _index)
#define CHECK_ARG_FLOAT(_index) checkArgumentIsFloat(functionName, args, _index)
#define CHECK_ARG_VEC2(_index) checkArgumentIsVec2(functionName, args, _index)
#define CHECK_ARG_VEC3(_index) checkArgumentIsVec3(functionName, args, _index)
#define CHECK_ARG_QUATERNION(_index) checkArgumentIsQuaternion(functionName, args, _index)
#define CHECK_ARG_TABLE(_index) checkArgumentIsTable(functionName, args, _index)
#define CHECK_ARG_LIST(_index) checkArgumentIsList(functionName, args, _index)
#define CHECK_ARG_LAMBDA(_index) checkArgumentIsLambda(functionName, args, _index)
#define CHECK_ARG_HANDLE(_index) checkArgumentIsHandle(functionName, args, _index)

#define CHECK_ARG_HANDLE_VALID(_index) checkArgumentHandleValid(functionName, args, _index)
#define CHECK_ARG_NAME_VALID(_index) checkArgumentNameValid(functionName, args, _index)

#define CHECK_ARG_INTEGER_RANGE(_index, _lower, _upper) checkArgumentIntegerRange(functionName, args, _index, _lower, _upper)
#define CHECK_ARG_INTEGER_LESS(_index, _upper) checkArgumentIntegerLess(functionName, args, _index, _upper)
#define CHECK_ARG_INTEGER_LESS_EQUAL(_index, _upper) checkArgumentIntegerLessEqual(functionName, args, _index, _upper)
#define CHECK_ARG_INTEGER_GREATER(_index, _lower) checkArgumentIntegerGreater(functionName, args, _index, _lower)
#define CHECK_ARG_INTEGER_GREATER_EQUAL(_index, _lower) checkArgumentIntegerGreaterEqual(functionName, args, _index, _lower)

#define CHECK_ARG_FLOAT_RANGE(_index, _lower, _upper) checkArgumentFloatRange(functionName, args, _index, _lower, _upper)
#define CHECK_ARG_FLOAT_LESS(_index, _upper) checkArgumentFloatLess(functionName, args, _index, _upper)
#define CHECK_ARG_FLOAT_LESS_EQUAL(_index, _upper) checkArgumentFloatLessEqual(functionName, args, _index, _upper)
#define CHECK_ARG_FLOAT_GREATER(_index, _lower) checkArgumentFloatGreater(functionName, args, _index, _lower)
#define CHECK_ARG_FLOAT_GREATER_EQUAL(_index, _lower) checkArgumentFloatGreaterEqual(functionName, args, _index, _lower)

#define CHECK_RET_NUMBER(_index, _value) checkReturnIsNumber(functionName, _index, _value)
#define CHECK_RET_INTEGER(_index, _value) checkReturnIsInteger(functionName, _index, _value)

#define CHECK_WORLD_EXISTS(_world) checkWorldExists(functionName, _world)
#define CHECK_ENTITY_EXISTS(_entity) checkEntityExists(functionName, _entity)
#define CHECK_USER_EXISTS(_user) checkUserExists(functionName, _user)

#define PARSE_USER_ARG(_index) parseUserArgument(context, functionName, args, _index)
