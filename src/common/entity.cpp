/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "entity.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"
#include "world.hpp"
#include "script_tools.hpp"
#include "scope_closure.hpp"
#include "field_definition_closure.hpp"
#include "game_state.hpp"
#include "error_messages.hpp"

#define START_INITIAL_EVENT(_event_type, _type) \
{ \
    _event_type event(_type, world, id); \
    event.transformed = transformed;

#define END_INITIAL_EVENT \
    events.push_back(event); \
}

static constexpr SamplePlaybackID previewSamplePlaybackID(numeric_limits<SamplePlaybackID::value_type>::max());
static constexpr ActionPlaybackID previewActionPlaybackID(numeric_limits<ActionPlaybackID::value_type>::max());

static EntityID eventID(const NetworkEvent& event)
{
    EntityID id;

    switch (event.type())
    {
    default :
        fatal("Non-entity event " + event.name() + " was pushed to entity.");
    case Network_Event_Entity :
        id = event.payload<EntityEvent>().id;
        break;
    case Network_Event_Entity_Body :
        id = event.payload<EntityBodyEvent>().id;
        break;
    case Network_Event_Entity_Sample :
        id = event.payload<EntitySampleEvent>().id;
        break;
    case Network_Event_Entity_Sample_Playback :
        id = event.payload<EntitySamplePlaybackEvent>().id;
        break;
    case Network_Event_Entity_Action :
        id = event.payload<EntityActionEvent>().id;
        break;
    case Network_Event_Entity_Action_Playback :
        id = event.payload<EntityActionPlaybackEvent>().id;
        break;
    case Network_Event_Entity_Input_Binding :
        id = event.payload<EntityInputBindingEvent>().id;
        break;
    case Network_Event_Entity_HUD :
        id = event.payload<EntityHUDEvent>().id;
        break;
    case Network_Event_Entity_Script :
        id = event.payload<EntityScriptEvent>().id;
        break;
    }

    return id;
}

static bool eventTransformed(const NetworkEvent& event)
{
    bool transformed;

    switch (event.type())
    {
    default :
        fatal("Non-entity event " + event.name() + " was pushed to entity.");
    case Network_Event_Entity :
        transformed = event.payload<EntityEvent>().transformed;
        break;
    case Network_Event_Entity_Body :
        transformed = event.payload<EntityBodyEvent>().transformed;
        break;
    case Network_Event_Entity_Sample :
        transformed = event.payload<EntitySampleEvent>().transformed;
        break;
    case Network_Event_Entity_Sample_Playback :
        transformed = event.payload<EntitySamplePlaybackEvent>().transformed;
        break;
    case Network_Event_Entity_Action :
        transformed = event.payload<EntityActionEvent>().transformed;
        break;
    case Network_Event_Entity_Action_Playback :
        transformed = event.payload<EntityActionPlaybackEvent>().transformed;
        break;
    case Network_Event_Entity_Input_Binding :
        transformed = event.payload<EntityInputBindingEvent>().transformed;
        break;
    case Network_Event_Entity_HUD :
        transformed = event.payload<EntityHUDEvent>().transformed;
        break;
    case Network_Event_Entity_Script :
        transformed = event.payload<EntityScriptEvent>().transformed;
        break;
    }

    return transformed;
}

EntityMetaState::EntityMetaState()
    : _parentPartID(0)
    , _parentPartSubID(0)
    , _active(false)

    , _ownerID(0)
    , _shared(false)
    , _attachedUserID(0)
    , _selectingUserID(0)
{

}

void EntityMetaState::compare(const EntityMetaState& previous, EntityUpdate& update) const
{
    update.notify<BodyPartID, EntityEvent>(parentPartID(), previous.parentPartID(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Parent_Part_ID;
        event.u = parentPartID().value();
    });

    update.notify<BodyPartSubID, EntityEvent>(parentPartSubID(), previous.parentPartSubID(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Parent_Part_Sub_ID;
        event.u = parentPartSubID().value();
    });

    update.notify<string, EntityEvent>(name(), previous.name(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Name;
        event.s = name();
    });

    update.notify<bool, EntityEvent>(active(), previous.active(), [&](EntityEvent& event) -> void {
        event.type = active() ? Entity_Event_Activate : Entity_Event_Deactivate;
    });

    update.notify<mat4, EntityEvent>(localTransform(), previous.localTransform(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Transform;
        event.m4 = localTransform();
    });

    update.notify<vec3, EntityEvent>(linearVelocity(), previous.linearVelocity(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Linear_Velocity;
        event.v3 = linearVelocity();
    });

    update.notify<vec3, EntityEvent>(angularVelocity(), previous.angularVelocity(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Angular_Velocity;
        event.v3 = angularVelocity();
    });

    update.notify<UserID, EntityEvent>(ownerID(), previous.ownerID(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Owner_ID;
        event.u = ownerID().value();
    });

    update.notify<bool, EntityEvent>(shared(), previous.shared(), [&](EntityEvent& event) -> void {
        event.type = shared() ? Entity_Event_Share : Entity_Event_Unshare;
    });

    update.notify<UserID, EntityEvent>(attachedUserID(), previous.attachedUserID(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Attached_User_ID;
        event.u = attachedUserID().value();
    });

    update.notify<UserID, EntityEvent>(selectingUserID(), previous.selectingUserID(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Selecting_User_ID;
        event.u = selectingUserID().value();
    });

    update.notifyMap<string, Sample, EntitySampleEvent>(
        samples(),
        previous.samples(),
        [&](EntitySampleEvent& event, const string& name, const Sample& sample) -> void
        {
            event.type = Entity_Sample_Event_Add;
            event.name = name;
            sample.compare(name, Sample(), update);
        },
        [&](EntitySampleEvent& event, const string& name) -> void
        {
            event.type = Entity_Sample_Event_Remove;
            event.name = name;
        },
        [&](const string& name, const Sample& current, const Sample& previous) -> void
        {
            current.compare(name, previous, update);
        }
    );

    update.notifyMap<string, Action, EntityActionEvent>(
        actions(),
        previous.actions(),
        [&](EntityActionEvent& event, const string& name, const Action& action) -> void
        {
            event.type = Entity_Action_Event_Add;
            event.name = name;
            action.compare(name, Action(), update);
        },
        [&](EntityActionEvent& event, const string& name) -> void
        {
            event.type = Entity_Action_Event_Remove;
            event.name = name;
        },
        [&](const string& name, const Action& current, const Action& previous) -> void
        {
            current.compare(name, previous, update);
        }
    );

    update.flag(active(), previous.active(), Entity_Update_Flag_Render | Entity_Update_Flag_Sound);
    update.flag(localTransform(), previous.localTransform(), Entity_Update_Flag_Render | Entity_Update_Flag_Sound);
    update.flag(linearVelocity(), previous.linearVelocity(), Entity_Update_Flag_Sound);
    update.flag(angularVelocity(), previous.angularVelocity(), Entity_Update_Flag_Sound);
}

void EntityMetaState::push(const NetworkEvent& event)
{
    switch (event.type())
    {
    default :
        break;
    case Network_Event_Entity :
    {
        auto subEvent = event.payload<EntityEvent>();

        switch (subEvent.type)
        {
        default :
            break;
        case Entity_Event_Parent_Part_ID :
            setParentPartID(BodyPartID(subEvent.u));
            break;
        case Entity_Event_Parent_Part_Sub_ID :
            setParentPartSubID(BodyPartSubID(subEvent.u));
            break;
        case Entity_Event_Name :
            setName(subEvent.s);
            break;
        case Entity_Event_Activate :
        case Entity_Event_Deactivate :
            setActive(subEvent.type == Entity_Event_Activate);
            break;
        case Entity_Event_Transform :
            setLocalTransform(subEvent.m4);
            break;
        case Entity_Event_Linear_Velocity :
            setLinearVelocity(subEvent.v3);
            break;
        case Entity_Event_Angular_Velocity :
            setAngularVelocity(subEvent.v3);
            break;
        case Entity_Event_Owner_ID :
            setOwnerID(UserID(subEvent.u));
            break;
        case Entity_Event_Share :
        case Entity_Event_Unshare :
            setShared(subEvent.type == Entity_Event_Share);
            break;
        case Entity_Event_Attached_User_ID :
#if defined(LMOD_CLIENT)
            setAttachedUserID(UserID(subEvent.u));
#endif
            break;
        case Entity_Event_Selecting_User_ID :
#if defined(LMOD_CLIENT)
            setSelectingUserID(UserID(subEvent.u));
#endif
            break;
        }
        break;
    }
    case Network_Event_Entity_Sample :
    {
        auto subEvent = event.payload<EntitySampleEvent>();

        map<string, Sample> samples = this->samples();

        switch (subEvent.type)
        {
        case Entity_Sample_Event_Add :
            samples.insert_or_assign(subEvent.name, Sample());
            break;
        case Entity_Sample_Event_Remove :
            samples.erase(subEvent.name);
            break;
        case Entity_Sample_Event_Next_Part_ID :
        case Entity_Sample_Event_Add_Part :
        case Entity_Sample_Event_Remove_Part :
        case Entity_Sample_Event_Update_Part :
        {
            auto it = samples.find(subEvent.name);

            if (it != samples.end())
            {
                it->second.push(event);
            }
            break;
        }
        }

        setSamples(samples);
        break;
    }
    case Network_Event_Entity_Action :
    {
        auto subEvent = event.payload<EntityActionEvent>();

        map<string, Action> actions = this->actions();

        switch (subEvent.type)
        {
        case Entity_Action_Event_Add :
            actions.insert_or_assign(subEvent.name, Action());
            break;
        case Entity_Action_Event_Remove :
            actions.erase(subEvent.name);
            break;
        case Entity_Action_Event_Next_Part_ID :
        case Entity_Action_Event_Add_Part :
        case Entity_Action_Event_Remove_Part :
        case Entity_Action_Event_Update_Part :
        {
            auto it = actions.find(subEvent.name);

            if (it != actions.end())
            {
                it->second.push(event);
            }
            break;
        }
        }

        setActions(actions);
        break;
    }
    }
}

vector<NetworkEvent> EntityMetaState::initial(const string& world, EntityID id) const
{
    vector<NetworkEvent> events;

    bool transformed = false;

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Parent_Part_ID)
        event.u = parentPartID().value();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Parent_Part_Sub_ID)
        event.u = parentPartSubID().value();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Name)
        event.s = name();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, active() ? Entity_Event_Activate : Entity_Event_Deactivate)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Transform)
        event.m4 = localTransform();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Linear_Velocity)
        event.v3 = linearVelocity();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Angular_Velocity)
        event.v3 = angularVelocity();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Owner_ID)
        event.u = ownerID().value();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, shared() ? Entity_Event_Share : Entity_Event_Unshare)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Attached_User_ID)
        event.u = attachedUserID().value();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Selecting_User_ID)
        event.u = selectingUserID().value();
    END_INITIAL_EVENT

    for (const auto& [ name, sample ] : samples())
    {
        vector<NetworkEvent> sampleEvents = sample.initial(world, id, name);

        events.insert(
            events.end(),
            sampleEvents.begin(),
            sampleEvents.end()
        );
    }

    for (const auto& [ name, action ] : actions())
    {
        vector<NetworkEvent> actionEvents = action.initial(world, id, name);

        events.insert(
            events.end(),
            actionEvents.begin(),
            actionEvents.end()
        );
    }

    return events;
}

BodyPartID EntityMetaState::parentPartID() const
{
    return _parentPartID;
}

BodyPartSubID EntityMetaState::parentPartSubID() const
{
    return _parentPartSubID;
}

const string& EntityMetaState::name() const
{
    return _name;
}

bool EntityMetaState::active() const
{
    return _active;
}

const mat4& EntityMetaState::localTransform() const
{
    return _transform;
}

const vec3& EntityMetaState::linearVelocity() const
{
    return _linearVelocity;
}

const vec3& EntityMetaState::angularVelocity() const
{
    return _angularVelocity;
}

UserID EntityMetaState::ownerID() const
{
    return _ownerID;
}

bool EntityMetaState::shared() const
{
    return _shared;
}

UserID EntityMetaState::attachedUserID() const
{
    return _attachedUserID;
}

UserID EntityMetaState::selectingUserID() const
{
    return _selectingUserID;
}

const map<string, Sample>& EntityMetaState::samples() const
{
    return _samples;
}

const map<string, Action>& EntityMetaState::actions() const
{
    return _actions;
}

void EntityMetaState::setParentPartID(BodyPartID id)
{
    _parentPartID = id;
}

void EntityMetaState::setParentPartSubID(BodyPartSubID id)
{
    _parentPartSubID = id;
}

void EntityMetaState::setName(const string& name)
{
    _name = isValidName(name) ? name : toValidName(name);
}

void EntityMetaState::setActive(bool active)
{
    _active = active;
}

void EntityMetaState::setLocalTransform(const mat4& transform)
{
    _transform = transform;
}

void EntityMetaState::setLinearVelocity(const vec3& linV)
{
    _linearVelocity = verifyLinearVelocity(linV);
}

void EntityMetaState::setAngularVelocity(const vec3& angV)
{
    _angularVelocity = verifyAngularVelocity(angV);
}

void EntityMetaState::setOwnerID(UserID id)
{
    _ownerID = id;
}

void EntityMetaState::setShared(bool state)
{
    _shared = state;
}

void EntityMetaState::setAttachedUserID(UserID id)
{
    _attachedUserID = id;
}

void EntityMetaState::setSelectingUserID(UserID id)
{
    _selectingUserID = id;
}

void EntityMetaState::setSamples(const map<string, Sample>& samples)
{
    _samples.clear();

    for (auto [ name, sample ] : samples)
    {
        sample.verify();

        _samples.insert({ isValidName(name) ? name : toValidName(name), sample });
    }
}

void EntityMetaState::setActions(const map<string, Action>& actions)
{
    _actions.clear();

    for (auto [ name, action ] : actions)
    {
        action.verify();

        _actions.insert({ isValidName(name) ? name : toValidName(name), action });
    }
}

template<>
EntityMetaState YAMLNode::convert() const
{
    EntityMetaState state;

    state.setParentPartID(at("parentPartID").as<BodyPartID>());
    state.setParentPartSubID(at("parentPartSubID").as<BodyPartSubID>());

    state.setName(at("name").as<string>());
    state.setActive(at("active").as<bool>());

    state.setLocalTransform(at("transform").as<mat4>());
    state.setLinearVelocity(at("linearVelocity").as<vec3>());
    state.setAngularVelocity(at("angularVelocity").as<vec3>());

    state.setOwnerID(at("ownerID").as<UserID>());
    state.setShared(at("shared").as<bool>());
    state.setAttachedUserID(at("attachedUserID").as<UserID>());
    state.setSelectingUserID(at("selectingUserID").as<UserID>());

    state.setSamples(at("samples").as<map<string, Sample>>());
    state.setActions(at("actions").as<map<string, Action>>());

    return state;
}

template<>
void YAMLSerializer::emit(EntityMetaState v)
{
    startMapping();

    emitPair("parentPartID", v.parentPartID());
    emitPair("parentPartSubID", v.parentPartSubID());

    emitPair("name", v.name());
    emitPair("active", v.active());

    emitPair("transform", v.localTransform());
    emitPair("linearVelocity", v.linearVelocity());
    emitPair("angularVelocity", v.angularVelocity());

    emitPair("ownerID", v.ownerID());
    emitPair("shared", v.shared());
    emitPair("attachedUserID", v.attachedUserID());
    emitPair("selectingUserID", v.selectingUserID());

    emitPair("samples", v.samples());
    emitPair("actions", v.actions());

    endMapping();
}

EntityBaseState::EntityBaseState()
    : _mass(0)
    , _drag(0.5)
    , _buoyancy(1)
    , _lift(0)
    , _magnetism(0)
    , _friction(0.5)
    , _restitution(0.5)

    , _host(false)
    , _shown(true)
    , _physical(true)
    , _visible(true)
    , _audible(true)

    , _fieldOfView(defaultCameraAngle)
    , _mouseLocked(false)
    , _voiceVolume(defaultVoiceVolume)
    , _tintColor(1)
    , _tintStrength(0)
{

}

void EntityBaseState::compare(const EntityBaseState& previous, EntityUpdate& update, bool transformed) const
{
    update.notifyMap<string, Value, EntityScriptEvent>(
        fields(),
        previous.fields(),
        [&](EntityScriptEvent& event, const string& name, const Value& value) -> void
        {
            event.type = Entity_Script_Event_Add;
            event.name = name;
            event.value = value;
        },
        [&](EntityScriptEvent& event, const string& name) -> void
        {
            event.type = Entity_Script_Event_Remove;
            event.name = name;
        },
        [&](EntityScriptEvent& event, const string& name, const Value& value) -> void
        {
            event.type = Entity_Script_Event_Update;
            event.name = name;
            event.value = value;
        },
        transformed
    );

    body().compare(previous.body(), update, transformed);

    update.notify<f64, EntityEvent>(mass(), previous.mass(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Mass;
        event.f = mass();
    }, transformed);

    update.notify<f64, EntityEvent>(drag(), previous.drag(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Drag;
        event.f = drag();
    }, transformed);

    update.notify<f64, EntityEvent>(buoyancy(), previous.buoyancy(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Buoyancy;
        event.f = buoyancy();
    }, transformed);

    update.notify<f64, EntityEvent>(lift(), previous.lift(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Lift;
        event.f = lift();
    }, transformed);

    update.notify<f64, EntityEvent>(magnetism(), previous.magnetism(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Magnetism;
        event.f = magnetism();
    }, transformed);

    update.notify<f64, EntityEvent>(friction(), previous.friction(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Friction;
        event.f = friction();
    }, transformed);

    update.notify<f64, EntityEvent>(restitution(), previous.restitution(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Restitution;
        event.f = restitution();
    }, transformed);

    update.notify<bool, EntityEvent>(host(), previous.host(), [&](EntityEvent& event) -> void {
        event.type = host() ? Entity_Event_Hostify : Entity_Event_Unhostify;
    }, transformed);

    update.notify<bool, EntityEvent>(shown(), previous.shown(), [&](EntityEvent& event) -> void {
        event.type = shown() ? Entity_Event_Show : Entity_Event_Hide;
    }, transformed);

    update.notify<bool, EntityEvent>(physical(), previous.physical(), [&](EntityEvent& event) -> void {
        event.type = physical() ? Entity_Event_Physicalize : Entity_Event_Etherealize;
    }, transformed);

    update.notify<bool, EntityEvent>(visible(), previous.visible(), [&](EntityEvent& event) -> void {
        event.type = visible() ? Entity_Event_Visualize : Entity_Event_Unvisualize;
    }, transformed);

    update.notify<bool, EntityEvent>(audible(), previous.audible(), [&](EntityEvent& event) -> void {
        event.type = audible() ? Entity_Event_Audialize : Entity_Event_Unaudialize;
    }, transformed);

    update.notifyMap<InputType, InputBinding, EntityInputBindingEvent>(
        bindings(),
        previous.bindings(),
        [&](EntityInputBindingEvent& event, const InputType& input, const InputBinding& binding) -> void
        {
            event.type = Entity_Input_Binding_Event_Add;
            event.input = input;
            event.binding = binding;
        },
        [&](EntityInputBindingEvent& event, const InputType& input) -> void
        {
            event.type = Entity_Input_Binding_Event_Remove;
            event.input = input;
        },
        [&](EntityInputBindingEvent& event, const InputType& input, const InputBinding& binding) -> void
        {
            event.type = Entity_Input_Binding_Event_Update;
            event.input = input;
            event.binding = binding;
        },
        transformed
    );

    update.notifyMap<string, HUDElement, EntityHUDEvent>(
        hudElements(),
        previous.hudElements(),
        [&](EntityHUDEvent& event, const string& name, const HUDElement& element) -> void
        {
            event.type = Entity_HUD_Event_Add;
            event.name = name;
            event.element = element;
        },
        [&](EntityHUDEvent& event, const string& name) -> void
        {
            event.type = Entity_HUD_Event_Remove;
            event.name = name;
        },
        [&](EntityHUDEvent& event, const string& name, const HUDElement& element) -> void
        {
            event.type = Entity_HUD_Event_Update;
            event.name = name;
            event.element = element;
        },
        transformed
    );

    update.notify<f64, EntityEvent>(fieldOfView(), previous.fieldOfView(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Field_Of_View;
        event.f = fieldOfView();
    }, transformed);

    update.notify<bool, EntityEvent>(mouseLocked(), previous.mouseLocked(), [&](EntityEvent& event) -> void {
        event.type = mouseLocked() ? Entity_Event_Lock_Mouse : Entity_Event_Unlock_Mouse;
    }, transformed);

    update.notify<f64, EntityEvent>(voiceVolume(), previous.voiceVolume(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Voice_Volume;
        event.f = voiceVolume();
    }, transformed);

    update.notify<vec3, EntityEvent>(tintColor(), previous.tintColor(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Tint_Color;
        event.v3 = tintColor();
    }, transformed);

    update.notify<f64, EntityEvent>(tintStrength(), previous.tintStrength(), [&](EntityEvent& event) -> void {
        event.type = Entity_Event_Tint_Strength;
        event.f = tintStrength();
    }, transformed);

    update.flag(body(), previous.body(), Entity_Update_Flag_Render | Entity_Update_Flag_Sound);

    update.flag(visible(), previous.visible(), Entity_Update_Flag_Render);
    update.flag(audible(), previous.audible(), Entity_Update_Flag_Sound);

    if (transformed)
    {
        update.flag(bindings(), previous.bindings(), Entity_Update_Flag_Attachment);
        update.flag(hudElements(), previous.hudElements(), Entity_Update_Flag_Attachment);
        update.flag(fieldOfView(), previous.fieldOfView(), Entity_Update_Flag_Attachment);
        update.flag(mouseLocked(), previous.mouseLocked(), Entity_Update_Flag_Attachment);
        update.flag(voiceVolume(), previous.voiceVolume(), Entity_Update_Flag_Attachment);
        update.flag(tintColor(), previous.tintColor(), Entity_Update_Flag_Attachment);
        update.flag(tintStrength(), previous.tintStrength(), Entity_Update_Flag_Attachment);
    }
}

void EntityBaseState::push(ScriptContext* context, const NetworkEvent& event, bool transformed)
{
    if (eventTransformed(event) && !transformed)
    {
        return;
    }

    switch (event.type())
    {
    default :
        break;
    case Network_Event_Entity :
    {
        auto subEvent = event.payload<EntityEvent>();

        switch (subEvent.type)
        {
        default :
            break;
        case Entity_Event_Mass :
            setMass(subEvent.f);
            break;
        case Entity_Event_Drag :
            setDrag(subEvent.f);
            break;
        case Entity_Event_Buoyancy :
            setBuoyancy(subEvent.f);
            break;
        case Entity_Event_Lift :
            setLift(subEvent.f);
            break;
        case Entity_Event_Magnetism :
            setMagnetism(subEvent.f);
            break;
        case Entity_Event_Friction :
            setFriction(subEvent.f);
            break;
        case Entity_Event_Restitution :
            setRestitution(subEvent.f);
            break;
        case Entity_Event_Hostify :
        case Entity_Event_Unhostify :
            setHost(subEvent.type == Entity_Event_Hostify);
            break;
        case Entity_Event_Show :
        case Entity_Event_Hide :
            setShown(subEvent.type == Entity_Event_Show);
            break;
        case Entity_Event_Physicalize :
        case Entity_Event_Etherealize :
            setPhysical(subEvent.type == Entity_Event_Physicalize);
            break;
        case Entity_Event_Visualize :
        case Entity_Event_Unvisualize :
            setVisible(subEvent.type == Entity_Event_Visualize);
            break;
        case Entity_Event_Audialize :
        case Entity_Event_Unaudialize :
            setAudible(subEvent.type == Entity_Event_Audialize);
            break;
        case Entity_Event_Field_Of_View :
            setFieldOfView(subEvent.f);
            break;
        case Entity_Event_Lock_Mouse :
        case Entity_Event_Unlock_Mouse :
            setMouseLocked(subEvent.type == Entity_Event_Lock_Mouse);
            break;
        case Entity_Event_Voice_Volume :
            setVoiceVolume(subEvent.f);
            break;
        case Entity_Event_Tint_Color :
            setTintColor(subEvent.v3);
            break;
        case Entity_Event_Tint_Strength :
            setTintStrength(subEvent.f);
            break;
        }
        break;
    }
    case Network_Event_Entity_Body :
        _body.push(event);
        _body.verify();
        break;
    case Network_Event_Entity_Input_Binding :
    {
        auto subEvent = event.payload<EntityInputBindingEvent>();

        map<InputType, InputBinding> bindings = this->bindings();

        switch (subEvent.type)
        {
        case Entity_Input_Binding_Event_Add :
            bindings.insert_or_assign(subEvent.input, subEvent.binding);
            break;
        case Entity_Input_Binding_Event_Remove :
            bindings.erase(subEvent.input);
            break;
        case Entity_Input_Binding_Event_Update :
        {
            auto it = bindings.find(subEvent.input);

            if (it != bindings.end())
            {
                it->second = subEvent.binding;
            }
            break;
        }
        }

        setBindings(bindings);
        break;
    }
    case Network_Event_Entity_HUD :
    {
        auto subEvent = event.payload<EntityHUDEvent>();

        map<string, HUDElement> hudElements = this->hudElements();

        switch (subEvent.type)
        {
        case Entity_HUD_Event_Add :
            hudElements.insert_or_assign(subEvent.name, subEvent.element);
            break;
        case Entity_HUD_Event_Remove :
            hudElements.erase(subEvent.name);
            break;
        case Entity_HUD_Event_Update :
        {
            auto it = hudElements.find(subEvent.name);

            if (it != hudElements.end())
            {
                it->second = subEvent.element;
            }
            break;
        }
        }

        setHUDElements(hudElements);
        break;
    }
    case Network_Event_Entity_Script :
    {
        auto subEvent = event.payload<EntityScriptEvent>();

        map<string, Value> fields = this->fields();

        switch (subEvent.type)
        {
        case Entity_Script_Event_Add :
            fields.insert_or_assign(subEvent.name, subEvent.value);
            break;
        case Entity_Script_Event_Remove :
            fields.erase(subEvent.name);
            break;
        case Entity_Script_Event_Update :
        {
            auto it = fields.find(subEvent.name);

            if (it != fields.end())
            {
                it->second = subEvent.value;
            }
            break;
        }
        }

        setFields(fields);
        break;
    }
    }
}

vector<NetworkEvent> EntityBaseState::initial(const string& world, EntityID id, bool transformed) const
{
    vector<NetworkEvent> events;

    for (const auto& [ name, value ] : fields())
    {
        START_INITIAL_EVENT(EntityScriptEvent, Entity_Script_Event_Add)
            event.name = name;
            event.value = value;
        END_INITIAL_EVENT
    }

    vector<NetworkEvent> bodyEvents = body().initial(world, id, transformed);
    events.insert(events.end(), bodyEvents.begin(), bodyEvents.end());

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Mass)
        event.f = mass();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Drag)
        event.f = drag();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Buoyancy)
        event.f = buoyancy();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Lift)
        event.f = lift();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Magnetism)
        event.f = magnetism();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Friction)
        event.f = friction();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Restitution)
        event.f = restitution();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, host() ? Entity_Event_Hostify : Entity_Event_Unhostify)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, shown() ? Entity_Event_Show : Entity_Event_Hide)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, physical() ? Entity_Event_Physicalize : Entity_Event_Etherealize)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, visible() ? Entity_Event_Visualize : Entity_Event_Unvisualize)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, audible() ? Entity_Event_Audialize : Entity_Event_Unaudialize)
    END_INITIAL_EVENT

    for (const auto& [ input, binding ] : bindings())
    {
        START_INITIAL_EVENT(EntityInputBindingEvent, Entity_Input_Binding_Event_Add)
            event.input = input;
            event.binding = binding;
        END_INITIAL_EVENT
    }

    for (const auto& [ name, element ] : hudElements())
    {
        START_INITIAL_EVENT(EntityHUDEvent, Entity_HUD_Event_Add)
            event.name = name;
            event.element = element;
        END_INITIAL_EVENT
    }

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Field_Of_View)
        event.f = fieldOfView();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, mouseLocked() ? Entity_Event_Lock_Mouse : Entity_Event_Unlock_Mouse)
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Voice_Volume)
        event.f = voiceVolume();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Tint_Color)
        event.v3 = tintColor();
    END_INITIAL_EVENT

    START_INITIAL_EVENT(EntityEvent, Entity_Event_Tint_Strength)
        event.f = tintStrength();
    END_INITIAL_EVENT

    return events;
}

const map<string, Value>& EntityBaseState::fields() const
{
    return _fields;
}

Body& EntityBaseState::body()
{
    return _body;
}

const Body& EntityBaseState::body() const
{
    return _body;
}

f64 EntityBaseState::mass() const
{
    return _mass;
}

f64 EntityBaseState::drag() const
{
    return _drag;
}

f64 EntityBaseState::buoyancy() const
{
    return _buoyancy;
}

f64 EntityBaseState::lift() const
{
    return _lift;
}

f64 EntityBaseState::magnetism() const
{
    return _magnetism;
}

f64 EntityBaseState::friction() const
{
    return _friction;
}

f64 EntityBaseState::restitution() const
{
    return _restitution;
}

bool EntityBaseState::host() const
{
    return _host;
}

bool EntityBaseState::shown() const
{
    return _shown;
}

bool EntityBaseState::physical() const
{
    return _physical;
}

bool EntityBaseState::visible() const
{
    return _visible;
}

bool EntityBaseState::audible() const
{
    return _audible;
}

const map<InputType, InputBinding>& EntityBaseState::bindings() const
{
    return _bindings;
}

const map<string, HUDElement>& EntityBaseState::hudElements() const
{
    return _hudElements;
}

f64 EntityBaseState::fieldOfView() const
{
    return _fieldOfView;
}

bool EntityBaseState::mouseLocked() const
{
    return _mouseLocked;
}

f64 EntityBaseState::voiceVolume() const
{
    return _voiceVolume;
}

vec3 EntityBaseState::tintColor() const
{
    return _tintColor;
}

f64 EntityBaseState::tintStrength() const
{
    return _tintStrength;
}

void EntityBaseState::setFields(const map<string, Value>& fields)
{
    _fields = fields;
}

void EntityBaseState::setBody(const Body& body)
{
    _body = body;

    _body.verify();
}

void EntityBaseState::setMass(f64 mass)
{
    _mass = max(mass, 0.0);
}

void EntityBaseState::setDrag(f64 drag)
{
    _drag = max(drag, 0.0);
}

void EntityBaseState::setBuoyancy(f64 buoyancy)
{
    _buoyancy = max(buoyancy, 0.0);
}

void EntityBaseState::setLift(f64 lift)
{
    _lift = max(lift, 0.0);
}

void EntityBaseState::setMagnetism(f64 magnetism)
{
    _magnetism = clamp(magnetism, -1.0, +1.0);
}

void EntityBaseState::setFriction(f64 friction)
{
    _friction = clamp(friction, 0.0, 1.0);
}

void EntityBaseState::setRestitution(f64 restitution)
{
    _restitution = clamp(restitution, 0.0, 1.0);
}

void EntityBaseState::setHost(bool state)
{
    _host = state;
}

void EntityBaseState::setShown(bool state)
{
    _shown = state;
}

void EntityBaseState::setPhysical(bool state)
{
    _physical = state;
}

void EntityBaseState::setVisible(bool state)
{
    _visible = state;
}

void EntityBaseState::setAudible(bool state)
{
    _audible = state;
}

void EntityBaseState::setBindings(const map<InputType, InputBinding>& bindings)
{
    _bindings = bindings;

    for (auto& [ type, binding ] : _bindings)
    {
        binding.verify();
    }
}

void EntityBaseState::setHUDElements(const map<string, HUDElement>& elements)
{
    _hudElements.clear();

    for (auto [ name, element ] : elements)
    {
        element.verify();

        _hudElements.insert({ isValidName(name) ? name : toValidName(name), element });
    }
}

void EntityBaseState::setFieldOfView(f64 angle)
{
    _fieldOfView = max(angle, minFieldOfView);
}

void EntityBaseState::setMouseLocked(bool state)
{
    _mouseLocked = state;
}

void EntityBaseState::setVoiceVolume(f64 volume)
{
    _voiceVolume = max(volume, 0.0);
}

void EntityBaseState::setTintColor(const vec3& color)
{
    _tintColor = verifyColor(color);
}

void EntityBaseState::setTintStrength(f64 strength)
{
    _tintStrength = clamp(strength, 0.0, 1.0);
}

string EntityBaseState::fieldsToString() const
{
    string s;

    for (const auto& [ name, value ] : fields())
    {
        s += composeList(symbol("field"), symbol(name), value).toPrettyString() + "\n\n";
    }

    return s;
}

void EntityBaseState::fieldsFromString(ScriptContext* context, const string& text)
{
    map<string, Value> fields;

    FieldDefinitionClosure closure(context, [&fields](const string& name, const Value& value) -> void {
        fields.insert_or_assign(name, value);
    });

    runSafely(context, text, "setting entity fields");

    setFields(fields);
}

template<>
EntityBaseState YAMLNode::convert() const
{
    EntityBaseState state;

    map<string, Value> fields;

    for (const auto& [ name, s ] : at("fields").as<map<string, string>>())
    {
        ScriptContext context;

        Value value = runSafely(
            &context,
            s,
            "loading entity field"
        )->front();

        fields.insert_or_assign(name, value);
    }

    state.setFields(fields);
    state.setBody(at("body").as<Body>());

    state.setMass(at("mass").as<f64>());
    state.setDrag(at("drag").as<f64>());
    state.setBuoyancy(at("buoyancy").as<f64>());
    state.setLift(at("lift").as<f64>());
    state.setMagnetism(at("magnetism").as<f64>());
    state.setFriction(at("friction").as<f64>());
    state.setRestitution(at("restitution").as<f64>());

    state.setHost(at("host").as<bool>());
    state.setShown(at("shown").as<bool>());
    state.setPhysical(at("physical").as<bool>());
    state.setVisible(at("visible").as<bool>());
    state.setAudible(at("audible").as<bool>());

    state.setBindings(at("bindings").as<map<InputType, InputBinding>>());
    state.setHUDElements(at("hudElements").as<map<string, HUDElement>>());
    state.setFieldOfView(at("fieldOfView").as<f64>());
    state.setMouseLocked(at("mouseLocked").as<bool>());

    state.setVoiceVolume(at("voiceVolume").as<f64>());
    state.setTintColor(at("tintColor").as<vec3>());
    state.setTintStrength(at("tintStrength").as<f64>());

    return state;
}

template<>
void YAMLSerializer::emit(EntityBaseState v)
{
    startMapping();

    emit("fields");
    startMapping();
    for (const auto& [ name, value ] : v.fields())
    {
        emitPair(name, value.toString());
    }
    endMapping();
    emitPair("body", v.body());

    emitPair("mass", v.mass());
    emitPair("drag", v.drag());
    emitPair("buoyancy", v.buoyancy());
    emitPair("lift", v.lift());
    emitPair("magnetism", v.magnetism());
    emitPair("friction", v.friction());
    emitPair("restitution", v.restitution());

    emitPair("host", v.host());
    emitPair("shown", v.shown());
    emitPair("physical", v.physical());
    emitPair("visible", v.visible());
    emitPair("audible", v.audible());

    emitPair("bindings", v.bindings());
    emitPair("hudElements", v.hudElements());
    emitPair("fieldOfView", v.fieldOfView());
    emitPair("mouseLocked", v.mouseLocked());

    emitPair("voiceVolume", v.voiceVolume());
    emitPair("tintColor", v.tintColor());
    emitPair("tintStrength", v.tintStrength());

    endMapping();
}

EntityTransformedState::EntityTransformedState()
    : EntityBaseState()
    , _nextSamplePlaybackID(1)
    , _nextActionPlaybackID(1)
{

}

EntityTransformedState::EntityTransformedState(const EntityBaseState& state)
    : EntityBaseState(state)
    , _nextSamplePlaybackID(1)
    , _nextActionPlaybackID(1)
{

}

void EntityTransformedState::compare(const EntityTransformedState& previous, EntityUpdate& update) const
{
    EntityBaseState::compare(previous, update, true);

    update.notifyMap<SamplePlaybackID, SamplePlayback, EntitySamplePlaybackEvent>(
        samplePlaybacks(),
        previous.samplePlaybacks(),
        [&](EntitySamplePlaybackEvent& event, SamplePlaybackID id, const SamplePlayback& playback) -> void
        {
            event.type = Entity_Sample_Playback_Event_Add;
            event.playbackID = id;
            event.playback = playback;
        },
        [&](EntitySamplePlaybackEvent& event, SamplePlaybackID id) -> void
        {
            event.type = Entity_Sample_Playback_Event_Remove;
            event.playbackID = id;
        },
        [&](SamplePlaybackID id, const SamplePlayback& current, const SamplePlayback& previous) -> void
        {
            current.compare(previous, update);
        },
        true
    );

    update.notifyMap<ActionPlaybackID, ActionPlayback, EntityActionPlaybackEvent>(
        actionPlaybacks(),
        previous.actionPlaybacks(),
        [&](EntityActionPlaybackEvent& event, ActionPlaybackID id, const ActionPlayback& playback) -> void
        {
            event.type = Entity_Action_Playback_Event_Add;
            event.playbackID = id;
            event.playback = playback;
        },
        [&](EntityActionPlaybackEvent& event, ActionPlaybackID id) -> void
        {
            event.type = Entity_Action_Playback_Event_Remove;
            event.playbackID = id;
        },
        [&](ActionPlaybackID id, const ActionPlayback& current, const ActionPlayback& previous) -> void
        {
            current.compare(previous, update);
        },
        true
    );

    update.flag(samplePlaybacks(), previous.samplePlaybacks(), Entity_Update_Flag_Sound);
}

void EntityTransformedState::push(ScriptContext* context, const NetworkEvent& event)
{
    if (!eventTransformed(event))
    {
        return;
    }

    switch (event.type())
    {
    default :
        error("Non-entity transformed event " + event.name() + " was pushed to entity transformed state.");
        break;
    case Network_Event_Entity :
    case Network_Event_Entity_Body :
    case Network_Event_Entity_Input_Binding :
    case Network_Event_Entity_HUD :
    case Network_Event_Entity_Script :
        EntityBaseState::push(context, event, true);
        break;
    case Network_Event_Entity_Sample_Playback :
    {
        map<SamplePlaybackID, SamplePlayback> samplePlaybacks = this->samplePlaybacks();

        auto subEvent = event.payload<EntitySamplePlaybackEvent>();

        switch (subEvent.type)
        {
        case Entity_Sample_Playback_Event_Add :
            samplePlaybacks.insert_or_assign(subEvent.playbackID, subEvent.playback);
            break;
        case Entity_Sample_Playback_Event_Remove :
            samplePlaybacks.erase(subEvent.playbackID);
            break;
        case Entity_Sample_Playback_Event_Resume :
        {
            auto it = samplePlaybacks.find(subEvent.playbackID);

            if (it != samplePlaybacks.end())
            {
                it->second.resume();
            }
            break;
        }
        case Entity_Sample_Playback_Event_Pause :
        {
            auto it = samplePlaybacks.find(subEvent.playbackID);

            if (it != samplePlaybacks.end())
            {
                it->second.pause();
            }
            break;
        }
        case Entity_Sample_Playback_Event_Seek :
        {
            auto it = samplePlaybacks.find(subEvent.playbackID);

            if (it != samplePlaybacks.end())
            {
                it->second.seek(subEvent.position);
            }
            break;
        }
        }

        setSamplePlaybacks(samplePlaybacks);
        break;
    }
    case Network_Event_Entity_Action_Playback :
    {
        map<ActionPlaybackID, ActionPlayback> actionPlaybacks = this->actionPlaybacks();

        auto subEvent = event.payload<EntityActionPlaybackEvent>();

        switch (subEvent.type)
        {
        case Entity_Action_Playback_Event_Add :
            actionPlaybacks.insert_or_assign(subEvent.playbackID, subEvent.playback);
            break;
        case Entity_Action_Playback_Event_Remove :
            actionPlaybacks.erase(subEvent.playbackID);
            break;
        case Entity_Action_Playback_Event_Resume :
        {
            auto it = actionPlaybacks.find(subEvent.playbackID);

            if (it != actionPlaybacks.end())
            {
                it->second.resume();
            }
            break;
        }
        case Entity_Action_Playback_Event_Pause :
        {
            auto it = actionPlaybacks.find(subEvent.playbackID);

            if (it != actionPlaybacks.end())
            {
                it->second.pause();
            }
            break;
        }
        case Entity_Action_Playback_Event_Seek :
        {
            auto it = actionPlaybacks.find(subEvent.playbackID);

            if (it != actionPlaybacks.end())
            {
                it->second.seek(subEvent.position);
            }
            break;
        }
        }

        setActionPlaybacks(actionPlaybacks);
        break;
    }
    }
}

vector<NetworkEvent> EntityTransformedState::initial(const string& world, EntityID id) const
{
    vector<NetworkEvent> events;

    bool transformed = true;

    for (const auto& [ playbackID, playback ] : samplePlaybacks())
    {
        START_INITIAL_EVENT(EntitySamplePlaybackEvent, Entity_Sample_Playback_Event_Add)
            event.playbackID = playbackID;
            event.playback = playback;
        END_INITIAL_EVENT
    }

    for (const auto& [ playbackID, playback ] : actionPlaybacks())
    {
        START_INITIAL_EVENT(EntityActionPlaybackEvent, Entity_Action_Playback_Event_Add)
            event.playbackID = playbackID;
            event.playback = playback;
        END_INITIAL_EVENT
    }

    return join(EntityBaseState::initial(world, id, true), events);
}

SamplePlaybackID EntityTransformedState::nextSamplePlaybackID() const
{
    return _nextSamplePlaybackID;
}

const map<SamplePlaybackID, SamplePlayback>& EntityTransformedState::samplePlaybacks() const
{
    return _samplePlaybacks;
}

ActionPlaybackID EntityTransformedState::nextActionPlaybackID() const
{
    return _nextActionPlaybackID;
}

const map<ActionPlaybackID, ActionPlayback>& EntityTransformedState::actionPlaybacks() const
{
    return _actionPlaybacks;
}

void EntityTransformedState::setNextSamplePlaybackID(SamplePlaybackID id)
{
    _nextSamplePlaybackID = id;
}

void EntityTransformedState::setSamplePlaybacks(const map<SamplePlaybackID, SamplePlayback>& playbacks)
{
    _samplePlaybacks = playbacks;

    for (auto& [ id, playback ] : _samplePlaybacks)
    {
        playback.verify();
    }
}

void EntityTransformedState::setNextActionPlaybackID(ActionPlaybackID id)
{
    _nextActionPlaybackID = id;
}

void EntityTransformedState::setActionPlaybacks(const map<ActionPlaybackID, ActionPlayback>& playbacks)
{
    _actionPlaybacks = playbacks;

    for (auto& [ id, playback ] : _actionPlaybacks)
    {
        playback.verify();
    }
}

template<>
EntityTransformedState YAMLNode::convert() const
{
    EntityTransformedState state;

    map<string, Value> fields;

    for (const auto& [ name, s ] : at("fields").as<map<string, string>>())
    {
        ScriptContext context;

        Value value = runSafely(
            &context,
            s,
            "loading entity field"
        )->front();

        fields.insert_or_assign(name, value);
    }

    state.setFields(fields);
    state.setBody(at("body").as<Body>());

    state.setMass(at("mass").as<f64>());
    state.setDrag(at("drag").as<f64>());
    state.setBuoyancy(at("buoyancy").as<f64>());
    state.setLift(at("lift").as<f64>());
    state.setMagnetism(at("magnetism").as<f64>());
    state.setFriction(at("friction").as<f64>());
    state.setRestitution(at("restitution").as<f64>());

    state.setHost(at("host").as<bool>());
    state.setShown(at("shown").as<bool>());
    state.setPhysical(at("physical").as<bool>());
    state.setVisible(at("visible").as<bool>());
    state.setAudible(at("audible").as<bool>());

    state.setBindings(at("bindings").as<map<InputType, InputBinding>>());
    state.setHUDElements(at("hudElements").as<map<string, HUDElement>>());
    state.setFieldOfView(at("fieldOfView").as<f64>());
    state.setMouseLocked(at("mouseLocked").as<bool>());

    state.setVoiceVolume(at("voiceVolume").as<f64>());
    state.setTintColor(at("tintColor").as<vec3>());
    state.setTintStrength(at("tintStrength").as<f64>());

    state.setNextSamplePlaybackID(at("nextSamplePlaybackID").as<SamplePlaybackID>());
    state.setSamplePlaybacks(at("samplePlaybacks").as<map<SamplePlaybackID, SamplePlayback>>());
    state.setNextActionPlaybackID(at("nextActionPlaybackID").as<ActionPlaybackID>());
    state.setActionPlaybacks(at("actionPlaybacks").as<map<ActionPlaybackID, ActionPlayback>>());

    return state;
}

template<>
void YAMLSerializer::emit(EntityTransformedState v)
{
    startMapping();

    emit("fields");
    startMapping();
    for (const auto& [ name, value ] : v.fields())
    {
        emitPair(name, value.toString());
    }
    endMapping();
    emitPair("body", v.body());

    emitPair("mass", v.mass());
    emitPair("drag", v.drag());
    emitPair("buoyancy", v.buoyancy());
    emitPair("lift", v.lift());
    emitPair("magnetism", v.magnetism());
    emitPair("friction", v.friction());
    emitPair("restitution", v.restitution());

    emitPair("host", v.host());
    emitPair("shown", v.shown());
    emitPair("physical", v.physical());
    emitPair("visible", v.visible());
    emitPair("audible", v.audible());

    emitPair("bindings", v.bindings());
    emitPair("hudElements", v.hudElements());
    emitPair("fieldOfView", v.fieldOfView());
    emitPair("mouseLocked", v.mouseLocked());

    emitPair("voiceVolume", v.voiceVolume());
    emitPair("tintColor", v.tintColor());
    emitPair("tintStrength", v.tintStrength());

    emitPair("nextSamplePlaybackID", v.nextSamplePlaybackID());
    emitPair("samplePlaybacks", v.samplePlaybacks());
    emitPair("nextActionPlaybackID", v.nextActionPlaybackID());
    emitPair("actionPlaybacks", v.actionPlaybacks());

    endMapping();
}

Entity::Entity(World* world, EntityID id)
    : _world(world)
    , _parent(nullptr)
    , _id(id)
{

}

Entity::Entity(Entity* parent, EntityID id)
    : _world(nullptr)
    , _parent(parent)
    , _id(id)
{

}

Entity::Entity(World* world, EntityID id, const Entity& rhs)
{
    *this = Entity(rhs);

    _world = world;
    _parent = nullptr;
    _id = id;
}

Entity::Entity(Entity* parent, EntityID id, const Entity& rhs)
{
    *this = Entity(rhs);

    _world = nullptr;
    _parent = parent;
    _id = id;
}

Entity::Entity(const Entity& rhs)
{
    copy(rhs);
    _children.clear();

    for (const auto& [ id, child ] : rhs._children)
    {
        _children.insert({ id, new Entity(*child) });
    }

    for (auto& [ id, child ] : _children)
    {
        child->_parent = this;
    }
}

Entity::Entity(Entity&& rhs) noexcept
{
    copy(rhs);
    rhs._children.clear();

    for (auto& [ id, child ] : _children)
    {
        child->_parent = this;
    }
}

Entity::~Entity()
{
    for (const auto& [ id, child ] : _children)
    {
        delete child;
    }
}

Entity& Entity::operator=(const Entity& rhs)
{
    if (&rhs != this)
    {
        for (const auto& [ id, child ] : _children)
        {
            delete child;
        }

        copy(rhs);
        _children.clear();

        for (const auto& [ id, child ] : rhs._children)
        {
            _children.insert({ id, new Entity(*child) });
        }

        for (auto& [ id, child ] : _children)
        {
            child->_parent = this;
        }
    }

    return *this;
}

Entity& Entity::operator=(Entity&& rhs) noexcept
{
    if (&rhs != this)
    {
        for (const auto& [ id, child ] : _children)
        {
            delete child;
        }

        copy(rhs);
        rhs._children.clear();

        for (auto& [ id, child ] : _children)
        {
            child->_parent = this;
        }
    }

    return *this;
}

Entity* Entity::load(World* world, const YAMLNode& node)
{
    auto entity = new Entity(world, node.at("id").as<EntityID>());

    entity->meta = node.at("meta").as<EntityMetaState>();
    entity->_base = node.at("base").as<EntityBaseState>();
    entity->_transformed = node.at("transformed").as<EntityTransformedState>();

    for (const auto& [ id, node ] : node.at("children").as<map<EntityID, YAMLNode>>())
    {
        entity->_children.insert({ id, Entity::load(entity, node) });
    }

    return entity;
}

Entity* Entity::load(Entity* parent, const YAMLNode& node)
{
    auto entity = new Entity(parent, node.at("id").as<EntityID>());

    entity->meta = node.at("meta").as<EntityMetaState>();
    entity->_base = node.at("base").as<EntityBaseState>();
    entity->_transformed = node.at("transformed").as<EntityTransformedState>();

    for (const auto& [ id, node ] : node.at("children").as<map<EntityID, YAMLNode>>())
    {
        entity->_children.insert({ id, Entity::load(entity, node) });
    }

    return entity;
}

void Entity::compare(const Entity& previous, EntityUpdate& update) const
{
    meta.compare(previous.meta, update);
    _base.compare(previous._base, update);
    _transformed.compare(previous._transformed, update);

    for (const auto& [ id, child ] : previous._children)
    {
        auto it = _children.find(id);

        if (it == _children.end())
        {
            update.removeChild(child);
        }
    }

    for (const auto& [ id, child ] : _children)
    {
        auto it = previous._children.find(id);

        if (it == previous._children.end())
        {
            update.addChild(child);
        }
        else
        {
            const Entity* previousChild = it->second;

            EntityUpdate childUpdate(child->world(), child);

            child->compare(*previousChild, childUpdate);

            update.updateChild(previousChild, child, childUpdate);
        }
    }
}

bool Entity::allowed(User* user) const
{
    return user->admin() || meta.shared() || meta.ownerID() == UserID() || meta.ownerID() == user->id();
}

void Entity::step(ScriptContext* context, f64 stepInterval)
{
    if (!active())
    {
        // NOTE: Special handling for preview playbacks
        map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

        if (samplePlaybacks.contains(previewSamplePlaybackID))
        {
            SamplePlayback& playback = samplePlaybacks.at(previewSamplePlaybackID);

            if (playback.playing)
            {
                playback.elapsed += stepInterval * 1000;

                _transformed.setSamplePlaybacks(samplePlaybacks);
            }
        }

        map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

        if (actionPlaybacks.contains(previewActionPlaybackID))
        {
            ActionPlayback& playback = actionPlaybacks.at(previewActionPlaybackID);

            if (playback.playing)
            {
                playback.elapsed += stepInterval * 1000;

                _transformed.setActionPlaybacks(actionPlaybacks);
            }
        }

        return;
    }

    if (has("step"))
    {
        callHook(context, "step", { toValue(stepInterval) });
    }

    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    for (auto it = samplePlaybacks.begin(); it != samplePlaybacks.end();)
    {
        SamplePlayback& playback = it->second;

        if (!playback.playing)
        {
            it++;
            continue;
        }

        const Sample& sample = meta.samples().at(playback.name);

        if (!playback.loop && playback.elapsed >= sample.length())
        {
            samplePlaybacks.erase(it++);
        }
        else
        {
            playback.elapsed += stepInterval * 1000;
            it++;
        }
    }

    _transformed.setSamplePlaybacks(samplePlaybacks);

    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    for (auto it = actionPlaybacks.begin(); it != actionPlaybacks.end();)
    {
        ActionPlayback& playback = it->second;

        if (!playback.playing)
        {
            it++;
            continue;
        }

        const Action& action = meta.actions().at(playback.name);

        if (!playback.loop && playback.elapsed >= action.length())
        {
            actionPlaybacks.erase(it++);
        }
        else
        {
            playback.elapsed += stepInterval * 1000;
            it++;
        }
    }

    _transformed.setActionPlaybacks(actionPlaybacks);

    for (const auto& [ id, child ] : _children)
    {
        child->step(context, stepInterval);
    }
}

void Entity::input(
    ScriptContext* context,
    const string& input,
    bool up,
    f64 intensity,
    const vec3& position,
    const quaternion& rotation,
    const vec3& linV,
    const vec3& angV,
    const InputDimensions& dimensions,
    const InputTouch& touch
)
{
    Value table = toValue(map<Value, Value>({
        { symbol("up"), toValue(up ? 1 : 0) },
        { symbol("intensity"), toValue(intensity) },
        { symbol("position"), toValue(position) },
        { symbol("rotation"), toValue(rotation) },
        { symbol("linear-velocity"), toValue(linV) },
        { symbol("angular-velocity"), toValue(angV) },
        { symbol("dimensions"), toValue(vector<f64>(dimensions.begin(), dimensions.end())) },
        { symbol("touch"), composeList(touch.id, touch.major, touch.minor, touch.orientation, touch.force) }
    }));

    callHook(context, input, { table });
}

void Entity::collide(ScriptContext* context, EntityID otherID, const vec3& position)
{
    if (!has("collide"))
    {
        return;
    }

    callHook(context, "collide", { toValue(otherID), toValue(position) });
}

#if defined(LMOD_SERVER)
void Entity::push(User* user, ScriptContext* context, const NetworkEvent& event)
#elif defined(LMOD_CLIENT)
void Entity::push(ScriptContext* context, const NetworkEvent& event)
#endif
{
    EntityID id = eventID(event);

    if (id != _id)
    {
        for (const auto& [ id, child ] : _children)
        {
#if defined(LMOD_SERVER)
            child->push(user, context, event);
#elif defined(LMOD_CLIENT)
            child->push(context, event);
#endif
        }
        return;
    }

#if defined(LMOD_SERVER)
    if (!allowed(user))
    {
        user->error(permissionDeniedError("modify-entity"));
        return;
    }
#endif

    switch (event.type())
    {
    default :
        error("Non-entity event " + event.name() + " was pushed to entity.");
        break;
    case Network_Event_Entity :
    {
        EntityEvent entityEvent = event.payload<EntityEvent>();

        switch (entityEvent.type)
        {
        case Entity_Event_Add :
        {
            auto it = _children.find(EntityID(entityEvent.u));

            if (it != _children.end())
            {
#if defined(LMOD_SERVER)
                user->pushMessage(entityFoundError(EntityID(entityEvent.u)));
#endif
                return;
            }

            auto child = new Entity(this, EntityID(entityEvent.u));

#if defined(LMOD_SERVER)
            child->setOwnerID(user->id());
#endif

            child->setName(entityEvent.s);

#if defined(LMOD_SERVER)
            world()->recordAdd(user->id(), child);
#endif

            _children.insert({ child->id(), child });
            break;
        }
        case Entity_Event_Remove :
        {
            auto it = _children.find(EntityID(entityEvent.u));

            if (it == _children.end())
            {
#if defined(LMOD_SERVER)
                user->pushMessage(entityNotFoundError(EntityID(entityEvent.u)));
#endif
                return;
            }

            Entity* child = it->second;

#if defined(LMOD_SERVER)
            if (!child->allowed(user))
            {
                user->error(permissionDeniedError("remove-entity"));
                return;
            }

            world()->recordRemove(user->id(), child);
#endif

            _children.erase(child->id());

            delete child;
            break;
        }
        default :
        {
            Entity before = *this;
            meta.push(event);
            _transformed.push(context, event);
            _base.push(context, event);
#if defined(LMOD_SERVER)
            world()->recordModify(user->id(), &before, this, entityEvent.mergeUndo);
#endif
            break;
        }
        }
        break;
    }
    case Network_Event_Entity_Body :
    {
        Entity before = *this;
        _transformed.push(context, event);
        _base.push(context, event);
#if defined(LMOD_SERVER)
        world()->recordModify(user->id(), &before, this, event.payload<EntityBodyEvent>().mergeUndo);
#endif
        break;
    }
    case Network_Event_Entity_Sample :
    case Network_Event_Entity_Action :
    {
        Entity before = *this;
        meta.push(event);
#if defined(LMOD_SERVER)
        world()->recordModify(
            user->id(),
            &before,
            this,
            event.type() == Network_Event_Entity_Sample
                ? event.payload<EntitySampleEvent>().mergeUndo
                : event.payload<EntityActionEvent>().mergeUndo
        );
#endif
        break;
    }
    case Network_Event_Entity_Sample_Playback :
    case Network_Event_Entity_Action_Playback :
    {
        Entity before = *this;
        _transformed.push(context, event);
#if defined(LMOD_SERVER)
        world()->recordModify(user->id(), &before, this, false);
#endif
        break;
    }
    case Network_Event_Entity_Input_Binding :
    case Network_Event_Entity_HUD :
    {
        Entity before = *this;
        _transformed.push(context, event);
        _base.push(context, event);
#if defined(LMOD_SERVER)
        world()->recordModify(user->id(), &before, this, false);
#endif
        break;
    }
    case Network_Event_Entity_Script :
    {
        Entity before = *this;
        _transformed.push(context, event);
        _base.push(context, event);
#if defined(LMOD_SERVER)
        world()->recordModify(user->id(), &before, this, false);
#endif
        break;
    }
    }
}

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> Entity::initial() const
{
    EntityID parentID = _parent ? _parent->id() : EntityID();

    EntityEvent event(Entity_Event_Add, world()->name(), parentID);
    event.u = _id.value();

    vector<NetworkEvent> events = { event };
    vector<EntityUpdateSideEffect> sideEffects = {
        { parentID, _id, Entity_Update_Display_Add }
    };

    vector<NetworkEvent> metaEvents = meta.initial(world()->name(), _id);
    vector<NetworkEvent> baseEvents = _base.initial(world()->name(), _id);
    vector<NetworkEvent> transformedEvents = _transformed.initial(world()->name(), _id);

    events = join(events, join(metaEvents, join(baseEvents, transformedEvents)));

    for (const auto& [ id, child ] : _children)
    {
        auto [ childEvents, childSideEffects ] = child->initial();

        events.insert(
            events.end(),
            childEvents.begin(),
            childEvents.end()
        );

        sideEffects.insert(
            sideEffects.end(),
            childSideEffects.begin(),
            childSideEffects.end()
        );
    }

    return { events, sideEffects };
}

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> Entity::terminal() const
{
    EntityID parentID = _parent ? _parent->id() : EntityID();

    EntityEvent event(Entity_Event_Remove, world()->name(), parentID);
    event.u = _id.value();

    vector<NetworkEvent> events = { event };
    vector<EntityUpdateSideEffect> sideEffects = {
        { parentID, _id, Entity_Update_Display_Remove }
    };

    return { events, sideEffects };
}

void Entity::store()
{
    if (!active())
    {
        return;
    }

    _base = _transformed;

    for (const auto& [ id, child ] : _children)
    {
        child->store();
    }

    setActive(false);
}

void Entity::reset()
{
    if (active())
    {
        return;
    }

    // NOTE: Special handling for preview playbacks
    optional<SamplePlayback> previewSamplePlayback;
    if (samplePlaybacks().contains(previewSamplePlaybackID))
    {
        previewSamplePlayback = samplePlaybacks().at(previewSamplePlaybackID);
    }

    optional<ActionPlayback> previewActionPlayback;
    if (actionPlaybacks().contains(previewActionPlaybackID))
    {
        previewActionPlayback = actionPlaybacks().at(previewActionPlaybackID);
    }

    _transformed = _base;

    if (previewSamplePlayback)
    {
        map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

        samplePlaybacks.insert_or_assign(previewSamplePlaybackID, *previewSamplePlayback);

        _transformed.setSamplePlaybacks(samplePlaybacks);
    }

    if (previewActionPlayback)
    {
        map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

        actionPlaybacks.insert_or_assign(previewActionPlaybackID, *previewActionPlayback);

        _transformed.setActionPlaybacks(actionPlaybacks);
    }

    for (const auto& [ id, child ] : _children)
    {
        child->reset();
    }

    setActive(true);
}

bool Entity::canDelete() const
{
    return meta.attachedUserID() == UserID();
}

World* Entity::world() const
{
    return _parent ? _parent->world() : _world;
}

Entity* Entity::parent() const
{
    return _parent;
}

const map<EntityID, Entity*>& Entity::children() const
{
    return _children;
}

Entity* Entity::rootParent()
{
    if (_parent)
    {
        return _parent->rootParent();
    }
    else
    {
        return this;
    }
}

bool Entity::hasParent(Entity* possible) const
{
    if (_parent)
    {
        if (_parent == possible)
        {
            return true;
        }
        else
        {
            return _parent->hasParent(possible);
        }
    }
    else
    {
        return false;
    }
}

size_t Entity::parentDepth() const
{
    return _parent ? 1 + _parent->parentDepth() : 0;
}

vec3 Entity::parentGlobalPosition() const
{
    if (!_parent)
    {
        return vec3();
    }

    if (parentPartID() == BodyPartID())
    {
        return _parent->globalPosition();
    }

    const BodyPart* part = _parent->body().get(parentPartID());

    if (parentPartSubID() == BodyPartSubID())
    {
        return _parent->globalTransform().applyToPosition(part->regionalPosition());
    }

    vec3 subpartPosition = part->subparts().at(parentPartSubID().value() - 1);

    return _parent->globalTransform().applyToPosition(subpartPosition);
}

void Entity::add(EntityID parentID, Entity* entity)
{
    if (parentID == _id)
    {
        _children.insert({ entity->id(), entity });
        return;
    }

    for (const auto& [ id, child ] : _children)
    {
        child->add(parentID, entity);
    }
}

void Entity::remove(EntityID id)
{
    auto it = _children.find(id);

    if (it != _children.end())
    {
        delete it->second;
        _children.erase(it);
        return;
    }

    for (const auto& [ childID, child ] : _children)
    {
        child->remove(id);
    }
}

Entity* Entity::get(EntityID id) const
{
    for (const auto& [ entityID, entity ] : _children)
    {
        if (entityID == id)
        {
            return entity;
        }

        Entity* child = entity->get(id);

        if (child)
        {
            return child;
        }
    }

    return nullptr;
}

const map<EntityID, Entity*>& Entity::entities() const
{
    return _children;
}

void Entity::traverse(const function<void(Entity*)>& behavior)
{
    behavior(this);

    for (auto& [ id, entity ] : _children)
    {
        entity->traverse(behavior);
    }
}

void Entity::traverse(const function<void(const Entity*)>& behavior) const
{
    behavior(this);

    for (const auto& [ id, entity ] : _children)
    {
        static_cast<const Entity*>(entity)->traverse(behavior);
    }
}

bool Entity::partiallyTraverse(const function<bool(Entity*)>& behavior)
{
    if (behavior(this))
    {
        return true;
    }

    for (auto& [ id, entity ] : _children)
    {
        if (entity->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool Entity::partiallyTraverse(const function<bool(const Entity*)>& behavior) const
{
    if (behavior(this))
    {
        return true;
    }

    for (const auto& [ id, entity ] : _children)
    {
        if (static_cast<const Entity*>(entity)->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

const EntityBaseState& Entity::base() const
{
    return _base;
}

const EntityBaseState& Entity::transformed() const
{
    return _transformed;
}

EntityID Entity::id() const
{
    return _id;
}

BodyPartID Entity::parentPartID() const
{
    return meta.parentPartID();
}

BodyPartSubID Entity::parentPartSubID() const
{
    return meta.parentPartSubID();
}

const string& Entity::name() const
{
    return meta.name();
}

bool Entity::active() const
{
    return (_parent ? _parent->active() : true) && meta.active();
}

UserID Entity::ownerID() const
{
    return meta.ownerID();
}

bool Entity::shared() const
{
    return meta.shared();
}

UserID Entity::attachedUserID() const
{
    return meta.attachedUserID();
}

UserID Entity::selectingUserID() const
{
    return meta.selectingUserID();
}

vec3 Entity::localPosition() const
{
    return localTransform().position();
}

quaternion Entity::localRotation() const
{
    return localTransform().rotation();
}

vec3 Entity::localScale() const
{
    return localTransform().scale();
}

mat4 Entity::localTransform() const
{
    return meta.localTransform();
}

vec3 Entity::globalPosition() const
{
    return globalTransform().position();
}

quaternion Entity::globalRotation() const
{
    return globalTransform().rotation();
}

vec3 Entity::globalScale() const
{
    return globalTransform().scale();
}

mat4 Entity::globalTransform() const
{
    return parent() ? parent()->globalTransform().applyToTransform(localTransform()) : localTransform();
}

const vec3& Entity::linearVelocity() const
{
    if (_parent)
    {
        return _parent->linearVelocity();
    }
    else
    {
        return meta.linearVelocity();
    }
}

const vec3& Entity::angularVelocity() const
{
    if (_parent)
    {
        return _parent->angularVelocity();
    }
    else
    {
        return meta.angularVelocity();
    }
}

const map<string, Value>& Entity::fields() const
{
    return active() ? _transformed.fields() : _base.fields();
}

Body& Entity::body()
{
    return active() ? _transformed.body() : _base.body();
}

const Body& Entity::body() const
{
    return active() ? _transformed.body() : _base.body();
}

const map<string, Sample>& Entity::samples() const
{
    return meta.samples();
}

const map<string, Action>& Entity::actions() const
{
    return meta.actions();
}

const map<SamplePlaybackID, SamplePlayback>& Entity::samplePlaybacks() const
{
    return _transformed.samplePlaybacks();
}

const map<ActionPlaybackID, ActionPlayback>& Entity::actionPlaybacks() const
{
    return _transformed.actionPlaybacks();
}

f64 Entity::mass() const
{
    return active() ? _transformed.mass() : _base.mass();
}

f64 Entity::drag() const
{
    return active() ? _transformed.drag() : _base.drag();
}

f64 Entity::buoyancy() const
{
    return active() ? _transformed.buoyancy() : _base.buoyancy();
}

f64 Entity::lift() const
{
    return active() ? _transformed.lift() : _base.lift();
}

f64 Entity::magnetism() const
{
    return active() ? _transformed.magnetism() : _base.magnetism();
}

f64 Entity::friction() const
{
    return active() ? _transformed.friction() : _base.friction();
}

f64 Entity::restitution() const
{
    return active() ? _transformed.restitution() : _base.restitution();
}

bool Entity::host() const
{
    return active() ? _transformed.host() : _base.host();
}

bool Entity::shown() const
{
    return (_parent ? _parent->shown() : true) && (active() ? _transformed.shown() : _base.shown());
}

bool Entity::physical() const
{
    return (_parent ? _parent->physical() : true) && (active() ? _transformed.physical() : _base.physical());
}

bool Entity::visible() const
{
    return (_parent ? _parent->visible() : true) && (active() ? _transformed.visible() : _base.visible());
}

bool Entity::audible() const
{
    return (_parent ? _parent->audible() : true) && (active() ? _transformed.audible() : _base.audible());
}

const map<InputType, InputBinding>& Entity::bindings() const
{
    return active() ? _transformed.bindings() : _base.bindings();
}

const map<string, HUDElement>& Entity::hudElements() const
{
    return active() ? _transformed.hudElements() : _base.hudElements();
}

f64 Entity::fieldOfView() const
{
    return active() ? _transformed.fieldOfView() : _base.fieldOfView();
}

bool Entity::mouseLocked() const
{
    return active() ? _transformed.mouseLocked() : _base.mouseLocked();
}

f64 Entity::voiceVolume() const
{
    return active() ? _transformed.voiceVolume() : _base.voiceVolume();
}

vec3 Entity::tintColor() const
{
    return active() ? _transformed.tintColor() : _base.tintColor();
}

f64 Entity::tintStrength() const
{
    return active() ? _transformed.tintStrength() : _base.tintStrength();
}

void Entity::setParentPartID(BodyPartID partID)
{
    meta.setParentPartID(partID);
}

void Entity::setParentPartSubID(BodyPartSubID partSubID)
{
    meta.setParentPartSubID(partSubID);
}

void Entity::setName(const string& name)
{
    meta.setName(name);
}

void Entity::setActive(bool state)
{
    meta.setActive(state);
}

void Entity::setOwnerID(UserID id)
{
    meta.setOwnerID(id);
}

void Entity::setShared(bool state)
{
    meta.setShared(state);
}

void Entity::setAttachedUserID(UserID id)
{
    meta.setAttachedUserID(id);
}

void Entity::setSelectingUserID(UserID id)
{
    meta.setSelectingUserID(id);
}

void Entity::setLocalPosition(const vec3& position)
{
    mat4 local = localTransform();

    local.setPosition(position);

    setLocalTransform(local);
}

void Entity::setLocalRotation(const quaternion& rotation)
{
    mat4 local = localTransform();

    local.setRotation(rotation);

    setLocalTransform(local);
}

void Entity::setLocalScale(const vec3& scale)
{
    mat4 local = localTransform();

    local.setScale(scale);

    setLocalTransform(local);
}

void Entity::setLocalTransform(const mat4& transform)
{
    meta.setLocalTransform(transform);
}

void Entity::setGlobalPosition(const vec3& position)
{
    mat4 global = globalTransform();

    global.setPosition(position);

    setGlobalTransform(global);
}

void Entity::setGlobalRotation(const quaternion& rotation)
{
    mat4 global = globalTransform();

    global.setRotation(rotation);

    setGlobalTransform(global);
}

void Entity::setGlobalScale(const vec3& scale)
{
    mat4 global = globalTransform();

    global.setScale(scale);

    setGlobalTransform(global);
}

void Entity::setGlobalTransform(const mat4& transform)
{
    setLocalTransform(parent() ? parent()->globalTransform().applyToTransform(transform, false) : transform);
}

void Entity::setLinearVelocity(const vec3& linV)
{
    meta.setLinearVelocity(linV);
}

void Entity::setAngularVelocity(const vec3& angV)
{
    meta.setAngularVelocity(angV);
}

bool Entity::has(const string& name) const
{
    return active()
        ? _transformed.fields().contains(name)
        : _base.fields().contains(name);
}

Value Entity::exec(ScriptContext* context, const string& name, const vector<Value>& arguments)
{
    return callHook(context, name, arguments);
}

const Value& Entity::read(const string& name) const
{
    return active()
        ? _transformed.fields().at(name)
        : _base.fields().at(name);
}

void Entity::write(const string& name, const Value& value)
{
    map<string, Value> fields = active() ? _transformed.fields() : _base.fields();

    fields.insert_or_assign(name, value);

    active() ? _transformed.setFields(fields) : _base.setFields(fields);
}

void Entity::remove(const string& name)
{
    map<string, Value> fields = active() ? _transformed.fields() : _base.fields();

    fields.erase(name);

    active() ? _transformed.setFields(fields) : _base.setFields(fields);
}

void Entity::addSample(const string& name, const Sample& sample)
{
    map<string, Sample> samples = meta.samples();

    samples.insert_or_assign(name, sample);

    meta.setSamples(samples);
}

void Entity::removeSample(const string& name)
{
    map<string, Sample> samples = meta.samples();

    samples.erase(name);

    meta.setSamples(samples);
}

void Entity::addAction(const string& name, const Action& action)
{
    map<string, Action> actions = meta.actions();

    actions.insert_or_assign(name, action);

    meta.setActions(actions);
}

void Entity::removeAction(const string& name)
{
    map<string, Action> actions = meta.actions();

    actions.erase(name);

    meta.setActions(actions);
}

SamplePlaybackID Entity::playSample(
    const string& name,
    f64 volume,
    bool directional,
    f64 angle,
    const string& anchorName,
    bool loop
)
{
    SamplePlaybackID id = _transformed.nextSamplePlaybackID();
    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    SamplePlayback playback;
    playback.name = name;
    playback.volume = volume;
    playback.directional = directional;
    playback.angle = angle;
    playback.anchorName = anchorName;
    playback.loop = loop;

    samplePlaybacks.insert_or_assign(id, playback);

    _transformed.setNextSamplePlaybackID(SamplePlaybackID(id + 1));
    _transformed.setSamplePlaybacks(samplePlaybacks);

    return id;
}

SamplePlaybackID Entity::playSamplePreview(const string& name)
{
    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    SamplePlayback playback;
    playback.name = name;
    playback.volume = 1;
    playback.directional = false;
    playback.angle = 0;
    playback.anchorName = "";
    playback.loop = true;

    samplePlaybacks.insert_or_assign(previewSamplePlaybackID, playback);

    _transformed.setSamplePlaybacks(samplePlaybacks);

    return previewSamplePlaybackID;
}

void Entity::stopSample(SamplePlaybackID id)
{
    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    samplePlaybacks.erase(id);

    _transformed.setSamplePlaybacks(samplePlaybacks);
}

void Entity::resumeSamplePlayback(SamplePlaybackID id)
{
    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    auto it = samplePlaybacks.find(id);

    if (it == samplePlaybacks.end())
    {
        return;
    }

    it->second.resume();

    _transformed.setSamplePlaybacks(samplePlaybacks);
}

void Entity::pauseSamplePlayback(SamplePlaybackID id)
{
    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    auto it = samplePlaybacks.find(id);

    if (it == samplePlaybacks.end())
    {
        return;
    }

    it->second.pause();

    _transformed.setSamplePlaybacks(samplePlaybacks);
}

void Entity::seekSamplePlayback(SamplePlaybackID id, u64 position)
{
    map<SamplePlaybackID, SamplePlayback> samplePlaybacks = _transformed.samplePlaybacks();

    auto it = samplePlaybacks.find(id);

    if (it == samplePlaybacks.end())
    {
        return;
    }

    it->second.seek(position);

    _transformed.setSamplePlaybacks(samplePlaybacks);
}

ActionPlaybackID Entity::playAction(const string& name, bool loop)
{
    ActionPlaybackID id = _transformed.nextActionPlaybackID();
    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    ActionPlayback playback;
    playback.name = name;
    playback.loop = loop;

    actionPlaybacks.insert_or_assign(id, playback);

    _transformed.setNextActionPlaybackID(ActionPlaybackID(id + 1));
    _transformed.setActionPlaybacks(actionPlaybacks);

    return id;
}

ActionPlaybackID Entity::playActionPreview(const string& name)
{
    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    ActionPlayback playback;
    playback.name = name;
    playback.loop = true;

    actionPlaybacks.insert_or_assign(previewActionPlaybackID, playback);

    _transformed.setActionPlaybacks(actionPlaybacks);

    return previewActionPlaybackID;
}

void Entity::stopAction(ActionPlaybackID id)
{
    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    actionPlaybacks.erase(id);

    _transformed.setActionPlaybacks(actionPlaybacks);
}

void Entity::resumeActionPlayback(ActionPlaybackID id)
{
    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    auto it = actionPlaybacks.find(id);

    if (it == actionPlaybacks.end())
    {
        return;
    }

    it->second.resume();

    _transformed.setActionPlaybacks(actionPlaybacks);
}

void Entity::pauseActionPlayback(ActionPlaybackID id)
{
    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    auto it = actionPlaybacks.find(id);

    if (it == actionPlaybacks.end())
    {
        return;
    }

    it->second.pause();

    _transformed.setActionPlaybacks(actionPlaybacks);
}

void Entity::seekActionPlayback(ActionPlaybackID id, u64 position)
{
    map<ActionPlaybackID, ActionPlayback> actionPlaybacks = _transformed.actionPlaybacks();

    auto it = actionPlaybacks.find(id);

    if (it == actionPlaybacks.end())
    {
        return;
    }

    it->second.seek(position);

    _transformed.setActionPlaybacks(actionPlaybacks);
}

void Entity::setMass(f64 v)
{
    active() ? _transformed.setMass(v) : _base.setMass(v);

    if (active() && roughly(v, 0.0))
    {
        setLinearVelocity(vec3());
        setAngularVelocity(vec3());
    }
}

void Entity::setDrag(f64 v)
{
    active() ? _transformed.setDrag(v) : _base.setDrag(v);
}

void Entity::setBuoyancy(f64 v)
{
    active() ? _transformed.setBuoyancy(v) : _base.setBuoyancy(v);
}

void Entity::setLift(f64 v)
{
    active() ? _transformed.setLift(v) : _base.setLift(v);
}

void Entity::setMagnetism(f64 v)
{
    active() ? _transformed.setMagnetism(v) : _base.setMagnetism(v);
}

void Entity::setFriction(f64 v)
{
    active() ? _transformed.setFriction(v) : _base.setFriction(v);
}

void Entity::setRestitution(f64 v)
{
    active() ? _transformed.setRestitution(v) : _base.setRestitution(v);
}

void Entity::setHost(bool state)
{
    active() ? _transformed.setHost(state) : _base.setHost(state);
}

void Entity::setShown(bool state)
{
    active() ? _transformed.setShown(state) : _base.setShown(state);
}

void Entity::setPhysical(bool state)
{
    active() ? _transformed.setPhysical(state) : _base.setPhysical(state);
}

void Entity::setVisible(bool state)
{
    active() ? _transformed.setVisible(state) : _base.setVisible(state);
}

void Entity::setAudible(bool state)
{
    active() ? _transformed.setAudible(state) : _base.setAudible(state);
}

void Entity::addBinding(InputType input, const InputBinding& binding)
{
    map<InputType, InputBinding> bindings = active() ? _transformed.bindings() : _base.bindings();

    bindings.insert_or_assign(input, binding);

    active() ? _transformed.setBindings(bindings) : _base.setBindings(bindings);
}

void Entity::removeBinding(InputType input)
{
    map<InputType, InputBinding> bindings = active() ? _transformed.bindings() : _base.bindings();

    bindings.erase(input);

    active() ? _transformed.setBindings(bindings) : _base.setBindings(bindings);
}

void Entity::addHUDElement(const string& name, const HUDElement& element)
{
    map<string, HUDElement> hudElements = active() ? _transformed.hudElements() : _base.hudElements();

    hudElements.insert_or_assign(name, element);

    active() ? _transformed.setHUDElements(hudElements) : _base.setHUDElements(hudElements);
}

void Entity::removeHUDElement(const string& name)
{
    map<string, HUDElement> hudElements = active() ? _transformed.hudElements() : _base.hudElements();

    hudElements.erase(name);

    active() ? _transformed.setHUDElements(hudElements) : _base.setHUDElements(hudElements);
}

void Entity::setFieldOfView(f64 fov)
{
    active() ? _transformed.setFieldOfView(fov) : _base.setFieldOfView(fov);
}

void Entity::setMouseLocked(bool state)
{
    active() ? _transformed.setMouseLocked(state) : _base.setMouseLocked(state);
}

void Entity::setVoiceVolume(f64 volume)
{
    active() ? _transformed.setVoiceVolume(volume) : _base.setVoiceVolume(volume);
}

void Entity::setTintColor(const vec3& color)
{
    active() ? _transformed.setTintColor(color) : _base.setTintColor(color);
}

void Entity::setTintStrength(f64 strength)
{
    active() ? _transformed.setTintStrength(strength) : _base.setTintStrength(strength);
}

f64 Entity::boundingRadius() const
{
    f64 radius = 0;

    (active() ? _transformed.body() : _base.body()).traverse([&](const BodyPart* part) -> void {
        radius = max(radius, part->boundingRadius());
    });

    return radius;
}

BoundingBox Entity::boundingBox() const
{
    BoundingBox box(vec3(0), vec3(0));

    (active() ? _transformed.body() : _base.body()).traverse([&](const BodyPart* part) -> void {
        box.add(part->boundingBox());
    });

    return box;
}

f64 Entity::volume() const
{
    vec3 size = boundingBox().size();

    return size.x * size.y * size.z;
}

f64 Entity::buoyancyVolume() const
{
    return buoyancy() * volume();
}

f64 Entity::angularCrossSectionalArea() const
{
    return pi * sq(boundingRadius());
}

f64 Entity::linearCrossSectionalArea(const vec3& globalDirection) const
{
    vec3 localDirection = globalTransform().applyToDirection(globalDirection, false);

    vec3 size = boundingBox().size();

    f64 sideArea = size.y * size.z;
    f64 frontArea = size.x * size.z;
    f64 topArea = size.x * size.y;

    return
        abs(rightDir.dot(localDirection)) * sideArea +
        abs(forwardDir.dot(localDirection)) * frontArea +
        abs(upDir.dot(localDirection)) * topArea;
}

vector<vec3> Entity::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vec3 start = globalTransform().applyToPosition(position, false);
    vec3 end = globalTransform().applyToPosition(position + direction * distance, false);

    vector<vec3> intersections = active()
        ? _transformed.body().intersect(
            start,
            (end - start).normalize(),
            start.distance(end)
        )
        : _base.body().intersect(
            start,
            (end - start).normalize(),
            start.distance(end)
        );

    for (vec3& intersection : intersections)
    {
        intersection = globalTransform().applyToPosition(intersection);
    }

    return intersections;
}

bool Entity::within(const vec3& position) const
{
    return active()
        ? _transformed.body().within(globalTransform().applyToPosition(position, false))
        : _base.body().within(globalTransform().applyToPosition(position, false));
}

optional<mat4> Entity::anchor(const string& name) const
{
    BodyPart* anchor = active()
        ? _transformed.body().get(Body_Anchor_Generic, name)
        : _base.body().get(Body_Anchor_Generic, name);

    if (!anchor)
    {
        return {};
    }

    return globalTransform().applyToTransform(anchor->regionalTransform());
}

string Entity::fieldsToString() const
{
    return active() ? _transformed.fieldsToString() : _base.fieldsToString();
}

void Entity::fieldsFromString(ScriptContext* context, const string& text)
{
    active() ? _transformed.fieldsFromString(context, text) : _base.fieldsFromString(context, text);
}

Value Entity::callHook(ScriptContext* context, const string& name, const vector<Value>& arguments) const
{
    if (!has(name))
    {
        throw toValue("Could not call hook '" + name + "' on entity " + to_string(_id.value()) + " (" + meta.name() + "): not found.");
    }

    const Value& value = read(name);

    if (value.type != Value_Lambda)
    {
        throw toValue("Could not call hook '" + name + "' on entity " + to_string(_id.value()) + " (" + meta.name() + "): not a lambda value.");
    }

    ScopeClosure closure(context);

    context->define(
        "self",
        toValue(_id)
    );

    context->define(
        "owner",
        toValue(meta.ownerID().value())
    );

    context->trace(name + " on entity " + to_string(_id.value()) + " (" + meta.name() + ")");

#if defined(LMOD_SERVER)
    optional<Value> result = runSafely(
        context,
        value.lambda,
        arguments,
        "running entity hook '" + meta.name() + "'",
        context->game->getUser(UserID(meta.ownerID()))
    );
#elif defined(LMOD_CLIENT)
    optional<Value> result = runSafely(
        context,
        value.lambda,
        arguments,
        "running entity hook '" + meta.name() + "'"
    );
#endif

    context->untrace();

    if (!result)
    {
        return Value();
    }

    return *result;
}

void Entity::copy(const Entity& rhs)
{
    _world = rhs._world;
    _parent = rhs._parent;

    _id = rhs._id;
    meta = rhs.meta;
    _base = rhs._base;
    _transformed = rhs._transformed;

    _children = rhs._children;
}

template<>
void YAMLSerializer::emit(Entity* v)
{
    startMapping();

    emitPair("id", v->id());
    emitPair("meta", v->meta);
    emitPair("base", v->_base);
    emitPair("transformed", v->_transformed);
    emitPair("children", v->_children);

    endMapping();
}
