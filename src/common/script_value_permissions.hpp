/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

class User;

struct ScriptValuePermissions
{
    ScriptValuePermissions();
    explicit ScriptValuePermissions(UserID id);
    ScriptValuePermissions(UserID id, bool restricted);
    explicit ScriptValuePermissions(const User* user);

    bool canRead(const User* user) const;
    bool canWrite(const User* user) const;

    bool operator==(const ScriptValuePermissions& rhs) const;
    bool operator!=(const ScriptValuePermissions& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            restricted
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        restricted = get<1>(payload);
    }

    typedef msgpack::type::tuple<UserID, bool> layout;

    UserID id;
    bool restricted;
};
