/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "symbols.hpp"

struct HUDElementLine
{
    HUDElementLine();

    void verify();

    bool operator==(const HUDElementLine& rhs) const;
    bool operator!=(const HUDElementLine& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            length,
            rotation
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        tie(length, rotation) = payload;
    }

    typedef msgpack::type::tuple<f64, f64> layout;

    f64 length;
    f64 rotation;
};

struct HUDElementText
{
    HUDElementText();

    void verify();

    bool operator==(const HUDElementText& rhs) const;
    bool operator!=(const HUDElementText& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            text
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        tie(text) = payload;
    }

    typedef msgpack::type::tuple<string> layout;

    string text;
};

struct HUDElementBar
{
    HUDElementBar();

    void verify();

    bool operator==(const HUDElementBar& rhs) const;
    bool operator!=(const HUDElementBar& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            fraction,
            length,
            rightToLeft
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        tie(fraction, length, rightToLeft) = payload;
    }

    typedef msgpack::type::tuple<f64, f64, bool> layout;

    f64 fraction;
    f64 length;
    bool rightToLeft;
};

struct HUDElementSymbol
{
    HUDElementSymbol();

    void verify();

    bool operator==(const HUDElementSymbol& rhs) const;
    bool operator!=(const HUDElementSymbol& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            type,
            size
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        tie(reinterpret_cast<u8&>(type), size) = payload;
    }

    typedef msgpack::type::tuple<u8, f64> layout;

    SymbolType type;
    f64 size;
};
