/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_part.hpp"
#include "entity_update.hpp"
#include "tools.hpp"

#if defined(LMOD_CLIENT)
#include "client/render_generators.hpp"
#endif

string bodyPartTypeToString(BodyPartType type)
{
    switch (type)
    {
    case Body_Part_Line :
        return "body-part-line";
    case Body_Part_Plane :
        return "body-part-plane";
    case Body_Part_Anchor :
        return "body-part-anchor";
    case Body_Part_Solid :
        return "body-part-solid";
    case Body_Part_Text :
        return "body-part-text";
    case Body_Part_Symbol :
        return "body-part-symbol";
    case Body_Part_Canvas :
        return "body-part-canvas";
    case Body_Part_Structure :
        return "body-part-structure";
    case Body_Part_Terrain :
        return "body-part-terrain";
    case Body_Part_Light :
        return "body-part-light";
    case Body_Part_Force :
        return "body-part-force";
    case Body_Part_Area :
        return "body-part-area";
    case Body_Part_Rope :
        return "body-part-rope";
    default :
        fatal("Encountered unknown body part type '" + to_string(type) + "'.");
    }
}

BodyPartType bodyPartTypeFromString(const string& s)
{
    if (s == "body-part-line")
    {
        return Body_Part_Line;
    }
    else if (s == "body-part-plane")
    {
        return Body_Part_Plane;
    }
    else if (s == "body-part-anchor")
    {
        return Body_Part_Anchor;
    }
    else if (s == "body-part-solid")
    {
        return Body_Part_Solid;
    }
    else if (s == "body-part-text")
    {
        return Body_Part_Text;
    }
    else if (s == "body-part-symbol")
    {
        return Body_Part_Symbol;
    }
    else if (s == "body-part-canvas")
    {
        return Body_Part_Canvas;
    }
    else if (s == "body-part-structure")
    {
        return Body_Part_Structure;
    }
    else if (s == "body-part-terrain")
    {
        return Body_Part_Terrain;
    }
    else if (s == "body-part-light")
    {
        return Body_Part_Light;
    }
    else if (s == "body-part-force")
    {
        return Body_Part_Force;
    }
    else if (s == "body-part-area")
    {
        return Body_Part_Area;
    }
    else if (s == "body-part-rope")
    {
        return Body_Part_Rope;
    }
    else
    {
        fatal("Encountered unknown body part type '" + s + "'.");
    }
}

template<>
BodyPartType YAMLNode::convert() const
{
    return bodyPartTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(BodyPartType v)
{
    emit(bodyPartTypeToString(v));
}

BodyPart::BodyPart()
    : _parent(nullptr)
    , _id(1)
    , _mass(0)
    , _maxForce(0)
    , _maxVelocity(0)
    , _type(Body_Part_Line)
    , data(BodyPartLine())
    , _constraintType(Body_Constraint_Fixed)
    , constraint(BodyConstraintFixed())
    , _constraintParentSubID(0)
{

}

BodyPart::BodyPart(BodyPart* parent, BodyPartID id)
    : BodyPart()
{
    _id = id;
    _parent = parent;
}

BodyPart::BodyPart(BodyPart* parent, BodyPartID id, const BodyPart& rhs)
{
    *this = BodyPart(rhs);

    _id = id;
    _parent = parent;
}

BodyPart::BodyPart(const BodyPart& rhs)
{
    copyWithoutParent(rhs);
    _parts.clear();

    for (const auto& [ id, part ] : rhs._parts)
    {
        _parts.insert({ id, new BodyPart(*part) });
    }

    for (auto& [ id, part ] : _parts)
    {
        part->_parent = this;
    }
}

BodyPart::BodyPart(BodyPart&& rhs) noexcept
{
    copyWithoutParent(rhs);
    rhs._parts.clear();

    for (auto& [ id, part ] : _parts)
    {
        part->_parent = this;
    }
}

BodyPart::~BodyPart()
{
    for (const auto& [ id, part ] : _parts)
    {
        delete part;
    }
}

BodyPart& BodyPart::operator=(const BodyPart& rhs)
{
    if (&rhs != this)
    {
        for (const auto& [ id, part ] : _parts)
        {
            delete part;
        }

        copyWithoutParent(rhs);
        _parts.clear();

        for (const auto& [ id, part ] : rhs._parts)
        {
            _parts.insert({ id, new BodyPart(*part) });
        }

        for (auto& [ id, part ] : _parts)
        {
            part->_parent = this;
        }
    }

    return *this;
}

BodyPart& BodyPart::operator=(BodyPart&& rhs) noexcept
{
    if (&rhs != this)
    {
        for (const auto& [ id, part ] : _parts)
        {
            delete part;
        }

        copyWithoutParent(rhs);
        rhs._parts.clear();

        for (auto& [ id, part ] : _parts)
        {
            part->_parent = this;
        }
    }

    return *this;
}

BodyPart* BodyPart::load(BodyPart* parent, const YAMLNode& node)
{
    auto part = new BodyPart(parent, node.at("id").as<BodyPartID>());

    part->transform = node.at("transform").as<mat4>();
    part->_mass = node.at("mass").as<f64>();
    part->_maxForce = node.at("maxForce").as<f64>();
    part->_maxVelocity = node.at("maxVelocity").as<f64>();
    part->_type = bodyPartTypeFromString(node.at("type").as<string>());

    switch (part->_type)
    {
    case Body_Part_Line :
        part->data = node.at("data").as<BodyPartLine>();
        break;
    case Body_Part_Plane :
        part->data = node.at("data").as<BodyPartPlane>();
        break;
    case Body_Part_Anchor :
        part->data = node.at("data").as<BodyPartAnchor>();
        break;
    case Body_Part_Solid :
        part->data = node.at("data").as<BodyPartSolid>();
        break;
    case Body_Part_Text :
        part->data = node.at("data").as<BodyPartText>();
        break;
    case Body_Part_Symbol :
        part->data = node.at("data").as<BodyPartSymbol>();
        break;
    case Body_Part_Canvas :
        part->data = node.at("data").as<BodyPartCanvas>();
        break;
    case Body_Part_Structure :
        part->data = node.at("data").as<BodyPartStructure>();
        break;
    case Body_Part_Terrain :
        part->data = node.at("data").as<BodyPartTerrain>();
        break;
    case Body_Part_Light :
        part->data = node.at("data").as<BodyPartLight>();
        break;
    case Body_Part_Force :
        part->data = node.at("data").as<BodyPartForce>();
        break;
    case Body_Part_Area :
        part->data = node.at("data").as<BodyPartArea>();
        break;
    case Body_Part_Rope :
        part->data = node.at("data").as<BodyPartRope>();
        break;
    }

    part->_constraintType = node.at("constraintType").as<BodyConstraintType>();

    switch (part->_constraintType)
    {
    case Body_Constraint_Fixed :
        part->constraint = node.at("constraint").as<BodyConstraintFixed>();
        break;
    case Body_Constraint_Ball :
        part->constraint = node.at("constraint").as<BodyConstraintBall>();
        break;
    case Body_Constraint_Cone :
        part->constraint = node.at("constraint").as<BodyConstraintCone>();
        break;
    case Body_Constraint_Hinge :
        part->constraint = node.at("constraint").as<BodyConstraintHinge>();
        break;
    }

    part->_constraintParentSubID = node.at("constraintParentSubID").as<BodyPartSubID>();

    for (const auto& [ id, node ] : node.at("parts").as<map<BodyPartID, YAMLNode>>())
    {
        part->_parts.insert({ id, BodyPart::load(part, node) });
    }

    return part;
}

void BodyPart::verify()
{
    transform = verifyTransform(transform);
    _mass = max(_mass, 0.0);
    _maxForce = max(_maxForce, 0.0);
    _maxVelocity = max(_maxVelocity, 0.0);

    if (auto line = get_if<BodyPartLine>(&data); line && _type == Body_Part_Line)
    {
        line->verify();
    }
    else if (auto plane = get_if<BodyPartPlane>(&data); plane && _type == Body_Part_Plane)
    {
        plane->verify();
    }
    else if (auto anchor = get_if<BodyPartAnchor>(&data); anchor && _type == Body_Part_Anchor)
    {
        anchor->verify();
    }
    else if (auto solid = get_if<BodyPartSolid>(&data); solid && _type == Body_Part_Solid)
    {
        solid->verify();
    }
    else if (auto text = get_if<BodyPartText>(&data); text && _type == Body_Part_Text)
    {
        text->verify();
    }
    else if (auto symbol = get_if<BodyPartSymbol>(&data); symbol && _type == Body_Part_Symbol)
    {
        symbol->verify();
    }
    else if (auto canvas = get_if<BodyPartCanvas>(&data); canvas && _type == Body_Part_Canvas)
    {
        canvas->verify();
    }
    else if (auto structure = get_if<BodyPartLight>(&data); structure && _type == Body_Part_Structure)
    {
        structure->verify();
    }
    else if (auto terrain = get_if<BodyPartTerrain>(&data); terrain && _type == Body_Part_Terrain)
    {
        terrain->verify();
    }
    else if (auto light = get_if<BodyPartLight>(&data); light && _type == Body_Part_Light)
    {
        light->verify();
    }
    else if (auto force = get_if<BodyPartForce>(&data); force && _type == Body_Part_Force)
    {
        force->verify();
    }
    else if (auto area = get_if<BodyPartArea>(&data); area && _type == Body_Part_Area)
    {
        area->verify();
    }
    else if (auto rope = get_if<BodyPartRope>(&data); rope && _type == Body_Part_Rope)
    {
        rope->verify();
    }
    else
    {
        _type = Body_Part_Line;
        data = BodyPartLine();
    }

    if (auto fixed = get_if<BodyConstraintFixed>(&constraint); fixed && _constraintType == Body_Constraint_Fixed)
    {
        fixed->verify();
    }
    else if (auto ball = get_if<BodyConstraintBall>(&constraint); ball && _constraintType == Body_Constraint_Ball)
    {
        ball->verify();
    }
    else if (auto cone = get_if<BodyConstraintCone>(&constraint); cone && _constraintType == Body_Constraint_Cone)
    {
        cone->verify();
    }
    else if (auto hinge = get_if<BodyConstraintHinge>(&constraint); hinge && _constraintType == Body_Constraint_Hinge)
    {
        hinge->verify();
    }
    else
    {
        _constraintType = Body_Constraint_Fixed;
        constraint = BodyConstraintFixed();
    }

    if (_parent)
    {
        _constraintParentSubID = BodyPartSubID(min<u32>(_constraintParentSubID.value(), _parent->subparts().size()));
    }
    else
    {
        _constraintParentSubID = BodyPartSubID();
    }

    for (auto& [ id, part ] : _parts)
    {
        part->verify();
    }
}

void BodyPart::compare(const BodyPart& previous, EntityUpdate& update, bool transformed) const
{
    if (transform != previous.transform ||
        _mass != previous._mass ||
        _maxForce != previous._maxForce ||
        _maxVelocity != previous._maxVelocity ||
        _type != previous._type ||
        data != previous.data ||
        _constraintType != previous._constraintType ||
        constraint != previous.constraint ||
        _constraintParentSubID != previous._constraintParentSubID
    )
    {
        update.notify<bool, EntityBodyEvent>(true, false, [&](EntityBodyEvent& event) -> void {
            event.type = Entity_Body_Event_Update_Part;
            event.parentID = _parent ? _parent->_id : BodyPartID();
            event.partID = _id;
            event.part = *this;
        }, transformed);
    }

    update.notifyMap<BodyPartID, BodyPart*, EntityBodyEvent>(
        _parts,
        previous._parts,
        [&](EntityBodyEvent& event, BodyPartID id, const BodyPart* part) -> void
        {
            event.type = Entity_Body_Event_Add_Part;
            event.parentID = _id;
            event.partID = id;
            event.part = *part;
        },
        [&](EntityBodyEvent& event, BodyPartID id) -> void
        {
            event.type = Entity_Body_Event_Remove_Part;
            event.parentID = _id;
            event.partID = id;
        },
        [&](BodyPartID id, const BodyPart* current, const BodyPart* previous) -> void
        {
            current->compare(*previous, update, transformed);
        },
        transformed
    );
}

void BodyPart::push(const NetworkEvent& event)
{
    auto subEvent = event.payload<EntityBodyEvent>();

    switch (subEvent.type)
    {
    default :
        break;
    case Entity_Body_Event_Add_Part :
        if (subEvent.parentID == _id)
        {
            auto it = _parts.find(subEvent.partID);

            if (it != _parts.end())
            {
                delete it->second;
            }

            _parts.insert_or_assign(subEvent.partID, new BodyPart(this, subEvent.partID, subEvent.part));
        }
        else
        {
            for (auto& [ id, part ] : _parts)
            {
                part->push(event);
            }
        }
        break;
    case Entity_Body_Event_Remove_Part :
        if (subEvent.parentID == _id)
        {
            auto it = _parts.find(subEvent.partID);

            if (it != _parts.end())
            {
                delete it->second;
                _parts.erase(it);
            }
        }
        else
        {
            for (auto& [ id, part ] : _parts)
            {
                part->push(event);
            }
        }
        break;
    case Entity_Body_Event_Update_Part :
        if (subEvent.parentID == _id)
        {
            auto it = _parts.find(subEvent.partID);

            if (it != _parts.end())
            {
                it->second->replaceSelfOnly(subEvent.part);
            }
        }
        else
        {
            for (auto& [ id, part ] : _parts)
            {
                part->push(event);
            }
        }
        break;
    }
}

vector<NetworkEvent> BodyPart::initial(const string& world, EntityID id, bool transformed) const
{
    vector<NetworkEvent> events;

    EntityBodyEvent event;
    event.type = Entity_Body_Event_Add_Part;
    event.world = world;
    event.id = id;
    event.parentID = _parent ? _parent->_id : BodyPartID();
    event.partID = _id;
    event.part = *this;
    event.transformed = transformed;

    events.push_back(event);

    for (const auto& [ partID, part ] : _parts)
    {
        vector<NetworkEvent> partEvents = part->initial(world, id, transformed);

        events.insert(
            events.end(),
            partEvents.begin(),
            partEvents.end()
        );
    }

    return events;
}

#if defined(LMOD_CLIENT)
BodyRenderData BodyPart::toRender(const set<BodyPartID>& hiddenBodyPartIDs) const
{
    BodyRenderData data;

    if (!hiddenBodyPartIDs.contains(_id))
    {
        switch (_type)
        {
        case Body_Part_Line :
            data.add(generateLine(regionalTransform(), get<BodyPartLine>()));
            break;
        case Body_Part_Plane :
            data.add(generatePlane(regionalTransform(), get<BodyPartPlane>()));
            break;
        case Body_Part_Anchor :
            break;
        case Body_Part_Solid :
            data.add(generateSolid(regionalTransform(), get<BodyPartSolid>()));
            break;
        case Body_Part_Text :
            data.add(generateText(regionalTransform(), get<BodyPartText>()));
            break;
        case Body_Part_Symbol :
            data.add(generateSymbol(regionalTransform(), get<BodyPartSymbol>()));
            break;
        case Body_Part_Canvas :
            data.add(generateCanvas(regionalTransform(), get<BodyPartCanvas>()));
            break;
        case Body_Part_Structure :
            data.add(generateStructure(regionalTransform(), get<BodyPartStructure>()));
            break;
        case Body_Part_Terrain :
            data.add(generateTerrain(regionalTransform(), get<BodyPartTerrain>()));
            break;
        case Body_Part_Light :
            data.add(generateLight(regionalTransform(), get<BodyPartLight>()));
            break;
        case Body_Part_Force :
            break;
        case Body_Part_Area :
            data.add(generateArea(regionalTransform(), get<BodyPartArea>()));
            break;
        case Body_Part_Rope :
            data.add(generateRope(regionalTransform(), get<BodyPartRope>()));
            break;
        }
    }

    for (const auto& [ id, part ] : _parts)
    {
        data.add(part->toRender(hiddenBodyPartIDs));
    }

    return data;
}
#endif

void BodyPart::add(BodyPartID parentID, BodyPart* part)
{
    if (parentID == _id)
    {
        _parts.insert({ part->id(), part });
    }
    else
    {
        for (auto& [ childID, child ] : _parts)
        {
            child->add(parentID, part);
        }
    }
}

void BodyPart::remove(BodyPartID id)
{
    auto it = _parts.find(id);

    if (it != _parts.end())
    {
        delete it->second;
        _parts.erase(it);
        return;
    }

    for (auto& [ childID, child ] : _parts)
    {
        child->remove(id);
    }
}

void BodyPart::update(BodyPartID id, const BodyPart& part)
{
    auto it = _parts.find(id);

    if (it != _parts.end())
    {
        *it->second = part;
        return;
    }

    for (auto& [ childID, child ] : _parts)
    {
        child->update(id, part);
    }
}

void BodyPart::clear()
{
    for (auto& [ id, part ] : _parts)
    {
        delete part;
    }

    _parts.clear();
}

BodyPart* BodyPart::parent() const
{
    return _parent;
}

BodyPart* BodyPart::rootParent()
{
    return _parent ? _parent->rootParent() : this;
}

bool BodyPart::hasParent(BodyPart* possible) const
{
    if (_parent)
    {
        if (_parent == possible)
        {
            return true;
        }
        else
        {
            return _parent->hasParent(possible);
        }
    }
    else
    {
        return false;
    }
}

size_t BodyPart::parentDepth() const
{
    return _parent ? 1 + _parent->parentDepth() : 0;
}

vec3 BodyPart::parentRegionalPosition() const
{
    if (!_parent)
    {
        return vec3();
    }

    if (_constraintParentSubID == BodyPartSubID())
    {
        return _parent->regionalPosition();
    }

    return _parent->subparts().at(_constraintParentSubID.value() - 1);
}

BodyPart* BodyPart::get(BodyPartID id) const
{
    auto it = _parts.find(id);

    if (it != _parts.end())
    {
        return it->second;
    }

    for (const auto& [ partID, part ] : _parts)
    {
        BodyPart* result = part->get(id);

        if (result)
        {
            return result;
        }
    }

    return nullptr;
}

BodyPart* BodyPart::get(BodyAnchorType type, const string& name) const
{
    for (const auto& [ partID, part ] : _parts)
    {
        if (part->_type == Body_Part_Anchor &&
            get<BodyPartAnchor>().type == type &&
            get<BodyPartAnchor>().name == name
        )
        {
            return part;
        }

        BodyPart* result = part->get(type, name);

        if (result)
        {
            return result;
        }
    }

    return nullptr;
}

const map<BodyPartID, BodyPart*>& BodyPart::parts() const
{
    return _parts;
}

void BodyPart::traverse(const function<void(BodyPart*)>& behavior)
{
    behavior(this);

    for (auto& [ id, part ] : _parts)
    {
        part->traverse(behavior);
    }
}

void BodyPart::traverse(const function<void(const BodyPart*)>& behavior) const
{
    behavior(this);

    for (const auto& [ id, part ] : _parts)
    {
        static_cast<const BodyPart*>(part)->traverse(behavior);
    }
}

bool BodyPart::partiallyTraverse(const function<bool(BodyPart*)>& behavior)
{
    if (behavior(this))
    {
        return true;
    }

    for (auto& [ id, part ] : _parts)
    {
        if (part->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool BodyPart::partiallyTraverse(const function<bool(const BodyPart*)>& behavior) const
{
    if (behavior(this))
    {
        return true;
    }

    for (const auto& [ id, part ] : _parts)
    {
        if (static_cast<const BodyPart*>(part)->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

vec3 BodyPart::localPosition() const
{
    return localTransform().position();
}

quaternion BodyPart::localRotation() const
{
    return localTransform().rotation();
}

vec3 BodyPart::localScale() const
{
    return localTransform().scale();
}

mat4 BodyPart::localTransform() const
{
    return transform;
}

vec3 BodyPart::regionalPosition() const
{
    return regionalTransform().position();
}

quaternion BodyPart::regionalRotation() const
{
    return regionalTransform().rotation();
}

vec3 BodyPart::regionalScale() const
{
    return regionalTransform().scale();
}

mat4 BodyPart::regionalTransform() const
{
    return _parent ? _parent->regionalTransform().applyToTransform(localTransform()) : localTransform();
}

void BodyPart::setLocalPosition(const vec3& position)
{
    mat4 local = localTransform();

    local.setPosition(position);

    setLocalTransform(local);
}

void BodyPart::setLocalRotation(const quaternion& rotation)
{
    mat4 local = localTransform();

    local.setRotation(rotation);

    setLocalTransform(local);
}

void BodyPart::setLocalScale(const vec3& scale)
{
    mat4 local = localTransform();

    local.setScale(scale);

    setLocalTransform(local);
}

void BodyPart::setLocalTransform(const mat4& transform)
{
    this->transform = transform;
}

void BodyPart::setRegionalPosition(const vec3& position)
{
    mat4 regional = regionalTransform();

    regional.setPosition(position);

    setRegionalTransform(regional);
}

void BodyPart::setRegionalRotation(const quaternion& rotation)
{
    mat4 regional = regionalTransform();

    regional.setRotation(rotation);

    setRegionalTransform(regional);
}

void BodyPart::setRegionalScale(const vec3& scale)
{
    mat4 regional = regionalTransform();

    regional.setScale(scale);

    setRegionalTransform(regional);
}

void BodyPart::setRegionalTransform(const mat4& transform)
{
    setLocalTransform(_parent ? _parent->regionalTransform().applyToTransform(transform, false) : transform);
}

bool BodyPart::collides() const
{
    switch (_type)
    {
    default :
        fatal("Encountered unknown body part type '" + to_string(_type) + "'.");
    case Body_Part_Line :
    case Body_Part_Plane :
    case Body_Part_Solid :
    case Body_Part_Structure :
    case Body_Part_Terrain :
    case Body_Part_Rope :
        return true;
    case Body_Part_Anchor :
    case Body_Part_Text :
    case Body_Part_Symbol :
    case Body_Part_Canvas :
    case Body_Part_Light :
    case Body_Part_Force :
    case Body_Part_Area :
        return false;
    }
}

vector<vec3> BodyPart::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vector<vec3> intersections;

    vec3 start = regionalTransform().applyToPosition(position, false);
    vec3 end = regionalTransform().applyToPosition(position + direction * distance, false);

    switch (_type)
    {
    case Body_Part_Line :
        intersections = get<BodyPartLine>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Plane :
        intersections = get<BodyPartPlane>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Anchor :
        intersections = get<BodyPartAnchor>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Solid :
        intersections = get<BodyPartSolid>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Text :
        intersections = get<BodyPartText>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Symbol :
        intersections = get<BodyPartSymbol>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Canvas :
        intersections = get<BodyPartCanvas>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Structure :
        intersections = get<BodyPartStructure>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Terrain :
        intersections = get<BodyPartTerrain>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Light :
        intersections = get<BodyPartLight>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Force :
        intersections = get<BodyPartForce>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Area :
        intersections = get<BodyPartArea>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    case Body_Part_Rope :
        intersections = get<BodyPartRope>().intersect(start, (end - start).normalize(), start.distance(end));
        break;
    }

    for (vec3& intersection : intersections)
    {
        intersection = regionalTransform().applyToPosition(intersection);
    }

    for (const auto& [ id, part ] : _parts)
    {
        vector<vec3> result = part->intersect(position, direction, distance);

        intersections.insert(
            intersections.end(),
            result.begin(),
            result.end()
        );
    }

    return intersections;
}

bool BodyPart::within(const vec3& position) const
{
    vec3 local = regionalTransform().applyToPosition(position, false);

    switch (_type)
    {
    case Body_Part_Line :
        if (get<BodyPartLine>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Plane :
        if (get<BodyPartPlane>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Anchor :
        if (get<BodyPartAnchor>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Solid :
        if (get<BodyPartSolid>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Text :
        if (get<BodyPartText>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Symbol :
        if (get<BodyPartSymbol>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Canvas :
        if (get<BodyPartCanvas>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Structure :
        if (get<BodyPartStructure>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Terrain :
        if (get<BodyPartTerrain>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Light :
        if (get<BodyPartLight>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Force :
        if (get<BodyPartForce>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Area :
        if (get<BodyPartArea>().within(local))
        {
            return true;
        }
        break;
    case Body_Part_Rope :
        if (get<BodyPartRope>().within(local))
        {
            return true;
        }
        break;
    }

    for (const auto& [ id, part ] : _parts)
    {
        if (part->within(position))
        {
            return true;
        }
    }

    return false;
}

f64 BodyPart::independentPartMass() const
{
    f64 mass = _constraintType != Body_Constraint_Fixed ? _mass : 0;

    for (const auto& [ id, part ] : _parts)
    {
        mass += part->independentPartMass();
    }

    return mass;
}

vector<vec3> BodyPart::subparts() const
{
    vector<vec3> subparts;

    switch (_type)
    {
    default :
        fatal("Encountered unknown body part type '" + to_string(_type) + "'.");
    case Body_Part_Line :
        subparts = get<BodyPartLine>().subparts();
        break;
    case Body_Part_Plane :
        subparts = get<BodyPartPlane>().subparts();
        break;
    case Body_Part_Anchor :
        subparts = get<BodyPartAnchor>().subparts();
        break;
    case Body_Part_Solid :
        subparts = get<BodyPartSolid>().subparts();
        break;
    case Body_Part_Text :
        subparts = get<BodyPartText>().subparts();
        break;
    case Body_Part_Symbol :
        subparts = get<BodyPartSymbol>().subparts();
        break;
    case Body_Part_Canvas :
        subparts = get<BodyPartCanvas>().subparts();
        break;
    case Body_Part_Structure :
        subparts = get<BodyPartStructure>().subparts();
        break;
    case Body_Part_Terrain :
        subparts = get<BodyPartTerrain>().subparts();
        break;
    case Body_Part_Light :
        subparts = get<BodyPartLight>().subparts();
        break;
    case Body_Part_Force :
        subparts = get<BodyPartForce>().subparts();
        break;
    case Body_Part_Area :
        subparts = get<BodyPartArea>().subparts();
        break;
    case Body_Part_Rope :
        subparts = get<BodyPartRope>().subparts();
        break;
    }

    for (vec3& subpart : subparts)
    {
        subpart = regionalTransform().applyToPosition(subpart);
    }

    return subparts;
}

mat4 BodyPart::constraintTransform() const
{
    mat4 transform;

    switch (_type)
    {
    case Body_Part_Line :
        if (get<BodyPartLine>().constraintAtEnd)
        {
            transform.setPosition(get<BodyPartLine>().subparts().back());
        }
        else
        {
            transform.setPosition(get<BodyPartLine>().subparts().front());
        }
        break;
    case Body_Part_Plane :
        if (get<BodyPartPlane>().constraintSubID == BodyPartSubID())
        {
            transform.setPosition(vec3());
        }
        else
        {
            vector<vec3> corners = get<BodyPartPlane>().subparts();

            transform.setPosition(corners[(get<BodyPartPlane>().constraintSubID.value() - 1) % corners.size()]);
        }
        break;
    case Body_Part_Anchor :
        break;
    case Body_Part_Solid :
        if (get<BodyPartSolid>().constraintSubID == BodyPartSubID())
        {
            transform.setPosition(vec3());
        }
        else
        {
            vector<vec3> corners = get<BodyPartSolid>().subparts();

            transform.setPosition(corners[(get<BodyPartSolid>().constraintSubID.value() - 1) % corners.size()]);
        }
        break;
    case Body_Part_Rope :
    {
        vector<vec3> vertices = get<BodyPartRope>().subparts();

        transform.setPosition(vertices[get<BodyPartRope>().constraintSubID.value() % vertices.size()]);
        break;
    }
    case Body_Part_Text :
    case Body_Part_Symbol :
    case Body_Part_Canvas :
    case Body_Part_Structure :
    case Body_Part_Terrain :
    case Body_Part_Light :
    case Body_Part_Force :
    case Body_Part_Area :
        break;
    }

    return regionalTransform().applyToTransform(transform);
}

f64 BodyPart::boundingRadius() const
{
    f64 radius = 0;

    switch (_type)
    {
    case Body_Part_Line :
        radius = get<BodyPartLine>().boundingRadius();
        break;
    case Body_Part_Plane :
        radius = get<BodyPartPlane>().boundingRadius();
        break;
    case Body_Part_Solid :
        radius = get<BodyPartSolid>().boundingRadius();
        break;
    case Body_Part_Structure :
        radius = get<BodyPartStructure>().boundingRadius();
        break;
    case Body_Part_Terrain :
        radius = get<BodyPartTerrain>().boundingRadius();
        break;
    case Body_Part_Rope :
        radius = get<BodyPartRope>().boundingRadius();
        break;
    case Body_Part_Anchor :
    case Body_Part_Text :
    case Body_Part_Symbol :
    case Body_Part_Canvas :
    case Body_Part_Light :
    case Body_Part_Force :
    case Body_Part_Area :
        break;
    }

    if (radius == 0)
    {
        return radius;
    }

    return regionalTransform().position().length() + radius;
}

BoundingBox BodyPart::boundingBox() const
{
    BoundingBox box(vec3(0), vec3(0));

    switch (_type)
    {
    case Body_Part_Line :
        box = get<BodyPartLine>().boundingBox();
        break;
    case Body_Part_Plane :
        box = get<BodyPartPlane>().boundingBox();
        break;
    case Body_Part_Solid :
        box = get<BodyPartSolid>().boundingBox();
        break;
    case Body_Part_Structure :
        box = get<BodyPartStructure>().boundingBox();
        break;
    case Body_Part_Terrain :
        box = get<BodyPartTerrain>().boundingBox();
        break;
    case Body_Part_Rope :
        box = get<BodyPartRope>().boundingBox();
        break;
    case Body_Part_Anchor :
    case Body_Part_Text :
    case Body_Part_Symbol :
    case Body_Part_Canvas :
    case Body_Part_Light :
    case Body_Part_Force :
    case Body_Part_Area :
        break;
    }

    if (roughly(box.size().length(), 0.0))
    {
        return box;
    }

    BoundingBox transformedBox(vec3(0), vec3(0));

    transformedBox.add(transform, box);

    return transformedBox;
}

bool BodyPart::operator==(const BodyPart& rhs) const
{
    if (_id != rhs._id)
    {
        return false;
    }

    if (_type != rhs._type)
    {
        return false;
    }

    if (transform != rhs.transform)
    {
        return false;
    }

    if (_mass != rhs._mass)
    {
        return false;
    }

    if (_maxForce != rhs._maxForce)
    {
        return false;
    }

    if (_maxVelocity != rhs._maxVelocity)
    {
        return false;
    }

    switch (_type)
    {
    case Body_Part_Line :
        if (get<BodyPartLine>() != rhs.get<BodyPartLine>())
        {
            return false;
        }
        break;
    case Body_Part_Plane :
        if (get<BodyPartPlane>() != rhs.get<BodyPartPlane>())
        {
            return false;
        }
        break;
    case Body_Part_Anchor :
        if (get<BodyPartAnchor>() != rhs.get<BodyPartAnchor>())
        {
            return false;
        }
        break;
    case Body_Part_Solid :
        if (get<BodyPartSolid>() != rhs.get<BodyPartSolid>())
        {
            return false;
        }
        break;
    case Body_Part_Text :
        if (get<BodyPartText>() != rhs.get<BodyPartText>())
        {
            return false;
        }
        break;
    case Body_Part_Symbol :
        if (get<BodyPartSymbol>() != rhs.get<BodyPartSymbol>())
        {
            return false;
        }
        break;
    case Body_Part_Canvas :
        if (get<BodyPartCanvas>() != rhs.get<BodyPartCanvas>())
        {
            return false;
        }
        break;
    case Body_Part_Structure :
        if (get<BodyPartStructure>() != rhs.get<BodyPartStructure>())
        {
            return false;
        }
        break;
    case Body_Part_Terrain :
        if (get<BodyPartTerrain>() != rhs.get<BodyPartTerrain>())
        {
            return false;
        }
        break;
    case Body_Part_Light :
        if (get<BodyPartLight>() != rhs.get<BodyPartLight>())
        {
            return false;
        }
        break;
    case Body_Part_Force :
        if (get<BodyPartForce>() != rhs.get<BodyPartForce>())
        {
            return false;
        }
        break;
    case Body_Part_Area :
        if (get<BodyPartArea>() != rhs.get<BodyPartArea>())
        {
            return false;
        }
        break;
    case Body_Part_Rope :
        if (get<BodyPartRope>() != rhs.get<BodyPartRope>())
        {
            return false;
        }
        break;
    }

    if (_constraintType != rhs._constraintType)
    {
        return false;
    }

    switch (_constraintType)
    {
    case Body_Constraint_Fixed :
        if (getConstraint<BodyConstraintFixed>() != rhs.getConstraint<BodyConstraintFixed>())
        {
            return false;
        }
        break;
    case Body_Constraint_Ball :
        if (getConstraint<BodyConstraintBall>() != rhs.getConstraint<BodyConstraintBall>())
        {
            return false;
        }
        break;
    case Body_Constraint_Cone :
        if (getConstraint<BodyConstraintCone>() != rhs.getConstraint<BodyConstraintCone>())
        {
            return false;
        }
        break;
    case Body_Constraint_Hinge :
        if (getConstraint<BodyConstraintHinge>() != rhs.getConstraint<BodyConstraintHinge>())
        {
            return false;
        }
        break;
    }

    if (_constraintParentSubID != rhs._constraintParentSubID)
    {
        return false;
    }

    return dynAllocMapEqual(_parts, rhs._parts);
}

bool BodyPart::operator!=(const BodyPart& rhs) const
{
    return !(*this == rhs);
}

BodyPartID BodyPart::id() const
{
    return _id;
}

f64 BodyPart::mass() const
{
    return _mass;
}

f64 BodyPart::maxForce() const
{
    return _maxForce;
}

f64 BodyPart::maxVelocity() const
{
    return _maxVelocity;
}

BodyPartType BodyPart::type() const
{
    return _type;
}

BodyConstraintType BodyPart::constraintType() const
{
    return _constraintType;
}

BodyPartSubID BodyPart::constraintParentSubID() const
{
    return _constraintParentSubID;
}

void BodyPart::setConstraintParentSubID(BodyPartSubID id)
{
    _constraintParentSubID = id;
}

void BodyPart::setMass(f64 mass)
{
    _mass = mass;
}

void BodyPart::setMaxForce(f64 maxForce)
{
    _maxForce = maxForce;
}

void BodyPart::setMaxVelocity(f64 maxVelocity)
{
    _maxVelocity = maxVelocity;
}

void BodyPart::setType(BodyPartType type)
{
    _type = type;

    switch (_type)
    {
    case Body_Part_Line :
        data = BodyPartLine();
        break;
    case Body_Part_Plane :
        data = BodyPartPlane();
        break;
    case Body_Part_Anchor :
        data = BodyPartAnchor();
        break;
    case Body_Part_Solid :
        data = BodyPartSolid();
        break;
    case Body_Part_Text :
        data = BodyPartText();
        break;
    case Body_Part_Symbol :
        data = BodyPartSymbol();
        break;
    case Body_Part_Canvas :
        data = BodyPartCanvas();
        break;
    case Body_Part_Structure :
        data = BodyPartStructure();
        break;
    case Body_Part_Terrain :
        data = BodyPartTerrain();
        break;
    case Body_Part_Light :
        data = BodyPartLight();
        break;
    case Body_Part_Force :
        data = BodyPartForce();
        break;
    case Body_Part_Area :
        data = BodyPartArea();
        break;
    case Body_Part_Rope :
        data = BodyPartRope();
        break;
    }
}

void BodyPart::setConstraintType(BodyConstraintType type)
{
    _constraintType = type;

    switch (_constraintType)
    {
    case Body_Constraint_Fixed :
        constraint = BodyConstraintFixed();
        break;
    case Body_Constraint_Ball :
        constraint = BodyConstraintBall();
        break;
    case Body_Constraint_Cone :
        constraint = BodyConstraintCone();
        break;
    case Body_Constraint_Hinge :
        constraint = BodyConstraintHinge();
        break;
    }
}

void BodyPart::replaceSelfOnly(const BodyPart& update)
{
    BodyPart updateInTree = update;

    updateInTree._parent = _parent;
    updateInTree._parts = _parts;

    copy(updateInTree);

    // prevent destructor of updateInTree from destroying children
    updateInTree._parent = nullptr;
    updateInTree._parts.clear();
}

string BodyPart::dataToString() const
{
    stringstream ss;

    switch (_type)
    {
    case Body_Part_Line :
        msgpack::pack(ss, get<BodyPartLine>());
        break;
    case Body_Part_Plane :
        msgpack::pack(ss, get<BodyPartPlane>());
        break;
    case Body_Part_Anchor :
        msgpack::pack(ss, get<BodyPartAnchor>());
        break;
    case Body_Part_Solid :
        msgpack::pack(ss, get<BodyPartSolid>());
        break;
    case Body_Part_Text :
        msgpack::pack(ss, get<BodyPartText>());
        break;
    case Body_Part_Symbol :
        msgpack::pack(ss, get<BodyPartSymbol>());
        break;
    case Body_Part_Canvas :
        msgpack::pack(ss, get<BodyPartCanvas>());
        break;
    case Body_Part_Structure :
        msgpack::pack(ss, get<BodyPartStructure>());
        break;
    case Body_Part_Terrain :
        msgpack::pack(ss, get<BodyPartTerrain>());
        break;
    case Body_Part_Light :
        msgpack::pack(ss, get<BodyPartLight>());
        break;
    case Body_Part_Force :
        msgpack::pack(ss, get<BodyPartForce>());
        break;
    case Body_Part_Area :
        msgpack::pack(ss, get<BodyPartArea>());
        break;
    case Body_Part_Rope :
        msgpack::pack(ss, get<BodyPartRope>());
        break;
    }

    return ss.str();
}

void BodyPart::dataFromString(const string& s)
{
    msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

    switch (_type)
    {
    case Body_Part_Line :
    {
        BodyPartLine part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Plane :
    {
        BodyPartPlane part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Anchor :
    {
        BodyPartAnchor part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Solid :
    {
        BodyPartSolid part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Text :
    {
        BodyPartText part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Symbol :
    {
        BodyPartSymbol part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Canvas :
    {
        BodyPartCanvas part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Structure :
    {
        BodyPartStructure part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Terrain :
    {
        BodyPartTerrain part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Light :
    {
        BodyPartLight part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Force :
    {
        BodyPartForce part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Area :
    {
        BodyPartArea part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Body_Part_Rope :
    {
        BodyPartRope part;
        oh.get().convert(part);

        data = part;
        break;
    }
    }
}

string BodyPart::constraintToString() const
{
    stringstream ss;

    switch (_constraintType)
    {
    case Body_Constraint_Fixed :
        msgpack::pack(ss, getConstraint<BodyConstraintFixed>());
        break;
    case Body_Constraint_Ball :
        msgpack::pack(ss, getConstraint<BodyConstraintBall>());
        break;
    case Body_Constraint_Cone :
        msgpack::pack(ss, getConstraint<BodyConstraintCone>());
        break;
    case Body_Constraint_Hinge :
        msgpack::pack(ss, getConstraint<BodyConstraintHinge>());
        break;
    }

    return ss.str();
}

void BodyPart::constraintFromString(const string& s)
{
    switch (_constraintType)
    {
    case Body_Constraint_Fixed :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        BodyConstraintFixed part;
        oh.get().convert(part);

        constraint = part;
        break;
    }
    case Body_Constraint_Ball :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        BodyConstraintBall part;
        oh.get().convert(part);

        constraint = part;
        break;
    }
    case Body_Constraint_Cone :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        BodyConstraintCone part;
        oh.get().convert(part);

        constraint = part;
        break;
    }
    case Body_Constraint_Hinge :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        BodyConstraintHinge part;
        oh.get().convert(part);

        constraint = part;
        break;
    }
    }
}

void BodyPart::copy(const BodyPart& rhs)
{
    _parent = rhs._parent;
    _id = rhs._id;
    transform = rhs.transform;
    _mass = rhs._mass;
    _maxForce = rhs._maxForce;
    _maxVelocity = rhs._maxVelocity;

    _type = rhs._type;
    data = rhs.data;

    _constraintType = rhs._constraintType;
    constraint = rhs.constraint;
    _constraintParentSubID = rhs._constraintParentSubID;

    _parts = rhs._parts;
}

void BodyPart::copyWithoutParent(const BodyPart& rhs)
{
    copy(rhs);
    _parent = nullptr;
}

template<>
void YAMLSerializer::emit(BodyPart* v)
{
    startMapping();

    emitPair("id", v->_id);
    emitPair("transform", v->localTransform());
    emitPair("mass", v->_mass);
    emitPair("maxForce", v->_maxForce);
    emitPair("maxVelocity", v->_maxVelocity);

    emitPair("type", bodyPartTypeToString(v->_type));
    switch (v->_type)
    {
    case Body_Part_Line :
        emitPair("data", v->get<BodyPartLine>());
        break;
    case Body_Part_Plane :
        emitPair("data", v->get<BodyPartPlane>());
        break;
    case Body_Part_Anchor :
        emitPair("data", v->get<BodyPartAnchor>());
        break;
    case Body_Part_Solid :
        emitPair("data", v->get<BodyPartSolid>());
        break;
    case Body_Part_Text :
        emitPair("data", v->get<BodyPartText>());
        break;
    case Body_Part_Symbol :
        emitPair("data", v->get<BodyPartSymbol>());
        break;
    case Body_Part_Canvas :
        emitPair("data", v->get<BodyPartCanvas>());
        break;
    case Body_Part_Structure :
        emitPair("data", v->get<BodyPartStructure>());
        break;
    case Body_Part_Terrain :
        emitPair("data", v->get<BodyPartTerrain>());
        break;
    case Body_Part_Light :
        emitPair("data", v->get<BodyPartLight>());
        break;
    case Body_Part_Force :
        emitPair("data", v->get<BodyPartForce>());
        break;
    case Body_Part_Area :
        emitPair("data", v->get<BodyPartArea>());
        break;
    case Body_Part_Rope :
        emitPair("data", v->get<BodyPartRope>());
        break;
    }

    emitPair("constraintType", v->_constraintType);
    switch (v->_constraintType)
    {
    case Body_Constraint_Fixed :
        emitPair("constraint", v->getConstraint<BodyConstraintFixed>());
        break;
    case Body_Constraint_Ball :
        emitPair("constraint", v->getConstraint<BodyConstraintBall>());
        break;
    case Body_Constraint_Cone :
        emitPair("constraint", v->getConstraint<BodyConstraintCone>());
        break;
    case Body_Constraint_Hinge :
        emitPair("constraint", v->getConstraint<BodyConstraintHinge>());
        break;
    }
    emitPair("constraintParentSubID", v->_constraintParentSubID);

    emitPair("parts", v->_parts);

    endMapping();
}
