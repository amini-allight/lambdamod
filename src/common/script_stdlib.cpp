/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_stdlib.hpp"
#include "script_tools.hpp"
#include "script_builtin_tools.hpp"
#include "scope_closure.hpp"
#include "tools.hpp"
#include "game_state.hpp"

static random_device device;
static mt19937 engine(device());

struct RandomWrapper
{
    RandomWrapper(const function<u64()>& func)
        : func(func)
    {

    }

    u64 operator()()
    {
        return func();
    }

    static constexpr u64 min()
    {
        return 0;
    }

    static constexpr u64 max()
    {
        return numeric_limits<u64>::max();
    }

    typedef u64 result_type;

    function<u64()> func;
};

static Value builtin_inc(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return toValue(value.i + 1);
    }
    else
    {
        return toValue(value.f + 1);
    }
}

static Value builtin_dec(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return toValue(value.i - 1);
    }
    else
    {
        return toValue(value.f - 1);
    }
}

static Value builtin_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);
    CHECK_ARG_NUMBER(0);

    Value result = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_NUMBER(i);

        Value item = args[i];

        if (item.type == Value_Integer)
        {
            result.type == Value_Integer
                ? result.i += item.i
                : result.f += item.i;
        }
        else
        {
            result.type == Value_Integer
                ? result = toValue(result.i + item.f)
                : result.f += item.f;
        }
    }

    return result;
}

static Value builtin_sub(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);
    CHECK_ARG_NUMBER(0);

    Value result;
    result.type = Value_Integer;
    result.i = 0;

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_NUMBER(i);

        Value item = args[i];

        if (item.type == Value_Integer)
        {
            result.type == Value_Integer
                ? result.i -= item.i
                : result.f -= item.i;
        }
        else
        {
            result.type == Value_Integer
                ? result = toValue(result.i - item.f)
                : result.f -= item.f;
        }
    }

    return result;
}

static Value builtin_mul(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);
    CHECK_ARG_NUMBER(0);

    Value result;
    result.type = Value_Integer;
    result.i = 1;

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_NUMBER(i);

        Value item = args[i];

        if (item.type == Value_Integer)
        {
            result.type == Value_Integer
                ? result.i *= item.i
                : result.f *= item.i;
        }
        else
        {
            result.type == Value_Integer
                ? result = toValue(result.i * item.f)
                : result.f *= item.f;
        }
    }

    return result;
}

static Value builtin_div(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);
    CHECK_ARG_NUMBER(0);

    if (args.size() == 1)
    {
        if (roughly(args[0].number<f64>(), 0.0))
        {
            throw toValue("Division by zero is not allowed.");
        }

        return toValue(1 / args[0].number<f64>());
    }
    else
    {
        Value result = args[0];

        for (size_t i = 1; i < args.size(); i++)
        {
            CHECK_ARG_NUMBER(i);

            Value item = args[i];

            if (item.type == Value_Integer)
            {
                if (item.i == 0)
                {
                    throw toValue("Division by zero is not allowed.");
                }

                result.type == Value_Integer
                    ? result.i /= item.i
                    : result.f /= item.i;
            }
            else
            {
                if (roughly(item.f, 0.0))
                {
                    throw toValue("Division by zero is not allowed.");
                }

                result.type == Value_Integer
                    ? result = toValue(result.i / item.f)
                    : result.f /= item.f;
            }
        }

        return result;
    }
}

static Value builtin_mod(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_NUMBER(0);

    Value result = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_NUMBER(i);

        const Value& item = args[i];

        if (roughly(item.number<f64>(), 0.0))
        {
            throw toValue("Modulo by zero is not allowed.");
        }

        if (result.type == Value_Float || item.type == Value_Float)
        {
            result = toValue(abs(fmod(result.number<f64>(), item.number<f64>())));
        }
        else
        {
            result = toValue(abs(result.i % item.i));
        }
    }

    return result;
}

static Value builtin_not(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value value = args[0];

    return toValue((bool)value);
}

static Value builtin_and(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    bool result = true;

    for (size_t i = 0; i < args.size(); i++)
    {
        Value item = args[i];

        result = result && item;

        if (!result)
        {
            break;
        }
    }

    return toValue(result);
}

static Value builtin_or(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    bool result = false;

    for (size_t i = 0; i < args.size(); i++)
    {
        Value item = args[i];

        result = result || item;

        if (result)
        {
            break;
        }
    }

    return toValue(result);
}

static Value builtin_xor(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    bool result = false;

    for (size_t i = 0; i < args.size(); i++)
    {
        Value value = args[i];

        result = result != (bool)value;
    }

    return toValue(result);
}

static Value builtin_bit_not(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value value = args[0];

    return toValue(~value.i);
}

static Value builtin_bit_and(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_INTEGER(0);

    Value result = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_INTEGER(i);

        Value value = args[i];

        result.i &= value.i;
    }

    return result;
}

static Value builtin_bit_or(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_INTEGER(0);

    Value result = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_INTEGER(i);

        Value value = args[i];

        result.i |= value.i;
    }

    return result;
}

static Value builtin_bit_xor(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_INTEGER(0);

    Value result = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_INTEGER(i);

        Value value = args[i];

        result.i ^= value.i;
    }

    return result;
}

static Value builtin_bit_shift_right(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value a = args[0];

    Value b = args[1];

    return toValue(a.i >> b.i);
}

static Value builtin_bit_shift_left(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value a = args[0];

    Value b = args[1];

    return toValue(a.i << b.i);
}

static Value builtin_equal(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    return toValue(a == b);
}

static Value builtin_not_equal(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    return toValue(a != b);
}

static Value builtin_greater(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    return toValue(a > b);
}

static Value builtin_less(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    return toValue(a < b);
}

static Value builtin_greater_equal(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    return toValue(a >= b);
}

static Value builtin_less_equal(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    return toValue(a <= b);
}

static Value builtin_spaceship(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value a = args[0];
    Value b = args[1];

    strong_ordering ordering = a <=> b;

    if (ordering == strong_ordering::less)
    {
        return toValue(-1);
    }
    else if (ordering == strong_ordering::equal)
    {
        return toValue(0);
    }
    else
    {
        return toValue(+1);
    }
}

static Value builtin_exponent(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_NUMBER(0);

    if (args.size() == 2)
    {
        CHECK_ARG_NUMBER(1);
    }

    Value value = args[0];
    Value exponent;

    if (args.size() == 1)
    {
        exponent.type = Value_Integer;
        exponent.i = 2;
    }
    else
    {
        exponent = args[1];
    }

    Value result;
    result.type = value.type == Value_Float || exponent.type == Value_Float
        ? Value_Float
        : Value_Integer;

    if (result.type == Value_Integer)
    {
        result.i = pow(
            value.type == Value_Integer ? value.i : value.f,
            exponent.type == Value_Integer ? exponent.i : exponent.f
        );
    }
    else
    {
        result.f = pow(
            value.type == Value_Integer ? value.i : value.f,
            exponent.type == Value_Integer ? exponent.i : exponent.f
        );
    }

    return result;
}

static Value builtin_root(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_NUMBER(0);

    if (args.size() == 2)
    {
        CHECK_ARG_NUMBER(1);
    }

    Value value = args[0];
    Value root;

    if (args.size() == 1)
    {
        root.type = Value_Integer;
        root.i = 2;
    }
    else
    {
        root = args[1];
    }

    if ((root.type == Value_Integer && root.i == 0) || (root.type == Value_Float && roughly(root.f, 0.0)))
    {
        throw toValue("The zeroth root is not defined.");
    }

    Value result;
    result.type = Value_Float;

    result.f = pow(
        value.type == Value_Integer ? value.i : value.f,
        1.0 / (root.type == Value_Integer ? root.i : root.f)
    );

    return result;
}

static Value builtin_sq(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value a = args[0];

    if (a.type == Value_Integer)
    {
        return toValue(static_cast<i64>(pow<i64>(a.i, 2)));
    }
    else
    {
        return toValue(static_cast<f64>(sq(a.f)));
    }
}

static Value builtin_sqrt(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value a = args[0];

    if (a.type == Value_Integer)
    {
        return toValue(static_cast<i64>(sqrt(a.i)));
    }
    else
    {
        return toValue(sqrt(a.f));
    }
}

static Value builtin_let(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    ScopeClosure closure(context);

    if (args[0].type != Value_List)
    {
        throw toValue("Expected list of symbol-expression pairs as argument 1 to 'let', got '" + args[0].toString() + "'.");
    }

    for (const Value& child : args[0].l)
    {
        if (child.type != Value_List || child.l.size() != 2 || child.l[0].type != Value_Symbol)
        {
            throw toValue("Expected list of symbol-expression pairs as argument 1 to 'let', got '" + args[0].toString() + "'.");
        }
    }

    for (const Value& child : args[0].l)
    {
        string name = child.l[0].s;
        Value value = context->run(child.l[1]);

        context->define(name, value);
    }

    Value result;

    for (const Value& arg : vector<Value>(args.begin() + 1, args.end()))
    {
        result = context->run(arg);
    }

    return result;
}

static Value builtin_begin(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);

    ScopeClosure closure(context);

    Value result;

    for (const Value& item : args)
    {
        result = context->run(item);
    }

    return result;
}

static Value builtin_if(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(3);

    if (context->run(args[0]))
    {
        return context->run(args[1]);
    }
    else
    {
        return context->run(args[2]);
    }
}

static Value builtin_when(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    if (context->run(args[0]))
    {
        Value result;

        for (const Value& arg : vector<Value>(args.begin() + 1, args.end()))
        {
            result = context->run(arg);
        }

        return result;
    }
    else
    {
        return Value();
    }
}

static Value builtin_switch(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value index = context->run(args[0]);

    for (size_t i = 1; i < args.size() - 1; i++)
    {
        if (args[i].type != Value_List || args[i].l.size() != 2)
        {
            throw toValue("Expected key-expression pairs as variadic arguments to 'switch', got '" + args[i].toString() + "'.");
        }

        if (context->run(args[i].l[0]))
        {
            Value result;

            for (const Value& arg : vector<Value>(args[i].l.begin() + 1, args[i].l.end()))
            {
                result = context->run(arg);
            }

            return result;
        }
    }

    return context->run(args[args.size() - 1]);
}

static Value builtin_condition(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    for (size_t i = 0; i < args.size() - 1; i++)
    {
        if (args[i].type != Value_List || args[i].l.size() != 2)
        {
            throw toValue("Expected condition-expression pairs as variadic arguments to 'condition', got '" + args[i].toString() + "'.");
        }

        if (context->run(args[i].l[0]))
        {
            Value result;

            for (const Value& arg : vector<Value>(args[i].l.begin() + 1, args[i].l.end()))
            {
                result = context->run(arg);
            }

            return result;
        }
    }

    return context->run(args[args.size() - 1]);
}

static Value builtin_for(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(4, false);

    ScopeClosure closure(context);

    if (args[0].type != Value_List)
    {
        throw toValue("Expected list of symbol-expression pairs as argument 1 to 'for', got '" + args[0].toString() + "'.");
    }

    if (args[2].type != Value_List)
    {
        throw toValue("Expected list of symbol-expression pairs as argument 3 to 'for', got '" + args[2].toString() + "'.");
    }

    for (const Value& child : args[0].l)
    {
        if (child.type != Value_List || child.l.size() != 2 || child.l[0].type != Value_Symbol)
        {
            throw toValue("Expected list of symbol-expression pairs as argument 1 to 'for', got '" + args[0].toString() + "'.");
        }
    }

    for (const Value& child : args[0].l)
    {
        string name = child.l[0].s;
        Value value = context->run(child.l[1]);

        context->define(name, value);
    }

    Value result;

    auto startTime = currentTime();
    while (true)
    {
        if (!context->run(args[1]))
        {
            break;
        }

        for (const Value& arg : vector<Value>(args.begin() + 3, args.end()))
        {
            result = context->run(arg);
        }

        for (const Value& child : args[2].l)
        {
            if (child.type != Value_List || child.l.size() != 2 || child.l[0].type != Value_Symbol)
            {
                throw toValue("Expected list of symbol-expression pairs as argument 3 to 'for', got '" + args[2].toString() + "'.");
            }
        }

        for (const Value& child : args[2].l)
        {
            string name = child.l[0].s;
            Value value = context->run(child.l[1]);

            context->define(name, value);
        }

        if (currentTime() - startTime > maxLoopDuration())
        {
            throw toValue("Maximum time spent in '" + functionName + "' loop exceeded.");
        }
    }

    return result;
}

static Value builtin_for_each(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2);

    if (args[0].type != Value_List || args[0].l.size() != 2 || args[0].l[0].type != Value_Symbol)
    {
        throw toValue("Expected symbol-list pair as argument 1 to 'for-each', got '" + args[0].toString() + "'.");
    }

    string name = args[0].l[0].s;
    Value items = context->run(args[0].l[1]);

    if (items.type != Value_List)
    {
        throw toValue("Expected list as argument 1 to 'for-each', got '" + items.toString() + "'.");
    }

    Value result;

    for (const Value& item : items.l)
    {
        ScopeClosure closure(context);

        context->define(name, item);

        for (const Value& arg : vector<Value>(args.begin() + 1, args.end()))
        {
            result = context->run(arg);
        }
    }

    return result;
}

static Value builtin_while(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value result;

    auto startTime = currentTime();
    while (context->run(args[0]))
    {
        for (const Value& arg : vector<Value>(args.begin() + 1, args.end()))
        {
            result = context->run(arg);
        }

        if (currentTime() - startTime > maxLoopDuration())
        {
            throw toValue("Maximum time spent in '" + functionName + "' loop exceeded.");
        }
    }

    return result;
}

static Value builtin_do(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value result;

    auto startTime = currentTime();
    do
    {
        for (const Value& arg : vector<Value>(args.begin() + 1, args.end()))
        {
            result = context->run(arg);
        }

        if (currentTime() - startTime > maxLoopDuration())
        {
            throw toValue("Maximum time spent in '" + functionName + "' loop exceeded.");
        }
    } while (context->run(args[0]));

    return result;
}

static Value builtin_zero(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value arg = args[0];

    if (arg.type == Value_Integer)
    {
        return toValue(arg.i == 0);
    }
    else
    {
        return toValue(roughly<f64>(arg.f, 0));
    }
}

static Value builtin_positive(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value arg = args[0];

    if (arg.type == Value_Integer)
    {
        return toValue(arg.i > 0);
    }
    else
    {
        return toValue(arg.f > 0);
    }
}

static Value builtin_negative(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value arg = args[0];

    if (arg.type == Value_Integer)
    {
        return toValue(arg.i < 0);
    }
    else
    {
        return toValue(arg.f < 0);
    }
}

static Value builtin_sin(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(sin(value.number<f64>()));
}

static Value builtin_cos(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(cos(value.number<f64>()));
}

static Value builtin_tan(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(tan(value.number<f64>()));
}

static Value builtin_asin(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(asin(value.number<f64>()));
}

static Value builtin_acos(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(acos(value.number<f64>()));
}

static Value builtin_atan(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(atan(value.number<f64>()));
}

static Value builtin_sinh(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(sinh(value.number<f64>()));
}

static Value builtin_cosh(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(cosh(value.number<f64>()));
}

static Value builtin_tanh(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(tanh(value.number<f64>()));
}

static Value builtin_asinh(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(asinh(value.number<f64>()));
}

static Value builtin_acosh(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(acosh(value.number<f64>()));
}

static Value builtin_atanh(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    return toValue(atanh(value.number<f64>()));
}

static Value builtin_log(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);
    CHECK_ARG_NUMBER(1);

    Value value = args[0];
    Value base = args[1];

    if (roughly(base.number<f64>(), 1.0))
    {
        throw runtime_error("Log to the base 1 is not defined.");
    }

    return toValue(log(value.number<f64>()) / log(base.number<f64>()));
}

static Value builtin_abs(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        value.i = abs(value.i);
    }
    else
    {
        value.f = abs(value.f);
    }

    return value;
}

static Value builtin_ceil(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return value;
    }
    else
    {
        return toValue(static_cast<i64>(ceil(value.f)));
    }
}

static Value builtin_floor(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return value;
    }
    else
    {
        return toValue(static_cast<i64>(floor(value.f)));
    }
}

static Value builtin_trunc(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return value;
    }
    else
    {
        return toValue(static_cast<i64>(trunc(value.f)));
    }
}

static Value builtin_fractional(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return toValue(static_cast<i64>(0));
    }
    else
    {
        return toValue(value.f - trunc(value.f));
    }
}

static Value builtin_round(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return value;
    }
    else
    {
        return toValue(static_cast<i64>(round(value.f)));
    }
}

static Value builtin_sign(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value value = args[0];

    if (value.type == Value_Integer)
    {
        return toValue(static_cast<i64>(value.i >= 0 ? +1 : -1));
    }
    else
    {
        return toValue(static_cast<i64>(value.f >= 0 ? +1 : -1));
    }
}

static Value builtin_min(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value result;

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_NUMBER(i);

        Value item = args[i];

        if (result.type == Value_Void)
        {
            result = item;
            continue;
        }

        if (item.type == Value_Integer && (
            (result.type == Value_Integer && result.i > item.i) ||
            (result.type == Value_Float && result.f > item.i)
        ))
        {
            result = item;
        }
        else if (item.type == Value_Float && (
            (result.type == Value_Integer && result.i > item.f) ||
            (result.type == Value_Float && result.f > item.f)
        ))
        {
            result = item;
        }
    }

    return result;
}

static Value builtin_max(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value result;

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_NUMBER(i);

        Value item = args[i];

        if (result.type == Value_Void)
        {
            result = item;
            continue;
        }

        if (item.type == Value_Integer && (
            (result.type == Value_Integer && result.i < item.i) ||
            (result.type == Value_Float && result.f < item.i)
        ))
        {
            result = item;
        }
        else if (item.type == Value_Float && (
            (result.type == Value_Integer && result.i < item.f) ||
            (result.type == Value_Float && result.f < item.f)))
        {
            result = item;
        }
    }

    return result;
}

static Value builtin_clamp(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);
    CHECK_ARG_NUMBER(1);
    CHECK_ARG_NUMBER(2);

    Value value = args[0];
    Value min = args[1];
    Value max = args[2];

    if (value.type == Value_Float)
    {
        if (min.type == Value_Integer && value.f < static_cast<f64>(min.i))
        {
            value.f = min.i;
        }
        else if (min.type == Value_Float && value.f < min.f)
        {
            value.f = min.f;
        }
        else if (max.type == Value_Integer && value.f > static_cast<f64>(max.i))
        {
            value.f = max.i;
        }
        else if (max.type == Value_Float && value.f > max.f)
        {
            value.f = max.f;
        }
    }
    else
    {
        if (min.type == Value_Integer && value.i < min.i)
        {
            value.i = min.i;
        }
        else if (min.type == Value_Float && value.i < static_cast<i64>(min.f))
        {
            value.i = min.f;
        }
        else if (max.type == Value_Integer && value.i > max.i)
        {
            value.i = max.i;
        }
        else if (max.type == Value_Float && value.i > static_cast<i64>(max.f))
        {
            value.i = max.f;
        }
    }

    return value;
}

static Value builtin_void(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_Void);
}

static Value builtin_symbol(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_Symbol);
}

static Value builtin_int(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_Integer);
}

static Value builtin_float(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_Float);
}

static Value builtin_string(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_String);
}

static Value builtin_list(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_List);
}

static Value builtin_lambda(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_Lambda);
}

static Value builtin_handle_is(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value arg = args[0];

    return toValue(arg.type == Value_Handle);
}

static Value builtin_string_integer(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value value = args[0];

    return toValue(static_cast<i64>(stoll(value.s)));
}

static Value builtin_string_float(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value value = args[0];

    return toValue(stod(value.s));
}

static Value builtin_string_symbol(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value value = args[0];

    if (!isValidSymbol(value.s))
    {
        throw toValue("Expected a valid string for a symbol as argument 1 to 'string->symbol', got '" + value.s + "'.");
    }

    return symbol(value.s);
}

static Value builtin_integer_float(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value value = args[0];

    return toValue(static_cast<f64>(value.i));
}

static Value builtin_integer_string(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value value = args[0];

    return toValue(to_string(value.i));
}

static Value builtin_float_integer(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_FLOAT(0);

    Value value = args[0];

    return toValue(static_cast<i64>(value.f));
}

static Value builtin_float_string(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_FLOAT(0);

    Value value = args[0];

    return toValue(to_string(value.f));
}

static Value builtin_symbol_string(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value value = args[0];

    return toValue(value.s);
}

static Value builtin_handle(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    return toValue(EntityID(id.i));
}

static Value builtin_is_nullptr(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value target = args[0];

    return toValue(target.id == EntityID());
}

static Value builtin_has(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->has(name.s));
}

static Value builtin_exec(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];

    vector<Value> subArgs = { args.begin() + 2, args.end() };

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    return entity->exec(context, name.s, subArgs);
}

static Value builtin_read(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (!entity->has(name.s))
    {
        throw toValue("Could not 'read' field '" + name.s + "' from entity: not found.");
    }

    return entity->read(name.s);
}

static Value builtin_write(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_NAME_VALID(1);

    Value target = args[0];
    Value name = args[1];
    Value value = args[2];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->write(name.s, value);

    return Value();
}

static Value builtin_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->remove(name.s);

    return Value();
}

static Value builtin_apply(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LAMBDA(0);
    CHECK_ARG_LIST(1);

    Value lambda = args[0];
    Value arguments = args[1];

    Value value;
    value.type = Value_List;
    value.l = { lambda };
    value.l.insert(value.l.end(), arguments.l.begin(), arguments.l.end());

    return context->run(value);
}

static Value builtin_compose(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value current = composeList(args[0], "x");

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_LAMBDA(i);

        Value lambda = args[i];

        current = composeList(lambda, current);
    }

    Value result;
    result.type = Value_Lambda;
    result.lambda.type = Lambda_Normal;
    result.lambda.argNames = { "x" };
    result.lambda.body = { current };

    return context->run(result);
}

static Value builtin_first(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];

    if (input.l.empty())
    {
        throw toValue("Expected non-empty list as argument 1 to 'first', got an empty list.");
    }

    return input.l.front();
}

static Value builtin_last(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];

    if (input.l.empty())
    {
        throw toValue("Expected non-empty list as argument 1 to 'last', got an empty list.");
    }

    return input.l.back();
}

static Value builtin_empty(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];

    return toValue(input.l.empty());
}

static Value builtin_contains(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];
    Value value = args[1];

    return toValue(find(input.l.begin(), input.l.end(), value) != input.l.end());
}

static Value builtin_index(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];
    Value value = args[1];

    size_t i = 0;
    for (const Value& item : input.l)
    {
        if (item == value)
        {
            return toValue(i);
        }

        i++;
    }

    return toValue(-1);
}

static Value builtin_replace(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];
    Value oldValue = args[1];
    Value newValue = args[2];

    for (Value& item : input.l)
    {
        if (item == oldValue)
        {
            item = newValue;
        }
    }

    return input;
}

static Value builtin_slice(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_INTEGER(2);

    Value input = args[0];
    Value start = args[1];
    Value count = args[2];

    if (
        start.i < 0 ||
        count.i < 0 ||
        static_cast<size_t>(start.i) >= input.l.size() ||
        static_cast<size_t>(start.i) + static_cast<size_t>(count.i) > input.l.size()
    )
    {
        throw toValue("Could not get slice " + to_string(start.i) + ":" + to_string(count.i) + ": index out of range.");
    }

    Value result;
    result.type = Value_List;
    result.l = { input.l.begin() + start.i, input.l.begin() + (start.i + count.i) };

    return result;
}

static Value builtin_zip(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    optional<size_t> size;

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_LIST(i);

        const Value& arg = args[i];

        if (!size)
        {
            size = arg.l.size();
        }
        else
        {
            if (*size != arg.l.size())
            {
                throw toValue("Expected all arguments to 'zip' to be of the same length.");
            }
        }
    }

    Value base;
    base.type = Value_List;

    Value result;
    result.type = Value_List;
    result.l = vector<Value>(*size, base);

    for (size_t i = 0; i < args.size(); i++)
    {
        const Value& arg = args[i];

        for (size_t j = 0; j < arg.l.size(); j++)
        {
            result.l[j].l.push_back(arg.l[j]);
        }
    }

    return result;
}

static Value builtin_insert(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_LIST(2);

    Value input = args[0];
    Value index = args[1];
    Value element = args[2];

    if (index.i < 0 || static_cast<size_t>(index.i) > input.l.size())
    {
        throw toValue("Could not 'insert' elements into list at position " + to_string(index.i) + ": index out of range.");
    }

    input.l.insert(
        input.l.begin() + index.i,
        element.l.begin(),
        element.l.end()
    );

    return input;
}

static Value builtin_erase(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_INTEGER(2);

    Value input = args[0];
    Value index = args[1];
    Value count = args[2];

    if (
        index.i < 0 ||
        count.i < 0 ||
        static_cast<size_t>(index.i) >= input.l.size() ||
        static_cast<size_t>(index.i) + static_cast<size_t>(count.i) > input.l.size()
    )
    {
        throw toValue("Could not 'erase' elements from list at position " + to_string(index.i) + " with length " + to_string(count.i) + ": index out of range.");
    }

    input.l.erase(
        input.l.begin() + index.i,
        input.l.begin() + (index.i + count.i)
    );

    return input;
}

static Value builtin_push_first(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];
    Value value = args[1];

    input.l.insert(input.l.begin(), value);

    return input;
}

static Value builtin_push_last(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];
    Value value = args[1];

    input.l.push_back(value);

    return input;
}

static Value builtin_pop_first(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];

    input.l.erase(input.l.begin());

    return input;
}

static Value builtin_pop_last(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];

    input.l.erase(input.l.begin() + input.l.size());

    return input;
}

static Value builtin_any_of(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    for (const Value& item : input.l)
    {
        if (context->run(lambda.lambda, { item }))
        {
            return toValue(true);
        }
    }

    return toValue(false);
}

static Value builtin_all_of(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    for (const Value& item : input.l)
    {
        if (!context->run(lambda.lambda, { item }))
        {
            return toValue(false);
        }
    }

    return toValue(true);
}

static Value builtin_none_of(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    for (const Value& item : input.l)
    {
        if (context->run(lambda.lambda, { item }))
        {
            return toValue(false);
        }
    }

    return toValue(true);
}

static Value builtin_sum_of(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    Value result;
    result.type = Value_Integer;
    result.i = 0;

    for (const Value& item : input.l)
    {
        Value component = context->run(lambda.lambda, { item });

        CHECK_RET_NUMBER(1, component);

        if (component.type == Value_Integer)
        {
            result.type == Value_Integer
                ? result.i += component.i
                : result.f += component.i;
        }
        else
        {
            result.type == Value_Integer
                ? result = toValue(result.i + component.f)
                : result.f += component.f;
        }
    }

    return result;
}

static Value builtin_min_of(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    Value result;
    Value lowest;
    lowest.type = Value_Integer;
    lowest.i = numeric_limits<i64>::max();

    for (const Value& item : input.l)
    {
        Value component = context->run(lambda.lambda, { item });

        CHECK_RET_NUMBER(1, component);

        bool replace = false;

        if (component.type == Value_Integer)
        {
            replace = lowest.type == Value_Integer
                ? component.i < lowest.i
                : static_cast<f64>(component.i) < lowest.f;
        }
        else
        {
            replace = lowest.type == Value_Integer
                ? component.f < static_cast<f64>(lowest.i)
                : component.f < lowest.f;
        }

        if (replace)
        {
            result = item;
            lowest = component;
        }
    }

    return result;
}

static Value builtin_max_of(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    Value result;
    Value highest;
    highest.type = Value_Integer;
    highest.i = numeric_limits<i64>::min();

    for (const Value& item : input.l)
    {
        Value component = context->run(lambda.lambda, { item });

        CHECK_RET_NUMBER(2, component);

        bool replace = false;

        if (component.type == Value_Integer)
        {
            replace = highest.type == Value_Integer
                ? component.i > highest.i
                : static_cast<f64>(component.i) > highest.f;
        }
        else
        {
            replace = highest.type == Value_Integer
                ? component.f > static_cast<f64>(highest.i)
                : component.f > highest.f;
        }

        if (replace)
        {
            result = item;
            highest = component;
        }
    }

    return result;
}

static Value builtin_select(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    Value result;
    result.type = Value_List;

    for (const Value& item : input.l)
    {
        if (context->run(lambda.lambda, { item }))
        {
            result.l.push_back(item);
        }
    }

    return result;
}

static Value builtin_sort(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    sort(
        input.l.begin(),
        input.l.end(),
        [&](const Value& a, const Value& b) -> bool
        {
            Value forwardsResult = context->run(lambda.lambda, { a, b });
            Value backwardsResult = context->run(lambda.lambda, { b, a });

            CHECK_RET_INTEGER(1, forwardsResult);
            CHECK_RET_INTEGER(2, backwardsResult);

            if (forwardsResult.i && backwardsResult.i)
            {
                throw toValue("Invalid sorting comparison function: (compare " + a.toString() + " " + b.toString() + ") and (compare " + b.toString() + " " + a.toString() + ") both return true.");
            }

            return forwardsResult.i;
        }
    );

    return input;
}

static Value builtin_shuffle(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    RandomWrapper randomSource(
        [&]() -> u64
        {
            Value result = context->run(lambda.lambda, {});

            CHECK_RET_INTEGER(1, result);

            return static_cast<u64>(result.i);
        }
    );

    shuffle(
        input.l.begin(),
        input.l.end(),
        randomSource
    );

    return input;
}

static Value builtin_choose(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    uniform_int_distribution<size_t> distribution(0, input.l.size() - 1);

    RandomWrapper randomSource(
        [&]() -> u64
        {
            Value result = context->run(lambda.lambda, {});

            CHECK_RET_INTEGER(1, result);

            return static_cast<u64>(result.i);
        }
    );

    return input.l[distribution(randomSource)];
}

static Value builtin_reverse(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value input = args[0];

    reverse(
        input.l.begin(),
        input.l.end()
    );

    return input;
}

static Value builtin_generate(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_LAMBDA(1);

    Value count = args[0];
    Value lambda = args[1];

    Value result;
    result.type = Value_List;
    result.l.reserve(count.i);

    for (i64 i = 0; i < count.i; i++)
    {
        result.l.push_back(context->run(lambda.lambda, { toValue(i) }));
    }

    return result;
}

static Value builtin_map(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    Value result;
    result.type = Value_List;
    result.l.reserve(input.l.size());

    for (const Value& item : input.l)
    {
        result.l.push_back(context->run(lambda.lambda, { item }));
    }

    return result;
}

static Value builtin_reduce(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(2);

    Value input = args[0];
    Value initial = args[1];
    Value lambda = args[2];

    Value result = initial;

    for (const Value& item : input.l)
    {
        result = context->run(lambda.lambda, { item, result });
    }

    return result;
}

static Value builtin_count(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);
    CHECK_ARG_LAMBDA(1);

    Value input = args[0];
    Value lambda = args[1];

    Value result;
    result.type = Value_Integer;
    result.i = 0;

    for (const Value& item : input.l)
    {
        if (context->run(lambda.lambda, { item }))
        {
            result.i++;
        }
    }

    return result;
}

static Value builtin_union(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    vector<Value> lists;
    lists.reserve(args.size());

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_LIST(i);

        Value item = args[i];

        lists.push_back(item);
    }

    set<Value> _union;

    for (const Value& list : lists)
    {
        for (const Value& item : list.l)
        {
            _union.insert(item);
        }
    }

    Value result;
    result.type = Value_List;
    result.l = vector<Value>(_union.begin(), _union.end());

    return result;
}

static Value builtin_difference(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    vector<Value> lists;
    lists.reserve(args.size());

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_LIST(i);

        Value item = args[i];

        lists.push_back(item);
    }

    set<Value> difference;

    for (size_t i = 0; i < lists.size(); i++)
    {
        const Value& listA = lists[i];

        for (const Value& a : listA.l)
        {
            bool shared = false;

            for (size_t j = 0; j < lists.size(); j++)
            {
                if (j == i)
                {
                    continue;
                }

                const Value& listB = lists[j];

                for (const Value& b : listB.l)
                {
                    if (a == b)
                    {
                        shared = true;
                        break;
                    }
                }
            }

            if (!shared)
            {
                difference.insert(a);
            }
        }
    }

    Value result;
    result.type = Value_List;
    result.l = vector<Value>(difference.begin(), difference.end());

    return result;
}

static Value builtin_intersection(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    vector<Value> lists;
    lists.reserve(args.size());

    for (size_t i = 0; i < args.size(); i++)
    {
        CHECK_ARG_LIST(i);

        Value item = args[i];

        lists.push_back(item);
    }

    set<Value> intersection;

    for (size_t i = 0; i < lists.size(); i++)
    {
        const Value& listA = lists[i];

        for (const Value& a : listA.l)
        {
            bool shared = false;

            for (size_t j = 0; j < lists.size(); j++)
            {
                if (j == i)
                {
                    continue;
                }

                const Value& listB = lists[j];

                for (const Value& b : listB.l)
                {
                    if (a == b)
                    {
                        shared = true;
                        break;
                    }
                }
            }

            if (shared)
            {
                intersection.insert(a);
            }
        }
    }

    Value result;
    result.type = Value_List;
    result.l = vector<Value>(intersection.begin(), intersection.end());

    return result;
}

static Value builtin_range(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value start = args[0];
    Value end = args[1];

    Value result;
    result.type = Value_List;
    result.l.reserve(end - start);

    for (i64 i = start.i; i < end.i; i++)
    {
        result.l.push_back(toValue(i));
    }

    return result;
}

static Value builtin_inrange(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value start = args[0];
    Value end = args[1];

    Value result;
    result.type = Value_List;
    result.l.reserve((end - start) + 1);

    for (i64 i = start.i; i <= end.i; i++)
    {
        result.l.push_back(toValue(i));
    }

    return result;
}

static Value builtin_string_empty(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value text = args[0];

    return toValue(text.s.size());
}

static Value builtin_string_insert(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_STRING(2);

    Value text = args[0];
    Value index = args[1];
    Value element = args[2];

    if (index.i < 0 || static_cast<size_t>(index.i) > text.s.size())
    {
        throw toValue("Could not 'string-insert' at position " + to_string(index.i) + ": index out of range.");
    }

    text.s.insert(
        index.i,
        element.s
    );

    return text;
}

static Value builtin_string_erase(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_INTEGER(2);

    Value text = args[0];
    Value index = args[1];
    Value count = args[2];

    if (
        index.i < 0 ||
        count.i < 0 ||
        static_cast<size_t>(index.i) >= text.s.size() ||
        static_cast<size_t>(index.i) + static_cast<size_t>(count.i) > text.s.size()
    )
    {
        throw toValue("Could not 'string-erase' at position " + to_string(index.i) + " with length " + to_string(count.i) + ": index out of range.");
    }

    text.s.erase(
        text.s.begin() + index.i,
        text.s.begin() + (index.i + count.i)
    );

    return text;
}

static Value builtin_string_format(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_STRING(0);

    Value text = args[0];

    size_t j = 0;

    for (size_t i = 1; i < args.size(); i++)
    {
        Value item = args[i];

        bool found = false;
        bool next = false;

        for (; j < text.s.size(); j++)
        {
            char c = text.s[j];

            if (c == '%')
            {
                if (!next)
                {
                    next = true;
                }
                else
                {
                    text.s.erase(text.s.begin() + j);
                    j--;
                    next = false;
                }
            }
            else if (next)
            {
                switch (c)
                {
                case 'i' :
                    CHECK_ARG_INTEGER(i);

                    text.s.erase(text.s.begin() + j);
                    text.s.erase(text.s.begin() + (j - 1));

                    j--;

                    text.s.insert(j, to_string(item.i));
                    break;
                case 's' :
                    CHECK_ARG_STRING(i);

                    text.s.erase(text.s.begin() + j);
                    text.s.erase(text.s.begin() + (j - 1));

                    j--;

                    text.s.insert(j, item.s);
                    break;
                case 'f' :
                    CHECK_ARG_FLOAT(i);

                    text.s.erase(text.s.begin() + j);
                    text.s.erase(text.s.begin() + (j - 1));

                    j--;

                    text.s.insert(j, to_string(item.f));
                    break;
                default :
                    throw toValue("Encountered unknown format symbol '%" + (string() + c) + "' encountered in string.");
                }

                found = true;
                break;
            }
        }

        if (!found)
        {
            throw toValue("Expected " + to_string(i - 1) + " arguments to 'string-format', got " + to_string(args.size()) + ".");
        }
    }

    return text;
}

static Value builtin_string_starts_with(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_STRING(1);

    Value text = args[0];
    Value query = args[1];

    return toValue(text.s.starts_with(query.s));
}

static Value builtin_string_ends_with(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_STRING(1);

    Value text = args[0];
    Value query = args[1];

    return toValue(text.s.ends_with(query.s));
}

static Value builtin_string_contains(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_STRING(1);

    Value text = args[0];
    Value query = args[1];

    return toValue(text.s.find(query.s) != string::npos);
}

static Value builtin_string_index(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_STRING(1);

    Value text = args[0];
    Value query = args[1];

    size_t pos = text.s.find(query.s);

    if (pos == string::npos)
    {
        return toValue(-1);
    }

    return toValue(pos);
}

static Value builtin_string_replace(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);

    Value text = args[0];
    Value query = args[1];
    Value replace = args[2];

    size_t pos = text.s.find(query.s);

    if (pos == string::npos)
    {
        return text;
    }

    text.s = text.s.replace(pos, query.s.size(), replace.s);

    return text;
}

static Value builtin_string_slice(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_INTEGER(2);

    Value text = args[0];
    Value start = args[1];
    Value count = args[2];

    if (
        start.i < 0 ||
        count.i < 0 ||
        static_cast<size_t>(start.i) >= text.s.size() ||
        static_cast<size_t>(start.i) + static_cast<size_t>(count.i) > text.s.size()
    )
    {
        throw toValue("Could not get string slice " + to_string(start.i) + ":" + to_string(count.i) + ": index out of range.");
    }

    return toValue(text.s.substr(start.i, count.i));
}

static Value builtin_string_upcase(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value text = args[0];

    for (char& c : text.s)
    {
        c = toupper(c);
    }

    return text;
}

static Value builtin_string_downcase(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value text = args[0];

    for (char& c : text.s)
    {
        c = tolower(c);
    }

    return text;
}

static Value builtin_random_engine(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(engine());
}

static Value builtin_random_linear(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);
    CHECK_ARG_NUMBER(1);
    CHECK_ARG_LAMBDA(2);

    Value start = args[0];
    Value end = args[1];
    Value lambda = args[2];

    RandomWrapper randomSource(
        [&]() -> u64
        {
            Value result = context->run(lambda.lambda, {});

            CHECK_RET_INTEGER(2, result);

            return static_cast<u64>(result.i);
        }
    );

    if (start.type == Value_Integer && end.type == Value_Integer)
    {
        uniform_int_distribution<i64> distribution(start.i, end.i);

        return toValue(distribution(randomSource));
    }
    else
    {
        uniform_real_distribution<f64> distribution(
            start.type == Value_Float ? start.f : start.i,
            end.type == Value_Float ? end.f : end.i
        );

        return toValue(distribution(randomSource));
    }
}

static Value builtin_random_gauss(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);
    CHECK_ARG_NUMBER(1);
    CHECK_ARG_LAMBDA(2);

    Value mean = args[0];
    Value stddev = args[1];
    Value lambda = args[2];

    RandomWrapper randomSource(
        [&]() -> u64
        {
            Value result = context->run(lambda.lambda, {});

            CHECK_RET_INTEGER(2, result);

            return static_cast<u64>(result.i);
        }
    );

    normal_distribution<f64> distribution(
        mean.type == Value_Float ? mean.f : mean.i,
        stddev.type == Value_Float ? stddev.f : stddev.i
    );

    return toValue(distribution(randomSource));
}

static Value builtin_degrees_radians(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value input = args[0];

    return toValue((input.number<f64>() / 180) * pi);
}

static Value builtin_radians_degrees(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);

    Value input = args[0];

    return toValue((input.number<f64>() / pi) * 180);
}

static Value builtin_struct_define(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_NAME_VALID(0);

    Value name = args[0];

    vector<string> fieldNames;
    fieldNames.reserve(args.size() - 1);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_SYMBOL(i);
        CHECK_ARG_NAME_VALID(i);

        fieldNames.push_back(args[i].s);
    }

    Value body = composeList(symbol("list"));

    for (const string& fieldName : fieldNames)
    {
        body.l.push_back(symbol(fieldName));
    }

    Lambda lambda;
    lambda.type = Lambda_Normal;
    lambda.argNames = fieldNames;
    lambda.body = { body };

    Value constructor;
    constructor.type = Value_Lambda;
    constructor.lambda = lambda;

    if (!context->canWrite(context->currentUser(), name.s))
    {
        throw toValue("Permission to 'struct-define' global variable '" + name.s + "' denied.");
    }

    context->define(name.s, constructor);

    i64 i = 0;
    for (const string& fieldName : fieldNames)
    {
        string accessorName = name.s + "-" + fieldName;

        Value body = composeList(
            symbol("let"),
            composeList(composeList(symbol("value-count"), composeList(symbol("size"), composeList(symbol("list"), symbol("value..."))))),
            composeList(
                symbol("if"),
                composeList(symbol(">"), symbol("value-count"), 1),
                composeList(
                    symbol("throw"),
                    composeList(symbol("join"), "Expected 1 or 2 arguments to '" + accessorName + "', got ", composeList(symbol("integer->string"), composeList(symbol("++"), symbol("value-count"))), ".")
                ),
                composeList(
                    symbol("if"),
                    composeList(symbol("=="), symbol("value-count"), 1),
                    composeList(symbol("set"), symbol("struct"), i, symbol("value...")),
                    composeList(symbol("get"), symbol("struct"), i)
                )
            )
        );

        Lambda lambda;
        lambda.type = Lambda_Syntax;
        lambda.body = { body };
        lambda.argLayout = { symbol("struct"), symbol("value...") };

        Value accessor;
        accessor.type = Value_Lambda;
        accessor.lambda = lambda;

        if (!context->canWrite(context->currentUser(), accessorName))
        {
            throw toValue("Permission to 'struct-define' global variable '" + accessorName + "' denied.");
        }

        context->define(accessorName, accessor);

        i++;
    }

    return Value();
}

static Value builtin_struct_undefine(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1);
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_NAME_VALID(0);

    Value name = args[0];

    if (!context->has(name.s))
    {
        throw toValue("Constructor for struct '" + name.s + "' in '" + functionName + "' could not be found.");
    }

    vector<string> fieldNames =  context->get(name.s)->lambda.argNames;

    for (const string& fieldName : fieldNames)
    {
        string accessor = name.s + "-" + fieldName;

        if (context->has(accessor))
        {
            if (!context->canWrite(context->currentUser(), accessor))
            {
                throw toValue("Permission to 'struct-undefine' global variable '" + accessor + "' denied.");
            }

            context->undefine(accessor);
        }
    }

    if (!context->canWrite(context->currentUser(), name.s))
    {
        throw toValue("Permission to 'struct-undefine' global variable '" + name.s + "' denied.");
    }

    context->undefine(name.s);

    return Value();
}

static Value builtin_table_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(3);
    CHECK_ARG_TABLE(0);

    Value table = args[0];
    Value key = args[1];
    Value value = args[2];

    for (size_t i = 0; i < table.l.size(); i++)
    {
        Value& entry = table.l[i];

        if (key == entry.l[0])
        {
            entry.l[1] = value;
            return table;
        }
    }

    table.l.push_back(composeList(key, value));

    return table;
}

static Value builtin_table_insert(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_TABLE(0);

    Value table = args[0];
    Value key = args[1];
    Value value = args[2];

    for (size_t i = 0; i < table.l.size(); i++)
    {
        const Value& entry = table.l[i];

        if (key == entry.l[0])
        {
            return table;
        }
    }

    table.l.push_back(composeList(key, value));

    return table;
}

static Value builtin_table_assign(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_TABLE(0);

    Value table = args[0];
    Value key = args[1];
    Value value = args[2];

    for (size_t i = 0; i < table.l.size(); i++)
    {
        Value& entry = table.l[i];

        if (key == entry.l[0])
        {
            entry.l[1] = value;
            break;
        }
    }

    return table;
}

static Value builtin_table_erase(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_TABLE(0);

    Value table = args[0];
    Value key = args[1];

    for (size_t i = 0; i < table.l.size(); i++)
    {
        const Value& entry = table.l[i];

        if (key == entry.l[0])
        {
            table.l.erase(table.l.begin() + i);
            break;
        }
    }

    return table;
}

static Value builtin_table_has(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_TABLE(0);

    Value table = args[0];
    Value key = args[1];

    for (const Value& entry : table.l)
    {
        if (key == entry.l[0])
        {
            return toValue(true);
        }
    }

    return toValue(false);
}

static Value builtin_table_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_TABLE(0);

    Value table = args[0];
    Value key = args[1];

    for (const Value& entry : table.l)
    {
        if (key == entry.l[0])
        {
            return entry.l[1];
        }
    }

    throw toValue("Could not 'table-get' value by key '" + key.toString() + "': key not found.");
}

static Value builtin_set_insert(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value set = args[0];
    Value value = args[1];

    for (const Value& item : set.l)
    {
        if (item == value)
        {
            return set;
        }
    }

    set.l.push_back(value);

    return set;
}

static Value builtin_set_erase(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value set = args[0];
    Value value = args[1];

    for (size_t i = 0; i < set.l.size(); i++)
    {
        const Value& item = set.l[i];

        if (item == value)
        {
            set.l.erase(set.l.begin() + i);
            break;
        }
    }

    return set;
}

static Value builtin_set_has(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LIST(0);

    Value set = args[0];
    Value value = args[1];

    for (size_t i = 0; i < set.l.size(); i++)
    {
        const Value& item = set.l[i];

        if (item == value)
        {
            return toValue(true);
        }
    }

    return toValue(false);
}

static Value builtin_vec2(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1, 2 });

    if (args.size() == 0)
    {
        return toValue(vec2());
    }
    else if (args.size() == 1)
    {
        CHECK_ARG_NUMBER(0);

        Value v = args[0];

        return toValue(vec2(v.number<f64>()));
    }
    else
    {
        CHECK_ARG_NUMBER(0);
        CHECK_ARG_NUMBER(1);

        Value vx = args[0];
        Value vy = args[1];

        return toValue(vec2(vx.number<f64>(), vy.number<f64>()));
    }
}

static Value builtin_vec2_is(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(isVec2(args[0]));
}

static Value builtin_vec2_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC2(0);

    Value v = args[0];

    vec2 result = fromValue<vec2>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC2(i);

        Value iv = args[i];

        result += fromValue<vec2>(iv);
    }

    return toValue(result);
}

static Value builtin_vec2_sub(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC2(0);

    Value v = args[0];

    vec2 result = fromValue<vec2>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC2(i);

        Value iv = args[i];

        result -= fromValue<vec2>(iv);
    }

    return toValue(result);
}

static Value builtin_vec2_mul(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC2(0);

    Value v = args[0];

    vec2 result = fromValue<vec2>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC2(i);

        Value iv = args[i];

        result *= fromValue<vec2>(iv);
    }

    return toValue(result);
}

static Value builtin_vec2_div(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC2(0);

    Value v = args[0];

    vec2 result = fromValue<vec2>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC2(i);

        Value iv = args[i];

        result /= fromValue<vec2>(iv);
    }

    return toValue(result);
}

static Value builtin_vec2_dot(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC2(0);
    CHECK_ARG_VEC2(1);

    Value a = args[0];
    Value b = args[1];

    return toValue(fromValue<vec2>(a).dot(fromValue<vec2>(b)));
}

static Value builtin_vec2_flip(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC2(0);

    Value arg = args[0];

    vec2 v = fromValue<vec2>(arg);

    return toValue(vec2(v.y, v.x));
}

static Value builtin_vec2_distance(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC2(0);
    CHECK_ARG_VEC2(1);

    Value a = args[0];
    Value b = args[1];

    return toValue(fromValue<vec2>(a).distance(fromValue<vec2>(b)));
}

static Value builtin_vec2_length(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC2(0);

    Value v = args[0];

    return toValue(fromValue<vec2>(v).length());
}

static Value builtin_vec2_normalize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC2(0);

    Value v = args[0];

    return toValue(fromValue<vec2>(v).normalize());
}

static Value builtin_vec3(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1, 3 });

    if (args.size() == 0)
    {
        return toValue(vec3());
    }
    else if (args.size() == 1)
    {
        CHECK_ARG_NUMBER(0);

        Value v = args[0];

        return toValue(vec3(v.number<f64>()));
    }
    else
    {
        CHECK_ARG_NUMBER(0);
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_NUMBER(2);

        Value vx = args[0];
        Value vy = args[1];
        Value vz = args[2];

        return toValue(vec3(vx.number<f64>(), vy.number<f64>(), vz.number<f64>()));
    }
}

static Value builtin_vec3_is(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(isVec3(args[0]));
}

static Value builtin_vec3_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC3(0);

    Value v = args[0];

    vec3 result = fromValue<vec3>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC3(i);

        Value iv = args[i];

        result += fromValue<vec3>(iv);
    }

    return toValue(result);
}

static Value builtin_vec3_sub(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC3(0);

    Value v = args[0];

    vec3 result = fromValue<vec3>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC3(i);

        Value iv = args[i];

        result -= fromValue<vec3>(iv);
    }

    return toValue(result);
}

static Value builtin_vec3_mul(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC3(0);

    Value v = args[0];

    vec3 result = fromValue<vec3>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC3(i);

        Value iv = args[i];

        result *= fromValue<vec3>(iv);
    }

    return toValue(result);
}

static Value builtin_vec3_div(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_VEC3(0);

    Value v = args[0];

    vec3 result = fromValue<vec3>(v);

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_VEC3(i);

        Value iv = args[i];

        result /= fromValue<vec3>(iv);
    }

    return toValue(result);
}

static Value builtin_vec3_dot(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_VEC3(1);

    Value a = args[0];
    Value b = args[1];

    return toValue(fromValue<vec3>(a).dot(fromValue<vec3>(b)));
}

static Value builtin_vec3_cross(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_VEC3(1);

    Value a = args[0];
    Value b = args[1];

    return toValue(fromValue<vec3>(a).cross(fromValue<vec3>(b)));
}

static Value builtin_vec3_distance(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_VEC3(1);

    Value a = args[0];
    Value b = args[1];

    return toValue(fromValue<vec3>(a).distance(fromValue<vec3>(b)));
}

static Value builtin_vec3_length(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);

    Value v = args[0];

    return toValue(fromValue<vec3>(v).length());
}

static Value builtin_vec3_normalize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);

    Value v = args[0];

    return toValue(fromValue<vec3>(v).normalize());
}

static Value builtin_quaternion(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 4 });

    if (args.size() == 0)
    {
        return toValue(quaternion());
    }

    CHECK_ARG_NUMBER(0);
    CHECK_ARG_NUMBER(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_NUMBER(3);

    return toValue(quaternion(args[0].number<f64>(), args[1].number<f64>(), args[2].number<f64>(), args[3].number<f64>()));
}

static Value builtin_quaternion_is(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(isQuaternion(args[0]));
}

static Value builtin_quaternion_from_direction(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_VEC3(1);

    Value dir = args[0];
    Value roll = args[1];

    return toValue(quaternion(fromValue<vec3>(dir), fromValue<vec3>(roll)));
}

static Value builtin_quaternion_from_axis_angle(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_NUMBER(1);

    Value axis = args[0];
    Value angle = args[1];

    return toValue(quaternion(fromValue<vec3>(axis), angle.number<f64>()));
}

static Value builtin_quaternion_mul(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_QUATERNION(i);

        Value iq = args[i];

        q.l[0].f = q.l[0].f*iq.l[0].f - q.l[1].f*iq.l[1].f - q.l[2].f*iq.l[2].f - q.l[3].f*iq.l[3].f;
        q.l[1].f = q.l[0].f*iq.l[1].f + q.l[1].f*iq.l[0].f + q.l[2].f*iq.l[3].f - q.l[3].f*iq.l[2].f;
        q.l[2].f = q.l[0].f*iq.l[2].f - q.l[1].f*iq.l[3].f + q.l[2].f*iq.l[0].f + q.l[3].f*iq.l[1].f;
        q.l[3].f = q.l[0].f*iq.l[3].f + q.l[1].f*iq.l[2].f - q.l[2].f*iq.l[1].f + q.l[3].f*iq.l[0].f;
    }

    return q;
}

static Value builtin_quaternion_div(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    for (size_t i = 1; i < args.size(); i++)
    {
        CHECK_ARG_QUATERNION(i);

        Value iq = args[i];

        iq.l[1].f *= -1;
        iq.l[2].f *= -1;
        iq.l[3].f *= -1;

        q.l[0].f = q.l[0].f*iq.l[0].f - q.l[1].f*iq.l[1].f - q.l[2].f*iq.l[2].f - q.l[3].f*iq.l[3].f;
        q.l[1].f = q.l[0].f*iq.l[1].f + q.l[1].f*iq.l[0].f + q.l[2].f*iq.l[3].f - q.l[3].f*iq.l[2].f;
        q.l[2].f = q.l[0].f*iq.l[2].f - q.l[1].f*iq.l[3].f + q.l[2].f*iq.l[0].f + q.l[3].f*iq.l[1].f;
        q.l[3].f = q.l[0].f*iq.l[3].f + q.l[1].f*iq.l[2].f - q.l[2].f*iq.l[1].f + q.l[3].f*iq.l[0].f;
    }

    return q;
}

static Value builtin_quaternion_inverse(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    q.l[1].f *= -1;
    q.l[2].f *= -1;
    q.l[3].f *= -1;

    return q;
}

static Value builtin_quaternion_rotate(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);
    CHECK_ARG_VEC3(1);

    Value q = args[0];
    Value v = args[1];

    return toValue(fromValue<quaternion>(q).rotate(fromValue<vec3>(v)));
}

static Value builtin_quaternion_forward(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).forward());
}

static Value builtin_quaternion_back(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).back());
}

static Value builtin_quaternion_left(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).left());
}

static Value builtin_quaternion_right(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).right());
}

static Value builtin_quaternion_up(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).up());
}

static Value builtin_quaternion_down(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).down());
}

static Value builtin_quaternion_axis(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).axis());
}

static Value builtin_quaternion_angle(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).angle());
}

static Value builtin_quaternion_length(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).length());
}

static Value builtin_quaternion_normalize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_QUATERNION(0);

    Value q = args[0];

    return toValue(fromValue<quaternion>(q).normalize());
}

static Value builtin_screen_project(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_QUATERNION(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_NUMBER(4);
    CHECK_ARG_NUMBER(5);
    CHECK_ARG_VEC3(6);

    Value position = args[0];
    Value rotation = args[1];
    Value angle = args[2];
    Value aspect = args[3];
    Value near = args[4];
    Value far = args[5];
    Value point = args[6];

    mat4 projection = createFlatPerspectiveMatrix(
        angle.number<f64>(),
        aspect.number<f64>(),
        near.number<f64>(),
        far.number<f64>()
    );
    mat4 view = createViewMatrix(fromValue<vec3>(position), fromValue<quaternion>(rotation));

    vec3 world = fromValue<vec3>(point);

    vec4 screen = projection * view * vec4(world.x, world.y, world.z, 1);

    screen.x /= screen.w;
    screen.y /= screen.w;
    screen.z /= screen.w;

    return toValue(screen.toVec3());
}

static Value builtin_screen_unproject(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_QUATERNION(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_NUMBER(4);
    CHECK_ARG_NUMBER(5);
    CHECK_ARG_VEC3(6);

    Value position = args[0];
    Value rotation = args[1];
    Value angle = args[2];
    Value aspect = args[3];
    Value near = args[4];
    Value far = args[5];
    Value point = args[6];

    mat4 projection = createFlatPerspectiveMatrix(
        angle.number<f64>(),
        aspect.number<f64>(),
        near.number<f64>(),
        far.number<f64>()
    );
    mat4 view = createViewMatrix(fromValue<vec3>(position), fromValue<quaternion>(rotation));

    vec3 screen = fromValue<vec3>(point);

    vec4 world = (projection * view).inverse() * vec4(screen.x, screen.y, screen.z, 1);

    world.x /= world.w;
    world.y /= world.w;
    world.z /= world.w;

    return toValue(world.toVec3());
}

static Value builtin_timer_start(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_NUMBER(0);
    CHECK_ARG_LAMBDA(1);
    CHECK_ARG_FLOAT_GREATER(0, 0);

    Value vDelay = args[0];
    Value vCallback = args[1];

    f64 delay = vDelay.number<f64>();
    Lambda lambda = vCallback.lambda;

    return toValue(context->game->addTimer(
        delay,
        lambda,
        context->currentUser()
    ).value());
}

static Value builtin_timer_stop(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    auto it = context->game->timers().find(TimerID(id.i));

    if (it == context->game->timers().end())
    {
        throw toValue("Target timer in '" + functionName + "' not found.");
    }

    if (!context->currentUser()->admin() && context->currentUser()->id() != it->second.userID)
    {
        throw toValue("Permission to execute '" + functionName + "' denied.");
    }

    context->game->removeTimer(TimerID(id.i));

    return Value();
}

static Value builtin_timer_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value result;
    result.type = Value_List;
    result.l.reserve(context->game->timers().size());

    for (const auto& [ id, timer ] : context->game->timers())
    {
        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_repeater_start(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_LAMBDA(0);

    Value vCallback = args[0];

    Lambda lambda = vCallback.lambda;

    return toValue(context->game->addRepeater(
        lambda,
        context->currentUser()
    ).value());
}

static Value builtin_repeater_stop(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    auto it = context->game->repeaters().find(RepeaterID(id.i));

    if (it == context->game->repeaters().end())
    {
        throw toValue("Target repeater in '" + functionName + "' not found.");
    }

    if (!context->currentUser()->admin() && context->currentUser()->id() != it->second.userID)
    {
        throw toValue("Permission to execute '" + functionName + "' denied.");
    }

    context->game->removeRepeater(RepeaterID(id.i));

    return Value();
}

static Value builtin_repeater_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value result;
    result.type = Value_List;
    result.l.reserve(context->game->repeaters().size());

    for (const auto& [ id, repeater ] : context->game->repeaters())
    {
        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

const map<string, ScriptBuiltin> stdlib = {
    makeBuiltin("++", { "a" }, builtin_inc),
    makeBuiltin("--", { "a" }, builtin_dec),
    makeVariadicBuiltin("+", builtin_add),
    makeVariadicBuiltin("-", builtin_sub),
    makeVariadicBuiltin("*", builtin_mul),
    makeVariadicBuiltin("/", builtin_div),
    makeVariadicBuiltin("%", builtin_mod),
    makeBuiltin("!", { "a" }, builtin_not),
    makeVariadicBuiltin("&&", builtin_and),
    makeVariadicBuiltin("||", builtin_or),
    makeVariadicBuiltin("^^", builtin_xor),
    makeBuiltin("~", { "a" }, builtin_bit_not),
    makeVariadicBuiltin("&", builtin_bit_and),
    makeVariadicBuiltin("|", builtin_bit_or),
    makeVariadicBuiltin("^", builtin_bit_xor),
    makeBuiltin(">>", { "a", "b" }, builtin_bit_shift_right),
    makeBuiltin("<<", { "a", "b" }, builtin_bit_shift_left),
    makeBuiltin("==", { "a", "b" }, builtin_equal),
    makeBuiltin("!=", { "a", "b" }, builtin_not_equal),
    makeBuiltin(">", { "a", "b" }, builtin_greater),
    makeBuiltin("<", { "a", "b" }, builtin_less),
    makeBuiltin(">=", { "a", "b" }, builtin_greater_equal),
    makeBuiltin("<=", { "a", "b" }, builtin_less_equal),
    makeBuiltin("<=>", { "a", "b" }, builtin_spaceship),
    makeVariadicBuiltin("**", builtin_exponent),
    makeVariadicBuiltin("//", builtin_root),

    makeVariadicBuiltin("modulo", builtin_mod),
    makeBuiltin("not", { "a" }, builtin_not),
    makeVariadicBuiltin("and", builtin_and),
    makeVariadicBuiltin("or", builtin_or),
    makeVariadicBuiltin("xor", builtin_xor),
    makeBuiltin("equal?", { "a", "b" }, builtin_equal),
    makeBuiltin("not-equal?", { "a", "b" }, builtin_not_equal),
    makeBuiltin("expt", { "a", "exponent" }, builtin_exponent),
    makeBuiltin("root", { "a", "root" }, builtin_root),

    makeBuiltin("sq", { "a" }, builtin_sq),
    makeBuiltin("sqrt", { "a" }, builtin_sqrt),

    makeSyntaxBuiltin("let", builtin_let),
    makeSyntaxBuiltin("begin", builtin_begin),
    makeSyntaxBuiltin("if", builtin_if),
    makeSyntaxBuiltin("when", builtin_when),
    makeSyntaxBuiltin("switch", builtin_switch),
    makeSyntaxBuiltin("condition", builtin_condition),
    makeSyntaxBuiltin("for", builtin_for),
    makeSyntaxBuiltin("for-each", builtin_for_each),
    makeSyntaxBuiltin("while", builtin_while),
    makeSyntaxBuiltin("do", builtin_do),

    makeBuiltin("zero?", { "a" }, builtin_zero),
    makeBuiltin("positive?", { "a" }, builtin_positive),
    makeBuiltin("negative?", { "a" }, builtin_negative),
    makeBuiltin("sin", { "a" }, builtin_sin),
    makeBuiltin("cos", { "a" }, builtin_cos),
    makeBuiltin("tan", { "a" }, builtin_tan),
    makeBuiltin("asin", { "a" }, builtin_asin),
    makeBuiltin("acos", { "a" }, builtin_acos),
    makeBuiltin("atan", { "a" }, builtin_atan),
    makeBuiltin("sinh", { "a" }, builtin_sinh),
    makeBuiltin("cosh", { "a" }, builtin_cosh),
    makeBuiltin("tanh", { "a" }, builtin_tanh),
    makeBuiltin("asinh", { "a" }, builtin_asinh),
    makeBuiltin("acosh", { "a" }, builtin_acosh),
    makeBuiltin("atanh", { "a" }, builtin_atanh),
    makeBuiltin("log", { "a", "base" }, builtin_log),
    makeBuiltin("abs", { "a" }, builtin_abs),
    makeBuiltin("ceil", { "a" }, builtin_ceil),
    makeBuiltin("floor", { "a" }, builtin_floor),
    makeBuiltin("trunc", { "a" }, builtin_trunc),
    makeBuiltin("fractional", { "a" }, builtin_fractional),
    makeBuiltin("round", { "a" }, builtin_round),
    makeBuiltin("sign", { "a" }, builtin_sign),
    makeVariadicBuiltin("min", builtin_min),
    makeVariadicBuiltin("max", builtin_max),
    makeBuiltin("clamp", { "a", "min", "max" }, builtin_clamp),

    makeBuiltin("void?", { "a" }, builtin_void),
    makeBuiltin("symbol?", { "a" }, builtin_symbol),
    makeBuiltin("int?", { "a" }, builtin_int),
    makeBuiltin("float?", { "a" }, builtin_float),
    makeBuiltin("string?", { "a" }, builtin_string),
    makeBuiltin("list?", { "a" }, builtin_list),
    makeBuiltin("lambda?", { "a" }, builtin_lambda),
    makeBuiltin("handle?", { "a" }, builtin_handle_is),

    makeBuiltin("string->integer", { "a" }, builtin_string_integer),
    makeBuiltin("string->float", { "a" }, builtin_string_float),
    makeBuiltin("string->symbol", { "a" }, builtin_string_symbol),
    makeBuiltin("integer->float", { "a" }, builtin_integer_float),
    makeBuiltin("integer->string", { "a" }, builtin_integer_string),
    makeBuiltin("float->integer", { "a" }, builtin_float_integer),
    makeBuiltin("float->string", { "a" }, builtin_float_string),
    makeBuiltin("symbol->string", { "a" }, builtin_symbol_string),

    makeBuiltin("handle", { "id" }, builtin_handle),
    makeBuiltin("nullptr?", { "handle" }, builtin_is_nullptr),

    makeBuiltin("has?", { "handle", "name" }, builtin_has),
    makeVariadicBuiltin("exec", builtin_exec, Script_Builtin_Auth_User),
    makeBuiltin("read", { "handle", "name" }, builtin_read),
    makeBuiltin("write", { "handle", "name", "value" }, builtin_write, Script_Builtin_Auth_User),
    makeBuiltin("remove", { "handle", "name" }, builtin_remove),

    makeBuiltin("apply", { "function", "arguments" }, builtin_apply),
    makeVariadicBuiltin("compose", builtin_compose),

    makeBuiltin("first", { "values" }, builtin_first),
    makeBuiltin("last", { "values" }, builtin_last),
    makeBuiltin("empty?", { "values" }, builtin_empty),
    makeBuiltin("insert", { "values", "index", "new-values" }, builtin_insert),
    makeBuiltin("erase", { "values", "index", "count" }, builtin_erase),
    makeBuiltin("contains?", { "values", "value" }, builtin_contains),
    makeBuiltin("index", { "values", "value" }, builtin_index),
    makeBuiltin("replace", { "values", "old", "new" }, builtin_replace),
    makeBuiltin("slice", { "s", "start", "count" }, builtin_slice),
    makeVariadicBuiltin("zip", builtin_zip),
    makeBuiltin("push-first", { "values", "value" }, builtin_push_first),
    makeBuiltin("push-last", { "values", "value" }, builtin_push_last),
    makeBuiltin("pop-first", { "values" }, builtin_pop_first),
    makeBuiltin("pop-last", { "values" }, builtin_pop_last),
    makeBuiltin("any-of?", { "values", "lambda" }, builtin_any_of),
    makeBuiltin("all-of?", { "values", "lambda" }, builtin_all_of),
    makeBuiltin("none-of?", { "values", "lambda" }, builtin_none_of),
    makeBuiltin("sum-of", { "values", "lambda" }, builtin_sum_of),
    makeBuiltin("min-of", { "values", "lambda" }, builtin_min_of),
    makeBuiltin("max-of", { "values", "lambda" }, builtin_max_of),
    makeBuiltin("select", { "values", "lambda" }, builtin_select),
    makeBuiltin("sort", { "values", "lambda" }, builtin_sort),
    makeBuiltin("shuffle", { "values", "random-engine" }, builtin_shuffle),
    makeBuiltin("choose", { "values", "random-engine" }, builtin_choose),
    makeBuiltin("reverse", { "values" }, builtin_reverse),
    makeBuiltin("generate", { "count", "lambda" }, builtin_generate),
    makeBuiltin("map", { "values", "lambda" }, builtin_map),
    makeBuiltin("reduce", { "values", "initial", "lambda" }, builtin_reduce),
    makeBuiltin("count", { "values", "lambda" }, builtin_count),
    makeVariadicBuiltin("union", builtin_union),
    makeVariadicBuiltin("difference", builtin_difference),
    makeVariadicBuiltin("intersection", builtin_intersection),
    makeBuiltin("range", { "start", "end" }, builtin_range),
    makeBuiltin("inrange", { "start", "end" }, builtin_inrange),

    makeBuiltin("string-empty?", { "s" }, builtin_string_empty),
    makeBuiltin("string-insert", { "s", "index", "new" }, builtin_string_insert),
    makeBuiltin("string-erase", { "s", "index", "count" }, builtin_string_erase),
    makeVariadicBuiltin("string-format", builtin_string_format),
    makeBuiltin("string-starts-with?", { "s", "prefix" }, builtin_string_starts_with),
    makeBuiltin("string-ends-with?", { "s", "suffix" }, builtin_string_ends_with),
    makeBuiltin("string-contains?", { "s", "element" }, builtin_string_contains),
    makeBuiltin("string-index", { "s", "element" }, builtin_string_index),
    makeBuiltin("string-replace", { "s", "old", "new" }, builtin_string_replace),
    makeBuiltin("string-slice", { "s", "start", "count" }, builtin_string_slice),
    makeBuiltin("string-upcase", { "s" }, builtin_string_upcase),
    makeBuiltin("string-downcase", { "s" }, builtin_string_downcase),

    makeBuiltin("random-engine", {}, builtin_random_engine),
    makeBuiltin("random-linear", { "start", "end", "random-engine" }, builtin_random_linear),
    makeBuiltin("random-gauss", { "mean", "stddev", "random-engine" }, builtin_random_gauss),

    makeBuiltin("degrees->radians", { "degrees" }, builtin_degrees_radians),
    makeBuiltin("radians->degrees", { "radians" }, builtin_radians_degrees),

    makeSyntaxBuiltin("struct-define", builtin_struct_define),
    makeSyntaxBuiltin("struct-undefine", builtin_struct_undefine),

    makeBuiltin("table-set", { "table", "key", "value" }, builtin_table_set),
    makeBuiltin("table-insert", { "table", "key", "value" }, builtin_table_insert),
    makeBuiltin("table-assign", { "table", "key", "value" }, builtin_table_assign),
    makeBuiltin("table-erase", { "table", "key" }, builtin_table_erase),
    makeBuiltin("table-has?", { "table", "key" }, builtin_table_has),
    makeBuiltin("table-get", { "table", "key" }, builtin_table_get),

    makeBuiltin("set-insert", { "set", "value" }, builtin_set_insert),
    makeBuiltin("set-erase", { "set", "value" }, builtin_set_erase),
    makeBuiltin("set-has?", { "set", "value" }, builtin_set_has),

    makeVariadicBuiltin("vec2", builtin_vec2),
    makeBuiltin("vec2?", { "a" }, builtin_vec2_is),
    makeVariadicBuiltin("vec2+", builtin_vec2_add),
    makeVariadicBuiltin("vec2-", builtin_vec2_sub),
    makeVariadicBuiltin("vec2*", builtin_vec2_mul),
    makeVariadicBuiltin("vec2/", builtin_vec2_div),
    makeBuiltin("vec2-dot", { "a", "b" }, builtin_vec2_dot),
    makeBuiltin("vec2-flip", { "a" }, builtin_vec2_flip),
    makeBuiltin("vec2-distance", { "a", "b" }, builtin_vec2_distance),
    makeBuiltin("vec2-length", { "a" }, builtin_vec2_length),
    makeBuiltin("vec2-normalize", { "a" }, builtin_vec2_normalize),

    makeVariadicBuiltin("vec3", builtin_vec3),
    makeBuiltin("vec3?", { "a" }, builtin_vec3_is),
    makeVariadicBuiltin("vec3+", builtin_vec3_add),
    makeVariadicBuiltin("vec3-", builtin_vec3_sub),
    makeVariadicBuiltin("vec3*", builtin_vec3_mul),
    makeVariadicBuiltin("vec3/", builtin_vec3_div),
    makeBuiltin("vec3-dot", { "a", "b" }, builtin_vec3_dot),
    makeBuiltin("vec3-cross", { "a", "b" }, builtin_vec3_cross),
    makeBuiltin("vec3-distance", { "a", "b" }, builtin_vec3_distance),
    makeBuiltin("vec3-length", { "a" }, builtin_vec3_length),
    makeBuiltin("vec3-normalize", { "a" }, builtin_vec3_normalize),

    makeVariadicBuiltin("quaternion", builtin_quaternion),
    makeBuiltin("quaternion?", { "a" }, builtin_quaternion_is),
    makeBuiltin("quaterion-from-direction", { "direction", "roll" }, builtin_quaternion_from_direction),
    makeBuiltin("quaterion-from-axis-angle", { "axis", "angle" }, builtin_quaternion_from_axis_angle),
    makeVariadicBuiltin("quaternion*", builtin_quaternion_mul),
    makeVariadicBuiltin("quaternion/", builtin_quaternion_div),
    makeBuiltin("quaternion-inverse", { "a" }, builtin_quaternion_inverse),
    makeBuiltin("quaternion-rotate", { "q", "v" }, builtin_quaternion_rotate),
    makeBuiltin("quaternion-forward", { "a" }, builtin_quaternion_forward),
    makeBuiltin("quaternion-back", { "a" }, builtin_quaternion_back),
    makeBuiltin("quaternion-left", { "a" }, builtin_quaternion_left),
    makeBuiltin("quaternion-right", { "a" }, builtin_quaternion_right),
    makeBuiltin("quaternion-up", { "a" }, builtin_quaternion_up),
    makeBuiltin("quaternion-down", { "a" }, builtin_quaternion_down),
    makeBuiltin("quaternion-axis", { "a" }, builtin_quaternion_axis),
    makeBuiltin("quaternion-angle", { "a" }, builtin_quaternion_angle),
    makeBuiltin("quaternion-length", { "a" }, builtin_quaternion_length),
    makeBuiltin("quaternion-normalize", { "a" }, builtin_quaternion_normalize),

    makeBuiltin("screen-project", { "position", "rotation", "angle", "aspect", "near", "far", "point" }, builtin_screen_project),
    makeBuiltin("screen-unproject", { "position", "rotation", "angle", "aspect", "near", "far", "point" }, builtin_screen_unproject),

    makeBuiltin("timer-start", { "delay", "callback" }, builtin_timer_start, Script_Builtin_Auth_User),
    makeBuiltin("timer-stop", { "id" }, builtin_timer_stop, Script_Builtin_Auth_User),
    makeBuiltin("timer-all", {}, builtin_timer_all),

    makeBuiltin("repeater-start", { "callback" }, builtin_repeater_start, Script_Builtin_Auth_User),
    makeBuiltin("repeater-stop", { "id" }, builtin_repeater_stop, Script_Builtin_Auth_User),
    makeBuiltin("repeater-all", {}, builtin_repeater_all)
};
