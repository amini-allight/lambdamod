/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "space_graph.hpp"

SpaceGraph::SpaceGraph()
    : _root(nullptr)
{

}

SpaceGraph::SpaceGraph(const map<EntityID, Entity*>& entities)
    : SpaceGraph()
{
    create(entities);
}

SpaceGraph::~SpaceGraph()
{
    destroy();
}

SpaceGraphNode* SpaceGraph::root()
{
    return _root;
}

const SpaceGraphNode* SpaceGraph::root() const
{
    return _root;
}

void SpaceGraph::traverse(const function<void(const uvec3&, SpaceGraphNode*)>& behavior)
{
    if (!_root)
    {
        return;
    }

    behavior(uvec3(), _root);

    _root->traverse(behavior);
}

void SpaceGraph::traverseAscending(const vec3& position, const function<void(const uvec3&, SpaceGraphNode*)>& behavior)
{
    if (!_root)
    {
        return;
    }

    _root->traverseAscending(position, behavior);

    behavior(uvec3(), _root);
}

void SpaceGraph::traverse(const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const
{
    if (!_root)
    {
        return;
    }

    behavior(uvec3(), _root);

    static_cast<const SpaceGraphNode*>(_root)->traverse(behavior);
}

void SpaceGraph::traverseAscending(const vec3& position, const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const
{
    if (!_root)
    {
        return;
    }

    static_cast<const SpaceGraphNode*>(_root)->traverseAscending(position, behavior);

    behavior(uvec3(), _root);
}

void SpaceGraph::update(const map<EntityID, Entity*>& entities)
{
    destroy();
    create(entities);
}

void SpaceGraph::summarizeEntities(const map<EntityID, Entity*>& in, vector<SpaceGraphGenerationEntity>& out) const
{
    out.reserve(in.size());

    for (const auto& [ id, entity ] : in)
    {
        out.push_back(SpaceGraphGenerationEntity(entity));

        summarizeEntities(entity->children(), out);
    }
}

void SpaceGraph::create(const map<EntityID, Entity*>& entities)
{
    if (entities.empty())
    {
        return;
    }

    vector<SpaceGraphGenerationEntity> generationEntities;

    summarizeEntities(entities, generationEntities);

    if (generationEntities.empty())
    {
        return;
    }

    SpaceGraphGenerationContext ctx(
        generationEntities,
        vec3(-numeric_limits<f64>::infinity()),
        vec3(+numeric_limits<f64>::infinity()),
        0
    );

    _root = new SpaceGraphNode(ctx);
}

void SpaceGraph::destroy()
{
    if (_root)
    {
        delete _root;
        _root = nullptr;
    }
}

vector<SpaceGraphGenerationEntity> SpaceGraph::selectWithin(
    const vector<SpaceGraphGenerationEntity>& entities,
    const vec3& innerExtent,
    const vec3& outerExtent
) const
{
    vector<SpaceGraphGenerationEntity> ret;
    ret.reserve(entities.size() / pow(spaceGraphDivisionCount, 3));

    for (const SpaceGraphGenerationEntity& entity : entities)
    {
        if (entity.overlaps(innerExtent, outerExtent))
        {
            ret.push_back(entity);
        }
    }

    return ret;
}
