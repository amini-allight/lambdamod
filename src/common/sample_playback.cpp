/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_playback.hpp"
#include "yaml_tools.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "entity_update.hpp"

SamplePlayback::SamplePlayback()
    : volume(1)
    , directional(false)
    , angle(0)
    , loop(false)
    , elapsed(0)
    , playing(true)
    , forcedSeek(false)
{

}

void SamplePlayback::verify()
{
    volume = max(volume, 0.0);
    angle = clamp(angle, 0.0, tau);
    anchorName = isValidName(anchorName) ? name : toValidName(anchorName);
}

void SamplePlayback::compare(const SamplePlayback& previous, EntityUpdate& update) const
{
    if (forcedSeek)
    {
        update.notify<u64, EntitySamplePlaybackEvent>(elapsed, previous.elapsed, [&](EntitySamplePlaybackEvent& event) -> void {
            event.type = Entity_Sample_Playback_Event_Seek;
            event.position = elapsed;
        }, true);

        forcedSeek = false;
    }

    update.notify<bool, EntitySamplePlaybackEvent>(playing, previous.playing, [&](EntitySamplePlaybackEvent& event) -> void {
        event.type = playing ? Entity_Sample_Playback_Event_Resume : Entity_Sample_Playback_Event_Pause;
    }, true);
}

void SamplePlayback::resume()
{
    playing = true;
}

void SamplePlayback::pause()
{
    playing = false;
}

void SamplePlayback::seek(u64 position)
{
    elapsed = position;
    forcedSeek = true;
}

bool SamplePlayback::operator==(const SamplePlayback& rhs) const
{
    return name == rhs.name &&
        directional == rhs.directional &&
        angle == rhs.angle &&
        volume == rhs.volume &&
        anchorName == rhs.anchorName &&
        loop == rhs.loop &&
        elapsed == rhs.elapsed &&
        playing == rhs.playing;
}

bool SamplePlayback::operator!=(const SamplePlayback& rhs) const
{
    return !(*this == rhs);
}

template<>
SamplePlayback YAMLNode::convert() const
{
    SamplePlayback playback;

    playback.name = at("name").as<string>();
    playback.volume = at("volume").as<f64>();
    playback.directional = at("directional").as<bool>();
    playback.angle = at("angle").as<f64>();
    playback.anchorName = at("anchorName").as<string>();
    playback.loop = at("loop").as<bool>();
    playback.elapsed = at("elapsed").as<u64>();
    playback.playing = at("playing").as<bool>();

    return playback;
}

template<>
void YAMLSerializer::emit(SamplePlayback v)
{
    startMapping();

    emitPair("name", v.name);
    emitPair("volume", v.volume);
    emitPair("directional", v.directional);
    emitPair("angle", v.angle);
    emitPair("anchorName", v.anchorName);
    emitPair("loop", v.loop);
    emitPair("elapsed", v.elapsed);
    emitPair("playing", v.playing);

    endMapping();
}
