/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "dynamics_component.hpp"

class Entity;
class BodyPart;

class DynamicsComponentPart final : public DynamicsComponent
{
public:
    DynamicsComponentPart(
        Dynamics* dynamics,
        DynamicsComponent* parent,
        Entity* entity,
        BodyPart* part
    );
    ~DynamicsComponentPart() override;

    void applyIKEffect(const vec3& translation, const quaternion& rotation) override;

    void writeback() override;

private:
    Entity* entity;
    BodyPart* part;
    mat4 lastRegionalTransform;

    quaternion constrainRotation(const quaternion& rotation) const override;

    bool supportsAnimation() const override;

    void createConstraint() override;
    void createChildren() override;

    mat4 regionalTransform() const;
};
