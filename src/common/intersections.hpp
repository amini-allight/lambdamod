/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

vector<vec3> intersectBodyPartLine(
    f64 length,
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectLineLine(
    const vec3& positionA,
    const vec3& directionA,
    const vec3& positionB,
    const vec3& directionB,
    f64 maxError
);

// These all test against shapes lying on the XY plane
vector<vec2> intersectPlane(
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec2> intersectTriangle(
    const vec2& a,
    const vec2& b,
    const vec2& c,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec2> intersectRectangle(
    f64 width,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec2> intersectCircle(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec2> intersectEllipse(
    f64 xRadius,
    f64 yRadius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec2> intersectPolygon(
    const vector<vec2>& polygon,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectPlane(
    const mat4& planeTransform,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectTriangle(
    const vec3& a,
    const vec3& b,
    const vec3& c,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectRectangle(
    const mat4& planeTransform,
    f64 width,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectCircle(
    const mat4& planeTransform,
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectEllipse(
    const mat4& planeTransform,
    f64 xRadius,
    f64 yRadius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectPolygon(
    const mat4& planeTransform,
    const vector<vec2>& polygon,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectCuboid(
    f64 width,
    f64 length,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectSphere(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectCylinder(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectCone(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectCapsule(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectPyramid(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectTetrahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectOctahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectDodecahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);

vector<vec3> intersectIcosahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
);
