/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "grip_state.hpp"

static constexpr f64 gripPullThreshold = 0.1;

GripStateFrame::GripStateFrame()
{
    strength = 0;
}

GripStateFrame::GripStateFrame(f64 strength, const vec3& linV, const vec3& angV)
    : strength(strength)
    , velocities(linV, angV)
{

}

GripState::GripState()
{

}

void GripState::grip(EntityID entityID)
{
    _entityID = entityID;
}

void GripState::release()
{
    _entityID = EntityID();
}

bool GripState::gripping() const
{
    return _entityID;
}

EntityID GripState::entityID() const
{
    return _entityID;
}

void GripState::push(f64 strength, const vec3& linV, const vec3& angV)
{
    size_t end = gripStateWindowSize - 1;

    for (size_t i = 0; i < end; i++)
    {
        frames[i] = frames[i + 1];
    }

    frames[end] = GripStateFrame(strength, linV, angV);
}

bool GripState::pulling() const
{
    size_t startIndex = 0;
    f64 previous = 1;

    for (size_t i = gripStateWindowSize - 1; i < gripStateWindowSize; i--)
    {
        if (frames[i].strength < previous)
        {
            startIndex = i;
            previous = frames[i].strength;
        }
        else
        {
            break;
        }
    }

    f64 delta = frames[gripStateWindowSize - 1].strength - frames[startIndex].strength;
    size_t length = gripStateWindowSize - startIndex;

    f64 rate = delta / length;

    return rate > gripPullThreshold;
}

Velocities GripState::exitVelocity() const
{
    i32 start = 0;
    i32 end = gripStateWindowSize - 1;

    // Linear velocity
    i32 linVPeakIndex = 0;
    f64 linVPeakSpeed = numeric_limits<f64>::lowest();
    vec3 linV;

    for (size_t i = 0; i < gripStateWindowSize; i++)
    {
        f64 speed = frames[i].velocities.linearVelocity.length();

        if (speed > linVPeakSpeed)
        {
            linVPeakIndex = i;
            linVPeakSpeed = speed;
        }
    }

    i32 linVStart = max(linVPeakIndex - 1, start);
    i32 linVEnd = min(linVPeakIndex + 2, end);

    for (i32 i = linVStart; i < linVEnd; i++)
    {
        linV += frames[i].velocities.linearVelocity;
    }

    linV /= linVEnd - linVStart;

    // Angular velocity
    i32 angVPeakIndex = 0;
    f64 angVPeakSpeed = numeric_limits<f64>::lowest();
    vec3 angV;

    for (size_t i = 0; i < gripStateWindowSize; i++)
    {
        f64 speed = frames[i].velocities.angularVelocity.length();

        if (speed > angVPeakSpeed)
        {
            angVPeakIndex = i;
            angVPeakSpeed = speed;
        }
    }

    i32 angVStart = max(angVPeakIndex - 1, start);
    i32 angVEnd = min(angVPeakIndex + 2, end);

    for (i32 i = angVStart; i < angVEnd; i++)
    {
        angV += frames[i].velocities.angularVelocity;
    }

    angV /= angVEnd - angVStart;

    return { linV, angV };
}
