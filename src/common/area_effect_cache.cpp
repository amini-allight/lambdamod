/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "area_effect_cache.hpp"
#include "world.hpp"

AreaEffectCache::AreaEffectCache()
    : lastPosition(numeric_limits<f64>::lowest())
{

}

void AreaEffectCache::updateWorld(const World* world)
{
    if (!world)
    {
        worldParameters = {
            defaultSpeedOfSound,
            defaultGravity,
            vec3(),
            defaultDensity,
            upDir,
            0,
            SkyParameters(),
            AtmosphereParameters()
        };
        lastPosition = vec3(numeric_limits<f64>::lowest());
        return;
    }

    worldParameters = {
        world->speedOfSound(),
        world->gravity(),
        world->flowVelocity(),
        world->density(),
        world->up(),
        world->fogDistance(),
        world->sky(),
        world->atmosphere()
    };

    world->traverse([&](const Entity* entity) -> void {
        entity->body().traverse([&](const BodyPart* part) -> void {
            if (part->type() != Body_Part_Area)
            {
                return;
            }

            BodyPartArea area = part->get<BodyPartArea>();

            worldAreas.push_back({
                entity->globalTransform().applyToTransform(part->regionalTransform()).inverse(),
                entity->physical(),
                entity->visible(),
                entity->audible(),
                area
            });
        });
    });

    lastPosition = vec3(numeric_limits<f64>::lowest());
}

const WorldParameters& AreaEffectCache::at(const vec3& position) const
{
    if (position != lastPosition)
    {
        lastPosition = position;
        lastResult = applyAreaEffects(position);
    }

    return lastResult;
}

WorldParameters AreaEffectCache::applyAreaEffects(const vec3& position) const
{
    optional<f64> speedOfSound;
    f64 closestSpeedOfSound = numeric_limits<f64>::max();
    optional<vec3> gravity;
    f64 closestGravity = numeric_limits<f64>::max();
    optional<vec3> flowVelocity;
    f64 closestFlowVelocity = numeric_limits<f64>::max();
    optional<f64> density;
    f64 closestDensity = numeric_limits<f64>::max();
    optional<vec3> up;
    f64 closestUp = numeric_limits<f64>::max();
    optional<f64> fogDistance;
    f64 closestFogDistance = numeric_limits<f64>::max();
    optional<SkyParameters> sky;
    f64 closestSky = numeric_limits<f64>::max();
    optional<AtmosphereParameters> atmosphere;
    f64 closestAtmosphere = numeric_limits<f64>::max();

    for (const WorldArea& area : worldAreas)
    {
        if (!area.area.enabled)
        {
            continue;
        }

        optional<f64> oImmersion = area.area.immersion(area.toAreaSpace.applyToPosition(position));

        if (!oImmersion)
        {
            continue;
        }

        f64 immersion = *oImmersion;

        if (area.audible && immersion < closestSpeedOfSound && area.area.speedOfSound)
        {
            speedOfSound = area.area.speedOfSound;
            closestSpeedOfSound = immersion;
        }

        if (area.physical && immersion < closestGravity && area.area.gravity)
        {
            gravity = area.area.gravity;
            closestGravity = immersion;
        }

        if (area.physical && immersion < closestFlowVelocity && area.area.flowVelocity)
        {
            flowVelocity = area.area.flowVelocity;
            closestFlowVelocity = immersion;
        }

        if (area.physical && immersion < closestDensity && area.area.density)
        {
            density = area.area.density;
            closestDensity = immersion;
        }

        if (immersion < closestUp && area.area.up)
        {
            up = area.area.up;
            closestUp = immersion;
        }

        if (area.visible && immersion < closestFogDistance && area.area.fogDistance)
        {
            fogDistance = area.area.fogDistance;
            closestFogDistance = immersion;
        }

        if (area.visible && immersion < closestSky && area.area.sky)
        {
            sky = area.area.sky;
            closestSky = immersion;
        }

        if (area.visible && immersion < closestAtmosphere && area.area.atmosphere)
        {
            atmosphere = area.area.atmosphere;
            closestAtmosphere = immersion;
        }
    }

    if (!speedOfSound)
    {
        speedOfSound = worldParameters.speedOfSound;
    }

    if (!gravity)
    {
        gravity = worldParameters.gravity;
    }

    if (!flowVelocity)
    {
        flowVelocity = worldParameters.flowVelocity;
    }

    if (!density)
    {
        density = worldParameters.density;
    }

    if (!up)
    {
        up = worldParameters.up;
    }

    if (!fogDistance)
    {
        fogDistance = worldParameters.fogDistance;
    }

    if (!sky)
    {
        sky = worldParameters.sky;
    }

    if (!atmosphere)
    {
        atmosphere = worldParameters.atmosphere;
    }

    return {
        *speedOfSound,
        *gravity,
        *flowVelocity,
        *density,
        *up,
        *fogDistance,
        *sky,
        *atmosphere
    };
}
