/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "terrain.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

TerrainNode::TerrainNode()
    : radius(1)
    , oneSided(true)
{

}

TerrainNode::TerrainNode(TerrainNodeID id)
    : TerrainNode()
{
    this->id = id;
}

void TerrainNode::verify(const set<TerrainNodeID>& nodeIDs)
{
    position = verifyPosition(position);
    if (color)
    {
        color->verify();
    }
    radius = max(radius, numeric_limits<f64>::min());
    for (auto it = linkedIDs.begin(); it != linkedIDs.end();)
    {
        if (!nodeIDs.contains(*it))
        {
            linkedIDs.erase(it++);
        }
        else
        {
            it++;
        }
    }
}

bool TerrainNode::operator==(const TerrainNode& rhs) const
{
    return id == rhs.id &&
        position == rhs.position &&
        color == rhs.color &&
        radius == rhs.radius &&
        oneSided == rhs.oneSided &&
        linkedIDs == rhs.linkedIDs;
}

bool TerrainNode::operator!=(const TerrainNode& rhs) const
{
    return !(*this == rhs);
}

template<>
TerrainNode YAMLNode::convert() const
{
    TerrainNode node;

    node.id = at("id").as<TerrainNodeID>();
    node.position = at("position").as<vec3>();
    node.color = at("color").as<optional<EmissiveColor>>();
    node.radius = at("radius").as<f64>();
    node.oneSided = at("oneSided").as<bool>();
    node.linkedIDs = at("linkedIDs").as<set<TerrainNodeID>>();

    return node;
}

template<>
void YAMLSerializer::emit(TerrainNode node)
{
    startMapping();

    emitPair("id", node.id);
    emitPair("position", node.position);
    emitPair("color", node.color);
    emitPair("radius", node.radius);
    emitPair("oneSided", node.oneSided);
    emitPair("linkedIDs", node.linkedIDs);

    endMapping();
}

vector<TerrainNode> createTerrainGrid(TerrainNodeID* nextID, f64 size, f64 spacing)
{
    u32 resolution = (size / spacing) + 1;
    f64 offset = -size / 2;

    vector<TerrainNode> nodes(resolution * resolution);

    for (u32 y = 0; y < resolution; y++)
    {
        for (u32 x = 0; x < resolution; x++)
        {
            TerrainNode node((*nextID)++);
            node.position = vec3(offset + x * spacing, offset + y * spacing, 0);

            nodes[y * resolution + x] = node;
        }
    }

    for (u32 y = 0; y < resolution; y++)
    {
        for (u32 x = 0; x < resolution; x++)
        {
            if (x != 0)
            {
                nodes[y * resolution + x].linkedIDs.insert(nodes[y * resolution + (x - 1)].id);
            }

            if (x + 1 != resolution)
            {
                nodes[y * resolution + x].linkedIDs.insert(nodes[y * resolution + (x + 1)].id);
            }

            if (y != 0)
            {
                nodes[y * resolution + x].linkedIDs.insert(nodes[(y - 1) * resolution + x].id);
            }

            if (y + 1 != resolution)
            {
                nodes[y * resolution + x].linkedIDs.insert(nodes[(y + 1) * resolution + x].id);
            }
        }
    }

    return nodes;
}
