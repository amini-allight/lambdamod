/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_DEBUG
#pragma once

#include "types.hpp"

class RateMonitor;

class RateMonitorEntry
{
public:
    RateMonitorEntry(RateMonitor* parent, size_t count);

    size_t count() const;
    bool expired() const;

private:
    RateMonitor* parent;
    size_t _count;
    chrono::milliseconds creationTime;
};

class RateMonitor
{
public:
    RateMonitor(chrono::milliseconds window);

    f64 push(size_t count);

private:
    friend class RateMonitorEntry;

    chrono::milliseconds window;
    deque<RateMonitorEntry> entries;
};
#endif
