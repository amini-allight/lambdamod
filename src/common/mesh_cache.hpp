/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "tools.hpp"
#include "structure_builder.hpp"
#include "terrain_builder.hpp"
#include "dynamics_raycast_receiver.hpp"

template<typename... Args>
class MeshCacheKey
{
public:
    MeshCacheKey(Args... args)
        : values(args...)
    {

    }

    bool operator==(const MeshCacheKey& rhs) const
    {
        return values == rhs.values;
    }

    bool operator!=(const MeshCacheKey& rhs) const
    {
        return !(*this == rhs);
    }

private:
    tuple<Args...> values;
};

template<typename T>
class MeshCacheEntry
{
public:
    MeshCacheEntry(const T& value, chrono::milliseconds timeout)
        : value(value)
        , lastAccessed(currentTime())
        , timeout(timeout)
    {
        btIndexedMesh indexedMesh{};
        indexedMesh.m_numTriangles = value.indices.size() / 3;
        indexedMesh.m_triangleIndexBase = reinterpret_cast<const u8*>(value.indices.data());
        indexedMesh.m_triangleIndexStride = 3 * sizeof(u32);
        indexedMesh.m_indexType = PHY_INTEGER;

        indexedMesh.m_numVertices = value.vertices.size();
        indexedMesh.m_vertexBase = reinterpret_cast<const u8*>(value.vertices.data());
        indexedMesh.m_vertexStride = sizeof(typename decltype(T::vertices)::value_type);
        indexedMesh.m_vertexType = PHY_FLOAT;

        mesh = new btTriangleMesh();
        mesh->addIndexedMesh(indexedMesh);

        shape = new btBvhTriangleMeshShape(mesh, true, true);
    }
    MeshCacheEntry(const MeshCacheEntry& rhs) = delete;
    MeshCacheEntry(MeshCacheEntry&& rhs) = delete;
    ~MeshCacheEntry()
    {
        delete shape;
        delete mesh;
    }

    MeshCacheEntry& operator=(const MeshCacheEntry& rhs) = delete;
    MeshCacheEntry& operator=(MeshCacheEntry&& rhs) = delete;

    const T& get() const
    {
        lastAccessed = currentTime();
        return value;
    }

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const
    {
        lastAccessed = currentTime();

        vec3 end = position + direction * distance;

        DynamicsRaycastReceiver receiver(position, end);

        shape->performRaycast(&receiver, btVector3(position.x, position.y, position.z), btVector3(end.x, end.y, end.z));

        receiver.sort();

        sort(
            receiver.intersections.begin(),
            receiver.intersections.end(),
            [position](const vec3& a, const vec3& b) -> bool { return a.distance(position) < b.distance(position); }
        );

        return receiver.intersections;
    }

    bool expired() const
    {
        return currentTime() - lastAccessed > timeout;
    }

private:
    T value;
    mutable chrono::milliseconds lastAccessed;
    chrono::milliseconds timeout;
    btTriangleMesh* mesh;
    btBvhTriangleMeshShape* shape;
};

class MeshCache
{
public:
    MeshCache();
    MeshCache(const MeshCache& rhs) = delete;
    MeshCache(MeshCache&& rhs) = delete;
    ~MeshCache();

    MeshCache& operator=(const MeshCache& rhs) = delete;
    MeshCache& operator=(MeshCache&& rhs) = delete;

    // These references are only valid until the next ensure call, do not hold them
    const StructureMesh& ensure(const StructureNode& node) const;
    const TerrainMesh& ensure(const EmissiveColor& color, const vector<TerrainNode>& nodes) const;
    vector<vec3> intersect(
        const StructureNode& node,
        const vec3& position,
        const vec3& direction,
        f64 distance
    ) const;
    vector<vec3> intersect(
        const EmissiveColor& color,
        const vector<TerrainNode>& nodes,
        const vec3& position,
        const vec3& direction,
        f64 distance
    ) const;

private:
    mutable vector<pair<MeshCacheKey<StructureNode>, MeshCacheEntry<StructureMesh>*>> cachedStructures;
    mutable vector<pair<MeshCacheKey<EmissiveColor, vector<TerrainNode>>, MeshCacheEntry<TerrainMesh>*>> cachedTerrains;

    const MeshCacheEntry<StructureMesh>* ensureEntry(const StructureNode& node) const;
    const MeshCacheEntry<TerrainMesh>* ensureEntry(const EmissiveColor& color, const vector<TerrainNode>& nodes) const;

    void clean() const;
};

extern MeshCache* meshCache;
