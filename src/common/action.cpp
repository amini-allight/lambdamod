/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action.hpp"
#include "entity_update.hpp"

Action::Action()
    : nextPartID(1)
{

}

void Action::verify()
{
    for (auto& [ id, part ] : _parts)
    {
        part.verify();
    }
}

void Action::compare(const string& name, const Action& previous, EntityUpdate& update) const
{
    update.notify<ActionPartID, EntityActionEvent>(nextPartID, previous.nextPartID, [&](EntityActionEvent& event) -> void {
        event.type = Entity_Action_Event_Next_Part_ID;
        event.name = name;
        event.partID = nextPartID;
    });

    update.notifyMap<ActionPartID, ActionPart, EntityActionEvent>(
        _parts,
        previous._parts,
        [&](EntityActionEvent& event, const ActionPartID& id, const ActionPart& part) -> void
        {
            event.type = Entity_Action_Event_Add_Part;
            event.name = name;
            event.partID = id;
            event.part = part;
        },
        [&](EntityActionEvent& event, const ActionPartID& id) -> void
        {
            event.type = Entity_Action_Event_Remove_Part;
            event.name = name;
            event.partID = id;
        },
        [&](EntityActionEvent& event, const ActionPartID& id, const ActionPart& part) -> void
        {
            event.type = Entity_Action_Event_Update_Part;
            event.name = name;
            event.partID = id;
            event.part = part;
        }
    );
}

void Action::push(const NetworkEvent& event)
{
    auto subEvent = event.payload<EntityActionEvent>();

    switch (subEvent.type)
    {
    default :
        break;
    case Entity_Action_Event_Next_Part_ID :
        nextPartID = subEvent.partID;
        break;
    case Entity_Action_Event_Add_Part :
        if (subEvent.partID > nextPartID)
        {
            nextPartID = subEvent.partID;
        }

        _parts.insert_or_assign(subEvent.partID, subEvent.part);
        break;
    case Entity_Action_Event_Remove_Part :
        _parts.erase(subEvent.partID);
        break;
    case Entity_Action_Event_Update_Part :
    {
        auto it = _parts.find(subEvent.partID);

        if (it != _parts.end())
        {
            it->second = subEvent.part;
        }
        break;
    }
    }
}

vector<NetworkEvent> Action::initial(const string& world, EntityID id, const string& name) const
{
    vector<NetworkEvent> events;

    {
        EntityActionEvent event;
        event.type = Entity_Action_Event_Add;
        event.world = world;
        event.id = id;
        event.name = name;

        events.push_back(event);
    }

    {
        EntityActionEvent event;
        event.type = Entity_Action_Event_Next_Part_ID;
        event.world = world;
        event.id = id;
        event.name = name;
        event.partID = nextPartID;

        events.push_back(event);
    }

    for (const auto& [ partID, part ] : _parts)
    {
        EntityActionEvent event;
        event.type = Entity_Action_Event_Add_Part;
        event.world = world;
        event.id = id;
        event.name = name;
        event.partID = partID;
        event.part = part;

        events.push_back(event);
    }

    return events;
}

ActionPartID Action::add()
{
    ActionPartID id = nextPartID++;

    ActionPart part(id);

    add(part);

    return id;
}

void Action::add(const ActionPart& part)
{
    if (part.id() > nextPartID)
    {
        nextPartID = part.id();
    }

    _parts.insert_or_assign(part.id(), part);
}

void Action::remove(ActionPartID id)
{
    _parts.erase(id);
}

const map<ActionPartID, ActionPart>& Action::parts() const
{
    return _parts;
}

u32 Action::length() const
{
    u32 length = 0;

    for (const auto& [ id, part ] : _parts)
    {
        if (part.end() > length)
        {
            length = part.end();
        }
    }

    return length;
}

bool Action::operator==(const Action& rhs) const
{
    return nextPartID == rhs.nextPartID &&
        _parts == rhs._parts;
}

bool Action::operator!=(const Action& rhs) const
{
    return !(*this == rhs);
}

template<>
Action YAMLNode::convert() const
{
    Action action;

    action.nextPartID = at("nextPartID").as<ActionPartID>();
    action._parts = at("parts").as<map<ActionPartID, ActionPart>>();

    return action;
}

template<>
void YAMLSerializer::emit(Action v)
{
    startMapping();

    emitPair("nextPartID", v.nextPartID);
    emitPair("parts", v._parts);

    endMapping();
}
