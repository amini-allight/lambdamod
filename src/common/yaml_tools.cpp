/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "yaml_tools.hpp"
#include "log.hpp"
#include "tools.hpp"

YAMLNode::YAMLNode()
    : _type(YAML_Node_Scalar)
    , null(false)
{

}

YAMLNode::YAMLNode(size_t i)
    : _type(YAML_Node_Scalar)
    , null(false)
    , _scalar(to_string(i))
{

}

YAMLNode::YAMLNode(const string& s)
    : _type(YAML_Node_Scalar)
    , null(false)
    , _scalar(s)
{

}

YAMLNode::YAMLNode(const string& path, yaml_parser_t& parser, yaml_event_t& event)
    : path(path)
    , null(false)
{
    switch (event.type)
    {
    default :
        throw runtime_error("Unexpected YAML event type '" + to_string(event.type) + "'.");
    case YAML_SCALAR_EVENT :
        _type = YAML_Node_Scalar;

        if (strcmp(reinterpret_cast<const char*>(event.data.scalar.value), "null") == 0 &&
            event.data.scalar.style != YAML_SINGLE_QUOTED_SCALAR_STYLE &&
            event.data.scalar.style != YAML_DOUBLE_QUOTED_SCALAR_STYLE
        )
        {
            null = true;
        }

        _scalar = reinterpret_cast<const char*>(event.data.scalar.value);
        yaml_event_delete(&event);
        break;
    case YAML_SEQUENCE_START_EVENT :
        yaml_event_delete(&event);
        _type = YAML_Node_Sequence;
        while (true)
        {
            check(yaml_parser_parse(&parser, &event));

            if (event.type == YAML_SEQUENCE_END_EVENT)
            {
                yaml_event_delete(&event);
                break;
            }
            else
            {
                _sequence.push_back(YAMLNode(path, parser, event));
            }
        }
        break;
    case YAML_MAPPING_START_EVENT :
    {
        yaml_event_delete(&event);
        _type = YAML_Node_Mapping;
        bool keyNext = true;
        YAMLNode key;
        while (true)
        {
            check(yaml_parser_parse(&parser, &event));

            if (event.type == YAML_MAPPING_END_EVENT)
            {
                yaml_event_delete(&event);
                break;
            }
            else if (keyNext)
            {
                key = YAMLNode(path, parser, event);
                keyNext = false;
            }
            else
            {
                _mapping.insert({ key, YAMLNode(path, parser, event) });
                keyNext = true;
            }
        }
        break;
    }
    }
}

YAMLNodeType YAMLNode::type() const
{
    return _type;
}

bool YAMLNode::isNull() const
{
    return null;
}

const string& YAMLNode::scalar() const
{
    return _scalar;
}

const vector<YAMLNode>& YAMLNode::sequence() const
{
    return _sequence;
}

const map<YAMLNode, YAMLNode>& YAMLNode::mapping() const
{
    return _mapping;
}

bool YAMLNode::has(size_t i) const
{
    switch (_type)
    {
    default :
    case YAML_Node_Scalar :
        return false;
    case YAML_Node_Sequence :
        return i < _sequence.size();
    case YAML_Node_Mapping :
        return _mapping.contains(YAMLNode(i));
    }
}

bool YAMLNode::has(const string& s) const
{
    switch (_type)
    {
    default :
    case YAML_Node_Scalar :
        return false;
    case YAML_Node_Sequence :
        return false;
    case YAML_Node_Mapping :
        return _mapping.contains(YAMLNode(s));
    }
}

const YAMLNode& YAMLNode::at(size_t i) const
{
    switch (_type)
    {
    default :
    case YAML_Node_Scalar :
        return *this;
    case YAML_Node_Sequence :
        return _sequence.at(i);
    case YAML_Node_Mapping :
        return _mapping.at(YAMLNode(i));
    }
}

const YAMLNode& YAMLNode::at(const string& s) const
{
    switch (_type)
    {
    default :
    case YAML_Node_Scalar :
        return *this;
    case YAML_Node_Sequence :
        return *this;
    case YAML_Node_Mapping :
        return _mapping.at(YAMLNode(s));
    }
}

const YAMLNode& YAMLNode::operator[](size_t i) const
{
    return at(i);
}

const YAMLNode& YAMLNode::operator[](const string& s) const
{
    return at(s);
}

bool YAMLNode::operator==(const YAMLNode& rhs) const
{
    if (_type != rhs._type)
    {
        return false;
    }

    switch (_type)
    {
    default :
        return false;
    case YAML_Node_Scalar :
        return _scalar == rhs._scalar;
    case YAML_Node_Sequence :
        return _sequence == rhs._sequence;
    case YAML_Node_Mapping :
        return _mapping == rhs._mapping;
    }
}

bool YAMLNode::operator!=(const YAMLNode& rhs) const
{
    return !(*this == rhs);
}

bool YAMLNode::operator>=(const YAMLNode& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool YAMLNode::operator<=(const YAMLNode& rhs) const
{
    return *this < rhs || *this == rhs;
}

bool YAMLNode::operator>(const YAMLNode& rhs) const
{
    if (_type != rhs._type)
    {
        return _type > rhs._type;
    }

    switch (_type)
    {
    default :
        return false;
    case YAML_Node_Scalar :
        return _scalar > rhs._scalar;
    case YAML_Node_Sequence :
        return _sequence > rhs._sequence;
    case YAML_Node_Mapping :
        return _mapping > rhs._mapping;
    }
}

bool YAMLNode::operator<(const YAMLNode& rhs) const
{
    if (_type != rhs._type)
    {
        return _type < rhs._type;
    }

    switch (_type)
    {
    default :
        return false;
    case YAML_Node_Scalar :
        return _scalar < rhs._scalar;
    case YAML_Node_Sequence :
        return _sequence < rhs._sequence;
    case YAML_Node_Mapping :
        return _mapping < rhs._mapping;
    }
}

strong_ordering YAMLNode::operator<=>(const YAMLNode& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

void YAMLNode::check(int status)
{
    if (!status)
    {
        throw runtime_error("Failed to parse file '" + path + "': " + to_string(status));
    }
}

template<>
bool YAMLNode::convert() const
{
    return boolFromYAML(_scalar);
}

template<>
string YAMLNode::convert() const
{
    return _scalar;
}

template<>
f64 YAMLNode::convert() const
{
    return stod(_scalar);
}

template<>
u8 YAMLNode::convert() const
{
    return stoi(_scalar);
}

template<>
u16 YAMLNode::convert() const
{
    return stoi(_scalar);
}

template<>
u32 YAMLNode::convert() const
{
    return toU64(_scalar);
}

template<>
u64 YAMLNode::convert() const
{
    return toU64(_scalar);
}

template<>
i8 YAMLNode::convert() const
{
    return stoi(_scalar);
}

template<>
i16 YAMLNode::convert() const
{
    return stoi(_scalar);
}

template<>
i32 YAMLNode::convert() const
{
    return stoi(_scalar);
}

template<>
i64 YAMLNode::convert() const
{
    return stoll(_scalar);
}

template<>
vec2 YAMLNode::convert() const
{
    return {
        _sequence.at(0).as<f64>(),
        _sequence.at(1).as<f64>()
    };
}

template<>
vec3 YAMLNode::convert() const
{
    return {
        _sequence.at(0).as<f64>(),
        _sequence.at(1).as<f64>(),
        _sequence.at(2).as<f64>()
    };
}

template<>
vec4 YAMLNode::convert() const
{
    return {
        _sequence.at(0).as<f64>(),
        _sequence.at(1).as<f64>(),
        _sequence.at(2).as<f64>(),
        _sequence.at(3).as<f64>()
    };
}

template<>
quaternion YAMLNode::convert() const
{
    return {
        _sequence.at(0).as<f64>(),
        _sequence.at(1).as<f64>(),
        _sequence.at(2).as<f64>(),
        _sequence.at(3).as<f64>()
    };
}

template<>
mat4 YAMLNode::convert() const
{
    return {
        at("position").as<vec3>(),
        at("rotation").as<quaternion>(),
        at("scale").as<vec3>()
    };
}

template<>
YAMLNode YAMLNode::convert() const
{
    return *this;
}

YAMLParser::YAMLParser(const string& path)
    : path(path)
    , _root(path)
{
    file = fopen(path.c_str(), "r");

    if (!file)
    {
        error("Failed to open file '" + path + "'.");
        return;
    }

    int ok = yaml_parser_initialize(&parser);

    if (!ok)
    {
        error("Failed to initialize YAML parser.");
        fclose(file);
        return;
    }

    Defer defer([&]{
        yaml_event_delete(&event);
        yaml_parser_delete(&parser);
        fclose(file);
    });

    yaml_parser_set_input_file(&parser, file);

    check(yaml_parser_parse(&parser, &event));
    check(event.type == YAML_STREAM_START_EVENT);
    yaml_event_delete(&event);

    check(yaml_parser_parse(&parser, &event));
    check(event.type == YAML_DOCUMENT_START_EVENT);
    yaml_event_delete(&event);

    check(yaml_parser_parse(&parser, &event));

    if (event.type == YAML_DOCUMENT_END_EVENT)
    {
        yaml_event_delete(&event);
        goto streamEnd;
    }

    _root = YAMLNode(path, parser, event);

    check(yaml_parser_parse(&parser, &event));
    check(event.type == YAML_DOCUMENT_END_EVENT);
    yaml_event_delete(&event);

streamEnd:
    check(yaml_parser_parse(&parser, &event));
    check(event.type == YAML_STREAM_END_EVENT);
    yaml_event_delete(&event);
}

const YAMLNode& YAMLParser::root() const
{
    return _root;
}

void YAMLParser::check(int status)
{
    if (!status)
    {
        throw runtime_error("Failed to parse file '" + path + "': " + to_string(status));
    }
}

YAMLSerializer::YAMLSerializer(const string& path)
    : path(path)
{
    file = fopen(path.c_str(), "w");

    if (!file)
    {
        error("Failed to open file '" + path + "'.");
        return;
    }

    int ok = yaml_emitter_initialize(&emitter);

    if (!ok)
    {
        error("Failed to initialize YAML emitter.");
        fclose(file);
        return;
    }

    yaml_emitter_set_output_file(&emitter, file);

    check(yaml_stream_start_event_initialize(&event, YAML_UTF8_ENCODING));
    check(yaml_emitter_emit(&emitter, &event));

    check(yaml_document_start_event_initialize(&event, nullptr, nullptr, nullptr, 0));
    check(yaml_emitter_emit(&emitter, &event));
}

YAMLSerializer::~YAMLSerializer()
{
    check(yaml_document_end_event_initialize(&event, 0));
    check(yaml_emitter_emit(&emitter, &event));

    check(yaml_stream_end_event_initialize(&event));
    check(yaml_emitter_emit(&emitter, &event));

    yaml_event_delete(&event);
    yaml_emitter_delete(&emitter);
    fclose(file);
}

void YAMLSerializer::startSequence()
{
    check(yaml_sequence_start_event_initialize(&event, nullptr, reinterpret_cast<const yaml_char_t*>(YAML_SEQ_TAG), 1, YAML_ANY_SEQUENCE_STYLE));
    check(yaml_emitter_emit(&emitter, &event));
}

void YAMLSerializer::endSequence()
{
    check(yaml_sequence_end_event_initialize(&event));
    check(yaml_emitter_emit(&emitter, &event));
}

void YAMLSerializer::startMapping()
{
    check(yaml_mapping_start_event_initialize(&event, nullptr, reinterpret_cast<const yaml_char_t*>(YAML_MAP_TAG), 1, YAML_ANY_MAPPING_STYLE));
    check(yaml_emitter_emit(&emitter, &event));
}

void YAMLSerializer::endMapping()
{
    check(yaml_mapping_end_event_initialize(&event));
    check(yaml_emitter_emit(&emitter, &event));
}

void YAMLSerializer::emitNull()
{
    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_NULL_TAG),
        reinterpret_cast<const yaml_char_t*>("null"),
        4,
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

void YAMLSerializer::check(int status)
{
    if (!status)
    {
        throw runtime_error("Failed to serialize file '" + path + "': " + to_string(status));
    }
}

template<>
void YAMLSerializer::emit(bool v)
{
    string s = v ? "yes" : "no";

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_BOOL_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(string v)
{
    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_STR_TAG),
        reinterpret_cast<const yaml_char_t*>(v.data()),
        v.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(const char* v)
{
    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_STR_TAG),
        reinterpret_cast<const yaml_char_t*>(v),
        strlen(v),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(f64 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_FLOAT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(u8 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(u16 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(u32 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(u64 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(i8 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(i16 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(i32 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(i64 v)
{
    string s = to_string(v);

    check(yaml_scalar_event_initialize(
        &event,
        nullptr,
        reinterpret_cast<const yaml_char_t*>(YAML_INT_TAG),
        reinterpret_cast<const yaml_char_t*>(s.data()),
        s.size(),
        1,
        0,
        YAML_PLAIN_SCALAR_STYLE
    ));
    check(yaml_emitter_emit(&emitter, &event));
}

template<>
void YAMLSerializer::emit(vec2 v)
{
    startSequence();

    emit(v.x);
    emit(v.y);

    endSequence();
}

template<>
void YAMLSerializer::emit(vec3 v)
{
    startSequence();

    emit(v.x);
    emit(v.y);
    emit(v.z);

    endSequence();
}

template<>
void YAMLSerializer::emit(vec4 v)
{
    startSequence();

    emit(v.x);
    emit(v.y);
    emit(v.z);
    emit(v.w);

    endSequence();
}

template<>
void YAMLSerializer::emit(quaternion v)
{
    startSequence();

    emit(v.w);
    emit(v.x);
    emit(v.y);
    emit(v.z);

    endSequence();
}

template<>
void YAMLSerializer::emit(mat4 v)
{
    startMapping();

    emitPair("position", v.position());
    emitPair("rotation", v.rotation());
    emitPair("scale", v.scale());

    endMapping();
}

bool boolFromYAML(const string& s)
{
    return regex_match(s, regex("(y|Y|yes|Yes|YES|true|True|TRUE|on|On|ON)"));
}
