/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "terrain_builder.hpp"
#include "tools.hpp"
#include "polygon_triangulation.hpp"

// TODO: TERRAIN: Meshing not implemented

TerrainVertex::TerrainVertex()
    : normal(forwardDir)
    , color(1)
{

}

TerrainVertex::TerrainVertex(const fvec3& position, const fvec3& normal, const fvec4& color)
    : position(position)
    , normal(normal)
    , color(color)
{

}

bool TerrainVertex::operator==(const TerrainVertex& rhs) const
{
    return position == rhs.position &&
        normal == rhs.normal &&
        color == rhs.color;
}

bool TerrainVertex::operator!=(const TerrainVertex& rhs) const
{
    return !(*this == rhs);
}

TerrainMesh::TerrainMesh()
{

}

TerrainMesh::TerrainMesh(const vector<TerrainVertex>& vertices, const vector<u32>& indices)
    : vertices(vertices)
    , indices(indices)
{

}

void TerrainMesh::add(const TerrainMesh& rhs)
{
    u32 offset = vertices.size();
    vertices.insert(vertices.end(), rhs.vertices.begin(), rhs.vertices.end());
    indices.reserve(indices.size() + rhs.indices.size());

    for (u32 index : rhs.indices)
    {
        indices.push_back(offset + index);
    }
}

bool TerrainMesh::operator==(const TerrainMesh& rhs) const
{
    return vertices == rhs.vertices &&
        indices == rhs.indices;
}

bool TerrainMesh::operator!=(const TerrainMesh& rhs) const
{
    return !(*this == rhs);
}

struct VertexData
{
    vec3 position;
    vec3 normal;
    fvec4 color;
};

struct LineData
{
    VertexData a;
    VertexData b;
};

struct LineIndices
{
    i64 a;
    i64 b;
};

struct LineLoop
{
    vector<i64> indices;
};

static vec3 buildTangent(const quaternion& space, const vec3& vertex, const vec3& origin, f64 radius)
{
    vec3 vertexToSelf = (origin - vertex).normalize();

    f64 vertexToSelfDistance = origin.distance(vertex);
    f64 selfToTangentDistance = radius;
    f64 vertexToTangentDistance = sqrt(sq(vertexToSelfDistance) - sq(selfToTangentDistance));

    f64 angle = asin(selfToTangentDistance / (vertexToSelfDistance / sin(pi / 2)));

    return vertex + quaternion(space.left(), angle).rotate(vertexToSelf) * vertexToTangentDistance;
}

static vector<LineData> buildNode(const fvec4& baseColor, const TerrainNode& self, const map<TerrainNodeID, TerrainNode>& nodes)
{
    vector<LineData> lines;

    fvec4 selfColor = self.color ? fvec4(self.color->toVec4()) : baseColor;

    for (TerrainNodeID id : self.linkedIDs)
    {
        const TerrainNode& neighbor = nodes.at(id);
        fvec4 neighborColor = neighbor.color ? fvec4(neighbor.color->toVec4()) : baseColor;

        vec3 interconnectPosition = (self.position + neighbor.position) / 2;
        vec3 interconnectForward = (neighbor.position - self.position).normalize();
        vec3 interconnectSide = pickUpDirection(interconnectForward).cross(interconnectForward).normalize();
        vec3 interconnectUp = interconnectForward.cross(interconnectSide).normalize();

        mat4 interconnectTransform(interconnectPosition, quaternion(interconnectForward, interconnectUp), vec3(1));

        f64 interconnectRadius = (self.radius + neighbor.radius) / 2;

        bool interconnectOneSided = self.oneSided && neighbor.oneSided;

        if (interconnectOneSided)
        {
            LineData line;

            vec3 outward = -interconnectUp;

            vec3 vertex = interconnectPosition + outward * interconnectRadius;

            line.a.position = vertex;
            line.a.normal = -outward;
            line.a.color = (selfColor + neighborColor) / 2;

            vec3 intercept = buildTangent(
                quaternion(interconnectForward, outward),
                vertex,
                self.position,
                self.radius
            );

            line.b.position = intercept;
            line.b.normal = (self.position - intercept).normalize();
            line.b.color = selfColor;

            lines.push_back(line);
        }
        else
        {
            for (u32 i = 0; i < terrainSegments; i++)
            {
                LineData line;

                vec3 outward = quaternion(interconnectForward, i * (tau / terrainSegments)).rotate(-interconnectUp);

                vec3 vertex = interconnectPosition + outward * interconnectRadius;

                line.a.position = vertex;
                line.a.normal = -outward;
                line.a.color = (selfColor + neighborColor) / 2;

                vec3 intercept = buildTangent(
                    quaternion(interconnectForward, outward),
                    vertex,
                    self.position,
                    self.radius
                );

                line.b.position = intercept;
                line.b.normal = (self.position - intercept).normalize();
                line.b.color = selfColor;

                lines.push_back(line);
            }
        }
    }

    return lines;
}

static void traceLeftLoop(
    const vector<VertexData>& vertices,
    const vector<vector<i64>>& vertexConnections,
    const vector<LineIndices>& lineIndices,
    const LineIndices& line,
    vector<i64>* loop,
    f64* angle
)
{
    if (line.b == loop->front())
    {
        return;
    }

    loop->push_back(line.b);

    vec3 lineDirection = (vertices[line.b].position - vertices[line.a].position).normalize();

    f64 bestAngle = numeric_limits<f64>::max();
    i64 bestCandidateIndex = -1;

    for (i64 candidateIndex : vertexConnections[line.b])
    {
        if (candidateIndex == line.a)
        {
            continue;
        }

        vec3 candidateDirection = (vertices[candidateIndex].position - vertices[line.b].position).normalize();

        f64 angle = handedAngleBetween(lineDirection.toVec2(), candidateDirection.toVec2());

        if (angle < bestAngle)
        {
            bestAngle = angle;
            bestCandidateIndex = candidateIndex;
        }
    }

    *angle += bestAngle;

    assert(bestCandidateIndex >= 0);

    traceLeftLoop(vertices, vertexConnections, lineIndices, { line.b, bestCandidateIndex }, loop, angle);
}

static void traceRightLoop(
    const vector<VertexData>& vertices,
    const vector<vector<i64>>& vertexConnections,
    const vector<LineIndices>& lineIndices,
    const LineIndices& line,
    vector<i64>* loop,
    f64* angle
)
{
    if (line.b == loop->front())
    {
        return;
    }

    loop->push_back(line.b);

    vec3 lineDirection = (vertices[line.b].position - vertices[line.a].position).normalize();

    f64 bestAngle = numeric_limits<f64>::lowest();
    i64 bestCandidateIndex = -1;

    for (i64 candidateIndex : vertexConnections[line.b])
    {
        if (candidateIndex == line.a)
        {
            continue;
        }

        vec3 candidateDirection = (vertices[candidateIndex].position - vertices[line.b].position).normalize();

        f64 angle = handedAngleBetween(lineDirection.toVec2(), candidateDirection.toVec2());

        if (angle > bestAngle)
        {
            bestAngle = angle;
            bestCandidateIndex = candidateIndex;
        }
    }

    *angle += bestAngle;

    assert(bestCandidateIndex >= 0);

    traceRightLoop(vertices, vertexConnections, lineIndices, { line.b, bestCandidateIndex }, loop, angle);
}

static void flipAndRotateLoop(vector<i64>* loop)
{
    // Flip
    i64 smallest = numeric_limits<i64>::max();
    i64 smallestIndex = -1;

    for (i64 i = 0; i < static_cast<i64>(loop->size()); i++)
    {
        i64 v = (*loop)[i];

        if (v < smallest)
        {
            smallest = v;
            smallestIndex = i;
        }
    }
    assert(smallest >= 0);

    i64 previous;
    i64 next;

    if (smallestIndex == 0)
    {
        previous = loop->back();
        next = (*loop)[smallestIndex + 1];
    }
    else if (smallestIndex + 1 == static_cast<i64>(loop->size()))
    {
        previous = (*loop)[smallestIndex - 1];
        next = loop->front();
    }
    else
    {
        previous = (*loop)[smallestIndex - 1];
        next = (*loop)[smallestIndex + 1];
    }

    if (previous > next)
    {
        reverse(loop->begin(), loop->end());
        smallestIndex = (loop->size() - 1) - smallestIndex;
    }

    // Rotate
    if (smallestIndex == 0)
    {
        return;
    }

    *loop = join(
        vector<i64>(loop->begin() + smallestIndex, loop->end()),
        vector<i64>(loop->begin(), loop->begin() + smallestIndex)
    );
}

TerrainMesh buildTerrain(const EmissiveColor& color, const vector<TerrainNode>& nodes)
{
    map<TerrainNodeID, TerrainNode> nodeLookup;

    for (const TerrainNode& node : nodes)
    {
        nodeLookup.insert_or_assign(node.id, node);
    }

    vector<LineData> lines;

    fvec4 baseColor = color.toVec4();

    for (const TerrainNode& node : nodes)
    {
        vector<LineData> nodeLines = buildNode(baseColor, node, nodeLookup);

        lines.insert(lines.end(), nodeLines.begin(), nodeLines.end());
    }

    vector<VertexData> vertices;
    vertices.reserve(lines.size() * 2);

    vector<vector<i64>> vertexConnections;
    vertexConnections.reserve(lines.size() * 2);

    vector<LineIndices> lineIndices;
    lineIndices.reserve(lines.size());

    for (const LineData& line : lines)
    {
        i64 aIndex = -1;
        i64 bIndex = -1;

        for (size_t i = 0; i < vertices.size(); i++)
        {
            const VertexData& vertex = vertices[i];

            if (roughly(vertex.position.distance(line.a.position), 0.0))
            {
                aIndex = i;

                if (bIndex >= 0)
                {
                    break;
                }
            }

            if (roughly(vertex.position.distance(line.b.position), 0.0))
            {
                bIndex = i;

                if (aIndex >= 0)
                {
                    break;
                }
            }
        }

        if (aIndex < 0)
        {
            aIndex = vertices.size();
            vertices.push_back(line.a);
            vertexConnections.push_back({});
            vertexConnections.back().reserve(lines.size());
        }

        if (bIndex < 0)
        {
            bIndex = vertices.size();
            vertices.push_back(line.b);
            vertexConnections.push_back({});
            vertexConnections.back().reserve(lines.size());
        }

        lineIndices.push_back({ aIndex, bIndex });
        vertexConnections[aIndex].push_back(bIndex);
        vertexConnections[bIndex].push_back(aIndex);
    }

    set<vector<i64>> loops;

    for (const LineIndices& line : lineIndices)
    {
        vector<i64> leftLoop = { line.a };
        f64 leftAngle = 0;
        traceLeftLoop(vertices, vertexConnections, lineIndices, line, &leftLoop, &leftAngle);
        flipAndRotateLoop(&leftLoop);

        if (leftAngle <= 0)
        {
            loops.insert(leftLoop);
        }

        vector<i64> rightLoop = { line.a };
        f64 rightAngle = 0;
        traceRightLoop(vertices, vertexConnections, lineIndices, line, &rightLoop, &rightAngle);
        flipAndRotateLoop(&rightLoop);

        if (rightAngle >= 0)
        {
            loops.insert(rightLoop);
        }
    }

    TerrainMesh mesh;

    for (const VertexData& vertex : vertices)
    {
        mesh.vertices.push_back({ vertex.position, vertex.normal, vertex.color });
    }

    for (const vector<i64>& loop : loops)
    {
        vector<vec2> points;
        points.reserve(loop.size());

        for (i64 i : loop)
        {
            points.push_back(vertices[i].position.toVec2());
        }

        for (u32 i : triangulatePolygon(points))
        {
            mesh.indices.push_back(loop[i]);
        }
    }

    return mesh;
}
