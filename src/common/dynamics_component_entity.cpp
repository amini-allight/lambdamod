/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_component_entity.hpp"
#include "dynamics.hpp"
#include "dynamics_component_part.hpp"
#include "entity.hpp"

DynamicsComponentEntity::DynamicsComponentEntity(
    Dynamics* dynamics,
    DynamicsComponent* parent,
    Entity* entity
)
    : DynamicsComponent(dynamics, parent, entity)
{
    createShape(entity->globalScale());

    for (const auto& [ id, part ] : entity->body().parts())
    {
        if (part->constraintType() != Body_Constraint_Fixed)
        {
            continue;
        }

        addToShape(part);
    }
    
    createBody(
        entity->globalPosition(),
        entity->globalRotation(),
        entity->mass() - entity->body().independentPartMass()
    );

    if (parent)
    {
        createConstraint();
    }

    createChildren();

    lastGlobalPosition = entity->globalPosition();
    lastGlobalRotation = entity->globalRotation();
    lastLinearVelocity = entity->linearVelocity();
    lastAngularVelocity = entity->angularVelocity();
}

DynamicsComponentEntity::~DynamicsComponentEntity()
{
    destroyChildren();

    if (parent)
    {
        destroyConstraint();
    }

    destroyBody();

    delete shape;
}

void DynamicsComponentEntity::applyIKEffect(const vec3& translation, const quaternion& rotation)
{
    vec3 globalPosition = translation + globalTransform().position();
    quaternion globalRotation = rotation * globalTransform().rotation();

    btTransform transform = body->getWorldTransform();

    transform.setOrigin(btVector3(globalPosition.x, globalPosition.y, globalPosition.z));
    transform.setRotation(btQuaternion(globalRotation.x, globalRotation.y, globalRotation.z, globalRotation.w));

    body->setWorldTransform(transform);
}

void DynamicsComponentEntity::writeback()
{
    if (!parent)
    {
        if (entity->globalPosition() == lastGlobalPosition && entity->globalRotation() == lastGlobalRotation)
        {
            entity->setGlobalPosition(globalTransform().position());
            entity->setGlobalRotation(globalTransform().rotation());
            lastGlobalPosition = entity->globalPosition();
            lastGlobalRotation = entity->globalRotation();
        }

        if (entity->linearVelocity() == lastLinearVelocity)
        {
            entity->setLinearVelocity(linearVelocity());
            lastLinearVelocity = entity->linearVelocity();
        }

        if (entity->angularVelocity() == lastAngularVelocity)
        {
            entity->setAngularVelocity(angularVelocity());
            lastAngularVelocity = entity->angularVelocity();
        }
    }

    DynamicsComponent::writeback();
}

bool DynamicsComponentEntity::supportsAnimation() const
{
    return !parent;
}

void DynamicsComponentEntity::createConstraint()
{
    mat4 constraintTransform = globalTransform();

    constraint = new btGeneric6DofSpring2Constraint(
        *parent->body,
        *body,
        parent->inBodySpace(constraintTransform),
        inBodySpace(constraintTransform)
    );
    dynamics->world()->addConstraint(constraint);

    constraint->setLinearLowerLimit(btVector3(0, 0, 0));
    constraint->setLinearUpperLimit(btVector3(0, 0, 0));
    constraint->setAngularLowerLimit(btVector3(0, 0, 0));
    constraint->setAngularUpperLimit(btVector3(0, 0, 0));
}

void DynamicsComponentEntity::createChildren()
{
    set<BodyPartID> includedPartIDs;

    for (const auto& [ id, part ] : entity->body().parts())
    {
        set<BodyPartID> ids = DynamicsComponent::createChildren(part);
        includedPartIDs.insert(ids.begin(), ids.end());
    }

    for (const auto& [ id, child ] : entity->children())
    {
        if (child->parentPartID() != BodyPartID() && !includedPartIDs.contains(child->parentPartID()))
        {
            continue;
        }

        children.push_back(new DynamicsComponentEntity(dynamics, this, child));
    }
}
