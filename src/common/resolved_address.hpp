/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "address.hpp"

#if defined(__unix__) || defined(__APPLE__)
#include <netdb.h>
#else
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>
#include <ws2tcpip.h>
#endif

class ResolvedAddress
{
public:
    explicit ResolvedAddress(const Address& address);
    ResolvedAddress(const ResolvedAddress& rhs) = delete;
    ResolvedAddress(ResolvedAddress&& rhs) = delete;
    ~ResolvedAddress();

    ResolvedAddress& operator=(const ResolvedAddress& rhs) = delete;
    ResolvedAddress& operator=(ResolvedAddress&& rhs) = delete;

    struct addrinfo* info;
};
