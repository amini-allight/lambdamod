/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sky_parameters.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

string skyBaseTypeToString(SkyBaseType type)
{
    switch (type)
    {
    case Sky_Base_Blank :
        return "sky-base-blank";
    case Sky_Base_Unidirectional :
        return "sky-base-unidirectional";
    case Sky_Base_Bidirectional :
        return "sky-base-bidirectional";
    case Sky_Base_Omnidirectional :
        return "sky-base-omnidirectional";
    default :
        fatal("Encountered unknown sky base type '" + to_string(type) + "'.");
    }
}

SkyBaseType skyBaseTypeFromString(const string& s)
{
    if (s == "sky-base-blank")
    {
        return Sky_Base_Blank;
    }
    else if (s == "sky-base-unidirectional")
    {
        return Sky_Base_Unidirectional;
    }
    else if (s == "sky-base-bidirectional")
    {
        return Sky_Base_Bidirectional;
    }
    else if (s == "sky-base-omnidirectional")
    {
        return Sky_Base_Omnidirectional;
    }
    else
    {
        fatal("Encountered unknown sky base type '" + s + "'.");
    }
}

template<>
SkyBaseType YAMLNode::convert() const
{
    return skyBaseTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(SkyBaseType v)
{
    emit(skyBaseTypeToString(v));
}

SkyBaseBlank::SkyBaseBlank()
{

}

void SkyBaseBlank::verify()
{
    // Do nothing
}

bool SkyBaseBlank::operator==(const SkyBaseBlank& rhs) const
{
    return true;
}

bool SkyBaseBlank::operator!=(const SkyBaseBlank& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyBaseBlank YAMLNode::convert() const
{
    return SkyBaseBlank();
}

template<>
void YAMLSerializer::emit(SkyBaseBlank v)
{
    startMapping();

    endMapping();
}

SkyBaseUnidirectional::SkyBaseUnidirectional()
{
    horizonColor = HDRColor(vec3(1), 1);
    zenithColor = HDRColor(vec3(1), 1);
}

void SkyBaseUnidirectional::verify()
{
    horizonColor.verify();
    zenithColor.verify();
}

bool SkyBaseUnidirectional::operator==(const SkyBaseUnidirectional& rhs) const
{
    return horizonColor == rhs.horizonColor && zenithColor == rhs.zenithColor;
}

bool SkyBaseUnidirectional::operator!=(const SkyBaseUnidirectional& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyBaseUnidirectional YAMLNode::convert() const
{
    SkyBaseUnidirectional unidirectional;

    unidirectional.horizonColor = at("horizonColor").as<HDRColor>();
    unidirectional.zenithColor = at("zenithColor").as<HDRColor>();

    return unidirectional;
}

template<>
void YAMLSerializer::emit(SkyBaseUnidirectional v)
{
    startMapping();

    emitPair("horizonColor", v.horizonColor);
    emitPair("zenithColor", v.zenithColor);

    endMapping();
}

SkyBaseBidirectional::SkyBaseBidirectional()
{
    horizonColor = HDRColor(vec3(1), 1);
    lowerZenithColor = HDRColor(vec3(1), 1);
    upperZenithColor = HDRColor(vec3(1), 1);
}

void SkyBaseBidirectional::verify()
{
    horizonColor.verify();
    lowerZenithColor.verify();
    upperZenithColor.verify();
}

bool SkyBaseBidirectional::operator==(const SkyBaseBidirectional& rhs) const
{
    return horizonColor == rhs.horizonColor &&
        lowerZenithColor == rhs.lowerZenithColor &&
        upperZenithColor == rhs.upperZenithColor;
}

bool SkyBaseBidirectional::operator!=(const SkyBaseBidirectional& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyBaseBidirectional YAMLNode::convert() const
{
    SkyBaseBidirectional bidirectional;

    bidirectional.horizonColor = at("horizonColor").as<HDRColor>();
    bidirectional.lowerZenithColor = at("lowerZenithColor").as<HDRColor>();
    bidirectional.upperZenithColor = at("upperZenithColor").as<HDRColor>();

    return bidirectional;
}

template<>
void YAMLSerializer::emit(SkyBaseBidirectional v)
{
    startMapping();

    emitPair("horizonColor", v.horizonColor);
    emitPair("lowerZenithColor", v.lowerZenithColor);
    emitPair("upperZenithColor", v.upperZenithColor);

    endMapping();
}

SkyBaseOmnidirectional::SkyBaseOmnidirectional()
{
    color = HDRColor(vec3(1), 1);
}

void SkyBaseOmnidirectional::verify()
{
    color.verify();
}

bool SkyBaseOmnidirectional::operator==(const SkyBaseOmnidirectional& rhs) const
{
    return color == rhs.color;
}

bool SkyBaseOmnidirectional::operator!=(const SkyBaseOmnidirectional& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyBaseOmnidirectional YAMLNode::convert() const
{
    SkyBaseOmnidirectional omnidirectional;

    omnidirectional.color = at("color").as<HDRColor>();

    return omnidirectional;
}

template<>
void YAMLSerializer::emit(SkyBaseOmnidirectional v)
{
    startMapping();

    emitPair("color", v.color);

    endMapping();
}

SkyBase::SkyBase()
{
    type = Sky_Base_Blank;
    data = SkyBaseBlank();
}

void SkyBase::verify()
{
    if (auto blank = get_if<SkyBaseBlank>(&data); blank && type == Sky_Base_Blank)
    {
        blank->verify();
    }
    else if (auto unidirectional = get_if<SkyBaseUnidirectional>(&data); unidirectional && type == Sky_Base_Unidirectional)
    {
        unidirectional->verify();
    }
    else if (auto bidirectional = get_if<SkyBaseBidirectional>(&data); bidirectional && type == Sky_Base_Bidirectional)
    {
        bidirectional->verify();
    }
    else if (auto omnidirectional = get_if<SkyBaseOmnidirectional>(&data); omnidirectional && type == Sky_Base_Omnidirectional)
    {
        omnidirectional->verify();
    }
    else
    {
        type = Sky_Base_Blank;
        data = SkyBaseBlank();
    }
}

bool SkyBase::operator==(const SkyBase& rhs) const
{
    if (type != rhs.type)
    {
        return false;
    }

    switch (type)
    {
    case Sky_Base_Blank :
        return get<SkyBaseBlank>() == rhs.get<SkyBaseBlank>();
    case Sky_Base_Unidirectional :
        return get<SkyBaseUnidirectional>() == rhs.get<SkyBaseUnidirectional>();
    case Sky_Base_Bidirectional :
        return get<SkyBaseBidirectional>() == rhs.get<SkyBaseBidirectional>();
    case Sky_Base_Omnidirectional :
        return get<SkyBaseOmnidirectional>() == rhs.get<SkyBaseOmnidirectional>();
    }

    return true;
}

bool SkyBase::operator!=(const SkyBase& rhs) const
{
    return !(*this == rhs);
}

string SkyBase::dataToString() const
{
    stringstream ss;

    switch (type)
    {
    case Sky_Base_Blank :
        msgpack::pack(ss, get<SkyBaseBlank>());
        break;
    case Sky_Base_Unidirectional :
        msgpack::pack(ss, get<SkyBaseUnidirectional>());
        break;
    case Sky_Base_Bidirectional :
        msgpack::pack(ss, get<SkyBaseBidirectional>());
        break;
    case Sky_Base_Omnidirectional :
        msgpack::pack(ss, get<SkyBaseOmnidirectional>());
        break;
    }

    return ss.str();
}

void SkyBase::dataFromString(const string& s)
{
    switch (type)
    {
    case Sky_Base_Blank :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyBaseBlank part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Base_Unidirectional :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyBaseUnidirectional part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Base_Bidirectional :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyBaseBidirectional part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Base_Omnidirectional :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyBaseOmnidirectional part;
        oh.get().convert(part);

        data = part;
        break;
    }
    }
}

template<>
SkyBase YAMLNode::convert() const
{
    SkyBase base;

    base.type = skyBaseTypeFromString(at("type").as<string>());

    switch (base.type)
    {
    case Sky_Base_Blank :
        base.data = at("data").as<SkyBaseBlank>();
        break;
    case Sky_Base_Unidirectional :
        base.data = at("data").as<SkyBaseUnidirectional>();
        break;
    case Sky_Base_Bidirectional :
        base.data = at("data").as<SkyBaseBidirectional>();
        break;
    case Sky_Base_Omnidirectional :
        base.data = at("data").as<SkyBaseOmnidirectional>();
        break;
    }

    return base;
}

template<>
void YAMLSerializer::emit(SkyBase v)
{
    startMapping();

    emitPair("type", skyBaseTypeToString(v.type));

    switch (v.type)
    {
    case Sky_Base_Blank :
        emitPair("data", get<SkyBaseBlank>(v.data));
        break;
    case Sky_Base_Unidirectional :
        emitPair("data", get<SkyBaseUnidirectional>(v.data));
        break;
    case Sky_Base_Bidirectional :
        emitPair("data", get<SkyBaseBidirectional>(v.data));
        break;
    case Sky_Base_Omnidirectional :
        emitPair("data", get<SkyBaseOmnidirectional>(v.data));
        break;
    }

    endMapping();
}

string skyLayerTypeToString(SkyLayerType type)
{
    switch (type)
    {
    case Sky_Layer_Stars :
        return "sky-layer-stars";
    case Sky_Layer_Circle :
        return "sky-layer-circle";
    case Sky_Layer_Cloud :
        return "sky-layer-cloud";
    case Sky_Layer_Aurora :
        return "sky-layer-aurora";
    case Sky_Layer_Comet :
        return "sky-layer-comet";
    case Sky_Layer_Vortex :
        return "sky-layer-vortex";
    case Sky_Layer_Meteor_Shower :
        return "sky-layer-meteor-shower";
    default :
        fatal("Encountered unknown sky layer type '" + to_string(type) + "'.");
    }
}

SkyLayerType skyLayerTypeFromString(const string& s)
{
    if (s == "sky-layer-stars")
    {
        return Sky_Layer_Stars;
    }
    else if (s == "sky-layer-circle")
    {
        return Sky_Layer_Circle;
    }
    else if (s == "sky-layer-cloud")
    {
        return Sky_Layer_Cloud;
    }
    else if (s == "sky-layer-aurora")
    {
        return Sky_Layer_Aurora;
    }
    else if (s == "sky-layer-comet")
    {
        return Sky_Layer_Comet;
    }
    else if (s == "sky-layer-vortex")
    {
        return Sky_Layer_Vortex;
    }
    else if (s == "sky-layer-meteor-shower")
    {
        return Sky_Layer_Meteor_Shower;
    }
    else
    {
        fatal("Encountered unknown sky layer type '" + s + "'.");
    }
}

template<>
SkyLayerType YAMLNode::convert() const
{
    return skyLayerTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(SkyLayerType v)
{
    emit(skyLayerTypeToString(v));
}

SkyLayerStars::SkyLayerStars()
    : density(0.5)
    , color(vec3(1), 1)
{

}

void SkyLayerStars::verify()
{
    density = clamp(density, 0.0, 1.0);
    color.verify();
}

bool SkyLayerStars::operator==(const SkyLayerStars& rhs) const
{
    return density == rhs.density && color == rhs.color;
}

bool SkyLayerStars::operator!=(const SkyLayerStars& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerStars YAMLNode::convert() const
{
    SkyLayerStars stars;

    stars.density = at("density").as<f64>();
    stars.color = at("color").as<HDRColor>();

    return stars;
}

template<>
void YAMLSerializer::emit(SkyLayerStars v)
{
    startMapping();

    emitPair("density", v.density);
    emitPair("color", v.color);

    endMapping();
}

SkyLayerCircle::SkyLayerCircle()
    : size(radians(10))
    , color(HDRColor(vec3(1), 1))
    , phase(0)
{

}

void SkyLayerCircle::verify()
{
    position = { clamp(position.x, -(pi / 2), +(pi / 2)), clamp(position.y, -pi, +pi) };
    size = clamp(size, 0.0, tau);
    color.verify();
    phase = clamp(phase, -pi, +pi);
}

bool SkyLayerCircle::operator==(const SkyLayerCircle& rhs) const
{
    return position == rhs.position &&
        size == rhs.size &&
        color == rhs.color &&
        phase == rhs.phase;
}

bool SkyLayerCircle::operator!=(const SkyLayerCircle& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerCircle YAMLNode::convert() const
{
    SkyLayerCircle circle;

    circle.position = at("position").as<vec2>();
    circle.size = at("size").as<f64>();
    circle.color = at("color").as<HDRColor>();
    circle.phase = at("phase").as<f64>();

    return circle;
}

template<>
void YAMLSerializer::emit(SkyLayerCircle v)
{
    startMapping();

    emitPair("position", v.position);
    emitPair("size", v.size);
    emitPair("color", v.color);
    emitPair("phase", v.phase);

    endMapping();
}

SkyLayerCloud::SkyLayerCloud()
    : density(0.5)
    , size(radians(10))
    , innerColor(vec3(1), 1)
    , outerColor(vec3(1), 1)
{

}

void SkyLayerCloud::verify()
{
    density = clamp(density, 0.0, 1.0);
    position = { clamp(position.x, -(pi / 2), +(pi / 2)), clamp(position.y, -pi, +pi) };
    size = { clamp(size.x, 0.0, pi), clamp(size.y, 0.0, pi / 2) };
    innerColor.verify();
    outerColor.verify();
}

bool SkyLayerCloud::operator==(const SkyLayerCloud& rhs) const
{
    return density == rhs.density &&
        position == rhs.position &&
        size == rhs.size &&
        innerColor == rhs.innerColor &&
        outerColor == rhs.outerColor;
}

bool SkyLayerCloud::operator!=(const SkyLayerCloud& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerCloud YAMLNode::convert() const
{
    SkyLayerCloud cloud;

    cloud.density = at("density").as<f64>();
    cloud.position = at("position").as<vec2>();
    cloud.size = at("size").as<vec2>();
    cloud.innerColor = at("innerColor").as<HDRColor>();
    cloud.outerColor = at("outerColor").as<HDRColor>();

    return cloud;
}

template<>
void YAMLSerializer::emit(SkyLayerCloud v)
{
    startMapping();

    emitPair("density", v.density);
    emitPair("position", v.position);
    emitPair("size", v.size);
    emitPair("innerColor", v.innerColor);
    emitPair("outerColor", v.outerColor);

    endMapping();
}

SkyLayerAurora::SkyLayerAurora()
    : density(0.5)
{

}

void SkyLayerAurora::verify()
{
    density = clamp(density, 0.0, 1.0);
    lowerColor.verify();
    upperColor.verify();
}

bool SkyLayerAurora::operator==(const SkyLayerAurora& rhs) const
{
    return density == rhs.density &&
        lowerColor == rhs.lowerColor &&
        upperColor == rhs.upperColor;
}

bool SkyLayerAurora::operator!=(const SkyLayerAurora& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerAurora YAMLNode::convert() const
{
    SkyLayerAurora aurora;

    aurora.density = at("density").as<f64>();
    aurora.lowerColor = at("lowerColor").as<HDRColor>();
    aurora.upperColor = at("upperColor").as<HDRColor>();

    return aurora;
}

template<>
void YAMLSerializer::emit(SkyLayerAurora v)
{
    startMapping();

    emitPair("density", v.density);
    emitPair("lowerColor", v.lowerColor);
    emitPair("upperColor", v.upperColor);

    endMapping();
}

SkyLayerComet::SkyLayerComet()
    : position()
    , size(radians(1))
    , length(radians(15))
    , rotation(radians(45))
    , headColor(vec3(1), 1)
    , tailColor(vec3(1), 1)
{

}

void SkyLayerComet::verify()
{

    headColor.verify();
    tailColor.verify();
}

bool SkyLayerComet::operator==(const SkyLayerComet& rhs) const
{
    return position == rhs.position &&
        size == rhs.size &&
        length == rhs.length &&
        rotation == rhs.rotation &&
        headColor == rhs.headColor &&
        tailColor == rhs.tailColor;
}

bool SkyLayerComet::operator!=(const SkyLayerComet& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerComet YAMLNode::convert() const
{
    SkyLayerComet comet;

    comet.position = at("position").as<vec2>();
    comet.size = at("size").as<f64>();
    comet.length = at("length").as<f64>();
    comet.rotation = at("rotation").as<f64>();
    comet.headColor = at("headColor").as<HDRColor>();
    comet.tailColor = at("tailColor").as<HDRColor>();

    return comet;
}

template<>
void YAMLSerializer::emit(SkyLayerComet v)
{
    startMapping();

    emitPair("position", v.position);
    emitPair("size", v.size);
    emitPair("length", v.length);
    emitPair("rotation", v.rotation);
    emitPair("headColor", v.headColor);
    emitPair("tailColor", v.tailColor);

    endMapping();
}

SkyLayerVortex::SkyLayerVortex()
    : position(vec2())
    , rotationSpeed(tau / 10)
    , radius(100)
    , innerColor(vec3(1), 1)
    , outerColor(vec3(1), 1)
{

}

void SkyLayerVortex::verify()
{
    radius = max(0.0, radius);
    innerColor.verify();
    outerColor.verify();
}

bool SkyLayerVortex::operator==(const SkyLayerVortex& rhs) const
{
    return position == rhs.position &&
        rotationSpeed == rhs.rotationSpeed &&
        radius == rhs.radius &&
        innerColor == rhs.innerColor &&
        outerColor == rhs.outerColor;
}

bool SkyLayerVortex::operator!=(const SkyLayerVortex& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerVortex YAMLNode::convert() const
{
    SkyLayerVortex vortex;

    vortex.position = at("position").as<vec2>();
    vortex.rotationSpeed = at("rotationSpeed").as<f64>();
    vortex.radius = at("radius").as<f64>();
    vortex.innerColor = at("innerColor").as<HDRColor>();
    vortex.outerColor = at("outerColor").as<HDRColor>();

    return vortex;
}

template<>
void YAMLSerializer::emit(SkyLayerVortex v)
{
    startMapping();

    emitPair("position", v.position);
    emitPair("rotationSpeed", v.rotationSpeed);
    emitPair("radius", v.radius);
    emitPair("innerColor", v.innerColor);
    emitPair("outerColor", v.outerColor);

    endMapping();
}

SkyLayerMeteorShower::SkyLayerMeteorShower()
    : density(0.5)
    , color(vec3(1), 1)
{

}

void SkyLayerMeteorShower::verify()
{
    density = clamp(density, 0.0, 1.0);
    color.verify();
}

bool SkyLayerMeteorShower::operator==(const SkyLayerMeteorShower& rhs) const
{
    return density == rhs.density && color == rhs.color;
}

bool SkyLayerMeteorShower::operator!=(const SkyLayerMeteorShower& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyLayerMeteorShower YAMLNode::convert() const
{
    SkyLayerMeteorShower meteorShower;

    meteorShower.density = at("density").as<f64>();
    meteorShower.color = at("color").as<HDRColor>();

    return meteorShower;
}

template<>
void YAMLSerializer::emit(SkyLayerMeteorShower v)
{
    startMapping();

    emitPair("density", v.density);
    emitPair("color", v.color);

    endMapping();
}

SkyLayer::SkyLayer()
    : type(Sky_Layer_Stars)
    , data(SkyLayerStars())
{

}

SkyLayer::SkyLayer(const SkyLayerStars& data)
    : type(Sky_Layer_Stars)
    , data(data)
{

}

SkyLayer::SkyLayer(const SkyLayerCircle& data)
    : type(Sky_Layer_Circle)
    , data(data)
{

}

SkyLayer::SkyLayer(const SkyLayerCloud& data)
    : type(Sky_Layer_Cloud)
    , data(data)
{

}

SkyLayer::SkyLayer(const SkyLayerAurora& data)
    : type(Sky_Layer_Aurora)
    , data(data)
{

}

SkyLayer::SkyLayer(const SkyLayerComet& data)
    : type(Sky_Layer_Comet)
    , data(data)
{

}

SkyLayer::SkyLayer(const SkyLayerVortex& data)
    : type(Sky_Layer_Vortex)
    , data(data)
{

}

SkyLayer::SkyLayer(const SkyLayerMeteorShower& data)
    : type(Sky_Layer_Meteor_Shower)
    , data(data)
{

}

void SkyLayer::verify()
{
    if (auto stars = get_if<SkyLayerStars>(&data); stars && type == Sky_Layer_Stars)
    {
        stars->verify();
    }
    else if (auto circle = get_if<SkyLayerCircle>(&data); circle && type == Sky_Layer_Circle)
    {
        circle->verify();
    }
    else if (auto cloud = get_if<SkyLayerCloud>(&data); cloud && type == Sky_Layer_Cloud)
    {
        cloud->verify();
    }
    else if (auto aurora = get_if<SkyLayerAurora>(&data); aurora && type == Sky_Layer_Aurora)
    {
        aurora->verify();
    }
    else if (auto comet = get_if<SkyLayerComet>(&data); comet && type == Sky_Layer_Comet)
    {
        comet->verify();
    }
    else if (auto vortex = get_if<SkyLayerVortex>(&data); vortex && type == Sky_Layer_Vortex)
    {
        vortex->verify();
    }
    else if (auto meteorShower = get_if<SkyLayerMeteorShower>(&data); meteorShower && type == Sky_Layer_Meteor_Shower)
    {
        meteorShower->verify();
    }
    else
    {
        type = Sky_Layer_Stars;
        data = SkyLayerStars();
    }
}

void SkyLayer::setType(SkyLayerType type)
{
    this->type = type;

    switch (type)
    {
    case Sky_Layer_Stars :
        data = SkyLayerStars();
        break;
    case Sky_Layer_Circle :
        data = SkyLayerCircle();
        break;
    case Sky_Layer_Cloud :
        data = SkyLayerCloud();
        break;
    case Sky_Layer_Aurora :
        data = SkyLayerAurora();
        break;
    case Sky_Layer_Comet :
        data = SkyLayerComet();
        break;
    case Sky_Layer_Vortex :
        data = SkyLayerVortex();
        break;
    case Sky_Layer_Meteor_Shower :
        data = SkyLayerMeteorShower();
        break;
    }
}

bool SkyLayer::operator==(const SkyLayer& rhs) const
{
    if (type != rhs.type)
    {
        return false;
    }

    switch (type)
    {
    case Sky_Layer_Stars :
        return get<SkyLayerStars>() == rhs.get<SkyLayerStars>();
    case Sky_Layer_Circle :
        return get<SkyLayerCircle>() == rhs.get<SkyLayerCircle>();
    case Sky_Layer_Cloud :
        return get<SkyLayerCloud>() == rhs.get<SkyLayerCloud>();
    case Sky_Layer_Aurora :
        return get<SkyLayerAurora>() == rhs.get<SkyLayerAurora>();
    case Sky_Layer_Comet :
        return get<SkyLayerComet>() == rhs.get<SkyLayerComet>();
    case Sky_Layer_Vortex :
        return get<SkyLayerVortex>() == rhs.get<SkyLayerVortex>();
    case Sky_Layer_Meteor_Shower :
        return get<SkyLayerMeteorShower>() == rhs.get<SkyLayerMeteorShower>();
    }

    return true;
}

bool SkyLayer::operator!=(const SkyLayer& rhs) const
{
    return !(*this == rhs);
}

string SkyLayer::dataToString() const
{
    stringstream ss;

    switch (type)
    {
    case Sky_Layer_Stars :
        msgpack::pack(ss, get<SkyLayerStars>());
        break;
    case Sky_Layer_Circle :
        msgpack::pack(ss, get<SkyLayerCircle>());
        break;
    case Sky_Layer_Cloud :
        msgpack::pack(ss, get<SkyLayerCloud>());
        break;
    case Sky_Layer_Aurora :
        msgpack::pack(ss, get<SkyLayerAurora>());
        break;
    case Sky_Layer_Comet :
        msgpack::pack(ss, get<SkyLayerComet>());
        break;
    case Sky_Layer_Vortex :
        msgpack::pack(ss, get<SkyLayerVortex>());
        break;
    case Sky_Layer_Meteor_Shower :
        msgpack::pack(ss, get<SkyLayerMeteorShower>());
        break;
    }

    return ss.str();
}

void SkyLayer::dataFromString(const string& s)
{
    switch (type)
    {
    case Sky_Layer_Stars :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerStars part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Layer_Circle :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerCircle part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Layer_Cloud :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerCloud part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Layer_Aurora :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerAurora part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Layer_Comet :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerComet part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Layer_Vortex :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerVortex part;
        oh.get().convert(part);

        data = part;
        break;
    }
    case Sky_Layer_Meteor_Shower :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        SkyLayerMeteorShower part;
        oh.get().convert(part);

        data = part;
        break;
    }
    }
}

template<>
SkyLayer YAMLNode::convert() const
{
    SkyLayer layer;

    layer.type = skyLayerTypeFromString(at("type").as<string>());
    
    switch (layer.type)
    {
    case Sky_Layer_Stars :
        layer.data = at("data").as<SkyLayerStars>();
        break;
    case Sky_Layer_Circle :
        layer.data = at("data").as<SkyLayerCircle>();
        break;
    case Sky_Layer_Cloud :
        layer.data = at("data").as<SkyLayerCloud>();
        break;
    case Sky_Layer_Aurora :
        layer.data = at("data").as<SkyLayerAurora>();
        break;
    case Sky_Layer_Comet :
        layer.data = at("data").as<SkyLayerComet>();
        break;
    case Sky_Layer_Vortex :
        layer.data = at("data").as<SkyLayerVortex>();
        break;
    case Sky_Layer_Meteor_Shower :
        layer.data = at("data").as<SkyLayerMeteorShower>();
        break;
    }

    return layer;
}

template<>
void YAMLSerializer::emit(SkyLayer v)
{
    startMapping();

    emitPair("type", skyLayerTypeToString(v.type));
    
    switch (v.type)
    {
    case Sky_Layer_Stars :
        emitPair("data", get<SkyLayerStars>(v.data));
        break;
    case Sky_Layer_Circle :
        emitPair("data", get<SkyLayerCircle>(v.data));
        break;
    case Sky_Layer_Cloud :
        emitPair("data", get<SkyLayerCloud>(v.data));
        break;
    case Sky_Layer_Aurora :
        emitPair("data", get<SkyLayerAurora>(v.data));
        break;
    case Sky_Layer_Comet :
        emitPair("data", get<SkyLayerComet>(v.data));
        break;
    case Sky_Layer_Vortex :
        emitPair("data", get<SkyLayerVortex>(v.data));
        break;
    case Sky_Layer_Meteor_Shower :
        emitPair("data", get<SkyLayerMeteorShower>(v.data));
        break;
    }

    endMapping();
}

SkyParameters::SkyParameters()
{
    SkyBaseUnidirectional unidirectional;
    unidirectional.horizonColor = HDRColor(defaultSkyHorizonColor, defaultSkyBrightness);
    unidirectional.zenithColor = HDRColor(defaultSkyZenithColor, defaultSkyBrightness);

    base.type = Sky_Base_Unidirectional;
    base.data = unidirectional;
    enabled = true;
    seed = 0;
}

void SkyParameters::verify()
{
    base.verify();

    for (SkyLayer& layer : layers)
    {
        layer.verify();
    }
}

bool SkyParameters::operator==(const SkyParameters& rhs) const
{
    return enabled == rhs.enabled &&
        seed == rhs.seed &&
        base == rhs.base &&
        layers == rhs.layers;
}

bool SkyParameters::operator!=(const SkyParameters& rhs) const
{
    return !(*this == rhs);
}

template<>
SkyParameters YAMLNode::convert() const
{
    SkyParameters parameters;

    parameters.enabled = at("enabled").as<bool>();
    parameters.seed = at("seed").as<u64>();
    parameters.base = at("base").as<SkyBase>();
    parameters.layers = at("layers").as<vector<SkyLayer>>();

    return parameters;
}

template<>
void YAMLSerializer::emit(SkyParameters v)
{
    startMapping();

    emitPair("enabled", v.enabled);
    emitPair("seed", v.seed);
    emitPair("base", v.base);
    emitPair("layers", v.layers);

    endMapping();
}
