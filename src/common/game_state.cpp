/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "game_state.hpp"
#include "tools.hpp"
#include "script_tools.hpp"
#include "yaml_tools.hpp"
#include "world.hpp"
#include "grip_state.hpp"
#include "vr_grip_interactions.hpp"
#include "error_messages.hpp"

#if defined(LMOD_SERVER)
#include "server/global.hpp"
#elif defined(LMOD_CLIENT)
#include "client/global.hpp"
#include "client/localization.hpp"
#endif

#if defined(LMOD_SERVER)
static const map<string, string> localMessages = {
    { "user-set-time-multiplier-to", "%1 set the time multiplier to %2." },
    { "user-locked-attachments", "%1 locked attachments." },
    { "user-unlocked-attachments", "%1 unlocked attachments." },
    { "user-pulled-the-brake", "%1 pulled the brake." },
    { "user-released-the-brake", "%1 released the brake." },
    { "user-changed-their-color-to", "%1 changed their color to %2." },
    { "user-changed-the-color-of-user-to", "%1 changed the color of user '%2' to %3." },
    { "user-kicked-user", "%1 kicked %2." },
    { "user-pinged", "%1 pinged." },
    { "user-set-their-magnitude-control-to", "%1 set their magnitude control to %2." },
    { "user-set-the-magnitude-control-limit-to", "%1 set the magnitude limit to %2." },
    { "user-attached-to-entity", "%1 attached to entity %2 (%3)." },
    { "user-detached-from-entity", "%1 detached from entity %2 (%3)." },
    { "user-joined", "%1 joined." },
    { "user-left", "%1 left." },
    { "magnitude-control-entered", "Magnitude control %1 entered." }
};
#endif

GameState::GameState()
    : _nextUserID(1)
    , _splashMessage("Welcome to the server!")
    , _timeMultiplier(1)
    , _attachmentsLocked(false)
    , _braking(false)
    , brakeElapsed(currentTime().count() / 1000.0)
    , _magnitudeLimit(0)
    , _nextEntityID(1)
    , nextTimerID(1)
    , nextRepeaterID(1)
    , _nextTextID(1)
{
    _context = new ScriptContext(this);
}

GameState::GameState(const GameState& rhs)
{
    copy(rhs);
    _worlds.clear();
    _context = nullptr;

    for (const auto& [ name, world ] : rhs._worlds)
    {
        _worlds.insert({ name, new World(*world) });
    }

    _context = new ScriptContext(this, rhs._context);
}

GameState::GameState(GameState&& rhs) noexcept
{
    copy(rhs);
    _context->game = this;
    rhs._worlds.clear();
    rhs._context = nullptr;
}

GameState::~GameState()
{
    if (_context)
    {
        delete _context;
    }

    for (const auto& [ name, world ] : _worlds)
    {
        delete world;
    }
}

GameState& GameState::operator=(const GameState& rhs)
{
    if (&rhs != this)
    {
        if (_context)
        {
            delete _context;
        }

        for (const auto& [ name, world ] : _worlds)
        {
            delete world;
        }

        copy(rhs);
        _worlds.clear();
        _context = nullptr;

        for (const auto& [ name, world ] : rhs._worlds)
        {
            _worlds.insert({ name, new World(*world) });
        }

        _context = new ScriptContext(this, rhs._context);
    }

    return *this;
}

GameState& GameState::operator=(GameState&& rhs) noexcept
{
    if (&rhs != this)
    {
        if (_context)
        {
            delete _context;
        }

        for (const auto& [ name, world ] : _worlds)
        {
            delete world;
        }

        copy(rhs);
        _context->game = this;
        rhs._worlds.clear();
        rhs._context = nullptr;
    }

    return *this;
}

tuple<vector<NetworkEvent>, vector<EntityUpdateSideEffect>> GameState::compare(const GameState& previous) const
{
    vector<NetworkEvent> events;
    vector<EntityUpdateSideEffect> sideEffects;

    for (const auto& [ id, user ] : previous._users)
    {
        auto it = _users.find(id);

        if (it == _users.end())
        {
            UserEvent event(User_Event_Remove, id);

            events.push_back(event);
        }
    }

    for (const auto& [ id, user ] : _users)
    {
        auto it = previous._users.find(id);

        if (it != previous._users.end())
        {
            vector<NetworkEvent> userEvents = user.compare(it->second);

            events.insert(
                events.end(),
                userEvents.begin(),
                userEvents.end()
            );
        }
        else
        {
            vector<NetworkEvent> userEvents = user.initial();

            events.insert(
                events.end(),
                userEvents.begin(),
                userEvents.end()
            );
        }
    }

    for (const auto& [ name, team ] : previous._teams)
    {
        auto it = _teams.find(name);

        if (it == _teams.end())
        {
            TeamEvent event(Team_Event_Remove, name);

            events.push_back(event);
        }
    }

    for (const auto& [ name, team ] : _teams)
    {
        auto it = previous._teams.find(name);

        if (it != previous._teams.end())
        {
            if (team != it->second)
            {
                events.push_back(TeamEvent(Team_Event_Update, name, team.userIDs));
            }
        }
        else
        {
            events.push_back(TeamEvent(Team_Event_Add, name));
            events.push_back(TeamEvent(Team_Event_Update, name, team.userIDs));
        }
    }

    if (_splashMessage != previous._splashMessage)
    {
        GameEvent event(Game_Event_Splash_Message);
        event.s = _splashMessage;

        events.push_back(event);
    }

    if (_timeMultiplier != previous._timeMultiplier)
    {
        GameEvent event(Game_Event_Time_Multiplier);
        event.f = _timeMultiplier;

        events.push_back(event);
    }

    if (_attachmentsLocked != previous._attachmentsLocked)
    {
        GameEvent event(_attachmentsLocked ? Game_Event_Lock_Attachments : Game_Event_Unlock_Attachments);

        events.push_back(event);
    }

    if (_braking != previous._braking)
    {
        GameEvent event(Game_Event_Brake);
        event.u = _brakingID.value();
        event.b = true;

        events.push_back(event);
    }

    if (_magnitudeLimit != previous._magnitudeLimit)
    {
        GameEvent event(Game_Event_Magnitude_Limit);
        event.u = _magnitudeLimit;

        events.push_back(event);
    }

    if (_nextEntityID != previous._nextEntityID)
    {
        GameEvent event(Game_Event_Next_Entity_ID);
        event.u = _nextEntityID.value();

        events.push_back(event);
    }

    if (nextTimerID != previous.nextTimerID)
    {
        GameEvent event(Game_Event_Next_Timer_ID);
        event.u = nextTimerID.value();

        events.push_back(event);
    }

    if (nextRepeaterID != previous.nextRepeaterID)
    {
        GameEvent event(Game_Event_Next_Repeater_ID);
        event.u = nextRepeaterID.value();

        events.push_back(event);
    }

    for (const auto& [ name, world ] : previous._worlds)
    {
        auto it = _worlds.find(name);

        if (it == _worlds.end())
        {
            WorldEvent event(World_Event_Remove, name);

            events.push_back(event);
        }
    }

    for (const auto& [ name, world ] : _worlds)
    {
        auto it = previous._worlds.find(name);

        if (it != previous._worlds.end())
        {
            WorldUpdate update(world);

            world->compare(*it->second, update);

            events.insert(
                events.end(),
                update.events().begin(),
                update.events().end()
            );

            sideEffects.insert(
                sideEffects.end(),
                update.sideEffects().begin(),
                update.sideEffects().end()
            );
        }
        else
        {
            auto [ worldEvents, worldSideEffects ] = world->initial();

            events.insert(
                events.end(),
                worldEvents.begin(),
                worldEvents.end()
            );

            sideEffects.insert(
                sideEffects.end(),
                worldSideEffects.begin(),
                worldSideEffects.end()
            );
        }
    }

    vector<NetworkEvent> scriptEvents = _context->compare(*previous._context);

    events.insert(
        events.end(),
        scriptEvents.begin(),
        scriptEvents.end()
    );

    return { events, sideEffects };
}

vector<NetworkEvent> GameState::compareUser(const GameState& previous, const User* user) const
{
    return _context->compareUser(*previous._context, user);
}

void GameState::updateDynamics()
{
    for (const auto& [ name, world ] : _worlds)
    {
        world->updateDynamics();
    }
}

#if defined(LMOD_SERVER)
void GameState::push(UserID userID, const NetworkEvent& event)
{
    User* user = getUser(userID);

    if (!user)
    {
        return;
    }

    switch (event.type())
    {
    default :
        error("Encountered unknown event type " + to_string(event.type()) + " received.");
        break;
    case Network_Event_User :
        handleUserEvent(user, event.payload<UserEvent>());
        break;
    case Network_Event_Team :
        handleTeamEvent(user, event.payload<TeamEvent>());
        break;
    case Network_Event_Document :
        handleDocumentEvent(user, event.payload<DocumentEvent>());
        break;
    case Network_Event_Game :
        handleGameEvent(user, event.payload<GameEvent>());
        break;
    case Network_Event_World :
        handleWorldEvent(user, event.payload<WorldEvent>());
        break;
    case Network_Event_World_Sky :
    case Network_Event_World_Atmosphere :
    case Network_Event_Entity :
    case Network_Event_Entity_Body :
    case Network_Event_Entity_Sample :
    case Network_Event_Entity_Sample_Playback :
    case Network_Event_Entity_Action :
    case Network_Event_Entity_Action_Playback :
    case Network_Event_Entity_Input_Binding :
    case Network_Event_Entity_HUD :
    case Network_Event_Entity_Script :
        pushWorldEvent(user, event);
        break;
    case Network_Event_Script :
        _context->push(user, event.payload<ScriptEvent>());
        break;
    case Network_Event_Input :
        handleInputEvent(user, event.payload<InputEvent>());
        break;
    case Network_Event_Grip :
        handleGripEvent(user, event.payload<GripEvent>());
        break;
    case Network_Event_Command :
        handleCommandEvent(user, event.payload<CommandEvent>());
        break;
    case Network_Event_Pose :
        handlePoseEvent(user, event.payload<PoseEvent>());
        break;
    case Network_Event_Text_Request :
        handleTextRequestEvent(user, event.payload<TextRequestEvent>());
        break;
    case Network_Event_Text_Response :
        handleTextResponseEvent(user, event.payload<TextResponseEvent>());
        break;
    case Network_Event_Tether :
        handleTetherEvent(user, event.payload<TetherEvent>());
        break;
    case Network_Event_Confinement :
        handleConfinementEvent(user, event.payload<ConfinementEvent>());
        break;
    case Network_Event_Goto :
        handleGotoEvent(user, event.payload<GotoEvent>());
        break;
    case Network_Event_Fade :
        handleFadeEvent(user, event.payload<FadeEvent>());
        break;
    case Network_Event_Wait :
        handleWaitEvent(user, event.payload<WaitEvent>());
        break;
    case Network_Event_Voice_Channel :
        handleVoiceChannelEvent(user, event.payload<VoiceChannelEvent>());
        break;
    }
}
#elif defined(LMOD_CLIENT)
bool GameState::push(const NetworkEvent& event)
{
    switch (event.type())
    {
    default :
        error("Encountered unknown event type " + to_string(event.type()) + " received.");
        break;
    case Network_Event_User :
        return handleUserEvent(event.payload<UserEvent>());
    case Network_Event_Team :
        handleTeamEvent(event.payload<TeamEvent>());
        break;
    case Network_Event_Game :
        return handleGameEvent(event.payload<GameEvent>());
    case Network_Event_World :
        handleWorldEvent(event.payload<WorldEvent>());
        break;
    case Network_Event_World_Sky :
    case Network_Event_World_Atmosphere :
    case Network_Event_Entity :
    case Network_Event_Entity_Body :
    case Network_Event_Entity_Sample :
    case Network_Event_Entity_Sample_Playback :
    case Network_Event_Entity_Action :
    case Network_Event_Entity_Action_Playback :
    case Network_Event_Entity_Input_Binding :
    case Network_Event_Entity_HUD :
    case Network_Event_Entity_Script :
        pushWorldEvent(event);
        break;
    case Network_Event_Script :
        _context->push(event.payload<ScriptEvent>());
        break;
    }

    return false;
}
#endif

void GameState::step()
{
    for (auto& [ name, world ] : _worlds)
    {
        world->step(_context, stepInterval());
    }

#if defined(LMOD_SERVER)
    for (auto& [ id, user ] : _users)
    {
        user.step(_context, stepInterval());
    }
#endif

    for (auto it = _timers.begin(); it != _timers.end();)
    {
        auto& [ id, timer ] = *it;

        bool done = timer.step(this, stepInterval());

        if (done)
        {
            _timers.erase(it++);
        }
        else
        {
            it++;
        }
    }

    for (const auto& [ id, repeater ] : _repeaters)
    {
        repeater.step(this, stepInterval());
    }

    brakeElapsed += stepInterval();
}

vector<NetworkEvent> GameState::initial() const
{
    vector<NetworkEvent> events;

    for (const auto& [ id, user ] : _users)
    {
        vector<NetworkEvent> userEvents = user.initial();

        events.insert(
            events.end(),
            userEvents.begin(),
            userEvents.end()
        );
    }

    for (const auto& [ name, team ] : _teams)
    {
        events.push_back(TeamEvent(Team_Event_Add, name));
        events.push_back(TeamEvent(Team_Event_Update, name, team.userIDs));
    }

    {
        GameEvent event(Game_Event_Splash_Message);
        event.s = _splashMessage;

        events.push_back(event);
    }

    {
        GameEvent event(Game_Event_Time_Multiplier);
        event.f = _timeMultiplier;

        events.push_back(event);
    }

    events.push_back(NetworkEvent(GameEvent(_attachmentsLocked ? Game_Event_Lock_Attachments : Game_Event_Unlock_Attachments)));

    if (_braking)
    {
        GameEvent event(Game_Event_Brake);
        event.b = false;

        events.push_back(event);
    }

    {
        GameEvent event(Game_Event_Magnitude_Limit);
        event.u = _magnitudeLimit;

        events.push_back(event);
    }

    {
        GameEvent event(Game_Event_Next_Entity_ID);
        event.u = _nextEntityID.value();

        events.push_back(event);
    }

    {
        GameEvent event(Game_Event_Next_Timer_ID);
        event.u = nextTimerID.value();

        events.push_back(event);
    }

    {
        GameEvent event(Game_Event_Next_Repeater_ID);
        event.u = nextRepeaterID.value();

        events.push_back(event);
    }

    for (const auto& [ name, world ] : _worlds)
    {
        auto [ worldEvents, worldSideEffects ] = world->initial();

        events.insert(
            events.end(),
            worldEvents.begin(),
            worldEvents.end()
        );
    }

    vector<NetworkEvent> scriptEvents = _context->initial();

    events.insert(
        events.end(),
        scriptEvents.begin(),
        scriptEvents.end()
    );

    return events;
}

vector<NetworkEvent> GameState::initialUser(const User* user) const
{
    return _context->initialUser(user);
}

#if defined(LMOD_SERVER)
void GameState::executeCommand(User* user, const string& command)
{
    optional<vector<Value>> result = runSafely(_context, command, "running command", user);

    if (!result)
    {
        return;
    }

    for (const Value& value : *result)
    {
        if (value.type == Value_Void)
        {
            continue;
        }

        user->logScript(value.toString());
    }
}
#endif

void GameState::clearSessionUserReferences(User* user)
{
    vrDrop(user);

    for (const auto& [ name, world ] : _worlds)
    {
        world->clearSessionUserReferences(user->id());
    }
}

void GameState::clearUserReferences(User* user)
{
    vrDrop(user);

    for (const auto& [ name, world ] : _worlds)
    {
        world->clearUserReferences(user->id());
    }

    _context->clearUserReferences(user->id());
}

void GameState::setPose(
    EntityID id,
    const map<VRDevice, mat4>& anchorMapping,
    const map<VRDevice, mat4>& pose
)
{
    for (const auto& [ name, world ] : _worlds)
    {
        world->setPose(id, anchorMapping, pose);
    }
}

void GameState::pushInput(EntityID id, const string& hook, const Input& input)
{
    if (magnitude() == 0)
    {
        return;
    }

    Entity* entity = get(id);

    if (!entity)
    {
        return;
    }

    entity->input(
        _context,
        hook,
        input.up,
        input.intensity,
        input.position,
        input.rotation,
        input.linearMotion,
        input.angularMotion,
        input.dimensions,
        input.touch
    );
}

void GameState::enable()
{
    for (const auto& [ name, world ] : _worlds)
    {
        world->enable();
    }
}

void GameState::disable()
{
    for (const auto& [ name, world ] : _worlds)
    {
        world->disable();
    }
}

void GameState::addUser(const User& user)
{
    _users.insert({ user.id(), user });
}

void GameState::removeUser(User* target)
{
    UserID id = target->id();

    // Clear attachment/ownership state of entities
    clearUserReferences(target);

    // Remove from teams
    for (const string& team : target->teams())
    {
        getTeam(team)->userIDs.erase(id);
    }

    delete target;
    _users.erase(id);
}

#if defined(LMOD_SERVER)
void GameState::kickUser(User* kicker, User* user)
{
    log("user-kicked-user", { kicker->name(), user->name() });
    user->kick();
}
#endif

UserID GameState::nextUserID()
{
    UserID id = _nextUserID++;

    return id;
}

const map<UserID, User>& GameState::users() const
{
    return _users;
}

User* GameState::getUser(UserID id)
{
    auto it = _users.find(id);

    if (it == _users.end())
    {
        return nullptr;
    }

    return &it->second;
}

const User* GameState::getUser(UserID id) const
{
    auto it = _users.find(id);

    if (it == _users.end())
    {
        return nullptr;
    }

    return &it->second;
}

User* GameState::getUserByName(const string& name)
{
    for (auto& [ id, user ] : _users)
    {
        if (user.name() == name)
        {
            return &user;
        }
    }

    return nullptr;
}

const User* GameState::getUserByName(const string& name) const
{
    for (const auto& [ id, user ] : _users)
    {
        if (user.name() == name)
        {
            return &user;
        }
    }

    return nullptr;
}

void GameState::addTeam(const string& name)
{
    auto it = _teams.find(name);

    if (it != _teams.end())
    {
        return;
    }

    Team team(name);

    _teams.insert({ name, team });
}

void GameState::removeTeam(const string& name)
{
    auto it = _teams.find(name);

    if (it == _teams.end())
    {
        return;
    }

    Team& team = it->second;

    for (UserID userID : team.userIDs)
    {
        User& user = _users.at(userID);

        set<string> teams = user.teams();
        teams.erase(name);
        user.setTeams(teams);
    }

    _teams.erase(it);
}

void GameState::updateTeam(const string& name, const set<UserID>& userIDs)
{
    auto it = _teams.find(name);

    if (it == _teams.end())
    {
        return;
    }

    Team& team = it->second;

    // Remove team from any leaving users
    for (UserID userID : team.userIDs)
    {
        if (userIDs.contains(userID))
        {
            continue;
        }

        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        set<string> teams = user->teams();
        teams.erase(name);
        user->setTeams(teams);
    }

    // Add team to any joining users
    for (UserID userID : userIDs)
    {
        if (team.userIDs.contains(userID))
        {
            continue;
        }

        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        set<string> teams = user->teams();
        teams.insert(name);
        user->setTeams(teams);
    }

    set<UserID> actualUserIDs;

    for (UserID userID : userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        actualUserIDs.insert(userID);
    }

    team.userIDs = actualUserIDs;
}

const map<string, Team>& GameState::teams() const
{
    return _teams;
}

Team* GameState::getTeam(const string& name)
{
    auto it = _teams.find(name);

    if (it == _teams.end())
    {
        return nullptr;
    }

    return &it->second;
}

void GameState::updateUserTeams(User* user, const set<string>& teams)
{
    // Remove from teams being left
    for (const string& name : user->teams())
    {
        if (teams.contains(name))
        {
            continue;
        }

        Team* team = getTeam(name);

        if (!team)
        {
            continue;
        }

        team->userIDs.erase(user->id());
    }

    // Add to teams being joined
    for (const string& name : teams)
    {
        if (user->teams().contains(name))
        {
            continue;
        }

        Team* team = getTeam(name);

        if (!team)
        {
            continue;
        }

        team->userIDs.insert(user->id());
    }

    set<string> actualTeams;

    for (const string& name : teams)
    {
        Team* team = getTeam(name);

        if (!team)
        {
            continue;
        }

        actualTeams.insert(name);
    }

    user->setTeams(actualTeams);
}

void GameState::splashMessage(const string& text)
{
    _splashMessage = text;
}

const string& GameState::splashMessage() const
{
    return _splashMessage;
}

void GameState::setTimeMultiplier(f64 multiplier)
{
    _timeMultiplier = multiplier;
}

void GameState::setTimeMultiplier(const User* user, f64 multiplier)
{
    log("user-set-the-time-multiplier-to", { user->name(), toPrettyString(multiplier) });

    _timeMultiplier = multiplier;
}

f64 GameState::timeMultiplier() const
{
    return _timeMultiplier;
}

void GameState::lockAttachments(const User* user)
{
    log("user-locked-attachments", { user->name() });

    lockAttachments();
}

void GameState::unlockAttachments(const User* user)
{
    log("user-unlocked-attachments", { user->name() });

    unlockAttachments();
}

void GameState::lockAttachments()
{
    _attachmentsLocked = true;
}

void GameState::unlockAttachments()
{
    _attachmentsLocked = false;
}

bool GameState::attachmentsLocked() const
{
    return _attachmentsLocked;
}

void GameState::brake(const User* user)
{
    log("user-pulled-the-brake", { user->name() });

    _braking = true;
    _brakingID = user->id();
    brakeElapsed = 0;
}

void GameState::unbrake(const User* user)
{
    log("user-released-the-brake", { user->name() });

    _braking = false;
    _brakingID = UserID();
    brakeElapsed = 0;
}

bool GameState::braking() const
{
    return _braking;
}

UserID GameState::brakingID() const
{
    return _brakingID;
}

f64 GameState::brakeProgress() const
{
    f64 progress = min(brakeElapsed / brakeDuration, 1.0);

    if (!_braking)
    {
        progress = 1 - progress;
    }

    return progress;
}

#if defined(LMOD_SERVER)
void GameState::ping(User* user)
{
    ::log(format(localMessages.at("user-pinged"), { user->name() }));

    for (auto& [ id, other ] : _users)
    {
        if (id != user->id() && other.attached())
        {
            continue;
        }

        if (other.active())
        {
            other.addPing(user->id());
        }

        other.pushMessage(Message(Message_Log, "", "user-pinged", { user->name() }));
    }
}

void GameState::attach(User* user, Entity* entity)
{
    adminLog("user-attached-to-entity", { user->name(), entity->name(), to_string(entity->id().value()) });

    u32 oldMagnitude = magnitude();

    user->attach(entity->id());
    entity->setAttachedUserID(user->id());

    checkMagnitude(oldMagnitude);
}

void GameState::detach(User* user)
{
    vrDrop(user);

    Entity* entity = get(user->entityID());

    adminLog("user-detached-from-entity", { user->name(), entity->name(), to_string(entity->id().value()) });

    u32 oldMagnitude = magnitude();

    entity->setAttachedUserID(UserID());
    user->detach();

    checkMagnitude(oldMagnitude);

    const BodyPart* anchor = entity->body().get(user->vr() ? Body_Anchor_Room : Body_Anchor_Viewer);

    if (!anchor)
    {
        return;
    }

    mat4 transform = entity->globalTransform().applyToTransform(anchor->regionalTransform());

    vec3 forward;
    vec3 side;
    vec3 up;

    vec3 localUp = entity->world()->up(entity->globalPosition());

    if (f64 d = abs(transform.forward().dot(localUp)); !roughly(d, 1.0))
    {
        forward = transform.forward();
        side = forward.cross(localUp).normalize();
        up = side.cross(forward).normalize();
    }
    else
    {
        forward = forwardDir;
        up = localUp;
    }

    mat4 newTransform = {
        transform.position(),
        quaternion(forward, up),
        vec3(1)
    };

    if (user->vr())
    {
        user->setRoom(newTransform, true);
    }
    else
    {
        user->setViewer(newTransform, true);
    }
}

void GameState::magnitude(User* user, u32 value)
{
    if (value > magnitudeLimit())
    {
        return;
    }

    u32 oldMagnitude = magnitude();

    user->setMagnitude(value);
    log("user-set-their-magnitude-control-to", { user->name(), to_string(user->magnitude()) });

    checkMagnitude(oldMagnitude);
}

void GameState::setMagnitudeLimit(const User* user, u32 limit)
{
    u32 oldMagnitude = magnitude();

    magnitudeLimit(limit);

    checkMagnitude(oldMagnitude);

    log("user-set-the-magnitude-control-limit-to", { user->name(), to_string(_magnitudeLimit) });
}
#endif

void GameState::magnitudeLimit(u32 limit)
{
    limit = min(limit, maxMagnitude);

    for (auto& [ name, user ] : _users)
    {
        if (user.magnitude() > limit)
        {
            user.setMagnitude(limit);
        }
    }

    _magnitudeLimit = limit;
}

u32 GameState::magnitudeLimit() const
{
    return _magnitudeLimit;
}

u32 GameState::magnitude() const
{
    u32 votes[magnitudeLevelCount]{0};

    for (const auto& [ id, user ] : _users)
    {
        if (!user.active() || !user.attached())
        {
            continue;
        }

        votes[user.magnitude()]++;
    }

    for (u32 i = 0; i <= _magnitudeLimit && i < magnitudeLevelCount; i++)
    {
        if (votes[i] > 0)
        {
            return i;
        }
    }

    return minMagnitude;
}

void GameState::addWorld(const string& name)
{
    _worlds.insert({ name, new World(name) });
}

void GameState::addWorld(const string& name, const World& world)
{
    _worlds.insert({ name, new World(world) });
}

void GameState::removeWorld(const string& name)
{
    if (!canDeleteWorld(name))
    {
        return;
    }

    delete _worlds.at(name);

    _worlds.erase(name);
}

World* GameState::getWorld(const string& name) const
{
    auto it = _worlds.find(name);

    if (it != _worlds.end())
    {
        return it->second;
    }

    return nullptr;
}

const map<string, World*>& GameState::worlds() const
{
    return _worlds;
}

void GameState::traverse(const function<void(Entity*)>& behavior)
{
    for (const auto& [ name, world ] : _worlds)
    {
        world->traverse(behavior);
    }
}

void GameState::traverse(const function<void(const Entity*)>& behavior) const
{
    for (const auto& [ name, world ] : _worlds)
    {
        static_cast<const World*>(world)->traverse(behavior);
    }
}

bool GameState::partiallyTraverse(const function<bool(Entity*)>& behavior)
{
    for (const auto& [ name, world ] : _worlds)
    {
        if (world->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool GameState::partiallyTraverse(const function<bool(const Entity*)>& behavior) const
{
    for (const auto& [ name, world ] : _worlds)
    {
        if (static_cast<const World*>(world)->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

TimerID GameState::addTimer(f64 delay, const Lambda& callback, User* user)
{
    TimerID id = nextTimerID++;

    _timers.insert({ id, Timer(delay, callback, user->id()) });

    return id;
}

void GameState::removeTimer(TimerID id)
{
    _timers.erase(id);
}

const map<TimerID, Timer>& GameState::timers() const
{
    return _timers;
}

RepeaterID GameState::addRepeater(const Lambda& callback, User* user)
{
    RepeaterID id = nextRepeaterID++;

    _repeaters.insert({ id, Repeater(callback, user->id()) });

    return id;
}

void GameState::removeRepeater(RepeaterID id)
{
    _repeaters.erase(id);
}

const map<RepeaterID, Repeater>& GameState::repeaters() const
{
    return _repeaters;
}

TextID GameState::nextTextID()
{
    TextID id = _nextTextID++;

    return id;
}

ScriptContext* GameState::context()
{
    return _context;
}

EntityID GameState::peekNextEntityID()
{
    return _nextEntityID;
}

EntityID GameState::nextEntityID()
{
    EntityID id = _nextEntityID++;

    return id;
}

Entity* GameState::get(EntityID id) const
{
    for (auto& [ name, world ] : _worlds)
    {
        Entity* entity = world->get(id);

        if (entity)
        {
            return entity;
        }
    }

    return nullptr;
}

f64 GameState::stepInterval() const
{
    return (1.0 / g_config.stepRate) * _timeMultiplier * (1 - brakeProgress());
}

#if defined(LMOD_SERVER)
void GameState::sendChatMessage(User* user, const string& message)
{
    ::log(user->name() + ": " + message);

    for (auto& [ id, otherUser ] : _users)
    {
        otherUser.chat(user, message);
    }
}

void GameState::sendDirectChatMessage(User* user, User* targetUser, const string& message)
{
    user->directChatOut(targetUser, message);

    targetUser->directChatIn(user, message);
}

void GameState::sendTeamChatMessage(User* user, Team* team, const string& message)
{
    user->teamChatOut(team->name, message);

    for (UserID userID : team->userIDs)
    {
        User* targetUser = getUser(userID);

        if (!targetUser)
        {
            continue;
        }

        targetUser->teamChatIn(user, team->name, message);
    }
}

void GameState::scriptPrint(const string& message)
{
    ::log(message);

    for (auto& [ id, user ] : _users)
    {
        user.log(message);
    }
}

void GameState::log(const string& text, const vector<string>& parameters)
{
    ::log(format(localMessages.at(text), parameters));

    for (auto& [ id, user ] : _users)
    {
        user.pushMessage(Message(Message_Log, "", text, parameters));
    }
}

void GameState::adminLog(const string& text, const vector<string>& parameters)
{
    ::log(format(localMessages.at(text), parameters));

    for (auto& [ id, user ] : _users)
    {
        if (!user.admin())
        {
            continue;
        }

        user.pushMessage(Message(Message_Log, "", text, parameters));
    }
}

// TODO: LOCALIZATION: Why is this necessary and why are there two different sorts of magnitude level reporting, one which is replayable hybrid and one hich isn't?
#elif defined(LMOD_CLIENT)
void GameState::log(const string& text, const vector<string>& parameters)
{
    ::log(localize(text, parameters));
}
#endif

bool GameState::canDeleteWorld(const string& name) const
{
    for (const auto& [ id, user ] : _users)
    {
        if (user.world() == name)
        {
            return false;
        }
    }

    return true;
}

#if defined(LMOD_SERVER)
void GameState::handleUserEvent(User* user, const UserEvent& event)
{
    switch (event.type)
    {
    default :
        error("Encountered unknown user event type " + to_string(event.type) + " received.");
        break;
    case User_Event_Add :
        break;
    case User_Event_Remove :
        handleUserRemove(user, event);
        break;
    case User_Event_Kick :
        handleUserKick(user, event);
        break;
    case User_Event_Shift :
        handleUserShift(user, event);
        break;
    case User_Event_Move_Forced :
        handleUserMoveForced(user, event);
        break;
    case User_Event_Move_Viewer :
        handleUserMoveViewer(user, event);
        break;
    case User_Event_Move_Room :
        handleUserMoveRoom(user, event);
        break;
    case User_Event_Move_Devices :
        handleUserMoveDevices(user, event);
        break;
    case User_Event_Angle :
        handleUserAngle(user, event);
        break;
    case User_Event_Aspect :
        handleUserAspect(user, event);
        break;
    case User_Event_Bounds :
        handleUserBounds(user, event);
        break;
    case User_Event_Color :
        handleUserColor(user, event);
        break;
    case User_Event_Teams :
        handleUserTeams(user, event);
        break;
    case User_Event_Anchor :
        handleUserAnchor(user, event);
        break;
    case User_Event_Unanchor :
        handleUserUnanchor(user, event);
        break;
    case User_Event_Room_Offset :
        handleUserRoomOffset(user, event);
        break;
    case User_Event_Magnitude :
        handleUserMagnitude(user, event);
        break;
    case User_Event_Advantage :
        handleUserAdvantage(user, event);
        break;
    case User_Event_Doom :
        handleUserDoom(user, event);
        break;
    }
}

void GameState::handleTeamEvent(User* user, const TeamEvent& event)
{
    switch (event.type)
    {
    case Team_Event_Add :
        handleTeamAdd(user, event);
        break;
    case Team_Event_Remove :
        handleTeamRemove(user, event);
        break;
    case Team_Event_Update :
        handleTeamUpdate(user, event);
        break;
    }
}

void GameState::handleDocumentEvent(User* user, const DocumentEvent& event)
{
    switch (event.type)
    {
    case Document_Event_Add :
        handleDocumentAdd(user, event);
        break;
    case Document_Event_Remove :
        handleDocumentRemove(user, event);
        break;
    case Document_Event_Update :
        handleDocumentUpdate(user, event);
        break;
    }
}

void GameState::handleGameEvent(User* user, const GameEvent& event)
{
    switch (event.type)
    {
    default :
        error("Encountered unknown game event type " + to_string(event.type) + " received.");
        break;
    case Game_Event_Attach :
        handleAttach(user, event);
        break;
    case Game_Event_Detach :
        handleDetach(user, event);
        break;
    case Game_Event_Select :
        handleSelect(user, event);
        break;
    case Game_Event_Deselect :
        handleDeselect(user, event);
        break;
    case Game_Event_Splash_Message :
        handleSplashMessage(user, event);
        break;
    case Game_Event_Time_Multiplier :
        handleTimeMultiplier(user, event);
        break;
    case Game_Event_Lock_Attachments :
        handleLockAttachments(user, event);
        break;
    case Game_Event_Unlock_Attachments :
        handleUnlockAttachments(user, event);
        break;
    case Game_Event_Ping :
        handlePing(user, event);
        break;
    case Game_Event_Brake :
        handleBrake(user, event);
        break;
    case Game_Event_Unbrake :
        handleUnbrake(user, event);
        break;
    case Game_Event_Magnitude_Limit :
        handleMagnitudeLimit(user, event);
        break;
    case Game_Event_Next_Entity_ID :
        handleNextEntityID(user, event);
        break;
    case Game_Event_Next_Timer_ID :
        handleNextTimerID(user, event);
        break;
    case Game_Event_Next_Repeater_ID :
        handleNextRepeaterID(user, event);
        break;
    }
}

void GameState::handleWorldEvent(User* user, const WorldEvent& event)
{
    switch (event.type)
    {
    case World_Event_Add :
        handleWorldAdd(user, event);
        break;
    case World_Event_Remove :
        handleWorldRemove(user, event);
        break;
    default :
        pushWorldEvent(user, event);
        break;
    }
}

void GameState::handleInputEvent(User* user, const InputEvent& event)
{
    // Ignore while paused
    if (roughly<f64>(stepInterval(), 0))
    {
        return;
    }

    EntityID id = user->entityID();

    if (!id)
    {
        return;
    }

    pushInput(id, event.hook, event.input);
}

void GameState::handleGripEvent(User* user, const GripEvent& event)
{
    // Ignore while paused
    if (roughly<f64>(stepInterval(), 0))
    {
        return;
    }

    EntityID entityID = user->entityID();

    if (!entityID)
    {
        return;
    }

    Entity* entity = get(entityID);

    if (!entity)
    {
        return;
    }

    const BodyPart* anchor = entity->body().get(Body_Anchor_Left_Hand);

    if (!anchor)
    {
        return;
    }

    bool gripping = event.strength > 0.5;

    GripState& state = user->gripStates(event.right ? 1 : 0);

    state.push(event.strength, event.linearVelocity, event.angularVelocity);

    if (state.gripping() && !gripping)
    {
        vector<Entity*> containingEntities = getWorld(user->world())->within(event.position);

        sort(
            containingEntities.begin(),
            containingEntities.end(),
            [](const Entity* a, const Entity* b) -> bool
            {
                return greaterMass(b->mass(), a->mass());
            }
        );

        if (state.pulling() && containingEntities.empty())
        {
            vector<tuple<Entity*, vec3>> pullEntities = getWorld(user->world())->intersect(
                event.position,
                event.rotation.forward(),
                anchor->get<BodyPartAnchor>().pullDistance
            );

            if (pullEntities.empty())
            {
                return;
            }

            Entity* target = std::get<0>(pullEntities.front());

            if (target->mass() > anchor->get<BodyPartAnchor>().massLimit)
            {
                return;
            }

            vrPull(user, entity, anchor, state, target);
        }
        else
        {
            vrGrip(user, entity, anchor, state, containingEntities.front());
        }
    }
    else if (!state.gripping() && gripping)
    {
        vrRelease(user, entity, anchor, state);
    }
}

void GameState::handleCommandEvent(User* user, const CommandEvent& event)
{
    switch (event.type)
    {
    case Command_Event_Command :
    {
        string text = sanitize(event.text);

        user->recordHistory(text);

        ::log(user->name() + ": " + text);
        user->logScript(text);

        string fullText = user->loadCommand() + " " + text;

        if (!isCompleteCommand(fullText))
        {
            user->storeCommand(fullText);
            return;
        }

        executeCommand(user, fullText);
        break;
    }
    case Command_Event_Chat :
        sendChatMessage(user, event.text);
        break;
    case Command_Event_Direct_Chat :
    {
        User* targetUser = getUserByName(event.destination);

        if (!targetUser)
        {
            return;
        }

        sendDirectChatMessage(user, targetUser, event.text);
        break;
    }
    case Command_Event_Team_Chat :
    {
        Team* team = getTeam(event.destination);

        if (!team)
        {
            return;
        }

        sendTeamChatMessage(user, team, event.text);
        break;
    }
    }
}

void GameState::handlePoseEvent(User* user, const PoseEvent& event)
{
    // Ignore while paused
    if (roughly<f64>(stepInterval(), 0))
    {
        return;
    }

    setPose(user->entityID(), user->anchorMapping(), event.pose);
}

void GameState::handleTextRequestEvent(User* user, const TextRequestEvent& event)
{
    if (!event.request.valid())
    {
        return;
    }

    TextRequest request;

    switch (event.request.type())
    {
    case Text_Signal :
        request = TextRequest(TextID(), event.request.data<TextSignalRequest>());
        break;
    case Text_Titlecard :
        request = TextRequest(_nextTextID++, event.request.data<TextTitlecardRequest>());
        break;
    case Text_Timeout :
        request = TextRequest(_nextTextID++, event.request.data<TextTimeoutRequest>());
        break;
    case Text_Unary :
        request = TextRequest(_nextTextID++, event.request.data<TextUnaryRequest>());
        break;
    case Text_Binary :
        request = TextRequest(_nextTextID++, event.request.data<TextBinaryRequest>());
        break;
    case Text_Options :
        request = TextRequest(_nextTextID++, event.request.data<TextOptionsRequest>());
        break;
    case Text_Choose :
        request = TextRequest(_nextTextID++, event.request.data<TextChooseRequest>());
        break;
    case Text_Assign_Values :
        request = TextRequest(_nextTextID++, event.request.data<TextAssignValuesRequest>());
        break;
    case Text_Spend_Points :
        request = TextRequest(_nextTextID++, event.request.data<TextSpendPointsRequest>());
        break;
    case Text_Assign_Points :
        request = TextRequest(_nextTextID++, event.request.data<TextAssignPointsRequest>());
        break;
    case Text_Vote :
        request = TextRequest(_nextTextID++, event.request.data<TextVoteRequest>());
        break;
    case Text_Choose_Major :
        request = TextRequest(_nextTextID++, event.request.data<TextChooseMajorRequest>());
        break;
    case Text_Knowledge :
        request = TextRequest(TextID(), event.request.data<TextKnowledgeRequest>());
        break;
    case Text_Clear :
        request = TextRequest(TextID(), event.request.data<TextClearRequest>());
        break;
    }

    if (!user->admin())
    {
        user->error(permissionDeniedError("create-text"));
        return;
    }

    vector<User*> targets;
    targets.reserve(event.userIDs.size());

    for (UserID userID : event.userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        targets.push_back(user);
    }

    for (User* target : targets)
    {
        target->requestText(request, user->id());
    }
}

void GameState::handleTextResponseEvent(User* user, const TextResponseEvent& event)
{
    // Ignore while paused
    if (roughly<f64>(stepInterval(), 0))
    {
        return;
    }

    user->respondToText(_context, event.response);
}

void GameState::handleTetherEvent(User* user, const TetherEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("configure-tether"));
        return;
    }

    string worldName;

    for (UserID userID : event.userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        if (!worldName.empty() && user->world() != worldName)
        {
            user->error(differentWorldsError());
            return;
        }

        worldName = user->world();

        if (event.type == Tether_Event_Tether)
        {
            user->tether(event.distance, event.userIDs);
        }
        else
        {
            user->untether();
        }
    }
}

void GameState::handleConfinementEvent(User* user, const ConfinementEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("configure-confinement"));
        return;
    }

    string worldName;

    for (UserID userID : event.userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        if (!worldName.empty() && user->world() != worldName)
        {
            user->error(differentWorldsError());
            return;
        }

        worldName = user->world();

        if (event.type == Confinement_Event_Confine)
        {
            user->confine(event.position, event.distance);
        }
        else
        {
            user->unconfine();
        }
    }
}

void GameState::handleGotoEvent(User* user, const GotoEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("configure-goto"));
        return;
    }

    vector<tuple<User*, Entity*>> users;

    string worldName;

    for (UserID userID : event.userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        if (!worldName.empty() && user->world() != worldName)
        {
            user->error(differentWorldsError());
            return;
        }

        worldName = user->world();

        users.push_back({ user, get(user->entityID()) });
    }

    userGoto(users, event.position, event.radius, event.up);
}

void GameState::handleFadeEvent(User* user, const FadeEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("configure-fade"));
        return;
    }

    for (UserID userID : event.userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        user->setFade(event.type == Fade_Event_Fade);
    }
}

void GameState::handleWaitEvent(User* user, const WaitEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("configure-wait"));
        return;
    }

    for (UserID userID : event.userIDs)
    {
        User* user = getUser(userID);

        if (!user)
        {
            continue;
        }

        user->setWait(event.type == Wait_Event_Wait);
    }
}

void GameState::handleVoiceChannelEvent(User* user, const VoiceChannelEvent& event)
{
    if (!event.name.empty() && !user->teams().contains(event.name))
    {
        user->pushMessage(teamNotFoundError(event.name));
        return;
    }

    user->setVoiceChannel(event.name);
}

void GameState::handleUserRemove(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("remove-another-user"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    if (target->active())
    {
        user->pushMessage(userLoggedInError(target->name()));
        return;
    }

    removeUser(target);
}

void GameState::handleUserKick(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("kick-another-user"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    if (!target->active())
    {
        user->pushMessage(userNotLoggedInError(target->name()));
        return;
    }

    kickUser(user, target);
}

void GameState::handleUserShiftForced(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("shift-another-user"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    auto world = getWorld(event.s);

    if (!world)
    {
        user->pushMessage(worldNotFoundError(event.s));
        return;
    }

    if (target->entityID())
    {
        Entity* host = new Entity(*get(target->entityID()));

        host->world()->remove(host->id());
        world->add(EntityID(), host);
    }

    target->shift(world->name());
}

void GameState::handleUserMoveForced(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("move-another-user"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    if (target->entityID())
    {
        Entity* host = get(target->entityID());

        vec3 posOffset = host->globalPosition() - target->viewer().position();
        quaternion rotOffset = (host->globalRotation() / target->viewer().rotation()).normalize();

        host->setGlobalPosition(event.transform.position() + posOffset);
        host->setGlobalRotation(event.transform.rotation() * rotOffset);
    }

    if (target->vr())
    {
        target->setRoom(event.transform, true);
    }
    else
    {
        target->setViewer(event.transform, true);
    }
}

void GameState::handleUserShift(User* user, const UserEvent& event)
{
    auto world = getWorld(event.s);

    if (!world)
    {
        user->pushMessage(worldNotFoundError(event.s));
        return;
    }

    if (!user->admin() && !world->shown())
    {
        user->pushMessage(worldNotFoundError(event.s));
        return;
    }

    if (user->attached())
    {
        user->error(shiftWhileAttachedError());
        return;
    }

    if (user->tether().active)
    {
        user->error(shiftWhileTetheredError());
        return;
    }

    if (user->confinement().active)
    {
        user->error(shiftWhileConfinedError());
        return;
    }

    user->shift(event.s);
}

void GameState::handleUserMoveViewer(User* user, const UserEvent& event)
{
    user->setViewer(event.transform, false);
}

void GameState::handleUserMoveRoom(User* user, const UserEvent& event)
{
    user->setRoom(event.transform, false);
}

void GameState::handleUserMoveDevices(User* user, const UserEvent& event)
{
    for (const auto& [ device, transform ] : event.mapping)
    {
        switch (device)
        {
        case VR_Device_Head :
            user->setHead(transform);
            break;
        case VR_Device_Left_Hand :
            user->setLeftHand(transform);
            break;
        case VR_Device_Right_Hand :
            user->setRightHand(transform);
            break;
        case VR_Device_Hips :
            user->setHips(transform);
            break;
        case VR_Device_Left_Foot :
            user->setLeftFoot(transform);
            break;
        case VR_Device_Right_Foot :
            user->setRightFoot(transform);
            break;
        case VR_Device_Chest :
            user->setChest(transform);
            break;
        case VR_Device_Left_Shoulder :
            user->setLeftShoulder(transform);
            break;
        case VR_Device_Right_Shoulder :
            user->setRightShoulder(transform);
            break;
        case VR_Device_Left_Elbow :
            user->setLeftElbow(transform);
            break;
        case VR_Device_Right_Elbow :
            user->setRightElbow(transform);
            break;
        case VR_Device_Left_Wrist :
            user->setLeftWrist(transform);
            break;
        case VR_Device_Right_Wrist :
            user->setRightWrist(transform);
            break;
        case VR_Device_Left_Knee :
            user->setLeftKnee(transform);
            break;
        case VR_Device_Right_Knee :
            user->setRightKnee(transform);
            break;
        case VR_Device_Left_Ankle :
            user->setLeftAnkle(transform);
            break;
        case VR_Device_Right_Ankle :
            user->setRightAnkle(transform);
            break;
        }
    }
}

void GameState::handleUserAngle(User* user, const UserEvent& event)
{
    user->setAngle(event.f);
}

void GameState::handleUserAspect(User* user, const UserEvent& event)
{
    user->setAspect(event.f);
}

void GameState::handleUserBounds(User* user, const UserEvent& event)
{
    user->setBounds(event.bounds);
}

void GameState::handleUserColor(User* user, const UserEvent& event)
{
    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    if (!user->admin() && target != user)
    {
        user->error(permissionDeniedError("change-another-user-color"));
        return;
    }

    target->setColor(event.color);

    stringstream ss;
    ss << event.color;

    if (target == user)
    {
        log("user-changed-their-color-to", { user->name(), ss.str() });
    }
    else
    {
        log("user-changed-the-color-of-user-to", { user->name(), target->name(), ss.str() });
    }
}

void GameState::handleUserTeams(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("change-another-user-teams"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    updateUserTeams(target, event.teams);
}

void GameState::handleUserAnchor(User* user, const UserEvent& event)
{
    if (!user->vr())
    {
        user->error(notInVRError());
        return;
    }

    if (!user->attached())
    {
        user->error(notAttachedError());
        return;
    }

    if (user->anchored())
    {
        user->error(alreadyAnchoredError());
        return;
    }

    user->anchor(event.mapping);
}

void GameState::handleUserUnanchor(User* user, const UserEvent& event)
{
    if (!user->vr())
    {
        user->error(notInVRError());
        return;
    }

    if (!user->attached())
    {
        user->error(notAttachedError());
        return;
    }

    if (!user->anchored())
    {
        user->error(notAnchoredError());
        return;
    }

    user->unanchor();
}

void GameState::handleUserRoomOffset(User* user, const UserEvent& event)
{
    if (!user->vr())
    {
        user->error(notInVRError());
        return;
    }

    if (!user->attached())
    {
        user->error(notAttachedError());
        return;
    }

    user->setRoomOffset(event.transform);
}

void GameState::handleUserMagnitude(User* user, const UserEvent& event)
{
    magnitude(user, event.u);
}

void GameState::handleUserAdvantage(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("change-another-user-advantage"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    target->setAdvantage(event.i);
}

void GameState::handleUserDoom(User* user, const UserEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("change-another-user-doom"));
        return;
    }

    User* target = getUser(event.id);

    if (!target)
    {
        user->pushMessage(userNotFoundError(event.id));
        return;
    }

    target->setDoom(event.b);
}

void GameState::handleDocumentAdd(User* user, const DocumentEvent& event)
{
    if (!isValidName(event.name))
    {
        user->pushMessage(invalidNameError(event.name));
        return;
    }

    user->addDocument(event.name);
}

void GameState::handleDocumentRemove(User* user, const DocumentEvent& event)
{
    user->removeDocument(event.name);
}

void GameState::handleDocumentUpdate(User* user, const DocumentEvent& event)
{
    user->updateDocument(event.name, sanitize(event.text, true));
}

void GameState::handleTeamAdd(User* user, const TeamEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("add-team"));
        return;
    }

    addTeam(event.name);
}

void GameState::handleTeamRemove(User* user, const TeamEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("remove-team"));
        return;
    }

    removeTeam(event.name);
}

void GameState::handleTeamUpdate(User* user, const TeamEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("update-team"));
        return;
    }

    updateTeam(event.name, event.userIDs);
}

void GameState::handleAttach(User* user, const GameEvent& event)
{
    if (user->attached())
    {
        user->error(alreadyAttachedError());
        return;
    }

    Entity* entity = get(EntityID(event.u));

    if (!entity)
    {
        user->pushMessage(entityNotFoundError(EntityID(event.u)));
        return;
    }

    if (entity->attachedUserID())
    {
        user->error(alreadyHasAttachedUserError());
        return;
    }

    if (!entity->active())
    {
        user->error(entityNotActiveError());
        return;
    }

    if (!entity->host())
    {
        user->error(entityNotHostError());
        return;
    }

    attach(user, entity);
}

void GameState::handleDetach(User* user, const GameEvent& event)
{
    if (!user->attached())
    {
        user->error(notAttachedError());
        return;
    }

    if (!user->admin() && _attachmentsLocked)
    {
        user->error(attachmentsLockedError());
        return;
    }

    detach(user);
}

void GameState::handleSelect(User* user, const GameEvent& event)
{
    Entity* entity = get(EntityID(event.u));

    if (!entity)
    {
        user->pushMessage(entityNotFoundError(EntityID(event.u)));
        return;
    }

    if (!user->admin() && entity->selectingUserID() && entity->selectingUserID() != user->id())
    {
        return;
    }

    entity->setSelectingUserID(user->id());
}

void GameState::handleDeselect(User* user, const GameEvent& event)
{
    Entity* entity = get(EntityID(event.u));

    if (!entity)
    {
        user->pushMessage(entityNotFoundError(EntityID(event.u)));
        return;
    }

    if (user->id() != entity->selectingUserID())
    {
        return;
    }

    entity->setSelectingUserID(UserID());
}

void GameState::handleSplashMessage(User* user, const GameEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("set-splash-message"));
        return;
    }

    splashMessage(event.s);
}

void GameState::handleTimeMultiplier(User* user, const GameEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("set-time-multiplier"));
        return;
    }

    setTimeMultiplier(user, event.f);
}

void GameState::handleLockAttachments(User* user, const GameEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("lock-attachments"));
        return;
    }

    lockAttachments(user);
}

void GameState::handleUnlockAttachments(User* user, const GameEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("unlock-attachments"));
        return;
    }

    unlockAttachments(user);
}

void GameState::handlePing(User* user, const GameEvent& event)
{
    if (!user->attached())
    {
        user->error(notAttachedError());
        return;
    }

    ping(user);
}

void GameState::handleBrake(User* user, const GameEvent& event)
{
    if (braking())
    {
        user->error(alreadyBrakingError());
        return;
    }

    if (!user->attached())
    {
        user->error(notAttachedError());
        return;
    }

    brake(user);
}

void GameState::handleUnbrake(User* user, const GameEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("release-brake"));
        return;
    }

    if (!braking())
    {
        user->error(notBrakingError());
        return;
    }

    unbrake(user);
}

void GameState::handleMagnitudeLimit(User* user, const GameEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("set-magnitude-limit"));
        return;
    }

    if (event.u < 0 || event.u > 3)
    {
        return;
    }

    setMagnitudeLimit(user, event.u);
}

void GameState::handleNextEntityID(User* user, const GameEvent& event)
{
    if (EntityID(event.u) < _nextEntityID)
    {
        return;
    }

    _nextEntityID = EntityID(event.u);
}

void GameState::handleNextTimerID(User* user, const GameEvent& event)
{
    if (TimerID(event.u) < nextTimerID)
    {
        return;
    }

    nextTimerID = TimerID(event.u);
}

void GameState::handleNextRepeaterID(User* user, const GameEvent& event)
{
    if (RepeaterID(event.u) < nextRepeaterID)
    {
        return;
    }

    nextRepeaterID = RepeaterID(event.u);
}

void GameState::handleWorldAdd(User* user, const WorldEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("add-world"));
        return;
    }

    if (_worlds.contains(event.name))
    {
        user->pushMessage(worldFoundError(event.name));
        return;
    }

    addWorld(event.name);
}

void GameState::handleWorldRemove(User* user, const WorldEvent& event)
{
    if (!user->admin())
    {
        user->error(permissionDeniedError("remove-world"));
        return;
    }

    if (!_worlds.contains(event.name))
    {
        user->pushMessage(worldNotFoundError(event.name));
        return;
    }

    if (!canDeleteWorld(event.name))
    {
        user->pushMessage(worldDeletionBlockedError(event.name));
        return;
    }

    removeWorld(event.name);
}

void GameState::pushWorldEvent(User* user, const NetworkEvent& event)
{
    string worldName;

    switch (event.type())
    {
    default :
        error("Non-world/entity event " + event.name() + " was pushed to world.");
        break;
    case Network_Event_World :
        worldName = event.payload<WorldEvent>().name;
        break;
    case Network_Event_World_Sky :
        worldName = event.payload<WorldSkyEvent>().name;
        break;
    case Network_Event_World_Atmosphere :
        worldName = event.payload<WorldAtmosphereEvent>().name;
        break;
    case Network_Event_Entity :
        worldName = event.payload<EntityEvent>().world;
        break;
    case Network_Event_Entity_Body :
        worldName = event.payload<EntityBodyEvent>().world;
        break;
    case Network_Event_Entity_Sample :
        worldName = event.payload<EntitySampleEvent>().world;
        break;
    case Network_Event_Entity_Sample_Playback :
        worldName = event.payload<EntitySamplePlaybackEvent>().world;
        break;
    case Network_Event_Entity_Action :
        worldName = event.payload<EntityActionEvent>().world;
        break;
    case Network_Event_Entity_Action_Playback :
        worldName = event.payload<EntityActionPlaybackEvent>().world;
        break;
    case Network_Event_Entity_Input_Binding :
        worldName = event.payload<EntityInputBindingEvent>().world;
        break;
    case Network_Event_Entity_HUD :
        worldName = event.payload<EntityHUDEvent>().world;
        break;
    case Network_Event_Entity_Script :
        worldName = event.payload<EntityScriptEvent>().world;
        break;
    }

    auto it = _worlds.find(worldName);

    if (it == _worlds.end())
    {
        user->pushMessage(worldNotFoundError(worldName));
        return;
    }

    World* world = it->second;

    world->push(_context, user, event);
}
#elif defined(LMOD_CLIENT)
bool GameState::handleUserEvent(const UserEvent& event)
{
    switch (event.type)
    {
    default :
        error("Encountered unknown user event type " + to_string(event.type) + " received.");
        break;
    case User_Event_Add :
        return handleUserAdd(event);
    case User_Event_Remove :
        return handleUserRemove(event);
    case User_Event_Shift :
        return handleUserShift(event);
    case User_Event_Move_Viewer :
        return handleUserMoveViewer(event);
    case User_Event_Move_Room :
        return handleUserMoveRoom(event);
    case User_Event_Move_Devices :
        return handleUserMoveDevices(event);
    case User_Event_Admin :
        handleUserAdmin(event);
        break;
    case User_Event_Angle :
        return handleUserAngle(event);
    case User_Event_Aspect :
        return handleUserAspect(event);
    case User_Event_Bounds :
        return handleUserBounds(event);
    case User_Event_Color :
        handleUserColor(event);
        break;
    case User_Event_Teams :
        handleUserTeams(event);
        break;
    case User_Event_Join :
        return handleUserJoin(event);
    case User_Event_Leave :
        return handleUserLeave(event);
    case User_Event_Attach :
        handleUserAttach(event);
        break;
    case User_Event_Detach :
        handleUserDetach(event);
        break;
    case User_Event_Anchor :
        handleUserAnchor(event);
        break;
    case User_Event_Unanchor :
        handleUserUnanchor(event);
        break;
    case User_Event_Room_Offset :
        handleUserRoomOffset(event);
        break;
    case User_Event_Ping :
        handleUserPing(event);
        break;
    case User_Event_Magnitude :
        handleUserMagnitude(event);
        break;
    case User_Event_Advantage :
        handleUserAdvantage(event);
        break;
    case User_Event_Doom :
        handleUserDoom(event);
        break;
    case User_Event_Tether :
        handleUserTether(event);
        break;
    case User_Event_Confinement :
        handleUserConfinement(event);
        break;
    case User_Event_Fade :
        handleUserFade(event);
        break;
    case User_Event_Wait :
        handleUserWait(event);
        break;
    case User_Event_Signal :
        handleUserSignal(event);
        break;
    case User_Event_Titlecard :
        handleUserTitlecard(event);
        break;
    case User_Event_Texts :
        handleUserTexts(event);
        break;
    case User_Event_Knowledge :
        handleUserKnowledge(event);
        break;
    }

    return false;
}

void GameState::handleTeamEvent(const TeamEvent& event)
{
    switch (event.type)
    {
    case Team_Event_Add :
        handleTeamAdd(event);
        break;
    case Team_Event_Remove :
        handleTeamRemove(event);
        break;
    case Team_Event_Update :
        handleTeamUpdate(event);
        break;
    }
}

bool GameState::handleGameEvent(const GameEvent& event)
{
    switch (event.type)
    {
    default :
        error("Encountered unknown game event type " + to_string(event.type) + " received.");
        break;
    case Game_Event_Splash_Message :
        handleSplashMessage(event);
        break;
    case Game_Event_Time_Multiplier :
        handleTimeMultiplier(event);
        break;
    case Game_Event_Lock_Attachments :
        handleLockAttachments(event);
        break;
    case Game_Event_Unlock_Attachments :
        handleUnlockAttachments(event);
        break;
    case Game_Event_Ping :
        return handlePing(event);
    case Game_Event_Brake :
        return handleBrake(event);
    case Game_Event_Unbrake :
        handleUnbrake(event);
        break;
    case Game_Event_Magnitude_Limit :
        handleMagnitudeLimit(event);
        break;
    case Game_Event_Next_Entity_ID :
        handleNextEntityID(event);
        break;
    case Game_Event_Next_Timer_ID :
        handleNextTimerID(event);
        break;
    case Game_Event_Next_Repeater_ID :
        handleNextRepeaterID(event);
        break;
    }

    return false;
}

void GameState::handleWorldEvent(const WorldEvent& event)
{
    switch (event.type)
    {
    case World_Event_Add :
        handleWorldAdd(event);
        break;
    case World_Event_Remove :
        handleWorldRemove(event);
        break;
    default :
        pushWorldEvent(event);
        break;
    }
}

bool GameState::handleUserAdd(const UserEvent& event)
{
    // Blank world name might cause problems but as long as all initial events are processed in one batch we should be okay
    _users.insert({ event.id, User(event.id, event.s, "") });

    if (event.b)
    {
        _users.at(event.id).setAdmin(true);
    }

    return true;
}

bool GameState::handleUserRemove(const UserEvent& event)
{
    _users.erase(event.id);

    return true;
}

bool GameState::handleUserShift(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->shift(event.s);

    return true;
}

bool GameState::handleUserMoveViewer(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->setViewer(event.transform, false);

    return true;
}

bool GameState::handleUserMoveRoom(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->setRoom(event.transform, false);

    return true;
}

bool GameState::handleUserMoveDevices(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    for (const auto& [ device, transform ] : event.mapping)
    {
        switch (device)
        {
        case VR_Device_Head :
            user->setHead(transform);
            break;
        case VR_Device_Left_Hand :
            user->setLeftHand(transform);
            break;
        case VR_Device_Right_Hand :
            user->setRightHand(transform);
            break;
        case VR_Device_Hips :
            user->setHips(transform);
            break;
        case VR_Device_Left_Foot :
            user->setLeftFoot(transform);
            break;
        case VR_Device_Right_Foot :
            user->setRightFoot(transform);
            break;
        case VR_Device_Chest :
            user->setChest(transform);
            break;
        case VR_Device_Left_Shoulder :
            user->setLeftShoulder(transform);
            break;
        case VR_Device_Right_Shoulder :
            user->setRightShoulder(transform);
            break;
        case VR_Device_Left_Elbow :
            user->setLeftElbow(transform);
            break;
        case VR_Device_Right_Elbow :
            user->setRightElbow(transform);
            break;
        case VR_Device_Left_Wrist :
            user->setLeftWrist(transform);
            break;
        case VR_Device_Right_Wrist :
            user->setRightWrist(transform);
            break;
        case VR_Device_Left_Knee :
            user->setLeftKnee(transform);
            break;
        case VR_Device_Right_Knee :
            user->setRightKnee(transform);
            break;
        case VR_Device_Left_Ankle :
            user->setLeftAnkle(transform);
            break;
        case VR_Device_Right_Ankle :
            user->setRightAnkle(transform);
            break;
        }
    }

    return true;
}

void GameState::handleUserAdmin(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setAdmin(event.b);
}

bool GameState::handleUserAngle(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->setAngle(event.f);

    return true;
}

bool GameState::handleUserAspect(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->setAspect(event.f);

    return true;
}

bool GameState::handleUserBounds(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->setBounds(event.bounds);

    return true;
}

void GameState::handleUserColor(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setColor(event.color);
}

void GameState::handleUserTeams(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setTeams(event.teams);
}

bool GameState::handleUserJoin(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->activate(event.b);

    return true;
}

bool GameState::handleUserLeave(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return false;
    }

    user->deactivate();

    return true;
}

void GameState::handleUserAttach(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->attach(EntityID(event.u));
}

void GameState::handleUserDetach(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->detach();
}

void GameState::handleUserAnchor(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->anchor(event.mapping);
}

void GameState::handleUserUnanchor(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->unanchor();
}

void GameState::handleUserRoomOffset(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setRoomOffset(event.transform);
}

void GameState::handleUserPing(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setPing(event.u);
}

void GameState::handleUserMagnitude(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setMagnitude(event.u);
}

void GameState::handleUserAdvantage(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setAdvantage(event.i);
}

void GameState::handleUserDoom(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setDoom(event.b);
}

void GameState::handleUserTether(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setTether(Tether(event.b, event.f, event.userIDs));
}

void GameState::handleUserConfinement(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setConfinement(Confinement(event.b, event.transform.position(), event.f));
}

void GameState::handleUserFade(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setFade(event.b);
}

void GameState::handleUserWait(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setWait(event.b);
}

void GameState::handleUserSignal(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setSignal(event.b);
}

void GameState::handleUserTitlecard(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    if (event.textRequests.empty())
    {
        user->setTitlecard({});
    }
    else
    {
        user->setTitlecard(event.textRequests.front().data<TextTitlecardRequest>());
    }
}

void GameState::handleUserTexts(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    user->setTexts({ event.textRequests.begin(), event.textRequests.end() });
}

void GameState::handleUserKnowledge(const UserEvent& event)
{
    User* user = getUser(event.id);

    if (!user)
    {
        return;
    }

    vector<TextKnowledgeRequest> knowledge;

    for (const TextRequest& request : event.textRequests)
    {
        knowledge.push_back(request.data<TextKnowledgeRequest>());
    }

    user->setKnowledge(knowledge);
}

void GameState::handleTeamAdd(const TeamEvent& event)
{
    _teams.insert({ event.name, Team(event.name) });
}

void GameState::handleTeamRemove(const TeamEvent& event)
{
    _teams.erase(event.name);
}

void GameState::handleTeamUpdate(const TeamEvent& event)
{
    _teams.insert_or_assign(event.name, Team(event.name, event.userIDs));
}

void GameState::handleSplashMessage(const GameEvent& event)
{
    splashMessage(event.s);
}

void GameState::handleTimeMultiplier(const GameEvent& event)
{
    setTimeMultiplier(event.f);
}

void GameState::handleLockAttachments(const GameEvent& event)
{
    _attachmentsLocked = true;
}

void GameState::handleUnlockAttachments(const GameEvent& event)
{
    _attachmentsLocked = false;
}

bool GameState::handlePing(const GameEvent& event)
{
    return event.b;
}

bool GameState::handleBrake(const GameEvent& event)
{
    _braking = true;
    _brakingID = UserID(event.u);
    brakeElapsed = 0;

    return event.b;
}

void GameState::handleUnbrake(const GameEvent& event)
{
    _braking = false;
    _brakingID = UserID();
    brakeElapsed = 0;
}

void GameState::handleMagnitudeLimit(const GameEvent& event)
{
    _magnitudeLimit = event.u;
}

void GameState::handleNextEntityID(const GameEvent& event)
{
    if (EntityID(event.u) < _nextEntityID)
    {
        return;
    }

    _nextEntityID = EntityID(event.u);
}

void GameState::handleNextTimerID(const GameEvent& event)
{
    if (EntityID(event.u) < nextTimerID)
    {
        return;
    }

    nextTimerID = TimerID(event.u);
}

void GameState::handleNextRepeaterID(const GameEvent& event)
{
    if (EntityID(event.u) < nextRepeaterID)
    {
        return;
    }

    nextRepeaterID = RepeaterID(event.u);
}

void GameState::handleWorldAdd(const WorldEvent& event)
{
    if (_worlds.contains(event.name))
    {
        return;
    }

    addWorld(event.name);
}

void GameState::handleWorldRemove(const WorldEvent& event)
{
    if (!_worlds.contains(event.name))
    {
        return;
    }

    removeWorld(event.name);
}

void GameState::pushWorldEvent(const NetworkEvent& event)
{
    string worldName;

    switch (event.type())
    {
    default :
        error("Non-world/entity event " + event.name() + " was pushed to world.");
        break;
    case Network_Event_World :
        worldName = event.payload<WorldEvent>().name;
        break;
    case Network_Event_World_Sky :
        worldName = event.payload<WorldSkyEvent>().name;
        break;
    case Network_Event_World_Atmosphere :
        worldName = event.payload<WorldAtmosphereEvent>().name;
        break;
    case Network_Event_Entity :
        worldName = event.payload<EntityEvent>().world;
        break;
    case Network_Event_Entity_Body :
        worldName = event.payload<EntityBodyEvent>().world;
        break;
    case Network_Event_Entity_Sample :
        worldName = event.payload<EntitySampleEvent>().world;
        break;
    case Network_Event_Entity_Sample_Playback :
        worldName = event.payload<EntitySamplePlaybackEvent>().world;
        break;
    case Network_Event_Entity_Action :
        worldName = event.payload<EntityActionEvent>().world;
        break;
    case Network_Event_Entity_Action_Playback :
        worldName = event.payload<EntityActionPlaybackEvent>().world;
        break;
    case Network_Event_Entity_Input_Binding :
        worldName = event.payload<EntityInputBindingEvent>().world;
        break;
    case Network_Event_Entity_HUD :
        worldName = event.payload<EntityHUDEvent>().world;
        break;
    case Network_Event_Entity_Script :
        worldName = event.payload<EntityScriptEvent>().world;
        break;
    }

    auto it = _worlds.find(worldName);

    if (it == _worlds.end())
    {
        return;
    }

    World* world = it->second;

    world->push(_context, event);
}
#endif

void GameState::copy(const GameState& rhs)
{
    _nextUserID = rhs._nextUserID;
    _users = rhs._users;
    _teams = rhs._teams;

    _splashMessage = rhs._splashMessage;
    _timeMultiplier = rhs._timeMultiplier;
    _attachmentsLocked = rhs._attachmentsLocked;
    _braking = rhs._braking;
    _brakingID = rhs._brakingID;
    brakeElapsed = rhs.brakeElapsed;
    _magnitudeLimit = rhs._magnitudeLimit;

    _nextEntityID = rhs._nextEntityID;
    _worlds = rhs._worlds;

    nextTimerID = rhs.nextTimerID;
    _timers = rhs._timers;

    nextRepeaterID = rhs.nextRepeaterID;
    _repeaters = rhs._repeaters;

    _context = rhs._context;

    _nextTextID = rhs._nextTextID;
}

void GameState::vrDrop(const User* user)
{
    World* world = getWorld(user->world());

    for (size_t i = 0; i < handCount; i++)
    {
        if (!user->gripStates(i).gripping())
        {
            continue;
        }

        Entity* entity = world->get(user->gripStates(i).entityID());

        if (!entity)
        {
            continue;
        }

        world->clearParent(entity);
    }
}

#if defined(LMOD_SERVER)
void GameState::checkMagnitude(u32 oldMagnitude)
{
    u32 newMagnitude = magnitude();

    if (newMagnitude != oldMagnitude)
    {
        log("magnitude-control-entered", { to_string(newMagnitude) });
    }
}
#endif

template<>
GameState YAMLNode::convert() const
{
    GameState state;

    state._nextUserID = at("nextUserID").as<UserID>();
    state._users = at("users").as<map<UserID, User>>();
    state._teams = at("teams").as<map<string, Team>>();

    state._splashMessage = at("splashMessage").as<string>();
    state._timeMultiplier = at("timeMultiplier").as<f64>();
    state._attachmentsLocked = at("attachmentsLocked").as<bool>();
    state._braking = at("braking").as<bool>();
    state._brakingID = at("brakingID").as<UserID>();
    state.brakeElapsed = at("brakeElapsed").as<f64>();
    state._magnitudeLimit = at("magnitudeLimit").as<u32>();

    state._nextEntityID = at("nextEntityID").as<EntityID>();
    state._worlds = at("worlds").as<map<string, World*>>();

    state.nextTimerID = at("nextTimerID").as<TimerID>();
    state._timers = at("timers").as<map<TimerID, Timer>>();

    state.nextRepeaterID = at("nextRepeaterID").as<RepeaterID>();
    state._repeaters = at("repeaters").as<map<RepeaterID, Repeater>>();

    ScriptContext* context = at("context").as<ScriptContext*>();

    delete state._context;
    state._context = new ScriptContext(&state, context);

    delete context;

    return state;
}

template<>
void YAMLSerializer::emit(GameState v)
{
    startMapping();

    emitPair("nextUserID", v._nextUserID);
    emitPair("users", v._users);
    emitPair("teams", v._teams);

    emitPair("splashMessage", v._splashMessage);
    emitPair("timeMultiplier", v._timeMultiplier);
    emitPair("attachmentsLocked", v._attachmentsLocked);
    emitPair("braking", v._braking);
    emitPair("brakingID", v._brakingID);
    emitPair("brakeElapsed", v.brakeElapsed);
    emitPair("magnitudeLimit", v._magnitudeLimit);

    emitPair("nextEntityID", v._nextEntityID);
    emitPair("worlds", v._worlds);

    emitPair("nextTimerID", v.nextTimerID);
    emitPair("timers", v.timers());

    emitPair("nextRepeaterID", v.nextRepeaterID);
    emitPair("repeaters", v.repeaters());

    emitPair("context", v.context());

    endMapping();
}
