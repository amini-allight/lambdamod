/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include <cmath>
#include <cstdint>
#include <cfloat>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>
#include <fstream>
#include <streambuf>
#include <chrono>
#include <thread>
#include <functional>
#include <optional>
#include <variant>
#include <deque>
#include <stack>
#include <mutex>
#include <shared_mutex>
#include <filesystem>
#include <random>
#include <regex>
#include <valarray>
#include <complex>
#include <numbers>

#include <msgpack.hpp>

// Windows defines these in the global namespace for some reason
#undef near
#undef far

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

template<typename T>
struct vec2_t;
template<typename T>
struct vec3_t;
template<typename T>
struct vec4_t;

#pragma pack(1)
template<typename T>
struct vec2_t
{
    constexpr vec2_t()
    {
        x = 0;
        y = 0;
    }

    template<typename OtherT>
    constexpr vec2_t(vec2_t<OtherT> v)
    {
        x = v.x;
        y = v.y;
    }

    explicit constexpr vec2_t(T v)
    {
        x = v;
        y = v;
    }

    constexpr vec2_t(T x, T y)
    {
        this->x = x;
        this->y = y;
    }

    T x;
    T y;

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            x,
            y
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        x = std::get<0>(payload);
        y = std::get<1>(payload);
    }

    typedef msgpack::type::tuple<T, T> layout;

    constexpr vec2_t operator+() const
    {
        return *this;
    }

    constexpr vec2_t operator-() const
    {
        return {
            -x,
            -y
        };
    }

    constexpr vec2_t operator+(const vec2_t& rhs) const
    {
        return {
            x + rhs.x,
            y + rhs.y
        };
    }

    constexpr vec2_t operator+(T v) const
    {
        return *this + vec2_t(v);
    }

    constexpr vec2_t& operator+=(const vec2_t& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec2_t& operator+=(T v)
    {
        *this = *this + vec2_t(v);

        return *this;
    }

    constexpr vec2_t operator-(const vec2_t& rhs) const
    {
        return {
            x - rhs.x,
            y - rhs.y
        };
    }

    constexpr vec2_t operator-(T v) const
    {
        return *this - vec2_t(v);
    }

    constexpr vec2_t& operator-=(const vec2_t& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec2_t& operator-=(T v)
    {
        *this = *this - vec2_t(v);

        return *this;
    }

    constexpr vec2_t operator*(const vec2_t& rhs) const
    {
        return {
            x * rhs.x,
            y * rhs.y
        };
    }

    constexpr vec2_t operator*(T v) const
    {
        return *this * vec2_t(v);
    }

    constexpr vec2_t& operator*=(const vec2_t& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec2_t& operator*=(T v)
    {
        *this = *this * vec2_t(v);

        return *this;
    }

    constexpr vec2_t operator/(const vec2_t& rhs) const
    {
        return {
            x / rhs.x,
            y / rhs.y
        };
    }

    constexpr vec2_t operator/(T v) const
    {
        return *this / vec2_t(v);
    }

    constexpr vec2_t& operator/=(const vec2_t& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec2_t& operator/=(T v)
    {
        *this = *this / vec2_t(v);

        return *this;
    }

    constexpr bool operator==(const vec2_t& rhs) const
    {
        return x == rhs.x && y == rhs.y;
    }

    constexpr bool operator!=(const vec2_t& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr bool operator>(const vec2_t& rhs) const
    {
        if (x > rhs.x)
        {
            return true;
        }
        else if (x < rhs.x)
        {
            return false;
        }
        else
        {
            if (y > rhs.y)
            {
                return true;
            }
            else if (y < rhs.y)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }

    constexpr bool operator<(const vec2_t& rhs) const
    {
        if (x < rhs.x)
        {
            return true;
        }
        else if (x > rhs.x)
        {
            return false;
        }
        else
        {
            if (y < rhs.y)
            {
                return true;
            }
            else if (y > rhs.y)
            {
                return false;
            }
            else
            {
                return false;
            }
        }
    }

    constexpr bool operator>=(const vec2_t& rhs) const
    {
        return *this > rhs || *this == rhs;
    }

    constexpr bool operator<=(const vec2_t& rhs) const
    {
        return *this < rhs || *this == rhs;
    }

    constexpr std::strong_ordering operator<=>(const vec2_t& rhs) const
    {
        if (*this == rhs)
        {
            return std::strong_ordering::equal;
        }
        else if (*this > rhs)
        {
            return std::strong_ordering::greater;
        }
        else
        {
            return std::strong_ordering::less;
        }
    }

    constexpr T dot(const vec2_t& rhs) const
    {
        return x*rhs.x + y*rhs.y;
    }

    constexpr T length() const
    {
        return sqrt(dot(*this));
    }

    constexpr vec2_t normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr T distance(const vec2_t& rhs) const
    {
        return (*this - rhs).length();
    }

    constexpr vec2_t rotate(T angle) const
    {
        T s = sin(angle);
        T c = cos(angle);

        return {
            x * c - y * s,
            x * s + y * c
        };
    }
};

template<typename T>
constexpr vec2_t<T> operator+(T lhs, const vec2_t<T>& rhs)
{
    return {
        lhs + rhs.x,
        lhs + rhs.y
    };
}

template<typename T>
constexpr vec2_t<T> operator-(T lhs, const vec2_t<T>& rhs)
{
    return {
        lhs - rhs.x,
        lhs - rhs.y
    };
}

template<typename T>
constexpr vec2_t<T> operator*(T lhs, const vec2_t<T>& rhs)
{
    return {
        lhs * rhs.x,
        lhs * rhs.y
    };
}

template<typename T>
constexpr vec2_t<T> operator/(T lhs, const vec2_t<T>& rhs)
{
    return {
        lhs / rhs.x,
        lhs / rhs.y
    };
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const vec2_t<T>& rhs)
{
    out << "(" << rhs.x << ", " << rhs.y << ")";

    return out;
}

template<typename T>
constexpr vec2_t<T> abs(vec2_t<T> v)
{
    return vec2_t<T>(
        std::abs(v.x),
        std::abs(v.y)
    );
}

template<typename T>
constexpr vec2_t<T> floor(vec2_t<T> v)
{
    return vec2_t<T>(
        std::floor(v.x),
        std::floor(v.y)
    );
}

template<typename T>
constexpr vec2_t<T> ceil(vec2_t<T> v)
{
    return vec2_t<T>(
        std::ceil(v.x),
        std::ceil(v.y)
    );
}

template<typename T>
constexpr vec2_t<T> pow(vec2_t<T> v, T n)
{
    return vec2_t<T>(
        std::pow(v.x, n),
        std::pow(v.y, n)
    );
}

template<typename T>
constexpr vec2_t<T> sq(vec2_t<T> v)
{
    return vec2_t<T>(
        std::pow(v.x, 2),
        std::pow(v.y, 2)
    );
}

template<typename T>
constexpr vec2_t<T> sqrt(vec2_t<T> v)
{
    return vec2_t<T>(
        std::sqrt(v.x),
        std::sqrt(v.y)
    );
}

template<typename T>
struct vec3_t
{
    constexpr vec3_t()
    {
        x = 0;
        y = 0;
        z = 0;
    }

    template<typename OtherT>
    constexpr vec3_t(vec3_t<OtherT> v)
    {
        x = v.x;
        y = v.y;
        z = v.z;
    }

    explicit constexpr vec3_t(T v)
    {
        x = v;
        y = v;
        z = v;
    }

    constexpr vec3_t(T x, T y, T z)
    {
        this->x = x;
        this->y = y;
        this->z = z;
    }

    constexpr vec3_t(const vec2_t<T>& v, T z)
    {
        this->x = v.x;
        this->y = v.y;
        this->z = z;
    }

    T x;
    T y;
    T z;

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            x,
            y,
            z
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        x = std::get<0>(payload);
        y = std::get<1>(payload);
        z = std::get<2>(payload);
    }

    typedef msgpack::type::tuple<T, T, T> layout;

    constexpr vec3_t operator+() const
    {
        return *this;
    }

    constexpr vec3_t operator-() const
    {
        return {
            -x,
            -y,
            -z
        };
    }

    constexpr vec3_t operator+(const vec3_t& rhs) const
    {
        return {
            x + rhs.x,
            y + rhs.y,
            z + rhs.z
        };
    }

    constexpr vec3_t operator+(T v) const
    {
        return *this + vec3_t(v);
    }

    constexpr vec3_t& operator+=(const vec3_t& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec3_t& operator+=(T v)
    {
        *this = *this + vec3_t(v);

        return *this;
    }

    constexpr vec3_t operator-(const vec3_t& rhs) const
    {
        return {
            x - rhs.x,
            y - rhs.y,
            z - rhs.z
        };
    }

    constexpr vec3_t operator-(T v) const
    {
        return *this - vec3_t(v);
    }

    constexpr vec3_t& operator-=(const vec3_t& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec3_t& operator-=(T v)
    {
        *this = *this - vec3_t(v);

        return *this;
    }

    constexpr vec3_t operator*(const vec3_t& rhs) const
    {
        return {
            x * rhs.x,
            y * rhs.y,
            z * rhs.z
        };
    }

    constexpr vec3_t operator*(T v) const
    {
        return *this * vec3_t(v);
    }

    constexpr vec3_t& operator*=(const vec3_t& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec3_t& operator*=(T v)
    {
        *this = *this * vec3_t(v);

        return *this;
    }

    constexpr vec3_t operator/(const vec3_t& rhs) const
    {
        return {
            x / rhs.x,
            y / rhs.y,
            z / rhs.z
        };
    }

    constexpr vec3_t operator/(T v) const
    {
        return *this / vec3_t(v);
    }

    constexpr vec3_t& operator/=(const vec3_t& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec3_t& operator/=(T v)
    {
        *this = *this / vec3_t(v);

        return *this;
    }

    constexpr bool operator==(const vec3_t& rhs) const
    {
        return x == rhs.x && y == rhs.y && z == rhs.z;
    }

    constexpr bool operator!=(const vec3_t& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr bool operator>(const vec3_t& rhs) const
    {
        if (x > rhs.x)
        {
            return true;
        }
        else if (x < rhs.x)
        {
            return false;
        }
        else
        {
            if (y > rhs.y)
            {
                return true;
            }
            else if (y < rhs.y)
            {
                return false;
            }
            else
            {
                if (z > rhs.z)
                {
                    return true;
                }
                else if (z < rhs.z)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    constexpr bool operator<(const vec3_t& rhs) const
    {
        if (x < rhs.x)
        {
            return true;
        }
        else if (x > rhs.x)
        {
            return false;
        }
        else
        {
            if (y < rhs.y)
            {
                return true;
            }
            else if (y > rhs.y)
            {
                return false;
            }
            else
            {
                if (z < rhs.z)
                {
                    return true;
                }
                else if (z > rhs.z)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    constexpr bool operator>=(const vec3_t& rhs) const
    {
        return *this > rhs || *this == rhs;
    }

    constexpr bool operator<=(const vec3_t& rhs) const
    {
        return *this < rhs || *this == rhs;
    }

    constexpr std::strong_ordering operator<=>(const vec3_t& rhs) const
    {
        if (*this == rhs)
        {
            return std::strong_ordering::equal;
        }
        else if (*this > rhs)
        {
            return std::strong_ordering::greater;
        }
        else
        {
            return std::strong_ordering::less;
        }
    }

    constexpr T dot(const vec3_t& rhs) const
    {
        return x*rhs.x + y*rhs.y + z*rhs.z;
    }

    constexpr vec3_t cross(const vec3_t& rhs) const
    {
        return {
            y*rhs.z - rhs.y*z,
            z*rhs.x - rhs.z*x,
            x*rhs.y - rhs.x*y
        };
    }

    constexpr T length() const
    {
        return sqrt(dot(*this));
    }

    constexpr vec3_t normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr T distance(const vec3_t& rhs) const
    {
        return (*this - rhs).length();
    }

    constexpr vec2_t<T> toVec2() const
    {
        return { x, y };
    }
};

template<typename T>
constexpr vec3_t<T> operator+(T lhs, const vec3_t<T>& rhs)
{
    return {
        lhs + rhs.x,
        lhs + rhs.y,
        lhs + rhs.z
    };
}

template<typename T>
constexpr vec3_t<T> operator-(T lhs, const vec3_t<T>& rhs)
{
    return {
        lhs - rhs.x,
        lhs - rhs.y,
        lhs - rhs.z
    };
}

template<typename T>
constexpr vec3_t<T> operator*(T lhs, const vec3_t<T>& rhs)
{
    return {
        lhs * rhs.x,
        lhs * rhs.y,
        lhs * rhs.z
    };
}

template<typename T>
constexpr vec3_t<T> operator/(T lhs, const vec3_t<T>& rhs)
{
    return {
        lhs / rhs.x,
        lhs / rhs.y,
        lhs / rhs.z
    };
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const vec3_t<T>& rhs)
{
    out << "(" << rhs.x << ", " << rhs.y << ", " << rhs.z << ")";

    return out;
}

template<typename T>
struct vec4_t
{
    constexpr vec4_t()
    {
        x = 0;
        y = 0;
        z = 0;
        w = 0;
    }

    template<typename OtherT>
    constexpr vec4_t(vec4_t<OtherT> v)
    {
        x = v.x;
        y = v.y;
        z = v.z;
        w = v.w;
    }

    explicit constexpr vec4_t(T v)
    {
        x = v;
        y = v;
        z = v;
        w = v;
    }

    constexpr vec4_t(T x, T y, T z, T w)
    {
        this->x = x;
        this->y = y;
        this->z = z;
        this->w = w;
    }

    constexpr vec4_t(const vec2_t<T>& v, T z, T w)
    {
        x = v.x;
        y = v.y;
        this->z = z;
        this->w = w;
    }

    constexpr vec4_t(const vec3_t<T>& v, T w)
    {
        x = v.x;
        y = v.y;
        z = v.z;
        this->w = w;
    }

    T x;
    T y;
    T z;
    T w;

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            x,
            y,
            z,
            w
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        x = std::get<0>(payload);
        y = std::get<1>(payload);
        z = std::get<2>(payload);
        w = std::get<3>(payload);
    }

    typedef msgpack::type::tuple<T, T, T, T> layout;

    constexpr vec4_t operator+() const
    {
        return *this;
    }

    constexpr vec4_t operator-() const
    {
        return {
            -x,
            -y,
            -z,
            -w
        };
    }

    constexpr vec4_t operator+(const vec4_t& rhs) const
    {
        return {
            x + rhs.x,
            y + rhs.y,
            z + rhs.z,
            w + rhs.w
        };
    }

    constexpr vec4_t operator+(T v) const
    {
        return *this + vec4_t(v);
    }

    constexpr vec4_t& operator+=(const vec4_t& rhs)
    {
        *this = *this + rhs;

        return *this;
    }

    constexpr vec4_t& operator+=(T v)
    {
        *this = *this + vec4_t(v);

        return *this;
    }

    constexpr vec4_t operator-(const vec4_t& rhs) const
    {
        return {
            x - rhs.x,
            y - rhs.y,
            z - rhs.z,
            w - rhs.w
        };
    }

    constexpr vec4_t operator-(T v) const
    {
        return *this - vec4_t(v);
    }

    constexpr vec4_t& operator-=(const vec4_t& rhs)
    {
        *this = *this - rhs;

        return *this;
    }

    constexpr vec4_t& operator-=(T v)
    {
        *this = *this - vec4_t(v);

        return *this;
    }

    constexpr vec4_t operator*(const vec4_t& rhs) const
    {
        return {
            x * rhs.x,
            y * rhs.y,
            z * rhs.z,
            w * rhs.w
        };
    }

    constexpr vec4_t operator*(T v) const
    {
        return *this * vec4_t(v);
    }

    constexpr vec4_t& operator*=(const vec4_t& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr vec4_t& operator*=(T v)
    {
        *this = *this * vec4_t(v);

        return *this;
    }

    constexpr vec4_t operator/(const vec4_t& rhs) const
    {
        return {
            x / rhs.x,
            y / rhs.y,
            z / rhs.z,
            w / rhs.w
        };
    }

    constexpr vec4_t operator/(T v) const
    {
        return *this / vec4_t(v);
    }

    constexpr vec4_t& operator/=(const vec4_t& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr vec4_t& operator/=(T v)
    {
        *this = *this / vec4_t(v);

        return *this;
    }

    constexpr bool operator==(const vec4_t& rhs) const
    {
        return x == rhs.x && y == rhs.y && z == rhs.z && w == rhs.w;
    }

    constexpr bool operator!=(const vec4_t& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr bool operator>(const vec4_t& rhs) const
    {
        if (x > rhs.x)
        {
            return true;
        }
        else if (x < rhs.x)
        {
            return false;
        }
        else
        {
            if (y > rhs.y)
            {
                return true;
            }
            else if (y < rhs.y)
            {
                return false;
            }
            else
            {
                if (z > rhs.z)
                {
                    return true;
                }
                else if (z < rhs.z)
                {
                    return false;
                }
                else
                {
                    if (w > rhs.w)
                    {
                        return true;
                    }
                    else if (w < rhs.w)
                    {
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }

    constexpr bool operator<(const vec4_t& rhs) const
    {
        if (x < rhs.x)
        {
            return true;
        }
        else if (x > rhs.x)
        {
            return false;
        }
        else
        {
            if (y < rhs.y)
            {
                return true;
            }
            else if (y > rhs.y)
            {
                return false;
            }
            else
            {
                if (z < rhs.z)
                {
                    return true;
                }
                else if (z > rhs.z)
                {
                    return false;
                }
                else
                {
                    if (w < rhs.w)
                    {
                        return true;
                    }
                    else if (w > rhs.w)
                    {
                        return false;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
        }
    }

    constexpr bool operator>=(const vec4_t& rhs) const
    {
        return *this > rhs || *this == rhs;
    }

    constexpr bool operator<=(const vec4_t& rhs) const
    {
        return *this < rhs || *this == rhs;
    }

    constexpr std::strong_ordering operator<=>(const vec4_t& rhs) const
    {
        if (*this == rhs)
        {
            return std::strong_ordering::equal;
        }
        else if (*this > rhs)
        {
            return std::strong_ordering::greater;
        }
        else
        {
            return std::strong_ordering::less;
        }
    }

    constexpr T dot(const vec4_t& rhs) const
    {
        return x*rhs.x + y*rhs.y + z*rhs.z + w*rhs.w;
    }

    constexpr T length() const
    {
        return sqrt(dot(*this));
    }

    constexpr vec4_t normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        return *this / length();
    }

    constexpr T distance(const vec4_t& rhs) const
    {
        return (*this - rhs).length();
    }

    constexpr vec2_t<T> toVec2() const
    {
        return { x, y };
    }

    constexpr vec3_t<T> toVec3() const
    {
        return { x, y, z };
    }
};

template<typename T>
constexpr vec4_t<T> operator+(T lhs, const vec4_t<T>& rhs)
{
    return {
        lhs + rhs.x,
        lhs + rhs.y
    };
}

template<typename T>
constexpr vec4_t<T> operator-(T lhs, const vec4_t<T>& rhs)
{
    return {
        lhs - rhs.x,
        lhs - rhs.y
    };
}

template<typename T>
constexpr vec4_t<T> operator*(T lhs, const vec4_t<T>& rhs)
{
    return {
        lhs * rhs.x,
        lhs * rhs.y
    };
}

template<typename T>
constexpr vec4_t<T> operator/(T lhs, const vec4_t<T>& rhs)
{
    return {
        lhs / rhs.x,
        lhs / rhs.y
    };
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const vec4_t<T>& rhs)
{
    out << "(" << rhs.x << ", " << rhs.y << ", " << rhs.z << ", " << rhs.w << ")";

    return out;
}

template<typename T>
constexpr vec4_t<T> abs(vec4_t<T> v)
{
    return vec4_t<T>(
        std::abs(v.x),
        std::abs(v.y),
        std::abs(v.z),
        std::abs(v.w)
    );
}

template<typename T>
constexpr vec4_t<T> floor(vec4_t<T> v)
{
    return vec4_t<T>(
        std::floor(v.x),
        std::floor(v.y),
        std::floor(v.z),
        std::floor(v.w)
    );
}

template<typename T>
constexpr vec4_t<T> ceil(vec4_t<T> v)
{
    return vec4_t<T>(
        std::ceil(v.x),
        std::ceil(v.y),
        std::ceil(v.z),
        std::ceil(v.w)
    );
}

template<typename T>
constexpr vec4_t<T> pow(vec4_t<T> v, T n)
{
    return vec4_t<T>(
        std::pow(v.x, n),
        std::pow(v.y, n),
        std::pow(v.z, n),
        std::pow(v.w, n)
    );
}

template<typename T>
constexpr vec4_t<T> sq(vec4_t<T> v)
{
    return vec4_t<T>(
        std::pow(v.x, 2),
        std::pow(v.y, 2),
        std::pow(v.z, 2),
        std::pow(v.w, 2)
    );
}

template<typename T>
constexpr vec4_t<T> sqrt(vec4_t<T> v)
{
    return vec4_t<T>(
        std::sqrt(v.x),
        std::sqrt(v.y),
        std::sqrt(v.z),
        std::sqrt(v.w)
    );
}

static constexpr vec3_t<f64> upDir = { 0, 0, 1 };
static constexpr vec3_t<f64> downDir = { 0, 0, -1 };
static constexpr vec3_t<f64> forwardDir = { 0, 1, 0 };
static constexpr vec3_t<f64> backDir = { 0, -1, 0 };
static constexpr vec3_t<f64> rightDir = { 1, 0, 0 };
static constexpr vec3_t<f64> leftDir = { -1, 0, 0 };

template<typename T>
struct quaternion_t
{
    constexpr quaternion_t()
    {
        w = 1;
        x = 0;
        y = 0;
        z = 0;
    }

    template<typename OtherT>
    constexpr quaternion_t(quaternion_t<OtherT> v)
    {
        w = v.w;
        x = v.x;
        y = v.y;
        z = v.z;
    }

    constexpr quaternion_t(T w, T x, T y, T z)
    {
        this->w = w;
        this->x = x;
        this->y = y;
        this->z = z;
    }

    constexpr quaternion_t(const vec3_t<T>& axis, T angle)
    {
        T c = sin(angle / 2.0);

        w = cos(angle / 2.0);
        x = axis.x * c;
        y = axis.y * c;
        z = axis.z * c;
    }

    explicit constexpr quaternion_t(const vec3_t<T>& euler)
    {
        T cx = cos(euler.x * 0.5);
        T sx = sin(euler.x * 0.5);

        T cy = cos(euler.y * 0.5);
        T sy = sin(euler.y * 0.5);

        T cz = cos(euler.z * 0.5);
        T sz = sin(euler.z * 0.5);

        w = cz * cx * cy + sz * sx * sy;
        x = cz * sx * cy - sz * cx * sy;
        y = cz * cx * sy + sz * sx * cy;
        z = sz * cx * cy - cz * sx * sy;
    }

    constexpr quaternion_t(const vec3_t<T>& direction, const vec3_t<T>& roll)
    {
        vec3_t<T> d = direction.normalize();
        vec3_t<T> r = roll.normalize();

        T m[3][3]{
            { (r.z*d.y - r.y*d.z), d.x, r.x },
            { (r.x*d.z - r.z*d.x), d.y, r.y },
            { (r.y*d.x - r.x*d.y), d.z, r.z }
        };
        T trace = m[0][0] + m[1][1] + m[2][2];

        if (trace > 0)
        {
            T s = 0.5 / sqrt(trace + 1.0);
            w = 0.25 / s;
            x = (m[2][1] - m[1][2]) * s;
            y = (m[0][2] - m[2][0]) * s;
            z = (m[1][0] - m[0][1]) * s;
        }
        else
        {
            if (m[0][0] > m[1][1] && m[0][0] > m[2][2])
            {
                T s = 2.0 * sqrt(1.0 + m[0][0] - m[1][1] - m[2][2]);
                w = (m[2][1] - m[1][2]) / s;
                x = 0.25 * s;
                y = (m[0][1] + m[1][0]) / s;
                z = (m[0][2] + m[2][0]) / s;
            }
            else if (m[1][1] > m[2][2])
            {
                T s = 2.0 * sqrt(1.0 + m[1][1] - m[0][0] - m[2][2]);
                w = (m[0][2] - m[2][0]) / s;
                x = (m[0][1] + m[1][0]) / s;
                y = 0.25 * s;
                z = (m[1][2] + m[2][1]) / s;
            }
            else
            {
                T s = 2.0 * sqrt(1.0 + m[2][2] - m[0][0] - m[1][1]);
                w = (m[1][0] - m[0][1]) / s;
                x = (m[0][2] + m[2][0]) / s;
                y = (m[1][2] + m[2][1]) / s;
                z = 0.25 * s;
            }
        }
    }

    explicit constexpr quaternion_t(const vec4_t<T>& rhs)
        : w(rhs.w)
        , x(rhs.x)
        , y(rhs.y)
        , z(rhs.z)
    {

    }

    T w;
    T x;
    T y;
    T z;

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            w,
            x,
            y,
            z
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        w = std::get<0>(payload);
        x = std::get<1>(payload);
        y = std::get<2>(payload);
        z = std::get<3>(payload);
    }

    typedef msgpack::type::tuple<T, T, T, T> layout;

    constexpr quaternion_t operator*(T x) const
    {
        vec3_t<T> axis = this->axis();
        T angle = this->angle();

        angle *= x;

        return quaternion_t(axis, angle);
    }

    constexpr quaternion_t& operator*=(T x)
    {
        vec3_t<T> axis = this->axis();
        T angle = this->angle();

        angle *= x;

        *this = quaternion_t(axis, angle);

        return *this;
    }

    constexpr quaternion_t operator*(const quaternion_t& rhs) const
    {
        return {
            w*rhs.w - x*rhs.x - y*rhs.y - z*rhs.z,
            w*rhs.x + x*rhs.w + y*rhs.z - z*rhs.y,
            w*rhs.y - x*rhs.z + y*rhs.w + z*rhs.x,
            w*rhs.z + x*rhs.y - y*rhs.x + z*rhs.w
        };
    }

    constexpr quaternion_t& operator*=(const quaternion_t& rhs)
    {
        *this = *this * rhs;

        return *this;
    }

    constexpr quaternion_t operator/(const quaternion_t& rhs) const
    {
        return *this * rhs.inverse();
    }

    constexpr quaternion_t& operator/=(const quaternion_t& rhs)
    {
        *this = *this / rhs;

        return *this;
    }

    constexpr bool operator==(const quaternion_t& rhs) const
    {
        return w == rhs.w &&
            x == rhs.x &&
            y == rhs.y &&
            z == rhs.z;
    }

    constexpr bool operator!=(const quaternion_t& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr quaternion_t inverse() const
    {
        return {
            w,
            x * -1,
            y * -1,
            z * -1
        };
    }

    constexpr vec3_t<T> rotate(const vec3_t<T>& p) const
    {
        quaternion_t result = *this * quaternion_t(0, p.x, p.y, p.z) * inverse();

        return vec3_t<T>(
            result.x,
            result.y,
            result.z
        );
    }

    constexpr vec3_t<T> forward() const
    {
        return rotate(forwardDir);
    }

    constexpr vec3_t<T> back() const
    {
        return rotate(backDir);
    }

    constexpr vec3_t<T> up() const
    {
        return rotate(upDir);
    }

    constexpr vec3_t<T> down() const
    {
        return rotate(downDir);
    }

    constexpr vec3_t<T> right() const
    {
        return rotate(rightDir);
    }

    constexpr vec3_t<T> left() const
    {
        return rotate(leftDir);
    }

    constexpr vec3_t<T> axis() const
    {
        T c = sqrt(1 - pow(w, 2));

        return {
            x / c,
            y / c,
            z / c
        };
    }

    constexpr T angle() const
    {
        return 2 * acos(w);
    }

    constexpr vec3_t<T> euler() const
    {
        vec3_t<T> v;

        v.x = atan2(2*x*w - 2*y*z , 1 - 2*x*x - 2*z*z);
        v.y = atan2(2*y*w - 2*x*z , 1 - 2*y*y - 2*z*z);
        v.z = asin(2*x*y + 2*z*w);

        return v;
    }

    constexpr T length() const
    {
        return sqrt(pow(w, 2) + pow(x, 2) + pow(y, 2) + pow(z, 2));
    }

    constexpr quaternion_t normalize() const
    {
        if (length() == 0)
        {
            return *this;
        }

        T l = length();

        return {
            w / l,
            x / l,
            y / l,
            z / l
        };
    }

    constexpr vec4_t<T> toVec4() const
    {
        return { x, y, z, w };
    }
};

template<typename T>
std::ostream& operator<<(std::ostream& out, const quaternion_t<T>& rhs)
{
    out << "(" << rhs.w << ", " << rhs.x << ", " << rhs.y << ", " << rhs.z << ")";

    return out;
}

template<typename T>
struct Velocities_t
{
    constexpr Velocities_t()
    {

    }

    constexpr Velocities_t(const vec3_t<T>& linV, const vec3_t<T>& angV)
    {
        linearVelocity = linV;
        angularVelocity = angV;
    }

    vec3_t<T> linearVelocity;
    vec3_t<T> angularVelocity;
};

template<typename T>
struct mat3_t
{
    constexpr mat3_t()
        : v{{0}}
    {
        v[0][0] = 1;
        v[1][1] = 1;
        v[2][2] = 1;
    }

    template<typename OtherT>
    constexpr mat3_t(mat3_t<OtherT> v)
    {
        for (size_t i = 0; i < 3; i++)
        {
            for (size_t j = 0; j < 3; j++)
            {
                this->v[i][j] = v.v[i][j];
            }
        }
    }

    constexpr mat3_t(const quaternion_t<T>& q)
        : mat3_t()
    {
        v[0][0] = 1 - 2*q.y*q.y - 2*q.z*q.z;
        v[0][1] = 2 * (q.x*q.y - q.w*q.z);
        v[0][2] = 2 * (q.w*q.y + q.x*q.z);

        v[1][0] = 2 * (q.x*q.y + q.w*q.z);
        v[1][1] = 1 - 2*q.x*q.x - 2*q.z*q.z;
        v[1][2] = 2 * (q.y*q.z - q.w*q.x);

        v[2][0] = 2 * (q.x*q.z - q.w*q.y);
        v[2][1] = 2 * (q.w*q.x + q.y*q.z);
        v[2][2] = 1 - 2*q.x*q.x - 2*q.y*q.y;
    }

    constexpr mat3_t(
        T v00, T v01, T v02,
        T v10, T v11, T v12,
        T v20, T v21, T v22
    )
        : mat3_t()
    {
        v[0][0] = v00;
        v[0][1] = v01;
        v[0][2] = v02;

        v[1][0] = v10;
        v[1][1] = v11;
        v[1][2] = v12;

        v[2][0] = v20;
        v[2][1] = v21;
        v[2][2] = v22;
    }

    T v[3][3];

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            v[0][0], v[0][1], v[0][2],
            v[1][0], v[1][1], v[1][2],
            v[2][0], v[2][1], v[2][2]
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        v[0][0] = std::get<0>(payload);
        v[0][1] = std::get<1>(payload);
        v[0][2] = std::get<2>(payload);
        v[1][0] = std::get<3>(payload);
        v[1][1] = std::get<4>(payload);
        v[1][2] = std::get<5>(payload);
        v[2][0] = std::get<6>(payload);
        v[2][1] = std::get<7>(payload);
        v[2][2] = std::get<8>(payload);
    }

    typedef msgpack::type::tuple<T, T, T, T, T, T, T, T, T> layout;

    constexpr bool operator==(const mat3_t& rhs) const
    {
        for (size_t i = 0; i < 3; i++)
        {
            for (size_t j = 0; j < 3; j++)
            {
                if (v[i][j] != rhs.v[i][j])
                {
                    return false;
                }
            }
        }

        return true;
    }

    constexpr bool operator!=(const mat3_t& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr mat3_t operator*(const mat3_t& rhs) const
    {
        mat3_t m;

        vec3_t<T> r0(rhs.v[0][0], rhs.v[0][1], rhs.v[0][2]);
        vec3_t<T> r1(rhs.v[1][0], rhs.v[1][1], rhs.v[1][2]);
        vec3_t<T> r2(rhs.v[2][0], rhs.v[2][1], rhs.v[2][2]);

        vec3_t<T> c0(v[0][0], v[1][0], v[2][0]);
        vec3_t<T> c1(v[0][1], v[1][1], v[2][1]);
        vec3_t<T> c2(v[0][2], v[1][2], v[2][2]);

        m.v[0][0] = r0.dot(c0);
        m.v[0][1] = r0.dot(c1);
        m.v[0][2] = r0.dot(c2);

        m.v[1][0] = r1.dot(c0);
        m.v[1][1] = r1.dot(c1);
        m.v[1][2] = r1.dot(c2);

        m.v[2][0] = r2.dot(c0);
        m.v[2][1] = r2.dot(c1);
        m.v[2][2] = r2.dot(c2);

        return m;
    }

    constexpr mat3_t& operator*=(const mat3_t& rhs)
    {
        *this = (*this) * rhs;

        return *this;
    }

    constexpr T determinant() const
    {
        return v[0][0] * (v[1][1] * v[2][2] - v[2][1] * v[1][2])
            - v[0][1] * (v[1][0] * v[2][2] - v[1][2] * v[2][0])
            + v[0][2] * (v[1][0] * v[2][1] - v[1][1] * v[2][0]);
    }

    constexpr mat3_t inverse() const
    {
        T idet = 1 / determinant();

        return mat3(
            (v[1][1] * v[2][2] - v[2][1] * v[1][2]) * idet,
            (v[0][2] * v[2][1] - v[0][1] * v[2][2]) * idet,
            (v[0][1] * v[1][2] - v[0][2] * v[1][1]) * idet,
            (v[1][2] * v[2][0] - v[1][0] * v[2][2]) * idet,
            (v[0][0] * v[2][2] - v[0][2] * v[2][0]) * idet,
            (v[1][0] * v[0][2] - v[0][0] * v[1][2]) * idet,
            (v[1][0] * v[2][1] - v[2][0] * v[1][1]) * idet,
            (v[2][0] * v[0][1] - v[0][0] * v[2][1]) * idet,
            (v[0][0] * v[1][1] - v[1][0] * v[0][1]) * idet
        );

    }

    constexpr mat3_t invert() const
    {
        mat3_t result;

        result.v[0][0] = v[0][0];
        result.v[0][1] = v[1][0];
        result.v[0][2] = v[2][0];

        result.v[1][0] = v[0][1];
        result.v[1][1] = v[1][1];
        result.v[1][2] = v[2][1];

        result.v[2][0] = v[0][2];
        result.v[2][1] = v[1][2];
        result.v[2][2] = v[2][2];

        return result;
    }

    constexpr vec3_t<T> row(size_t index) const
    {
        return vec3_t<T>(v[index][0], v[index][1], v[index][2]);
    }

    constexpr void row(size_t index, const vec3_t<T>& v)
    {
        this->v[index][0] = v.x;
        this->v[index][1] = v.y;
        this->v[index][2] = v.z;
    }

    constexpr vec3_t<T> column(size_t index) const
    {
        return vec3_t<T>(v[0][index], v[1][index], v[2][index]);
    }

    constexpr void column(size_t index, const vec3_t<T>& v)
    {
        this->v[0][index] = v.x;
        this->v[1][index] = v.y;
        this->v[2][index] = v.z;
    }
};

template<typename T>
struct mat4_t
{
    constexpr mat4_t()
        : v{{0}}
    {
        v[0][0] = 1;
        v[1][1] = 1;
        v[2][2] = 1;
        v[3][3] = 1;
    }

    template<typename OtherT>
    constexpr mat4_t(mat4_t<OtherT> v)
    {
        for (size_t i = 0; i < 4; i++)
        {
            for (size_t j = 0; j < 4; j++)
            {
                this->v[i][j] = v.v[i][j];
            }
        }
    }

    constexpr mat4_t(const quaternion_t<T>& q)
        : mat4_t()
    {
        v[0][0] = 1 - 2*q.y*q.y - 2*q.z*q.z;
        v[0][1] = 2 * (q.x*q.y - q.w*q.z);
        v[0][2] = 2 * (q.w*q.y + q.x*q.z);

        v[1][0] = 2 * (q.x*q.y + q.w*q.z);
        v[1][1] = 1 - 2*q.x*q.x - 2*q.z*q.z;
        v[1][2] = 2 * (q.y*q.z - q.w*q.x);

        v[2][0] = 2 * (q.x*q.z - q.w*q.y);
        v[2][1] = 2 * (q.w*q.x + q.y*q.z);
        v[2][2] = 1 - 2*q.x*q.x - 2*q.y*q.y;
    }

    constexpr mat4_t(
        T v00, T v01, T v02, T v03,
        T v10, T v11, T v12, T v13,
        T v20, T v21, T v22, T v23,
        T v30, T v31, T v32, T v33
    )
        : mat4_t()
    {
        v[0][0] = v00;
        v[0][1] = v01;
        v[0][2] = v02;
        v[0][3] = v03;

        v[1][0] = v10;
        v[1][1] = v11;
        v[1][2] = v12;
        v[1][3] = v13;

        v[2][0] = v20;
        v[2][1] = v21;
        v[2][2] = v22;
        v[2][3] = v23;

        v[3][0] = v30;
        v[3][1] = v31;
        v[3][2] = v32;
        v[3][3] = v33;
    }

    constexpr mat4_t(const vec3_t<T>& position, const quaternion_t<T>& rotation, const vec3_t<T>& scale)
    {
        T w = rotation.w;
        T x = rotation.x;
        T y = rotation.y;
        T z = rotation.z;

        vec3_t<T> rotX = vec3_t<T>(1 - 2*y*y - 2*z*z, 2 * (x*y + w*z), 2 * (x*z - w*y)).normalize() * scale.x;
        vec3_t<T> rotY = vec3_t<T>(2 * (x*y - w*z), 1 - 2*x*x - 2*z*z, 2 * (w*x + y*z)).normalize() * scale.y;
        vec3_t<T> rotZ = vec3_t<T>(2 * (w*y + x*z), 2 * (y*z - w*x), 1 - 2*x*x - 2*y*y).normalize() * scale.z;

        v[0][0] = rotX.x;
        v[1][0] = rotY.x;
        v[2][0] = rotZ.x;

        v[0][1] = rotX.y;
        v[1][1] = rotY.y;
        v[2][1] = rotZ.y;

        v[0][2] = rotX.z;
        v[1][2] = rotY.z;
        v[2][2] = rotZ.z;

        v[3][0] = position.x;
        v[3][1] = position.y;
        v[3][2] = position.z;

        v[0][3] = 0;
        v[1][3] = 0;
        v[2][3] = 0;
        v[3][3] = 1;
    }

    constexpr std::array<T, 3 * 4> toMat3x4() const
    {
        return {
            v[0][0], v[1][0], v[2][0], v[3][0],
            v[0][1], v[1][1], v[2][1], v[3][1],
            v[0][2], v[1][2], v[2][2], v[3][2]
        };
    }

    T v[4][4];

    template<typename MsgpackT>
    void msgpack_pack(msgpack::packer<MsgpackT>& packer) const
    {
        layout payload = {
            v[0][0], v[0][1], v[0][2], v[0][3],
            v[1][0], v[1][1], v[1][2], v[1][3],
            v[2][0], v[2][1], v[2][2], v[2][3],
            v[3][0], v[3][1], v[3][2], v[3][3]
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        v[0][0] = std::get<0>(payload);
        v[0][1] = std::get<1>(payload);
        v[0][2] = std::get<2>(payload);
        v[0][3] = std::get<3>(payload);
        v[1][0] = std::get<4>(payload);
        v[1][1] = std::get<5>(payload);
        v[1][2] = std::get<6>(payload);
        v[1][3] = std::get<7>(payload);
        v[2][0] = std::get<8>(payload);
        v[2][1] = std::get<9>(payload);
        v[2][2] = std::get<10>(payload);
        v[2][3] = std::get<11>(payload);
        v[3][0] = std::get<12>(payload);
        v[3][1] = std::get<13>(payload);
        v[3][2] = std::get<14>(payload);
        v[3][3] = std::get<15>(payload);
    }

    typedef msgpack::type::tuple<T, T, T, T, T, T, T, T, T, T, T, T, T, T, T, T> layout;

    constexpr bool operator==(const mat4_t& rhs) const
    {
        for (size_t i = 0; i < 4; i++)
        {
            for (size_t j = 0; j < 4; j++)
            {
                if (v[i][j] != rhs.v[i][j])
                {
                    return false;
                }
            }
        }

        return true;
    }

    constexpr bool operator!=(const mat4_t& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr mat4_t operator*(const mat4_t& rhs) const
    {
        mat4_t m;

        vec4_t<T> r0(rhs.v[0][0], rhs.v[0][1], rhs.v[0][2], rhs.v[0][3]);
        vec4_t<T> r1(rhs.v[1][0], rhs.v[1][1], rhs.v[1][2], rhs.v[1][3]);
        vec4_t<T> r2(rhs.v[2][0], rhs.v[2][1], rhs.v[2][2], rhs.v[2][3]);
        vec4_t<T> r3(rhs.v[3][0], rhs.v[3][1], rhs.v[3][2], rhs.v[3][3]);

        vec4_t<T> c0(v[0][0], v[1][0], v[2][0], v[3][0]);
        vec4_t<T> c1(v[0][1], v[1][1], v[2][1], v[3][1]);
        vec4_t<T> c2(v[0][2], v[1][2], v[2][2], v[3][2]);
        vec4_t<T> c3(v[0][3], v[1][3], v[2][3], v[3][3]);

        m.v[0][0] = r0.dot(c0);
        m.v[0][1] = r0.dot(c1);
        m.v[0][2] = r0.dot(c2);
        m.v[0][3] = r0.dot(c3);

        m.v[1][0] = r1.dot(c0);
        m.v[1][1] = r1.dot(c1);
        m.v[1][2] = r1.dot(c2);
        m.v[1][3] = r1.dot(c3);

        m.v[2][0] = r2.dot(c0);
        m.v[2][1] = r2.dot(c1);
        m.v[2][2] = r2.dot(c2);
        m.v[2][3] = r2.dot(c3);

        m.v[3][0] = r3.dot(c0);
        m.v[3][1] = r3.dot(c1);
        m.v[3][2] = r3.dot(c2);
        m.v[3][3] = r3.dot(c3);

        return m;
    }

    constexpr mat4_t& operator*=(const mat4_t& rhs)
    {
        *this = (*this) * rhs;

        return *this;
    }

    constexpr T determinant() const
    {
        T a2323 = v[2][2] * v[3][3] - v[2][3] * v[3][2];
        T a1323 = v[2][1] * v[3][3] - v[2][3] * v[3][1];
        T a1223 = v[2][1] * v[3][2] - v[2][2] * v[3][1];
        T a0323 = v[2][0] * v[3][3] - v[2][3] * v[3][0];
        T a0223 = v[2][0] * v[3][2] - v[2][2] * v[3][0];
        T a0123 = v[2][0] * v[3][1] - v[2][1] * v[3][0];

        return +v[0][0] * (v[1][1] * a2323 - v[1][2] * a1323 + v[1][3] * a1223)
               -v[0][1] * (v[1][0] * a2323 - v[1][2] * a0323 + v[1][3] * a0223)
               +v[0][2] * (v[1][0] * a1323 - v[1][1] * a0323 + v[1][3] * a0123)
               -v[0][3] * (v[1][0] * a1223 - v[1][1] * a0223 + v[1][2] * a0123);
    }

    constexpr mat4_t inverse() const
    {
        T a2323 = v[2][2] * v[3][3] - v[2][3] * v[3][2];
        T a1323 = v[2][1] * v[3][3] - v[2][3] * v[3][1];
        T a1223 = v[2][1] * v[3][2] - v[2][2] * v[3][1];
        T a0323 = v[2][0] * v[3][3] - v[2][3] * v[3][0];
        T a0223 = v[2][0] * v[3][2] - v[2][2] * v[3][0];
        T a0123 = v[2][0] * v[3][1] - v[2][1] * v[3][0];
        T a2313 = v[1][2] * v[3][3] - v[1][3] * v[3][2];
        T a1313 = v[1][1] * v[3][3] - v[1][3] * v[3][1];
        T a1213 = v[1][1] * v[3][2] - v[1][2] * v[3][1];
        T a2312 = v[1][2] * v[2][3] - v[1][3] * v[2][2];
        T a1312 = v[1][1] * v[2][3] - v[1][3] * v[2][1];
        T a1212 = v[1][1] * v[2][2] - v[1][2] * v[2][1];
        T a0313 = v[1][0] * v[3][3] - v[1][3] * v[3][0];
        T a0213 = v[1][0] * v[3][2] - v[1][2] * v[3][0];
        T a0312 = v[1][0] * v[2][3] - v[1][3] * v[2][0];
        T a0212 = v[1][0] * v[2][2] - v[1][2] * v[2][0];
        T a0113 = v[1][0] * v[3][1] - v[1][1] * v[3][0];
        T a0112 = v[1][0] * v[2][1] - v[1][1] * v[2][0];

        T idet = 1 / determinant();

        return mat4_t(
           idet * +(v[1][1] * a2323 - v[1][2] * a1323 + v[1][3] * a1223),
           idet * -(v[0][1] * a2323 - v[0][2] * a1323 + v[0][3] * a1223),
           idet * +(v[0][1] * a2313 - v[0][2] * a1313 + v[0][3] * a1213),
           idet * -(v[0][1] * a2312 - v[0][2] * a1312 + v[0][3] * a1212),
           idet * -(v[1][0] * a2323 - v[1][2] * a0323 + v[1][3] * a0223),
           idet * +(v[0][0] * a2323 - v[0][2] * a0323 + v[0][3] * a0223),
           idet * -(v[0][0] * a2313 - v[0][2] * a0313 + v[0][3] * a0213),
           idet * +(v[0][0] * a2312 - v[0][2] * a0312 + v[0][3] * a0212),
           idet * +(v[1][0] * a1323 - v[1][1] * a0323 + v[1][3] * a0123),
           idet * -(v[0][0] * a1323 - v[0][1] * a0323 + v[0][3] * a0123),
           idet * +(v[0][0] * a1313 - v[0][1] * a0313 + v[0][3] * a0113),
           idet * -(v[0][0] * a1312 - v[0][1] * a0312 + v[0][3] * a0112),
           idet * -(v[1][0] * a1223 - v[1][1] * a0223 + v[1][2] * a0123),
           idet * +(v[0][0] * a1223 - v[0][1] * a0223 + v[0][2] * a0123),
           idet * -(v[0][0] * a1213 - v[0][1] * a0213 + v[0][2] * a0113),
           idet * +(v[0][0] * a1212 - v[0][1] * a0212 + v[0][2] * a0112)
        );
    }

    constexpr mat4_t invert() const
    {
        mat4_t result;

        result.v[0][0] = v[0][0];
        result.v[0][1] = v[1][0];
        result.v[0][2] = v[2][0];
        result.v[0][3] = v[3][0];

        result.v[1][0] = v[0][1];
        result.v[1][1] = v[1][1];
        result.v[1][2] = v[2][1];
        result.v[1][3] = v[3][1];

        result.v[2][0] = v[0][2];
        result.v[2][1] = v[1][2];
        result.v[2][2] = v[2][2];
        result.v[2][3] = v[3][2];

        result.v[3][0] = v[0][3];
        result.v[3][1] = v[1][3];
        result.v[3][2] = v[2][3];
        result.v[3][3] = v[3][3];

        return result;
    }

    constexpr vec4_t<T> row(size_t index) const
    {
        return vec4_t<T>(v[index][0], v[index][1], v[index][2], v[index][3]);
    }

    constexpr void row(size_t index, const vec4_t<T>& v)
    {
        this->v[index][0] = v.x;
        this->v[index][1] = v.y;
        this->v[index][2] = v.z;
        this->v[index][3] = v.w;
    }

    constexpr vec4_t<T> column(size_t index) const
    {
        return vec4_t<T>(v[0][index], v[1][index], v[2][index], v[3][index]);
    }

    constexpr void column(size_t index, const vec4_t<T>& v)
    {
        this->v[0][index] = v.x;
        this->v[1][index] = v.y;
        this->v[2][index] = v.z;
        this->v[3][index] = v.w;
    }

    constexpr vec3_t<T> applyToPosition(const vec3_t<T>& position, bool toWorld = true) const
    {
        if (toWorld)
        {
            return (*this * vec4_t<T>(position, 1)).toVec3();
        }
        else
        {
            return (inverse() * vec4_t<T>(position, 1)).toVec3();
        }
    }

    constexpr vec3_t<T> applyToDirection(const vec3_t<T>& direction, bool toWorld = true) const
    {
        return (applyToPosition(direction, toWorld) - applyToPosition(vec3_t<T>(), toWorld)).normalize();
    }

    constexpr vec3_t<T> applyToVector(const vec3_t<T>& vector, bool toWorld = true) const
    {
        return applyToDirection(vector.normalize(), toWorld) * vector.length();
    }

    constexpr quaternion_t<T> applyToRotation(const quaternion_t<T>& rotation, bool toWorld = true) const
    {
        if (toWorld)
        {
            return this->rotation() * rotation;
        }
        else
        {
            return this->rotation().inverse() * rotation;
        }
    }

    constexpr mat4_t applyToTransform(const mat4_t& transform, bool toWorld = true) const
    {
        if (toWorld)
        {
            return *this * transform;
        }
        else
        {
            return inverse() * transform;
        }
    }

    constexpr vec3_t<T> position() const
    {
        return applyToPosition(vec3_t<T>());
    }

    constexpr quaternion_t<T> rotation() const
    {
        return quaternion_t<T>(
            applyToDirection(forwardDir),
            applyToDirection(upDir)
        );
    }

    constexpr vec3_t<T> scale() const
    {
        return {
            row(0).toVec3().length(),
            row(1).toVec3().length(),
            row(2).toVec3().length()
        };
    }

    constexpr void setPosition(const vec3_t<T>& position)
    {
        row(3, vec4_t<T>(position, 1));
    }

    constexpr void setRotation(const quaternion_t<T>& rotation)
    {
        vec3_t<T> scale = this->scale();

        T w = rotation.w;
        T x = rotation.x;
        T y = rotation.y;
        T z = rotation.z;

        row(0, vec4_t<T>(vec3_t<T>(1 - 2*y*y - 2*z*z, 2 * (x*y + w*z), 2 * (x*z - w*y)).normalize() * scale.x, 0));
        row(1, vec4_t<T>(vec3_t<T>(2 * (x*y - w*z), 1 - 2*x*x - 2*z*z, 2 * (w*x + y*z)).normalize() * scale.y, 0));
        row(2, vec4_t<T>(vec3_t<T>(2 * (w*y + x*z), 2 * (y*z - w*x), 1 - 2*x*x - 2*y*y).normalize() * scale.z, 0));
    }

    constexpr void setScale(const vec3_t<T>& scale)
    {
        row(0, vec4_t<T>(row(0).toVec3().normalize() * scale.x, 0));
        row(1, vec4_t<T>(row(1).toVec3().normalize() * scale.y, 0));
        row(2, vec4_t<T>(row(2).toVec3().normalize() * scale.z, 0));
    }

    constexpr vec3_t<T> right() const
    {
        return rotation().right();
    }

    constexpr vec3_t<T> left() const
    {
        return rotation().left();
    }

    constexpr vec3_t<T> forward() const
    {
        return rotation().forward();
    }

    constexpr vec3_t<T> back() const
    {
        return rotation().back();
    }

    constexpr vec3_t<T> up() const
    {
        return rotation().up();
    }

    constexpr vec3_t<T> down() const
    {
        return rotation().down();
    }
};

template<typename T>
constexpr vec4_t<T> operator*(const mat4_t<T>& m, const vec4_t<T>& v)
{
    vec4_t<T> c0(m.v[0][0], m.v[1][0], m.v[2][0], m.v[3][0]);
    vec4_t<T> c1(m.v[0][1], m.v[1][1], m.v[2][1], m.v[3][1]);
    vec4_t<T> c2(m.v[0][2], m.v[1][2], m.v[2][2], m.v[3][2]);
    vec4_t<T> c3(m.v[0][3], m.v[1][3], m.v[2][3], m.v[3][3]);

    return {
        v.dot(c0),
        v.dot(c1),
        v.dot(c2),
        v.dot(c3)
    };
}

template<typename T>
constexpr vec4_t<T> operator*(const vec4_t<T>& v, const mat4_t<T>& m)
{
    vec4_t<T> r0(m.v[0][0], m.v[0][1], m.v[0][2], m.v[0][3]);
    vec4_t<T> r1(m.v[1][0], m.v[1][1], m.v[1][2], m.v[1][3]);
    vec4_t<T> r2(m.v[2][0], m.v[2][1], m.v[2][2], m.v[2][3]);
    vec4_t<T> r3(m.v[3][0], m.v[3][1], m.v[3][2], m.v[3][3]);

    return {
        v.dot(r0),
        v.dot(r1),
        v.dot(r2),
        v.dot(r3)
    };
}

template<typename T>
std::ostream& operator<<(std::ostream& out, const mat4_t<T>& rhs)
{
    out << "((" << rhs.v[0][0] << ", " << rhs.v[0][1] << ", " << rhs.v[0][2] << ", " << rhs.v[0][3] << "),\n";
    out << "(" << rhs.v[1][0] << ", " << rhs.v[1][1] << ", " << rhs.v[1][2] << ", " << rhs.v[1][3] << "),\n";
    out << "(" << rhs.v[2][0] << ", " << rhs.v[2][1] << ", " << rhs.v[2][2] << ", " << rhs.v[2][3] << "),\n";
    out << "(" << rhs.v[3][0] << ", " << rhs.v[3][1] << ", " << rhs.v[3][2] << ", " << rhs.v[3][3] << "))";

    return out;
}

template<typename T>
constexpr vec3_t<T> abs(vec3_t<T> v)
{
    return vec3_t<T>(
        std::abs(v.x),
        std::abs(v.y),
        std::abs(v.z)
    );
}

template<typename T>
constexpr vec3_t<T> floor(vec3_t<T> v)
{
    return vec3_t<T>(
        std::floor(v.x),
        std::floor(v.y),
        std::floor(v.z)
    );
}

template<typename T>
constexpr vec3_t<T> ceil(vec3_t<T> v)
{
    return vec3_t<T>(
        std::ceil(v.x),
        std::ceil(v.y),
        std::ceil(v.z)
    );
}

template<typename T>
constexpr vec3_t<T> pow(vec3_t<T> v, T n)
{
    return vec3_t<T>(
        std::pow(v.x, n),
        std::pow(v.y, n),
        std::pow(v.z, n)
    );
}

template<typename T>
constexpr vec3_t<T> sq(vec3_t<T> v)
{
    return vec3_t<T>(
        std::pow(v.x, 2),
        std::pow(v.y, 2),
        std::pow(v.z, 2)
    );
}

template<typename T>
constexpr vec3_t<T> sqrt(vec3_t<T> v)
{
    return vec3_t<T>(
        std::sqrt(v.x),
        std::sqrt(v.y),
        std::sqrt(v.z)
    );
}

template<typename T>
constexpr mat4_t<T> createFlatPerspectiveMatrix(T fov, T aspect, T near, T far)
{
    mat4_t<T> m;

    // FOV X to FOV Y
    fov = fov / aspect;

    m.v[0][0] = 1 / (aspect * tan(fov / 2));
    m.v[0][1] = 0;
    m.v[0][2] = 0;
    m.v[0][3] = 0;

    m.v[1][0] = 0;
    m.v[1][1] = 1 / tan(fov / 2);
    m.v[1][2] = 0;
    m.v[1][3] = 0;

    m.v[2][0] = 0;
    m.v[2][1] = 0;
    m.v[2][2] = far / (near - far);
    m.v[2][3] = -1;

    m.v[3][0] = 0;
    m.v[3][1] = 0;
    m.v[3][2] = -(far * near) / (far - near);
    m.v[3][3] = 0;

    return m;
}

template<typename T>
constexpr mat4_t<T> createFlatOrthographicMatrix(T left, T right, T bottom, T top, T near, T far)
{
    mat4_t<T> m;

    m.v[0][0] = 2 / (right - left);
    m.v[1][1] = 2 / (top - bottom);
    m.v[2][2] = -1 / (far - near);
    m.v[3][0] = -(right + left) / (right - left);
    m.v[3][1] = -(top + bottom) / (top - bottom);
    m.v[3][2] = -near / (far - near);

    return m;
}

template<typename T>
constexpr mat4_t<T> createVRPerspectiveMatrix(
    T up,
    T down,
    T left,
    T right,
    T near,
    T far
)
{
    mat4_t<T> m;

    T tup = tan(up);
    T tdown = tan(down);
    T tleft = tan(left);
    T tright = tan(right);

    T width = tright - tleft;
    T height = tdown - tup;

    m.v[0][0] = 2 / width;
    m.v[1][0] = 0;
    m.v[2][0] = (tright + tleft) / width;
    m.v[3][0] = 0;

    m.v[0][1] = 0;
    m.v[1][1] = 2 / height;
    m.v[2][1] = (tup + tdown) / height;
    m.v[3][1] = 0;

    m.v[0][2] = 0;
    m.v[1][2] = 0;
    m.v[2][2] = -far / (far - near);
    m.v[3][2] = -(far * near) / (far - near);

    m.v[0][3] = 0;
    m.v[1][3] = 0;
    m.v[2][3] = -1;
    m.v[3][3] = 0;

    return m;
}

template<typename T>
constexpr mat4_t<T> createViewMatrix(const vec3_t<T>& position, const quaternion_t<T>& rotation)
{
    vec3_t<T> zAxis = -rotation.forward();
    vec3_t<T> xAxis = rotation.up().cross(zAxis).normalize();
    vec3_t<T> yAxis = zAxis.cross(xAxis).normalize();

    mat4_t<T> orientation;
    orientation.v[0][0] = xAxis.x;
    orientation.v[0][1] = yAxis.x;
    orientation.v[0][2] = zAxis.x;
    orientation.v[0][3] = 0;

    orientation.v[1][0] = xAxis.y;
    orientation.v[1][1] = yAxis.y;
    orientation.v[1][2] = zAxis.y;
    orientation.v[1][3] = 0;

    orientation.v[2][0] = xAxis.z;
    orientation.v[2][1] = yAxis.z;
    orientation.v[2][2] = zAxis.z;
    orientation.v[2][3] = 0;

    orientation.v[3][0] = 0;
    orientation.v[3][1] = 0;
    orientation.v[3][2] = 0;
    orientation.v[3][3] = 1;

    mat4_t<T> translation;
    translation.v[0][0] = 1;
    translation.v[0][1] = 0;
    translation.v[0][2] = 0;
    translation.v[0][3] = 0;

    translation.v[1][0] = 0;
    translation.v[1][1] = 1;
    translation.v[1][2] = 0;
    translation.v[1][3] = 0;

    translation.v[2][0] = 0;
    translation.v[2][1] = 0;
    translation.v[2][2] = 1;
    translation.v[2][3] = 0;

    translation.v[3][0] = -position.dot(xAxis);
    translation.v[3][1] = -position.dot(yAxis);
    translation.v[3][2] = -position.dot(zAxis);
    translation.v[3][3] = 1;

    return translation * orientation;
}

struct Version
{
    constexpr Version()
    {
        major = 0;
        minor = 0;
        patch = 0;
    }

    constexpr Version(u64 major, u64 minor, u64 patch)
        : major(major)
        , minor(minor)
        , patch(patch)
    {

    }

    u64 major;
    u64 minor;
    u64 patch;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            major,
            minor,
            patch
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        major = std::get<0>(payload);
        minor = std::get<1>(payload);
        patch = std::get<2>(payload);
    }

    typedef msgpack::type::tuple<u64, u64, u64> layout;

    constexpr bool operator==(const Version& rhs) const
    {
        return major == rhs.major &&
            minor == rhs.minor &&
            patch == rhs.patch;
    }

    constexpr bool operator!=(const Version& rhs) const
    {
        return !(*this == rhs);
    }

    constexpr bool operator<(const Version& rhs) const
    {
        if (major < rhs.major)
        {
            return true;
        }
        else if (major > rhs.major)
        {
            return false;
        }
        else
        {
            if (minor < rhs.minor)
            {
                return true;
            }
            else if (minor > rhs.minor)
            {
                return false;
            }
            else
            {
                if (patch < rhs.patch)
                {
                    return true;
                }
                else if (patch > rhs.patch)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    constexpr bool operator>(const Version& rhs) const
    {
        if (major > rhs.major)
        {
            return true;
        }
        else if (major < rhs.major)
        {
            return false;
        }
        else
        {
            if (minor > rhs.minor)
            {
                return true;
            }
            else if (minor < rhs.minor)
            {
                return false;
            }
            else
            {
                if (patch > rhs.patch)
                {
                    return true;
                }
                else if (patch < rhs.patch)
                {
                    return false;
                }
                else
                {
                    return false;
                }
            }
        }
    }

    constexpr bool operator<=(const Version& rhs) const
    {
        return (*this < rhs) || (*this == rhs);
    }

    constexpr bool operator>=(const Version& rhs) const
    {
        return (*this > rhs) || (*this == rhs);
    }

    constexpr std::strong_ordering operator<=>(const Version& rhs) const
    {
        if (*this == rhs)
        {
            return std::strong_ordering::equal;
        }
        else if (*this > rhs)
        {
            return std::strong_ordering::greater;
        }
        else
        {
            return std::strong_ordering::less;
        }
    }
};
#pragma pack()

typedef vec2_t<f64> vec2;
typedef vec3_t<f64> vec3;
typedef vec4_t<f64> vec4;
typedef quaternion_t<f64> quaternion;
typedef mat3_t<f64> mat3;
typedef mat4_t<f64> mat4;

typedef vec2_t<f32> fvec2;
typedef vec3_t<f32> fvec3;
typedef vec4_t<f32> fvec4;
typedef quaternion_t<f32> fquaternion;
typedef mat3_t<f32> fmat3;
typedef mat4_t<f32> fmat4;

typedef vec2_t<u32> uvec2;
typedef vec3_t<u32> uvec3;
typedef vec4_t<u32> uvec4;

typedef vec2_t<i32> ivec2;
typedef vec3_t<i32> ivec3;
typedef vec4_t<i32> ivec4;

typedef Velocities_t<f64> Velocities;

using namespace std;
namespace fs = filesystem;
