/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sample_parts.hpp"
#include "yaml_tools.hpp"

SamplePartTone::SamplePartTone()
{
    pitch = 440;
}

void SamplePartTone::verify()
{
    pitch = clamp(pitch, 20.0, 20000.0);
}

bool SamplePartTone::operator==(const SamplePartTone& rhs) const
{
    return pitch == rhs.pitch;
}

bool SamplePartTone::operator!=(const SamplePartTone& rhs) const
{
    return !(*this == rhs);
}

template<>
SamplePartTone YAMLNode::convert() const
{
    SamplePartTone tone;

    tone.pitch = at("pitch").as<f64>();

    return tone;
}

template<>
void YAMLSerializer::emit(SamplePartTone v)
{
    startMapping();

    emitPair("pitch", v.pitch);

    endMapping();
}
