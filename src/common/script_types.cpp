/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_types.hpp"
#include "tools.hpp"
#include "constants.hpp"

static constexpr u32 maxLineLength = 60;

Token::Token(const string& text, bool isString)
{
    this->text = text;
    this->isString = isString;
}

Lambda::Lambda()
{
    type = Lambda_Normal;
}

string Lambda::toString() const
{
    switch (type)
    {
    default :
    case Lambda_Normal :
    {
        string s = "(lambda (";

        for (size_t i = 0; i < argNames.size(); i++)
        {
            s += argNames[i];

            if (i + 1 != argNames.size())
            {
                s += " ";
            }
        }

        s += ")";

        for (size_t i = 0; i < body.size(); i++)
        {
            const Value& item = body[i];

            s += " ";
            s += item.toString();
        }

        s += ")";

        return s;
    }
    case Lambda_Syntax :
    {
        string s = "(syntax (";

        for (size_t i = 0; i < argLayout.size(); i++)
        {
            s += argLayout[i].toString();

            if (i + 1 != argLayout.size())
            {
                s += " ";
            }
        }

        s += ")";

        for (size_t i = 0; i < body.size(); i++)
        {
            const Value& item = body[i];

            s += " ";
            s += item.toString();
        }

        s += ")";

        return s;
    }
    case Lambda_Builtin :
        return builtinName;
    }
}

size_t Lambda::memorySize() const
{
    size_t size = sizeof(Lambda);

    size += argNames.capacity() * sizeof(string);

    for (const string& argName : argNames)
    {
        size += argName.size();
    }

    for (const Value& value : body)
    {
        size += value.memorySize();
    }

    size += argLayout.capacity() * sizeof(Value);

    for (const Value& i : argLayout)
    {
        size += i.memorySize() - sizeof(Value);
    }

    size += builtinName.size();

    return size;
}

bool Lambda::operator==(const Lambda& rhs) const
{
    if (type != rhs.type)
    {
        return false;
    }

    switch (type)
    {
    default :
        fatal("Encountered unknown lambda type '" + to_string(type) + "'.");
    case Lambda_Normal :
        return argNames == rhs.argNames && body == rhs.body;
    case Lambda_Syntax :
        return argLayout == rhs.argLayout && body == rhs.body;
    case Lambda_Builtin :
        return builtinName == rhs.builtinName;
    }
}

bool Lambda::operator!=(const Lambda& rhs) const
{
    return !(*this == rhs);
}

bool Lambda::operator>(const Lambda& rhs) const
{
    if (type > rhs.type)
    {
        return true;
    }
    else if (type < rhs.type)
    {
        return false;
    }
    else
    {
        switch (type)
        {
        default :
            fatal("Encountered unknown lambda type '" + to_string(type) + "'.");
        case Lambda_Normal :
            if (argNames > rhs.argNames)
            {
                return true;
            }
            else if (argNames < rhs.argNames)
            {
                return false;
            }
            else
            {
                return body > rhs.body;
            }
        case Lambda_Syntax :
            if (argLayout > rhs.argLayout)
            {
                return true;
            }
            else if (argLayout < rhs.argLayout)
            {
                return false;
            }
            else
            {
                return body > rhs.body;
            }
        case Lambda_Builtin :
            return builtinName > rhs.builtinName;
        }
    }
}

bool Lambda::operator<(const Lambda& rhs) const
{
    if (type < rhs.type)
    {
        return true;
    }
    else if (type > rhs.type)
    {
        return false;
    }
    else
    {
        switch (type)
        {
        default :
            fatal("Encountered unknown lambda type '" + to_string(type) + "'.");
        case Lambda_Normal :
            if (argNames < rhs.argNames)
            {
                return true;
            }
            else if (argNames > rhs.argNames)
            {
                return false;
            }
            else
            {
                return body < rhs.body;
            }
        case Lambda_Syntax :
            if (argLayout < rhs.argLayout)
            {
                return true;
            }
            else if (argLayout > rhs.argLayout)
            {
                return false;
            }
            else
            {
                return body < rhs.body;
            }
        case Lambda_Builtin :
            return builtinName < rhs.builtinName;
        }
    }
}

bool Lambda::operator>=(const Lambda& rhs) const
{
    return *this > rhs || *this == rhs;
}

bool Lambda::operator<=(const Lambda& rhs) const
{
    return *this < rhs || *this == rhs;
}

strong_ordering Lambda::operator<=>(const Lambda& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

void Lambda::msgpack_unpack(const msgpack::object& obj)
{
    layout payload;
    obj.convert(payload);

    type = static_cast<LambdaType>(get<0>(payload));
    argNames = get<1>(payload);
    body = get<2>(payload);
    argLayout = get<3>(payload);
    builtinName = get<4>(payload);
}

Value::Value()
{
    type = Value_Void;
    i = 0;
    f = 0;
}

Value::Value(const Token& value)
    : Value()
{
    if (value.isString)
    {
        type = Value_String;
        s = sanitize(value.text);
    }
    else if (isNumber(value.text))
    {
        *this = number(value.text);
    }
    else
    {
        type = Value_Symbol;
        s = value.text;
    }
}

Value::Value(const vector<Value>& children)
    : Value()
{
    type = Value_List;
    l = children;
}

string Value::toString() const
{
    switch (type)
    {
    default :
    case Value_Void :
        return "void";
    case Value_Symbol :
        return s;
    case Value_Integer :
        return to_string(i);
    case Value_Float :
        return ::toPrettyString(f);
    case Value_String :
        return '"' + escapeQuotes(s) + '"';
    case Value_List :
    {
        if (l.size() == 2 && l[0].type == Value_Symbol && l[0].s == "quote")
        {
            return '\'' + l[1].toString();
        }

        string s = "(";

        for (size_t i = 0; i < l.size(); i++)
        {
            s += l[i].toString();

            if (i + 1 != l.size())
            {
                s += " ";
            }
        }

        s += ")";

        return s;
    }
    case Value_Lambda :
    {
        return lambda.toString();
    }
    case Value_Handle :
    {
        return "(handle " + to_string(id.value()) + ")";
    }
    }
}

string Value::toPrettyString(u32 indent) const
{
    switch (type)
    {
    default :
    case Value_Void :
    case Value_Symbol :
    case Value_Integer :
    case Value_Float :
    case Value_String :
    case Value_Lambda :
    case Value_Handle :
        return toString();
    case Value_List :
    {
        if (l.empty())
        {
            return toString();
        }

        if (l.size() == 2 && l[0].type == Value_Symbol && l[0].s == "quote")
        {
            return '\'' + l[1].toPrettyString(indent);
        }

        if (indent * indentSize  + toString().size() > maxLineLength)
        {
            string s = "(";

            for (size_t i = 0; i < l.size(); i++)
            {
                s += l[i].toPrettyString(indent + 1) + "\n";

                if (i + 1 == l.size())
                {
                    s += string(indent * indentSize, ' ');
                }
                else
                {
                    s += string((indent + 1) * indentSize, ' ');
                }
            }

            s += ")";

            return s;
        }
        else
        {
            return toString();
        }
    }
    }
}

size_t Value::memorySize() const
{
    size_t size = sizeof(Value);

    switch (type)
    {
    case Value_Void :
    case Value_Integer :
    case Value_Float :
    case Value_Handle :
        break;
    case Value_Symbol :
    case Value_String :
        size += s.size();
        break;
    case Value_List :
        size += l.capacity() * sizeof(Value);
        for (const Value& i : l)
        {
            size += i.memorySize() - sizeof(Value);
        }
        break;
    case Value_Lambda :
        size += lambda.memorySize() - sizeof(Lambda);
        break;
    }

    return size;
}

Value::operator bool() const
{
    switch (type)
    {
    default :
    case Value_Void : return false;
    case Value_Symbol : return true;
    case Value_Integer : return i;
    case Value_Float : return f;
    case Value_String : return s.size();
    case Value_List : return l.size();
    case Value_Lambda : return true;
    case Value_Handle : return id;
    }
}

bool Value::operator==(const Value& rhs) const
{
    if (type == Value_Float && rhs.type == Value_Integer)
    {
        return f == rhs.i;
    }
    else if (type == Value_Integer && rhs.type == Value_Float)
    {
        return i == rhs.f;
    }
    else if (type == Value_Integer && rhs.type == Value_Integer)
    {
        return i == rhs.i;
    }
    else if (type == Value_Float && rhs.type == Value_Float)
    {
        return f == rhs.f;
    }
    else if (type == Value_Void && rhs.type == Value_Void)
    {
        return true;
    }
    else if (type == Value_Symbol && rhs.type == Value_Symbol)
    {
        return s == rhs.s;
    }
    else if (type == Value_String && rhs.type == Value_String)
    {
        return s == rhs.s;
    }
    else if (type == Value_Handle && rhs.type == Value_Handle)
    {
        return id == rhs.id;
    }
    else if (type == Value_List && rhs.type == Value_List)
    {
        if (l.size() != rhs.l.size())
        {
            return false;
        }

        for (size_t i = 0; i < l.size(); i++)
        {
            if (l[i] != rhs.l[i])
            {
                return false;
            }
        }

        return true;
    }
    else if (type == Value_Lambda && rhs.type == Value_Lambda)
    {
        return lambda == rhs.lambda;
    }
    else
    {
        return false;
    }
}

bool Value::operator!=(const Value& rhs) const
{
    return !(*this == rhs);
}

bool Value::operator>(const Value& rhs) const
{
    if (type == Value_Float && rhs.type == Value_Integer)
    {
        return f > rhs.i;
    }
    else if (type == Value_Integer && rhs.type == Value_Float)
    {
        return i > rhs.f;
    }
    else if (type == Value_Integer && rhs.type == Value_Integer)
    {
        return i > rhs.i;
    }
    else if (type == Value_Float && rhs.type == Value_Float)
    {
        return f > rhs.f;
    }
    else if (type == Value_Void && rhs.type == Value_Void)
    {
        return false;
    }
    else if (type == Value_Symbol && rhs.type == Value_Symbol)
    {
        return s > rhs.s;
    }
    else if (type == Value_String && rhs.type == Value_String)
    {
        return s > rhs.s;
    }
    else if (type == Value_Handle && rhs.type == Value_Handle)
    {
        return id > rhs.id;
    }
    else if (type == Value_List && rhs.type == Value_List)
    {
        if (l.size() > rhs.l.size())
        {
            return true;
        }
        else if (l.size() < rhs.l.size())
        {
            return false;
        }
        else
        {
            for (size_t i = 0; i < l.size(); i++)
            {
                if (l[i] > rhs.l[i])
                {
                    return true;
                }
                else if (l[i] > rhs.l[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
    else
    {
        return type > rhs.type;
    }
}

bool Value::operator<(const Value& rhs) const
{
    if (type == Value_Float && rhs.type == Value_Integer)
    {
        return f < rhs.i;
    }
    else if (type == Value_Integer && rhs.type == Value_Float)
    {
        return i < rhs.f;
    }
    else if (type == Value_Integer && rhs.type == Value_Integer)
    {
        return i < rhs.i;
    }
    else if (type == Value_Float && rhs.type == Value_Float)
    {
        return f < rhs.f;
    }
    else if (type == Value_Void && rhs.type == Value_Void)
    {
        return false;
    }
    else if (type == Value_Symbol && rhs.type == Value_Symbol)
    {
        return s < rhs.s;
    }
    else if (type == Value_String && rhs.type == Value_String)
    {
        return s < rhs.s;
    }
    else if (type == Value_Handle && rhs.type == Value_Handle)
    {
        return id < rhs.id;
    }
    else if (type == Value_List && rhs.type == Value_List)
    {
        if (l.size() < rhs.l.size())
        {
            return true;
        }
        else if (l.size() > rhs.l.size())
        {
            return false;
        }
        else
        {
            for (size_t i = 0; i < l.size(); i++)
            {
                if (l[i] < rhs.l[i])
                {
                    return true;
                }
                else if (l[i] > rhs.l[i])
                {
                    return false;
                }
            }

            return true;
        }
    }
    else
    {
        return type < rhs.type;
    }
}

bool Value::operator>=(const Value& rhs) const
{
    return (*this > rhs) || (*this == rhs);
}

bool Value::operator<=(const Value& rhs) const
{
    return (*this < rhs) || (*this == rhs);
}

strong_ordering Value::operator<=>(const Value& rhs) const
{
    if (*this == rhs)
    {
        return strong_ordering::equal;
    }
    else if (*this > rhs)
    {
        return strong_ordering::greater;
    }
    else
    {
        return strong_ordering::less;
    }
}

Value Value::number(const string& s) const
{
    if (s.find(".") == string::npos)
    {
        Value v;
        v.type = Value_Integer;
        v.i = stol(s);

        return v;
    }
    else
    {
        Value v;
        v.type = Value_Float;
        v.f = stod(s);

        return v;
    }
}

template<>
f64 Value::number() const
{
    return type == Value_Integer ? i : f;
}

template<>
i64 Value::number() const
{
    return type == Value_Integer ? i : f;
}

void Value::msgpack_unpack(const msgpack::object& obj)
{
    layout payload;
    obj.convert(payload);

    type = static_cast<ValueType>(get<0>(payload));
    i = get<1>(payload);
    f = get<2>(payload);
    s = get<3>(payload);
    l = get<4>(payload);
    lambda = get<5>(payload);
    id = get<6>(payload);
}
