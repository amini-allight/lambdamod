/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_playback.hpp"
#include "yaml_tools.hpp"
#include "entity_update.hpp"

ActionPlayback::ActionPlayback()
    : loop(false)
    , elapsed(0)
    , playing(true)
    , forcedSeek(false)
{

}

void ActionPlayback::verify()
{

}

void ActionPlayback::compare(const ActionPlayback& previous, EntityUpdate& update) const
{
    if (forcedSeek)
    {
        update.notify<u64, EntityActionPlaybackEvent>(elapsed, previous.elapsed, [&](EntityActionPlaybackEvent& event) -> void {
            event.type = Entity_Action_Playback_Event_Seek;
            event.position = elapsed;
        }, true);

        forcedSeek = false;
    }

    update.notify<bool, EntityActionPlaybackEvent>(playing, previous.playing, [&](EntityActionPlaybackEvent& event) -> void {
        event.type = playing ? Entity_Action_Playback_Event_Resume : Entity_Action_Playback_Event_Pause;
    }, true);
}

void ActionPlayback::resume()
{
    playing = true;
}

void ActionPlayback::pause()
{
    playing = false;
}

void ActionPlayback::seek(u64 position)
{
    elapsed = position;
    forcedSeek = true;
}

bool ActionPlayback::operator==(const ActionPlayback& rhs) const
{
    return name == rhs.name &&
        loop == rhs.loop &&
        elapsed == rhs.elapsed &&
        playing == rhs.playing;
}

bool ActionPlayback::operator!=(const ActionPlayback& rhs) const
{
    return !(*this == rhs);
}

template<>
ActionPlayback YAMLNode::convert() const
{
    ActionPlayback playback;

    playback.name = at("name").as<string>();
    playback.loop = at("loop").as<bool>();
    playback.elapsed = at("elapsed").as<u64>();
    playback.playing = at("playing").as<bool>();

    return playback;
}

template<>
void YAMLSerializer::emit(ActionPlayback v)
{
    startMapping();

    emitPair("name", v.name);
    emitPair("loop", v.loop);
    emitPair("elapsed", v.elapsed);
    emitPair("playing", v.playing);

    endMapping();
}
