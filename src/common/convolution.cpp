/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "convolution.hpp"

// This is more heavily optimized than most functions because it consumes a substantial fraction of program runtime in debug mode
vector<f32> convolve(const vector<f32>& samples, const vector<f32>& coefficients)
{
    const i32 sampleCount = static_cast<i32>(samples.size());
    const i32 lastCoefficientIndex = coefficients.size() - 1;

    vector<f32> result(samples.size(), 0);

    const f32* i = samples.data();
    f32* o = result.data();
    const f32* c = coefficients.data();

    for (i32 n = 0; n < sampleCount; n++)
    {
        for (i32 m = min<i32>(n, lastCoefficientIndex); m >= 0; m--)
        {
            o[n] += i[n - m] * c[m];
        }
    }

    return result;
}
