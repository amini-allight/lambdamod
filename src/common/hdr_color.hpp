/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

struct HDRColor
{
    HDRColor();
    HDRColor(const vec3& color, f64 intensity);

    void verify();

    bool operator==(const HDRColor& rhs) const;
    bool operator!=(const HDRColor& rhs) const;
    
    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            color,
            intensity
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        color = get<0>(payload);
        intensity = get<1>(payload);
    }

    vec3 toVec3() const;
    vec4 toVec4() const;

    typedef msgpack::type::tuple<vec3, f64> layout;

    vec3 color;
    f64 intensity;
};

ostream& operator<<(ostream& out, const HDRColor& color);
