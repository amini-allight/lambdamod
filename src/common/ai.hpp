/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

vector<vec3> pathfind(
    const vec3& start,
    const vec3& end,
    const function<vector<vec3>(const vec3&)>& generator,
    const function<f64(const vec3&, const vec3&)>& heuristic
);

template<typename StateT, typename OptionT>
f64 alphaBeta(
    u32 depth,
    const StateT& state,
    const function<vector<OptionT>(const StateT&, bool)>& generator,
    const function<StateT(const StateT&, const OptionT&)>& predictor,
    const function<f64(const StateT&)>& heuristic,
    f64 alpha,
    f64 beta,
    bool maximizing
)
{
    if (depth == 0)
    {
        return heuristic(state);
    }

    if (maximizing)
    {
        f64 score = numeric_limits<f64>::lowest();

        for (const OptionT& option : generator(state, true))
        {
            f64 optionScore = alphaBeta(
                depth - 1,
                predictor(state, option),
                generator,
                predictor,
                heuristic,
                alpha,
                beta,
                false
            );

            score = max(score, optionScore);

            if (score >= beta)
            {
                break;
            }

            alpha = max(alpha, score);
        }

        return score;
    }
    else
    {
        f64 score = numeric_limits<f64>::max();

        for (const OptionT& option : generator(state, false))
        {
            f64 optionScore = alphaBeta(
                depth - 1,
                predictor(state, option),
                generator,
                predictor,
                heuristic,
                alpha,
                beta,
                true
            );

            score = min(score, optionScore);

            if (score <= alpha)
            {
                break;
            }

            beta = min(beta, score);
        }

        return score;
    }
}

template<typename StateT, typename OptionT>
OptionT decide(
    u32 depth,
    const StateT& state,
    const function<vector<OptionT>(const StateT&, bool)>& generator,
    const function<StateT(const StateT&, const OptionT&)>& predictor,
    const function<f64(const StateT&)>& heuristic
)
{
    f64 alpha = numeric_limits<f64>::lowest();
    f64 beta = numeric_limits<f64>::max();

    OptionT bestOption;
    f64 bestScore = numeric_limits<f64>::lowest();

    for (const OptionT& option : generator(state, true))
    {
        f64 score = alphaBeta(
            depth,
            predictor(state, option),
            generator,
            predictor,
            heuristic,
            alpha,
            beta,
            false
        );

        if (score > bestScore)
        {
            bestOption = option;
            bestScore = score;
        }
    }

    return bestOption;
}
