/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "vr_device.hpp"
#include "reverb_effect.hpp"
#include "dynamics_entity.hpp"
#include "space_graph.hpp"

#define BT_USE_DOUBLE_PRECISION
#include <bullet/btBulletCollisionCommon.h>
#include <bullet/btBulletDynamicsCommon.h>
#include <bullet/BulletDynamics/Dynamics/btDiscreteDynamicsWorld.h>
#include <bullet/BulletCollision/CollisionShapes/btBoxShape.h>

class World;
class Entity;
class ScriptContext;

class Dynamics
{
public:
    Dynamics(World* self);
    Dynamics(const Dynamics& rhs) = delete;
    Dynamics(Dynamics&& rhs) = delete;
    ~Dynamics();

    Dynamics& operator=(const Dynamics& rhs) = delete;
    Dynamics& operator=(Dynamics&& rhs) = delete;

    void step(ScriptContext* context, f64 stepInterval);

    void update();

    void updatePose(const Entity* entity, const map<VRDevice, mat4>& pose);

    f64 soundOcclusion(const vec3& a, const vec3& b) const;
    vector<ReverbEffect> soundReverb(const vec3& position) const;

    const SpaceGraph& spaceGraph() const;

private:
    friend class DynamicsComponent;
    friend class DynamicsComponentEntity;
    friend class DynamicsComponentPart;

    btDiscreteDynamicsWorld* world() const;

private:
    World* self;

    btDbvtBroadphase* broadphase;
    btCollisionConfiguration* collisionConfiguration;
    btCollisionDispatcher* collisionDispatcher;
    btSequentialImpulseConstraintSolver* constraintSolver;
    btDiscreteDynamicsWorld* _world;

    map<EntityID, DynamicsEntity*> entities;

    SpaceGraph _spaceGraph;

    tuple<vector<DynamicsForceApplication>, vector<DynamicsArea>> getEnvironmentalEffects() const;

    static void onTick(btDynamicsWorld* world, btScalar timeStep);
};
