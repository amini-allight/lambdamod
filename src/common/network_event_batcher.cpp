/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "network_event_batcher.hpp"

NetworkEventBatcher::NetworkEventBatcher()
{

}

NetworkEventBatcher::NetworkEventBatcher(const NetworkEventBatcher& rhs)
    : activeBatch(rhs.activeBatch)
    , batches(rhs.batches)
{

}

NetworkEventBatcher::NetworkEventBatcher(NetworkEventBatcher&& rhs) noexcept
    : activeBatch(rhs.activeBatch)
    , batches(rhs.batches)
{

}

NetworkEventBatcher::~NetworkEventBatcher()
{

}

NetworkEventBatcher& NetworkEventBatcher::operator=(const NetworkEventBatcher& rhs)
{
    if (&rhs != this)
    {
        activeBatch = rhs.activeBatch;
        batches = rhs.batches;
    }

    return *this;
}

NetworkEventBatcher& NetworkEventBatcher::operator=(NetworkEventBatcher&& rhs) noexcept
{
    if (&rhs != this)
    {
        activeBatch = rhs.activeBatch;
        batches = rhs.batches;
    }

    return *this;
}

void NetworkEventBatcher::push(const NetworkEvent& event)
{
    activeBatch.push_back(event);

    if (event.type() == Network_Event_Step)
    {
        lock_guard<mutex> lock(batchesLock);

        batches.push_back(NetworkEventBatch(activeBatch));
        activeBatch.clear();
    }
}

optional<NetworkEventBatch> NetworkEventBatcher::pull()
{
    lock_guard<mutex> lock(batchesLock);

    if (batches.empty())
    {
        return {};
    }
    else
    {
        NetworkEventBatch batch = batches.front();

        batches.pop_front();

        return batch;
    }
}

void NetworkEventBatcher::clear()
{
    lock_guard<mutex> lock(batchesLock);

    activeBatch.clear();
    batches.clear();
}
