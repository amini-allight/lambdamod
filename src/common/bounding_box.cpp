/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "bounding_box.hpp"

BoundingBox::BoundingBox(const vec3& minBounds, const vec3& maxBounds)
    : minBounds(minBounds)
    , maxBounds(maxBounds)
{

}

void BoundingBox::add(const vec3& position)
{
    minBounds.x = min(minBounds.x, position.x);
    minBounds.y = min(minBounds.y, position.y);
    minBounds.z = min(minBounds.z, position.z);

    maxBounds.x = max(maxBounds.x, position.x);
    maxBounds.y = max(maxBounds.y, position.y);
    maxBounds.z = max(maxBounds.z, position.z);
}

void BoundingBox::add(const BoundingBox& other)
{
    minBounds.x = min(minBounds.x, other.minBounds.x);
    minBounds.y = min(minBounds.y, other.minBounds.y);
    minBounds.z = min(minBounds.z, other.minBounds.z);

    maxBounds.x = max(maxBounds.x, other.maxBounds.x);
    maxBounds.y = max(maxBounds.y, other.maxBounds.y);
    maxBounds.z = max(maxBounds.z, other.maxBounds.z);
}

void BoundingBox::add(const mat4& otherTransform, const BoundingBox& other)
{
    static constexpr vec3 corners[] = {
        vec3(-1, -1, -1),
        vec3(+1, -1, -1),
        vec3(-1, +1, -1),
        vec3(+1, +1, -1),
        vec3(-1, -1, +1),
        vec3(+1, -1, +1),
        vec3(-1, +1, +1),
        vec3(+1, +1, +1)
    };

    for (const vec3& corner : corners)
    {
        vec3 transformedCorner = otherTransform.applyToPosition(other.center() + corner * (other.size() / 2));

        add(transformedCorner);
    }
}

vec3 BoundingBox::center() const
{
    return (minBounds + maxBounds) / 2;
}

vec3 BoundingBox::size() const
{
    return maxBounds - minBounds;
}
