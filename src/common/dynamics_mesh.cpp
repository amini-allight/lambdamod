/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_mesh.hpp"

DynamicsMesh::DynamicsMesh(const StructureMesh& mesh)
    : indices(mesh.indices)
{
    vertices.reserve(mesh.vertices.size());
    for (const StructureVertex& vertex : mesh.vertices)
    {
        if (vertex.foreground)
        {
            continue;
        }

        vertices.push_back(vertex.position);
    }

    btIndexedMesh indexedMesh;
    indexedMesh.m_numTriangles = indices.size() / 3;
    indexedMesh.m_triangleIndexBase = reinterpret_cast<const u8*>(indices.data());
    indexedMesh.m_triangleIndexStride = 3 * sizeof(u32);
    indexedMesh.m_indexType = PHY_INTEGER;

    indexedMesh.m_numVertices = vertices.size();
    indexedMesh.m_vertexBase = reinterpret_cast<const u8*>(vertices.data());
    indexedMesh.m_vertexStride = sizeof(fvec3);
    indexedMesh.m_vertexType = PHY_FLOAT;

    _mesh = new btTriangleMesh();
    _mesh->addIndexedMesh(indexedMesh);
}

DynamicsMesh::DynamicsMesh(const TerrainMesh& mesh)
    : indices(mesh.indices)
{
    vertices.reserve(mesh.vertices.size());
    for (const TerrainVertex& vertex : mesh.vertices)
    {
        vertices.push_back(vertex.position);
    }

    btIndexedMesh indexedMesh;
    indexedMesh.m_numTriangles = indices.size() / 3;
    indexedMesh.m_triangleIndexBase = reinterpret_cast<const u8*>(indices.data());
    indexedMesh.m_triangleIndexStride = 3 * sizeof(u32);
    indexedMesh.m_indexType = PHY_INTEGER;

    indexedMesh.m_numVertices = vertices.size();
    indexedMesh.m_vertexBase = reinterpret_cast<const u8*>(vertices.data());
    indexedMesh.m_vertexStride = sizeof(fvec3);
    indexedMesh.m_vertexType = PHY_FLOAT;

    _mesh = new btTriangleMesh();
    _mesh->addIndexedMesh(indexedMesh);
}

DynamicsMesh::~DynamicsMesh()
{
    delete _mesh;
}

btTriangleMesh* DynamicsMesh::mesh() const
{
    return _mesh;
}
