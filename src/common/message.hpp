/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum MessageType : u8
{
    Message_Direct_Chat_In,
    Message_Direct_Chat_Out,
    Message_Team_Chat_In,
    Message_Team_Chat_Out,
    Message_Chat,
    Message_Script,
    Message_Log,
    Message_Warning,
    Message_Error
};

string messageTypeToString(MessageType type);
MessageType messageTypeFromString(const string& s);

struct Message
{
    Message();
    Message(MessageType type, const string& name, const string& text, const vector<string>& parameters = {});

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            name,
            text,
            parameters
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<MessageType>(std::get<0>(payload));
        name = std::get<1>(payload);
        text = std::get<2>(payload);
        parameters = std::get<3>(payload);
    }

#if defined(LMOD_CLIENT)
    string toString() const;
#endif

    typedef msgpack::type::tuple<u8, string, string, vector<string>> layout;

    MessageType type;
    string name;
    string text;
    vector<string> parameters;
};
