/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "intersections.hpp"
#include "tools.hpp"

vector<vec3> intersectBodyPartLine(
    f64 length,
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(length, 0.0) || roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    f64 parallelism = abs(forwardDir.dot(direction));

    if (roughly(parallelism, 1.0))
    {
        return {};
    }

    vec3 n = direction.cross(forwardDir);

    f64 gap = abs(position.dot(n) / n.length());

    if (gap > radius)
    {
        return {};
    }

    vec3 n1 = direction.cross(n);

    if (roughly(forwardDir.dot(n1), 0.0))
    {
        return {};
    }

    vec3 hit = forwardDir * (position.dot(n1) / forwardDir.dot(n1));

    if (hit.length() > length / 2)
    {
        return {};
    }

    f64 directionality = (hit - position).normalize().dot(direction);

    if (!roughly(directionality, 1.0))
    {
        return {};
    }

    if (position.distance(hit) > distance)
    {
        return {};
    }

    return { hit };
}

vector<vec3> intersectLineLine(
    const vec3& positionA,
    const vec3& directionA,
    const vec3& positionB,
    const vec3& directionB,
    f64 maxError
)
{
    f64 b = sq(directionA.cross(directionB).length());

    if (roughly(b, 0.0))
    {
        return {};
    }

    vec3 posDelta = positionB - positionA;
    vec3 crossProduct = directionA.cross(directionB);

    f64 t = mat3(
        posDelta.x, posDelta.y, posDelta.z,
        directionB.x, directionB.y, directionB.z,
        crossProduct.x, crossProduct.y, crossProduct.z
    ).determinant() / b;
    f64 s = mat3(
        posDelta.x, posDelta.y, posDelta.z,
        directionA.x, directionA.y, directionA.z,
        crossProduct.x, crossProduct.y, crossProduct.z
    ).determinant() / b;

    vec3 closestA = positionA + directionA * t;
    vec3 closestB = positionB + directionB * s;

    if (closestA.distance(closestB) > maxError)
    {
        return {};
    }

    return { (closestA + closestB) / 2 };
}

vector<vec2> intersectPlane(
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(distance, 0.0))
    {
        return {};
    }

    f64 t = -position.dot(upDir);
    f64 b = direction.dot(upDir);

    if (roughly(b, 0.0))
    {
        return {};
    }

    f64 d = t / b;

    if (d < 0 || d > distance)
    {
        return {};
    }

    return { (position + direction * d).toVec2() };
}

vector<vec2> intersectTriangle(
    const vec2& a,
    const vec2& b,
    const vec2& c,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec2> intersections = intersectPlane(position, direction, distance);

    if (intersections.empty())
    {
        return {};
    }

    vec2 point = intersections.front();

    auto sign = [](const vec2& a, const vec2& b, const vec2& c) -> f64
    {
        return (a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y);
    };

    f64 ad = sign(point, a, b);
    f64 bd = sign(point, b, c);
    f64 cd = sign(point, c, a);

    bool neg = (!roughly(ad, 0.0) && ad < 0) || (!roughly(bd, 0.0) && bd < 0) || (!roughly(cd, 0.0) && cd < 0);
    bool pos = (!roughly(ad, 0.0) && ad > 0) || (!roughly(bd, 0.0) && bd > 0) || (!roughly(cd, 0.0) && cd > 0);

    if (!(neg && pos))
    {
        return { point };
    }
    else
    {
        return {};
    }
}

vector<vec2> intersectRectangle(
    f64 width,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(width, 0.0) || roughly(height, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec2> intersections = intersectPlane(position, direction, distance);

    if (intersections.empty())
    {
        return {};
    }

    vec2 point = intersections.front();

    if (abs(point.x) > width / 2 || abs(point.y) > height / 2)
    {
        return {};
    }

    return { point };
}

vector<vec2> intersectCircle(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec2> intersections = intersectPlane(position, direction, distance);

    if (intersections.empty())
    {
        return {};
    }

    vec2 point = intersections.front();

    if (point.length() > radius)
    {
        return {};
    }

    return { point };
}

vector<vec2> intersectEllipse(
    f64 xRadius,
    f64 yRadius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(xRadius, 0.0) || roughly(yRadius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec2> intersections = intersectPlane(position, direction, distance);

    if (intersections.empty())
    {
        return {};
    }

    vec2 point = intersections.front();

    if (sq(point.x) / sq(xRadius / 2) + sq(point.y) / sq(yRadius / 2) > 1)
    {
        return {};
    }

    return { point };
}

vector<vec2> intersectPolygon(
    const vector<vec2>& polygon,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec2> intersections = intersectPlane(position, direction, distance);

    if (intersections.empty())
    {
        return {};
    }

    vec2 point = intersections.front();

    bool hit = false;

    for (size_t i = 0, j = polygon.size() - 1; i < polygon.size(); j = i++)
    {
        const vec2& p0 = polygon[i];
        const vec2& p1 = polygon[j];

        if (
            (p0.y > point.y) != (p1.y > point.y) &&
            point.x < ((p1.x - p0.x) * (point.y - p0.y) / (p1.y - p0.y) + p0.x)
        )
        {
            hit = !hit;
        }
    }

    if (hit)
    {
        return { point };
    }

    return {};
}

vector<vec3> intersectPlane(
    const mat4& planeTransform,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    vector<vec2> intersections = intersectPlane(
        planeTransform.applyToPosition(position, false),
        planeTransform.applyToDirection(direction, false),
        distance
    );

    vector<vec3> intersections3D;
    intersections3D.reserve(intersections.size());

    for (const vec2& intersection : intersections)
    {
        intersections3D.push_back(planeTransform.applyToPosition(vec3(intersection, 0)));
    }

    return intersections3D;
}

vector<vec3> intersectTriangle(
    const vec3& a,
    const vec3& b,
    const vec3& c,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    mat4 triangleSpace(a, quaternion((b - a).normalize(), (b - a).cross(c - a).normalize()), vec3(1));

    vector<vec2> intersections = intersectTriangle(
        triangleSpace.applyToPosition(a, false).toVec2(),
        triangleSpace.applyToPosition(b, false).toVec2(),
        triangleSpace.applyToPosition(c, false).toVec2(),
        triangleSpace.applyToPosition(position, false),
        triangleSpace.applyToDirection(direction, false),
        distance
    );

    vector<vec3> intersections3D;
    intersections3D.reserve(intersections.size());

    for (const vec2& intersection : intersections)
    {
        intersections3D.push_back(triangleSpace.applyToPosition(vec3(intersection, 0)));
    }

    return intersections3D;
}

vector<vec3> intersectRectangle(
    const mat4& planeTransform,
    f64 width,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    vector<vec2> intersections = intersectRectangle(
        width,
        height,
        planeTransform.applyToPosition(position, false),
        planeTransform.applyToDirection(direction, false),
        distance
    );

    vector<vec3> intersections3D;
    intersections3D.reserve(intersections.size());

    for (const vec2& intersection : intersections)
    {
        intersections3D.push_back(planeTransform.applyToPosition(vec3(intersection, 0)));
    }

    return intersections3D;
}

vector<vec3> intersectCircle(
    const mat4& planeTransform,
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    vector<vec2> intersections = intersectCircle(
        radius,
        planeTransform.applyToPosition(position, false),
        planeTransform.applyToDirection(direction, false),
        distance
    );

    vector<vec3> intersections3D;
    intersections3D.reserve(intersections.size());

    for (const vec2& intersection : intersections)
    {
        intersections3D.push_back(planeTransform.applyToPosition(vec3(intersection, 0)));
    }

    return intersections3D;
}

vector<vec3> intersectEllipse(
    const mat4& planeTransform,
    f64 xRadius,
    f64 yRadius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    vector<vec2> intersections = intersectEllipse(
        xRadius,
        yRadius,
        planeTransform.applyToPosition(position, false),
        planeTransform.applyToDirection(direction, false),
        distance
    );

    vector<vec3> intersections3D;
    intersections3D.reserve(intersections.size());

    for (const vec2& intersection : intersections)
    {
        intersections3D.push_back(planeTransform.applyToPosition(vec3(intersection, 0)));
    }

    return intersections3D;
}

vector<vec3> intersectPolygon(
    const mat4& planeTransform,
    const vector<vec2>& polygon,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    vector<vec2> intersections = intersectPolygon(
        polygon,
        planeTransform.applyToPosition(position, false),
        planeTransform.applyToDirection(direction, false),
        distance
    );

    vector<vec3> intersections3D;
    intersections3D.reserve(intersections.size());

    for (const vec2& intersection : intersections)
    {
        intersections3D.push_back(planeTransform.applyToPosition(vec3(intersection, 0)));
    }

    return intersections3D;
}

vector<vec3> intersectCuboid(
    f64 width,
    f64 length,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(width, 0.0) || roughly(length, 0.0) || roughly(height, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec3> points;

    auto intersectSide = [&](const vec3& sidePos, const quaternion& sideRot, f64 width, f64 height) -> void
    {
        mat4 sideTransform(sidePos, sideRot, vec3(1));

        vec3 start = sideTransform.applyToPosition(position, false);
        vec3 end = sideTransform.applyToPosition(position + direction * distance, false);

        vector<vec2> localPoints = intersectRectangle(
            width,
            height,
            start,
            (end - start).normalize(),
            start.distance(end)
        );

        if (localPoints.empty())
        {
            return;
        }

        points.push_back(sideTransform.applyToPosition(vec3(localPoints.front(), 0)));
    };

    intersectSide(
        vec3(0, +length / 2, 0),
        quaternion(downDir, forwardDir),
        width,
        height
    );

    intersectSide(
        vec3(0, -length / 2, 0),
        quaternion(upDir, backDir),
        width,
        height
    );

    intersectSide(
        vec3(+width / 2, 0, 0),
        quaternion(upDir, rightDir),
        length,
        height
    );

    intersectSide(
        vec3(-width / 2, 0, 0),
        quaternion(downDir, leftDir),
        length,
        height
    );

    intersectSide(
        vec3(0, 0, +height / 2),
        quaternion(forwardDir, upDir),
        width,
        length
    );

    intersectSide(
        vec3(0, 0, -height / 2),
        quaternion(backDir, downDir),
        width,
        length
    );

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (points.empty() || position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectSphere(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    f64 a = direction.dot(direction);
    f64 b = 2 * position.dot(direction);
    f64 c = position.dot(position) - sq(radius);

    auto [ r0, r1 ] = quadratic(a, b, c);

    vector<vec3> points;
    points.reserve(2);

    if (!isnan(r0) && r0 > 0 && r0 < distance)
    {
        points.push_back(position + direction * r0);
    }

    if (!isnan(r1) && r1 > 0 && r1 < distance)
    {
        points.push_back(position + direction * r1);
    }

    return points;
}

vector<vec3> intersectCylinder(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(height, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec3> points;

    // curved surface
    vec2 dir2D = direction.toVec2().normalize();

    f64 a = sq(dir2D.x) + sq(dir2D.y);
    f64 b = 2 * position.toVec2().dot(dir2D);
    f64 c = sq(position.x) + sq(position.y) - sq(radius);

    auto [ r0, r1 ] = quadratic(a, b, c);

    if (!isnan(r0) && r0 >= 0)
    {
        vec3 v;
        v.x = position.x + dir2D.x * r0;
        v.y = position.y + dir2D.y * r0;
        v.z = (((v.x - position.x) / direction.x) * direction.z) + position.z;

        if (v.z >= -height / 2 && v.z <= height / 2)
        {
            points.push_back(v);
        }
    }

    if (!isnan(r1) && r1 >= 0)
    {
        vec3 v;
        v.x = position.x + dir2D.x * r1;
        v.y = position.y + dir2D.y * r1;
        v.z = (((v.x - position.x) / direction.x) * direction.z) + position.z;

        if (v.z >= -height / 2 && v.z <= height / 2)
        {
            points.push_back(v);
        }
    }

    // top cap
    vec3 topOrigin(0, 0, height / 2);

    points = join(points, intersectCircle(mat4(topOrigin, quaternion(), vec3(1)), radius, position, direction, distance));

    // bottom cap
    vec3 bottomOrigin(0, 0, -height / 2);

    points = join(points, intersectCircle(mat4(topOrigin, quaternion(), vec3(1)), radius, position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectCone(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(height, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec3> points;

    // curved cone surface
    f64 a = sq(direction.x) + sq(direction.y) - sq(direction.z)*(sq(radius)/sq(height));
    f64 b = 2*position.x*direction.x + 2*position.y*direction.y + 2*direction.z*(sq(radius)/height) - 2*position.z*direction.z*(sq(radius)/sq(height));
    f64 c = sq(position.x) + sq(position.y) - sq(radius) + 2*position.z*(sq(radius)/height) - sq(position.z)*(sq(radius)/sq(height));

    auto [ r0, r1 ] = quadratic(a, b, c);

    if (!isnan(r0) && r0 >= 0)
    {
        vec3 v = position + direction * r0;

        if (v.z >= 0 && v.z <= height)
        {
            points.push_back(v);
        }
    }

    if (!isnan(r1) && r1 >= 0)
    {
        vec3 v = position + direction * r1;

        if (v.z >= 0 && v.z <= height)
        {
            points.push_back(v);
        }
    }

    // bottom cap
    points = join(points, intersectCircle(mat4(), radius, position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectCapsule(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(height, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vector<vec3> points;

    // curved surface
    vec2 dir2D = direction.toVec2().normalize();

    f64 a = sq(dir2D.x) + sq(dir2D.y);
    f64 b = 2 * position.toVec2().dot(dir2D);
    f64 c = sq(position.x) + sq(position.y) - sq(radius);

    auto [ r0, r1 ] = quadratic(a, b, c);

    if (!isnan(r0) && r0 >= 0)
    {
        vec3 v;
        v.x = position.x + dir2D.x * r0;
        v.y = position.y + dir2D.y * r0;
        v.z = (((v.x - position.x) / direction.x) * direction.z) + position.z;

        if (v.z >= -height / 2 && v.z <= height / 2)
        {
            points.push_back(v);
        }
    }

    if (!isnan(r1) && r1 >= 0)
    {
        vec3 v;
        v.x = position.x + dir2D.x * r1;
        v.y = position.y + dir2D.y * r1;
        v.z = (((v.x - position.x) / direction.x) * direction.z) + position.z;

        if (v.z >= -height / 2 && v.z <= height / 2)
        {
            points.push_back(v);
        }
    }

    // top cap
    vec3 topOrigin(0, 0, height / 2);

    points = join(points, intersectSphere(radius, position - topOrigin, direction, distance));

    // bottom cap
    vec3 bottomOrigin(0, 0, -height / 2);

    points = join(points, intersectSphere(radius, position - bottomOrigin, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectPyramid(
    f64 radius,
    f64 height,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(height, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vec3 vertices[] = {
        vec3(0, 0, height),
        vec3(+radius, +radius, 0),
        vec3(+radius, -radius, 0),
        vec3(-radius, -radius, 0),
        vec3(-radius, +radius, 0)
    };

    vector<vec3> points;

    points = join(points, intersectTriangle(vertices[0], vertices[1], vertices[2], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[2], vertices[3], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[3], vertices[4], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[4], vertices[1], position, direction, distance));
    points = join(points, intersectRectangle(mat4(), radius * 2, radius * 2, position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectTetrahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vec3 ringStart = quaternion(leftDir, radians(120)).rotate(vec3(0, 0, radius));

    vec3 vertices[] = {
        vec3(0, 0, radius),
        ringStart,
        quaternion(upDir, radians(+120)).rotate(ringStart),
        quaternion(upDir, radians(-120)).rotate(ringStart)
    };

    vector<vec3> points;

    points = join(points, intersectTriangle(vertices[0], vertices[1], vertices[2], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[2], vertices[3], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[3], vertices[1], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[2], vertices[3], position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectOctahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vec3 vertices[] = {
        vec3(0, 0, +radius),
        vec3(0, 0, -radius),
        vec3(-radius, 0, 0),
        vec3(0, +radius, 0),
        vec3(+radius, 0, 0),
        vec3(0, -radius, 0),
    };

    vector<vec3> points;

    points = join(points, intersectTriangle(vertices[0], vertices[2], vertices[3], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[3], vertices[4], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[4], vertices[5], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[5], vertices[2], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[2], vertices[3], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[3], vertices[4], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[4], vertices[5], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[5], vertices[2], position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

static vector<vec3> intersectPentagon(
    const vec3& a,
    const vec3& b,
    const vec3& c,
    const vec3& d,
    const vec3& e,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    vec3 forward = (a - b).normalize();
    vec3 up = forward.cross((b - c).normalize()).normalize();

    mat4 space(a, quaternion(forward, up), vec3(1));

    return intersectPolygon(
        space,
        {
            space.applyToPosition(a, false).toVec2(),
            space.applyToPosition(b, false).toVec2(),
            space.applyToPosition(c, false).toVec2(),
            space.applyToPosition(d, false).toVec2(),
            space.applyToPosition(e, false).toVec2()
        },
        position,
        direction,
        distance
    );
}

vector<vec3> intersectDodecahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vec3 upperRingStart = radius * +dodecahedronRingStart;
    vec3 upperMiddleRingStart = radius * +dodecahedronMiddleRingStart;
    vec3 lowerMiddleRingStart = radius * -dodecahedronMiddleRingStart;
    vec3 lowerRingStart = radius * -dodecahedronRingStart;

    vec3 vertices[] = {
        upperRingStart,
        quaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart),
        quaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart),
        quaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart),
        quaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart),

        upperMiddleRingStart,
        quaternion(upDir, 1 * (tau / 5)).rotate(upperMiddleRingStart),
        quaternion(upDir, 2 * (tau / 5)).rotate(upperMiddleRingStart),
        quaternion(upDir, 3 * (tau / 5)).rotate(upperMiddleRingStart),
        quaternion(upDir, 4 * (tau / 5)).rotate(upperMiddleRingStart),

        lowerMiddleRingStart,
        quaternion(upDir, 1 * (tau / 5)).rotate(lowerMiddleRingStart),
        quaternion(upDir, 2 * (tau / 5)).rotate(lowerMiddleRingStart),
        quaternion(upDir, 3 * (tau / 5)).rotate(lowerMiddleRingStart),
        quaternion(upDir, 4 * (tau / 5)).rotate(lowerMiddleRingStart),

        lowerRingStart,
        quaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart),
        quaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart),
        quaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart),
        quaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart)
    };

    vector<vec3> points;

    points = join(points, intersectPentagon(vertices[0], vertices[1], vertices[2], vertices[3], vertices[4], position, direction, distance));
    points = join(points, intersectPentagon(vertices[15], vertices[16], vertices[17], vertices[18], vertices[19], position, direction, distance));

    points = join(points, intersectPentagon(vertices[0], vertices[1], vertices[5], vertices[13], vertices[6], position, direction, distance));
    points = join(points, intersectPentagon(vertices[1], vertices[2], vertices[6], vertices[14], vertices[7], position, direction, distance));
    points = join(points, intersectPentagon(vertices[2], vertices[3], vertices[7], vertices[10], vertices[8], position, direction, distance));
    points = join(points, intersectPentagon(vertices[3], vertices[4], vertices[8], vertices[11], vertices[9], position, direction, distance));
    points = join(points, intersectPentagon(vertices[4], vertices[0], vertices[9], vertices[12], vertices[5], position, direction, distance));

    points = join(points, intersectPentagon(vertices[8], vertices[10], vertices[15], vertices[16], vertices[11], position, direction, distance));
    points = join(points, intersectPentagon(vertices[9], vertices[11], vertices[16], vertices[17], vertices[12], position, direction, distance));
    points = join(points, intersectPentagon(vertices[5], vertices[12], vertices[17], vertices[18], vertices[13], position, direction, distance));
    points = join(points, intersectPentagon(vertices[6], vertices[13], vertices[18], vertices[19], vertices[14], position, direction, distance));
    points = join(points, intersectPentagon(vertices[7], vertices[14], vertices[19], vertices[15], vertices[10], position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}

vector<vec3> intersectIcosahedron(
    f64 radius,
    const vec3& position,
    const vec3& direction,
    f64 distance
)
{
    if (roughly(radius, 0.0) || roughly(distance, 0.0))
    {
        return {};
    }

    vec3 upperRingStart = radius * +icosahedronRingStart;
    vec3 lowerRingStart = radius * -icosahedronRingStart;

    vec3 vertices[] = {
        vec3(0, 0, +radius / 2),
        vec3(0, 0, -radius / 2),
        upperRingStart,
        quaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart),
        quaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart),
        quaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart),
        quaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart),
        lowerRingStart,
        quaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart),
        quaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart),
        quaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart),
        quaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart)
    };

    vector<vec3> points;

    points = join(points, intersectTriangle(vertices[0], vertices[2], vertices[3], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[3], vertices[4], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[4], vertices[5], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[5], vertices[6], position, direction, distance));
    points = join(points, intersectTriangle(vertices[0], vertices[6], vertices[2], position, direction, distance));

    points = join(points, intersectTriangle(vertices[1], vertices[7], vertices[8], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[8], vertices[9], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[9], vertices[10], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[10], vertices[11], position, direction, distance));
    points = join(points, intersectTriangle(vertices[1], vertices[11], vertices[7], position, direction, distance));

    points = join(points, intersectTriangle(vertices[2], vertices[3], vertices[10], position, direction, distance));
    points = join(points, intersectTriangle(vertices[3], vertices[4], vertices[11], position, direction, distance));
    points = join(points, intersectTriangle(vertices[4], vertices[5], vertices[7], position, direction, distance));
    points = join(points, intersectTriangle(vertices[5], vertices[6], vertices[8], position, direction, distance));
    points = join(points, intersectTriangle(vertices[6], vertices[2], vertices[9], position, direction, distance));

    points = join(points, intersectTriangle(vertices[7], vertices[8], vertices[5], position, direction, distance));
    points = join(points, intersectTriangle(vertices[8], vertices[9], vertices[6], position, direction, distance));
    points = join(points, intersectTriangle(vertices[9], vertices[10], vertices[2], position, direction, distance));
    points = join(points, intersectTriangle(vertices[10], vertices[11], vertices[3], position, direction, distance));
    points = join(points, intersectTriangle(vertices[11], vertices[7], vertices[4], position, direction, distance));

    sort(
        points.begin(),
        points.end(),
        [&](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    if (position.distance(points.front()) > distance)
    {
        return {};
    }
    else
    {
        return points;
    }
}
