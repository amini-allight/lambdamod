/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body.hpp"
#include "entity_update.hpp"
#include "tools.hpp"

Body::Body()
    : _nextPartID(1)
{

}

Body::Body(const Body& rhs)
{
    copy(rhs);
    _parts.clear();

    for (const auto& [ id, part ] : rhs._parts)
    {
        _parts.insert({ id, new BodyPart(*part) });
    }
}

Body::Body(Body&& rhs) noexcept
{
    copy(rhs);
    rhs._parts.clear();
}

Body::~Body()
{
    for (const auto& [ id, part ] : _parts)
    {
        delete part;
    }
}

Body& Body::operator=(const Body& rhs)
{
    if (&rhs != this)
    {
        for (const auto& [ id, part ] : _parts)
        {
            delete part;
        }

        copy(rhs);
        _parts.clear();

        for (const auto& [ id, part ] : rhs._parts)
        {
            _parts.insert({ id, new BodyPart(*part) });
        }
    }

    return *this;
}

Body& Body::operator=(Body&& rhs) noexcept
{
    if (&rhs != this)
    {
        for (const auto& [ id, part ] : _parts)
        {
            delete part;
        }

        copy(rhs);
        rhs._parts.clear();
    }

    return *this;
}

void Body::verify()
{
    for (auto& [ id, part ] : _parts)
    {
        part->verify();
    }
}

void Body::compare(const Body& previous, EntityUpdate& update, bool transformed) const
{
    update.notify<BodyPartID, EntityBodyEvent>(_nextPartID, previous._nextPartID, [&](EntityBodyEvent& event) -> void {
        event.type = Entity_Body_Event_Next_Part_ID;
        event.partID = _nextPartID;
    }, transformed);

    update.notifyMap<BodyPartID, BodyPart*, EntityBodyEvent>(
        _parts,
        previous._parts,
        [&](EntityBodyEvent& event, BodyPartID id, const BodyPart* part) -> void
        {
            event.type = Entity_Body_Event_Add_Part;
            event.partID = id;
            event.part = *part;
        },
        [&](EntityBodyEvent& event, BodyPartID id) -> void
        {
            event.type = Entity_Body_Event_Remove_Part;
            event.partID = id;
        },
        [&](BodyPartID id, const BodyPart* current, const BodyPart* previous) -> void
        {
            current->compare(*previous, update, transformed);
        },
        transformed
    );
}

void Body::push(const NetworkEvent& event)
{
    auto subEvent = event.payload<EntityBodyEvent>();

    switch (subEvent.type)
    {
    case Entity_Body_Event_Next_Part_ID :
        if (subEvent.partID > _nextPartID)
        {
            _nextPartID = subEvent.partID;
        }
        break;
    case Entity_Body_Event_Add_Part :
        if (subEvent.partID > _nextPartID)
        {
            _nextPartID = subEvent.partID;
        }

        if (subEvent.parentID == BodyPartID())
        {
            auto it = _parts.find(subEvent.partID);

            if (it != _parts.end())
            {
                delete it->second;
            }

            _parts.insert_or_assign(subEvent.partID, new BodyPart(nullptr, subEvent.partID, subEvent.part));
        }
        else
        {
            for (auto& [ id, part ] : _parts)
            {
                part->push(event);
            }
        }
        break;
    case Entity_Body_Event_Remove_Part :
    {
        if (subEvent.parentID == BodyPartID())
        {
            auto it = _parts.find(subEvent.partID);

            if (it != _parts.end())
            {
                delete it->second;
                _parts.erase(it);
            }
        }
        else
        {
            for (auto& [ id, part ] : _parts)
            {
                part->push(event);
            }
        }
        break;
    }
    case Entity_Body_Event_Update_Part :
    {
        if (subEvent.parentID == BodyPartID())
        {
            auto it = _parts.find(subEvent.partID);

            if (it != _parts.end())
            {
                it->second->replaceSelfOnly(subEvent.part);
            }
        }
        else
        {
            for (auto& [ id, part ] : _parts)
            {
                part->push(event);
            }
        }
        break;
    }
    }
}

vector<NetworkEvent> Body::initial(const string& world, EntityID id, bool transformed) const
{
    vector<NetworkEvent> events;

    EntityBodyEvent event;
    event.type = Entity_Body_Event_Next_Part_ID;
    event.world = world;
    event.id = id;
    event.partID = _nextPartID;
    event.transformed = transformed;

    events.push_back(event);

    for (const auto& [ partID, part ] : _parts)
    {
        vector<NetworkEvent> partEvents = part->initial(world, id, transformed);

        events.insert(
            events.end(),
            partEvents.begin(),
            partEvents.end()
        );
    }

    return events;
}

#if defined(LMOD_CLIENT)
BodyRenderData Body::toRender(const set<BodyPartID>& hiddenBodyPartIDs) const
{
    BodyRenderData data;

    for (const auto& [ id, part ] : _parts)
    {
        data.add(part->toRender(hiddenBodyPartIDs));
    }

    return data;
}
#endif

BodyPartID Body::nextPartID()
{
    return _nextPartID++;
}

void Body::add(BodyPartID parentID, BodyPart* part)
{
    if (part->id() > _nextPartID)
    {
        _nextPartID = part->id();
    }

    if (parentID == BodyPartID())
    {
        _parts.insert({ part->id(), part });
    }
    else
    {
        for (auto& [ childID, child ] : _parts)
        {
            child->add(parentID, part);
        }
    }
}

void Body::remove(BodyPartID id)
{
    auto it = _parts.find(id);

    if (it != _parts.end())
    {
        delete it->second;
        _parts.erase(it);
        return;
    }

    for (auto& [ partID, part ] : _parts)
    {
        part->remove(id);
    }
}

void Body::update(BodyPartID id, const BodyPart& part)
{
    auto it = _parts.find(id);

    if (it != _parts.end())
    {
        *it->second = part;
        return;
    }

    for (auto& [ partID, child ] : _parts)
    {
        child->update(id, part);
    }
}

void Body::clear()
{
    for (auto& [ id, part ] : _parts)
    {
        delete part;
    }

    _parts.clear();
}

BodyPart* Body::get(BodyPartID id) const
{
    auto it = _parts.find(id);

    if (it != _parts.end())
    {
        return it->second;
    }

    for (const auto& [ partID, part ] : _parts)
    {
        BodyPart* result = part->get(id);

        if (result)
        {
            return result;
        }
    }

    return nullptr;
}

BodyPart* Body::get(BodyAnchorType type, const string& name) const
{
    for (const auto& [ partID, part ] : _parts)
    {
        if (part->type() == Body_Part_Anchor &&
            part->get<BodyPartAnchor>().type == type &&
            part->get<BodyPartAnchor>().name == name
        )
        {
            return part;
        }

        BodyPart* result = part->get(type, name);

        if (result)
        {
            return result;
        }
    }

    return nullptr;
}

const map<BodyPartID, BodyPart*>& Body::parts() const
{
    return _parts;
}

void Body::traverse(const function<void(BodyPart*)>& behavior)
{
    for (auto& [ id, part ] : _parts)
    {
        part->traverse(behavior);
    }
}

void Body::traverse(const function<void(const BodyPart*)>& behavior) const
{
    for (const auto& [ id, part ] : _parts)
    {
        static_cast<const BodyPart*>(part)->traverse(behavior);
    }
}

bool Body::partiallyTraverse(const function<bool(BodyPart*)>& behavior)
{
    for (auto& [ id, part ] : _parts)
    {
        if (part->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool Body::partiallyTraverse(const function<bool(const BodyPart*)>& behavior) const
{
    for (const auto& [ id, part ] : _parts)
    {
        if (static_cast<const BodyPart*>(part)->partiallyTraverse(behavior))
        {
            return true;
        }
    }

    return false;
}

bool Body::empty(const set<BodyPartID>& hiddenBodyPartIDs) const
{
    if (_parts.empty())
    {
        return true;
    }

    bool empty = true;

    partiallyTraverse([&](const BodyPart* part) -> bool {
        empty &= hiddenBodyPartIDs.contains(part->id());

        return !empty;
    });

    return empty;
}

vector<vec3> Body::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vector<vec3> intersections;

    for (const auto& [ id, part ] : _parts)
    {
        vector<vec3> result = part->intersect(position, direction, distance);

        intersections.insert(
            intersections.end(),
            result.begin(),
            result.end()
        );
    }

    sort(
        intersections.begin(),
        intersections.end(),
        [=](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    return intersections;
}

bool Body::within(const vec3& position) const
{
    for (const auto& [ id, part ] : _parts)
    {
        if (part->within(position))
        {
            return true;
        }
    }

    return false;
}

f64 Body::independentPartMass() const
{
    f64 mass = 0;

    for (const auto& [ id, part ] : _parts)
    {
        mass += part->independentPartMass();
    }

    return mass;
}

vector<vec3> Body::subparts() const
{
    vector<vec3> subparts;

    traverse([&](const BodyPart* part) -> void {
        vector<vec3> partSubparts = part->subparts();

        subparts.insert(subparts.end(), partSubparts.begin(), partSubparts.end());
    });

    return subparts;
}

tuple<vec3, vec3> Body::boundingBox() const
{
    vec3 min;
    vec3 max;

    for (const vec3& subpart : subparts())
    {
        min.x = std::min(min.x, subpart.x);
        min.y = std::min(min.y, subpart.y);
        min.z = std::min(min.z, subpart.z);

        max.x = std::max(max.x, subpart.x);
        max.y = std::max(max.y, subpart.y);
        max.z = std::max(max.z, subpart.z);
    }

    return { min, max };
}

void Body::setParent(BodyPart* child, BodyPartSubID childSubID, BodyPart* parent, BodyPartSubID parentSubID)
{
    if (isCircular(child, parent))
    {
        return;
    }

    auto newPart = new BodyPart(parent, child->id(), *child);
    newPart->setConstraintParentSubID(parentSubID);
    newPart->setRegionalTransform(child->regionalTransform());

    switch (newPart->type())
    {
    case Body_Part_Line :
        newPart->get<BodyPartLine>().constraintAtEnd = childSubID == BodyPartSubID(2);
        break;
    case Body_Part_Plane :
        newPart->get<BodyPartPlane>().constraintSubID = childSubID;
        break;
    case Body_Part_Solid :
        newPart->get<BodyPartSolid>().constraintSubID = childSubID;
        break;
    case Body_Part_Rope :
        newPart->get<BodyPartRope>().constraintSubID = childSubID;
        break;
    case Body_Part_Anchor :
    case Body_Part_Text :
    case Body_Part_Symbol :
    case Body_Part_Canvas :
    case Body_Part_Structure :
    case Body_Part_Terrain :
    case Body_Part_Light :
    case Body_Part_Force :
    case Body_Part_Area :
        break;
    }

    remove(child->id());
    add(parent->id(), newPart);
}

void Body::clearParent(BodyPart* child)
{
    auto newPart = new BodyPart(nullptr, child->id(), *child);
    newPart->setConstraintParentSubID(BodyPartSubID());
    newPart->setRegionalTransform(child->regionalTransform());

    remove(child->id());
    add(BodyPartID(), newPart);
}

bool Body::isCircular(BodyPart* child, BodyPart* parent) const
{
    return !child->hasParent(parent) && child->rootParent() == parent->rootParent();
}

bool Body::operator==(const Body& rhs) const
{
    return _nextPartID == rhs._nextPartID &&
        dynAllocMapEqual(_parts, rhs._parts);
}

bool Body::operator!=(const Body& rhs) const
{
    return !(*this == rhs);
}

void Body::copy(const Body& rhs)
{
    _nextPartID = rhs._nextPartID;
    _parts = rhs._parts;
}

template<>
Body YAMLNode::convert() const
{
    Body body;

    body._nextPartID = at("nextPartID").as<BodyPartID>();

    for (const auto& [ id, node ] : at("parts").as<map<BodyPartID, YAMLNode>>())
    {
        body._parts.insert({ id, BodyPart::load(nullptr, node) });
    }

    return body;
}

template<>
void YAMLSerializer::emit(Body v)
{
    startMapping();

    emitPair("nextPartID", v._nextPartID);
    emitPair("parts", v._parts);

    endMapping();
}
