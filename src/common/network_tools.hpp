/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "address.hpp"

#if defined(__unix__) || defined(__APPLE__)
int setSocketOption(int socket, int level, int option, int value);
int setFileControl(int file, int flag, bool state);
#elif defined(WIN32)
int setSocketOption(u64 socket, int level, int option, const char* value, int length);
int ioctlSocket(u64 socket, long command, unsigned long value);
#endif

bool isWildcardIPv4Address(const string& address);
bool isWildcardIPv6Address(const string& address);
