/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "team.hpp"

Team::Team(const string& name)
    : name(name)
{

}

Team::Team(const string& name, const set<UserID>& userIDs)
    : name(name)
    , userIDs(userIDs)
{

}

bool Team::operator==(const Team& rhs) const
{
    return name == rhs.name && userIDs == rhs.userIDs;
}

bool Team::operator!=(const Team& rhs) const
{
    return !(*this == rhs);
}

template<>
Team YAMLNode::convert() const
{
    Team team(at("name").as<string>());

    team.userIDs = at("userIDs").as<set<UserID>>();

    return team;
}

template<>
void YAMLSerializer::emit(Team team)
{
    startMapping();

    emitPair("name", team.name);
    emitPair("userIDs", team.userIDs);

    endMapping();
}
