/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "confinement.hpp"
#include "yaml_tools.hpp"

Confinement::Confinement()
{
    active = false;
    distance = 0;
}

Confinement::Confinement(bool active, const vec3& position, f64 distance)
    : active(active)
    , position(position)
    , distance(distance)
{

}

bool Confinement::operator==(const Confinement& rhs) const
{
    return active == rhs.active &&
        position == rhs.position &&
        distance == rhs.distance;
}

bool Confinement::operator!=(const Confinement& rhs) const
{
    return !(*this == rhs);
}

template<>
Confinement YAMLNode::convert() const
{
    Confinement confinement;

    confinement.active = at("active").as<bool>();
    confinement.position = at("position").as<vec3>();
    confinement.distance = at("distance").as<f64>();

    return confinement;
}

template<>
void YAMLSerializer::emit(Confinement v)
{
    startMapping();

    emitPair("active", v.active);
    emitPair("position", v.position);
    emitPair("distance", v.distance);

    endMapping();
}
