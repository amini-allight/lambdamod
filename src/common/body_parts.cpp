/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_parts.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"
#include "intersections.hpp"
#include "mesh_cache.hpp"

#if defined(LMOD_CLIENT)
#include "client/render_constants.hpp"
#endif

BodyPartLine::BodyPartLine()
    : diameter(0.01)
    , length(1)
    , startColor({ 1, 1, 1 }, 1, false)
    , endColor({ 1, 1, 1 }, 1, false)
    , constraintAtEnd(false)
{

}

void BodyPartLine::verify()
{
    diameter = max(diameter, 0.0);
    length = max(length, 0.0);
    startColor.verify();
    endColor.verify();
}

vector<vec3> BodyPartLine::subparts() const
{
    return {
        vec3(0, -(length / 2), 0),
        vec3(0, +(length / 2), 0)
    };
}

vector<vec3> BodyPartLine::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return intersectBodyPartLine(length, diameter / 2, position, direction, distance);
}

bool BodyPartLine::within(const vec3& position) const
{
    return (vec2(position.x, position.z).length() <= diameter / 2 && abs(position.y) <= length / 2) ||
        position.distance(forwardDir) < diameter / 2 ||
        position.distance(backDir) < diameter / 2;
}

f64 BodyPartLine::boundingRadius() const
{
    return length / 2;
}

BoundingBox BodyPartLine::boundingBox() const
{
    return BoundingBox(vec3(0, -length / 2, 0), vec3(0, +length / 2, 0));
}

bool BodyPartLine::operator==(const BodyPartLine& rhs) const
{
    return diameter == rhs.diameter &&
        length == rhs.length &&
        startColor == rhs.startColor &&
        endColor == rhs.endColor &&
        constraintAtEnd == rhs.constraintAtEnd;
}

bool BodyPartLine::operator!=(const BodyPartLine& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartLine YAMLNode::convert() const
{
    BodyPartLine line;

    line.diameter = at("diameter").as<f64>();
    line.length = at("length").as<f64>();
    line.startColor = at("startColor").as<EmissiveColor>();
    line.endColor = at("endColor").as<EmissiveColor>();
    line.constraintAtEnd = at("constraintAtEnd").as<bool>();

    return line;
}

template<>
void YAMLSerializer::emit(BodyPartLine v)
{
    startMapping();

    emitPair("diameter", v.diameter);
    emitPair("length", v.length);
    emitPair("startColor", v.startColor);
    emitPair("endColor", v.endColor);
    emitPair("constraintAtEnd", v.constraintAtEnd);

    endMapping();
}

string bodyPlaneTypeToString(BodyPlaneType type)
{
    switch (type)
    {
    case Body_Plane_Ellipse :
        return "body-plane-ellipse";
    case Body_Plane_Equilateral_Triangle :
        return "body-plane-equilateral-triangle";
    case Body_Plane_Isosceles_Triangle :
        return "body-plane-isosceles-triangle";
    case Body_Plane_Right_Angle_Triangle :
        return "body-plane-right-angle-triangle";
    case Body_Plane_Rectangle :
        return "body-plane-rectangle";
    case Body_Plane_Pentagon :
        return "body-plane-pentagon";
    case Body_Plane_Hexagon :
        return "body-plane-hexagon";
    case Body_Plane_Heptagon :
        return "body-plane-heptagon";
    case Body_Plane_Octagon :
        return "body-plane-octagon";
    default :
        fatal("Encountered unknown body plane type '" + to_string(type) + "'.");
    }
}

BodyPlaneType bodyPlaneTypeFromString(const string& s)
{
    if (s == "body-plane-ellipse")
    {
        return Body_Plane_Ellipse;
    }
    else if (s == "body-plane-equilateral-triangle")
    {
        return Body_Plane_Equilateral_Triangle;
    }
    else if (s == "body-plane-isosceles-triangle")
    {
        return Body_Plane_Isosceles_Triangle;
    }
    else
    {
        fatal("Encountered unknown body plane type '" + s + "'.");
    }
}

template<>
BodyPlaneType YAMLNode::convert() const
{
    return bodyPlaneTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(BodyPlaneType v)
{
    emit(bodyPlaneTypeToString(v));
}

BodyPartPlane::BodyPartPlane()
    : type(Body_Plane_Ellipse)
    , thickness(0.01)
    , width(1)
    , height(1)
    , color({ 1, 1, 1 }, 1, false)
    , constraintSubID(0)
    , empty(false)
{

}

void BodyPartPlane::verify()
{
    type = min(type, Body_Plane_Octagon); // NOTE: Update this when adding new plane types
    thickness = max(thickness, 0.0);
    width = max(width, 0.0);
    height = max(height, 0.0);
    color.verify();
    constraintSubID = BodyPartSubID(min<u32>(constraintSubID.value(), subparts().size()));
}

vector<vec3> BodyPartPlane::subparts() const
{
    vector<vec3> subparts;

    switch (type)
    {
    default :
        fatal("Encountered unknown body plane type '" + to_string(type) + "'.");
    case Body_Plane_Ellipse :
        subparts = {
            vec3(0, +(height / 2), 0),
            vec3(+(width / 2), 0, 0),
            vec3(0, -(height / 2), 0),
            vec3(-(width / 2), 0, 0)
        };
        break;
    case Body_Plane_Equilateral_Triangle :
        subparts = {
            vec3(0, width / 2, 0),
            quaternion(upDir, +(tau / 3)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, -(tau / 3)).rotate(vec3(0, width / 2, 0))
        };
        break;
    case Body_Plane_Isosceles_Triangle :
        subparts = {
            vec3(0, height, 0),
            vec3(+(width / 2), 0, 0),
            vec3(-(width / 2), 0, 0)
        };
        break;
    case Body_Plane_Right_Angle_Triangle :
        subparts = {
            vec3(0, height, 0),
            vec3(width, 0, 0),
            vec3()
        };
        break;
    case Body_Plane_Rectangle :
        subparts = {
            vec3(-(width / 2), -(height / 2), 0),
            vec3(-(width / 2), +(height / 2), 0),
            vec3(+(width / 2), +(height / 2), 0),
            vec3(+(width / 2), -(height / 2), 0)
        };
        break;
    case Body_Plane_Pentagon :
        subparts = {
            quaternion(upDir, radians(0)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(72)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(144)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(216)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(288)).rotate(vec3(0, width / 2, 0))
        };
        break;
    case Body_Plane_Hexagon :
        subparts = {
            quaternion(upDir, radians(30)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(90)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(150)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(210)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(270)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(330)).rotate(vec3(0, width / 2, 0))
        };
        break;
    case Body_Plane_Heptagon :
        subparts = {
            quaternion(upDir, radians(0)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(51.4285)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(102.8571)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(154.2857)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(205.7142)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(257.1428)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(308.5714)).rotate(vec3(0, width / 2, 0))
        };
        break;
    case Body_Plane_Octagon :
        subparts = {
            quaternion(upDir, radians(22.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(67.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(112.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(157.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(202.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(247.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(292.5)).rotate(vec3(0, width / 2, 0)),
            quaternion(upDir, radians(337.5)).rotate(vec3(0, width / 2, 0))
        };
        break;
    }

    return subparts;
}

vector<vec3> BodyPartPlane::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    switch (type)
    {
    default :
        fatal("Encountered unknown body plane type '" + to_string(type) + "' while testing intersection.");
    case Body_Plane_Ellipse :
        return intersectEllipse(mat4(), width / 2, height / 2, position, direction, distance);
    case Body_Plane_Rectangle :
        return intersectRectangle(mat4(), width, height, position, direction, distance);
    case Body_Plane_Equilateral_Triangle :
    case Body_Plane_Isosceles_Triangle :
    case Body_Plane_Right_Angle_Triangle :
    case Body_Plane_Pentagon :
    case Body_Plane_Hexagon :
    case Body_Plane_Heptagon :
    case Body_Plane_Octagon :
    {
        vector<vec2> polygon;

        for (const vec3& subpart : subparts())
        {
            polygon.push_back(subpart.toVec2());
        }

        return intersectPolygon(mat4(), polygon, position, direction, distance);
    }
    }
}

bool BodyPartPlane::within(const vec3& position) const
{
    vector<vec2> flatSubparts;

    for (const vec3& subpart : subparts())
    {
        flatSubparts.push_back(subpart.toVec2());
    }

    return position.z < thickness / 2 && polygonContains(flatSubparts, position.toVec2());
}

f64 BodyPartPlane::boundingRadius() const
{
    f64 radius = 0;

    switch (type)
    {
    case Body_Plane_Ellipse :
        radius = max(width / 2, height / 2);
        break;
    case Body_Plane_Isosceles_Triangle :
        radius = max(width / 2, height);
        break;
    case Body_Plane_Right_Angle_Triangle :
        radius = max(width, height);
        break;
    case Body_Plane_Rectangle :
        radius = hypot(width / 2, height / 2);
        break;
    case Body_Plane_Equilateral_Triangle :
    case Body_Plane_Pentagon :
    case Body_Plane_Hexagon :
    case Body_Plane_Heptagon :
    case Body_Plane_Octagon :
        radius = width / 2;
        break;
    }

    return radius;
}

BoundingBox BodyPartPlane::boundingBox() const
{
    BoundingBox box(vec3(0), vec3(0));

    switch (type)
    {
    case Body_Plane_Isosceles_Triangle :
        box.minBounds = vec3(-width / 2, 0, 0);
        box.maxBounds = vec3(+width / 2, height, 0);
        break;
    case Body_Plane_Right_Angle_Triangle :
        box.maxBounds = vec3(width, height, 0);
        break;
    case Body_Plane_Ellipse :
    case Body_Plane_Rectangle :
        box.minBounds = vec3(-width / 2, -height / 2, 0);
        box.maxBounds = vec3(+width / 2, +height / 2, 0);
        break;
    case Body_Plane_Equilateral_Triangle :
    case Body_Plane_Pentagon :
    case Body_Plane_Hexagon :
    case Body_Plane_Heptagon :
    case Body_Plane_Octagon :
        for (const vec3& subpart : subparts())
        {
            box.add(subpart);
        }
        break;
    }

    return box;
}

bool BodyPartPlane::operator==(const BodyPartPlane& rhs) const
{
    return type == rhs.type &&
        thickness == rhs.thickness &&
        width == rhs.width &&
        height == rhs.height &&
        color == rhs.color &&
        constraintSubID == rhs.constraintSubID &&
        empty == rhs.empty;
}

bool BodyPartPlane::operator!=(const BodyPartPlane& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartPlane YAMLNode::convert() const
{
    BodyPartPlane part;

    part.type = bodyPlaneTypeFromString(at("type").as<string>());
    part.thickness = at("thickness").as<f64>();
    part.width = at("width").as<f64>();
    part.height = at("height").as<f64>();
    part.color = at("color").as<EmissiveColor>();
    part.constraintSubID = at("constraintSubID").as<BodyPartSubID>();
    part.empty = at("empty").as<bool>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartPlane plane)
{
    startMapping();

    emitPair("type", bodyPlaneTypeToString(plane.type));
    emitPair("thickness", plane.thickness);
    emitPair("width", plane.width);
    emitPair("height", plane.height);
    emitPair("color", plane.color);
    emitPair("constraintSubID", plane.constraintSubID);
    emitPair("empty", plane.empty);

    endMapping();
}

string bodyAnchorTypeToString(BodyAnchorType type)
{
    switch (type)
    {
    case Body_Anchor_Generic : return "body-anchor-generic";

    case Body_Anchor_Grip : return "body-anchor-grip";

    case Body_Anchor_Viewer : return "body-anchor-viewer";
    case Body_Anchor_Room : return "body-anchor-room";

    case Body_Anchor_Head : return "body-anchor-head";
    case Body_Anchor_Hips : return "body-anchor-hips";
    case Body_Anchor_Left_Hand : return "body-anchor-left-hand";
    case Body_Anchor_Right_Hand : return "body-anchor-right-hand";
    case Body_Anchor_Left_Foot : return "body-anchor-left-foot";
    case Body_Anchor_Right_Foot : return "body-anchor-right-foot";
    case Body_Anchor_Chest : return "body-anchor-chest";
    case Body_Anchor_Left_Shoulder : return "body-anchor-left-shoulder";
    case Body_Anchor_Right_Shoulder : return "body-anchor-right-shoulder";
    case Body_Anchor_Left_Elbow : return "body-anchor-left-elbow";
    case Body_Anchor_Right_Elbow : return "body-anchor-right-elbow";
    case Body_Anchor_Left_Wrist : return "body-anchor-left-wrist";
    case Body_Anchor_Right_Wrist : return "body-anchor-right-wrist";
    case Body_Anchor_Left_Knee : return "body-anchor-left-knee";
    case Body_Anchor_Right_Knee : return "body-anchor-right-knee";
    case Body_Anchor_Left_Ankle : return "body-anchor-left-ankle";
    case Body_Anchor_Right_Ankle : return "body-anchor-right-ankle";

    case Body_Anchor_Eyes : return "body-anchor-eyes";

    case Body_Anchor_Left_Eye : return "body-anchor-left-eye";
    case Body_Anchor_Left_Eyebrow : return "body-anchor-left-eyebrow";
    case Body_Anchor_Left_Eyelid : return "body-anchor-left-eyelid";
    case Body_Anchor_Right_Eye : return "body-anchor-right-eye";
    case Body_Anchor_Right_Eyebrow : return "body-anchor-right-eyebrow";
    case Body_Anchor_Right_Eyelid : return "body-anchor-right-eyelid";
    case Body_Anchor_Mouth : return "body-anchor-mouth";

    case Body_Anchor_Left_Thumb : return "body-anchor-left-thumb";
    case Body_Anchor_Left_Index : return "body-anchor-left-index";
    case Body_Anchor_Left_Middle : return "body-anchor-left-middle";
    case Body_Anchor_Left_Ring : return "body-anchor-left-ring";
    case Body_Anchor_Left_Little : return "body-anchor-left-little";

    case Body_Anchor_Right_Thumb : return "body-anchor-right-thumb";
    case Body_Anchor_Right_Index : return "body-anchor-right-index";
    case Body_Anchor_Right_Middle : return "body-anchor-right-middle";
    case Body_Anchor_Right_Ring : return "body-anchor-right-ring";
    case Body_Anchor_Right_Little : return "body-anchor-right-little";

    default : fatal("Encountered unknown body anchor type '" + to_string(type) + "'.");
    }
}

BodyAnchorType bodyAnchorTypeFromString(const string& s)
{
    if (s == "body-anchor-generic")
    {
        return Body_Anchor_Generic;
    }
    else if (s == "body-anchor-grip")
    {
        return Body_Anchor_Grip;
    }
    else if (s == "body-anchor-viewer")
    {
        return Body_Anchor_Viewer;
    }
    else if (s == "body-anchor-room")
    {
        return Body_Anchor_Room;
    }
    else if (s == "body-anchor-head")
    {
        return Body_Anchor_Head;
    }
    else if (s == "body-anchor-hips")
    {
        return Body_Anchor_Hips;
    }
    else if (s == "body-anchor-left-hand")
    {
        return Body_Anchor_Left_Hand;
    }
    else if (s == "body-anchor-right-hand")
    {
        return Body_Anchor_Right_Hand;
    }
    else if (s == "body-anchor-left-foot")
    {
        return Body_Anchor_Left_Foot;
    }
    else if (s == "body-anchor-right-foot")
    {
        return Body_Anchor_Right_Foot;
    }
    else if (s == "body-anchor-chest")
    {
        return Body_Anchor_Chest;
    }
    else if (s == "body-anchor-left-shoulder")
    {
        return Body_Anchor_Left_Shoulder;
    }
    else if (s == "body-anchor-right-shoulder")
    {
        return Body_Anchor_Right_Shoulder;
    }
    else if (s == "body-anchor-left-elbow")
    {
        return Body_Anchor_Left_Elbow;
    }
    else if (s == "body-anchor-right-elbow")
    {
        return Body_Anchor_Right_Elbow;
    }
    else if (s == "body-anchor-left-wrist")
    {
        return Body_Anchor_Left_Wrist;
    }
    else if (s == "body-anchor-right-wrist")
    {
        return Body_Anchor_Right_Wrist;
    }
    else if (s == "body-anchor-left-knee")
    {
        return Body_Anchor_Left_Knee;
    }
    else if (s == "body-anchor-right-knee")
    {
        return Body_Anchor_Right_Knee;
    }
    else if (s == "body-anchor-left-ankle")
    {
        return Body_Anchor_Left_Ankle;
    }
    else if (s == "body-anchor-right-ankle")
    {
        return Body_Anchor_Right_Ankle;
    }
    else if (s == "body-anchor-eyes")
    {
        return Body_Anchor_Eyes;
    }
    else if (s == "body-anchor-left-eye")
    {
        return Body_Anchor_Left_Eye;
    }
    else if (s == "body-anchor-left-eyebrow")
    {
        return Body_Anchor_Left_Eyebrow;
    }
    else if (s == "body-anchor-left-eyelid")
    {
        return Body_Anchor_Left_Eyelid;
    }
    else if (s == "body-anchor-right-eye")
    {
        return Body_Anchor_Right_Eye;
    }
    else if (s == "body-anchor-right-eyebrow")
    {
        return Body_Anchor_Right_Eyebrow;
    }
    else if (s == "body-anchor-right-eyelid")
    {
        return Body_Anchor_Right_Eyelid;
    }
    else if (s == "body-anchor-mouth")
    {
        return Body_Anchor_Mouth;
    }
    else if (s == "body-anchor-left-thumb")
    {
        return Body_Anchor_Left_Thumb;
    }
    else if (s == "body-anchor-left-index")
    {
        return Body_Anchor_Left_Index;
    }
    else if (s == "body-anchor-left-middle")
    {
        return Body_Anchor_Left_Middle;
    }
    else if (s == "body-anchor-left-ring")
    {
        return Body_Anchor_Left_Ring;
    }
    else if (s == "body-anchor-left-little")
    {
        return Body_Anchor_Left_Little;
    }
    else if (s == "body-anchor-right-thumb")
    {
        return Body_Anchor_Right_Thumb;
    }
    else if (s == "body-anchor-right-index")
    {
        return Body_Anchor_Right_Index;
    }
    else if (s == "body-anchor-right-middle")
    {
        return Body_Anchor_Right_Middle;
    }
    else if (s == "body-anchor-right-ring")
    {
        return Body_Anchor_Right_Ring;
    }
    else if (s == "body-anchor-right-little")
    {
        return Body_Anchor_Right_Little;
    }
    else
    {
        fatal("Encountered unknown body anchor type '" + s + "'.");
    }
}

template<>
BodyAnchorType YAMLNode::convert() const
{
    return bodyAnchorTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(BodyAnchorType v)
{
    emit(bodyAnchorTypeToString(v));
}

BodyPartAnchor::BodyPartAnchor()
    : type(Body_Anchor_Generic)
    , name("anchor")
    , massLimit(10)
    , pullDistance(5)
{

}

void BodyPartAnchor::verify()
{
    type = min(type, Body_Anchor_Right_Little); // NOTE: Update this when adding new anchor types
    name = isValidName(name) ? name : toValidName(name);
    massLimit = min(massLimit, 0.0);
    pullDistance = min(pullDistance, 0.0);
}

vector<vec3> BodyPartAnchor::subparts() const
{
    return {};
}

vector<vec3> BodyPartAnchor::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return {};
}

bool BodyPartAnchor::within(const vec3& position) const
{
    return false;
}

f64 BodyPartAnchor::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartAnchor::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

bool BodyPartAnchor::operator==(const BodyPartAnchor& rhs) const
{
    return type == rhs.type &&
        name == rhs.name &&
        massLimit == rhs.massLimit &&
        pullDistance == rhs.pullDistance;
}

bool BodyPartAnchor::operator!=(const BodyPartAnchor& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartAnchor YAMLNode::convert() const
{
    BodyPartAnchor part;

    part.type = bodyAnchorTypeFromString(at("type").as<string>());
    part.name = at("name").as<string>();
    part.massLimit = at("massLimit").as<f64>();
    part.pullDistance = at("pullDistance").as<f64>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartAnchor anchor)
{
    startMapping();

    emitPair("type", bodyAnchorTypeToString(anchor.type));
    emitPair("name", anchor.name);
    emitPair("massLimit", anchor.massLimit);
    emitPair("pullDistance", anchor.pullDistance);

    endMapping();
}

string bodySolidTypeToString(BodySolidType type)
{
    switch (type)
    {
    case Body_Solid_Cuboid :
        return "body-solid-cuboid";
    case Body_Solid_Sphere :
        return "body-solid-sphere";
    case Body_Solid_Cylinder :
        return "body-solid-cylinder";
    case Body_Solid_Cone :
        return "body-solid-cone";
    case Body_Solid_Capsule :
        return "body-solid-capsule";
    case Body_Solid_Pyramid :
        return "body-solid-pyramid";
    case Body_Solid_Tetrahedron :
        return "body-solid-tetrahedron";
    case Body_Solid_Octahedron :
        return "body-solid-octahedron";
    case Body_Solid_Dodecahedron :
        return "body-solid-dodecahedron";
    case Body_Solid_Icosahedron :
        return "body-solid-icosahedron";
    default :
        fatal("Encountered unknown body solid type '" + to_string(type) + "'.");
    }
}

BodySolidType bodySolidTypeFromString(const string& s)
{
    if (s == "body-solid-cuboid")
    {
        return Body_Solid_Cuboid;
    }
    else if (s == "body-solid-sphere")
    {
        return Body_Solid_Sphere;
    }
    else if (s == "body-solid-cylinder")
    {
        return Body_Solid_Cylinder;
    }
    else if (s == "body-solid-cone")
    {
        return Body_Solid_Cone;
    }
    else if (s == "body-solid-capsule")
    {
        return Body_Solid_Capsule;
    }
    else if (s == "body-solid-pyramid")
    {
        return Body_Solid_Pyramid;
    }
    else if (s == "body-solid-tetrahedron")
    {
        return Body_Solid_Tetrahedron;
    }
    else if (s == "body-solid-octahedron")
    {
        return Body_Solid_Octahedron;
    }
    else if (s == "body-solid-dodecahedron")
    {
        return Body_Solid_Dodecahedron;
    }
    else if (s == "body-solid-icosahedron")
    {
        return Body_Solid_Icosahedron;
    }
    else
    {
        fatal("Encountered unknown body solid type '" + s + "'.");
    }
}

template<>
BodySolidType YAMLNode::convert() const
{
    return bodySolidTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(BodySolidType v)
{
    emit(bodySolidTypeToString(v));
}

BodyPartSolid::BodyPartSolid()
    : type(Body_Solid_Cuboid)
    , size(1, 1, 1)
    , color({ 1, 1, 1 }, 1, false)
    , constraintSubID(0)
{

}

void BodyPartSolid::verify()
{
    type = min(type, Body_Solid_Icosahedron); // NOTE: Update this when adding new solid types
    size = { max(size.x, 0.0), max(size.y, 0.0), max(size.z, 0.0) };
    color.verify();
    constraintSubID = BodyPartSubID(min<u32>(constraintSubID.value(), subparts().size()));
}

vector<vec3> BodyPartSolid::subparts() const
{
    vector<vec3> subparts;

    switch (type)
    {
    case Body_Solid_Cuboid :
        subparts.reserve(8);
        subparts.push_back(vec3(+(size.x / 2), +(size.y / 2), +(size.z / 2)));
        subparts.push_back(vec3(+(size.x / 2), -(size.y / 2), +(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), +(size.y / 2), +(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), -(size.y / 2), +(size.z / 2)));
        subparts.push_back(vec3(+(size.x / 2), +(size.y / 2), -(size.z / 2)));
        subparts.push_back(vec3(+(size.x / 2), -(size.y / 2), -(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), +(size.y / 2), -(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), -(size.y / 2), -(size.z / 2)));
        break;
    case Body_Solid_Sphere :
        subparts.reserve(6);
        subparts.push_back(vec3(+(size.x / 2), 0, 0));
        subparts.push_back(vec3(-(size.x / 2), 0, 0));
        subparts.push_back(vec3(0, +(size.x / 2), 0));
        subparts.push_back(vec3(0, -(size.x / 2), 0));
        subparts.push_back(vec3(0, 0, +(size.x / 2)));
        subparts.push_back(vec3(0, 0, -(size.x / 2)));
        break;
    case Body_Solid_Cylinder :
        subparts.reserve(10);
        subparts.push_back(vec3(0, 0, +(size.z / 2)));
        subparts.push_back(vec3(0, 0, -(size.z / 2)));
        subparts.push_back(vec3(+(size.x / 2), 0, +(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), 0, +(size.z / 2)));
        subparts.push_back(vec3(0, +(size.x / 2), +(size.z / 2)));
        subparts.push_back(vec3(0, -(size.x / 2), +(size.z / 2)));
        subparts.push_back(vec3(+(size.x / 2), 0, -(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), 0, -(size.z / 2)));
        subparts.push_back(vec3(0, +(size.x / 2), -(size.z / 2)));
        subparts.push_back(vec3(0, -(size.x / 2), -(size.z / 2)));
        break;
    case Body_Solid_Cone :
        subparts.reserve(5);
        subparts.push_back(vec3(0, 0, size.z));
        subparts.push_back(vec3(+(size.x / 2), 0, 0));
        subparts.push_back(vec3(-(size.x / 2), 0, 0));
        subparts.push_back(vec3(0, +(size.x / 2), 0));
        subparts.push_back(vec3(0, -(size.x / 2), 0));
        break;
    case Body_Solid_Capsule :
        subparts.reserve(12);
        subparts.push_back(vec3(0, 0, +(size.z / 2)));
        subparts.push_back(vec3(0, 0, -(size.z / 2)));
        subparts.push_back(vec3(0, 0, +(size.z + size.x) / 2));
        subparts.push_back(vec3(0, 0, -(size.z + size.x) / 2));
        subparts.push_back(vec3(+(size.x / 2), 0, +(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), 0, +(size.z / 2)));
        subparts.push_back(vec3(0, +(size.x / 2), +(size.z / 2)));
        subparts.push_back(vec3(0, -(size.x / 2), +(size.z / 2)));
        subparts.push_back(vec3(+(size.x / 2), 0, -(size.z / 2)));
        subparts.push_back(vec3(-(size.x / 2), 0, -(size.z / 2)));
        subparts.push_back(vec3(0, +(size.x / 2), -(size.z / 2)));
        subparts.push_back(vec3(0, -(size.x / 2), -(size.z / 2)));
        break;
    case Body_Solid_Pyramid :
        subparts.reserve(5);
        subparts.push_back(vec3(0, 0, size.z));
        subparts.push_back(vec3(+size.x / 2, +size.x / 2, 0));
        subparts.push_back(vec3(+size.x / 2, -size.x / 2, 0));
        subparts.push_back(vec3(-size.x / 2, +size.x / 2, 0));
        subparts.push_back(vec3(-size.x / 2, -size.x / 2, 0));
        break;
    case Body_Solid_Tetrahedron :
    {
        vec3 ringStart = quaternion(leftDir, radians(120)).rotate(vec3(0, 0, size.x / 2));

        subparts.reserve(4);
        subparts.push_back(vec3(0, 0, size.x / 2));
        subparts.push_back(ringStart);
        subparts.push_back(quaternion(upDir, radians(+120)).rotate(ringStart));
        subparts.push_back(quaternion(upDir, radians(-120)).rotate(ringStart));
        break;
    }
    case Body_Solid_Octahedron :
        subparts.reserve(8);
        subparts.push_back(vec3(+size.x / 2, 0, 0));
        subparts.push_back(vec3(-size.x / 2, 0, 0));
        subparts.push_back(vec3(0, +size.x / 2, 0));
        subparts.push_back(vec3(0, -size.x / 2, 0));
        subparts.push_back(vec3(0, 0, +size.x / 2));
        subparts.push_back(vec3(0, 0, -size.x / 2));
        break;
    case Body_Solid_Dodecahedron :
    {
        vec3 upperRingStart = (size.x / 2) * vec3(+dodecahedronRingStart);
        vec3 upperMiddleRingStart = (size.x / 2) * vec3(+dodecahedronMiddleRingStart);
        vec3 lowerMiddleRingStart = (size.x / 2) * vec3(-dodecahedronMiddleRingStart);
        vec3 lowerRingStart = (size.x / 2) * vec3(-dodecahedronRingStart);

        subparts.reserve(20);

        subparts.push_back(upperRingStart);
        subparts.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart));
        subparts.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart));
        subparts.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart));
        subparts.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart));

        subparts.push_back(upperMiddleRingStart);
        subparts.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(upperMiddleRingStart));
        subparts.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(upperMiddleRingStart));
        subparts.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(upperMiddleRingStart));
        subparts.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(upperMiddleRingStart));

        subparts.push_back(lowerMiddleRingStart);
        subparts.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(lowerMiddleRingStart));
        subparts.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(lowerMiddleRingStart));
        subparts.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(lowerMiddleRingStart));
        subparts.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(lowerMiddleRingStart));

        subparts.push_back(lowerRingStart);
        subparts.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart));
        subparts.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart));
        subparts.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart));
        subparts.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart));
        break;
    }
    case Body_Solid_Icosahedron :
    {
        vec3 upperRingStart = (size.x / 2) * +icosahedronRingStart;
        vec3 lowerRingStart = (size.x / 2) * -icosahedronRingStart;

        subparts.reserve(12);

        subparts.push_back(vec3(0, 0, +(size.x / 2)));
        subparts.push_back(vec3(0, 0, -(size.x / 2)));

        subparts.push_back(upperRingStart);
        subparts.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart));
        subparts.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart));
        subparts.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart));
        subparts.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart));

        subparts.push_back(lowerRingStart);
        subparts.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart));
        subparts.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart));
        subparts.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart));
        subparts.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart));
        break;
    }
    }

    return subparts;
}

vector<vec3> BodyPartSolid::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vector<vec3> points;

    switch (type)
    {
    case Body_Solid_Cuboid :
        points = intersectCuboid(size.x, size.y, size.z, position, direction, distance);
        break;
    case Body_Solid_Sphere :
        points = intersectSphere(size.x / 2, position, direction, distance);
        break;
    case Body_Solid_Cylinder :
        points = intersectCylinder(size.x / 2, size.z, position, direction, distance);
        break;
    case Body_Solid_Cone :
        points = intersectCone(size.x / 2, size.z, position, direction, distance);
        break;
    case Body_Solid_Capsule :
        points = intersectCapsule(size.x / 2, size.z, position, direction, distance);
        break;
    case Body_Solid_Pyramid :
        points = intersectPyramid(size.x / 2, size.z, position, direction, distance);
        break;
    case Body_Solid_Tetrahedron :
        points = intersectTetrahedron(size.x / 2, position, direction, distance);
        break;
    case Body_Solid_Octahedron :
        points = intersectOctahedron(size.x / 2, position, direction, distance);
        break;
    case Body_Solid_Dodecahedron :
        points = intersectDodecahedron(size.x / 2, position, direction, distance);
        break;
    case Body_Solid_Icosahedron :
        points = intersectIcosahedron(size.x / 2, position, direction, distance);
        break;
    }

    return points;
}

bool BodyPartSolid::within(const vec3& position) const
{
    switch (type)
    {
    default :
        fatal("Encountered unknown body solid type '" + to_string(type) + "'.");
    case Body_Solid_Cuboid :
        return abs(position.x) <= size.x / 2 && abs(position.y) <= size.y / 2 && abs(position.z) <= size.z;
    case Body_Solid_Sphere :
        return position.length() <= size.x / 2;
    case Body_Solid_Cylinder :
        return position.z >= -size.z / 2 && position.z <= +size.z / 2 && ellipseContains(vec2(), size.x / 2, size.y / 2, position.toVec2());
    case Body_Solid_Cone :
        return position.z >= 0 && position.z <= size.z && position.toVec2().length() <= (size.x / 2) * (1 - position.z / size.z);
    case Body_Solid_Capsule :
        if (abs(position.z) <= size.z / 2)
        {
            return position.toVec2().length() <= size.x / 2;
        }
        else if (position.z <= -(size.z + size.x) / 2)
        {
            return (position - vec3(0, 0, -(size.z + size.x) / 2)).length() <= size.x / 2;
        }
        else if (position.z >= +(size.z + size.x) / 2)
        {
            return (position - vec3(0, 0, +(size.z + size.x) / 2)).length() <= size.x / 2;
        }
        else
        {
            return false;
        }
    case Body_Solid_Pyramid :
    {
        if (position.z < 0)
        {
            return false;
        }

        f64 edgeOffset = size.x / 2 - max(abs(position.x), abs(position.y));

        if (edgeOffset < 0)
        {
            return false;
        }

        f64 angle = atan2(size.z, size.x / 2);

        return tan(angle) * edgeOffset >= position.z;
    }
    case Body_Solid_Tetrahedron :
    {
        f64 radius = size.x / 2;

        vec3 a = vec3(0, 0, radius);
        vec3 b = quaternion(leftDir, radians(120)).rotate(vec3(0, 0, radius));
        vec3 c = quaternion(upDir, radians(120)).rotate(b);
        vec3 d = quaternion(upDir, radians(120)).rotate(c);

        auto centeredFaceSpace = [](const vec3& a, const vec3& b, const vec3& c) -> mat4
        {
            return triangleSpace(a, b, c, ((a + b + c) / 3).normalize());
        };

        mat4 faces[] = {
            centeredFaceSpace(a, b, c),
            centeredFaceSpace(b, c, d),
            centeredFaceSpace(c, d, a),
            centeredFaceSpace(d, a, b)
        };

        for (const mat4& face : faces)
        {
            if (face.applyToPosition(position).z > 0)
            {
                return false;
            }
        }

        return true;
    }
    case Body_Solid_Octahedron :
    {
        f64 radius = size.x / 2;

        vec3 a = vec3(0, 0, +radius);
        vec3 b = vec3(0, 0, -radius);
        vec3 c = vec3(+radius, 0, 0);
        vec3 d = vec3(0, +radius, 0);
        vec3 e = vec3(-radius, 0, 0);
        vec3 f = vec3(0, +radius, 0);

        mat4 faces[] = {
            triangleSpace(a, c, d, ((a + c + d) / 3).normalize()),
            triangleSpace(a, d, e, ((a + d + e) / 3).normalize()),
            triangleSpace(a, e, f, ((a + e + f) / 3).normalize()),
            triangleSpace(a, f, c, ((a + f + c) / 3).normalize()),
            triangleSpace(b, c, d, ((b + c + d) / 3).normalize()),
            triangleSpace(b, d, e, ((b + d + e) / 3).normalize()),
            triangleSpace(b, e, f, ((b + e + f) / 3).normalize()),
            triangleSpace(b, f, c, ((b + f + c) / 3).normalize())
        };

        for (const mat4& face : faces)
        {
            if (face.applyToPosition(position).z > 0)
            {
                return false;
            }
        }

        return true;
    }
    case Body_Solid_Dodecahedron :
    {
        f64 radius = size.x / 2;

        auto centeredFaceSpace = [](const vec3& center) -> mat4
        {
            return mat4(
                center,
                quaternion(pickForwardDirection(center.normalize()), center),
                vec3(1)
            );
        };

        mat4 faces[] = {
            mat4(radius * vec3(0, 0, +0.794654), quaternion(forwardDir, upDir), vec3(1)),
            mat4(radius * vec3(0, 0, -0.794654), quaternion(forwardDir, downDir), vec3(1)),

            centeredFaceSpace(radius * vec3(0, -0.711038, 0.354825)),
            centeredFaceSpace(quaternion(upDir, 1 * (tau / 5)).rotate(radius * vec3(0, -0.711038, 0.354825))),
            centeredFaceSpace(quaternion(upDir, 2 * (tau / 5)).rotate(radius * vec3(0, -0.711038, 0.354825))),
            centeredFaceSpace(quaternion(upDir, 3 * (tau / 5)).rotate(radius * vec3(0, -0.711038, 0.354825))),
            centeredFaceSpace(quaternion(upDir, 4 * (tau / 5)).rotate(radius * vec3(0, -0.711038, 0.354825))),

            centeredFaceSpace(radius * vec3(0, 0.711038, -0.354825)),
            centeredFaceSpace(quaternion(upDir, 1 * (tau / 5)).rotate(radius * vec3(0, 0.711038, -0.354825))),
            centeredFaceSpace(quaternion(upDir, 2 * (tau / 5)).rotate(radius * vec3(0, 0.711038, -0.354825))),
            centeredFaceSpace(quaternion(upDir, 3 * (tau / 5)).rotate(radius * vec3(0, 0.711038, -0.354825))),
            centeredFaceSpace(quaternion(upDir, 4 * (tau / 5)).rotate(radius * vec3(0, 0.711038, -0.354825)))
        };

        for (const mat4& face : faces)
        {
            if (face.applyToPosition(position).z > 0)
            {
                return false;
            }
        }

        return true;
    }
    case Body_Solid_Icosahedron :
    {
        f64 radius = size.x / 2;

        vec3 upperRingStart = radius * +icosahedronRingStart;
        vec3 lowerRingStart = radius * -icosahedronRingStart;

        vector<vec3> vertices;
        vertices.reserve(12);

        vertices.push_back(vec3(0, 0, +radius));
        vertices.push_back(vec3(0, 0, -radius));

        vertices.push_back(upperRingStart);
        vertices.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(upperRingStart));
        vertices.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(upperRingStart));
        vertices.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(upperRingStart));
        vertices.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(upperRingStart));

        vertices.push_back(lowerRingStart);
        vertices.push_back(quaternion(upDir, 1 * (tau / 5)).rotate(lowerRingStart));
        vertices.push_back(quaternion(upDir, 2 * (tau / 5)).rotate(lowerRingStart));
        vertices.push_back(quaternion(upDir, 3 * (tau / 5)).rotate(lowerRingStart));
        vertices.push_back(quaternion(upDir, 4 * (tau / 5)).rotate(lowerRingStart));

        auto centeredFaceSpace = [](const vec3& a, const vec3& b, const vec3& c) -> mat4
        {
            return triangleSpace(a, b, c, ((a + b + c) / 3).normalize());
        };

        mat4 faces[] = {
            centeredFaceSpace(vertices[0], vertices[2], vertices[3]),
            centeredFaceSpace(vertices[0], vertices[3], vertices[4]),
            centeredFaceSpace(vertices[0], vertices[4], vertices[5]),
            centeredFaceSpace(vertices[0], vertices[5], vertices[6]),
            centeredFaceSpace(vertices[0], vertices[6], vertices[2]),

            centeredFaceSpace(vertices[2], vertices[3], vertices[10]),
            centeredFaceSpace(vertices[3], vertices[4], vertices[11]),
            centeredFaceSpace(vertices[4], vertices[5], vertices[7]),
            centeredFaceSpace(vertices[5], vertices[6], vertices[8]),
            centeredFaceSpace(vertices[6], vertices[2], vertices[9]),

            centeredFaceSpace(vertices[7], vertices[8], vertices[5]),
            centeredFaceSpace(vertices[8], vertices[9], vertices[6]),
            centeredFaceSpace(vertices[9], vertices[10], vertices[2]),
            centeredFaceSpace(vertices[10], vertices[11], vertices[3]),
            centeredFaceSpace(vertices[11], vertices[7], vertices[4]),

            centeredFaceSpace(vertices[1], vertices[7], vertices[8]),
            centeredFaceSpace(vertices[1], vertices[8], vertices[9]),
            centeredFaceSpace(vertices[1], vertices[9], vertices[10]),
            centeredFaceSpace(vertices[1], vertices[10], vertices[11]),
            centeredFaceSpace(vertices[1], vertices[11], vertices[7])
        };

        for (const mat4& face : faces)
        {
            if (face.applyToPosition(position).z > 0)
            {
                return false;
            }
        }

        return true;
    }
    }
}

f64 BodyPartSolid::boundingRadius() const
{
    f64 radius = 0;

    switch (type)
    {
    case Body_Solid_Cuboid :
        radius = hypot(size.x / 2, hypot(size.y / 2, size.z / 2));
        break;
    case Body_Solid_Cylinder :
        radius = hypot(size.x / 2, size.z / 2);
        break;
    case Body_Solid_Cone :
        radius = max(size.x / 2, size.z);
        break;
    case Body_Solid_Capsule :
        radius = size.x / 2 + size.z / 2;
        break;
    case Body_Solid_Pyramid :
        radius = max(hypot(size.x / 2, size.x / 2), size.z);
        break;
    case Body_Solid_Sphere :
    case Body_Solid_Tetrahedron :
    case Body_Solid_Octahedron :
    case Body_Solid_Dodecahedron :
    case Body_Solid_Icosahedron :
        radius = size.x / 2;
        break;
    }

    return radius;
}

BoundingBox BodyPartSolid::boundingBox() const
{
    BoundingBox box(vec3(0), vec3(0));

    switch (type)
    {
    case Body_Solid_Cuboid :
        box.minBounds = -size / 2;
        box.maxBounds = +size / 2;
        break;
    case Body_Solid_Cylinder :
        box.minBounds = vec3(-size.x / 2, -size.x / 2, -size.z / 2);
        box.maxBounds = vec3(+size.x / 2, +size.x / 2, +size.z / 2);
        break;
    case Body_Solid_Capsule :
        box.minBounds = vec3(-size.x / 2, -size.x / 2, -(size.x / 2 + size.z / 2));
        box.maxBounds = vec3(+size.x / 2, +size.x / 2, +(size.x / 2 + size.z / 2));
        break;
    case Body_Solid_Cone :
    case Body_Solid_Pyramid :
        box.minBounds = vec3(-size.x / 2, -size.x / 2, 0);
        box.maxBounds = vec3(+size.x / 2, +size.x / 2, +size.z / 2);
        break;
    case Body_Solid_Sphere :
    case Body_Solid_Octahedron :
        box.minBounds = vec3(-size.x / 2);
        box.maxBounds = vec3(+size.x / 2);
        break;
    case Body_Solid_Tetrahedron :
    case Body_Solid_Dodecahedron :
    case Body_Solid_Icosahedron :
        for (const vec3& subpart : subparts())
        {
            box.add(subpart);
        }
        break;
    }

    return box;
}

bool BodyPartSolid::operator==(const BodyPartSolid& rhs) const
{
    return type == rhs.type &&
        size == rhs.size &&
        color == rhs.color &&
        constraintSubID == rhs.constraintSubID;
}

bool BodyPartSolid::operator!=(const BodyPartSolid& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartSolid YAMLNode::convert() const
{
    BodyPartSolid part;

    part.type = bodySolidTypeFromString(at("type").as<string>());
    part.size = at("size").as<vec3>();
    part.color = at("color").as<EmissiveColor>();
    part.constraintSubID = at("constraintSubID").as<BodyPartSubID>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartSolid solid)
{
    startMapping();

    emitPair("type", bodySolidTypeToString(solid.type));
    emitPair("size", solid.size);
    emitPair("color", solid.color);
    emitPair("constraintSubID", solid.constraintSubID);

    endMapping();
}

string bodyTextTypeToString(BodyTextType type)
{
    switch (type)
    {
    case Body_Text_Default :
        return "body-text-default";
    default :
        fatal("Encountered unknown body text type '" + to_string(type) + "'.");
    }
}

BodyTextType bodyTextTypeFromString(const string& s)
{
    if (s == "body-text-default")
    {
        return Body_Text_Default;
    }
    else
    {
        fatal("Encountered unknown body text type '" + s + "'.");
    }
}

template<>
BodyTextType YAMLNode::convert() const
{
    return bodyTextTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(BodyTextType v)
{
    emit(bodyTextTypeToString(v));
}

BodyPartText::BodyPartText()
    : type(Body_Text_Default)
    , text("Text")
    , height(0.1)
    , color({ 1, 1, 1 }, 1, false)
{

}

void BodyPartText::verify()
{
    type = min(type, Body_Text_Default); // NOTE: Update this when adding new fonts
    text = sanitize(text);
    height = max(height, 0.0);
    color.verify();
}

vector<vec3> BodyPartText::subparts() const
{
#if defined(LMOD_SERVER)
    f64 width = height;
#elif defined(LMOD_CLIENT)
    f64 width = height * (style().getFont()->width(text) / static_cast<f64>(style().getFont()->height()));
#endif

    vector<vec3> subparts = {
        vec3(-width / 2, -height / 2, 0),
        vec3(-width / 2, +height / 2, 0),
        vec3(+width / 2, +height / 2, 0),
        vec3(+width / 2, -height / 2, 0)
    };

    return subparts;
}

vector<vec3> BodyPartText::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return {};
}

bool BodyPartText::within(const vec3& position) const
{
    return false;
}

f64 BodyPartText::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartText::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

bool BodyPartText::operator==(const BodyPartText& rhs) const
{
    return type == rhs.type &&
        text == rhs.text &&
        height == rhs.height &&
        color == rhs.color;
}

bool BodyPartText::operator!=(const BodyPartText& rhs) const
{
    return !(*this == rhs);
}

#if defined(LMOD_CLIENT)
TextStyle BodyPartText::style() const
{
    TextStyle style;
    style.size = min<u32>(height * textPixelsPerUnit, maxTextPixels);

    switch (type)
    {
    default :
        throw runtime_error("Encountered unknown body part text type '" + to_string(type) + "' getting body part text style.");
    case Body_Text_Default :
        style.font = Text_Sans_Serif;
        style.weight = Text_Regular;
        break;
    }

    return style;
}
#endif

template<>
BodyPartText YAMLNode::convert() const
{
    BodyPartText part;

    part.type = bodyTextTypeFromString(at("type").as<string>());
    part.text = at("text").as<string>();
    part.height = at("height").as<f64>();
    part.color = at("color").as<EmissiveColor>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartText text)
{
    startMapping();

    emitPair("type", bodyTextTypeToString(text.type));
    emitPair("text", text.text);
    emitPair("height", text.height);
    emitPair("color", text.color);

    endMapping();
}

BodyPartSymbol::BodyPartSymbol()
    : type(Symbol_Star)
    , size(0.1)
    , color({ 1, 1, 1 }, 1, false)
{

}

void BodyPartSymbol::verify()
{
    type = min(type, Symbol_Star); // NOTE: Update this when adding new symbols
    size = max(size, 0.0);
    color.verify();
}

vector<vec3> BodyPartSymbol::subparts() const
{
    vector<vec3> subparts = {
        vec3(-(size / 2), -(size / 2), 0),
        vec3(-(size / 2), +(size / 2), 0),
        vec3(+(size / 2), +(size / 2), 0),
        vec3(+(size / 2), -(size / 2), 0)
    };

    return subparts;
}

vector<vec3> BodyPartSymbol::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return {};
}

bool BodyPartSymbol::within(const vec3& position) const
{
    return false;
}

f64 BodyPartSymbol::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartSymbol::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

bool BodyPartSymbol::operator==(const BodyPartSymbol& rhs) const
{
    return type == rhs.type &&
        size == rhs.size &&
        color == rhs.color;
}

bool BodyPartSymbol::operator!=(const BodyPartSymbol& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartSymbol YAMLNode::convert() const
{
    BodyPartSymbol part;

    part.type = symbolTypeFromString(at("type").as<string>());
    part.size = at("size").as<f64>();
    part.color = at("color").as<EmissiveColor>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartSymbol symbol)
{
    startMapping();

    emitPair("type", symbolTypeToString(symbol.type));
    emitPair("size", symbol.size);
    emitPair("color", symbol.color);

    endMapping();
}

BodyPartCanvas::BodyPartCanvas()
    : size(0.1)
    , pixels((canvasResolution * canvasResolution) / 8, 0)
    , color({ 1, 1, 1 }, 1, false)
{
    u32 start = (canvasResolution / 4) - 1;
    u32 end = ((canvasResolution / 4) * 3) - 1;

    for (u32 y = 0; y < canvasResolution; y++)
    {
        for (u32 x = 0; x < canvasResolution; x++)
        {
            if (x >= start && x <= end && y >= start && y <= end && (
                x == start || x == end || y == start || y == end
            ))
            {
                set({ x, y }, true);
            }
        }
    }
}

void BodyPartCanvas::verify()
{
    size = max(size, 0.0);
    color.verify();
}

vector<vec3> BodyPartCanvas::subparts() const
{
    vector<vec3> subparts = {
        vec3(-(size / 2), -(size / 2), 0),
        vec3(-(size / 2), +(size / 2), 0),
        vec3(+(size / 2), +(size / 2), 0),
        vec3(+(size / 2), -(size / 2), 0)
    };

    return subparts;
}

vector<vec3> BodyPartCanvas::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return {};
}

bool BodyPartCanvas::within(const vec3& position) const
{
    return false;
}

f64 BodyPartCanvas::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartCanvas::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

bool BodyPartCanvas::operator==(const BodyPartCanvas& rhs) const
{
    return size == rhs.size &&
        pixels == rhs.pixels &&
        color == rhs.color;
}

bool BodyPartCanvas::operator!=(const BodyPartCanvas& rhs) const
{
    return !(*this == rhs);
}

void BodyPartCanvas::set(const uvec2& position, bool value)
{
    u32 index = position.y * canvasResolution + position.x;
    u32 byteIndex = index / 8;
    u32 bitIndex = index % 8;

    if (value)
    {
        pixels[byteIndex] |= (1 << bitIndex);
    }
    else
    {
        pixels[byteIndex] &= ~(1 << bitIndex);
    }
}

bool BodyPartCanvas::get(const uvec2& position) const
{
    u32 index = position.y * canvasResolution + position.x;
    u32 byteIndex = index / 8;
    u32 bitIndex = index % 8;

    return 1 & (pixels[byteIndex] >> bitIndex);
}

template<>
BodyPartCanvas YAMLNode::convert() const
{
    BodyPartCanvas part;

    part.size = at("size").as<f64>();
    part.pixels = at("pixels").as<vector<u8>>();
    part.color = at("color").as<EmissiveColor>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartCanvas canvas)
{
    startMapping();

    emitPair("size", canvas.size);
    emitPair("pixels", canvas.pixels);
    emitPair("color", canvas.color);

    endMapping();
}

BodyPartStructure::BodyPartStructure()
    : positive(true)
    , nextTypeID(1)
    , nextNodeID(1)
    , color({ 1, 1, 1 }, 1, false)
{
    StructureType type(nextTypeID++);
    type.thickness = 0.1;
    types.push_back(type);

    node = StructureNode(nextNodeID++);
    node.length = 0.1;
}

void BodyPartStructure::verify()
{
    set<StructureTypeID> typeIDs;

    for (StructureType& type : types)
    {
        typeIDs.insert(type.id);
        type.verify();
    }

    node.verify(typeIDs);

    color.verify();
}

vector<vec3> BodyPartStructure::subparts() const
{
    // TODO: STRUCTURE: Subparts
    return {};
}

vector<vec3> BodyPartStructure::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return meshCache->intersect(node, position, direction, distance);
}

bool BodyPartStructure::within(const vec3& position) const
{
    // TODO: STRUCTURE: Within
    return false;
}

f64 BodyPartStructure::boundingRadius() const
{
    // TODO: STRUCTURE: Bounds
    return 0;
}

BoundingBox BodyPartStructure::boundingBox() const
{
    // TODO: STRUCTURE: Bounds
    return BoundingBox(vec3(), vec3());
}

bool BodyPartStructure::operator==(const BodyPartStructure& rhs) const
{
    return positive == rhs.positive &&
        nextTypeID == rhs.nextTypeID &&
        types == rhs.types &&
        nextNodeID == rhs.nextNodeID &&
        node == rhs.node &&
        color == rhs.color;
}

bool BodyPartStructure::operator!=(const BodyPartStructure& rhs) const
{
    return !(*this == rhs);
}

StructureNode* BodyPartStructure::getNodeByIndex(size_t index)
{
    size_t currentIndex = 0;

    return node.getNodeByIndex(&currentIndex, index);
}

const StructureNode* BodyPartStructure::getNodeByIndex(size_t index) const
{
    size_t currentIndex = 0;

    return node.getNodeByIndex(&currentIndex, index);
}

template<>
BodyPartStructure YAMLNode::convert() const
{
    BodyPartStructure part;

    part.positive = at("positive").as<bool>();
    part.nextTypeID = at("nextTypeID").as<StructureTypeID>();
    part.types = at("types").as<vector<StructureType>>();
    part.nextNodeID = at("nextNodeID").as<StructureNodeID>();
    part.node = at("node").as<StructureNode>();
    part.color = at("color").as<EmissiveColor>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartStructure structure)
{
    startMapping();

    emitPair("positive", structure.positive);
    emitPair("nextTypeID", structure.nextTypeID);
    emitPair("types", structure.types);
    emitPair("nextNodeID", structure.nextNodeID);
    emitPair("node", structure.node);
    emitPair("color", structure.color);

    endMapping();
}

BodyPartTerrain::BodyPartTerrain()
    : nextNodeID(1)
    , nodes(createTerrainGrid(&nextNodeID, defaultTerrainSize, defaultTerrainSpacing))
    , color({ 1, 1, 1 }, 1, false)
{

}

void BodyPartTerrain::verify()
{
    set<TerrainNodeID> nodeIDs;

    for (const TerrainNode& node : nodes)
    {
        nodeIDs.insert(node.id);
    }

    for (TerrainNode& node : nodes)
    {
        node.verify(nodeIDs);
    }

    color.verify();
}

vector<vec3> BodyPartTerrain::subparts() const
{
    vector<vec3> subparts;
    subparts.reserve(nodes.size());

    for (const TerrainNode& node : nodes)
    {
        subparts.push_back(node.position);
    }

    return subparts;
}

vector<vec3> BodyPartTerrain::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return meshCache->intersect(color, nodes, position, direction, distance);
}

bool BodyPartTerrain::within(const vec3& position) const
{
    for (const TerrainNode& node : nodes)
    {
        if (node.position.distance(position) < node.radius)
        {
            return true;
        }
    }

    return false;
}

f64 BodyPartTerrain::boundingRadius() const
{
    f64 radius = 0;

    for (const TerrainNode& node : nodes)
    {
        radius = max(radius, node.position.length() + node.radius);
    }

    return radius;
}

BoundingBox BodyPartTerrain::boundingBox() const
{
    BoundingBox box(vec3(0), vec3(0));

    for (const TerrainNode& node : nodes)
    {
        box.add(BoundingBox(node.position - node.radius, node.position + node.radius));
    }

    return box;
}

bool BodyPartTerrain::operator==(const BodyPartTerrain& rhs) const
{
    return nextNodeID == rhs.nextNodeID &&
        nodes == rhs.nodes &&
        color == rhs.color;
}

bool BodyPartTerrain::operator!=(const BodyPartTerrain& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartTerrain YAMLNode::convert() const
{
    BodyPartTerrain part;

    part.nextNodeID = at("nextNodeID").as<TerrainNodeID>();
    part.nodes = at("nodes").as<vector<TerrainNode>>();
    part.color = at("color").as<EmissiveColor>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartTerrain terrain)
{
    startMapping();

    emitPair("nextNodeID", terrain.nextNodeID);
    emitPair("nodes", terrain.nodes);
    emitPair("color", terrain.color);

    endMapping();
}

BodyPartLight::BodyPartLight()
    : enabled(true)
    , angle(tau)
    , color({ 1, 1, 1 }, 1)
{

}

void BodyPartLight::verify()
{
    angle = clamp<f64>(angle, 0, tau);
    color.verify();
}

vector<vec3> BodyPartLight::subparts() const
{
    return {};
}

vector<vec3> BodyPartLight::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return {};
}

bool BodyPartLight::within(const vec3& position) const
{
    return false;
}

f64 BodyPartLight::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartLight::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

bool BodyPartLight::operator==(const BodyPartLight& rhs) const
{
    return enabled == rhs.enabled &&
        angle == rhs.angle &&
        color == rhs.color;
}

bool BodyPartLight::operator!=(const BodyPartLight& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartLight YAMLNode::convert() const
{
    BodyPartLight part;

    part.enabled = at("enabled").as<bool>();
    part.angle = at("angle").as<f64>();
    part.color = at("color").as<HDRColor>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartLight light)
{
    startMapping();

    emitPair("enabled", light.enabled);
    emitPair("angle", light.angle);
    emitPair("color", light.color);

    endMapping();
}

string bodyForceTypeToString(BodyForceType type)
{
    switch (type)
    {
    case Body_Force_Linear :
        return "body-force-linear";
    case Body_Force_Vortex :
        return "body-force-vortex";
    case Body_Force_Omnidirectional :
        return "body-force-omnidirectional";
    case Body_Force_Fan :
        return "body-force-fan";
    case Body_Force_Rocket :
        return "body-force-rocket";
    case Body_Force_Gravity :
        return "body-force-gravity";
    case Body_Force_Magnetism :
        return "body-force-magnetism";
    default :
        fatal("Encountered unknown body force type '" + to_string(type) + "'.");
    }
}

BodyForceType bodyForceTypeFromString(const string& s)
{
    if (s == "body-force-linear")
    {
        return Body_Force_Linear;
    }
    else if (s == "body-force-vortex")
    {
        return Body_Force_Vortex;
    }
    else if (s == "body-force-omnidirectional")
    {
        return Body_Force_Omnidirectional;
    }
    else if (s == "body-force-fan")
    {
        return Body_Force_Fan;
    }
    else if (s == "body-force-rocket")
    {
        return Body_Force_Rocket;
    }
    else if (s == "body-force-gravity")
    {
        return Body_Force_Gravity;
    }
    else if (s == "body-force-magnetism")
    {
        return Body_Force_Magnetism;
    }
    else
    {
        fatal("Encountered unknown body force type '" + s + "'.");
    }
}

BodyPartForce::BodyPartForce()
    : enabled(true)
    , type(Body_Force_Linear)
    , intensity(1)
    , radius(1)
    , massFlowRate(1)
    , reaction(false)
{

}

void BodyPartForce::verify()
{
    type = min(type, Body_Force_Magnetism); // NOTE: Update this when adding new force types
    radius = max(radius, 0.0);
    massFlowRate = max(massFlowRate, 0.0);
}

vector<vec3> BodyPartForce::subparts() const
{
    return {};
}

vector<vec3> BodyPartForce::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    return {};
}

bool BodyPartForce::within(const vec3& position) const
{
    return false;
}

f64 BodyPartForce::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartForce::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

bool BodyPartForce::operator==(const BodyPartForce& rhs) const
{
    return enabled == rhs.enabled &&
        type == rhs.type &&
        intensity == rhs.intensity &&
        radius == rhs.radius &&
        massFlowRate == rhs.massFlowRate &&
        reaction == rhs.reaction;
}

bool BodyPartForce::operator!=(const BodyPartForce& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartForce YAMLNode::convert() const
{
    BodyPartForce part;

    part.enabled = at("enabled").as<bool>();
    part.type = bodyForceTypeFromString(at("type").as<string>());
    part.intensity = at("intensity").as<f64>();
    part.radius = at("radius").as<f64>();
    part.massFlowRate = at("massFlowRate").as<f64>();
    part.reaction = at("reaction").as<bool>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartForce force)
{
    startMapping();

    emitPair("enabled", force.enabled);
    emitPair("type", bodyForceTypeToString(force.type));
    emitPair("intensity", force.intensity);
    emitPair("radius", force.radius);
    emitPair("massFlowRate", force.massFlowRate);
    emitPair("reaction", force.reaction);

    endMapping();
}

BodyPartArea::BodyPartArea()
    : height(1)
    , enabled(true)
    , surface(false)
    , color({ 1, 1, 1 }, 1, false)
    , opaque(false)
    , animationSpeed(0.1)
{
    points = {
        vec2(-1, -1),
        vec2(-1, +1),
        vec2(+1, +1),
        vec2(+1, -1)
    };
}

void BodyPartArea::verify()
{
    for (vec2& point : points)
    {
        point = verifyPosition(vec3(point, 0)).toVec2();
    }

    height = max<f64>(height, 0);

    if (speedOfSound)
    {
        speedOfSound = max(*speedOfSound, 0.0);
    }

    if (density)
    {
        density = max(*density, 0.0);
    }

    if (up)
    {
        up = up->normalize();
    }

    if (fogDistance)
    {
        fogDistance = max(*fogDistance, 0.0);
    }

    if (sky)
    {
        sky->verify();
    }

    if (atmosphere)
    {
        atmosphere->verify();
    }

    color.verify();

    animationSpeed = max(animationSpeed, 0.0);
}

vector<vec3> BodyPartArea::subparts() const
{
    vector<vec3> subparts;
    subparts.reserve(points.size() * 2);

    // Don't change this from alternating between low and high, things rely upon it
    for (const vec2& point : points)
    {
        subparts.push_back(vec3(point, -height / 2));
        subparts.push_back(vec3(point, +height / 2));
    }

    return subparts;
}

vector<vec3> BodyPartArea::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vector<vec3> intersections;

    intersections = join(intersections, intersectPolygon(
        mat4(vec3(0, 0, -height / 2), quaternion(), vec3(1)),
        points,
        position,
        direction,
        distance
    ));

    intersections = join(intersections, intersectPolygon(
        mat4(vec3(0, 0, +height / 2), quaternion(), vec3(1)),
        points,
        position,
        direction,
        distance
    ));

    for (size_t i = 0; i < points.size(); i++)
    {
        const vec2& point = points[i];
        const vec2& nextPoint = points[loopIndex(i + 1, points.size())];

        vec2 center = (point + nextPoint) / 2;
        vec2 normal = rotate((point - nextPoint).normalize(), pi / 2);
        f64 width = point.distance(nextPoint);

        mat4 faceTransform(
            vec3(center, 0),
            quaternion(upDir, vec3(normal, 0)),
            vec3(1)
        );

        intersections = join(intersections, intersectRectangle(
            faceTransform,
            width,
            height,
            position,
            direction,
            distance
        ));
    }

    return intersections;
}

bool BodyPartArea::within(const vec3& position) const
{
    return false;
}

f64 BodyPartArea::boundingRadius() const
{
    return 0;
}

BoundingBox BodyPartArea::boundingBox() const
{
    return BoundingBox(vec3(), vec3());
}

optional<f64> BodyPartArea::immersion(const vec3& position) const
{
    f64 verticalImmersion = (height / 2) - abs(position.z);

    if (verticalImmersion < 0)
    {
        return {};
    }

    bool hit = false;

    for (size_t i = 0, j = points.size() - 1; i < points.size(); j = i++)
    {
        const vec2& p0 = points[i];
        const vec2& p1 = points[j];

        if (
            (p0.y > position.y) != (p1.y > position.y) &&
            position.x < ((p1.x - p0.x) * (position.y - p0.y) / (p1.y - p0.y) + p0.x)
        )
        {
            hit = !hit;
        }
    }

    if (hit)
    {
        f64 immersion = verticalImmersion;

        vec2 p = position.toVec2();

        for (size_t i = 0; i < points.size(); i++)
        {
            const vec2& a = points[i];
            const vec2& b = points[loopIndex<size_t>(i + 1, points.size())];

            f64 l2 = sq(a.distance(b));

            f64 distance;

            if (roughly(l2, 0.0))
            {
                distance = a.distance(p);
            }
            else
            {
                f64 t = max(0.0, min(1.0, (p - a).dot(b - a) / l2));

                distance = (a + t * (b - a)).distance(p);
            }

            immersion = min(immersion, distance);
        }

        return immersion;
    }
    else
    {
        return {};
    }
}

bool BodyPartArea::operator==(const BodyPartArea& rhs) const
{
    return points == rhs.points &&
        height == rhs.height &&
        enabled == rhs.enabled &&
        speedOfSound == rhs.speedOfSound &&
        gravity == rhs.gravity &&
        flowVelocity == rhs.flowVelocity &&
        density == rhs.density &&
        up == rhs.up &&
        fogDistance == rhs.fogDistance &&
        sky == rhs.sky &&
        atmosphere == rhs.atmosphere &&
        surface == rhs.surface &&
        color == rhs.color &&
        opaque == rhs.opaque &&
        animationSpeed == rhs.animationSpeed;
}

bool BodyPartArea::operator!=(const BodyPartArea& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyPartArea YAMLNode::convert() const
{
    BodyPartArea part;

    part.points = at("points").as<vector<vec2>>();
    part.height = at("height").as<f64>();

    part.enabled = at("enabled").as<bool>();
    part.speedOfSound = at("speedOfSound").as<optional<f64>>();
    part.gravity = at("gravity").as<optional<vec3>>();
    part.flowVelocity = at("flowVelocity").as<optional<vec3>>();
    part.density = at("density").as<optional<f64>>();
    part.up = at("up").as<optional<vec3>>();
    part.fogDistance = at("fogDistance").as<optional<f64>>();
    part.sky = at("sky").as<optional<SkyParameters>>();
    part.atmosphere = at("atmosphere").as<optional<AtmosphereParameters>>();

    part.surface = at("surface").as<bool>();
    part.color = at("color").as<EmissiveColor>();
    part.opaque = at("opaque").as<bool>();
    part.animationSpeed = at("animationSpeed").as<f64>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartArea area)
{
    startMapping();

    emitPair("points", area.points);
    emitPair("height", area.height);

    emitPair("enabled", area.enabled);
    emitPair("speedOfSound", area.speedOfSound);
    emitPair("gravity", area.gravity);
    emitPair("flowVelocity", area.flowVelocity);
    emitPair("density", area.density);
    emitPair("up", area.up);
    emitPair("fogDistance", area.fogDistance);
    emitPair("sky", area.sky);
    emitPair("atmosphere", area.atmosphere);

    emitPair("surface", area.surface);
    emitPair("color", area.color);
    emitPair("opaque", area.opaque);
    emitPair("animationSpeed", area.animationSpeed);

    endMapping();
}

static vector<vec3> createDefaultRopePositions(u32 segmentCount, f64 length)
{
    vector<vec3> positions(segmentCount + 1);

    for (u32 i = 0; i <= segmentCount; i++)
    {
        positions[i] = vec3(0, 0, -length * (i / static_cast<f64>(segmentCount)));
    }

    return positions;
}

BodyPartRope::BodyPartRope()
    : segmentCount(0)
    , length(0)
    , radius(0.025)
    , startColor({ 1, 1, 1 }, 1, false)
    , endColor({ 1, 1, 1 }, 1, false)
    , thin(false)
    , constraintSubID(0)
{
    setExtent(4, 1);
}

void BodyPartRope::verify()
{
    segmentCount = max<u32>(segmentCount, 1);
    length = max(length, 0.0);
    if (points.size() != segmentCount + 1)
    {
        points = createDefaultRopePositions(segmentCount, length);
    }
    radius = max(radius, 0.0);
    startColor.verify();
    endColor.verify();
    constraintSubID = BodyPartSubID(min<u32>(constraintSubID.value(), segmentCount));
}

vector<vec3> BodyPartRope::subparts() const
{
    return points;
}

vector<vec3> BodyPartRope::intersect(const vec3& position, const vec3& direction, f64 distance) const
{
    vector<vec3> intersections;

    for (size_t i = 0; i < points.size() - 1; i++)
    {
        const vec3& a = points[i];
        const vec3& b = points[i + 1];

        vec3 direction = (b - a).normalize();

        mat4 toGlobal = mat4(a, quaternion(direction, pickUpDirection(direction)), vec3(1));
        mat4 toLocal = toGlobal.inverse();

        vector<vec3> segmentIntersections = intersectBodyPartLine(
            a.distance(b),
            radius,
            toLocal.applyToPosition(position),
            toLocal.applyToDirection(direction),
            distance
        );

        for (vec3& intersection : segmentIntersections)
        {
            intersection = toGlobal.applyToPosition(intersection);
        }

        intersections.insert(
            intersections.end(),
            segmentIntersections.begin(),
            segmentIntersections.end()
        );
    }

    sort(
        intersections.begin(),
        intersections.end(),
        [=](const vec3& a, const vec3& b) -> bool
        {
            return position.distance(a) < position.distance(b);
        }
    );

    return intersections;
}

bool BodyPartRope::within(const vec3& position) const
{
    for (size_t i = 0; i < points.size() - 1; i++)
    {
        const vec3& a = points[i];
        const vec3& b = points[i + 1];

        vec3 direction = (b - a).normalize();
        f64 length = a.distance(b);

        mat4 toLocal = mat4(a, quaternion(direction, pickUpDirection(direction)), vec3(1)).inverse();

        vec3 local = toLocal.applyToPosition(position);

        if (
            local.distance(vec3()) < radius ||
            local.distance(vec3(0, length, 0)) < radius ||
            (local.y >= 0 && local.y < length && vec2(local.x, local.z).length() < radius)
        )
        {
            return true;
        }
    }

    return false;
}

f64 BodyPartRope::boundingRadius() const
{
    // no effect because this could severely deform the bounding sphere
    return 0;
}

BoundingBox BodyPartRope::boundingBox() const
{
    // no effect because this could severely deform the bounding box
    return BoundingBox(vec3(0), vec3(0));
}

bool BodyPartRope::operator==(const BodyPartRope& rhs) const
{
    return segmentCount == rhs.segmentCount &&
        length == rhs.length &&
        points == rhs.points &&
        radius == rhs.radius &&
        startColor == rhs.startColor &&
        endColor == rhs.endColor &&
        thin == rhs.thin &&
        constraintSubID == rhs.constraintSubID;
}

bool BodyPartRope::operator!=(const BodyPartRope& rhs) const
{
    return !(*this == rhs);
}

void BodyPartRope::setExtent(u32 segmentCount, f64 length)
{
    if (segmentCount != this->segmentCount || !roughly(length, this->length))
    {
        points = createDefaultRopePositions(segmentCount, length);
    }

    this->segmentCount = segmentCount;
    this->length = length;
}

template<>
BodyPartRope YAMLNode::convert() const
{
    BodyPartRope part;

    part.segmentCount = at("segmentCount").as<u32>();
    part.length = at("length").as<f64>();
    part.points = at("points").as<vector<vec3>>();
    part.radius = at("radius").as<f64>();
    part.startColor = at("startColor").as<EmissiveColor>();
    part.endColor = at("endColor").as<EmissiveColor>();
    part.thin = at("thin").as<bool>();
    part.constraintSubID = at("constraintSubID").as<BodyPartSubID>();

    return part;
}

template<>
void YAMLSerializer::emit(BodyPartRope rope)
{
    startMapping();

    emitPair("segmentCount", rope.segmentCount);
    emitPair("length", rope.length);
    emitPair("points", rope.points);
    emitPair("radius", rope.radius);
    emitPair("startColor", rope.startColor);
    emitPair("endColor", rope.endColor);
    emitPair("thin", rope.thin);
    emitPair("constraintSubID", rope.constraintSubID);

    endMapping();
}
