/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "network_event.hpp"

class World;
class Entity;

enum EntityUpdateFlags : u8
{
    Entity_Update_Flag_Render = 1,
    Entity_Update_Flag_Sound = 2,
    Entity_Update_Flag_Attachment = 4
};

enum EntityUpdateSideEffectType : u8
{
    Entity_Update_Display_Add,
    Entity_Update_Display_Remove,
    Entity_Update_Display_Update_Render,
    Entity_Update_Display_Update_Sound,
    Entity_Update_Attachment_Update
};

struct EntityUpdateSideEffect
{
    EntityUpdateSideEffect();
    EntityUpdateSideEffect(EntityID parentID, EntityID id, EntityUpdateSideEffectType type);

    EntityID parentID;
    EntityID id;
    EntityUpdateSideEffectType type;
};

class EntityUpdate
{
public:
    EntityUpdate(const World* world, const Entity* entity);

    template<typename T, typename EventT>
    void notify(const T& current, const T& previous, const function<void(EventT&)>& behavior, bool transformed = false)
    {
        if (current == previous)
        {
            return;
        }

        EventT event;
        event.world = worldName;
        event.id = entityID;
        event.transformed = transformed;

        behavior(event);

        _events.push_back(NetworkEvent(event));
    }

    template<typename T, typename EventT>
    void notifySet(
        const set<T>& current,
        const set<T>& previous,
        const function<void(EventT&, const T&)>& add,
        const function<void(EventT&, const T&)>& remove,
        bool transformed = false
    )
    {
        for (const T& i : previous)
        {
            if (!current.contains(i))
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                remove(event, i);

                _events.push_back(NetworkEvent(event));
            }
        }

        for (const T& i : current)
        {
            if (!previous.contains(i))
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                add(event, i);

                _events.push_back(NetworkEvent(event));
            }
        }
    }

    template<typename KeyT, typename ValT, typename EventT>
    void notifyMap(
        const map<KeyT, ValT>& current,
        const map<KeyT, ValT>& previous,
        const function<void(EventT&, const KeyT&, const ValT&)>& add,
        const function<void(EventT&, const KeyT&)>& remove,
        const function<void(EventT&, const KeyT&, const ValT&)>& update,
        bool transformed = false
    )
    {
        for (const auto& [ k, v ] : current)
        {
            auto it = previous.find(k);

            if (it == previous.end())
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                add(event, k, v);

                _events.push_back(NetworkEvent(event));
            }
            else
            {
                if (v != it->second)
                {
                    EventT event;
                    event.world = worldName;
                    event.id = entityID;
                    event.transformed = transformed;

                    update(event, k, v);

                    _events.push_back(NetworkEvent(event));
                }
            }
        }

        for (const auto& [ k, v ] : previous)
        {
            auto it = current.find(k);

            if (it == current.end())
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                remove(event, k);

                _events.push_back(NetworkEvent(event));
            }
        }
    }

    template<typename KeyT, typename ValT, typename EventT>
    void notifyMap(
        const map<KeyT, ValT>& current,
        const map<KeyT, ValT>& previous,
        const function<void(EventT&, const KeyT&, const ValT&)>& add,
        const function<void(EventT&, const KeyT&)>& remove,
        const function<void(const KeyT&, const ValT&, const ValT&)>& update,
        bool transformed = false
    )
    {
        for (const auto& [ k, v ] : current)
        {
            auto it = previous.find(k);

            if (it == previous.end())
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                add(event, k, v);

                _events.push_back(NetworkEvent(event));
            }
            else
            {
                if (v != it->second)
                {
                    update(k, v, it->second);
                }
            }
        }

        for (const auto& [ k, v ] : previous)
        {
            auto it = current.find(k);

            if (it == current.end())
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                remove(event, k);

                _events.push_back(NetworkEvent(event));
            }
        }
    }

    template<typename KeyT, typename ValT, typename EventT>
    void notifyMap(
        const map<KeyT, ValT>& current,
        const map<KeyT, ValT>& previous,
        const function<void(EventT&, const KeyT&, const ValT&)>& add,
        const function<void(EventT&, const KeyT&)>& remove,
        bool transformed = false
    )
    {
        for (const auto& [ k, v ] : current)
        {
            auto it = previous.find(k);

            if (it == previous.end())
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                add(event, k, v);

                _events.push_back(NetworkEvent(event));
            }
        }

        for (const auto& [ k, v ] : previous)
        {
            auto it = current.find(k);

            if (it == current.end())
            {
                EventT event;
                event.world = worldName;
                event.id = entityID;
                event.transformed = transformed;

                remove(event, k);

                _events.push_back(NetworkEvent(event));
            }
        }
    }

    void flag(u8 flags);

    template<typename T>
    void flag(const T& current, const T& previous, u8 flags)
    {
        if (current == previous)
        {
            return;
        }

        flag(flags);
    }

    const vector<NetworkEvent>& events() const;
    const vector<EntityUpdateSideEffect>& sideEffects() const;

    void addChild(Entity* child);
    void removeChild(Entity* child);
    void updateChild(const Entity* previousChild, Entity* child, const EntityUpdate& update);

    bool attachment() const;

private:
    const Entity* entity;
    string worldName;
    EntityID entityID;

    vector<NetworkEvent> _events;
    vector<EntityUpdateSideEffect> _sideEffects;

    bool _attachment;

    void push(const NetworkEvent& event);
    void push(const vector<NetworkEvent>& events);

    void push(const EntityUpdateSideEffect& sideEffect);
    void push(const vector<EntityUpdateSideEffect>& sideEffects);

    void attachmentUpdate(EntityID id);

    EntityID parentID() const;
};
