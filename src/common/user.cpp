/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "user.hpp"
#include "tools.hpp"
#include "cryptography.hpp"
#include "entity.hpp"
#include "game_state.hpp"
#include "script_tools.hpp"
#include "script_context.hpp"
#include "scope_closure.hpp"

static constexpr size_t saltLength = 32;

User::User(UserID id, const string& name, const string& world)
    : _id(id)
    , _name(name)
    , _salt("")
    , _password("")
    , _active(false)
    , _vr(false)
    , _admin(false)
    , _magnitude(0)
    , _advantage(0)
    , _doom(false)
    , _world(world)
    , _color(vec3(0.752, 0.109, 0.156))
    , _viewer({ 2, 2, 2 }, quaternion(upDir, radians(135)) * quaternion(rightDir, radians(-35)), vec3(1))
    , viewerForcedUpdate(false)
    , roomForcedUpdate(false)
    , _anchored(false)
    , _ping(0)
    , _angle(defaultCameraAngle)
    , _aspect(16.0 / 9.0)
#if defined(LMOD_SERVER)
    , signal(false)
    , _fade(false)
    , _wait(false)
    , _shouldKick(false)
#elif defined(LMOD_CLIENT)
    , _signal(false)
    , _fade(false)
    , _wait(false)
#endif
{

}

User::User(UserID id, const string& name, const string& world, const string& password)
    : User(id, name, world)
{
    _salt = randomSalt(saltLength);
    _password = sha256(password + _salt);
}

#if defined(LMOD_SERVER)
void User::step(ScriptContext* context, f64 stepInterval)
{
    if (titlecard && titlecard->timedOut())
    {
        callResponseHook(context, *titlecard);

        titlecard = {};
    }

    if (!requests.empty() && requests.front().timedOut())
    {
        callResponseHook(context, requests.front());

        requests.pop_front();

        if (!requests.empty())
        {
            requests.front().start();
        }
    }

    if (!attached())
    {
        return;
    }

    Entity* host = context->game->get(entityID());

    // Only adjusts host positions
    if (_tether.active)
    {
        vec3 center;
        size_t count = 0;

        for (UserID userID : _tether.userIDs)
        {
            if (userID == _id)
            {
                continue;
            }

            const User* user = context->game->getUser(userID);

            if (!user)
            {
                continue;
            }

            if (user->attached())
            {
                center += context->game->get(user->entityID())->globalPosition();
            }
            else if (user->vr())
            {
                center += user->room().position();
            }
            else
            {
                center += user->viewer().position();
            }

            count++;
        }

        center /= count;

        const vec3& position = host->globalPosition();

        if (center.distance(position) > _tether.distance)
        {
            host->setGlobalPosition(center + (position - center).normalize() * _tether.distance);
        }
    }

    if (_confinement.active)
    {
        const vec3& position = host->globalPosition();

        if (_confinement.position.distance(position) > _confinement.distance)
        {
            host->setGlobalPosition(_confinement.position + (position - _confinement.position).normalize() * _confinement.distance);
        }
    }
}
#endif

vector<NetworkEvent> User::compare(const User& previous) const
{
    vector<NetworkEvent> events;

    if (_admin != previous._admin)
    {
        UserEvent event(User_Event_Shift, _id);
        event.s = _world;

        events.push_back(event);
    }

    if (_world != previous._world)
    {
        UserEvent event(User_Event_Shift, _id);
        event.s = _world;

        events.push_back(event);
    }

    if (_viewer != previous._viewer)
    {
        UserEvent event(User_Event_Move_Viewer, _id);
        event.transform = _viewer;

        events.push_back(event);
    }

    if (_room != previous._room)
    {
        UserEvent event(User_Event_Move_Room, _id);
        event.transform = _room;

        events.push_back(event);
    }

    if (
        _head != previous._head ||
        _leftHand != previous._leftHand ||
        _rightHand != previous._rightHand ||
        _hips != previous._hips ||
        _leftFoot != previous._leftFoot ||
        _rightFoot != previous._rightFoot ||
        _chest != previous._chest ||
        _leftShoulder != previous._leftShoulder ||
        _rightShoulder != previous._rightShoulder ||
        _leftElbow != previous._leftElbow ||
        _rightElbow != previous._rightElbow ||
        _leftWrist != previous._leftWrist ||
        _rightWrist != previous._rightWrist ||
        _leftKnee != previous._leftKnee ||
        _rightKnee != previous._rightKnee ||
        _leftAnkle != previous._leftAnkle ||
        _rightAnkle != previous._rightAnkle
    )
    {
        UserEvent event(User_Event_Move_Devices, _id);

        if (_head)
        {
            event.mapping.insert({ VR_Device_Head, *_head });
        }

        if (_leftHand)
        {
            event.mapping.insert({ VR_Device_Left_Hand, *_leftHand });
        }

        if (_rightHand)
        {
            event.mapping.insert({ VR_Device_Right_Hand, *_rightHand });
        }

        if (_hips)
        {
            event.mapping.insert({ VR_Device_Hips, *_hips });
        }

        if (_leftFoot)
        {
            event.mapping.insert({ VR_Device_Left_Foot, *_leftFoot });
        }

        if (_rightFoot)
        {
            event.mapping.insert({ VR_Device_Right_Foot, *_rightFoot });
        }

        if (_chest)
        {
            event.mapping.insert({ VR_Device_Chest, *_chest });
        }

        if (_leftShoulder)
        {
            event.mapping.insert({ VR_Device_Left_Shoulder, *_leftShoulder });
        }

        if (_rightShoulder)
        {
            event.mapping.insert({ VR_Device_Right_Shoulder, *_rightShoulder });
        }

        if (_leftElbow)
        {
            event.mapping.insert({ VR_Device_Left_Elbow, *_leftElbow });
        }

        if (_rightElbow)
        {
            event.mapping.insert({ VR_Device_Right_Elbow, *_rightElbow });
        }

        if (_leftWrist)
        {
            event.mapping.insert({ VR_Device_Left_Wrist, *_leftWrist });
        }

        if (_rightWrist)
        {
            event.mapping.insert({ VR_Device_Right_Wrist, *_rightWrist });
        }

        if (_leftKnee)
        {
            event.mapping.insert({ VR_Device_Left_Knee, *_leftKnee });
        }

        if (_rightKnee)
        {
            event.mapping.insert({ VR_Device_Right_Knee, *_rightKnee });
        }

        if (_leftAnkle)
        {
            event.mapping.insert({ VR_Device_Left_Ankle, *_leftAnkle });
        }

        if (_rightAnkle)
        {
            event.mapping.insert({ VR_Device_Right_Ankle, *_rightAnkle });
        }

        events.push_back(event);
    }

    if (_angle != previous._angle)
    {
        UserEvent event(User_Event_Angle, _id);
        event.f = _angle;

        events.push_back(event);
    }

    if (_aspect != previous._aspect)
    {
        UserEvent event(User_Event_Aspect, _id);
        event.f = _aspect;

        events.push_back(event);
    }

    if (_bounds != previous._bounds)
    {
        UserEvent event(User_Event_Bounds, _id);
        event.bounds = _bounds;

        events.push_back(event);
    }

    if (_color != previous._color)
    {
        UserEvent event(User_Event_Color, _id);
        event.color = _color;

        events.push_back(event);
    }

    if (_teams != previous._teams)
    {
        UserEvent event(User_Event_Teams, _id);
        event.teams = _teams;

        events.push_back(event);
    }

    if (_active != previous._active || _vr != previous._vr)
    {
        UserEvent event(_active ? User_Event_Join : User_Event_Leave, _id);
        event.b = _vr;

        events.push_back(event);
    }

    if (_entityID != previous._entityID)
    {
        UserEvent event(_entityID ? User_Event_Attach : User_Event_Detach, _id);
        event.u = _entityID.value();

        events.push_back(event);
    }

    if (_anchored != previous._anchored || _anchorMapping != previous._anchorMapping)
    {
        UserEvent event(_anchored ? User_Event_Anchor : User_Event_Unanchor, _id);
        event.mapping = _anchorMapping;

        events.push_back(event);
    }

    if (_roomOffset != previous._roomOffset)
    {
        UserEvent event(User_Event_Room_Offset, _id);
        event.transform = _roomOffset;

        events.push_back(event);
    }

    if (_ping != previous._ping)
    {
        UserEvent event(User_Event_Ping, _id);
        event.u = _ping;

        events.push_back(event);
    }

    if (_magnitude != previous._magnitude)
    {
        UserEvent event(User_Event_Magnitude, _id);
        event.u = _magnitude;

        events.push_back(event);
    }

    if (_advantage != previous._advantage)
    {
        UserEvent event(User_Event_Advantage, _id);
        event.i = _advantage;

        events.push_back(event);
    }

    if (_doom != previous._doom)
    {
        UserEvent event(User_Event_Doom, _id);
        event.b = _doom;

        events.push_back(event);
    }

#if defined(LMOD_SERVER)
    if (_tether != previous._tether)
    {
        UserEvent event(User_Event_Tether, _id);
        event.b = _tether.active;
        event.f = _tether.distance;
        event.userIDs = _tether.userIDs;

        events.push_back(event);
    }

    if (_confinement != previous._confinement)
    {
        UserEvent event(User_Event_Confinement, _id);
        event.b = _confinement.active;
        event.transform = mat4(_confinement.position, quaternion(), vec3(1));
        event.f = _confinement.distance;

        events.push_back(event);
    }

    if (_fade != previous._fade)
    {
        UserEvent event(User_Event_Fade, _id);
        event.b = _fade;

        events.push_back(event);
    }

    if (_wait != previous._wait)
    {
        UserEvent event(User_Event_Wait, _id);
        event.b = _wait;

        events.push_back(event);
    }

    if (signal != previous.signal)
    {
        UserEvent event(User_Event_Signal, _id);
        event.b = signal;

        events.push_back(event);
    }

    if (titlecard != previous.titlecard)
    {
        UserEvent event(User_Event_Titlecard, _id);

        if (titlecard)
        {
            event.textRequests = { TextRequest(titlecard->id(), titlecard->data<TextTitlecardRequest>()) };
        }

        events.push_back(event);
    }

    if (requests != previous.requests)
    {
        UserEvent event(User_Event_Texts, _id);

        for (const TextTracker& tracker : requests)
        {
            switch (tracker.type())
            {
            default :
                fatal("Encountered unknown text tracker type '" + to_string(tracker.type()) + "'.");
            case Text_Timeout :
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextTimeoutRequest>()));
                break;
            case Text_Unary:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextUnaryRequest>()));
                break;
            case Text_Binary:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextBinaryRequest>()));
                break;
            case Text_Options:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextOptionsRequest>()));
                break;
            case Text_Choose:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextChooseRequest>()));
                break;
            case Text_Assign_Values:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextAssignValuesRequest>()));
                break;
            case Text_Spend_Points:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextSpendPointsRequest>()));
                break;
            case Text_Assign_Points:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextAssignPointsRequest>()));
                break;
            case Text_Vote:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextVoteRequest>()));
                break;
            case Text_Choose_Major:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextChooseMajorRequest>()));
                break;
            }
        }

        events.push_back(event);
    }

    if (knowledge != previous.knowledge)
    {
        UserEvent event(User_Event_Knowledge, _id);

        for (const TextKnowledgeRequest& knowledge : knowledge)
        {
            event.textRequests.push_back(TextRequest(TextID(), knowledge));
        }

        events.push_back(event);
    }
#endif

    return events;
}

vector<NetworkEvent> User::compareSelf(const User& previous, const Entity* host) const
{
    vector<NetworkEvent> events;

    if (_vr && _room != previous._room && roomForcedUpdate)
    {
        MoveEvent event;
        event.type = Move_Event_Room;
        event.transform = _room;

        events.push_back(event);
        roomForcedUpdate = false;
    }
    else if (!_vr && _viewer != previous._viewer && viewerForcedUpdate)
    {
        MoveEvent event;
        event.type = Move_Event_Viewer;
        event.transform = _viewer;

        events.push_back(event);
        viewerForcedUpdate = false;
    }

    if (_voiceChannel != previous._voiceChannel)
    {
        VoiceChannelEvent event;
        event.name = _voiceChannel;

        events.push_back(event);
    }

#if defined(LMOD_SERVER)
    if (commandHistory.size() != previous.commandHistory.size())
    {
        for (size_t i = previous.commandHistory.size(); i < commandHistory.size(); i++)
        {
            HistoryEvent event;
            event.command = commandHistory[i];

            events.push_back(event);
        }
    }

    if (messages.size() != previous.messages.size())
    {
        for (size_t i = previous.messages.size(); i < messages.size(); i++)
        {
            LogEvent event;
            event.message = messages[i];

            events.push_back(event);
        }
    }

    if (pings.size() != previous.pings.size())
    {
        for (size_t i = previous.pings.size(); i < pings.size(); i++)
        {
            GameEvent event(Game_Event_Ping);
            event.u = pings[i].value();

            events.push_back(event);
        }
    }

    if (hapticEffects.size() != previous.hapticEffects.size())
    {
        for (size_t i = previous.hapticEffects.size(); i < hapticEffects.size(); i++)
        {
            HapticEvent event;
            event.effect = hapticEffects[i];

            events.push_back(event);
        }
    }

    if (documents != previous.documents)
    {
        for (const auto& [ name, text ] : previous.documents)
        {
            auto it = documents.find(name);

            if (it == documents.end())
            {
                DocumentEvent event(Document_Event_Remove, name);

                events.push_back(event);
            }
        }

        for (const auto& [ name, text ] : documents)
        {
            auto it = previous.documents.find(name);

            if (it == previous.documents.end())
            {
                {
                    DocumentEvent event(Document_Event_Add, name);

                    events.push_back(event);
                }

                {
                    DocumentEvent event(Document_Event_Update, name, text);

                    events.push_back(event);
                }
            }
            else
            {
                DocumentEvent event(Document_Event_Update, name, text);

                events.push_back(event);
            }
        }
    }

    if (allTextRequests.size() != previous.allTextRequests.size())
    {
        for (size_t i = previous.allTextRequests.size(); i < allTextRequests.size(); i++)
        {
            TextRequestEvent event;
            event.request = allTextRequests[i];

            events.push_back(event);
        }
    }

    if (_tether != previous._tether)
    {
        TetherEvent event(_tether.active ? Tether_Event_Tether : Tether_Event_Untether);
        event.distance = _tether.distance;
        event.userIDs = _tether.userIDs;

        events.push_back(event);
    }

    if (_confinement != previous._confinement)
    {
        ConfinementEvent event(_confinement.active ? Confinement_Event_Confine : Confinement_Event_Unconfine);
        event.position = _confinement.position;
        event.distance = _confinement.distance;

        events.push_back(event);
    }

    if (_fade != previous._fade)
    {
        FadeEvent event(_fade ? Fade_Event_Fade : Fade_Event_Unfade);

        events.push_back(event);
    }

    if (_wait != previous._wait)
    {
        WaitEvent event(_wait ? Wait_Event_Wait : Wait_Event_Unwait);

        events.push_back(event);
    }

    if (_entityID != previous._entityID)
    {
        if (_entityID == EntityID())
        {
            DetachEvent event;

            events.push_back(event);
        }
        else
        {
            AttachEvent event(
                host->bindings(),
                host->hudElements(),
                host->fieldOfView(),
                host->mouseLocked(),
                host->voiceVolume(),
                host->tintColor(),
                host->tintStrength()
            );

            events.push_back(event);
        }
    }
#endif

    return events;
}

vector<NetworkEvent> User::initial() const
{
    vector<NetworkEvent> events;

    {
        UserEvent event(User_Event_Add, _id);
        event.s = _name;

        events.push_back(event);
    }

    if (_admin)
    {
        UserEvent event(User_Event_Admin, _id);
        event.b = true;

        events.push_back(event);
    }

    if (_active)
    {
        UserEvent event(User_Event_Join, _id);
        event.b = _vr;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Shift, _id);
        event.s = _world;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Move_Viewer, _id);
        event.transform = _viewer;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Move_Room, _id);
        event.transform = _room;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Move_Devices, _id);

        if (_head)
        {
            event.mapping.insert({ VR_Device_Head, *_head });
        }

        if (_leftHand)
        {
            event.mapping.insert({ VR_Device_Left_Hand, *_leftHand });
        }

        if (_rightHand)
        {
            event.mapping.insert({ VR_Device_Right_Hand, *_rightHand });
        }

        if (_hips)
        {
            event.mapping.insert({ VR_Device_Hips, *_hips });
        }

        if (_leftFoot)
        {
            event.mapping.insert({ VR_Device_Left_Foot, *_leftFoot });
        }

        if (_rightFoot)
        {
            event.mapping.insert({ VR_Device_Right_Foot, *_rightFoot });
        }

        if (_chest)
        {
            event.mapping.insert({ VR_Device_Chest, *_chest });
        }

        if (_leftShoulder)
        {
            event.mapping.insert({ VR_Device_Left_Shoulder, *_leftShoulder });
        }

        if (_rightShoulder)
        {
            event.mapping.insert({ VR_Device_Right_Shoulder, *_rightShoulder });
        }

        if (_leftElbow)
        {
            event.mapping.insert({ VR_Device_Left_Elbow, *_leftElbow });
        }

        if (_rightElbow)
        {
            event.mapping.insert({ VR_Device_Right_Elbow, *_rightElbow });
        }

        if (_leftWrist)
        {
            event.mapping.insert({ VR_Device_Left_Wrist, *_leftWrist });
        }

        if (_rightWrist)
        {
            event.mapping.insert({ VR_Device_Right_Wrist, *_rightWrist });
        }

        if (_leftKnee)
        {
            event.mapping.insert({ VR_Device_Left_Knee, *_leftKnee });
        }

        if (_rightKnee)
        {
            event.mapping.insert({ VR_Device_Right_Knee, *_rightKnee });
        }

        if (_leftAnkle)
        {
            event.mapping.insert({ VR_Device_Left_Ankle, *_leftAnkle });
        }

        if (_rightAnkle)
        {
            event.mapping.insert({ VR_Device_Right_Ankle, *_rightAnkle });
        }

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Angle, _id);
        event.f = _angle;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Aspect, _id);
        event.f = _aspect;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Bounds, _id);
        event.bounds = _bounds;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Color, _id);
        event.color = _color;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Teams, _id);
        event.teams = _teams;

        events.push_back(event);
    }

    if (_entityID)
    {
        UserEvent event(User_Event_Attach, _id);
        event.u = _entityID.value();

        events.push_back(event);
    }

    if (_anchored)
    {
        UserEvent event(User_Event_Anchor, _id);
        event.mapping = _anchorMapping;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Room_Offset, _id);
        event.transform = _roomOffset;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Ping, _id);
        event.u = _ping;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Magnitude, _id);
        event.u = _magnitude;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Advantage, _id);
        event.i = _advantage;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Doom, _id);
        event.i = _doom;

        events.push_back(event);
    }

#if defined(LMOD_SERVER)
    {
        UserEvent event(User_Event_Tether, _id);
        event.b = _tether.active;
        event.f = _tether.distance;
        event.userIDs = _tether.userIDs;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Confinement, _id);
        event.b = _confinement.active;
        event.transform = mat4(_confinement.position, quaternion(), vec3(1));
        event.f = _confinement.distance;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Fade, _id);
        event.b = _fade;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Wait, _id);
        event.b = _wait;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Signal, _id);
        event.b = signal;

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Titlecard, _id);

        if (titlecard)
        {
            event.textRequests = { TextRequest(titlecard->id(), titlecard->data<TextTitlecardRequest>()) };
        }

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Texts, _id);

        for (const TextTracker& tracker : requests)
        {
            switch (tracker.type())
            {
            default :
                fatal("Encountered unknown text tracker type '" + to_string(tracker.type()) + "'.");
            case Text_Timeout :
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextTimeoutRequest>()));
                break;
            case Text_Unary:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextUnaryRequest>()));
                break;
            case Text_Binary:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextBinaryRequest>()));
                break;
            case Text_Options:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextOptionsRequest>()));
                break;
            case Text_Choose:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextChooseRequest>()));
                break;
            case Text_Assign_Values:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextAssignValuesRequest>()));
                break;
            case Text_Spend_Points:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextSpendPointsRequest>()));
                break;
            case Text_Assign_Points:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextAssignPointsRequest>()));
                break;
            case Text_Vote:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextVoteRequest>()));
                break;
            case Text_Choose_Major:
                event.textRequests.push_back(TextRequest(tracker.id(), tracker.data<TextChooseMajorRequest>()));
                break;
            }
        }

        events.push_back(event);
    }

    {
        UserEvent event(User_Event_Knowledge, _id);

        for (const TextKnowledgeRequest& knowledge : knowledge)
        {
            event.textRequests.push_back(TextRequest(TextID(), knowledge));
        }

        events.push_back(event);
    }
#endif

    return events;
}

vector<NetworkEvent> User::initialSelf() const
{
    vector<NetworkEvent> events;

    if (_vr)
    {
        MoveEvent event;
        event.type = Move_Event_Room;
        event.transform = _room;

        events.push_back(event);
    }
    else
    {
        MoveEvent event;
        event.type = Move_Event_Viewer;
        event.transform = _viewer;

        events.push_back(event);
    }

    if (!_voiceChannel.empty())
    {
        VoiceChannelEvent event;
        event.name = _voiceChannel;

        events.push_back(event);
    }

#if defined(LMOD_SERVER)
    for (const string& command : commandHistory)
    {
        HistoryEvent event;
        event.command = command;

        events.push_back(event);
    }

    for (const Message& message : messages)
    {
        LogEvent event;
        event.message = message;

        events.push_back(event);
    }

    for (const auto& [ name, text ] : documents)
    {
        {
            DocumentEvent event;
            event.type = Document_Event_Add;
            event.name = name;

            events.push_back(event);
        }

        {
            DocumentEvent event;
            event.type = Document_Event_Update;
            event.name = name;
            event.text = text;

            events.push_back(event);
        }
    }

    if (signal)
    {
        TextSignalRequest request;
        request.state = true;

        TextRequestEvent event;
        event.request = TextRequest(TextID(), request);

        events.push_back(event);
    }

    if (titlecard)
    {
        events.push_back(titlecard->event());
    }

    for (const TextTracker& tracker : requests)
    {
        events.push_back(tracker.event());
    }

    for (const TextKnowledgeRequest& request : knowledge)
    {
        TextRequestEvent event;
        event.request = TextRequest(TextID(), request);

        events.push_back(event);
    }

    if (_tether.active)
    {
        TetherEvent event;
        event.type = Tether_Event_Tether;
        event.distance = _tether.distance;
        event.userIDs = _tether.userIDs;

        events.push_back(event);
    }

    if (_confinement.active)
    {
        ConfinementEvent event;
        event.type = Confinement_Event_Confine;
        event.position = _confinement.position;
        event.distance = _confinement.distance;

        events.push_back(event);
    }

    if (_fade)
    {
        FadeEvent event;
        event.type = Fade_Event_Fade;

        events.push_back(event);
    }

    if (_wait)
    {
        WaitEvent event;
        event.type = Wait_Event_Wait;

        events.push_back(event);
    }
#endif

    return events;
}

UserID User::id() const
{
    return _id;
}

const string& User::name() const
{
    return _name;
}

const string& User::salt() const
{
    return _salt;
}

const string& User::password() const
{
    return _password;
}

void User::activate(bool vr)
{
    _active = true;
    _vr = vr;
}

void User::deactivate()
{
    _active = false;
    _vr = false;
    viewerForcedUpdate = false;
    roomForcedUpdate = false;
}

bool User::active() const
{
    return _active;
}

bool User::vr() const
{
    return _vr;
}

void User::attach(EntityID id)
{
    _entityID = id;
}

void User::detach()
{
    _entityID = EntityID();
}

bool User::attached() const
{
    return _entityID;
}

EntityID User::entityID() const
{
    return _entityID;
}

void User::setAdmin(bool state)
{
    _admin = state;
}

bool User::admin() const
{
    return _admin;
}

void User::setTeams(const set<string>& teams)
{
    _teams = teams;
}

const set<string>& User::teams() const
{
    return _teams;
}

void User::setMagnitude(u32 magnitude)
{
    _magnitude = magnitude;
}

u32 User::magnitude() const
{
    return _magnitude;
}

void User::setAdvantage(i32 advantage)
{
    _advantage = advantage;
}

i32 User::advantage() const
{
    return _advantage;
}

void User::setDoom(bool state)
{
    _doom = state;
}

bool User::doom() const
{
    return _doom;
}

GripState& User::gripStates(size_t index)
{
    return _gripStates[index];
}

const GripState& User::gripStates(size_t index) const
{
    return _gripStates[index];
}

void User::shift(const string& world)
{
    _world = world;
}

const string& User::world() const
{
    return _world;
}

void User::setVoiceChannel(const string& name)
{
    _voiceChannel = name;
}

const string& User::voiceChannel() const
{
    return _voiceChannel;
}

void User::setColor(const vec3& color)
{
    _color = color;
}

const vec3& User::color() const
{
    return _color;
}

void User::setViewer(const mat4& transform, bool forced)
{
    _viewer = transform;
    viewerForcedUpdate = _active && forced;
}

const mat4& User::viewer() const
{
    return _viewer;
}

void User::setRoom(const mat4& transform, bool forced)
{
    _room = transform;
    roomForcedUpdate = _active && forced;
}

const mat4& User::room() const
{
    return _room;
}

void User::setRoomOffset(const mat4& transform)
{
    _roomOffset = transform;
}

const mat4& User::roomOffset() const
{
    return _roomOffset;
}

void User::setHead(const mat4& transform)
{
    _head = transform;
}

const optional<mat4>& User::head() const
{
    return _head;
}

void User::setLeftHand(const mat4& transform)
{
    _leftHand = transform;
}

const optional<mat4>& User::leftHand() const
{
    return _leftHand;
}

void User::setRightHand(const mat4& transform)
{
    _rightHand = transform;
}

const optional<mat4>& User::rightHand() const
{
    return _rightHand;
}

void User::setHips(const mat4& transform)
{
    _hips = transform;
}

const optional<mat4>& User::hips() const
{
    return _hips;
}

void User::setLeftFoot(const mat4& transform)
{
    _leftFoot = transform;
}

const optional<mat4>& User::leftFoot() const
{
    return _leftFoot;
}

void User::setRightFoot(const mat4& transform)
{
    _rightFoot = transform;
}

const optional<mat4>& User::rightFoot() const
{
    return _rightFoot;
}

void User::setChest(const mat4& transform)
{
    _chest = transform;
}

const optional<mat4>& User::chest() const
{
    return _chest;
}

void User::setLeftShoulder(const mat4& transform)
{
    _leftShoulder = transform;
}

const optional<mat4>& User::leftShoulder() const
{
    return _leftShoulder;
}

void User::setRightShoulder(const mat4& transform)
{
    _rightShoulder = transform;
}

const optional<mat4>& User::rightShoulder() const
{
    return _rightShoulder;
}

void User::setLeftElbow(const mat4& transform)
{
    _leftElbow = transform;
}

const optional<mat4>& User::leftElbow() const
{
    return _leftElbow;
}

void User::setRightElbow(const mat4& transform)
{
    _rightElbow = transform;
}

const optional<mat4>& User::rightElbow() const
{
    return _rightElbow;
}

void User::setLeftWrist(const mat4& transform)
{
    _leftWrist = transform;
}

const optional<mat4>& User::leftWrist() const
{
    return _leftWrist;
}

void User::setRightWrist(const mat4& transform)
{
    _rightWrist = transform;
}

const optional<mat4>& User::rightWrist() const
{
    return _rightWrist;
}

void User::setLeftKnee(const mat4& transform)
{
    _leftKnee = transform;
}

const optional<mat4>& User::leftKnee() const
{
    return _leftKnee;
}

void User::setRightKnee(const mat4& transform)
{
    _rightKnee = transform;
}

const optional<mat4>& User::rightKnee() const
{
    return _rightKnee;
}

void User::setLeftAnkle(const mat4& transform)
{
    _leftAnkle = transform;
}

const optional<mat4>& User::leftAnkle() const
{
    return _leftAnkle;
}

void User::setRightAnkle(const mat4& transform)
{
    _rightAnkle = transform;
}

const optional<mat4>& User::rightAnkle() const
{
    return _rightAnkle;
}

void User::anchor(const map<VRDevice, mat4>& mapping)
{
    _anchored = true;
    _anchorMapping = mapping;
}

void User::unanchor()
{
    _anchored = false;
    _anchorMapping.clear();
}

bool User::anchored() const
{
    return _anchored;
}

const map<VRDevice, mat4>& User::anchorMapping() const
{
    return _anchorMapping;
}

void User::setPing(u32 ping)
{
    _ping = ping;
}

u32 User::ping() const
{
    return _ping;
}

void User::setAngle(f64 angle)
{
    _angle = angle;
}

f64 User::angle() const
{
    return _angle;
}

void User::setAspect(f64 aspect)
{
    _aspect = aspect;
}

f64 User::aspect() const
{
    return _aspect;
}

void User::setBounds(const vec2& bounds)
{
    _bounds = bounds;
}

const vec2& User::bounds() const
{
    return _bounds;
}

#if defined(LMOD_SERVER)
void User::storeCommand(const string& text)
{
    partialCommand = text;
}

string User::loadCommand()
{
    string result = partialCommand;

    partialCommand = "";

    return result;
}

void User::recordHistory(const string& text)
{
    commandHistory.push_back(text);
}

void User::logScript(const string& message)
{
    messages.push_back(Message(Message_Script, "", message));
}

void User::log(const string& message)
{
    messages.push_back(Message(Message_Log, "", message));
}

void User::error(const string& message)
{
    messages.push_back(Message(Message_Error, "", message));
}

void User::chat(const User* user, const string& message)
{
    messages.push_back(Message(Message_Chat, user->name(), message));
}

void User::directChatIn(const User* user, const string& message)
{
    messages.push_back(Message(Message_Direct_Chat_In, user->name(), message));
}

void User::directChatOut(const User* user, const string& message)
{
    messages.push_back(Message(Message_Direct_Chat_Out, user->name(), message));
}

void User::teamChatIn(const User* user, const string& name, const string& message)
{
    messages.push_back(Message(Message_Team_Chat_In, user->name() + " (team " + name + ")", message));
}

void User::teamChatOut(const string& name, const string& message)
{
    messages.push_back(Message(Message_Team_Chat_Out, "team " + name, message));
}

void User::pushMessage(const Message& message)
{
    messages.push_back(message);
}

void User::addPing(UserID id)
{
    pings.push_back(id);
}

void User::addHaptic(const HapticEffect& effect)
{
    hapticEffects.push_back(effect);
}

void User::addDocument(const string& name)
{
    documents.insert({ name, "" });
}

void User::removeDocument(const string& name)
{
    documents.erase(name);
}

void User::updateDocument(const string& name, const string& text)
{
    documents.insert_or_assign(name, text);
}

void User::requestText(const TextRequest& request, UserID userID, const optional<Lambda>& callback)
{
    allTextRequests.push_back(request);

    TextID frontID = !requests.empty() ? requests.front().id() : TextID();

    switch (request.type())
    {
    case Text_Signal :
        signal = request.data<TextSignalRequest>().state;
        break;
    case Text_Titlecard :
        titlecard = TextTracker(request.id(), request.data<TextTitlecardRequest>(), userID, callback);
        titlecard->start();
        break;
    case Text_Timeout :
        requests.push_back(TextTracker(request.id(), request.data<TextTimeoutRequest>(), userID, callback));
        break;
    case Text_Unary :
        requests.push_back(TextTracker(request.id(), request.data<TextUnaryRequest>(), userID, callback));
        break;
    case Text_Binary :
        requests.push_back(TextTracker(request.id(), request.data<TextBinaryRequest>(), userID, callback));
        break;
    case Text_Options :
        requests.push_back(TextTracker(request.id(), request.data<TextOptionsRequest>(), userID, callback));
        break;
    case Text_Choose :
        requests.push_back(TextTracker(request.id(), request.data<TextChooseRequest>(), userID, callback));
        break;
    case Text_Assign_Values :
        requests.push_back(TextTracker(request.id(), request.data<TextAssignValuesRequest>(), userID, callback));
        break;
    case Text_Spend_Points :
        requests.push_back(TextTracker(request.id(), request.data<TextSpendPointsRequest>(), userID, callback));
        break;
    case Text_Assign_Points :
        requests.push_back(TextTracker(request.id(), request.data<TextAssignPointsRequest>(), userID, callback));
        break;
    case Text_Vote :
        requests.push_back(TextTracker(request.id(), request.data<TextVoteRequest>(), userID, callback));
        break;
    case Text_Choose_Major :
        requests.push_back(TextTracker(request.id(), request.data<TextChooseMajorRequest>(), userID, callback));
        break;
    case Text_Knowledge :
    {
        bool found = false;

        for (TextKnowledgeRequest& item : knowledge)
        {
            if (request.data<TextKnowledgeRequest>().title == item.title)
            {
                item = request.data<TextKnowledgeRequest>();
                found = true;
                break;
            }
        }

        if (!found)
        {
            knowledge.push_back(request.data<TextKnowledgeRequest>());
        }
        break;
    }
    case Text_Clear :
        switch (request.data<TextClearRequest>().type)
        {
        case Text_Clear_All :
            knowledge.clear();
            [[fallthrough]];
        case Text_Clear_All_But_Knowledge :
            signal = false;
            titlecard = {};
            requests.clear();
            break;
        case Text_Clear_Specific_Knowledge :
            for (auto it = knowledge.begin(); it != knowledge.end(); it++)
            {
                if (it->title == request.data<TextClearRequest>().title)
                {
                    knowledge.erase(it);
                    break;
                }
            }
        }
        break;
    }

    if (!requests.empty() && (!frontID || requests.front().id() != frontID))
    {
        requests.front().start();
    }
}

void User::respondToText(ScriptContext* context, const TextResponse& response)
{
    switch (response.type())
    {
    default :
        break;
    case Text_Signal :
    case Text_Titlecard :
    case Text_Timeout :
    case Text_Knowledge :
    case Text_Clear :
        return;
    }

    TextID frontID = !requests.empty() ? requests.front().id() : TextID();
    size_t index = requests.size();

    for (size_t i = 0; i < requests.size(); i++)
    {
        if (requests[i].matches(response))
        {
            index = i;
            break;
        }
    }

    if (index == requests.size())
    {
        return;
    }

    const TextTracker& tracker = requests[index];

    bool success = false;

    switch (response.type())
    {
    default :
        break;
    case Text_Unary :
        if (tracker.data<TextUnaryRequest>().valid(response.data<TextUnaryResponse>()))
        {
            respondToUnary(context, tracker, response.data<TextUnaryResponse>());
            success = true;
        }
        break;
    case Text_Binary :
        if (tracker.data<TextBinaryRequest>().valid(response.data<TextBinaryResponse>()))
        {
            respondToBinary(context, tracker, response.data<TextBinaryResponse>());
            success = true;
        }
        break;
    case Text_Options :
        if (tracker.data<TextOptionsRequest>().valid(response.data<TextOptionsResponse>()))
        {
            respondToOptions(context, tracker, response.data<TextOptionsResponse>());
            success = true;
        }
        break;
    case Text_Choose :
        if (tracker.data<TextChooseRequest>().valid(response.data<TextChooseResponse>()))
        {
            respondToChoose(context, tracker, response.data<TextChooseResponse>());
            success = true;
        }
        break;
    case Text_Assign_Values :
        if (tracker.data<TextAssignValuesRequest>().valid(response.data<TextAssignValuesResponse>()))
        {
            respondToAssignValues(context, tracker, response.data<TextAssignValuesResponse>());
            success = true;
        }
        break;
    case Text_Spend_Points :
        if (tracker.data<TextSpendPointsRequest>().valid(response.data<TextSpendPointsResponse>()))
        {
            respondToSpendPoints(context, tracker, response.data<TextSpendPointsResponse>());
            success = true;
        }
        break;
    case Text_Assign_Points :
        if (tracker.data<TextAssignPointsRequest>().valid(response.data<TextAssignPointsResponse>()))
        {
            respondToAssignPoints(context, tracker, response.data<TextAssignPointsResponse>());
            success = true;
        }
        break;
    case Text_Vote :
        if (tracker.data<TextVoteRequest>().valid(response.data<TextVoteResponse>()))
        {
            respondToVote(context, tracker, response.data<TextVoteResponse>());
            success = true;
        }
        break;
    case Text_Choose_Major :
        if (tracker.data<TextChooseMajorRequest>().valid(response.data<TextChooseMajorResponse>()))
        {
            respondToChooseMajor(context, tracker, response.data<TextChooseMajorResponse>());
            success = true;
        }
        break;
    }

    if (success)
    {
        requests.erase(requests.begin() + index);

        if (!requests.empty() && (!frontID || requests.front().id() != frontID))
        {
            requests.front().start();
        }
    }
}

void User::tether(f64 distance, const set<UserID>& userIDs)
{
    _tether = Tether(true, distance, userIDs);
}

void User::untether()
{
    _tether = Tether();
}

const Tether& User::tether() const
{
    return _tether;
}

void User::confine(const vec3& position, f64 distance)
{
    _confinement = Confinement(true, position, distance);
}

void User::unconfine()
{
    _confinement = Confinement();
}

const Confinement& User::confinement() const
{
    return _confinement;
}

void User::setFade(bool state)
{
    _fade = state;
}

bool User::fade() const
{
    return _fade;
}

void User::setWait(bool state)
{
    _wait = state;
}

bool User::wait() const
{
    return _wait;
}

void User::kick()
{
    _shouldKick = true;
}

void User::resetKick()
{
    _shouldKick = false;
}

bool User::shouldKick() const
{
    return _shouldKick;
}
#elif defined(LMOD_CLIENT)
void User::setSignal(bool signal)
{
    _signal = signal;
}

bool User::signal() const
{
    return _signal;
}

void User::setTitlecard(const optional<TextTitlecardRequest>& titlecard)
{
    _titlecard = titlecard;
}

const optional<TextTitlecardRequest>& User::titlecard() const
{
    return _titlecard;
}

void User::setTexts(const deque<TextRequest>& texts)
{
    _texts = texts;
}

const deque<TextRequest>& User::texts() const
{
    return _texts;
}

void User::setKnowledge(const vector<TextKnowledgeRequest>& knowledge)
{
    _knowledge = knowledge;
}

const vector<TextKnowledgeRequest>& User::knowledge() const
{
    return _knowledge;
}

void User::setTether(const Tether& tether)
{
    _tether = tether;
}

const Tether& User::tether() const
{
    return _tether;
}

void User::setConfinement(const Confinement& confinement)
{
    _confinement = confinement;
}

const Confinement& User::confinement() const
{
    return _confinement;
}

void User::setFade(bool state)
{
    _fade = state;
}

bool User::fade() const
{
    return _fade;
}

void User::setWait(bool state)
{
    _wait = state;
}

bool User::wait() const
{
    return _wait;
}
#endif

#if defined(LMOD_SERVER)
void User::respondToUnary(ScriptContext* context, const TextTracker& tracker, const TextUnaryResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    user->pushMessage(Message(Message_Log, "", "user-confirmed-text", {
        _name,
        tracker.data<TextUnaryRequest>().text
    }));

    callResponseHook(context, tracker);
}

void User::respondToBinary(ScriptContext* context, const TextTracker& tracker, const TextBinaryResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text-" + string(response.choice ? "yes" : "no"), {
        _name,
        tracker.data<TextBinaryRequest>().text
    }));

    Value value;
    value.type = Value_Integer;
    value.i = response.choice;

    callResponseHook(context, tracker, value);
}

void User::respondToOptions(ScriptContext* context, const TextTracker& tracker, const TextOptionsResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextOptionsRequest>().title,
        response.choice
    }));

    Value value;
    value.type = Value_String;
    value.s = response.choice;

    callResponseHook(context, tracker, value);
}

void User::respondToChoose(ScriptContext* context, const TextTracker& tracker, const TextChooseResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextChooseRequest>().title,
        join(response.choices, ", ")
    }));

    Value value;
    value.type = Value_List;
    value.l.reserve(response.choices.size());

    for (const string& choice : response.choices)
    {
        value.l.push_back(symbol(choice));
    }

    callResponseHook(context, tracker, value);
}

void User::respondToAssignValues(ScriptContext* context, const TextTracker& tracker, const TextAssignValuesResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    string s;

    size_t i = 0;
    for (const auto& [ name, value ] : response.assignment)
    {
        s += name + ": " + value;

        if (i + 1 != response.assignment.size())
        {
            s += ", ";
        }

        i++;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextAssignValuesRequest>().title,
        s
    }));

    Value value;
    value.type = Value_List;
    value.l.reserve(response.assignment.size());

    for (const auto& [ name, val ] : response.assignment)
    {
        value.l.push_back(composeList(symbol(name), symbol(val)));
    }

    callResponseHook(context, tracker, value);
}

void User::respondToSpendPoints(ScriptContext* context, const TextTracker& tracker, const TextSpendPointsResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextSpendPointsRequest>().title,
        join(response.purchases, ", ")
    }));

    Value value;
    value.type = Value_List;
    value.l.reserve(response.purchases.size());

    for (const string& name : response.purchases)
    {
        value.l.push_back(symbol(name));
    }

    callResponseHook(context, tracker, value);
}

void User::respondToAssignPoints(ScriptContext* context, const TextTracker& tracker, const TextAssignPointsResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    string s;

    size_t i = 0;
    for (const auto& [ name, points ] : response.assignment)
    {
        s += name + ": " + to_string(points);

        if (i + 1 != response.assignment.size())
        {
            s += ", ";
        }

        i++;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextAssignPointsRequest>().title,
        s
    }));

    Value value;
    value.type = Value_List;
    value.l.reserve(response.assignment.size());

    for (const auto& [ name, points ] : response.assignment)
    {
        value.l.push_back(composeList(symbol(name), toValue(points)));
    }

    callResponseHook(context, tracker, value);
}

void User::respondToVote(ScriptContext* context, const TextTracker& tracker, const TextVoteResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    string s;

    size_t i = 0;
    for (u64 vote : response.votes)
    {
        s += tracker.data<TextVoteRequest>().options[vote];

        if (i + 1 != response.votes.size())
        {
            s += ", ";
        }

        i++;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextVoteRequest>().title,
        s
    }));

    Value value;
    value.type = Value_List;
    value.l.reserve(response.votes.size());

    for (u64 vote : response.votes)
    {
        value.l.push_back(toValue(vote));
    }

    callResponseHook(context, tracker, value);
}

void User::respondToChooseMajor(ScriptContext* context, const TextTracker& tracker, const TextChooseMajorResponse& response)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    user->pushMessage(Message(Message_Log, "", "user-responded-to-text", {
        _name,
        tracker.data<TextChooseMajorRequest>().title,
        get<0>(tracker.data<TextChooseMajorRequest>().options[response.choice])
    }));

    Value value;
    value.type = Value_Integer;
    value.i = response.choice;

    callResponseHook(context, tracker, value);
}

void User::callResponseHook(ScriptContext* context, const TextTracker& tracker)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    if (!tracker.lambda())
    {
        return;
    }

    Lambda lambda = *tracker.lambda();

    ScopeClosure closure(context);

    runSafely(context, lambda, {}, "running text response hook", user);
}

void User::callResponseHook(ScriptContext* context, const TextTracker& tracker, const Value& value)
{
    User* user = context->game->getUser(tracker.userID());

    if (!user)
    {
        return;
    }

    if (!tracker.lambda())
    {
        return;
    }

    Lambda lambda = *tracker.lambda();

    ScopeClosure closure(context);

    runSafely(context, lambda, { value }, "running text response hook", user);
}
#endif

template<>
User YAMLNode::convert() const
{
    User user(at("id").as<UserID>(), at("name").as<string>(), at("world").as<string>());

    user._salt = fromHex(at("salt").as<string>());
    user._password = fromHex(at("password").as<string>());
    user._entityID = at("entityID").as<EntityID>();
    user._teams = at("teams").as<set<string>>();
    user._magnitude = at("magnitude").as<u32>();
    user._advantage = at("advantage").as<i32>();
    user._doom = at("doom").as<bool>();
    user._voiceChannel = at("voiceChannel").as<string>();
    user._color = at("color").as<vec3>();

    user._viewer = at("viewer").as<mat4>();
    user._room = at("room").as<mat4>();
    user._roomOffset = at("roomOffset").as<mat4>();

    user._anchored = at("anchored").as<bool>();
    user._anchorMapping = at("anchorMapping").as<map<VRDevice, mat4>>();

#if defined(LMOD_SERVER)
    user.partialCommand = at("partialCommand").as<string>();
    user.commandHistory = at("commandHistory").as<vector<string>>();

    user.messages = at("messages").as<vector<Message>>();
    user.documents = at("documents").as<map<string, string>>();

    user.signal = at("signal").as<bool>();
    user.titlecard = at("titlecard").as<optional<TextTracker>>();
    user.requests = at("requests").as<deque<TextTracker>>();
    user.knowledge = at("knowledge").as<vector<TextKnowledgeRequest>>();
    user._tether = at("tether").as<Tether>();
    user._confinement = at("confinement").as<Confinement>();
    user._fade = at("fade").as<bool>();
    user._wait = at("wait").as<bool>();
#endif

    return user;
}

template<>
void YAMLSerializer::emit(User v)
{
    startMapping();

    emitPair("id", v._id);
    emitPair("name", v._name);
    emitPair("world", v._world);

    emitPair("salt", toHex(v._salt));
    emitPair("password", toHex(v._password));
    emitPair("entityID", v._entityID);
    emitPair("teams", v._teams);
    emitPair("magnitude", v._magnitude);
    emitPair("advantage", v._advantage);
    emitPair("doom", v._doom);
    emitPair("voiceChannel", v._voiceChannel);
    emitPair("color", v._color);

    emitPair("viewer", v._viewer);
    emitPair("room", v._room);
    emitPair("roomOffset", v._roomOffset);

    emitPair("anchored", v._anchored);
    emitPair("anchorMapping", v._anchorMapping);

#if defined(LMOD_SERVER)
    emitPair("partialCommand", v.partialCommand);
    emitPair("commandHistory", v.commandHistory);

    emitPair("messages", v.messages);
    emitPair("documents", v.documents);

    emitPair("signal", v.signal);
    emitPair("titlecard", v.titlecard);
    emitPair("requests", v.requests);
    emitPair("knowledge", v.knowledge);
    emitPair("tether", v._tether);
    emitPair("confinement", v._confinement);
    emitPair("fade", v._fade);
    emitPair("wait", v._wait);
#endif

    endMapping();
}

void userMove(User* user, Entity* host, const mat4& transform)
{
    if (host)
    {
        vec3 posOffset = host->globalPosition() - user->viewer().position();
        quaternion rotOffset = (host->globalRotation() / user->viewer().rotation()).normalize();

        host->setGlobalPosition(transform.position() + posOffset);
        host->setGlobalRotation(transform.rotation() * rotOffset);
    }

    if (user->vr())
    {
        user->setRoom(transform, true);
    }
    else
    {
        user->setViewer(transform, true);
    }
}

void userGoto(const vector<tuple<User*, Entity*>>& users, const vec3& position, f64 radius, const vec3& up)
{
    f64 angle = 0;
    f64 circumference = 2 * pi * radius;

    for (const auto& [ user, host ] : users)
    {
        quaternion rotation(up, angle);

        mat4 transform = {
            position + rotation.rotate(backDir * radius),
            rotation,
            vec3(1)
        };

        userMove(user, host, transform);

        f64 boundingRadius;

        if (host)
        {
            boundingRadius = host->boundingRadius();
        }
        else
        {
            boundingRadius = circumference / users.size();
        }

        angle += (boundingRadius / circumference) * 2 * pi;
    }
}
