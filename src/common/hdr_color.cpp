/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hdr_color.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

HDRColor::HDRColor()
{
    color = vec3(1);
    intensity = 1;
}

HDRColor::HDRColor(const vec3& color, f64 intensity)
    : color(color)
    , intensity(intensity)
{

}

void HDRColor::verify()
{
    color = verifyColor(color);
    intensity = max(intensity, 0.0);
}

bool HDRColor::operator==(const HDRColor& rhs) const
{
    return color == rhs.color && intensity == rhs.intensity;
}

bool HDRColor::operator!=(const HDRColor& rhs) const
{
    return !(*this == rhs);
}

vec3 HDRColor::toVec3() const
{
    return color * intensity;
}

vec4 HDRColor::toVec4() const
{
    return vec4(color * intensity, 1);
}

template<>
HDRColor YAMLNode::convert() const
{
    HDRColor color;

    color.color = at("color").as<vec3>();
    color.intensity = at("intensity").as<f64>();

    return color;
}

template<>
void YAMLSerializer::emit(HDRColor v)
{
    startMapping();

    emitPair("color", v.color);
    emitPair("intensity", v.intensity);

    endMapping();
}

ostream& operator<<(ostream& out, const HDRColor& color)
{
    out << "(" << color.color << " @ " << color.intensity << ")";

    return out;
}
