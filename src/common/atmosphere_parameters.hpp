/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "hdr_color.hpp"
#include "emissive_color.hpp"

enum AtmosphereEffectType : u8
{
    Atmosphere_Effect_Line,
    Atmosphere_Effect_Solid,
    Atmosphere_Effect_Cloud,
    Atmosphere_Effect_Streamer,
    Atmosphere_Effect_Warp
};

string atmosphereEffectTypeToString(AtmosphereEffectType type);
AtmosphereEffectType atmosphereEffectTypeFromString(const string& s);

struct AtmosphereEffect
{
    AtmosphereEffect();

    void verify();

    bool operator==(const AtmosphereEffect& rhs) const;
    bool operator!=(const AtmosphereEffect& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(type),
            density,
            size,
            radius,
            mass,
            flowVelocity,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<AtmosphereEffectType>(get<0>(payload));
        density = get<1>(payload);
        size = get<2>(payload);
        radius = get<3>(payload);
        mass = get<4>(payload);
        flowVelocity = get<5>(payload);
        color = get<6>(payload);
    }

    typedef msgpack::type::tuple<u8, f64, f64, f64, f64, vec3, EmissiveColor> layout;

    AtmosphereEffectType type;
    f64 density;
    f64 size;
    f64 radius;
    f64 mass;
    vec3 flowVelocity;
    EmissiveColor color;
};

struct AtmosphereLightning
{
    AtmosphereLightning();

    void verify();

    bool operator==(const AtmosphereLightning& rhs) const;
    bool operator!=(const AtmosphereLightning& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            frequency,
            minDistance,
            maxDistance,
            lowerHeight,
            upperHeight,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        frequency = get<0>(payload);
        minDistance = get<1>(payload);
        maxDistance = get<2>(payload);
        lowerHeight = get<3>(payload);
        upperHeight = get<4>(payload);
        color = get<5>(payload);
    }

    typedef msgpack::type::tuple<f64, f64, f64, f64, f64, HDRColor> layout;

    f64 frequency;
    f64 minDistance;
    f64 maxDistance;
    f64 lowerHeight;
    f64 upperHeight;
    HDRColor color;
};

struct AtmosphereParameters
{
    AtmosphereParameters();

    void verify();

    bool operator==(const AtmosphereParameters& rhs) const;
    bool operator!=(const AtmosphereParameters& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            enabled,
            seed,
            effects,
            lightning
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        enabled = get<0>(payload);
        seed = get<1>(payload);
        effects = get<2>(payload);
        lightning = get<3>(payload);
    }

    typedef msgpack::type::tuple<bool, u64, vector<AtmosphereEffect>, AtmosphereLightning> layout;

    bool enabled;
    u64 seed;
    vector<AtmosphereEffect> effects;
    AtmosphereLightning lightning;
};
