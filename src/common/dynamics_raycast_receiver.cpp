/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_raycast_receiver.hpp"

DynamicsRaycastReceiver::DynamicsRaycastReceiver(const vec3& start, const vec3& end)
    : btTriangleRaycastCallback(
        btVector3(start.x, start.y, start.z),
        btVector3(end.x, end.y, end.z)
    )
    , start(start)
    , end(end)
{

}

void DynamicsRaycastReceiver::sort()
{
    std::sort(
        intersections.begin(),
        intersections.end(),
        [this](const vec3& a, const vec3& b) -> bool { return a.distance(start) < b.distance(start); }
    );
}

btScalar DynamicsRaycastReceiver::reportHit(const btVector3& localNormal, btScalar fraction, int partID, int triangleIndex)
{
    intersections.push_back(start * (1 - fraction) + end * fraction);

    return fraction;
}
