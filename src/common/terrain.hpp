/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "emissive_color.hpp"

struct TerrainNode
{
    TerrainNode();
    TerrainNode(TerrainNodeID id);

    void verify(const set<TerrainNodeID>& nodeIDs);

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            position,
            color,
            radius,
            oneSided,
            linkedIDs
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        position = get<1>(payload);
        color = get<2>(payload);
        radius = get<3>(payload);
        oneSided = get<4>(payload);
        linkedIDs = get<5>(payload);
    }

    bool operator==(const TerrainNode& rhs) const;
    bool operator!=(const TerrainNode& rhs) const;

    typedef msgpack::type::tuple<TerrainNodeID, vec3, optional<EmissiveColor>, f64, bool, set<TerrainNodeID>> layout;

    TerrainNodeID id;
    vec3 position;
    optional<EmissiveColor> color;
    f64 radius;
    bool oneSided;
    set<TerrainNodeID> linkedIDs;
};

vector<TerrainNode> createTerrainGrid(TerrainNodeID* nextID, f64 size, f64 spacing);
