/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "constants.hpp"
#include "log.hpp"

inline string getFile(const string& path)
{
    ifstream file(path);

    if (!file.good())
    {
        error("Failed to read file: " + path);
        return "";
    }

    return string(istreambuf_iterator<char>(file), istreambuf_iterator<char>());
}

inline void setFile(const string& path, const string& data)
{
    ofstream file(path);

    if (!file.good())
    {
        error("Failed to write file: " + path);
        return;
    }

    file << data;
}

constexpr f64 sq(f64 x)
{
    return x * x;
}

constexpr f64 degrees(f64 radians)
{
    return (radians / pi) * 180.0;
}

constexpr f64 radians(f64 degrees)
{
    return (degrees / 180.0) * pi;
}

template<typename T>
constexpr vec2_t<T> degrees(const vec2_t<T>& radians)
{
    return (radians / pi) * 180.0;
}

template<typename T>
constexpr vec2_t<T> radians(const vec2_t<T>& degrees)
{
    return (degrees / 180.0) * pi;
}

template<typename T>
constexpr vec3_t<T> degrees(const vec3_t<T>& radians)
{
    return (radians / pi) * 180.0;
}

template<typename T>
constexpr vec3_t<T> radians(const vec3_t<T>& degrees)
{
    return (degrees / 180.0) * pi;
}

template<typename T>
constexpr tuple<T, T> quadratic(T a, T b, T c)
{
    return {
        (-b + sqrt(sq(b) - 4*a*c)) / (2*a),
        (-b - sqrt(sq(b) - 4*a*c)) / (2*a)
    };
}

template<typename T = chrono::milliseconds>
T currentTime()
{
    return chrono::duration_cast<T>(chrono::system_clock::now().time_since_epoch());
}

template<typename T>
vector<T> join(const vector<T>& a, const vector<T>& b)
{
    vector<T> c;
    c.reserve(a.size() + b.size());
    c.insert(c.end(), a.begin(), a.end());
    c.insert(c.end(), b.begin(), b.end());

    return c;
}

template<typename T>
deque<T> join(const deque<T>& a, const deque<T>& b)
{
    deque<T> c;
    c.insert(c.end(), a.begin(), a.end());
    c.insert(c.end(), b.begin(), b.end());

    return c;
}

template<typename T>
set<T> join(const set<T>& a, const set<T>& b)
{
    set<T> c;
    c.insert(a.begin(), a.end());
    c.insert(b.begin(), b.end());

    return c;
}

template<typename KeyT, typename ValT>
map<KeyT, ValT> join(const map<KeyT, ValT>& a, const map<KeyT, ValT>& b)
{
    map<KeyT, ValT> c = a;

    for (const auto& [ k, v ] : b)
    {
        c.insert_or_assign(k, v);
    }

    return c;
}

inline bool isNumber(const string& s)
{
    regex r("(-)?[0-9]+(\\.[0-9]+)?");
    smatch m;

    return regex_match(s, m, r);
}

inline bool isValidUsername(const string& name)
{
    if (name.empty())
    {
        return false;
    }

    if (name.size() > 32)
    {
        return false;
    }

    if (name.front() == '-')
    {
        return false;
    }

    if (name.back() == '-')
    {
        return false;
    }

    if (isNumber(name))
    {
        return false;
    }

    char last = '\0';

    for (char c : name)
    {
        if ((c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '-')
        {
            return false;
        }

        if (last == '-' && c == '-')
        {
            return false;
        }

        last = c;
    }

    return true;
}

inline bool isValidName(const string& name)
{
    if (name.empty())
    {
        return false;
    }

    if (name.front() == '-')
    {
        return false;
    }

    if (name.back() == '-')
    {
        return false;
    }

    if (isNumber(name))
    {
        return false;
    }

    char last = '\0';

    for (char c : name)
    {
        if ((c < 'a' || c > 'z') && (c < '0' || c > '9') && c != '-')
        {
            return false;
        }

        if (last == '-' && c == '-')
        {
            return false;
        }

        last = c;
    }

    return true;
}

inline string toValidName(const string& s)
{
    string result;

    for (char c : s)
    {
        if (c >= 'A' && c <= 'Z')
        {
            result += c + ('a' - 'A');
        }
        else if (c >= 'a' && c <= 'z')
        {
            result += c;
        }
        else if (c >= '0' && c <= '9')
        {
            result += c;
        }
        else if ((c == ' ' || c == '-' || c == '_') && !result.empty() && result.back() != '-')
        {
            result += '-';
        }
    }

    size_t cutCount = result.size();

    for (size_t i = result.size() - 1; i < result.size(); i--)
    {
        if (result[i] == '-')
        {
            cutCount = i;
        }
        else
        {
            break;
        }
    }

    result = result.substr(0, cutCount);

    if (result.empty() || isNumber(result))
    {
        return "invalid-name";
    }

    return result;
}

inline bool isValidSymbol(const string& name)
{
    if (name.empty())
    {
        return false;
    }

    if (isNumber(name))
    {
        return false;
    }

    for (char c : name)
    {
        if (c < ' ' || c > '~' || c == ' ' || c == '(' || c == ')' || c == '\'' || c == '"')
        {
            return false;
        }
    }

    return true;
}

inline bool isCompleteCommand(const string& command)
{
    int depth = 0;

    for (char c : command)
    {
        if (c == '(')
        {
            depth++;
        }
        else if (c == ')')
        {
            depth--;
        }
    }

    // Allow negatives because that implies an invalid command form that cannot be rectified by adding more text, better to just execute it and error
    return depth <= 0;
}

constexpr bool isWordBoundary(char c)
{
    return c == ' ' || c == '\n' || c == '\r' || c == '\t' || c == '(' || c == ')' || c == '\'' || c == '"';
}

inline string toPrettyString(f64 f)
{
    string s = to_string(f);

    size_t count = s.size();

    for (size_t i = s.size() - 1; i < s.size(); i--)
    {
        char c = s[i];

        if (c == '.')
        {
            count = i;
            break;
        }
        else if (c != '0')
        {
            count = i + 1;
            break;
        }
    }

    s = s.substr(0, count);

    return s;
}

inline string toFixedPrecision(f64 f, u32 size)
{
    string s = to_string(f);

    string result;
    bool found = false;
    size_t count = 0;

    for (size_t i = 0; i < s.size(); i++)
    {
        char c = s[i];

        if (c == '.')
        {
            found = true;
            result += c;
        }
        else
        {
            if (found)
            {
                result += c;
                count++;

                if (count == size)
                {
                    break;
                }
            }
            else
            {
                result += c;
            }
        }
    }

    if (!found)
    {
        result += '.';
    }

    for (size_t i = 0; i < size - count; i++)
    {
        result += '0';
    }

    return result;
}

inline vector<const char*> toRawNames(const set<string>& names)
{
    vector<const char*> ret;
    ret.reserve(names.size());

    for (const string& name : names)
    {
        ret.push_back(name.c_str());
    }

    return ret;
}

inline string join(const set<string>& values, const string& separator)
{
    string s;

    size_t i = 0;
    for (const string& value : values)
    {
        s += value;

        if (i + 1 != values.size())
        {
            s += separator;
        }

        i++;
    }

    return s;
}

inline string join(const vector<string>& values, const string& separator)
{
    string s;

    size_t i = 0;
    for (const string& value : values)
    {
        s += value;

        if (i + 1 != values.size())
        {
            s += separator;
        }

        i++;
    }

    return s;
}

template<typename T>
constexpr vec2_t<T> rotate(const vec2_t<T>& p, T angle)
{
    T s = sin(angle);
    T c = cos(angle);

    return {
        p.x * c - p.y * s,
        p.x * s + p.y * c
    };
}

template<typename T>
constexpr T angleBetween(vec2_t<T> p, vec2_t<T> q)
{
    return acos(p.dot(q));
}

template<typename T>
constexpr T handedAngleBetween(vec2_t<T> p, vec2_t<T> q)
{
    return atan2(p.x * q.y - p.y * q.x, p.dot(q));
}

template<typename T>
constexpr T angleBetween(vec3_t<T> p, vec3_t<T> q)
{
    return acos(p.dot(q));
}

template<typename T>
constexpr T handedAngleBetween(vec3_t<T> p, vec3_t<T> q, vec3_t<T> axis)
{
    return handedAngleBetween<T>(vec2_t<T>(0, 1), quaternion_t<T>(p, axis).inverse().rotate(q).toVec2());
}

constexpr bool ellipseContains(const vec2& center, f64 rx, f64 ry, const vec2& point)
{
    return (pow(point.x - center.x, 2) / pow(rx, 2)) + (pow(point.y - center.y, 2) / pow(ry, 2)) <= 1;
}

inline bool polygonContains(const vector<vec2>& polygon, const vec2& point)
{
    bool hit = false;

    vec2 t = point;

    for (size_t i = 0, j = polygon.size() - 1; i < polygon.size(); j = i++)
    {
        const vec2& p0 = polygon[i];
        const vec2& p1 = polygon[j];

        if (((p0.y > t.y) != (p1.y > t.y)) && (t.x < ((p1.x - p0.x) * (t.y - p0.y) / (p1.y - p0.y) + p0.x)))
        {
            hit = !hit;
        }
    }

    return hit;
}

template<typename T>
constexpr vec3_t<T> rgbToHSV(const vec3_t<T>& rgb)
{
    T r = rgb.x;
    T g = rgb.y;
    T b = rgb.z;

    T h = 0;
    T s = 0;
    T v = 0;

    T max = std::max(std::max(r, g), b);
    T min = std::min(std::min(r, g), b);
    T delta = max - min;

    if (delta > 0)
    {
        if (max == r)
        {
            h = 60 * fmod((g - b) / delta, 6);
        }
        else if (max == g)
        {
            h = 60 * (((b - r) / delta) + 2);
        }
        else if (max == b)
        {
            h = 60 * (((r - g) / delta) + 4);
        }

        if (max > 0)
        {
            s = delta / max;
        }
        else
        {
            s = 0;
        }

        v = max;
    }
    else
    {
        h = 0;
        s = 0;
        v = max;
    }

    if (h < 0)
    {
        h = 360 + h;
    }

    return { h / 360, s, v };
}

template<typename T>
constexpr vec3_t<T> hsvToRGB(const vec3_t<T>& hsv)
{
    T h = hsv.x * 360;
    T s = hsv.y;
    T v = hsv.z;

    T r = 0;
    T g = 0;
    T b = 0;

    T c = v * s;
    T hPrime = fmod(h / 60, 6);
    T x = c * (1 - abs(fmod(hPrime, 2) - 1));
    T m = v - c;

    if (hPrime >= 0 && hPrime < 1)
    {
        r = c;
        g = x;
        b = 0;
    }
    else if (hPrime >= 1 && hPrime < 2)
    {
        r = x;
        g = c;
        b = 0;
    }
    else if (hPrime >= 2 && hPrime < 3)
    {
        r = 0;
        g = c;
        b = x;
    }
    else if (hPrime >= 3 && hPrime < 4)
    {
        r = 0;
        g = x;
        b = c;
    }
    else if (hPrime >= 4 && hPrime < 5)
    {
        r = x;
        g = 0;
        b = c;
    }
    else if (hPrime >= 5 && hPrime < 6)
    {
        r = c;
        g = 0;
        b = x;
    }
    else
    {
        r = 0;
        g = 0;
        b = 0;
    }

    r += m;
    g += m;
    b += m;

    return { r, g, b };
}

inline string sanitize(const string& s, bool allowNewLines = false)
{
    string sanitized;

    for (char c : s)
    {
        if ((c < ' ' || c > '~') && (c != '\n' || !allowNewLines))
        {
            continue;
        }

        sanitized += c;
    }

    return sanitized;
}

inline string configPath()
{
#if defined(WIN32)
    const char* basePath = getenv("APPDATA");
#elif defined(__unix__) || defined(__APPLE__)
    const char* basePath = getenv("HOME");
#else
#error Unknown operating system encountered.
#endif

    if (!basePath)
    {
        basePath = ".";
    }

#if defined(WIN32)
    return string(basePath) + "\\LambdaMod\\";
#elif defined(__APPLE__)
    return string(basePath) + "/Library/Application Support/LambdaMod/";
#elif defined(__unix__)
    return string(basePath) + "/.config/lambdamod/";
#else
#error Unknown operating system encountered.
#endif
}

inline string cachePath()
{
#if defined(WIN32)
    const char* basePath = getenv("LOCALAPPDATA");
#elif defined(__unix__) || defined(__APPLE__)
    const char* basePath = getenv("HOME");
#else
#error Unknown operating system encountered.
#endif

    if (!basePath)
    {
        basePath = ".";
    }

#if defined(WIN32)
    return string(basePath) + "\\LambdaMod\\";
#elif defined(__APPLE__)
    return string(basePath) + "/Library/Caches/LambdaMod/";
#elif defined(__unix__)
    return string(basePath) + "/.cache/lambdamod/";
#else
#error Unknown operating system encountered.
#endif
}

inline string escapeQuotes(const string& s)
{
    string escaped;

    for (char c : s)
    {
        if (c == '"')
        {
            escaped += '\\';
            escaped += c;
        }
        else
        {
            escaped += c;
        }
    }

    return escaped;
}

inline string urlEncode(const string& raw)
{
    string encoded;

    for (size_t i = 0; i < raw.size(); i++)
    {
        char c = raw[i];

        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z') || (c >= '0' && c <= '9') || c == '-' || c == '.' || c == '_' || c == '~')
        {
            encoded += c;
        }
        else
        {
            stringstream ss;

            ss << hex;
            ss << setw(2);
            ss << setfill('0');
            ss << (int)c;

            encoded += '%' + ss.str();
        }
    }

    return raw;
}

inline string urlDecode(const string& encoded)
{
    string raw;

    for (size_t i = 0; i < encoded.size(); i++)
    {
        char c = encoded[i];

        if (c == '%')
        {
            raw += static_cast<char>(stoi(encoded.substr(i + 1, 2), 0, 16));
            i += 2;
        }
        else
        {
            raw += c;
        }
    }

    return raw;
}

inline u16 randomPort()
{
    random_device device;
    mt19937 engine(device());
    uniform_int_distribution<u16> dist(49152, 65534);

    return dist(engine);
}

template<typename T>
constexpr bool isPowerOfTwo(T x)
{
    return x != 0 && (x & (x - 1)) == 0;
}

template<typename T>
constexpr vec3_t<T> pickForwardDirection(vec3_t<T> direction)
{
    if (roughly(direction, vec3_t<T>(forwardDir)) || roughly(direction, vec3_t<T>(backDir)))
    {
        return downDir;
    }
    else
    {
        return direction.cross(forwardDir).normalize().cross(direction).normalize();
    }
}

template<typename T>
constexpr vec3_t<T> pickSideDirection(const vec3_t<T>& direction)
{
    if (roughly(direction, vec3_t<T>(upDir)) || roughly(direction, vec3_t<T>(downDir)))
    {
        return direction.cross(forwardDir).normalize();
    }
    else
    {
        return direction.cross(upDir).normalize();
    }
}

template<typename T>
constexpr vec3_t<T> pickUpDirection(vec3_t<T> direction)
{
    if (roughly(direction, vec3_t<T>(upDir)) || roughly(direction, vec3_t<T>(downDir)))
    {
        return backDir;
    }
    else
    {
        return direction.cross(upDir).normalize().cross(direction).normalize();
    }
}

template<typename T>
constexpr bool roughly(T x, T target)
{
    return x > target - static_cast<T>(epsilon) && x < target + static_cast<T>(epsilon);
}

template<typename T>
constexpr bool roughly(vec2_t<T> x, vec2_t<T> target)
{
    return
        roughly<T>(x.x, target.x) &&
        roughly<T>(x.y, target.y);
}

template<typename T>
constexpr bool roughly(vec3_t<T> x, vec3_t<T> target)
{
    return
        roughly<T>(x.x, target.x) &&
        roughly<T>(x.y, target.y) &&
        roughly<T>(x.z, target.z);
}

template<typename t>
constexpr bool roughly(vec4_t<t> x, vec4_t<t> target)
{
    return
        roughly<t>(x.x, target.x) &&
        roughly<t>(x.y, target.y) &&
        roughly<t>(x.z, target.z) &&
        roughly<t>(x.w, target.w);
}

template<typename t>
constexpr bool roughly(quaternion_t<t> x, quaternion_t<t> target)
{
    return
        roughly<t>(x.w, target.w) &&
        roughly<t>(x.x, target.x) &&
        roughly<t>(x.y, target.y) &&
        roughly<t>(x.z, target.z);
}

template<typename T>
constexpr vec2_t<T> verifyPosition(vec2_t<T> p)
{
    if (isnan(p.x) || isnan(p.y))
    {
        return vec2_t<T>();
    }
    else
    {
        return p;
    }
}

template<typename T>
constexpr vec3_t<T> verifyPosition(vec3_t<T> p)
{
    if (isnan(p.x) || isnan(p.y) || isnan(p.z))
    {
        return vec3_t<T>();
    }
    else
    {
        return p;
    }
}

template<typename T>
constexpr quaternion_t<T> verifyRotation(quaternion_t<T> r)
{
    if (isnan(r.w) || isnan(r.x) || isnan(r.y) || isnan(r.z))
    {
        return quaternion_t<T>();
    }
    else
    {
        return r;
    }
}

template<typename T>
constexpr vec3_t<T> verifyScale(vec3_t<T> s)
{
    if (isnan(s.x) || isnan(s.y) || isnan(s.z) || roughly<T>(s.x, 0) || roughly<T>(s.y, 0) || roughly<T>(s.z, 0))
    {
        return vec3_t<T>(1);
    }
    else
    {
        return s;
    }
}

template<typename T>
constexpr vec3_t<T> verifyLinearVelocity(vec3_t<T> l)
{
    if (isnan(l.x) || isnan(l.y) || isnan(l.z))
    {
        return vec3_t<T>();
    }
    else
    {
        return l;
    }
}

template<typename T>
constexpr vec3_t<T> verifyAngularVelocity(vec3_t<T> a)
{
    if (isnan(a.x) || isnan(a.y) || isnan(a.z))
    {
        return vec3_t<T>();
    }
    else
    {
        return a;
    }
}

template<typename T>
constexpr mat4_t<T> verifyTransform(mat4_t<T> transform)
{
    return {
        verifyPosition<T>(transform.position()),
        verifyRotation<T>(transform.rotation()),
        verifyScale<T>(transform.scale())
    };
}

template<typename T>
constexpr vec3_t<T> verifyColor(vec3_t<T> c)
{
    return {
        clamp<T>(c.x, 0, 1),
        clamp<T>(c.y, 0, 1),
        clamp<T>(c.z, 0, 1)
    };
}

template<typename T>
constexpr bool greaterMass(T a, T b)
{
    if (a == 0 && b != 0)
    {
        return true;
    }
    else if (a != 0 && b == 0)
    {
        return false;
    }
    else
    {
        return a > b;
    }
}

class Defer
{
public:
    Defer(const function<void(void)>& f)
        : f(f)
    {

    }
    Defer(const Defer& rhs) = delete;
    Defer(Defer&& rhs) = delete;
    ~Defer()
    {
        f();
    }

    Defer& operator=(const Defer& rhs) = delete;
    Defer& operator=(Defer&& rhs) = delete;

private:
    function<void(void)> f;
};

// Compares the equality of maps where the members are dynamically allocated, ignoring differences in memory addresses
template<typename KeyT, typename ValueT>
bool dynAllocMapEqual(const map<KeyT, ValueT*>& a, const map<KeyT, ValueT*>& b)
{
    if (a.size() != b.size())
    {
        return false;
    }

    for (auto aIt = a.begin(), bIt = b.begin(); aIt != a.end() && bIt != b.end(); aIt++, bIt++)
    {
        if (aIt->first != bIt->first)
        {
            return false;
        }

        if (*aIt->second != *bIt->second)
        {
            return false;
        }
    }

    return true;
}

constexpr f64 applyDeadZone(f64 value, f64 zone)
{
    if (value <= zone)
    {
        return 0;
    }
    else
    {
        return (value - zone) / (1 - zone);
    }
}

inline string toUppercase(string s)
{
    for (char& c : s)
    {
        if (c >= 'a' && c <= 'z')
        {
            c -= 'a' - 'A';
        }
    }

    return s;
}

inline string toLowercase(string s)
{
    for (char& c : s)
    {
        if (c >= 'A' && c <= 'Z')
        {
            c += 'a' - 'A';
        }
    }

    return s;
}

constexpr f64 log(f64 n, f64 b)
{
    return log(n) / log(b);
}

constexpr bool isWhitespace(char c)
{
    return c == ' ' || c == '\n' || c == '\t';
}

inline string removeLeftWhitespace(const string& s)
{
    size_t i = 0;

    while (i < s.size())
    {
        if (!isWhitespace(s[i]))
        {
            break;
        }

        i++;
    }

    return s.substr(i, s.size() - i);
}

inline string removeRightWhitespace(const string& s)
{
    size_t i = s.size() - 1;

    while (i < s.size())
    {
        if (!isWhitespace(s[i]))
        {
            break;
        }

        i--;
    }

    return s.substr(0, i + 1);
}

inline u64 toU64(const string& s)
{
    static const regex r("^[^0-9]*-.*");
    smatch m;

    if (regex_match(s, m, r))
    {
        throw invalid_argument("toU64");
    }
    else
    {
        return stoull(s);
    }
}

constexpr f64 angularDiameter(f64 actualDiameter, f64 distance)
{
    return 2 * atan2(actualDiameter, 2 * distance);
}

constexpr f64 actualDiameter(f64 angularDiameter, f64 distance)
{
    return 2 * distance * tan(angularDiameter / 2);
}

constexpr f32 toNormalizedAngularFrequency(f32 frequency, f64 samplingFrequency)
{
    return (frequency / (samplingFrequency / 2)) * pi;
}

constexpr f32 fromNormalizedAngularFrequency(f64 fraction, f64 samplingFrequency)
{
    return (fraction / pi) * (samplingFrequency / 2);
}

constexpr f32 sinc(f32 x)
{
    if (x == 0)
    {
        return 1;
    }
    else
    {
        return sin(x * pi) / (x * pi);
    }
}

template<typename T>
constexpr T sign(T x)
{
    return x < 0 ? T(-1) : T(+1);
}

template<typename T>
constexpr T roundTowardsZero(T x)
{
    return x < 0 ? ceil(x) : floor(x);
}

template<typename T>
constexpr T roundAwayFromZero(T x)
{
    return x < 0 ? floor(x) : ceil(x);
}

constexpr string byteToHex(u8 x)
{
    const char chars[] = {
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'
    };

    string s;

    s += chars[(x & 0b11110000) >> 4];
    s += chars[x & 0b00001111];

    return s;
}

template<typename T>
constexpr T loopIndex(T i, T size)
{
    return i >= size ? i - size : i;
}

inline mat4 triangleSpace(const vec3& a, const vec3& b, const vec3& c, const optional<vec3>& n = {})
{
    vec3 normal = n ? *n : (b - a).normalize().cross((c - a).normalize());

    return mat4(
        a,
        quaternion((b - a).normalize(), normal),
        vec3(1)
    );
}

template<typename T>
vector<T> toVector(const set<T>& s)
{
    return { s.begin(), s.end() };
}

// Loosely analogous to Haskell's >>= operator
template<typename T>
optional<T> optionalBind(const function<T(T)>& operation, const optional<T>& value)
{
    if (value)
    {
        return operation(*value);
    }
    else
    {
        return value;
    }
}

constexpr string format(const string& format, const vector<string>& inputs)
{
    vector<string> parts;

    bool subMode = false;
    string part;

    for (size_t i = 0; i < format.size(); i++)
    {
        char c = format[i];

        if (c == '%')
        {
            if (subMode)
            {
                parts.push_back(part);
                part = "";

                subMode = false;
            }
            else
            {
                parts.push_back(part);
                part = string() + c;

                subMode = true;
            }
        }
        else if (isdigit(c))
        {
            part += c;
        }
        else
        {
            if (subMode)
            {
                parts.push_back(part);
                part = string() + c;

                subMode = false;
            }
            else
            {
                part += c;
            }
        }
    }

    if (!part.empty())
    {
        parts.push_back(part);
    }

    for (size_t i = 0; i < inputs.size(); i++)
    {
        string name = "%" + to_string(i + 1);
        const string& input = inputs[i];

        for (size_t j = 0; j < parts.size(); j++)
        {
            if (parts[j] == name)
            {
                parts[j] = input;
                break;
            }
        }
    }

    string s;

    for (const string& part : parts)
    {
        s += part;
    }

    return s;
}
