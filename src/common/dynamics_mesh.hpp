/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "structure_builder.hpp"
#include "terrain_builder.hpp"

#define BT_USE_DOUBLE_PRECISION
#include <bullet/btBulletDynamicsCommon.h>

class DynamicsMesh
{
public:
    DynamicsMesh(const StructureMesh& mesh);
    DynamicsMesh(const TerrainMesh& mesh);
    DynamicsMesh(const DynamicsMesh& rhs) = delete;
    DynamicsMesh(DynamicsMesh&& rhs) = delete;
    ~DynamicsMesh();

    DynamicsMesh& operator=(const DynamicsMesh& rhs) = delete;
    DynamicsMesh& operator=(DynamicsMesh&& rhs) = delete;

    btTriangleMesh* mesh() const;

private:
    vector<u32> indices;
    vector<fvec3> vertices;
    btTriangleMesh* _mesh;
};
