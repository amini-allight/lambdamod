/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "entity.hpp"

struct SpaceGraphGenerationEntity
{
    SpaceGraphGenerationEntity(const Entity* entity);

    bool operator==(const SpaceGraphGenerationEntity& rhs) const;
    bool operator!=(const SpaceGraphGenerationEntity& rhs) const;
    bool operator>(const SpaceGraphGenerationEntity& rhs) const;
    bool operator<(const SpaceGraphGenerationEntity& rhs) const;
    bool operator>=(const SpaceGraphGenerationEntity& rhs) const;
    bool operator<=(const SpaceGraphGenerationEntity& rhs) const;
    strong_ordering operator<=>(const SpaceGraphGenerationEntity& rhs) const;

    pair<vec3, vec3> localExtent(const vec3& nodeInner, const vec3& nodeOuter) const;
    bool overlaps(const vec3& nodeInner, const vec3& nodeOuter) const;

    EntityID id;
    mat4 transform;
    vec3 minExtent;
    vec3 maxExtent;
};

struct SpaceGraphGenerationContext
{
    SpaceGraphGenerationContext(
        const vector<SpaceGraphGenerationEntity>& entities,
        const vec3& innerExtent,
        const vec3& outerExtent,
        u32 depth
    );

    vector<SpaceGraphGenerationEntity> entities;
    vec3 innerExtent;
    vec3 outerExtent;
    u32 depth;
};

f64 pickCenter(SpaceGraphGenerationContext& ctx, const function<f64(const vec3&)>& pick);
