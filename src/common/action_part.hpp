/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "yaml_tools.hpp"
#include "action_parts.hpp"

enum ActionPartType : u8
{
    Action_Part_IK
};

class ActionPart
{
public:
    ActionPart();
    explicit ActionPart(ActionPartID id);
    ActionPart(ActionPartID id, const ActionPart& other);

    // We still use verify rather than doing it per-function because we receive *Part types whole from the network and we need to validate that their _type and data fields match
    void verify();

    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }

    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }

    bool operator==(const ActionPart& rhs) const;
    bool operator!=(const ActionPart& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            _id,
            _targetID,
            _end,
            static_cast<u8>(_type),
            dataToString()
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        _id = std::get<0>(payload);
        _targetID = std::get<1>(payload);
        _end = std::get<2>(payload);
        _type = static_cast<ActionPartType>(std::get<3>(payload));
        dataFromString(std::get<4>(payload));
    }

    ActionPartID id() const;
    BodyPartID targetID() const;
    u32 end() const;
    ActionPartType type() const;

    void setTargetID(BodyPartID targetID);
    void setEnd(u32 end);

    // Special setter with side effect
    void setType(ActionPartType type);

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    ActionPartID _id;
    BodyPartID _targetID;
    u32 _end;
    ActionPartType _type;
    variant<ActionPartIK> data;

    typedef msgpack::type::tuple<ActionPartID, BodyPartID, u32, u8, string> layout;

    string dataToString() const;
    void dataFromString(const string& s);
};
