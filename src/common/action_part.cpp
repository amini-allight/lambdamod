/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "action_part.hpp"
#include "log.hpp"

string actionPartTypeToString(ActionPartType type)
{
    switch (type)
    {
    case Action_Part_IK :
        return "action-part-ik";
    default :
        fatal("Encountered unknown action part type '" + to_string(type) + "'.");
    }
}

ActionPartType actionPartTypeFromString(const string& s)
{
    if (s == "action-part-ik")
    {
        return Action_Part_IK;
    }
    else
    {
        fatal("Encountered unknown action part type '" + s + "'.");
    }
}

template<>
ActionPartType YAMLNode::convert() const
{
    return actionPartTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(ActionPartType v)
{
    emit(actionPartTypeToString(v));
}

ActionPart::ActionPart()
{
    _id = ActionPartID(1);

    _targetID = BodyPartID();
    _end = 0;

    _type = Action_Part_IK;
    data = ActionPartIK();
}

ActionPart::ActionPart(ActionPartID id)
    : ActionPart()
{
    _id = id;
}

ActionPart::ActionPart(ActionPartID id, const ActionPart& other)
    : ActionPart(other)
{
    _id = id;
}

void ActionPart::verify()
{
    if (auto ik = get_if<ActionPartIK>(&data); ik && _type == Action_Part_IK)
    {
        ik->verify();
    }
    else
    {
        _type = Action_Part_IK;
        data = ActionPartIK();
    }
}

bool ActionPart::operator==(const ActionPart& rhs) const
{
    if (_id != rhs._id)
    {
        return false;
    }

    if (_type != rhs._type)
    {
        return false;
    }

    if (_targetID != rhs._targetID)
    {
        return false;
    }

    if (_end != rhs._end)
    {
        return false;
    }

    switch (_type)
    {
    default :
        fatal("Encountered unknown action part type '" + to_string(_type) + "'.");
    case Action_Part_IK :
        return get<ActionPartIK>() == rhs.get<ActionPartIK>();
    }
}

bool ActionPart::operator!=(const ActionPart& rhs) const
{
    return !(*this == rhs);
}

ActionPartID ActionPart::id() const
{
    return _id;
}

BodyPartID ActionPart::targetID() const
{
    return _targetID;
}

u32 ActionPart::end() const
{
    return _end;
}

ActionPartType ActionPart::type() const
{
    return _type;
}

void ActionPart::setTargetID(BodyPartID targetID)
{
    _targetID = targetID;
}

void ActionPart::setEnd(u32 end)
{
    _end = end;
}

void ActionPart::setType(ActionPartType type)
{
    _type = type;

    switch (_type)
    {
    case Action_Part_IK :
        data = ActionPartIK();
        break;
    }
}

string ActionPart::dataToString() const
{
    stringstream ss;

    switch (_type)
    {
    case Action_Part_IK :
        msgpack::pack(ss, get<ActionPartIK>());
        break;
    }

    return ss.str();
}

void ActionPart::dataFromString(const string& s)
{
    switch (_type)
    {
    case Action_Part_IK :
    {
        msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

        ActionPartIK part;
        oh.get().convert(part);

        data = part;
        break;
    }
    }
}

template<>
ActionPart YAMLNode::convert() const
{
    ActionPart part;

    part._id = at("id").as<ActionPartID>();
    part._targetID = at("targetID").as<BodyPartID>();
    part._end = at("end").as<u32>();
    part._type = actionPartTypeFromString(at("type").as<string>());

    switch (part._type)
    {
    case Action_Part_IK :
        part.data = at("data").as<ActionPartIK>();
        break;
    }

    return part;
}

template<>
void YAMLSerializer::emit(ActionPart v)
{
    startMapping();

    emitPair("id", v._id);
    emitPair("targetID", v._id);
    emitPair("end", v._end);
    emitPair("type", actionPartTypeToString(v._type));

    switch (v._type)
    {
    case Action_Part_IK :
        emitPair("data", v.get<ActionPartIK>());
        break;
    }

    endMapping();
}
