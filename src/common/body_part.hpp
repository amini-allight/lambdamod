/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "body_parts.hpp"
#include "body_constraints.hpp"
#include "body_render_data.hpp"
#include "yaml_tools.hpp"
#include "bounding_box.hpp"

enum BodyPartType : u8
{
    Body_Part_Line,
    Body_Part_Plane,
    Body_Part_Anchor,
    Body_Part_Solid,
    Body_Part_Text,
    Body_Part_Symbol,
    Body_Part_Canvas,
    Body_Part_Structure,
    Body_Part_Terrain,
    Body_Part_Light,
    Body_Part_Force,
    Body_Part_Area,
    Body_Part_Rope
};

string bodyPartTypeToString(BodyPartType type);
BodyPartType bodyPartTypeFromString(const string& s);

class EntityUpdate;
class NetworkEvent;

class BodyPart
{
public:
    BodyPart();
    BodyPart(BodyPart* parent, BodyPartID id);
    BodyPart(BodyPart* parent, BodyPartID id, const BodyPart& rhs);
    BodyPart(const BodyPart& rhs);
    BodyPart(BodyPart&& rhs) noexcept;
    ~BodyPart();

    BodyPart& operator=(const BodyPart& rhs);
    BodyPart& operator=(BodyPart&& rhs) noexcept;

    static BodyPart* load(BodyPart* parent, const YAMLNode& node);

    // We still use verify rather than doing it per-function because we receive *Part types whole from the network and we need to validate that their _type and data fields match
    void verify();

    void compare(const BodyPart& previous, EntityUpdate& update, bool transformed) const;

    void push(const NetworkEvent& event);
    vector<NetworkEvent> initial(const string& world, EntityID id, bool transformed) const;

    template<typename T>
    T& get()
    {
        return std::get<T>(data);
    }

    template<typename T>
    const T& get() const
    {
        return std::get<T>(data);
    }

    template<typename T>
    T& getConstraint()
    {
        return std::get<T>(constraint);
    }

    template<typename T>
    const T& getConstraint() const
    {
        return std::get<T>(constraint);
    }

#if defined(LMOD_CLIENT)
    BodyRenderData toRender(const set<BodyPartID>& hiddenBodyPartIDs) const;
#endif

    void add(BodyPartID parentID, BodyPart* part);
    void remove(BodyPartID id);
    void update(BodyPartID id, const BodyPart& part);
    void clear();

    BodyPart* parent() const;
    BodyPart* rootParent();
    bool hasParent(BodyPart* possible) const;
    size_t parentDepth() const;
    vec3 parentRegionalPosition() const;

    BodyPart* get(BodyPartID id) const;
    BodyPart* get(BodyAnchorType type, const string& name = "") const;
    const map<BodyPartID, BodyPart*>& parts() const;
    void traverse(const function<void(BodyPart*)>& behavior);
    void traverse(const function<void(const BodyPart*)>& behavior) const;
    bool partiallyTraverse(const function<bool(BodyPart*)>& behavior);
    bool partiallyTraverse(const function<bool(const BodyPart*)>& behavior) const;

    vec3 localPosition() const;
    quaternion localRotation() const;
    vec3 localScale() const;
    mat4 localTransform() const;

    // Regional means entity-local, local means parent-body-part-local
    vec3 regionalPosition() const;
    quaternion regionalRotation() const;
    vec3 regionalScale() const;
    mat4 regionalTransform() const;

    void setLocalPosition(const vec3& position);
    void setLocalRotation(const quaternion& rotation);
    void setLocalScale(const vec3& scale);
    void setLocalTransform(const mat4& transform);

    void setRegionalPosition(const vec3& position);
    void setRegionalRotation(const quaternion& rotation);
    void setRegionalScale(const vec3& scale);
    void setRegionalTransform(const mat4& transform);

    bool collides() const;
    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 independentPartMass() const;
    // Returns regional positions
    vector<vec3> subparts() const;
    mat4 constraintTransform() const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    bool operator==(const BodyPart& rhs) const;
    bool operator!=(const BodyPart& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            _id,
            transform,
            _mass,
            _maxForce,
            _maxVelocity,
            _type,
            dataToString(),
            _constraintType,
            constraintToString(),
            _constraintParentSubID
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        _id = std::get<0>(payload);
        transform = std::get<1>(payload);
        _mass = std::get<2>(payload);
        _maxForce = std::get<3>(payload);
        _maxVelocity = std::get<4>(payload);
        _type = static_cast<BodyPartType>(std::get<5>(payload));
        dataFromString(std::get<6>(payload));
        _constraintType = static_cast<BodyConstraintType>(std::get<7>(payload));
        constraintFromString(std::get<8>(payload));
        _constraintParentSubID = std::get<9>(payload);
    }

    BodyPartID id() const;
    f64 mass() const;
    f64 maxForce() const;
    f64 maxVelocity() const;
    BodyPartType type() const;
    BodyConstraintType constraintType() const;

    BodyPartSubID constraintParentSubID() const;
    void setConstraintParentSubID(BodyPartSubID id);

    void setMass(f64 mass);
    void setMaxForce(f64 maxForce);
    void setMaxVelocity(f64 maxVelocity);

    // Special setters with side effects
    void setType(BodyPartType type);
    void setConstraintType(BodyConstraintType type);

    // Special function for replacing all fields with those from a network update without affecting children
    void replaceSelfOnly(const BodyPart& update);

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    BodyPart* _parent;
    BodyPartID _id;
    mat4 transform;
    f64 _mass;
    f64 _maxForce;
    f64 _maxVelocity;

    BodyPartType _type;
    variant<
        BodyPartLine,
        BodyPartPlane,
        BodyPartAnchor,
        BodyPartSolid,
        BodyPartText,
        BodyPartSymbol,
        BodyPartCanvas,
        BodyPartStructure,
        BodyPartTerrain,
        BodyPartLight,
        BodyPartForce,
        BodyPartArea,
        BodyPartRope
    > data;

    BodyConstraintType _constraintType;
    variant<BodyConstraintFixed, BodyConstraintBall, BodyConstraintCone, BodyConstraintHinge> constraint;
    BodyPartSubID _constraintParentSubID;

    map<BodyPartID, BodyPart*> _parts;

private:
    typedef msgpack::type::tuple<BodyPartID, mat4, f64, f64, f64, u8, string, u8, string, BodyPartSubID> layout;

    string dataToString() const;
    void dataFromString(const string& s);

    string constraintToString() const;
    void constraintFromString(const string& s);

    void copy(const BodyPart& rhs);
    void copyWithoutParent(const BodyPart& rhs);
};
