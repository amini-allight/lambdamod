/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "events.hpp"

enum NetworkEventType : u8
{
    Network_Event_Ping,

    Network_Event_Join,
    Network_Event_Leave,
    Network_Event_Welcome,
    Network_Event_Kick,

    Network_Event_User,
    Network_Event_Team,
    Network_Event_Checkpoint,
    Network_Event_Document,
    Network_Event_Game,
    Network_Event_World,
    Network_Event_World_Sky,
    Network_Event_World_Atmosphere,

    Network_Event_Entity,
    Network_Event_Entity_Body,
    Network_Event_Entity_Sample,
    Network_Event_Entity_Sample_Playback,
    Network_Event_Entity_Action,
    Network_Event_Entity_Action_Playback,
    Network_Event_Entity_Input_Binding,
    Network_Event_Entity_HUD,
    Network_Event_Entity_Script,

    Network_Event_Script,

    Network_Event_Attach,
    Network_Event_Detach,
    Network_Event_Move,

    Network_Event_Haptic,
    Network_Event_Input,
    Network_Event_Grip,
    Network_Event_Command,
    Network_Event_Pose,

    Network_Event_Log,
    Network_Event_History,

    Network_Event_Text_Request,
    Network_Event_Text_Response,
    Network_Event_Tether,
    Network_Event_Confinement,
    Network_Event_Goto,
    Network_Event_Fade,
    Network_Event_Wait,
    Network_Event_Voice,
    Network_Event_Voice_Channel,

    Network_Event_Step,

    Network_Event_URL,
    Network_Event_Save
};

class NetworkEvent
{
public:
    template<typename T>
    NetworkEvent(const T& payload);

    NetworkEvent(const string& data);

    string data() const;

    NetworkEventType type() const;
    template<typename T>
    T& payload()
    {
        return get<T>(_payload);
    }
    template<typename T>
    const T& payload() const
    {
        return get<T>(_payload);
    }

    bool isSystem() const;
    string name() const;

    bool operator==(const NetworkEvent& rhs) const;
    bool operator!=(const NetworkEvent& rhs) const;

private:
    NetworkEventType _type;
    variant<
        PingEvent,
        JoinEvent,
        LeaveEvent,
        WelcomeEvent,
        KickEvent,
        UserEvent,
        TeamEvent,
        CheckpointEvent,
        DocumentEvent,
        GameEvent,
        WorldEvent,
        WorldSkyEvent,
        WorldAtmosphereEvent,
        EntityEvent,
        EntityBodyEvent,
        EntitySampleEvent,
        EntitySamplePlaybackEvent,
        EntityActionEvent,
        EntityActionPlaybackEvent,
        EntityInputBindingEvent,
        EntityHUDEvent,
        EntityScriptEvent,
        ScriptEvent,
        AttachEvent,
        DetachEvent,
        MoveEvent,
        HapticEvent,
        InputEvent,
        GripEvent,
        CommandEvent,
        PoseEvent,
        LogEvent,
        HistoryEvent,
        TextRequestEvent,
        TextResponseEvent,
        TetherEvent,
        ConfinementEvent,
        GotoEvent,
        FadeEvent,
        WaitEvent,
        VoiceEvent,
        VoiceChannelEvent,
        StepEvent,
        URLEvent,
        SaveEvent
    > _payload;
};

ostream& operator<<(ostream& out, const NetworkEvent& event);

template<>
NetworkEvent::NetworkEvent(const PingEvent& payload);

template<>
NetworkEvent::NetworkEvent(const JoinEvent& payload);

template<>
NetworkEvent::NetworkEvent(const LeaveEvent& payload);

template<>
NetworkEvent::NetworkEvent(const WelcomeEvent& payload);

template<>
NetworkEvent::NetworkEvent(const KickEvent& payload);

template<>
NetworkEvent::NetworkEvent(const UserEvent& payload);

template<>
NetworkEvent::NetworkEvent(const TeamEvent& payload);

template<>
NetworkEvent::NetworkEvent(const CheckpointEvent& payload);

template<>
NetworkEvent::NetworkEvent(const DocumentEvent& payload);

template<>
NetworkEvent::NetworkEvent(const GameEvent& payload);

template<>
NetworkEvent::NetworkEvent(const WorldEvent& payload);

template<>
NetworkEvent::NetworkEvent(const WorldSkyEvent& payload);

template<>
NetworkEvent::NetworkEvent(const WorldAtmosphereEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityBodyEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntitySampleEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntitySamplePlaybackEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityActionEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityActionPlaybackEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityInputBindingEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityHUDEvent& payload);

template<>
NetworkEvent::NetworkEvent(const EntityScriptEvent& payload);

template<>
NetworkEvent::NetworkEvent(const ScriptEvent& payload);

template<>
NetworkEvent::NetworkEvent(const AttachEvent& payload);

template<>
NetworkEvent::NetworkEvent(const DetachEvent& payload);

template<>
NetworkEvent::NetworkEvent(const MoveEvent& payload);

template<>
NetworkEvent::NetworkEvent(const HapticEvent& event);

template<>
NetworkEvent::NetworkEvent(const InputEvent& payload);

template<>
NetworkEvent::NetworkEvent(const GripEvent& payload);

template<>
NetworkEvent::NetworkEvent(const CommandEvent& payload);

template<>
NetworkEvent::NetworkEvent(const PoseEvent& payload);

template<>
NetworkEvent::NetworkEvent(const LogEvent& payload);

template<>
NetworkEvent::NetworkEvent(const HistoryEvent& event);

template<>
NetworkEvent::NetworkEvent(const TextRequestEvent& payload);

template<>
NetworkEvent::NetworkEvent(const TextResponseEvent& payload);

template<>
NetworkEvent::NetworkEvent(const TetherEvent& payload);

template<>
NetworkEvent::NetworkEvent(const ConfinementEvent& payload);

template<>
NetworkEvent::NetworkEvent(const GotoEvent& payload);

template<>
NetworkEvent::NetworkEvent(const FadeEvent& payload);

template<>
NetworkEvent::NetworkEvent(const WaitEvent& payload);

template<>
NetworkEvent::NetworkEvent(const VoiceEvent& payload);

template<>
NetworkEvent::NetworkEvent(const VoiceChannelEvent& payload);

template<>
NetworkEvent::NetworkEvent(const StepEvent& payload);

template<>
NetworkEvent::NetworkEvent(const URLEvent& payload);

template<>
NetworkEvent::NetworkEvent(const SaveEvent& payload);
