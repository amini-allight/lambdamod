/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "fourier_transform.hpp"
#include "tools.hpp"

static void fastFourierTransform(valarray<complex<f32>>& x)
{
    size_t n = x.size();

    if (n <= 1)
    {
        return;
    }

    valarray<complex<f32>> even = x[slice(0, n / 2, 2)];
    valarray<complex<f32>> odd = x[slice(1, n / 2, 2)];

    fastFourierTransform(even);
    fastFourierTransform(odd);

    for (size_t k = 0; k < n / 2; k++)
    {
        complex<f32> t = polar<f32>(1, -2 * pi * k / n) * odd[k];

        x[k] = even[k] + t;
        x[k + (n / 2)] = even[k] - t;
    }
}

valarray<complex<f32>> fft(const vector<f32>& samples)
{
    if (!isPowerOfTwo(samples.size()))
    {
        fatal("Fourier transform can only be applied to power-of-two sized blocks: " + to_string(samples.size()) + " is not a power of two.");
    }

    valarray<complex<f32>> x(samples.size());

    for (size_t i = 0; i < samples.size(); i++)
    {
        x[i] = samples[i];
    }

    fastFourierTransform(x);

    return x;
}

vector<f32> ifft(valarray<complex<f32>> x)
{
    if (!isPowerOfTwo(x.size()))
    {
        fatal("Inverse fourier transform can only be applied to power-of-two sized blocks: " + to_string(x.size()) + " is not a power of two.");
    }

    for (complex<f32>& v : x)
    {
        v = conj(v);
    }

    fastFourierTransform(x);

    for (complex<f32>& v : x)
    {
        v = conj(v);

        v /= x.size();
    }

    vector<f32> samples(x.size());

    for (size_t i = 0; i < x.size(); i++)
    {
        samples[i] = x[i].real();
    }

    return samples;
}
