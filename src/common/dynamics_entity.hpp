/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "vr_device.hpp"
#include "body_part.hpp"
#include "dynamics_component_entity.hpp"

#define BT_USE_DOUBLE_PRECISION
#include <bullet/btBulletDynamicsCommon.h>

class Dynamics;
class World;
class Entity;

class DynamicsEntity
{
public:
    DynamicsEntity(Dynamics* dynamics, Entity* entity);
    DynamicsEntity(const DynamicsEntity& rhs) = delete;
    DynamicsEntity(DynamicsEntity&& rhs) = delete;
    ~DynamicsEntity();

    DynamicsEntity& operator=(const DynamicsEntity& rhs) = delete;
    DynamicsEntity& operator=(DynamicsEntity&& rhs) = delete;

    Entity* entity() const;

    tuple<vector<DynamicsForceApplication>, vector<DynamicsArea>> getEnvironmentalEffects();
    void applyEnvironmentalEffects(vector<DynamicsForceApplication>* forces, const vector<DynamicsArea>& areas);
    void applyActions(f64 stepInterval);

    void update();
    void updatePose(const map<VRDevice, mat4>& pose);

    void writeback();

    void applyLinearForce(const vec3& force);
    void applyAngularForce(const vec3& force);

private:
    Dynamics* dynamics;
    World* world;
    EntityID id;

    DynamicsComponentEntity* component;

    void add();
    void remove();

    void applyForces();

    void applyIKEffects(const vector<DynamicsIKEffect>& effects);
};
