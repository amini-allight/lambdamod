/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "body_part.hpp"
#include "body_render_data.hpp"
#include "yaml_tools.hpp"

class EntityUpdate;
class NetworkEvent;

class Body
{
public:
    Body();
    Body(const Body& rhs);
    Body(Body&& rhs) noexcept;
    ~Body();

    Body& operator=(const Body& rhs);
    Body& operator=(Body&& rhs) noexcept;

    void verify();

    void compare(const Body& previous, EntityUpdate& update, bool transformed) const;

    void push(const NetworkEvent& event);
    vector<NetworkEvent> initial(const string& world, EntityID id, bool transformed) const;

#if defined(LMOD_CLIENT)
    BodyRenderData toRender(const set<BodyPartID>& hiddenBodyPartIDs) const;
#endif

    BodyPartID nextPartID();

    void add(BodyPartID parentID, BodyPart* part);
    void remove(BodyPartID id);
    void update(BodyPartID id, const BodyPart& part);
    void clear();

    BodyPart* get(BodyPartID id) const;
    BodyPart* get(BodyAnchorType type, const string& name = "") const;
    const map<BodyPartID, BodyPart*>& parts() const;
    void traverse(const function<void(BodyPart*)>& behavior);
    void traverse(const function<void(const BodyPart*)>& behavior) const;
    bool partiallyTraverse(const function<bool(BodyPart*)>& behavior);
    bool partiallyTraverse(const function<bool(const BodyPart*)>& behavior) const;
    bool empty(const set<BodyPartID>& hiddenBodyPartIDs) const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 independentPartMass() const;
    vector<vec3> subparts() const;
    tuple<vec3, vec3> boundingBox() const;

    void setParent(BodyPart* child, BodyPartSubID childSubID, BodyPart* parent, BodyPartSubID parentSubID);
    void clearParent(BodyPart* child);
    bool isCircular(BodyPart* child, BodyPart* parent) const;

    bool operator==(const Body& rhs) const;
    bool operator!=(const Body& rhs) const;

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    BodyPartID _nextPartID;
    map<BodyPartID, BodyPart*> _parts;

    void copy(const Body& rhs);
};
