/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script.hpp"
#include "tools.hpp"
#include "script_tools.hpp"
#include "recursion_depth_closure.hpp"
#include "scope_closure.hpp"

static vector<Value> parse(const vector<Token>& tokens, size_t& i, bool root = false);
Value execute(ScriptContext* context, const Value& value);

static vector<Token> tokenize(const string& s)
{
    vector<Token> tokens;
    string token;
    bool stringMode = false;
    bool escapeNext = false;

    auto endToken = [&]() -> void
    {
        if (!token.empty())
        {
            tokens.push_back(Token(token));
            token = "";
        }
    };

    for (char c : s)
    {
        if ((c < ' ' || c > '~') && c != '\r' && c != '\n' && c != '\t')
        {
            throw runtime_error("Encountered character outside of normal printable ASCII range before end of input.");
        }

        if (c == '"' && !stringMode)
        {
            endToken();
            stringMode = true;
        }
        else if (c == '"' && stringMode)
        {
            if (!escapeNext)
            {
                tokens.push_back(Token(token, true));
                token = "";
                stringMode = false;
            }
            else
            {
                token += c;
            }
        }
        else if ((c == ' ' || c == '\r' || c == '\n' || c == '\t') && !stringMode)
        {
            endToken();
        }
        else if ((c == '(' || c == ')' || c == '\'') && !stringMode)
        {
            endToken();
            tokens.push_back(Token(string() + c));
        }
        else if (c == '\\' && stringMode)
        {
            if (!escapeNext)
            {
                escapeNext = true;
            }
            else
            {
                escapeNext = false;
                token += c;
            }
        }
        else
        {
            escapeNext = false;
            token += c;
        }
    }

    if (stringMode)
    {
        throw runtime_error("Expected matching '\"' before end of input.");
    }

    endToken();

    return tokens;
}

static Value parseQuoted(const vector<Token>& tokens, size_t& i)
{
    if (i >= tokens.size())
    {
        throw runtime_error("Expected value after ''' before end of input.");
    }

    const Token& token = tokens[i];

    if (token.isString)
    {
        return Value(token);
    }
    else if (token.text == "'")
    {
        i++;
        return quote(parseQuoted(tokens, i));
    }
    else if (token.text == "(")
    {
        i++;
        return Value(parse(tokens, i));
    }
    else if (token.text == ")")
    {
        throw runtime_error("Expected value after ''' before ')'.");
    }
    else
    {
        return Value(token);
    }
}

static vector<Value> parse(const vector<Token>& tokens, size_t& i, bool root)
{
    vector<Value> values;

    for (; i < tokens.size(); i++)
    {
        const Token& token = tokens[i];

        if (token.isString)
        {
            values.push_back(Value(token));
        }
        else if (token.text == "'")
        {
            i++;
            values.push_back(quote(parseQuoted(tokens, i)));
        }
        else if (token.text == "(")
        {
            i++;
            values.push_back(Value(parse(tokens, i)));
        }
        else if (token.text == ")")
        {
            return values;
        }
        else
        {
            values.push_back(Value(token));
        }
    }

    if (!root)
    {
        throw runtime_error("Expected matching ')' before end of input.");
    }

    return values;
}

vector<Value> parse(const string& s)
{
    size_t i = 0;

    return parse(tokenize(s), i, true);
}

Value evaluate(ScriptContext* context, const Value& value)
{
    switch (value.type)
    {
    default :
    case Value_Void : return value;
    case Value_Symbol :
    {
        optional<Value> def = context->get(value.s);

        if (!def)
        {
            throw runtime_error("Definition '" + value.s + "' not found.");
        }

        return *def;
    }
    case Value_Integer : return value;
    case Value_Float : return value;
    case Value_String : return value;
    case Value_List : return execute(context, value);
    case Value_Lambda : return value;
    case Value_Handle : return value;
    }
}

Value bind(ScriptContext* context, const Value& value)
{
    switch (value.type)
    {
    default :
    case Value_Void : return value;
    case Value_Symbol :
    {
        // Does not bind globals
        optional<Value> def = context->getLocal(value.s);

        if (def)
        {
            return *def;
        }
        else
        {
            return value;
        }
    }
    case Value_Integer : return value;
    case Value_Float : return value;
    case Value_String : return value;
    case Value_List :
    {
        Value result;
        result.type = Value_List;
        result.l.reserve(value.l.size());

        for (const Value& item : value.l)
        {
            result.l.push_back(bind(context, item));
        }

        return result;
    }
    case Value_Lambda : return value;
    case Value_Handle : return value;
    }
}

static map<string, vector<Value>> zipSyntax(
    const string& name,
    const vector<Value>& argLayout,
    const vector<Value>& args
)
{
    map<string, vector<Value>> mapping;

    bool collected = false;
    size_t j = 0;

    auto collect = [&](size_t remainder) -> vector<Value>
    {
        vector<Value> collection;
        collection.reserve(args.size() - (remainder + j));

        for (; j < static_cast<i64>(args.size()) - remainder; j++)
        {
            collection.push_back(args[j]);
        }

        return collection;
    };

    for (size_t i = 0; i < argLayout.size(); i++)
    {
        const Value& value = argLayout[i];

        if (value.type == Value_Symbol)
        {
            if (value.s.ends_with("..."))
            {
                if (collected)
                {
                    throw runtime_error("Only one wildcard is allowed per list in argument layout of '" + name + "'.");
                }

                if (mapping.contains(value.s))
                {
                    throw runtime_error("Multiple definitions of '" + value.s + "' encountered in argument layout of '" + name + "'.");
                }

                mapping.insert({ value.s, collect(argLayout.size() - (i + 1)) });
                collected = true;
            }
            else if (value.s.ends_with("...+"))
            {
                if (collected)
                {
                    throw runtime_error("Only one wildcard is allowed per list in argument layout of '" + name + "'.");
                }

                if (j >= args.size())
                {
                    throw runtime_error("Insufficient arguments supplied to '" + name + "'.");
                }

                if (mapping.contains(value.s))
                {
                    throw runtime_error("Multiple definitions of '" + value.s + "' encountered in argument layout.");
                }

                vector<Value> a = { args[j++] };
                vector<Value> b = collect(argLayout.size() - (i + 1));

                mapping.insert({ value.s, join(a, b) });
                collected = true;
            }
            else
            {
                if (j >= args.size())
                {
                    throw runtime_error("Insufficient arguments supplied to '" + name + "'.");
                }

                if (mapping.contains(value.s))
                {
                    throw runtime_error("Multiple definitions of '" + value.s + "' encountered in argument layout of '" + name + "'.");
                }

                mapping.insert({ value.s, { args[j] } });
                j++;
            }
        }
        else if (value.type == Value_List)
        {
            if (j >= args.size())
            {
                throw runtime_error("Insufficient arguments supplied to '" + name + "'.");
            }

            if (args[j].type != Value_List)
            {
                throw runtime_error("Expected list as argument " + to_string(j + 1) + " to '" + name + "', got something else.");
            }

            map<string, vector<Value>> childMapping = zipSyntax(name, value.l, args[j].l);
            j++;

            for (const auto& [ key, value ] : childMapping)
            {
                if (mapping.contains(key))
                {
                    throw runtime_error("Multiple definitions of '" + key + "' encountered in argument layout of '" + name + "'.");
                }

                mapping.insert({ key, value });
            }
        }
        else
        {
            throw runtime_error("Expected list and tokens in argument layout of '" + name + "', got something else.");
        }
    }

    return mapping;
}

static Value applySyntax(Value body, const map<string, vector<Value>>& args)
{
    for (size_t i = 0; i < body.l.size();)
    {
        Value& child = body.l[i];

        if (child.type == Value_List)
        {
            child = applySyntax(child, args);
            i++;
        }
        else if (child.type == Value_Symbol && args.contains(child.s))
        {
            const vector<Value>& values = args.at(child.s);

            body.l.erase(body.l.begin() + i);
            body.l.insert(
                body.l.begin() + i,
                values.begin(),
                values.end()
            );
            i += values.size();
        }
        else
        {
            i++;
        }
    }

    return body;
}

Value execute(ScriptContext* context, const Value& value)
{
    if (value.type != Value_List)
    {
        throw runtime_error("Impossible state entered, received non-expression for execution.");
    }

    if (value.l.empty())
    {
        throw runtime_error("Cannot execute an empty expression.");
    }

    Value head = value.l.front();
    vector<Value> rest({ value.l.begin() + 1, value.l.end() });

    // Resolve to lambda
    string name = "<anonymous lambda>";

    if (head.type == Value_Symbol)
    {
        name = head.s;
    }

    head = evaluate(context, head);

    if (head.type != Value_Lambda)
    {
        throw runtime_error("First element in execution must evaluate to a lambda value.");
    }

    Lambda lambda = head.lambda;

    RecursionDepthClosure recursionDepth(context, name);
    context->trace(name);

    // Apply lambda to arguments
    Value result;

    switch (lambda.type)
    {
    default :
        throw runtime_error("Impossible state entered, received unknown lambda type for execution.");
    case Lambda_Normal :
    {
        if (rest.size() != lambda.argNames.size())
        {
            throw runtime_error(lambda.argNames.size() == 1
                ? "Expected 1 argument to '" + name + "', got " + to_string(rest.size()) + "."
                : "Expected " + to_string(lambda.argNames.size()) + " arguments to '" + name + "', got " + to_string(rest.size()) + "."
            );
        }

        vector<Value> values;
        values.reserve(rest.size());

        for (const Value& item : rest)
        {
            values.push_back(evaluate(context, item));
        }

        ScopeClosure closure(context);

        for (size_t i = 0; i < lambda.argNames.size(); i++)
        {
            context->define(
                lambda.argNames[i],
                values[i]
            );
        }

        for (const Value& item : lambda.body)
        {
            result = evaluate(context, item);
        }
        break;
    }
    case Lambda_Syntax :
    {
        Value body = lambda.body.front();

        body = applySyntax(body, zipSyntax(name, lambda.argLayout, rest));

        result = evaluate(context, body);
        break;
    }
    case Lambda_Builtin :
    {
        const ScriptBuiltin* builtin = context->getBuiltin(lambda.builtinName);

        if (!builtin)
        {
            throw runtime_error("Builtin '" + lambda.builtinName + "' referenced by lambda not found.");
        }

        result = builtin->call(context, rest);
        break;
    }
    }

    context->untrace();
    return result;
}
