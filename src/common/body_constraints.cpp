/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "body_constraints.hpp"
#include "log.hpp"
#include "constants.hpp"
#include "yaml_tools.hpp"

string bodyConstraintTypeToString(BodyConstraintType type)
{
    switch (type)
    {
    case Body_Constraint_Fixed :
        return "body-constraint-fixed";
    case Body_Constraint_Ball :
        return "body-constraint-ball";
    case Body_Constraint_Cone :
        return "body-constraint-cone";
    case Body_Constraint_Hinge :
        return "body-constraint-hinge";
    default :
        fatal("Encountered unknown body constraint type '" + to_string(type) + "'.");
    }
}

BodyConstraintType bodyConstraintTypeFromString(const string& s)
{
    if (s == "body-constraint-fixed")
    {
        return Body_Constraint_Fixed;
    }
    else if (s == "body-constraint-ball")
    {
        return Body_Constraint_Ball;
    }
    else if (s == "body-constraint-cone")
    {
        return Body_Constraint_Cone;
    }
    else if (s == "body-constraint-hinge")
    {
        return Body_Constraint_Hinge;
    }
    else
    {
        fatal("Encountered unknown body constraint type '" + s + "'.");
    }
}

template<>
BodyConstraintType YAMLNode::convert() const
{
    return bodyConstraintTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(BodyConstraintType v)
{
    emit(bodyConstraintTypeToString(v));
}

BodyConstraintFixed::BodyConstraintFixed()
{

}

void BodyConstraintFixed::verify()
{

}

bool BodyConstraintFixed::operator==(const BodyConstraintFixed& rhs) const
{
    return true;
}

bool BodyConstraintFixed::operator!=(const BodyConstraintFixed& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyConstraintFixed YAMLNode::convert() const
{
    BodyConstraintFixed constraint;

    return constraint;
}

template<>
void YAMLSerializer::emit(BodyConstraintFixed v)
{
    startMapping();

    endMapping();
}

BodyConstraintBall::BodyConstraintBall()
{
    minimum.x = clamp(minimum.x, -pi, 0.0);
    minimum.y = clamp(minimum.y, -pi, 0.0);
    minimum.z = clamp(minimum.z, -pi, 0.0);

    maximum.x = clamp(maximum.x, 0.0, +pi);
    maximum.y = clamp(maximum.y, 0.0, +pi);
    maximum.z = clamp(maximum.z, 0.0, +pi);
}

void BodyConstraintBall::verify()
{

}

bool BodyConstraintBall::operator==(const BodyConstraintBall& rhs) const
{
    return minimum == rhs.minimum &&
        maximum == rhs.maximum;
}

bool BodyConstraintBall::operator!=(const BodyConstraintBall& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyConstraintBall YAMLNode::convert() const
{
    BodyConstraintBall constraint;

    constraint.minimum = at("minimum").as<vec3>();
    constraint.maximum = at("maximum").as<vec3>();

    return constraint;
}

template<>
void YAMLSerializer::emit(BodyConstraintBall v)
{
    startMapping();

    emitPair("minimum", v.minimum);
    emitPair("maximum", v.maximum);

    endMapping();
}

BodyConstraintCone::BodyConstraintCone()
{
    twistMinimum = 0;
    twistMaximum = 0;
}

void BodyConstraintCone::verify()
{
    span.x = clamp(span.x, 0.0, pi * 2);
    span.y = clamp(span.y, 0.0, pi * 2);
    twistMinimum = clamp(twistMinimum, -pi, 0.0);
    twistMaximum = clamp(twistMaximum, 0.0, +pi);
}

bool BodyConstraintCone::operator==(const BodyConstraintCone& rhs) const
{
    return span == rhs.span &&
        twistMinimum == rhs.twistMinimum &&
        twistMaximum == rhs.twistMaximum;
}

bool BodyConstraintCone::operator!=(const BodyConstraintCone& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyConstraintCone YAMLNode::convert() const
{
    BodyConstraintCone constraint;

    constraint.span = at("span").as<vec2>();
    constraint.twistMinimum = at("twistMinimum").as<f64>();
    constraint.twistMaximum = at("twistMaximum").as<f64>();

    return constraint;
}

template<>
void YAMLSerializer::emit(BodyConstraintCone v)
{
    startMapping();

    emitPair("span", v.span);
    emitPair("twistMinimum", v.twistMinimum);
    emitPair("twistMaximum", v.twistMaximum);

    endMapping();
}

BodyConstraintHinge::BodyConstraintHinge()
{
    minimum = 0;
    maximum = 0;
}

void BodyConstraintHinge::verify()
{
    minimum = clamp(minimum, -pi, 0.0);
    maximum = clamp(maximum, 0.0, +pi);
}

bool BodyConstraintHinge::operator==(const BodyConstraintHinge& rhs) const
{
    return minimum == rhs.minimum &&
        maximum == rhs.maximum;
}

bool BodyConstraintHinge::operator!=(const BodyConstraintHinge& rhs) const
{
    return !(*this == rhs);
}

template<>
BodyConstraintHinge YAMLNode::convert() const
{
    BodyConstraintHinge constraint;

    constraint.minimum = at("minimum").as<f64>();
    constraint.maximum = at("maximum").as<f64>();

    return constraint;
}

template<>
void YAMLSerializer::emit(BodyConstraintHinge v)
{
    startMapping();

    emitPair("minimum", v.minimum);
    emitPair("maximum", v.maximum);

    endMapping();
}
