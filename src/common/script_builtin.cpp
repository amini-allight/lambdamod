/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_builtin.hpp"
#include "script_tools.hpp"
#include "user.hpp"

ScriptBuiltin::ScriptBuiltin(
    const string& name,
    const vector<string>& argNames,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth
)
    : _name(name)
    , _argNames(argNames)
    , _type(Script_Builtin_Normal)
    , _auth(auth)
    , behavior(behavior)
{

}

ScriptBuiltin::ScriptBuiltin(
    const string& name,
    bool variadic,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth
)
    : _name(name)
    , _type(variadic ? Script_Builtin_Variadic : Script_Builtin_Syntax)
    , _auth(auth)
    , behavior(behavior)
{

}

const string& ScriptBuiltin::name() const
{
    return _name;
}

const vector<string>& ScriptBuiltin::argNames() const
{
    return _argNames;
}

ScriptBuiltinType ScriptBuiltin::type() const
{
    return _type;
}

ScriptBuiltinAuth ScriptBuiltin::auth() const
{
    return _auth;
}

Value ScriptBuiltin::call(ScriptContext* ctx, vector<Value> args) const
{
    switch (_auth)
    {
    case Script_Builtin_Auth_None :
        break;
    case Script_Builtin_Auth_User :
        if (!ctx->currentUser())
        {
            throw toValue("Function '" + _name + "' must be called with a user's authorization.");
        }
        break;
    case Script_Builtin_Auth_Admin :
        if (!ctx->currentUser())
        {
            throw toValue("Function '" + _name + "' must be called with a user's authorization.");
        }

        if (!ctx->currentUser()->admin())
        {
            throw toValue("Permission to execute '" + _name + "' denied.");
        }
        break;
    }

    switch (_type)
    {
    case Script_Builtin_Syntax :
        break;
    case Script_Builtin_Normal :
        if (args.size() != _argNames.size())
        {
            throw toValue("Expected " + to_string(_argNames.size()) + (_argNames.size() == 1 ? " argument" : " arguments") + " to function '" + _name + "', got " + to_string(args.size()) + ".");
        }
        [[fallthrough]];
    case Script_Builtin_Variadic :
        for (Value& arg : args)
        {
            arg = evaluate(ctx, arg);
        }
        break;
    }

    return behavior(_name, ctx, args);
}

pair<string, ScriptBuiltin> makeBuiltin(
    const string& name,
    const vector<string>& argNames,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth
)
{
    return { name, ScriptBuiltin(name, argNames, behavior, auth) };
}

pair<string, ScriptBuiltin> makeVariadicBuiltin(
    const string& name,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth
)
{
    return { name, ScriptBuiltin(name, true, behavior, auth) };
}

pair<string, ScriptBuiltin> makeSyntaxBuiltin(
    const string& name,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth
)
{
    return { name, ScriptBuiltin(name, false, behavior, auth) };
}

pair<string, ScriptBuiltin> clientSideStub(const string& name, const Value& value)
{
    return { name, ScriptBuiltin(name, {}, [=](const string& functionName, ScriptContext* context, const vector<Value>& args) -> Value {
        return value;
    }) };
}
