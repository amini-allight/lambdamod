/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "inverse_kinematics.hpp"
#include "tools.hpp"

static mat4 interpolate(const mat4& current, const mat4& target, f64 stepInterval, u32 delay)
{
    f64 fraction;

    if (delay != 0)
    {
        fraction = stepInterval / (delay * 1000.0);
    }
    else
    {
        fraction = 1;
    }

    vec3 positionDelta = target.position() - current.position();
    quaternion rotationDelta = target.rotation() / current.rotation();
    vec3 scaleDelta = target.scale() / current.scale();

    return {
        current.position() + positionDelta * fraction,
        (rotationDelta * fraction) * current.rotation(),
        current.scale() * (((scaleDelta - 1) * fraction) + 1)
    };
}

tuple<vec3, quaternion> cyclicCoordinateDescent(
    const mat4& self,
    const mat4& currentChild,
    const mat4& targetChild,
    f64 stepInterval,
    u32 delay
)
{
    mat4 nextChild = interpolate(currentChild, targetChild, stepInterval, delay);

    vec3 currentDir = (currentChild.position() - self.position()).normalize();
    vec3 nextDir = (nextChild.position() - self.position()).normalize();

    vec3 axis;

    if (roughly(abs(currentDir.dot(nextDir)), 1.0))
    {
        axis = pickSideDirection(currentDir);
    }
    else
    {
        axis = currentDir.cross(nextDir).normalize();
    }

    f64 angle = handedAngleBetween(currentDir, nextDir, axis);

    quaternion rotation(axis, angle);

    vec3 currentPos = rotation.rotate(currentChild.position() - self.position()) + self.position();
    vec3 nextPos = nextChild.position();

    vec3 translation = nextPos - currentPos;

    return {
        translation,
        rotation
    };
}
