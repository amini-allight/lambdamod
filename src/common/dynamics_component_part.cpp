/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_component_part.hpp"
#include "dynamics.hpp"
#include "entity.hpp"

DynamicsComponentPart::DynamicsComponentPart(
    Dynamics* dynamics,
    DynamicsComponent* parent,
    Entity* entity,
    BodyPart* part
)
    : DynamicsComponent(dynamics, parent, entity)
    , part(part)
{
    createShape(entity->globalScale());

    addToShape(part);

    createBody(
        entity->globalTransform().applyToPosition(part->regionalPosition()),
        entity->globalTransform().applyToRotation(part->regionalRotation()),
        part->mass()
    );

    createConstraint();

    createChildren();

    lastRegionalTransform = regionalTransform();
}

DynamicsComponentPart::~DynamicsComponentPart()
{
    destroyChildren();
    destroyConstraint();
    destroyBody();
    destroyShape();
}

void DynamicsComponentPart::applyIKEffect(const vec3& translation, const quaternion& rotation)
{
    btTransform transform = body->getWorldTransform();
    transform.setOrigin(transform.getOrigin() + btVector3(translation.x, translation.y, translation.z));
    body->setWorldTransform(transform);

    f64 angles[3]{};

    btQuaternion q(rotation.x, rotation.y, rotation.z, rotation.w);
    q.getEulerZYX(angles[2], angles[1], angles[0]);

    for (int i = Motor_Angular_X; i <= Motor_Angular_Z; i++)
    {
        constraint->setServoTarget(i, angles[i - Motor_Angular_X]);
    }
}

void DynamicsComponentPart::writeback()
{
    if (mat4(part->regionalPosition(), part->regionalRotation(), vec3(1)) == lastRegionalTransform)
    {
        part->setRegionalTransform(regionalTransform());
        lastRegionalTransform = regionalTransform();
    }
}

quaternion DynamicsComponentPart::constrainRotation(const quaternion& rotation) const
{
    vec3 euler = rotation.euler();

    switch (part->constraintType())
    {
    case Body_Constraint_Fixed :
        fatal("Fixed part constraint cannot be animated.");
    case Body_Constraint_Ball :
    {
        const BodyConstraintBall& ball = part->getConstraint<BodyConstraintBall>();

        euler.x = clamp(euler.x, ball.minimum.x, ball.maximum.x);
        euler.y = clamp(euler.y, ball.minimum.y, ball.maximum.y);
        euler.z = clamp(euler.z, ball.minimum.z, ball.maximum.z);
        break;
    }
    case Body_Constraint_Cone :
    {
        const BodyConstraintCone& cone = part->getConstraint<BodyConstraintCone>();

        euler.x = clamp(euler.x, cone.span.x / -2, cone.span.x / +2);
        euler.y = clamp(euler.y, cone.twistMinimum, cone.twistMaximum);
        euler.z = clamp(euler.z, cone.span.y / -2, cone.span.y / +2);
        break;
    }
    case Body_Constraint_Hinge :
    {
        const BodyConstraintHinge& hinge = part->getConstraint<BodyConstraintHinge>();

        euler.x = clamp(euler.x, hinge.minimum, hinge.maximum);
        euler.y = clamp(euler.y, 0.0, 0.0);
        euler.z = clamp(euler.z, 0.0, 0.0);
        break;
    }
    }

    return quaternion(euler);
}

bool DynamicsComponentPart::supportsAnimation() const
{
    return true;
}

void DynamicsComponentPart::createConstraint()
{
    mat4 constraintGlobalTransform = entity->globalTransform().applyToTransform(part->constraintTransform());

    constraint = new btGeneric6DofSpring2Constraint(
        *parent->body,
        *body,
        parent->inBodySpace(constraintGlobalTransform),
        inBodySpace(constraintGlobalTransform)
    );
    dynamics->world()->addConstraint(constraint);

    constraint->setLinearLowerLimit(btVector3(0, 0, 0));
    constraint->setLinearUpperLimit(btVector3(0, 0, 0));
    constraint->setAngularLowerLimit(btVector3(0, 0, 0));
    constraint->setAngularUpperLimit(btVector3(0, 0, 0));

    switch (part->constraintType())
    {
    case Body_Constraint_Fixed :
        fatal("Fixed part constraints cannot be instantiated.");
    case Body_Constraint_Ball :
    {
        const BodyConstraintBall& ball = part->getConstraint<BodyConstraintBall>();

        constraint->setAngularLowerLimit(btVector3(ball.minimum.x, ball.minimum.y, ball.minimum.z));
        constraint->setAngularUpperLimit(btVector3(ball.maximum.x, ball.maximum.y, ball.maximum.z));
        break;
    }
    case Body_Constraint_Cone :
    {
        const BodyConstraintCone& cone = part->getConstraint<BodyConstraintCone>();

        constraint->setAngularLowerLimit(btVector3(cone.span.x / -2, cone.twistMinimum, cone.span.y / -2));
        constraint->setAngularUpperLimit(btVector3(cone.span.x / +2, cone.twistMaximum, cone.span.y / +2));
        break;
    }
    case Body_Constraint_Hinge :
    {
        const BodyConstraintHinge& hinge = part->getConstraint<BodyConstraintHinge>();

        constraint->setAngularLowerLimit(btVector3(hinge.minimum, 0, 0));
        constraint->setAngularUpperLimit(btVector3(hinge.maximum, 0, 0));
        break;
    }
    }

    for (int i = Motor_Angular_X; i <= Motor_Angular_Z; i++)
    {
        constraint->enableMotor(i, true);
        constraint->setMaxMotorForce(i, part->maxForce());
        constraint->setTargetVelocity(i, part->maxVelocity());
        constraint->setServo(i, true);
    }
}

void DynamicsComponentPart::createChildren()
{
    set<BodyPartID> includedPartIDs = DynamicsComponent::createChildren(part);

    for (const auto& [ id, child ] : entity->children())
    {
        if (!includedPartIDs.contains(child->parentPartID()))
        {
            continue;
        }

        children.push_back(new DynamicsComponentEntity(dynamics, this, child));
    }
}

mat4 DynamicsComponentPart::regionalTransform() const
{
    return entity->globalTransform().applyToTransform(globalTransform(), false);
}
