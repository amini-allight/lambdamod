/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_gamelib.hpp"
#include "script_tools.hpp"
#include "script_builtin_tools.hpp"
#include "input_types.hpp"
#include "game_state.hpp"
#include "prefabs.hpp"

#if defined(LMOD_SERVER)
#include "server/global.hpp"
#include "server/controller.hpp"
#elif defined(LMOD_CLIENT)
#include "client/global.hpp"
#include "client/controller.hpp"
#endif

static Value builtin_time_multiplier(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1 });

    if (args.size() == 0)
    {
        return toValue(context->game->timeMultiplier());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_NUMBER(0);

        Value multiplier = args[0];

        context->game->setTimeMultiplier(context->currentUser(), multiplier.number<f64>());

        return Value();
    }
}

#if defined(LMOD_SERVER)
static Value builtin_checkpoint_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value result;
    result.type = Value_List;

    for (const auto& [ name, checkpoint ] : g_controller->checkpoints())
    {
        result.l.push_back(symbol(name));
    }

    return result;
}

static Value builtin_checkpoint_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_NAME_VALID(0);

    Value name = args[0];

    if (g_controller->checkpoints().contains(name.s))
    {
        throw toValue("Target checkpoint '" + name.s + "' in '" + functionName + "' not found.");
    }

    g_controller->addCheckpoint(name.s);

    return Value();
}

static Value builtin_checkpoint_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    if (!g_controller->checkpoints().contains(name.s))
    {
        throw toValue("Target checkpoint '" + name.s + "' in '" + functionName + "' not found.");
    }

    g_controller->removeCheckpoint(name.s);

    return Value();
}

static Value builtin_checkpoint_revert(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    if (!g_controller->checkpoints().contains(name.s))
    {
        throw toValue("Target checkpoint '" + name.s + "' in '" + functionName + "' not found.");
    }

    g_controller->revertCheckpoint(name.s);

    return Value();
}
#endif

static Value builtin_world_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value result;
    result.type = Value_List;
    result.l.reserve(context->game->worlds().size());

    for (const auto& [ name, world ] : context->game->worlds())
    {
        result.l.push_back(symbol(name));
    }

    return result;
}

static Value builtin_world_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    const World* world = context->game->getWorld(name.s);

    if (world)
    {
        throw toValue("Target world '" + name.s + "' in '" + functionName + "' already exists.");
    }

    context->game->addWorld(name.s);

    return Value();
}

static Value builtin_world_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    const World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    context->game->removeWorld(name.s);

    return Value();
}

static Value builtin_world_speed_of_sound(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    if (args.size() == 1)
    {
        return toValue(world->speedOfSound());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_GREATER(1, 0);

        Value speed = args[1];

        world->setSpeedOfSound(speed.number<f64>());

        return Value();
    }
}

static Value builtin_world_gravity(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_VEC3(1);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    if (args.size() == 1)
    {
        return toValue(world->gravity());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_VEC3(1);

        Value gravity = args[1];

        world->setGravity(fromValue<vec3>(gravity));

        return Value();
    }
}

static Value builtin_world_flow_velocity(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_VEC3(1);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    if (args.size() == 1)
    {
        return toValue(world->flowVelocity());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_VEC3(1);

        Value flowVelocity = args[1];

        world->setFlowVelocity(fromValue<vec3>(flowVelocity));

        return Value();
    }
}

static Value builtin_world_density(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    if (args.size() == 1)
    {
        return toValue(world->density());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_GREATER_EQUAL(1, 0);

        Value density = args[1];

        world->setDensity(density.number<f64>());

        return Value();
    }
}

static Value builtin_world_up(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    if (args.size() == 1)
    {
        return toValue(world->up());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_VEC3(1);

        Value up = args[1];

        world->setUp(fromValue<vec3>(up));

        return Value();
    }
}

static Value builtin_world_show(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    world->setShown(true);

    return Value();
}

static Value builtin_world_hide(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    world->setShown(false);

    return Value();
}

static Value builtin_world_shown_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_INTEGER(0);

    Value name = args[0];
    Value state = args[1];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    world->setShown(state.i);

    return Value();
}

static Value builtin_world_shown_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    const World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    return toValue(world->shown());
}

static Value builtin_world_fog_distance(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    if (args.size() == 1)
    {
        return toValue(world->fogDistance());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ADMIN;
        CHECK_ARG_FLOAT(1);

        Value distance = args[1];

        world->setFogDistance(distance.number<f64>());

        return Value();
    }
}

static Value builtin_world_entity_find(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_SYMBOL(1);

    Value name = args[0];
    Value pattern = args[1];

    const World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    Value list;
    list.type = Value_List;

    for (const Entity* entity : world->find(pattern.s))
    {
        Value handle;
        handle.type = Value_Handle;
        handle.id = entity->id();

        list.l.push_back(handle);
    }

    return list;
}

static Value builtin_world_entity_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    const World* world = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(world);

    Value result;
    result.type = Value_List;

    world->traverse([&](const Entity* entity) -> void {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    });

    return result;
}

static Value builtin_world_entity_near(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_FLOAT_GREATER(2, 0);

    Value name = args[0];
    Value vPosition = args[1];
    Value vDistance = args[2];

    const World* world = context->game->getWorld(name.s);
    vec3 position = fromValue<vec3>(vPosition);
    f64 distance = vDistance.number<f64>();

    CHECK_WORLD_EXISTS(world);

    Value result;
    result.type = Value_List;

    for (const Entity* entity : world->near(position, distance))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_world_entity_ray(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_VEC3(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_FLOAT_GREATER(3, 0);

    Value name = args[0];
    Value vPosition = args[1];
    Value vDirection = args[2];
    Value vDistance = args[3];

    const World* world = context->game->getWorld(name.s);
    vec3 position = fromValue<vec3>(vPosition);
    vec3 direction = fromValue<vec3>(vDirection);
    f64 distance = vDistance.number<f64>();

    CHECK_WORLD_EXISTS(world);

    Value result;
    result.type = Value_List;

    for (const auto& [ entity, hit ] : world->intersect(position, direction, distance))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(composeList(item, toValue(hit)));
    }

    return result;
}

static Value builtin_world_entity_view(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_VEC3(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_NUMBER(4);
    CHECK_ARG_FLOAT_GREATER(3, 0);
    CHECK_ARG_FLOAT_GREATER(4, 0);

    Value name = args[0];
    Value vPosition = args[1];
    Value vDirection = args[2];
    Value vAngle = args[3];
    Value vDistance = args[4];

    const World* world = context->game->getWorld(name.s);
    vec3 position = fromValue<vec3>(vPosition);
    vec3 direction = fromValue<vec3>(vDirection);
    f64 angle = vAngle.number<f64>();
    f64 distance = vDistance.number<f64>();

    CHECK_WORLD_EXISTS(world);

    Value result;
    result.type = Value_List;

    for (const Entity* entity : world->view(position, direction, angle, distance))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_entity_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_NAME_VALID(1);

    Value worldName = args[0];
    Value name = args[1];

    World* world = context->game->getWorld(worldName.s);

    CHECK_WORLD_EXISTS(world);

    auto entity = new Entity(world, context->game->nextEntityID());
    prefabDefault(entity, mat4());

    entity->setName(name.s);
    entity->setOwnerID(context->currentUser()->id());

    world->add(EntityID(), entity);

    Value value;
    value.type = Value_Handle;
    value.id = entity->id();

    return value;
}

static Value builtin_entity_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    World* world = context->game->get(target.id)->world();

    world->remove(target.id);

    return Value();
}

static Value builtin_entity_copy(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value handle = args[0];

    const Entity* entity = context->game->get(handle.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    World* world = context->game->get(handle.id)->world();

    auto newEntity = new Entity(*entity);
    newEntity->setOwnerID(context->currentUser()->id());

    world->add(newEntity->parent() ? newEntity->parent()->id() : EntityID(), newEntity);

    return toValue(newEntity->id());
}

static Value builtin_entity_shift(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    if (entity->parent())
    {
        throw toValue("Target entity in '" + functionName + "' must not be a child.");
    }

    World* oldWorld = context->game->get(target.id)->world();
    World* newWorld = context->game->getWorld(name.s);

    CHECK_WORLD_EXISTS(newWorld);

    auto newEntity = new Entity(*entity);

    oldWorld->remove(newEntity->id());
    newWorld->add(EntityID(), newEntity);

    return Value();
}

static Value builtin_entity_find(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value pattern = args[1];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    World* world = context->game->get(target.id)->world();

    Value result;
    result.type = Value_List;

    for (const Entity* entity : world->find(pattern.s))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_entity_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    World* world = context->game->get(target.id)->world();

    Value result;
    result.type = Value_List;

    world->traverse([&](const Entity* entity) -> void {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    });

    return result;
}

static Value builtin_entity_near(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value vPosition = args[1];
    Value vDistance = args[2];

    const Entity* entity = context->game->get(target.id);
    vec3 position = fromValue<vec3>(vPosition);
    f64 distance = vDistance.number<f64>();

    CHECK_ENTITY_EXISTS(entity);

    World* world = context->game->get(target.id)->world();

    position = entity->globalPosition() + entity->globalRotation().rotate(position);

    Value result;
    result.type = Value_List;

    for (const Entity* entity : world->near(position, distance))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_entity_ray(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_VEC3(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value vPosition = args[1];
    Value vDirection = args[2];
    Value vDistance = args[3];

    const Entity* entity = context->game->get(target.id);
    vec3 position = fromValue<vec3>(vPosition);
    vec3 direction = fromValue<vec3>(vDirection);
    f64 distance = vDistance.number<f64>();

    CHECK_ENTITY_EXISTS(entity);

    World* world = context->game->get(target.id)->world();

    position = entity->globalPosition() + entity->globalRotation().rotate(position);
    direction = entity->globalRotation().rotate(direction);

    Value result;
    result.type = Value_List;

    for (const auto& [ entity, hit ] : world->intersect(position, direction, distance))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(composeList(item, toValue(hit)));
    }

    return result;
}

static Value builtin_entity_view(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_VEC3(1);
    CHECK_ARG_VEC3(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_NUMBER(4);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value vPosition = args[1];
    Value vDirection = args[2];
    Value vAngle = args[3];
    Value vDistance = args[4];

    const Entity* entity = context->game->get(target.id);
    vec3 position = fromValue<vec3>(vPosition);
    vec3 direction = fromValue<vec3>(vDirection);
    f64 angle = vAngle.number<f64>();
    f64 distance = vDistance.number<f64>();

    CHECK_ENTITY_EXISTS(entity);

    World* world = context->game->get(target.id)->world();

    position = entity->globalPosition() + entity->globalRotation().rotate(position);
    direction = entity->globalRotation().rotate(direction);

    Value result;
    result.type = Value_List;

    for (const Entity* entity : world->view(position, direction, angle, distance))
    {
        Value item;
        item.type = Value_Handle;
        item.id = entity->id();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_entity_transfer(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value id = args[1];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setOwnerID(user->id());

    return Value();
}

static Value builtin_entity_take(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setOwnerID(context->currentUser()->id());

    return Value();
}

static Value builtin_entity_discard(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setOwnerID(UserID());

    return Value();
}

static Value builtin_entity_owner(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->ownerID());
}

static Value builtin_entity_share(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setShared(true);

    return Value();
}

static Value builtin_entity_unshare(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setShared(false);

    return Value();
}

static Value builtin_entity_shared_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setShared(state.i);

    return Value();
}

static Value builtin_entity_shared_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->shared());
}

#if defined(LMOD_SERVER)
static Value builtin_entity_attach(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_INTEGER(1);

    Value handle = args[0];
    Value id = args[1];

    Entity* entity = context->game->get(handle.id);

    CHECK_ENTITY_EXISTS(entity);

    if (entity->attachedUserID())
    {
        throw toValue("Target entity in '" + functionName + "' already has a user attached.");
    }

    if (!entity->active())
    {
        throw toValue("Target entity in '" + functionName + "' is not active.");
    }

    if (!entity->host())
    {
        throw toValue("Target entity in '" + functionName + "' is not marked as a host.");
    }

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    if (user->entityID())
    {
        throw toValue("Target user in '" + functionName + "' is already attached.");
    }

    context->game->attach(user, entity);

    return Value();
}

static Value builtin_entity_detach(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value handle = args[0];

    Entity* entity = context->game->get(handle.id);

    CHECK_ENTITY_EXISTS(entity);

    if (!entity->attachedUserID())
    {
        throw toValue("Target entity in '" + functionName + "' does not have a user attached.");
    }

    context->game->detach(context->game->getUser(entity->attachedUserID()));

    return Value();
}
#endif

static Value builtin_entity_attached_user(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->attachedUserID());
}

static Value builtin_entity_global_position(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->globalPosition());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setGlobalPosition(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_global_rotation(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->globalRotation());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_QUATERNION(1);

        Value value = args[1];

        entity->setGlobalRotation(fromValue<quaternion>(value));

        return Value();
    }
}

static Value builtin_entity_global_scale(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->globalScale());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setGlobalScale(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_local_position(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->localPosition());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setGlobalPosition(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_local_rotation(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->localRotation());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_QUATERNION(1);

        Value value = args[1];

        entity->setGlobalRotation(fromValue<quaternion>(value));

        return Value();
    }
}

static Value builtin_entity_local_scale(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->localScale());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setGlobalScale(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_linear_velocity(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->linearVelocity());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setLinearVelocity(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_angular_velocity(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->angularVelocity());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setAngularVelocity(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_world(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return symbol(entity->world()->name());
}

static Value builtin_entity_name(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return symbol(entity->name());
}

static Value builtin_entity_play(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setActive(true);

    return Value();
}

static Value builtin_entity_pause(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setActive(false);

    return Value();
}

static Value builtin_entity_store(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->store();

    return Value();
}

static Value builtin_entity_reset(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->reset();

    return Value();
}

static Value builtin_entity_active_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setActive(state.i);

    return Value();
}

static Value builtin_entity_active_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->active());
}

static Value builtin_entity_parent(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value childID = args[0];

    const Entity* child = context->game->get(childID.id);

    CHECK_ENTITY_EXISTS(child);

    return toValue(child->parent() ? child->parent()->id() : EntityID());
}

static Value builtin_entity_parent_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_HANDLE(1);
    CHECK_ARG_HANDLE_VALID(1);

    Value childID = args[0];
    Value parentID = args[1];

    Entity* child = context->game->get(childID.id);

    CHECK_ENTITY_EXISTS(child);
    CHECK_USER_ALLOWED(*child);

    Entity* parent = context->game->get(parentID.id);

    CHECK_ENTITY_EXISTS(parent);

    World* world = child->world();

    if (parent->world() != world)
    {
        throw toValue("Cannot parent entities in different worlds.");
    }

    if (world->isCircular(child, parent))
    {
        throw toValue("Cannot create a circular parent relationship.");
    }

    world->setParent(child, parent);

    return Value();
}

static Value builtin_entity_parent_clear(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value childID = args[0];

    Entity* child = context->game->get(childID.id);

    CHECK_ENTITY_EXISTS(child);

    World* world = child->world();

    world->clearParent(child);

    return Value();
}

static Value builtin_entity_children(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value parentID = args[0];

    const Entity* parent = context->game->get(parentID.id);

    CHECK_ENTITY_EXISTS(parent);

    Value result;
    result.type = Value_List;
    result.l.reserve(parent->children().size());

    for (const auto& [ id, child ] : parent->children())
    {
        Value item;
        item.type = Value_Handle;
        item.id = id;

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_entity_mass(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->mass());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_GREATER_EQUAL(1, 0);

        Value value = args[1];

        entity->setMass(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_drag(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->drag());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_GREATER_EQUAL(1, 0);

        Value value = args[1];

        entity->setDrag(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_buoyancy(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->buoyancy());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_GREATER_EQUAL(1, 0);

        Value value = args[1];

        entity->setBuoyancy(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_lift(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->lift());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);

        Value value = args[1];

        entity->setLift(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_magnetism(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->magnetism());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);

        Value value = args[1];

        entity->setMagnetism(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_friction(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->friction());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_RANGE(1, 0, 1);

        Value value = args[1];

        entity->setFriction(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_restitution(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->restitution());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);
        CHECK_ARG_FLOAT_RANGE(1, 0, 1);

        Value value = args[1];

        entity->setRestitution(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_sample_play(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_INTEGER(3);
    CHECK_ARG_NUMBER(4);
    CHECK_ARG_SYMBOL(5);
    CHECK_ARG_INTEGER(6);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];
    Value volume = args[2];
    Value directional = args[3];
    Value angle = args[4];
    Value anchorName = args[5];
    Value loop = args[6];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    SamplePlaybackID playbackID = entity->playSample(
        name.s,
        volume.number<f64>(),
        directional.i,
        angle.number<f64>(),
        anchorName.s,
        loop.i
    );

    return toValue(playbackID);
}

static Value builtin_entity_sample_stop(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value id = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->stopSample(SamplePlaybackID(id.i));

    return Value();
}

static Value builtin_entity_action_play(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_INTEGER(2);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];
    Value loop = args[2];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    ActionPlaybackID playbackID = entity->playAction(name.s, loop.i);

    return toValue(playbackID);
}

static Value builtin_entity_action_stop(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value id = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->stopAction(ActionPlaybackID(id.i));

    return Value();
}

static Value builtin_entity_body_anchor(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_STRING(1);

    Value target = args[0];
    string anchorName = args[1].s;

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    optional<mat4> transform = entity->anchor(anchorName);

    if (transform)
    {
        return composeList(transform->position(), transform->rotation());
    }
    else
    {
        throw toValue("Target anchor '" + anchorName + "' not found.");
    }
}

static Value builtin_entity_hostify(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setHost(true);

    return Value();
}

static Value builtin_entity_unhostify(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setHost(false);

    return Value();
}

static Value builtin_entity_host_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setHost(state.i);

    return Value();
}

static Value builtin_entity_host_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->host());
}

static Value builtin_entity_show(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setShown(true);

    return Value();
}

static Value builtin_entity_hide(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setShown(false);

    return Value();
}

static Value builtin_entity_shown_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setShown(state.i);

    return Value();
}

static Value builtin_entity_shown_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->shown());
}

static Value builtin_entity_physicalize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setPhysical(true);

    return Value();
}

static Value builtin_entity_etherealize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setPhysical(false);

    return Value();
}

static Value builtin_entity_physical_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setPhysical(state.i);

    return Value();
}

static Value builtin_entity_physical_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->physical());
}

static Value builtin_entity_visualize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setVisible(true);

    return Value();
}

static Value builtin_entity_unvisualize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setVisible(false);

    return Value();
}

static Value builtin_entity_visible_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setVisible(state.i);

    return Value();
}

static Value builtin_entity_visible_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->visible());
}

static Value builtin_entity_audialize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setAudible(true);

    return Value();
}

static Value builtin_entity_unaudialize(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setAudible(false);

    return Value();
}

static Value builtin_entity_audible_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setAudible(state.i);

    return Value();
}

static Value builtin_entity_audible_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->audible());
}

static Value builtin_entity_binding_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_SYMBOL(2);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_NAME_VALID(2);

    Value target = args[0];
    Value type = args[1];
    Value input = args[2];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->addBinding(inputTypeFromString(type.s), { InputBinding(input.s) });

    return Value();
}

static Value builtin_entity_binding_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value type = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->removeBinding(inputTypeFromString(type.s));

    return Value();
}

static Value builtin_entity_bindings(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    Value result;
    result.type = Value_List;

    for (const auto& [ input, binding ] : entity->bindings())
    {
        result.l.push_back(composeList(
            symbol(inputTypeToString(static_cast<InputType>(input))),
            symbol(binding.input)
        ));
    }

    return result;
}

static Value builtin_entity_hud_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_SYMBOL(2);
    CHECK_ARG_SYMBOL(3);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_NAME_VALID(1);

    Value target = args[0];
    Value name = args[1];
    Value type = args[2];
    Value anchor = args[3];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    HUDElement element;

    element.setType(hudElementTypeFromString(type.s));
    element.setAnchor(hudElementAnchorFromString(anchor.s));

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_hud_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->removeHUDElement(name.s);

    return Value();
}

static Value builtin_entity_hud_position(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_VEC2(2);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];
    Value value = args[2];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    auto it = entity->hudElements().find(name.s);

    if (it == entity->hudElements().end())
    {
        throw toValue("Target HUD element '" + name.s + "' not found.");
    }

    HUDElement element = it->second;

    element.setPosition(fromValue<vec2>(value));

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_hud_color(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_VEC3(2);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];
    Value value = args[2];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    auto it = entity->hudElements().find(name.s);

    if (it == entity->hudElements().end())
    {
        throw toValue("Target HUD element '" + name.s + "' not found.");
    }

    HUDElement element = it->second;

    element.setColor(fromValue<vec3>(value));

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_hud_line(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_FLOAT_GREATER_EQUAL(2, 0);
    CHECK_ARG_FLOAT_GREATER_EQUAL(3, 0);

    Value target = args[0];
    Value name = args[1];
    Value length = args[2];
    Value rotation = args[3];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    auto it = entity->hudElements().find(name.s);

    if (it == entity->hudElements().end())
    {
        throw toValue("Target HUD element '" + name.s + "' not found.");
    }

    HUDElement element = it->second;

    element.setType(HUD_Element_Line);

    auto& data = element.get<HUDElementLine>();
    data.length = length.number<f64>();
    data.rotation = rotation.number<f64>();

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_hud_text(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_STRING(2);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];
    Value name = args[1];
    Value text = args[2];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    auto it = entity->hudElements().find(name.s);

    if (it == entity->hudElements().end())
    {
        throw toValue("Target HUD element '" + name.s + "' not found.");
    }

    HUDElement element = it->second;

    element.setType(HUD_Element_Text);

    auto& data = element.get<HUDElementText>();
    data.text = text.s;

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_hud_bar(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_INTEGER(4);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_FLOAT_GREATER_EQUAL(2, 0);
    CHECK_ARG_FLOAT_GREATER_EQUAL(3, 0);

    Value target = args[0];
    Value name = args[1];
    Value length = args[2];
    Value fraction = args[3];
    Value rightToLeft = args[4];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    auto it = entity->hudElements().find(name.s);

    if (it == entity->hudElements().end())
    {
        throw toValue("Target HUD element '" + name.s + "' not found.");
    }

    HUDElement element = it->second;

    element.setType(HUD_Element_Bar);

    auto& data = element.get<HUDElementBar>();
    data.length = length.number<f64>();
    data.fraction = fraction.number<f64>();
    data.rightToLeft = rightToLeft.i;

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_hud_symbol(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_SYMBOL(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_FLOAT_GREATER_EQUAL(2, 0);
    CHECK_ARG_FLOAT_GREATER_EQUAL(3, 0);

    Value target = args[0];
    Value name = args[1];
    Value symbol = args[2];
    Value size = args[3];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    auto it = entity->hudElements().find(name.s);

    if (it == entity->hudElements().end())
    {
        throw toValue("Target HUD element '" + name.s + "' not found.");
    }

    HUDElement element = it->second;

    element.setType(HUD_Element_Symbol);

    auto& data = element.get<HUDElementSymbol>();
    data.type = symbolTypeFromString(symbol.s);
    data.size = size.number<f64>();

    entity->addHUDElement(name.s, element);

    return Value();
}

static Value builtin_entity_field_of_view(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->fieldOfView());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);

        Value value = args[1];

        entity->setFieldOfView(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_mouse_lock(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setMouseLocked(true);

    return Value();
}

static Value builtin_entity_mouse_unlock(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    entity->setMouseLocked(false);

    return Value();
}

static Value builtin_entity_mouse_locked_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value state = args[1];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    entity->setMouseLocked(state.i);

    return Value();
}

static Value builtin_entity_mouse_locked_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    return toValue(entity->mouseLocked());
}

static Value builtin_entity_voice_volume(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->voiceVolume());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);

        Value value = args[1];

        entity->setVoiceVolume(value.number<f64>());

        return Value();
    }
}

static Value builtin_entity_tint_color(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->tintColor());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_VEC3(1);

        Value value = args[1];

        entity->setTintColor(fromValue<vec3>(value));

        return Value();
    }
}

static Value builtin_entity_tint_strength(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_HANDLE_VALID(0);

    Value target = args[0];

    Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);

    if (args.size() == 1)
    {
        return toValue(entity->tintStrength());
    }
    else
    {
        CHECK_USER_AUTH;
        CHECK_USER_ALLOWED(*entity);
        CHECK_ARG_NUMBER(1);

        Value value = args[1];

        entity->setTintStrength(value.number<f64>());

        return Value();
    }
}

#if defined(LMOD_SERVER)
static Value builtin_entity_haptic(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);
    CHECK_ARG_SYMBOL(1);
    CHECK_ARG_NUMBER(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_NUMBER(4);
    CHECK_ARG_HANDLE_VALID(0);
    CHECK_ARG_FLOAT_GREATER(2, 0);
    CHECK_ARG_FLOAT_GREATER(3, 0);
    CHECK_ARG_FLOAT_GREATER(4, 0);

    Value target = args[0];
    Value targetType = args[1];
    Value duration = args[2];
    Value frequency = args[3];
    Value amplitude = args[4];

    const Entity* entity = context->game->get(target.id);

    CHECK_ENTITY_EXISTS(entity);
    CHECK_USER_ALLOWED(*entity);

    if (!entity->attachedUserID())
    {
        return Value();
    }

    User* targetUser = context->game->getUser(entity->attachedUserID());

    CHECK_USER_EXISTS(targetUser);

    targetUser->addHaptic({
        hapticTargetFromString(targetType.s),
        static_cast<u32>(duration.number<f64>() * 1000),
        frequency.number<f64>(),
        amplitude.number<f64>()
    });

    return Value();
}

static Value builtin_text_signal(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value state = args[1];

    TextSignalRequest signal;
    signal.state = state.i != 0;

    for (User* user : users)
    {
        user->requestText(TextRequest(TextID(), signal), context->currentUser()->id());
    }

    return Value();
}

static Value builtin_text_titlecard(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 3, 4 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value subtitle = args[2];

    optional<Lambda> lambda;

    if (args.size() == 4)
    {
        CHECK_ARG_LAMBDA(3);

        Value value = args[3];

        lambda = value.lambda;
    }

    TextTitlecardRequest titlecard;
    titlecard.title = title.s;
    titlecard.subtitle = subtitle.s;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), titlecard), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_timeout(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);
    CHECK_ARG_NUMBER(3);
    CHECK_ARG_FLOAT_GREATER(3, 0);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value text = args[2];
    Value duration = args[3];

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    TextTimeoutRequest timeout;
    timeout.title = title.s;
    timeout.text = text.s;
    timeout.timeout = duration.number<f64>();

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), timeout), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_unary(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 3, 4 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value text = args[2];

    optional<Lambda> lambda;

    if (args.size() == 4)
    {
        CHECK_ARG_LAMBDA(3);

        Value value = args[3];

        lambda = value.lambda;
    }

    TextUnaryRequest unary;
    unary.title = title.s;
    unary.text = text.s;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), unary), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_binary(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 3, 4 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value text = args[2];

    optional<Lambda> lambda;

    if (args.size() == 4)
    {
        CHECK_ARG_LAMBDA(3);

        Value value = args[3];

        lambda = value.lambda;
    }

    TextBinaryRequest binary;
    binary.title = title.s;
    binary.text = text.s;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), binary), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_options(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value text = args[2];
    Value vOptions = args[3];

    if (!isListOf(vOptions, Value_String))
    {
        throw toValue("Expected list of strings as argument 4 to 'text-options', got '" + vOptions.toString() + "'.");
    }

    if (vOptions.l.empty())
    {
        throw toValue("Expected at least 1 string in argument 4 to 'text-options'.");
    }

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    set<string> optionNames;

    for (const Value& item : vOptions.l)
    {
        optionNames.insert(item.s);
    }

    if (optionNames.size() != vOptions.l.size())
    {
        throw toValue("Expected strings supplied as argument 4 to 'text-options' to be unique.");
    }

    TextOptionsRequest options;
    options.title = title.s;
    options.text = text.s;
    options.options = optionNames;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), options), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_choose(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(0);
    CHECK_ARG_INTEGER(3);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value vOptions = args[2];
    Value count = args[3];

    if (!isListOf(vOptions, Value_String))
    {
        throw toValue("Expected list of strings as argument 3 to 'text-choose', got '" + vOptions.toString() + "'.");
    }

    if (vOptions.l.empty())
    {
        throw toValue("Expected at least 1 string in argument 3 to 'text-choose'.");
    }

    if (count.i <= 0 || static_cast<size_t>(count.i) >= vOptions.l.size())
    {
        throw toValue("Expected an integer between 1 and option count - 1 as argument 4 to 'text-choose'.");
    }

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    set<string> optionNames;

    for (const Value& option : vOptions.l)
    {
        optionNames.insert(option.s);
    }

    if (optionNames.size() != vOptions.l.size())
    {
        throw toValue("Expected strings supplied as argument 3 to 'text-choose' to be unique.");
    }

    TextChooseRequest choose;
    choose.title = title.s;
    choose.options = optionNames;
    choose.count = count.i;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), choose), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_assign(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value keys = args[2];
    Value values = args[3];

    if (!isListOf(keys, Value_String))
    {
        throw toValue("Expected list of strings as argument 3 to 'text-assign', got '" + keys.toString() + "'.");
    }

    if (keys.l.empty())
    {
        throw toValue("Expected at least 1 string in argument 3 to 'text-assign'.");
    }

    if (!isListOf(values, Value_String))
    {
        throw toValue("Expected list of strings as argument 4 to 'text-assign', got '" + values.toString() + "'.");
    }

    if (values.l.empty())
    {
        throw toValue("Expected at least 1 string in argument 4 to 'text-assign'.");
    }

    if (keys.l.size() != values.l.size())
    {
        throw toValue("Expected arguments 3 and 4 to 'text-assign' to be of the same length.");
    }

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    set<string> keyNames;

    for (const Value& key : keys.l)
    {
        keyNames.insert(key.s);
    }

    if (keyNames.size() != keys.l.size())
    {
        throw toValue("Expected strings supplied as argument 3 to 'text-assign' to be unique.");
    }

    set<string> valueNames;

    for (const Value& value : values.l)
    {
        valueNames.insert(value.s);
    }

    if (valueNames.size() != values.l.size())
    {
        throw toValue("Expected strings supplied as argument 4 to 'text-assign' to be unique.");
    }

    TextAssignValuesRequest assignValues;
    assignValues.title = title.s;
    assignValues.keys = keyNames;
    assignValues.values = valueNames;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), assignValues), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_point_buy(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_INTEGER(3);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value prices = args[2];
    Value points = args[3];

    if (!isTableOf(prices, Value_String, Value_Integer))
    {
        throw toValue("Expected list of string-integer pairs as argument 3 to 'text-point-buy', got '" + prices.toString() + "'.");
    }

    if (prices.l.empty())
    {
        throw toValue("Expected at least 1 string-integer pair in argument 3 to 'text-point-buy'.");
    }

    if (points.i <= 0)
    {
        throw toValue("Expected an integer greater than 0 as argument 4 to 'text-point-buy'.");
    }

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    map<string, u64> priceMapping;

    for (const Value& pair : prices.l)
    {
        if (pair.l[1].i <= 0)
        {
            throw toValue("Expected integers greater than 0 in argument 3 to 'text-point-buy'.");
        }

        priceMapping.insert({
            pair.l[0].s,
            pair.l[1].i
        });
    }

    TextSpendPointsRequest spendPoints;
    spendPoints.title = title.s;
    spendPoints.prices = priceMapping;
    spendPoints.points = points.i;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), spendPoints), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_point_assign(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_INTEGER(3);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value keys = args[2];
    Value points = args[3];

    if (!isListOf(keys, Value_String))
    {
        throw toValue("Expected list of strings as argument 3 to 'text-point-assign', got '" + keys.toString() + "'.");
    }

    if (keys.l.empty())
    {
        throw toValue("Expected at least 1 strings in argument 3 to 'text-point-assign'.");
    }

    if (points.i <= 0)
    {
        throw toValue("Expected an integer greater than 0 as argument 4 to 'text-point-assign'.");
    }

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    set<string> keyNames;

    for (const Value& key : keys.l)
    {
        keyNames.insert(key.s);
    }

    if (keyNames.size() != keys.l.size())
    {
        throw toValue("Expected strings supplied as argument 3 to 'text-point-assign' to be unique.");
    }

    TextAssignPointsRequest assignPoints;
    assignPoints.title = title.s;
    assignPoints.keys = keyNames;
    assignPoints.points = points.i;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), assignPoints), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_vote(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 4, 5 });
    CHECK_ARG_STRING(1);
    CHECK_ARG_INTEGER(2);
    CHECK_ARG_FLOAT_GREATER(3, 0);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value options = args[2];
    Value votes = args[3];

    if (!isListOf(options, Value_String))
    {
        throw toValue("Expected list of strings as argument 3 to 'text-vote', got '" + options.toString() + "'.");
    }

    if (options.l.empty())
    {
        throw toValue("Expected at least 1 string in argument to 'text-vote'.");
    }

    if (votes.i <= 0 || static_cast<size_t>(votes.i) >= options.l.size())
    {
        throw toValue("Expected an integer between 1 and option count - 1 as argument 4 to 'text-vote'.");
    }

    optional<Lambda> lambda;

    if (args.size() == 5)
    {
        CHECK_ARG_LAMBDA(4);

        Value value = args[4];

        lambda = value.lambda;
    }

    vector<string> optionNames;

    for (const Value& option : options.l)
    {
        optionNames.push_back(option.s);
    }

    TextVoteRequest vote;
    vote.title = title.s;
    vote.options = optionNames;
    vote.votes = votes;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), vote), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_choose_major(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 3, 4 });

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value options = args[2];

    if (options.type != Value_List)
    {
        throw toValue("Expected list of length 3 lists of strings as argument 3 to 'text-choose-major', got '" + options.toString() + "'.");
    }

    if (options.l.empty())
    {
        throw toValue("Expected at least 1 list in argument 3 to 'text-choose-major'.");
    }

    vector<tuple<string, string, string>> optionObjects;

    for (const Value& i : options.l)
    {
        if (i.type != Value_List ||
            i.l.size() != 3 ||
            i.l[0].type != Value_String ||
            i.l[1].type != Value_String ||
            i.l[2].type != Value_String
        )
        {
            throw toValue("Expected list of length 3 lists of strings as argument 3 to 'text-choose-major', got '" + options.toString() + "'.");
        }

        optionObjects.push_back({
            i.l[0].s,
            i.l[1].s,
            i.l[2].s
        });
    }

    optional<Lambda> lambda;

    if (args.size() == 4)
    {
        CHECK_ARG_LAMBDA(3);

        Value value = args[3];

        lambda = value.lambda;
    }

    TextChooseMajorRequest chooseMajor;
    chooseMajor.title = title.s;
    chooseMajor.options = optionObjects;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), chooseMajor), context->currentUser()->id(), lambda);
    }

    return Value();
}

static Value builtin_text_knowledge(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(1);
    CHECK_ARG_STRING(2);
    CHECK_ARG_STRING(3);

    vector<User*> users = PARSE_USER_ARG(0);
    Value title = args[1];
    Value subtitle = args[2];
    Value text = args[3];

    TextKnowledgeRequest knowledge;
    knowledge.title = title.s;
    knowledge.subtitle = subtitle.s;
    knowledge.text = text.s;

    for (User* user : users)
    {
        user->requestText(TextRequest(context->game->nextTextID(), knowledge), context->currentUser()->id());
    }

    return Value();
}

static Value builtin_text_clear(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    vector<User*> users = PARSE_USER_ARG(0);
    Value arg = args[1];

    TextClearRequest clear;

    switch (arg.type)
    {
    case Value_String :
        clear.type = Text_Clear_Specific_Knowledge;
        clear.title = arg.s;
        break;
    case Value_Integer :
        clear.type = arg.number<i64>() == 0 ? Text_Clear_All_But_Knowledge : Text_Clear_All;
        break;
    default :
        throw toValue("Expected integer or string as argument 2 to 'text-clear', got '" + arg.toString() + "'.");
    }

    for (User* user : users)
    {
        user->requestText(TextRequest(TextID(), clear), context->currentUser()->id());
    }

    return Value();
}
#endif

static Value builtin_team_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value result;
    result.type = Value_List;
    result.l.reserve(context->game->teams().size());

    for (const auto& [ name, team ] : context->game->teams())
    {
        result.l.push_back(symbol(name));
    }

    return result;
}

static Value builtin_team_add(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_NAME_VALID(0);

    Value name = args[0];

    if (context->game->getTeam(name.s))
    {
        throw toValue("Team '" + name.s + "' already exists.");
    }

    context->game->addTeam(name.s);

    return Value();
}

static Value builtin_team_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    Team* team = context->game->getTeam(name.s);

    if (!team)
    {
        throw toValue("Target team in '" + functionName + "' not found.");
    }

    context->game->removeTeam(name.s);

    return Value();
}

static Value builtin_team_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    Team* team = context->game->getTeam(name.s);

    if (!team)
    {
        throw toValue("Target team in '" + functionName + "' not found.");
    }

    Value result;
    result.type = Value_List;
    result.l.reserve(team->userIDs.size());

    for (UserID userID : team->userIDs)
    {
        Value item;
        item.type = Value_Integer;
        item.i = userID.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_team_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];
    Value users = args[1];

    if (!isListOf(users, Value_Integer))
    {
        throw toValue("Expected list of integers as argument 2 to 'team-set', got '" + users.toString() + "'.");
    }

    Team* team = context->game->getTeam(name.s);

    if (!team)
    {
        throw toValue("Target team in '" + functionName + "' not found.");
    }

    set<UserID> userIDs;

    for (const Value& item : users.l)
    {
        if (!context->game->getUser(UserID(item.i)))
        {
            throw toValue("Target user in '" + functionName + "' not found.");
        }

        userIDs.insert(UserID(item.i));
    }

    context->game->updateTeam(name.s, userIDs);

    return Value();
}

static Value builtin_user_one(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        if (user.name() != name.s)
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_not_one(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        if (user.name() == name.s)
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_group(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value group = args[0];

    if (group.type != Value_List)
    {
        throw toValue("Expected list of symbols as argument 1 to 'user-group', got '" + group.toString() + "'.");
    }

    vector<string> names;
    names.reserve(group.l.size());

    for (const Value& item : group.l)
    {
        if (item.type != Value_Symbol)
        {
            throw toValue("Expected list of symbols as argument 1 to 'user-group', got '" + group.toString() + "'.");
        }

        names.push_back(item.s);
    }

    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        if (find(names.begin(), names.end(), user.name()) == names.end())
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_not_group(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value group = args[0];

    if (group.type != Value_List)
    {
        throw toValue("Expected list of symbols as argument 1 to 'user-not-group', got '" + group.toString() + "'.");
    }

    vector<string> names;
    names.reserve(group.l.size());

    for (const Value& item : group.l)
    {
        if (item.type != Value_Symbol)
        {
            throw toValue("Expected list of symbols as argument 1 to 'user-not-group', got '" + group.toString() + "'.");
        }

        names.push_back(item.s);
    }

    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        if (find(names.begin(), names.end(), user.name()) != names.end())
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_all(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_near(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(0);
    CHECK_ARG_NUMBER(1);

    Value positionValue = args[0];
    Value distanceValue = args[1];

    vec3 position = fromValue<vec3>(positionValue);
    f64 distance = distanceValue.number<f64>();

    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        if (user.viewer().position().distance(position) > distance)
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_world(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    Value result;
    result.type = Value_List;

    for (const auto& [ id, user ] : context->game->users())
    {
        if (!user.active())
        {
            continue;
        }

        if (user.world() != name.s)
        {
            continue;
        }

        Value item;
        item.type = Value_Integer;
        item.i = id.value();

        result.l.push_back(item);
    }

    return result;
}

static Value builtin_user_team(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);

    Value name = args[0];

    Team* team = context->game->getTeam(name.s);

    if (!team)
    {
        throw toValue("Target team in '" + functionName + "' not found.");
    }

    Value result;
    result.type = Value_List;
    result.l.reserve(team->userIDs.size());

    for (UserID userID : team->userIDs)
    {
        result.l.push_back(toValue(userID));
    }

    return result;
}

static Value builtin_user_active(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value target = args[0];

    if (target.type == Value_Integer)
    {
        User* user = context->game->getUser(UserID(target.i));

        CHECK_USER_EXISTS(user);

        return toValue(user->active());
    }
    else if (target.type == Value_List)
    {
        Value result;
        result.type = Value_List;

        for (const Value& target : target.l)
        {
            User* user = context->game->getUser(UserID(target.i));

            CHECK_USER_EXISTS(user);

            Value resultItem;
            resultItem.type = Value_Integer;
            resultItem.i = user->active();

            result.l.push_back(resultItem);
        }

        return result;
    }
    else
    {
        throw toValue("Expected integer or list of integers as argument 1 to 'user-active?', got '" + target.toString() + "'.");
    }
}

static Value builtin_user_attached_entity(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    vector<User*> users = PARSE_USER_ARG(0);

    if (users.size() == 1)
    {
        Value handle;
        handle.type = Value_Handle;
        handle.id = users.front()->entityID();

        return handle;
    }
    else
    {
        vector<Value> handles;

        for (User* user : users)
        {
            Value handle;
            handle.type = Value_Handle;
            handle.id = user->entityID();

            handles.push_back(handle);
        }

        return Value(handles);
    }
}

static Value builtin_user_remove(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    vector<User*> users = PARSE_USER_ARG(0);

    for (User* user : users)
    {
        context->game->removeUser(user);
    }

    return Value();
}

static Value builtin_user_kick(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    vector<User*> users = PARSE_USER_ARG(0);

#if defined(LMOD_SERVER)
    for (User* user : users)
    {
        context->game->kickUser(context->currentUser(), user);
    }
#endif

    return Value();
}

static Value builtin_user_shift(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value name = args[1];

    for (User* user : users)
    {
        user->shift(name.s);
    }

    return Value();
}

static Value builtin_user_move(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_VEC3(1);
    CHECK_ARG_QUATERNION(2);

    vector<User*> users = PARSE_USER_ARG(0);
    Value position = args[1];
    Value rotation = args[2];

    vec3 newPosition = fromValue<vec3>(position);
    quaternion newRotation = fromValue<quaternion>(rotation);

    mat4 newTransform = { newPosition, newRotation, vec3(1) };

    for (User* user : users)
    {
        userMove(user, context->game->get(user->entityID()), newTransform);
    }

    return Value();
}

static Value builtin_user_color(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });

    vector<User*> users = PARSE_USER_ARG(0);

    if (args.size() == 1)
    {
        if (users.size() == 1)
        {
            return toValue(users.front()->color());
        }
        else
        {
            Value result;
            result.type = Value_List;

            for (User* user : users)
            {
                result.l.push_back(toValue(user->color()));
            }

            return result;
        }
    }
    else
    {
        Value color = args[1];

        if (!isVec3(color))
        {
            throw toValue("Expected length 3 list of integers or floats as argument 2 to 'user-color', got '" + color.toString() + "'.");
        }

        for (User* user : users)
        {
            user->setColor(fromValue<vec3>(color));
        }

        return Value();
    }
}

static Value builtin_user_teams(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });

    vector<User*> users = PARSE_USER_ARG(0);

    if (args.size() == 1)
    {
        if (users.size() == 1)
        {
            Value resultItem;
            resultItem.type = Value_List;
            resultItem.l.reserve(users.front()->teams().size());

            for (const string& team : users.front()->teams())
            {
                resultItem.l.push_back(toValue(team));
            }

            return resultItem;
        }
        else
        {
            Value result;
            result.type = Value_List;
            result.l.reserve(users.size());

            for (User* user : users)
            {
                Value resultItem;
                resultItem.type = Value_List;
                resultItem.l.reserve(user->teams().size());

                for (const string& team : user->teams())
                {
                    resultItem.l.push_back(toValue(team));
                }

                result.l.push_back(resultItem);
            }

            return result;
        }
    }
    else
    {
        Value names = args[1];

        if (!isListOf(names, Value_Symbol))
        {
            throw toValue("Expected list of symbols as argument 2 to 'user-teams', got '" + names.toString() + "'.");
        }

        set<string> teams;

        for (const Value& name : names.l)
        {
            if (!context->game->getTeam(name.s))
            {
                throw toValue("Target team in '" + functionName + "' not found.");
            }

            teams.insert(name.s);
        }

        for (User* user : users)
        {
            context->game->updateUserTeams(user, teams);
        }

        return Value();
    }
}

#if defined(LMOD_SERVER)
static Value builtin_user_attach(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_HANDLE(1);

    Value id = args[0];
    Value handle = args[1];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    if (user->entityID())
    {
        throw toValue("Target user in '" + functionName + "' is already attached.");
    }

    Entity* entity = context->game->get(handle.id);

    CHECK_ENTITY_EXISTS(entity);

    if (entity->attachedUserID())
    {
        throw toValue("Target entity in '" + functionName + "' already has a user attached.");
    }

    if (!entity->active())
    {
        throw toValue("Target entity in '" + functionName + "' is not active.");
    }

    if (!entity->host())
    {
        throw toValue("Target entity in '" + functionName + "' is not marked as a host.");
    }

    context->game->attach(user, entity);

    return Value();
}

static Value builtin_user_detach(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    if (!user->entityID())
    {
        throw toValue("Target user in '" + functionName + "' is not attached.");
    }

    // Ignore attachment lock, we're an admin if we're here

    context->game->detach(user);

    return Value();
}
#endif

static Value builtin_user_magnitude(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    User* target = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(target);

    return toValue(target->magnitude());
}

static Value builtin_user_advantage(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    User* target = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(target);

    return toValue(target->advantage());
}

static Value builtin_user_doom(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    User* target = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(target);

    return toValue(target->doom());
}

static Value builtin_user_goto(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 3, 4 });
    CHECK_ARG_VEC3(1);
    CHECK_ARG_NUMBER(2);

    vector<User*> users = PARSE_USER_ARG(0);
    Value vPosition = args[1];
    Value vRadius = args[2];

    vec3 position = fromValue<vec3>(vPosition);
    f64 radius = vRadius.number<f64>();
    vec3 up = upDir;

    string worldName;

    for (User* user : users)
    {
        if (!worldName.empty() && user->world() != worldName)
        {
            throw toValue("Target users in '" + functionName + "' must all be in the same world.");
        }

        worldName = user->world();
    }

    if (args.size() == 4)
    {
        CHECK_ARG_VEC3(3);

        up = fromValue<vec3>(args[3]);
    }

    vector<tuple<User*, Entity*>> usersWithHosts;
    usersWithHosts.reserve(users.size());

    for (User* user : users)
    {
        usersWithHosts.push_back({ user, context->game->get(user->entityID()) });
    }

    userGoto(usersWithHosts, position, radius, up);

    return Value();
}

#if defined(LMOD_SERVER)
static Value builtin_user_tether(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 2, 3 });
    CHECK_ARG_INTEGER(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value state = args[1];

    Value distance;

    string worldName;

    for (User* user : users)
    {
        if (!worldName.empty() && user->world() != worldName)
        {
            throw toValue("Target users in '" + functionName + "' must all be in the same world.");
        }

        worldName = user->world();
    }

    if (args.size() == 3)
    {
        CHECK_ARG_NUMBER(2);
        CHECK_ARG_FLOAT_GREATER(2, 0);

        distance = args[2];
    }
    else if (state.i && args.size() == 2)
    {
        throw toValue("If only 2 arguments are supplied to 'user-tether' then argument 2 must be 0.");
    }

    set<UserID> userIDs;

    for (User* user : users)
    {
        userIDs.insert(user->id());
    }

    for (User* user : users)
    {
        if (!state.i)
        {
            user->untether();
        }
        else
        {
            user->tether(distance.number<f64>(), userIDs);
        }
    }

    return Value();
}

static Value builtin_user_confine(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 2, 4 });
    CHECK_ARG_INTEGER(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value state = args[1];

    Value position;
    Value distance;

    string worldName;

    for (User* user : users)
    {
        if (!worldName.empty() && user->world() != worldName)
        {
            throw toValue("Target users in '" + functionName + "' must all be in the same world.");
        }

        worldName = user->world();
    }

    if (args.size() == 4)
    {
        CHECK_ARG_VEC3(2);
        CHECK_ARG_NUMBER(3);
        CHECK_ARG_FLOAT_GREATER(3, 0);

        position = args[2];
        distance = args[3];
    }
    else if (state.i && args.size() == 2)
    {
        throw runtime_error("If only 2 arguments are supplied to 'user-confine' then argument 2 must be 0.");
    }

    for (User* user : users)
    {
        if (state.i)
        {
            user->confine(
                fromValue<vec3>(position),
                distance.number<f64>()
            );
        }
        else
        {
            user->unconfine();
        }
    }

    return Value();
}

static Value builtin_user_fade(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value state = args[1];

    for (User* user : users)
    {
        user->setFade(state.i);
    }

    return Value();
}

static Value builtin_user_wait(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value state = args[1];

    for (User* user : users)
    {
        user->setWait(state.i);
    }

    return Value();
}

static Value builtin_chat_send(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);

    Value message = args[0];

    context->game->sendChatMessage(context->currentUser(), message.s);

    return Value();
}

static Value builtin_chat_send_team(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_SYMBOL(0);
    CHECK_ARG_STRING(1);

    Value target = args[0];
    Value message = args[1];

    Team* team = context->game->getTeam(target.s);

    if (!team)
    {
        throw toValue("Target team in '" + functionName + "' not found.");
    }

    context->game->sendTeamChatMessage(context->currentUser(), team, message.s);

    return Value();
}

static Value builtin_chat_send_user(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(1);

    vector<User*> users = PARSE_USER_ARG(0);
    Value message = args[1];

    for (User* user : users)
    {
        context->game->sendDirectChatMessage(context->currentUser(), user, message.s);
    }

    return Value();
}

static Value builtin_voice_channel(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1 });

    if (args.size() == 0)
    {
        if (context->currentUser()->voiceChannel().empty())
        {
            return Value();
        }
        else
        {
            return symbol(context->currentUser()->voiceChannel());
        }
    }
    else
    {
        string team;

        if (args[0].type == Value_Symbol)
        {
            team = args[0].s;

            if (!context->currentUser()->teams().contains(team))
            {
                throw toValue("Target team in '" + functionName + "' not found.");
            }
        }
        else if (args[0].type == Value_Void)
        {
            team = "";
        }
        else
        {
            throw toValue("Expected symbol or void as argument 1 to 'voice-channel', got '" + args[0].toString() + "'.");
        }

        context->currentUser()->setVoiceChannel(team);
        context->currentUser()->pushMessage(
            team.empty()
                ? Message(Message_Log, "", "you-are-now-in-the-global-voice-channel")
                : Message(Message_Log, "", "you-are-now-in-the-team-voice-channel", { team })
        );

        return Value();
    }
}
#endif

static Value builtin_attachments_locked_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value state = args[0];

    state.i
        ? context->game->lockAttachments(context->currentUser())
        : context->game->unlockAttachments(context->currentUser());

    return Value();
}

static Value builtin_attachments_locked_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(context->game->attachmentsLocked());
}

static Value builtin_attachments_lock(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    context->game->lockAttachments(context->currentUser());

    return Value();
}

static Value builtin_attachments_unlock(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    context->game->unlockAttachments(context->currentUser());

    return Value();
}

static Value builtin_magnitude_global(const string& functionName, ScriptContext* context, const vector<Value>& args)
{

    return toValue(context->game->magnitude());
}

static Value builtin_magnitude_with(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_INTEGER(1);

    Value name = args[0];
    Value target = args[1];
    Value value = args[2];

    i64 targetMagnitude = target.i;

    if (targetMagnitude < minMagnitude || targetMagnitude > maxMagnitude)
    {
        throw toValue("Expected integer between " + to_string(minMagnitude) +  " and " + to_string(maxMagnitude) + " inclusive as magnitude value, got " + to_string(targetMagnitude) + ".");
    }

    u32 currentMagnitude = context->game->magnitude();

    if (currentMagnitude < targetMagnitude)
    {
#if defined(LMOD_SERVER)
        context->game->log("action-blocked-by-magnitude-control", { name.s, to_string(targetMagnitude), to_string(currentMagnitude) });
#endif
        return Value();
    }

    return context->run(args[2]);
}

#if defined(LMOD_SERVER)
static Value builtin_magnitude_limit(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1 });

    if (args.size() == 0)
    {
        return toValue(context->game->magnitudeLimit());
    }
    else
    {
        CHECK_ARG_INTEGER(0);

        Value tMValue = args[0];

        i64 targetMagnitude = tMValue.i;

        if (targetMagnitude < minMagnitude || targetMagnitude > maxMagnitude)
        {
            throw toValue("Expected integer between " + to_string(minMagnitude) +  " and " + to_string(maxMagnitude) + " inclusive as magnitude value in '" + functionName + "', got " + to_string(targetMagnitude) + ".");
        }

        context->game->setMagnitudeLimit(context->currentUser(), targetMagnitude);
    }

    return Value();
}

static Value builtin_magnitude(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1 });

    if (args.empty())
    {
        return toValue(context->currentUser()->magnitude());
    }
    else
    {
        CHECK_ARG_INTEGER(0);

        Value magnitude = args[0];

        context->game->magnitude(context->currentUser(), magnitude.i);

        return Value();
    }
}
#endif

static Value builtin_advantage_apply(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_NUMBER(1);

    Value id = args[0];
    Value value = args[1];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    if (value.type == Value_Integer)
    {
        value.i *= pow(advantageMultiplier, user->advantage());

        return value;
    }
    else
    {
        value.f *= pow(advantageMultiplier, user->advantage());

        return value;
    }
}

static Value builtin_advantage_take(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_STRING(0);
    CHECK_ARG_INTEGER(1);
    CHECK_ARG_INTEGER(2);

    Value action = args[0];
    Value id = args[1];
    Value value = args[2];
    Value result = args[3];

    if (value.i <= 0)
    {
        throw toValue("Expected an integer greater than 0 as argument 3 to 'advantage-take'.");
    }

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    if (!context->currentUser()->admin() && context->currentUser() != user)
    {
        throw toValue("Permission to execute 'advantage-take' denied.");
    }

    if (user->advantage() < value.i)
    {
#if defined(LMOD_SERVER)
        context->game->log("action-blocked-by-advantage-control", { action.s, to_string(value.i), to_string(user->advantage()) });
#endif
        return Value();
    }
    else
    {
        user->setAdvantage(user->advantage() - value.i);

        return context->run(result);
    }
}

#if defined(LMOD_SERVER)
static Value builtin_advantage_give(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);
    CHECK_ARG_INTEGER(1);

    Value id = args[0];
    Value value = args[1];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    user->setAdvantage(user->advantage() + value.i);

    return Value();
}
#endif

static Value builtin_advantage(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(context->currentUser()->advantage());
}

static Value builtin_doom_flag(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    user->setDoom(true);

    return Value();
}

static Value builtin_doom_clear(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value id = args[0];

    User* user = context->game->getUser(UserID(id.i));

    CHECK_USER_EXISTS(user);

    user->setDoom(false);

    return Value();
}

static Value builtin_doom(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(context->currentUser()->doom());
}

#if defined(LMOD_SERVER)
static Value builtin_attach(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_HANDLE(0);

    Value handle = args[1];

    if (context->currentUser()->attached())
    {
        throw toValue("You must not be already attached when calling 'attach'.");
    }

    Entity* entity = context->game->get(handle.id);

    CHECK_ENTITY_EXISTS(entity);

    if (entity->attachedUserID())
    {
        throw toValue("Target entity in '" + functionName + "' already has a user attached.");
    }

    if (!entity->active())
    {
        throw toValue("Target entity in '" + functionName + "' is not active.");
    }

    if (!entity->host())
    {
        throw toValue("Target entity in '" + functionName + "' is not marked as a host.");
    }

    context->game->attach(context->currentUser(), entity);

    return Value();
}

static Value builtin_detach(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    if (!context->currentUser()->attached())
    {
        throw toValue("You must be attached when calling 'detach'.");
    }

    if (!context->currentUser()->admin() && context->game->attachmentsLocked())
    {
        throw toValue("Function 'detach' blocked, attachments are locked for non-admin users.");
    }

    context->game->detach(context->currentUser());

    return Value();
}

static Value builtin_ping(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    context->game->ping(context->currentUser());

    return Value();
}
#endif

static Value builtin_braking_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(0);

    Value state = args[0];

    state.i
        ? context->game->brake(context->currentUser())
        : context->game->unbrake(context->currentUser());

    return Value();
}

static Value builtin_braking_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(context->game->braking());
}

static Value builtin_brake(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    context->game->brake(context->currentUser());

    return Value();
}

static Value builtin_unbrake(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    context->game->unbrake(context->currentUser());

    return Value();
}

static Value builtin_splash_message(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 0, 1 });

    if (args.empty())
    {
        return toValue(context->game->splashMessage());
    }
    else
    {
        CHECK_ARG_STRING(0);

        Value message = args[0];

        context->game->splashMessage(message.s);
    }

    return Value();
}

#if defined(LMOD_SERVER)
static Value builtin_save(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    g_controller->setShouldSave();

    return Value();
}
#endif

const map<string, ScriptBuiltin> gamelib = {
    makeVariadicBuiltin("time-multiplier", builtin_time_multiplier),

#if defined(LMOD_SERVER)
    makeBuiltin("checkpoint-all", {}, builtin_checkpoint_all),
    makeBuiltin("checkpoint-add", { "name" }, builtin_checkpoint_add, Script_Builtin_Auth_Admin),
    makeBuiltin("checkpoint-remove", { "name" }, builtin_checkpoint_remove, Script_Builtin_Auth_Admin),
    makeBuiltin("checkpoint-revert", { "name" }, builtin_checkpoint_revert, Script_Builtin_Auth_Admin),
#elif defined(LMOD_CLIENT)
    clientSideStub("checkpoint-all", listValue()),
    clientSideStub("checkpoint-add"),
    clientSideStub("checkpoint-remove"),
    clientSideStub("checkpoint-revert"),
#endif

    makeBuiltin("world-all", {}, builtin_world_all),

    makeBuiltin("world-add", { "world-name" }, builtin_world_add, Script_Builtin_Auth_Admin),
    makeBuiltin("world-remove", { "world-name" }, builtin_world_remove, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("world-speed-of-sound", builtin_world_speed_of_sound),
    makeVariadicBuiltin("world-gravity", builtin_world_gravity),
    makeVariadicBuiltin("world-flow-velocity", builtin_world_flow_velocity),
    makeVariadicBuiltin("world-density", builtin_world_density),
    makeVariadicBuiltin("world-up", builtin_world_up),
    makeBuiltin("world-show", { "world-name" }, builtin_world_show, Script_Builtin_Auth_Admin),
    makeBuiltin("world-hide",  { "world-name" }, builtin_world_hide, Script_Builtin_Auth_Admin),
    makeBuiltin("world-shown", { "world-name", "state" }, builtin_world_shown_set, Script_Builtin_Auth_Admin),
    makeBuiltin("world-shown?",  { "world-name" }, builtin_world_shown_get),
    makeVariadicBuiltin("world-fog-distance", builtin_world_fog_distance),

    makeBuiltin("world-entity-find", { "world-name", "pattern" }, builtin_world_entity_find),
    makeBuiltin("world-entity-all", { "world-name" }, builtin_world_entity_all),
    makeBuiltin("world-entity-near", { "entity", "position", "range" }, builtin_world_entity_near),
    makeBuiltin("world-entity-ray", { "entity", "position", "direction", "range" }, builtin_world_entity_ray),
    makeBuiltin("world-entity-view", { "entity", "position", "direction", "angle", "range" }, builtin_world_entity_view),

    makeBuiltin("entity-add", { "entity-name" }, builtin_entity_add, Script_Builtin_Auth_User),
    makeBuiltin("entity-remove", { "entity" }, builtin_entity_remove, Script_Builtin_Auth_User),
    makeBuiltin("entity-copy", { "entity" }, builtin_entity_copy, Script_Builtin_Auth_User),
    makeBuiltin("entity-shift", { "entity", "world-name" }, builtin_entity_shift, Script_Builtin_Auth_User),

    makeBuiltin("entity-find", { "entity", "pattern" }, builtin_entity_find),
    makeBuiltin("entity-all", { "entity" }, builtin_entity_all),
    makeBuiltin("entity-near", { "entity", "position", "range" }, builtin_entity_near),
    makeBuiltin("entity-ray", { "entity", "position", "direction", "range" }, builtin_entity_ray),
    makeBuiltin("entity-view", { "entity", "position", "direction", "angle", "range" }, builtin_entity_view),

    makeBuiltin("entity-transfer", { "entity", "user-id" }, builtin_entity_transfer, Script_Builtin_Auth_User),
    makeBuiltin("entity-take", { "entity" }, builtin_entity_take, Script_Builtin_Auth_User),
    makeBuiltin("entity-discard", { "entity" }, builtin_entity_discard, Script_Builtin_Auth_User),
    makeBuiltin("entity-owner", { "entity" }, builtin_entity_owner),
    makeBuiltin("entity-share", { "entity" }, builtin_entity_share, Script_Builtin_Auth_User),
    makeBuiltin("entity-unshare", { "entity" }, builtin_entity_unshare, Script_Builtin_Auth_User),
    makeBuiltin("entity-shared", { "entity", "state" }, builtin_entity_shared_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-shared?", { "entity" }, builtin_entity_shared_get),
#if defined(LMOD_SERVER)
    makeBuiltin("entity-attach", { "entity", "user-id" }, builtin_entity_attach, Script_Builtin_Auth_Admin),
    makeBuiltin("entity-detach", { "entity" }, builtin_entity_detach, Script_Builtin_Auth_Admin),
#elif defined(LMOD_CLIENT)
    clientSideStub("entity-attach"),
    clientSideStub("entity-detach"),
    clientSideStub("entity-attach-other"),
    clientSideStub("entity-detach-other"),
#endif
    makeBuiltin("entity-attached-user", { "entity" }, builtin_entity_attached_user),

    makeVariadicBuiltin("entity-global-position", builtin_entity_global_position),
    makeVariadicBuiltin("entity-global-rotation", builtin_entity_global_rotation),
    makeVariadicBuiltin("entity-global-scale", builtin_entity_global_scale),
    makeVariadicBuiltin("entity-local-position", builtin_entity_local_position),
    makeVariadicBuiltin("entity-local-rotation", builtin_entity_local_rotation),
    makeVariadicBuiltin("entity-local-scale", builtin_entity_local_scale),
    makeVariadicBuiltin("entity-linear-velocity", builtin_entity_linear_velocity),
    makeVariadicBuiltin("entity-angular-velocity", builtin_entity_angular_velocity),

    makeBuiltin("entity-world", { "entity" }, builtin_entity_world),
    makeBuiltin("entity-name", { "entity" }, builtin_entity_name),
    makeBuiltin("entity-play", { "entity" }, builtin_entity_play, Script_Builtin_Auth_User),
    makeBuiltin("entity-pause", { "entity" }, builtin_entity_pause, Script_Builtin_Auth_User),
    makeBuiltin("entity-store", { "entity" }, builtin_entity_store, Script_Builtin_Auth_User),
    makeBuiltin("entity-reset", { "entity" }, builtin_entity_reset, Script_Builtin_Auth_User),
    makeBuiltin("entity-active", { "entity", "state" }, builtin_entity_active_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-active?", { "entity" }, builtin_entity_active_get),
    makeBuiltin("entity-parent", { "entity" }, builtin_entity_parent),
    makeBuiltin("entity-parent-set", { "entity", "parent" }, builtin_entity_parent_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-parent-clear", { "entity" }, builtin_entity_parent_clear, Script_Builtin_Auth_User),
    makeBuiltin("entity-children", { "entity" }, builtin_entity_children),

    makeVariadicBuiltin("entity-mass", builtin_entity_mass),
    makeVariadicBuiltin("entity-drag", builtin_entity_drag),
    makeVariadicBuiltin("entity-buoyancy", builtin_entity_buoyancy),
    makeVariadicBuiltin("entity-lift", builtin_entity_lift),
    makeVariadicBuiltin("entity-magnetism", builtin_entity_magnetism),
    makeVariadicBuiltin("entity-friction", builtin_entity_friction),
    makeVariadicBuiltin("entity-restitution", builtin_entity_restitution),

    makeBuiltin("entity-sample-play", { "entity", "name", "volume", "directional", "angle", "anchor-id" }, builtin_entity_sample_play, Script_Builtin_Auth_User),
    makeBuiltin("entity-sample-stop", { "entity", "name" }, builtin_entity_sample_stop, Script_Builtin_Auth_User),
    makeBuiltin("entity-action-play", { "entity", "name" }, builtin_entity_action_play, Script_Builtin_Auth_User),
    makeBuiltin("entity-action-stop", { "entity", "name" }, builtin_entity_action_stop, Script_Builtin_Auth_User),

    makeBuiltin("entity-body-anchor", { "entity", "name" }, builtin_entity_body_anchor),

    makeBuiltin("entity-hostify", { "entity" }, builtin_entity_hostify, Script_Builtin_Auth_User),
    makeBuiltin("entity-unhostify", { "entity" }, builtin_entity_unhostify, Script_Builtin_Auth_User),
    makeBuiltin("entity-host", { "entity", "state" }, builtin_entity_host_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-host?", { "entity" }, builtin_entity_host_get),
    makeBuiltin("entity-show", { "entity" }, builtin_entity_show, Script_Builtin_Auth_User),
    makeBuiltin("entity-hide", { "entity" }, builtin_entity_hide, Script_Builtin_Auth_User),
    makeBuiltin("entity-shown", { "entity", "state" }, builtin_entity_shown_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-shown?", { "entity" }, builtin_entity_shown_get),
    makeBuiltin("entity-physicalize", { "entity" }, builtin_entity_physicalize, Script_Builtin_Auth_User),
    makeBuiltin("entity-etherealize", { "entity" }, builtin_entity_etherealize, Script_Builtin_Auth_User),
    makeBuiltin("entity-physical", { "entity", "state" }, builtin_entity_physical_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-physical?", { "entity" }, builtin_entity_physical_get),
    makeBuiltin("entity-visualize", { "entity" }, builtin_entity_visualize, Script_Builtin_Auth_User),
    makeBuiltin("entity-unvisualize", { "entity" }, builtin_entity_unvisualize, Script_Builtin_Auth_User),
    makeBuiltin("entity-visible", { "entity", "state" }, builtin_entity_visible_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-visible?", { "entity" }, builtin_entity_visible_get),
    makeBuiltin("entity-audialize", { "entity" }, builtin_entity_audialize, Script_Builtin_Auth_User),
    makeBuiltin("entity-unaudialize", { "entity" }, builtin_entity_unaudialize, Script_Builtin_Auth_User),
    makeBuiltin("entity-audible", { "entity", "state" }, builtin_entity_audible_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-audible?", { "entity" }, builtin_entity_audible_get),

    makeBuiltin("entity-binding-add", { "entity", "input", "name" }, builtin_entity_binding_add, Script_Builtin_Auth_User),
    makeBuiltin("entity-binding-remove", { "entity", "name" }, builtin_entity_binding_remove, Script_Builtin_Auth_User),
    makeBuiltin("entity-bindings", { "entity" }, builtin_entity_bindings),
    makeBuiltin("entity-hud-add", { "entity", "name", "type", "anchor" }, builtin_entity_hud_add, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-remove", { "entity", "name" }, builtin_entity_hud_remove, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-position", { "entity", "name", "position" }, builtin_entity_hud_position, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-color", { "entity", "name", "color" }, builtin_entity_hud_color, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-line", { "entity", "name", "length", "rotation" }, builtin_entity_hud_line, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-text", { "entity", "name", "text" }, builtin_entity_hud_text, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-bar", { "entity", "name", "length", "fraction", "right-to-left" }, builtin_entity_hud_bar, Script_Builtin_Auth_User),
    makeBuiltin("entity-hud-symbol", { "entity", "type", "size" }, builtin_entity_hud_symbol, Script_Builtin_Auth_User),
    makeVariadicBuiltin("entity-field-of-view", builtin_entity_field_of_view),
    makeBuiltin("entity-mouse-lock", { "entity" }, builtin_entity_mouse_lock, Script_Builtin_Auth_User),
    makeBuiltin("entity-mouse-unlock", { "entity" }, builtin_entity_mouse_unlock, Script_Builtin_Auth_User),
    makeBuiltin("entity-mouse-locked", { "entity", "state" }, builtin_entity_mouse_locked_set, Script_Builtin_Auth_User),
    makeBuiltin("entity-mouse-locked?", { "entity" }, builtin_entity_mouse_locked_get),
    makeVariadicBuiltin("entity-voice-volume", builtin_entity_voice_volume),
    makeVariadicBuiltin("entity-tint-color", builtin_entity_tint_color),
    makeVariadicBuiltin("entity-tint-strength", builtin_entity_tint_strength),
#if defined(LMOD_SERVER)
    makeBuiltin("entity-haptic", { "entity", "target", "duration", "frequency", "amplitude" }, builtin_entity_haptic, Script_Builtin_Auth_User),

    makeBuiltin("text-signal", { "user-ids", "state" }, builtin_text_signal, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-titlecard", builtin_text_titlecard, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-timeout", builtin_text_timeout, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-unary", builtin_text_unary, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-binary", builtin_text_binary, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-options", builtin_text_options, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-choose", builtin_text_choose, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-assign", builtin_text_assign, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-point-buy", builtin_text_point_buy, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-point-assign", builtin_text_point_assign, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-vote", builtin_text_vote, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("text-choose-major", builtin_text_choose_major, Script_Builtin_Auth_Admin),
    makeBuiltin("text-knowledge", { "user-ids", "title", "subtitle", "text" }, builtin_text_knowledge, Script_Builtin_Auth_Admin),
    makeBuiltin("text-clear", { "user-ids", "knowledge" }, builtin_text_clear, Script_Builtin_Auth_Admin),
#elif defined(LMOD_CLIENT)
    clientSideStub("entity-haptic"),

    clientSideStub("text-signal"),
    clientSideStub("text-titlecard"),
    clientSideStub("text-timeout"),
    clientSideStub("text-unary"),
    clientSideStub("text-binary"),
    clientSideStub("text-options"),
    clientSideStub("text-choose"),
    clientSideStub("text-assign"),
    clientSideStub("text-point-buy"),
    clientSideStub("text-point-assign"),
    clientSideStub("text-vote"),
    clientSideStub("text-choose-major"),
    clientSideStub("text-knowledge"),
    clientSideStub("text-clear"),
#endif

    makeBuiltin("team-all", {}, builtin_team_all),
    makeBuiltin("team-add", { "team-name" }, builtin_team_add, Script_Builtin_Auth_Admin),
    makeBuiltin("team-remove", { "team-name" }, builtin_team_remove, Script_Builtin_Auth_Admin),
    makeBuiltin("team-get", { "team-name" }, builtin_team_get),
    makeBuiltin("team-set", { "team-name", "user-ids" }, builtin_team_set, Script_Builtin_Auth_Admin),

    makeBuiltin("user-one", { "user-name" }, builtin_user_one),
    makeBuiltin("user-not-one", { "user-name" }, builtin_user_not_one),
    makeBuiltin("user-group", { "user-names" }, builtin_user_group),
    makeBuiltin("user-not-group", { "user-names" }, builtin_user_not_group),
    makeBuiltin("user-all", {}, builtin_user_all),
    makeBuiltin("user-near", { "position", "range" }, builtin_user_near),
    makeBuiltin("user-world", { "world-name" }, builtin_user_world),
    makeBuiltin("user-team", { "team-name" }, builtin_user_team),

    makeBuiltin("user-active?", { "user-id(s)" }, builtin_user_active),
    makeBuiltin("user-attached-entity", { "user-id(s)" }, builtin_user_attached_entity),
    makeBuiltin("user-remove", { "user-id(s)" }, builtin_user_remove, Script_Builtin_Auth_Admin),
    makeBuiltin("user-kick", { "user-id(s)" }, builtin_user_kick, Script_Builtin_Auth_Admin),
    makeBuiltin("user-shift", { "user-id(s)", "world-name" }, builtin_user_shift, Script_Builtin_Auth_Admin),
    makeBuiltin("user-move", { "user-id(s)", "position", "rotation" }, builtin_user_move, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("user-color", builtin_user_color, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("user-teams", builtin_user_teams, Script_Builtin_Auth_Admin),
#if defined(LMOD_SERVER)
    makeBuiltin("user-attach", { "user-id", "entity" }, builtin_user_attach, Script_Builtin_Auth_Admin),
    makeBuiltin("user-detach", { "user-id" }, builtin_user_detach, Script_Builtin_Auth_Admin),
#elif defined(LMOD_CLIENT)
    clientSideStub("user-attach"),
    clientSideStub("user-detach"),
#endif
    makeBuiltin("user-magnitude", { "user-id" }, builtin_user_magnitude),
    makeBuiltin("user-advantage", { "user-id" }, builtin_user_advantage),
    makeBuiltin("user-doom", { "user-id" }, builtin_user_doom),
    makeVariadicBuiltin("user-goto", builtin_user_goto, Script_Builtin_Auth_Admin),
#if defined(LMOD_SERVER)
    makeVariadicBuiltin("user-tether", builtin_user_tether, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("user-confine", builtin_user_confine, Script_Builtin_Auth_Admin),
    makeBuiltin("user-fade", { "user-id(s)", "state" }, builtin_user_fade, Script_Builtin_Auth_Admin),
    makeBuiltin("user-wait", { "user-id(s)", "state" }, builtin_user_wait, Script_Builtin_Auth_Admin),

    makeBuiltin("chat-send", { "message" }, builtin_chat_send, Script_Builtin_Auth_User),
    makeBuiltin("chat-send-team", { "team-name(s)", "message" }, builtin_chat_send_team, Script_Builtin_Auth_User),
    makeBuiltin("chat-send-user", { "user-id(s)", "message" }, builtin_chat_send_user, Script_Builtin_Auth_User),

    makeVariadicBuiltin("voice-channel", builtin_voice_channel, Script_Builtin_Auth_User),
#elif defined(LMOD_CLIENT)
    clientSideStub("user-tether"),
    clientSideStub("user-confine"),
    clientSideStub("user-fade"),
    clientSideStub("user-wait"),

    clientSideStub("chat-send"),
    clientSideStub("chat-send-team"),
    clientSideStub("chat-send-user"),

    clientSideStub("voice-channel", toValue("")),
#endif

    makeBuiltin("attachments-locked", { "state" }, builtin_attachments_locked_set, Script_Builtin_Auth_Admin),
    makeBuiltin("attachments-locked?", {}, builtin_attachments_locked_get),
    makeBuiltin("attachments-lock", {}, builtin_attachments_lock, Script_Builtin_Auth_Admin),
    makeBuiltin("attachments-unlock", {}, builtin_attachments_unlock, Script_Builtin_Auth_Admin),

    makeBuiltin("magnitude-global", {}, builtin_magnitude_global),
    makeBuiltin("magnitude-with", { "name", "value", "lambda" }, builtin_magnitude_with),
#if defined(LMOD_SERVER)
    makeVariadicBuiltin("magnitude-limit", builtin_magnitude_limit, Script_Builtin_Auth_Admin),
    makeVariadicBuiltin("magnitude", builtin_magnitude, Script_Builtin_Auth_User),
#elif defined(LMOD_CLIENT)
    clientSideStub("magnitude-limit", toValue(0)),
    clientSideStub("magnitude", toValue(0)),
#endif

    makeBuiltin("advantage-apply", { "user-id", "value" }, builtin_advantage_apply),
#if defined(LMOD_SERVER)
    makeBuiltin("advantage-give", { "user-id", "count" }, builtin_advantage_give, Script_Builtin_Auth_Admin),
#elif defined(LMOD_CLIENT)
    clientSideStub("advantage-give"),
#endif
    makeBuiltin("advantage-take", { "user-id", "count", "lambda" }, builtin_advantage_take, Script_Builtin_Auth_User),
    makeBuiltin("advantage", {}, builtin_advantage, Script_Builtin_Auth_User),

    makeBuiltin("doom-flag", { "user-id" }, builtin_doom_flag, Script_Builtin_Auth_Admin),
    makeBuiltin("doom-clear", { "user-id" }, builtin_doom_clear, Script_Builtin_Auth_Admin),
    makeBuiltin("doom", {}, builtin_doom, Script_Builtin_Auth_User),

#if defined(LMOD_SERVER)
    makeBuiltin("attach", { "entity" }, builtin_attach, Script_Builtin_Auth_User),
    makeBuiltin("detach", {}, builtin_detach, Script_Builtin_Auth_User),
    makeBuiltin("ping", {}, builtin_ping, Script_Builtin_Auth_User),
#elif defined(LMOD_CLIENT)
    clientSideStub("ping"),
#endif
    makeBuiltin("braking", { "state" }, builtin_braking_set, Script_Builtin_Auth_Admin),
    makeBuiltin("braking?", {}, builtin_braking_get),
    makeBuiltin("brake", {}, builtin_brake, Script_Builtin_Auth_User),
    makeBuiltin("unbrake", {}, builtin_unbrake, Script_Builtin_Auth_Admin),

    makeVariadicBuiltin("splash-message", builtin_splash_message, Script_Builtin_Auth_Admin),

#if defined(LMOD_SERVER)
    makeBuiltin("save", {}, builtin_save)
#elif defined(LMOD_CLIENT)
    clientSideStub("save")
#endif
};
