/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_component.hpp"
#include "tools.hpp"
#include "dynamics.hpp"
#include "dynamics_component_part.hpp"
#include "world.hpp"
#include "entity.hpp"
#include "body_part.hpp"
#include "inverse_kinematics.hpp"
#include "mesh_cache.hpp"

DynamicsComponent::DynamicsComponent(Dynamics* dynamics, DynamicsComponent* parent, Entity* entity)
    : dynamics(dynamics)
    , parent(parent)
    , entity(entity)
    , shape(nullptr)
    , body(nullptr)
    , constraint(nullptr)
    , _creationBody(entity->body())
    , _creationMass(entity->mass())
    , _mass(0)
    , _boundingRadius(0)
    , _boundingBox(vec3(), vec3())
{

}

DynamicsComponent::~DynamicsComponent()
{
    for (btCollisionShape* shape : shapeParts)
    {
        delete shape;
    }

    for (DynamicsMesh* mesh : shapeMeshes)
    {
        delete mesh;
    }
}

tuple<vector<DynamicsForceApplication>, vector<DynamicsArea>> DynamicsComponent::getEnvironmentalEffects()
{
    vector<DynamicsForceApplication> forces;
    forces.reserve(this->forces.size());

    for (const DynamicsForce& force : this->forces)
    {
        forces.push_back(DynamicsForceApplication(this, force));
    }

    vector<DynamicsArea> areas;
    areas.reserve(this->areas.size());

    for (const DynamicsArea& area : this->areas)
    {
        areas.push_back(area.toGlobal(this));
    }

    for (DynamicsComponent* child : children)
    {
        auto [ childForces, childAreas ] = child->getEnvironmentalEffects();

        forces = join(forces, childForces);
        areas = join(areas, childAreas);
    }

    return { forces, areas };
}

void DynamicsComponent::applyEnvironmentalEffects(vector<DynamicsForceApplication>* forces, const vector<DynamicsArea>& areas)
{
    auto [ gravityAcceleration, mediumFlowVelocity, mediumDensity ] = getEnvironment(
        dynamics->self,
        areas,
        globalTransform().position()
    );

    vec3 linearVelocity = this->linearVelocity();
    vec3 angularVelocity = this->angularVelocity();

    vec3 relativeFlowDirection = (mediumFlowVelocity - linearVelocity).normalize();
    f64 relativeFlowSpeed = (mediumFlowVelocity - linearVelocity).length();
    vec3 liftDirection = pickUpDirection(relativeFlowDirection);

    f64 drag = entity->drag();
    f64 mass = this->mass();
    f64 buoyancyVolume = this->buoyancyVolume();
    f64 liftCoefficient = entity->lift();

    vec3 linearDrag = 0.5
        * relativeFlowDirection
        * mediumDensity
        * sq(relativeFlowSpeed)
        * drag
        * linearCrossSectionalArea(relativeFlowDirection);
    applyLinearForce(linearDrag);

    vec3 angularDrag = 0.5
        * -angularVelocity.normalize()
        * mediumDensity
        * sq(angularVelocity.length())
        * drag
        * angularCrossSectionalArea();
    applyAngularForce(angularDrag);

    vec3 gravity = mass * gravityAcceleration;
    applyLinearForce(gravity);

    vec3 buoyancy = mediumDensity * buoyancyVolume * -gravityAcceleration;
    applyLinearForce(buoyancy);

    vec3 lift = 0.5
        * liftDirection
        * liftCoefficient
        * mediumDensity
        * sq(relativeFlowSpeed)
        * linearCrossSectionalArea(liftDirection);
    applyLinearForce(lift);

    for (DynamicsForceApplication& force : *forces)
    {
        force.applyForce(this, mediumDensity);
    }

    for (DynamicsComponent* child : children)
    {
        child->applyEnvironmentalEffects(forces, areas);
    }
}

// Lack of support for scale is intentional, scale transforms with inverse kinematics make no sense
vector<DynamicsIKEffect> DynamicsComponent::generateIKEffects(
    f64 stepInterval,
    const map<BodyPartID, DynamicsAnimationTarget>& pose,
    const mat4& previousTransform
)
{
    vector<DynamicsIKEffect> effects;

    mat4 appliedTransform = previousTransform;

    if (supportsAnimation())
    {
        mat4 upcomingGlobalSelf = previousTransform.applyToTransform(globalTransform());

        vec3 combinedTranslation;
        vec3 combinedForward;
        vec3 combinedUp;
        size_t count = 0;

        for (const auto& [ id, target ] : pose)
        {
            BodyPart* child = entity->body().get(id);

            if (!child)
            {
                continue;
            }

            mat4 upcomingGlobalChild = previousTransform.applyToTransform(entity->globalTransform().applyToTransform(child->regionalTransform()));

            auto [ translation, rotation ] = cyclicCoordinateDescent(
                upcomingGlobalSelf,
                upcomingGlobalChild,
                target.transform,
                stepInterval,
                target.delay
            );

            combinedTranslation += translation;
            combinedForward += rotation.forward();
            combinedUp += rotation.up();
            count++;
        }

        if (count != 0)
        {
            combinedTranslation /= count;
            combinedForward /= count;
            combinedUp /= count;

            quaternion combinedRotation(combinedForward, combinedUp);

            combinedTranslation = constrainTranslation(combinedTranslation);
            combinedRotation = constrainRotation(combinedRotation);

            effects.push_back({ this, combinedTranslation, combinedRotation });

            appliedTransform = previousTransform.applyToTransform({ combinedTranslation, combinedRotation, vec3(1) });
        }
    }

    for (DynamicsComponent* child : children)
    {
        vector<DynamicsIKEffect> childEffects = child->generateIKEffects(
            stepInterval,
            pose,
            appliedTransform
        );

        effects.insert(
            effects.end(),
            childEffects.begin(),
            childEffects.end()
        );
    }

    return effects;
}

const Body& DynamicsComponent::creationBody() const
{
    return _creationBody;
}

f64 DynamicsComponent::creationMass() const
{
    return _creationMass;
}

f64 DynamicsComponent::mass() const
{
    return _mass;
}

f64 DynamicsComponent::boundingRadius() const
{
    return _boundingRadius;
}

BoundingBox DynamicsComponent::boundingBox() const
{
    return _boundingBox;
}

f64 DynamicsComponent::volume() const
{
    vec3 size = boundingBox().size();

    return size.x * size.y * size.z;
}

f64 DynamicsComponent::buoyancyVolume() const
{
    return entity->buoyancy() * volume();
}

f64 DynamicsComponent::angularCrossSectionalArea() const
{
    return pi * sq(boundingRadius());
}

f64 DynamicsComponent::linearCrossSectionalArea(const vec3& globalDirection) const
{
    vec3 localDirection = globalTransform().applyToDirection(globalDirection, false);

    vec3 size = boundingBox().size();

    f64 sideArea = size.y * size.z;
    f64 frontArea = size.x * size.z;
    f64 topArea = size.x * size.y;

    return
        abs(rightDir.dot(localDirection)) * sideArea +
        abs(forwardDir.dot(localDirection)) * frontArea +
        abs(upDir.dot(localDirection)) * topArea;
}

void DynamicsComponent::setGlobalTransform(const mat4& transform)
{
    vec3 position = transform.position();
    quaternion rotation = transform.rotation();

    btTransform worldTransform(
        btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w),
        btVector3(position.x, position.y, position.z)
    );

    body->setWorldTransform(worldTransform);
}

mat4 DynamicsComponent::globalTransform() const
{
    btTransform transform = body->getWorldTransform();

    btVector3 position = transform.getOrigin();
    btQuaternion rotation = transform.getRotation();

    // Bullet doesn't consider scale to be part of transform so we leave it out
    return {
        vec3(position.x(), position.y(), position.z()),
        quaternion(rotation.getW(), rotation.getX(), rotation.getY(), rotation.getZ()),
        vec3(1)
    };
}

void DynamicsComponent::setScale(const vec3& scale)
{
    shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
}

vec3 DynamicsComponent::scale() const
{
    btVector3 scale = shape->getLocalScaling();

    return vec3(scale.x(), scale.y(), scale.z());
}

void DynamicsComponent::setLinearVelocity(const vec3& linV)
{
    body->setLinearVelocity(btVector3(linV.x, linV.y, linV.z));
}

vec3 DynamicsComponent::linearVelocity() const
{
    btVector3 linV = body->getLinearVelocity();

    return vec3(linV.x(), linV.y(), linV.z());
}

void DynamicsComponent::setAngularVelocity(const vec3& angV)
{
    body->setAngularVelocity(btVector3(angV.x, angV.y, angV.z));
}

vec3 DynamicsComponent::angularVelocity() const
{
    btVector3 angV = body->getAngularVelocity();

    return vec3(angV.x(), angV.y(), angV.z());
}

void DynamicsComponent::setFriction(f64 friction)
{
    body->setFriction(friction);
}

f64 DynamicsComponent::friction() const
{
    return body->getFriction();
}

void DynamicsComponent::setRestitution(f64 restitution)
{
    body->setRestitution(restitution);
}

f64 DynamicsComponent::restitution() const
{
    return body->getRestitution();
}

void DynamicsComponent::writeback()
{
    for (DynamicsComponent* child : children)
    {
        child->writeback();
    }
}

void DynamicsComponent::applyLinearForce(const vec3& position, const vec3& force)
{
    body->applyForce(btVector3(force.x, force.y, force.z), btVector3(position.x, position.y, position.z));
}

void DynamicsComponent::applyLinearForce(const vec3& force)
{
    body->applyCentralForce(btVector3(force.x, force.y, force.z));
}

void DynamicsComponent::applyAngularForce(const vec3& force)
{
    body->applyTorque(btVector3(force.x, force.y, force.z));
}

btTransform DynamicsComponent::inBodySpace(const mat4& transform) const
{
    mat4 constraintLocal = globalTransform().applyToTransform(transform, false);

    btTransform result = btTransform::getIdentity();
    result.setOrigin(btVector3(
        constraintLocal.position().x,
        constraintLocal.position().y,
        constraintLocal.position().z
    ));
    result.setRotation(btQuaternion(
        constraintLocal.rotation().x,
        constraintLocal.rotation().y,
        constraintLocal.rotation().z,
        constraintLocal.rotation().w
    ));

    return result;
}

static void setVec3Scalars(vector<f64>& points, size_t index, const vec3& point)
{
    points[index + 0] = point.x;
    points[index + 1] = point.y;
    points[index + 2] = point.z;
}

void DynamicsComponent::addToShape(BodyPart* part)
{
    _boundingRadius = max(_boundingRadius, part->boundingRadius());

    _boundingBox.add(part->regionalTransform(), part->boundingBox());

    btTransform childTransform;
    childTransform.setOrigin(btVector3(
        part->regionalPosition().x,
        part->regionalPosition().y,
        part->regionalPosition().z
    ));
    childTransform.setRotation(btQuaternion(
        part->regionalRotation().x,
        part->regionalRotation().y,
        part->regionalRotation().z,
        part->regionalRotation().w
    ));
    btCollisionShape* childShape;
    DynamicsMesh* childMesh = nullptr;

    switch (part->type())
    {
    default :
        fatal("Encountered unknown body part type '" + to_string(part->type()) + "'.");
    case Body_Part_Line :
    {
        f64 radius = part->get<BodyPartLine>().diameter / 2;

        childShape = new btCapsuleShape(
            radius,
            part->get<BodyPartLine>().length - (radius * 2)
        );
        break;
    }
    case Body_Part_Plane :
        switch (part->get<BodyPartPlane>().type)
        {
        default :
            fatal("Encountered unknown plane type '" + to_string(part->get<BodyPartPlane>().type) + "'.");
        case Body_Plane_Ellipse :
            childShape = new btCylinderShapeZ(btVector3(
                part->get<BodyPartPlane>().width / 2,
                part->get<BodyPartPlane>().height / 2,
                part->get<BodyPartPlane>().thickness / 2
            ));
            break;
        case Body_Plane_Isosceles_Triangle :
        case Body_Plane_Right_Angle_Triangle :
        {
            vector<vec3> corners = part->get<BodyPartPlane>().subparts();
            vec3 center = (corners.at(0) + corners.at(1) + corners.at(2)) / 3;
            vector<f64> points(3 * 2 * 3);

            for (size_t i = 0; i < 3; i++)
            {
                vec3 base = corners.at(i);
                base = center + ((base - center).normalize() * (base.length() + lineWidth / 2));

                for (size_t j = 0; j < 2; j++)
                {
                    vec3 point = base + vec3(
                        0,
                        0,
                        j == 0 ? -(part->get<BodyPartPlane>().thickness / 2) : +(part->get<BodyPartPlane>().thickness / 2)
                    );

                    size_t index = (i * 3 * 2) + (j * 3);

                    setVec3Scalars(points, index, point);
                }
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        case Body_Plane_Rectangle :
            childShape = new btBoxShape(btVector3(
                part->get<BodyPartPlane>().width / 2,
                part->get<BodyPartPlane>().height / 2,
                part->get<BodyPartPlane>().thickness / 2
            ));
            break;
        case Body_Plane_Equilateral_Triangle :
        case Body_Plane_Pentagon :
        case Body_Plane_Hexagon :
        case Body_Plane_Heptagon :
        case Body_Plane_Octagon :
        {
            size_t subpartCount = part->get<BodyPartPlane>().subparts().size();

            vector<f64> points(subpartCount * 2 * 3);

            for (size_t i = 0; i < subpartCount; i++)
            {
                vec3 base = part->get<BodyPartPlane>().subparts().at(i);
                base = base.normalize() * (base.length() + lineWidth / 2);

                for (size_t j = 0; j < 2; j++)
                {
                    vec3 point = base + vec3(
                        0,
                        0,
                        j == 0 ? -(part->get<BodyPartPlane>().thickness / 2) : +(part->get<BodyPartPlane>().thickness / 2)
                    );

                    size_t index = (i * 3 * 2) + (j * 3);

                    setVec3Scalars(points, index, point);
                }
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        }
        break;
    case Body_Part_Anchor :
        childShape = new btEmptyShape();
        break;
    case Body_Part_Solid :
        switch (part->get<BodyPartSolid>().type)
        {
        default :
            fatal("Encountered unknown solid type '" + to_string(part->get<BodyPartSolid>().type) + "'.");
        case Body_Solid_Cuboid :
            childShape = new btBoxShape(btVector3(
                part->get<BodyPartSolid>().size.x / 2,
                part->get<BodyPartSolid>().size.y / 2,
                part->get<BodyPartSolid>().size.z / 2
            ));
            break;
        case Body_Solid_Sphere :
            childShape = new btSphereShape(part->get<BodyPartSolid>().size.x / 2);
            break;
        case Body_Solid_Cylinder :
            childShape = new btCylinderShapeZ(btVector3(
                part->get<BodyPartSolid>().size.x / 2,
                part->get<BodyPartSolid>().size.x / 2,
                part->get<BodyPartSolid>().size.z / 2
            ));
            break;
        case Body_Solid_Cone :
            childShape = new btConeShapeZ(part->get<BodyPartSolid>().size.x / 2, part->get<BodyPartSolid>().size.z);
            break;
        case Body_Solid_Capsule :
            childShape = new btCapsuleShapeZ(part->get<BodyPartSolid>().size.x / 2, part->get<BodyPartSolid>().size.z);
            break;
        case Body_Solid_Pyramid :
        {
            vector<vec3> corners = part->get<BodyPartSolid>().subparts();

            vector<f64> points(5 * 3);

            for (size_t i = 0; i < corners.size(); i++)
            {
                setVec3Scalars(points, i * 3, corners[i]);
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        case Body_Solid_Tetrahedron :
        {
            vector<vec3> corners = part->get<BodyPartSolid>().subparts();

            vector<f64> points(4 * 3);

            for (size_t i = 0; i < corners.size(); i++)
            {
                setVec3Scalars(points, i * 3, corners[i]);
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        case Body_Solid_Octahedron :
        {
            vector<vec3> corners = part->get<BodyPartSolid>().subparts();

            vector<f64> points(6 * 3);

            for (size_t i = 0; i < corners.size(); i++)
            {
                setVec3Scalars(points, i * 3, corners[i]);
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        case Body_Solid_Dodecahedron :
        {
            vector<vec3> corners = part->get<BodyPartSolid>().subparts();

            vector<f64> points(20 * 3);

            for (size_t i = 0; i < corners.size(); i++)
            {
                setVec3Scalars(points, i * 3, corners[i]);
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        case Body_Solid_Icosahedron :
        {
            vector<vec3> corners = part->get<BodyPartSolid>().subparts();

            vector<f64> points(12 * 3);

            for (size_t i = 0; i < corners.size(); i++)
            {
                setVec3Scalars(points, i * 3, corners[i]);
            }

            childShape = new btConvexHullShape(points.data(), points.size() / 3, sizeof(f64) * 3);
            break;
        }
        }
        break;
    case Body_Part_Text :
        childShape = new btEmptyShape();
        break;
    case Body_Part_Symbol :
        childShape = new btEmptyShape();
        break;
    case Body_Part_Canvas :
        childShape = new btEmptyShape();
        break;
    case Body_Part_Structure :
        childMesh = new DynamicsMesh(meshCache->ensure(part->get<BodyPartStructure>().node));
        childShape = new btBvhTriangleMeshShape(childMesh->mesh(), true, true);
        break;
    case Body_Part_Terrain :
        childMesh = new DynamicsMesh(meshCache->ensure(EmissiveColor(), part->get<BodyPartTerrain>().nodes));
        childShape = new btBvhTriangleMeshShape(childMesh->mesh(), true, true);
        break;
    case Body_Part_Light :
        childShape = new btEmptyShape();
        break;
    case Body_Part_Force :
        childShape = new btEmptyShape();
        forces.push_back(DynamicsForce(part));
        break;
    case Body_Part_Area :
        childShape = new btEmptyShape();
        areas.push_back(DynamicsArea(part));
        break;
    case Body_Part_Rope :
        // TODO: ROPE: Dynamics
        childShape = new btEmptyShape();
        break;
    }

    childShape->setLocalScaling(btVector3(
        part->regionalScale().x,
        part->regionalScale().y,
        part->regionalScale().z
    ));

    shape->addChildShape(childTransform, childShape);
    shapeParts.push_back(childShape);
    if (childMesh)
    {
        shapeMeshes.push_back(childMesh);
    }

    for (const auto& [ id, part ] : part->parts())
    {
        if (part->constraintType() != Body_Constraint_Fixed)
        {
            continue;
        }

        addToShape(part);
    }
}

vec3 DynamicsComponent::constrainTranslation(const vec3& translation) const
{
    if (!supportsTranslation())
    {
        return vec3();
    }

    return translation;
}

quaternion DynamicsComponent::constrainRotation(const quaternion& rotation) const
{
    return rotation;
}

bool DynamicsComponent::supportsTranslation() const
{
    return !parent;
}

void DynamicsComponent::createShape(const vec3& scale)
{
    shape = new btCompoundShape();
    shape->setLocalScaling(btVector3(scale.x, scale.y, scale.z));
}

void DynamicsComponent::destroyShape()
{
    delete shape;
}

void DynamicsComponent::createBody(const vec3& position, const quaternion& rotation, f64 mass)
{
    motionState = new btDefaultMotionState(
        btTransform(
            btQuaternion(rotation.x, rotation.y, rotation.z, rotation.w),
            btVector3(position.x, position.y, position.z)
        ),
        btTransform(
            btQuaternion(0, 0, 0, 1),
            btVector3(0, 0, 0)
        )
    );

    btVector3 inertia;
    shape->calculateLocalInertia(mass, inertia);

    body = new btRigidBody(mass, motionState, shape, inertia);
    body->setLinearVelocity(btVector3(
        entity->linearVelocity().x,
        entity->linearVelocity().y,
        entity->linearVelocity().z
    ));
    body->setAngularVelocity(btVector3(
        entity->angularVelocity().x,
        entity->angularVelocity().y,
        entity->angularVelocity().z
    ));
    body->setFriction(entity->friction());
    body->setRestitution(entity->restitution());
    body->setDamping(0, 0);
    body->setUserPointer(entity);

    dynamics->world()->addRigidBody(body);

    _mass = mass;
}

void DynamicsComponent::destroyBody()
{
    dynamics->world()->removeRigidBody(body);
    delete body;

    delete motionState;
}

void DynamicsComponent::destroyConstraint()
{
    dynamics->world()->removeConstraint(constraint);
    delete constraint;
}

set<BodyPartID> DynamicsComponent::createChildren(BodyPart* part)
{
    set<BodyPartID> includedPartIDs;

    if (part->constraintType() == Body_Constraint_Fixed)
    {
        includedPartIDs.insert(part->id());

        for (const auto& [ id, part ] : part->parts())
        {
            set<BodyPartID> ids = createChildren(part);
            includedPartIDs.insert(ids.begin(), ids.end());
        }
    }
    else
    {
        children.push_back(new DynamicsComponentPart(dynamics, this, entity, part));
    }

    return includedPartIDs;
}

void DynamicsComponent::destroyChildren()
{
    for (DynamicsComponent* child : children)
    {
        delete child;
    }
}
