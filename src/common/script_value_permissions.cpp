/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_value_permissions.hpp"
#include "user.hpp"

ScriptValuePermissions::ScriptValuePermissions()
{
    restricted = false;
}

ScriptValuePermissions::ScriptValuePermissions(UserID id)
    : ScriptValuePermissions()
{
    this->id = id;
}

ScriptValuePermissions::ScriptValuePermissions(UserID id, bool restricted)
    : ScriptValuePermissions()
{
    this->id = id;
    this->restricted = restricted;
}

ScriptValuePermissions::ScriptValuePermissions(const User* user)
    : ScriptValuePermissions()
{
    id = user ? user->id() : UserID();
}

bool ScriptValuePermissions::canRead(const User* user) const
{
    if (!user)
    {
        return !restricted || id == UserID();
    }

    return !restricted || id == UserID() || user->admin() || user->id() == id;
}

bool ScriptValuePermissions::canWrite(const User* user) const
{
    if (!user)
    {
        return id == UserID();
    }

    return id == UserID() || user->admin() || user->id() == id;
}

bool ScriptValuePermissions::operator==(const ScriptValuePermissions& rhs) const
{
    return id == rhs.id && restricted == rhs.restricted;
}

bool ScriptValuePermissions::operator!=(const ScriptValuePermissions& rhs) const
{
    return !(*this == rhs);
}

template<>
ScriptValuePermissions YAMLNode::convert() const
{
    ScriptValuePermissions permissions;

    permissions.id = at("id").as<UserID>();
    permissions.restricted = at("restricted").as<bool>();

    return permissions;
}

template<>
void YAMLSerializer::emit(ScriptValuePermissions v)
{
    startMapping();

    emitPair("id", v.id);
    emitPair("restricted", v.restricted);

    endMapping();
}
