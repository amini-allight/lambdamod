/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "bresenham.hpp"

static void plotLineLo2D(const ivec2& start, const ivec2& end, const function<void(ivec2)>& behavior)
{
    ivec2 d = end - start;

    i32 yi = 1;

    if (d.y < 0)
    {
        yi = -1;
        d.y *= -1;
    }

    i32 D = (2 * d.y) - d.x;
    i32 y = start.y;

    for (i32 x = start.x; x <= end.x; x++)
    {
        behavior({ x, y });

        if (D > 0)
        {
            y += yi;
            D += (2 * (d.y - d.x));
        }
        else
        {
            D += 2 * d.y;
        }
    }
}

static void plotLineHi2D(const ivec2& start, const ivec2& end, const function<void(ivec2)>& behavior)
{
    ivec2 d = end - start;

    i32 xi = 1;

    if (d.x < 0)
    {
        xi = -1;
        d.x *= -1;
    }

    i32 D = (2 * d.x) - d.y;
    i32 x = start.x;

    for (i32 y = start.y; y <= end.y; y++)
    {
        behavior({ x, y });

        if (D > 0)
        {
            x += xi;
            D += (2 * (d.x - d.y));
        }
        else
        {
            D += 2 * d.x;
        }
    }
}

void bresenham2D(const ivec2& start, const ivec2& end, const function<void(ivec2)>& behavior)
{
    if (abs(end.y - start.y) < abs(end.x - start.x))
    {
        if (start.x > end.x)
        {
            plotLineLo2D(end, start, behavior);
        }
        else
        {
            plotLineLo2D(start, end, behavior);
        }
    }
    else
    {
        if (start.y > end.y)
        {
            plotLineHi2D(end, start, behavior);
        }
        else
        {
            plotLineHi2D(start, end, behavior);
        }
    }

}
