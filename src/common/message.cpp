/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "message.hpp"
#include "yaml_tools.hpp"
#include "log.hpp"

#if defined(LMOD_CLIENT)
#include "client/localization.hpp"
#endif

string messageTypeToString(MessageType type)
{
    switch (type)
    {
    case Message_Direct_Chat_In :
        return "message-direct-chat-in";
    case Message_Direct_Chat_Out :
        return "message-direct-chat-out";
    case Message_Team_Chat_In :
        return "message-team-chat-in";
    case Message_Team_Chat_Out :
        return "message-team-chat-out";
    case Message_Chat :
        return "message-chat";
    case Message_Script :
        return "message-script";
    case Message_Log :
        return "message-log";
    case Message_Warning :
        return "message-warning";
    case Message_Error :
        return "message-error";
    default :
        fatal("Encountered unknown message type '" + to_string(type) + "'.");
    }
}

MessageType messageTypeFromString(const string& s)
{
    if (s == "message-direct-chat-in")
    {
        return Message_Direct_Chat_In;
    }
    else if (s == "message-direct-chat-out")
    {
        return Message_Direct_Chat_Out;
    }
    else if (s == "message-team-chat-in")
    {
        return Message_Team_Chat_In;
    }
    else if (s == "message-team-chat-out")
    {
        return Message_Team_Chat_Out;
    }
    else if (s == "message-chat")
    {
        return Message_Chat;
    }
    else if (s == "message-script")
    {
        return Message_Script;
    }
    else if (s == "message-log")
    {
        return Message_Log;
    }
    else if (s == "message-warning")
    {
        return Message_Warning;
    }
    else if (s == "message-error")
    {
        return Message_Error;
    }
    else
    {
        fatal("Encountered unknown message type '" + s + "'.");
    }
}

Message::Message()
    : type(Message_Log)
{
}

Message::Message(MessageType type, const string& name, const string& text, const vector<string>& parameters)
    : type(type)
    , name(name)
    , text(text)
    , parameters(parameters)
{

}

#if defined(LMOD_CLIENT)
string Message::toString() const
{
    switch (type)
    {
    case Message_Direct_Chat_In :
        return "From " + name + ": " + text;
    case Message_Direct_Chat_Out :
        return "To " + name + ": " + text;
    case Message_Team_Chat_In :
        return "From " + name + ": " + text;
    case Message_Team_Chat_Out :
        return "To " + name + ": " + text;
    case Message_Chat :
        return name + ": " + text;
    default :
    case Message_Script :
    case Message_Log :
    case Message_Warning :
    case Message_Error :
        return localize(text, parameters);
    }
}
#endif

template<>
Message YAMLNode::convert() const
{
    return {
        messageTypeFromString(at("type").as<string>()),
        at("name").as<string>(),
        at("text").as<string>(),
        at("parameters").as<vector<string>>()
    };
}

template<>
void YAMLSerializer::emit(Message v)
{
    startMapping();

    emitPair("type", messageTypeToString(v.type));
    emitPair("name", v.name);
    emitPair("text", v.text);
    emitPair("parameters", v.parameters);

    endMapping();
}
