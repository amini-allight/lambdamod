/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(__unix__) || defined(__APPLE__)
#include "network_tools.hpp"
#include "log.hpp"

#include <netinet/tcp.h>
#include <fcntl.h>
#include <sys/socket.h>

int setSocketOption(int socket, int level, int option, int value)
{
    int result = setsockopt(socket, level, option, &value, sizeof(int));

    if (result != 0)
    {
        error("Failed to set socket option: " + to_string(errno));
    }

    return result;
}

int setFileControl(int file, int flag, bool state)
{
    int flags = fcntl(file, F_GETFL, 0);
    
    if (flags == -1)
    {
        error("Failed to set file control: " + to_string(errno));
        return 1;
    }

    if (state)
    {
        flags |= flag;
    }
    else
    {
        flags &= ~flag;
    }

    int result = fcntl(file, F_SETFL, flags);

    if (result == -1)
    {
        error("Failed to set file control: " + to_string(errno));
    }

    return result;
}
#endif
