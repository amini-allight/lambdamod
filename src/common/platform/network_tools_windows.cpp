/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#if defined(WIN32)
#include "network_tools.hpp"
#include "log.hpp"

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <winsock2.h>

int setSocketOption(u64 socket, int level, int option, const char* value, int length)
{
    int result = setsockopt(socket, level, option, value, length);

    if (result == SOCKET_ERROR)
    {
        error("Failed to set socket option: " + to_string(WSAGetLastError()));
    }

    return result;
}

int ioctlSocket(u64 socket, long command, unsigned long value)
{
    int result = ioctlsocket(socket, command, &value);

    if (result == SOCKET_ERROR)
    {
        error("Failed to ioctl socket: " + to_string(WSAGetLastError()));
    }

    return result;
}
#endif
