/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "log.hpp"

#ifdef LMOD_DEBUG_MESSAGES
static string debugColor(const string& text)
{
#if defined(__unix__) || defined(__APPLE__)
    return "\033[90m" + text + "\033[0m";
#else
    return text;
#endif
}
#endif

static string warningColor(const string& text)
{
#if defined(__unix__) || defined(__APPLE__)
    return "\033[33m" + text + "\033[0m";
#else
    return text;
#endif
}

static string errorColor(const string& text)
{
#if defined(__unix__) || defined(__APPLE__)
    return "\033[31m" + text + "\033[0m";
#else
    return text;
#endif
}

void debug(const string& message)
{
#ifdef LMOD_DEBUG_MESSAGES
    cout << debugColor("DBG " + message) << endl;
#endif
}

void log(const string& message)
{
    cout << "LOG " << message << endl;
}

void warning(const string& message)
{
    cerr << warningColor("WRN " + message) << endl;
}

void error(const string& message)
{
    cerr << errorColor("ERR " + message) << endl;
}

void fatal(const string& message)
{
    cerr << errorColor("FTL " + message) << endl;
    throw runtime_error(message);
}
