/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics.hpp"
#include "world.hpp"
#include "tools.hpp"
#include "constants.hpp"
#include "intersections.hpp"
#include "sound_constants.hpp"

static constexpr i32 subSteps = 3;

struct DynamicsUserData
{
    Dynamics* dynamics;
    ScriptContext* context;
};

Dynamics::Dynamics(World* self)
    : self(self)
{
    broadphase = new btDbvtBroadphase();

    collisionConfiguration = new btDefaultCollisionConfiguration();
    collisionDispatcher = new btCollisionDispatcher(collisionConfiguration);

    constraintSolver = new btSequentialImpulseConstraintSolver();

    _world = new btDiscreteDynamicsWorld(
        collisionDispatcher,
        broadphase,
        constraintSolver,
        collisionConfiguration
    );
    _world->setGravity(btVector3(0, 0, 0));

    update();
}

Dynamics::~Dynamics()
{
    for (const auto& [ id, entity ] : entities)
    {
        delete entity;
    }

    delete _world;
    delete constraintSolver;
    delete collisionDispatcher;
    delete collisionConfiguration;
    delete broadphase;
}

void Dynamics::step(ScriptContext* context, f64 stepInterval)
{
    auto [ forces, areas ] = getEnvironmentalEffects();

    for (const auto& [ id, entity ] : entities)
    {
        entity->applyEnvironmentalEffects(&forces, areas);
        entity->applyActions(stepInterval);
    }

    for (const DynamicsForceApplication& force : forces)
    {
        force.applyReaction(self, areas);
    }

    DynamicsUserData userData(this, context);

    _world->setInternalTickCallback(&Dynamics::onTick, &userData);
    _world->stepSimulation(stepInterval, subSteps);

    for (const auto& [ id, entity ] : entities)
    {
        entity->writeback();
    }
}

void Dynamics::update()
{
    for (auto it = entities.begin(); it != entities.end();)
    {
        const auto& [ id, entity ] = *it;

        auto worldIt = self->entities().find(id);

        if (worldIt == self->entities().end() || !worldIt->second->active() || !worldIt->second->physical())
        {
            delete it->second;
            entities.erase(it++);
        }
        else
        {
            entity->update();
            it++;
        }
    }

    for (auto it = self->entities().begin(); it != self->entities().end(); it++)
    {
        const auto& [ id, entity ] = *it;

        if (!entities.contains(id) && entity->active() && entity->physical())
        {
            entities.insert({ id, new DynamicsEntity(this, entity) });
        }
    }

    _spaceGraph.update(self->entities());
}

static optional<tuple<vec3, vec3>> occlusion(const SpaceGraphNode* node, const vec3& position, const vec3& direction, f64 distance)
{
    vec3 size = node->size();

    vector<vec3> intersections = intersectCuboid(
        size.x,
        size.y,
        size.z,
        position - node->center(),
        direction,
        distance
    );

    if (intersections.size() < 2)
    {
        return {};
    }

    if (intersections.front().distance(intersections.back()) < soundOcclusionMinThickness)
    {
        return {};
    }

    return tuple<vec3, vec3>(node->center() + intersections.front(), node->center() + intersections.back());
}

void Dynamics::updatePose(const Entity* entity, const map<VRDevice, mat4>& pose)
{
    auto it = entities.find(entity->id());

    if (it == entities.end())
    {
        return;
    }

    it->second->updatePose(pose);
}

f64 Dynamics::soundOcclusion(const vec3& a, const vec3& b) const
{
    if (!_spaceGraph.root())
    {
        return 0;
    }

    vector<const SpaceGraphNode*> aPath;

    _spaceGraph.traverseAscending(a, [&](const uvec3& index, const SpaceGraphNode* node) -> void {
        aPath.push_back(node);
    });

    const SpaceGraphNode* commonParent = nullptr;

    _spaceGraph.traverseAscending(b, [&](const uvec3& index, const SpaceGraphNode* node) -> void {
        for (const SpaceGraphNode* part : aPath)
        {
            if (part == node && (!commonParent || part->depth() > commonParent->depth()))
            {
                commonParent = part;
                break;
            }
        }
    });

    f64 angularDiameter = 0;

    commonParent->traverse([&](const uvec3& index, const SpaceGraphNode* node) -> void {
        if (!node->isLeaf() || node->isInfinite())
        {
            return;
        }

        optional<tuple<vec3, vec3>> intersections = occlusion(node, a, (b - a).normalize(), a.distance(b));

        if (!intersections)
        {
            return;
        }

        auto [ aIntersect, bIntersect ] = *intersections;

        f64 aPerspectiveAngularDiameter = ::angularDiameter(node->boundingRadius(), a.distance(aIntersect));
        f64 bPerspectiveAngularDiameter = ::angularDiameter(node->boundingRadius(), b.distance(bIntersect));

        angularDiameter = max(angularDiameter, max(aPerspectiveAngularDiameter, bPerspectiveAngularDiameter));
    });

    return angularDiameter / (angularDiameter + soundOcclusionAngularDiameterCoefficient);
}

vector<ReverbEffect> Dynamics::soundReverb(const vec3& position) const
{
    vector<ReverbEffect> effects;

    if (!_spaceGraph.root())
    {
        return effects;
    }

    uvec3 previousIndex(numeric_limits<u32>::max());

    _spaceGraph.traverseAscending(position, [&](const uvec3& index, const SpaceGraphNode* node) -> void {
        if (previousIndex != uvec3(numeric_limits<u32>::max()))
        {
            effects = join(effects, findReverbSurfaces(node, previousIndex, position));
        }

        previousIndex = index;
    });

    if (previousIndex != uvec3(numeric_limits<u32>::max()))
    {
        effects = join(effects, findReverbSurfaces(_spaceGraph.root(), previousIndex, position));
    }

    for (ReverbEffect& effect : effects)
    {
        effect.strength *= soundOcclusion(effect.position, position);
    }

    return effects;
}

const SpaceGraph& Dynamics::spaceGraph() const
{
    return _spaceGraph;
}

btDiscreteDynamicsWorld* Dynamics::world() const
{
    return _world;
}

tuple<vector<DynamicsForceApplication>, vector<DynamicsArea>> Dynamics::getEnvironmentalEffects() const
{
    vector<DynamicsForceApplication> forces;
    vector<DynamicsArea> areas;

    for (const auto& [ id, entity ] : entities)
    {
        auto [ childForces, childAreas ] = entity->getEnvironmentalEffects();

        forces = join(forces, childForces);
        areas = join(areas, childAreas);
    }

    return { forces, areas };
}

void Dynamics::onTick(btDynamicsWorld* world, btScalar timeStep)
{
    auto userData = static_cast<DynamicsUserData*>(world->getWorldUserInfo());

    for (i32 i = 0; i < world->getDispatcher()->getNumManifolds(); i++)
    {
        btPersistentManifold* manifold = world->getDispatcher()->getManifoldByIndexInternal(i);

        vec3 pos;

        for (i32 j = 0; j < manifold->getNumContacts(); j++)
        {
            const btManifoldPoint& contactPoint = manifold->getContactPoint(j);

            btVector3 posA = contactPoint.getPositionWorldOnA();
            btVector3 posB = contactPoint.getPositionWorldOnB();

            pos += vec3(posA.x(), posA.y(), posA.z());
            pos += vec3(posB.x(), posB.y(), posB.z());
        }

        pos /= manifold->getNumContacts() * 2;

        const btCollisionObject* object0 = manifold->getBody0();
        const btCollisionObject* object1 = manifold->getBody1();

        userData->dynamics->self->handleCollision(
            userData->context,
            static_cast<Entity*>(object0->getUserPointer()),
            static_cast<Entity*>(object1->getUserPointer()),
            pos
        );
    }
}
