/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "texts.hpp"
#include "yaml_tools.hpp"

bool TextSignalRequest::operator==(const TextSignalRequest& rhs) const
{
    return state == rhs.state;
}

bool TextSignalRequest::operator!=(const TextSignalRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextSignalRequest::valid() const
{
    return true;
}

bool TextTitlecardRequest::operator==(const TextTitlecardRequest& rhs) const
{
    return title == rhs.title &&
        subtitle == rhs.subtitle;
}

bool TextTitlecardRequest::operator!=(const TextTitlecardRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextTitlecardRequest::valid() const
{
    return true;
}

template<>
TextTitlecardRequest YAMLNode::convert() const
{
    TextTitlecardRequest request;

    request.title = at("title").as<string>();
    request.subtitle = at("subtitle").as<string>();

    return request;
}

template<>
void YAMLSerializer::emit(TextTitlecardRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("subtitle", v.subtitle);

    endMapping();
}

bool TextTimeoutRequest::operator==(const TextTimeoutRequest& rhs) const
{
    return title == rhs.title &&
        text == rhs.text &&
        timeout == rhs.timeout;
}

bool TextTimeoutRequest::operator!=(const TextTimeoutRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextTimeoutRequest::valid() const
{
    return timeout > 0;
}

template<>
TextTimeoutRequest YAMLNode::convert() const
{
    TextTimeoutRequest request;

    request.title = at("title").as<string>();
    request.text = at("text").as<string>();
    request.timeout = at("timeout").as<f64>();

    return request;
}

template<>
void YAMLSerializer::emit(TextTimeoutRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("text", v.text);
    emitPair("timeout", v.timeout);

    endMapping();
}

bool TextUnaryRequest::operator==(const TextUnaryRequest& rhs) const
{
    return title == rhs.title &&
        text == rhs.text;
}

bool TextUnaryRequest::operator!=(const TextUnaryRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextUnaryRequest::valid() const
{
    return true;
}

bool TextUnaryRequest::valid(const TextUnaryResponse& response) const
{
    return true;
}

template<>
TextUnaryRequest YAMLNode::convert() const
{
    TextUnaryRequest request;

    request.title = at("title").as<string>();
    request.text = at("text").as<string>();

    return request;
}

template<>
void YAMLSerializer::emit(TextUnaryRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("text", v.text);

    endMapping();
}

bool TextBinaryRequest::operator==(const TextBinaryRequest& rhs) const
{
    return title == rhs.title &&
        text == rhs.text;
}

bool TextBinaryRequest::operator!=(const TextBinaryRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextBinaryRequest::valid() const
{
    return true;
}

bool TextBinaryRequest::valid(const TextBinaryResponse& response) const
{
    return response.choice < 2;
}

template<>
TextBinaryRequest YAMLNode::convert() const
{
    TextBinaryRequest request;

    request.title = at("title").as<string>();
    request.text = at("text").as<string>();

    return request;
}

template<>
void YAMLSerializer::emit(TextBinaryRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("text", v.text);

    endMapping();
}

bool TextOptionsRequest::operator==(const TextOptionsRequest& rhs) const
{
    return title == rhs.title &&
        text == rhs.text &&
        options == rhs.options;
}

bool TextOptionsRequest::operator!=(const TextOptionsRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextOptionsRequest::valid() const
{
    return !options.empty();
}

bool TextOptionsRequest::valid(const TextOptionsResponse& response) const
{
    return options.contains(response.choice);
}

template<>
TextOptionsRequest YAMLNode::convert() const
{
    TextOptionsRequest request;

    request.title = at("title").as<string>();
    request.text = at("text").as<string>();
    request.options = at("options").as<set<string>>();

    return request;
}

template<>
void YAMLSerializer::emit(TextOptionsRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("text", v.text);
    emitPair("options", v.options);

    endMapping();
}

bool TextChooseRequest::operator==(const TextChooseRequest& rhs) const
{
    return title == rhs.title &&
        options == rhs.options &&
        count == rhs.count;
}

bool TextChooseRequest::operator!=(const TextChooseRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextChooseRequest::valid() const
{
    return !options.empty() && count > 0 && count < options.size() - 1;
}

bool TextChooseRequest::valid(const TextChooseResponse& response) const
{
    if (response.choices.size() > count)
    {
        return false;
    }

    for (const string& choice : response.choices)
    {
        auto it = options.find(choice);

        if (it == options.end())
        {
            return false;
        }
    }

    return true;
}

template<>
TextChooseRequest YAMLNode::convert() const
{
    TextChooseRequest request;

    request.title = at("title").as<string>();
    request.options = at("options").as<set<string>>();
    request.count = at("count").as<u64>();

    return request;
}

template<>
void YAMLSerializer::emit(TextChooseRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("options", v.options);
    emitPair("count", v.count);

    endMapping();
}

bool TextAssignValuesRequest::operator==(const TextAssignValuesRequest& rhs) const
{
    return title == rhs.title &&
        keys == rhs.keys &&
        values == rhs.values;
}

bool TextAssignValuesRequest::operator!=(const TextAssignValuesRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextAssignValuesRequest::valid() const
{
    return !keys.empty() && keys.size() == values.size();
}

bool TextAssignValuesRequest::valid(const TextAssignValuesResponse& response) const
{
    if (response.assignment.size() != keys.size())
    {
        return false;
    }

    set<string> usedValues;

    for (const auto& [ key, value ] : response.assignment)
    {
        if (value.empty())
        {
            continue;
        }

        auto keyIt = keys.find(key);

        if (keyIt == keys.end())
        {
            return false;
        }

        auto valIt = values.find(value);

        if (valIt == values.end())
        {
            return false;
        }

        size_t oldSize = usedValues.size();

        usedValues.insert(value);

        if (usedValues.size() == oldSize)
        {
            return false;
        }
    }

    return true;
}

template<>
TextAssignValuesRequest YAMLNode::convert() const
{
    TextAssignValuesRequest request;

    request.title = at("title").as<string>();
    request.keys = at("options").as<set<string>>();
    request.values = at("values").as<set<string>>();

    return request;
}

template<>
void YAMLSerializer::emit(TextAssignValuesRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("keys", v.keys);
    emitPair("values", v.values);

    endMapping();
}

bool TextSpendPointsRequest::operator==(const TextSpendPointsRequest& rhs) const
{
    return title == rhs.title &&
        prices == rhs.prices &&
        points == rhs.points;
}

bool TextSpendPointsRequest::operator!=(const TextSpendPointsRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextSpendPointsRequest::valid() const
{
    for (const auto& [ name, price ] : prices)
    {
        if (price <= 0)
        {
            return false;
        }
    }

    return !prices.empty() && points > 0;
}

bool TextSpendPointsRequest::valid(const TextSpendPointsResponse& response) const
{
    u64 total = 0;

    for (const string& purchase : response.purchases)
    {
        auto it = prices.find(purchase);

        if (it == prices.end())
        {
            return false;
        }

        total += it->second;
    }

    return total <= points;
}

template<>
TextSpendPointsRequest YAMLNode::convert() const
{
    TextSpendPointsRequest request;

    request.title = at("title").as<string>();
    request.prices = at("prices").as<map<string, u64>>();
    request.points = at("points").as<u64>();

    return request;
}

template<>
void YAMLSerializer::emit(TextSpendPointsRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("prices", v.prices);
    emitPair("points", v.points);

    endMapping();
}

bool TextAssignPointsRequest::operator==(const TextAssignPointsRequest& rhs) const
{
    return title == rhs.title &&
        keys == rhs.keys &&
        points == rhs.points;
}

bool TextAssignPointsRequest::operator!=(const TextAssignPointsRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextAssignPointsRequest::valid() const
{
    return !keys.empty() && points > 0;
}

bool TextAssignPointsRequest::valid(const TextAssignPointsResponse& response) const
{
    if (response.assignment.size() != keys.size())
    {
        return false;
    }

    u64 total = 0;

    for (const auto& [ key, value ] : response.assignment)
    {
        auto keyIt = keys.find(key);

        if (keyIt == keys.end())
        {
            return false;
        }

        total += value;
    }

    return total <= points;
}

template<>
TextAssignPointsRequest YAMLNode::convert() const
{
    TextAssignPointsRequest request;

    request.title = at("title").as<string>();
    request.keys = at("keys").as<set<string>>();
    request.points = at("points").as<u64>();

    return request;
}

template<>
void YAMLSerializer::emit(TextAssignPointsRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("keys", v.keys);
    emitPair("points", v.points);

    endMapping();
}

bool TextVoteRequest::operator==(const TextVoteRequest& rhs) const
{
    return title == rhs.title &&
        options == rhs.options &&
        votes == rhs.votes;
}

bool TextVoteRequest::operator!=(const TextVoteRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextVoteRequest::valid() const
{
    return !options.empty() && votes > 0;
}

bool TextVoteRequest::valid(const TextVoteResponse& response) const
{
    if (response.votes.size() != votes)
    {
        return false;
    }

    for (u64 vote : response.votes)
    {
        if (vote >= options.size())
        {
            return false;
        }
    }

    return true;
}

template<>
TextVoteRequest YAMLNode::convert() const
{
    TextVoteRequest request;

    request.title = at("title").as<string>();
    request.options = at("options").as<vector<string>>();
    request.votes = at("votes").as<u64>();

    return request;
}

template<>
void YAMLSerializer::emit(TextVoteRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("options", v.options);
    emitPair("votes", v.votes);

    endMapping();
}

bool TextChooseMajorRequest::operator==(const TextChooseMajorRequest& rhs) const
{
    return title == rhs.title &&
        options == rhs.options;
}

bool TextChooseMajorRequest::operator!=(const TextChooseMajorRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextChooseMajorRequest::valid() const
{
    return !options.empty();
}

bool TextChooseMajorRequest::valid(const TextChooseMajorResponse& response) const
{
    return response.choice < options.size();
}

template<>
TextChooseMajorRequest YAMLNode::convert() const
{
    TextChooseMajorRequest request;

    request.title = at("title").as<string>();

    for (const YAMLNode& node : at("options").as<vector<YAMLNode>>())
    {
        request.options.push_back({
            node.at(0).as<string>(),
            node.at(1).as<string>(),
            node.at(2).as<string>()
        });
    }

    return request;
}

template<>
void YAMLSerializer::emit(TextChooseMajorRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emit("options");
    startSequence();

    for (const auto& [ title, subtitle, text ] : v.options)
    {
        startSequence();

        emit(title);
        emit(subtitle);
        emit(text);

        endSequence();
    }

    endSequence();

    endMapping();
}

bool TextKnowledgeRequest::operator==(const TextKnowledgeRequest& rhs) const
{
    return title == rhs.title &&
        subtitle == rhs.subtitle &&
        text == rhs.text;
}

bool TextKnowledgeRequest::operator!=(const TextKnowledgeRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextKnowledgeRequest::valid() const
{
    return true;
}

template<>
TextKnowledgeRequest YAMLNode::convert() const
{
    TextKnowledgeRequest request;

    request.title = at("title").as<string>();
    request.subtitle = at("subtitle").as<string>();
    request.text = at("text").as<string>();

    return request;
}

template<>
void YAMLSerializer::emit(TextKnowledgeRequest v)
{
    startMapping();

    emitPair("title", v.title);
    emitPair("subtitle", v.subtitle);
    emitPair("text", v.text);

    endMapping();
}

bool TextClearRequest::operator==(const TextClearRequest& rhs) const
{
    return type == rhs.type &&
        title == rhs.title;
}

bool TextClearRequest::operator!=(const TextClearRequest& rhs) const
{
    return !(*this == rhs);
}

bool TextClearRequest::valid() const
{
    return type >= Text_Clear_All_But_Knowledge && type <= Text_Clear_Specific_Knowledge;
}
