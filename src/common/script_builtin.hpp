/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script_types.hpp"

class ScriptContext;

enum ScriptBuiltinAuth : u8
{
    Script_Builtin_Auth_None,
    Script_Builtin_Auth_User,
    Script_Builtin_Auth_Admin
};

enum ScriptBuiltinType : u8
{
    Script_Builtin_Normal,
    Script_Builtin_Syntax,
    // This is doesn't have named arguments but also doesn't suspend argument evaluation
    Script_Builtin_Variadic
};

class ScriptBuiltin
{
public:
    ScriptBuiltin(
        const string& name,
        const vector<string>& argNames,
        const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
        ScriptBuiltinAuth auth = Script_Builtin_Auth_None
    );
    ScriptBuiltin(
        const string& name,
        bool variadic,
        const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
        ScriptBuiltinAuth auth = Script_Builtin_Auth_None
    );

    const string& name() const;
    const vector<string>& argNames() const;
    ScriptBuiltinType type() const;
    ScriptBuiltinAuth auth() const;

    Value call(ScriptContext* ctx, vector<Value> args) const;

private:
    string _name;
    vector<string> _argNames;
    ScriptBuiltinType _type;
    ScriptBuiltinAuth _auth;
    function<Value(const string&, ScriptContext*, const vector<Value>&)> behavior;
};

pair<string, ScriptBuiltin> makeBuiltin(
    const string& name,
    const vector<string>& argNames,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth = Script_Builtin_Auth_None
);

pair<string, ScriptBuiltin> makeVariadicBuiltin(
    const string& name,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth = Script_Builtin_Auth_None
);

pair<string, ScriptBuiltin> makeSyntaxBuiltin(
    const string& name,
    const function<Value(const string&, ScriptContext*, const vector<Value>&)>& behavior,
    ScriptBuiltinAuth auth = Script_Builtin_Auth_None
);

pair<string, ScriptBuiltin> clientSideStub(const string& name, const Value& value = Value());
