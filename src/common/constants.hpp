/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

static constexpr Version version = { 0, 7, 0 };

static constexpr const char* const gameName = "LambdaMod";
static constexpr const char* const appID = "lambdamod";
static constexpr const char* const appClass = "lambdamod";
static const string gameVersion = to_string(version.major) + "." + to_string(version.minor) + "." + to_string(version.patch);

static constexpr i32 indentSize = 2;

static constexpr f64 brakeDuration = 2; // seconds

static constexpr u32 minMagnitude = 0;
static constexpr u32 maxMagnitude = 3;
static constexpr u32 magnitudeLevelCount = maxMagnitude + 1;

static constexpr f64 advantageMultiplier = 1.2;

static constexpr f64 pi = 3.14159;
static constexpr f64 tau = pi * 2;

static constexpr f64 epsilon = 1e-6;

static constexpr f64 defaultCameraAngle = pi / 2; // 90 deg
static constexpr f64 defaultNearDistance = 0.01;
static constexpr f64 defaultFarDistance = 1'000'000;

static constexpr f64 defaultSpeedOfSound = 343;

static constexpr vec3 defaultGravity = { 0, 0, -9.81 };
static constexpr f64 defaultDensity = 1.225;

static constexpr chrono::milliseconds textTitlecardDuration(10 * 1000);
static constexpr chrono::milliseconds voteDuration(30 * 1000);

static constexpr i32 gamepadPointerSpeed = 1024;

static constexpr chrono::milliseconds minButtonInterval(500);

static constexpr f32 lineWidth = 0.01;

static constexpr const char* const routingServerPrefix = "https://lambdamod.org";

static constexpr chrono::seconds maxHolePunchDuration(5);
static constexpr const char* const holePunchHost = "0.0.0.0";

static constexpr f64 defaultSkyBrightness = 20'000;
static constexpr vec3 defaultSkyZenithColor = vec3(0.101, 0.313, 0.49);
static constexpr vec3 defaultSkyHorizonColor = vec3(0.713, 0.772, 0.796);

static constexpr f64 defaultAtmosphereLightningBrightness = 40'000;
static constexpr vec3 defaultAtmosphereLightningColor = vec3(1);

static constexpr i32 bodyPartCount = 2;
static constexpr i32 eyeCount = bodyPartCount;
static constexpr i32 earCount = bodyPartCount;
static constexpr i32 handCount = bodyPartCount;

static constexpr f64 defaultVoiceVolume = 0.0000001;
static constexpr f64 unattachedVoiceVolume = 1;

static constexpr u32 spaceGraphDivisionCount = 2;
static constexpr u32 maxSpaceGraphDepth = 16;

static constexpr f64 interEarDistance = 0.2;

static constexpr f64 minUIScale = 0.01;

static constexpr f64 defaultTerrainSize = 4;
static constexpr f64 defaultTerrainSpacing = 0.5;

static constexpr f64 minStructureWallThickness = 0.005; // m

static constexpr f64 minFieldOfView = (0.1 / 180) * pi; // rad

static constexpr vec3 icosahedronRingStart = vec3(0, 0.894425, 0.447215);

static constexpr vec3 dodecahedronRingStart = vec3(0, 0.607063, 0.794654);
static constexpr vec3 dodecahedronMiddleRingStart = vec3(0, 0.982247, 0.187592);

static constexpr u32 terrainSegments = 8;

static constexpr chrono::seconds pingInterval(1);

static constexpr f64 particleDragCoefficient = 0.5;

static constexpr size_t minAreaPoints = 3;

static constexpr u32 canvasResolution = 512;
