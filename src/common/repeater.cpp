/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "repeater.hpp"
#include "yaml_tools.hpp"
#include "script_tools.hpp"
#include "game_state.hpp"
#include "scope_closure.hpp"

Repeater::Repeater()
{

}

Repeater::Repeater(const Lambda& lambda, UserID userID)
    : lambda(lambda)
    , userID(userID)
{

}

void Repeater::step(GameState* game, f64 stepInterval) const
{
    User* user = game->getUser(userID);

    if (user)
    {
        ScopeClosure closure(game->context());

        runSafely(game->context(), lambda, { toValue(stepInterval) }, "running repeater hook", user);
    }
}

template<>
Repeater YAMLNode::convert() const
{
    Repeater repeater;

    ScriptContext context;

    repeater.lambda = runSafely(
        &context,
        at("lambda").as<string>(),
        "loading repeater lambda"
    )->front().lambda;
    repeater.userID = at("userID").as<UserID>();

    return repeater;
}

template<>
void YAMLSerializer::emit(Repeater v)
{
    startMapping();

    emitPair("lambda", v.lambda.toString());
    emitPair("userID", v.userID);

    endMapping();
}
