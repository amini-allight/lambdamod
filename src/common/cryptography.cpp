/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "cryptography.hpp"
#include "constants.hpp"

#include <openssl/x509.h>
#include <openssl/rand.h>
#include <openssl/sha.h>
#include <openssl/pem.h>

tuple<string, string> randomRSAKeyPair()
{
    EVP_PKEY* keypair = nullptr;
    BIO* publicWriter = BIO_new(BIO_s_mem());
    BIO* privateWriter = BIO_new(BIO_s_mem());
    EVP_PKEY_CTX* ctx = EVP_PKEY_CTX_new_id(EVP_PKEY_RSA, nullptr);

    EVP_PKEY_keygen_init(ctx);

    EVP_PKEY_CTX_set_rsa_keygen_bits(ctx, 4096);

    EVP_PKEY_keygen(ctx, &keypair);

    PEM_write_bio_PUBKEY(publicWriter, keypair);
    string publicKeyText(BIO_pending(publicWriter), '\0');
    BIO_read(publicWriter, publicKeyText.data(), publicKeyText.size());

    PEM_write_bio_PrivateKey(privateWriter, keypair, nullptr, nullptr, 0, nullptr, nullptr);
    string privateKeyText(BIO_pending(privateWriter), '\0');
    BIO_read(privateWriter, privateKeyText.data(), privateKeyText.size());

    EVP_PKEY_free(keypair);
    EVP_PKEY_CTX_free(ctx);
    BIO_free_all(privateWriter);
    BIO_free_all(publicWriter);

    return { publicKeyText, privateKeyText };
}

string certificateFromKey(const string& privateKey)
{
    X509* cert = X509_new();
    BIO* certWriter = BIO_new(BIO_s_mem());
    BIO* keyReader = BIO_new_mem_buf(privateKey.data(), -1);

    EVP_PKEY* key;
    key = PEM_read_bio_PrivateKey(keyReader, nullptr, nullptr, 0);

    ASN1_INTEGER_set(X509_get_serialNumber(cert), 1);

    X509_gmtime_adj(X509_get_notBefore(cert), 0);
    X509_gmtime_adj(X509_get_notAfter(cert), 315'360'000);

    X509_set_pubkey(cert, key);

    X509_NAME* name = X509_get_subject_name(cert);

    X509_NAME_add_entry_by_txt(name, "CN", MBSTRING_ASC, reinterpret_cast<const u8*>(gameName), -1, -1, 0);

    X509_set_issuer_name(cert, name);

    X509_sign(cert, key, EVP_sha256());

    PEM_write_bio_X509(certWriter, cert);

    string certText(BIO_pending(certWriter), '\0');
    BIO_read(certWriter, certText.data(), certText.size());

    EVP_PKEY_free(key);
    BIO_free_all(keyReader);
    BIO_free_all(certWriter);
    X509_free(cert);

    return certText;
}

string toHex(const string& data)
{
    string s;

    char characters[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };

    for (size_t i = 0; i < data.size(); i++)
    {
        u8 b = data[i];

        s += characters[(b >> 4)];
        s += characters[(b & 0b00001111)];
    }

    return s;
}

string fromHex(const string& text)
{
    string s = text;

    if (text.size() % 2 != 0)
    {
        s = '0' + s;
    }

    string data(s.size() / 2, '\0');

    for (size_t i = 0; i < s.size(); i += 2)
    {
        data[i / 2] = stol(s.substr(i, 2), 0, 16);
    }

    return data;
}

string randomSalt(size_t size)
{
    string s(size, '\0');

    RAND_bytes(reinterpret_cast<u8*>(s.data()), s.size());

    return s;
}

string sha256(const string& text)
{
    u8 hash[SHA256_DIGEST_LENGTH];

    SHA256(reinterpret_cast<const u8*>(text.data()), text.size(), hash);

    return string(reinterpret_cast<const char*>(hash), SHA256_DIGEST_LENGTH);
}
