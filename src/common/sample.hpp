/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"
#include "sample_part.hpp"
#include "sample_sound_data.hpp"
#include "yaml_tools.hpp"

class EntityUpdate;
class NetworkEvent;

class Sample
{
public:
    Sample();

    void verify();

    void compare(const string& name, const Sample& previous, EntityUpdate& update) const;

    void push(const NetworkEvent& event);
    vector<NetworkEvent> initial(const string& world, EntityID id, const string& name) const;

    SampleSoundData toSound() const;

    SamplePartID add();
    void add(const SamplePart& part);
    void remove(SamplePartID id);

    const map<SamplePartID, SamplePart>& parts() const;

    u32 length() const;
    u32 sampleCount() const;

    bool operator==(const Sample& rhs) const;
    bool operator!=(const Sample& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            nextPartID,
            _parts
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        nextPartID = get<0>(payload);
        _parts = get<1>(payload);
    }

    typedef msgpack::type::tuple<SamplePartID, map<SamplePartID, SamplePart>> layout;

private:
    template<typename T>
    friend T YAMLNode::convert() const;

    template<typename T>
    friend void YAMLSerializer::emit(T v);

    SamplePartID nextPartID;
    map<SamplePartID, SamplePart> _parts;
};
