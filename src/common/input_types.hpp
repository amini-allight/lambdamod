/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum InputType : u32
{
    Input_Null,

    Input_0,
    Input_1,
    Input_2,
    Input_3,
    Input_4,
    Input_5,
    Input_6,
    Input_7,
    Input_8,
    Input_9,

    Input_A,
    Input_B,
    Input_C,
    Input_D,
    Input_E,
    Input_F,
    Input_G,
    Input_H,
    Input_I,
    Input_J,
    Input_K,
    Input_L,
    Input_M,
    Input_N,
    Input_O,
    Input_P,
    Input_Q,
    Input_R,
    Input_S,
    Input_T,
    Input_U,
    Input_V,
    Input_W,
    Input_X,
    Input_Y,
    Input_Z,

    Input_F1,
    Input_F2,
    Input_F3,
    Input_F4,
    Input_F5,
    Input_F6,
    Input_F7,
    Input_F8,
    Input_F9,
    Input_F10,
    Input_F11,
    Input_F12,

    Input_Escape,
    Input_Backspace,
    Input_Tab,
    Input_Caps_Lock,
    Input_Return,
    Input_Space,
    Input_Menu,

    Input_Grave_Accent,
    Input_Minus,
    Input_Equals,
    Input_Left_Brace,
    Input_Right_Brace,
    Input_Backslash,
    Input_Semicolon,
    Input_Quote,
    Input_Period,
    Input_Comma,
    Input_Slash,

    Input_Left_Shift,
    Input_Left_Control,
    Input_Left_Super,
    Input_Left_Alt,
    Input_Right_Shift,
    Input_Right_Control,
    Input_Right_Super,
    Input_Right_Alt,

    Input_Print_Screen,
    Input_Insert,
    Input_Delete,
    Input_Scroll_Lock,
    Input_Home,
    Input_End,
    Input_Pause,
    Input_Page_Up,
    Input_Page_Down,

    Input_Left,
    Input_Right,
    Input_Up,
    Input_Down,

    Input_Num_Lock,
    Input_Num_Add,
    Input_Num_Subtract,
    Input_Num_Multiply,
    Input_Num_Divide,
    Input_Num_0,
    Input_Num_1,
    Input_Num_2,
    Input_Num_3,
    Input_Num_4,
    Input_Num_5,
    Input_Num_6,
    Input_Num_7,
    Input_Num_8,
    Input_Num_9,
    Input_Num_Period,
    Input_Num_Return,

    Input_Left_Mouse,
    Input_Middle_Mouse,
    Input_Right_Mouse,
    Input_X1_Mouse,
    Input_X2_Mouse,
    Input_Mouse_Wheel_Up,
    Input_Mouse_Wheel_Down,
    Input_Mouse_Wheel_Left,
    Input_Mouse_Wheel_Right,
    Input_Mouse_Move,

    Input_Gamepad_DPad_Left,
    Input_Gamepad_DPad_Right,
    Input_Gamepad_DPad_Up,
    Input_Gamepad_DPad_Down,
    Input_Gamepad_A,
    Input_Gamepad_B,
    Input_Gamepad_X,
    Input_Gamepad_Y,
    Input_Gamepad_Left_Stick,
    Input_Gamepad_Left_Stick_Click,
    Input_Gamepad_Left_Bumper,
    Input_Gamepad_Left_Trigger,
    Input_Gamepad_Right_Stick,
    Input_Gamepad_Right_Stick_Click,
    Input_Gamepad_Right_Bumper,
    Input_Gamepad_Right_Trigger,
    Input_Gamepad_Back,
    Input_Gamepad_Guide,
    Input_Gamepad_Start,

    Input_Touch,

    Input_VR_Heartrate,

    Input_VR_System_Click,
    Input_VR_Volume_Up,
    Input_VR_Volume_Down,
    Input_VR_Mute_Microphone,

    Input_VR_Head,
    Input_VR_Hips,
    Input_VR_Left_Hand,
    Input_VR_Right_Hand,
    Input_VR_Left_Foot,
    Input_VR_Right_Foot,
    Input_VR_Chest,
    Input_VR_Left_Shoulder,
    Input_VR_Right_Shoulder,
    Input_VR_Left_Elbow,
    Input_VR_Right_Elbow,
    Input_VR_Left_Wrist,
    Input_VR_Right_Wrist,
    Input_VR_Left_Knee,
    Input_VR_Right_Knee,
    Input_VR_Left_Ankle,
    Input_VR_Right_Ankle,

    Input_VR_Eyes,

    Input_VR_Left_Eye,
    Input_VR_Left_Eyebrow,
    Input_VR_Left_Eyelid,
    Input_VR_Left_Pupil,
    Input_VR_Right_Eye,
    Input_VR_Right_Eyebrow,
    Input_VR_Right_Eyelid,
    Input_VR_Right_Pupil,
    Input_VR_Jaw,
    Input_VR_Upper_Lip,
    Input_VR_Lower_Lip,
    Input_VR_Mouth,
    Input_VR_Left_Cheek,
    Input_VR_Right_Cheek,
    Input_VR_Tongue,

    Input_VR_Left_Thumb,
    Input_VR_Left_Index,
    Input_VR_Left_Middle,
    Input_VR_Left_Ring,
    Input_VR_Left_Little,

    Input_VR_Right_Thumb,
    Input_VR_Right_Index,
    Input_VR_Right_Middle,
    Input_VR_Right_Ring,
    Input_VR_Right_Little,

    Input_VR_Left_Trigger_Touch,
    Input_VR_Left_Trigger,
    Input_VR_Left_Trigger_Click,
    Input_VR_Left_Grip_Touch,
    Input_VR_Left_Grip,
    Input_VR_Left_Grip_Click,
    Input_VR_Left_Stick_Touch,
    Input_VR_Left_Stick,
    Input_VR_Left_Stick_Click,

    Input_VR_Left_Touchpad_Touch,
    Input_VR_Left_Touchpad,
    Input_VR_Left_Touchpad_Press,
    Input_VR_Left_Touchpad_Click,
    Input_VR_Left_System_Touch,
    Input_VR_Left_System,
    Input_VR_Left_A_Touch,
    Input_VR_Left_A,
    Input_VR_Left_B_Touch,
    Input_VR_Left_B,
    Input_VR_Left_Menu_Touch,
    Input_VR_Left_Menu,
    Input_VR_Left_Bumper_Touch,
    Input_VR_Left_Bumper,
    Input_VR_Left_Thumbrest,
    Input_VR_Left_Thumbrest_Touch,

    Input_VR_Right_Trigger_Touch,
    Input_VR_Right_Trigger,
    Input_VR_Right_Trigger_Click,
    Input_VR_Right_Grip_Touch,
    Input_VR_Right_Grip,
    Input_VR_Right_Grip_Click,
    Input_VR_Right_Stick_Touch,
    Input_VR_Right_Stick,
    Input_VR_Right_Stick_Click,

    Input_VR_Right_Touchpad_Touch,
    Input_VR_Right_Touchpad,
    Input_VR_Right_Touchpad_Press,
    Input_VR_Right_Touchpad_Click,
    Input_VR_Right_System_Touch,
    Input_VR_Right_System,
    Input_VR_Right_A_Touch,
    Input_VR_Right_A,
    Input_VR_Right_B_Touch,
    Input_VR_Right_B,
    Input_VR_Right_Menu_Touch,
    Input_VR_Right_Menu,
    Input_VR_Right_Bumper_Touch,
    Input_VR_Right_Bumper,
    Input_VR_Right_Thumbrest,
    Input_VR_Right_Thumbrest_Touch,

    Input_Count
};

string inputTypeToString(InputType type);
InputType inputTypeFromString(const string& s);

bool isKeyboard(InputType type);
bool isMouse(InputType type);
bool isGamepad(InputType type);
bool isTouch(InputType type);
bool isFlat(InputType type);
bool isVR(InputType type);
bool isLeft(InputType type);
bool isRight(InputType type);
bool isLeftHand(InputType type);
bool isRightHand(InputType type);

typedef array<f64, 6> InputDimensions;

InputDimensions emptyDimensions();

struct InputTouch
{
    InputTouch();
    InputTouch(u32 id, f64 major, f64 minor, f64 orientation, f64 force);

    bool operator==(const InputTouch& rhs) const;
    bool operator!=(const InputTouch& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            id,
            major,
            minor,
            orientation,
            force
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        id = get<0>(payload);
        major = get<1>(payload);
        minor = get<2>(payload);
        orientation = get<3>(payload);
        force = get<4>(payload);
    }

    typedef msgpack::type::tuple<u32, f64, f64, f64, f64> layout;

    u32 id;
    f64 major;
    f64 minor;
    f64 orientation;
    f64 force;
};

struct Input
{
    Input();
    explicit Input(
        InputType type,
        bool up = false,
        f64 intensity = 0,
        const vec3& position = vec3(),
        const quaternion& rotation = quaternion(),
        const InputDimensions& dimensions = emptyDimensions(),
        const InputTouch& touch = InputTouch()
    );

    bool operator==(const Input& rhs) const;
    bool operator!=(const Input& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u32>(type),
            up,
            intensity,
            position,
            rotation,
            linearMotion,
            angularMotion,
            dimensions,
            touch,
            shift,
            control,
            alt,
            super
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<InputType>(get<0>(payload));
        up = get<1>(payload);
        intensity = get<2>(payload);
        position = get<3>(payload);
        rotation = get<4>(payload);
        linearMotion = get<5>(payload);
        angularMotion = get<6>(payload);
        dimensions = get<7>(payload);
        touch = get<8>(payload);
        shift = get<9>(payload);
        control = get<10>(payload);
        alt = get<11>(payload);
        super = get<12>(payload);
    }

    bool isKeyboard() const;
    bool isMouse() const;
    bool isGamepad() const;
    bool isTouch() const;
    bool isFlat() const;
    bool isVR() const;
    bool isLeft() const;
    bool isRight() const;
    bool isLeftHand() const;
    bool isRightHand() const;
    string text() const;

    typedef msgpack::type::tuple<u32, bool, f64, vec3, quaternion, vec3, vec3, InputDimensions, InputTouch, bool, bool, bool, bool> layout;

    InputType type;
    bool up;
    f64 intensity;
    vec3 position;
    quaternion rotation;
    vec3 linearMotion;
    vec3 angularMotion;
    InputDimensions dimensions;
    InputTouch touch;

    bool shift;
    bool control;
    bool alt;
    bool super;

    // Field to carry event serial through to clipboard operations
#if defined(LMOD_CLIENT) && defined(LMOD_WAYLAND)
    u32 serial;
#endif
};

ostream& operator<<(ostream& out, const Input& input);

struct InputBinding
{
    InputBinding();
    explicit InputBinding(const string& input);

    void verify();

    bool operator==(const InputBinding& rhs) const;
    bool operator!=(const InputBinding& rhs) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            input
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        input = get<0>(payload);
    }

    typedef msgpack::type::tuple<string> layout;

    string input;
};

enum HapticTarget : u8
{
    Haptic_Target_Left_Hand,
    Haptic_Target_Right_Hand
};

HapticTarget hapticTargetFromString(const string& s);
string hapticTargetToString(HapticTarget target);

struct HapticEffect
{
    HapticEffect();
    HapticEffect(HapticTarget target, u32 duration, f64 frequency, f64 amplitude);

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            static_cast<u8>(target),
            duration,
            frequency,
            amplitude
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        target = static_cast<HapticTarget>(get<0>(payload));
        duration = get<1>(payload);
        frequency = get<2>(payload);
        amplitude = get<3>(payload);
    }

    typedef msgpack::type::tuple<u8, u32, f64, f64> layout;

    HapticTarget target;
    u32 duration;
    f64 frequency;
    f64 amplitude;
};
