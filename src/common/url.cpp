/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "url.hpp"
#include "log.hpp"
#include "tools.hpp"

#include <curl/curl.h>

static unsigned int onBody(char* in, unsigned int size, unsigned int nmemb, void* userdata)
{
    auto joinURL = static_cast<string*>(userdata);

    *joinURL += string(in, size * nmemb);

    return size * nmemb;
}

optional<string> generateJoinURL(u16 port, const string& serverPassword)
{
    string url = string(routingServerPrefix) + "/action/generate-invite?port=" + to_string(port) + "&server-password=" + urlEncode(serverPassword);

    CURL* curl = curl_easy_init();

    if (!curl)
    {
        error("Failed to initialize curl.");
        return {};
    }

    string joinURL;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, onBody);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &joinURL);

    CURLcode result = curl_easy_perform(curl);

    if (result != CURLE_OK)
    {
        error("Failed to request join URL: " + string(curl_easy_strerror(result)));
        return {};
    }

    curl_easy_cleanup(curl);

    return joinURL;
}

optional<string> createSession(const string& serverPassword)
{
    string url = string(routingServerPrefix) + "/action/create-session?server-password=" + urlEncode(serverPassword);

    CURL* curl = curl_easy_init();

    if (!curl)
    {
        error("Failed to initialize curl.");
        return {};
    }

    string sessionID;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, onBody);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &sessionID);

    CURLcode result = curl_easy_perform(curl);

    if (result != CURLE_OK)
    {
        error("Failed to create session: " + string(curl_easy_strerror(result)));
        return {};
    }

    curl_easy_cleanup(curl);

    if (sessionID.empty())
    {
        return {};
    }

    return sessionID;
}

optional<tuple<string, u16>> offerSession(const string& sessionID, u16 port)
{
    string url = string(routingServerPrefix) + "/action/offer-session?session-id=" + sessionID + "&port=" + to_string(port);

    CURL* curl = curl_easy_init();

    if (!curl)
    {
        error("Failed to initialize curl.");
        return {};
    }

    string body;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, onBody);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &body);

    CURLcode result = curl_easy_perform(curl);

    if (result != CURLE_OK)
    {
        error("Failed to get next session joiner: " + string(curl_easy_strerror(result)));
        return {};
    }

    curl_easy_cleanup(curl);

    smatch m;
    if (!regex_match(body, m, regex(".*\n[0-9]+")))
    {
        return {};
    }

    size_t i = 0;
    string s;

    string clientHost;

    for (; i < body.size(); i++)
    {
        char c = body[i];

        if (c == '\n')
        {
            clientHost = s;
            s = "";
            i++;
            break;
        }
        else
        {
            s += c;
        }
    }

    u16 clientPort = stoi(body.substr(i, body.size() - i));

    if (clientHost.empty())
    {
        return {};
    }

    return tuple<string, u16>(clientHost, clientPort);
}

optional<tuple<string, u16, string>> joinSession(const string& sessionID, u16 port)
{
    string url = string(routingServerPrefix) + "/action/join-session?session-id=" + sessionID + "&port=" + to_string(port);

    CURL* curl = curl_easy_init();

    if (!curl)
    {
        error("Failed to initialize curl.");
        return {};
    }

    string body;

    curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, true);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, onBody);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, &body);

    CURLcode result = curl_easy_perform(curl);

    if (result != CURLE_OK)
    {
        error("Failed to join session: " + string(curl_easy_strerror(result)));
        return {};
    }

    curl_easy_cleanup(curl);

    smatch m;
    if (!regex_match(body, m, regex(".*\n[0-9]+\n.*")))
    {
        return {};
    }

    size_t i = 0;
    string s;

    string serverHost;

    for (; i < body.size(); i++)
    {
        char c = body[i];

        if (c == '\n')
        {
            serverHost = s;
            s = "";
            i++;
            break;
        }
        else
        {
            s += c;
        }
    }

    u16 serverPort = 0;

    for (; i < body.size(); i++)
    {
        char c = body[i];

        if (c == '\n')
        {
            serverPort = stoi(s);
            s = "";
            i++;
            break;
        }
        else
        {
            s += c;
        }
    }

    string serverPassword = body.substr(i, body.size() - i);

    if (serverHost.empty())
    {
        return {};
    }

    return tuple<string, u16, string>(
        serverHost,
        serverPort,
        serverPassword
    );
}
