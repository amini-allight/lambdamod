/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "bounding_box.hpp"
#include "body.hpp"
#include "dynamics_animation_target.hpp"
#include "dynamics_ik_effect.hpp"
#include "dynamics_force.hpp"
#include "dynamics_force_application.hpp"
#include "dynamics_area.hpp"
#include "dynamics_mesh.hpp"

#define BT_USE_DOUBLE_PRECISION
#include <bullet/btBulletDynamicsCommon.h>

class Dynamics;
class Entity;
class DynamicsComponent;

enum MotorID
{
    Motor_Linear_X,
    Motor_Linear_Y,
    Motor_Linear_Z,
    Motor_Angular_X,
    Motor_Angular_Y,
    Motor_Angular_Z
};

class DynamicsComponent
{
public:
    DynamicsComponent(Dynamics* dynamics, DynamicsComponent* parent, Entity* entity);
    DynamicsComponent(const DynamicsComponent& rhs) = delete;
    DynamicsComponent(DynamicsComponent&& rhs) = delete;
    virtual ~DynamicsComponent();

    DynamicsComponent& operator=(const DynamicsComponent& rhs) = delete;
    DynamicsComponent& operator=(DynamicsComponent&& rhs) = delete;

    tuple<vector<DynamicsForceApplication>, vector<DynamicsArea>> getEnvironmentalEffects();
    void applyEnvironmentalEffects(vector<DynamicsForceApplication>* forces, const vector<DynamicsArea>& areas);

    vector<DynamicsIKEffect> generateIKEffects(
        f64 stepInterval,
        const map<BodyPartID, DynamicsAnimationTarget>& pose,
        const mat4& previousTransform = {}
    );
    virtual void applyIKEffect(const vec3& translation, const quaternion& rotation) = 0;

    const Body& creationBody() const;
    f64 creationMass() const;

    f64 mass() const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;
    f64 volume() const;
    f64 buoyancyVolume() const;
    f64 angularCrossSectionalArea() const;
    f64 linearCrossSectionalArea(const vec3& globalDirection) const;

    void setGlobalTransform(const mat4& transform);
    mat4 globalTransform() const;
    void setScale(const vec3& scale);
    vec3 scale() const;
    void setLinearVelocity(const vec3& linV);
    vec3 linearVelocity() const;
    void setAngularVelocity(const vec3& angV);
    vec3 angularVelocity() const;

    void setFriction(f64 friction);
    f64 friction() const;
    void setRestitution(f64 restitution);
    f64 restitution() const;

    virtual void writeback();

    void applyLinearForce(const vec3& position, const vec3& force);
    void applyLinearForce(const vec3& force);
    void applyAngularForce(const vec3& force);

protected:
    friend class DynamicsComponentEntity;
    friend class DynamicsComponentPart;
    friend class DynamicsForce;

    Dynamics* dynamics;
    DynamicsComponent* parent;
    vector<DynamicsComponent*> children;
    vector<DynamicsForce> forces;
    vector<DynamicsArea> areas;
    Entity* entity;
    btCompoundShape* shape;
    vector<btCollisionShape*> shapeParts;
    vector<DynamicsMesh*> shapeMeshes;
    btMotionState* motionState;
    btRigidBody* body;
    btGeneric6DofSpring2Constraint* constraint;

    Body _creationBody;
    f64 _creationMass;

    f64 _mass;
    f64 _boundingRadius;
    BoundingBox _boundingBox;

    btTransform inBodySpace(const mat4& transform) const;
    void addToShape(BodyPart* part);

    virtual vec3 constrainTranslation(const vec3& translation) const;
    virtual quaternion constrainRotation(const quaternion& rotation) const;

    virtual bool supportsAnimation() const = 0;
    // TODO: ANIMATION: Eventually override this to support constraints with linear freedom of movement
    virtual bool supportsTranslation() const;

    void createShape(const vec3& scale);
    void destroyShape();

    void createBody(const vec3& position, const quaternion& rotation, f64 mass);
    void destroyBody();

    virtual void createConstraint() = 0;
    void destroyConstraint();

    virtual void createChildren() = 0;
    set<BodyPartID> createChildren(BodyPart* part);
    void destroyChildren();
};
