/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "polygon_triangulation.hpp"
#include "tools.hpp"

static bool intersectLineLine(const vec2& a, const vec2& b, const vec2& c, const vec2& d)
{
    f64 s = ((a.x - c.x) * (c.y - d.y) - (a.y - c.y) * (c.x - d.x))
        / ((a.x - b.x) * (c.y - d.y) - (a.y - b.y) * (c.x - d.x));

    f64 t = ((a.x - b.x) * (a.y - c.y) - (a.y - b.y) * (a.x - c.x))
        / ((a.x - b.x) * (c.y - d.y) - (a.y - b.y) * (c.x - d.x));

    return s >= 0 && s < 1 && t >= 0 && t < 1;
}

struct Edge
{
    u32 aIndex;
    vec2 a;
    u32 bIndex;
    vec2 b;
};

static bool interiorLine(const vector<Edge>& edges, const vec2& a, const vec2& b)
{
    for (const Edge& edge : edges)
    {
        if (roughly(edge.a, a) || roughly(edge.a, b) || roughly(edge.b, a) || roughly(edge.b, b))
        {
            continue;
        }

        if (intersectLineLine(edge.a, edge.b, a, b))
        {
            return false;
        }
    }

    return true;
}

vector<u32> triangulatePolygon(const vector<vec2>& points)
{
    vector<u32> indices;

    vector<Edge> edges;
    edges.reserve(points.size());

    for (u32 i = 0; i < points.size(); i++)
    {
        u32 nextIndex = loopIndex<u32>(i + 1, points.size());

        const vec2& point = points[i];
        const vec2& nextPoint = points[nextIndex];

        edges.push_back({ i, point, nextIndex, nextPoint });
    }

    bool found;

    do
    {
        found = false;

        for (u32 i = 0; i < edges.size(); i++)
        {
            u32 nextIndex = loopIndex<u32>(i + 1, points.size());

            const Edge& edgeA = edges[i];
            const Edge& edgeB = edges[nextIndex];

            if (interiorLine(edges, edgeA.a, edgeB.b))
            {
                indices.push_back(edgeA.aIndex);
                indices.push_back(edgeA.bIndex);
                indices.push_back(edgeB.bIndex);
                Edge newEdge = { edgeA.aIndex, edgeA.a, edgeB.bIndex, edgeB.b };
                edges.erase(edges.begin() + (i < nextIndex ? nextIndex : i));
                edges.erase(edges.begin() + (i < nextIndex ? i : nextIndex));
                edges.insert(edges.begin() + i, newEdge);
                found = true;
                break;
            }
        }
    } while (found && edges.size() >= 3);

    return indices;
}
