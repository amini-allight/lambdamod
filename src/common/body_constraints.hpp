/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"

enum BodyConstraintType : u8
{
    Body_Constraint_Fixed,
    Body_Constraint_Ball,
    Body_Constraint_Cone,
    Body_Constraint_Hinge
};

string bodyConstraintTypeToString(BodyConstraintType type);
BodyConstraintType bodyConstraintTypeFromString(const string& s);

struct BodyConstraintFixed
{
    BodyConstraintFixed();

    void verify();

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {};

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);
    }

    bool operator==(const BodyConstraintFixed& rhs) const;
    bool operator!=(const BodyConstraintFixed& rhs) const;

    typedef msgpack::type::tuple<> layout;
};

struct BodyConstraintBall
{
    BodyConstraintBall();

    void verify();

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            minimum,
            maximum
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        minimum = get<0>(payload);
        maximum = get<1>(payload);
    }

    bool operator==(const BodyConstraintBall& rhs) const;
    bool operator!=(const BodyConstraintBall& rhs) const;

    typedef msgpack::type::tuple<vec3, vec3> layout;

    vec3 minimum;
    vec3 maximum;
};

struct BodyConstraintCone
{
    BodyConstraintCone();

    void verify();

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            span,
            twistMinimum,
            twistMaximum
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        span = get<0>(payload);
        twistMinimum = get<1>(payload);
        twistMaximum = get<2>(payload);
    }

    bool operator==(const BodyConstraintCone& rhs) const;
    bool operator!=(const BodyConstraintCone& rhs) const;

    typedef msgpack::type::tuple<vec2, f64, f64> layout;

    vec2 span;
    f64 twistMinimum;
    f64 twistMaximum;
};

struct BodyConstraintHinge
{
    BodyConstraintHinge();

    void verify();

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            minimum,
            maximum
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        minimum = get<0>(payload);
        maximum = get<1>(payload);
    }

    bool operator==(const BodyConstraintHinge& rhs) const;
    bool operator!=(const BodyConstraintHinge& rhs) const;

    typedef msgpack::type::tuple<f64, f64> layout;

    f64 minimum;
    f64 maximum;
};
