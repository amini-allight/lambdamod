/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifdef LMOD_DEBUG
#include "rate_monitor.hpp"
#include "tools.hpp"

RateMonitorEntry::RateMonitorEntry(RateMonitor* parent, size_t count)
    : parent(parent)
    , _count(count)
    , creationTime(currentTime())
{

}

size_t RateMonitorEntry::count() const
{
    return _count;
}

bool RateMonitorEntry::expired() const
{
    return currentTime() - creationTime > parent->window;
}

RateMonitor::RateMonitor(chrono::milliseconds window)
    : window(window)
{

}

f64 RateMonitor::push(size_t count)
{
    for (auto it = entries.begin(); it != entries.end(); it++)
    {
        if (!it->expired())
        {
            entries.erase(entries.begin(), it);
            break;
        }
    }

    entries.push_back(RateMonitorEntry(this, count));

    size_t total = 0;

    for (const RateMonitorEntry& entry : entries)
    {
        total += entry.count();
    }

    return (total / static_cast<f64>(window.count())) * 1000;
}
#endif
