/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "sound_generators.hpp"
#include "sound_constants.hpp"
#include "constants.hpp"

vector<f32> generateTone(u32 count, const SamplePartTone& tone)
{
    vector<f32> samples(count, 0);

    for (u32 i = 0; i < count; i++)
    {
        f32 x = i / static_cast<f32>(soundFrequency);

        samples[i] = sin(static_cast<f32>(tone.pitch) * static_cast<f32>(tau) * x);
    }

    return samples;
}
