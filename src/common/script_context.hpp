/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "script_types.hpp"
#include "network_event.hpp"
#include "script_value_permissions.hpp"
#include "script_builtin.hpp"

class User;
class GameState;
class ScriptContext;

typedef function<Value(const string&, ScriptContext*, const vector<Value>&)> ScriptFunction;

static constexpr size_t builtinScope = 0;
static constexpr size_t globalScope = 1;

class ScriptContext
{
public:
    ScriptContext();
    ScriptContext(GameState* game);
    ScriptContext(GameState* game, const ScriptContext* rhs);
    ScriptContext(const ScriptContext& rhs) = delete;
    ScriptContext(ScriptContext&& rhs) = delete;
    ~ScriptContext();

    ScriptContext& operator=(const ScriptContext& rhs) = delete;
    ScriptContext& operator=(ScriptContext&& rhs) = delete;

    // Used by the client to get changes to send to the server
    vector<NetworkEvent> compare(const ScriptContext& previous) const;
    vector<NetworkEvent> compareUser(const ScriptContext& previous, const User* user) const;

#if defined(LMOD_SERVER)
    void push(User* user, const ScriptEvent& event);
#elif defined(LMOD_CLIENT)
    void push(const ScriptEvent& event);
#endif
    vector<NetworkEvent> initial() const;
    vector<NetworkEvent> initialUser(const User* user) const;

    void clearUserReferences(UserID id);

    vector<Value> run(const string& s);
    Value run(const Value& value);
    Value run(const Lambda& l, const vector<Value>& args);

    Value bind(const Value& value);

    void push();
    void pop();

    void define(const string& name, const Value& value, const optional<ScriptValuePermissions>& permissions = {});
    void undefine(const string& name);
    void restrictDefine(const string& name, bool state);
    bool isRestricted(const string& name) const;
    void controlDefine(const string& name, UserID id);
    UserID getOwner(const string& name) const;
    bool has(const string& name) const;
    bool hasAnywhere(const string& name) const;
    void set(const string& name, const Value& value);

    optional<Value> get(const string& name) const;
    optional<Value> getLocal(const string& name) const;

    void addBuiltin(
        const string& name,
        const vector<string>& argNames,
        const ScriptFunction& builtin,
        ScriptBuiltinAuth auth = Script_Builtin_Auth_None
    );
    void addBuiltin(
        const string& name,
        bool variadic,
        const ScriptFunction& builtin,
        ScriptBuiltinAuth auth = Script_Builtin_Auth_None
    );
    const ScriptBuiltin* getBuiltin(const string& name) const;

    User* currentUser() const;

    void trace(const string& name);
    void untrace();
    void clearTraceback();
    const deque<string>& traceback() const;

    size_t memorySize() const;

    bool canRead(const User* user, const string& name) const;
    bool canWrite(const User* user, const string& name, bool set = false) const;

    GameState* game;

    deque<map<string, ScriptBuiltin>> builtins;
    deque<map<string, Value>> scopes;
    map<string, ScriptValuePermissions> permissions;

private:
    friend class UserClosure;

    stack<User*> user;

private:
    friend class RecursionDepthClosure;

    size_t recursionDepth;

private:
    deque<string> _traceback;
    size_t _memorySize;

    void acquireMemory(size_t size);
    void releaseMemory(size_t size);
    size_t memorySizeOf(const map<string, Value>& scope) const;

    void copy(const ScriptContext* rhs);
};
