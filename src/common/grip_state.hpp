/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "id_types.hpp"

static constexpr size_t gripStateWindowSize = 10;

struct GripStateFrame
{
    GripStateFrame();
    GripStateFrame(f64 strength, const vec3& linV, const vec3& angV);

    f64 strength;
    Velocities velocities;
};

class GripState
{
public:
    GripState();

    void grip(EntityID entityID);
    void release();
    bool gripping() const;
    EntityID entityID() const;

    void push(f64 strength, const vec3& linV, const vec3& angV);
    bool pulling() const;
    Velocities exitVelocity() const;

private:
    EntityID _entityID;
    GripStateFrame frames[gripStateWindowSize];
};
