/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "space_graph_node.hpp"

SpaceGraphNode::SpaceGraphNode(SpaceGraphGenerationContext ctx)
    : _innerExtent(ctx.innerExtent)
    , _outerExtent(ctx.outerExtent)
    , _depth(ctx.depth)
    , children{{{nullptr}}}
{
    if (ctx.depth == maxSpaceGraphDepth)
    {
        return;
    }

    _center.x = pickCenter(ctx, [](const vec3& v) -> f64 { return v.x; });
    _center.y = pickCenter(ctx, [](const vec3& v) -> f64 { return v.y; });
    _center.z = pickCenter(ctx, [](const vec3& v) -> f64 { return v.z; });

    for (u32 z = 0; z < spaceGraphDivisionCount; z++)
    {
        for (u32 y = 0; y < spaceGraphDivisionCount; y++)
        {
            for (u32 x = 0; x < spaceGraphDivisionCount; x++)
            {
                vec3 childInnerExtent;
                vec3 childOuterExtent;

                switch (x)
                {
                case 0 :
                    childInnerExtent.x = ctx.innerExtent.x;
                    childOuterExtent.x = _center.x;
                    break;
                case 1 :
                    childInnerExtent.x = _center.x;
                    childOuterExtent.x = ctx.outerExtent.x;
                    break;
                }

                switch (y)
                {
                case 0 :
                    childInnerExtent.y = ctx.innerExtent.y;
                    childOuterExtent.y = _center.y;
                    break;
                case 1 :
                    childInnerExtent.y = _center.y;
                    childOuterExtent.y = ctx.outerExtent.y;
                    break;
                }

                switch (z)
                {
                case 0 :
                    childInnerExtent.z = ctx.innerExtent.z;
                    childOuterExtent.z = _center.z;
                    break;
                case 1 :
                    childInnerExtent.z = _center.z;
                    childOuterExtent.z = ctx.outerExtent.z;
                    break;
                }

                if (
                    childOuterExtent.x - childInnerExtent.x == 0 ||
                    childOuterExtent.y - childInnerExtent.y == 0 ||
                    childOuterExtent.z - childInnerExtent.z == 0
                )
                {
                    continue;
                }

                vector<SpaceGraphGenerationEntity> childEntities = selectWithin(ctx.entities, childInnerExtent, childOuterExtent);

                if (childEntities.empty() || (childInnerExtent == _innerExtent && childOuterExtent == _outerExtent))
                {
                    continue;
                }

                SpaceGraphGenerationContext childCtx(
                    childEntities,
                    childInnerExtent,
                    childOuterExtent,
                    ctx.depth + 1
                );

                children[z][y][x] = new SpaceGraphNode(childCtx);
            }
        }
    }
}

SpaceGraphNode::~SpaceGraphNode()
{
    for (u32 z = 0; z < spaceGraphDivisionCount; z++)
    {
        for (u32 y = 0; y < spaceGraphDivisionCount; y++)
        {
            for (u32 x = 0; x < spaceGraphDivisionCount; x++)
            {
                if (children[z][y][x])
                {
                    delete children[z][y][x];
                }
            }
        }
    }
}

vec3 SpaceGraphNode::minExtent() const
{
    return vec3(
        min(_innerExtent.x, _outerExtent.x),
        min(_innerExtent.y, _outerExtent.y),
        min(_innerExtent.z, _outerExtent.z)
    );
}

vec3 SpaceGraphNode::maxExtent() const
{
    return vec3(
        max(_innerExtent.x, _outerExtent.x),
        max(_innerExtent.y, _outerExtent.y),
        max(_innerExtent.z, _outerExtent.z)
    );
}

vec3 SpaceGraphNode::size() const
{
    return maxExtent() - minExtent();
}

const vec3& SpaceGraphNode::innerExtent() const
{
    return _innerExtent;
}

const vec3& SpaceGraphNode::outerExtent() const
{
    return _outerExtent;
}

u32 SpaceGraphNode::depth() const
{
    return _depth;
}

vec3 SpaceGraphNode::center() const
{
    return (minExtent() + maxExtent()) / 2;
}

f64 SpaceGraphNode::boundingRadius() const
{
    return hypot(size().x, hypot(size().y, size().z));
}

bool SpaceGraphNode::isLeaf() const
{
    for (u32 z = 0; z < spaceGraphDivisionCount; z++)
    {
        for (u32 y = 0; y < spaceGraphDivisionCount; y++)
        {
            for (u32 x = 0; x < spaceGraphDivisionCount; x++)
            {
                SpaceGraphNode* child = children[z][y][x];

                if (child)
                {
                    return false;
                }
            }
        }
    }

    return true;
}

bool SpaceGraphNode::isInfinite() const
{
    return
        isinf(_innerExtent.x) ||
        isinf(_innerExtent.y) ||
        isinf(_innerExtent.z) ||
        isinf(_outerExtent.x) ||
        isinf(_outerExtent.y) ||
        isinf(_outerExtent.z);
}

SpaceGraphNode* SpaceGraphNode::at(const uvec3& index)
{
    return children[index.z][index.y][index.x];
}

const SpaceGraphNode* SpaceGraphNode::at(const uvec3& index) const
{
    return children[index.z][index.y][index.x];
}

void SpaceGraphNode::traverse(const function<void(const uvec3&, SpaceGraphNode*)>& behavior)
{
    for (u32 z = 0; z < spaceGraphDivisionCount; z++)
    {
        for (u32 y = 0; y < spaceGraphDivisionCount; y++)
        {
            for (u32 x = 0; x < spaceGraphDivisionCount; x++)
            {
                SpaceGraphNode* child = children[z][y][x];

                if (child)
                {
                    behavior(uvec3(x, y, z), child);

                    child->traverse(behavior);
                }
            }
        }
    }
}

void SpaceGraphNode::traverseAscending(const vec3& position, const function<void(const uvec3&, SpaceGraphNode*)>& behavior)
{
    u32 x = position.x < _center.x ? 0 : 1;
    u32 y = position.y < _center.y ? 0 : 1;
    u32 z = position.z < _center.z ? 0 : 1;

    SpaceGraphNode* child = children[z][y][x];

    if (!child)
    {
        return;
    }

    child->traverseAscending(position, behavior);

    behavior(uvec3(x, y, z), child);
}

void SpaceGraphNode::traverse(const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const
{
    for (u32 z = 0; z < spaceGraphDivisionCount; z++)
    {
        for (u32 y = 0; y < spaceGraphDivisionCount; y++)
        {
            for (u32 x = 0; x < spaceGraphDivisionCount; x++)
            {
                const SpaceGraphNode* child = children[z][y][x];

                if (child)
                {
                    behavior(uvec3(x, y, z), child);

                    child->traverse(behavior);
                }
            }
        }
    }
}

void SpaceGraphNode::traverseAscending(const vec3& position, const function<void(const uvec3&, const SpaceGraphNode*)>& behavior) const
{
    u32 x = position.x < _center.x ? 0 : 1;
    u32 y = position.y < _center.y ? 0 : 1;
    u32 z = position.z < _center.z ? 0 : 1;

    const SpaceGraphNode* child = children[z][y][x];

    if (!child)
    {
        return;
    }

    child->traverseAscending(position, behavior);

    behavior(uvec3(x, y, z), child);
}

void SpaceGraphNode::print(size_t indent)
{
    for (u32 z = 0; z < spaceGraphDivisionCount; z++)
    {
        for (u32 y = 0; y < spaceGraphDivisionCount; y++)
        {
            for (u32 x = 0; x < spaceGraphDivisionCount; x++)
            {
                cout << string(indent * indentSize, ' ');
                cout << (z == 0 ? "bottom" : "top") << "-" << (y == 0 ? "back" : "front") << "-" << (x == 0 ? "left" : "right") << endl;

                if (children[z][y][x])
                {
                    children[z][y][x]->print(indent + 1);
                }
            }
        }
    }
}

vector<SpaceGraphGenerationEntity> SpaceGraphNode::selectWithin(
    const vector<SpaceGraphGenerationEntity>& entities,
    const vec3& innerExtent,
    const vec3& outerExtent
) const
{
    vector<SpaceGraphGenerationEntity> ret;
    ret.reserve(entities.size() / pow(spaceGraphDivisionCount, 3));

    for (const SpaceGraphGenerationEntity& entity : entities)
    {
        if (entity.overlaps(innerExtent, outerExtent))
        {
            ret.push_back(entity);
        }
    }

    return ret;
}
