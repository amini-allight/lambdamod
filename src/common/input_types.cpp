/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "input_types.hpp"
#include "yaml_tools.hpp"
#include "tools.hpp"

string inputTypeToString(InputType type)
{
    switch (type)
    {
    case Input_Null : return "input-null";

    case Input_0 : return "input-0";
    case Input_1 : return "input-1";
    case Input_2 : return "input-2";
    case Input_3 : return "input-3";
    case Input_4 : return "input-4";
    case Input_5 : return "input-5";
    case Input_6 : return "input-6";
    case Input_7 : return "input-7";
    case Input_8 : return "input-8";
    case Input_9 : return "input-9";
    case Input_A : return "input-a";
    case Input_B : return "input-b";
    case Input_C : return "input-c";
    case Input_D : return "input-d";
    case Input_E : return "input-e";
    case Input_F : return "input-f";
    case Input_G : return "input-g";
    case Input_H : return "input-h";
    case Input_I : return "input-i";
    case Input_J : return "input-j";
    case Input_K : return "input-k";
    case Input_L : return "input-l";
    case Input_M : return "input-m";
    case Input_N : return "input-n";
    case Input_O : return "input-o";
    case Input_P : return "input-p";
    case Input_Q : return "input-q";
    case Input_R : return "input-r";
    case Input_S : return "input-s";
    case Input_T : return "input-t";
    case Input_U : return "input-u";
    case Input_V : return "input-v";
    case Input_W : return "input-w";
    case Input_X : return "input-x";
    case Input_Y : return "input-y";
    case Input_Z : return "input-z";
    case Input_F1 : return "input-f1";
    case Input_F2 : return "input-f2";
    case Input_F3 : return "input-f3";
    case Input_F4 : return "input-f4";
    case Input_F5 : return "input-f5";
    case Input_F6 : return "input-f6";
    case Input_F7 : return "input-f7";
    case Input_F8 : return "input-f8";
    case Input_F9 : return "input-f9";
    case Input_F10 : return "input-f10";
    case Input_F11 : return "input-f11";
    case Input_F12 : return "input-f12";
    case Input_Escape : return "input-escape";
    case Input_Backspace : return "input-backspace";
    case Input_Tab : return "input-tab";
    case Input_Caps_Lock : return "input-caps-lock";
    case Input_Return : return "input-return";
    case Input_Space : return "input-space";
    case Input_Menu : return "input-menu";
    case Input_Grave_Accent : return "input-grave-accent";
    case Input_Minus : return "input-minus";
    case Input_Equals : return "input-equals";
    case Input_Left_Brace : return "input-left-brace";
    case Input_Right_Brace : return "input-right-brace";
    case Input_Backslash : return "input-backslash";
    case Input_Semicolon : return "input-semicolon";
    case Input_Quote : return "input-quote";
    case Input_Period : return "input-period";
    case Input_Comma : return "input-comma";
    case Input_Slash : return "input-slash";
    case Input_Left_Shift : return "input-left-shift";
    case Input_Left_Control : return "input-left-control";
    case Input_Left_Super : return "input-left-super";
    case Input_Left_Alt : return "input-left-alt";
    case Input_Right_Shift : return "input-right-shift";
    case Input_Right_Control : return "input-right-control";
    case Input_Right_Super : return "input-right-super";
    case Input_Right_Alt : return "input-right-alt";
    case Input_Print_Screen : return "input-print-screen";
    case Input_Insert : return "input-insert";
    case Input_Delete : return "input-delete";
    case Input_Scroll_Lock : return "input-scroll-lock";
    case Input_Home : return "input-home";
    case Input_End : return "input-end";
    case Input_Pause : return "input-pause";
    case Input_Page_Up : return "input-page-up";
    case Input_Page_Down : return "input-page-down";
    case Input_Left : return "input-left";
    case Input_Right : return "input-right";
    case Input_Up : return "input-up";
    case Input_Down : return "input-down";
    case Input_Num_Lock : return "input-num-lock";
    case Input_Num_Add : return "input-num-add";
    case Input_Num_Subtract : return "input-num-subtract";
    case Input_Num_Multiply : return "input-num-multiply";
    case Input_Num_Divide : return "input-num-divide";
    case Input_Num_0 : return "input-num-0";
    case Input_Num_1 : return "input-num-1";
    case Input_Num_2 : return "input-num-2";
    case Input_Num_3 : return "input-num-3";
    case Input_Num_4 : return "input-num-4";
    case Input_Num_5 : return "input-num-5";
    case Input_Num_6 : return "input-num-6";
    case Input_Num_7 : return "input-num-7";
    case Input_Num_8 : return "input-num-8";
    case Input_Num_9 : return "input-num-9";
    case Input_Num_Period : return "input-num-period";
    case Input_Num_Return : return "input-num-return";

    case Input_Left_Mouse : return "input-left-mouse";
    case Input_Middle_Mouse : return "input-middle-mouse";
    case Input_Right_Mouse : return "input-right-mouse";
    case Input_X1_Mouse : return "input-x1-mouse";
    case Input_X2_Mouse : return "input-x2-mouse";
    case Input_Mouse_Move : return "input-mouse-move";
    case Input_Mouse_Wheel_Up : return "input-mouse-wheel-up";
    case Input_Mouse_Wheel_Down : return "input-mouse-wheel-down";
    case Input_Mouse_Wheel_Left : return "input-mouse-wheel-left";
    case Input_Mouse_Wheel_Right : return "input-mouse-wheel-right";

    case Input_Gamepad_DPad_Left : return "input-gamepad-dpad-left";
    case Input_Gamepad_DPad_Right : return "input-gamepad-dpad-right";
    case Input_Gamepad_DPad_Up : return "input-gamepad-dpad-up";
    case Input_Gamepad_DPad_Down : return "input-gamepad-dpad-down";
    case Input_Gamepad_A : return "input-gamepad-a";
    case Input_Gamepad_B : return "input-gamepad-b";
    case Input_Gamepad_X : return "input-gamepad-x";
    case Input_Gamepad_Y : return "input-gamepad-y";
    case Input_Gamepad_Left_Stick : return "input-gamepad-left-stick";
    case Input_Gamepad_Left_Stick_Click : return "input-gamepad-left-stick-click";
    case Input_Gamepad_Left_Bumper : return "input-gamepad-left-bumper";
    case Input_Gamepad_Left_Trigger : return "input-gamepad-left-trigger";
    case Input_Gamepad_Right_Stick : return "input-gamepad-right-stick";
    case Input_Gamepad_Right_Stick_Click : return "input-gamepad-right-stick-click";
    case Input_Gamepad_Right_Bumper : return "input-gamepad-right-bumper";
    case Input_Gamepad_Right_Trigger : return "input-gamepad-right-trigger";
    case Input_Gamepad_Back : return "input-gamepad-back";
    case Input_Gamepad_Guide : return "input-gamepad-guide";
    case Input_Gamepad_Start : return "input-gamepad-start";

    case Input_Touch : return "input-touch";

    case Input_VR_Heartrate : return "input-vr-heartrate";

    case Input_VR_System_Click : return "input-vr-system-click";
    case Input_VR_Volume_Up : return "input-vr-volume-up";
    case Input_VR_Volume_Down : return "input-vr-volume-down";
    case Input_VR_Mute_Microphone : return "input-vr-mute-microphone";

    case Input_VR_Head : return "input-vr-head";
    case Input_VR_Hips : return "input-vr-hips";
    case Input_VR_Left_Hand : return "input-vr-left-hand";
    case Input_VR_Right_Hand : return "input-vr-right-hand";
    case Input_VR_Left_Foot : return "input-vr-left-foot";
    case Input_VR_Right_Foot : return "input-vr-right-foot";
    case Input_VR_Chest : return "input-vr-chest";
    case Input_VR_Left_Shoulder : return "input-vr-left-shoulder";
    case Input_VR_Right_Shoulder : return "input-vr-right-shoulder";
    case Input_VR_Left_Elbow : return "input-vr-left-elbow";
    case Input_VR_Right_Elbow : return "input-vr-right-elbow";
    case Input_VR_Left_Wrist : return "input-vr-left-wrist";
    case Input_VR_Right_Wrist : return "input-vr-right-wrist";
    case Input_VR_Left_Knee : return "input-vr-left-knee";
    case Input_VR_Right_Knee : return "input-vr-right-knee";
    case Input_VR_Left_Ankle : return "input-vr-left-ankle";
    case Input_VR_Right_Ankle : return "input-vr-right-ankle";

    case Input_VR_Eyes : return "input-vr-eyes";

    case Input_VR_Left_Eye : return "input-vr-left-eye";
    case Input_VR_Left_Eyebrow : return "input-vr-left-eyebrow";
    case Input_VR_Left_Eyelid : return "input-vr-left-eyelid";
    case Input_VR_Left_Pupil : return "input-vr-left-pupil";
    case Input_VR_Right_Eye : return "input-vr-right-eye";
    case Input_VR_Right_Eyebrow : return "input-vr-right-eyebrow";
    case Input_VR_Right_Eyelid : return "input-vr-right-eyelid";
    case Input_VR_Right_Pupil : return "input-vr-right-pupil";
    case Input_VR_Jaw : return "input-vr-jaw";
    case Input_VR_Upper_Lip : return "input-vr-upper-lip";
    case Input_VR_Lower_Lip : return "input-vr-lower-lip";
    case Input_VR_Mouth : return "input-vr-mouth";
    case Input_VR_Left_Cheek : return "input-vr-left-cheek";
    case Input_VR_Right_Cheek : return "input-vr-right-cheek";
    case Input_VR_Tongue : return "input-vr-tongue";

    case Input_VR_Left_Thumb : return "input-vr-left-thumb";
    case Input_VR_Left_Index : return "input-vr-left-index";
    case Input_VR_Left_Middle : return "input-vr-left-middle";
    case Input_VR_Left_Ring : return "input-vr-left-ring";
    case Input_VR_Left_Little : return "input-vr-left-little";

    case Input_VR_Right_Thumb : return "input-vr-right-thumb";
    case Input_VR_Right_Index : return "input-vr-right-index";
    case Input_VR_Right_Middle : return "input-vr-right-middle";
    case Input_VR_Right_Ring : return "input-vr-right-ring";
    case Input_VR_Right_Little : return "input-vr-right-little";

    case Input_VR_Left_Trigger_Touch : return "input-vr-left-trigger-touch";
    case Input_VR_Left_Trigger : return "input-vr-left-trigger";
    case Input_VR_Left_Trigger_Click : return "input-vr-left-trigger-click";
    case Input_VR_Left_Grip_Touch : return "input-vr-left-grip-touch";
    case Input_VR_Left_Grip : return "input-vr-left-grip";
    case Input_VR_Left_Grip_Click : return "input-vr-left-grip-click";
    case Input_VR_Left_Stick_Touch : return "input-vr-left-stick-touch";
    case Input_VR_Left_Stick : return "input-vr-left-stick";
    case Input_VR_Left_Stick_Click : return "input-vr-left-stick-click";
    case Input_VR_Left_Touchpad_Touch : return "input-vr-left-touchpad-touch";
    case Input_VR_Left_Touchpad : return "input-vr-left-touchpad";
    case Input_VR_Left_Touchpad_Press : return "input-vr-left-touchpad-press";
    case Input_VR_Left_Touchpad_Click : return "input-vr-left-touchpad-click";
    case Input_VR_Left_System_Touch : return "input-vr-left-system-touch";
    case Input_VR_Left_System : return "input-vr-left-system";
    case Input_VR_Left_A_Touch : return "input-vr-left-a-touch";
    case Input_VR_Left_A : return "input-vr-left-a";
    case Input_VR_Left_B_Touch : return "input-vr-left-b-touch";
    case Input_VR_Left_B : return "input-vr-left-b";
    case Input_VR_Left_Menu_Touch : return "input-vr-left-menu-touch";
    case Input_VR_Left_Menu : return "input-vr-left-menu";

    case Input_VR_Left_Bumper_Touch : return "input-vr-left-bumper-touch";
    case Input_VR_Left_Bumper : return "input-vr-left-bumper";

    case Input_VR_Left_Thumbrest : return "input-vr-left-thumbrest";
    case Input_VR_Left_Thumbrest_Touch : return "input-vr-left-thumbrest-touch";

    case Input_VR_Right_Trigger_Touch : return "input-vr-right-trigger-touch";
    case Input_VR_Right_Trigger : return "input-vr-right-trigger";
    case Input_VR_Right_Trigger_Click : return "input-vr-right-trigger-click";
    case Input_VR_Right_Grip_Touch : return "input-vr-right-grip-touch";
    case Input_VR_Right_Grip : return "input-vr-right-grip";
    case Input_VR_Right_Grip_Click : return "input-vr-right-grip-click";
    case Input_VR_Right_Stick_Touch : return "input-vr-right-stick-touch";
    case Input_VR_Right_Stick : return "input-vr-right-stick";
    case Input_VR_Right_Stick_Click : return "input-vr-right-stick-click";
    case Input_VR_Right_Touchpad_Touch : return "input-vr-right-touchpad-touch";
    case Input_VR_Right_Touchpad : return "input-vr-right-touchpad";
    case Input_VR_Right_Touchpad_Press : return "input-vr-right-touchpad-press";
    case Input_VR_Right_Touchpad_Click : return "input-vr-right-touchpad-click";
    case Input_VR_Right_System_Touch : return "input-vr-right-system-touch";
    case Input_VR_Right_System : return "input-vr-right-system";
    case Input_VR_Right_A_Touch : return "input-vr-right-a-touch";
    case Input_VR_Right_A : return "input-vr-right-a";
    case Input_VR_Right_B_Touch : return "input-vr-right-b-touch";
    case Input_VR_Right_B : return "input-vr-right-b";
    case Input_VR_Right_Menu_Touch : return "input-vr-right-c-touch";
    case Input_VR_Right_Menu : return "input-vr-right-c";

    case Input_VR_Right_Bumper_Touch : return "input-vr-right-bumper-touch";
    case Input_VR_Right_Bumper : return "input-vr-right-bumper";

    case Input_VR_Right_Thumbrest : return "input-vr-right-thumbrest";
    case Input_VR_Right_Thumbrest_Touch : return "input-vr-right-thumbrest-touch";

    default : fatal("Encountered unknown input type '" + to_string(type) + "'.");
    }
}

InputType inputTypeFromString(const string& s)
{
    if (s == "input-null")
    {
        return Input_Null;
    }
    else if (s == "input-0")
    {
        return Input_0;
    }
    else if (s == "input-1")
    {
        return Input_1;
    }
    else if (s == "input-2")
    {
        return Input_2;
    }
    else if (s == "input-3")
    {
        return Input_3;
    }
    else if (s == "input-4")
    {
        return Input_4;
    }
    else if (s == "input-5")
    {
        return Input_5;
    }
    else if (s == "input-6")
    {
        return Input_6;
    }
    else if (s == "input-7")
    {
        return Input_7;
    }
    else if (s == "input-8")
    {
        return Input_8;
    }
    else if (s == "input-9")
    {
        return Input_9;
    }
    else if (s == "input-a")
    {
        return Input_A;
    }
    else if (s == "input-b")
    {
        return Input_B;
    }
    else if (s == "input-c")
    {
        return Input_C;
    }
    else if (s == "input-d")
    {
        return Input_D;
    }
    else if (s == "input-e")
    {
        return Input_E;
    }
    else if (s == "input-f")
    {
        return Input_F;
    }
    else if (s == "input-g")
    {
        return Input_G;
    }
    else if (s == "input-h")
    {
        return Input_H;
    }
    else if (s == "input-i")
    {
        return Input_I;
    }
    else if (s == "input-j")
    {
        return Input_J;
    }
    else if (s == "input-k")
    {
        return Input_K;
    }
    else if (s == "input-l")
    {
        return Input_L;
    }
    else if (s == "input-m")
    {
        return Input_M;
    }
    else if (s == "input-n")
    {
        return Input_N;
    }
    else if (s == "input-o")
    {
        return Input_O;
    }
    else if (s == "input-p")
    {
        return Input_P;
    }
    else if (s == "input-q")
    {
        return Input_Q;
    }
    else if (s == "input-r")
    {
        return Input_R;
    }
    else if (s == "input-s")
    {
        return Input_S;
    }
    else if (s == "input-t")
    {
        return Input_T;
    }
    else if (s == "input-u")
    {
        return Input_U;
    }
    else if (s == "input-v")
    {
        return Input_V;
    }
    else if (s == "input-w")
    {
        return Input_W;
    }
    else if (s == "input-x")
    {
        return Input_X;
    }
    else if (s == "input-y")
    {
        return Input_Y;
    }
    else if (s == "input-z")
    {
        return Input_Z;
    }
    else if (s == "input-f1")
    {
        return Input_F1;
    }
    else if (s == "input-f2")
    {
        return Input_F2;
    }
    else if (s == "input-f3")
    {
        return Input_F3;
    }
    else if (s == "input-f4")
    {
        return Input_F4;
    }
    else if (s == "input-f5")
    {
        return Input_F5;
    }
    else if (s == "input-f6")
    {
        return Input_F6;
    }
    else if (s == "input-f7")
    {
        return Input_F7;
    }
    else if (s == "input-f8")
    {
        return Input_F8;
    }
    else if (s == "input-f9")
    {
        return Input_F9;
    }
    else if (s == "input-f10")
    {
        return Input_F10;
    }
    else if (s == "input-f11")
    {
        return Input_F11;
    }
    else if (s == "input-f12")
    {
        return Input_F12;
    }
    else if (s == "input-escape")
    {
        return Input_Escape;
    }
    else if (s == "input-backspace")
    {
        return Input_Backspace;
    }
    else if (s == "input-tab")
    {
        return Input_Tab;
    }
    else if (s == "input-caps-lock")
    {
        return Input_Caps_Lock;
    }
    else if (s == "input-return")
    {
        return Input_Return;
    }
    else if (s == "input-space")
    {
        return Input_Space;
    }
    else if (s == "input-menu")
    {
        return Input_Menu;
    }
    else if (s == "input-grave-accent")
    {
        return Input_Grave_Accent;
    }
    else if (s == "input-minus")
    {
        return Input_Minus;
    }
    else if (s == "input-equals")
    {
        return Input_Equals;
    }
    else if (s == "input-left-brace")
    {
        return Input_Left_Brace;
    }
    else if (s == "input-right-brace")
    {
        return Input_Right_Brace;
    }
    else if (s == "input-backslash")
    {
        return Input_Backslash;
    }
    else if (s == "input-semicolon")
    {
        return Input_Semicolon;
    }
    else if (s == "input-quote")
    {
        return Input_Quote;
    }
    else if (s == "input-period")
    {
        return Input_Period;
    }
    else if (s == "input-comma")
    {
        return Input_Comma;
    }
    else if (s == "input-slash")
    {
        return Input_Slash;
    }
    else if (s == "input-left-shift")
    {
        return Input_Left_Shift;
    }
    else if (s == "input-left-control")
    {
        return Input_Left_Control;
    }
    else if (s == "input-left-super")
    {
        return Input_Left_Super;
    }
    else if (s == "input-left-alt")
    {
        return Input_Left_Alt;
    }
    else if (s == "input-right-shift")
    {
        return Input_Right_Shift;
    }
    else if (s == "input-right-control")
    {
        return Input_Right_Control;
    }
    else if (s == "input-right-super")
    {
        return Input_Right_Super;
    }
    else if (s == "input-right-alt")
    {
        return Input_Right_Alt;
    }
    else if (s == "input-print-screen")
    {
        return Input_Print_Screen;
    }
    else if (s == "input-insert")
    {
        return Input_Insert;
    }
    else if (s == "input-delete")
    {
        return Input_Delete;
    }
    else if (s == "input-scroll-lock")
    {
        return Input_Scroll_Lock;
    }
    else if (s == "input-home")
    {
        return Input_Home;
    }
    else if (s == "input-end")
    {
        return Input_End;
    }
    else if (s == "input-pause")
    {
        return Input_Pause;
    }
    else if (s == "input-page-up")
    {
        return Input_Page_Up;
    }
    else if (s == "input-page-down")
    {
        return Input_Page_Down;
    }
    else if (s == "input-left")
    {
        return Input_Left;
    }
    else if (s == "input-right")
    {
        return Input_Right;
    }
    else if (s == "input-up")
    {
        return Input_Up;
    }
    else if (s == "input-down")
    {
        return Input_Down;
    }
    else if (s == "input-num-lock")
    {
        return Input_Num_Lock;
    }
    else if (s == "input-num-add")
    {
        return Input_Num_Add;
    }
    else if (s == "input-num-subtract")
    {
        return Input_Num_Subtract;
    }
    else if (s == "input-num-multiply")
    {
        return Input_Num_Multiply;
    }
    else if (s == "input-num-divide")
    {
        return Input_Num_Divide;
    }
    else if (s == "input-num-0")
    {
        return Input_Num_0;
    }
    else if (s == "input-num-1")
    {
        return Input_Num_1;
    }
    else if (s == "input-num-2")
    {
        return Input_Num_2;
    }
    else if (s == "input-num-3")
    {
        return Input_Num_3;
    }
    else if (s == "input-num-4")
    {
        return Input_Num_4;
    }
    else if (s == "input-num-5")
    {
        return Input_Num_5;
    }
    else if (s == "input-num-6")
    {
        return Input_Num_6;
    }
    else if (s == "input-num-7")
    {
        return Input_Num_7;
    }
    else if (s == "input-num-8")
    {
        return Input_Num_8;
    }
    else if (s == "input-num-9")
    {
        return Input_Num_9;
    }
    else if (s == "input-num-period")
    {
        return Input_Num_Period;
    }
    else if (s == "input-num-return")
    {
        return Input_Num_Return;
    }
    else if (s == "input-left-mouse")
    {
        return Input_Left_Mouse;
    }
    else if (s == "input-middle-mouse")
    {
        return Input_Middle_Mouse;
    }
    else if (s == "input-right-mouse")
    {
        return Input_Right_Mouse;
    }
    else if (s == "input-x1-mouse")
    {
        return Input_X1_Mouse;
    }
    else if (s == "input-x2-mouse")
    {
        return Input_X2_Mouse;
    }
    else if (s == "input-mouse-move")
    {
        return Input_Mouse_Move;
    }
    else if (s == "input-mouse-wheel-up")
    {
        return Input_Mouse_Wheel_Up;
    }
    else if (s == "input-mouse-wheel-down")
    {
        return Input_Mouse_Wheel_Down;
    }
    else if (s == "input-mouse-wheel-left")
    {
        return Input_Mouse_Wheel_Left;
    }
    else if (s == "input-mouse-wheel-right")
    {
        return Input_Mouse_Wheel_Right;
    }
    else if (s == "input-gamepad-dpad-left")
    {
        return Input_Gamepad_DPad_Left;
    }
    else if (s == "input-gamepad-dpad-right")
    {
        return Input_Gamepad_DPad_Right;
    }
    else if (s == "input-gamepad-dpad-up")
    {
        return Input_Gamepad_DPad_Up;
    }
    else if (s == "input-gamepad-dpad-down")
    {
        return Input_Gamepad_DPad_Down;
    }
    else if (s == "input-gamepad-a")
    {
        return Input_Gamepad_A;
    }
    else if (s == "input-gamepad-b")
    {
        return Input_Gamepad_B;
    }
    else if (s == "input-gamepad-x")
    {
        return Input_Gamepad_X;
    }
    else if (s == "input-gamepad-y")
    {
        return Input_Gamepad_Y;
    }
    else if (s == "input-gamepad-left-stick")
    {
        return Input_Gamepad_Left_Stick;
    }
    else if (s == "input-gamepad-left-stick-click")
    {
        return Input_Gamepad_Left_Stick_Click;
    }
    else if (s == "input-gamepad-left-bumper")
    {
        return Input_Gamepad_Left_Bumper;
    }
    else if (s == "input-gamepad-left-trigger")
    {
        return Input_Gamepad_Left_Trigger;
    }
    else if (s == "input-gamepad-right-stick")
    {
        return Input_Gamepad_Right_Stick;
    }
    else if (s == "input-gamepad-right-stick-click")
    {
        return Input_Gamepad_Right_Stick_Click;
    }
    else if (s == "input-gamepad-right-bumper")
    {
        return Input_Gamepad_Right_Bumper;
    }
    else if (s == "input-gamepad-right-trigger")
    {
        return Input_Gamepad_Right_Trigger;
    }
    else if (s == "input-gamepad-back")
    {
        return Input_Gamepad_Back;
    }
    else if (s == "input-gamepad-guide")
    {
        return Input_Gamepad_Guide;
    }
    else if (s == "input-gamepad-start")
    {
        return Input_Gamepad_Start;
    }
    else if (s == "input-touch")
    {
        return Input_Touch;
    }
    else if (s == "input-vr-heartrate")
    {
        return Input_VR_Heartrate;
    }
    else if (s == "input-vr-system-click")
    {
        return Input_VR_System_Click;
    }
    else if (s == "input-vr-volume-up")
    {
        return Input_VR_Volume_Up;
    }
    else if (s == "input-vr-volume-down")
    {
        return Input_VR_Volume_Down;
    }
    else if (s == "input-vr-mute-microphone")
    {
        return Input_VR_Mute_Microphone;
    }
    else if (s == "input-vr-head")
    {
        return Input_VR_Head;
    }
    else if (s == "input-vr-hips")
    {
        return Input_VR_Hips;
    }
    else if (s == "input-vr-left-hand")
    {
        return Input_VR_Left_Hand;
    }
    else if (s == "input-vr-right-hand")
    {
        return Input_VR_Right_Hand;
    }
    else if (s == "input-vr-left-foot")
    {
        return Input_VR_Left_Foot;
    }
    else if (s == "input-vr-right-foot")
    {
        return Input_VR_Right_Foot;
    }
    else if (s == "input-vr-chest")
    {
        return Input_VR_Chest;
    }
    else if (s == "input-vr-left-shoulder")
    {
        return Input_VR_Left_Shoulder;
    }
    else if (s == "input-vr-right-shoulder")
    {
        return Input_VR_Right_Shoulder;
    }
    else if (s == "input-vr-left-elbow")
    {
        return Input_VR_Left_Elbow;
    }
    else if (s == "input-vr-right-elbow")
    {
        return Input_VR_Right_Elbow;
    }
    else if (s == "input-vr-left-wrist")
    {
        return Input_VR_Left_Wrist;
    }
    else if (s == "input-vr-right-wrist")
    {
        return Input_VR_Right_Wrist;
    }
    else if (s == "input-vr-left-knee")
    {
        return Input_VR_Left_Knee;
    }
    else if (s == "input-vr-right-knee")
    {
        return Input_VR_Right_Knee;
    }
    else if (s == "input-vr-left-ankle")
    {
        return Input_VR_Left_Ankle;
    }
    else if (s == "input-vr-right-ankle")
    {
        return Input_VR_Right_Ankle;
    }
    else if (s == "input-vr-eyes")
    {
        return Input_VR_Eyes;
    }
    else if (s == "input-vr-left-eye")
    {
        return Input_VR_Left_Eye;
    }
    else if (s == "input-vr-left-eyebrow")
    {
        return Input_VR_Left_Eyebrow;
    }
    else if (s == "input-vr-left-eyelid")
    {
        return Input_VR_Left_Eyelid;
    }
    else if (s == "input-vr-right-eye")
    {
        return Input_VR_Right_Eye;
    }
    else if (s == "input-vr-right-eyebrow")
    {
        return Input_VR_Right_Eyebrow;
    }
    else if (s == "input-vr-right-eyelid")
    {
        return Input_VR_Right_Eyelid;
    }
    else if (s == "input-vr-jaw")
    {
        return Input_VR_Jaw;
    }
    else if (s == "input-vr-upper-lip")
    {
        return Input_VR_Upper_Lip;
    }
    else if (s == "input-vr-lower-lip")
    {
        return Input_VR_Lower_Lip;
    }
    else if (s == "input-vr-mouth")
    {
        return Input_VR_Mouth;
    }
    else if (s == "input-vr-left-cheek")
    {
        return Input_VR_Left_Cheek;
    }
    else if (s == "input-vr-right-cheek")
    {
        return Input_VR_Right_Cheek;
    }
    else if (s == "input-vr-tongue")
    {
        return Input_VR_Tongue;
    }
    else if (s == "input-vr-left-thumb")
    {
        return Input_VR_Left_Thumb;
    }
    else if (s == "input-vr-left-index")
    {
        return Input_VR_Left_Index;
    }
    else if (s == "input-vr-left-middle")
    {
        return Input_VR_Left_Middle;
    }
    else if (s == "input-vr-left-ring")
    {
        return Input_VR_Left_Ring;
    }
    else if (s == "input-vr-left-little")
    {
        return Input_VR_Left_Little;
    }
    else if (s == "input-vr-right-thumb")
    {
        return Input_VR_Right_Thumb;
    }
    else if (s == "input-vr-right-index")
    {
        return Input_VR_Right_Index;
    }
    else if (s == "input-vr-right-middle")
    {
        return Input_VR_Right_Middle;
    }
    else if (s == "input-vr-right-ring")
    {
        return Input_VR_Right_Ring;
    }
    else if (s == "input-vr-right-little")
    {
        return Input_VR_Right_Little;
    }
    else if (s == "input-vr-left-trigger-touch")
    {
        return Input_VR_Left_Trigger_Touch;
    }
    else if (s == "input-vr-left-trigger")
    {
        return Input_VR_Left_Trigger;
    }
    else if (s == "input-vr-left-trigger-click")
    {
        return Input_VR_Left_Trigger_Click;
    }
    else if (s == "input-vr-left-grip-touch")
    {
        return Input_VR_Left_Grip_Touch;
    }
    else if (s == "input-vr-left-grip")
    {
        return Input_VR_Left_Grip;
    }
    else if (s == "input-vr-left-grip-click")
    {
        return Input_VR_Left_Grip_Click;
    }
    else if (s == "input-vr-left-stick-touch")
    {
        return Input_VR_Left_Stick_Touch;
    }
    else if (s == "input-vr-left-stick")
    {
        return Input_VR_Left_Stick;
    }
    else if (s == "input-vr-left-stick-click")
    {
        return Input_VR_Left_Stick_Click;
    }
    else if (s == "input-vr-left-touchpad-touch")
    {
        return Input_VR_Left_Touchpad_Touch;
    }
    else if (s == "input-vr-left-touchpad")
    {
        return Input_VR_Left_Touchpad;
    }
    else if (s == "input-vr-left-touchpad-press")
    {
        return Input_VR_Left_Touchpad_Press;
    }
    else if (s == "input-vr-left-touchpad-click")
    {
        return Input_VR_Left_Touchpad_Click;
    }
    else if (s == "input-vr-left-system-touch")
    {
        return Input_VR_Left_System_Touch;
    }
    else if (s == "input-vr-left-system")
    {
        return Input_VR_Left_System;
    }
    else if (s == "input-vr-left-a-touch")
    {
        return Input_VR_Left_A_Touch;
    }
    else if (s == "input-vr-left-a")
    {
        return Input_VR_Left_A;
    }
    else if (s == "input-vr-left-b-touch")
    {
        return Input_VR_Left_B_Touch;
    }
    else if (s == "input-vr-left-b")
    {
        return Input_VR_Left_B;
    }
    else if (s == "input-vr-left-menu-touch")
    {
        return Input_VR_Left_Menu_Touch;
    }
    else if (s == "input-vr-left-menu")
    {
        return Input_VR_Left_Menu;
    }
    else if (s == "input-vr-left-bumper-touch")
    {
        return Input_VR_Left_Bumper_Touch;
    }
    else if (s == "input-vr-left-bumper")
    {
        return Input_VR_Left_Bumper;
    }
    else if (s == "input-vr-left-thumbrest-touch")
    {
        return Input_VR_Left_Thumbrest;
    }
    else if (s == "input-vr-left-thumbrest")
    {
        return Input_VR_Left_Thumbrest_Touch;
    }
    else if (s == "input-vr-right-trigger-touch")
    {
        return Input_VR_Right_Trigger_Touch;
    }
    else if (s == "input-vr-right-trigger")
    {
        return Input_VR_Right_Trigger;
    }
    else if (s == "input-vr-right-trigger-click")
    {
        return Input_VR_Right_Trigger_Click;
    }
    else if (s == "input-vr-right-grip-touch")
    {
        return Input_VR_Right_Grip_Touch;
    }
    else if (s == "input-vr-right-grip")
    {
        return Input_VR_Right_Grip;
    }
    else if (s == "input-vr-right-grip-click")
    {
        return Input_VR_Right_Grip_Click;
    }
    else if (s == "input-vr-right-stick-touch")
    {
        return Input_VR_Right_Stick_Touch;
    }
    else if (s == "input-vr-right-stick")
    {
        return Input_VR_Right_Stick;
    }
    else if (s == "input-vr-right-stick-click")
    {
        return Input_VR_Right_Stick_Click;
    }
    else if (s == "input-vr-right-touchpad-touch")
    {
        return Input_VR_Right_Touchpad_Touch;
    }
    else if (s == "input-vr-right-touchpad")
    {
        return Input_VR_Right_Touchpad;
    }
    else if (s == "input-vr-right-touchpad-press")
    {
        return Input_VR_Right_Touchpad_Press;
    }
    else if (s == "input-vr-right-touchpad-click")
    {
        return Input_VR_Right_Touchpad_Click;
    }
    else if (s == "input-vr-right-system-touch")
    {
        return Input_VR_Right_System_Touch;
    }
    else if (s == "input-vr-right-system")
    {
        return Input_VR_Right_System;
    }
    else if (s == "input-vr-right-a-touch")
    {
        return Input_VR_Right_A_Touch;
    }
    else if (s == "input-vr-right-a")
    {
        return Input_VR_Right_A;
    }
    else if (s == "input-vr-right-b-touch")
    {
        return Input_VR_Right_B_Touch;
    }
    else if (s == "input-vr-right-b")
    {
        return Input_VR_Right_B;
    }
    else if (s == "input-vr-right-menu-touch")
    {
        return Input_VR_Right_Menu_Touch;
    }
    else if (s == "input-vr-right-menu")
    {
        return Input_VR_Right_Menu;
    }
    else if (s == "input-vr-right-bumper-touch")
    {
        return Input_VR_Right_Bumper_Touch;
    }
    else if (s == "input-vr-right-bumper")
    {
        return Input_VR_Right_Bumper;
    }
    else if (s == "input-vr-right-thumbrest-touch")
    {
        return Input_VR_Right_Thumbrest_Touch;
    }
    else if (s == "input-vr-right-thumbrest")
    {
        return Input_VR_Right_Thumbrest;
    }
    else
    {
        throw runtime_error("Encountered unknown input type '" + s + "'.");
    }
}

template<>
InputType YAMLNode::convert() const
{
    return inputTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(InputType v)
{
    emit(inputTypeToString(v));
}

bool isKeyboard(InputType type)
{
    switch (type)
    {
    case Input_0 :
    case Input_1 :
    case Input_2 :
    case Input_3 :
    case Input_4 :
    case Input_5 :
    case Input_6 :
    case Input_7 :
    case Input_8 :
    case Input_9 :
    case Input_A :
    case Input_B :
    case Input_C :
    case Input_D :
    case Input_E :
    case Input_F :
    case Input_G :
    case Input_H :
    case Input_I :
    case Input_J :
    case Input_K :
    case Input_L :
    case Input_M :
    case Input_N :
    case Input_O :
    case Input_P :
    case Input_Q :
    case Input_R :
    case Input_S :
    case Input_T :
    case Input_U :
    case Input_V :
    case Input_W :
    case Input_X :
    case Input_Y :
    case Input_Z :
    case Input_F1 :
    case Input_F2 :
    case Input_F3 :
    case Input_F4 :
    case Input_F5 :
    case Input_F6 :
    case Input_F7 :
    case Input_F8 :
    case Input_F9 :
    case Input_F10 :
    case Input_F11 :
    case Input_F12 :
    case Input_Escape :
    case Input_Backspace :
    case Input_Tab :
    case Input_Caps_Lock :
    case Input_Return :
    case Input_Space :
    case Input_Menu :
    case Input_Grave_Accent :
    case Input_Minus :
    case Input_Equals :
    case Input_Left_Brace :
    case Input_Right_Brace :
    case Input_Backslash :
    case Input_Semicolon :
    case Input_Quote :
    case Input_Period :
    case Input_Comma :
    case Input_Slash :
    case Input_Left_Shift :
    case Input_Left_Control :
    case Input_Left_Super :
    case Input_Left_Alt :
    case Input_Right_Shift :
    case Input_Right_Control :
    case Input_Right_Super :
    case Input_Right_Alt :
    case Input_Print_Screen :
    case Input_Insert :
    case Input_Delete :
    case Input_Scroll_Lock :
    case Input_Home :
    case Input_End :
    case Input_Pause :
    case Input_Page_Up :
    case Input_Page_Down :
    case Input_Left :
    case Input_Right :
    case Input_Up :
    case Input_Down :
    case Input_Num_Lock :
    case Input_Num_Add :
    case Input_Num_Subtract :
    case Input_Num_Multiply :
    case Input_Num_Divide :
    case Input_Num_0 :
    case Input_Num_1 :
    case Input_Num_2 :
    case Input_Num_3 :
    case Input_Num_4 :
    case Input_Num_5 :
    case Input_Num_6 :
    case Input_Num_7 :
    case Input_Num_8 :
    case Input_Num_9 :
    case Input_Num_Period :
    case Input_Num_Return :
        return true;
    default :
        return false;
    }
}

bool isMouse(InputType type)
{
    switch (type)
    {
    case Input_Left_Mouse :
    case Input_Middle_Mouse :
    case Input_Right_Mouse :
    case Input_X1_Mouse :
    case Input_X2_Mouse :
    case Input_Mouse_Move :
    case Input_Mouse_Wheel_Up :
    case Input_Mouse_Wheel_Down :
    case Input_Mouse_Wheel_Left :
    case Input_Mouse_Wheel_Right :
        return true;
    default :
        return false;
    }
}

bool isGamepad(InputType type)
{
    switch (type)
    {
    case Input_Gamepad_DPad_Left :
    case Input_Gamepad_DPad_Right :
    case Input_Gamepad_DPad_Up :
    case Input_Gamepad_DPad_Down :
    case Input_Gamepad_A :
    case Input_Gamepad_B :
    case Input_Gamepad_X :
    case Input_Gamepad_Y :
    case Input_Gamepad_Left_Stick :
    case Input_Gamepad_Left_Stick_Click :
    case Input_Gamepad_Left_Bumper :
    case Input_Gamepad_Left_Trigger :
    case Input_Gamepad_Right_Stick :
    case Input_Gamepad_Right_Stick_Click :
    case Input_Gamepad_Right_Bumper :
    case Input_Gamepad_Right_Trigger :
    case Input_Gamepad_Back :
    case Input_Gamepad_Guide :
    case Input_Gamepad_Start :
        return true;
    default :
        return false;
    }
}

bool isTouch(InputType type)
{
    switch (type)
    {
    case Input_Touch :
        return true;
    default :
        return false;
    }
}

bool isFlat(InputType type)
{
    return isKeyboard(type) || isMouse(type) || isGamepad(type) || isTouch(type);
}

bool isVR(InputType type)
{
    switch (type)
    {
    case Input_VR_Heartrate :
    case Input_VR_System_Click :
    case Input_VR_Volume_Up :
    case Input_VR_Volume_Down :
    case Input_VR_Mute_Microphone :
    case Input_VR_Head :
    case Input_VR_Hips :
    case Input_VR_Left_Hand :
    case Input_VR_Right_Hand :
    case Input_VR_Left_Foot :
    case Input_VR_Right_Foot :
    case Input_VR_Chest :
    case Input_VR_Left_Shoulder :
    case Input_VR_Right_Shoulder :
    case Input_VR_Left_Elbow :
    case Input_VR_Right_Elbow :
    case Input_VR_Left_Wrist :
    case Input_VR_Right_Wrist :
    case Input_VR_Left_Knee :
    case Input_VR_Right_Knee :
    case Input_VR_Left_Ankle :
    case Input_VR_Right_Ankle :
    case Input_VR_Eyes :
    case Input_VR_Left_Eye :
    case Input_VR_Left_Eyebrow :
    case Input_VR_Left_Eyelid :
    case Input_VR_Left_Pupil :
    case Input_VR_Right_Eye :
    case Input_VR_Right_Eyebrow :
    case Input_VR_Right_Eyelid :
    case Input_VR_Right_Pupil :
    case Input_VR_Jaw :
    case Input_VR_Upper_Lip :
    case Input_VR_Lower_Lip :
    case Input_VR_Mouth :
    case Input_VR_Left_Cheek :
    case Input_VR_Right_Cheek :
    case Input_VR_Tongue :
    case Input_VR_Left_Thumb :
    case Input_VR_Left_Index :
    case Input_VR_Left_Middle :
    case Input_VR_Left_Ring :
    case Input_VR_Left_Little :
    case Input_VR_Right_Thumb :
    case Input_VR_Right_Index :
    case Input_VR_Right_Middle :
    case Input_VR_Right_Ring :
    case Input_VR_Right_Little :
    case Input_VR_Left_Trigger_Touch :
    case Input_VR_Left_Trigger :
    case Input_VR_Left_Trigger_Click :
    case Input_VR_Left_Grip_Touch :
    case Input_VR_Left_Grip :
    case Input_VR_Left_Grip_Click :
    case Input_VR_Left_Stick_Touch :
    case Input_VR_Left_Stick :
    case Input_VR_Left_Stick_Click :
    case Input_VR_Left_Touchpad_Touch :
    case Input_VR_Left_Touchpad :
    case Input_VR_Left_Touchpad_Press :
    case Input_VR_Left_Touchpad_Click :
    case Input_VR_Left_System_Touch :
    case Input_VR_Left_System :
    case Input_VR_Left_A_Touch :
    case Input_VR_Left_A :
    case Input_VR_Left_B_Touch :
    case Input_VR_Left_B :
    case Input_VR_Left_Menu_Touch :
    case Input_VR_Left_Menu :
    case Input_VR_Left_Bumper_Touch :
    case Input_VR_Left_Bumper :
    case Input_VR_Left_Thumbrest_Touch :
    case Input_VR_Left_Thumbrest :
    case Input_VR_Right_Trigger_Touch :
    case Input_VR_Right_Trigger :
    case Input_VR_Right_Trigger_Click :
    case Input_VR_Right_Grip_Touch :
    case Input_VR_Right_Grip :
    case Input_VR_Right_Grip_Click :
    case Input_VR_Right_Stick_Touch :
    case Input_VR_Right_Stick :
    case Input_VR_Right_Stick_Click :
    case Input_VR_Right_Touchpad_Touch :
    case Input_VR_Right_Touchpad :
    case Input_VR_Right_Touchpad_Press :
    case Input_VR_Right_Touchpad_Click :
    case Input_VR_Right_System_Touch :
    case Input_VR_Right_System :
    case Input_VR_Right_A_Touch :
    case Input_VR_Right_A :
    case Input_VR_Right_B_Touch :
    case Input_VR_Right_B :
    case Input_VR_Right_Menu_Touch :
    case Input_VR_Right_Menu :
    case Input_VR_Right_Bumper_Touch :
    case Input_VR_Right_Bumper :
    case Input_VR_Right_Thumbrest_Touch :
    case Input_VR_Right_Thumbrest :
        return true;
    default :
        return false;
    }
}

bool isLeft(InputType type)
{
    switch (type)
    {
    case Input_Gamepad_Left_Stick :
    case Input_Gamepad_Left_Stick_Click :
    case Input_Gamepad_Left_Bumper :
    case Input_Gamepad_Left_Trigger :
    case Input_VR_Left_Hand :
    case Input_VR_Left_Foot :
    case Input_VR_Left_Shoulder :
    case Input_VR_Left_Elbow :
    case Input_VR_Left_Wrist :
    case Input_VR_Left_Knee :
    case Input_VR_Left_Ankle :
    case Input_VR_Left_Eye :
    case Input_VR_Left_Eyebrow :
    case Input_VR_Left_Eyelid :
    case Input_VR_Left_Pupil :
    case Input_VR_Left_Thumb :
    case Input_VR_Left_Index :
    case Input_VR_Left_Middle :
    case Input_VR_Left_Ring :
    case Input_VR_Left_Little :
    case Input_VR_Left_Trigger_Touch :
    case Input_VR_Left_Trigger :
    case Input_VR_Left_Trigger_Click :
    case Input_VR_Left_Grip_Touch :
    case Input_VR_Left_Grip :
    case Input_VR_Left_Grip_Click :
    case Input_VR_Left_Stick_Touch :
    case Input_VR_Left_Stick :
    case Input_VR_Left_Stick_Click :
    case Input_VR_Left_Touchpad_Touch :
    case Input_VR_Left_Touchpad :
    case Input_VR_Left_Touchpad_Press :
    case Input_VR_Left_Touchpad_Click :
    case Input_VR_Left_System_Touch :
    case Input_VR_Left_System :
    case Input_VR_Left_A_Touch :
    case Input_VR_Left_A :
    case Input_VR_Left_B_Touch :
    case Input_VR_Left_B :
    case Input_VR_Left_Menu_Touch :
    case Input_VR_Left_Menu :
    case Input_VR_Left_Bumper_Touch :
    case Input_VR_Left_Bumper :
    case Input_VR_Left_Thumbrest :
    case Input_VR_Left_Thumbrest_Touch :
        return true;
    default :
        return false;
    }
}

bool isRight(InputType type)
{
    switch (type)
    {
    case Input_Gamepad_Right_Stick :
    case Input_Gamepad_Right_Stick_Click :
    case Input_Gamepad_Right_Bumper :
    case Input_Gamepad_Right_Trigger :
    case Input_VR_Right_Hand :
    case Input_VR_Right_Foot :
    case Input_VR_Right_Shoulder :
    case Input_VR_Right_Elbow :
    case Input_VR_Right_Wrist :
    case Input_VR_Right_Knee :
    case Input_VR_Right_Ankle :
    case Input_VR_Right_Eye :
    case Input_VR_Right_Eyebrow :
    case Input_VR_Right_Eyelid :
    case Input_VR_Right_Pupil :
    case Input_VR_Right_Thumb :
    case Input_VR_Right_Index :
    case Input_VR_Right_Middle :
    case Input_VR_Right_Ring :
    case Input_VR_Right_Little :
    case Input_VR_Right_Trigger_Touch :
    case Input_VR_Right_Trigger :
    case Input_VR_Right_Trigger_Click :
    case Input_VR_Right_Grip_Touch :
    case Input_VR_Right_Grip :
    case Input_VR_Right_Grip_Click :
    case Input_VR_Right_Stick_Touch :
    case Input_VR_Right_Stick :
    case Input_VR_Right_Stick_Click :
    case Input_VR_Right_Touchpad_Touch :
    case Input_VR_Right_Touchpad :
    case Input_VR_Right_Touchpad_Press :
    case Input_VR_Right_Touchpad_Click :
    case Input_VR_Right_System_Touch :
    case Input_VR_Right_System :
    case Input_VR_Right_A_Touch :
    case Input_VR_Right_A :
    case Input_VR_Right_B_Touch :
    case Input_VR_Right_B :
    case Input_VR_Right_Menu_Touch :
    case Input_VR_Right_Menu :
    case Input_VR_Right_Bumper_Touch :
    case Input_VR_Right_Bumper :
    case Input_VR_Right_Thumbrest :
    case Input_VR_Right_Thumbrest_Touch :
        return true;
    default :
        return false;
    }
}

bool isLeftHand(InputType type)
{
    switch (type)
    {
    case Input_VR_Left_Hand :
    case Input_VR_Left_Trigger_Touch :
    case Input_VR_Left_Trigger :
    case Input_VR_Left_Trigger_Click :
    case Input_VR_Left_Grip_Touch :
    case Input_VR_Left_Grip :
    case Input_VR_Left_Grip_Click :
    case Input_VR_Left_Stick_Touch :
    case Input_VR_Left_Stick :
    case Input_VR_Left_Stick_Click :
    case Input_VR_Left_Touchpad_Touch :
    case Input_VR_Left_Touchpad :
    case Input_VR_Left_Touchpad_Press :
    case Input_VR_Left_Touchpad_Click :
    case Input_VR_Left_System_Touch :
    case Input_VR_Left_System :
    case Input_VR_Left_A_Touch :
    case Input_VR_Left_A :
    case Input_VR_Left_B_Touch :
    case Input_VR_Left_B :
    case Input_VR_Left_Menu_Touch :
    case Input_VR_Left_Menu :
    case Input_VR_Left_Bumper_Touch :
    case Input_VR_Left_Bumper :
    case Input_VR_Left_Thumbrest_Touch :
    case Input_VR_Left_Thumbrest :
        return true;
    default :
        return false;
    }
}

bool isRightHand(InputType type)
{
    switch (type)
    {
    case Input_VR_Right_Hand :
    case Input_VR_Right_Trigger_Touch :
    case Input_VR_Right_Trigger :
    case Input_VR_Right_Trigger_Click :
    case Input_VR_Right_Grip_Touch :
    case Input_VR_Right_Grip :
    case Input_VR_Right_Grip_Click :
    case Input_VR_Right_Stick_Touch :
    case Input_VR_Right_Stick :
    case Input_VR_Right_Stick_Click :
    case Input_VR_Right_Touchpad_Touch :
    case Input_VR_Right_Touchpad :
    case Input_VR_Right_Touchpad_Press :
    case Input_VR_Right_Touchpad_Click :
    case Input_VR_Right_System_Touch :
    case Input_VR_Right_System :
    case Input_VR_Right_A_Touch :
    case Input_VR_Right_A :
    case Input_VR_Right_B_Touch :
    case Input_VR_Right_B :
    case Input_VR_Right_Menu_Touch :
    case Input_VR_Right_Menu :
    case Input_VR_Right_Bumper_Touch :
    case Input_VR_Right_Bumper :
    case Input_VR_Right_Thumbrest_Touch :
    case Input_VR_Right_Thumbrest :
        return true;
    default :
        return false;
    }
}

InputDimensions emptyDimensions()
{
    InputDimensions dimensions;

    dimensions.fill(0);

    return dimensions;
}

// FreeBSD defines these in the global namespace for some reason
#undef major
#undef minor

InputTouch::InputTouch()
    : id(0)
    , major(0)
    , minor(0)
    , orientation(0)
    , force(0)
{

}

InputTouch::InputTouch(u32 id, f64 major, f64 minor, f64 orientation, f64 force)
    : id(id)
    , major(major)
    , minor(minor)
    , orientation(orientation)
    , force(force)
{

}

bool InputTouch::operator==(const InputTouch& rhs) const
{
    return id == rhs.id &&
        major == rhs.major &&
        minor == rhs.minor &&
        orientation == rhs.orientation &&
        force == rhs.force;
}

bool InputTouch::operator!=(const InputTouch& rhs) const
{
    return !(*this == rhs);
}

Input::Input()
{
    type = Input_Null;
    up = false;
    intensity = 0;

    shift = false;
    control = false;
    alt = false;
    super = false;

    dimensions = emptyDimensions();

#if defined(LMOD_CLIENT) && defined(LMOD_WAYLAND)
    serial = 0;
#endif
}

Input::Input(
    InputType type,
    bool up,
    f64 intensity,
    const vec3& position,
    const quaternion& rotation,
    const InputDimensions& dimensions,
    const InputTouch& touch
)
    : Input()
{
    this->type = type;
    this->up = up;
    this->intensity = intensity;
    this->position = position;
    this->rotation = rotation;
    this->dimensions = dimensions;
    this->touch = touch;
}

bool Input::operator==(const Input& rhs) const
{
    return type == rhs.type &&
        up == rhs.up &&
        intensity == rhs.intensity &&
        position == rhs.position &&
        rotation == rhs.rotation &&
        linearMotion == rhs.linearMotion &&
        angularMotion == rhs.angularMotion &&
        dimensions == rhs.dimensions &&
        touch == rhs.touch &&
        shift == rhs.shift &&
        control == rhs.control &&
        alt == rhs.alt &&
        super == rhs.super;
}

bool Input::operator!=(const Input& rhs) const
{
    return !(*this == rhs);
}

bool Input::isKeyboard() const
{
    return ::isKeyboard(type);
}

bool Input::isMouse() const
{
    return ::isMouse(type);
}

bool Input::isGamepad() const
{
    return ::isGamepad(type);
}

bool Input::isTouch() const
{
    return ::isTouch(type);
}

bool Input::isFlat() const
{
    return ::isFlat(type);
}

bool Input::isVR() const
{
    return ::isVR(type);
}

bool Input::isLeft() const
{
    return ::isLeft(type);
}

bool Input::isRight() const
{
    return ::isRight(type);
}

bool Input::isLeftHand() const
{
    return ::isLeftHand(type);
}

bool Input::isRightHand() const
{
    return ::isRightHand(type);
}

string Input::text() const
{
    if (up)
    {
        return "";
    }

    switch (type)
    {
    case Input_0 : return shift ? ")" : "0";
    case Input_1 : return shift ? "!" : "1";
    case Input_2 : return shift ? "@" : "2";
    case Input_3 : return shift ? "#" : "3";
    case Input_4 : return shift ? "$" : "4";
    case Input_5 : return shift ? "%" : "5";
    case Input_6 : return shift ? "^" : "6";
    case Input_7 : return shift ? "&" : "7";
    case Input_8 : return shift ? "*" : "8";
    case Input_9 : return shift ? "(" : "9";
    case Input_A : return shift ? "A" : "a";
    case Input_B : return shift ? "B" : "b";
    case Input_C : return shift ? "C" : "c";
    case Input_D : return shift ? "D" : "d";
    case Input_E : return shift ? "E" : "e";
    case Input_F : return shift ? "F" : "f";
    case Input_G : return shift ? "G" : "g";
    case Input_H : return shift ? "H" : "h";
    case Input_I : return shift ? "I" : "i";
    case Input_J : return shift ? "J" : "j";
    case Input_K : return shift ? "K" : "k";
    case Input_L : return shift ? "L" : "l";
    case Input_M : return shift ? "M" : "m";
    case Input_N : return shift ? "N" : "n";
    case Input_O : return shift ? "O" : "o";
    case Input_P : return shift ? "P" : "p";
    case Input_Q : return shift ? "Q" : "q";
    case Input_R : return shift ? "R" : "r";
    case Input_S : return shift ? "S" : "s";
    case Input_T : return shift ? "T" : "t";
    case Input_U : return shift ? "U" : "u";
    case Input_V : return shift ? "V" : "v";
    case Input_W : return shift ? "W" : "w";
    case Input_X : return shift ? "X" : "x";
    case Input_Y : return shift ? "Y" : "y";
    case Input_Z : return shift ? "Z" : "z";
    case Input_Tab : return "\t";
    case Input_Return : return "\n";
    case Input_Space : return " ";
    case Input_Grave_Accent : return shift ? "~" : "`";
    case Input_Minus : return shift ? "_" : "-";
    case Input_Equals : return shift ? "+" : "=";
    case Input_Left_Brace : return shift ? "{" : "[";
    case Input_Right_Brace : return shift ? "}" : "]";
    case Input_Backslash : return shift ? "|" : "\\";
    case Input_Semicolon : return shift ? ":" : ";";
    case Input_Quote : return shift ? "\"" : "'";
    case Input_Period : return shift ? ">" : ".";
    case Input_Comma : return shift ? "<" : ",";
    case Input_Slash : return shift ? "?" : "/";
    case Input_Num_Add : return "+";
    case Input_Num_Subtract : return "-";
    case Input_Num_Multiply : return "*";
    case Input_Num_Divide : return "/";
    case Input_Num_0 : return "0";
    case Input_Num_1 : return "1";
    case Input_Num_2 : return "2";
    case Input_Num_3 : return "3";
    case Input_Num_4 : return "4";
    case Input_Num_5 : return "5";
    case Input_Num_6 : return "6";
    case Input_Num_7 : return "7";
    case Input_Num_8 : return "8";
    case Input_Num_9 : return "9";
    case Input_Num_Period : return ".";
    case Input_Num_Return : return "\n";
    default : return "";
    }
}

ostream& operator<<(ostream& out, const Input& input)
{
    out << "(" << inputTypeToString(input.type);

    out << ", up: " << (input.up ? "yes" : "no");

    out << ", intensity: " << input.intensity;

    out << ", position: " << input.position;
    out << ", rotation: " << input.rotation;
    out << ", linearMotion: " << input.linearMotion;
    out << ", angularMotion: " << input.angularMotion;
    out << ", dimensions: [";

    for (size_t i = 0; i < 6; i++)
    {
        out << input.dimensions[i];

        if (i + 1 != 6)
        {
            out << ", ";
        }
    }

    out << "])" << endl;

    return out;
}

InputBinding::InputBinding()
{

}

InputBinding::InputBinding(const string& input)
    : InputBinding()
{
    this->input = input;
}

void InputBinding::verify()
{
    input = isValidName(input) ? input : toValidName(input);
}

bool InputBinding::operator==(const InputBinding& rhs) const
{
    return input == rhs.input;
}

bool InputBinding::operator!=(const InputBinding& rhs) const
{
    return !(*this == rhs);
}

template<>
InputBinding YAMLNode::convert() const
{
    InputBinding binding;

    binding.input = at("input").as<string>();

    return binding;
}

template<>
void YAMLSerializer::emit(InputBinding binding)
{
    startMapping();

    emitPair("input", binding.input);

    endMapping();
}

HapticTarget hapticTargetFromString(const string& s)
{
    if (s == "haptic-left-hand")
    {
        return Haptic_Target_Left_Hand;
    }
    else if (s == "haptic-right-hand")
    {
        return Haptic_Target_Right_Hand;
    }
    else
    {
        throw runtime_error("Encountered unknown haptic target '" + s + "'.");
    }
}

string hapticTargetToString(HapticTarget target)
{
    switch (target)
    {
    default :
        fatal("Encountered unknown haptic target '" + to_string(target) + "'.");
    case Haptic_Target_Left_Hand :
        return "haptic-left-hand";
    case Haptic_Target_Right_Hand :
        return "haptic-right-hand";
    }
}

HapticEffect::HapticEffect()
{
    target = Haptic_Target_Left_Hand;
    duration = 0;
    frequency = 0;
    amplitude = 0;
}

HapticEffect::HapticEffect(HapticTarget target, u32 duration, f64 frequency, f64 amplitude)
    : HapticEffect()
{
    this->target = target;
    this->duration = duration;
    this->frequency = frequency;
    this->amplitude = amplitude;
}
