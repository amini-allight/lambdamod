/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "symbols.hpp"
#include "emissive_color.hpp"
#include "hdr_color.hpp"
#include "structure.hpp"
#include "terrain.hpp"
#include "bounding_box.hpp"
#if defined(LMOD_CLIENT)
#include "client/text_style.hpp"
#endif
#include "sky_parameters.hpp"
#include "atmosphere_parameters.hpp"

struct BodyPartLine
{
    BodyPartLine();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            diameter,
            length,
            startColor,
            endColor,
            constraintAtEnd
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        diameter = std::get<0>(payload);
        length = std::get<1>(payload);
        startColor = std::get<2>(payload);
        endColor = std::get<3>(payload);
        constraintAtEnd = std::get<4>(payload);
    }

    bool operator==(const BodyPartLine& rhs) const;
    bool operator!=(const BodyPartLine& rhs) const;

    typedef msgpack::type::tuple<f64, f64, EmissiveColor, EmissiveColor, bool> layout;

    f64 diameter;
    f64 length;
    EmissiveColor startColor;
    EmissiveColor endColor;
    bool constraintAtEnd;
};

enum BodyPlaneType : u8
{
    Body_Plane_Ellipse,
    Body_Plane_Equilateral_Triangle,
    Body_Plane_Isosceles_Triangle,
    Body_Plane_Right_Angle_Triangle,
    Body_Plane_Rectangle,
    Body_Plane_Pentagon,
    Body_Plane_Hexagon,
    Body_Plane_Heptagon,
    Body_Plane_Octagon
};

string bodyPlaneTypeToString(BodyPlaneType type);
BodyPlaneType bodyPlaneTypeFromString(const string& s);

struct BodyPartPlane
{
    BodyPartPlane();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            type,
            thickness,
            width,
            height,
            color,
            constraintSubID,
            empty
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<BodyPlaneType>(get<0>(payload));
        thickness = std::get<1>(payload);
        width = std::get<2>(payload);
        height = std::get<3>(payload);
        color = std::get<4>(payload);
        constraintSubID = std::get<5>(payload);
        empty = std::get<6>(payload);
    }

    bool operator==(const BodyPartPlane& rhs) const;
    bool operator!=(const BodyPartPlane& rhs) const;

    typedef msgpack::type::tuple<u8, f64, f64, f64, EmissiveColor, BodyPartSubID, bool> layout;

    BodyPlaneType type;
    f64 thickness;
    f64 width;
    f64 height;
    EmissiveColor color;
    BodyPartSubID constraintSubID;
    bool empty;
};

enum BodyAnchorType : u8
{
    Body_Anchor_Generic,

    Body_Anchor_Grip,

    Body_Anchor_Viewer,
    Body_Anchor_Room,

    Body_Anchor_Head,
    Body_Anchor_Hips,
    Body_Anchor_Left_Hand,
    Body_Anchor_Right_Hand,
    Body_Anchor_Left_Foot,
    Body_Anchor_Right_Foot,
    Body_Anchor_Chest,
    Body_Anchor_Left_Shoulder,
    Body_Anchor_Right_Shoulder,
    Body_Anchor_Left_Elbow,
    Body_Anchor_Right_Elbow,
    Body_Anchor_Left_Wrist,
    Body_Anchor_Right_Wrist,
    Body_Anchor_Left_Knee,
    Body_Anchor_Right_Knee,
    Body_Anchor_Left_Ankle,
    Body_Anchor_Right_Ankle,

    Body_Anchor_Eyes,

    Body_Anchor_Left_Eye,
    Body_Anchor_Left_Eyebrow,
    Body_Anchor_Left_Eyelid,
    Body_Anchor_Right_Eye,
    Body_Anchor_Right_Eyebrow,
    Body_Anchor_Right_Eyelid,
    Body_Anchor_Mouth,

    Body_Anchor_Left_Thumb,
    Body_Anchor_Left_Index,
    Body_Anchor_Left_Middle,
    Body_Anchor_Left_Ring,
    Body_Anchor_Left_Little,

    Body_Anchor_Right_Thumb,
    Body_Anchor_Right_Index,
    Body_Anchor_Right_Middle,
    Body_Anchor_Right_Ring,
    Body_Anchor_Right_Little
};

string bodyAnchorTypeToString(BodyAnchorType type);
BodyAnchorType bodyAnchorTypeFromString(const string& s);

struct BodyPartAnchor
{
    BodyPartAnchor();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            type,
            name,
            massLimit,
            pullDistance
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<BodyAnchorType>(get<0>(payload));
        name = std::get<1>(payload);
        massLimit = std::get<2>(payload);
        pullDistance = std::get<3>(payload);
    }

    bool operator==(const BodyPartAnchor& rhs) const;
    bool operator!=(const BodyPartAnchor& rhs) const;

    typedef msgpack::type::tuple<u8, string, f64, f64> layout;

    BodyAnchorType type;
    string name;
    f64 massLimit;
    f64 pullDistance;
};

enum BodySolidType : u8
{
    Body_Solid_Cuboid,
    Body_Solid_Sphere,
    Body_Solid_Cylinder,
    Body_Solid_Cone,
    Body_Solid_Capsule,
    Body_Solid_Pyramid,
    Body_Solid_Tetrahedron,
    Body_Solid_Octahedron,
    Body_Solid_Dodecahedron,
    Body_Solid_Icosahedron
};

string bodySolidTypeToString(BodySolidType type);
BodySolidType bodySolidTypeFromString(const string& s);

struct BodyPartSolid
{
    BodyPartSolid();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            type,
            size,
            color,
            constraintSubID
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<BodySolidType>(get<0>(payload));
        size = std::get<1>(payload);
        color = std::get<2>(payload);
        constraintSubID = std::get<3>(payload);
    }

    bool operator==(const BodyPartSolid& rhs) const;
    bool operator!=(const BodyPartSolid& rhs) const;

    typedef msgpack::type::tuple<u8, vec3, EmissiveColor, BodyPartSubID> layout;

    BodySolidType type;
    vec3 size;
    EmissiveColor color;
    BodyPartSubID constraintSubID;
};

enum BodyTextType : u8
{
    Body_Text_Default
};

struct BodyPartText
{
    BodyPartText();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            type,
            text,
            height,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<BodyTextType>(get<0>(payload));
        text = std::get<1>(payload);
        height = std::get<2>(payload);
        color = std::get<3>(payload);
    }

    bool operator==(const BodyPartText& rhs) const;
    bool operator!=(const BodyPartText& rhs) const;

#if defined(LMOD_CLIENT)
    TextStyle style() const;
#endif

    typedef msgpack::type::tuple<u8, string, f64, EmissiveColor> layout;

    BodyTextType type;
    string text;
    f64 height;
    EmissiveColor color;
};

struct BodyPartSymbol
{
    BodyPartSymbol();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            type,
            size,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        type = static_cast<SymbolType>(get<0>(payload));
        size = std::get<1>(payload);
        color = std::get<2>(payload);
    }

    bool operator==(const BodyPartSymbol& rhs) const;
    bool operator!=(const BodyPartSymbol& rhs) const;

    typedef msgpack::type::tuple<u8, f64, EmissiveColor> layout;

    SymbolType type;
    f64 size;
    EmissiveColor color;
};

struct BodyPartCanvas
{
    BodyPartCanvas();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            size,
            pixels,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        size = std::get<0>(payload);
        pixels = std::get<1>(payload);
        color = std::get<2>(payload);
    }

    bool operator==(const BodyPartCanvas& rhs) const;
    bool operator!=(const BodyPartCanvas& rhs) const;

    void set(const uvec2& position, bool value);
    bool get(const uvec2& position) const;

    typedef msgpack::type::tuple<f64, vector<u8>, EmissiveColor> layout;

    f64 size;
    vector<u8> pixels;
    EmissiveColor color;
};

struct BodyPartStructure
{
    BodyPartStructure();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            positive,
            nextTypeID,
            types,
            nextNodeID,
            node,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        positive = std::get<0>(payload);
        nextTypeID = std::get<1>(payload);
        types = std::get<2>(payload);
        nextNodeID = std::get<3>(payload);
        node = std::get<4>(payload);
        color = std::get<5>(payload);
    }

    bool operator==(const BodyPartStructure& rhs) const;
    bool operator!=(const BodyPartStructure& rhs) const;

    StructureNode* getNodeByIndex(size_t index);
    const StructureNode* getNodeByIndex(size_t index) const;

    typedef msgpack::type::tuple<bool, StructureTypeID, vector<StructureType>, StructureNodeID, StructureNode, EmissiveColor> layout;

    bool positive;
    StructureTypeID nextTypeID;
    vector<StructureType> types;
    StructureNodeID nextNodeID;
    StructureNode node;
    EmissiveColor color;
};

struct BodyPartTerrain
{
    BodyPartTerrain();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            nextNodeID,
            nodes,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        nextNodeID = std::get<0>(payload);
        nodes = std::get<1>(payload);
        color = std::get<2>(payload);
    }

    bool operator==(const BodyPartTerrain& rhs) const;
    bool operator!=(const BodyPartTerrain& rhs) const;

    typedef msgpack::type::tuple<TerrainNodeID, vector<TerrainNode>, EmissiveColor> layout;

    TerrainNodeID nextNodeID;
    vector<TerrainNode> nodes;
    EmissiveColor color;
};

struct BodyPartLight
{
    BodyPartLight();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            enabled,
            angle,
            color
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        enabled = std::get<0>(payload);
        angle = std::get<1>(payload);
        color = std::get<2>(payload);
    }

    bool operator==(const BodyPartLight& rhs) const;
    bool operator!=(const BodyPartLight& rhs) const;

    typedef msgpack::type::tuple<bool, f64, HDRColor> layout;

    bool enabled;
    f64 angle;
    HDRColor color;
};

enum BodyForceType : u8
{
    Body_Force_Linear,
    Body_Force_Vortex,
    Body_Force_Omnidirectional,
    Body_Force_Fan,
    Body_Force_Rocket,
    Body_Force_Gravity,
    Body_Force_Magnetism
};

string bodyForceTypeToString(BodyForceType type);
BodyForceType bodyForceTypeFromString(const string& s);

struct BodyPartForce
{
    BodyPartForce();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            enabled,
            static_cast<u8>(type),
            intensity,
            radius,
            massFlowRate,
            reaction
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        enabled = std::get<0>(payload);
        type = static_cast<BodyForceType>(get<1>(payload));
        intensity = std::get<2>(payload);
        radius = std::get<3>(payload);
        massFlowRate = std::get<4>(payload);
        reaction = std::get<5>(payload);
    }

    bool operator==(const BodyPartForce& rhs) const;
    bool operator!=(const BodyPartForce& rhs) const;

    typedef msgpack::type::tuple<bool, u8, f64, f64, f64, bool> layout;

    bool enabled;
    BodyForceType type;
    f64 intensity;
    f64 radius;
    f64 massFlowRate;
    bool reaction;
};

struct BodyPartArea
{
    BodyPartArea();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    optional<f64> immersion(const vec3& position) const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            points,
            height,

            enabled,
            speedOfSound,
            gravity,
            flowVelocity,
            density,
            up,
            fogDistance,
            sky,
            atmosphere,

            surface,
            color,
            opaque,
            animationSpeed
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        points = std::get<0>(payload);
        height = std::get<1>(payload);

        enabled = std::get<2>(payload);
        speedOfSound = std::get<3>(payload);
        gravity = std::get<4>(payload);
        flowVelocity = std::get<5>(payload);
        density = std::get<6>(payload);
        up = std::get<7>(payload);
        fogDistance = std::get<8>(payload);
        sky = std::get<9>(payload);
        atmosphere = std::get<10>(payload);

        surface = std::get<11>(payload);
        color = std::get<12>(payload);
        opaque = std::get<13>(payload);
        animationSpeed = std::get<14>(payload);
    }

    bool operator==(const BodyPartArea& rhs) const;
    bool operator!=(const BodyPartArea& rhs) const;

    typedef msgpack::type::tuple<
        vector<vec2>,
        f64,
        bool,
        optional<f64>,
        optional<vec3>,
        optional<vec3>,
        optional<f64>,
        optional<vec3>,
        optional<f64>,
        optional<SkyParameters>,
        optional<AtmosphereParameters>,
        bool,
        EmissiveColor,
        bool,
        f64
    > layout;

    vector<vec2> points;
    f64 height;

    bool enabled;
    optional<f64> speedOfSound;
    optional<vec3> gravity;
    optional<vec3> flowVelocity;
    optional<f64> density;
    optional<vec3> up;
    optional<f64> fogDistance;
    optional<SkyParameters> sky;
    optional<AtmosphereParameters> atmosphere;

    bool surface;
    EmissiveColor color;
    bool opaque;
    f64 animationSpeed;
};

struct BodyPartRope
{
    BodyPartRope();

    void verify();

    vector<vec3> subparts() const;

    vector<vec3> intersect(const vec3& position, const vec3& direction, f64 distance) const;
    bool within(const vec3& position) const;
    f64 boundingRadius() const;
    BoundingBox boundingBox() const;

    template<typename T>
    void msgpack_pack(msgpack::packer<T>& packer) const
    {
        layout payload = {
            segmentCount,
            length,
            points,
            radius,
            startColor,
            endColor,
            thin,
            constraintSubID
        };

        packer.pack(payload);
    }

    void msgpack_unpack(const msgpack::object& obj)
    {
        layout payload;
        obj.convert(payload);

        segmentCount = std::get<0>(payload);
        length = std::get<1>(payload);
        points = std::get<2>(payload);
        radius = std::get<3>(payload);
        startColor = std::get<4>(payload);
        endColor = std::get<5>(payload);
        thin = std::get<6>(payload);
        constraintSubID = std::get<7>(payload);
    }

    bool operator==(const BodyPartRope& rhs) const;
    bool operator!=(const BodyPartRope& rhs) const;

    void setExtent(u32 segmentCount, f64 length);

    typedef msgpack::type::tuple<u32, f64, vector<vec3>, f64, EmissiveColor, EmissiveColor, bool, BodyPartSubID> layout;

    u32 segmentCount;
    f64 length;
    vector<vec3> points;
    f64 radius;
    EmissiveColor startColor;
    EmissiveColor endColor;
    bool thin;
    BodyPartSubID constraintSubID;
};
