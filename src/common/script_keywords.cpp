/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "script_keywords.hpp"
#include "script_tools.hpp"
#include "script_builtin_tools.hpp"
#include "scope_closure.hpp"
#include "tools.hpp"
#include "game_state.hpp"

static void checkArgumentLayout(const string& functionName, const vector<Value>& values)
{
    bool collector = false;
    set<string> definitions;

    for (const Value& value : values)
    {
        if (value.type == Value_Symbol)
        {
            if (value.s.ends_with("...") || value.s.ends_with("...+"))
            {
                if (collector)
                {
                    throw toValue("Only one wildcard is allowed per list in syntax definition argument layout in '" + functionName + "'.");
                }

                collector = true;
            }
            else
            {
                if (definitions.contains(value.s))
                {
                    throw toValue("Multiple definitions of '" + value.s + "' encountered in syntax definition argument layout in '" + functionName + "'.");
                }

                definitions.insert(value.s);
            }
        }
        else if (value.type == Value_List)
        {
            checkArgumentLayout(functionName, value.l);
        }
        else
        {
            throw toValue("Expected lists and symbols in syntax definition argument layout in '" + functionName + "', got '" + value.toString() + "'.");
        }
    }
}

static Value builtin_define(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    const Value& signature = args[0];
    vector<Value> bodies = { args.begin() + 1, args.end() };

    string name;
    Value value;

    if (signature.type == Value_Symbol)
    {
        name = signature.s;

        if (bodies.size() != 1)
        {
            throw toValue("Expected only 2 arguments to the basic form of 'define', got '" + to_string(args.size()) + "'.");
        }

        value = context->run(bodies.front());
    }
    else if (signature.type == Value_List && !signature.l.empty())
    {
        name = signature.l[0].s;

        vector<string> argNames;

        for (size_t i = 1; i < signature.l.size(); i++)
        {
            const Value& item = signature.l[i];

            if (item.type != Value_Symbol)
            {
                throw toValue("Expected symbol or list of symbols as argument 1 to 'define', got '" + signature.toString() + "'.");
            }

            if (find(value.lambda.argNames.begin(), value.lambda.argNames.end(), item.s) != value.lambda.argNames.end())
            {
                throw toValue("Encountered argument '" + item.s + "' multiple times in argument 1 to 'define'.");
            }

            argNames.push_back(item.s);
        }

        ScopeClosure closure(context);

        for (const string& argName : argNames)
        {
            context->define(argName, symbol(argName));
        }

        value.type = Value_Lambda;
        value.lambda.type = Lambda_Normal;
        value.lambda.argNames = argNames;

        for (const Value& item : bodies)
        {
            value.lambda.body.push_back(context->bind(item));
        }
    }
    else
    {
        throw toValue("Expected symbol or list of symbols as argument 1 to 'define', got '" + signature.toString() + "'.");
    }

    if (!context->canWrite(context->currentUser(), name))
    {
        throw toValue("Permission to 'define' global variable '" + name + "' denied.");
    }

    context->define(
        name,
        value
    );

    return Value();
}

static Value builtin_undefine(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1);
    CHECK_ARG_SYMBOL(0);

    string name = args[0].s;

    if (!context->has(name))
    {
        throw toValue("Could not 'undefine', definition '" + name + "' not found.");
    }

    if (!context->canWrite(context->currentUser(), name))
    {
        throw toValue("Permission to 'undefine' global variable '" + name + "' denied.");
    }

    context->undefine(name);

    return Value();
}

static Value builtin_define_restrict(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);

    string name = args[0].s;

    if (args.size() == 1)
    {
        if (!context->has(name))
        {
            throw toValue("Could not 'define-restrict', definition '" + name + "' not found.");
        }

        return toValue(context->isRestricted(name));
    }
    else
    {
        Value state = context->run(args[1]);

        if (state.type != Value_Integer)
        {
            throw toValue("Expected integer as argument 2 to 'define-restrict', got '" + state.toString() + "'.");
        }

        if (!context->has(name))
        {
            throw toValue("Could not 'define-restrict', definition '" + name + "' not found.");
        }

        context->restrictDefine(name, state.i);

        return Value();
    }
}

static Value builtin_define_control(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT({ 1, 2 });
    CHECK_ARG_SYMBOL(0);

    string name = args[0].s;
    Value userID = context->run(args[1]);

    if (args.size() == 1)
    {
        if (!context->has(name))
        {
            throw toValue("Could not 'define-control', definition '" + name + "' not found.");
        }

        return toValue(context->getOwner(name));
    }
    else
    {
        if (userID.type != Value_Integer)
        {
            throw toValue("Expected integer as argument 2 to 'define-restrict', got '" + userID.toString() + "'.");
        }

        if (!context->has(name))
        {
            throw toValue("Could not 'define-control', definition '" + name + "' not found.");
        }

        if (!context->canWrite(context->currentUser(), name))
        {
            throw toValue("Could not 'define-control', permission to alter global variable '" + name + "' denied.");
        }

        context->controlDefine(name, UserID(userID.i));

        return Value();
    }
}

static Value builtin_define_find(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1);
    CHECK_ARG_SYMBOL(0);

    string pattern = args[0].s;

    set<string> defines;

    for (const map<string, Value>& scope : context->scopes)
    {
        for (const auto& [ name, value ] : scope)
        {
            if (name.find(pattern) == string::npos)
            {
                continue;
            }

            defines.insert(name);
        }
    }

    Value result;
    result.type = Value_List;
    result.l.reserve(defines.size());

    for (const string& define : defines)
    {
        result.l.push_back(symbol(define));
    }

    return result;
}

static Value builtin_define_list(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    set<string> defines;

    for (const map<string, Value>& scope : context->scopes)
    {
        for (const auto& [ name, value ] : scope)
        {
            defines.insert(name);
        }
    }

    Value result;
    result.type = Value_List;
    result.l.reserve(defines.size());

    for (const string& define : defines)
    {
        result.l.push_back(symbol(define));
    }

    return result;
}

static Value builtin_define_exists(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1);
    CHECK_ARG_SYMBOL(0);

    string name = args[0].s;

    return toValue(context->get(name).has_value());
}

static Value builtin_define_syntax(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2);
    CHECK_ARG_LIST(1);

    if (args[0].type != Value_List || args[0].l.empty())
    {
        throw toValue("Expected list of symbols as argument 1 to 'define-syntax', got '" + args[0].toString() + "'.");
    }

    if (args[0].l[0].type != Value_Symbol)
    {
        throw toValue("Expected symbol as element 1 in argument 1 to 'define-syntax', got '" + args[0].l[0].toString() + "'.");
    }

    string name = args[0].l[0].s;
    vector<Value> layout = { args[0].l.begin() + 1, args[0].l.end() };

    checkArgumentLayout(functionName, layout);

    Value body;
    body.type = Value_Lambda;
    body.lambda.type = Lambda_Syntax;
    body.lambda.argLayout = layout;
    body.lambda.body = { args[1] };

    if (!context->canWrite(context->currentUser(), name))
    {
        throw toValue("Permission to 'define' global variable '" + name + "' denied.");
    }

    context->define(
        name,
        body
    );

    return Value();
}

static Value builtin_get(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value index = args[1];

    if (target.type == Value_List)
    {
        if (index.i < 0 || static_cast<size_t>(index.i) >= target.l.size())
        {
            throw toValue("Could not 'get' element " + to_string(index.i) + " in list, index out of range.");
        }

        return target.l[index.i];
    }
    else if (target.type == Value_String)
    {
        if (index.i < 0 || static_cast<size_t>(index.i) >= target.s.size())
        {
            throw toValue("Could not 'get' element " + to_string(index.i) + " in string, index out of range.");
        }

        return toValue(static_cast<i64>(target.s[index.i]));
    }
    else if (target.type == Value_Symbol)
    {
        if (index.i < 0 || static_cast<size_t>(index.i) >= target.s.size())
        {
            throw toValue("Could not 'get' element " + to_string(index.i) + " in symbol, index out of range.");
        }

        return toValue(static_cast<i64>(target.s[index.i]));
    }
    else
    {
        throw toValue("Expected list, string or symbol as argument 1 to 'get', got '" + target.toString() + "'.");
    }
}

static Value builtin_set(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARG_INTEGER(1);

    Value target = args[0];
    Value index = args[1];
    Value value = args[2];

    if (target.type == Value_List)
    {
        if (index.i < 0 || static_cast<size_t>(index.i) >= target.l.size())
        {
            throw toValue("Could not 'set' element " + to_string(index.i) + " in list, index out of range.");
        }

        target.l[index.i] = value;

        return target;
    }
    else if (target.type == Value_String)
    {
        if (index.i < 0 || static_cast<size_t>(index.i) >= target.s.size())
        {
            throw toValue("Could not 'set' element " + to_string(index.i) + " in string, index out of range.");
        }

        if (value.type != Value_Integer)
        {
            throw toValue("Expected integer as argument 3 to 'set' when the argument 1 is a string, got '" + value.toString() + "'.");
        }

        target.s[index.i] = value.i;

        return target;
    }
    else if (target.type == Value_Symbol)
    {
        if (index.i < 0 || static_cast<size_t>(index.i) >= target.s.size())
        {
            throw toValue("Could not 'set' element " + to_string(index.i) + " in symbol, index out of range.");
        }

        if (value.type != Value_Integer)
        {
            throw toValue("Expected integer as argument 3 to 'set' when the argument 1 is a symbol, got '" + value.toString() + "'.");
        }

        target.s[index.i] = value.i;

        return target;
    }
    else
    {
        throw toValue("Expected list, string or symbol as argument 1 to 'set', got '" + target.toString() + "'.");
    }
}

static Value builtin_size(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    Value value = args[0];

    if (value.type == Value_List)
    {
        return toValue(value.l.size());
    }
    else if (value.type == Value_Symbol || value.type == Value_String)
    {
        return toValue(value.s.size());
    }
    else
    {
        throw toValue("Expected list, string or symbol as argument 1 to 'size', got '" + value.toString() + "'.");
    }
}

static Value builtin_join(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    size_t listCount = 0;
    size_t stringCount = 0;
    size_t symbolCount = 0;

    for (const Value& item : args)
    {
        if (item.type == Value_List)
        {
            listCount++;
        }
        else if (item.type == Value_String)
        {
            stringCount++;
        }
        else if (item.type == Value_Symbol)
        {
            symbolCount++;
        }
        else
        {
            throw toValue("Expected only lists, only strings or only symbols as variadic arguments to 'join'.");
        }
    }

    if ((listCount && stringCount) || (listCount && symbolCount) || (stringCount && symbolCount))
    {
        throw toValue("Expected only lists, only strings or only symbols as variadic arguments to 'join'.");
    }

    Value result;

    if (listCount)
    {
        result.type = Value_List;

        for (const Value& item : args)
        {
            result.l.insert(
                result.l.end(),
                item.l.begin(),
                item.l.end()
            );
        }
    }
    else if (stringCount)
    {
        result.type = Value_String;

        for (const Value& item : args)
        {
            result.s.insert(
                result.s.end(),
                item.s.begin(),
                item.s.end()
            );
        }
    }
    else
    {
        result.type = Value_Symbol;

        for (const Value& item : args)
        {
            result.s.insert(
                result.s.end(),
                item.s.begin(),
                item.s.end()
            );
        }
    }

    return result;
}

static Value builtin_set_define(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2);
    CHECK_ARG_SYMBOL(0);

    string name = args[0].s;
    Value value = context->run(args[1]);

    if (!context->hasAnywhere(name))
    {
        throw toValue("Definition '" + name + "' passed to 'set!' not found.");
    }

    if (!context->canWrite(context->currentUser(), name, true))
    {
        throw toValue("Permission to 'set!' global variable '" + name + "' denied.");
    }

    context->set(name, value);

    return Value();
}

static Value builtin_quote(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1);

    return context->bind(args[0]);
}

static Value builtin_list(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(args);
}

static Value builtin_eval(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return context->run(args[0]);
}

static Value builtin_lambda(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value result;
    result.type = Value_Lambda;
    result.lambda.type = Lambda_Normal;

    if (!isListOf(args[0], Value_Symbol))
    {
        throw toValue("Expected list of symbols as argument 1 to 'lambda', got '" + args[0].toString() + "'.");
    }

    for (const Value& child : args[0].l)
    {
        if (find(result.lambda.argNames.begin(), result.lambda.argNames.end(), child.s) != result.lambda.argNames.end())
        {
            throw toValue("Encountered argument name '" + child.s + "' multiple times in argument 1 to 'lambda'.");
        }

        result.lambda.argNames.push_back(child.s);
    }

    ScopeClosure closure(context);

    for (const string& argName : result.lambda.argNames)
    {
        context->define(argName, symbol(argName));
    }

    for (const Value& item : vector<Value>(args.begin() + 1, args.end()))
    {
        result.lambda.body.push_back(context->bind(item));
    }

    return result;
}

static Value builtin_syntax(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2);
    CHECK_ARG_LIST(0);
    CHECK_ARG_LIST(1);

    Value result;
    result.type = Value_Lambda;
    result.lambda.type = Lambda_Syntax;

    checkArgumentLayout(functionName, args[0].l);

    result.lambda.argLayout = args[0].l;
    result.lambda.body = { args[1] };

    return result;
}

static Value builtin_throw(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    throw args[0];
}

static Value builtin_catch(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(2, false);

    Value onError = context->run(args[0]);

    if (onError.type != Value_Lambda)
    {
        throw toValue("Expected lambda as argument 1 to 'catch', got '" + onError.toString() + "'.");
    }

    try
    {
        Value result;

        for (const Value& arg : vector<Value>(args.begin() + 1, args.end()))
        {
            result = context->run(arg);
        }

        return result;
    }
    catch (const Value& e)
    {
        return context->run(composeList(onError, e));
    }
}

static Value builtin_print(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    CHECK_ARGUMENT_COUNT(1, false);

    for (const Value& item : args)
    {
#if defined(LMOD_SERVER)
        context->game->scriptPrint(item.toString());
#else
        log(item.toString());
#endif
    }

    return Value();
}

static Value builtin_type(const string& functionName, ScriptContext* context, const vector<Value>& args)
{
    return toValue(static_cast<i64>(args[0].type));
}

const map<string, ScriptBuiltin> keywords = {
    makeSyntaxBuiltin("define", builtin_define),
    makeSyntaxBuiltin("undefine", builtin_undefine),
    makeSyntaxBuiltin("define-restrict", builtin_define_restrict, Script_Builtin_Auth_Admin),
    makeSyntaxBuiltin("define-control", builtin_define_control, Script_Builtin_Auth_User),
    makeSyntaxBuiltin("define-find", builtin_define_find),
    makeBuiltin("define-list", {}, builtin_define_list),
    makeSyntaxBuiltin("define-exists?", builtin_define_exists),
    makeSyntaxBuiltin("define-syntax", builtin_define_syntax),
    makeBuiltin("get", { "object", "index" }, builtin_get),
    makeBuiltin("set", { "object", "index", "value" }, builtin_set),
    makeBuiltin("size", { "object" }, builtin_size),
    makeVariadicBuiltin("join", builtin_join),
    makeSyntaxBuiltin("set!", builtin_set_define),
    makeSyntaxBuiltin("quote", builtin_quote),
    makeVariadicBuiltin("list", builtin_list),
    makeBuiltin("eval", { "value" }, builtin_eval),
    makeSyntaxBuiltin("lambda", builtin_lambda),
    makeSyntaxBuiltin("syntax", builtin_syntax),
    makeBuiltin("throw", { "value" }, builtin_throw),
    makeSyntaxBuiltin("catch", builtin_catch),
    makeVariadicBuiltin("print", builtin_print),
    makeBuiltin("type", { "value" }, builtin_type),
    makeSyntaxBuiltin("->", builtin_lambda)
};
