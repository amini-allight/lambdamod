/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "emissive_color.hpp"
#include "tools.hpp"
#include "yaml_tools.hpp"

EmissiveColor::EmissiveColor()
    : color(1)
    , intensity(1)
    , emissive(false)
{

}

EmissiveColor::EmissiveColor(const vec3& color, f64 intensity, bool emissive)
    : color(color)
    , intensity(intensity)
    , emissive(emissive)
{

}

void EmissiveColor::verify()
{
    color = verifyColor(color);
    intensity = max(intensity, 0.0);
}

bool EmissiveColor::operator==(const EmissiveColor& rhs) const
{
    return color == rhs.color && intensity == rhs.intensity && emissive == rhs.emissive;
}

bool EmissiveColor::operator!=(const EmissiveColor& rhs) const
{
    return !(*this == rhs);
}

vec3 EmissiveColor::toVec3() const
{
    return emissive ? color * intensity : color;
}

vec4 EmissiveColor::toVec4() const
{
    return vec4(toVec3(), emissive ? 1 : 0);
}

template<>
EmissiveColor YAMLNode::convert() const
{
    EmissiveColor color;

    color.color = at("color").as<vec3>();
    color.intensity = at("intensity").as<f64>();
    color.emissive = at("emissive").as<bool>();

    return color;
}

template<>
void YAMLSerializer::emit(EmissiveColor v)
{
    startMapping();

    emitPair("color", v.color);
    emitPair("intensity", v.intensity);
    emitPair("emissive", v.emissive);

    endMapping();
}

ostream& operator<<(ostream& out, const EmissiveColor& color)
{
    out << "(" << color.color << " @ " << color.intensity << ", " << color.emissive << ")";

    return out;
}
