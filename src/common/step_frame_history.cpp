/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "step_frame_history.hpp"
#include "tools.hpp"

#if defined(LMOD_SERVER)
#include "server/global.hpp"
#elif defined(LMOD_CLIENT)
#include "client/global.hpp"
#endif

StepFrameHistoryEntry::StepFrameHistoryEntry(const StepFrame& frame, const optional<chrono::milliseconds>& creationTime)
    : frame(frame)
    , creationTime(creationTime ? *creationTime : currentTime())
{

}

bool StepFrameHistoryEntry::expired() const
{
    return creationTime + chrono::milliseconds(static_cast<u64>(g_config.historyWindow * 1000)) < currentTime();
}

template<>
StepFrameHistoryEntry YAMLNode::convert() const
{
    return StepFrameHistoryEntry(
        at("frame").as<StepFrame>(),
        currentTime() - chrono::milliseconds(at("age").as<u64>())
    );
}

template<>
void YAMLSerializer::emit(StepFrameHistoryEntry v)
{
    startMapping();

    emitPair("frame", v.frame);
    emitPair("age", static_cast<i64>((currentTime() - v.creationTime).count()));

    endMapping();
}

StepFrameHistory::StepFrameHistory()
{

}

StepFrameHistory::StepFrameHistory(const map<u64, StepFrameHistoryEntry>& entries)
    : _entries(entries)
{

}

bool StepFrameHistory::has(u64 index) const
{
    return _entries.contains(index);
}

void StepFrameHistory::set(u64 index, const StepFrame& frame)
{
    _entries.insert_or_assign(index, StepFrameHistoryEntry(frame));

    clean();
}

const StepFrame& StepFrameHistory::get(u64 index) const
{
    return _entries.at(index).frame;
}

void StepFrameHistory::clear()
{
    _entries.clear();
}

void StepFrameHistory::remove(u64 index)
{
    _entries.erase(index);
}

bool StepFrameHistory::empty() const
{
    return _entries.empty();
}

const map<u64, StepFrameHistoryEntry>& StepFrameHistory::entries() const
{
    return _entries;
}

void StepFrameHistory::clean()
{
    for (auto it = _entries.begin(); it != _entries.end();)
    {
        if (it->second.expired())
        {
            _entries.erase(it++);
        }
        else
        {
            it++;
        }
    }
}

template<>
StepFrameHistory YAMLNode::convert() const
{
    return StepFrameHistory(at("entries").as<map<u64, StepFrameHistoryEntry>>());
}

template<>
void YAMLSerializer::emit(StepFrameHistory v)
{
    startMapping();

    emitPair("entries", v.entries());

    endMapping();
}
