/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "url_tools.hpp"
#include "tools.hpp"

bool isURL(const string& s)
{
    regex r("^lambdamod://[a-z0-9-]+:.+@.+/.+$");
    smatch m;

    return regex_match(s, m, r);
}

bool isDirectURL(const string& s)
{
    regex r("^lambdamod://[a-z0-9-]+:.+@.+:[0-9]+/\\?.*server-password=.+.*$");
    smatch m;

    return regex_match(s, m, r);
}

static string getURLComponent(const string& s, const string& prefix, const string& pattern, const string& suffix)
{
    smatch m;
    regex r(prefix + pattern + suffix);

    regex_search(s, m, r);

    if (m.empty())
    {
        throw runtime_error("Required URL component not found.");
    }

    return s.substr(
        m.position(0) + prefix.size(),
        m.length(0) - (prefix.size() + suffix.size())
    );
}

tuple<string, string, string, u16, string> parseDirectURL(const string& s)
{
    string username = getURLComponent(s, "lambdamod://", "[a-z0-9-]+", ":");
    string password = urlDecode(getURLComponent(s, ":", "[^(@|:)]+", "@"));
    string host = getURLComponent(s, "@", "(\\[[a-fA-F0-9:]+\\]|[^:]+)", ":");
    u16 port = stoi(getURLComponent(s, ":", "[0-9]+", "/"));
    string serverPassword = urlDecode(getURLComponent(s, "server-password=", "[^&]+", ""));

    if (host.front() == '[')
    {
        host = host.substr(1, host.size() - 2);
    }

    return { username, password, host, port, serverPassword };
}

tuple<string, string, string, string> parseSessionURL(const string& s)
{
    string username = getURLComponent(s, "lambdamod://", "[a-z0-9-]+", ":");
    string password = urlDecode(getURLComponent(s, ":", "[^(@|:)]+", "@"));
    string sessionID = getURLComponent(s, "session-id=", "[0-9a-f]+", "");
    string serverPassword = urlDecode(getURLComponent(s, "server-password=", "[^&]+", ""));

    return { username, password, sessionID, serverPassword };
}
