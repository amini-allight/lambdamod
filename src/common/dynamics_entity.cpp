/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_entity.hpp"
#include "dynamics.hpp"
#include "world.hpp"
#include "entity.hpp"

DynamicsEntity::DynamicsEntity(Dynamics* dynamics, Entity* entity)
    : dynamics(dynamics)
    , world(entity->world())
    , id(entity->id())
{
    add();
}

DynamicsEntity::~DynamicsEntity()
{
    remove();
}

Entity* DynamicsEntity::entity() const
{
    return world->get(id);
}

tuple<vector<DynamicsForceApplication>, vector<DynamicsArea>> DynamicsEntity::getEnvironmentalEffects()
{
    return component->getEnvironmentalEffects();
}

void DynamicsEntity::applyEnvironmentalEffects(vector<DynamicsForceApplication>* forces, const vector<DynamicsArea>& areas)
{
    component->applyEnvironmentalEffects(forces, areas);
}

void DynamicsEntity::applyActions(f64 stepInterval)
{
    Entity* entity = this->entity();

    map<BodyPartID, DynamicsAnimationTarget> pose;

    for (const auto& [ id, playback ] : entity->actionPlaybacks())
    {
        auto it = entity->actions().find(playback.name);

        if (it == entity->actions().end())
        {
            continue;
        }

        const Action& action = it->second;

        for (const auto& [ partID, part ] : action.parts())
        {
            if (part.end() < playback.elapsed)
            {
                continue;
            }

            DynamicsAnimationTarget target;
            target.delay = part.end() - playback.elapsed;

            switch (part.type())
            {
            case Action_Part_IK :
                target.transform = part.get<ActionPartIK>().transform;
                break;
            }

            auto it = pose.find(part.targetID());

            if (it != pose.end() && it->second.delay > target.delay)
            {
                it->second = target;
            }
            else
            {
                pose.insert({ part.targetID(), target });
            }
        }
    }

    applyIKEffects(component->generateIKEffects(stepInterval, pose));
}

void DynamicsEntity::update()
{
    Entity* entity = this->entity();

    mat4 entityTransform = entity->globalTransform();
    mat4 dynamicsTransform = component->globalTransform();

    if (entity->mass() != component->creationMass() ||
        entity->body() != component->creationBody()
    )
    {
        remove();
        add();
        return;
    }

    if (entityTransform.scale() != component->scale())
    {
        component->setScale(entityTransform.scale());
    }

    if (entity->friction() != component->friction())
    {
        component->setFriction(entity->friction());
    }

    if (entity->restitution() != component->restitution())
    {
        component->setRestitution(entity->restitution());
    }

    if (entityTransform.position() != dynamicsTransform.position() ||
        entityTransform.rotation() != dynamicsTransform.rotation() ||
        entity->linearVelocity() != component->linearVelocity() ||
        entity->angularVelocity() != component->angularVelocity()
    )
    {
        component->setGlobalTransform(entityTransform);
        component->setLinearVelocity(entity->linearVelocity());
        component->setAngularVelocity(entity->angularVelocity());
    }
}

void DynamicsEntity::updatePose(const map<VRDevice, mat4>& pose)
{
    Entity* entity = this->entity();

    map<BodyPartID, DynamicsAnimationTarget> actualPose;

    for (const auto& [ device, transform ] : pose)
    {
        BodyAnchorType anchorType;

        switch (device)
        {
        default :
            fatal("Encountered unknown VR device type '" + to_string(device) + "'.");
        case VR_Device_Head :
            anchorType = Body_Anchor_Head;
            break;
        case VR_Device_Left_Hand :
            anchorType = Body_Anchor_Left_Hand;
            break;
        case VR_Device_Right_Hand :
            anchorType = Body_Anchor_Right_Hand;
            break;
        case VR_Device_Hips :
            anchorType = Body_Anchor_Hips;
            break;
        case VR_Device_Left_Foot :
            anchorType = Body_Anchor_Left_Foot;
            break;
        case VR_Device_Right_Foot :
            anchorType = Body_Anchor_Right_Foot;
            break;
        case VR_Device_Chest :
            anchorType = Body_Anchor_Chest;
            break;
        case VR_Device_Left_Shoulder :
            anchorType = Body_Anchor_Left_Shoulder;
            break;
        case VR_Device_Right_Shoulder :
            anchorType = Body_Anchor_Right_Shoulder;
            break;
        case VR_Device_Left_Elbow :
            anchorType = Body_Anchor_Left_Elbow;
            break;
        case VR_Device_Right_Elbow :
            anchorType = Body_Anchor_Right_Elbow;
            break;
        case VR_Device_Left_Wrist :
            anchorType = Body_Anchor_Left_Wrist;
            break;
        case VR_Device_Right_Wrist :
            anchorType = Body_Anchor_Right_Wrist;
            break;
        case VR_Device_Left_Knee :
            anchorType = Body_Anchor_Left_Knee;
            break;
        case VR_Device_Right_Knee :
            anchorType = Body_Anchor_Right_Knee;
            break;
        case VR_Device_Left_Ankle :
            anchorType = Body_Anchor_Left_Ankle;
            break;
        case VR_Device_Right_Ankle :
            anchorType = Body_Anchor_Right_Ankle;
            break;
        }

        BodyPart* anchor = entity->body().get(anchorType);

        if (!anchor)
        {
            continue;
        }

        actualPose.insert({ anchor->id(), { transform, 0 } });
    }

    applyIKEffects(component->generateIKEffects(0, actualPose));
}

void DynamicsEntity::writeback()
{
    component->writeback();
}

void DynamicsEntity::applyLinearForce(const vec3& force)
{
    component->applyLinearForce(force);
}

void DynamicsEntity::applyAngularForce(const vec3& force)
{
    component->applyAngularForce(force);
}

void DynamicsEntity::add()
{
    component = new DynamicsComponentEntity(
        dynamics,
        nullptr,
        entity()
    );
}

void DynamicsEntity::remove()
{
    delete component;
}

void DynamicsEntity::applyIKEffects(const vector<DynamicsIKEffect>& effects)
{
    for (const DynamicsIKEffect& effect : effects)
    {
        effect.apply();
    }
}
