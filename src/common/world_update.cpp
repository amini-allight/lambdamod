/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "world_update.hpp"
#include "world.hpp"
#include "entity.hpp"

WorldUpdate::WorldUpdate(const World* world)
    : worldName(world->name())
{

}

const vector<NetworkEvent>& WorldUpdate::events() const
{
    return _events;
}

const vector<EntityUpdateSideEffect>& WorldUpdate::sideEffects() const
{
    return _sideEffects;
}

void WorldUpdate::addChild(Entity* child)
{
    auto [ childEvents, childSideEffects ] = child->initial();

    push(childEvents);
    push(childSideEffects);
}

void WorldUpdate::removeChild(Entity* child)
{
    auto [ childEvents, childSideEffects ] = child->terminal();

    push(childEvents);
    push(childSideEffects);
}

void WorldUpdate::updateChild(const Entity* previousChild, Entity* child, const EntityUpdate& update)
{
    if (child->attachedUserID() && update.attachment())
    {
        attachmentUpdate(child->id());
    }

    push(update.events());
    push(update.sideEffects());
}

void WorldUpdate::push(const NetworkEvent& event)
{
    _events.push_back(event);
}

void WorldUpdate::push(const vector<NetworkEvent>& events)
{
    _events.insert(
        _events.end(),
        events.begin(),
        events.end()
    );
}

void WorldUpdate::push(const EntityUpdateSideEffect& sideEffect)
{
    _sideEffects.push_back(sideEffect);
}

void WorldUpdate::push(const vector<EntityUpdateSideEffect>& sideEffects)
{
    _sideEffects.insert(
        _sideEffects.end(),
        sideEffects.begin(),
        sideEffects.end()
    );
}

void WorldUpdate::attachmentUpdate(EntityID id)
{
    _sideEffects.push_back({ EntityID(), id, Entity_Update_Attachment_Update });
}
