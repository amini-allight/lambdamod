/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "tether.hpp"
#include "yaml_tools.hpp"

Tether::Tether()
{
    active = false;
    distance = 0;
}

Tether::Tether(bool active, f64 distance, const set<UserID>& userIDs)
    : active(active)
    , distance(distance)
    , userIDs(userIDs)
{

}

bool Tether::operator==(const Tether& rhs) const
{
    return active == rhs.active &&
        distance == rhs.distance &&
        userIDs == rhs.userIDs;
}

bool Tether::operator!=(const Tether& rhs) const
{
    return !(*this == rhs);
}

template<>
Tether YAMLNode::convert() const
{
    Tether tether;

    tether.active = at("active").as<bool>();
    tether.distance = at("distance").as<f64>();
    tether.userIDs = at("userIDs").as<set<UserID>>();

    return tether;
}

template<>
void YAMLSerializer::emit(Tether v)
{
    startMapping();

    emitPair("active", v.active);
    emitPair("distance", v.distance);
    emitPair("userIDs", v.userIDs);

    endMapping();
}
