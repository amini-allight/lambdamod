/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "ai.hpp"
#include "tools.hpp"

static vector<vec3> reconstructPath(const map<vec3, vec3>& cameFrom, const vec3& point)
{
    auto it = cameFrom.find(point);

    if (it == cameFrom.end())
    {
        return { point };
    }
    else
    {
        return join(reconstructPath(cameFrom, it->second), { point });
    }
}

vector<vec3> pathfind(
    const vec3& start,
    const vec3& end,
    const function<vector<vec3>(const vec3&)>& generator,
    const function<f64(const vec3&, const vec3&)>& heuristic
)
{
    set<vec3> open = { start };

    map<vec3, vec3> cameFrom;

    map<vec3, f64> gScores;
    auto gScore = [&](const vec3& point) -> f64
    {
        auto it = gScores.find(point);

        if (it == gScores.end())
        {
            return numeric_limits<f64>::max();
        }

        return it->second;
    };
    gScores.insert({ start, 0 });

    map<vec3, f64> fScores;
    auto fScore = [&](const vec3& point) -> f64
    {
        auto it = fScores.find(point);

        if (it == fScores.end())
        {
            return numeric_limits<f64>::max();
        }

        return it->second;
    };
    fScores.insert({ start, heuristic(start, end) });

    auto lowestFScore = [&]() -> vec3
    {
        vec3 lowestPoint;
        f64 lowestScore = numeric_limits<f64>::max();

        for (const vec3& point : open)
        {
            f64 score = fScore(point);

            if (score < lowestScore)
            {
                lowestPoint = point;
                lowestScore = score;
            }
        }

        return lowestPoint;
    };

    while (!open.empty())
    {
        vec3 current = lowestFScore();

        if (current == end)
        {
            return reconstructPath(cameFrom, current);
        }

        open.erase(current);

        for (const vec3& point : generator(current))
        {
            f64 tg = gScore(current) + heuristic(current, point);

            if (tg < gScore(point))
            {
                cameFrom.insert({ point, current });
                gScores.insert({ point, tg });
                fScores.insert({ point, tg + heuristic(point, end) });

                open.insert(point);
            }
        }
    }

    return {};
}
