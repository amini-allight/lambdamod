/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "dynamics_force_application.hpp"
#include "dynamics_component.hpp"
#include "tools.hpp"
#include "world.hpp"

DynamicsForceApplication::DynamicsForceApplication(DynamicsComponent* self, const DynamicsForce& force)
    : self(self)
    , force(force)
{

}

void DynamicsForceApplication::applyForce(DynamicsComponent* other, f64 mediumDensity)
{
    if (self == other)
    {
        return;
    }

    vec3 forceVector = force.forceOnComponent(self, other, mediumDensity);

    other->applyLinearForce(forceVector);

    reaction -= forceVector;
}

void DynamicsForceApplication::applyReaction(const World* world, const vector<DynamicsArea>& areas) const
{
    if (optional<vec3> position = force.reactionPosition(); position)
    {
        switch (force.force().type)
        {
        case Body_Force_Linear :
        case Body_Force_Omnidirectional :
        case Body_Force_Gravity :
        case Body_Force_Magnetism :
            self->applyLinearForce(*position, reaction);
            break;
        case Body_Force_Vortex :
            self->applyAngularForce(force.regionalTransform().back() * force.force().intensity);
            break;
        case Body_Force_Fan :
        {
            f64 density = get<2>(getEnvironment(
                world,
                areas,
                self->globalTransform().applyToPosition(force.regionalTransform().position())
            ));

            f64 massFlowFactor = pi * (sq(force.force().radius * 2) / 2) * density;

            self->applyLinearForce(*position, force.regionalTransform().back() * cbrt(force.force().intensity * massFlowFactor));
            break;
        }
        case Body_Force_Rocket :
            self->applyLinearForce(*position, force.regionalTransform().back() * force.force().intensity);
            break;
        }
    }
}
