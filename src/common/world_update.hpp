/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#include "types.hpp"
#include "network_event.hpp"
#include "entity_update.hpp"

class WorldUpdate
{
public:
    WorldUpdate(const World* world);

    template<typename T, typename EventT>
    void notify(const T& current, const T& previous, const function<void(EventT&)>& behavior, bool transformed = false)
    {
        if (current == previous)
        {
            return;
        }

        EventT event;
        event.name = worldName;

        behavior(event);

        _events.push_back(NetworkEvent(event));
    }

    const vector<NetworkEvent>& events() const;
    const vector<EntityUpdateSideEffect>& sideEffects() const;

    void addChild(Entity* child);
    void removeChild(Entity* child);
    void updateChild(const Entity* previousChild, Entity* child, const EntityUpdate& update);

private:
    string worldName;

    vector<NetworkEvent> _events;
    vector<EntityUpdateSideEffect> _sideEffects;

    void push(const NetworkEvent& event);
    void push(const vector<NetworkEvent>& events);

    void push(const EntityUpdateSideEffect& sideEffect);
    void push(const vector<EntityUpdateSideEffect>& sideEffects);

    void attachmentUpdate(EntityID id);
};
