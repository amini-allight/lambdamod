/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#include "hud_element.hpp"
#include "tools.hpp"
#include "constants.hpp"

HUDElementType hudElementTypeFromString(const string& s)
{
    if (s == "hud-line")
    {
        return HUD_Element_Line;
    }
    else if (s == "hud-text")
    {
        return HUD_Element_Text;
    }
    else if (s == "hud-bar")
    {
        return HUD_Element_Bar;
    }
    else if (s == "hud-symbol")
    {
        return HUD_Element_Symbol;
    }
    else
    {
        throw runtime_error("Encountered unknown HUD element type '" + s + "'.");
    }
}

string hudElementTypeToString(HUDElementType type)
{
    switch (type)
    {
    case HUD_Element_Line :
        return "hud-line";
    case HUD_Element_Text :
        return "hud-text";
    case HUD_Element_Bar :
        return "hud-bar";
    case HUD_Element_Symbol :
        return "hud-symbol";
    default :
        fatal("Encountered unknown HUD element type '" + to_string(type) + "'.");
    }
}

template<>
HUDElementType YAMLNode::convert() const
{
    return hudElementTypeFromString(_scalar);
}

template<>
void YAMLSerializer::emit(HUDElementType v)
{
    emit(hudElementTypeToString(v));
}

HUDElementAnchor hudElementAnchorFromString(const string& s)
{
    if (s == "hud-anchor-center")
    {
        return HUD_Element_Anchor_Center;
    }
    else if (s == "hud-anchor-left")
    {
        return HUD_Element_Anchor_Left;
    }
    else if (s == "hud-anchor-right")
    {
        return HUD_Element_Anchor_Right;
    }
    else if (s == "hud-anchor-up")
    {
        return HUD_Element_Anchor_Up;
    }
    else if (s == "hud-anchor-down")
    {
        return HUD_Element_Anchor_Down;
    }
    else if (s == "hud-anchor-up-left")
    {
        return HUD_Element_Anchor_Up_Left;
    }
    else if (s == "hud-anchor-down-left")
    {
        return HUD_Element_Anchor_Down_Left;
    }
    else if (s == "hud-anchor-up-right")
    {
        return HUD_Element_Anchor_Up_Right;
    }
    else if (s == "hud-anchor-down-right")
    {
        return HUD_Element_Anchor_Down_Right;
    }
    else
    {
        throw runtime_error("Encountered unknown HUD element anchor '" + s + "'.");
    }
}

string hudElementAnchorToString(HUDElementAnchor anchor)
{
    switch (anchor)
    {
    case HUD_Element_Anchor_Center :
        return "hud-anchor-center";
    case HUD_Element_Anchor_Left :
        return "hud-anchor-left";
    case HUD_Element_Anchor_Right :
        return "hud-anchor-right";
    case HUD_Element_Anchor_Up :
        return "hud-anchor-up";
    case HUD_Element_Anchor_Down :
        return "hud-anchor-down";
    case HUD_Element_Anchor_Up_Left :
        return "hud-anchor-up-left";
    case HUD_Element_Anchor_Down_Left :
        return "hud-anchor-down-left";
    case HUD_Element_Anchor_Up_Right :
        return "hud-anchor-up-right";
    case HUD_Element_Anchor_Down_Right :
        return "hud-anchor-down-right";
    default :
        fatal("Encountered unknown HUD element anchor '" + to_string(anchor) + "'.");
    }
}

template<>
HUDElementAnchor YAMLNode::convert() const
{
    return hudElementAnchorFromString(_scalar);
}

template<>
void YAMLSerializer::emit(HUDElementAnchor v)
{
    emit(hudElementAnchorToString(v));
}

vec2 anchorToTopLeft(HUDElementAnchor anchor, const vec2& viewportSize, const vec2& position, const vec2& size)
{
    switch (anchor)
    {
    default :
        fatal("Encountered unknown HUD element anchor '" + to_string(anchor) + "'.");
    case HUD_Element_Anchor_Center :
        return (position * viewportSize) - (size * vec2(0.5));
    case HUD_Element_Anchor_Left :
        return (position * viewportSize) - (size * vec2(0, 0.5));
    case HUD_Element_Anchor_Right :
        return (position * viewportSize) - (size * vec2(1, 0.5));
    case HUD_Element_Anchor_Up :
        return (position * viewportSize) - (size * vec2(0.5, 0));
    case HUD_Element_Anchor_Down :
        return (position * viewportSize) - (size * vec2(0.5, 1));
    case HUD_Element_Anchor_Up_Left :
        return (position * viewportSize) - (size * vec2(0));
    case HUD_Element_Anchor_Down_Left :
        return (position * viewportSize) - (size * vec2(0, 1));
    case HUD_Element_Anchor_Up_Right :
        return (position * viewportSize) - (size * vec2(1, 0));
    case HUD_Element_Anchor_Down_Right :
        return (position * viewportSize) - (size * vec2(1));
    }
}

HUDElement::HUDElement()
{
    _anchor = HUD_Element_Anchor_Center;
    _position = { 0.5, 0.5 };
    _color = { 1, 1, 1 };
    _type = HUD_Element_Line;
    data = HUDElementLine();
}

void HUDElement::verify()
{
    _anchor = min(_anchor, HUD_Element_Anchor_Down_Right);
    _color = verifyColor(_color);
    _type = min(_type, HUD_Element_Symbol); // NOTE: Update this when adding new HUD element types

    if (auto line = get_if<HUDElementLine>(&data); line)
    {
        line->verify();
    }
    else if (auto text = get_if<HUDElementText>(&data); text)
    {
        text->verify();
    }
    else if (auto bar = get_if<HUDElementBar>(&data); bar)
    {
        bar->verify();
    }
    else if (auto symbol = get_if<HUDElementSymbol>(&data); symbol)
    {
        symbol->verify();
    }
}

bool HUDElement::operator==(const HUDElement& rhs) const
{
    if (_anchor != rhs._anchor)
    {
        return false;
    }

    if (_position != rhs._position)
    {
        return false;
    }

    if (_color != rhs._color)
    {
        return false;
    }

    if (_type != rhs._type)
    {
        return false;
    }

    switch (_type)
    {
    case HUD_Element_Line :
        if (get<HUDElementLine>() != rhs.get<HUDElementLine>())
        {
            return false;
        }
        break;
    case HUD_Element_Text :
        if (get<HUDElementText>() != rhs.get<HUDElementText>())
        {
            return false;
        }
        break;
    case HUD_Element_Bar :
        if (get<HUDElementBar>() != rhs.get<HUDElementBar>())
        {
            return false;
        }
        break;
    case HUD_Element_Symbol :
        if (get<HUDElementSymbol>() != rhs.get<HUDElementSymbol>())
        {
            return false;
        }
        break;
    }

    return true;
}

bool HUDElement::operator!=(const HUDElement& rhs) const
{
    return !(*this == rhs);
}

HUDElementAnchor HUDElement::anchor() const
{
    return _anchor;
}

const vec2& HUDElement::position() const
{
    return _position;
}

const vec3& HUDElement::color() const
{
    return _color;
}

HUDElementType HUDElement::type() const
{
    return _type;
}

void HUDElement::setAnchor(HUDElementAnchor anchor)
{
    _anchor = anchor;
}

void HUDElement::setPosition(const vec2& position)
{
    _position = position;
}

void HUDElement::setColor(const vec3& color)
{
    _color = color;
}

void HUDElement::setType(HUDElementType type)
{
    _type = type;

    switch (_type)
    {
    case HUD_Element_Line :
        data = HUDElementLine();
        break;
    case HUD_Element_Text :
        data = HUDElementText();
        break;
    case HUD_Element_Bar :
        data = HUDElementBar();
        break;
    case HUD_Element_Symbol :
        data = HUDElementSymbol();
        break;
    }
}

string HUDElement::dataToString() const
{
    stringstream ss;

    switch (_type)
    {
    case HUD_Element_Line :
        msgpack::pack(ss, get<HUDElementLine>());
        break;
    case HUD_Element_Text :
        msgpack::pack(ss, get<HUDElementText>());
        break;
    case HUD_Element_Bar :
        msgpack::pack(ss, get<HUDElementBar>());
        break;
    case HUD_Element_Symbol :
        msgpack::pack(ss, get<HUDElementSymbol>());
        break;
    }

    return ss.str();
}

void HUDElement::dataFromString(const string& s)
{
    msgpack::object_handle oh = msgpack::unpack(s.data(), s.size());

    switch (_type)
    {
    case HUD_Element_Line :
    {
        HUDElementLine element;
        oh.get().convert(element);

        data = element;
        break;
    }
    case HUD_Element_Text :
    {
        HUDElementText element;
        oh.get().convert(element);

        data = element;
        break;
    }
    case HUD_Element_Bar :
    {
        HUDElementBar element;
        oh.get().convert(element);

        data = element;
        break;
    }
    case HUD_Element_Symbol :
    {
        HUDElementSymbol element;
        oh.get().convert(element);

        data = element;
        break;
    }
    }
}

template<>
HUDElement YAMLNode::convert() const
{
    HUDElement element;

    element._anchor = at("anchor").as<HUDElementAnchor>();
    element._position = at("position").as<vec2>();
    element._color = at("color").as<vec3>();
    element._type = hudElementTypeFromString(at("type").as<string>());

    switch (element._type)
    {
    case HUD_Element_Line :
        element.data = at("data").as<HUDElementLine>();
        break;
    case HUD_Element_Text :
        element.data = at("data").as<HUDElementText>();
        break;
    case HUD_Element_Bar :
        element.data = at("data").as<HUDElementBar>();
        break;
    case HUD_Element_Symbol :
        element.data = at("data").as<HUDElementSymbol>();
        break;
    }

    return element;
}

template<>
void YAMLSerializer::emit(HUDElement v)
{
    startMapping();

    emitPair("anchor", v._anchor);
    emitPair("position", v._position);
    emitPair("color", v._color);
    emitPair("type", hudElementTypeToString(v._type));

    switch (v._type)
    {
    case HUD_Element_Line :
        emitPair("data", v.get<HUDElementLine>());
        break;
    case HUD_Element_Text :
        emitPair("data", v.get<HUDElementText>());
        break;
    case HUD_Element_Bar :
        emitPair("data", v.get<HUDElementBar>());
        break;
    case HUD_Element_Symbol :
        emitPair("data", v.get<HUDElementSymbol>());
        break;
    }

    endMapping();
}
