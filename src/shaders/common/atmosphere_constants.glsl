/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef ATMOSPHERE_CONSTANTS_GLSL
#define ATMOSPHERE_CONSTANTS_GLSL

const vec2 atmosphereEffectLineStretchFactor = vec2(1, 16);
const int atmosphereEffectRotationCount = 16;

const int atmosphereEffectCloudVarietyCount = 8;
const int atmosphereEffectCloudSubParticleCount = 32;

const int atmosphereEffectStreamerCapCount = 2;
const int atmosphereEffectStreamerSegmentCount = 16;

const float atmosphereEffectStreamerSpeedMultiplier = 2;
const float atmosphereEffectStreamerHeight = 0.1;
const float atmosphereEffectStreamerHalfAmplitude = 10 * atmosphereEffectStreamerHeight;
const float atmosphereEffectStreamerWidth = atmosphereEffectStreamerHeight * 8;

const int atmosphereEffectWarpCapCount = 2;
const int atmosphereEffectWarpSegmentCount = 1;
const float atmosphereEffectWarpWidthMultiplier = 8;
const float atmosphereEffectWarpSegmentMultiplier = 16;

#endif
