/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef LIGHTING_GLSL
#define LIGHTING_GLSL

struct FragTransform
{
    vec3 position;
    vec3 normal;
};

const float partiallyLitThreshold = 0;
const float fullyLitTheta = 1;
const float fullyLitThreshold = 0.3;
const float partiallyLitTheta = 0.5;
const float unlitTheta = 0;
const float minRayTracingDistance = 0.001;
const int ambientOcclusionSegments = 8;
const float ambientOcclusionDistance = 1000000;
const int ambientOcclusionSampleCount = ambientOcclusionSegments + 1;

const int Occlusion_Primitive_Ellipse = 0;
const int Occlusion_Primitive_Equilateral_Triangle = 1;
const int Occlusion_Primitive_Isosceles_Triangle = 2;
const int Occlusion_Primitive_Right_Angle_Triangle = 3;
const int Occlusion_Primitive_Rectangle = 4;
const int Occlusion_Primitive_Pentagon = 5;
const int Occlusion_Primitive_Hexagon = 6;
const int Occlusion_Primitive_Heptagon = 7;
const int Occlusion_Primitive_Octagon = 8;
const int Occlusion_Primitive_Cuboid = 9;
const int Occlusion_Primitive_Sphere = 10;
const int Occlusion_Primitive_Cylinder = 11;
const int Occlusion_Primitive_Cone = 12;
const int Occlusion_Primitive_Capsule = 13;
const int Occlusion_Primitive_Pyramid = 14;
const int Occlusion_Primitive_Tetrahedron = 15;
const int Occlusion_Primitive_Octahedron  = 16;
const int Occlusion_Primitive_Dodecahedron = 17;
const int Occlusion_Primitive_Icosahedron = 18;

#ifndef LMOD_RAY_TRACING
float triangleSign(vec2 a, vec2 b, vec2 c)
{
    return (a.x - c.x) * (b.y - c.y) - (b.x - c.x) * (a.y - c.y);
}

bool occludeTriangle(vec3 a, vec3 b, vec3 c, vec3 pos, vec3 dir, float dist)
{
    vec3 normal = normalize(cross(b - a, c - a));

    float top = dot(a - pos, normal);
    float bottom = dot(dir, normal);

    if (roughly(bottom, 0))
    {
        return false;
    }

    float d = top / bottom;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec4 toXYPlane = quaternionInverse(quaternionFromDirectionRoll(normalize(b - a), normal));

    vec3 point = quaternionRotate(toXYPlane, pos + dir * d);

    a = quaternionRotate(toXYPlane, a);
    b = quaternionRotate(toXYPlane, b);
    c = quaternionRotate(toXYPlane, c);

    float ad = triangleSign(point.xy, a.xy, b.xy);
    float bd = triangleSign(point.xy, b.xy, c.xy);
    float cd = triangleSign(point.xy, c.xy, a.xy);

    bool negative = (!roughly(ad, 0) && ad < 0) || (!roughly(bd, 0) && bd < 0) || (!roughly(cd, 0) && cd < 0);
    bool positive = (!roughly(ad, 0) && ad > 0) || (!roughly(bd, 0) && bd > 0) || (!roughly(cd, 0) && cd > 0);

    return !(negative && positive);
}

bool occludeOriginTriangle(vec2 a, vec2 b, vec2 c, vec3 pos, vec3 dir, float dist)
{
    float top = dot(pos, upDir);
    float bottom = dot(dir, upDir);

    if (roughly(bottom, 0))
    {
        return false;
    }

    float d = top / bottom;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec2 point = (pos + dir * d).xy;

    float ad = triangleSign(point, a, b);
    float bd = triangleSign(point, b, c);
    float cd = triangleSign(point, c, a);

    bool negative = (!roughly(ad, 0) && ad < 0) || (!roughly(bd, 0) && bd < 0) || (!roughly(cd, 0) && cd < 0);
    bool positive = (!roughly(ad, 0) && ad > 0) || (!roughly(bd, 0) && bd > 0) || (!roughly(cd, 0) && cd > 0);

    return !(negative && positive);
}

bool occludeCircle(
    float radius,
    vec3 pos,
    vec3 dir,
    float dist
)
{
    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    return length(point) <= radius;
}

bool occludeLine(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;
    float l = dimensions.y;

    vec3 n = cross(dir, forwardDir);

    float parallelism = abs(dot(forwardDir, dir));

    if (roughly(parallelism, 1))
    {
        return false;
    }

    float gap = abs(dot(pos, n) / length(n));

    if (gap > radius)
    {
        return false;
    }

    vec3 n1 = cross(dir, n);

    vec3 hit = forwardDir * (dot(pos, n1) / dot(forwardDir, n1));

    if (length(hit) > l / 2)
    {
        return false;
    }

    float directionality = dot(normalize(hit - pos), dir);

    if (!roughly(directionality, 1))
    {
        return false;
    }

    return distance(pos, hit) <= dist;
}

bool occludeEllipse(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float xRadius = dimensions.x / 2;
    float yRadius = dimensions.y / 2;

    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    return sq(point.x) / sq(xRadius / 2) + sq(point.y) / sq(yRadius / 2) <= 1;
}

bool occludeEquilateralTriangle(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    vec2 points[3] = vec2[](
        vec2(0, 1) * radius,
        vec2(0.86603, -0.5) * radius,
        vec2(-0.86603, -0.5) * radius
    );

    return occludeOriginTriangle(points[0], points[1], points[2], pos, dir, dist);
}

bool occludeIsoscelesTriangle(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float w = dimensions.x;
    float h = dimensions.y;

    vec2 points[3] = vec2[](
        vec2(-w / 2, 0),
        vec2(+w / 2, 0),
        vec2(0, h)
    );

    return occludeOriginTriangle(points[0], points[1], points[2], pos, dir, dist);
}

bool occludeRightAngleTriangle(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float w = dimensions.x;
    float h = dimensions.y;

    vec2 points[3] = vec2[](
        vec2(0, 0),
        vec2(0, h),
        vec2(w, 0)
    );

    return occludeOriginTriangle(points[0], points[1], points[2], pos, dir, dist);
}

bool occludeRectangle(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float w = dimensions.x;
    float h = dimensions.y;

    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    return abs(point.x) <= w / 2 && abs(point.y) <= h / 2;
}

bool occludePentagon(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    const int pointCount = 5;
    vec2 points[pointCount] = vec2[](
        vec2(0, 1),
        vec2(0.95106, 0.30902),
        vec2(0.58779, -0.80902),
        vec2(-0.58779, -0.80902),
        vec2(-0.95106, 0.30902)
    );

    for (int i = 0; i < pointCount; i++)
    {
        points[i] = points[i] * radius;
    }

    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    bool hit = false;

    for (int i = 0, j = pointCount - 1; i < pointCount; j = i++)
    {
        vec2 p0 = points[i];
        vec2 p1 = points[j];

        if (((p0.y > point.y) != (p1.y > point.y)) && (point.x < ((p1.x - p0.x) * (point.y - p0.y) / (p1.y - p0.y) + p0.x)))
        {
            hit = !hit;
        }
    }

    return hit;
}

bool occludeHexagon(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    const int pointCount = 6;
    vec2 points[pointCount] = vec2[](
        vec2(0.5, 0.86603),
        vec2(1, 0),
        vec2(0.5, -0.86603),
        vec2(-0.5, -0.86603),
        vec2(-1, 0),
        vec2(-0.5, 0.86603)
    );

    for (int i = 0; i < pointCount; i++)
    {
        points[i] = points[i] * radius;
    }

    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    bool hit = false;

    for (int i = 0, j = pointCount - 1; i < pointCount; j = i++)
    {
        vec2 p0 = points[i];
        vec2 p1 = points[j];

        if (((p0.y > point.y) != (p1.y > point.y)) && (point.x < ((p1.x - p0.x) * (point.y - p0.y) / (p1.y - p0.y) + p0.x)))
        {
            hit = !hit;
        }
    }

    return hit;
}

bool occludeHeptagon(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    const int pointCount = 7;
    vec2 points[pointCount] = vec2[](
        vec2(0, 1),
        vec2(0.78183, 0.62349),
        vec2(0.97492, -0.22252),
        vec2(0.43388, -0.90096),
        vec2(-0.43388, -0.90096),
        vec2(-0.97492, -0.22252),
        vec2(-0.78183, 0.62349)
    );

    for (int i = 0; i < pointCount; i++)
    {
        points[i] = points[i] * radius;
    }

    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    bool hit = false;

    for (int i = 0, j = pointCount - 1; i < pointCount; j = i++)
    {
        vec2 p0 = points[i];
        vec2 p1 = points[j];

        if (((p0.y > point.y) != (p1.y > point.y)) && (point.x < ((p1.x - p0.x) * (point.y - p0.y) / (p1.y - p0.y) + p0.x)))
        {
            hit = !hit;
        }
    }

    return hit;
}

bool occludeOctagon(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    const int pointCount = 8;
    vec2 points[pointCount] = vec2[](
        vec2(0.38268, 0.92388),
        vec2(0.92388, 0.38268),
        vec2(0.92388, -0.38268),
        vec2(0.38268, -0.92388),
        vec2(-0.38268, -0.92388),
        vec2(-0.92388, -0.38268),
        vec2(-0.92388, 0.38268),
        vec2(-0.38268, 0.92388)
    );

    for (int i = 0; i < pointCount; i++)
    {
        points[i] = points[i] * radius;
    }

    float t = dot(-pos, upDir);
    float b = dot(dir, upDir);

    if (roughly(b, 0))
    {
        return false;
    }

    float d = t / b;

    if (d < 0 || d > dist)
    {
        return false;
    }

    vec3 point = pos + dir * d;

    bool hit = false;

    for (int i = 0, j = pointCount - 1; i < pointCount; j = i++)
    {
        vec2 p0 = points[i];
        vec2 p1 = points[j];

        if (((p0.y > point.y) != (p1.y > point.y)) && (point.x < ((p1.x - p0.x) * (point.y - p0.y) / (p1.y - p0.y) + p0.x)))
        {
            hit = !hit;
        }
    }

    return hit;
}

bool occludeCuboid(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float w = dimensions.x;
    float l = dimensions.y;
    float h = dimensions.z;

    if (occludeRectangle(dimensions.xyz, (pos - vec3(0, 0, h / 2)).xyz, dir.xyz, dist))
    {
        return true;
    }

    if (occludeRectangle(dimensions.xyz, (pos + vec3(0, 0, h / 2)).xyz, dir.xyz, dist))
    {
        return true;
    }

    if (occludeRectangle(dimensions.xzy, (pos - vec3(0, l / 2, 0)).xzy, dir.xzy, dist))
    {
        return true;
    }

    if (occludeRectangle(dimensions.xzy, (pos + vec3(0, l / 2, 0)).xzy, dir.xzy, dist))
    {
        return true;
    }

    if (occludeRectangle(dimensions.yzx, (pos - vec3(w / 2, 0, 0)).yzx, dir.yzx, dist))
    {
        return true;
    }

    if (occludeRectangle(dimensions.yzx, (pos + vec3(w / 2, 0, 0)).yzx, dir.yzx, dist))
    {
        return true;
    }

    return false;
}

bool occludeSphere(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    float a = dot(dir, dir);
    float b = 2 * dot(pos, dir);
    float c = dot(pos, pos) - sq(radius);

    vec2 r = quadratic(a, b, c);

    if (isnan(r.x) && !isnan(r.y) && r.y >= 0)
    {
        return r.y <= dist;
    }
    else if (!isnan(r.x) && isnan(r.y) && r.x >= 0)
    {
        return r.x <= dist;
    }
    else if (!isnan(r.x) && !isnan(r.y))
    {
        return min(r.x, r.y) >= 0 && min(r.x, r.y) <= dist;
    }
    else
    {
        return false;
    }
}

bool occludeCylinder(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;
    float h = dimensions.z;

    vec2 dir2D = normalize(dir.xy);

    float a = sq(dir2D.x) + sq(dir2D.y);
    float b = 2 * dot(pos.xy, dir2D);
    float c = sq(pos.x) + sq(pos.y) - sq(radius);

    vec2 r = quadratic(a, b, c);

    if (!isnan(r.x) && r.x >= 0)
    {
        vec3 v = vec3(0);
        v.xy = pos.xy + dir2D * r.x;
        v.z = (((v.x - pos.x) / dir.x) * dir.z) + pos.z;

        if (v.z >= -h / 2 && v.z <= h / 2)
        {
            return true;
        }
    }

    if (!isnan(r.y) && r.y >= 0)
    {
        vec3 v = vec3(0);
        v.xy = pos.xy + dir2D * r.y;
        v.z = (((v.x - pos.x) / dir.x) * dir.z) + pos.z;

        if (v.z >= -h / 2 && v.z <= h / 2)
        {
            return true;
        }
    }

    if (occludeCircle(radius, pos - vec3(0, 0, h / 2), dir, dist))
    {
        return true;
    }

    if (occludeCircle(radius, pos + vec3(0, 0, h / 2), dir, dist))
    {
        return true;
    }

    return false;
}

bool occludeCone(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;
    float h = dimensions.z;

    float a = sq(dir.x) + sq(dir.y) - sq(dir.z)*(sq(radius)/sq(h));
    float b = 2*pos.x*dir.x + 2*pos.y*dir.y + 2*dir.z*(sq(radius)/h) - 2*pos.z*dir.z*(sq(radius)/sq(h));
    float c = sq(pos.x) + sq(pos.y) - sq(radius) + 2*pos.z*(sq(radius)/h) - sq(pos.z)*(sq(radius)/sq(h));

    vec2 r = quadratic(a, b, c);

    if (!isnan(r.x) && r.x >= 0)
    {
        vec3 v = pos + dir * r.x;

        if (v.z >= 0 && v.z <= h)
        {
            return true;
        }
    }

    if (!isnan(r.y) && r.y >= 0)
    {
        vec3 v = pos + dir * r.y;

        if (v.z >= 0 && v.z <= h)
        {
            return true;
        }
    }

    if (occludeCircle(radius, pos, dir, dist))
    {
        return true;
    }

    return false;
}

bool occludeCapsule(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;
    float h = dimensions.z;

    vec2 dir2D = normalize(dir.xy);

    float a = sq(dir2D.x) + sq(dir2D.y);
    float b = 2 * dot(pos.xy, dir2D);
    float c = sq(pos.x) + sq(pos.y) - sq(radius);

    vec2 r = quadratic(a, b, c);

    if (!isnan(r.x) && r.x >= 0)
    {
        vec3 v = vec3(0);
        v.xy = pos.xy + dir2D * r.x;
        v.z = (((v.x - pos.x) / dir.x) * dir.z) + pos.z;

        if (v.z >= -h / 2 && v.z <= h / 2)
        {
            return true;
        }
    }

    if (!isnan(r.y) && r.y >= 0)
    {
        vec3 v = vec3(0);
        v.xy = pos.xy + dir2D * r.y;
        v.z = (((v.x - pos.x) / dir.x) * dir.z) + pos.z;

        if (v.z >= -h / 2 && v.z <= h / 2)
        {
            return true;
        }
    }

    if (occludeSphere(dimensions, pos - vec3(0, 0, h / 2), dir, dist))
    {
        return true;
    }

    if (occludeSphere(dimensions, pos + vec3(0, 0, h / 2), dir, dist))
    {
        return true;
    }

    return false;
}

bool occludePyramid(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;
    float height = dimensions.z;

    vec3 vertices[5] = vec3[](
        vec3(0, 0, height),
        vec3(+radius, +radius, 0),
        vec3(+radius, -radius, 0),
        vec3(-radius, -radius, 0),
        vec3(-radius, +radius, 0)
    );

    if (occludeTriangle(vertices[0], vertices[1], vertices[2], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[2], vertices[3], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[3], vertices[4], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[4], vertices[1], pos, dir, dist))
    {
        return true;
    }

    if (occludeRectangle(vec3(radius * 2, radius * 2, 0), pos, dir, dist))
    {
        return true;
    }

    return false;
}

bool occludeTetrahedron(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    vec3 ringStart = quaternionRotate(quaternionFromAxisAngle(leftDir, radians(120)), vec3(0, 0, radius));

    vec3 vertices[4] = vec3[](
        vec3(0, 0, radius),
        ringStart,
        quaternionRotate(quaternionFromAxisAngle(upDir, radians(+120)), ringStart),
        quaternionRotate(quaternionFromAxisAngle(upDir, radians(-120)), ringStart)
    );

    if (occludeTriangle(vertices[0], vertices[1], vertices[2], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[2], vertices[3], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[3], vertices[1], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[1], vertices[2], vertices[3], pos, dir, dist))
    {
        return true;
    }

    return false;
}

bool occludeOctahedron(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    float radius = dimensions.x / 2;

    vec3 vertices[6] = vec3[](
        vec3(0, 0, +radius),
        vec3(0, 0, -radius),
        vec3(-radius, 0, 0),
        vec3(0, +radius, 0),
        vec3(+radius, 0, 0),
        vec3(0, -radius, 0)
    );

    if (occludeTriangle(vertices[0], vertices[2], vertices[3], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[3], vertices[4], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[4], vertices[5], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[0], vertices[5], vertices[2], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[1], vertices[2], vertices[3], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[1], vertices[3], vertices[4], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[1], vertices[4], vertices[5], pos, dir, dist))
    {
        return true;
    }

    if (occludeTriangle(vertices[1], vertices[5], vertices[2], pos, dir, dist))
    {
        return true;
    }

    return false;
}

bool occludeDodecahedron(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    // Approximated as sphere
    return occludeSphere(dimensions, pos, dir, dist);
}

bool occludeIcosahedron(vec3 dimensions, vec3 pos, vec3 dir, float dist)
{
    // Approximated as sphere
    return occludeSphere(dimensions, pos, dir, dist);
}

bool occludes(OcclusionPrimitive primitive, vec3 start, vec3 end)
{
    vec3 dimensions = vec3(primitive.w, primitive.l, primitive.h);

    start = (inverse(primitive.transform[gl_ViewIndex]) * vec4(start, 1)).xyz;
    end = (inverse(primitive.transform[gl_ViewIndex]) * vec4(end, 1)).xyz;

    vec3 pos = start;
    vec3 dir = normalize(end - start);
    float dist = distance(start, end);

    switch (primitive.type)
    {
    default :
        return false;
    case Occlusion_Primitive_Ellipse :
        return occludeEllipse(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Equilateral_Triangle :
        return occludeEquilateralTriangle(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Isosceles_Triangle :
        return occludeIsoscelesTriangle(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Right_Angle_Triangle :
        return occludeRightAngleTriangle(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Rectangle :
        return occludeRectangle(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Pentagon :
        return occludePentagon(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Hexagon :
        return occludeHexagon(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Heptagon :
        return occludeHeptagon(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Octagon :
        return occludeOctagon(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Cuboid :
        return occludeCuboid(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Sphere :
        return occludeSphere(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Cylinder :
        return occludeCylinder(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Cone :
        return occludeCone(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Capsule :
        return occludeCapsule(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Pyramid :
        return occludePyramid(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Tetrahedron :
        return occludeTetrahedron(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Octahedron :
        return occludeOctahedron(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Dodecahedron :
        return occludeDodecahedron(dimensions, pos, dir, dist);
    case Occlusion_Primitive_Icosahedron :
        return occludeIcosahedron(dimensions, pos, dir, dist);
    }
}
#endif

bool lightOccluded(vec3 pos, vec3 lightPos)
{
    bool occluded = false;

    vec3 dir = normalize(lightPos - pos);
    float dist = distance(pos, lightPos);

#ifdef LMOD_RAY_TRACING
    rayQueryEXT rayQuery;
    rayQueryInitializeEXT(
        rayQuery,
        occlusionScene[gl_ViewIndex],
        gl_RayFlagsTerminateOnFirstHitEXT,
        0xff,
        pos,
        minRayTracingDistance,
        dir,
        dist + minRayTracingDistance
    );
    rayQueryProceedEXT(rayQuery);
    occluded = rayQueryGetIntersectionTypeEXT(rayQuery, true) != gl_RayQueryCommittedIntersectionNoneEXT;
#else
    for (int i = 0; i < occlusionScene.primitiveCount && i < maxPrimitiveCount; i++)
    {
        if (occludes(occlusionScene.primitives[i], pos, lightPos))
        {
            occluded = true;
            break;
        }
    }
#endif

    return occluded;
}

bool ambientOccluded(vec3 pos, vec3 dir)
{
#ifdef LMOD_RAY_TRACING
    rayQueryEXT rayQuery;
    rayQueryInitializeEXT(
        rayQuery,
        occlusionScene[gl_ViewIndex],
        gl_RayFlagsTerminateOnFirstHitEXT,
        0xff,
        pos,
        minRayTracingDistance,
        dir,
        ambientOcclusionDistance + minRayTracingDistance
    );
    rayQueryProceedEXT(rayQuery);

    return rayQueryGetIntersectionTypeEXT(rayQuery, true) != gl_RayQueryCommittedIntersectionNoneEXT;
#else
    for (int i = 0; i < occlusionScene.primitiveCount && i < maxPrimitiveCount; i++)
    {
        if (occludes(occlusionScene.primitives[i], pos, pos + dir * ambientOcclusionDistance))
        {
            return true;
        }
    }

    return false;
#endif
}

float ambientVisibility(vec3 position, vec3 normal)
{
    int occlusionCount = 0;

    if (ambientOccluded(position, normal))
    {
        occlusionCount += 1;
    }

    vec3 side = pickSideDirection(normal);

    for (int i = 0; i < ambientOcclusionSegments; i++)
    {
        vec3 dir = quaternionRotate(
            quaternionFromAxisAngle(normal, tau * (i / float(ambientOcclusionSegments))),
            quaternionRotate(
                quaternionFromAxisAngle(side, pi / 4),
                normal
            )
        );

        if (ambientOccluded(position, dir))
        {
            occlusionCount += 1;
        }
    }

    return 1 - (occlusionCount / float(ambientOcclusionSampleCount));
}

float celShadeTheta(vec3 lightDir, vec3 normal)
{
    float theta = max(dot(lightDir, normal) * -1, 0);

    const float blurSize = 0.01;

    if (theta > fullyLitThreshold)
    {
        if (theta < fullyLitThreshold + blurSize)
        {
            theta = mix(partiallyLitTheta, fullyLitTheta, (theta - fullyLitThreshold) / blurSize);
        }
        else
        {
            theta = fullyLitTheta;
        }
    }
    else if (theta > partiallyLitThreshold)
    {
        if (theta < partiallyLitThreshold + blurSize)
        {
            theta = mix(unlitTheta, partiallyLitTheta, (theta - partiallyLitThreshold) / blurSize);
        }
        else
        {
            theta = partiallyLitTheta;
        }
    }
    else
    {
        theta = unlitTheta;
    }

    return theta;
}

float celShadeFalloff(float dist)
{
    const float bandSize = 0.1;
    const float iBandSize = 1 / bandSize;
    const float blurSize = 0.01 / iBandSize;

    float progress = dist * iBandSize;
    float step = floor(progress);
    float blurStart = step - blurSize / 2;
    float blurEnd = step + blurSize / 2;

    if (step != 0 && progress > blurStart && progress < blurEnd)
    {
        progress = mix(step - 1, step, (progress - blurStart) / blurSize);
    }
    else
    {
        progress = step;
    }

    dist = floor(progress) / iBandSize;

    return 1 / sq(dist + 1);
}

vec3 lightEffect(Light light, vec3 position, vec3 normal, vec3 color)
{
    vec3 lightPos = positionFromTransform(light.transform[gl_ViewIndex]);

    if (lightOccluded(position, lightPos))
    {
        return vec3(0);
    }

    vec3 lightDir = forwardFromTransform(light.transform[gl_ViewIndex]);

    vec3 dir = normalize(position - lightPos);
    float dist = distance(position, lightPos);

    if (angleBetween(lightDir, dir) > light.angle / 2)
    {
        return vec3(0);
    }

    return color * light.color.rgb * celShadeTheta(dir, normal) * celShadeFalloff(dist);
}

vec3 ambientEffect(vec3 position, vec3 normal, vec3 color)
{
    vec3 ambient = texture(environmentTexture, normal).rgb;

    return color * ambient * ambientVisibility(position, normal);
}

vec4 fogMix(vec4 value, vec3 position)
{
    if (object.fogDistance == 0)
    {
        return value;
    }

    float progress = sq(min(length(position), object.fogDistance) / object.fogDistance);

    return mix(value, bgStencil, progress);
}

FragTransform correctNormals(vec3 position, vec3 normal)
{
    mat4 model = object.model[gl_ViewIndex];

    FragTransform result;

    if (!roughly(dot(normalize(normal), normalize(position)), 0))
    {
        vec3 origin = (model * vec4(vec3(0), 1)).xyz;

        vec4 fromLocalSpace = quaternionFromDirectionRoll(
            normalize(position),
            pickSideDirection(normalize(position))
        );
        vec4 toLocalSpace = quaternionInverse(fromLocalSpace);

        vec3 localOrigin = quaternionRotate(toLocalSpace, origin);
        vec3 localPosition = quaternionRotate(toLocalSpace, position);
        vec3 localNormal = quaternionRotate(toLocalSpace, normal);

        localPosition.y = localOrigin.y - (localPosition.y - localOrigin.y);
        localNormal.y *= -1;

        result.position = quaternionRotate(fromLocalSpace, localPosition);
        result.normal = quaternionRotate(fromLocalSpace, localNormal);
    }
    else
    {
        result.position = position;
        result.normal = normal;
    }

    return result;
}
#endif
