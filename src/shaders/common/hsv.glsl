/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
vec4 rgbaToHSVA(vec4 rgba)
{
    float r = rgba.r;
    float g = rgba.g;
    float b = rgba.b;

    float h = 0;
    float s = 0;
    float v = 0;

    float max = max(max(r, g), b);
    float min = min(min(r, g), b);
    float delta = max - min;

    if (delta > 0)
    {
        if (max == r)
        {
            h = 60 * mod((g - b) / delta, 6);
        }
        else if (max == g)
        {
            h = 60 * (((b - r) / delta) + 2);
        }
        else if (max == b)
        {
            h = 60 * (((r - g) / delta) + 4);
        }

        if (max > 0)
        {
            s = delta / max;
        }
        else
        {
            s = 0;
        }

        v = max;
    }
    else
    {
        h = 0;
        s = 0;
        v = max;
    }

    if (h < 0)
    {
        h = 360 + h;
    }

    return vec4(h / 360, s, v, rgba.a);
}

vec4 hsvaToRGBA(vec4 hsva)
{
    float h = hsva.r * 360;
    float s = hsva.g;
    float v = hsva.b;

    float r = 0;
    float g = 0;
    float b = 0;

    float c = v * s;
    float hPrime = mod(h / 60, 6);
    float x = c * (1 - abs(mod(hPrime, 2) - 1));
    float m = v - c;

    if (hPrime >= 0 && hPrime < 1)
    {
        r = c;
        g = x;
        b = 0;
    }
    else if (hPrime >= 1 && hPrime < 2)
    {
        r = x;
        g = c;
        b = 0;
    }
    else if (hPrime >= 2 && hPrime < 3)
    {
        r = 0;
        g = c;
        b = x;
    }
    else if (hPrime >= 3 && hPrime < 4)
    {
        r = 0;
        g = x;
        b = c;
    }
    else if (hPrime >= 4 && hPrime < 5)
    {
        r = x;
        g = 0;
        b = c;
    }
    else if (hPrime >= 5 && hPrime < 6)
    {
        r = c;
        g = 0;
        b = x;
    }
    else
    {
        r = 0;
        g = 0;
        b = 0;
    }

    r += m;
    g += m;
    b += m;

    return vec4(r, g, b, hsva.a);
}

vec4 mixAsHSVA(vec4 a, vec4 b, float t)
{
    vec4 aHSV = rgbaToHSVA(a);
    vec4 bHSV = rgbaToHSVA(b);

    // Adjusts values to prevent red <-> violet transitions from crossing the entire hue range
    if (abs(aHSV.r - bHSV.r) > 0.5)
    {
        if (aHSV.r > 0.5)
        {
            aHSV.r -= 1;
        }
        else
        {
            bHSV.r -= 1;
        }
    }

    return hsvaToRGBA(mix(aHSV, bHSV, t));
}
