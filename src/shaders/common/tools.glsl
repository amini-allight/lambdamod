/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef TOOLS_GLSL
#define TOOLS_GLSL

bool roughly(float x, float target)
{
    return x > target - epsilon && x < target + epsilon;
}

bool roughly(vec2 v, vec2 target)
{
    return roughly(v.x, target.x) ||
        roughly(v.y, target.y);
}

bool roughly(vec3 v, vec3 target)
{
    return roughly(v.x, target.x) ||
        roughly(v.y, target.y) ||
        roughly(v.z, target.z);
}

bool roughly(vec4 v, vec4 target)
{
    return roughly(v.x, target.x) ||
        roughly(v.y, target.y) ||
        roughly(v.z, target.z) ||
        roughly(v.w, target.w);
}

float sq(float x)
{
    return pow(x, 2);
}

vec2 quadratic(float a, float b, float c)
{
    return vec2(
        (-b + sqrt(sq(b) - 4*a*c)) / (2*a),
        (-b - sqrt(sq(b) - 4*a*c)) / (2*a)
    );
}

float angleBetween(vec3 p, vec3 q)
{
    return acos(dot(p, q));
}

vec3 pickForwardDirection(vec3 dir)
{
    if (roughly(dir, forwardDir) || roughly(dir, backDir))
    {
        return downDir;
    }
    else
    {
        return normalize(cross(normalize(cross(dir, forwardDir)), dir));
    }
}

vec3 pickSideDirection(vec3 dir)
{
    if (roughly(dir, upDir) || roughly(dir, downDir))
    {
        return normalize(cross(dir, forwardDir));
    }
    else
    {
        return normalize(cross(dir, upDir));
    }
}

vec3 pickUpDirection(vec3 dir)
{
    if (roughly(dir, upDir) || roughly(dir, downDir))
    {
        return backDir;
    }
    else
    {
        return normalize(cross(normalize(cross(dir, upDir)), dir));
    }
}

vec4 quaternionFromAxisAngle(vec3 axis, float angle)
{
    float c = sin(float(angle / 2));

    return vec4(
        axis.x * c,
        axis.y * c,
        axis.z * c,
        cos(float(angle / 2))
    );
}

vec4 quaternionFromDirectionRoll(vec3 d, vec3 r)
{
    mat3 m = mat3(
        vec3((r.z*d.y - r.y*d.z), d.x, r.x),
        vec3((r.x*d.z - r.z*d.x), d.y, r.y),
        vec3((r.y*d.x - r.x*d.y), d.z, r.z)
    );
    float trace = m[0][0] + m[1][1] + m[2][2];

    vec4 q;

    if (trace > 0)
    {
        float s = 0.5 / sqrt(trace + 1.0);
        q.w = 0.25 / s;
        q.x = (m[2][1] - m[1][2]) * s;
        q.y = (m[0][2] - m[2][0]) * s;
        q.z = (m[1][0] - m[0][1]) * s;
    }
    else
    {
        if (m[0][0] > m[1][1] && m[0][0] > m[2][2])
        {
            float s = 2.0 * sqrt(1.0 + m[0][0] - m[1][1] - m[2][2]);
            q.w = (m[2][1] - m[1][2]) / s;
            q.x = 0.25 * s;
            q.y = (m[0][1] + m[1][0]) / s;
            q.z = (m[0][2] + m[2][0]) / s;
        }
        else if (m[1][1] > m[2][2])
        {
            float s = 2.0 * sqrt(1.0 + m[1][1] - m[0][0] - m[2][2]);
            q.w = (m[0][2] - m[2][0]) / s;
            q.x = (m[0][1] + m[1][0]) / s;
            q.y = 0.25 * s;
            q.z = (m[1][2] + m[2][1]) / s;
        }
        else
        {
            float s = 2.0 * sqrt(1.0 + m[2][2] - m[0][0] - m[1][1]);
            q.w = (m[1][0] - m[0][1]) / s;
            q.x = (m[0][2] + m[2][0]) / s;
            q.y = (m[1][2] + m[2][1]) / s;
            q.z = 0.25 * s;
        }
    }

    return q;
}

vec4 quaternionInverse(vec4 q)
{
    return vec4(
        q.x * -1,
        q.y * -1,
        q.z * -1,
        q.w
    );
}

vec4 hamiltonProduct(vec4 p, vec4 q)
{
    return vec4(
        p.w*q.x + p.x*q.w + p.y*q.z - p.z*q.y,
        p.w*q.y - p.x*q.z + p.y*q.w + p.z*q.x,
        p.w*q.z + p.x*q.y - p.y*q.x + p.z*q.w,
        p.w*q.w - p.x*q.x - p.y*q.y - p.z*q.z
    );
}

vec3 quaternionRotate(vec4 q, vec3 p)
{
    return hamiltonProduct(hamiltonProduct(q, vec4(p, 0)), quaternionInverse(q)).xyz;
}

vec3 positionFromTransform(mat4 transform)
{
    return (transform * vec4(vec3(0), 1)).xyz;
}

vec3 forwardFromTransform(mat4 transform)
{
    return (transform * vec4(forwardDir, 1)).xyz - positionFromTransform(transform);
}

vec2 rotate(vec2 p, float a)
{
    float s = sin(a);
    float c = cos(a);

    float x = p.x * c - p.y * s;
    float y = p.x * s + p.y * c;

    return vec2(x, y);
}

vec2 rotate(vec2 o, vec2 p, float a)
{
    p -= o;

    float s = sin(a);
    float c = cos(a);

    float x = p.x * c - p.y * s;
    float y = p.x * s + p.y * c;

    return o + vec2(x, y);
}

float handedAngleBetween(vec2 p, vec2 q)
{
    return atan((p.x * q.y - p.y * q.x) / dot(p, q));
}

float handedAngleBetween(vec3 p, vec3 q, vec3 axis)
{
    return handedAngleBetween(
        vec2(0, 1),
        quaternionRotate(quaternionInverse(quaternionFromDirectionRoll(p, axis)), q).xy
    );
}

#endif
