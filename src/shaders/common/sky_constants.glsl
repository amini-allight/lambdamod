/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef SKY_CONSTANTS_GLSL
#define SKY_CONSTANTS_GLSL

const int skySampleCount = 4;
const float skySampleAngularStep = 0.0001;
const float skySampleLinearStep = 0.01;

const int skyDirectionalLineCount = 8;
const float skyDirectionalLineWidth = radians(0.75);

const int skyOmnidirectionalCenterCount = 20;
const float skyOmnidirectionalAngle = radians(30);
const float skyOmnidirectionalLineWidth = radians(0.75);
const vec3 skyOmnidirectionalCenters[skyOmnidirectionalCenterCount] = vec3[](
    vec3(-0.6071, 0.0000, 0.7947),
    vec3(-0.1876, 0.5773, 0.7947),
    vec3(0.4911, 0.3568, 0.7947),
    vec3(0.4911, -0.3568, 0.7947),
    vec3(-0.1876, -0.5773, 0.7947),

    vec3(-0.3035, -0.9342, 0.1876),
    vec3(0.3035, -0.9342, -0.1876),
    vec3(0.7947, -0.5773, 0.1876),
    vec3(0.9822, 0.0000, -0.1876),
    vec3(0.7947, 0.5773, 0.1876),
    vec3(0.3035, 0.9342, -0.1876),
    vec3(-0.3035, 0.9342, 0.1876),
    vec3(-0.7947, 0.5773, -0.1876),
    vec3(-0.9822, 0.0000, 0.1876),
    vec3(-0.7947, -0.5773, -0.1876),

    vec3(0.6071, 0.0000, -0.7947),
    vec3(0.1876, 0.5773, -0.7947),
    vec3(-0.4911, 0.3568, -0.7947),
    vec3(-0.4911, -0.3568, -0.7947),
    vec3(0.1876, -0.5773, -0.7947)
);

const int Sky_Base_Blank = 0;
const int Sky_Base_Unidirectional = 1;
const int Sky_Base_Bidirectional = 2;
const int Sky_Base_Omnidirectional = 3;

const uint skyMaxAuroraPartCount = 1024;

const uint skyMaxBillboardPartCount = 512;

const uint skyMaxCometPartCount = 256;

const float skyCircleLineWidth = radians(0.75);

const float skyUnidirectionalLowerEnvMapStrength = 0.25;

#endif
