/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef CONSTANTS_GLSL
#define CONSTANTS_GLSL

const int eyeCount = 2;
const int envMapCameraCount = 6;

const vec4 fgStencil = vec4(+1, 0, 0, 1);
const vec4 fgEmissiveStencil = vec4(+1, +1, 0, 1);
const vec4 bgStencil = vec4(-1, 0, 0, 1);

const float pi = 3.14159;
const float tau = pi * 2;
const float epsilon = 1e-6;

const vec3 leftDir = vec3(-1, 0, 0);
const vec3 rightDir = vec3(+1, 0, 0);
const vec3 backDir = vec3(0, -1, 0);
const vec3 forwardDir = vec3(0, +1, 0);
const vec3 downDir = vec3(0, 0, -1);
const vec3 upDir = vec3(0, 0, +1);

// NOTE: If you change the near and far clipping planes change them here too
const float near = 0.01;
const float far = 1000000;

const int maxLightCount = 128;
const int maxPrimitiveCount = 16 * 1024;
const int maxTextureTileCount = 64;

const float maxChromaticAberrationStrength = 0.02;
const vec3 chromaticAberrationColorOffset = vec3(0.9, 0.6, -0.6);

const float motionBlurDecayFactor = 0.65;

const float depthOfFieldAdaptationDelay = 0.1;

const int blurSampleCount = 5;
const float blurWeights[blurSampleCount] = float[](
    0.261416,
    0.21081,
    0.111167,
    0.038013,
    0.008266
);

// Full bloom is full foreground
const float hdrBloomStencil = 1;

const float areaGridInterval = 0.5;
const float areaGridThickness = 0.02;
const float areaNoiseScale = 0.5;

const float terrainGridInterval = 1;
const float terrainGridThickness = 0.02;

const float particleDragCoefficient = 0.5;

const int lineSegments = 12;
const float lineWidth = 0.01;

#endif
