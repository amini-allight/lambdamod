/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform Gradient {
    vec2 position;
    vec2 size;
    vec4 startColor;
    vec4 endColor;
} gradient;

vec2 vertices[6] = vec2[](
    vec2(0, 0),
    vec2(1, 1),
    vec2(0, 1),
    vec2(0, 0),
    vec2(1, 0),
    vec2(1, 1)
);

void main()
{
    vec2 vertexPosition = vertices[gl_VertexIndex];

    gl_Position = vec4(((gradient.position + (vertexPosition * gradient.size)) * 2) - 1, 0, 1);

    vec4 startColor = gradient.startColor;
    vec4 endColor = gradient.endColor;

    fragColor = mix(startColor, endColor, vertexPosition.y);
}
