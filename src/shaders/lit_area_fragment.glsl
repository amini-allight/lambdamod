/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#ifdef LMOD_RAY_TRACING
#extension GL_EXT_ray_query : enable
#endif
#include "common/constants.glsl"
#include "common/tools.glsl"
#include "common/lighting_types.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec2 fragUV;
layout(location = 2) in vec3 fragNormal;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
} object;
layout(std140, binding = 2) uniform Material {
    vec4 color;
    float animationSpeed;
    int opaque;
} material;
layout(binding = 3) uniform sampler2D noiseTexture;
layout(binding = 4) uniform samplerCube environmentTexture;
#ifdef LMOD_RAY_TRACING
layout(binding = 5) uniform accelerationStructureEXT occlusionScene[eyeCount];
#else
layout(binding = 5) uniform OcclusionPrimitives {
    OcclusionPrimitive primitives[maxPrimitiveCount];
    int primitiveCount;
} occlusionScene;
#endif

#include "common/lighting.glsl"

void main()
{
    float noise = texture(noiseTexture, fragUV + vec2(object.timer * material.animationSpeed)).r * areaNoiseScale;

    vec2 uv = fragUV + vec2(noise);

    vec2 nearestLine = round(uv / areaGridInterval) * areaGridInterval;
    vec2 distanceToLine = abs(uv - nearestLine);

    vec4 color;

    if (distanceToLine.x < areaGridThickness / 2 || distanceToLine.y < areaGridThickness / 2)
    {
        color = material.color;
    }
    else
    {
        if (material.opaque == 1)
        {
            color = vec4(vec3(0), -1);
        }
        else
        {
            discard;
        }
    }

    FragTransform fragTransform = correctNormals(fragPosition, fragNormal);

    // Alpha channel is used as a flag where -1 denotes "depth" areas which shouldn't receive lighting, 0 denotes non-emissive areas and +1 indicates emissive areas
    if (color.a == 0)
    {
        outColor.rgb = vec3(0);

        for (int i = 0; i < object.lightCount && i < maxLightCount; i++)
        {
            outColor.rgb += lightEffect(object.lights[i], fragTransform.position, fragTransform.normal, color.rgb);
        }

        outColor.rgb += ambientEffect(fragTransform.position, fragTransform.normal, color.rgb);

        outColor.a = 1;
        outStencil = fogMix(fgStencil, fragTransform.position);
    }
    else if (color.a > 0)
    {
        outColor = vec4(color.rgb, 1);
        outStencil = fogMix(fgEmissiveStencil, fragTransform.position);
    }
    else
    {
        outColor = vec4(vec3(0), 1);
        outStencil = bgStencil;
    }
}
