/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"

layout(location = 0) out vec3 fragPosition;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;

vec3 vertices[8] = vec3[](
    vec3(+1, +1, +1),
    vec3(+1, -1, +1),
    vec3(-1, -1, +1),
    vec3(-1, +1, +1),
    vec3(+1, +1, -1),
    vec3(+1, -1, -1),
    vec3(-1, -1, -1),
    vec3(-1, +1, -1)
);

int indices[36] = int[](
    6, 2, 3,
    6, 7, 3,

    5, 1, 0,
    5, 4, 0,

    7, 3, 0,
    7, 4, 0,

    6, 2, 1,
    6, 5, 1,

    2, 1, 0,
    2, 3, 0,

    6, 5, 4,
    6, 7, 4
);

void main()
{
    vec3 vertex = vertices[indices[gl_VertexIndex]];

    gl_Position = (camera.projection[gl_ViewIndex] * camera.view[gl_ViewIndex] * vec4(vertex, 1));
    fragPosition = vertex;
}
