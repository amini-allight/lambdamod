/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/tools.glsl"
#include "common/hsv.glsl"
#include "common/sky_constants.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Parameters {
    mat4 worldTransform;
    vec4 backgroundColor;
    vec4 horizonColor;
    vec4 lowerZenithColor;
    vec4 upperZenithColor;
    int type;
} parameters;

void skyBaseBlankColor(vec3 direction)
{
    outColor = parameters.backgroundColor;
    outStencil = bgStencil;
}

void skyBaseUnidirectionalColor(vec3 direction)
{
    if (direction.z < 0)
    {
        outColor = parameters.backgroundColor;
        outStencil = bgStencil;
        return;
    }

    const float range = pi / 2;
    const vec3 up = upDir;

    float angle = angleBetween(direction, up);
    float progress = 1 - (angle / range);
    float bandProgress = round(progress * skyDirectionalLineCount) / skyDirectionalLineCount;
    float bandAngle = (1 - bandProgress) * range;

    vec4 mixedColor = mixAsHSVA(parameters.horizonColor, parameters.upperZenithColor, bandProgress);

    vec4 color = vec4(0);
    vec4 stencil = vec4(0);

    for (int i = 0; i < skySampleCount; i++)
    {
        float sampleAngle = (angle - skySampleAngularStep * ((skySampleCount / 2) - 0.5)) + i * skySampleAngularStep;

        if (abs(sampleAngle - bandAngle) < skyDirectionalLineWidth / 2)
        {
            color += mixedColor;
            stencil += fgEmissiveStencil;
        }
        else
        {
            color += parameters.backgroundColor;
            stencil += bgStencil;
        }
    }

    outColor = color / skySampleCount;
    outStencil = stencil / skySampleCount;
}

void skyBaseBidirectionalColor(vec3 direction)
{
    const float range = pi / 2;

    if (direction.z == 0)
    {
        outColor = parameters.horizonColor;
        outStencil = fgEmissiveStencil;
    }
    else if (direction.z < 0)
    {
        const vec3 up = downDir;

        float angle = angleBetween(direction, up);
        float progress = 1 - (angle / range);
        float bandProgress = round(progress * skyDirectionalLineCount) / skyDirectionalLineCount;
        float bandAngle = (1 - bandProgress) * range;

        vec4 mixedColor = mixAsHSVA(parameters.horizonColor, parameters.lowerZenithColor, bandProgress);

        vec4 color = vec4(0);
        vec4 stencil = vec4(0);

        for (int i = 0; i < skySampleCount; i++)
        {
            float sampleAngle = (angle - skySampleAngularStep * ((skySampleCount / 2) - 0.5)) + i * skySampleAngularStep;

            if (abs(sampleAngle - bandAngle) < skyDirectionalLineWidth / 2)
            {
                color += mixedColor;
                stencil += fgEmissiveStencil;
            }
            else
            {
                color += parameters.backgroundColor;
                stencil += bgStencil;
            }
        }

        outColor = color / skySampleCount;
        outStencil = stencil / skySampleCount;
    }
    else if (direction.z > 0)
    {
        const vec3 up = upDir;

        float angle = angleBetween(direction, up);
        float progress = 1 - (angle / range);
        float bandProgress = round(progress * skyDirectionalLineCount) / skyDirectionalLineCount;
        float bandAngle = (1 - bandProgress) * range;

        vec4 mixedColor = mixAsHSVA(parameters.horizonColor, parameters.upperZenithColor, bandProgress);

        vec4 color = vec4(0);
        vec4 stencil = vec4(0);

        for (int i = 0; i < skySampleCount; i++)
        {
            float sampleAngle = (angle - skySampleAngularStep * ((skySampleCount / 2) - 0.5)) + i * skySampleAngularStep;

            if (abs(sampleAngle - bandAngle) < skyDirectionalLineWidth / 2)
            {
                color += mixedColor;
                stencil += fgEmissiveStencil;
            }
            else
            {
                color += parameters.backgroundColor;
                stencil += bgStencil;
            }
        }

        outColor = color / skySampleCount;
        outStencil = stencil / skySampleCount;
    }
}

void skyBaseOmnidirectionalColor(vec3 direction)
{
    outColor = parameters.backgroundColor;
    outStencil = bgStencil;

    for (int i = 0; i < skyOmnidirectionalCenterCount; i++)
    {
        vec3 centerDirection = skyOmnidirectionalCenters[i];
        float angle = angleBetween(centerDirection, direction);

        vec4 color = vec4(0);
        vec4 stencil = vec4(0);

        for (int j = 0; j < skySampleCount; j++)
        {
            // This one requires a bigger offset for some reason
            float sampleAngle = (angle - skySampleAngularStep * ((skySampleCount / 2) - 0.5)) + j * skySampleAngularStep;

            if (abs(sampleAngle - skyOmnidirectionalAngle) < skyOmnidirectionalLineWidth / 2)
            {
                color += parameters.upperZenithColor;
                stencil += fgEmissiveStencil;
            }
            else if (abs(sampleAngle - (skyOmnidirectionalAngle / 2)) < skyOmnidirectionalLineWidth / 2)
            {
                color += parameters.upperZenithColor;
                stencil += fgEmissiveStencil;
            }
            else
            {
                color += parameters.backgroundColor;
                stencil += bgStencil;
            }
        }

        outColor = max(outColor, color / skySampleCount);
        outStencil = max(outStencil, stencil / skySampleCount);
    }
}

void skyBaseColor(vec3 direction)
{
    switch (parameters.type)
    {
    case Sky_Base_Blank :
        skyBaseBlankColor(direction);
        break;
    case Sky_Base_Unidirectional :
        skyBaseUnidirectionalColor(direction);
        break;
    case Sky_Base_Bidirectional :
        skyBaseBidirectionalColor(direction);
        break;
    case Sky_Base_Omnidirectional :
        skyBaseOmnidirectionalColor(direction);
        break;
    }
}

void main()
{
    vec3 direction = (parameters.worldTransform * vec4(normalize(fragPosition), 1)).xyz;

    skyBaseColor(direction);
}
