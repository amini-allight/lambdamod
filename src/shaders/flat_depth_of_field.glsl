/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"

const vec2 center = vec2(0.5, 0.5);

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform DepthOfField {
    float range;
    float timer;
} depthOfField;
layout(binding = 1) uniform sampler2D colorTexture;
layout(binding = 2) uniform sampler2D depthTexture;
layout(std140, binding = 3) coherent restrict buffer State {
    float previousFocalDistance;
    float previousTimer;
    float nextFocalDistance;
    float nextTimer;
} state;

vec3 blurVertical(vec2 p)
{
    vec2 pixelSize = 1.0 / textureSize(colorTexture, 0);
    vec3 result = texture(colorTexture, p).rgb * blurWeights[0];

    for (int i = 1; i < blurSampleCount; i++)
    {
        result += texture(colorTexture, p + vec2(0.0, pixelSize.y * i)).rgb * blurWeights[i];
        result += texture(colorTexture, p - vec2(0.0, pixelSize.y * i)).rgb * blurWeights[i];
    }

    return result;
}

vec3 blur(vec2 p)
{
    vec2 pixelSize = 1.0 / textureSize(colorTexture, 0);
    vec3 result = blurVertical(p) * blurWeights[0];

    for (int i = 1; i < blurSampleCount; i++)
    {
        result += blurVertical(p + vec2(pixelSize.x * i, 0.0)) * blurWeights[i];
        result += blurVertical(p - vec2(pixelSize.x * i, 0.0)) * blurWeights[i];
    }

    return result;
}

float depthToDistance(float depth)
{
    float a = -(far * near) / (far - near);
    float b = far / (near - far);

    float dist = abs(a / (b + depth));

    return isinf(dist) ? far : dist;
}

void main()
{
    float targetFocalDepth = texture(depthTexture, center).r;
    float localDepth = texture(depthTexture, fragUV).r;
    vec3 originalColor = texture(colorTexture, fragUV).rgb;
    vec3 blurredColor = blur(fragUV);

    float targetFocalDistance = depthToDistance(targetFocalDepth);
    float localDistance = depthToDistance(localDepth);

    float blurriness = clamp((localDistance - state.previousFocalDistance) / depthOfField.range, 0, 1);

    float elapsed = depthOfField.timer - state.previousTimer;
    state.nextFocalDistance = state.previousFocalDistance + (1 - exp(-(elapsed / depthOfFieldAdaptationDelay))) * (targetFocalDistance - state.previousFocalDistance);
    state.nextTimer = depthOfField.timer;

    outColor.rgb = mix(originalColor, blurredColor, blurriness);
    outColor.a = 1;
}
