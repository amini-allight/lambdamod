/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"
#include "common/atmosphere_constants.glsl"
#include "common/tools.glsl"
#include "common/lighting_types.glsl"

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = atmosphereEffectCloudSubParticleCount * 4) out;

layout(location = 0) in float rotation[];
layout(location = 1) in int instanceIndex[];

layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec3 fragNormal;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    vec4 worldGravity;
    vec4 worldFlowVelocity;
    vec4 color;
    float worldDensity;
    float size;
    float radius;
    float mass;
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
    float areaRadius;
    vec4 viewerPositionDelta;
} object;
layout(std430, binding = 4) buffer Clouds {
    vec4 data[];
} clouds;

void outputTransformed(vec3 position, float rotation, vec4 billboardRotation, vec2 sign)
{
    gl_Position = camera.projection[gl_ViewIndex]
        * camera.view[gl_ViewIndex]
        * vec4(position + quaternionRotate(billboardRotation, vec3(rotate(sign * (object.size / 2), rotation), 0)), 1);
    fragPosition = position;
    fragNormal = quaternionRotate(billboardRotation, vec3(rotate(forwardDir.xy, rotation), 0));
}

void makeParticle(vec3 position, float rotation)
{
    vec3 particleForward = -normalize(position);
    vec3 cameraUp = (camera.view[gl_ViewIndex] * vec4(backDir, 1)).xyz;
    vec3 particleUp = pickUpDirection(particleForward);

    // Particle faces the Z axis
    vec4 billboardRotation = quaternionFromDirectionRoll(particleUp, particleForward);

    outputTransformed(position, rotation, billboardRotation, vec2(-1, -1));
    EmitVertex();

    outputTransformed(position, rotation, billboardRotation, vec2(-1, +1));
    EmitVertex();

    outputTransformed(position, rotation, billboardRotation, vec2(+1, -1));
    EmitVertex();

    outputTransformed(position, rotation, billboardRotation, vec2(+1, +1));
    EmitVertex();

    EndPrimitive();
}

void main()
{
    for (int i = 0; i < atmosphereEffectCloudSubParticleCount; i++)
    {
        int subParticleIndex = ((instanceIndex[0] % atmosphereEffectCloudVarietyCount) * atmosphereEffectCloudSubParticleCount) + i;

        vec3 subParticlePosition = gl_in[0].gl_Position.xyz + clouds.data[subParticleIndex].xyz;
        float subParticleRotation = rotation[0] + clouds.data[subParticleIndex].w;

        makeParticle(subParticlePosition, subParticleRotation);
    }
}
