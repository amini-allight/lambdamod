/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"

layout(location = 0) in vec3 position;
layout(location = 0) out vec3 fragColor;
layout(location = 1) out float fragStrength;
layout(location = 2) out float fragProgress;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    vec4 yStartColor;
    vec4 yEndColor;
    float size;
    float strength;
} object;

void main()
{
    gl_Position = camera.projection[gl_ViewIndex] * camera.view[gl_ViewIndex] * object.model[gl_ViewIndex] * vec4(position, 1);
    fragColor = gl_VertexIndex == 0 ? object.yStartColor.rgb : object.yEndColor.rgb;
    fragStrength = object.strength;
    fragProgress = gl_VertexIndex * object.size;
}
