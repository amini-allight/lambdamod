/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"
#include "common/atmosphere_constants.glsl"
#include "common/tools.glsl"
#include "common/lighting_types.glsl"
#include "common/atmosphere_types.glsl"

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    vec4 worldGravity;
    vec4 worldFlowVelocity;
    vec4 color;
    float worldDensity;
    float size;
    float radius;
    float mass;
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
    float areaRadius;
    vec4 viewerPositionDelta;
} object;
layout(std430, binding = 2) buffer Particles {
    EffectParticle data[];
} particles;

void main()
{
    EffectParticle particle = particles.data[gl_InstanceIndex];

    gl_Position = object.model[gl_ViewIndex] * vec4(particle.position.xyz, 1);

    // Particle motion
    float interval = object.timer - particle.timer;

    particle.position.xyz += object.worldFlowVelocity.xyz * atmosphereEffectStreamerSpeedMultiplier * interval;
    particle.timer = object.timer;

    if (length(particle.position.xyz) > object.areaRadius)
    {
        particle.position.xyz -= normalize(particle.position.xyz) * object.areaRadius * 2;
    }

    particles.data[gl_InstanceIndex] = particle;
}
