/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/tools.glsl"
#include "common/sky_constants.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Parameters {
    mat4 worldTransform;
    vec4 backgroundColor;
    vec4 color;
    vec2 position;
    float size;
    float phase;
} parameters;

void skyLayerCircleColor(vec3 direction)
{
    float phase = parameters.phase;

    vec3 circleDirection = quaternionRotate(
        quaternionFromAxisAngle(downDir, parameters.position.x),
        quaternionRotate(
            quaternionFromAxisAngle(rightDir, parameters.position.y),
            forwardDir
        )
    );
    float circleLineAngle = parameters.size / 2;
    float totalCircleSize = circleLineAngle + skyCircleLineWidth / 2;

    vec3 coverDirection = quaternionRotate(
        quaternionFromAxisAngle(downDir, parameters.position.x + circleLineAngle * phase * 2),
        quaternionRotate(
            quaternionFromAxisAngle(rightDir, parameters.position.y),
            forwardDir
        )
    );

    float angleFromCenter = angleBetween(direction, circleDirection);
    float angleFromCover = angleBetween(direction, coverDirection);

    vec4 color = vec4(0);
    vec4 stencil = vec4(0);

    for (int i = 0; i < skySampleCount; i++)
    {
        float sampleAngleFromCenter = (angleFromCenter - skySampleAngularStep * ((skySampleCount / 2) - 0.5)) + i * skySampleAngularStep;
        float sampleAngleFromCover = (angleFromCover - skySampleAngularStep * ((skySampleCount / 2) - 0.5)) + i * skySampleAngularStep;

        if (
            (abs(sampleAngleFromCenter - circleLineAngle) < skyCircleLineWidth / 2 && sampleAngleFromCover > totalCircleSize) ||
            (abs(sampleAngleFromCover - circleLineAngle) < skyCircleLineWidth / 2 && sampleAngleFromCenter < totalCircleSize)
        )
        {
            color += parameters.color;
            stencil += fgEmissiveStencil;
        }
        else if (sampleAngleFromCenter < circleLineAngle)
        {
            color += parameters.backgroundColor;
            stencil += bgStencil;
        }
    }

    color /= skySampleCount;

    if (color.a < 1)
    {
        color.rgb *= 1 / color.a;
    }

    outColor = color;

    stencil /= skySampleCount;

    if (stencil.a < 1)
    {
        stencil.rgb *= 1 / stencil.a;
    }

    outStencil = stencil;
}

void main()
{
    vec3 direction = (parameters.worldTransform * vec4(normalize(fragPosition), 1)).xyz;

    skyLayerCircleColor(direction);
}

