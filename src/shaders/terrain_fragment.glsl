/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/lighting_types.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec4 fragColor;
layout(location = 2) in vec3 fragSamplePosition;
layout(location = 3) in vec3 fragSampleNormal;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
} object;

#include "common/shadeless.glsl"

void main()
{
    vec3 nearestLine = round(fragSamplePosition / terrainGridInterval) * terrainGridInterval;
    vec3 distanceToLine = abs(fragSamplePosition - nearestLine);

    vec3 color;
    vec4 stencil;

    if (
        (distanceToLine.x < terrainGridThickness / 2 && abs(dot(fragSampleNormal, rightDir)) < 0.5) ||
        (distanceToLine.y < terrainGridThickness / 2 && abs(dot(fragSampleNormal, forwardDir)) < 0.5) ||
        (distanceToLine.z < terrainGridThickness / 2 && abs(dot(fragSampleNormal, upDir)) < 0.5)
    )
    {
        color = fragColor.rgb;
        stencil = fgEmissiveStencil;
    }
    else
    {
        color = vec3(0);
        stencil = bgStencil;
    }

    outColor = vec4(color, 1);
    outStencil = fogMix(stencil);
}
