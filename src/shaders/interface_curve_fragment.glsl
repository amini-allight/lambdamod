/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

#include "common/constants.glsl"
#include "common/tools.glsl"

layout(location = 0) in float fragEdge;

layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform Curve {
    vec2 p0;
    vec2 p1;
    vec2 p2;
    vec2 p3;
    vec4 color;
} curve;

void main()
{
    outColor = curve.color;
    outColor.a = 1 - pow(fragEdge, 4);
}
