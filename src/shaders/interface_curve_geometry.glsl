/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

#include "common/constants.glsl"
#include "common/tools.glsl"

const int curveSegmentCount = 16;

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 192) out;

layout(location = 0) out float fragEdge;

layout(std140, binding = 0) uniform Curve {
    vec2 p0;
    vec2 p1;
    vec2 p2;
    vec2 p3;
    vec4 color;
    vec2 screenSize;
    float width;
} curve;

vec2 cubicBezierCurvePosition(vec2 p0, vec2 p1, vec2 p2, vec2 p3, float t)
{
    return pow(1 - t, 3)*p0 + 3*sq(1 - t)*t*p1 + 3*(1 - t)*sq(t)*p2 + pow(t, 3)*p3;
}

vec2 cubicBezierCurveDirection(vec2 p0, vec2 p1, vec2 p2, vec2 p3, float t)
{
    return 3*sq(1 - t)*(p1 - p0) + 6*(1 - t)*t*(p2 - p1) + 3*sq(t)*(p3 - p2);
}

vec2 toScreen(vec2 p)
{
    return (p / curve.screenSize) * 2 - 1;
}

void main()
{
    for (int i = 0; i < curveSegmentCount; i++)
    {
        float t = i / float(curveSegmentCount);

        vec2 pos = cubicBezierCurvePosition(curve.p0, curve.p1, curve.p2, curve.p3, t);
        vec2 dir = cubicBezierCurveDirection(curve.p0, curve.p1, curve.p2, curve.p3, t);
        vec2 left = rotate(normalize(dir), +tau / 4);
        vec2 right = rotate(normalize(dir), -tau / 4);

        vec4 topRear = vec4(toScreen(pos + left * (curve.width / 2)), 0, 1);
        vec4 midRear = vec4(toScreen(pos), 0, 1);
        vec4 botRear = vec4(toScreen(pos + right * (curve.width / 2)), 0, 1);

        t = (i + 1) / float(curveSegmentCount);

        pos = cubicBezierCurvePosition(curve.p0, curve.p1, curve.p2, curve.p3, t);
        dir = cubicBezierCurveDirection(curve.p0, curve.p1, curve.p2, curve.p3, t);
        left = rotate(normalize(dir), +tau / 4);
        right = rotate(normalize(dir), -tau / 4);

        vec4 topFore = vec4(toScreen(pos + left * (curve.width / 2)), 0, 1);
        vec4 midFore = vec4(toScreen(pos), 0, 1);
        vec4 botFore = vec4(toScreen(pos + right * (curve.width / 2)), 0, 1);

        // A
        gl_Position = topRear;
        fragEdge = 1;
        EmitVertex();

        gl_Position = topFore;
        fragEdge = 1;
        EmitVertex();

        gl_Position = midRear;
        fragEdge = 0;
        EmitVertex();

        EndPrimitive();

        // B
        gl_Position = topFore;
        fragEdge = 1;
        EmitVertex();

        gl_Position = midFore;
        fragEdge = 0;
        EmitVertex();

        gl_Position = midRear;
        fragEdge = 0;
        EmitVertex();

        EndPrimitive();

        // C
        gl_Position = midRear;
        fragEdge = 0;
        EmitVertex();

        gl_Position = midFore;
        fragEdge = 0;
        EmitVertex();

        gl_Position = botFore;
        fragEdge = 1;
        EmitVertex();

        EndPrimitive();

        // D
        gl_Position = botFore;
        fragEdge = 1;
        EmitVertex();

        gl_Position = botRear;
        fragEdge = 1;
        EmitVertex();

        gl_Position = midRear;
        fragEdge = 0;
        EmitVertex();

        EndPrimitive();
    }
}
