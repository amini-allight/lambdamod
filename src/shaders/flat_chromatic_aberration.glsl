/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"

const vec2 center = vec2(0.5, 0.5);

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform ChromaticAberration {
    float strength;
    float timer;
} chromaticAberration;
layout(binding = 1) uniform sampler2D colorTexture;
layout(binding = 2) uniform sampler2D depthTexture;

void main()
{
    vec2 size = textureSize(colorTexture, 0).xy;
    vec2 dir = normalize(fragUV - center);
    float dist = distance(fragUV, center) / sqrt(pow(0.5, 2) + pow(0.5, 2));
    vec2 strength = vec2(maxChromaticAberrationStrength * (size.y / size.x), maxChromaticAberrationStrength) * chromaticAberration.strength * dist;

    float r = texture(colorTexture, fragUV + dir * chromaticAberrationColorOffset.r * strength).r;
    float g = texture(colorTexture, fragUV + dir * chromaticAberrationColorOffset.g * strength).g;
    float b = texture(colorTexture, fragUV + dir * chromaticAberrationColorOffset.b * strength).b;

    outColor.r = r;
    outColor.g = g;
    outColor.b = b;
    outColor.a = 1;
}
