/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 rotation;
layout(location = 2) in vec3 scale;
layout(location = 3) in vec4 color;
layout(location = 0) out vec4 geomRotation;
layout(location = 1) out vec3 geomScale;
layout(location = 2) out vec4 geomColor;

void main()
{
    gl_Position = vec4(position, 1);
    geomRotation = rotation;
    geomScale = scale;
    geomColor = color;
}
