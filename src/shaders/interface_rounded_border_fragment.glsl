/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform RoundedBorder {
    vec2 position;
    vec2 size;
    vec4 color;
    float width;
    float radius;
    vec2 screenSize;
} roundedBorder;

void main()
{
    vec2 pixelSize = roundedBorder.size * roundedBorder.screenSize;
    vec2 pixel = fragUV * pixelSize;

    vec2 topLeftCenter = vec2(roundedBorder.radius, roundedBorder.radius);
    vec2 topRightCenter = vec2(pixelSize.x - roundedBorder.radius, roundedBorder.radius);
    vec2 bottomLeftCenter = vec2(roundedBorder.radius, pixelSize.y - roundedBorder.radius);
    vec2 bottomRightCenter = vec2(pixelSize.x - roundedBorder.radius, pixelSize.y - roundedBorder.radius);

    if (pixel.x < topLeftCenter.x && pixel.y < topLeftCenter.y)
    {
        if (
            length(pixel - topLeftCenter) < roundedBorder.radius &&
            length(pixel - topLeftCenter) >= roundedBorder.radius - roundedBorder.width
        )
        {
            outColor = roundedBorder.color;
        }
        else
        {
            discard;
        }
    }
    else if (pixel.x >= topRightCenter.x && pixel.y < topRightCenter.y)
    {
        if (
            length(pixel - topRightCenter) < roundedBorder.radius &&
            length(pixel - topRightCenter) >= roundedBorder.radius - roundedBorder.width
        )
        {
            outColor = roundedBorder.color;
        }
        else
        {
            discard;
        }
    }
    else if (pixel.x < bottomLeftCenter.x && pixel.y >= bottomLeftCenter.y)
    {
        if (
            length(pixel - bottomLeftCenter) < roundedBorder.radius &&
            length(pixel - bottomLeftCenter) >= roundedBorder.radius - roundedBorder.width
        )
        {
            outColor = roundedBorder.color;
        }
        else
        {
            discard;
        }
    }
    else if (pixel.x >= bottomRightCenter.x && pixel.y >= bottomRightCenter.y)
    {
        if (
            length(pixel - bottomRightCenter) < roundedBorder.radius &&
            length(pixel - bottomRightCenter) >= roundedBorder.radius - roundedBorder.width
        )
        {
            outColor = roundedBorder.color;
        }
        else
        {
            discard;
        }
    }
    else if (
        pixel.x < roundedBorder.width ||
        pixel.y < roundedBorder.width ||
        pixel.x >= pixelSize.x - roundedBorder.width ||
        pixel.y >= pixelSize.y - roundedBorder.width
    )
    {
        outColor = roundedBorder.color;
    }
    else
    {
        discard;
    }
}
