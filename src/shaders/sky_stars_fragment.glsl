/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/sky_constants.glsl"

layout(location = 0) in vec2 fragUV;
// Color is unused, it's for clouds
layout(location = 1) in vec4 fragColor;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Parameters {
    mat4 transforms[skyMaxBillboardPartCount];
    vec4 colors[skyMaxBillboardPartCount];
    vec4 color;
} parameters;

void main()
{
    outColor = parameters.color;
    outStencil = fgEmissiveStencil;
}
