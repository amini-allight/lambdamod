/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/lighting_types.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec4 fragColor;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
} object;

#include "common/shadeless.glsl"

void main()
{
    outColor = vec4(fragColor.rgb, 1);
    outStencil = fogMix(fragColor.a < 0 ? bgStencil : fgEmissiveStencil);
}
