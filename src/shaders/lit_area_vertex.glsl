/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec3 normal;
layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec2 fragUV;
layout(location = 2) out vec3 fragNormal;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    // ignoring rest of uniform buffer
} object;

void main()
{
    gl_Position = camera.projection[gl_ViewIndex] * camera.view[gl_ViewIndex] * object.model[gl_ViewIndex] * vec4(position, 1);
    fragPosition = (object.model[gl_ViewIndex] * vec4(position, 1)).xyz;
    fragUV = uv;
    fragNormal = (object.model[gl_ViewIndex] * vec4(normal, 1)).xyz - (object.model[gl_ViewIndex] * vec4(vec3(0), 1)).xyz;
}
