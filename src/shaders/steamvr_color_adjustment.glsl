/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable

// This shader exists to work around a bug whereby SteamVR applies an unnecessary sRGB -> linear conversion to any supplied swapchain image, regardless of its actual format

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform SteamVRColorAdjustment {
    float timer;
} steamvrColorAdjustment;
layout(binding = 1) uniform sampler2DArray colorTexture;

float srgbToLinear(float srgb)
{
    if (srgb < 0.04045)
    {
        return srgb / 12.92;
    }
    else
    {
        return pow((srgb + 0.055) / 1.055, 2.4);
    }
}

float linearToSRGB(float linear)
{
    if (linear < 0.0031308)
    {
        return linear * 12.92;
    }
    else
    {
        return pow(1.055 * linear, 1 / 2.4) - 0.055;
    }
}

void main()
{
    vec3 rgb = texture(colorTexture, vec3(fragUV, gl_ViewIndex)).rgb;

    rgb.r = srgbToLinear(rgb.r);
    rgb.g = srgbToLinear(rgb.g);
    rgb.b = srgbToLinear(rgb.b);
    
    outColor.rgb = rgb;
    outColor.a = 1;
}
