/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#ifdef LMOD_RAY_TRACING
#extension GL_EXT_ray_query : enable
#endif
#include "common/constants.glsl"
#include "common/tools.glsl"
#include "common/lighting_types.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec3 fragNormal;
layout(location = 2) in vec4 fragColor;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
} object;
layout(binding = 2) uniform samplerCube environmentTexture;
#ifdef LMOD_RAY_TRACING
layout(binding = 3) uniform accelerationStructureEXT occlusionScene[eyeCount];
#else
layout(binding = 3) uniform OcclusionPrimitives {
    OcclusionPrimitive primitives[maxPrimitiveCount];
    int primitiveCount;
} occlusionScene;
#endif

#include "common/lighting.glsl"

void main()
{
    FragTransform fragTransform = correctNormals(fragPosition, fragNormal);

    // Alpha channel is used as a flag where -1 denotes "depth" areas which shouldn't receive lighting, 0 denotes non-emissive areas and +1 indicates emissive areas
    if (fragColor.a == 0)
    {
        outColor.rgb = vec3(0);

        for (int i = 0; i < object.lightCount && i < maxLightCount; i++)
        {
            outColor.rgb += lightEffect(object.lights[i], fragTransform.position, fragTransform.normal, fragColor.rgb);
        }

        outColor.rgb += ambientEffect(fragTransform.position, fragTransform.normal, fragColor.rgb);

        outColor.a = 1;
        outStencil = fogMix(fgStencil, fragTransform.position);
    }
    else if (fragColor.a > 0)
    {
        outColor = vec4(fragColor.rgb, 1);
        outStencil = fogMix(fgEmissiveStencil, fragTransform.position);
    }
    else
    {
        outColor = vec4(vec3(0), 1);
        outStencil = bgStencil;
    }
}
