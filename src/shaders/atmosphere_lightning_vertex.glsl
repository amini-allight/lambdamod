/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"
#include "common/tools.glsl"
#include "common/atmosphere_types.glsl"

layout (location = 0) out vec3 fragPosition;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 worldTransform;
    mat4 model[eyeCount];
    // Ignore rest of structure
} object;
layout (std430, binding = 2) readonly buffer Particles {
    LightningParticle data[];
} particles;
layout (std430, binding = 3) readonly buffer Vertices {
    vec4 data[];
} vertices;

void main()
{
    LightningParticle particle = particles.data[gl_InstanceIndex];
    vec3 vertex = vertices.data[particle.vertexOffset + gl_VertexIndex].xyz;

    vec3 position = (object.model[gl_ViewIndex] * vec4(particle.position.xyz, 1)).xyz;

    vec3 localParticleDir = vec3(normalize((inverse(object.worldTransform) * vec4(position, 1)).xy), 0);

    vec3 worldParticleDir = (object.worldTransform * vec4(localParticleDir, 1)).xyz;
    vec3 worldUpDir = (object.worldTransform * vec4(upDir, 1)).xyz;

    vec4 billboardRotation = quaternionFromDirectionRoll(worldParticleDir, worldUpDir);
    vertex = position + quaternionRotate(billboardRotation, vertex);

    gl_Position = camera.projection[gl_ViewIndex] * camera.view[gl_ViewIndex] * vec4(vertex, 1);
    fragPosition = vertex;
}
