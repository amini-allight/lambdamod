/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

#include "common/constants.glsl"
#include "common/tools.glsl"

layout(location = 0) out vec2 fragUV;

layout(std140, binding = 0) uniform Image {
    vec2 position;
    vec2 size;
    float alpha;
    float rotation;
} image;

vec2 vertices[6] = vec2[](
    vec2(0, 0),
    vec2(1, 1),
    vec2(0, 1),
    vec2(0, 0),
    vec2(1, 0),
    vec2(1, 1)
);

void main()
{
    vec2 vertex = rotate(vec2(0.5), vertices[gl_VertexIndex], image.rotation) * image.size;

    gl_Position = vec4(((image.position + vertex) * 2) - 1, 0, 1);
    fragUV = vertices[gl_VertexIndex];
}
