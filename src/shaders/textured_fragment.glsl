/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/lighting_types.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 1) in vec2 fragUV;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
} object;
layout(binding = 2) uniform Material {
    vec4 color;
    ivec2 tiles;
} material;
layout(binding = 3) uniform sampler2D colorTexture[maxTextureTileCount];

#include "common/shadeless.glsl"

float readTiledMaskTexture(vec2 fragUV)
{
    ivec2 tile = min(ivec2(fragUV * material.tiles), material.tiles - 1);

    return texture(colorTexture[tile.y * material.tiles.x + tile.x], fragUV).r;
}

void main()
{
    vec4 color = readTiledMaskTexture(fragUV) * material.color;

    // without this transparent objects draw over the perspective grid, it's not a perfect solution but it'll do
    if (color.a == 0)
    {
        discard;
    }

    outColor = color;
    outStencil = fogMix(fgEmissiveStencil);

    outStencil.a = color.a;
}
