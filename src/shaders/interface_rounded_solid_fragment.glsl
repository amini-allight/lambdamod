/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform RoundedSolid {
    vec2 position;
    vec2 size;
    vec4 color;
    vec2 screenSize;
    float radius;
} roundedSolid;

void main()
{
    vec2 pixelSize = roundedSolid.size * roundedSolid.screenSize;
    vec2 pixel = fragUV * pixelSize;

    vec2 topLeftCenter = vec2(roundedSolid.radius, roundedSolid.radius);
    vec2 topRightCenter = vec2(pixelSize.x - roundedSolid.radius, roundedSolid.radius);
    vec2 bottomLeftCenter = vec2(roundedSolid.radius, pixelSize.y - roundedSolid.radius);
    vec2 bottomRightCenter = vec2(pixelSize.x - roundedSolid.radius, pixelSize.y - roundedSolid.radius);

    if (pixel.x < topLeftCenter.x && pixel.y < topLeftCenter.y)
    {
        if (length(pixel - topLeftCenter) > roundedSolid.radius)
        {
            discard;
        }
        else
        {
            outColor = roundedSolid.color;
        }
    }
    else if (pixel.x >= topRightCenter.x && pixel.y < topRightCenter.y)
    {
        if (length(pixel - topRightCenter) > roundedSolid.radius)
        {
            discard;
        }
        else
        {
            outColor = roundedSolid.color;
        }
    }
    else if (pixel.x < bottomLeftCenter.x && pixel.y >= bottomLeftCenter.y)
    {
        if (length(pixel - bottomLeftCenter) > roundedSolid.radius)
        {
            discard;
        }
        else
        {
            outColor = roundedSolid.color;
        }
    }
    else if (pixel.x >= bottomRightCenter.x && pixel.y >= bottomRightCenter.y)
    {
        if (length(pixel - bottomRightCenter) > roundedSolid.radius)
        {
            discard;
        }
        else
        {
            outColor = roundedSolid.color;
        }
    }
    else
    {
        outColor = roundedSolid.color;
    }
}
