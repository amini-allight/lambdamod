/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform HDR {
    vec4 backgroundColor;
    float exposure;
    float gamma;
    int bloom;
} hdr;
layout(binding = 1) uniform sampler2D colorTexture;
layout(binding = 2, rgba32f) readonly uniform image2D stencilTexture;

// fraction successfully bloomed is returned in the alpha channel
vec4 blurVertical(vec2 p)
{
    vec2 pixelSize = 1.0 / textureSize(colorTexture, 0);
    vec4 result = vec4(0);

    vec2 coord = p;
    bool emissive = imageLoad(stencilTexture, ivec2(coord * imageSize(stencilTexture))).g != 0;

    if (emissive)
    {
        result.rgb += texture(colorTexture, coord).rgb * blurWeights[0];
        result.a += blurWeights[0];
    }

    for (int i = 1; i < blurSampleCount; i++)
    {
        vec2 aCoord = p + vec2(0.0, pixelSize.y * i);
        bool aEmissive = imageLoad(stencilTexture, ivec2(aCoord * imageSize(stencilTexture))).g != 0;

        if (aEmissive)
        {
            result.rgb += texture(colorTexture, aCoord).rgb * blurWeights[i];
            result.a += blurWeights[i];
        }

        vec2 bCoord = p - vec2(0.0, pixelSize.y * i);
        bool bEmissive = imageLoad(stencilTexture, ivec2(bCoord * imageSize(stencilTexture))).g != 0;

        if (bEmissive)
        {
            result.rgb += texture(colorTexture, bCoord).rgb * blurWeights[i];
            result.a += blurWeights[i];
        }
    }

    return result;
}

vec4 blur(vec2 p)
{
    vec2 pixelSize = 1.0 / textureSize(colorTexture, 0);
    vec4 result = blurVertical(p) * blurWeights[0];

    for (int i = 1; i < blurSampleCount; i++)
    {
        result += blurVertical(p + vec2(pixelSize.x * i, 0.0)) * blurWeights[i];
        result += blurVertical(p - vec2(pixelSize.x * i, 0.0)) * blurWeights[i];
    }

    return result;
}

void main()
{
    vec4 bloom = blur(fragUV);
    vec3 bloomColor = bloom.rgb;
    float bloomStrength = bloom.a;

    vec3 foregroundColor = texture(colorTexture, fragUV).rgb;

    if (hdr.bloom == 1)
    {
        foregroundColor += bloomColor;
    }

    float stencil = imageLoad(stencilTexture, ivec2(fragUV * imageSize(stencilTexture))).r;

    if (hdr.bloom == 1)
    {
        stencil = mix(stencil, hdrBloomStencil, bloomStrength);
    }

    float fgFraction = (stencil + 1) / 2;

    if (fgFraction != 0)
    {
        foregroundColor *= 1 / fgFraction;
    }

    foregroundColor = pow(
        vec3(1) - exp(-foregroundColor * hdr.exposure),
        vec3(1 / hdr.gamma)
    );

    // Causes objects to fade to background color as they get darker, rather than fading to black, causing them to still be visible when non-black background colors are used
    float brightness = max(foregroundColor.r, max(foregroundColor.g, foregroundColor.b));

    outColor.rgb = mix(hdr.backgroundColor.rgb, foregroundColor, brightness * fgFraction);
    outColor.a = 1;
}
