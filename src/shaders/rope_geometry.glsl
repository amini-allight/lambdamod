/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"
#include "common/tools.glsl"

layout(lines) in;
layout(triangle_strip) out;
layout(max_vertices = lineSegments * 2 * 3 * 2) out;
layout(location = 0) in vec4 geomRotation[];
layout(location = 1) in vec3 geomScale[];
layout(location = 2) in vec3 geomColor[];
layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec4 fragColor;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
} object;
layout(std140, binding = 2) uniform Material {
    float radius;
    int thin;
} material;

// TODO: ROPE: Scale seems to be omnidirectional

void outputTransformed(vec3 position, vec4 rotation, vec3 vertex, vec4 color)
{
    vertex = position + quaternionRotate(rotation, vertex);

    gl_Position = camera.projection[gl_ViewIndex] * camera.view[gl_ViewIndex] * object.model[gl_ViewIndex] * vec4(vertex, 1);
    fragPosition = (object.model[gl_ViewIndex] * vec4(vertex, 1)).xyz;
    fragColor = color;
    EmitVertex();
}

vec3 applyScale(vec3 base, vec3 scale)
{
    if (roughly(length(scale), 0))
    {
        return base;
    }

    vec3 direction = normalize(scale);
    float magnitude = length(scale);

    vec4 space = quaternionFromDirectionRoll(direction, pickUpDirection(direction));

    base = quaternionRotate(quaternionInverse(space), base);

    base *= vec3(1, magnitude, 1);

    base = quaternionRotate(space, base);

    return base;
}

void main()
{
    vec3 positionA = gl_in[0].gl_Position.xyz;
    vec4 rotationA = geomRotation[0];
    vec3 scaleA = geomScale[0];
    vec4 colorA = vec4(geomColor[0], 1);

    vec3 positionB = gl_in[1].gl_Position.xyz;
    vec4 rotationB = geomRotation[1];
    vec3 scaleB = geomScale[1];
    vec4 colorB = vec4(geomColor[1], 1);

    vec3 base = upDir * material.radius;
    vec3 innerBase = upDir * (material.radius - lineWidth);
    vec4 backgroundColor = vec4(vec3(0), -1);

    for (int i = 0; i < lineSegments; i++)
    {
        vec4 rotation = quaternionFromAxisAngle(forwardDir, tau * (i / float(lineSegments)));
        vec4 nextRotation = quaternionFromAxisAngle(forwardDir, tau * ((i + 1) / float(lineSegments)));

        vec3 a = quaternionRotate(rotation, applyScale(base, scaleA));
        vec3 b = quaternionRotate(nextRotation, applyScale(base, scaleA));
        vec3 c = quaternionRotate(rotation, applyScale(base, scaleB));
        vec3 d = quaternionRotate(nextRotation, applyScale(base, scaleB));

        if (material.thin == 1)
        {
            outputTransformed(positionA, rotationA, a, colorA);
            outputTransformed(positionA, rotationA, b, colorA);
            outputTransformed(positionB, rotationB, d, colorB);
            EndPrimitive();

            outputTransformed(positionA, rotationA, a, colorA);
            outputTransformed(positionB, rotationB, d, colorB);
            outputTransformed(positionB, rotationB, c, colorB);
            EndPrimitive();
        }
        else
        {
            vec3 innerA = quaternionRotate(rotation, applyScale(innerBase, scaleA));
            vec3 innerB = quaternionRotate(nextRotation, applyScale(innerBase, scaleA));
            vec3 innerC = quaternionRotate(rotation, applyScale(innerBase, scaleB));
            vec3 innerD = quaternionRotate(nextRotation, applyScale(innerBase, scaleB));

            outputTransformed(positionA, rotationA, a, colorA);
            outputTransformed(positionB, rotationB, d, colorB);
            outputTransformed(positionA, rotationA, b, colorA);
            EndPrimitive();

            outputTransformed(positionA, rotationA, a, colorA);
            outputTransformed(positionB, rotationB, c, colorB);
            outputTransformed(positionB, rotationB, d, colorB);
            EndPrimitive();

            outputTransformed(positionA, rotationA, innerA, backgroundColor);
            outputTransformed(positionA, rotationA, innerB, backgroundColor);
            outputTransformed(positionB, rotationB, innerD, backgroundColor);
            EndPrimitive();

            outputTransformed(positionA, rotationA, innerA, backgroundColor);
            outputTransformed(positionB, rotationB, innerD, backgroundColor);
            outputTransformed(positionB, rotationB, innerC, backgroundColor);
            EndPrimitive();
        }
    }
}
