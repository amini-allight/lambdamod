/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"
#include "common/atmosphere_constants.glsl"
#include "common/tools.glsl"
#include "common/lighting_types.glsl"

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = atmosphereEffectWarpCapCount * 3 + atmosphereEffectWarpSegmentCount * 4) out;

layout(location = 0) out vec3 fragPosition;
layout(location = 1) out vec3 fragNormal;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Object {
    mat4 model[eyeCount];
    vec4 worldGravity;
    vec4 worldFlowVelocity;
    vec4 color;
    float worldDensity;
    float size;
    float radius;
    float mass;
    Light lights[maxLightCount];
    int lightCount;
    float fogDistance;
    float timer;
    float areaRadius;
    vec4 viewerPositionDelta;
} object;

float horizontalSize()
{
    return atmosphereEffectWarpWidthMultiplier * object.size;
}

float halfVerticalSize()
{
    return object.size / 2;
}

vec4 flowSpace()
{
    vec3 toParticle = -normalize(gl_in[0].gl_Position.xyz);

    vec3 flowForward = normalize(object.worldFlowVelocity.xyz);
    vec3 flowSide = normalize(cross(flowForward, toParticle));

    return quaternionFromDirectionRoll(flowForward, flowSide);
}

void outputTransformed(vec2 position)
{
    vec3 worldPosition = gl_in[0].gl_Position.xyz + quaternionRotate(flowSpace(), vec3(0, position.x, position.y));

    gl_Position = camera.projection[gl_ViewIndex]
        * camera.view[gl_ViewIndex]
        * vec4(worldPosition, 1);
    fragPosition = worldPosition;
    fragNormal = quaternionRotate(flowSpace(), upDir);
}

void makeStartCap(float x, float nextX)
{
    outputTransformed(vec2(x, 0));
    EmitVertex();

    outputTransformed(vec2(nextX, -halfVerticalSize()));
    EmitVertex();

    outputTransformed(vec2(nextX, +halfVerticalSize()));
    EmitVertex();

    EndPrimitive();
}

void makeSegment(float x, float nextX)
{
    outputTransformed(vec2(x, -halfVerticalSize()));
    EmitVertex();

    outputTransformed(vec2(x, +halfVerticalSize()));
    EmitVertex();

    outputTransformed(vec2(nextX, -halfVerticalSize()));
    EmitVertex();

    outputTransformed(vec2(nextX, +halfVerticalSize()));
    EmitVertex();

    EndPrimitive();
}

void makeEndCap(float x, float nextX)
{
    outputTransformed(vec2(x, -halfVerticalSize()));
    EmitVertex();

    outputTransformed(vec2(x, +halfVerticalSize()));
    EmitVertex();

    outputTransformed(vec2(nextX, 0));
    EmitVertex();

    EndPrimitive();
}

void main()
{
    makeStartCap(0, horizontalSize());
    makeSegment(horizontalSize(), horizontalSize() * atmosphereEffectWarpSegmentMultiplier);
    makeEndCap(horizontalSize() * atmosphereEffectWarpSegmentMultiplier, horizontalSize() * (atmosphereEffectWarpSegmentMultiplier + 1));
}
