/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#extension GL_EXT_multiview : enable
#include "common/constants.glsl"
#include "common/sky_constants.glsl"

layout(points) in;
layout(triangle_strip) out;
layout(max_vertices = 4) out;
layout(location = 0) out vec2 fragUV;
layout(location = 1) out vec4 fragColor;

layout(std140, binding = 0) uniform Camera {
    mat4 projection[eyeCount];
    mat4 view[eyeCount];
} camera;
layout(std140, binding = 1) uniform Parameters {
    mat4 transforms[skyMaxBillboardPartCount];
    vec4 colors[skyMaxBillboardPartCount];
    // Ignore rest of structure
} parameters;

void main()
{
    mat4 pvm = camera.projection[gl_ViewIndex] * camera.view[gl_ViewIndex] * parameters.transforms[gl_PrimitiveIDIn];
    vec4 color = parameters.colors[gl_PrimitiveIDIn];

    gl_Position = pvm * vec4(-1, -1, 0, 1);
    fragUV = vec2(0, 0);
    fragColor = color;
    EmitVertex();

    gl_Position = pvm * vec4(-1, 1, 0, 1);
    fragUV = vec2(0, 1);
    fragColor = color;
    EmitVertex();

    gl_Position = pvm * vec4(1, -1, 0, 1);
    fragUV = vec2(1, 0);
    fragColor = color;
    EmitVertex();

    gl_Position = pvm * vec4(1, 1, 0, 1);
    fragUV = vec2(1, 1);
    fragColor = color;
    EmitVertex();

    EndPrimitive();
}
