/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/hsv.glsl"

layout(location = 0) out vec4 fragColor;

layout(std140, binding = 0) uniform Gradient {
    vec2 position;
    vec2 size;
    vec4 startColor;
    vec4 endColor;
} gradient;

vec2 vertices[6] = vec2[](
    vec2(0, 0),
    vec2(1, 1),
    vec2(0, 1),
    vec2(0, 0),
    vec2(1, 0),
    vec2(1, 1)
);

vec4 desaturate(vec4 color)
{
    color.g = 0;

    return color;
}

void main()
{
    vec2 vertexPosition = vertices[gl_VertexIndex];

    gl_Position = vec4(((gradient.position + (vertexPosition * gradient.size)) * 2) - 1, 0, 1);

    vec4 startColor = rgbaToHSVA(gradient.startColor);
    vec4 endColor = rgbaToHSVA(gradient.endColor);

    if (vertexPosition.x == 0 && vertexPosition.y == 0)
    {
        fragColor = vec4(endColor.r, startColor.g, endColor.b, 1);
    }
    else if (vertexPosition.x == 1 && vertexPosition.y == 0)
    {
        fragColor = endColor;
    }
    else if (vertexPosition.x == 0 && vertexPosition.y == 1)
    {
        fragColor = vec4(endColor.r, startColor.g, startColor.b, 1);
    }
    else if (vertexPosition.x == 1 && vertexPosition.y == 1)
    {
        fragColor = vec4(endColor.r, endColor.g, startColor.b, 1);
    }
}
