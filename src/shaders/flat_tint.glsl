/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable

const vec2 center = vec2(0.5, 0.5);

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform Tint {
    vec4 color;
    float strength;
    float timer;
} tint;
layout(binding = 1) uniform sampler2D colorTexture;
layout(binding = 2) uniform sampler2D depthTexture;

void main()
{
    float dist = distance(fragUV, center) / sqrt(pow(0.5, 2) + pow(0.5, 2));
    vec3 originalColor = texture(colorTexture, fragUV).rgb;

    outColor.rgb = mix(originalColor, tint.color.rgb, pow(dist, 2) * tint.strength);
    outColor.a = 1;
}
