/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/tools.glsl"
#include "common/sky_constants.glsl"
#include "common/hsv.glsl"

layout(location = 0) in vec3 fragPosition;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 1) uniform Parameters {
    mat4 worldTransform;
    vec4 backgroundColor;
    vec4 horizonColor;
    vec4 lowerZenithColor;
    vec4 upperZenithColor;
    int type;
} parameters;

void skyBaseBlankColor(vec3 direction)
{
    outColor = parameters.backgroundColor;
}

void skyBaseUnidirectionalColor(vec3 direction)
{
    const float range = pi / 2;

    if (direction.z == 0)
    {
        outColor = parameters.horizonColor;
    }
    else if (direction.z < 0)
    {
        const vec3 up = downDir;

        float angle = angleBetween(direction, up);
        float progress = 1 - (angle / range);

        outColor = mixAsHSVA(parameters.horizonColor, parameters.horizonColor * skyUnidirectionalLowerEnvMapStrength, progress);
    }
    else if (direction.z > 0)
    {
        const vec3 up = upDir;

        float angle = angleBetween(direction, up);
        float progress = 1 - (angle / range);

        outColor = mixAsHSVA(parameters.horizonColor, parameters.upperZenithColor, progress);
    }
}

void skyBaseBidirectionalColor(vec3 direction)
{
    const float range = pi / 2;

    if (direction.z == 0)
    {
        outColor = parameters.horizonColor;
    }
    else if (direction.z < 0)
    {
        const vec3 up = downDir;

        float angle = angleBetween(direction, up);
        float progress = 1 - (angle / range);

        outColor = mixAsHSVA(parameters.horizonColor, parameters.lowerZenithColor, progress);
    }
    else if (direction.z > 0)
    {
        const vec3 up = upDir;

        float angle = angleBetween(direction, up);
        float progress = 1 - (angle / range);

        outColor = mixAsHSVA(parameters.horizonColor, parameters.upperZenithColor, progress);
    }
}

void skyBaseOmnidirectionalColor(vec3 direction)
{
    outColor = parameters.upperZenithColor;
}

void skyBaseColor(vec3 direction)
{
    switch (parameters.type)
    {
    case Sky_Base_Blank :
        skyBaseBlankColor(direction);
        break;
    case Sky_Base_Unidirectional :
        skyBaseUnidirectionalColor(direction);
        break;
    case Sky_Base_Bidirectional :
        skyBaseBidirectionalColor(direction);
        break;
    case Sky_Base_Omnidirectional :
        skyBaseOmnidirectionalColor(direction);
        break;
    }
}

void main()
{
    vec3 direction = (parameters.worldTransform * vec4(normalize(fragPosition), 1)).xyz;

    skyBaseColor(direction);
}
