/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"

layout(location = 0) in vec2 fragUV;
layout(location = 0) out vec4 outColor;

layout(std140, binding = 0) uniform MotionBlur {
    float strength;
    float timer;
} motionBlur;
layout(binding = 1) uniform sampler2D colorTexture;
layout(binding = 2) uniform sampler2D depthTexture;
layout(binding = 3, rgba32f) uniform image2D accumulationTexture;

void main()
{
    ivec2 size = imageSize(accumulationTexture);
    vec3 currentColor = texture(colorTexture, fragUV).rgb;
    vec3 previousColor = imageLoad(accumulationTexture, ivec2(size * fragUV)).rgb * motionBlurDecayFactor;

    vec3 color = previousColor + currentColor;

    imageStore(accumulationTexture, ivec2(size * fragUV), vec4(color, 1));

    outColor.rgb = mix(currentColor, color, motionBlurDecayFactor * motionBlur.strength);
    outColor.a = 1;
}
