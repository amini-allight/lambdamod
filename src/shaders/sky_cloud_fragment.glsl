/*
Copyright 2025 Amini Allight

This file is part of LambdaMod.

LambdaMod is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

LambdaMod is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with LambdaMod. If not, see <https://www.gnu.org/licenses/>.
*/
#version 460
#extension GL_GOOGLE_include_directive : enable
#include "common/constants.glsl"
#include "common/sky_constants.glsl"

layout(location = 0) in vec2 fragUV;
layout(location = 1) in vec4 fragColor;
layout(location = 0) out vec4 outColor;
layout(location = 1) out vec4 outStencil;

layout(std140, binding = 1) uniform Parameters {
    mat4 transforms[skyMaxBillboardPartCount];
    vec4 colors[skyMaxBillboardPartCount];
    vec4 backgroundColor;
} parameters;

void main()
{
    vec4 color = vec4(0);
    vec4 stencil = vec4(0);

    for (int y = 0; y < sqrt(skySampleCount); y++)
    {
        for (int x = 0; x < sqrt(skySampleCount); x++)
        {
            vec2 uv = fragUV + (vec2(x, y) - vec2(skySampleCount / 2 - 0.5)) * skySampleLinearStep;

            if (uv.x < 0.1 || uv.y < 0.1 || uv.x >= 0.9 || uv.y >= 0.9)
            {
                color += fragColor;
                stencil += fgEmissiveStencil;
            }
            else
            {
                color += parameters.backgroundColor;
                stencil += bgStencil;
            }
        }
    }

    outColor = color / skySampleCount;
    outStencil = stencil / skySampleCount;
}
