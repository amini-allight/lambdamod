# LambdaMod Player's Guide

Welcome to the LambdaMod Player's Guide! This guide will fill you in on all you need to know to be a player in LambdaMod sessions.

## Contents

1. [Introduction](#introduction)
2. [Pinging and Braking](#pinging-and-braking)
3. [Magnitude](#magnitude)
4. [Other User Controls](#other-user-controls)
5. [Advantage](#advantage)
6. [Doom](#doom)
7. [Knowledge](#knowledge)
8. [Utilities](#utilities)
9. [VR Specific Utilities](#vr-specific-utilities)

<a name="introduction"></a>

## Introduction

LambdaMod is a unique kind of game which I call a visual mode roleplaying game (VMRPG). It's basically a tabletop roleplaying game such as Dungeons & Dragons, but in 100% video game form. There are no dice rolls or character sheets, the gameplay is that of a video game, but there is still a game master behind the scenes orchestrating events. When you join a server you are a disembodied viewpoint flying through the world. Your game master will provide a character for you. In some cases this character may have been pre-created for the scenario and in others the game master will allow you to make some choices which customize its abilities, appearance, etc. Once your character has been created you will "attach" to it. This will remove your ability to freely move around the world and instead give you direct control over the character. Once the game master **locks attachments** you will no longer be able to detach from your character. You will have to interact with the world exclusively through them, seeing through their eyes and moving them with your inputs. Your character is a custom creation of the game master and might have a complex custom control scheme so you can bring up the help window to tell you what inputs are recognized.

Flat bindings used in this section:

- **Attach:** U in viewer mode with the cursor placed over the target entity.
- **Detach:** Escape
- **Open Help Window:** F1
- **Close Help Window:** Escape

VR bindings used in this section:

- **Attach:** Either trigger with the hand placed on the target entity.
- **Detach:** Hold the secondary button (e.g. B, menu, Y) on both controllers for one second to open the menu. Select "detach" from the menu.
- **Open Help Window:** Unavailable, there are both fewer VR bindings possible and VR inputs tend to be more complex (potentially integrating contextual cues and gestures) meaning a help window is unlikely to help much. A solution to this may be provided eventually.

Gamepad bindings used in this section:

- **Attach:** Either trigger with the center of the screen over the target entity.
- **Detach:** Press start (or equivalent button) to open the menu. Select "detach" from the menu.
- **Open Help Window:** Unbound, you will need to press F1 on your keyboard to access it.

<a name="pinging-and-braking"></a>

## Pinging & Braking

Imagine you're exploring an abandoned building and you come to a door. You want to try break the door down but you discover the game master hasn't configured the door as a breakable physics object. You might want to contact the game master so you can ask them to adjucate your attempt to break it down, as they would in a tabletop game. You can do this by **pinging** them. Pings make the game master aware of your request with a location marker and an audible sound.

Now imagine enemies are storming your location and you want to break down the same door so you can escape. There may not be time to ping the game master, wait for them to respond, explain the situation and have them decide whether to remove the door. In this situation you can pull the **brake**. The brake is the same as a ping, but it notifies everyone on the server when pulled and it slows the game to a stop. This gives the game master time to respond in time critical situations. Once the game master has decided whether or not to remove the door they can release the brake and the game will return to normal speed. Once pulled only the game master can release the brake.

Flat bindings used in this section:

- **Ping:** Tab
- **Brake:** Ctrl+Space

VR bindings used in this section:

- **Ping:** Hold the secondary button (e.g. B, menu, Y) on both controllers for one second to open the menu. Select "ping" from the menu.
- **Detach:** Hold the secondary button (e.g. B, menu, Y) on both controllers for one second to open the menu. Select "brake" from the menu.

Gamepad bindings used in this section:

- **Ping:** Press start (or equivalent button) to open the menu. Select "ping" from the menu.
- **Detach:** Press start (or equivalent button) to open the menu. Select "brake" from the menu.

<a name="magnitude"></a>

## Magnitude

Imagine you're speaking with some plot critical characters when suddenly your friend accidentally presses their character's grenade key. They toss a grenade into the crowded room, killing several members of your party and several important characters. This sort of thing doesn't happen in tabletop games, players can't really do _anything_ accidentally, but in LambdaMod it is a significant problem.

Now imagine another scenario. You and another player are exploring a city hub area. But you're both afraid that if you take too long the other player will get ahead of you and experience all the fun content without you so you both start to rush a little just in case the other player is rushing. Worse, still if you see the other player appears to be rushing you might rush even more creating a feedback loop. Again, this problem doesn't exist in tabletop games, players can see and hear everything that goes on around the table and have constant feedback about what the rest of the group is doing.

LambdaMod's main solution to both of these problems is **magnitude**. The session can be in one of four magnitude levels: 0, 1, 2 or 3. The levels are as follows:

- **Magnitude 0:** All player input is ignored, the game is effectively paused.
- **Magnitude 1:** Only reversible actions are available, like walking around and talking to people.
- **Magnitude 2:** Some minor irreversible actions are available, like accepting quests or crafting items.
- **Magnitude 3:** All actions are available, nothing is restricted.

All players get a vote on the magnitude, which they can adjust at any time. The overall magnitude of the session is the lowest value that gets at least one vote. The game master can also set a maximum allowed value which can change at any time. Let's consider an example:

- Alice, Bob and Charlie are all playing LambdaMod together. They have been fighting some monsters so their magnitudes area all set to 3. The session magnitude is 3.
- They enter a city environment, the intention here is for them to interact with quest givers and vendors, not fight, so the game master drops the maximum magnitude to 2, forcing all the players' magnitudes down to 2. The session magnitude is now 2.
- Alice says she has to step away for a minute and doesn't want the others spending the group's money without her, so she drops her magnitude to 1. Magnitude takes the lowest value anybody votes for, so the session magnitude is now 1.
- Bob is wandering around the city waiting for Alice to get back when he finds himself in a dark alleyway and set upon by bandits. The game master raises the max magnitude to 3 when the bandits appear and Bob and Charlie raise their magnitudes to 3 as well. But alice is still away, so the session magnitude is still 1. Bob throws the brake while he waits for Alice.
- Finally Alice returns and, once she's caught up on what's happening, she raises her magnitude to 3 and tells the game master to release the brake. The players can then fight the bandits.

The core concept is that anyone can lower the magnitude but only a consensus by all users can raise it. This applies to the game master as well, they can lower the magnitude by lowering the maximum, but raising the maximum will have no effect until the players raise their own votes to match.

Flat bindings used in this section:

- **Magnitude 0:** Ctrl+Escape
- **Magnitude 1:** Ctrl+F1
- **Magnitude 2:** Ctrl+F2
- **Magnitude 3:** Ctrl+F3

VR bindings used in this section:

- **Set Magnitude:** Hold the secondary button (e.g. B, menu, Y) on both controllers for one second to open the menu. Select your desired magnitude from the menu.

Gamepad bindings used in this section:

- **Set Magnitude:** Press start (or equivalent button) to open the menu. Select your desired magnitude from the menu.

<a name="other-user-controls"></a>

## Other User Controls

Sometimes magnitude isn't enough. There are a couple other user controls that the game master might employ to minimize disruptions. You might be trekking across a vast expanse of jungle. To keep the players from becoming separated the game master might use the **tether** control. This control reduces the speed at which players can moved based on their distance from the average position of the other players.

Or you might be exploring one district of a city, with another district off-limits to be visited later. To contain the players the game master might use the **confinement** control which prevents the players from leaving a spherical region.

If the game master needs to indicate to the players that there will be a delay, e.g. while they set up the next part of the adventure, they can use the **wait** control. This displays a waiting spinner, either in the center of the screen on flat mode or on the ground surrounding the player in VR mode. When you see this symbol just sit tight and wait for the game master to get things moving again.

Imagine the players are sneaking around in the rafters of a large warehouse, above the heads of several guards. Magnitude is set to 1 to prevent them from opening fire until everyone's ready but then suddenly, one of the players slips off and lands in front of the guards. Perhaps that makes for an exciting story moment and you decide to keep it, but if not the game master can choose to **rewind**, causing time to run in reverse and undoing the last several seconds. A more advanced version of this are **checkpoints**, the game master can create checkpoints at any time and reset the entire session back to them so the players can try again.

Finally the game master can also choose to **slow** the game, giving you more time to react to events (e.g. in combat) or **pause** it, causing the passage of time to stop entirely.

<a name="advantage"></a>

## Advantage

**Advantage** is a metacurrency the game master can award or remove at any time. When you have advantage points they can give you a passive stacking 20% boost to some of your stats, though exactly which stats and when will be decided by the game master. The purpose of advantage is to provide a buffer around failure. In a tabletop game players will often go tens or hundreds of hours without a death amongst the group. This creates great story continuity but is very hard to achieve in a game like LambdaMod without making the gameplay trivial. Advantage is a solution to this. Whenever the player would suffer a critical injury the game master can instead remove an advantage point (even going below zero, at which point it imposes a penalty) and discount the injury.

Advantage is awarded at the discretion of the game master: usually for making progress in the story, resting your character between encounters or roleplaying well.

<a name="doom"></a>

## Doom

**Doom** is a flag the game master might set to communicate that you are threatened by unseen danger. Imagine your party is breaking into an art gallery for a heist when you trip a motion detector that triggers a silent alarm. Inside of the game there will be no warning that the police are coming but the doom flag being set hints that something has gone wrong and you should reevaluate your situation. The doom flag will be cleared whenever you either escape the danger, e.g. by fleeing the art gallery, or when the consequences arrive and become obvious to you, e.g. when you hear the police cars pull up outside.

<a name="knowledge"></a>

## Knowledge

Your character usually knows more about their world than you do. Rather than needing to get all your information from exchanges with the game master or characters, you can also get some from your character's own "knowledge". You can open the **knowledge viewer** and consult a set of entries created by the game master which provide an overview of the things your character knows about their world. The game master might put other things in there too, like a summary of your exploits across past sessions or your currently active objectives.

Flat bindings used in this section:

- **Show Knowledge Viewer:** F4

VR bindings used in this section:

- **Show Knowledge Viewer:** Hold the secondary button (e.g. B, menu, Y) on both controllers for one second to open the menu. Select "knowledge" from the menu.

Gamepad bindings used in this section:

- **Show Knowledge Viewer:** Press start (or equivalent button) to open the menu. Select "knowledge" from the menu.

<a name="utilities"></a>

## Utilities

LambdaMod includes a variety of utilities that are similar to those you might be used to using in tabletop games. If you want to take notes you can open the **document manager**. Here you can create notes which only you can see (they are saved on the server as part of the save file so whoever operates the server can also see them). If you want to roll dice or do quick calculations you can open the **console** and use the scripting language. Math is done in prefix notation form, for example `(4 / 2) + (3 * 6)` becomes `(+ (/ 4 2) (* 3 6))`. `3d6` becomes `(dice 3 6)`. There's also `(zdice 1 10)` if you want to roll a dice that starts from zero instead of one. Finally if you want to roll on random tables that are installed in the server you can open the **oracle manager**, select one of the available oracles and press roll.

Flat bindings used in this section:

- **Open Document Manager:** F2
- **Open Console:** Backquote
- **Open Oracle Manager:** F6

VR bindings used in this section:

These features are currently unavailable in VR mode.

Gamepad bindings used in this section:

These features are currently unavailable when using a gamepad.

<a name="vr-specific-utilities"></a>

## VR Specific Utilities

There are a couple special utilities that are only available in VR mode. All of these utilities are available via the attached VR menu, accessible by holding the secondary button (e.g. B, menu, Y) on both controllers for one second.

- **Anchor:** This anchors you to your character. When you first attach to a character in VR mode you won't have any control over its limbs until you do this. After pressing the menu button you need to stand in a pose that matches the pose of the character, lining up your hands with its hands, your head with its head, etc. Then you press the primary button (e.g. A, grip, X) on either controller to confirm the calibration. You can repeat this to adjust the calibration at any time.
- **Scaling:** This increases or decreases your effective height. This is LMOD's only compensation for mismatched character and player sizes so it's important to use it. For example if your character is 3 m tall but you're only 1.7 m tall in real life you need to increase your scale to about 1.75 to compensate, making you taller than you really are (you will perceive this as everything else getting smaller).
- **Recenter:** When you get into a vehicle it's likely your character's head and yours won't align anymore, especially if you move to sit down on something within your real life playspace. To correct this you should put your body into a relaxed position that you'd like to be the new center of your pose (e.g. sitting in your chair in a relaxed pose, looking level and forward) and press recenter on the menu. This will readjust your room offset to make this pose the new default.
- **Orient Room/Reset Room:** A more extreme version of this is the orient room tool. Once pressed this will count down for three seconds and then reorient the room so that forward is whichever way the player is looking. For example if you lie down on the floor during those three seconds and look upwards the playspace will be reoriented so that up in real life corresponds to forward in LambdaMod. This can be useful for navigating zero-G environments or playing the game while lying down. You can use the reset room button to return to the default orientation.
