# LambdaMod User Manual

Welcome to the LambdaMod user manual! The manual is pretty large so this page functions as a table of contents, see below for a list of other pages with actual content:

- [Basic Setup](basic-setup.md): Learn how to set up a LambdaMod server.
- [Advanced Setup](advanced-setup.md): Details of all the different connection options provided by LambdaMod.
- [TLS Setup](tls-setup.md): Learn how to set up a LambdaMod server with properly configured network encryption.
- [Player's Guide](players-guide.md): A guide to being a player in LambdaMod.
- [Game Master's Guide](game-masters-guide.md): A guide to being a game master in LambdaMod.
- [Scripting](scripting.md): An overview of the LambdaMod scripting language.
- [State](state.md): Details of how and where LambdaMod stores persistent data.
- [Configuration](configuration.md): Complete documentation of the format used in all the different configuration files.
- [Bindings](bindings.md): A list of all the control bindings in LambdaMod.
- [Security](security.md): An overview of LambdaMod's security architecture.
- [Binary Formats](binary-formats.md): Documentation for the custom binary formats used in LambdaMod.
