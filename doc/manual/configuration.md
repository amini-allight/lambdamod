# Configuration

This guide covers the format of the various configuration files that LambdaMod reads. For details of the location of these files see the [state documentation](state.md).

## Contents

1. [Client Configuration](#client-configuration)
2. [Server Configuration](#server-configuration)
3. [Client Theme](#client-theme)
4. [Client Bindings](#client-bindings)
5. [Client Localization](#client-localization)

<a name="client-configuration"></a>

## Client Configuration

The LambdaMod client configuration file (`client.yml`) supports the following keys:

| Name                     | Default Value | Description                                                                                                                                                                                                                                                              |
|--------------------------|---------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| startupSplash            | Yes           | Enables the developer splash displaying upon startup.                                                                                                                                                                                                                    |
| splash                   | Yes           | Enables the help splash screen.                                                                                                                                                                                                                                          |
| frameCounter             | No            | Enables the frame counter. Flat mode only.                                                                                                                                                                                                                               |
| frameLimit               | 0             | A value of zero enables vsync and limits the frame rate to the refresh rate of the display. A non-zero value disables vsync and instead limits the frame rate to the specified value (this may induce tearing).                                                          |
| stepRate                 | 100           | Number of simulation steps per second. Lower values produce a smoother simulation but increase the performance requirements of the client.                                                                                                                               |
| historyWindow            | 2             | Length of time in seconds that the client will store history for, allowing for client-side prediction.                                                                                                                                                                   |
| pingCounter              | No            | Enables the ping counter. Flat mode only.                                                                                                                                                                                                                                |
| antiAliasingLevel        | 4             | The number of MSAA samples to use. Higher values will significantly increase VRAM usage. Ignored in VR mode, the value will be read from the OpenXR runtime instead.                                                                                                     |
| width                    | 1600          | The initial width of the window in pixels. Flat mode only.                                                                                                                                                                                                               |
| height                   | 900           | The height of the window in pixels. Flat mode only.                                                                                                                                                                                                                      |
| fullscreen               | Yes           | Whether or not the window will start in fullscreen mode. Flat mode only.                                                                                                                                                                                                 |
| outputMuted              | No            | Whether or not audio output will be muted initially.                                                                                                                                                                                                                     |
| outputVolume             | 1             | The initial volume of the audio output from 0 to 1.                                                                                                                                                                                                                      |
| inputMuted               | No            | Whether or not audio input will be muted initially.                                                                                                                                                                                                                      |
| inputVolume              | 1             | The initial volume of the audio input from 0 to 1.                                                                                                                                                                                                                       |
| vrMotionMode             | head          | Specifies which VR device is used to determine the movement direction. Possible values are `head`, `hand` and `hip`.                                                                                                                                                     |
| vrSmoothTurning          | No            | When enabled the snap turning in unattached VR mode will be replaced with smooth turning.                                                                                                                                                                                |
| vrSmoothTurnSpeed        | 22.5          | The rate of turn when using VR smooth turning, in degrees per second.                                                                                                                                                                                                    |
| vrSnapTurnIncrement      | 45            | The increment of turning when using VR snap turning, in degrees.                                                                                                                                                                                                         |
| vrKeyboardRightHand      | No            | When enabled the VR keyboard will be anchored to your right hand rather than your left.                                                                                                                                                                                  |
| vrForced                 | No            | When enabled the game will launch in VR mode regardless of command line options.                                                                                                                                                                                         |
| vrRollCage               | No            | When enabled will display a cage around the player at all times to alleviate motion sickness.                                                                                                                                                                            |
| graphicsDevice           | ""            | Forces the use of a specific graphics device, otherwise LambdaMod will pick one automatically. Unused in VR mode.                                                                                                                                                        |
| soundInputDevice         | ""            | Forces the use of a specific sound input device, otherwise LambdaMod will pick one automatically. Unused on Web.                                                                                                                                                         |
| soundOutputDevice        | ""            | Forces the use of a specific sound output device, otherwise LambdaMod will pick one automatically. Unused on Web.                                                                                                                                                        |
| gamepadDevice            | ""            | Forces the use of a specific gamepad device, otherwise LambdaMod will pick one automatically. Unused on Windows.                                                                                                                                                         |
| touchDevice              | ""            | Forces the use of a specific touch device, otherwise LambdaMod will pick one automatically. Unused on Windows and Web.                                                                                                                                                   |
| caFilePath               | ""            | Path to the certificate authority file used to verify the server's identity. Disables verification if empty.                                                                                                                                                             |
| advancedTextEditor       | No            | Enables the Vim-style advanced text editor, replacing the basic text editor.                                                                                                                                                                                             |
| nodeEditor               | Yes           | Enables the visual node editor, as an alternative to textual code editing.                                                                                                                                                                                               |
| quaternionEditor         | No            | Enables the quaternion editor, exposing quaternions in the UI instead of euler angles. These are a more correct way of expressing rotation, but harder to understand.                                                                                                    |
| numpadAlternative        | No            | Enables the use of the number keys in the place of numpad keys, for use on keyboards without numpads.                                                                                                                                                                    |
| maxRecursionDepth        | 4096          | Maximum number of functions calling other functions permitted in the scripting language. Higher values will allow more complex recursive programs, but may cause the server to crash if the operating system's limits on stack size are exceeded as a result.            |
| maxMemoryUsage           | 10485760      | Approximate amount of memory permitted to be used by the scripting language. Higher values will allow more complex programs, but may allow runaway programs to consume too much system memory.                                                                           |
| maxLoopDuration          | 0.02          | Maximum amount of time in seconds that the scripting language can spend in a `while`, `for`, etc. loop. Higher values will allow programs to crunch for longer, but may cause the system to freeze if a runaway program is executed.                                     |
| invertMouseViewerX       | No            | Inverts mouse panning of the viewer in the left/right direction.                                                                                                                                                                                                         |
| invertMouseViewerY       | No            | Inverts mouse panning of the viewer in the up/down direction.                                                                                                                                                                                                            |
| mouseViewerSensitivity   | [ 1.0, 1.0 ]  | A multiplier applied to mouse panning of the viewer.                                                                                                                                                                                                                     |
| invertGamepadViewerX     | No            | Inverts thumbstick panning of the viewer in the left/right direction.                                                                                                                                                                                                    |
| invertGamepadViewerY     | No            | Inverts thumbstick panning of the viewer in the up/down direction.                                                                                                                                                                                                       |
| gamepadViewerSensitivity | [ 1.0, 1.0 ]  | A multiplier applied to thumbstick panning of the viewer.                                                                                                                                                                                                                |
| gamepadLeftDeadZone      | [ 0.1, 0.1 ]  | The fraction of the total movement range of the thumbstick that will be ignored. Expressed in the range 0 - 1, applied separately to each side (i.e. 0.1 ignores the bottom 10% of leftward motion and the bottom 10% of rightward motion).                              |
| gamepadRightDeadZone     | [ 0.1, 0.1 ]  | The fraction of the total movement range of the thumbstick that will be ignored. Expressed in the range 0 - 1, applied separately to each side (i.e. 0.1 ignores the bottom 10% of leftward motion and the bottom 10% of rightward motion).                              |
| vrLeftDeadZone           | [ 0.0, 0.0 ]  | The fraction of the total movement range of the left VR thumbstick that will be ignored.                                                                                                                                                                                 |
| vrRightDeadZone          | [ 0.0, 0.0 ]  | The fraction of the total movement range of the left VR thumbstick that will be ignored.                                                                                                                                                                                 |
| soundSpatialMode         | headphones    | Controls whether the sound system will use HRTF spatialization (better result but only suitable for headphones) or regular spatialization (worse result but suitable for speakers). Possible values are `headphones` and `speakers`.                                     |
| soundMono                | No            | When enabled the same audio will be output through both the left and right channels.                                                                                                                                                                                     |
| touchInput               | Yes           | Controls whether the virtual gamepad will be enabled when a touchscreen is detected.                                                                                                                                                                                     |
| flatMotionBlur           | No            | Enables use of the motion blur post process shader in flat mode.                                                                                                                                                                                                         |
| flatDepthOfField         | No            | Enables use of the depth of field post process shader in flat mode.                                                                                                                                                                                                      |
| flatBloom                | Yes           | Enables use of the bloom post process shader in flat mode.                                                                                                                                                                                                               |
| flatChromaticAberration  | No            | Enables use of the chromatic aberration post process shader in flat mode.                                                                                                                                                                                                |
| vrMotionBlur             | No            | Enables use of the motion blur post process shader in vr mode.                                                                                                                                                                                                           |
| vrDepthOfField           | No            | Enables use of the depth of field post process shader in vr mode.                                                                                                                                                                                                        |
| vrBloom                  | Yes           | Enables use of the bloom post process shader in vr mode.                                                                                                                                                                                                                 |
| vrChromaticAberration    | No            | Enables use of the chromatic aberration post process shader in vr mode.                                                                                                                                                                                                  |
| rayTracing               | Yes           | Enables use of ray tracing. This setting only has an effect if the graphics device supports raytracing, otherwise it is ignored.                                                                                                                                         |
| imperialUnits            | No            | Enables the use of US customary/imperial units in the UI. Units in the underlying simulation are always in metric, including those accessed by the scripting language.                                                                                                   |
| degrees                  | Yes           | Enables the use of degrees instead of degrees in the UI. Units in the underlying simulation are always in radians, including those accessed by the scripting language.                                                                                                   |
| uiSounds                 | Yes           | Enables the sounds that play with UI elements are moused over or interacted with.                                                                                                                                                                                        |
| uiAutoScale              | No            | Enables dynamic adjustment of the UI scale to match the current window size. This means the game will automatically adjust to being played on a TV or Steam Deck but using a larger screen to get more screen real estate becomes impossible.                            |
| uiScale                  | 1             | The UI scale multiplier. When auto scaling is disabled this dictates the scale of the UI elements. When auto scaling is enabled it acts as a multiplier on top of the auto-scaled size.                                                                                  |
| luminanceScale           | 0.5           | The luminance scale multiplier. Scales the image down by this multiplier before calculating luminance, improving luminance calculation performance at large screen resolutions.                                                                                          |

On Linux or FreeBSD you can find device names with the following commands:

- `vulkaninfo` for graphics devices.
- `pactl list sources` for sound input devices.
- `pactl list sinks` for sound output devices.
- `ls -l /dev/input/by-id` for gamepad devices.

<a name="server-configuration"></a>

## Server Configuration

The LambdaMod server configuration file (`server.yml`) supports the following keys:

| Name              | Default Value  | Description                                                                                                                                                                                                                                                                |
|-------------------|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| host              | 0.0.0.0        | The network address to accept connections on, if none is specified when starting the server. Use `0.0.0.0` for all available interfaces, `127.0.0.1` to only accept local connections, or an interface's specific address to only use that interface.                      |
| port              | 7443           | The network port to accept connections on. Can be anything between 1024 and 65535.                                                                                                                                                                                         |
| password          | password       | The server password which all users must be supply to connect.                                                                                                                                                                                                             |
| adminPassword     | secretpassword | An alternative server password which, when supplied in place of the regular server password, grants the user admin permissions.                                                                                                                                            |
| savePath          | ./save         | The path where save data is stored/loaded. The target directory will be created if it does not exist and the files will be placed inside of it.                                                                                                                            |
| stepRate          | 100            | Number of simulation steps per second. Lower values produce a smoother simulation but increase the performance requirements of the server.                                                                                                                                 |
| historyWindow     | 10             | Length of time in seconds that the server will store history for, allowing for time to be rewound or faciliating server-side correction.                                                                                                                                   |
| saveHistory       | no             | Enables saving history, which will allow you to rewind immediately upon loading the server's save, at the cost of slower saving/loading and increased save file size.                                                                                                      |
| certFilePath      | ""             | Path to the certificate file used to verify the server's identity. If empty the server will autogenerate one.                                                                                                                                                              |
| keyFilePath       | ""             | Path to the private key file used to verify the server's identity. If empty the server will autogenerate once.                                                                                                                                                             |
| connectionMode    | direct         | Controls the server's use of `lambdamod.org` to assist connection. In `direct` mode the external server is not used. In `invite` mode it is used to generate a join URL. In `session` mode it is used to support TCP hole punching to remove the need for port forwarding. |
| saveInterval      | 10             | Length of time in seconds between the server's saves of the game state to disk.                                                                                                                                                                                            |
| maxUndoHistory    | 128            | Number of steps of undo history preserved for each user-world combination.                                                                                                                                                                                                 |
| maxRecursionDepth | 4096           | Maximum number of functions calling other functions permitted in the scripting language. Higher values will allow more complex recursive programs, but may cause the server to crash if the operating system's limits on stack size are exceeded as a result.              |
| maxMemoryUsage    | 10485760       | Approximate amount of memory permitted to be used by the scripting language. Higher values will allow more complex programs, but may allow runaway programs to consume too much system memory.                                                                             |
| maxLoopDuration   | 0.02           | Maximum amount of time in seconds that the scripting language can spend in a `while`, `for`, etc. loop. Higher values will allow programs to crunch for longer, but may cause the system to freeze if a runaway program is executed.                                       |
| upnp              | Yes            | Enables the use of UPnP to automatically perform port forwarding for a non-hole punch server.                                                                                                                                                                              |
| upnpIPv6          | No             | Enables searching for UPnP devices on IPv6.                                                                                                                                                                                                                                |
| webSocketHost     | 0.0.0.0        | The network address to accept WebSocket connections on, if none is specified when starting the server. Use `0.0.0.0` for all available interfaces, `127.0.0.1` to only accept local connections, or an interface's specific address to only use that interface.            |
| webSocketPort     | 7444           | The network port to accept WebSocket connections on. Can be anything between 1024 and 65535, but must differ from the port used for normal connections.                                                                                                                    |

<a name="client-theme"></a>

## Client Theme

The LambdaMod client theme configuration file (`client-theme.yml`) supports the following keys:

| Name                      | Description                                                                                                       |
|---------------------------|-------------------------------------------------------------------------------------------------------------------|
| panelForegroundColor      | Color used for foreground elements overlaying the world (i.e. HUD, text panel, titlecard, disconnection message). |
| worldBackgroundColor      | Color used for the background of the world.                                                                       |
| backgroundColor           | Color used for the background of UI windows.                                                                      |
| altBackgroundColor        | Alternative color used for the background of UI windows (mostly for dialogs).                                     |
| softIndentColor           | Color used for the background of the timeline widgets.                                                            |
| indentColor               | Color used to imply indentation of UI elements (i.e. line edits, checkboxes).                                     |
| outdentColor              | Color used to imply outdentation of UI elements (i.e. buttons, window borders).                                   |
| accentColor               | Color used to highlight selected or active items in the UI.                                                       |
| activeTextColor           | Color used for text that is being hovered or selected.                                                            |
| textColor                 | Color used for most text.                                                                                         |
| inactiveTextColor         | Color used to imply text is disabled i.e. when an entity doesn't have an owner.                                   |
| warningColor              | Color used for warnings in the console.                                                                           |
| errorColor                | Color used for errors in the console.                                                                             |
| dividerColor              | Color used for dividing lines between UI elements.                                                                |
| positiveColor             | Color used for positive indicators.                                                                               |
| negativeColor             | Color used for negative indicators.                                                                               |
| keywordColor              | Color used for syntax highlighting keywords.                                                                      |
| stdlibColor               | Color used for syntax highlighting standard library definitions.                                                  |
| bracketColor              | Color used for syntax highlighting brackets.                                                                      |
| quoteColor                | Color used for syntax highlighting the quote symbol.                                                              |
| escapedColor              | Color used for syntax highlighting escaped characters within strings.                                             |
| stringColor               | Color used for syntax highlighting strings.                                                                       |
| numberColor               | Color used for syntax highlighting numbers.                                                                       |
| cursorColor               | Color used for the cursor in the text editor.                                                                     |
| bracketHighlightColor     | Color used to highlight the range between two matching brackets.                                                  |
| xAxisColor                | Color used for the X axis.                                                                                        |
| yAxisColor                | Color used for the Y axis.                                                                                        |
| zAxisColor                | Color used for the Z axis.                                                                                        |
| gridColor                 | Color used for the perspective and orthographic grids (every 10th line).                                          |
| gridSecondaryColor        | Color used for the perspective and orthographic grids (all other non-axis lines).                                 |
| touchInterfaceColor       | Color used for the touch interface when not touched.                                                              |
| touchInterfaceActiveColor | Color used for the touch interface when touched.                                                                  |
| accentGradientXStartColor | Starting color on the X gradient of accent colored 3D objects.                                                    |
| accentGradientXEndColor   | Ending color on the X gradient of accent colored 3D objects.                                                      |
| accentGradientYStartColor | Starting color on the Y gradient of accent colored 3D objects.                                                    |
| accentGradientYEndColor   | End color on the Y gradient of accent colored 3D objects.                                                         |

<a name="client-bindings"></a>

## Client Bindings

All keys expect hex color strings (i.e. `"#002b36"`) as their value. An additional two characters can be included to specify an alpha channel (useful for `cursorColor` and `bracketHighlightColor`).

The LambdaMod client bindings configuration file (`client-bindings.yml`) supports the following keys:

| Key                           |
|-------------------------------|
| add-children                  |
| add-keyframes                 |
| add-new                       |
| add-new-prefab                |
| add-numeric-input-0           |
| add-numeric-input-1           |
| add-numeric-input-2           |
| add-numeric-input-3           |
| add-numeric-input-4           |
| add-numeric-input-5           |
| add-numeric-input-6           |
| add-numeric-input-7           |
| add-numeric-input-8           |
| add-numeric-input-9           |
| add-text                      |
| attach                        |
| brake                         |
| camera-to-center-of-selection |
| cancel-tool                   |
| cancel-tracking-calibration   |
| close-context-menu            |
| close-dialog                  |
| close-editor                  |
| confirm-tracking-calibration  |
| copy                          |
| cursor-down                   |
| cursor-left                   |
| cursor-right                  |
| cursor-to                     |
| cursor-to-next-word           |
| cursor-to-previous-word       |
| cursor-to-selection           |
| cursor-up                     |
| cut                           |
| decrease-volume               |
| deselect-all                  |
| detach                        |
| dismiss-splash                |
| document-go-to-end            |
| document-go-to-start          |
| document-pan-left             |
| document-pan-right            |
| document-scroll-down          |
| document-scroll-up            |
| downcase-next-word            |
| drag                          |
| duplicate                     |
| end-add-select                |
| end-pan                       |
| end-remove-select             |
| end-replace-select            |
| end-show-vr-menu-left         |
| end-show-vr-menu-right        |
| end-snap                      |
| end-tool                      |
| ensure-numeric-input-period   |
| extrude                       |
| force-close-editor            |
| go-to-end                     |
| go-to-start                   |
| hide                          |
| hide-gamepad-menu             |
| hide-knowledge-viewer         |
| hide-selected                 |
| hide-unselected               |
| hide-vr-menu                  |
| increase-volume               |
| interact                      |
| invert-selection              |
| join                          |
| look-stick-motion             |
| mouse-motion                  |
| move-chest                    |
| move-cursor                   |
| move-gamepad-pointer          |
| move-head                     |
| move-interface-pointer        |
| move-left-elbow               |
| move-left-foot                |
| move-left-hand                |
| move-left-knee                |
| move-right-elbow              |
| move-right-foot               |
| move-right-hand               |
| move-right-knee               |
| move-select                   |
| move-stick-motion             |
| move-hips                     |
| move-window                   |
| next-history                  |
| next-tab                      |
| open-context-menu             |
| open-editor                   |
| ortho-invert                  |
| ortho-look-back               |
| ortho-look-down               |
| ortho-look-forward            |
| ortho-look-left               |
| ortho-look-right              |
| ortho-look-up                 |
| ortho-rotate-down             |
| ortho-rotate-left             |
| ortho-rotate-right            |
| ortho-rotate-up               |
| ortho-zoom-in                 |
| ortho-zoom-out                |
| page-down                     |
| page-up                       |
| pan                           |
| pan-left                      |
| pan-right                     |
| paste                         |
| paste-primary                 |
| ping                          |
| previous-history              |
| previous-tab                  |
| quit                          |
| redo                          |
| release                       |
| reload                        |
| remove-keyframes              |
| remove-link                   |
| remove-next-character         |
| remove-next-word              |
| remove-numeric-input          |
| remove-parent                 |
| remove-previous-character     |
| remove-previous-word          |
| remove-selected               |
| remove-to-end                 |
| remove-to-start               |
| reset                         |
| reset-angular-velocity        |
| reset-camera                  |
| reset-cursor                  |
| reset-linear-velocity         |
| reset-position                |
| reset-rotation                |
| reset-scale                   |
| return-to-menu                |
| rotate                        |
| save                          |
| scale                         |
| scroll-down                   |
| scroll-up                     |
| select-all                    |
| select-down                   |
| select-left                   |
| select-linked                 |
| select-right                  |
| select-to                     |
| select-to-next-word           |
| select-to-previous-word       |
| select-up                     |
| selection-to-cursor           |
| set-magnitude-0               |
| set-magnitude-1               |
| set-magnitude-2               |
| set-magnitude-3               |
| show-gamepad-menu             |
| show-knowledge-viewer         |
| split                         |
| start-pan                     |
| start-select                  |
| start-show-vr-menu-left       |
| start-show-vr-menu-right      |
| start-snap                    |
| store                         |
| swap-characters               |
| swap-words                    |
| text-select-all               |
| to-free-look                  |
| toggle-console                |
| toggle-console-mode           |
| toggle-document-manager       |
| toggle-edit-mode              |
| toggle-fullscreen             |
| toggle-help                   |
| toggle-hud                    |
| toggle-microphone             |
| toggle-new-text-tool          |
| toggle-numeric-input-sign     |
| toggle-oracle-manager         |
| toggle-orthographic           |
| toggle-pivot-mode             |
| toggle-play                   |
| toggle-quick-accessor         |
| toggle-settings               |
| toggle-side-panel             |
| toggle-user-controls-tool     |
| toggle-x                      |
| toggle-y                      |
| toggle-z                      |
| touch                         |
| translate                     |
| unbrake                       |
| undo                          |
| upcase-next-word              |
| use-tool                      |
| viewer-end-back               |
| viewer-end-down               |
| viewer-end-forward            |
| viewer-end-left               |
| viewer-end-right              |
| viewer-end-rotate             |
| viewer-end-shift              |
| viewer-end-sprint             |
| viewer-end-up                 |
| viewer-end-zoom               |
| viewer-start-back             |
| viewer-start-down             |
| viewer-start-forward          |
| viewer-start-left             |
| viewer-start-right            |
| viewer-start-rotate           |
| viewer-start-shift            |
| viewer-start-sprint           |
| viewer-start-up               |
| viewer-start-zoom             |
| vr-stick-motion               |
| vr-touchpad-motion            |

All keys expect a list of bindings as their value like so:

```yaml
[
    { type: input-x },
    { type: input-delete }
]
```

Each binding can have a number of keys to customize it further, the full form is as follows:

```yaml
{
    type: input-w,
    up: false,
    shift: false,
    control: false,
    alt: false,
    super: false,
    ignoreModifiers: false,
    repeatCount: 0
}
```

A value for `repeatCount` greater than 1 ignores an input until it has been repeated this many times in a short space of time. This is useful for binding actions like double click. The `up` setting delays the input until it is released e.g. the user removes their finger from the key. The `ignoreModifiers` setting causes an input to apply regardless of what modifiers keys are pressed, for example if an input is set to `{ type: input-x, ignoreModifiers: true }` then pressing X and left shift at the same time will still trigger the input. This is useful for movement keys, where you might wish to press W to go forward at the same time as pressing left control to descend.

Valid names for inputs are the same as listed in the [Special Symbols](scripting.md#special-symbols) section of the scripting guide.

<a name="client-localization"></a>

## Client Localization

The LambdaMod client string localization file (`client-localization.yml`) supports too many keys to list in this guide. A complete list of them can be found in the project's source code at `src/client/localization.cpp`. Each key expects to be mapped to the text it should produce such as `vec2-edit-x: "X"`. Some strings will have values such as numbers or entity names inserted into them during localization, this can be supported by writing `%1`, `%2`, etc. where the values should be inserted.
