# TLS Setup

By default network traffic in LambdaMod is encrypted and unreadable to casual observers. However as discussed in the [security guide](security.md#network-security), a determined attacker could still man-in-the-middle attack the connection. This guide explains how to prevent this.

## Contents

1. [Rationale](#rationale)
2. [Creating a CA](#creating-a-ca)
3. [Creating a Server Certificate](#creating-a-server-certificate)

<a name="rationale"></a>

## Rationale

By default a LambdaMod client has no way to verify that the server it's connecting to is in fact the intended one and not an impostor. A determined attacker with the ability to modify network traffic could accept the initial connection from the client and make its own connection to the real server, then begin passing traffic back and forth between the two real parties, reading or altering packets at will. This is an unlikely scenario but it is possible.

<a name="creating-a-ca"></a>

## Creating a CA

To prevent this you can generate your own encryption certificates and distribute them to your players before the game. To start with you need a certificate authority. This is a 3rd party (in theory, in reality it's just you again) with a certificate, trusted by default, which verifies the other certificates. Enter the following command into a shell of your choice with the command line OpenSSL utility installed:

**Note:** The second command will ask for a variety of details like your location and company name. These are all optional **except for Common Name, which must be filled out, I recommend entering "ca". Challenge password must also be left blank.**

```sh
openssl genrsa -out "ca.key" 4096
openssl req -new -key "ca.key" -out ca.csr
openssl x509 -req -days 3650 -in ca.csr -signkey ca.key -out ca.crt
```

This will create two files you need to keep, `ca.key` and `ca.crt`. Keep `ca.key` private but send `ca.crt` to your players and have them modify their client configurations so that `caFilePath` points to wherever they have placed `ca.crt` on their device. You can delete `ca.csr`, it's a temporary certificate signing request file.

<a name="creating-a-server-certificate"></a>

## Creating a Server Certificate

Now you need to create a certificate for your actual server. Enter the following commands:

**Note:** The second command will ask for a variety of details like your location and company name. These are all optional **except for Common Name, which must be filled out, I recommend entering "server". Challenge password must also be left blank.**

```sh
openssl genrsa -out server.key 4096
openssl req -new -key server.key -out server.csr
openssl x509 -req -days 3650 -in server.key -CA ca.crt -CAkey ca.key -out server.crt
```

Again this will create two files you need to keep, `server.key` and `server.crt`. Update your LambdaMod server configuration to have `keyFilePath` contain the path to `server.key` and `certFilePath` contain the path to `server.crt`. You don't need to distribute either of these files to anyone else (although you can distribute `server.crt` safely, it contains no secret information and is sent over the network during the initial TLS handshake anyway). You can delete `server.csr`, it's a temporary certificate signing request file.

Also, both `.crt` files will eventually expire, as encryption certificates are designed to do. I've set the lifespan in these examples to be ten years however so it's more likely you'll lose the files first. If they do expire you can just re-run the last two commands in each block again to regenerate them.
