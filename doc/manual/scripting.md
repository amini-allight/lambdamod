# Scripting

Welcome to the LambdaMod scripting manual. LambdaMod has an integrated scripting language inspired by Lisp. This manual describes how to use it.

## Contents

1. [Language](#language)
    1. [Keywords](#keywords)
    2. [Standard Library](#standard-library)
    3. [Game Library](#game-library)
    4. [Content Library](#content-library)
    5. [Constants](#constants)
2. [Special Symbols](#special-symbols)
3. [Entity Scripting](#entity-scripting)
4. [Timers](#timers)
5. [Repeaters](#repeaters)

<a name="language"></a>

## Language

LambdaMod has an embedded Lisp-style language that is used in the creation of content. The language uses a minimal syntax with only four reserved symbols: (, ), ' and ". There are no comments as the system stores supplied code as trees of objects rather than as a string. The syntax should be familiar to anyone who has used a Lisp before:

```
> (+ 2 3)
5
> (+ (+ 1 1) 3)
5
```

It supports numeric literals:

```
> 3
3
```

Floating point literals:

```
> 3.1
3.1
```

Strings:

```
> "Hello world!"
"Hello world!"
```

Lists, both evaluated and not:

```
> (list 1 2 (+ 1 2))
(1 2 3)
> '(1 2 (+ 1 2))
(1 2 (+ 1 2))
```

Lambdas:

```
> (lambda (x y) (+ x y))
(lambda (x y) (+ x y))
```

Binding:

```
> (let ((x 2)) '(+ x 2))
(+ 2 2)
```

Application:

```
> ((lambda (x y) (+ x y)) 2 3)
5
```

Defining functions:

```
> (define (add x y) (+ x y))
> (add 2 3)
5
```

Defining variables:

```
> (define x 4)
> x
4
```

Referring to them by symbol without evaluation:

```
> 'x
x
```

Updating them later:

```
> (let ((y 2)) (set! x (* y 2)))
> x
4
```

Removing them when they are no longer needed:

```
> (undefine x)
```

And even defining syntax rules, for example to copy the inbuilt definition function's lambda form:

```
> (define-syntax (my-define (name args...) body) (define name (lambda (args...) body)))
> (my-define (add x y) (+ x y))
> (add 2 3)
5
```

<a name="keywords"></a>

### Keywords

A set of keywords providing fundamental language functionality is provided:

| Name              | Format                                     | Behavior                                                                                                                                                                                 |
|-------------------|--------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `define`          | `(define name\|(name [args...]) body...)`  | Creates a new definition. If brackets are used around the name and any argument names the resulting definition will have a lambda value, otherwise `body` will be evaluated immediately. |
| `undefine`        | `(undefine name)`                          | Removes an existing definition.                                                                                                                                                          |
| `define-restrict` | `(define-restrict name [state])`           | Makes a definition's contents public when `state` is 0, otherwise makes it public. If the second argument is omitted it returns the current restriction state of the definition.         |
| `define-control`  | `(define-control name [user-id])`          | Sets the owner of a define to the specified user, or no user if `user-id` is `no-user`/0. If the second argument is omitted it returns the current owner.                                |
| `define-find`     | `(define-find pattern)`                    | Returns a list of definitions containing the pattern in their name.                                                                                                                      |
| `define-list`     | `(define-list)`                            | Returns a list of all definitions.                                                                                                                                                       |
| `define-exists?`  | `(define-exists? name)`                    | Returns 1 if a definition with the given name exists.                                                                                                                                    |
| `define-syntax`   | `(define-syntax (name [args...]) body)`    | Creates a new definition. The definition will be a syntax lambda.                                                                                                                        |
| `get`             | `(get object index)`                       | Returns the value of an indexed member in a list or string.                                                                                                                              |
| `set`             | `(set object index value)`                 | Returns a list or string with the member at `index` replaced with `value`.                                                                                                               |
| `size`            | `(size object)`                            | Returns the length of a list or string.                                                                                                                                                  |
| `join`            | `(join first rest...)`                     | Returns lists, strings or symbols concatenated.                                                                                                                                          |
| `set!`            | `(set! name value)`                        | Changes the value of a previous definition.                                                                                                                                              |
| `quote`           | `(quote value)`                            | Returns the value without evaluating it.                                                                                                                                                 |
| `list`            | `(list values...)`                         | Returns a list value.                                                                                                                                                                    |
| `eval`            | `(eval value)`                             | Evaluates the value and returns the result.                                                                                                                                              |
| `lambda`          | `(lambda ([args...]) body...)`             | Returns a lambda value.                                                                                                                                                                  |
| `syntax`          | `(syntax ([args...]) body)`                | Returns a syntax lambda value.                                                                                                                                                           |
| `throw`           | `(throw value)`                            | Throws an exception with the supplied value, ending execution.                                                                                                                           |
| `catch`           | `(catch callback body...)`                 | Catches an exception thrown by `throw` and passes its value to `callback`, returning the result of callback. Otherwise returns the result of the last expression in `body`.              |
| `print`           | `(print value [values...])`                | Prints the provided values.                                                                                                                                                              |
| `type`            | `(type value)`                             | Returns a number indicating the type of the value. Possible values are below.                                                                                                            |
| `->`              | `(-> ([args...]) body...)`                 | Shorter form of `lambda` with the same behavior.                                                                                                                                         |

Possible values for the number returned from `type`:

| Value | Meaning |
|-------|---------|
| 0     | Void    |
| 1     | Symbol  |
| 2     | Integer |
| 3     | Float   |
| 4     | String  |
| 5     | List    |
| 6     | Lambda  |
| 7     | Handle  |

<a name="standard-library"></a>

### Standard Library

**Note:** Square brackets around an argument indicate that it is optional. An ellipsis indicates that any number of arguments can be supplied. A vertical bar indicates a choice between multiple different argument forms.

A standard library implementing common math and control flow functions is also available:

| Name                        | Format                                                             | Behavior                                                                                                                                                 |
|-----------------------------|--------------------------------------------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| `++`                        | `(++ a)`                                                           | Increment.                                                                                                                                               |
| `--`                        | `(-- a)`                                                           | Decrement.                                                                                                                                               |
| `+`                         | `(+ a [values...])`                                                | Does nothing when given one argument or adds multiple.                                                                                                   |
| `-`                         | `(- a [values...])`                                                | Flips sign when given one argument or subtracts multiple.                                                                                                |
| `*`                         | `(* a [values...])`                                                | Multiplication.                                                                                                                                          |
| `/`                         | `(/ a [values...])`                                                | Division.                                                                                                                                                |
| `%`                         | `(% a b [values...])`                                              | Modulo. Always returns a positive remainder and supports floating point numbers as input. Floating point modulo is truncation-based.                     |
| `!`                         | `(! a)`                                                            | Logical NOT.                                                                                                                                             |
| `&&`                        | `(&& a [values...])`                                               | Logical AND.                                                                                                                                             |
| `\|\|`                      | `(\|\| a [values...])`                                             | Logical OR.                                                                                                                                              |
| `^^`                        | `(^^ a [values...])`                                               | Logical XOR.                                                                                                                                             |
| `~`                         | `(~ a)`                                                            | Bitwise NOT.                                                                                                                                             |
| `&`                         | `(& a [values...])`                                                | Bitwise AND.                                                                                                                                             |
| `\|`                        | `(\| a [values...])`                                               | Bitwise OR.                                                                                                                                              |
| `^`                         | `(^ a [values...])`                                                | Bitwise XOR.                                                                                                                                             |
| `>>`                        | `(>> a b)`                                                         | Bit shift right.                                                                                                                                         |
| `<<`                        | `(<< a b)`                                                         | Bit shift left.                                                                                                                                          |
| `==`                        | `(== a b)`                                                         | Equal to.                                                                                                                                                |
| `!=`                        | `(!= a b)`                                                         | Not equal to.                                                                                                                                            |
| `>`                         | `(> a b)`                                                          | Greater than.                                                                                                                                            |
| `<`                         | `(< a b)`                                                          | Less than.                                                                                                                                               |
| `>=`                        | `(>= a b)`                                                         | Greater than or equal to.                                                                                                                                |
| `<=`                        | `(<= a b)`                                                         | Less than or equal to.                                                                                                                                   |
| `<=>`                       | `(<=> a b)`                                                        | Three-way comparison. Returns -1 if less than, 0 if equal and 1 if greater than.                                                                         |
| `**`                        | `(** a exponent)`                                                  | Exponent.                                                                                                                                                |
| `//`                        | `(// a root)`                                                      | Root.                                                                                                                                                    |
| `modulo`                    | `(modulo a b [values...])`                                         | Same as `%`.                                                                                                                                             |
| `not`                       | `(not a)`                                                          | Same as `!`.                                                                                                                                             |
| `and`                       | `(and a [values...])`                                              | Same as `&&`.                                                                                                                                            |
| `or`                        | `(or a [values..])`                                                | Same as `||`.                                                                                                                                            |
| `xor`                       | `(xor a [values...])`                                              | Same as `^^`.                                                                                                                                            |
| `equal?`                    | `(equal? a b)`                                                     | Same as `==`.                                                                                                                                            |
| `not-equal?`                | `(not-equal? a b)`                                                 | Same as `!=`.                                                                                                                                            |
| `expt`                      | `(expt a exponent)`                                                | Same as `**`.                                                                                                                                            |
| `root`                      | `(// a root)`                                                      | Same as `//`.                                                                                                                                            |
| `sq`                        | `(sq a)`                                                           | Returns the square of `a`.                                                                                                                               |
| `sqrt`                      | `(sqrt a)`                                                         | Returns the square root of `a`.                                                                                                                          |
| `let`                       | `(let ([(name expr)...]) exprs...)`                                | Executes an expression with any number of local definitions.                                                                                             |
| `begin`                     | `(begin exprs...)`                                                 | Executes a series of expressions in order.                                                                                                               |
| `if`                        | `(if cond on-true on-false)`                                       | Executes one of two expressions based on the truth of a condition.                                                                                       |
| `when`                      | `(when cond exprs...)`                                             | Executes an expression if a condition is true.                                                                                                           |
| `switch`                    | `(switch cond [(key exprs...)...] default)`                        | Executes any expressions where the value of `key` matches that of `cond`, otherwise executes `default`.                                                  |
| `condition`                 | `(condition (cond exprs...)... else)`                              | Executes the first expression whose condition is true, otherwise executes `else`.                                                                        |
| `for`                       | `(for ([(name expr)...]) cond ([(name iter-expr)...]) exprs...)`   | Adds the local definitions defined in the first argument, then iterates until `cond` is false, executing `expr` then each `iter-expr` on each iteration. |
| `for-each`                  | `(for-each (name list) exprs...)`                                  | Iterates over every object in a list, executing an expression with the current element available as `name`.                                              |
| `while`                     | `(while cond exprs...)`                                            | Executes an expression until `cond` is false.                                                                                                            |
| `do`                        | `(do cond exprs...)`                                               | Executes an expression once, then repeatedly until `cond` is false.                                                                                      |
| `zero?`                     | `(zero? a)`                                                        | Returns 1 if `a` is 0.                                                                                                                                   |
| `positive?`                 | `(positive? a)`                                                    | Returns 1 if `a` is greater than 0.                                                                                                                      |
| `negative?`                 | `(negative? a)`                                                    | Returns 1 if `a` is less than 0.                                                                                                                         |
| `sin`                       | `(sin a)`                                                          | Sin.                                                                                                                                                     |
| `cos`                       | `(cos a)`                                                          | Cos.                                                                                                                                                     |
| `tan`                       | `(tan a)`                                                          | Tan.                                                                                                                                                     |
| `asin`                      | `(asin a)`                                                         | Arcsin.                                                                                                                                                  |
| `acos`                      | `(acos a)`                                                         | Arccos.                                                                                                                                                  |
| `atan`                      | `(atan a)`                                                         | Arctan.                                                                                                                                                  |
| `sinh`                      | `(sinh a)`                                                         | Hyperbolic sin.                                                                                                                                          |
| `cosh`                      | `(cosh a)`                                                         | Hyperbolic cos.                                                                                                                                          |
| `tanh`                      | `(tanh a)`                                                         | Hyperbolic tan.                                                                                                                                          |
| `asinh`                     | `(asinh a)`                                                        | Hyperbolic arcsin.                                                                                                                                       |
| `acosh`                     | `(acosh a)`                                                        | Hyperbolic arccos.                                                                                                                                       |
| `atanh`                     | `(atanh a)`                                                        | Hyperbolic arctan.                                                                                                                                       |
| `log`                       | `(log a base)`                                                     | Log.                                                                                                                                                     |
| `abs`                       | `(abs a)`                                                          | Absolute value.                                                                                                                                          |
| `ceil`                      | `(ceil a)`                                                         | Round floating point number up.                                                                                                                          |
| `floor`                     | `(floor a)`                                                        | Round floating point number down.                                                                                                                        |
| `trunc`                     | `(trunc a)`                                                        | Removes the fractional part of a floating point number.                                                                                                  |
| `fractional`                | `(fractional a)`                                                   | Returns only the fractional part of a floating point number.                                                                                             |
| `round`                     | `(round a)`                                                        | Round floating point number to the nearest integer.                                                                                                      |
| `sign`                      | `(sign a)`                                                         | Returns the sign of a number, either -1 or +1. 0 is considered positive.                                                                                 |
| `min`                       | `(min a b [values...])`                                            | Returns the smallest of two or more numbers.                                                                                                             |
| `max`                       | `(max a b [values...])`                                            | Returns the largest of two or more numbers.                                                                                                              |
| `clamp`                     | `(clamp a min max)`                                                | Returns the value `a` clamped to a minimum of `min` and a maximum of `max`.                                                                              |
| `void?`                     | `(void? a)`                                                        | Returns 1 if `a` is a void value.                                                                                                                        |
| `symbol?`                   | `(symbol? a)`                                                      | Returns 1 if `a` is a symbol value.                                                                                                                      |
| `int?`                      | `(int? a)`                                                         | Returns 1 if `a` is a int value.                                                                                                                         |
| `float?`                    | `(float? a)`                                                       | Returns 1 if `a` is a float value.                                                                                                                       |
| `string?`                   | `(string? a)`                                                      | Returns 1 if `a` is a string value.                                                                                                                      |
| `list?`                     | `(list? a)`                                                        | Returns 1 if `a` is a list value.                                                                                                                        |
| `lambda?`                   | `(lambda? a)`                                                      | Returns 1 if `a` is a lambda value.                                                                                                                      |
| `handle?`                   | `(handle? a)`                                                      | Returns 1 if `a` is a handle value.                                                                                                                      |
| `string->integer`           | `(string->integer a)`                                              | Converts a string to an integer.                                                                                                                         |
| `string->float`             | `(string->float a)`                                                | Converts a string to a floating point value.                                                                                                             |
| `string->symbol`            | `(string->symbol a)`                                               | Converts a string to a symbol.                                                                                                                           |
| `integer->float`            | `(integer->float a)`                                               | Converts a integer to a floating point value.                                                                                                            |
| `integer->string`           | `(integer->string a)`                                              | Converts a integer to a string.                                                                                                                          |
| `float->integer`            | `(float->integer a)`                                               | Converts a floating point value to an integer.                                                                                                           |
| `float->string`             | `(float->string a)`                                                | Converts a floating point value to a string.                                                                                                             |
| `symbol->string`            | `(symbol->string a)`                                               | Converts a symbol to a string.                                                                                                                           |
| `handle`                    | `(handle id)`                                                      | Creates a handle from a raw entity ID. Mostly used internally to make saving to disk work.                                                               |
| `nullptr?`                  | `(nullptr? handle)`                                                | Returns 1 if a handle is invalid.                                                                                                                        |
| `has?`                      | `(has? handle name)`                                               | Returns 1 if the entity has a field with the given name.                                                                                                 |
| `exec`                      | `(exec handle name [args...])`                                     | Calls a function on the entity referred to by a handle.                                                                                                  |
| `read`                      | `(read handle name)`                                               | Returns the value of a field on the entity.                                                                                                              |
| `write`                     | `(write handle name value)`                                        | Sets the value of a field on the entity.                                                                                                                 |
| `remove`                    | `(remove handle name)`                                             | Removes a field from an entity.                                                                                                                          |
| `apply`                     | `(apply function arguments)`                                       | Executes the functions `function` with the values in the list `arguments` as its arguments.                                                              |
| `compose`                   | `(compose functions...)`                                           | Returns a new lambda that executes each lambda on the result of the previous lambda, starting with the last and ending with the first.                   |
| `first`                     | `(first values)`                                                   | Returns the first element of a list.                                                                                                                     |
| `last`                      | `(last values)`                                                    | Returns the last element of a list.                                                                                                                      |
| `empty?`                    | `(empty? values)`                                                  | Returns 1 if the list is empty.                                                                                                                          |
| `insert`                    | `(insert values index new-values)`                                 | Inserts the values in the second list at the `index` in the first list and returns the new list.                                                         |
| `erase`                     | `(erase values index count)`                                       | Removes `count` values at `index` in the list and returns the new list.                                                                                  |
| `contains?`                 | `(contains? values value)`                                         | Returns 1 if any of the elements in `values` is equal to `value`.                                                                                        |
| `index`                     | `(index values value)`                                             | Returns the index at which `value` occurs, or -1 if it is not present.                                                                                   |
| `replace`                   | `(replace values old new)`                                         | Returns a new list with every occurrence of `old` replaced with `new`.                                                                                   |
| `slice`                     | `(slice s start count)`                                            | Returns the section of the list starting at index `start` and running for `count` elements.                                                              |
| `zip`                       | `(zip first lists...)`                                             | Returns a list of lists, each containing all the elements from the input lists at a given index.                                                         |
| `push-first`                | `(push-first values value)`                                        | Adds `value` to the start of the list and returns the new list.                                                                                          |
| `push-last`                 | `(push-last values value)`                                         | Adds `value` to the end of the list and returns the new list.                                                                                            |
| `pop-first`                 | `(pop-first values)`                                               | Removes the first value from the list and returns the new list.                                                                                          |
| `pop-last`                  | `(pop-last values)`                                                | Removes the last value from the list and returns the new list.                                                                                           |
| `any-of?`                   | `(any-of? values lambda)`                                          | Returns 1 if the lambda returns true for any of the supplied values.                                                                                     |
| `all-of?`                   | `(all-of? values lambda)`                                          | Returns 1 if the lambda returns true for all of the supplied values.                                                                                     |
| `none-of?`                  | `(none-of? values lambda)`                                         | Returns 1 if the lambda returns true for none of the supplied values.                                                                                    |
| `sum-of`                    | `(sum-of values lambda)`                                           | Returns the sum of the lambda's return values for each of the supplied values.                                                                           |
| `min-of`                    | `(min-of values lambda)`                                           | Returns the value for which the lambda returned the lowest number.                                                                                       |
| `max-of`                    | `(max-of values lambda)`                                           | Returns the value for which the lambda returned the highest number.                                                                                      |
| `select`                    | `(select values lambda)`                                           | Returns a list of all values for which the lambda returns true.                                                                                          |
| `sort`                      | `(sort values lambda)`                                             | Returns the list sorted with comparisons done by the lambda.                                                                                             |
| `shuffle`                   | `(shuffle values random-engine)`                                   | Returns the list randomly shuffled using the supplied random engine.                                                                                     |
| `choose`                    | `(choose values random-engine)`                                    | Returns one value from the list chosen at random using the supplied random engine.                                                                       |
| `reverse`                   | `(reverse values)`                                                 | Returns the list reversed.                                                                                                                               |
| `generate`                  | `(generate count lambda)`                                          | Executes the lambda `count` times and returns a list of the results.                                                                                     |
| `map`                       | `(map values lambda)`                                              | Returns a list of the lambda's return values for each of the supplied values.                                                                            |
| `reduce`                    | `(reduce values initial lambda)`                                   | Passes a value to each execution of the lambda and then replaces the value with the return value of the lambda. Returns the final return value.          |
| `count`                     | `(count values lambda)`                                            | Returns the number of items the lambda returned true for a supplied value.                                                                               |
| `union`                     | `(union a b [values...])`                                          | Returns a list containing each unique value in the supplied lists.                                                                                       |
| `difference`                | `(difference a b [values...])`                                     | Returns a list containing each value not found in the all the supplied lists.                                                                            |
| `intersection`              | `(intersection a b [values...])`                                   | Returns a list containing each value found in the all supplied lists.                                                                                    |
| `range`                     | `(range start end)`                                                | Returns a list containing all the integers between `start` and `end`, exclusive.                                                                         |
| `inrange`                   | `(inrange start end)`                                              | Returns a list containing all the integers between `start` and `end`, inclusive.                                                                         |
| `string-empty?`             | `(string-empty? s)`                                                | Returns 1 if the string is empty.                                                                                                                        |
| `string-insert`             | `(string-insert s index new)`                                      | Inserts the `new` string into the string at `index and returns the new string.                                                                           |
| `string-erase`              | `(string-erase s index count)`                                     | Removes `count` characters from the string at `index` and returns the new string.                                                                        |
| `string-format`             | `(string-format s [values...])`                                    | Substitutes values into a template string. Use `%s` for strings, `%f` for floats and `%i` for integers.                                                  |
| `string-starts-with?`       | `(string-starts-with? s prefix)`                                   | Returns 1 if the string begins with `prefix`.                                                                                                            |
| `string-ends-with?`         | `(string-ends-with? s suffix)`                                     | Returns 1 if the string ends with `prefix`.                                                                                                              |
| `string-contains?`          | `(string-contains? s element)`                                     | Returns 1 if the string contains `element`.                                                                                                              |
| `string-index`              | `(string-index s element)`                                         | Returns the index at which `element` occurs, or -1 if it is not present.                                                                                 |
| `string-replace`            | `(string-replace s old new)`                                       | Returns the string with `old` replaced with `new`.                                                                                                       |
| `string-slice`              | `(string-slice s start count)`                                     | Returns the section of the string starting at index `start` and running for `count` characters.                                                          |
| `string-upcase`             | `(string-upcase s)`                                                | Returns the string with all lowercase characters replaced with their uppercase versions.                                                                 |
| `string-downcase`           | `(string-downcase s)`                                              | Returns the string with all uppercase characters replaced with their lowercase versions.                                                                 |
| `random-engine`             | `(random-engine)`                                                  | Returns a random number from the random engine.                                                                                                          |
| `random-linear`             | `(random-linear start end random-engine)`                          | Returns a random number between start and end inclusive using the supplied random engine.                                                                |
| `random-gauss`              | `(random-gauss mean stddev random-engine)`                         | Returns a random number with a mean of `mean` and a standard deviation of `stddev` using the supplied random engine.                                     |
| `degrees->radians`          | `(degrees->radians degrees)`                                       | Returns the number of degrees expressed in radians.                                                                                                      |
| `radians->degrees`          | `(radians->degrees radians)`                                       | Returns the number of radians expressed in degrees.                                                                                                      |
| `struct-define`             | `(struct-define name [field-names...])`                            | Defines a constructor and accessor functions for a struct with the given field names.                                                                    |
| `struct-undefine`           | `(struct-undefine name)`                                           | Undefines the definition `name` and all definitions beginning with `name`-.                                                                              |
| `table-set`                 | `(table-set table key value)`                                      | Takes a table (a list of key-value pairs) and replaces the value in the pair with key `key, adds a new pair otherwise. Returns the new table.            |
| `table-insert`              | `(table-insert table key value)`                                   | Takes a table (a list of key-value pairs) and adds a new pair `(key value)` if no pair with key `key` already exists. Returns the new table.             |
| `table-assign`              | `(table-assign table key value)`                                   | Takes a table (a list of key-value pairs) and replaces the value in the pair with key `key` if it exists. Returns the new table.                         |
| `table-erase`               | `(table-erase table key)`                                         | Takes a table (a list of key-value pairs) and removes the pair with key `key`, if one exists. Returns the new table.                                     |
| `table-has?`                | `(table-has? table key)`                                           | Returns 1 if the table contains a pair with first item `key`.                                                                                            |
| `table-get`                 | `(table-get table key)`                                            | Returns the second item in the pair with first item `key` if it exists, errors otherwise.                                                                |
| `set-insert`                | `(set-insert set value)`                                           | Treats a list as a set and inserts the given value only if it is not already present and returns the new list.                                           |
| `set-erase`                 | `(set-erase set value)`                                           | Removes the item from the list if it exists and returns the new list.                                                                                    |
| `set-has?`                  | `(set-has? set value)`                                             | Returns 1 if the list contains the supplied value.                                                                                                       |
| `vec2`                      | `(vec2 [v\|x y])`                                                  | Returns a list of two floats.                                                                                                                            |
| `vec2?`                     | `(vec2? a)`                                                        | Returns 1 if `a` is a list of two integers or floats.                                                                                                    |
| `vec2+`                     | `(vec2+ a b [values...])`                                          | Adds 2-component vectors.                                                                                                                                |
| `vec2-`                     | `(vec2- a b [values...])`                                          | Subtracts 2-component vectors.                                                                                                                           |
| `vec2*`                     | `(vec2* a b [values...])`                                          | Multiplies 2-component vectors.                                                                                                                          |
| `vec2/`                     | `(vec2/ a b [values...])`                                          | Divides 2-component vectors.                                                                                                                             |
| `vec2-dot`                  | `(vec2-dot a b)`                                                   | Returns the dot product of two 2-component vectors.                                                                                                      |
| `vec2-flip`                 | `(vec2-flip a)`                                                    | Returns the vector with the X and Y components swapped.                                                                                                  |
| `vec2-distance`             | `(vec2-distance a b)`                                              | Returns the distance between two 2-component vectors.                                                                                                    |
| `vec2-length`               | `(vec2-length a)`                                                  | Returns the length of a 2-component vector.                                                                                                              |
| `vec2-normalize`            | `(vec2-normalize a)`                                               | Returns a normalized vector in the same direction as a 2-component vector.                                                                               |
| `vec3`                      | `(vec3 [v\|x y z])`                                                | Returns a list of three floats.                                                                                                                          |
| `vec3?`                     | `(vec3? a)`                                                        | Returns 1 if `a` is a list of three integers or floats.                                                                                                  |
| `vec3+`                     | `(vec3+ a b [values...])`                                          | Adds 3-component vectors.                                                                                                                                |
| `vec3-`                     | `(vec3- a b [values...])`                                          | Subtracts 3-component vectors.                                                                                                                           |
| `vec3*`                     | `(vec3* a b [values...])`                                          | Multiplies 3-component vectors.                                                                                                                          |
| `vec3/`                     | `(vec3/ a b [values...])`                                          | Divides 3-component vectors.                                                                                                                             |
| `vec3-dot`                  | `(vec3-dot a b)`                                                   | Returns the dot product of two 3-component vectors.                                                                                                      |
| `vec3-cross`                | `(vec3-cross a b)`                                                 | Returns the cross product of two 3-component vectors.                                                                                                    |
| `vec3-distance`             | `(vec3-distance a b)`                                              | Returns the distance between two 3-component vectors.                                                                                                    |
| `vec3-length`               | `(vec3-length a)`                                                  | Returns the length of a 3-component vector.                                                                                                              |
| `vec3-normalize`            | `(vec3-normalize a)`                                               | Returns a normalized vector in the same direction as a 3-component vector.                                                                               |
| `quaternion`                | `(quaternion [w x y z])`                                           | Returns a list of four floats.                                                                                                                           |
| `quaternion?`               | `(quaternion? a)`                                                  | Returns 1 if `a` is a list of four integers or floats.                                                                                                   |
| `quaterion-from-direction`  | `(quaternion-from-direction direction roll)`                       | Returns a list of four floats representing a quaternion generated from the given forward and up directions.                                              |
| `quaterion-from-axis-angle` | `(quaternion-from-axis-angle axis angle)`                          | Returns a list of four floats representing a quaternion generated from the given axis and angle.                                                         |
| `quaternion*`               | `(quaternion* a b [values...])`                                    | Multiplies quaternions.                                                                                                                                  |
| `quaternion/`               | `(quaternion/ a b [values...])`                                    | Divides quaternions.                                                                                                                                     |
| `quaternion-inverse`        | `(quaternion-inverse a)`                                           | Returns the inverse of a quaternion.                                                                                                                     |
| `quaternion-rotate`         | `(quaternion-rotate q v)`                                          | Returns the point rotated about the origin by the quaternion.                                                                                            |
| `quaternion-forward`        | `(quaternion-forward a)`                                           | Returns a normalized vector in the forward direction of the quaternion.                                                                                  |
| `quaternion-back`           | `(quaternion-back a)`                                              | Returns a normalized vector in the backward direction of the quaternion.                                                                                 |
| `quaternion-left`           | `(quaternion-left a)`                                              | Returns a normalized vector in the left direction of the quaternion.                                                                                     |
| `quaternion-right`          | `(quaternion-right a)`                                             | Returns a normalized vector in the right direction of the quaternion.                                                                                    |
| `quaternion-up`             | `(quaternion-up a)`                                                | Returns a normalized vector in the up direction of the quaternion.                                                                                       |
| `quaternion-down`           | `(quaternion-down a)`                                              | Returns a normalized vector in the down direction of the quaternion.                                                                                     |
| `quaternion-axis`           | `(quaternion-axis a)`                                              | Returns the axis component of the quaternion's rotation.                                                                                                 |
| `quaternion-angle`          | `(quaternion-angle a)`                                             | Returns the angle component of the quaternion's rotation.                                                                                                |
| `quaternion-length`         | `(quaternion-length a)`                                            | Returns the length of the quaternion.                                                                                                                    |
| `quaternion-normalize`      | `(quaternion-normalize a)`                                         | Returns a normalized version of the quaternion.                                                                                                          |
| `screen-project`            | `(screen-project position rotation angle aspect near far point)`   | Returns the point converted to normalized device coordinates in the view frustum.                                                                        |
| `screen-unproject`          | `(screen-unproject position rotation angle aspect near far point)` | Returns the point in world space.                                                                                                                        |
| `timer-start`               | `(timer-start delay callback)`                                     | Executes the given callback after a delay of `delay` seconds. Returns an id identifying the timer created.                                               |
| `timer-stop`                | `(timer-stop id)`                                                  | Stops an existing timer from firing. Timers are updated serially on the main thread so this does not create a race condition.                            |
| `timer-all`                 | `(timer-all)`                                                      | Returns a list of all active timer IDs.                                                                                                                  |
| `repeater-start`            | `(repeater-start callback)`                                        | Executes the given callback on every step. Returns an id identifying the repeater created.                                                               |
| `repeater-stop`             | `(repeater-stop id)`                                               | Stops an existing repeater from firing. Repeaters are updated serially on the main thread so this does not create a race condition.                      |
| `repeater-all`              | `(repeater-all)`                                                   | Returns a list of all active repeater IDs.                                                                                                               |

<a name="game-library"></a>

### Game Library

**Note:** Square brackets around an argument indicate that it is optional. An ellipsis indicates that any number of arguments can be supplied. A vertical bar indicates a choice between multiple different argument forms.

These functions provide access to fundamental engine features.

| Name                       | Format                                                                  | Behavior                                                                                                                                                              |
|----------------------------|-------------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `time-multiplier`          | `(time-multiplier [multiplier])`                                        | Gets the time multiplier or sets it to the given value, changing the rate at which time progresses in the game world.                                                 |
| `checkpoint-all`           | `(checkpoint-all)`                                                      | Returns a list of symbols containing the names of all the existing checkpoints.                                                                                       |
| `checkpoint-add`           | `(checkpoint-add name)`                                                 | Adds a new checkpoint based on the current state of the world with the given symbol as its name.                                                                      |
| `checkpoint-remove`        | `(checkpoint-remove name)`                                              | Removes the checkpoint with the given name.                                                                                                                           |
| `checkpoint-revert`        | `(checkpoint-revert name)`                                              | Reverts to the specified checkpoint, resetting the world state and deleting all checkpoints created between the present and the target checkpoint.                    |
| `world-all`                | `(world-all)`                                                           | Returns a list of symbols containing the names of all the existing worlds.                                                                                            |
| `world-add`                | `(world-add world-name)`                                                | Adds a new world with the given symbol as its name.                                                                                                                   |
| `world-remove`             | `(world-remove world-name)`                                             | Removes the world with the given name.                                                                                                                                |
| `world-speed-of-sound`     | `(world-speed-of-sound world-name [speed])`                             | Gets the speed of sound in the specified world or sets it.                                                                                                            |
| `world-gravity`            | `(world-gravity world-name [acceleration])`                             | Gets the acceleration due to gravity in the specified world or sets it.                                                                                               |
| `world-flow-velocity`      | `(world-flow-velocity world-name [velocity])`                           | Gets the flow velocity of the medium in the specified world or sets it.                                                                                               |
| `world-density`            | `(world-density world-name [density])`                                  | Gets the density of the medium in the specified world or sets it.                                                                                                     |
| `world-up`                 | `(world-up world-name [direction])`                                     | Gets the up direction in the specified world or sets it.                                                                                                              |
| `world-show`               | `(world-show world-name)`                                               | Enables the world being visible to non-admin players on the world list.                                                                                               |
| `world-hide`               | `(world-hide world-name)`                                               | Disables the world being visible to non-admin players on the world list.                                                                                              |
| `world-shown`              | `(world-shown world-name state)`                                        | Sets whether or not the world is visible to non-admin players on the world list.                                                                                      |
| `world-shown?`             | `(world-shown? world-name)`                                             | Returns 1 if the world is shown to non-admin players on the world list.                                                                                               |
| `world-fog-distance`       | `(world-fog-distance world-name [distance])`                            | Gets the distance to total fog opacity in the specific world or sets it.                                                                                              |
| `world-entity-find`        | `(world-entity-find world-name pattern)`                                | Get all entities in the world whose names contain `pattern`.                                                                                                          |
| `world-entity-all`         | `(world-entity-all world-name)`                                         | Returns a list containing all entities in the world.                                                                                                                  |
| `world-entity-near`        | `(world-entity-near world-name position range)`                         | Returns a list containing all entities in the world within `range` of `position`, nearest first.                                                                      |
| `world-entity-ray`         | `(world-entity-ray world-name position direction range)`                | Returns a list of pairs containing all entities in the world along `direction` from `position` within `range` and their hit positions, nearest first.                 |
| `world-entity-view`        | `(world-entity-view world-name position direction angle range)`         | Returns a list containing all entities in the world within `angle` of `direction` from `position` within `range`, nearest first.                                      |
| `entity-add`               | `(entity-add entity-name)`                                              | Create a new entity.                                                                                                                                                  |
| `entity-remove`            | `(entity-remove entity)`                                                | Remove an existing entity.                                                                                                                                            |
| `entity-copy`              | `(entity-copy entity)`                                                  | Creates a copy of an existing entity.                                                                                                                                 |
| `entity-shift`             | `(entity-shift entity world)`                                           | Moves an entity to another world.                                                                                                                                     |
| `entity-find`              | `(entity-find entity pattern)`                                          | Get all entities in the entity's world whose names contain `pattern`.                                                                                                 |
| `entity-all`               | `(entity-all entity)`                                                   | Returns a list containing all entities in the entity's world.                                                                                                         |
| `entity-near`              | `(entity-near entity position range)`                                   | Returns a list containing all entities in the entity's world within `range` of `position`, nearest first.                                                             |
| `entity-ray`               | `(entity-ray entity position direction range)`                          | Returns a list of pairs containing all entities in the entity's world along `direction` from `position` within `range` and their hit positions, nearest first.        |
| `entity-view`              | `(entity-view entity position direction angle range)`                   | Returns a list containing all entities in the entity's world within `angle` of `direction` from `position` within `range`, nearest first.                             |
| `entity-transfer`          | `(entity-transfer entity user-id)`                                      | Transfers ownership of an entity to the specified user.                                                                                                               |
| `entity-take`              | `(entity-take entity)`                                                  | Takes ownership of an entity with no owner.                                                                                                                           |
| `entity-discard`           | `(entity-discard entity)`                                               | Relinquishes ownership of an entity.                                                                                                                                  |
| `entity-owner`             | `(entity-owner entity)`                                                 | Returns the user ID of the owning user, or 0 if no owner.                                                                                                             |
| `entity-share`             | `(entity-share entity)`                                                 | Shares the entity, allowing anyone to edit it.                                                                                                                        |
| `entity-unshare`           | `(entity-unshare entity)`                                               | Unshares the entity, preventing others from editing it.                                                                                                               |
| `entity-shared`            | `(entity-shared entity state)`                                          | Sets whether or not users other than the owner can edit the entity.                                                                                                   |
| `entity-shared?`           | `(entity-shared? entity)`                                               | Returns 1 if the entity is shared.                                                                                                                                    |
| `entity-attach`            | `(entity-attach entity user-id)`                                        | Attach a user to the entity.                                                                                                                                          |
| `entity-detach`            | `(entity-detach entity)`                                                | Detach the user from the entity.                                                                                                                                      |
| `entity-attached-user`     | `(entity-attached-user entity)`                                         | Returns the ID of the attached user, or 0 if none is attached.                                                                                                        |
| `entity-global-position`   | `(entity-global-position entity [position])`                            | Gets the entity's global position or sets it.                                                                                                                         |
| `entity-global-rotation`   | `(entity-global-rotation entity [rotation])`                            | Gets the entity's global rotation or sets it.                                                                                                                         |
| `entity-global-scale`      | `(entity-global-scale entity [scale])`                                  | Gets the entity's global scale or sets it.                                                                                                                            |
| `entity-local-position`    | `(entity-local-position entity [position])`                             | Gets the entity's local position or sets it.                                                                                                                          |
| `entity-local-rotation`    | `(entity-local-rotation entity [rotation])`                             | Gets the entity's local rotation or sets it.                                                                                                                          |
| `entity-local-scale`       | `(entity-local-scale entity [scale])`                                   | Gets the entity's local scale or sets it.                                                                                                                             |
| `entity-linear-velocity`   | `(entity-linear-velocity entity [velocity])`                            | Gets the entity's linear velocity or sets it.                                                                                                                         |
| `entity-angular-velocity`  | `(entity-angular-velocity entity [velocity])`                           | Gets the entity's angular velocity or sets it.                                                                                                                        |
| `entity-world`             | `(entity-world entity)`                                                 | Returns the name of the entity's world.                                                                                                                               |
| `entity-name`              | `(entity-name entity)`                                                  | Returns the name of the entity.                                                                                                                                       |
| `entity-play`              | `(entity-play entity)`                                                  | Activates the entity.                                                                                                                                                 |
| `entity-pause`             | `(entity-pause entity)`                                                 | Deactivates the entity.                                                                                                                                               |
| `entity-store`             | `(entity-store entity)`                                                 | Deactivates an entity, overwriting the inactive state with the active one.                                                                                            |
| `entity-reset`             | `(entity-reset entity)`                                                 | Activates an entity, overwriting the active state with the inactive one.                                                                                              |
| `entity-active`            | `(entity-active entity state)`                                          | Sets the entity to be active or inactive.                                                                                                                             |
| `entity-active?`           | `(entity-active? entity)`                                               | Returns 1 if the entity is active.                                                                                                                                    |
| `entity-parent`            | `(entity-parent entity)`                                                | Gets the entity's parent or sets it.                                                                                                                                  |
| `entity-parent-set`        | `(entity-parent-set entity parent)`                                     | Sets the entity's parent.                                                                                                                                             |
| `entity-parent-clear`      | `(entity-parent-clear entity)`                                          | Clears the entity's parent.                                                                                                                                           |
| `entity-children`          | `(entity-children entity)`                                              | Gets the entity's children.                                                                                                                                           |
| `entity-mass`              | `(entity-mass entity [mass])`                                           | Gets the entity's mass or sets it.                                                                                                                                    |
| `entity-drag`              | `(entity-drag entity [drag])`                                           | Gets the entity's drag or sets it.                                                                                                                                    |
| `entity-buoyancy`          | `(entity-buoyancy entity [buoyancy])`                                   | Gets the entity's buoyancy or sets it.                                                                                                                                |
| `entity-lift`              | `(entity-lift entity [lift])`                                           | Gets the entity's lift coefficient or sets it.                                                                                                                        |
| `entity-magnetism`         | `(entity-magnetism entity [magnetism])`                                 | Gets the entity's magnetic susceptibility or sets it.                                                                                                                 |
| `entity-friction`          | `(entity-friction entity [friction])`                                   | Get the entity's friction coefficient or sets it.                                                                                                                     |
| `entity-restitution`       | `(entity-restitution entity [restitution])`                             | Get the entity's restitution coefficient or sets it.                                                                                                                  |
| `entity-sample-play`       | `(entity-play-sample entity name volume directional angle anchor-id)`   | Starts a sample playing with the specified emitter angle, and positioned at the specified anchor.                                                                     |
| `entity-sample-stop`       | `(entity-stop-sample entity name)`                                      | Stops a sample that was playing.                                                                                                                                      |
| `entity-action-play`       | `(entity-play-action entity name)`                                      | Starts an action playing.                                                                                                                                             |
| `entity-action-stop`       | `(entity-stop-action entity name)`                                      | Stops an action that was playing.                                                                                                                                     |
| `entity-body-anchor`       | `(entity-body-anchor entity name)`                                      | Gets the position and rotation of a named generic anchor on the entity. The position and rotation are returned as a list in that order.                               |
| `entity-hostify`           | `(entity-hostify entity)`                                               | Makes an entity attachable, allowing users to attach to it.                                                                                                           |
| `entity-unhostify`         | `(entity-unhostify entity)`                                             | Makes an entity unattachable, disallowing users from attaching to it.                                                                                                 |
| `entity-host`              | `(entity-host entity state)`                                            | Sets whether or not users can attach to the entity.                                                                                                                   |
| `entity-host?`             | `(entity-host? entity)`                                                 | Returns 1 if the entity is attachable.                                                                                                                                |
| `entity-show`              | `(entity-show entity)`                                                  | Makes an entity visible on the entity list of non-admin non-owner users.                                                                                              |
| `entity-hide`              | `(entity-hide entity)`                                                  | Hides an entity from the entity list of non-admin non-owner users.                                                                                                    |
| `entity-shown`             | `(entity-shown entity state)`                                           | Sets whether or not the entity is visible on the entity list of non-admin non-owner users.                                                                            |
| `entity-shown?`            | `(entity-shown? entity)`                                                | Returns 1 if the entity is visible on the entity list of non-admin non-owner users.                                                                                   |
| `entity-physicalize`       | `(entity-physicalize entity)`                                           | Enables physics for an entity.                                                                                                                                        |
| `entity-etherealize`       | `(entity-etherealize entity)`                                           | Disables physics for an entity.                                                                                                                                       |
| `entity-physical`          | `(entity-physical entity state)`                                        | Sets whether or not physics is enabled for the entity.                                                                                                                |
| `entity-physical?`         | `(entity-physical? entity)`                                             | Returns 1 if physics is enabled for the entity.                                                                                                                       |
| `entity-visualize`         | `(entity-visualize entity)`                                             | Enables visibility of an entity.                                                                                                                                      |
| `entity-unvisualize`       | `(entity-unvisualize entity)`                                           | Disables visibility of an entity.                                                                                                                                     |
| `entity-visible`           | `(entity-visible entity state)`                                         | Sets whether or not the entity is visible.                                                                                                                            |
| `entity-visible?`          | `(entity-visible? entity)`                                              | Returns 1 if the entity is visible.                                                                                                                                   |
| `entity-audialize`         | `(entity-audialize entity)`                                             | Enables audibility of an entity.                                                                                                                                      |
| `entity-unaudialize`       | `(entity-unaudialize entity)`                                           | Disables audibility of an entity.                                                                                                                                     |
| `entity-audible`           | `(entity-audible entity state)`                                         | Sets whether or not the entity is audible.                                                                                                                            |
| `entity-audible?`          | `(entity-audible? entity)`                                              | Returns 1 if the entity is audible.                                                                                                                                   |
| `entity-binding-add`       | `(entity-binding-add entity input name)`                                | Adds a new input binding to the entity.                                                                                                                               |
| `entity-binding-remove`    | `(entity-binding-remove entity input)`                                  | Removes an input binding from the entity.                                                                                                                             |
| `entity-bindings`          | `(entity-bindings entity)`                                              | Returns all input bindings on the entity.                                                                                                                             |
| `entity-hud-add`           | `(entity-hud-add entity name type anchor)`                              | Adds a new HUD element to the entity.                                                                                                                                 |
| `entity-hud-remove`        | `(entity-hud-remove entity name)`                                       | Removes a HUD element from the entity.                                                                                                                                |
| `entity-hud-position`      | `(entity-hud-position entity name position)`                            | Sets the position of a HUD element on the entity.                                                                                                                     |
| `entity-hud-color`         | `(entity-hud-color entity name color)`                                  | Sets the color of a HUD element on the entity.                                                                                                                        |
| `entity-hud-line`          | `(entity-hud-line entity name length rotation)`                         | Sets the length and rotation of a line HUD element on the entity.                                                                                                     |
| `entity-hud-text`          | `(entity-hud-text entity name text)`                                    | Sets the text of a text HUD element on the entity.                                                                                                                    |
| `entity-hud-bar`           | `(entity-hud-bar entity name length fraction right-to-left)`            | Sets the length, fraction and orientation of a bar HUD element on the entity.                                                                                         |
| `entity-hud-symbol`        | `(entity-hud-symbol entity type size)`                                  | Sets the type and size of a symbol HUD element on the entity.                                                                                                         |
| `entity-field-of-view`     | `entity-field-of-view entity [fov])`                                    | Gets the entity's attached FOV or sets it.
| `entity-mouse-lock`        | `(entity-mouse-lock entity)`                                            | Locks the mouse of any attached user.                                                                                                                                 |
| `entity-mouse-unlock`      | `(entity-mouse-unlock entity)`                                          | Unlocks the mouse of any attached user.                                                                                                                               |
| `entity-mouse-locked`      | `(entity-mouse-locked entity state)`                                    | Sets whether or not the mouse of any attached user is locked.                                                                                                         |
| `entity-mouse-locked?`     | `(entity-mouse-locked? entity)`                                         | Returns 1 if the mouse is locked.                                                                                                                                     |
| `entity-voice-volume`      | `(entity-voice-volume entity [volume])                                  | Gets the entity's VoIP volume multiplier or sets it.                                                                                                                  |
| `entity-tint-color`        | `(entity-tint-color entity [color])`                                    | Gets the entity's screen tint color or sets it.                                                                                                                       |
| `entity-tint-strength`     | `(entity-tint-strength entity [strength])`                              | Gets the entity's screen tint strength or sets it.                                                                                                                    |
| `entity-haptic`            | `(entity-haptic entity target duration frequency amplitude)`            | Causes a haptic effect on the specified VR controller of the attached user, if any.                                                                                   |
| `text-signal`              | `(text-signal user-id|user-ids state)`                                  | Toggles showing the text symbol to the specified users, indicating that text is coming.                                                                               |
| `text-titlecard`           | `(text-titlecard user-id|user-ids title text [callback])`               | Displays a titlecard, such as when entering a new area, skipping to a point in time or starting a new quest.                                                          |
| `text-timeout`             | `(text-timeout user-id|user-ids title text duration [callback])`        | Displays a text block to the specified users that disappears after a period of time in seconds.                                                                       |
| `text-unary`               | `(text-unary user-id|user-ids title text [callback])`                   | Displays a text block to the specified users with one interaction: "OK".                                                                                              |
| `text-binary`              | `(text-binary user-id|user-ids title text [callback])`                  | Displays a text block to the specified users with two interactions: "Yes" and "No".                                                                                   |
| `text-options`             | `(text-options user-id|user-ids title text options [callback])`         | Displays a text block to the specified users with any number of custom interactions.                                                                                  |
| `text-choose`              | `(text-choose user-id|user-ids title options count [callback])`         | Displays a text utility to the specified users which prompts them to choose `count` unique items from `options`.                                                      |
| `text-assign`              | `(text-assign user-id|user-ids title keys values [callback])`           | Displays a text utility to the specified users which prompts them to assign each member of `values` to a unique member of `keys`.                                     |
| `text-point-buy`           | `(text-point-buy user-id|user-ids title prices points [callback])`      | Displays a text utility to the specified users which prompts them to buy as many unique items from the prices table as they can afford with their `points`.           |
| `text-point-assign`        | `(text-point-assign user-id|user-ids title keys points [callback])`     | Displays a text utility to the specified users which prompts them to assign each member of `keys` a share of their `points`.                                          |
| `text-vote`                | `(text-vote user-id|user-ids title options votes [callback])`           | Displays a text utility to the specified users which gives them a fixed length of time in which to cast a vote for `count` unique items from `options`.               |
| `text-choose-major`        | `(text-choose-major user-id|user-ids title options [callback])`         | Displays a text utility to the specified users which prompts them to choose one of the detailed options provided to them.                                             |
| `text-knowledge`           | `(text-knowledge user-id|user-ids title subtitle text)`                 | Adds a knowledge entry to the knowledge viewer of the specified users.                                                                                                |
| `text-clear`               | `(text-clear user-id|user-ids knowledge)`                               | Clears all previously queued text blocks, including knowledge if `knowledge` is not 0.                                                                                |
| `team-all`                 | `(team-all)`                                                            | Returns a list of the names of all teams.                                                                                                                             |
| `team-add`                 | `(team-add team-name)`                                                  | Creates a new team with the given name.                                                                                                                               |
| `team-remove`              | `(team-remove team-name)`                                               | Removes an existing team with the given name.                                                                                                                         |
| `team-get`                 | `(team-get team-name)`                                                  | Returns a list of all the users within a team.                                                                                                                        |
| `team-set`                 | `(team-set team-name user-ids)`                                         | Sets the users within a team.                                                                                                                                         |
| `user-one`                 | `(user-one user-name)`                                                  | Returns a list containing the user ID of the named user.                                                                                                              |
| `user-not-one`             | `(user-not-one user-name)`                                              | Returns a list containing the user IDs of all active users except the named user.                                                                                     |
| `user-group`               | `(user-group user-names)`                                               | Returns a list containing the user IDs of all active named users.                                                                                                     |
| `user-not-group`           | `(user-not-group user-names)`                                           | Returns a list containing the user IDs of all active users excepted the named users.                                                                                  |
| `user-all`                 | `(user-all)`                                                            | Returns a list containing the user IDs of all active users.                                                                                                           |
| `user-near`                | `(user-near position range)`                                            | Returns a list containing the user IDs of all active users within `range` of `position`.                                                                              |
| `user-team`                | `(user-team team-name)`                                                 | Returns a list containing the user IDs of all active users within the specified team.                                                                                 |
| `user-active?`             | `(user-active? user-id|user-ids)`                                       | Returns 1 if the user is connected. Returns a list of values if passed a list of user IDs.                                                                            |
| `user-attached-entity`     | `(user-attached-entity user-id|user-ids)`                               | Returns the entity the user is attached to, or a null handle if the user is not attached to anything. Returns a list of values if passed a list of user IDs.          |
| `user-remove`              | `(user-remove user-id|user-ids)`                                        | Deletes the user(s).                                                                                                                                                  |
| `user-kick`                | `(user-kick user-id|user-ids)`                                          | Kicks the user(s) from the server.                                                                                                                                    |
| `user-move`                | `(user-move user-id|user-ids position rotation)`                        | Moves the user(s)'s view to a new position.                                                                                                                           |
| `user-color`               | `(user-color user-id|user-ids [color])`                                 | Gets the user(s)'s color or sets it.                                                                                                                                  |
| `user-teams`               | `(user-teams user-id|user-ids [teams])`                                 | Gets a list of teams the user in or sets it.                                                                                                                          |
| `user-attach`              | `(user-attach user-id entity)`                                          | Attach the user to an entity.                                                                                                                                         |
| `user-detach`              | `(user-detach user-id)`                                                 | Detach the user from an entity.                                                                                                                                       |
| `user-magnitude`           | `(user-magnitude user-id)`                                              | Gets the user's current magnitude setting.                                                                                                                            |
| `user-advantage`           | `(user-advantage user-id)`                                              | Gets the user's current advantage.                                                                                                                                    |
| `user-doom`                | `(user-doom user-id)`                                                   | Returns 1 if the user's doom flag is set, 0 otherwise.                                                                                                                |
| `user-goto`                | `(user-goto user-id|user-ids position radius [up])`                     | Moves the user(s) so that their host entities are arranged in a circle of `radius` around `position`.                                                                 |
| `user-tether`              | `(user-tether user-id|user-ids state [distance])`                       | When `state` is true creates a tether tying the users together. User motion becomes slower the further from the center point of the group they travel.                |
| `user-confine`             | `(user-confine user-id|user-ids state [position range])`                | When `state` is true creates a sphere that the users will be confined to, whether in attached more or not.                                                            |
| `user-fade`                | `(user-fade user-id|user-ids state)`                                    | When `state` is true fades the user's view of the world out. Otherwise reverses a previous fade.                                                                      |
| `user-wait`                | `(user-wait user-id|user-ids state)`                                    | When `state` is true displays a waiting message to the user, regardless of their view being faded. Otherwise hides a previously displayed waiting message.            |
| `chat-send`                | `(chat-send message)`                                                   | Sends a direct message to the specified users. The message will appear to originate from the executing user.                                                          |
| `chat-send-team`           | `(chat-send-team team-name\|team-names message)`                        | Sends a direct message to the specified users. The message will appear to originate from the executing user.                                                          |
| `chat-send-user`           | `(chat-send-user user-id|user-ids message)`                             | Sends a global chat message. The message will appear to originate from the executing user.                                                                            |
| `voice-channel`            | `(voice-channel [team-name\|void])`                                     | Gets the current user's voice team or sets it. Passing the void value uses the global channel.                                                                        |
| `attachments-locked`       | `(attachments-locked state)`                                            | Sets the attachment lock state of the game.                                                                                                                           |
| `attachments-locked?`      | `(attachments-locked?)`                                                 | Returns 1 if attachments are locked, 0 otherwise.                                                                                                                     |
| `attachments-lock`         | `(attachments-lock)`                                                    | Lock attachments, preventing non-admin users from detaching.                                                                                                          |
| `attachments-unlock`       | `(attachments-unlock)`                                                  | Unlock attachments, allowing non-admin users to detach.                                                                                                               |
| `magnitude-global`         | `(magnitude-global)`                                                    | Returns the current global magnitude level.                                                                                                                           |
| `magnitude-with`           | `(magnitude-with name value lambda)`                                    | Executes `lambda` only if the magnitude is higher than or equal to `value`. Uses `name` when reporting failure.                                                       |
| `magnitude-limit`          | `(magnitude-limit [value])`                                             | Sets the maximum magnitude level a user can signal or gets it.                                                                                                        |
| `magnitude`                | `(magnitude [value])`                                                   | Changes the magnitude level you are signalling or returns the current value.                                                                                          |
| `advantage-apply`          | `(advantage-apply user-id value)`                                       | Applies the given user's advantage to the supplied numerical value.                                                                                                   |
| `advantage-give`           | `(advantage-give user-id count)`                                        | Adds the specified number of points to the user's advantage.                                                                                                          |
| `advantage-take`           | `(advantage-take user-id count lambda)`                                 | If `count` is <= the user's advantage: subtracts `count` from the user's advantage and executes `lambda`.                                                             |
| `advantage`                | `(advantage)`                                                           | Returns your advantage.                                                                                                                                               |
| `doom-flag`                | `(doom-flag user-id)`                                                   | Sets the user's doom flag.                                                                                                                                            |
| `doom-clear`               | `(doom-clear user-id)`                                                  | Clears the user's doom flag.                                                                                                                                          |
| `doom`                     | `(doom)`                                                                | Returns 1 if your doom flag is set, 0 otherwise.                                                                                                                      |
| `attach`                   | `(attach entity)`                                                       | Attach yourself to an entity.                                                                                                                                         |
| `detach`                   | `(detach)`                                                              | Detach yourself from an entity.                                                                                                                                       |
| `ping`                     | `(ping)`                                                                | Signals the game master(s).                                                                                                                                           |
| `braking`                  | `(braking state)`                                                       | Sets the braking state of the game.                                                                                                                                   |
| `braking?`                 | `(braking?)`                                                            | Returns 1 if the game is braking, 0 otherwise.                                                                                                                        |
| `brake`                    | `(brake)`                                                               | Signals everyone and applies increasingly strong time dilation to the world, eventually pausing time.                                                                 |
| `unbrake`                  | `(unbrake)`                                                             | Reverses a previous brake.                                                                                                                                            |
| `splash-message`           | `(splash-message [message])`                                            | Changes the server's splash message or returns the current message.                                                                                                   |
| `save`                     | `(save)`                                                                | Instructs the server to immediately save its state to disk.                                                                                                           |

<a name="content-library"></a>

### Content Library

**Note:** Square brackets around an argument indicate that it is optional. An ellipsis indicates that any number of arguments can be supplied. A vertical bar indicates a choice between multiple different argument forms.

These functions implement specific convenience functions for patterns found in common scenarios.

| Name          | Format                                                  | Behavior                                                                                                                                                   |
|---------------|---------------------------------------------------------|------------------------------------------------------------------------------------------------------------------------------------------------------------|
| `ai-pathfind` | `(ai-pathfind start end generator heuristic)`           | Implements the A\* pathfinding algorithm.                                                                                                                  |
| `ai-decide`   | `(ai-decide depth state generator predictor heuristic)` | Implements the alpha-beta pruned minimax decision making algorithm.                                                                                        |
| `dice`        | `(dice count sides)`                                    | Simulates rolling `count` dice with `sides` sides. Intended for use in meta-game mechanics only and uses its own internal randomness.                      |
| `zdice`       | `(zdice count sides)`                                   | Simulates rolling `count` dice with `sides` sides. Sides start at zero. Intended for use in meta-game mechanics only and uses its own internal randomness. |

<a name="constants"></a>

### Constants

The following constants are defined:

| Name           | Meaning                                            |
|----------------|----------------------------------------------------|
| `void`         | Void value.                                        |
| `nullptr`      | An invalid handle.                                 |
| `no-user`      | The user ID of an invalid user (0).                |
| `true`         | The number 1.                                      |
| `false`        | The number 0.                                      |
| `pi`           | Pi.                                                |
| `tau`          | The number of radians in a full turn, i.e. 2 * Pi. |
| `phi`          | The golden ratio.                                  |
| `euler`        | Euler's number.                                    |
| `forward`      | 3D unit vector in the forward direction.           |
| `back`         | 3D unit vector in the back direction.              |
| `left`         | 3D unit vector in the left direction.              |
| `right`        | 3D unit vector in the right direction.             |
| `up`           | 3D unit vector in the up direction.                |
| `down`         | 3D unit vector in the down direction.              |
| `type-void`    | The value returned by `type` for a void value.     |
| `type-symbol`  | The value returned by `type` for a symbol value.   |
| `type-integer` | The value returned by `type` for a integer value.  |
| `type-float`   | The value returned by `type` for a float value.    |
| `type-string`  | The value returned by `type` for a string value.   |
| `type-list`    | The value returned by `type` for a list value.     |
| `type-lambda`  | The value returned by `type` for a lambda value.   |
| `type-handle`  | The value returned by `type` for a handle value.   |

<a name="special-symbols"></a>

## Special symbols

Some API functions recognize special symbols as arguments, such as `entity-binding-add`, which takes a symbol representing the type of input to bind as its second argument.

Input symbols:

| Symbol                          |
|---------------------------------|
| input-null                      |
| input-0                         |
| input-1                         |
| input-2                         |
| input-3                         |
| input-4                         |
| input-5                         |
| input-6                         |
| input-7                         |
| input-8                         |
| input-9                         |
| input-a                         |
| input-b                         |
| input-c                         |
| input-d                         |
| input-e                         |
| input-f                         |
| input-g                         |
| input-h                         |
| input-i                         |
| input-j                         |
| input-k                         |
| input-l                         |
| input-m                         |
| input-n                         |
| input-o                         |
| input-p                         |
| input-q                         |
| input-r                         |
| input-s                         |
| input-t                         |
| input-u                         |
| input-v                         |
| input-w                         |
| input-x                         |
| input-y                         |
| input-z                         |
| input-f1                        |
| input-f2                        |
| input-f3                        |
| input-f4                        |
| input-f5                        |
| input-f6                        |
| input-f7                        |
| input-f8                        |
| input-f9                        |
| input-f10                       |
| input-f11                       |
| input-f12                       |
| input-escape                    |
| input-backspace                 |
| input-tab                       |
| input-caps-lock                 |
| input-return                    |
| input-space                     |
| input-menu                      |
| input-grave-accent              |
| input-minus                     |
| input-equals                    |
| input-left-brace                |
| input-right-brace               |
| input-backslash                 |
| input-semicolon                 |
| input-quote                     |
| input-period                    |
| input-comma                     |
| input-slash                     |
| input-left-shift                |
| input-left-control              |
| input-left-super                |
| input-left-alt                  |
| input-right-shift               |
| input-right-control             |
| input-right-super               |
| input-right-alt                 |
| input-print-screen              |
| input-insert                    |
| input-delete                    |
| input-scroll-lock               |
| input-home                      |
| input-end                       |
| input-pause                     |
| input-page-up                   |
| input-page-down                 |
| input-left                      |
| input-right                     |
| input-up                        |
| input-down                      |
| input-num-lock                  |
| input-num-add                   |
| input-num-subtract              |
| input-num-multiply              |
| input-num-divide                |
| input-num-0                     |
| input-num-1                     |
| input-num-2                     |
| input-num-3                     |
| input-num-4                     |
| input-num-5                     |
| input-num-6                     |
| input-num-7                     |
| input-num-8                     |
| input-num-9                     |
| input-num-period                |
| input-num-return                |
| input-left-mouse                |
| input-middle-mouse              |
| input-right-mouse               |
| input-x1-mouse                  |
| input-x2-mouse                  |
| input-mouse-wheel-up            |
| input-mouse-wheel-down          |
| input-mouse-wheel-left          |
| input-mouse-wheel-right         |
| input-mouse-move                |
| input-gamepad-dpad-left         |
| input-gamepad-dpad-right        |
| input-gamepad-dpad-up           |
| input-gamepad-dpad-down         |
| input-gamepad-a                 |
| input-gamepad-b                 |
| input-gamepad-x                 |
| input-gamepad-y                 |
| input-gamepad-left-stick        |
| input-gamepad-left-stick-click  |
| input-gamepad-left-bumper       |
| input-gamepad-left-trigger      |
| input-gamepad-right-stick       |
| input-gamepad-right-stick-click |
| input-gamepad-right-bumper      |
| input-gamepad-right-trigger     |
| input-gamepad-back              |
| input-gamepad-guide             |
| input-gamepad-start             |
| input-touch                     |
| input-vr-heartrate              |
| input-vr-system-click           |
| input-vr-volume-up              |
| input-vr-volume-down            |
| input-vr-mute-microphone        |
| input-vr-head                   |
| input-vr-hips                   |
| input-vr-left_Hand              |
| input-vr-right_Hand             |
| input-vr-left_Foot              |
| input-vr-right_Foot             |
| input-vr-chest                  |
| input-vr-left-shoulder          |
| input-vr-right-shoulder         |
| input-vr-left-elbow             |
| input-vr-right-elbow            |
| input-vr-left-wrist             |
| input-vr-right-wrist            |
| input-vr-left-knee              |
| input-vr-right-knee             |
| input-vr-left-ankle             |
| input-vr-right-ankle            |
| input-vr-eyes                   |
| input-vr-left-eye               |
| input-vr-left-eyebrow           |
| input-vr-left-eyelid            |
| input-vr-right-eye              |
| input-vr-right-eyebrow          |
| input-vr-right-eyelid           |
| input-vr-jaw                    |
| input-vr-upper-lip              |
| input-vr-lower-lip              |
| input-vr-mouth                  |
| input-vr-left-cheek             |
| input-vr-right-cheek            |
| input-vr-tongue                 |
| input-vr-left-thumb             |
| input-vr-left-index             |
| input-vr-left-middle            |
| input-vr-left-ring              |
| input-vr-left-little            |
| input-vr-right-thumb            |
| input-vr-right-index            |
| input-vr-right-middle           |
| input-vr-right-ring             |
| input-vr-right-little           |
| input-vr-left-trigger-touch     |
| input-vr-left-trigger           |
| input-vr-left-trigger-click     |
| input-vr-left-grip-touch        |
| input-vr-left-grip              |
| input-vr-left-grip-click        |
| input-vr-left-stick-touch       |
| input-vr-left-stick             |
| input-vr-left-stick-click       |
| input-vr-left-touchpad-touch    |
| input-vr-left-touchpad          |
| input-vr-left-touchpad-press    |
| input-vr-left-touchpad-click    |
| input-vr-left-system-touch      |
| input-vr-left-system            |
| input-vr-left-a-touch           |
| input-vr-left-a                 |
| input-vr-left-b-touch           |
| input-vr-left-b                 |
| input-vr-left-menu-touch        |
| input-vr-left-menu              |
| input-vr-left-bumper-touch      |
| input-vr-left-bumper            |
| input-vr-left-thumbrest-touch   |
| input-vr-left-thumbrest         |
| input-vr-right-trigger-touch    |
| input-vr-right-trigger          |
| input-vr-right-trigger-click    |
| input-vr-right-grip-touch       |
| input-vr-right-grip             |
| input-vr-right-grip-click       |
| input-vr-right-stick-touch      |
| input-vr-right-stick            |
| input-vr-right-stick-click      |
| input-vr-right-touchpad-touch   |
| input-vr-right-touchpad         |
| input-vr-right-touchpad-press   |
| input-vr-right-touchpad-click   |
| input-vr-right-system-touch     |
| input-vr-right-system           |
| input-vr-right-a-touch          |
| input-vr-right-a                |
| input-vr-right-b-touch          |
| input-vr-right-b                |
| input-vr-right-menu-touch       |
| input-vr-right-menu             |
| input-vr-right-bumper-touch     |
| input-vr-right-bumper           |
| input-vr-right-thumbrest-touch  |
| input-vr-right-thumbrest        |

**Note:** For VR controllers, the first available "application button" (this is often called X on left hand controllers, it's the grip button on Vive wands) is bound as "A", the second available application button (if one exists, this is often called Y on left hand controllers, it's the menu button on Vive wands) is bound as B. If there is a third button called menu, it's bound as the "Menu" button. Any button that is usually reserved for use by the runtime (the SteamVR button on the Index controllers) is bound as the "System" button and may not be available for use.

HUD type symbols:

| Symbol     |
|------------|
| hud-line   |
| hud-text   |
| hud-bar    |
| hud-symbol |

HUD anchor symbols:

| Symbol                |
|-----------------------|
| hud-anchor-center     |
| hud-anchor-left       |
| hud-anchor-right      |
| hud-anchor-up         |
| hud-anchor-down       |
| hud-anchor-up-left    |
| hud-anchor-down-left  |
| hud-anchor-up-right   |
| hud-anchor-down-right |

Haptic symbols:

| Symbol            |
|-------------------|
| haptic-left-hand  |
| haptic-right-hand |

<a name="entity-scripting"></a>

## Entity Scripting

Entities are comprised of fields from the perspective of the scripting environment. These fields are written with `write` and read with `read`. They can also be executed (if they have a lambda value) with `exec`. When they are executed the scripting environment enters the "entity context" where extra definitions are added to the namespace.

- `self`: A handle to the entity.
- `owner`: An integer containing the user ID of the owner of the entity.

And they will override any fields of the same name.

There are also two special field names which will be called by the game itself, they are `step` and `collide`. Every tick of the game's simulation any field called `step` containing a lambda value will be executed and any field called `collide` containing a lambda value will be executed when the entity collides with another in the physics system.

- `step` is called with one argument representing the amount of time in seconds this step represents, which may vary due to Time Controls or the Brake Control.
- `collide` is called with two arguments, a handle for the other entity involved in the collision and the position where the collision took place.
- Input callbacks are called with one argument, a table in the following form:

```
(
    (up 0)
    (intensity 1.0)
    (position '(0.0 0.0 0.0))
    (rotation '(1.0 0.0 0.0 0.0))
    (linear-velocity '(0.0 0.0 0.0))
    (angular-velocity '(0.0 0.0 0.0))
    (dimensions '(0.0 0.0 0.0 0.0 0.0 0.0))
    (touch '(1 0.0 0.0 0.0 0.0))
    (step 32)
)
```

- The `up` field contains the direction of binary inputs, like buttons, as either 0 or 1.
- The `intensity` field contains the intensity of linear inputs, like squeezing a trigger, in the range 0 to 1.
- The `position` field contains the positional element of an input. For input events originating from devices like thumbsticks, mouse motion and trackpads this is position in the `x` and `y` components with the `z` component unused. For tracked VR devices this is a 3D position in world space where the device is located.
- The `rotation` field contains the rotational element of an input. For tracked VR devices this is their rotation in world space.
- The `linear-velocity` field contains the linear motion element of an input. For mouse motion this is the amount the mouse moved. For tracked VR devices this is their linear velocity in world space.
- The `angular-velocity` field contains the angular motion element of an input. For tracked VR devices this is their angular velocity in world space.
- The `dimensions` field contains the dimensions of a shape input from 0 to 1. This is only used for the left eye, right eye, cheeks and mouth facial tracking inputs.
- The `touch` field contains the parameters of a touch input, in the following order:
    1. ID of the touch event, an arbitrary number used to distinguish between multiple simultaneous touches.
    2. major axis of the touch area, expressed as a fraction of the total screen height.
    3. minor axis of the touch area, expressed as a fraction of the total screen height.
    4. orientation of the touch, expressed as an angle in radians.
    5. force of the touch, in the range 0 to 1.
- The `step` field contains the number of the simulation step this input was created based on i.e. if the user has a 200 ms ping (the round trip time) then they created the input 100 ms ago, but they did so based on 100 ms old data, hence the `step` value is the index of the simulation step that occurred 200 ms ago.

<a name="timers"></a>

## Timers

If you need to execute a piece of code after some time delay e.g. to detonate a grenade three seconds after it is thrown, you can use a language feature called timers. The feature is accessible through three functions `timer-start`, `timer-stop` and `timer-all`. To start a timer call `(timer-start delay callback)`, for example `(timer-start 3 (lambda () (print "Boom!")))`. This function will return an integer ID uniquely identifying the timer which can be passed to `(timer-stop id)` to cancel the timer. If a timer is not stopped before the specified delay is elapsed the callback will be executed after running all other game code for that frame and the timer will be deleted.

Finally, the `(timer-all)` function returns a list of all active timer IDs, which can be used to clear all timers when resetting the system.

<a name="repeaters"></a>

## Repeaters

If you need to execute a piece of code every frame without association to any particular entity e.g. to update a weather system, you can use a language feature called repeaters. The feature is accessible through three functions `repeater-start`, `repeater-stop` and `repeater-all`. To start a repeater call `(repeater-start callback)`, for example `(repeater-start (lambda () (print "Tick!")))`. This function will return an integer ID uniquely identifying the repeater which can be passed to `(repeater-stop id)` to cancel the repeater.

Finally, the `(repeater-all)` function returns a list of all active repeater IDs, which can be used to clear all timers when resetting the system.
