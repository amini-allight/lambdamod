# Advanced Setup

This is a tutorial on setting up a more advanced LambdaMod server that discusses all the available connection modes.

## Contents

1. [Choosing a Mode](#choosing-a-mode)
2. [Hosting a Game](#hosting-a-game)
3. [Inviting Players](#inviting-players)
    1. [Direct Mode](#direct-mode)
    2. [Invite Mode](#invite-mode)
    3. [Session Mode](#session-mode)

<a name="choosing-a-mode"></a>

## Choosing a Mode

The LambdaMod server supports three different connection modes:

- `direct`: Normal direct connection using a host and port. Port forwarding required.
- `invite`: Connection via an invite URL followed by direct connection. Port forwarding required.
- `session`: Connection via an invite URL followed by TCP hole punching. Port forwarding not required, but may not work on all networks.

<a name="hosting-a-game"></a>

## Hosting a Game

To host a game first enter your chosen connection mode into the server configuration file at `~/.config/lambdamod/server.yml`:

```yaml
connectionMode: direct
```

Then run the following command to start the server:

```sh
./lambdamod-server host port password adminPassword savePath
```

- `host`: Should usually be `0.0.0.0`.
- `port`: Can be anything between 1024 and 65535.
- `password`: Is a password that all users will need to provide to connect.
- `adminPassword`: Is an alternative password that when used instead of the server password will grant admin permissions to the user.
- `savePath`: Is the path to a folder where the save data for the server should be stored.

A typical command would look like:

```sh
./lambdamod-server 0.0.0.0 7443 mypassword mysecretpassword ./save
```

If you are not using `session` mode you will now need to forward your chosen port through your router.

<a name="inviting-players"></a>

## Inviting Players

How players should be invited varies by mode:

<a name="direct-mode"></a>

### Direct Mode

Give the player you wish to connect your server's external IP (which can be discovered by searching "what's my IP" in your favorite search engine), chosen port and either the password or admin password, depending on whether or not you wish the player to have admin permissions. They can connect to a game by opening the client and typing connection details into the main menu, or by running:

```sh
./lambdamod host port serverpassword username password [vr]
```

Their `username` can be anything that meets the name criteria (see [here](game-masters-guide.md#naming)) and their `password` can be anything. Their personal password prevents other users from logging into the server under their name while they are offline. The `vr` argument is optional and should be `0` for flat mode and `1` for VR mode. A value of `1` for the `vr` argument is only valid if LambdaMod was compiled with VR support.

A typical command would look like:

```sh
./lambdamod 127.0.0.1 7443 mypassword amini topsecret 0
```

<a name="invite-mode"></a>

### Invite Mode

**Note:** These instructions use an external server. For more information on the implications of this see [here](security.md#invite-urls).

If successful the server will print out an invite URL, something like:

```
https://lambdamod.org/invite?host=127.0.0.1&port=7443&server-password=password
```

Give this URL to your players and they will be able to use the web UI to connect.

<a name="session-mode"></a>

### Session Mode

**Note:** These instructions use an external server. For more information on the implications of this see [here](security.md#sessions).

If successful the server will print out an invite URL, something like:

```
https://lambdamod.org/invite?session-id=0123456789abcdef
```

Give this URL to your players and they will be able to use the web UI to connect.
