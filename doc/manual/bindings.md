# Bindings

This is an automatically generated list of all control bindings in LambdaMod's various interaction modes.

## Contents

1. [Flat Bindings](#flat-bindings)
2. [VR Bindings](#vr-bindings)
3. [Gamepad Bindings](#gamepad-bindings)

<a name="flat-bindings"></a>

## Flat Bindings

Not all bindings are available in all modes.

| Input                         | Bindings                                                                                                                                                      |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Add Children                  | ctrl+p                                                                                                                                                        |
| Add Keyframes                 | i                                                                                                                                                             |
| Add New                       | shift+a                                                                                                                                                       |
| Add New Prefab                | ctrl+shift+a                                                                                                                                                  |
| Add Numeric Input 0           | 0, num-0                                                                                                                                                      |
| Add Numeric Input 1           | 1, num-1                                                                                                                                                      |
| Add Numeric Input 2           | 2, num-2                                                                                                                                                      |
| Add Numeric Input 3           | 3, num-3                                                                                                                                                      |
| Add Numeric Input 4           | 4, num-4                                                                                                                                                      |
| Add Numeric Input 5           | 5, num-5                                                                                                                                                      |
| Add Numeric Input 6           | 6, num-6                                                                                                                                                      |
| Add Numeric Input 7           | 7, num-7                                                                                                                                                      |
| Add Numeric Input 8           | 8, num-8                                                                                                                                                      |
| Add Numeric Input 9           | 9, num-9                                                                                                                                                      |
| Add Text                      | any-text                                                                                                                                                      |
| Attach                        | u                                                                                                                                                             |
| Body to Origin                | unbound                                                                                                                                                       |
| Brake                         | ctrl+space                                                                                                                                                    |
| Camera To Center Of Selection | ctrl+num-period                                                                                                                                               |
| Cancel Tool                   | escape, right-mouse                                                                                                                                           |
| Close Context Menu            | right-mouse                                                                                                                                                   |
| Close Dialog                  | escape                                                                                                                                                        |
| Close Editor                  | ctrl+escape                                                                                                                                                   |
| Copy                          | ctrl+c, ctrl+insert                                                                                                                                           |
| Cursor Down                   | down, ctrl+n                                                                                                                                                  |
| Cursor Left                   | left, ctrl+b                                                                                                                                                  |
| Cursor Right                  | right, ctrl+f                                                                                                                                                 |
| Cursor To                     | left-mouse                                                                                                                                                    |
| Cursor To Next Word           | ctrl+right, alt+f                                                                                                                                             |
| Cursor To Previous Word       | ctrl+left, alt+b                                                                                                                                              |
| Cursor To Selection           | shift+s                                                                                                                                                       |
| Cursor Up                     | up, ctrl+p                                                                                                                                                    |
| Cut                           | ctrl+x, shift+delete                                                                                                                                          |
| Deselect All                  | ctrl+a                                                                                                                                                        |
| Detach                        | escape                                                                                                                                                        |
| Dismiss Splash                | return                                                                                                                                                        |
| Document Go To End            | ctrl+end                                                                                                                                                      |
| Document Go To Start          | ctrl+home                                                                                                                                                     |
| Document Pan Left             | mouse-wheel-left                                                                                                                                              |
| Document Pan Right            | mouse-wheel-right                                                                                                                                             |
| Document Scroll Down          | mouse-wheel-down                                                                                                                                              |
| Document Scroll Up            | mouse-wheel-up                                                                                                                                                |
| Double Touch                  | touch x2                                                                                                                                                      |
| Downcase Next Word            | alt+l                                                                                                                                                         |
| Drag                          | mouse-move                                                                                                                                                    |
| Duplicate                     | shift+d                                                                                                                                                       |
| End Add Select                | release shift+left-mouse                                                                                                                                      |
| End Pan                       | release right-mouse                                                                                                                                           |
| End Remove Select             | release ctrl+left-mouse                                                                                                                                       |
| End Replace Select            | release left-mouse                                                                                                                                            |
| End Snap                      | release left-control                                                                                                                                          |
| End Special Edit              | release right-mouse                                                                                                                                           |
| End Tool                      | return, left-mouse                                                                                                                                            |
| Ensure Numeric Input Period   | period, num-period                                                                                                                                            |
| Extrude                       | e                                                                                                                                                             |
| Force Close Editor            | ctrl+shift+escape                                                                                                                                             |
| Go To End                     | end, ctrl+e                                                                                                                                                   |
| Go To Start                   | home, ctrl+a                                                                                                                                                  |
| Hide Knowledge Viewer         | escape                                                                                                                                                        |
| Hide Selected                 | h                                                                                                                                                             |
| Hide Unselected               | shift+h                                                                                                                                                       |
| Interact                      | left-mouse                                                                                                                                                    |
| Invert Selection              | ctrl+i                                                                                                                                                        |
| Join                          | ctrl+j                                                                                                                                                        |
| Mouse Motion                  | mouse-move                                                                                                                                                    |
| Move Cursor                   | alt+left-mouse                                                                                                                                                |
| Move Interface Pointer        | mouse-move                                                                                                                                                    |
| Move Select                   | mouse-move                                                                                                                                                    |
| Move Special Edit             | mouse-move                                                                                                                                                    |
| Move Window                   | ctrl+left-mouse                                                                                                                                               |
| Next History                  | down, shift+alt+period, ctrl+n                                                                                                                                |
| Next Tab                      | ctrl+tab                                                                                                                                                      |
| Open Context Menu             | right-mouse                                                                                                                                                   |
| Open Editor                   | left-mouse x2                                                                                                                                                 |
| Origin to 3D Cursor           | unbound                                                                                                                                                       |
| Origin to Body                | unbound                                                                                                                                                       |
| Ortho Invert                  | num-9                                                                                                                                                         |
| Ortho Look Back               | num-1                                                                                                                                                         |
| Ortho Look Down               | num-7                                                                                                                                                         |
| Ortho Look Forward            | ctrl+num-1                                                                                                                                                    |
| Ortho Look Left               | num-3                                                                                                                                                         |
| Ortho Look Right              | ctrl+num-3                                                                                                                                                    |
| Ortho Look Up                 | ctrl+num-7                                                                                                                                                    |
| Ortho Rotate Down             | num-2                                                                                                                                                         |
| Ortho Rotate Left             | num-4                                                                                                                                                         |
| Ortho Rotate Right            | num-6                                                                                                                                                         |
| Ortho Rotate Up               | num-8                                                                                                                                                         |
| Ortho Zoom In                 | mouse-wheel-up                                                                                                                                                |
| Ortho Zoom Out                | mouse-wheel-down                                                                                                                                              |
| Page Down                     | page-down                                                                                                                                                     |
| Page Up                       | page-up                                                                                                                                                       |
| Pan                           | mouse-move                                                                                                                                                    |
| Pan Left                      | left, ctrl+b, mouse-wheel-left                                                                                                                                |
| Pan Right                     | right, ctrl+f, mouse-wheel-right                                                                                                                              |
| Paste                         | ctrl+v, shift+insert, ctrl+y                                                                                                                                  |
| Paste Primary                 | middle-mouse                                                                                                                                                  |
| Ping                          | tab                                                                                                                                                           |
| Previous History              | up, shift+alt+comma, ctrl+p                                                                                                                                   |
| Previous Tab                  | ctrl+shift+tab                                                                                                                                                |
| Quit                          | ctrl+q, alt+f4                                                                                                                                                |
| Redo                          | ctrl+shift+z                                                                                                                                                  |
| Release                       | release left-mouse                                                                                                                                            |
| Reload                        | ctrl+r                                                                                                                                                        |
| Remove Keyframes              | alt+i                                                                                                                                                         |
| Remove Link                   | ctrl+shift+left-mouse                                                                                                                                         |
| Remove Next Character         | delete, ctrl+d                                                                                                                                                |
| Remove Next Word              | ctrl+delete, alt+d                                                                                                                                            |
| Remove Numeric Input          | backspace                                                                                                                                                     |
| Remove Parent                 | alt+p                                                                                                                                                         |
| Remove Previous Character     | backspace, ctrl+h                                                                                                                                             |
| Remove Previous Word          | ctrl+backspace, ctrl+w, alt+backspace                                                                                                                         |
| Remove Selected               | x, delete                                                                                                                                                     |
| Remove To End                 | ctrl+k                                                                                                                                                        |
| Remove To Start               | ctrl+u                                                                                                                                                        |
| Reset                         | ctrl+r                                                                                                                                                        |
| Reset Angular Velocity        | alt+a                                                                                                                                                         |
| Reset Camera                  | ctrl+h                                                                                                                                                        |
| Reset Cursor                  | shift+c                                                                                                                                                       |
| Reset Linear Velocity         | alt+l                                                                                                                                                         |
| Reset Position                | alt+g                                                                                                                                                         |
| Reset Rotation                | alt+r                                                                                                                                                         |
| Reset Scale                   | alt+s                                                                                                                                                         |
| Return To Menu                | left-mouse, space, return                                                                                                                                     |
| Rotate                        | r                                                                                                                                                             |
| Save                          | ctrl+s                                                                                                                                                        |
| Scale                         | s                                                                                                                                                             |
| Scroll Down                   | down, ctrl+n, mouse-wheel-down                                                                                                                                |
| Scroll Up                     | up, ctrl+p, mouse-wheel-up                                                                                                                                    |
| Select All                    | a                                                                                                                                                             |
| Select Down                   | shift+down                                                                                                                                                    |
| Select Left                   | shift+left                                                                                                                                                    |
| Select Linked                 | ctrl+l                                                                                                                                                        |
| Select Right                  | shift+right                                                                                                                                                   |
| Select To                     | shift+left-mouse                                                                                                                                              |
| Select To Next Word           | ctrl+shift+right                                                                                                                                              |
| Select To Previous Word       | ctrl+shift+left                                                                                                                                               |
| Select Up                     | shift+up                                                                                                                                                      |
| Selection To Cursor           | ctrl+shift+s                                                                                                                                                  |
| Set Magnitude 0               | ctrl+escape                                                                                                                                                   |
| Set Magnitude 1               | ctrl+f1                                                                                                                                                       |
| Set Magnitude 2               | ctrl+f2                                                                                                                                                       |
| Set Magnitude 3               | ctrl+f3                                                                                                                                                       |
| Show Knowledge Viewer         | f4                                                                                                                                                            |
| Split                         | p                                                                                                                                                             |
| Start Alt Special Edit        | ctrl+right-mouse                                                                                                                                              |
| Start Pan                     | right-mouse                                                                                                                                                   |
| Start Select                  | left-mouse, ctrl+left-mouse, shift+left-mouse                                                                                                                 |
| Start Snap                    | left-control                                                                                                                                                  |
| Start Special Edit            | right-mouse                                                                                                                                                   |
| Store                         | ctrl+s                                                                                                                                                        |
| Swap Characters               | ctrl+t                                                                                                                                                        |
| Swap Words                    | alt+t                                                                                                                                                         |
| Text Select All               | ctrl+shift+a                                                                                                                                                  |
| To Free Look                  | num-0                                                                                                                                                         |
| Toggle Console                | grave-accent                                                                                                                                                  |
| Toggle Console Mode           | ctrl+tab                                                                                                                                                      |
| Toggle Document Manager       | f2                                                                                                                                                            |
| Toggle Edit Mode              | tab                                                                                                                                                           |
| Toggle Fullscreen             | f11                                                                                                                                                           |
| Toggle Help                   | f1                                                                                                                                                            |
| Toggle Hud                    | f10                                                                                                                                                           |
| Toggle New Text Tool          | f4                                                                                                                                                            |
| Toggle Numeric Input Sign     | minus, num-subtract                                                                                                                                           |
| Toggle Oracle Manager         | f6                                                                                                                                                            |
| Toggle Orthographic           | num-5                                                                                                                                                         |
| Toggle Pivot Mode             | comma                                                                                                                                                         |
| Toggle Quick Accessor         | f1                                                                                                                                                            |
| Toggle Settings               | f3                                                                                                                                                            |
| Toggle Side Panel             | tab                                                                                                                                                           |
| Toggle System Settings        | f7                                                                                                                                                            |
| Toggle User Controls Tool     | f5                                                                                                                                                            |
| Toggle X                      | x                                                                                                                                                             |
| Toggle Y                      | y                                                                                                                                                             |
| Toggle Z                      | z                                                                                                                                                             |
| Touch                         | touch                                                                                                                                                         |
| Translate                     | g                                                                                                                                                             |
| Unbrake                       | ctrl+space                                                                                                                                                    |
| Undo                          | ctrl+z                                                                                                                                                        |
| Unhide All                    | alt+h                                                                                                                                                         |
| Upcase Next Word              | alt+u                                                                                                                                                         |
| Use Tool                      | mouse-move                                                                                                                                                    |
| Viewer End Back               | release s                                                                                                                                                     |
| Viewer End Down               | release left-control                                                                                                                                          |
| Viewer End Forward            | release w                                                                                                                                                     |
| Viewer End Left               | release a                                                                                                                                                     |
| Viewer End Right              | release d                                                                                                                                                     |
| Viewer End Rotate             | release right-mouse                                                                                                                                           |
| Viewer End Shift              | release middle-mouse                                                                                                                                          |
| Viewer End Sprint             | release left-shift                                                                                                                                            |
| Viewer End Up                 | release space                                                                                                                                                 |
| Viewer End Zoom               | release middle-mouse                                                                                                                                          |
| Viewer Start Back             | s                                                                                                                                                             |
| Viewer Start Down             | left-control                                                                                                                                                  |
| Viewer Start Forward          | w                                                                                                                                                             |
| Viewer Start Left             | a                                                                                                                                                             |
| Viewer Start Right            | d                                                                                                                                                             |
| Viewer Start Rotate           | right-mouse                                                                                                                                                   |
| Viewer Start Shift            | shift+middle-mouse                                                                                                                                            |
| Viewer Start Sprint           | left-shift                                                                                                                                                    |
| Viewer Start Up               | space                                                                                                                                                         |
| Viewer Start Zoom             | ctrl+middle-mouse                                                                                                                                             |

<a name="vr-bindings"></a>

## VR Bindings

**Note:** VR mode is intended for use with **attached mode**, no bindings for authoring content are available.

Not all bindings are available in all modes. To open the menu you must hold the B buttons (for Index controllers, the equivalent button is used on other controllers) on both controllers for one second.

| Input                         | Bindings                                                                                                                                                      |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Attach                        | vr-left-trigger, vr-right-trigger, vr-left-trigger-click, vr-right-trigger-click                                                                              |
| Cancel Tracking Calibration   | vr-left-b, vr-right-b                                                                                                                                         |
| Confirm Tracking Calibration  | vr-left-a, vr-right-a                                                                                                                                         |
| Decrease Volume               | vr-volume-down                                                                                                                                                |
| Dismiss Splash                | vr-left-b, vr-right-b                                                                                                                                         |
| End Show VR Menu Left         | release vr-left-b                                                                                                                                             |
| End Show VR Menu Right        | release vr-right-b                                                                                                                                            |
| Hide Knowledge Viewer         | vr-left-b, vr-right-b                                                                                                                                         |
| Hide VR Menu                  | vr-left-b, vr-right-b                                                                                                                                         |
| Increase Volume               | vr-volume-up                                                                                                                                                  |
| Interact                      | vr-left-a, vr-right-a, vr-left-trigger, vr-right-trigger, vr-left-trigger-click, vr-right-trigger-click                                                       |
| Move Chest                    | vr-chest                                                                                                                                                      |
| Move Head                     | vr-head                                                                                                                                                       |
| Move Left Ankle               | vr-left-ankle                                                                                                                                                 |
| Move Left Elbow               | vr-left-elbow                                                                                                                                                 |
| Move Left Foot                | vr-left-foot                                                                                                                                                  |
| Move Left Hand                | vr-left-hand                                                                                                                                                  |
| Move Left Knee                | vr-left-knee                                                                                                                                                  |
| Move Left Shoulder            | vr-left-shoulder                                                                                                                                              |
| Move Left Wrist               | vr-left-wrist                                                                                                                                                 |
| Move Right Ankle              | vr-right-ankle                                                                                                                                                |
| Move Right Elbow              | vr-right-elbow                                                                                                                                                |
| Move Right Foot               | vr-right-foot                                                                                                                                                 |
| Move Right Hand               | vr-right-hand                                                                                                                                                 |
| Move Right Knee               | vr-right-knee                                                                                                                                                 |
| Move Right Shoulder           | vr-right-shoulder                                                                                                                                             |
| Move Right Wrist              | vr-right-wrist                                                                                                                                                |
| Move Hips                     | vr-hips                                                                                                                                                       |
| Release                       | release vr-left-a, release vr-right-a, release vr-left-trigger, release vr-right-trigger, release vr-left-trigger-click, release vr-right-trigger-click       |
| Return To Menu                | vr-left-a, vr-right-a, vr-left-trigger, vr-right-trigger, vr-left-trigger-click, vr-right-trigger-click                                                       |
| Start Show VR Menu Left       | vr-left-b                                                                                                                                                     |
| Start Show VR Menu Right      | vr-right-b                                                                                                                                                    |
| Toggle Microphone             | vr-mute-microphone                                                                                                                                            |
| Viewer End Down               | release vr-left-b, release vr-right-b                                                                                                                         |
| Viewer End Up                 | release vr-left-a, release vr-right-a                                                                                                                         |
| Viewer Start Down             | vr-left-b, vr-right-b                                                                                                                                         |
| Viewer Start Up               | vr-left-a, vr-right-a                                                                                                                                         |
| VR Stick Motion               | vr-left-stick, vr-right-stick                                                                                                                                 |
| VR Touchpad Motion            | vr-left-touchpad, vr-right-touchpad                                                                                                                           |

<a name="gamepad-bindings"></a>

## Gamepad Bindings

**Note:** Gamepad mode is intended for use with **attached mode**, no bindings for authoring content are available.

| Input                         | Bindings                                                                                                                                                      |
|-------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Attach                        | gamepad-left-trigger, gamepad-right-trigger                                                                                                                   |
| Dismiss Splash                | gamepad-b                                                                                                                                                     |
| Hide Gamepad Menu             | gamepad-start, gamepad-b                                                                                                                                      |
| Hide Knowledge Viewer         | gamepad-b                                                                                                                                                     |
| Interact                      | gamepad-right-trigger, gamepad-a                                                                                                                              |
| Look Stick Motion             | gamepad-right-stick                                                                                                                                           |
| Move Gamepad Pointer          | gamepad-right-stick                                                                                                                                           |
| Move Stick Motion             | gamepad-left-stick                                                                                                                                            |
| Release                       | release gamepad-right-trigger, release gamepad-a                                                                                                              |
| Return To Menu                | gamepad-right-trigger, gamepad-a                                                                                                                              |
| Show Gamepad Menu             | gamepad-start                                                                                                                                                 |
