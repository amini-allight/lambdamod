# LambdaMod Game Master's Guide

Welcome to the LambdaMod Game Master's Guide! This guide will fill you in on all you need to know to start hosting LambdaMod sessions.

## Contents

1. [Introduction](#introduction)
2. [Naming](#naming)
3. [Coordinate System](#coordinate-system)
4. [Colors](#colors)
5. [Users](#users)
6. [Teams](#teams)
7. [Worlds](#worlds)
    1. [Physical Environment Settings](#physical-environment-settings)
    2. [Visual Environment Settings](#visual-environment-settings)
    3. [Audial Environment Settings](#audial-environment-settings)
    4. [Other Settings](#worlds-other-settings)
8. [Entities](#entities)
    1. [Entity Parenting](#entity-parenting)
    2. [Entity States](#entity-states)
    3. [Entity Permissions](#entity-permissions)
    4. [Entity General Settings](#entity-general-settings)
    5. [Entity Physics Settings](#entity-physics-settings)
    6. [Entity Host Settings](#entity-host-settings)
    7. [Entity Bindings](#entity-bindings)
    8. [Entity HUD](#entity-hud)
    9. [Entity Body](#entity-body)
    10. [Entity Sounds](#entity-sounds)
    11. [Entity Animations](#entity-animations)
9. [Text Tools](#text-tools)
    1. [Signal](#text-signal)
    2. [Titlecard](#text-titlecard)
    3. [Timeout](#text-timeout)
    4. [Unary](#text-unary)
    5. [Binary](#text-binary)
    6. [Options](#text-options)
    7. [Choose](#text-choose)
    8. [Assign](#text-assign)
    9. [Point Buy](#text-point-buy)
    10. [Point Assign](#text-point-assign)
    11. [Vote](#text-vote)
    12. [Choose Major](#text-choose-major)
    13. [Knowledge](#text-knowledge)
    14. [Clear](#text-clear)
9. [User Controls](#user-controls)
    1. [Attachment Lock](#user-control-attachment-lock)
    2. [Magnitude](#user-control-magnitude)
    3. [Advantage](#user-control-advantage)
    4. [Doom](#user-control-doom)
    5. [Ping](#user-control-ping)
    6. [Brake](#user-control-brake)
    7. [Fade](#user-control-fade)
    8. [Wait](#user-control-wait)
    9. [Goto](#user-control-goto)
    10. [Tether](#user-control-tether)
    11. [Confine](#user-control-confine)
    12. [Checkpoints](#user-control-checkpoints)
    13. [Time Multiplier](#user-control-time-multiplier)
    14. [Splash Message](#user-control-splash-message)
10. [Utilities](#utilities)

<a name="introduction"></a>

## Introduction

LambdaMod is a unique kind of game which I call a visual mode roleplaying game (VMRPG). It's basically a tabletop roleplaying game such as Dungeons & Dragons, but in 100% video game form. There are no dice rolls or character sheets, the gameplay is that of a video game, but there is still a game master behind the scenes orchestrating events. This guide will teach you how to be a game master and run sessions in LambdaMod.

**Note:** LambdaMod supports VR and gamepad input, but these are intended for players only. Creating content relies upon a complex suite of tools which do not map easily to either of these input methods. As a game master you will need to be in flat mode using mouse and keyboard input.

LambdaMod is designed around a fully self-contained editing concept. It doesn't support the importing of assets like images, meshes, sounds, animations, etc. from other programs. This is a deliberate choice and a core part of LambdaMod's design philosophy. Assets need to be simple so the game master can create them on the fly and consistency is more important to a game's visual look than the actual fidelity of any individual asset. You wouldn't use a gun made out of a couple glowing lines in Garry's Mod because it's surrounded by professionally made assets ripped from other games but you would in LambdaMod where it fits with everything else.

LambdaMod's editing tools are inspired by Blender. It's not required to know Blender to use them and they are much simpler than Blender (because Blender is a full 3D content creation suite and LambdaMod is not) but it will help if you already have muscle memory from some of Blender's shortcuts. There will be frequent references to Blender throughout this manual for those familiar with it.

<a name="naming"></a>

## Naming

**Note:** Be sure to tell the players about these constraints or they will have trouble creating accounts and connecting to your server.

Names for users, entities, etc. in LambdaMod have a number of constraints:

- Names must contain at least one character.
- Names must only contain lowercase a-z, 0-9 and -.
- Names must not start with '-'.
- Names must not end with '-'.
- Names must not be valid numbers i.e. not a sequence of only digits containing at most one '.' and optionally beginning with a '-'.
- Usernames must be no more than 32 characters.

These restrictions are imposed to improve game master productivity by ensuring names are consistent and easy to type.

<a name="coordinate-system"></a>

## Coordinate System

LamdbdaMod uses a right-handed Z-up coordinate system. X is associated with the color red, Y with the color green and Z with the color blue.

- **+X** Right
- **-X** Left
- **+Y** Forward
- **-Y** Back
- **+Z** Up
- **-Z** Down

<a name="colors"></a>

## Colors

LambdaMod takes a realistic approach to color. **Reflective** surfaces, that is objects that don't emit their own light but instead reflect light from other sources, have a traditional color. This is expressed as an amount of red from 0 — 255, an amount of green in the same range and an amount of blue in the same range. This includes most objects in most scenes, from pieces of armor to weapons to rocks and trees.

Other objects, like flares, lightbulbs and flames are **emissive** and emit their own light. These objects have a traditional color, the same as reflective objects, but they also have an intensity. an LED light and the sun might have the same color, say roughly #ffffff pure white in both cases, but they will have radically different intensities with the sun being tens of thousands of times brighter. This intensity is expressed in lumens (symbol `lm`) for light sources that weaken with distance, like flashlights and torches, and lux (symbol `lx`) for light sources at infinite distance like the sky and the sun. The units are equivalent except that `lx` has the distance falloff pre-applied.

<a name="users"></a>

## Users

**Users** are the accounts of people who have at some point connected the LambdaMod server.

You can manage users from the **settings** window. The "Users" tab contains a list of all users. You can press the binding listed below on users to edit them or remove them. New users are created when one connects to the server and cannot be created by the game master. It's recommended that you set colors for your users so you can more easily distinguish them, particularly when you have multiple game masters selecting entities within a world.

**Note:** While you can technically kick users this feature is intended mostly for technical purposes e.g. removing ghost connections where the client has crashed but the server believes the connection still exists. For the complications of using this feature for moderation purposes see [here](security.md#abusive-behavior)

Flat bindings used in this section:

- **Open Settings:** F3
- **Modify User:** Right click over a user in the "Users" tab.

<a name="teams"></a>

## Teams

**Teams** are a way of organizing users. It isn't normally necessary to use them but there are some circumstances where they can be invaluable, like when you have multiple separate teams of players in the same game, either acting in different parts of a shared world or competing against each other. Once players have been added to a team they have access to a team specific voice channel which will prevent their voice chat from being heard by people outside of the team. They also have access to a private text chat channel only for members of the team. Finally the team can be addressed in the scripting language as shorthand for the specific set of players it refers to.

You can manage teams from the **settings** window. The "Teams" tab contains a list of all teams. You can press the binding listed below on teams to edit their members or remove them. To create a new team press the binding listed below and enter the name for your new team. Note that names for teams must adhere to the [naming rules](#naming).

Flat bindings used in this section:

- **Open Settings:** F3
- **Add New Team:** Ctrl+N in the "Team" tab to add a new team.
- **Modify Team:** Right click over a team in the "Team" tab.

<a name="worlds"></a>

## Worlds

**Worlds** are completely separate 3D scenes in LambdaMod, analogous to scenes in Blender. A user can only be looking at one world at a time but physics, AI, etc. continue in all worlds simultaneously, regardless of the presence or absence of users. Worlds are useful for all sorts of tasks. You can create a scratchpad world where you create and test content away from the prying eyes of the players. If the players are traveling between multiple distant locations, for example a city and some ancient ruins many kilometers outside the city walls, you can put each in its own world and **shift** the players between them. You can also use worlds to represent different planes of existence, parallel universes, pocket dimensions, etc.

You can manage worlds from the **settings** window. The "Worlds" tab contains a list of all worlds while the "World" tab contains settings specific to your current world. To change your current world, right click a world in the list and choose "Shift". To create a new world press the binding listed below to summon a dialog box where you can enter the name for your new world. Note that names for worlds must adhere to the [naming rules](#naming).

**Note:** By default worlds are listed publicly and anyone connected to the server can see their names. To prevent this you can go into the "World" tab while shifted into the target world and untick "Shown". This will hide the world from the list so it remains a surprise for the players.

**Note:** Worlds use 64 bit positioning so each can support a space about the size of the solar system without any great difficulty, you should never have to worry about running out of space due to floating point imprecision in LambdaMod.

Flat bindings used in this section:

- **Open Settings:** F3
- **Add New World:** Ctrl+N in the "Worlds" tab to add a new world.

<a name="physical-environment-settings"></a>

### Physical Environment Settings

Worlds have a number of settings to customize how physics behaves within them.

- **Gravity:** Acceleration due to gravity experienced by all entities in the world. You can set this to zero to create space worlds.
- **Flow Velocity:** The rate at which the medium is flowing in the world. This exerts drag on entities and can be used to simulate a strong wind which could move boats with sails or lightweight entities.
- **Density:** The density of the medium in the world. This exerts drag on entities. If you set this to zero entities will experience no drag and once pushed will move forever until they hit something, like in space.

If your world has both non-zero gravity and non-zero density entities will experience a buoyant force which will push them upwards if they are less dense than the medium. This can be used to create hot air balloons, for example.

Some common densities:

| Medium                | Density     |
|-----------------------|-------------|
| Air @ 101 kPa & 20 °C | 1.204 kg/m³ |
| Water                 | 997 kg/m³   |

<a name="visual-environment-settings"></a>

### Visual Environment Settings

Worlds have a number of settings to customize how things look within them.

- **Fog Distance:** The distance at which objects will completely fade to background color due to atmospheric occlusion. A value of zero disables fog. Combining fog with sky effects isn't recommended because objects hidden by fog will still be visible if they are silhouetted against parts of the sky (which is unaffected by fog).

The **sky** is a whole system of settings. The sky has a **base**, which can be one of four types:

- **Blank:** This sky displays nothing but the background color. Useful for space, underground worlds, cloudy nights and generally anywhere where you don't really want a sky.
- **Unidirectional:** This sky displays a series of lines going from the horizon to the upper zenith, and nothing below the horizon. This is the normal sky to use for outdoor worlds.
- **Bidirectional:** This sky displays a series of lines going from the horizon to both the lower and upper zeniths. You can use this one for outdoor scenes too, or for scenes that take place high up in the atmosphere or in a realm of endless sky in all directions.
- **Omnidirectional:** This sky displays a repeating pattern in all directions, making it impossible to tell which way is up. You can use this sky for strange distorted realms, nebulae in space, places shrouded completely in cloud, etc.

On top of the base can be placed a number of **layers**. These layers display other things in the sky beyond the base color and come in four types:

- **Stars:** This layer displays stars scattered randomly across the sky, useful for night time or space worlds.
- **Circle:** This layer displays a circle of specified size at the specified location in the sky. You can use this for the sun, moons, etc. The phase can be used to create a cutout shape, useful for representing moon phases or partial eclipses.
- **Cloud:** This layer displays a cloud of the specified size at the specified location in the sky. You can use this for clouds, smoke, nebulae, etc.
- **Aurora:** This layer displays an aurora above the world. You can use this for arctic worlds.
- **Comet:** This layer displays a comet-like celestial object with a tail and a color that changes from head to tail, useful for representing comets and re-entering spacecraft.
- **Vortex:** This layer displays a rotating vortex of clouds above the world. You can use this for portals, magical rituals and weird science.
- **Meteor Shower:** This layer displays brief flashes of falling meteors across the upper hemisphere of the sky, useful for meteor showers and re-entering space junk.

Each layer is drawn on top of the one behind it so you can partially obscure the moon behind cloud, for example.

The sky isn't just a visual effect, it also casts light into the scene onto all entities within so it's important to adjust the brightnesses correctly. Some common illuminance values:

| Source               | Illuminance |
|----------------------|-------------|
| Full Moon            | 0.2 lx      |
| Indoor Lighting      | 80 lx       |
| Overcast Daytime Sky | 100 lx      |
| Clear Sunrise Sky    | 400 lx      |
| Clear Daytime Sky    | 20000 lx    |
| Sun                  | 100000 lx   |

The **atmosphere** is a whole system of settings. The atmosphere has **lightning**. This displays lightning effects at random around the player. In addition to this the atmosphere has a number of **effects**. These layers display particles streaming past the player, which can be one of five types:

- **Line:** A straight line that experiences both flow velocity and gravity, useful for implying rain or hail.
- **Solid:** A square shape that experiences both flow velocity and gravity, useful for implying snow or ash.
- **Cloud:** A cloud of particles that experience only flow velocity, useful for implying ocean spray, blizzards or sandstorms.
- **Streamer:** A wavy line that experiences only flow velocity, useful for implying high winds.
- **Warp:** A straight line that moves in the specified direction, useful for implying teleportation and space travel.

#### Sky

The sky has the following settings:

- **Enabled:** If enabled the sky will be visible.
- **Seed:** The seed used for all random generation of sky layer components (e.g. star placements).

#### Blank Sky Base

The blank sky base has no settings.

#### Unidirectional Sky Base

The unidirectional sky base has the following settings:

- **Horizon Color:** The color seen at the horizon.
- **Zenith Color:** The color seen at the zenith above.

#### Bidirectional Sky Base

The bidirectional sky base has the following settings:

- **Horizon Color:** The color seen at the horizon.
- **Lower Zenith Color:** The color seen at the zenith below.
- **Upper Zenith Color:** The color seen at the zenith above.

#### Omnidirectional Sky Base

The omnidirectional sky base has the following settings:

- **Color:** The color of the sky.

#### Stars Sky Layer

The stars sky layer has the following settings:

- **Density:** The number of stars in the sky.
- **Color:** The color of the stars.

#### Circle Sky Layer

The stars sky layer has the following settings:

- **Position:** The position of the circle in the sky.
- **Size:** The size of the circle.
- **Color:** The color of the circle.
- **Phase:** The moon phase of the circle.

#### Cloud Sky Layer

The stars sky layer has the following settings:

- **Density:** The number of particles in the cloud.
- **Position:** The position of the cloud in the sky.
- **Size:** The size of the cloud.
- **Inner Color:** The color at the outer edges of the cloud.
- **Outer Color:** The color at the center of the cloud.

#### Aurora Sky Layer

The stars sky layer has the following settings:

- **Density:** The number of aurora curtain particles in the sky.
- **Lower Color:** The color at the bottom of the curtain.
- **Upper Color:** The color at the top of the curtain.

#### Comet Sky Layer

The comet sky layer has the following settings:

- **Position:** The position of the comet in the sky.
- **Size:** The size of the comet.
- **Length:** The length of the comet.
- **Rotation:** The angle of the comet.
- **Head Color:** The color at the head of the comet.
- **Tail Color:** The color at the tail of the comet.

#### Vortex Sky Layer

The vortex sky layer has the following settings:

- **Position:** The position of the vortex in the sky.
- **Rotation Speed:** The speed at which the vortex rotates.
- **Radius:** The radius of the vortex.
- **Inner Color:** The color at the outer edges of the vortex.
- **Outer Color:** The color at the center of the vortex.

#### Meteor Shower Sky Layer

The meteor shower sky layer has the following settings:

- **Density:** The frequency of the meteors.
- **Color:** The color of the meteors.

#### Atmosphere

The atmosphere has the following settings:

- **Enabled:** If enabled the atmosphere will be visible.
- **Seed:** The seed used for all random generation of atmosphere effect components.

#### Lightning Atmosphere Effect

The lightning atmosphere effect has the following settings:

- **Frequency:** The average frequency with which lightning strikes occur.
- **Minimum Distance:** The minimum distance away from the player that lightning strikes appear.
- **Maximum Distance:** The maximum distance away from the player that lightning strikes appear.
- **Lower Height:** The height below the player, along the world up direction, that lightning strikes terminate.
- **Upper Height:** The height above the player, along the world up direction, that lightning strikes originate.
- **Color:** The color of the lightning.

#### Line Atmosphere Effect

A line atmosphere effect has the following settings:

- **Density:** How tightly the particles are packed around the player.
- **Size:** The visual size of the particles.
- **Radius:** The radius of the particles, used to compute the drag force they experience.
- **Mass:** The mass of the particles, used to compute their reaction to physical forces like drag.
- **Color:** The color of the particles.

#### Solid Atmosphere Effect

A solid atmosphere effect has the following settings:

- **Density:** How tightly the particles are packed around the player.
- **Size:** The visual size of the particles.
- **Radius:** The radius of the particles, used to compute the drag force they experience.
- **Mass:** The mass of the particles, used to compute their reaction to physical forces like drag.
- **Color:** The color of the particles.

#### Cloud Atmosphere Effect

A cloud atmosphere effect has the following settings:

- **Density:** How tightly the particles are packed around the player.
- **Size:** The visual size of the particles.
- **Color:** The color of the particles.

#### Streamer Atmosphere Effect

A streamer atmosphere effect has the following settings:

- **Color:** The color of the particles. Using an emissive color is recommended for this effect.

#### Warp Atmosphere Effect

A warp atmosphere effect has the following settings:

- **Density:** How tightly the particles are packed around the player.
- **Size:** The visual size of the particles.
- **Flow Velocity:** The direction and speed with which the warp effect flows.
- **Color:** The color of the particles. Using an emissive color is recommended for this effect.

<a name="audial-environment-settings"></a>

### Audial Environment Settings

Worlds have one setting to customize how things sound within them.

- **Speed of sound:** This adjusts the Doppler shift effect heard when a sound source moves at speed relative to the user. You can see how it effects the calculation [here](https://en.wikipedia.org/wiki/Doppler_effect#General) (it's the constant `c`). It also controls how long sounds emitted by distant sources will take to reach the listener. If you set this value to zero it will disable all sound, this is useful for creating realistic space worlds.

Some common speeds of sound:

| Medium         | Speed of Sound   |
|----------------|------------------|
| Air @ 20 °C    | 343 m/s          |
| Water @ 20 °C  | 1480 m/s         |

<a name="worlds-other-settings"></a>

### Other Settings

Miscellaneous other world settings.

- **Sky Seed:** This setting seeds the random number generator used to generate star, cloud and aurora placement in the sky. You can change it to get different results.
- **Up:** The up direction is used for a variety of functions, from the rotation of the free camera to the placement of users when they are teleported using the **goto** tool. You can change it if your world is orientated differently than the default, for example if the players are climbing up the wall of a building in zero-G you might find it helpful to change the up direction to face away from the wall.

<a name="entities"></a>

## Entities

**Entities** are the backbone of scenarios in LambdaMod. They are roughly analogous to objects in Blender. Everything inside of a world is an entity, all physical or visible objects and everything that emits sound. Entities are complex things with a wide variety of functionality which will be detailed in this section. Every entity is identified by a unique ID which can always be used to refer specifically to it e.g. in the scripting language. To edit entities you will need to switch to **entity mode** from the default **viewer mode**. Once in entity mode you can add an entity then double click it to open the **editor** window. The various tabs of the editor window will let you customize different functionality of the entity.

All the entities in your current world are also listed in the "Entities" tab of the **settings** window. You can use this to find entities you've lost track of and get an overview of your whole world.

**Note:** By default entities are listed publicly and anyone connected to the server in the same world can see their names. To prevent this you can go into the "General" tab and untick "Shown". This will hide the entity from the list so it remains a surprise for the players.

Flat bindings used in this section:

- **Switch Editing Mode:** Tab.
- **Add New Entity:** Shift+A when in entity mode.
- **Open Entity Editor:** Double left click over entity.
- **Open Settings:** F3

<a name="entity-parenting"></a>

### Entity Parenting

Entities can be **parented** to physically weld them together. An entity can be parented to one entity and can have any number of entities as children, but parent relationships can never form loops. Entities which are parented together are physically locked together and move as one. They are also deleted as one and copy-pasted as one, they generally behave in most ways like a single unified object. This is useful for anything where you might have an object made up of many separate parts: a vehicle with a gun mounted on top, a character with a bag on their back, a building with a door. Entities can also be attached to a specific one of their parent's body parts, which will cause them to track that specific body part rather than the entity center. This can be useful for example for a piece of armor attached to a character's forearm, or a trailer being dragged behind a vehicle.

Entities can be parented together using the keybindings listed below while in Entity Mode.

Entity parent relationships can also be controlled from the scripting language via the `entity-parent`, `entity-parent-set` and `entity-parent-clear` functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

Flat bindings used in this section:

- **Parent Selected Entities to Entity Under Cursor:** Ctrl+P.
- **Clear Entity Parent:** Alt+P.

<a name="entity-states"></a>

### Entity States

Entities exist in one of two modes, **active** or **inactive**. You can tell which state an entity is in by looking at the card that appears when you mouse over it, a play icon is displayed for active entities and a pause icon for inactive ones. When editing an entity the entity's current state is displayed prominently at the bottom of the editor window. These states are controlled via a set of buttons at the bottom of the "General" tab of the entity editor window.

The purpose of this system is to allow for non-destructive development and re-use of entities. Imagine for example that you make an entity to represent a specific kind of enemy. You're in a hurry so you drop it into the scenario and have the players fight it without making a backup copy. During the fight you need to quickly edit the enemy, e.g. to maim it in response to one of the player's attacks. By the end of the fight the enemy has been modified significantly, its body has been edited by you and posed by a death animation and its fields for things like health and stamina have hit zero. More disciplined use of LambdaMod could fix this, if you always remember to make backups there's no issue, but this adds another thing game masters have to think about when they should be focusing on providing a good experience for their players. LambdaMod's solution for this is that entities contain essentially two versions of themselves, one for active mode and one for inactive. In active mode entities can play animations and sound, experience physical forces and have their [specially named fields](scripting.md#entity-scripting) invoked. In inactive mode none of these things will occur.

1. You create your entity by editing the inactive version.
2. Once the entity is ready you press "Reset Active State" to copy what you've created into the active version of the entity.
3. The entity is now active and can be used in your scene.
4. Once you're done using your entity you can press "Pause" to return to the inactive version for further editing. If you want to copy the changes that happened to the active version and overwrite the original you can press "Overwrite Inactive State" instead, but usually you just want to pause.
5. When you want to use the entity again you can either press "Play" to return to the state the entity was in when it was last paused, or press "Reset Active State" to replace that version with a fresh copy of the inactive state.

Entity state can also be controlled via the `entity-play`, `entity-pause`, `entity-store` and `entity-reset` functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

**Note:** In summary you should only edit an entity when it's in the inactive state and only use an entity in your scenario or testing when it's in the active mode. Use "Reset Active State" to transition from inactive to active mode and use "Pause" to do the reverse.

<a name="entity-permissions"></a>

### Entity Permissions

LambdaMod has per-entity **permissions** which control which users are allowed to edit the entity. These form two parts a **shared** setting (indicated by a share icon visible on the entity card when enabled) and an **owner** setting (indicated by the username visible on the entity card).

The rules are as follows:

- Users with system-wide admin permissions (from entering the admin password during login) can always edit any entity.
- All other users can only edit:
    - Entities they own.
    - Entities somebody else owns which have the shared setting enabled.
    - Entities nobody owns.

You can customize an entity's permissions in the "General" tab of its entity editor window.

These settings can also be interacted with via the scripting language using various functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

**Note:** These settings are unaffected by the state the entity is in e.g. they do not change when switching from inactive to active or vice versa.

<a name="entity-general-settings"></a>

### Entity General Settings

Entities have a number of high-level settings which adjust various parts of their behavior.

- **Name:** This is the name of the entity, which must adhere to the [naming rules](#naming) for ease-of-use. Entities can be accessed via their name in the scripting language, otherwise the name is purely for the game master's own reference so they can better organize their entities.
- **Shown:** If this is enabled the entity will be listed in the entity list inside the settings window for all users on the server to see.
- **Visible:** If this is enabled the entity will be visible and users will be able to see it.
- **Audible:** If this is enabled the entity will be audible and users will be able to hear sounds emitted by it.

You can customize an entity's general settings in the "General" tab of its entity editor window.

These settings can also be interacted with via the scripting language using various functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

<a name="entity-physics-settings"></a>

### Entity Physics Settings

An entity's physical behavior is influenced by the shape of the entity's body and the settings on individual body parts and also by a number of settings that effect the entity as a whole.

- **Position:** Where the entity is. When set to local this is expressed relative to the entity's parent, if any, otherwise it is expressed relative to the world.
- **Rotation:** The orientation of the entity. When set to local this is expressed relative to the entity's parent, if any, otherwise it is expressed relative to the world. Depending on your settings (see [here](configuration.md#client-configuration) this may be displayed either as three angles representing rotations around the X, Y and Z axises as a quaternion. Internally rotations are always stored as quaternions to prevent issues with [gimbal lock](https://en.wikipedia.org/wiki/Gimbal_lock).
- **Scale:** How much the entity has been resized from its default size. When set to local this scaling applied only to this entity, otherwise it is the total sum of all scaling including that of the entity's parent, if any. Using this isn't recommended but it's provided for completeness sake.
- **Linear Velocity:** The rate of motion of the entity. If the entity is parented to another entity this is the root parent's linear velocity.
- **Angular Velocity:** The direction and rate of rotation of the entity. If the entity is parent to another entity this is the root parent's angular velocity.
- **Physical:** If this is enabled the entity will move in response to physical forces and take part in collisions.
- **Mass:** How heavy the entity is. A value of zero will cause the entity to be rooted in place and never move. This is used for objects that are effectively "infinitely heavy", like the ground on a planet or a building that rests on that ground.
- **Drag:** How much drag the entity will experience. This is the drag coefficient in the [drag equation](https://en.wikipedia.org/wiki/Drag_equation). Cross-sectional area is estimated assuming the smallest unrotated bounding box that surrounds of all the entity's body parts, you can reduce or increase your supplied drag coefficient to adjust for inaccuracies in this estimate.
- **Buoyancy:** How much buoyancy the entity will experience. Buoyancy is calculated using the [buoyancy equation](https://en.wikipedia.org/wiki/Buoyancy#Forces_and_equilibrium). The supplied value acts as a multiplier on the volume, which is estimated as the volume of a sphere with a radius that equals the bounding radius of all the entity's body parts.
- **Lift:** How much lift the entity will experience. Lift is calculated using the [lift equation](https://en.wikipedia.org/wiki/Lift_coefficient#Definitions). Lift acts peripendicular to the direction of flow, upwards or downwards depending on the sign of the coefficient.
- **Magnetism:** How much the entity will be pulled towards or repelled away from magnetic forces.
- **Friction:** How much energy will be lost to friction during collisions that involve this entity.
- **Restitution:** How much energy will be converted into bounciness during collisions that involve this entity.

You can customize an entity's physics settings in the "General" tab of its entity editor window.

These settings can also be interacted with via the scripting language using various functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

<a name="entity-host-settings"></a>

### Entity Host Settings

Entities have a number of high-level settings that control how they behave when acting as a host e.g. when a user is attached to them.

- **Host:** If this is enabled the entity will be available for players to attach to and use as a player character. It's recommended you only enable this on player characters to avoid players accidentally becoming attached to the wrong entities.
- **Field of View:** The horizontal field of view experienced by an attached player. This only applies to players in flat mode, in VR mode the field of view is configured by the VR runtime according to the needs of the headset.
- **Mouse Locked:** If enabled the attached player's mouse will be locked to the center of the screen. This is necessary to implement mouse look and only applies to players in flat mode.
- **Voice Volume:** The volume at which the attached player's voice will be broadcast into the scene. This can be used for example to make a large character like a dragon's voice much louder than a small character like an imp.
- **Tint Color:** The color the edges of the player's view will be tinted. You can use this to communicate status effects, like a red tint to indicate injury or a green one to indicate breathing in poison gas.
- **Tint Strength:** How much the color tint will extend from the edges of the screen.

You can customize an entity's host settings in the "General" tab of its entity editor window.

These settings can also be interacted with via the scripting language using various functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

<a name="entity-fields"></a>

### Entity Fields

Entities appear to the scripting language as a table of fields. Each field has a name and a value and can contain any kind of script value. For example you could create an explosive barrel entity and give it a `health` field which would start at 100 and reduce each time the entity is damaged before it finally hits 0 and blows up. Fields can be triggered by input bindings as described below, or some special fields with unique names might be triggered by the system automatically. You can read more about this in the [relevant section of the scripting manual](scripting.md#entity-scripting).

You can customize an entity's fields in the "Logic" tab of its entity editor window. Two different interfaces are available for doing this: the node editor and the code editor. The nodes to define a field look like this:

![](res/field-nodes.png)

While the code looks like this:

```
(field health 100)
```

Don't forget to save your nodes or code after you've finished editing it and make sure it isn't causing any errors in the console before moving on.

Entity fields can also be interacted with from the scripting language via the `write`, `read`, `remove`, `has?` and `exec` functions which are discussed in more detail in the [standard library section of the scripting manual](scripting.md#standard-library)

<a name="entity-bindings"></a>

### Entity Bindings

Entities which are going to be used as hosts (e.g. have players attached to them) can have bindings which map between various hardware inputs and the names of entity fields. When the specified input is triggered by the attached player the function stored in the named entity field will be executed, if any is available.

For example if we bind the "W" key to a field called `walk-forward` and then we define that field with `(write my-entity-handle 'walk-forward (lambda (input) (print "walk forward!")))` whenever the attached player presses the "W" key it will print "walk forward!" into the system console.

You can customize an entity's bindings in the "Bindings" tab of its entity editor window. To add a new binding simply select an input from the dropdown menu and then enter the name of the field you wish to be executed, then press the "+" button.

You can also add and remove entity bindings from the scripting language using the `entity-binding-add` and `entity-binding-remove` functions which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

<a name="entity-hud"></a>

### Entity HUD

Entities which are going to be used as hosts (e.g. have players attached to them) can have HUD elements which are visible to flat players on the screen and to VR players on a transparent curved panel that hovers in front of them. There are four different kinds of HUD element you can add:

- **Line:** The simplest HUD element, this just displays a solid color line on the HUD. You can use this for drawing widgets such as a reticule in the center of the screen for players in flat mode.
- **Bar:** This displays a bar with a border and a fill, which can range from completely full to completely empty. This can be used for all sorts of metered quantities like health, stamina or mana.
- **Text:** This displays a piece of text in a solid color. This can be used to display other miscellanous information like the number of bullets remaining the player's magazine or the name of the weapon they are currently using.
- **Symbol:** This displays one of a number of builtin symbols. This can be used to indicate information like what magical power the player currently has active.

All of these are designed to be controlled from the scripting language to create dynamic UIs that update automatically. To create a new HUD element simply type its name into the "Add new" field in the top left and press the "+" button. Then select the HUD element from the list and open the settings panel where you can modify its settings. You can also transform the HUD element with the usual bindings for move/rotate/scale although not all HUD elements support all operations. The HUD element's position within the black preview area reflects where it will be positioned on the UI of the player attached to the entity.

To control HUD elements from the scripting language you can use the functions beginning with `entity-hud-` which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

#### All Elements

- **Anchor:** The anchor point of the element. The supplied position will be interpreted as the position of this anchor point.
- **Position:** The position of the element, as a fraction of the width and height of the display respectively.
- **Color:** Th color of the element.
- **Type:** The type of HUD element, one of the element types listed above.

#### Line Element

- **Length:** The length of the line, as a fraction of the width of the display.
- **Rotation:** The rotation of the line.

#### Bar Element

- **Fraction:** The fraction of the bar that's currently filled.
- **Length:** The length of the bar, as a fraction of the width of the display.
- **Right to Left:** By default the bar will fill from the left to right. Enabling this causes it to fill from right to left instead.

#### Text Element

- **Text:** The actual text displayed on the element.

#### Symbol Element

- **Type:** The specific symbol pattern displayed on the body part. Only one is currently available as a proof of concept.
- **Size:** The displayed size of the symbol, as a fraction of the width of the display.

Flat bindings used in this section:

- **Toggle HUD Element Settings Panel:** Tab.
- **Move HUD Element:** G.
- **Rotate HUD Element:** R.
- **Scale HUD Element:** S.

<a name="entity-body"></a>

### Entity Body

An entity's physical shape is called its body. This serves as the entity's visual appearance, its physical shape used for collisions and basis for all animations of the entity. An entity's body is made up of parts which are each taken from a kind of primitive. The primitives that are currently available in LMOD are as follows:

- **Line:** A line is a simple line, used to draw basic shapes like the outline of a sword.
- **Plane:** A plane is a flat 2D shape with an outline made of lines and a solid interior in the background color (by default black). The solid interior can be toggled on and off and planes are available in many different shapes: circle, ellipse, triangle, rectangle, hexagon, etc.
- **Anchor:** An anchor is an invisible marker that denotes some special location on the entity. Some anchors have special meanings relating to attached players while others mark locations where the entity will emit bullets from, play sounds from or other custom behaviors. This body part has no physical presence.
- **Solid:** A solid is a 3D solid primitive in one of the common shapes: sphere, cuboid, cylinder, cone, capsule, pyramid, tetrahedron, octahedron, dodecahedron or icosahedron. It has a solid interior in the background color (by default black) and edges made of lines.
- **Text:** A text is a 2D display with some text. This body part has no physical presence.
- **Structure:** A structure is a series of additive and subtractive geometry operations which create an artificial structure like a castle, house or dungeon. The structure can be above ground or carved out of the underground.
- **Terrain:** A terrain is a graph of points in 3D space around which a terrain is constructed. The terrain can be tunnels/caves or a landscape or a mixture of the two.
- **Symbol:** A symbol is a 2D display with some symbol from the symbol library. This body part has no physical presence.
- **Canvas:** A canvas is a 2D display with some user drawing. The drawing is binary (e.g. pixels either have color or no color, all colored pixels are the same color) to reduce the art target and limit network bandwidth usage. This body part has no physical presence.
- **Light:** A light is an invisible marker that denotes a position on the entity that emits light, illuminating the entity itself and other nearby entities. This body part has no physical presence.
- **Force:** A force is an invisible marker that denotes a position on the entity that emits force. This force will move other entities in the environment. This body part has no physical presence, except for the force it applies.
- **Area:** An area is an invisible polygonal volume that denotes a region of space where world properties are altered. This could be a vacuum chamber, a body of water, or an area of artificial gravity. It can only have an optional surface to indicate its presence. This body part has no physical presence, except for the effect it has on the world properties.

Further details of the part primitives are provided below.

Parts can be parented to one other the same as entities can, which has the same effects, but with an additional feature where a constraint can be defined. This allows body parts some freedom of movement while remaining attached to one another, for example for modeling ball and hinge joints. This is the basis of animation, allowing segments of limbs to move relative to one another, and also useful for physical effects like constructing a chain made of physically simulated links joined together. The constraints that are currently available in LMOD are as follows:

- **Fixed:** A fixed constraint prevents all movement.
- **Ball:** A ball constraint allows the body part to roll around the constraint on multiple axises like a shoulder or hip joint.
- **Cone:** A cone constraint allows the body part to rotate freely within a conical shaped region.
- **Hinge:** A hinge constraint allows the body part to rotate around the constraint on one axis like an elbow or knee joint.

Further details of the constraints are provided below.

You can customize an entity's body in the "Body" tab of its entity editor window. Once this tab is opened the 3D editor will switch to Body Mode as denoted by the status text in the bottom left and you can click on the individual parts within the entity to select them. Once one or more parts have been selected their settings will appear in the entity editor window for you to modify.

You can access the position and rotation of an entity's generic anchor primitives by their names using the `entity-body-anchor` function, which is discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

#### All Primitives

These settings are common to all body part primitives.

- **Type:** The type of the body part. One of the primitive types listed above.
- **Constraint Type:** The type of constraint used to attach this body part to its parent. One of the constraint types listed above.
- **Position:** The position of the body part. When set to local this is expressed relative to the centerpoint of the entity, along the entity's axises (which may be rotated relative to the world axises).
- **Rotation:** The rotation of the body part. When set to local this is expressed relative to the rotation of the entity.
- **Scale:** How much the body part has been resized from its default size. When set to local this scaling applied only to this body part, otherwise it is the total sum of all scaling including that of the entity, if any. Using this isn't recommended but it's provided for completeness sake.
- **Mass:** The independent mass of the body part. This is used to define the masses of parts which move independently from the rest of the entity e.g. because they are joined by constraints with some degree of freedom. The mass is subtracted from the overall mass of the entity. This field is not meaningful if the body part a "Fixed" type constraint.
- **Max Force:** The maximum force that can be exerted by the constraint as part of animation. For example if this body part were the forearm of a character, connected to the upper arm via a hinge joint at the elbow, this would control the mass the arm could lift by bending the elbow joint. This field is not meaningful if the body part a "Fixed" type constraint.
- **Max Velocity:** The maximum velocity that can be used while animating a constraint. For example if this body part were the forearm of a character, connected to the upper arm via a hinge joint at the elbow, this would control how fast the elbow joint would rotate when told to extend the arm as fast as possible. This field is not meaningful if the body part a "Fixed" type constraint.

#### Line Primitive

The line primitive has the following settings:

- **Diameter:** The diameter of the line used for collision. Has no visual effect.
- **Length:** The length of the line.
- **Start Color:** The color at the start of the line. The color will blend into the end color along the line's length.
- **End Color:** The color at the end of the line. The color will blend into the start color along the line's length.
- **Constraint At End:** By default any constraint applied to the line will act from the start. Enabling this causes it to act from the end instead.

#### Plane Primitive

The plane primitive has the following settings:

- **Plane Type:** The type of plane shape, one of:
    - **Ellipse**
    - **Equilateral Triangle**
    - **Isosceles Triangle**
    - **Right-angle Triangle**
    - **Rectangle**
    - **Pentagon**
    - **Hexagon**
    - **Heptagon**
    - **Octagon**
- **Thickness:** The thickness of the plane used for collision. Has no visual effect.
- **Width:** One of the dimensions of the plane. May not be available for every plane type.
- **Height:** One of the dimensions of the plane. May not be available for every plane type.
- **Color:** The color of the lines at the plane's edges.
- **Constraint Index:** The sub part that the constraint acts around. This is one of the body part's corners, visible when in body mode.
- **Empty:** If enabled the plane won't have an area of background color in its center.

#### Anchor Primitive

The anchor primitive has the following settings:

- **Anchor Type:** The type of anchor.
    - **Generic:** An anchor with no special meaning, used for custom purposes by the game master.
    - **Viewer:** The placement of the camera when a flat mode player attaches to the entity. Usually in front of the eyes.
    - **Room:** The placement of the room center when a VR mode player attaches to the entity. Usually on the ground in between the feet.
    - **Grip:** Where the automatic VR gripping system acts from, allowing users to pick up, drop, throw and "pull" (e.g. a Half-Life: Alyx gravity gloves style pickup mechanic) entities. Usually one is placed in each hand.
    - **Left Hand, Right Hand, Left Foot, Right Foot, Hips, etc.:** These are used to control the entity's limbs in VR using controller tracking, hand tracking, full body trackers, etc. Anchors will only be used if the player is in VR mode and has a tracking source available for that particular body part.
- **Name:** A name used to distinguish multiple generic anchors. Has no effect on non-generic anchors.
- **Mass Limit:** The maximum mass that can be pulled using this grip anchor. A value of zero disables the pull mechanic. Has no effect on non-grip anchors.
- **Pull Distance:** The maximum distance an entity can be pulled from using this grip anchor. Has no effect on non-grip anchors.

#### Solid Primitive

The solid primitive has the following settings:

- **Type:** The type of solid shape, one of:
    - **Sphere**
    - **Cuboid**
    - **Cylinder**
    - **Cone**
    - **Capsule**
    - **Pyramid**
    - **Tetrahedron**
    - **Octahedron**
    - **Dodecahedron**
    - **Icosahedron**
- **Width:** One of the dimensions of the solid. May not be available for every solid type.
- **Length:** One of the dimensions of the solid. May not be available for every solid type.
- **Height:** One of the dimensions of the solid. May not be available for every solid type.
- **Constraint Index:** The sub part that the constraint acts around. This is one of the body part's corners, visible when in body mode.

#### Text Primitive

The text primitive has the following settings:

- **Text Type:** The font used to display the text. Only one is currently available as a proof of concept.
- **Text:** The actual text displayed on the body part.
- **Height:** The displayed height of the text.
- **Color:** The color of the text.

#### Symbol Primitive

The symbol primitive has the following settings:

- **Symbol Type:** The specific symbol pattern displayed on the body part. Only one is currently available as a proof of concept.
- **Size:** The displayed size of the symbol.
- **Color:** The color of the symbol.

#### Canvas Primitive

The canvas primitive has the following settings:

- **Size:** The displayed size of the canvas.
- **Color:** The color of the canvas.

#### Structure Primitive

The structure primitive has the following settings:

- **Positive:** If enabled the structure will be built up like an open-air structure, otherwise it will be carved out of solid material like an underground structure.
- **Color:** The color of the structure.

##### Structure Type

Structures contain types which identify varieties of wall that structures are made of. These types have their own settings:

- **Thickness:** The thickness of the wall.

##### Structure Node

Structures contain nodes which describe the layout of the structure. These nodes have their own settings:

**TODO:** Structure Node

#### Terrain Primitive

The terrain primitive has the following settings:

- **Color:** The color of the terrain.

##### Terrain Node

Terrain primitives are primarily comprised of their nodes, which draw the shape of the terrain as a graph. These nodes have their own settings:

- **Position:** The position of the node in 3D space.
- **Radius:** The radius of the node.
- **One-sided:** By default a terrain node will generate terrain all around itself, creating a cave-like structure. Enabling this will cause it to generate terrain on only one side, useful for creating open-air landscapes.

#### Light Primitive

The light primitive has the following settings:

- **Angle:** The angle of the cone of light emitted.
- **Color:** The color of the light emitted. This is a HDR color with an intensity to represent the full range of light brightnesses found in nature.

#### Force Primitive

The force primitive has the following settings:

- **Enabled:** If enabled the force will act upon things in the environment, otherwise it will have no effect.
- **Type:** The type of force, one of:
    - **Linear:** A force that pushes or pulls entities in a specific direction. Useful for representing linear force fields.
    - **Omnidirectional:** A force that pushes or pulls entities in all directions around itself. Useful for representing spherical force fields.
    - **Fan:** A force that pushes or pulls entities in a specific direction via accelerating the medium. Useful for representing a hovercraft's rotors and the downwash they create on other entities.
    - **Rocket:** A force that pushes or pulls entities in a specific direction via emitting mass. Useful for representing a spacecraft's engines and the force they exert on anything behind them.
    - **Gravity:** A force that pulls or pushes entities in all directions around itself, proportional to their mass. Useful for representing the gravity of a celestial body.
    - **Magnetism:** A force that pulls or pushes entities around itself in a pattern like a magnetic dipole. Useful for representing large magnets.
- **Intensity:** The strength of the force.
- **Radius:** The radius around the part center in which the force acts.
- **Mass Flow Rate:** The rate at which mass is emitted by a force using the rocket mechanism.
- **Reaction:** By default the force will only push other entities, not itself. Enabling this causes any actions taken by the force to have an equal but opposite reaction on itself.

#### Area Primitive

The area primitive has the following settings:

- **Height:** The height of the area.
- **Speed of Sound:** An alternative speed of sound to be used instead of the world speed of sound when inside this area.
- **Gravity:** An alternative gravitational acceleration to be used instead of the world gravitational acceleration when inside this area.
- **Flow Velocity:** An alternative flow velocity to be used instead of the world flow velocity when inside this area.
- **Density:** An alternative density to be used instead of the world density when inside this area.
- **Up:** An alternative up direction to be used instead of the world up direction when inside this area.
- **Fog Distance:** An alternative fog distance to be used instead of the world fog distance when inside this area.
- **Sky:** An alternative sky to be displayed instead of the world sky when inside this area.
- **Atmosphere:** An alternative atmosphere to be displayed instead of the world atmosphere when inside this area.
- **Surface:** By default the area will be visible. Enabling this causes a rippling grid of lines to be displayed on the top surface, useful for representing bodies of liquid.
- **Color:** The color of the lines on the surface of the area.
- **Opaque:** By default the surface of the area will be transparent between the lines. Enabling this causes the area between the lines to be filled the background color.
- **Animation Speed:** The rate at which ripples will move across the area's surface.

#### Fixed Constraint

The fixed constraint has no settings.

#### Ball Constraint

The ball constraint has the following settings:

- **Minimum:** The minimum angles permitted on the constraint.
- **Maximum:** The maximum angles permitted on the constraint.

#### Cone Constraint

The cone constraint has the following settings:

- **Span:** The angular width and height of the constraint's cone region.
- **Twist Minimum:** The minimum twist angle (rotating around the cone axis) permitted on the constraint.
- **Twist Maximum:** The maximum twist angle (rotating around the cone axis) permitted on the constraint.

#### Hinge Constraint

The hinge constraint has the following settings:

- **Minimum:** The minimum angle permitted on the constraint.
- **Maximum:** The maximum angle permitted on the constraint.

<a name="entity-sounds"></a>

### Entity Sounds

Entities can emit sounds, which is where all custom sounds in LMOD originate from. The original sounds are called samples. Samples are intended to be authored in the entity editor window and then played from the scripting language in response to events happening in the game. Samples contain notes which are bursts of sound with a given length, timing, volume and other parameters. Notes belong to one of a number of primitive types which create different sounds. The primitives that are currently available in LMOD are as follows:

- **Tone:** This generates a sine wave tone at the user-supplied frequency.

Further details of the note primitives are provided below.

You can customize an entity's samples in the "Sound" tab of its entity editor window. To create a new sample enter a name for it into the "Add new" field in the top left then press the "+" button. Then select the sample from the list on the left and add a note to it. Once you've added a node you can open the settings panel and start editing the settings of the note, customizing it to your liking. You can use the buttons at the bottom of the window to listen to your sound. This will use the normal sound playback system and create noise in the world that other users can hear so make sure your entity is far from any users you don't want to hear the sound and close to yourself.

To control entity sounds from the scripting language you can use the functions beginning with `entity-sample-` which are discussed in more detail in the [game library section of the scripting manual](scripting.md#game-library).

Flat bindings used in this section:

- **Add Note:** Shift+A.
- **Move Note:** G.
- **Toggle Sample Note Settings Panel:** Tab.

#### Tone Primitive

The tone primitive has the following settings:

- **Pitch:** This sets the frequency of the sine wave generated.

<a name="entity-animations"></a>

### Entity Animations

**TODO:** Entity animations

<a name="text-tools"></a>

## Text Tools

Text tools are a core part of LambdaMod that serve a wide variety of purposes. Most of them allow you to display some piece of text hovering in space in front of the players, with differing levels of interactivity. The text appears only for the targeted players and cannot be seen by anyone else. You can choose them for dialogue with NPCs, to interact with things in the environment that can't easily be represented physically or to give the players choices when creating their characters or leveling up. Each tool can be targeted all the players or only a subset of them.

If a text tool has any kind of user input then whenever a player confirms their choice it will be displayed to you in your system log, accessible in the **console**. It is only visible to you (as the user who initiated the text tool) and no one else, so you can allow players to make secret choices which the other players are not privy to.

You can check what text tools are currently active on a user in the "Status" tab of the new text tool.

Flat bindings used in this section:

- **Open New Text Tool:** F4
- **Open Console:** Backquote

<a name="text-signal"></a>

### Signal

The signal text tool is a binary toggle. When enabled it displays an icon representing text in front of the player. This is intended to be used to signal to the players that whatever dialogue they're taking part in is ongoing even if no text is currently visible (e.g. while you are typing the next part). When a player enters into dialogue with an NPC switch the signal on and leave it on until the dialogue has definitively finished.

<a name="text-titlecard"></a>

### Titlecard

The titlecard text tool is used to set the stage and announce significant milestones to the player. It consists of a title and subtitle and hovers in space out in front of the player at some distance. It appears with a dramatic noise which signals its arrival. Some suggested uses:

| Use                                                             | Title                          | Subtitle                 |
|-----------------------------------------------------------------|--------------------------------|--------------------------|
| Introducing a new location after travel                         | New York                       | 1700 hours               |
| Telling the players how long their characters have been waiting | 5 Hours Later...               | No subtitle.             |
| Announcing the start of a quest and their goal                  | Blood in the Water             | Venture into the sewers  |
| Announcing a story milestone                                    | Act II: The Gathering Darkness | No subtitle.             |
| Announcing a gameplay milestone                                 | Level Up!                      | No subtitle.             |

<a name="text-timeout"></a>

### Timeout

The timeout text tool presents a message with a title and some text that disappears after a length of time that you choose.

| Use                                                                        | Title               | Text                                                          |
|----------------------------------------------------------------------------|---------------------|---------------------------------------------------------------|
| Informing the player of things that can't be easily communicated otherwise | Tremor              | The ground shakes beneath your feet.                          |

<a name="text-unary"></a>

### Unary

The unary text tool presents a message with a title and some text and a single button underneath which says "OK". You can use this for simple interactions of various kinds.

| Use                              | Title               | Text                                                          |
|----------------------------------|---------------------|---------------------------------------------------------------|
| Simple dialogue interactions     | Alice               | Bye for now, adventurer!                                      |
| Simple non-dialogue interactions | Ancient Contraption | The machine is broken. There is nothing more you can do here. |

<a name="text-binary"></a>

### Binary

The binary text tool presents a message with a title and some text and two buttons underneath, one reading "Yes" and the other reading "No".

| Use                              | Title         | Text                                                                                                                |
|----------------------------------|---------------|---------------------------------------------------------------------------------------------------------------------|
| Simple dialogue interactions     | Bob           | Hey, are you ready to go?                                                                                           |
| Simple non-dialogue interactions | Broken Engine | The engine has gone silent and is gently smoking. Do you want to try repair it, at the risk of breaking it further? |

<a name="text-options"></a>

### Options

The options text tool presents a message with a title and some text and one or more buttons underneath. Unlike the preceding two text tools you can specify how many buttons there are and what they say. You can use this for classic CRPG-style dialogue trees.

<a name="text-choose"></a>

### Choose

The choose text tool has a title and any number of options: the player selects up to a number of them chosen by the game master. This can be used for some non-dialogue interactions, like choosing the floor in an elevator, but is mostly intended for meta mechanics like choosing a two new perks from a list of many when the player's character levels up.

<a name="text-assign"></a>

### Assign

The assign text tool has any number of keys and the same number of values: the player pairs them up. This is intended for meta mechanics e.g. when outfitting the players' spaceship it may have four slots which can take any upgrade module of the player's choosing.

<a name="text-point-buy"></a>

### Point Buy

The point buy text tool has any number of items with prices and a number of points: the player can buy each item once as long as they have enough points. They do not have to spend every point. This is intended for meta mechanics e.g. buying character features during character creation with abstract currency.

<a name="text-point-assign"></a>

### Point Assign

The point assign text tool has any number of items and a number of points: the player assigns varying numbers of points to the items. They do not have to spend every point. This is intended for meta mechanics e.g. assigning basic attribute scores during character creation.

<a name="text-vote"></a>

### Vote

The vote text tool makes a vote with any number of options, a timeout you specify and a number of votes which should match the number of players who will be voting. Each player votes for one option. This is intended to be used for organizational purposes e.g. polling the players on whether or not they would like to revert to the last checkpoint.

<a name="text-choose-major"></a>

### Choose Major

The choose major text tool is similar to the choose tool above. The player is presented with any number of items and picks one. Unlike the choose tool however each item is displayed in a larger box with a title, subtitle and description. This is intended for use in meta mechanics for critical choices that require more weight and more information, like choosing their character's species or class.

<a name="text-knowledge"></a>

### Knowledge

The knowledge text tool allows you to add items to the player's knowledge banks. They aren't notified when new knowledge is added but they can open their knowledge viewer and review this information at any time. You should use this to record things they might forget between sessions or to add details about the world that their character would know. Knowledge items have a title, a subtitle and a body of text. These knowledge items are stored on a per-player basis and follow them if they change character. Newer knowledge items with the same title replace older ones.

<a name="text-clear"></a>

### Clear

The clear text tool removes previous text tools that are still active on the target players. If the "Clear Knowledge" option is ticked it will also empty a player's memory banks. This can be useful if a player's character has died and they're switching to a new one, or just if you decide you made a mistake.

<a name="user-controls"></a>

## User Controls

LambdaMod's greatest enemy is the inherent messiness of the video game format. One player can run off without the others even noticing, players can accidentally throw a grenade into a group of plot critical NPCs, players can stumble into unfinished areas, etc. These problems don't exist in tabletop games as a natural result of the format: each player is always aware of the actions of the others because they are spoken aloud around the table, no accidental inputs can derail a session because every event goes through multiple human brains for sanity checking first.

Fortunately, LambdaMod provides a rich suite of tools for managing these issues. These are called user controls and they take the form of various meta-game mechanics you can use to shape the player's behavior towards an experience that's enjoyable for everyone.

<a name="user-control-attachment-lock"></a>

### Attachment Lock

By default players can attach to and detach from entities at will. This is probably not what you want because once unattached they can move around the environment with impunity as a ghostly fly camera, spoiling future parts of the adventure for themselves. To prevent this you can turn on **attachment lock**. You can turn this on from the very start of a session because attachments are locked players can still attach to entities, but once attached they cannot detach until the lock is released.

<a name="user-control-magnitude"></a>

### Magnitude

LambdaMod's main solution to the above problems is **magnitude**. The session can be in one of four magnitude levels: 0, 1, 2 or 3. The levels are as follows:

- **Magnitude 0:** All player input is ignored, the game is effectively paused.
- **Magnitude 1:** Only reversible actions are available, like walking around and talking to people.
- **Magnitude 2:** Some minor irreversible actions are available, like accepting quests or crafting items.
- **Magnitude 3:** All actions are available, nothing is restricted.

All players get a vote on the magnitude, which they can adjust at any time. The overall magnitude of the session is the lowest value that gets at least one vote. As the game master you can set a maximum allowed value which can change at any time. The core concept is that anyone can lower the magnitude but only a consensus by all users can raise it. This applies to you as well, you can lower the magnitude by lowering the maximum, but raising the maximum will have no effect until the players raise their own votes to match.

It's recommended that you set an appropriate magnitude limit for the activity the players are currently doing.

- If you aren't ready for the players to act at all the magnitude limit should be set to **0**.
- If the players are engaging in a peaceful exploration or social scene the magnitude limit should be set to **1**.
- If the players are engaged in anything short of combat or a similar life-threatening situation (meeting with dangerous individuals, exploring an unknown environment, sailing across the seas) the magnitude limit should be set to **2**.
- If the players are engaged in combat or a similar life-threatening situation the magnitude limit should be set to **3**.

Magnitude levels 0 and 3 work automatically, at level 0 the game doesn't run at all while at level 3 no restrictions are applied. For levels 1 & 2 to work however you will need to support them when creating entities. You can do this with the `magnitude-with` function, which will prevent certain code from executing below the specified magnitude level, printing a warning to the system log instead.

You can check a user's magnitude in the "Users" tab of the settings window.

<a name="user-control-advantage"></a>

### Advantage

**Advantage** is a metacurrency you can award to the players or take away from them at any time. The purpose of advantage is to provide a buffer around failure. In a tabletop game players will often go tens or hundreds of hours without a death amongst the group. This creates great story continuity but is very hard to achieve in a game like LambdaMod without making the gameplay trivial. Advantage is a solution to this. Whenever the player would suffer a critical injury you can instead remove an advantage point (even going below zero, at which point it imposes a penalty) and discount the injury.

Advantage provides players with a stacking 20% bonus to some of their character's attributes. It's up to as the game master to decide which attributes. While creating the players' characters you should use the `advantage-apply` function to apply the multiplier to a desired value. This is done so players will value advantage although do be aware that if advantage has too significant an effect on the player character's abilities it may create a death spiral effect where losing advantage causes more injuries which causes more lost advantage and so on.

Advantage should be given to the players whenever they make progress in the game e.g. when they kill a group of enemies, when they uncover a secret clue, when they successfully escape from the monster chasing them. This can also be automated within the player character's code with the `advantage-give` function.

Advantage should be removed from the players whenever they would have died or suffered a debilitating injury. If a player is at zero advantage you can continue to subtract it into negative values, which will apply an inverse multiplier to the player character's stats instead. This can also be automated within the player character's code with the `advantage-take` function.

You can check or adjust a player's advantage from the "Users" tab of the settings window.

Flat bindings used in this section:

- **Open Settings:** F3

<a name="user-control-doom"></a>

### Doom

One problem that can be difficult to manage is the "silent alarm problem". This occurs when the players get themselves into a negative situation and there is a significant delay between them making the mistakes that got them there and the consequences arriving. This can make the consequences feel arbitrary or unfair. I call it the silent alarm problem because a silent alarm is the most obvious example: imagine a group of players carrying out a heist in an art gallery. On their way in someone trips a motion detector and a silent alarm is triggered. 20 minutes later the police arrive and the players are arrested, never having realized anything was wrong. This problem exists in tabletop games as well but it's worse in LambdaMod because the players are much more error prone when they have to do everything in real time with video game mechanics.

**Doom** is a mechanic LambdaMod introduces to try and counteract this. Whenever the players get themselves into a situation where they are in significant peril without realizing it, you should set their doom flag. This will display the word "DOOM" in the top-left of their HUD, alongside their magnitude and advantage. This signals that something is wrong and they should re-evaluate their current situation without telling them exactly what. At best they figure out what's gone wrong and correct it and at worst the consequences will feel less arbitrary when they arrive.

Once consequences do arrive, e.g. the players have heard the police cars pull up outside, you should clear their doom flag. This communicates that this is the specific event the doom flag was presaging and helps the players better understand where they went wrong. You should also clear the doom flag whenever the players successfully manage to evade the consequences entirely e.g. if they decide to leave the art gallery entirely and hide elsewhere.

<a name="user-control-ping"></a>

### Ping

The **ping** control is a way for players to contact you e.g. when they would like to do something that makes sense in the game world but for which game mechanics haven't been set up, like writing a message on a wall. When they trigger it an exclamation mark will appear in the world marking their position and a sound will play. When this happens you should tend to them and see what they need help with.

<a name="user-control-brake"></a>

### Brake

The **brake** control is a more extreme version of the ping control. When a player uses it you will be ping as normal (but with a different symbol and sound) but the game will also rapidly slow to a stop, freezing the passage of time. Players use this when they need to contact you with a time critical request e.g. they would like to do something that isn't mechanically supported and they are in the middle of combat. Once you have dealt with the player's request you must remember to release the brake, which only you can do.

Flat bindings used in this section:

- **Release Brake:** Ctrl+Space

<a name="user-control-fade"></a>

### Fade

When toggled on the **fade** control darkens the screen a group of players, rendering them unable to see anything. This is intended to be used to signify transitions e.g. when the players' characters go to sleep for the night or travel a long distance "off-screen". Their view will fade out slowly and then fade back in again when you disable the control.

Flat bindings used in this section:

- **Open User Controls Tool:** F5

<a name="user-control-wait"></a>

### Wait

When toggled on the **wait** control displays a spinning wait symbol to a group of players. For players in flat mode this symbol will appear in the center of their screen, while for VR players it will be on the floor surrounding the center of their playspace. Use this to signal to the players when you need them to wait while you prepare the next part of the adventure. You can check if a user is currently being told to wait in the "Status" tab of the user controls tool.

Flat bindings used in this section:

- **Open User Controls Tool:** F5

<a name="user-control-goto"></a>

### Goto

The **goto** control teleports a group of players. They appear in a circle of the specified radius around the specified point, facing inwards. Use this to move the players between different areas. Usually you should use the fade tool first to hide the teleportation for a less jarring effect.

Flat bindings used in this section:

- **Open User Controls Tool:** F5

<a name="user-control-tether"></a>

### Tether

The **tether** control binds a group of players together. They will each see a dashed line leading towards the center of the group which gets brighter the further apart they are. The further apart they are the tether control will slow their movement as a means of keeping the party together. The distance parameter controls the distance the tether control will try limit the players to. Use this as a means of stopping the players from becoming separated when they are moving through large environments. You can check if a user is currently tethered in the "Status" tab of the user controls tool.

Flat bindings used in this section:

- **Open User Controls Tool:** F5

<a name="user-control-confine"></a>

### Confine

The **confine** control confines a group of players to an area. The area is spherical with the given radius around a central point. The confinement is visible as a sphere made of crosses that gets brighter as the players approach its edge. Use this as a means of keeping the players in a specific area e.g. because you haven't built the next area yet or they have agreed to deal with one part of the encounter first before proceeding. You can check if a user is currently confined in the "Status" tab of the user controls tool.

Flat bindings used in this section:

- **Open User Controls Tool:** F5

<a name="user-control-checkpoints"></a>

### Checkpoints

**Checkpoints** are a special type of control. You can create one at any time via the settings window and then at any later point you can revert to it, reverting everything in the game to how it was when the checkpoint was created. Rolling back over long spans of time isn't recommended as you run the risk of undoing too much progress, and rolling back too often isn't recommended either: part of the fun of roleplaying games is dealing with the failures. So you should make checkpoints often and only revert to them rarely. Still, sometimes things can go so badly wrong that reverting is less disruptive than trying to deal with it in-game.

**Note:** Checkpoints can also be useful if you're testing stuff in a world by yourself, you can make a checkpoint with everything set up for a destructive test and then run the test before reverting to make changes and run it again.

Flat bindings used in this section:

- **Open Settings:** F3
- **New Checkpoint (with checkpoint settings focused):** Ctrl+N

<a name="user-control-time-multiplier"></a>

### Time Multiplier

The **time multiplier** can be used to control the rate at which time passes in the game. You can adjust this via the "Game" tab of the settings window. Setting it to 0 will **pause** the game. You can use this as a sort of "game master's brake" to give yourself more time to respond. Setting it to a number >0 and <1 will cause time to move **slow**, this can make things easier to the players, giving them more time to react e.g. in combat. Setting it to a negative value will cause the game to **rewind** until the limit of the server's history window is reached (the amount of history the server keeps stored for the purposes of network lag compensation and rewinding). You can use this to undo things that have immediately gone wrong within the game, like a player character slipping into a ravine.

Flat bindings used in this section:

- **Open Settings:** F3

<a name="user-control-splash-message"></a>

### Splash Message

The **splash message** isn't a proper user control but it can be useful for shaping player behavior. By default when a player joins a LambdaMod server they will see the default LambdaMod splash message with some quick tips on how to play the game. After dismissing that they will see your splash message. You can use your splash message to tell players what to do after joining or to inform them of the rules of your server. The splash message can be configured from the "Game" tab of the settings window.

Flat bindings used in this section:

- **Open Settings:** F3

<a name="utilities"></a>

## Utilities

LambdaMod includes a variety of utilities that are similar to those you might be used to using in tabletop games.

The **quick accessor** is a fuzzy find tool that you can use to quickly access different parts of LambdaMod. After opening it just start typing and then confirm once the action you want has floated to the top of the list.

The **document manager** allows you to take notes and keep track of things between sessions. These notes are stored in the server's save file and are private: the players cannot see your notes, nor are they transmitted to their computers.

The **console** provides access to almost all of LambdaMod's functionality. From the console you can create and delete entities, configure user controls, create text events and almost anything else LambdaMod can do. All of this is scriptable, you can write functions that do multiple different tasks together and set them to execute after a set delay. Also the console functions as a calculator where you can do calculations for your game. Finally, it also functions as a dice roller through the `dice` and `zdice` functions. The `dice` function rolls normal dice so 3d6 becomes `(dice 3 6)` while `zdice` functions the same but for dice that start from zero, for example 1d10 becomes `(zdice 1 10)`.

The **oracle manager** automates random tables which you can roll on to decide things for your session. These random tables are pulled from the `res/oracles` directory of your LambdaMod server and you can easily add custom ones by just adding additional files. The final is a standard [YAML](https://en.wikipedia.org/wiki/YAML) file in the following format:

```yml
name: Random Object
columns:
  - title: Color
    options:
      - Red
      - Green
      - Blue
  - title: Material
    options:
      - Metal
      - Plastic
      - Wood
  - title: Shape
    options:
      - Key
      - Car
      - Boot
```

When you roll on this oracle it will select one item from each column at random, for example it might tell that the object the players have found is a Green Plastic Car.

Flat bindings used in this section:

- **Open Quick Accessor:** F1
- **Confirm Quick Accessor:** Enter
- **Open Document Manager:** F2
- **Open Console:** Backquote
- **Open Oracle Manager:** F6

**TODO:** Game master's guide
