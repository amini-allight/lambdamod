# State

This guide covers where LambdaMod stores persistent state. This is important because if you ever want to reset your LambdaMod to factory settings you need to know what files it's reading when it starts up. For details of the internal layout of (some of) these files, see the [configuration documentation](configuration.md).

## Contents

1. [Save Files](#save-files)
2. [Configuration Files](#configuration-files)
3. [Client-only Files](#client-only-files)
4. [Cache Files](#cache-files)

<a name="save-files"></a>

## Save Files

The LambdaMod server creates the save file, created at the path specified when launching the server. This file is just a large YAML document and you are encouraged to create tools that interact with it e.g. procedurally generating a level by writing it into the save file.

<a name="configuration-files"></a>

## Configuration files

When the LambdaMod server or client start up they look for a configuration file in the following directory:

| Platform | Path                                                      |
|----------|-----------------------------------------------------------|
| FreeBSD  | `~/.config/lambdamod/`                                    |
| Linux    | `~/.config/lambdamod/`                                    |
| Windows  | `%APPDATA%\LambdaMod\`                                    |
| MacOS    | `~/Library/Application Support/LambdaMod/`                |

The file is called `server.yml` for the server and `client.yml` for the client. If the program doesn't find a pre-existing configuration file it creates one containing all of the default settings.

<a name="client-only-files"></a>

## Client-only Files

All of these files are stored in the same directory as the configuration files above.

The client also creates an additional configuration file upon connection, `client-connection.yml`, which contains the most recently used connection parameters. These are used to autofill the main menu for quick reconnection in future. Note that this file contains the used connection passwords in **unencrypted plain text**, so you should choose passwords that you don't mind being stored in this way on your device. LambdaMod passwords are intended to prevent spam and trivial impersonation, not withstand a concerted malicious attack.

The client can also accept a theme configuration file (`client-theme.yml`), a string localization file (`client-localization.yml`) and an input bindings configuration file (`client-bindings.yml`) but the client does not create any of these itself.

<a name="cache-files"></a>

## Cache Files

The LambdaMod client renders everything in the scene with graphics pipelines, which are logical objects describing a combination of shaders and other rendering state. These pipelines can take a while to build for your particular graphics hardware and driver version so they are stored in a cache file for re-use on subsequent runs.

**Note:** The format of the cache file is specified by your graphics driver's implementation of `VkPipelineCache` and is opaque to LambdaMod.

The file is stored at the following locations, you may wish to delete it if you experience unexplained issues e.g. after changing your hardware or upgrading your drivers:

| Platform | Path                                                      |
|----------|-----------------------------------------------------------|
| FreeBSD  | `~/.cache/lambdamod/`                                     |
| Linux    | `~/.cache/lambdamod/`                                     |
| Windows  | `%LOCALAPPDATA%\LambdaMod\`                               |
| MacOS    | `~/Library/Cache/LambdaMod/`                              |
