# Basic Setup

This is a tutorial on setting up a simple LambdaMod server.

## Contents

1. [Hosting a Game](#hosting-a-game)
2. [Inviting Players](#inviting-players)

<a name="hosting-a-game"></a>

## Hosting a Game

**Note:** This tutorial does not require port forwarding but may not support all networks. If you run into trouble try the [advanced tutorial](advanced-setup.md).

**Note:** These instructions use an external server. For more information on the implications of this see [here](security.md#sessions).

First create the file `~/.config/lambdamod/server.yml` if it does not exist and make sure it contains the following lines:

```yaml
password: mypassword
adminPassword: secretpassword
savePath: ./save
connectionMode: session
```

Replace `mypassword` and `secretpassword` with a server password of your choosing and an admin password which will provide admin permissions when used in place of the first. Optionally you can also replace `./save` with a custom save path to choose where the server state will be loaded from/stored to. Finally, run `server` to host a game.

<a name="inviting-players"></a>

## Inviting Players

The server will start up and then emit a session URL. something like:

```
https://lambdamod.org/invite?session-id=1234567890abcdef
```

Visit this URL yourself and enter a username and password to connect, using the optional admin password field to supply your admin password if you wish to have admin persmissions.

Finally, distribute this URL to anyone you want to join your server and they will be able to connect via the web UI.
